﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Automation.Utils;

namespace Automation.Utils.Data
{
    public class Execution: IDisposable
    {
        #region Variables

        protected SqlConnection Connection { get; set; }
        public List<SqlParameter> Parameters { get; set; }

        #endregion

        #region Properties

        public virtual string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString;
            }
        }

        #endregion

        #region Methods

        public Execution()
        {
            Connection = new SqlConnection(ConnectionString);
            Parameters = new List<SqlParameter>();
        }

        public virtual object LookUp(string Expression, string Domain)
        {
            object retval = null;
            using (SqlCommand sqlCommand = new SqlCommand(String.Format("SELECT {0} FROM {1}", Expression, Domain), Connection))
            {
                Connection.Open();
                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    if (sqlDataReader.Read())
                        retval = sqlDataReader[0];
                    sqlDataReader.Close();
                }
                Connection.Close();
            }
            return retval;
        }

        public virtual object LookUp(string Expression, string Domain, string Criteria)
        {
            object retval = null;

            using (SqlCommand sqlCommand = new SqlCommand(String.IsNullOrEmpty(Criteria) ? String.Format("SELECT TOP(1) {0} FROM {1}", Expression, Domain) : String.Format("SELECT TOP(1) {0} FROM {1} WHERE {2}", Expression, Domain, Criteria), Connection))
            {
                Connection.Open();
                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    if (sqlDataReader.Read())
                        retval = sqlDataReader[0];
                    sqlDataReader.Close();
                }
                Connection.Close();
            }
            return retval;
        }

        public virtual void Execute(string SQL)
        {
            using (SqlCommand sqlCommand = new SqlCommand(SQL, Connection))
            {
                Connection.Open();
                foreach (SqlParameter p in Parameters)
                {
                    sqlCommand.Parameters.Add(p);
                }
                sqlCommand.ExecuteNonQuery();
                Connection.Close();
            }
        }

        public virtual DataSet Read(string SQLText)
        {
            DataSet dataSet = new DataSet();

            if (!string.IsNullOrEmpty(SQLText))
            {
                using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(new SqlCommand { CommandText = SQLText, Connection = Connection, CommandTimeout = 300 }))
                {
                    Connection.Open();
                    sqlDataAdapter.Fill(dataSet);
                }
                Connection.Close();
            }
            return dataSet;
        }

        public DataTable Structure(string SQLText)
        {
            DataTable retval = new DataTable();
            if (!string.IsNullOrEmpty(SQLText))
            {
                using (SqlCommand sqlCommand = new SqlCommand { CommandText = SQLText, Connection = Connection, CommandTimeout = 300 })
                {
                    if (Connection.State != ConnectionState.Open)
                        Connection.Open();

                    SqlDataReader dr = sqlCommand.ExecuteReader(CommandBehavior.SchemaOnly);

                    retval = new DataTable();
                    for (int i = 0; i < dr.FieldCount; i++)
                    {
                        retval.Columns.Add(dr.GetName(i));
                    }

                    Connection.Close();
                }
            }
            return retval;
        }

        public virtual List<string> GetColumns(string SQLText)
        {
            List<string> retval = new List<string>();

            using (SqlCommand sqlCommand = new SqlCommand(SQLText, Connection))
            {
                Connection.Open();
                try
                {
                    using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.SchemaOnly))
                    {
                        if (sqlDataReader != null)
                            for (int i = 0; i < sqlDataReader.FieldCount; i++)
                            {
                                retval.Add(sqlDataReader.GetName(i));
                            }
                        sqlDataReader.Close();
                    }
                }
                catch (SqlException ex)
                {
                    if (ex.ErrorCode.Equals(-2146232060))
                    {
                        using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.SingleResult))
                        {
                            if (sqlDataReader != null)
                                for (int i = 0; i < sqlDataReader.FieldCount; i++)
                                {
                                    retval.Add(sqlDataReader.GetName(i));
                                }
                            sqlDataReader.Close();
                        }
                    }
                    else
                    {
                        throw ex;
                    }
                }
                Connection.Close();
            }
            return retval;

        }

        public virtual string Scalar(string Function, string Parameters, bool EXEC)
        {
            object retval = string.Empty;

            string SQLText = EXEC ? "Exec " + Function + " " + Parameters : "Select " + Function + "(" + Parameters + ")";

            using (SqlCommand sqlCommand = new SqlCommand(SQLText, Connection))
            {
                Connection.Open();
                retval = sqlCommand.ExecuteScalar();
            }
            Connection.Close();

            return retval != null ? retval.ToString() : string.Empty;
        }

        public virtual void DataTableToDB(string Database, string Schema, string TableName, List<string> TargetColumns, DataTable Table, string SOEID, string OptionalWhere = null)
        {
            DataTable tempTable = Table.Copy();

            string columns = string.Empty;
            foreach (string s in TargetColumns)
            {
                columns += string.IsNullOrEmpty(columns) ? s : ", " + s;
            }

            string fields = string.Empty;
            for (int i = 0; i < TargetColumns.Count; i++)
            {

                fields += string.IsNullOrEmpty(fields) ? "F" + (i + 1).ToString() : ", F" + (i + 1).ToString();
            }
            
            String SQLText = string.Format("INSERT INTO {0}.{1}.{2} ({3}) SELECT {4} FROM @table ", Database, Schema, TableName, columns, fields);

            if (!string.IsNullOrEmpty(OptionalWhere))
                SQLText += OptionalWhere;

            using (SqlCommand sqlCommand = new SqlCommand { CommandText = "", Connection = Connection, CommandType = CommandType.StoredProcedure })
            {
                sqlCommand.Parameters.Add(new SqlParameter { ParameterName = "@table", SqlDbType = SqlDbType.Structured, Value = tempTable });
                sqlCommand.Parameters.Add(new SqlParameter { ParameterName = "@SOEID", SqlDbType = SqlDbType.VarChar, Value = SOEID });
                sqlCommand.Parameters.Add(new SqlParameter { ParameterName = "@SQLText", SqlDbType = SqlDbType.VarChar, Value = SQLText });

                Connection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            Connection.Close();
        }

        public virtual void DataTableToDB(string Database, string Schema, string TableName, List<string> TargetColumns, List<string> SourceFields, DataTable Table, string SOEID, string OptionalWhere = null)
        {
            DataTable tempTable = Table.Copy();

            string columns = string.Empty;
            foreach (string s in TargetColumns)
            {
                columns += string.IsNullOrEmpty(columns) ? s : ", " + s;
            }

            string fields = string.Empty;
            foreach (string s in SourceFields)
            {

                fields += string.IsNullOrEmpty(fields) ? s : ", " + s;
            }

            String SQLText = string.Format("INSERT INTO {0}.{1}.{2} ({3}) SELECT {4} FROM @table ", Database, Schema, TableName, columns, fields);

            if (!string.IsNullOrEmpty(OptionalWhere))
                SQLText += OptionalWhere;

            using (SqlCommand sqlCommand = new SqlCommand { CommandText = SQLText, Connection = Connection, CommandType = CommandType.StoredProcedure })
            {
                sqlCommand.Parameters.Add(new SqlParameter { ParameterName = "@table", SqlDbType = SqlDbType.Structured, Value = tempTable });
                sqlCommand.Parameters.Add(new SqlParameter { ParameterName = "@SOEID", SqlDbType = SqlDbType.VarChar, Value = SOEID });
                sqlCommand.Parameters.Add(new SqlParameter { ParameterName = "@SQLText", SqlDbType = SqlDbType.VarChar, Value = SQLText });

                Connection.Open();
                sqlCommand.ExecuteNonQuery();
            }
            Connection.Close();
        }


        #endregion

        #region IDisposable Members

        public void Dispose()
        {

            if (Connection != null)
            {
                if (Connection.State != ConnectionState.Closed)
                {
                    Connection.Close();
                }
                Connection.Dispose();
                Connection = null;
            }
        }

        #endregion
    }
}
