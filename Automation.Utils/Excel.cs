﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSExcel = Microsoft.Office.Interop.Excel;
using OfficeOpenXml;
using OfficeOpenXml.Utils;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Xml;
using OfficeOpenXml.Style;
using System.Drawing;

namespace Automation.Utils
{
    public class Excel
    {


        public static void ExportToExcel(GridView gridView)
        {
            gridView.Page.Response.Clear();
            gridView.Page.Response.Buffer = true;
            gridView.Page.Response.AddHeader("Content-Disposition", "attachment; filename=DownloadedFile.xls");
            gridView.Page.Response.ContentType = "application/vnd.ms-excel";
            gridView.Page.Response.Charset = "";
            gridView.Page.EnableViewState = false;

           

            System.IO.StringWriter oStringWriter = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter oHtmlTextWriter = new System.Web.UI.HtmlTextWriter(oStringWriter);
            gridView.RenderControl(oHtmlTextWriter);
            gridView.Page.Response.Write(oStringWriter.ToString());
            gridView.Page.Response.End();
        }

        public static void ExportToExcel(SqlDataSource DataSource, UserControl Control)
        {
            Page page = Control.Page;
            page.Response.Clear();
            page.Response.Buffer = true;

            //Create Control
            DataGrid dgGrid = new DataGrid();
            dgGrid.AllowPaging = false;
            dgGrid.DataSource = DataSource;
            dgGrid.HeaderStyle.Font.Bold = true;
            dgGrid.HeaderStyle.BackColor = System.Drawing.Color.Gray;
            dgGrid.DataBind();

            System.IO.StringWriter tw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

            //   Get the HTML for the control.
            dgGrid.RenderControl(hw);

            //   Write the HTML back to the browser.
            page.Response.AddHeader("Content-Disposition", "attachment; filename=DownloadedFile.xls");
            page.Response.ContentType = "application/vnd.ms-excel";
            page.Response.Charset = "";
            page.EnableViewState = false;
            string s = tw.ToString();
            page.Response.Write(s);
            page.Response.End();
        }

        private static void ReleaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        public static bool ToCSV(string sourceFile, string worksheetName, string targetFile)
        {
            if (!File.Exists(sourceFile))
            {
                return false;
            }
            bool converted = false;
            string strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\"" + sourceFile + "\";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\";Persist Security Info=False";
            //string strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=\"" + sourceFile + "\";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\";Persist Security Info=False";
            StreamWriter wrtr = null;

            using (OleDbConnection conn = new OleDbConnection(strConn))
            {
                try
                {
                    conn.Open();
                    using (OleDbCommand cmd = new OleDbCommand("SELECT * FROM [" + worksheetName + "$]", conn))
                    {
                        cmd.CommandType = CommandType.Text;
                        using (OleDbDataAdapter da = new OleDbDataAdapter(cmd))
                        {
                            using (wrtr = new StreamWriter(targetFile))
                            {
                                System.Data.DataTable dt = new System.Data.DataTable();
                                da.Fill(dt);
                                int rowsLimit = dt.Rows.Count > 4001 ? 4001 : dt.Rows.Count;
                                for (int x = 0; x < rowsLimit; x++)
                                {
                                    string rowString = "";
                                    for (int y = 0; y < dt.Columns.Count; y++)
                                    {
                                        rowString += String.IsNullOrEmpty(rowString) ? "\"" + dt.Rows[x][y].ToString() + "\"" : ",\"" + dt.Rows[x][y].ToString() + "\"";
                                    }
                                    if (!String.IsNullOrEmpty(rowString.Replace(",", "").Replace("\"", "")))
                                        wrtr.WriteLine(rowString);
                                }
                                wrtr.Close();
                                converted = true;
                            }
                        }
                    }
                }
                catch
                {
                    converted = false;
                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                }
            }
            return converted;
        }

        public static DataTable ToDataTable(string sourceFile, string worksheetName)
        {
            DataTable retval = null;

            if (!File.Exists(sourceFile))
                return retval;

            //string strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\"" + sourceFile + "\";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\";Persist Security Info=False";
            //string strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=\"" + sourceFile + "\";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\";Persist Security Info=False";
            string strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + sourceFile + ";Extended Properties=Excel 12.0;";
            using (OleDbConnection conn = new OleDbConnection(strConn))
            {
                try
                {
                    conn.Open();
                    using (OleDbCommand cmd = new OleDbCommand("SELECT * FROM [" + worksheetName + "$]", conn))
                    {
                        cmd.CommandType = CommandType.Text;
                        using (OleDbDataAdapter da = new OleDbDataAdapter(cmd))
                        {

                            retval = new System.Data.DataTable();
                            da.Fill(retval);
                        }
                    }
                }
                catch { }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                        conn.Close();
                }
            }
            return retval;
        }

        public static bool ToCSVByAPI(string sourceFile, string worksheetName, string targetFile)
        {
            bool converted = false;
            MSExcel.Application xlApp = null;
            MSExcel.Workbook xlWorkBook = null;
            MSExcel.Worksheet xlWorkSheet = null;
            object misValue = System.Reflection.Missing.Value;
            try
            {
                xlApp = new MSExcel.ApplicationClass();

                if (xlApp == null)
                    return false;

                xlApp.DisplayAlerts = false;
                xlApp.Visible = false;
                xlWorkBook = xlApp.Workbooks.Open(sourceFile,
                    Type.Missing,
                    Type.Missing,
                    Type.Missing,
                    Type.Missing,
                    Type.Missing,
                    Type.Missing,
                    Type.Missing,
                    Type.Missing,
                    Type.Missing,
                    Type.Missing,
                    Type.Missing,
                    Type.Missing,
                    Type.Missing,
                    Type.Missing);

                if (xlWorkBook == null)
                    return false;

                if (xlWorkBook.Worksheets[worksheetName] is MSExcel.Worksheet)
                    xlWorkSheet = xlWorkBook.Worksheets[worksheetName] as MSExcel.Worksheet;

                if (xlWorkSheet == null)
                    return false;

                xlWorkSheet.Select(misValue);

                xlWorkSheet.SaveAs(targetFile,
                    MSExcel.XlFileFormat.xlCSV,
                    misValue,
                    misValue, misValue,
                    false,
                    misValue,
                    misValue,
                    misValue,
                    misValue);

                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Workbooks.Close();

                converted = true;
            }
            catch
            {
                if (xlWorkBook != null) xlWorkBook.Close(true, misValue, misValue);
                converted = false;
            }
            finally
            {
                if (xlApp != null) xlApp.Quit();
                if (xlWorkSheet != null) ReleaseObject(xlWorkSheet);
                if (xlWorkBook != null) ReleaseObject(xlWorkBook);
                if (xlApp != null) ReleaseObject(xlApp);
            }
            return converted;
        }

        public static bool ToExcel(object DataSource, string targetFile)
        {
            bool hasError = true;
            MSExcel.Application xlApp = null;
            MSExcel.Workbook xlWorkBook = null;
            MSExcel.Worksheet xlWorkSheet = null;
            object misValue = System.Reflection.Missing.Value;
            try
            {
                xlApp = new MSExcel.ApplicationClass();

                if (xlApp == null)
                    return hasError = false;

                xlApp.DisplayAlerts = false;
                xlApp.Visible = false;

                xlWorkBook = xlApp.Workbooks.Add(misValue);

                if (xlWorkBook == null)
                    return hasError = false;


                xlWorkSheet = (MSExcel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                if (xlWorkSheet == null)
                    return hasError = false;

                DataTable table = null;
                if (DataSource is DataTable)
                    table = DataSource as DataTable;

                if (DataSource is SqlDataSource)
                    table = (DataSource as SqlDataSource).ToDataTable();

                if (DataSource is ObjectDataSource)
                    table = (DataSource as ObjectDataSource).ToDataTable();

                if (table == null)
                    return hasError = false; ;

                for (int i = 0; i < table.Columns.Count; i++)
                {
                    MSExcel.Range r = xlWorkSheet.Cells[1, i + 1] as MSExcel.Range;
                    r.Value = table.Columns[i].ColumnName;
                }
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    for (int j = 0; j < table.Rows.Count; j++)
                    {
                        MSExcel.Range r = xlWorkSheet.Cells[j + 2, i + 1] as MSExcel.Range;
                        r.Value = table.Rows[j][table.Columns[i]].ToString();
                    }
                }

                xlWorkBook.SaveAs(targetFile,
                    MSExcel.XlFileFormat.xlWorkbookNormal,
                    misValue,
                    misValue,
                    misValue,
                    false,
                    MSExcel.XlSaveAsAccessMode.xlExclusive,
                    misValue,
                    misValue,
                    misValue,
                    misValue,
                    misValue);

                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Workbooks.Close();
                hasError = true;
            }
            catch
            {
                if (xlWorkBook != null)
                    xlWorkBook.Close(true, misValue, misValue);
                hasError = false;
            }
            finally
            {
                if (xlApp != null) xlApp.Quit();
                if (xlWorkSheet != null) ReleaseObject(xlWorkSheet);
                if (xlWorkBook != null) ReleaseObject(xlWorkBook);
                if (xlApp != null) ReleaseObject(xlApp);
            }
            return hasError;
        }

        public static bool ToXMLExcel(object DataSource, string TargetFile)
        {
            bool hasError = true;
            MSExcel.Application xlApp = null;
            MSExcel.Workbook xlWorkBook = null;
            MSExcel.Worksheet xlWorkSheet = null;
            object misValue = System.Reflection.Missing.Value;
            try
            {
                xlApp = new MSExcel.ApplicationClass();

                if (xlApp == null)
                    return hasError = false;

                xlApp.DisplayAlerts = false;
                xlApp.Visible = false;

                xlWorkBook = xlApp.Workbooks.Add(misValue);

                if (xlWorkBook == null)
                    return hasError = false;

                xlWorkSheet = (MSExcel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                if (xlWorkSheet == null)
                    return hasError = false;

                DataTable table = null;
                if (DataSource is DataTable)
                    table = DataSource as DataTable;

                if (DataSource is SqlDataSource)
                    table = (DataSource as SqlDataSource).ToDataTable();

                if (DataSource is ObjectDataSource)
                    table = (DataSource as ObjectDataSource).ToDataTable();

                if (table == null)
                    return hasError = false; ;

                for (int i = 0; i < table.Columns.Count; i++)
                {
                    MSExcel.Range r = xlWorkSheet.Cells[1, i + 1] as MSExcel.Range;
                    r.Value = table.Columns[i].ColumnName;
                }
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    for (int j = 0; j < table.Rows.Count; j++)
                    {
                        MSExcel.Range r = xlWorkSheet.Cells[j + 2, i + 1] as MSExcel.Range;
                        r.Value = table.Rows[j][table.Columns[i]].ToString();
                    }
                }

                xlWorkBook.SaveAs(TargetFile,
                    MSExcel.XlFileFormat.xlXMLSpreadsheet,
                    misValue,
                    misValue,
                    misValue,
                    false,
                    MSExcel.XlSaveAsAccessMode.xlExclusive,
                    misValue,
                    misValue,
                    misValue,
                    misValue,
                    misValue);

                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Workbooks.Close();
                hasError = true;
            }
            catch
            {
                if (xlWorkBook != null)
                    xlWorkBook.Close(true, misValue, misValue);
                hasError = false;
            }
            finally
            {
                if (xlApp != null) xlApp.Quit();
                if (xlWorkSheet != null) ReleaseObject(xlWorkSheet);
                if (xlWorkBook != null) ReleaseObject(xlWorkBook);
                if (xlApp != null) ReleaseObject(xlApp);
            }
            return hasError;
        }

        public static void MadeDownloadable(Control control, string FileName)
        {
            Page page = control.Page;

            if (!string.IsNullOrEmpty(FileName))
            {
                page.Response.Clear();
                page.Response.Buffer = true;

                //   Write the HTML back to the browser.
                page.Response.AddHeader("Content-Disposition", "attachment; filename=DCFC Template.xls");
                page.Response.ContentType = "application/vnd.ms-excel";
                page.Response.Charset = "";
                page.EnableViewState = false;
                if (File.Exists(FileName))
                {
                    page.Response.WriteFile(FileName);
                }
                page.Response.End();
            }
        }

        public static void ExcelXMLPackage(Control control, object DataSource, string PackageFileName)
        {
            Page page = control.Page;

            Guid guid = Guid.NewGuid();
            string FolderPath = control.Page.Server.MapPath("Files") + "\\" + guid.ToString();
            DirectoryInfo directoryInfo = Directory.CreateDirectory(FolderPath);
            FileInfo fileInfo = new FileInfo(control.Page.Server.MapPath(PackageFileName));
            File.Copy(fileInfo.FullName, directoryInfo.FullName + "\\" + fileInfo.Name);
            XML.ToXML(DataSource, directoryInfo.FullName + "\\" + Path.GetFileNameWithoutExtension(fileInfo.Name) + ".xml");
            List<string> files = new List<string>();
            files.Add(directoryInfo.FullName + "\\" + fileInfo.Name);
            files.Add(directoryInfo.FullName + "\\" + Path.GetFileNameWithoutExtension(fileInfo.Name) + ".xml");
            Zip.Create(files, FolderPath + ".zip");
            File.Delete(directoryInfo.FullName + "\\" + Path.GetFileNameWithoutExtension(fileInfo.Name) + ".xml");

            page.Response.Clear();
            page.Response.Buffer = true;

            //   Write the HTML back to the browser.
            page.Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(FolderPath + ".zip"));
            page.Response.ContentType = "application/zip";
            page.Response.Charset = "";
            page.EnableViewState = false;

            if (File.Exists(FolderPath + ".zip"))
            {
                page.Response.WriteFile(FolderPath + ".zip");
            }

            page.Response.End();
            //File.Delete(FolderPath + ".zip");
        }

        public static void ExcelXMLPackage20(Control control, object DataSource, string PackageFileName)
        {
            Page page = control.Page;

            Guid guid = Guid.NewGuid();
            string FolderPath = control.Page.Server.MapPath("Files") + "\\" + guid.ToString();
            DirectoryInfo directoryInfo = Directory.CreateDirectory(FolderPath);
            FileInfo fileInfo = new FileInfo(control.Page.Server.MapPath(PackageFileName));
            File.Copy(fileInfo.FullName, directoryInfo.FullName + "\\" + fileInfo.Name);

            XML.ToXML(DataSource, directoryInfo.FullName + "\\" + Path.GetFileNameWithoutExtension(fileInfo.Name) + ".xml");
            List<string> files = new List<string>();
            files.Add(directoryInfo.FullName + "\\" + fileInfo.Name);
            files.Add(directoryInfo.FullName + "\\" + Path.GetFileNameWithoutExtension(fileInfo.Name) + ".xml");
            Zip.Create(files, FolderPath + ".zip");
            File.Delete(directoryInfo.FullName + "\\" + Path.GetFileNameWithoutExtension(fileInfo.Name) + ".xml");

            page.Response.Clear();
            page.Response.Buffer = true;

            //   Write the HTML back to the browser.
            page.Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(FolderPath + ".zip"));
            page.Response.ContentType = "application/zip";
            page.Response.Charset = "";
            page.EnableViewState = false;

            if (File.Exists(FolderPath + ".zip"))
            {
                page.Response.WriteFile(FolderPath + ".zip");
            }

            page.Response.End();
            //File.Delete(FolderPath + ".zip");
        }

        public static void ToXLSX1(Control control, object DataSource, string PackageFileName)
        {
            Page page = control.Page;
            Guid guid = Guid.NewGuid();
            String filename = page.Server.MapPath("Files") + "\\" + guid.ToString() + ".xlsx";
            FileInfo F = new FileInfo(filename);

            using (ExcelPackage package = new ExcelPackage(F))
            {
                ExcelWorksheet ws = package.Workbook.Worksheets.Add("Data");

                DataTable table = DataSource as DataTable;
                
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    ws.Cells[1, i + 1].Value = table.Columns[i].ColumnName;
                }
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    for (int j = 0; j < table.Rows.Count; j++)
                    {
                        try
                        {
                            List<string> attributes = XML.GetAttributes(table.Rows[j][table.Columns[i]].ToString(), "img", "alt");
                            attributes.AddRange(XML.GetAttributes(table.Rows[j][table.Columns[i]].ToString(), "a", "title"));
                            if (attributes.Count > 0)
                            {
                                ws.Cells[j + 2, i + 1].Value = attributes[0];
                            }
                            else
                            {
                                ws.Cells[j + 2, i + 1].Value = table.Rows[j][table.Columns[i]].ToString();
                            }
                        }
                        catch
                        {
                            ws.Cells[j + 2, i + 1].Value = table.Rows[j][table.Columns[i]].ToString();
                        }
                    }
                }

                ws.Cells.AutoFitColumns();
                var exceltable = ws.Tables.Add(new ExcelAddressBase(1, 1, table.Rows.Count+1, table.Columns.Count), "Download");
                exceltable.TableStyle = OfficeOpenXml.Table.TableStyles.Medium15;

                package.Save();
            }

            string ZipFileName = Path.GetDirectoryName(filename) + "\\" + Path.GetFileNameWithoutExtension(filename) + ".zip";
            Zip.Create(filename, ZipFileName);

            page.Response.Clear();
            page.Response.Buffer = true;

            //   Write the HTML back to the browser.
            page.Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileNameWithoutExtension(filename) + ".zip");
            page.Response.ContentType = "application/zip";
            page.Response.Charset = "";
            page.EnableViewState = false;

            if (File.Exists(ZipFileName))
            {
                page.Response.WriteFile(ZipFileName);
            }

            page.Response.End();

            DeleteOld(control);
            //File.Delete(ZipFileName);
        }

      
        private static void DeleteOld(Control control)
        {
            Page page = control.Page;
            Guid guid = Guid.NewGuid();
            String Directory = page.Server.MapPath("Files") + "\\";
            DirectoryInfo D = new DirectoryInfo(Directory);
            foreach (FileInfo F in D.GetFiles())
            {
                if (F.CreationTime.AddHours(24) > DateTime.UtcNow)
                {
                    F.Delete();
                }
            }
        }

        public static string RequestToExcel(Control control, object DataSource, string PackageFileName)
        {
            Page page = control.Page;
            Guid guid = Guid.NewGuid();
            String filename = page.Server.MapPath("Files") + "\\" + PackageFileName + ".xlsx";
            try
            {
                File.Delete(filename);
            }
            catch
            {
            }
            
            FileInfo F = new FileInfo(filename);
            try
            {
                File.Delete(filename);
            }
            catch
            {
            }
            using (ExcelPackage package = new ExcelPackage(F))
            {
                ExcelWorksheet ws = package.Workbook.Worksheets.Add("Report");
                //ExcelWorksheet ws2 = package.Workbook.Worksheets.Add("All In");

                DataGrid dataGrid = new DataGrid();

                dataGrid.DataSource = DataSource;
                dataGrid.AutoGenerateColumns = true;
                dataGrid.DataBind();

                
                DataTable table = DataSource as DataTable;

                for (int i = 0; i < table.Columns.Count; i++)
                {
                    ws.Cells[1, i + 1].Value = table.Columns[i].ColumnName;

                }
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    for (int j = 0; j < table.Rows.Count; j++)
                    {
                        try
                        {
                            //List<string> attributes = XML.GetAttributes(table.Rows[j][table.Columns[i]].ToString(), "img", "alt");
                            //attributes.AddRange(XML.GetAttributes(table.Rows[j][table.Columns[i]].ToString(), "a", "title"));
                            //if (attributes.Count > 0)
                            //{
                             //   ws.Cells[j + 2, i + 1].Value = attributes[0];
                                
                           // }
                           // else
                           // {
                                ws.Cells[j + 2, i + 1].Value = table.Rows[j][table.Columns[i]];
                                ws.Cells[j + 2, i + 1].Style.Numberformat.Format = "#,##0.00";
                            //}
                        }
                        catch
                        {
                            ws.Cells[j + 2, i + 1].Value = table.Rows[j][table.Columns[i]];
                            ws.Cells[j + 2, i + 1].Style.Numberformat.Format = "#,##0.00";
                        }
                        finally
                        {
                            //ws.Cells[j + 2, i + 1].Style.Locked = false;
                           // ws.Row(j + 2).Style.Locked = false;

                         //   try
                          //  {
                            ws.Row(j + 2).Style.Numberformat.Format = "#,##0.00";
                            //}
                            //catch
                            //{
//
  //                          }
                        }
                    }
                }


                var exceltable = ws.Tables.Add(new ExcelAddressBase(1, 1, table.Rows.Count + 1, table.Columns.Count), "Download");
                exceltable.TableStyle = OfficeOpenXml.Table.TableStyles.Medium15;

               // ws.Cells.AutoFitColumns();
                for (int i = table.Columns.Count + 1; i < ws.Cells.End.Column + 1; i++)
                {
                    ws.Column(i).Hidden = true;
                }

                //ws.Protection.AllowSelectLockedCells = false;
                //ws.Protection.AllowSelectUnlockedCells = false;
                //ws.Protection.AllowAutoFilter = false;
                //ws.Protection.AllowInsertRows = false;
                //ws.Protection.AllowDeleteRows = true;
                //ws.Protection.AllowSort = true;
                //ws.Protection.SetPassword("AR3T3mplat3");
                //ws.Protection.IsProtected = true;
              
                package.Save();

                
            }

            return filename;
          
        }

        public static string GetColumnName(int index)
        {
            const string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            var value = "";

            if (index >= letters.Length)
                value += letters[index / letters.Length - 1];

            value += letters[index % letters.Length];

            return value;
        }

        public static void FasterExcelDropDowns(System.Web.HttpResponseBase Response, DataTable tbl, List<Dictionary<String, Object>> colDef, string fileName, string sheetName = "Template", string pathToSave = "")
        {
            using (ExcelPackage pck = new ExcelPackage())
            {
                //Create the worksheet for Main Data
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add(sheetName);

                //Create the worksheet for Drop Down Data
                ExcelWorksheet wsDropDown = pck.Workbook.Worksheets.Add("DropDownInfoIgnore");

                //Datatable with drop downs info
                DataTable dtDropDowns = new DataTable();
                int maxLenghtOptions = 0;

                //Add columns Drop Downs
                foreach (Dictionary<String, Object> itemColDef in colDef)
                {
                    dtDropDowns.Columns.Add(itemColDef["ColName"].ToString());
                    var tempLenght = ((string[])itemColDef["DropDownList"]).Length;

                    if(tempLenght > maxLenghtOptions)
                    {
                        maxLenghtOptions = tempLenght;
                    }
                }

                //Add DataTable Data
                for (int i = 0; i < maxLenghtOptions; i++)
                {
                    DataRow objTempRow = dtDropDowns.NewRow();

                    //Add rows by columns
                    foreach (Dictionary<String, Object> itemColDef in colDef)
                    {
                        string[] listOptions = (string[])itemColDef["DropDownList"];
                        if(i < listOptions.Length)
                        {
                            objTempRow[itemColDef["ColName"].ToString()] = listOptions[i];
                        }
                    }

                    dtDropDowns.Rows.InsertAt(objTempRow, i);
                }

                //Add dinamic Drop Downs to Excel
                foreach (Dictionary<String, Object> itemColDef in colDef)
                {
                    var excelDropDowns = ws.DataValidations.AddListValidation(itemColDef["ExcelColumn"].ToString());
                    excelDropDowns.Formula.ExcelFormula = itemColDef["ExcelFormula"].ToString();
                }

                //Bind Datatable data with Excel Sheet
                wsDropDown.Cells["A1"].LoadFromDataTable(dtDropDowns, true);

                //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
                ws.Cells["A1"].LoadFromDataTable(tbl, true);
                ws.Column(1).Width = 40;
                ws.Column(3).Width = 70;
                ws.Column(4).Width = 60;
                ws.Column(5).Width = 60;
                ws.Column(6).Width = 70;
                ws.Column(7).Width = 60;
                ws.Column(8).Width = 80;
                ws.Column(9).Width = 70;
                ws.Column(11).Width = 80;
                ws.Column(12).Width = 80;
                ws.Column(13).Width = 80;
                //Format the header for first row
                using (ExcelRange rng = ws.Cells["A1:" + GetColumnName(tbl.Columns.Count - 1) + "1"])
                {
                    rng.Style.Font.Bold = true;
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                    rng.Style.Font.Color.SetColor(Color.White);
                }

                byte[] excelData = pck.GetAsByteArray();

                //@"C:\test1.xlsx";
                if (!String.IsNullOrEmpty(pathToSave))
                {
                    File.WriteAllBytes(pathToSave + fileName + ".xlsx", excelData);
                }

                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=" + fileName + ".xlsx");
                Response.BinaryWrite(excelData);
            }
        }
        public static void FasterExcel(System.Web.HttpResponseBase Response, DataTable tbl, string fileName, string sheetName = "Report", string pathToSave = "")
        {
            using (ExcelPackage pck = new ExcelPackage())
            {
                //Create the worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add(sheetName);

                //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
                ws.Cells["A1"].LoadFromDataTable(tbl, true);
                
                //Format the header for first row
                using (ExcelRange rng = ws.Cells["A1:" + GetColumnName(tbl.Columns.Count - 1) + "1"])
                {
                    rng.Style.Font.Bold = true;
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                    rng.Style.Font.Color.SetColor(Color.White);
                }

                byte[] excelData = pck.GetAsByteArray();

                //@"C:\test1.xlsx";
                if (!String.IsNullOrEmpty(pathToSave))
                {
                    File.WriteAllBytes(pathToSave + fileName + ".xlsx", excelData);
                }

                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=" + fileName + ".xlsx");
                Response.BinaryWrite(excelData);
            }
        }

        public static void FasterExcelCopyServer(System.Web.HttpResponseBase Response, DataTable tbl, string fileName, string pathLocalFolderToSave, string sheetName = "Report", string pathCustomToSave = "")
        {
            //Add extention to file if not present
            if (!fileName.Contains(".xlsx"))
            {
                fileName += ".xlsx";
            }

            //Remove incorrect extention (".xls" in "QueryManagerReport.xls.xlsx")
            fileName = fileName.Replace(".xls.", ".");
            
            using (ExcelPackage pck = new ExcelPackage())
            {
                //Create the worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add(sheetName);

                //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
                ws.Cells["A1"].LoadFromDataTable(tbl, true);
                
                //Format the header for first row
                using (ExcelRange rng = ws.Cells["A1:" + GetColumnName(tbl.Columns.Count - 1) + "1"])
                {
                    rng.Style.Font.Bold = true;
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                    rng.Style.Font.Color.SetColor(Color.White);
                }

                byte[] excelData = pck.GetAsByteArray();

                //@"C:\test1.xlsx";
                if (!String.IsNullOrEmpty(pathCustomToSave))
                {
                    File.WriteAllBytes(pathCustomToSave + fileName, excelData);
                }

                //Save excel temporary in the server
                if (!String.IsNullOrEmpty(pathLocalFolderToSave))
                {
                    //Delete others file
                    var fileGenerationDir = new DirectoryInfo(pathLocalFolderToSave);
                    fileGenerationDir.GetFiles("*", SearchOption.AllDirectories).ToList().ForEach(objFile => { objFile.Delete(); });

                    File.WriteAllBytes(pathLocalFolderToSave + fileName, excelData);
                }

                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=" + fileName);
                Response.TransmitFile(pathLocalFolderToSave + fileName);
                Response.Flush();
                Response.End();
                //Response.BinaryWrite(excelData);
            }
        }

        public static void FasterExcel2(Control WebControl, DataTable tbl, string fileName)
        {
            using (ExcelPackage pck = new ExcelPackage())
            {
                //Create the worksheet
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report");

                //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
                ws.Cells["A1"].LoadFromDataTable(tbl, true);

                ws.Cells.Style.Numberformat.Format = "#,##0.00";
                //Format the header for first row
                using (ExcelRange rng = ws.Cells["A1:" + GetColumnName(tbl.Columns.Count - 1) + "1"])
                {
                    rng.Style.Font.Bold = true;
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                    
                }
                ws.Cells.Style.Numberformat.Format = "#,##0.00";
                ws.Cells["U:U"].Style.Numberformat.Format = "###0";
                ws.Cells["U:U"].Style.Hidden = true;
                ws.Cells["G:O"].AutoFitColumns();


                var exceltable = ws.Tables.Add(new ExcelAddressBase(1, 1, tbl.Rows.Count + 1, tbl.Columns.Count), "Download");
                exceltable.TableStyle = OfficeOpenXml.Table.TableStyles.Medium15;
   

                //Write it back to the client
                WebControl.Page.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                WebControl.Page.Response.AddHeader("content-disposition", "attachment;  filename=" + fileName + ".xlsx");
                WebControl.Page.Response.BinaryWrite(pck.GetAsByteArray());
            }
        }

        public static void Download(Control WebControl, object DataSource, string PackageFileName)
        {
            string filename = RequestToExcel(WebControl,  DataSource,  PackageFileName);

            WebControl.Page.Response.Clear();
            WebControl.Page.Response.Buffer = true;

            //   Write the HTML back to the browser.
            WebControl.Page.Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileNameWithoutExtension(filename) + ".xlsx");
            WebControl.Page.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            WebControl.Page.Response.Charset = "";
            WebControl.Page.EnableViewState = false;

            if (File.Exists(filename))
            {
                WebControl.Page.Response.WriteFile(filename);
            }

            WebControl.Page.Response.End();
        }

        public static string Download(Control WebControl, object DataSource,object DataSource2, string PackageFileName)
        {
            string filename = RequestToExcel(WebControl, DataSource,DataSource2, PackageFileName);

            WebControl.Page.Response.Clear();
            WebControl.Page.Response.Buffer = true;

            //   Write the HTML back to the browser.
            WebControl.Page.Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileNameWithoutExtension(filename) + ".xlsx");
            WebControl.Page.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            WebControl.Page.Response.Charset = "";
            WebControl.Page.EnableViewState = false;

            if (File.Exists(filename))
            {
                WebControl.Page.Response.WriteFile(filename);
            }

            //WebControl.Page.Response.End();

            return filename;
        }

        public static string RequestToExcel(Control control, object DataSource, object DataSource2, string PackageFileName)
        {
            Page page = control.Page;
            Guid guid = Guid.NewGuid();
            String filename = page.Server.MapPath("Files") + "\\" + PackageFileName + ".xlsx";
            FileInfo F = new FileInfo(filename);
            try
            {
                File.Delete(filename);
            }
            catch
            {
            }
            using (ExcelPackage package = new ExcelPackage(F))
            {
                ExcelWorksheet ws = package.Workbook.Worksheets.Add("Potencial SoeId");
                
                DataGrid dataGrid = new DataGrid();

                dataGrid.DataSource = DataSource;
                dataGrid.AutoGenerateColumns = true;
                dataGrid.DataBind();


                DataTable table = DataSource as DataTable;

                for (int i = 0; i < table.Columns.Count; i++)
                {
                    ws.Cells[1, i + 1].Value = table.Columns[i].ColumnName;

                }
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    for (int j = 0; j < table.Rows.Count; j++)
                    {
                        try
                        {
                            List<string> attributes = XML.GetAttributes(table.Rows[j][table.Columns[i]].ToString(), "img", "alt");
                            attributes.AddRange(XML.GetAttributes(table.Rows[j][table.Columns[i]].ToString(), "a", "title"));
                            if (attributes.Count > 0)
                            {
                                ws.Cells[j + 2, i + 1].Value = attributes[0];
                            }
                            else
                            {
                                ws.Cells[j + 2, i + 1].Value = table.Rows[j][table.Columns[i]].ToString();
                            }
                        }
                        catch
                        {
                            ws.Cells[j + 2, i + 1].Value = table.Rows[j][table.Columns[i]].ToString();
                        }
                        finally
                        {
                            //ws.Cells[j + 2, i + 1].Style.Locked = false;
                            ws.Row(j + 2).Style.Locked = false;

                            try
                            {
                                ws.Row(j + 2).Style.Numberformat.Format = "@";
                            }
                            catch
                            {

                            }
                        }
                    }
                }

                var exceltable = ws.Tables.Add(new ExcelAddressBase(1, 1, table.Rows.Count + 1, table.Columns.Count), "Download");
                exceltable.TableStyle = OfficeOpenXml.Table.TableStyles.Medium15;

                ws.Cells.AutoFitColumns();
                for (int i = table.Columns.Count + 1; i < ws.Cells.End.Column + 1; i++)
                {
                    ws.Column(i).Hidden = true;
                }

                //ws.Protection.AllowSelectLockedCells = false;
                //ws.Protection.AllowSelectUnlockedCells = false;
                //ws.Protection.AllowAutoFilter = false;
                //ws.Protection.AllowInsertRows = false;
                //ws.Protection.AllowDeleteRows = true;
                //ws.Protection.AllowSort = true;
                //ws.Protection.SetPassword("AR3T3mplat3");
                //ws.Protection.IsProtected = true;

                ExcelWorksheet ws2 = package.Workbook.Worksheets.Add("Detail");

                DataGrid dataGrid2 = new DataGrid();

                dataGrid2.DataSource = DataSource;
                dataGrid2.AutoGenerateColumns = true;
                dataGrid2.DataBind();


                DataTable table2 = DataSource2 as DataTable;

                for (int i = 0; i < table2.Columns.Count; i++)
                {
                    ws2.Cells[1, i + 1].Value = table2.Columns[i].ColumnName;

                }
                for (int i = 0; i < table2.Columns.Count; i++)
                {
                    for (int j = 0; j < table2.Rows.Count; j++)
                    {
                        try
                        {
                            List<string> attributes = XML.GetAttributes(table2.Rows[j][table2.Columns[i]].ToString(), "img", "alt");
                            attributes.AddRange(XML.GetAttributes(table2.Rows[j][table2.Columns[i]].ToString(), "a", "title"));
                            if (attributes.Count > 0)
                            {
                                ws2.Cells[j + 2, i + 1].Value = attributes[0];
                            }
                            else
                            {
                                ws2.Cells[j + 2, i + 1].Value = table2.Rows[j][table2.Columns[i]].ToString();
                            }
                        }
                        catch
                        {
                            ws2.Cells[j + 2, i + 1].Value = table2.Rows[j][table2.Columns[i]].ToString();
                        }
                        finally
                        {
                            //ws2.Cells[j + 2, i + 1].Style.Locked = false;
                            ws2.Row(j + 2).Style.Locked = false;

                            try
                            {
                                ws2.Row(j + 2).Style.Numberformat.Format = "@";
                            }
                            catch
                            {

                            }
                        }
                    }
                }

                var exceltable2 = ws2.Tables.Add(new ExcelAddressBase(1, 1, table2.Rows.Count + 1, table2.Columns.Count), "Download2");
                exceltable2.TableStyle = OfficeOpenXml.Table.TableStyles.Medium15;

                ws2.Cells.AutoFitColumns();
                for (int i = table2.Columns.Count + 1; i < ws2.Cells.End.Column + 1; i++)
                {
                    ws2.Column(i).Hidden = true;
                }


                package.Save();


            }

            return filename;

        }

        public static void ToXLSX12(Control control, object DataSource, string PackageFileName)
        {
            Page page = control.Page;
            Guid guid = Guid.NewGuid();
            String filename = page.Server.MapPath("Files") + "\\" + guid.ToString() + ".xlsx";
            FileInfo F = new FileInfo(filename);

            using (ExcelPackage package = new ExcelPackage(F))
            {
                ExcelWorksheet ws = package.Workbook.Worksheets.Add("Data");
                ;
                DataGrid dataGrid = new DataGrid();

                dataGrid.DataSource = DataSource;
                dataGrid.AutoGenerateColumns = true;
                dataGrid.DataBind();


                DataTable table = DataSource as DataTable;

                for (int i = 0; i < table.Columns.Count; i++)
                {
                    ws.Cells[1, i + 1].Value = table.Columns[i].ColumnName;
                    
                }
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    for (int j = 0; j < table.Rows.Count; j++)
                    {
                        try
                        {
                            List<string> attributes = XML.GetAttributes(table.Rows[j][table.Columns[i]].ToString(), "img", "alt");
                            attributes.AddRange(XML.GetAttributes(table.Rows[j][table.Columns[i]].ToString(), "a", "title"));
                            if (attributes.Count > 0)
                            {
                                ws.Cells[j + 2, i + 1].Value = attributes[0];
                            }
                            else
                            {
                                ws.Cells[j + 2, i + 1].Value = table.Rows[j][table.Columns[i]].ToString();
                            }
                        }
                        catch
                        {
                            ws.Cells[j + 2, i + 1].Value = table.Rows[j][table.Columns[i]].ToString();
                        }
                        finally
                        {
                            //ws.Cells[j + 2, i + 1].Style.Locked = false;
                            ws.Row(j + 2).Style.Locked = false;

                            try
                            {
                                ws.Row(j + 2).Style.Numberformat.Format = "@";
                            }
                            catch
                            {

                            }
                        }
                    }
                }

                var exceltable = ws.Tables.Add(new ExcelAddressBase(1, 1, table.Rows.Count + 1, table.Columns.Count), "Download");
                exceltable.TableStyle = OfficeOpenXml.Table.TableStyles.Medium15;
                
                ws.Cells.AutoFitColumns();
                for (int i = table.Columns.Count + 1; i < ws.Cells.End.Column + 1; i++)
                {
                    ws.Column(i).Hidden = true;
                }
                
                //ws.Protection.AllowSelectLockedCells = false;
                //ws.Protection.AllowSelectUnlockedCells = false;
                //ws.Protection.AllowAutoFilter = false;
                //ws.Protection.AllowInsertRows = false;
                //ws.Protection.AllowDeleteRows = true;
                //ws.Protection.AllowSort = true;
                //ws.Protection.SetPassword("AR3T3mplat3");
                //ws.Protection.IsProtected = true;

                package.Save();
            }

            string ZipFileName = Path.GetDirectoryName(filename) + "\\" + Path.GetFileNameWithoutExtension(filename) + ".zip";
            Zip.Create(filename, ZipFileName);

            page.Response.Clear();
            page.Response.Buffer = true;

            //   Write the HTML back to the browser.
            page.Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileNameWithoutExtension(filename) + ".zip");
            page.Response.ContentType = "application/zip";
            page.Response.Charset = "";
            page.EnableViewState = false;

            if (File.Exists(ZipFileName))
            {
                page.Response.WriteFile(ZipFileName);
            }

            page.Response.End();

            DeleteOld(control);
            //File.Delete(ZipFileName);
        }
    }
}