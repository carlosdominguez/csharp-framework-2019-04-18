﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions; 
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web;

namespace Automation.Utils
{
    /// <summary>
    /// 
    /// </summary>
    public static class Extension
    {

        #region System.Data.DataTable Extensions

        /// <summary>  
        /// Convert a List{T} to a DataTable.  
        /// </summary>  
        public static DataTable ToDataTable(this ObjectDataSource DataSource)
        {
            //var ds = new DataSet();
            DataSource.DataBind();
            var dv = (DataView)DataSource.Select();
            if (dv != null && dv.Count > 0)
                return dv.ToTable();
            return null;
        }

        ///// <summary>  
        ///// Convert a List{T} to a DataTable.  
        ///// </summary>  
        //private DataTable ToDataTable(this LinqDataSource DataSource)
        //{
        //    var ds = new DataSet();
        //    var dv = (DataView)DataSource.GetView().Select();
        //    if (dv != null && dv.Count > 0)
        //        return dv.ToTable();
        //    return null;
        //}

        /// <summary>  
        /// Convert a List{T} to a DataTable.  
        /// </summary>  
        public static DataTable ToDataTable(this SqlDataSource DataSource)
        {
            //var ds = new DataSet();
            DataSource.DataBind();
            var dv = (DataView)DataSource.Select(null);
            if (dv != null && dv.Count > 0)
                return dv.ToTable();
            return null;
        }

        /// <summary>  
        /// Convert a List{T} to a DataTable.  
        /// </summary>  
        public static DataTable ToDataTable(this DataTable DataSource)
        {
            return DataSource;
        }

        /// <summary>  
        /// Convert a List{T} to a DataTable.  
        /// </summary>  
        public static DataTable ToDataTable<T>(this List<T> items)
        {
            var tb = new DataTable(typeof(T).Name);
            PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in props)
            {
                Type t = GetCoreType(prop.PropertyType);
                tb.Columns.Add(prop.Name, t);
            }

            foreach (T item in items)
            {
                var values = new object[props.Length];
                for (int i = 0; i < props.Length; i++)
                {
                    values[i] = props[i].GetValue(item, null);
                }
                tb.Rows.Add(values);
            }

            return tb;
        }


        /// <summary>  
        /// Convert a List{T} to a DataTable.  
        /// </summary>  
        public static DataRow NewRow<T>(this List<T> items)
        {
            var tb = new DataTable(typeof(T).Name);
            PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in props)
            {
                Type t = GetCoreType(prop.PropertyType);
                tb.Columns.Add(prop.Name, t);
            }

            return tb.NewRow();
        }

        /// <summary>  
        /// Determine of specified type is nullable  
        /// </summary>  
        private static bool IsNullable(Type t)
        {
            return !t.IsValueType || (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>));
        }

        /// <summary>  
        /// Return underlying type if type is Nullable otherwise return the type  
        /// </summary>  
        public static Type GetCoreType(Type t)
        {
            if (t != null && IsNullable(t))
            {
                if (!t.IsValueType)
                {
                    return t;
                }

                else
                {
                    return Nullable.GetUnderlyingType(t);
                }
            }
            else
            {
                return t;
            }
        }

        #endregion

        #region System.String Extensions

        public static bool Like(this string str, string Pattern, RegexOptions Options = RegexOptions.IgnoreCase)
        {
            return Regex.IsMatch(str, Pattern, Options);
        }

        public static bool IsValidEmail(this String str)
        {
            if (!String.IsNullOrEmpty(str.Trim()))
            {
                System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[com|org|net]{2,3})$");
                if (!regex.Match(str).Success)
                    return false;
            }
            else
                return false;
            return true;
        }

        public static bool IsDate(this String str)
        {
            if (!String.IsNullOrEmpty(str.Trim()))
            {
                DateTime chkDate;
                if (!DateTime.TryParse(str, out chkDate))
                    return false;
            }
            else
                return false;
            return true;
        }

        public static bool IsNullOrEmpty(this String str)
        {
            return string.IsNullOrEmpty(str);
        }

        public static Control FindControl(this String ID, Control Control)
        {
            if (Control != null && !string.IsNullOrEmpty(ID))
            {
                Control retval = Control.FindControl(ID);
                if (retval != null)
                    return retval;
                else
                    return FindControl(ID, Control.Parent);
            }
            return null;
        }

        #endregion

        #region System.Web.UI.GridView Extensions

        public static void ToExcel(this Control GridView, object DataSource)
        {
            Page page = GridView.Page;

            if (page == null)
                return;
            if (GridView == null)
                return;

            page.Response.Clear();
            page.Response.Buffer = true;

            //   Create Control
            DataGrid dgGrid = new DataGrid();
            dgGrid.AllowPaging = false;
            dgGrid.DataSource = DataSource;
            dgGrid.AutoGenerateColumns = true;
            dgGrid.HeaderStyle.Font.Bold = true;
            dgGrid.HeaderStyle.BackColor = System.Drawing.Color.Gray;

            dgGrid.DataBind();

            System.IO.StringWriter tw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

            //   Get the HTML for the control.
            dgGrid.RenderControl(hw);

            //   Write the HTML back to the browser.
            page.Response.AddHeader("Content-Disposition", "attachment; filename=DownloadedFile.xls");
            page.Response.ContentType = "application/vnd.ms-excel";
            page.Response.Charset = "";
            page.EnableViewState = false;
            string s = tw.ToString();
            page.Response.Write(s);
            page.Response.End();
        }

        public static void ToExcel(this GridView GridView, object DataSource)
        {
            Page page = GridView.Page;

            if (page == null)
                return;
            if (GridView == null)
                return;

            page.Response.Clear();
            page.Response.Buffer = true;

            //   Create Control
            DataGrid dgGrid = new DataGrid();
            dgGrid.AllowPaging = false;
            dgGrid.DataSource = DataSource;
            dgGrid.AutoGenerateColumns = false;
            dgGrid.HeaderStyle.Font.Bold = true;
            dgGrid.HeaderStyle.BackColor = System.Drawing.Color.Gray;

            foreach (DataControlField d in GridView.Columns)
            {
                if (!string.IsNullOrEmpty(d.SortExpression))
                {
                    BoundColumn b = new BoundColumn();
                    b.HeaderText = d.HeaderText;
                    b.DataField = d.SortExpression;
                    dgGrid.Columns.Add(b);
                }
            }
            dgGrid.DataBind();

            System.IO.StringWriter tw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

            //   Get the HTML for the control.
            dgGrid.RenderControl(hw);

            //   Write the HTML back to the browser.
            page.Response.AddHeader("Content-Disposition", "attachment; filename=DownloadedFile.xls");
            page.Response.ContentType = "application/vnd.ms-excel";
            page.Response.Charset = "";
            page.EnableViewState = false;
            string s = tw.ToString();
            page.Response.Write(s);
            page.Response.End();
        }

        public static void ExportToExcel(this Control Control, object DataSource)
        {
            string filename = HttpRuntime.AppDomainAppPath + "Files\\" + Guid.NewGuid().ToString() + ".xls";

            Page page = Control.Page;
            try
            {
                page.Response.Clear();
                page.Response.Buffer = true;

                System.IO.StringWriter tw = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

                //   Get the HTML for the control.

                //   Write the HTML back to the browser.
                page.Response.AddHeader("Content-Disposition", "attachment; filename=DownloadedFile.xls");
                page.Response.ContentType = "application/vnd.ms-excel";
                page.Response.Charset = "";
                page.EnableViewState = false;

                if (Excel.ToExcel(DataSource, filename))
                    page.Response.WriteFile(filename);
            }
            finally
            {
                page.Response.Flush();
                page.Response.End();
                File.Delete(filename);
            }
        }

        #endregion

        #region System.Web.UI.ListItemCollection Extensions

        public static void SelectAll(this ListItemCollection ListItemCollection)
        {
            foreach (ListItem i in ListItemCollection)
            {
                i.Selected = true;
            }
        }

        public static void SelectAll(this ListItemCollection ListItemCollection, bool Checked)
        {
            foreach (ListItem i in ListItemCollection)
            {
                i.Selected = Checked;
            }
        }

        public static void AddAttributes(this ListItemCollection ListItemCollection, string key, string value)
        {
            foreach (ListItem i in ListItemCollection)
            {
                i.Attributes.Add(key, value);
            }
        }

        public static string GetAllText(this ListItemCollection ListItemCollection, string separator)
        {
            string retval = string.Empty;
            foreach (ListItem i in ListItemCollection)
            {
                retval += string.IsNullOrEmpty(retval) ? i.Text : separator + " " + i.Text;
            }
            return retval;
        }

        public static string GetAllText(this ListItemCollection ListItemCollection)
        {
            string retval = string.Empty;
            foreach (ListItem i in ListItemCollection)
            {
                retval += string.IsNullOrEmpty(retval) ? i.Text : ", " + i.Text;
            }
            return retval;
        }

        public static bool IsAllSelected(this ListItemCollection ListItemCollection)
        {
            bool retval = true;
            foreach (ListItem i in ListItemCollection)
            {
                retval &= i.Selected;
            }
            return retval;
        }

        public static bool IsNoneSelected(this ListItemCollection ListItemCollection)
        {
            bool retval = false;
            foreach (ListItem i in ListItemCollection)
            {
                retval |= i.Selected;
            }
            return !retval;
        }

        #endregion

        #region System.Web.UI.Control Extensions

        public static UpdatePanel GetUpdatePanel(this Control Control)
        {
            if (Control != null)
            {
                //foreach (Control c in Control.Controls)
                //{
                if (Control is UpdatePanel)
                    return Control as UpdatePanel;
                //}
                return Control.Parent.GetUpdatePanel();
            }
            else
                return null;
        }

        public static UpdatePanel FindUpdatePanel(this Control Control)
        {
            if (Control != null)
            {
                if (Control is UpdatePanel)
                    return Control as UpdatePanel;
                if (Control.Parent != null)
                    return Control.Parent.FindUpdatePanel();
                else
                    return null;
            }
            else
                return null;
        }

        public static UserControl GetUserControl(this Control Control)
        {
            if (Control != null)
            {
                if (Control is UserControl)
                    return Control as UserControl;
                return Control.Parent.GetUserControl();
            }
            else
                return null;
        }

        public static UserControl FindUserControl(this Control Control)
        {
            if (Control != null)
            {
                if (Control is UserControl)
                    return Control as UserControl;
                if (Control.Parent != null)
                    return Control.Parent.FindUserControl();
                else
                    return null;
            }
            else
                return null;
        }

        public static Control FindParentControl(this Control control, string ControlID)
        {
            if (string.IsNullOrEmpty(ControlID))
                return null;
            if (control != null)
            {
                Control aux = control.FindControl(ControlID);
                if (aux != null)
                    return aux;
                return FindParentControl(control.Parent, ControlID);
            }
            else
                return null;
        }

        #endregion

        #region System.Web.UI.Control Extensions
        /// <summary>
        /// Perform a deep Copy of the object.
        /// </summary>
        /// <typeparam name="T">The type of object being copied.</typeparam>
        /// <param name="source">The object instance to copy.</param>
        /// <returns>The copied object.</returns>
        public static T Clone<T>(this T source)
        {
            if (!typeof(T).IsSerializable)
            {
                throw new ArgumentException("The type must be serializable.", "source");
            }

            // Don't serialize a null object, simply return the default for that object
            if (Object.ReferenceEquals(source, null))
            {
                return default(T);
            }

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new MemoryStream();
            using (stream)
            {
                formatter.Serialize(stream, source);
                stream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }
        #endregion

        #region LoadingNotifier

        public static void InitNotify(this Control control, string Splash)
        {

            // Only do this on the first call to the page
            if ((!control.Page.IsCallback) && (!control.Page.IsPostBack))
            {
                //Register loadingNotifier.js for showing the Progress Bar
                control.Page.Response.Write(string.Format(@"
                          <script language='javascript' type='text/javascript'>
                          initLoader('{0}');
                          </script>", Splash));
                // Send it to the client
                //control.Page.Response.Flush();
            }

        }

        public static void Notify(this Control control, string Percent, string Message)
        {

            // Only do this on the first call to the page
            if ((!control.Page.IsCallback) && (!control.Page.IsPostBack))
            {
                //Update the Progress bar

                control.Page.Response.Write(string.Format("<script language='javascript' type='text/javascript'>setProgress({0},'{1}'); </script>", Percent, Message));
                //control.Page.Response.Flush();
            }
        }

        #endregion
    }
}