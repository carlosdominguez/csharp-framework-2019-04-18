﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web.UI.WebControls;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;

namespace Automation.Utils
{
    public class XML
    {

        public static List<string> GetAttributes(string htmlSource, string Tag ,string Attribute)
        {
            List<string> Attributes = new List<string>();
            string regexImgSrc = String.Format(@"<{0}[^>]*?{1}\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>", Tag, Attribute);
            MatchCollection matchesImgSrc = Regex.Matches(htmlSource, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            foreach (Match m in matchesImgSrc)
            {
                //string href = m.Groups[1].Value;
                Attributes.Add(m.Groups[1].Value);
            }
            return Attributes;
        }

        public static bool ToXML(object DataSource, string TargetFile)
        {
            bool hasError = true;
            
            try
            {
                DataTable table = null;
                if (DataSource is DataTable)
                    table = DataSource as DataTable;

                if (DataSource is SqlDataSource)
                    table = (DataSource as SqlDataSource).ToDataTable();

                if (DataSource is ObjectDataSource)
                    table = (DataSource as ObjectDataSource).ToDataTable();

                if (table == null)
                    return hasError = false; ;

                table.TableName = "Output";
                table.WriteXml(TargetFile);
                hasError = true;
            }
            catch
            {
                hasError = false;
            }
            finally
            {
                
            }
            return hasError;
        }

        public static XmlDocument ToXMLDocument(object DataSource)
        {
            try
            {
                DataTable table = null;
                if (DataSource is DataTable)
                    table = DataSource as DataTable;

                if (DataSource is SqlDataSource)
                    table = (DataSource as SqlDataSource).ToDataTable();

                if (DataSource is ObjectDataSource)
                    table = (DataSource as ObjectDataSource).ToDataTable();

                //if (DataSource is EntityDataSource)
                //    table = (DataSource as ObjectDataSource).ToDataTable();

                if (table == null)
                    table = new DataTable();

                table.TableName = "Output";
                StringWriter stringWriter = new StringWriter();

                table.WriteXml(stringWriter, true);

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(stringWriter.ToString());
                return xmlDoc;
            }
            catch
            {
                return null;
            }            
        }

        public static string ToXMLString(object DataSource)
        {
            try
            {
                DataTable table = null;
                if (DataSource is DataTable)
                    table = DataSource as DataTable;

                if (DataSource is SqlDataSource)
                    table = (DataSource as SqlDataSource).ToDataTable();

                if (DataSource is ObjectDataSource)
                    table = (DataSource as ObjectDataSource).ToDataTable();

                //if (DataSource is EntityDataSource)
                //    table = (DataSource as ObjectDataSource).ToDataTable();

                if (table == null)
                    table = new DataTable();

                table.TableName = "Output";
                StringWriter stringWriter = new StringWriter();

                table.WriteXml(stringWriter, true);

                return stringWriter.ToString();
            }
            catch
            {
                return string.Empty;
            }            
        }
    }
}
