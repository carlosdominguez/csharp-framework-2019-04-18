﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.JqxGrid
{
    public class JqxGrid
    {
        public string showTo { get; set; }
        public JqxSp sp { get; set; }
        public List<Dictionary<string, object>> columns { get; set; }
        public string url { get; set; }
        public string width { get; set; }
    }
}
