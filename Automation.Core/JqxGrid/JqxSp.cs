﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.JqxGrid
{
    public class JqxSp
    {
        public string SQL { get; set; }
        public string Name { get; set; }
        public List<Dictionary<string, string>> Params { get; set; }
    }
}
