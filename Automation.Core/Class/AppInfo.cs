﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Class
{
    public class AppInfo
    {
        public string CSID { get; set; }
        public string KeyName { get; set; }
        public string ShortName { get; set; }
        public string FullName { get; set; }
        public string AppOwnerSOEID { get; set; }
        public string AppOwnerName { get; set; }
        public string AutomationID { get; set; }
        public string AutomationName { get; set; }
        public bool IsDefault { get; set; }
        public bool StructureOkInProject { get; set; }
    }
}
