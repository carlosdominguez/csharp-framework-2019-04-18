﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Class
{
    public class FileUpload
    {
        public string Filename { get; set; }
        public Stream InputStream { get; set; }
    }
}
