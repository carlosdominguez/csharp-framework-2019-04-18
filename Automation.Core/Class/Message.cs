﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Class
{
    public class Message
    {
        public string ID { get; set; }
        public string Type { get; set; }
        public string Msg { get; set; }

        public string GetClassMessage()
        {
            string strClass = "";

            switch (Type)
            {
                case "Warning":
                    strClass = "alert-warning";
                    break;

                case "Success":
                    strClass = "alert-success";
                    break;

                case "Info":
                    strClass = "alert-info";
                    break;

                case "Error":
                    strClass = "alert-error";
                    break;

                case "Primary":
                    strClass = "alert-primary";
                    break;

                default:
                    strClass = "alert-default";
                    break;
            }

            return strClass;
        }
    }
}
