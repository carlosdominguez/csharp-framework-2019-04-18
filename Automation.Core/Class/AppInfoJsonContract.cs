﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Class
{
    public class AppInfoJsonContract : DefaultContractResolver
    {
        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            IList<JsonProperty> props = base.CreateProperties(type, memberSerialization);
            var propsFilter = props.Where(p =>
                (p.PropertyName != "IsDefault" &&
                  p.PropertyName != "StructureOkInProject")
            ).ToList();
            return propsFilter;
        }
    }
}
