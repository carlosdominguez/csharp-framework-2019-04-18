﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Reflection;
using Newtonsoft.Json;
using System.Diagnostics;
using Automation.Core.Model;
using System.Data.SqlClient;
using System.IO;
using System.Data.OleDb;
using System.Web.Configuration;
using System.Globalization;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;

namespace Automation.Core.Class
{
    public static class GlobalUtil
    {
        public static string PathGlobalConfig = "~/Content/Shared/config/automation-global.config";
        public static bool SOEIDWasChanged = false;
        public static bool ForceSessionRefresh = false;
        public static Dictionary<String, Boolean> listOfDLGuidSavedInDB = new Dictionary<string, bool>();

        // ---------------------------------------------
        // Util Methods
        // ---------------------------------------------
        public static string GetSOEID()
        {
            string SOEID = "";
            
            //User testing
            string psoeid = "";

            //Query String Simulate User
            string psim = HttpContext.Current.Request.QueryString["psim"];
            if (!String.IsNullOrEmpty(psim))
            {
                psoeid = DecodeAsciiString(psim);
            }

            //Query String User
            if (String.IsNullOrEmpty(psoeid))
            {
                psoeid = HttpContext.Current.Request.QueryString["psoeid"];
            }
            
            if (!String.IsNullOrEmpty(psoeid))
            {
                HttpContext.Current.Session["TestUser"] = psoeid;
                SOEID = psoeid;
            }
            else
            {
                if (HttpContext.Current.Session != null && HttpContext.Current.Session["TestUser"] != null)
                {
                    SOEID = HttpContext.Current.Session["TestUser"].ToString();
                }
            }

            string pclear = HttpContext.Current.Request.QueryString["pclear"];
            if (!String.IsNullOrEmpty(pclear))
            {
                HttpContext.Current.Session.Remove("TestUser");
                SOEID = "";
                //We can call this every where, but only once by post back is executed "sessionWasRefreshed"
                //RefreshSession();
            }

            //Priority to queryString
            if (!String.IsNullOrEmpty(SOEID))
            {
                return SOEID;
            }

            //SSO User
            if (HttpContext.Current.Request.Headers.Get("SM_User") != null)
            {
                SOEID = HttpContext.Current.Request.Headers.Get("SM_User").ToString();
            }

            //Try with Windows User if SSO not exists
            if (String.IsNullOrEmpty(SOEID))
            {
                string UserLine = HttpContext.Current.User.Identity.Name.ToString();
                if (!String.IsNullOrEmpty(UserLine))
                {
                    string[] UserRow = UserLine.Split('\\');
                    SOEID = UserRow[1].ToUpper();
                }
            }

            //Try with system environment
            if (String.IsNullOrEmpty(SOEID))
            {
                SOEID = System.Environment.UserName.ToString().ToUpper();
            }

            if (!String.IsNullOrEmpty(SOEID))
            {
                return SOEID;
            }
            else
            {
                AddSessionMessage("Error", "SOEID not found in Session.", true);
                return "";
            }
        }

        public static void CheckIfSOEIDOrAppKeyWasChaged()
        {
            var SessionWasRefresh = false;

            //If the SOEID is different for the UserInfo in the session set new session
            if (HttpContext.Current.Session != null && HttpContext.Current.Session["UserInfo"] != null && GetSOEID().ToUpper() != ((Dictionary<string, string>)HttpContext.Current.Session["UserInfo"])["SOEID"].ToUpper())
            {
                SOEIDWasChanged = true;
                SessionStart(true);
                SOEIDWasChanged = false;
                SessionWasRefresh = true;
            }

            //Validate if AppKey changed
            if (HttpContext.Current.Session != null && HttpContext.Current.Session["AppKey"] != null && HttpContext.Current.Session["AppKey"].ToString() != GetAppKey() && SessionWasRefresh == false)
            {
                SOEIDWasChanged = true;
                SessionStart(true);
                SOEIDWasChanged = false;
            }

            //Set App Key Value
            if (HttpContext.Current.Session != null)
            {
                HttpContext.Current.Session["AppKey"] = GetAppKey();
            }
        }

        public static void AddSessionMessage(string ptype, string pmessage, bool addStackInfo = false, Exception pex = null, string idMessage = "Default")
        {
            if (pex == null)
            {
                //Add stackTrace
                if (addStackInfo)
                {
                    string messageStack = "";
                    StackTrace stackTrace = new StackTrace(true);
                    var listStack = stackTrace.ToString().Split(new string[] { "\r\n" }, StringSplitOptions.None);

                    for (int i = 1; i < 7 && i < listStack.Length; i++)
                    {
                        messageStack += listStack[i] + "<br />";
                    }
                    pmessage += "<pre>" + messageStack + "</pre>";
                }
            }
            else
            {
                pmessage += "<pre>" + GetExceptionMessage(pex) + "</pre>";

                //Save Exception in DB
                SaveExceptionInDB(pex);
            }

            if (HttpContext.Current.Session["Message"] != null)
            {
                bool existsIdMessage = false;
                List<Message> list = (List<Message>)HttpContext.Current.Session["Message"];

                //Check if idMessage exists
                foreach (var objMessage in list)
                {
                    if (objMessage.ID == idMessage)
                    {
                        existsIdMessage = true;
                        break;
                    }
                }

                if (existsIdMessage == false)
                {
                    list.Add(new Message { Type = ptype, Msg = pmessage, ID = idMessage });
                    HttpContext.Current.Session["Message"] = list;
                }
            }
            else
            {
                HttpContext.Current.Session["Message"] = new List<Message> { 
                    new Message { Type = ptype, Msg = pmessage, ID = idMessage }
                };
            }
        }

        public static void SessionStart(bool pforceRefresh = false)
        {
            ForceSessionRefresh = pforceRefresh;

            if (IsOkToolConfiguration())
            {
                var DBIsConfigured = GetValueAppConfig("AppSettings", "DBIsConfigured");
                if (DBIsConfigured == "1")
                {
                    SetNewSessionVariables();
                }
                else
                {
                    //Load user information
                    var userInfo = GetUserInfo();
                    HttpContext.Current.Session["UserInfo"] = userInfo;

                    var DBIsConfiguredShowMsg = GetValueAppConfig("AppSettings", "DBIsConfiguredShowMsg");
                    if (DBIsConfiguredShowMsg == "1")
                    {
                        var appName = GetCurrentAppInfo().KeyName;
                        AddSessionMessage("info", "Please go to the Menu in the option 'Apps Manager', select '" + appName + "' then click in 'Review database' button, and 'Create missing objects in DB' and click 'Save' to create the basic structure.");
                    }
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect(GetSubAppPath() + "/Error/Offline");
            }

            if (ForceSessionRefresh)
            {
                ForceSessionRefresh = false;
            }
        }

        public static void SetNewSessionVariables()
        {
            try
            {
                //Set default Role 
                var spValidateUserLogin = GetDBObjectFullName("ValidateUserLogin");
                (new GlobalModel()).excecuteProcedureNoReturn(spValidateUserLogin, new List<Dictionary<string, string>> { 
                    new Dictionary<string, string>
                    {
                        { "Name", "@SOEID" },
                        { "Value", GetSOEID() }
                    },
                    new Dictionary<string, string>
                    {
                        { "Name", "@RequestedURL" },
                        { "Value", HttpContext.Current.Request.Url.AbsoluteUri }
                    }
                });

                //Load user information
                var userInfo = GetUserInfo();
                HttpContext.Current.Session["UserInfo"] = userInfo;
            }
            catch (Exception e)
            {
                string msg = "";
                msg += "Setting Session information error <br />";
                AddSessionMessage("Error", msg, false, e);
            }
            
        }

        public static Dictionary<string, string> GetUserInfo()
        {
            Dictionary<string, string> userInfo = null;
            List<Dictionary<string, string>> permits = new List<Dictionary<string, string>>();
            try
            {
                if (HttpContext.Current.Session["UserInfo"] != null && SOEIDWasChanged == false && ForceSessionRefresh == false)
                {
                    userInfo = (Dictionary<string, string>)HttpContext.Current.Session["UserInfo"];
                }
                else 
                {
                    var DBIsConfigured = GetValueAppConfig("AppSettings", "DBIsConfigured");
                    List<Dictionary<string, string>> resultList = null;
                    if (DBIsConfigured == "1")
                    {   
                        //Update connetion string for entities
                        UpdateEntitiesConnString();

                        //Load user information
                        var spUserInfo = GetDBObjectFullName("UserInfo");
                        resultList = (new GlobalModel()).excecuteProcedureReturn(spUserInfo, new List<Dictionary<string, string>> { 
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEID" },
                                { "Value", GetSOEID() }
                            }
                        });

                        //When user not found load default information from Automation
                        if(resultList.Count == 0)
                        {
                            var tempSql =
                                "SELECT " +
                                "   T.[SOEID], " +
                                "   T.[GEID], " +
                                "   T.[EMAIL_ADDR]	                        AS [Email], " +
                                "   T.[FIRST_NAME] + ' ' + T.[LAST_NAME]    AS [Name], " +
                                "   'TEMP_ROLE'                             AS [Roles], " +
                                "   'Temporary Role'                        AS [RolesDesc], " +
                                "   '0'                                     AS [Organizations], " +
                                "   'TBD'                                   AS [OrganizationsDesc], " +
                                "   'TBD'                                   AS [Center] " +
                                "FROM " +
                                "   [Automation].[dbo].[tblEmployee] T WITH(READUNCOMMITTED) " +
                                "WHERE " +
                                "   [SOEID] = '" + GetSOEID() + "'";

                            resultList = (new GlobalModel()).getResultListBySQL(tempSql);
                        }

                        //Load permits of user
                        var spGetPermitsByUser = GetDBObjectFullName("GetPermitsByUser");
                        permits = (new GlobalModel()).excecuteProcedureReturn(spGetPermitsByUser, new List<Dictionary<string, string>> { 
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEID" },
                                { "Value", GetSOEID() }
                            }
                        });
                    }
                    else
                    {
                        //Connect to database and get default user info only if exist connections string
                        if (!String.IsNullOrEmpty(GetConnectionString()))
                        {
                            var sql =
                                "SELECT " +
                                "   T.[SOEID], " +
                                "   T.[GEID], " +
                                "   T.[EMAIL_ADDR]	                        AS [Email], " +
                                "   T.[FIRST_NAME] + ' ' + T.[LAST_NAME]    AS [Name], " +
                                "   'TEMP_ROLE'                             AS [Roles], " +
                                "   'Temporary Role'                        AS [RolesDesc], " +
                                "   '0'                                     AS [Organizations], " +
                                "   'TBD'                                   AS [OrganizationsDesc], " +
                                "   'TBD'                                   AS [Center] " +
                                "FROM " +
                                "   [Automation].[dbo].[tblEmployee] T WITH(READUNCOMMITTED) " +
                                "WHERE " +
                                "   [SOEID] = '" + GetSOEID() + "'";

                            resultList = (new GlobalModel()).getResultListBySQL(sql);
                        }
                    }

                    if (resultList != null && resultList.Count == 1)
                    {
                        userInfo = resultList[0];

                        //userInfo["IsFRO"] = "0";

                        //Set IsFRO = 1
                        //if (userInfo["Organizations"].Contains("12"))
                        //{
                        //    userInfo["IsFRO"] = "1";
                        //}

                        //TODO: Create method to validate if the user is FRSS
                        userInfo["Organizations"] = "0";
                        userInfo["OrganizationsDesc"] = "TBD";
                        userInfo["Center"] = "TBD";
                        userInfo["IsFRO"] = "0";
                    }
                    else
                    {
                        userInfo = new Dictionary<string, string>{
                            { "SOEID", "" },
                            { "GEID", "" },
                            { "Email", "Email Not Found" },
                            { "Name", "User Not Found" },
                            { "Roles", "Role Not Found" },
                            { "RolesDesc", "Role Not Found" },
                            { "Organizations", "0" },
                            { "OrganizationsDesc", "TBD" },
                            { "Center", "TBD" },
                            { "IsFRO", "-1" }
                        }; 
                    }

                    userInfo["Permits"] = JsonConvert.SerializeObject(permits);
                    userInfo["DefaultAppKey"] = GetAppKey();
                }
            }
            catch (Exception e)
            {
                userInfo = new Dictionary<string, string>{
                    { "SOEID", "" },
                    { "GEID", "" },
                    { "Email", "Email Not Found" },
                    { "Name", "User Not Found" },
                    { "Roles", "Role Not Found" },
                    { "RolesDesc", "Role Not Found" },
                    { "Organizations", "0" },
                    { "OrganizationsDesc", "TBD" },
                    { "Center", "TBD" },
                    { "IsFRO", "-1" }
                }; 

                string msg = "";
                msg += "Getting user information error <br />";
                AddSessionMessage("Error", msg, false, e);
            }
            
            return userInfo;
        }

        public static bool IsOkToolConfiguration()
        {
            bool result = false;
            //Validate if exists config file
            if (ExistsGlobalConfigFile())
            {
                //Validate if exists soeid in session
                if (!String.IsNullOrEmpty(GetSOEID()))
                {
                    //Validate if exists database conection
                    var dbInfo = CheckDatabaseConnection();
                    if (dbInfo.Count == 1 && dbInfo[0]["ColumnUser"] != "Error")
                    {
                        result = true;
                    }
                }
            };

            return result;
        }

        public static string DecodeSkipSideminder(string ptext)
        {
            if (!String.IsNullOrEmpty(ptext))
            {
                ptext = ptext.Replace(@"_i_", @"%");
                ptext = ptext.Replace(@"_u_", @"'");
                ptext = ptext.Replace(@"_u1_", @"<>");
                ptext = ptext.Replace(@"_u2_", @"""");
                ptext = ptext.Replace(@"_u3_", @"*");
                ptext = ptext.Replace(@"_u4_", @",");
                ptext = ptext.Replace(@"_y1_", @".");
                ptext = ptext.Replace(@"_y2_", @"[");
                ptext = ptext.Replace(@"_y3_", @"]");
                ptext = ptext.Replace(@"_y4_", @"=");
                ptext = ptext.Replace(@"_y5_", @"<");
                ptext = ptext.Replace(@"_y6_", @">");
                ptext = ptext.Replace(@"_y7_", @" ");
                ptext = ptext.Replace(@"_y8_", @"\");
                ptext = ptext.Replace(@"_y9_", @"/");
                ptext = ptext.Replace(@"_z1_", @":");
                ptext = ptext.Replace(@"_z2_", "\n");
                ptext = ptext.Replace(@"_z3_", "\t");
            }
            else
            {
                ptext = "";
            }
            
            return ptext;
        }

        public static string CreateCustomID()
        {
            return DateTime.Now.ToString("yyyyMMddHmmssff");
        }

        public static string runCMD(string cmd)
        {
            string output = "";
            Process process = new Process
            {
                StartInfo =
                {
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = true,
                    FileName = "cmd.exe",
                    Arguments = "/C " + cmd
                }
            };
            process.Start();
            process.WaitForExit();
            if (process.HasExited)
            {
                output = process.StandardOutput.ReadToEnd();
            }

            return output;
        }

        public static Dictionary<string, string> GetListEntitiesConnString()
        {
            Dictionary<string, string> listResult = new Dictionary<string, string>();

            Configuration cfg = WebConfigurationManager.OpenWebConfiguration("/");
            var connSection = (ConnectionStringsSection)cfg.GetSection("connectionStrings");
            var entitiesNames = GetValueAppConfig("appSettings", "EntitiesNames");
            bool existsEntityInApp;

            if (!String.IsNullOrEmpty(entitiesNames))
            {
                var listAppEntities = entitiesNames.Split(',');

                foreach (ConnectionStringSettings item in connSection.ConnectionStrings)
                {
                    existsEntityInApp = false;
                    foreach (var appEntity in listAppEntities)
                    {
                        if (appEntity == item.Name)
                        {
                            existsEntityInApp = true;
                            break;
                        }
                    }

                    if (existsEntityInApp)
                    {
                        listResult.Add(item.Name, item.ConnectionString);
                    }
                }
            }
            return listResult;
        }

        public static void CreateReportLog(string reportName)
        {
            String automationID = GetAutomationID();

            if (!String.IsNullOrEmpty(automationID))
            {
                String pSQL =
                "EXEC [Automation].[dbo].[proc_Create_ReportLog] " +
                "     @ApplicationID = " + automationID + ", " +
                "     @ReportName = '" + reportName + "', " +
                "     @ExecutedBy = '" + GetSOEID() + "' ";

                (new GlobalModel()).excuteNonQueryBySQL(pSQL);
            }
        }

        public static void UpdateEntitiesConnString()
        {
            Configuration cfg = WebConfigurationManager.OpenWebConfiguration("/");
            var connSection = (ConnectionStringsSection)cfg.GetSection("connectionStrings");
            var entitiesNames = GetValueAppConfig("appSettings", "EntitiesNames");
            var hasChanges = false;
            bool existsEntityInApp;

            if(!String.IsNullOrEmpty(entitiesNames)){
                var listAppEntities = entitiesNames.Split(',');

                foreach (ConnectionStringSettings item in connSection.ConnectionStrings)
                {
                    existsEntityInApp = false;
                    foreach (var appEntity in listAppEntities)
                    {
                        if (appEntity == item.Name)
                        {
                            existsEntityInApp = true;
                            break;
                        }
                    }

                    if (existsEntityInApp)
                    {
                        var connString = item.ConnectionString;
                        var startIndexDBServer = connString.IndexOf("data source=");
                        var startIndexDBName = connString.IndexOf(";initial catalog=");
                        var endIndexDBName = connString.IndexOf(";integrated security=");
                        var lengthDBServer = startIndexDBName - startIndexDBServer;
                        var lengthDBName = endIndexDBName - startIndexDBName;

                        var partDBServer = connString.Substring(startIndexDBServer, lengthDBServer);
                        var dbConnStrServer = GetDBConnStrServer();
                        var newPartDBServer = "data source=" + dbConnStrServer;

                        var partDBName = connString.Substring(startIndexDBName, lengthDBName);
                        var dbName = GetValueAppConfig("appSettings", "DBName");
                        var newPartDBName = ";initial catalog=" + dbName;

                        //Replace DBServer and DBName only if it have values
                        if (!String.IsNullOrEmpty(dbConnStrServer) && !String.IsNullOrEmpty(dbName))
                        {
                            connString = connString.Replace(partDBServer, newPartDBServer);
                            connString = connString.Replace(partDBName, newPartDBName);
                        }

                        //Update Web.config
                        if (item.ConnectionString != connString)
                        {
                            item.ConnectionString = connString;
                            hasChanges = true;
                        }
                    }
                }
            }
            
            if (hasChanges)
            {
                cfg.Save();
            }
        }

        public static string GetAssemblyVersion()
        {
            //Automation.Core, Version=2017.3.5.112, Culture=neutral, PublicKeyToken=null
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public static string GetUrlBase()
        {
            return (HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath.TrimEnd('/'));
        }

        public static string GetAutomationID()
        {
            var objCurrentAppInfo = GetCurrentAppInfo();
            String automationID = objCurrentAppInfo.AutomationID;
            //String appSetting = "";
            //HttpRequest request = HttpContext.Current.Request;
            //String serverMachineName = System.Environment.MachineName;

            ////Check If is Local
            //if (request.ServerVariables["SERVER_NAME"] == "localhost")
            //{
            //    automationID = GetValueAppConfig("appSettings", "AutomationIDDEV");
            //    appSetting = "AutomationIDDEV";
            //}
            //else
            //{
            //    //Check PROD
            //    if (serverMachineName == GetValueGlobalConfig("appSettings", "ISSServerMachineNamePROD"))
            //    {
            //        automationID = GetValueAppConfig("appSettings", "AutomationIDPROD");
            //        appSetting = "AutomationIDPROD";
            //    }
            //    else
            //    {
            //        //Check DEV
            //        if (serverMachineName == GetValueGlobalConfig("appSettings", "ISSServerMachineNameDEV"))
            //        {
            //            automationID = GetValueAppConfig("appSettings", "AutomationIDDEV");
            //            appSetting = "AutomationIDDEV";
            //        }
            //        else
            //        {
            //            //Check UAT
            //            if (serverMachineName == GetValueGlobalConfig("appSettings", "ISSServerMachineNameUAT"))
            //            {
            //                automationID = GetValueAppConfig("appSettings", "AutomationIDUAT");
            //                appSetting = "AutomationIDUAT";
            //            }
            //            else
            //            {
            //                //Check COB
            //                if (serverMachineName == GetValueGlobalConfig("appSettings", "ISSServerMachineNameCOB"))
            //                {
            //                    automationID = GetValueAppConfig("appSettings", "AutomationIDPROD");
            //                    appSetting = "AutomationIDPROD";
            //                }
            //                else
            //                {
            //                    //Set default DEV
            //                    automationID = GetValueAppConfig("appSettings", "AutomationIDDEV");
            //                    appSetting = "AutomationIDDEV";
            //                }
            //            }
            //        }
            //    }
            //}

            if (String.IsNullOrEmpty(automationID))
            {
                var pathAppInfo = GetValueAppConfig("appSettings", "PathAppInfo");
                AddSessionMessage("Error", "The key 'AutomationID' is empty, please fix the error in the file '" + pathAppInfo + "'.", true);
            }

            return automationID;
        }

        public static string GetReportServerUrl()
        {
            String serverUrl = "";
            HttpRequest request = HttpContext.Current.Request;
            String serverMachineName = System.Environment.MachineName;

            //Check If is Local
            if (request.ServerVariables["SERVER_NAME"] == "localhost")
            {
                serverUrl = GetValueGlobalConfig("appSettings", "ReportServerUrlDEV");
            }
            else
            {
                //Check PROD
                if (serverMachineName == GetValueGlobalConfig("appSettings", "ISSServerMachineNamePROD"))
                {
                    serverUrl = GetValueGlobalConfig("appSettings", "ReportServerUrlPROD");
                }
                else
                {
                    //Check DEV
                    if (serverMachineName == GetValueGlobalConfig("appSettings", "ISSServerMachineNameDEV"))
                    {
                        serverUrl = GetValueGlobalConfig("appSettings", "ReportServerUrlDEV");
                    }
                    else
                    {
                        //Check UAT
                        if (serverMachineName == GetValueGlobalConfig("appSettings", "ISSServerMachineNameUAT"))
                        {
                            serverUrl = GetValueGlobalConfig("appSettings", "ReportServerUrlUAT");
                        }
                        else
                        {
                            //Check COB
                            if (serverMachineName == GetValueGlobalConfig("appSettings", "ISSServerMachineNameCOB"))
                            {
                                serverUrl = GetValueGlobalConfig("appSettings", "ReportServerUrlCOB");
                            }
                            else
                            {
                                //Set default DEV
                                serverUrl = GetValueGlobalConfig("appSettings", "ReportServerUrlDEV");
                            }
                        }
                    }
                }
            }

            if (!String.IsNullOrEmpty(GetValueAppConfig("appSettings", "ReportServerUrlOverride")))
            {
                serverUrl = GetValueAppConfig("appSettings", "ReportServerUrlOverride");
            }

            return serverUrl;
        }

        public static string GetEnviroment()
        {
            String result = "";
            HttpRequest request = HttpContext.Current.Request;
            String serverMachineName = System.Environment.MachineName;

            //Check If is Local
            if (request.ServerVariables["SERVER_NAME"] == "localhost")
            {
                result = "Local";
            }
            else
            {
                //Check PROD
                if (serverMachineName == GetValueGlobalConfig("appSettings", "ISSServerMachineNamePROD"))
                {
                    result = "PROD";
                }
                else
                {
                    //Check DEV
                    if (serverMachineName == GetValueGlobalConfig("appSettings", "ISSServerMachineNameDEV"))
                    {
                        result = "DEV";
                    }
                    else
                    {
                        //Check UAT
                        if (serverMachineName == GetValueGlobalConfig("appSettings", "ISSServerMachineNameUAT"))
                        {
                            result = "UAT";
                        }
                        else
                        {
                            //Check COB
                            if (serverMachineName == GetValueGlobalConfig("appSettings", "ISSServerMachineNameCOB"))
                            {
                                result = "COB";
                            }
                            else
                            {
                                //Set Default DEV
                                result = "DEV";
                            }
                        }
                    }
                }
            }
            
            return result;
        }

        public static string GetDBConnStrServer()
        {
            String serverUrl = "";
            HttpRequest request = HttpContext.Current.Request;
            String serverMachineName = System.Environment.MachineName;

            //Check If is Local
            if (request.ServerVariables["SERVER_NAME"] == "localhost")
            {
                serverUrl = GetValueGlobalConfig("appSettings", "DBConnStrServerDEV");
            }
            else
            {
                //Check PROD
                if (serverMachineName == GetValueGlobalConfig("appSettings", "ISSServerMachineNamePROD"))
                {
                    serverUrl = GetValueGlobalConfig("appSettings", "DBConnStrServerPROD");
                }
                else
                {
                    //Check DEV
                    if (serverMachineName == GetValueGlobalConfig("appSettings", "ISSServerMachineNameDEV"))
                    {
                        serverUrl = GetValueGlobalConfig("appSettings", "DBConnStrServerDEV");
                    }
                    else
                    {
                        //Check UAT
                        if (serverMachineName == GetValueGlobalConfig("appSettings", "ISSServerMachineNameUAT"))
                        {
                            serverUrl = GetValueGlobalConfig("appSettings", "DBConnStrServerUAT");
                        }
                        else
                        {
                            //Check COB
                            if (serverMachineName == GetValueGlobalConfig("appSettings", "ISSServerMachineNameCOB"))
                            {
                                serverUrl = GetValueGlobalConfig("appSettings", "DBConnStrServerCOB");
                            }
                            else
                            {
                                //Set Default DEV
                                serverUrl = GetValueGlobalConfig("appSettings", "DBConnStrServerDEV");
                            }
                        }
                    }
                }
            }

            if (!String.IsNullOrEmpty(GetValueAppConfig("appSettings", "DBConnStrServerOverride")))
            {
                serverUrl = GetValueAppConfig("appSettings", "DBConnStrServerOverride");
            }

            return serverUrl;
        }

        public static string GetConnectionString()
        {
            string dbName = GetValueAppConfig("appSettings", "DBName");
            string dbConnStrServer = GetDBConnStrServer();
            string connStringTemplate = GetValueAppConfig("connectionStrings", "DefaultConnString");

            connStringTemplate = connStringTemplate.Replace("[:DBConnStrServer]", dbConnStrServer);
            connStringTemplate = connStringTemplate.Replace("[:DBName]", dbName);

            return connStringTemplate;
        }

        public static List<Dictionary<string, string>> CheckDatabaseConnection()
        {
            List<Dictionary<string, string>> resultList = null;
            try
            {
                var sql = "SELECT SYSTEM_USER AS ColumnUser, DB_NAME() AS ColumnDB, @@SERVERNAME AS ColumnServer";
                resultList = (new GlobalModel()).getResultListBySQL(sql);
                HttpContext.Current.Session["DBIsOnline"] = "1";
            }
            catch (Exception e)
            {
                //SqlException (0x80131904): A network-related or instance-specific error occurred while establishing a connection to SQL Server. The server was not found or was not accessible. Verify that the instance name is correct and that SQL Server is configured to allow remote connections. (provider: TCP Provider, error: 0 - The requested name is valid, but no data of the requested type was found.)]
                //System.Data.SqlClient.SqlInternalConnectionTds..ctor(DbConnectionPoolIdentity identity, SqlConnectionString connectionOptions, SqlCredential credential, Object providerInfo, String newPassword, SecureString newSecurePassword, Boolean redirectedUserInstance, SqlConnectionString userConnectionOptions, SessionData reconnectSessionData, DbConnectionPool pool, String accessToken, Boolean applyTransientFaultHandling) +970
                //System.Data.SqlClient.SqlException (0x80131904): Timeout expired.  The timeout period elapsed prior to completion of the operation or the server is not responding.

                string msg = "";
                msg += "Database error <br />";
                msg += "<b>ConnectionStrings: </b>" + GetConnectionString() + " <br />";
                msg += "<b>Configuration File: </b>" + GetPathGlobalConfigFile();
                AddSessionMessage("Error", msg, false, e);
                HttpContext.Current.Session["DBIsOnline"] = "0";

                resultList = new List<Dictionary<string,string>> {
                    new Dictionary<string, string>
                    {
                        { "ColumnUser", "Error"},
                        { "ColumnDB", "Error"},
                        { "ColumnServer", "Error"}
                    }
                };
            }
            return resultList;
        }

        public static string GetDBObjectFullName(string pobjKey)
        {
            string objectFullName = "";
            string pathJsonDBStructure = GetValueGlobalConfig("appSettings", "PathDBLocalStructure");
            string tablesPrefix = GetValueAppConfig("appSettings", "TablesPrefix");
            string procPrefix = GetValueAppConfig("appSettings", "ProcPrefix");
            string funcPrefix = GetValueAppConfig("appSettings", "FuncPrefix");
            var jsonDBStructure = new StringBuilder(System.IO.File.ReadAllText(@"" + HttpContext.Current.Server.MapPath(pathJsonDBStructure)));
            var resultList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(jsonDBStructure.ToString());

            foreach (var objDBObject in resultList)
            {
                if (objDBObject["key"] == pobjKey)
                {
                    switch (objDBObject["objectType"])
                    {
                        case "Table":
                            objDBObject["prefix"] = tablesPrefix;
                            break;
                        case "Procedure":
                            objDBObject["prefix"] = procPrefix;
                            break;
                        case "Function":
                            objDBObject["prefix"] = funcPrefix;
                            break;
                        default:
                            objDBObject["prefix"] = "";
                            break;
                    }

                    objectFullName = objDBObject["prefix"] + objDBObject["objectName"];
                    break;
                }
            }

            return objectFullName;
        }

        public static string DecodeAsciiString(string ptext)
        {
            var newString = "";

            if (!String.IsNullOrEmpty(ptext))
            {
                string[] letters = ptext.Split('-');


                for (int i = 0; i < letters.Length; i++)
                {
                    newString += "" + (char)Int32.Parse(letters[i]);
                }
            }

            return newString;
        }

        public static string StripHTML(string input)
        {
            if (!String.IsNullOrEmpty(input))
            {
                input = input.Replace("&amp;", "&");
                input = input.Replace("&#58;", ":");
                input = input.Replace("&#160;", " ");
                input = input.Replace("&nbsp;", " ");
                input = System.Text.RegularExpressions.Regex.Replace(input, "<.*?>", String.Empty).Trim();
            }

            return input;
        }

        // ---------------------------------------------
        // Active Directory Methods
        // ---------------------------------------------
        public static string GetAutomationIDDL(string pemail, string pdescriptionDL)
        {
            var idDL = "0";

            if (!String.IsNullOrEmpty(pemail) || !String.IsNullOrEmpty(pdescriptionDL))
            {
                //Validate if exists Email of DL in Automation
                var sql = "";

                if (!String.IsNullOrEmpty(pemail))
                {
                    sql = string.Format("SELECT [ID] FROM [Automation].[dbo].[tblDL] WHERE [IsDeleted] = 0 AND [Email] = '{0}' ", pemail);
                }
                else
                {
                    if (!String.IsNullOrEmpty(pdescriptionDL))
                    {
                        sql = string.Format("SELECT [ID] FROM [Automation].[dbo].[tblDL] WHERE [IsDeleted] = 0 AND [DisplayName] = '{0}' ", pdescriptionDL);
                    }
                }
                
                List<Dictionary<string, string>> resultList = (new GlobalModel()).excecuteQuery(sql);

                if (resultList.Count > 0)
                {
                    idDL = resultList[0]["ID"];
                }
            }

            return idDL;
        }

        public static string AddAutomationDL(string pemail, string pdescriptionDL, string pemailGuid = "", string pemailLastUpdate = "", string pforced = "False")
        {
            var msgResult = "-1";
            
            DirectoryEntry acGroup = null;

            //Find DL by Guid in Active Directory
            if (!String.IsNullOrEmpty(pemailGuid))
            {
                acGroup = new DirectoryEntry(string.Format("LDAP://<GUID={0}>", pemailGuid));

                //Avoid save again the DL in Recursive call
                listOfDLGuidSavedInDB.Add(pemailGuid, true);
            }

            //Find DL by email in Active Directory
            if (acGroup == null && (!String.IsNullOrEmpty(pemail) || !String.IsNullOrEmpty(pdescriptionDL)))
            {
                //Reset in first call to Avoid save again the DL in Recursive call
                listOfDLGuidSavedInDB = new Dictionary<string, bool>();

                DirectoryEntry gc = new DirectoryEntry("GC:");

                foreach (DirectoryEntry root in gc.Children)
                {
                    gc = root;
                }

                //Search DL's Email in Active Directory
                DirectorySearcher searcher = new DirectorySearcher();
                searcher.SearchRoot = gc;
                if (!String.IsNullOrEmpty(pemail))
                {
                    searcher.Filter = string.Format("(mail={0})", pemail);
                }
                else
                {
                    if (!String.IsNullOrEmpty(pdescriptionDL))
                    {
                        //Groups with cn starting with 
                        //"Test" or "Admin"	(&(objectCategory=group) 
                        //                  (|(cn=Test*)(cn=Admin*)))
                        //(groupType=8)(objectCategory=group)(hideDLMembership=FALSE)
                        //searcher.Filter = string.Format("(&(groupType=8)(displayName={0}))", pdescriptionDL);
                        //searcher.Filter = string.Format("(displayName={0})", pdescriptionDL);
                        searcher.Filter = string.Format("(&(|(groupType=8)(groupType=-2147483640))(displayName={0}))", pdescriptionDL);
                    }
                }
                
                SearchResultCollection results = searcher.FindAll();
                if (results.Count > 0)
                {
                    //Load by DistinguishedName
                    //string path = results[0].Properties["DistinguishedName"][0].ToString();
                    //DirectoryEntry dc = new DirectoryEntry();
                    //dc.Path = string.Format("LDAP://{0}", path);

                    //Load by Guid
                    //var dc = new DirectoryEntry(string.Format("LDAP://<GUID={0}>", dc.Guid.ToString("D")));

                    acGroup = results[0].GetDirectoryEntry();
                }
            }

            //If Group exists
            if (acGroup != null)
            {
                var proceedToUpdate = true;
                //Check if exists LastUpdate
                if (!String.IsNullOrEmpty(pemailLastUpdate) && pforced == "false")
                {
                    var dtLastUpdate = DateTime.Parse(pemailLastUpdate);
                    var acWhenChanged = acGroup.Properties["whenChanged"].Value;
                    if(acWhenChanged != null)
                    {
                        var dtACWhenChanged = (DateTime)acWhenChanged;
                        if(dtLastUpdate > dtACWhenChanged)
                        {
                            proceedToUpdate = false;
                        }
                    }
                }

                if (proceedToUpdate)
                {
                    //Get DL Information
                    var path = acGroup.Properties["DistinguishedName"].Value.ToString();
                    var guid = pemailGuid;
                    if (String.IsNullOrEmpty(guid))
                    {
                        guid = acGroup.Guid.ToString("D");

                        //Avoid save again the DL in Recursive call
                        listOfDLGuidSavedInDB.Add(guid, true);
                    }

                    var email = pemail;
                    if (String.IsNullOrEmpty(email))
                    {
                        email = acGroup.Properties["mail"].Value.ToString();
                    }

                    var samAccoutName = acGroup.Properties["sAMAccountName"].Value.ToString();
                    var displayName = acGroup.Properties["displayName"].Value.ToString();

                    var managedBySOEID = "";
                    var acManagedByPath = acGroup.Properties["managedBy"].Value;

                    if (acManagedByPath != null)
                    {
                        var acManagedBy = new DirectoryEntry(string.Format("LDAP://{0}", acManagedByPath.ToString()));
                        managedBySOEID = acManagedBy.Name.Replace("CN=", "");
                    }

                    //Get DL Members (Users or other DL)
                    var strDLSOEIDs = "";
                    var strDLGuidGroups = "";
                    var memberPaths = acGroup.Properties["member"];
                    for (int i = 0; i < memberPaths.Count; i++)
                    {
                        var tempPath = (string)memberPaths[i];
                        var entryType = "user";
                        var userSOEID = "";
                        DirectoryEntry dcTemp = null;

                        //Faster check to get SOEID
                        //tempPath: CN=sl84003,OU=GRN_Tristate,OU=Users,OU=GCOT_Users,OU=Accounts_ICGCORP,OU=ICGCORP,DC=nam,DC=nsroot,DC=net: "",
                        //          CN=dl_EQ_Global_HT_Offshore_Support,OU=Dist_Groups,OU=EXCHANGE,OU=INFRA,DC=nam,DC=nsroot,DC=net:
                        //          CN=EQ_HT_NAM_SUPPORT,OU=svc_Accts,OU=Functional-IDs_GIDA_MailBox,OU=Functional-IDs_GIDA,OU=GIDA,DC=nam,DC=nsroot,DC=net
                        //          CN=sys_settledist,OU=Remote,OU=Users,OU=GCOT_Users,OU=Accounts_ICGCORP,OU=ICGCORP,DC=nam,DC=nsroot,DC=net
                        if (tempPath.Contains("OU=Users"))
                        {
                            //CN=sl84003,
                            var soeidPart = tempPath.Substring(0, 11);
                            if (soeidPart.Contains(","))
                            {
                                soeidPart = soeidPart.Replace("CN=", "");
                                soeidPart = soeidPart.Replace(",", "");
                                userSOEID = soeidPart;
                            }
                        }
                        else
                        {
                            dcTemp = new DirectoryEntry();
                            dcTemp.Path = string.Format("LDAP://{0}", tempPath);
                            entryType = dcTemp.SchemaClassName;
                        }

                        //Calculate if DirectoryEntry if 
                        //METHOD 1: "user" || "group"
                        //------------------------------------
                         
                        switch (entryType)
                        {
                            case "user":
                                //When is NULL is for incorrects entries like:
                                // CN=EQ_HT_NAM_SUPPORT,OU=svc_Accts,OU=Functional-IDs_GIDA_MailBox,OU=Functional-IDs_GIDA,OU=GIDA,DC=nam,DC=nsroot,DC=net
                                // CN=sys_settledist,OU=Remote,OU=Users,OU=GCOT_Users,OU=Accounts_ICGCORP,OU=ICGCORP,DC=nam,DC=nsroot,DC=net
                                if (!String.IsNullOrEmpty(userSOEID))
                                {
                                    //Add soeid found in path
                                    strDLSOEIDs += userSOEID + ";";
                                }

                                //if (String.IsNullOrEmpty(userSOEID))
                                //{
                                //    //Ignore Funtional IDs e. g. "EQ_HT_NAM_SUPPORT"
                                //    userSOEID = dcTemp.Name.Replace("CN=", "");
                                    
                                //    //Only insert SOEIDs
                                //    if (userSOEID.Length == 7)
                                //    {
                                //        strDLSOEIDs += userSOEID + ";";
                                //    }
                                //}
                                
                                break;

                            case "group":
                                var childDLGuid = dcTemp.Guid.ToString("D");

                                //Only add DL to database if it not was proccesed to avoid infinity loop
                                if (listOfDLGuidSavedInDB.ContainsKey(childDLGuid) == false)
                                {
                                    //Call recursive to insert groups of DL by Guid
                                    AddAutomationDL("", "", childDLGuid);
                                }

                                //Create string of Guid Groups to Map in DB Childs groups of DL
                                strDLGuidGroups += childDLGuid + ";";
                                break;
                        }

                        //METHOD 2: "user" || "group"
                        //------------------------------------
                        //var entryType = ((object[])dcTemp.Properties["objectClass"].Value).Last().ToString();

                        //METHOD 3: "Person" || "Group"
                        //------------------------------------
                        //var entryType = "Person";
                        //var objCategoryPath = dcTemp.Properties["objectCategory"].Value.ToString();
                        //if (objCategoryPath.Contains("Group"))
                        //{
                        //    entryType = "Group";
                        //}

                        //TODO: Insert Group Members
                    }

                    //Insert DL in Database
                    var newDLData = (new GlobalModel()).excecuteProcedureReturn("[Automation].[dbo].[spAFrwkAdminDL]", new List<Dictionary<string, string>> {
                        new Dictionary<string, string>
                        {
                            { "Name", "@Action" },
                            { "Value", "New" }
                        },
                        new Dictionary<string, string>
                        {
                            { "Name", "@Guid" },
                            { "Value", guid }
                        },
                        new Dictionary<string, string>
                        {
                            { "Name", "@Path" },
                            { "Value", path }
                        },
                        new Dictionary<string, string>
                        {
                            { "Name", "@SamAccountName" },
                            { "Value", samAccoutName }
                        },
                        new Dictionary<string, string>
                        {
                            { "Name", "@DisplayName" },
                            { "Value", displayName }
                        },
                        new Dictionary<string, string>
                        {
                            { "Name", "@Email" },
                            { "Value", email }
                        },
                        new Dictionary<string, string>
                        {
                            { "Name", "@ManagedBy" },
                            { "Value", managedBySOEID }
                        },
                        new Dictionary<string, string>
                        {
                            { "Name", "@StrListUsers" },
                            { "Value", strDLSOEIDs }
                        },
                        new Dictionary<string, string>
                        {
                            { "Name", "@StrListGuidChildDLs" },
                            { "Value", strDLGuidGroups }
                        },
                        new Dictionary<string, string>
                        {
                            { "Name", "@SessionSOEID" },
                            { "Value", GetSOEID() }
                        }
                    })[0];

                    msgResult = newDLData["ID"];
                }
            }
            else
            {
                msgResult = "-1";
            }

            return msgResult;
        }

        public static Dictionary<string, string> GetACUsersByACGroup(string pemailDL, string pemailGuid = "", string SAMAccountName = "")
        {
            var result = new Dictionary<string, string>();
            DirectoryEntry acGroup = null;

            //Find DL by Guid in Active Directory
            if (!String.IsNullOrEmpty(pemailGuid))
            {
                acGroup = new DirectoryEntry(string.Format("LDAP://<GUID={0}>", pemailGuid));

                //Avoid save again the DL in Recursive call
                listOfDLGuidSavedInDB.Add(pemailGuid, true);
            }

            //Find DL by email in Active Directory
            if (acGroup == null && (!String.IsNullOrEmpty(pemailDL) || !String.IsNullOrEmpty(SAMAccountName)))
            {
                //Reset in first call to Avoid save again the DL in Recursive call
                listOfDLGuidSavedInDB = new Dictionary<string, bool>();

                DirectoryEntry gc = new DirectoryEntry("GC:");

                foreach (DirectoryEntry root in gc.Children)
                {
                    gc = root;
                }

                //Search DL's Email in Active Directory
                DirectorySearcher searcher = new DirectorySearcher();
                searcher.SearchRoot = gc;

                if (!String.IsNullOrEmpty(pemailDL))
                {
                    searcher.Filter = string.Format("(mail={0})", pemailDL);
                }

                if (!String.IsNullOrEmpty(SAMAccountName))
                {
                    searcher.Filter = string.Format("(sAMAccountName={0})", SAMAccountName);
                }

                SearchResultCollection results = searcher.FindAll();
                if (results.Count > 0)
                {
                    acGroup = results[0].GetDirectoryEntry();
                }
            }

            //If Group exists
            if (acGroup != null)
            {

                //Load Users with Group Principal by issue with acGroup.Properties["member"];
                PrincipalContext ctx = new PrincipalContext(ContextType.Domain, GetPrincipalDomainByPath(acGroup.Path));
                GroupPrincipal groupPrincipal = GroupPrincipal.FindByIdentity(ctx, IdentityType.Guid, acGroup.Guid.ToString());
                
                var groupPrincipalMembers = groupPrincipal.GetMembers();
                foreach (UserPrincipal tempUser in groupPrincipalMembers)
                {
                    result.Add(tempUser.DisplayName + " - [" + tempUser.SamAccountName + "]", "");
                }

                //Get Users
                //var members = acGroup.Properties["member"];
                //for (int i = 0; i < members.Count; i++)
                //{
                //    string tempPath = members[i].ToString();
                //    result.Add(tempPath, "");   
                //}
            }

            return result;
        }
        
        public static string GetPrincipalDomainByPath(String ACPath)
        {
            var resultDomain = "";
            var splitData = ACPath.Split(',');
            for (int i = splitData.Length - 1; i >= (splitData.Length - 3); i--)
            {
                resultDomain = (i == (splitData.Length - 3) ? "" : ".") + splitData[i].Replace("DC=", "") + resultDomain;
            }
            
            return resultDomain;
        }

        public static Dictionary<string, string> GetACGroupsBySOEID(string psoeid)
        {
            var result = new Dictionary<string, string>();
            DirectoryEntry acUser = null;
            DirectoryEntry gc = new DirectoryEntry("GC:");

            foreach (DirectoryEntry root in gc.Children)
            {
                gc = root;
            }

            //Search DL's Email in Active Directory
            DirectorySearcher searcher = new DirectorySearcher();
            searcher.SearchRoot = gc;
            searcher.Filter = string.Format("(cn={0})", psoeid);

            SearchResultCollection results = searcher.FindAll();
            if (results.Count > 0)
            {
                acUser = results[0].GetDirectoryEntry();
            }

            //If User exists
            if (acUser != null)
            {
                //Get Groups
                var memberOfPaths = acUser.Properties["memberOf"];
                for (int i = 0; i < memberOfPaths.Count; i++)
                {
                    string tempPath = memberOfPaths[i].ToString();
                    result.Add(tempPath, "");

                    //DirectoryEntry dcTemp = new DirectoryEntry();
                    //dcTemp.Path = string.Format("LDAP://{0}", tempPath);
                    //var groupEmail = dcTemp.Properties["mail"].Value;

                    //if (groupEmail != null)
                    //{
                    //    result.Add(dcTemp.Guid.ToString("D"), groupEmail.ToString());
                    //}

                }
            }

            return result;
        }

        public static Dictionary<string, string> GETACGroupsBySOEIDAM(string psoeid, string paddomain = "")
        {
            var result = new Dictionary<string, string>();

            //Create context to Domain
            PrincipalContext ctx = null;

            if (String.IsNullOrEmpty(paddomain))
            {
                ctx = new PrincipalContext(ContextType.Domain);
            }
            else
            {
                ctx = new PrincipalContext(ContextType.Domain, paddomain);
            }

            UserPrincipal userPrincipal = UserPrincipal.FindByIdentity(ctx, IdentityType.SamAccountName, psoeid);

            if (userPrincipal != null)
            {
                var userGroups = userPrincipal.GetGroups();
                foreach (GroupPrincipal tempGroup in userGroups)
                {
                    result.Add(tempGroup.Guid.ToString(), "");
                }
            }

            return result;
        }

        public static string UpdateCurrentUserDL()
        {
            var countOfGroupSync = "0";
            DirectoryEntry acUser = null;
            DirectoryEntry gc = new DirectoryEntry("GC:");

            //Create DataTable with all groups of current user
            System.Data.DataTable tblACPaths = new System.Data.DataTable();
            tblACPaths.Columns.Add("Path");

            //Get Global Catalog
            foreach (DirectoryEntry root in gc.Children)
            {
                gc = root;
            }

            //Search DL's Email in Active Directory
            DirectorySearcher searcher = new DirectorySearcher();
            searcher.SearchRoot = gc;
            searcher.Filter = string.Format("(cn={0})", GetSOEID());

            SearchResultCollection results = searcher.FindAll();
            if (results.Count > 0)
            {
                acUser = results[0].GetDirectoryEntry();
            }

            //If User exists
            if (acUser != null)
            {
                //Get Groups
                var memberOfPaths = acUser.Properties["memberOf"];
                for (int i = 0; i < memberOfPaths.Count; i++)
                {
                    //Get path of group in active directory
                    System.Data.DataRow tempRow = tblACPaths.NewRow();
                    tempRow["Path"] = memberOfPaths[i].ToString();
                    tblACPaths.Rows.Add(tempRow);
                }

                //Save data in the database
                if (tblACPaths != null && tblACPaths.Rows.Count > 0)
                {
                    (new GlobalModel()).SaveDataTableDB(tblACPaths, "[Automation].[dbo].[spAFrwkAdminDLOfUser]", new List<Dictionary<string, string>> {
                        new Dictionary<string, string>
                        {
                            { "Name", "@SOEIDUploader" },
                            { "Value", GlobalUtil.GetSOEID() }
                        }
                    });

                    countOfGroupSync = tblACPaths.Rows.Count.ToString();
                }
            }

            return countOfGroupSync;
        }

        public static string UpdateAutomationDL(string pemailGuid, string pemailLastUpdated, string pforced)
        {
            //Reset in first call to Avoid save again the DL in Recursive call
            listOfDLGuidSavedInDB = new Dictionary<string, bool>();

            //Update selected DL
            AddAutomationDL("", "", pemailGuid, pemailLastUpdated, pforced);

            return listOfDLGuidSavedInDB.Count.ToString();
        }
        
        public static string UpdateAllAutomationDL()
        {
            var sql = "SELECT [Guid], ISNULL([ModifiedDate], [CreatedDate]) AS [LastUpdate] FROM Automation..tblDL WHERE [IsDeleted] = 0";
            var resultList = (new GlobalModel()).excecuteQuery(sql);

            if(resultList.Count > 0)
            {
                //Reset in first call to Avoid save again the DL in Recursive call
                listOfDLGuidSavedInDB = new Dictionary<string, bool>();

                foreach (var itemDL in resultList)
                {
                    //Only add DL to database if it not was proccesed to avoid infinity loop
                    if (listOfDLGuidSavedInDB.ContainsKey(itemDL["Guid"]) == false)
                    {
                        AddAutomationDL("", "", itemDL["Guid"], itemDL["LastUpdate"]);
                    }   
                }
            }

            return listOfDLGuidSavedInDB.Count.ToString();
        }

        public static Dictionary<string, string> GetAllPropertiesOfEmail(string pemail)
        {
            var result = new Dictionary<string, string>();
            DirectoryEntry acEntry = null;
            
            //Find DL by email in Active Directory
            if (acEntry == null && !String.IsNullOrEmpty(pemail))
            {
                DirectoryEntry gc = new DirectoryEntry("GC:");

                foreach (DirectoryEntry root in gc.Children)
                {
                    gc = root;
                }

                //Search DL's Email in Active Directory
                DirectorySearcher searcher = new DirectorySearcher();
                searcher.SearchRoot = gc;
                searcher.Filter = string.Format("(mail={0})", pemail);

                SearchResultCollection results = searcher.FindAll();
                if (results.Count > 0)
                {
                    acEntry = results[0].GetDirectoryEntry();
                }
            }

            //If entry found
            if(acEntry != null)
            {
                //Add default properties
                result.Add("Var.Name", acEntry.Name);
                result.Add("Var.SchemaClassName", acEntry.SchemaClassName);
                result.Add("Var.Guid", acEntry.Guid.ToString("D"));
                result.Add("Var.Path", acEntry.Path);

                var properties = acEntry.Properties.PropertyNames;
                foreach (string propName in properties)
                {
                    result.Add(propName, acEntry.Properties[propName].Value.ToString());
                }
            }

            return result;
        }

        public static Dictionary<string, string> GetAllPropertiesBySAMAccount(string psAMAccount)
        {
            var result = new Dictionary<string, string>();
            DirectoryEntry acEntry = null;

            //Find DL by email in Active Directory
            if (acEntry == null && !String.IsNullOrEmpty(psAMAccount))
            {
                DirectoryEntry gc = new DirectoryEntry("GC:");

                foreach (DirectoryEntry root in gc.Children)
                {
                    gc = root;
                }

                //Search DL's Email in Active Directory
                DirectorySearcher searcher = new DirectorySearcher();
                searcher.SearchRoot = gc;
                searcher.Filter = string.Format("(sAMAccountName={0})", psAMAccount);

                SearchResultCollection results = searcher.FindAll();
                if (results.Count > 0)
                {
                    acEntry = results[0].GetDirectoryEntry();
                }
            }

            //If entry found
            if (acEntry != null)
            {
                //Add default properties
                result.Add("Var.Name", acEntry.Name);
                result.Add("Var.SchemaClassName", acEntry.SchemaClassName);
                result.Add("Var.Guid", acEntry.Guid.ToString("D"));
                result.Add("Var.Path", acEntry.Path);

                var properties = acEntry.Properties.PropertyNames;
                foreach (string propName in properties)
                {
                    result.Add(propName, acEntry.Properties[propName].Value.ToString());
                }
            }

            return result;
        }

        // ---------------------------------------------
        // App config and Global Config Methods
        // ---------------------------------------------
        public static bool StructureOkInProject(string pkeyApp)
        {
            bool result = true;
            var message = string.Empty;

            //Validate if exists folder
            string contentPath = @"" + HttpContext.Current.Server.MapPath("~/Content/" + pkeyApp + "/");
            string controllersPath = @"" + HttpContext.Current.Server.MapPath("~/Controllers/" + pkeyApp + "/");
            string viewsPath = @"" + HttpContext.Current.Server.MapPath("~/Views/" + pkeyApp + "/");

            //If not exists create basic structure
            var directory = new FileInfo(contentPath).Directory;
            if (directory == null || (directory != null && directory.Exists == false))
            {
                result = false;
            }

            //If not exists create basic structure
            //directory = new FileInfo(controllersPath).Directory;
            //if (directory == null || (directory != null && directory.Exists == false))
            //{
            //    result = false;
            //}

            //If not exists create basic structure
            directory = new FileInfo(viewsPath).Directory;
            if (directory == null || (directory != null && directory.Exists == false))
            {
                result = false;
            }

            return result;
        }

        public static string CreateAppStructureIfNotExists(AppInfo objApp)
        {
            string resultMsg = "";
            var message = string.Empty;

            //Validate if exists folder
            string contentPath = @"" + HttpContext.Current.Server.MapPath("~/Content/" + objApp.KeyName + "/");
            string controllersPath = @"" + HttpContext.Current.Server.MapPath("~/Controllers/" + objApp.KeyName + "/");
            string viewsPath = @"" + HttpContext.Current.Server.MapPath("~/Views/" + objApp.KeyName + "/");

            //If not exists create basic structure
            var directory = new FileInfo(contentPath).Directory;
            if (directory == null || (directory != null && directory.Exists == false))
            {
                string contentTemplatePath = @"" + HttpContext.Current.Server.MapPath("~/Content/Shared/config/NewAppTemplate/Content/_KeyName/");
                string keyRole = objApp.KeyName.ToUpper();
                keyRole = keyRole.Replace(" ", "_");
                var userInfo = GetUserInfo();

                //Copy files in Content
                Directory.CreateDirectory(contentPath);
                CopyFiles(contentTemplatePath, contentPath);
                ReplaceFileContent(contentPath + "js/Home.js", "_KeyName", objApp.KeyName);
                ReplaceFileContent(contentPath + "config/app-data.txt", "_KeyName", objApp.KeyName);
                ReplaceFileContent(contentPath + "config/app-data.txt", "_ShortName", objApp.ShortName);
                ReplaceFileContent(contentPath + "config/app-data.txt", "_KeyRole", keyRole);
                ReplaceFileContent(contentPath + "config/app-data.txt", "_CSID", objApp.CSID);
                ReplaceFileContent(contentPath + "config/app-data.txt", "_SOEID", userInfo["SOEID"]);
                ReplaceFileContent(contentPath + "config/app-data.txt", "_GEID", userInfo["GEID"]);
                ReplaceFileContent(contentPath + "config/app-data.txt", "_Email", userInfo["Email"]);
                ReplaceFileContent(contentPath + "config/app-data.txt", "_Name", userInfo["Name"]);
                ReplaceFileContent(contentPath + "config/app-data.txt", "_CreatedDate", DateTime.Now.ToString("M/dd/yyyy hh:mm:ss tt"));
                
                ReplaceFileContent(contentPath + "config/app-info.txt", "_KeyName", objApp.KeyName);
                ReplaceFileContent(contentPath + "config/app.config", "_KeyName", objApp.KeyName);

                message += "~/Content/" + objApp.KeyName + "/" + Environment.NewLine +
                    "~/Content/" + objApp.KeyName + "/config/app-data.txt" + Environment.NewLine +
                    "~/Content/" + objApp.KeyName + "/config/app-info.txt" + Environment.NewLine +
                    "~/Content/" + objApp.KeyName + "/config/app.config" + Environment.NewLine +
                    "~/Content/" + objApp.KeyName + "/js/" + Environment.NewLine +
                    "~/Content/" + objApp.KeyName + "/js/Home.js" + Environment.NewLine;
                    
            }

            //If not exists create basic structure
            directory = new FileInfo(controllersPath).Directory;
            if (directory == null || (directory != null && directory.Exists == false))
            {
                string controllersTemplatePath = @"" + HttpContext.Current.Server.MapPath("~/Content/Shared/config/NewAppTemplate/Controllers/_KeyName/");

                //Copy files in Controller
                Directory.CreateDirectory(controllersPath);
                CopyFiles(controllersTemplatePath, controllersPath);
                ReplaceFileContent(controllersPath + "HomeController.cs", "_KeyName", objApp.KeyName);

                message += "~/Controllers/" + objApp.KeyName + "/" + Environment.NewLine +
                    "~/Controllers/" + objApp.KeyName + "/HomeController.cs" + Environment.NewLine;
            }

            //If not exists create basic structure
            directory = new FileInfo(viewsPath).Directory;
            if (directory == null || (directory != null && directory.Exists == false))
            {
                string viewsTemplatePath = @"" + HttpContext.Current.Server.MapPath("~/Content/Shared/config/NewAppTemplate/Views/_KeyName/");

                //Copy files in View
                Directory.CreateDirectory(viewsPath);
                CopyFiles(viewsTemplatePath, viewsPath);
                ReplaceFileContent(viewsPath + "Home.cshtml", "_KeyName", objApp.KeyName);

                message += "~/Views/" + objApp.KeyName + "/" + Environment.NewLine +
                    "~/Views/" + objApp.KeyName + "/Home.cshtml" + Environment.NewLine;
                    
            }

            resultMsg = !string.IsNullOrWhiteSpace(message) ?
                String.Format("{0}{1}{2}", "<p>New app detected, the following files were created in the solution:</p><pre>", message, "</pre>") : string.Empty;

            return resultMsg;
        }

        public static void ReplaceFileContent(string SourceFilePath, string OldVaule, string NewValue) 
        {
            string text = File.ReadAllText(SourceFilePath);
            text = text.Replace(OldVaule, NewValue);
            File.WriteAllText(SourceFilePath, text);
        }

        public static void CopyFiles(string SourcePath, string DestinationPath)
        {
            //Now Create all of the directories
            foreach (string dirPath in Directory.GetDirectories(SourcePath, "*", SearchOption.AllDirectories)) {
                Directory.CreateDirectory(dirPath.Replace(SourcePath, DestinationPath));
            }
                    
            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(SourcePath, "*.*", SearchOption.AllDirectories)){
                File.Copy(newPath, newPath.Replace(SourcePath, DestinationPath), true);
            }
        }

        public static string GetPathGlobalConfigFile(bool showExt = true)
        {
            string fullPathGlobalConfig = @"" + HttpContext.Current.Server.MapPath(PathGlobalConfig);
            return System.Web.HttpUtility.UrlDecode(fullPathGlobalConfig);
        }

        public static string GetPathAppConfigFile(bool showExt = true)
        {
            string fullPathAppConfig = "";

            //string defaultAppKey = globalConfig.AppSettings.Settings["DefaultAppKey"].Value;
            string defaultAppKey = GetAppKey();
            string path = @"~/Content/" + defaultAppKey + "/config/";
            fullPathAppConfig = @"" + HttpContext.Current.Server.MapPath(path);

            fullPathAppConfig += "app";
            if (showExt)
            {
                fullPathAppConfig += ".config";
            }

            return System.Web.HttpUtility.UrlDecode(fullPathAppConfig);
        }

        public static bool IsValidRequestUrlFramework()
        {
            var result = true;
            var url = HttpContext.Current.Request.Url.ToString();

            //Ignore Report Server URL
            if (url.Contains(".axd"))
            {
                result = false;
            }

            //Ignore File Manager Util
            if (url.Contains(".aspx"))
            {
                result = false;
            }

            //Ignore /Error/PageNotFound
            if (url.Contains("Error/PageNotFound"))
            {
                result = false;
            }

            //Ignore /Error/AccessDenied
            if (url.Contains("Error/AccessDenied"))
            {
                result = false;
            }

            //Ignore /Error/Offline
            if (url.Contains("Error/Offline"))
            {
                result = false;
            }
            
            return result;
        }

        public static string GetFilesVersion()
        {
            var filesVersion = Automation.Core.Class.GlobalUtil.GetValueAppConfig("appSettings", "FilesVersion");

            if (String.IsNullOrEmpty(filesVersion))
            {
                filesVersion = "v01";
            }

            return filesVersion;
        }

        public static string GetSubAppPath()
        {
            var result = "";
            if (System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath != "/")
            {
                result = System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath;
            }
            return result;
        }

        public static string GetAppKey()
        {
            //App Key result
            var resultAppKey = "";

            //Get Default App From QueryString
            var pappKey = HttpContext.Current.Request.QueryString.Get("pappKey");
            if (!String.IsNullOrEmpty(pappKey))
            {
                resultAppKey = pappKey;
            }
            else
            {
                //Ignore Report Server URL
                if (IsValidRequestUrlFramework())
                {
                    // Calculate Default application from URL
                    // http://localhost:55777/SupportDocProdCtrl/
                    var requestURLSplit = HttpContext.Current.Request.RawUrl.Split('/');
                    if (requestURLSplit != null && requestURLSplit.Length > 1)
                    {
                        //Get URL App Key
                        var urlAppKey = requestURLSplit[1];

                        //Validate Sub Application
                        if (!String.IsNullOrEmpty(GetSubAppPath()))
                        {
                            //Fix: Index was outside the bounds of the array.
                            if (requestURLSplit.Length > 2)
                            {
                                //Get Application Name in next index
                                urlAppKey = requestURLSplit[2];
                            }
                            else
                            {
                                //When the URL is like https://uat.accountownership.financeandrisk.citigroup.net/SD
                                //Set empty appKey
                                urlAppKey = "";
                            }
                        }

                        //Return URL App Key
                        if (!String.IsNullOrEmpty(urlAppKey))
                        {
                            //Validate Framework Controllers Routes
                            if (urlAppKey != "Ajax" &&
                                urlAppKey != "Shared" &&
                                urlAppKey != "Reports" &&
                                urlAppKey != "Configuration" &&
                                urlAppKey != "Error")
                            {

                                resultAppKey = urlAppKey;
                            }
                        }
                    }

                    if (String.IsNullOrEmpty(resultAppKey))
                    {
                        //Return to Default of automation-global.config when is Root Directory
                        if (HttpContext.Current.Request.RawUrl == "/" ||
                            HttpContext.Current.Request.RawUrl == GetSubAppPath() ||
                            HttpContext.Current.Request.RawUrl == (GetSubAppPath() + "/"))
                        {
                            var defaultAppKey = Automation.Core.Class.GlobalUtil.GetValueGlobalConfig("appSettings", "DefaultAppKey");

                            //Fix bug: Cannot redirect after HTTP headers have been sent.
                            try
                            {
                                HttpContext.Current.Response.Redirect(GetSubAppPath() + "/" + defaultAppKey);
                            }
                            catch (Exception e) { }

                            resultAppKey = defaultAppKey;
                        }
                    }
                }

                if (String.IsNullOrEmpty(resultAppKey))
                {
                    //Return Default App "AutomationFramework"
                    resultAppKey = "AutomationFramework";
                }
            }

            //Validate if exists config files for current App
            string path = @"~/Content/" + resultAppKey + "/config/app.config";
            var fullPathAppConfig = @"" + HttpContext.Current.Server.MapPath(path);
            fullPathAppConfig = System.Web.HttpUtility.UrlDecode(fullPathAppConfig);
            if (System.IO.File.Exists(fullPathAppConfig) == false)
            {
                //Return Page Not Found
                var urlRequested = HttpContext.Current.Request.Url.AbsoluteUri;
                var linkPageNotFound = GetSubAppPath() + "/Error/PageNotFound?purl=" + Uri.EscapeDataString(urlRequested);
                
                if (HttpContext.Current.Request.RawUrl != linkPageNotFound)
                {
                    string msgError = "URL '" + HttpContext.Current.Request.Url.AbsoluteUri + "', app configuration file '" + path + "' was not found.";
                    AddSessionMessage("Error", msgError);

                    HttpContext.Current.Response.Redirect(linkPageNotFound);
                }

                //App Key not exists so return default "AutomationFramework"
                resultAppKey = "AutomationFramework";
            }

            return resultAppKey;
        }

        public static string GetPathAppConfigFile(bool showExt, string pkeyAppName)
        {
            string fullPathAppConfig = "";

            string path = @"~/Content/" + pkeyAppName + "/config/";
            fullPathAppConfig = @"" + HttpContext.Current.Server.MapPath(path);

            fullPathAppConfig += "app";
            if (showExt)
            {
                fullPathAppConfig += ".config";
            }

            return System.Web.HttpUtility.UrlDecode(fullPathAppConfig);
        }

        public static bool ExistsGlobalConfigFile(bool showMessage = true)
        {
            bool result = true;
            string filePath = GetPathGlobalConfigFile();
            if (System.IO.File.Exists(filePath) == false)
            {
                result = false;
            }

            if (result == false)
            {
                if (showMessage)
                {
                    AddSessionMessage("Error", "Global configuration file '" + filePath + "' was not found.", true);
                }
            }

            return result;
        }

        public static bool ExistsAppConfigFile(bool showMessage = true)
        {
            bool result = true;
            string filePath = GetPathAppConfigFile();
            if (System.IO.File.Exists(filePath) == false)
            {
                result = false;
            }

            if (result == false)
            {
                if (showMessage)
                {
                    AddSessionMessage("Error", "App configuration file '" + filePath + "' was not found.", true);
                }
            }

            return result;
        }

        public static Configuration GetGlobalConfiguration()
        {
            // Get Default app configuration options
            ExeConfigurationFileMap configMap = new ExeConfigurationFileMap();
            configMap.ExeConfigFilename = GetPathGlobalConfigFile(true);
            Configuration cfg = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);

            return cfg;
        }   

        public static Configuration GetAppConfiguration()
        {
            // Get Default app configuration options
            ExeConfigurationFileMap configMap = new ExeConfigurationFileMap();
            configMap.ExeConfigFilename = GetPathAppConfigFile(true);
            Configuration cfg = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);

            return cfg;
        }

        public static string SaveFileConfig(string file, string psection, string pkey, string pvalue, string paction)
        {
            string connString = "";
            Configuration cfg = null;

            if (file == "Global") 
            {
                cfg = GetGlobalConfiguration();
            }

            if (file == "App")
            {
                cfg = GetAppConfiguration();
            }

            switch (psection.ToLower())
            {
                case "connectionstrings":
                    var connSection = (ConnectionStringsSection)cfg.GetSection("connectionStrings");

                    if (paction.ToLower() == "update")
                    {
                        if (connSection.ConnectionStrings[pkey] != null)
                        {
                            connSection.ConnectionStrings[pkey].ConnectionString = pvalue;
                        }
                        else
                        {
                            connSection.ConnectionStrings.Add(new ConnectionStringSettings(pkey, pvalue));
                        }
                    }

                    if (paction.ToLower() == "delete")
                    {
                        connSection.ConnectionStrings.Remove(pkey);
                    }
                    break;

                case "appsettings":
                    var appSection = (AppSettingsSection)cfg.GetSection("appSettings");

                    if (paction.ToLower() == "update")
                    {
                        if (appSection.Settings[pkey] != null)
                        {
                            appSection.Settings[pkey].Value = pvalue;
                        }
                        else
                        {
                            appSection.Settings.Add(new KeyValueConfigurationElement(pkey, pvalue));
                        }
                    }

                    if (paction.ToLower() == "delete")
                    {
                        appSection.Settings.Remove(pkey);
                    }

                    break;
            }
           
            cfg.Save();

            return connString;
        }

        public static List<Dictionary<string, string>> ListAppConfig()
        {
            //var resultList = new List<Dictionary<string, string>> { 
            //    new Dictionary<string, string>
            //    {
            //        { "name", "soeid" },
            //        { "value", GlobalUtil.GetValueWebConfig("appSettings", "SOEIDOverride") }
            //    },
            //    new Dictionary<string, string>
            //    {
            //        { "name", "param2" },
            //        { "value", "value2" }
            //    }
            //};

            //List<object> list = new List<object>();
            //list.Add(new { name = "param1", value = "value1" });
            //list.Add(new { name = "param2", value = "value2" });
            //list.Add(new { name = "param3", value = "value3" });

            var resultList = new List<Dictionary<string, string>>();
            System.Configuration.Configuration config = GlobalUtil.GetAppConfiguration();

            //Get Connection Strings
            System.Configuration.ConnectionStringSettingsCollection connections = config.ConnectionStrings.ConnectionStrings;
            foreach (System.Configuration.ConnectionStringSettings item in connections)
            {
                resultList.Add(new Dictionary<string, string>
                {
                    { "section", "ConnectionStrings" },
                    { "name", item.Name },
                    { "value", item.ToString() }
                });
            }

            //Get App Settings
            System.Configuration.KeyValueConfigurationCollection appSetting = config.AppSettings.Settings;
            foreach (System.Configuration.KeyValueConfigurationElement item in appSetting)
            {
                resultList.Add(new Dictionary<string, string>
                {
                    { "section", "AppSettings" },
                    { "name", item.Key },
                    { "value", item.Value}
                });
            }

            return resultList;
        }

        public static string GetValueAppConfig(string psection, string pkey, string pkeyAppName)
        {
            string webConfigValue = "";

            if (ExistsGlobalConfigFile(false))
            {
                // Get Default app configuration options
                ExeConfigurationFileMap configMap = new ExeConfigurationFileMap();
                configMap.ExeConfigFilename = GetPathAppConfigFile(true, pkeyAppName);
                Configuration cfg = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);

                switch (psection.ToLower())
                {
                    case "connectionstrings":
                        if (cfg.ConnectionStrings.ConnectionStrings[pkey] != null)
                        {
                            webConfigValue = cfg.ConnectionStrings.ConnectionStrings[pkey].ConnectionString;
                        }
                        else
                        {
                            AddSessionMessage("Error", "The key '" + pkey + "' was not found in 'ConnectionStrings' in the file '" + GetPathAppConfigFile() + "'.", true);
                        }
                        break;

                    case "appsettings":
                        if (cfg.AppSettings.Settings[pkey] != null)
                        {
                            webConfigValue = cfg.AppSettings.Settings[pkey].Value;
                        }
                        else
                        {
                            AddSessionMessage("Error", "The key '" + pkey + "' was not found in 'AppSettings' in the file '" + GetPathAppConfigFile() + "'.", true);
                        }
                        break;
                }
            }

            return webConfigValue;
        }

        public static string GetValueAppConfig(string psection, string pkey)
        {
            string webConfigValue = "";

            if (ExistsGlobalConfigFile(false))
            {
                // Get Default app configuration options
                ExeConfigurationFileMap configMap = new ExeConfigurationFileMap();
                configMap.ExeConfigFilename = GetPathAppConfigFile(true);
                Configuration cfg = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);

                switch (psection.ToLower())
                {
                    case "connectionstrings":
                        if (cfg.ConnectionStrings.ConnectionStrings[pkey] != null)
                        {
                            webConfigValue = cfg.ConnectionStrings.ConnectionStrings[pkey].ConnectionString;
                        }
                        else
                        {
                            AddSessionMessage("Error", "The key '" + pkey + "' was not found in 'ConnectionStrings' in the file '" + GetPathAppConfigFile() + "'.", true);
                        }
                        break;

                    case "appsettings":
                        if (cfg.AppSettings.Settings[pkey] != null)
                        {
                            webConfigValue = cfg.AppSettings.Settings[pkey].Value;
                        }
                        else
                        {
                            AddSessionMessage("Error", "The key '" + pkey + "' was not found in 'AppSettings' in the file '" + GetPathAppConfigFile() + "'.", true);
                        }
                        break;
                }
            }

            return webConfigValue;
        }

        public static string GetValueGlobalConfig(string psection, string pkey)
        {
            string webConfigValue = "";

            if (ExistsGlobalConfigFile(false))
            {
                Configuration cfg = GetGlobalConfiguration();

                switch (psection.ToLower())
                {
                    case "connectionstrings":
                        if (cfg.ConnectionStrings.ConnectionStrings[pkey] != null)
                        {
                            webConfigValue = cfg.ConnectionStrings.ConnectionStrings[pkey].ConnectionString;
                        }
                        else
                        {
                            AddSessionMessage("Error", "The key '" + pkey + "' was not found in 'ConnectionStrings' in the file '" + GetPathGlobalConfigFile() + "'.", true);
                        }
                        break;

                    case "appsettings":
                        if (cfg.AppSettings.Settings[pkey] != null)
                        {
                            webConfigValue = cfg.AppSettings.Settings[pkey].Value;
                        }
                        else
                        {
                            AddSessionMessage("Error", "The key '" + pkey + "' was not found in 'AppSettings' in the file '" + GetPathGlobalConfigFile() + "'.", true);
                        }
                        break;
                }
            }

            return webConfigValue;
        }

        public static string GetTextFileAppConfig(string pkey)
        {
            string _pathToTextFile = GlobalUtil.GetValueAppConfig("appSettings", pkey);
            var textFile = new StringBuilder(System.IO.File.ReadAllText(@"" + System.Web.HttpContext.Current.Server.MapPath(_pathToTextFile)));

            return textFile.ToString();
        }

        public static string GetTextFileGlobalConfig(string pkey)
        {
            string _pathToTextFile = GlobalUtil.GetValueGlobalConfig("appSettings", pkey);
            var textFile = new StringBuilder(System.IO.File.ReadAllText(@"" + System.Web.HttpContext.Current.Server.MapPath(_pathToTextFile)));

            return textFile.ToString();
        }

        public static List<Dictionary<string, string>> GetListDictionayFromJsonTextFile(string file, string pkey)
        {
            string jsonText = null;
            if(file == "Global")
            {
                jsonText = GetTextFileGlobalConfig(pkey);
            }
            else
            {
                jsonText = GetTextFileAppConfig(pkey);
            }

            return JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(jsonText);
        }

        public static AppInfo GetCurrentAppInfo()
        {
            var pathAppInfo = GetValueAppConfig("AppSettings", "PathAppInfo");
            AppInfo objCurrentAppInfo = GetAppInfoFromPathJson(pathAppInfo);

            return objCurrentAppInfo;
        }

        public static Dictionary<string, List<Dictionary<string, string>>> GetCurrentAppData()
        {
            var pathAppData = GetValueAppConfig("AppSettings", "PathAppData");
            var textFile = new StringBuilder(System.IO.File.ReadAllText(@"" + System.Web.HttpContext.Current.Server.MapPath(pathAppData)));

            return JsonConvert.DeserializeObject<Dictionary<string, List<Dictionary<string, string>>>>(textFile.ToString()); ;
        }

        public static AppInfo GetAppInfoFromPathJson(string path)
        {
            var textFile = new StringBuilder(System.IO.File.ReadAllText(@"" + System.Web.HttpContext.Current.Server.MapPath(path)));
            return JsonConvert.DeserializeObject<AppInfo>(textFile.ToString());
        }

        public static List<AppInfo> GetListAppInfoFromJson(string json)
        {
            return JsonConvert.DeserializeObject<List<AppInfo>>(json);
        }

        // ---------------------------------------------
        // Admin exceptions Methods
        // ---------------------------------------------
        public static string GetExecutingMethodInfo(Exception pex)
        {
            string result = "No file found";
            Exception exception = pex;
            if (pex.InnerException != null)
            {
                exception = pex.InnerException;
            }
            var frames = new System.Diagnostics.StackTrace(exception, true).GetFrames();
            var frameError = new System.Diagnostics.StackTrace(exception, true).GetFrame(0);

            //Sometimes the information of the error line is in other Frame
            if (frames != null)
            {
                foreach (var objFrame in frames)
                {
                    if (objFrame.GetFileLineNumber() > 0)
                    {
                        frameError = objFrame;
                        break;
                    }
                }
            }
            
            if(frameError != null)
            {
                var fileName = frameError.GetFileName();
                if (!String.IsNullOrEmpty(fileName))
                {
                    string[] fileNameWords = fileName.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries);
                    result = string.Concat(fileNameWords[fileNameWords.Length - 1], ":", frameError.GetFileLineNumber());
                }
            }
            
            return result;
        }

        public static string GetExceptionMessage(Exception pexception)
        {
            HttpRequest prequest = HttpContext.Current.Request;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            var frames = new System.Diagnostics.StackTrace(pexception, true).GetFrames();
            StackFrame frameError = null;

            //Sometimes the information of the error line is in other Frame
            if (frames != null)
            {
                frameError = new System.Diagnostics.StackTrace(pexception, true).GetFrame(0);
                foreach (var objFrame in frames)
                {
                    if (objFrame.GetFileLineNumber() > 0)
                    {
                        frameError = objFrame;
                        break;
                    }
                }
            }

            //Set Connection String
            sb.Append("[ConnectionString]:" + System.Environment.NewLine + GetConnectionString() + System.Environment.NewLine + System.Environment.NewLine);

            //Set url request
            sb.Append("[URL]:" + System.Environment.NewLine + prequest.Url.ToString() + System.Environment.NewLine + System.Environment.NewLine);

            //Set Post Form Data
            if (!String.IsNullOrEmpty(prequest.Form.ToString()))
            {
                sb.Append("[Request.Form]:" + System.Environment.NewLine + prequest.Form.ToString() + System.Environment.NewLine + System.Environment.NewLine);
            }

            //Set Query String Url Data
            if (!String.IsNullOrEmpty(prequest.QueryString.ToString()))
            {
                sb.Append("[Request.QueryString]:" + System.Environment.NewLine + prequest.QueryString.ToString() + System.Environment.NewLine + System.Environment.NewLine);
            }

            if (pexception.Source != null)
            {
                //Set Source error
                sb.Append("[Source]:" + System.Environment.NewLine + pexception.Source.ToString() + System.Environment.NewLine + System.Environment.NewLine);
            }
            
            if (frameError != null)
            {
                //Set error line
                sb.Append("[Error Line]:" + System.Environment.NewLine + frameError.GetFileName() + ":" + frameError.GetFileLineNumber() + System.Environment.NewLine + System.Environment.NewLine);
            }
            
            //Set SOEID
            sb.Append("[SOEID]:" + System.Environment.NewLine + GetSOEID() + System.Environment.NewLine + System.Environment.NewLine);

            //Set Message
            sb.Append("[Message]:" + System.Environment.NewLine + pexception.Message + System.Environment.NewLine + System.Environment.NewLine);

            if (!String.IsNullOrEmpty(pexception.StackTrace))
            {
                //Set Stack Trace
                sb.Append("[Stack Trace]:" + System.Environment.NewLine + pexception.StackTrace + System.Environment.NewLine + System.Environment.NewLine);
            }
            
            if (pexception.InnerException != null)
            {
                sb.Append("-----------------" + System.Environment.NewLine);
                sb.Append("INNER EXCEPTION:" + System.Environment.NewLine);
                sb.Append("-----------------" + System.Environment.NewLine);
                sb.Append(GetExceptionMessage(pexception.InnerException));
            }

            return sb.ToString();
        }

        public static void SaveExceptionInDB(Exception pexception)
        {
            //Get exception message
            string pexceptionMsg = GetExceptionMessage(pexception);
            string idException = "0";

            //if (HttpContext.Current.Session != null && HttpContext.Current.Session["DBIsOnline"] != null && HttpContext.Current.Session["DBIsOnline"].ToString() == "1")
            if(GetValueAppConfig("AppSettings", "DBIsConfigured") == "1")
            {
                //Get id of exception in DB
                idException = (new Automation.Core.Model.GlobalModel()).saveExceptionDB(pexceptionMsg, pexception);
            }
            
            //Set cookie to show a custom error
            HttpCookie exceptionMsgCookie = new HttpCookie("exceptionMsg");
            exceptionMsgCookie.Value = "An error ocurred. Exception ID " + idException + ".";
            exceptionMsgCookie.Expires = DateTime.Now.AddSeconds(60);

            //Fix Bug: Server cannot modify cookies after HTTP headers have been sent.
            try
            {
                HttpContext.Current.Response.Cookies.Add(exceptionMsgCookie);
            }
            catch (Exception e){}
            
        }

        // ---------------------------------------------
        // File Manager Methods
        // ---------------------------------------------
        public static string CheckPathIsFileOrDirectory(string path) {
            //1 Option
            //------------------------------------
            // get the file attributes for file or directory
            FileAttributes attr = File.GetAttributes(@"" + path);

            //detect whether its a directory or file
            if (attr.HasFlag(FileAttributes.Directory))
            {
                return "Directory";
            }
            else
            {
                return "File";
            }
            
            //2 Option
            //------------------------------------
            //if (Directory.Exists(@"" + path))
            //{
            //    return "Directory";
            //}

            //if (File.Exists(@"" + path))
            //{
            //    return "File";
            //}

            //return "";

            //3 Option
            //------------------------------------
            //var dinfo = new DirectoryInfo(@"" + path);
            //var finfo = new FileInfo(@"" + path);

            //if (finfo.Exists)
            //{
            //    return "File";
            //}

            //if (dinfo.Exists)
            //{
            //    return "Directory";
            //}

            //return "Not Found!";
        }

        public static string FileSaveAs(string plocation, string pfileName, Stream pinputStream, bool isLocalFolderToUpload = true)
        {
            string uploadFolder = "";
            string fullPath = "";

            if (isLocalFolderToUpload)
            {
                uploadFolder = GetValueAppConfig("appSettings", "UploadFolder") + plocation + @"\";
                fullPath = @"" + HttpContext.Current.Server.MapPath(uploadFolder);
            }
            else
            {
                uploadFolder = plocation;
                fullPath = uploadFolder;
            }

            bool overwrite = true;
            bool autoCreateDirectory = true;
            
            if (autoCreateDirectory)
            {
                var directory = new FileInfo(fullPath).Directory;
                if (directory != null) {
                    directory.Create();
                } 
            }

            fullPath += pfileName;

            using (var file = new FileStream(fullPath, (overwrite ? FileMode.Create : FileMode.CreateNew))) 
            {
                pinputStream.CopyTo(file);
            }

            return fullPath;
        }

        public static FileUpload GetFileFromCurrentRequest(bool addCustomID = true)
        {
            FileUpload file = null;

            if(HttpContext.Current.Request.Files.Count > 0){

                var fileName = HttpContext.Current.Request.Files[0].FileName;

                if(!String.IsNullOrEmpty(HttpContext.Current.Request.Params["qqfilename"])){
                    fileName = HttpContext.Current.Request.Params["qqfilename"];
                }

                if (addCustomID)
                {
                    fileName = CreateCustomID() + "_" + fileName;
                }

                file = new FileUpload
                {
                    Filename = fileName,
                    InputStream = HttpContext.Current.Request.Files[0].InputStream
                };
            }

            return file;
        }

        public static System.Data.DataTable GetDataTableFromExcel(string pexcelPath, string psheetName = "", string pselect = "*")
        {
            System.Data.DataTable retval = null;
            try
            {
                string userSOEID = GetSOEID();
                string strConn = null;
                string fileExtension = Path.GetExtension(pexcelPath);

                if (fileExtension == ".xls")
                {
                    strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + pexcelPath + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\"";
                }
                else
                {
                    // for ".xlsx"
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + pexcelPath + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=1\"";
                }

                using (System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection(strConn))
                {
                    conn.Open();
                    String sheetName = "";
                    bool sheetNameFound = false;
                    string defaultSheetName = "TOUPLOAD$";
                    System.Data.DataTable dt = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new Object[] { null, null, null, "TABLE" });

                    if (!String.IsNullOrEmpty(psheetName))
                    {
                        defaultSheetName = psheetName + "$";
                    }

                    //Validate if we have "TOUPLOAD" Sheet
                    for (int i = 0; i < dt.Rows.Count && !sheetNameFound; i++)
                    {
                        if (dt.Rows[i]["TABLE_NAME"].ToString().Equals(defaultSheetName) || dt.Rows[i]["TABLE_NAME"].ToString().Equals("'" + defaultSheetName + "'"))
                        {
                            sheetName = dt.Rows[i]["TABLE_NAME"].ToString();
                            sheetNameFound = true;
                        }
                    }

                    if (!sheetNameFound)
                    {
                        //When we have only one Sheet
                        if (dt.Rows.Count == 1)
                        {
                            sheetName = dt.Rows[0]["TABLE_NAME"].ToString();
                            sheetNameFound = true;
                        }
                        else
                        {
                            //When we have TOUPLOAD$ and TOUPLOAD$_xlnm#_FilterDatabase
                            if (dt.Rows.Count == 2)
                            {
                                var sheetName1 = dt.Rows[0]["TABLE_NAME"].ToString();
                                var sheetName2 = dt.Rows[1]["TABLE_NAME"].ToString();

                                sheetName1 = sheetName1.Replace("_xlnm#", "");
                                sheetName1 = sheetName1.Replace("_FilterDatabase", "");

                                sheetName2 = sheetName2.Replace("_xlnm#", "");
                                sheetName2 = sheetName2.Replace("_FilterDatabase", "");

                                if (sheetName1.Equals(sheetName2))
                                {
                                    sheetName = sheetName1;
                                    sheetNameFound = true;
                                }
                            }
                        }
                    }

                    if (sheetNameFound)
                    {
                        using (System.Data.OleDb.OleDbCommand cmd = new System.Data.OleDb.OleDbCommand("SELECT " + pselect + " FROM [" + sheetName + "]", conn))
                        {
                            cmd.CommandType = System.Data.CommandType.Text;
                            using (System.Data.OleDb.OleDbDataAdapter da = new System.Data.OleDb.OleDbDataAdapter(cmd))
                            {
                                retval = new System.Data.DataTable();
                                da.Fill(retval);
                            }
                        }
                        conn.Close();
                    }
                    else
                    {
                        AddSessionMessage("Error", "You have multiples sheets in the excel file, please use 'TOUPLOAD' as sheet name to specify the Sheet with the data to upload.");
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("user-defined table type requires"))
                {
                    AddSessionMessage("Error", "Please review your excel file some columns are missing or there're extra columns in the file.");
                }
                else
                {
                    throw ex;
                }
                retval = null;
            }
            return retval;
        }

        public static System.Data.DataTable GetDataTableFromCSV(string pexcelPath)
        {
            string strConn = null;
            System.Data.DataTable retval = new System.Data.DataTable();
            string full = System.IO.Path.GetFullPath(pexcelPath);
            string file = System.IO.Path.GetFileName(full);
            string dir = System.IO.Path.GetDirectoryName(full);
            strConn = "Provider=Microsoft.Jet.OLEDB.4.0;"
                          + "Data Source=\"" + dir + "\\\";"
                          + "Extended Properties=\"text;HDR=YES;FMT=Delimited\"";
            string query = "SELECT * FROM [" + file + "]";

            //create an OleDbDataAdapter to execute the query
            OleDbDataAdapter dAdapter = new OleDbDataAdapter(query, strConn);
            dAdapter.Fill(retval);

            dAdapter.Dispose();
            //dsOutput.Tables.Add(retval.Copy());
            return retval;
        }

        //public static string CreatePDFFromHTML(string pname, string phtml){
        //    string uploadFolder = GetValueAppConfig("appSettings", "UploadFolder") + "TempPDF" + @"/";
        //    string uploadFolderMap = HttpContext.Current.Server.MapPath(uploadFolder) ;

        //    //If not exist TempPDF Create it
        //    var directory = new FileInfo(uploadFolderMap).Directory;
        //    if (directory == null || (directory != null && directory.Exists == false))
        //    {
        //        //Create TempPDF Folder
        //        Directory.CreateDirectory(uploadFolderMap);

        //    }

        //    //Delete files from UploadFolder/TempPDF
        //    var fileGenerationDir = new DirectoryInfo(uploadFolderMap);
        //    fileGenerationDir.GetFiles("*", SearchOption.AllDirectories).ToList().ForEach(objFile => { objFile.Delete(); }); 

        //    // Directory to write
        //    //sPathToWritePdfTo: C:\\Users\\CD25867\\Documents\\TeamFoundationProjects\\ARE.HRTT\\Solution\\HRTT.Web\\Temp\\DevelopmentPlanReports\\DevelopmentPlan_CD25867_2017050813353011.pdf
        //    var sPathToWritePdfTo = uploadFolderMap + pname + "_" + CreateCustomID() + ".pdf";

        //    //Create new file
        //    var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
        //    var pdfBytes = htmlToPdf.GeneratePdf(phtml);
        //    File.WriteAllBytes(sPathToWritePdfTo, pdfBytes);

        //    return sPathToWritePdfTo;
        //}

        // ---------------------------------------------
        // JqxGrid api Methods
        // ---------------------------------------------
        public static Automation.Core.JqxGrid.JqxGrid GetJqxGridFromJson(string json)
        {
            return JsonConvert.DeserializeObject<Automation.Core.JqxGrid.JqxGrid>(json);
        }

        public static string GetWhereFilters()
        {
            string where = "";
            var Request = HttpContext.Current.Request;

            if (Request.Params["filterscount"] != null && Request.Params["filterscount"] != "0")
            {
                int filterCount = Convert.ToInt32(Request.Params["filterscount"]);

                where = " WHERE (";
                string tmpDataField = "";
                int tmpFilterOperator = 0;

                string filterValue = "";
                string filterCondition = "";
                int filterOperator = 0;
                string filterDataField = "";
                string filterDateTimeValue = "";

                for (var i = 0; i < filterCount; i++)
                {
                    //Get the filter's value.
                    filterValue = Request.Params["filtervalue" + i];

                    //Get the filter's condition.
                    filterCondition = Request.Params["filtercondition" + i];

                    //Get the filter's column.
                    filterDataField = Request.Params["filterdatafield" + i];

                    //Get the filter's operator.
                    filterOperator = Convert.ToInt32(Request.Params["filteroperator" + i]);

                    //Datetime Value
                    filterDateTimeValue = Request.Params["filterGroups["+i+"][filters][0][value]"];
                    if (filterDateTimeValue != null)
                    {
                        if (Request.Params["filterGroups[" + i + "][filters][0][type]"] == "datefilter")
                        {
                            //Remove GMT-0600 (Central America Standard Time)
                            int index = filterDateTimeValue.IndexOf(" GMT");
                            if (index > 0)
                            {
                                filterDateTimeValue = filterDateTimeValue.Substring(0, index);
                            }
                            DateTime temjpDate = DateTime.ParseExact(filterDateTimeValue, "ddd MMM dd yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                            filterValue = temjpDate.ToString("yyyy-MM-dd");
                        }
                    }

                    //filterOperatorText = pparams[filterDataField + "operator"];
                    if (tmpDataField == "")
                    {
                        tmpDataField = filterDataField;
                    }
                    else if (tmpDataField != filterDataField)
                    {

                        where += ") AND (";
                    }
                    else if (tmpDataField == filterDataField)
                    {
                        if (tmpFilterOperator == 0)
                        {
                            where += " AND ";
                        }
                        else 
                        {
                            where += " OR ";
                        } 
                    }

                    //Build the "WHERE" clause depending on the filter's condition, value and datafield.
                    switch (filterCondition)
                    {
                        case "CONTAINS":
                            where += " " + filterDataField + " LIKE '%" + filterValue + "%'";
                            break;
                        case "DOES_NOT_CONTAIN":
                            where += " " + filterDataField + " NOT LIKE '%" + filterValue + "%'";
                            break;
                        case "EQUAL":
                            where += " " + filterDataField + " = '" + filterValue + "'";
                            break;
                        case "NOT_EQUAL":
                            where += " " + filterDataField + " <> '" + filterValue + "'";
                            break;
                        case "GREATER_THAN":
                            where += " " + filterDataField + " > '" + filterValue + "'";
                            break;
                        case "LESS_THAN":
                            where += " " + filterDataField + " < '" + filterValue + "'";
                            break;
                        case "GREATER_THAN_OR_EQUAL":
                            where += " " + filterDataField + " >= '" + filterValue + "'";
                            break;
                        case "LESS_THAN_OR_EQUAL":
                            where += " " + filterDataField + " <= '" + filterValue + "'";
                            break;
                        case "STARTS_WITH":
                            where += " " + filterDataField + " LIKE '" + filterValue + "%'";
                            break;
                        case "ENDS_WITH":
                            where += " " + filterDataField + " LIKE '%" + filterValue + "'";
                            break;
                    }

                    if (i == (filterCount - 1))
                    {
                        where += ")";
                    }

                    tmpFilterOperator = filterOperator;
                    tmpDataField = filterDataField;
                }
            }

            return where;
        }

        public static string GetOrderBy()
        {
            string orderBy = "";
            var Request = HttpContext.Current.Request;

            //ORDER BY
            if (Request.Params["sortdatafield"] != null)
            {
                orderBy += " ORDER BY " + Request.Params["sortdatafield"] + " " + Request.Params["sortorder"];
            }

            return orderBy;
        }
    }
}
