﻿using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Automation.Core.Class;

namespace Automation.Core.Model
{
    public class GlobalModel
    {
        public bool HasColumn(DbDataReader Reader, string ColumnName)
        {
            var schemaTable = Reader.GetSchemaTable();
            if (schemaTable != null)
            {
                foreach (DataRow row in schemaTable.Rows)
                {
                    if (row["ColumnName"].ToString() == ColumnName)
                        return true;
                } //Still here? Column not found. 
            }
                
            return false;
        }

        public List<Dictionary<string, string>> excecuteProcedureReturn(string pspDB, List<Dictionary<string, string>> spDBParams)
        {
            var resultList = new List<Dictionary<string, string>>();
            try
            {
                using (var conn = GetSqlConnection())
                {
                    using (var command = new SqlCommand(pspDB, conn))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        foreach (var item in spDBParams)
                        {
                            command.Parameters.AddWithValue(item["Name"], item["Value"]);
                            //db.AddInParameter(comm, item["Name"], DbType.String, item["Value"]);
                        }

                        System.Data.SqlClient.SqlDataReader rdr = command.ExecuteReader();
                        //var hasTotalRows = HasColumn(rdr, "TotalRows");

                        while (rdr.Read())
                        {
                            var rowData = new Dictionary<String, String>();
                            var columns = new List<string>();

                            //Get columns names from DataTable
                            foreach (DataRow row in rdr.GetSchemaTable().Rows)
                            {
                                columns.Add(row["ColumnName"].ToString());
                            }

                            //Add columns information
                            foreach (var columnName in columns)
                            {
                                rowData.Add(columnName, rdr[columnName].ToString());
                            }
                            //if (hasTotalRows)
                            //{
                            //    rowData.Add("TotalRows", rdr["TotalRows"].ToString());
                            //}

                            resultList.Add(rowData);
                        }

                        command.Connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                //Add extra information to the original exception
                String extraInfo = "[SQL]:" + System.Environment.NewLine + pspDB + System.Environment.NewLine + System.Environment.NewLine;
                extraInfo += "[Params]:" + System.Environment.NewLine;

                if (spDBParams != null)
                {
                    foreach (var item in spDBParams)
                    {
                        extraInfo += item["Name"] + ": [" + item["Value"] + "]" + System.Environment.NewLine;
                    }
                }

                Exception customEx = new Exception(extraInfo, ex);
                throw customEx;
            }
            return resultList;
        }

        public int excecuteProcedureNoReturn(string pspDB, List<Dictionary<string, string>> spDBParams)
        {
            int affectedRows = 0;
            try
            {
                String connString = GlobalUtil.GetConnectionString();

                using (var conn = new SqlConnection(connString))
                {
                    using (var command = new SqlCommand(pspDB, conn))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        foreach (var item in spDBParams)
                        {
                            command.Parameters.AddWithValue(item["Name"], item["Value"]);
                            //db.AddInParameter(comm, item["Name"], DbType.String, item["Value"]);
                        }

                        conn.Open();

                        affectedRows = command.ExecuteNonQuery();

                        command.Connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                //Add extra information to the original exception
                String extraInfo = "[SQL]:" + System.Environment.NewLine + pspDB + System.Environment.NewLine + System.Environment.NewLine;
                extraInfo += "[Params]:" + System.Environment.NewLine;

                if (spDBParams != null)
                {
                    foreach (var item in spDBParams)
                    {
                        extraInfo += item["Name"] + ": [" + item["Value"] + "]" + System.Environment.NewLine;
                    }
                }

                Exception customEx = new Exception(extraInfo, ex);

                //Throw new exception
                throw customEx;
            }
            return affectedRows;
        }

        public List<Dictionary<string, string>> getResultListJqxGrid(string pspDB, List<Dictionary<string, string>> pspDBParmas, List<Dictionary<string, string>> pcolsToSelect)
        {
            return excecuteProcedureReturn(pspDB, pspDBParmas);
        }

        public List<Dictionary<string, string>> getResultList(string pspDB, List<Dictionary<string, string>> pspDBParmas, List<Dictionary<string, string>> pcolsToSelect)
        {
            var resultList = new List<Dictionary<string, string>>();
            
            DataSet ds = null;
            String connString = GlobalUtil.GetConnectionString();
            Database db = new Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase(connString);
            DbCommand comm = db.GetStoredProcCommand(pspDB);
            comm.CommandTimeout = Int32.Parse(GlobalUtil.GetValueAppConfig("Appsettings", "DBQueriesTimeoutInSeconds"));

            foreach (var item in pspDBParmas)
            {
                db.AddInParameter(comm, item["Name"], DbType.String, item["Value"]);
            }

            ds = db.ExecuteDataSet(comm);

            if ((ds != null))
            {
                if (ds.Tables.Count >= 1)
                {
                    DataTable objTable = ds.Tables[0];

                    for (int _i = 0; _i <= objTable.Rows.Count - 1; _i++)
                    {
                        var rowData = new Dictionary<String, String>();

                        //Add columns information
                        foreach (var item in pcolsToSelect)
                        {
                            rowData.Add(item["Name"], objTable.Rows[_i][item["Name"]].ToString());
                        }
                        resultList.Add(rowData);
                    }
                }
            }
            else
            {
                return null;
            }

            return resultList;
        }

        public string saveExceptionDB(string pexceptionMsg, Exception pexception)
        {
            //Get Method info: 
            //ExceptionClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            //ExceptionMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var idException = "0";
            var spName = GlobalUtil.GetDBObjectFullName("InsertException");

            var _data = new List<Dictionary<string, string>> { 
                new Dictionary<string, string>
                {
                    { "Name", "@SOE_ID_SESSION" },
                    { "Value", GlobalUtil.GetSOEID() }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@METHOD" },
                    { "Value", GlobalUtil.GetExecutingMethodInfo(pexception) }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@EXCEPTION" },
                    { "Value", pexceptionMsg }
                }
            };

            var result = excecuteProcedureReturn(spName, _data);

            if (result.Count > 0)
            {
                idException = result[0]["ID_EXCEPTION"];
            }
            return idException;
        }

        public List<Dictionary<string, string>> excecuteQuery(string cmdText, Dictionary<string, string> parameters = null)
        {
            Dictionary<string, string> row;
            int rowsAffected = 0;
            List<Dictionary<string, string>> results = new List<Dictionary<string, string>>();
            SqlDataReader reader = null;
            SqlConnection connection = null;

            try
            {
                String LConnectionString = GlobalUtil.GetConnectionString();

                using (connection = new SqlConnection(LConnectionString))
                {
                    connection.Open();

                    string[] commands = cmdText.Split(new string[] { "GO\r\n", "GO\n", "\nGO", "GO\t", "GO \n", "go\r\n", "go\n", "\ngo", "go\t", "go \n" }, StringSplitOptions.RemoveEmptyEntries);
                    int cont = 1;
                    int qtyCommands = commands.Length;
                    int dbQueryTimeout = Int32.Parse(GlobalUtil.GetValueAppConfig("Appsettings", "DBQueriesTimeoutInSeconds"));

                    foreach (string c in commands)
                    {
                        using (SqlCommand command = new SqlCommand(c, connection))
                        {
                            command.CommandTimeout = dbQueryTimeout;

                            //Add parameters
                            if (parameters != null)
                            {
                                foreach (string key in parameters.Keys)
                                {
                                    command.Parameters.AddWithValue(key, parameters[key]);
                                }
                            }

                            if (cont == qtyCommands)
                            {
                                reader = command.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
                            }
                            else
                            {
                                rowsAffected = command.ExecuteNonQuery();
                            }
                            cont++;
                        }
                    }

                    while (reader.Read())
                    {
                        row = new Dictionary<string, string>();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            object rowCol = reader.GetValue(i);
                            row.Add(reader.GetName(i), rowCol.ToString());
                        }
                        results.Add(row);
                    }
                    connection.Close();

                }
            }
            catch (Exception ex)
            {
                //Add extra information to the original exception
                String extraInfo = "[SQL]:" + System.Environment.NewLine + cmdText + System.Environment.NewLine + System.Environment.NewLine;
                Exception customEx = new Exception(extraInfo, ex);

                //Throw new exception
                throw customEx;
            }
            return results;
        }

        public List<Dictionary<string, string>> getResultListBySQL(string psqlDB)
        {
            var resultList = new List<Dictionary<string, string>>();
            SqlConnection conn = null;

            try
            {
                String LConnectionString = GlobalUtil.GetConnectionString();

                using (conn = new SqlConnection(LConnectionString))
                {
                    using (SqlCommand comm = new SqlCommand(psqlDB, conn))
                    {
                        comm.CommandTimeout = Int32.Parse(GlobalUtil.GetValueAppConfig("Appsettings", "DBQueriesTimeoutInSeconds"));
                        comm.Connection.Open();

                        System.Data.SqlClient.SqlDataReader rdr = comm.ExecuteReader();

                        var hasTotalRows = HasColumn(rdr, "TotalRows");

                        while (rdr.Read())
                        {
                            var rowData = new Dictionary<String, String>();
                            var columns = new List<string>();

                            //Get columns names from DataTable
                            foreach (DataRow row in rdr.GetSchemaTable().Rows)
                            {
                                columns.Add(row["ColumnName"].ToString());
                            }

                            //Add columns information
                            foreach (var columnName in columns)
                            {
                                rowData.Add(columnName, rdr[columnName].ToString());
                            }
                            if (hasTotalRows)
                            {
                                rowData.Add("TotalRows", rdr["TotalRows"].ToString());
                            }

                            resultList.Add(rowData);
                        }

                        comm.Connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                //Add extra information to the original exception
                String extraInfo = "[SQL]:" + System.Environment.NewLine + psqlDB;
                Exception customEx = new Exception(extraInfo, ex);

                //Throw new exception
                throw customEx;
            }
            finally 
            {
                if(conn != null && conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            
            return resultList;
        }

        public void excuteNonQueryBySQL(string psqlDB)
        {
            var resultList = new List<Dictionary<string, string>>();
            SqlConnection conn = null;

            try
            {
                String LConnectionString = GlobalUtil.GetConnectionString();

                using (conn = new SqlConnection(LConnectionString))
                {
                    using (SqlCommand comm = new SqlCommand(psqlDB, conn))
                    {
                        comm.CommandTimeout = Int32.Parse(GlobalUtil.GetValueAppConfig("Appsettings", "DBQueriesTimeoutInSeconds"));
                        comm.Connection.Open();

                        comm.ExecuteNonQuery();

                        comm.Connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                //Add extra information to the original exception
                String extraInfo = "[SQL]:" + System.Environment.NewLine + psqlDB;
                Exception customEx = new Exception(extraInfo, ex);

                //Throw new exception
                throw customEx;
            }
            finally
            {
                if (conn != null && conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
        }

        public DataTable getDataTableBySQL(string psqlDB, int timeout = 0)
        {
            DataTable dtResult = new DataTable();
            SqlConnection conn = null;
            SqlCommand comm = null;

            String LConnectionString = GlobalUtil.GetConnectionString();
            conn = new SqlConnection(LConnectionString);
            comm = new SqlCommand(psqlDB, conn);
            comm.CommandTimeout = Int32.Parse(GlobalUtil.GetValueAppConfig("Appsettings", "DBQueriesTimeoutInSeconds")); 
            comm.Connection.Open();

            System.Data.SqlClient.SqlDataReader rdr = comm.ExecuteReader();
            dtResult = new DataTable();
            dtResult.Load(rdr);

            comm.Connection.Close();

            return dtResult;
        }

        public SqlConnection GetSqlConnection()
        {
            String connString = GlobalUtil.GetConnectionString();
            SqlConnection conn = null;
            conn = new SqlConnection(connString);

            int maxAttempts = 1;
            int attempts = 0;
            Exception lastException = null;

            while (conn.State != ConnectionState.Open && attempts != maxAttempts)
            {
                //Fix for [Win32Exception (0x80004005): The network path was not found]
                try
                {
                    conn.Open();
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null && (ex.InnerException.Message.Contains("The network path was not found") || ex.InnerException.Message.Contains("The wait operation timed out")))
                    {
                        String ClearPoolTimes = GlobalUtil.GetValueAppConfig("appSettings", "ClearPoolTimes");
                        SqlConnection.ClearPool(conn);
                        GlobalUtil.SaveFileConfig("App", "AppSettings", "ClearPoolTimes", (Int32.Parse(ClearPoolTimes) + 1).ToString(), "Update");

                        lastException = ex;
                    }
                    else
                    {
                        throw ex;
                    }
                }

                if (conn.State != ConnectionState.Open)
                {
                    System.Threading.Thread.Sleep(1000);
                    attempts++;
                }
            }

            if (conn.State != ConnectionState.Open)
            {
                throw lastException;
            }
            return conn;
        }

        public int SaveDataTableDB(DataTable pdataExcel, string pspName, List<Dictionary<string, string>> spDBParams = null)
        {
            int affectedRows = 0;

            try
            {
                using (var conn = GetSqlConnection())
                {
                    using (var command = new SqlCommand(pspName, conn))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        SqlParameter ptable = new SqlParameter("@ExcelTable", SqlDbType.Structured);
                        ptable.Value = pdataExcel;
                        command.Parameters.Add(ptable);

                        if (spDBParams != null)
                        {
                            foreach (var item in spDBParams)
                            {
                                command.Parameters.AddWithValue(item["Name"], item["Value"]);
                            }
                        }

                        affectedRows = command.ExecuteNonQuery();

                        command.Connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                //Add extra information to the original exception
                String extraInfo = "[SQL]:" + System.Environment.NewLine + pspName + System.Environment.NewLine + System.Environment.NewLine;
                extraInfo += "[Params]:" + System.Environment.NewLine;

                if (spDBParams != null)
                {
                    foreach (var item in spDBParams)
                    {
                        extraInfo += item["Name"] + ": [" + item["Value"] + "]" + System.Environment.NewLine;
                    }
                }

                Exception customEx = new Exception(extraInfo, ex);

                //Throw new exception
                throw customEx;
            }

            return affectedRows;
        }






        public int SaveDataTableDB(
            DataTable UDT_5Columns,
            DataTable UDT_10Columns,
            DataTable UDT_20Columns,
            DataTable UDT_30Columns,
            DataTable UDT_40Columns,
            DataTable UDT_50Columns,
            DataTable UDT_60Columns,
            DataTable UDT_70Columns,
            DataTable UDT_80Columns,
            DataTable UDT_90Columns,
            DataTable UDT_100Columns,
            DataTable UDT_110Columns,
            DataTable UDT_120Columns,
            DataTable UDT_130Columns,
            DataTable UDT_140Columns,
            DataTable UDT_150Columns,
            DataTable UDT_160Columns,
            DataTable UDT_170Columns,
            DataTable UDT_180Columns,
            DataTable UDT_190Columns,
            DataTable UDT_200Columns,
            string pspName, List<Dictionary<string, string>> spDBParams = null)
        {
            int affectedRows = 0;

            try
            {
                using (var conn = GetSqlConnection())
                {
                    using (var command = new SqlCommand(pspName, conn))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        SqlParameter pUDT_5Columns = new SqlParameter("@UDT_5Columns", SqlDbType.Structured); pUDT_5Columns.Value = UDT_5Columns; command.Parameters.Add(pUDT_5Columns);
                        SqlParameter pUDT_10Columns = new SqlParameter("@UDT_10Columns", SqlDbType.Structured); pUDT_10Columns.Value = UDT_10Columns; command.Parameters.Add(pUDT_10Columns);
                        SqlParameter pUDT_20Columns = new SqlParameter("@UDT_20Columns", SqlDbType.Structured); pUDT_20Columns.Value = UDT_20Columns; command.Parameters.Add(pUDT_20Columns);
                        SqlParameter pUDT_30Columns = new SqlParameter("@UDT_30Columns", SqlDbType.Structured); pUDT_30Columns.Value = UDT_30Columns; command.Parameters.Add(pUDT_30Columns);
                        SqlParameter pUDT_40Columns = new SqlParameter("@UDT_40Columns", SqlDbType.Structured); pUDT_40Columns.Value = UDT_40Columns; command.Parameters.Add(pUDT_40Columns);
                        SqlParameter pUDT_50Columns = new SqlParameter("@UDT_50Columns", SqlDbType.Structured); pUDT_50Columns.Value = UDT_50Columns; command.Parameters.Add(pUDT_50Columns);
                        SqlParameter pUDT_60Columns = new SqlParameter("@UDT_60Columns", SqlDbType.Structured); pUDT_60Columns.Value = UDT_60Columns; command.Parameters.Add(pUDT_60Columns);
                        SqlParameter pUDT_70Columns = new SqlParameter("@UDT_70Columns", SqlDbType.Structured); pUDT_70Columns.Value = UDT_70Columns; command.Parameters.Add(pUDT_70Columns);
                        SqlParameter pUDT_80Columns = new SqlParameter("@UDT_80Columns", SqlDbType.Structured); pUDT_80Columns.Value = UDT_80Columns; command.Parameters.Add(pUDT_80Columns);
                        SqlParameter pUDT_90Columns = new SqlParameter("@UDT_90Columns", SqlDbType.Structured); pUDT_90Columns.Value = UDT_90Columns; command.Parameters.Add(pUDT_90Columns);
                        SqlParameter pUDT_100Columns = new SqlParameter("@UDT_100Columns", SqlDbType.Structured); pUDT_100Columns.Value = UDT_100Columns; command.Parameters.Add(pUDT_100Columns);
                        SqlParameter pUDT_110Columns = new SqlParameter("@UDT_110Columns", SqlDbType.Structured); pUDT_110Columns.Value = UDT_110Columns; command.Parameters.Add(pUDT_110Columns);
                        SqlParameter pUDT_120Columns = new SqlParameter("@UDT_120Columns", SqlDbType.Structured); pUDT_120Columns.Value = UDT_120Columns; command.Parameters.Add(pUDT_120Columns);
                        SqlParameter pUDT_130Columns = new SqlParameter("@UDT_130Columns", SqlDbType.Structured); pUDT_130Columns.Value = UDT_130Columns; command.Parameters.Add(pUDT_130Columns);
                        SqlParameter pUDT_140Columns = new SqlParameter("@UDT_140Columns", SqlDbType.Structured); pUDT_140Columns.Value = UDT_140Columns; command.Parameters.Add(pUDT_140Columns);
                        SqlParameter pUDT_150Columns = new SqlParameter("@UDT_150Columns", SqlDbType.Structured); pUDT_150Columns.Value = UDT_150Columns; command.Parameters.Add(pUDT_150Columns);
                        SqlParameter pUDT_160Columns = new SqlParameter("@UDT_160Columns", SqlDbType.Structured); pUDT_160Columns.Value = UDT_160Columns; command.Parameters.Add(pUDT_160Columns);
                        SqlParameter pUDT_170Columns = new SqlParameter("@UDT_170Columns", SqlDbType.Structured); pUDT_170Columns.Value = UDT_170Columns; command.Parameters.Add(pUDT_170Columns);
                        SqlParameter pUDT_180Columns = new SqlParameter("@UDT_180Columns", SqlDbType.Structured); pUDT_180Columns.Value = UDT_180Columns; command.Parameters.Add(pUDT_180Columns);
                        SqlParameter pUDT_190Columns = new SqlParameter("@UDT_190Columns", SqlDbType.Structured); pUDT_190Columns.Value = UDT_190Columns; command.Parameters.Add(pUDT_190Columns);
                        SqlParameter pUDT_200Columns = new SqlParameter("@UDT_200Columns", SqlDbType.Structured); pUDT_200Columns.Value = UDT_200Columns; command.Parameters.Add(pUDT_200Columns);

                        if (spDBParams != null)
                        {
                            foreach (var item in spDBParams)
                            {
                                command.Parameters.AddWithValue(item["Name"], item["Value"]);
                            }
                        }

                        command.CommandTimeout = 0;

                        affectedRows = command.ExecuteNonQuery();

                        command.Connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                //Add extra information to the original exception
                String extraInfo = "[SQL]:" + System.Environment.NewLine + pspName + System.Environment.NewLine + System.Environment.NewLine;
                extraInfo += "[Params]:" + System.Environment.NewLine;

                if (spDBParams != null)
                {
                    foreach (var item in spDBParams)
                    {
                        extraInfo += item["Name"] + ": [" + item["Value"] + "]" + System.Environment.NewLine;
                    }
                }

                Exception customEx = new Exception(extraInfo, ex);

                //Throw new exception
                throw customEx;
            }

            return affectedRows;
        }
    }
}
