﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Automation.Core.Class;

namespace Automation.Web.Views.Shared
{
    public partial class CustomReportViewer : System.Web.UI.Page
    {
        public static string filesVersion = Automation.Core.Class.GlobalUtil.GetFilesVersion();
        public static string subAppPath = Automation.Core.Class.GlobalUtil.GetSubAppPath();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {   
                DataBind();

                string reportServerUrl = GlobalUtil.GetReportServerUrl();
                string sesionSOEID = GlobalUtil.GetSOEID();

                if (ReportViewer.ServerReport.IsDrillthroughReport)
                {
                    ReportViewer.Reset();
                }

                ReportViewer.Visible = true;

                ReportViewer.ProcessingMode = ProcessingMode.Remote;
                ReportViewer.ShowParameterPrompts = true;
                ReportViewer.ShowPrintButton = false;
                ReportViewer.SizeToReportContent = true;
                ReportViewer.ShowFindControls = true;
                ReportViewer.ShowBackButton = true;
                ReportViewer.ShowZoomControl = true;


                //show selected reportname in ReportViewer Header
                //pass serverUrl and reportpath to ReportViewer
                this.ReportViewer.ServerReport.ReportServerUrl = new System.Uri(reportServerUrl);
                this.ReportViewer.ServerReport.ReportPath = Request.QueryString["reportPath"];
                HFIframeID.Value = Request.QueryString["iframeID"];

                
                //passs appuser soeid as report parameter
                try
                {
                    this.ReportViewer.ServerReport.SetParameters(new ReportParameter("SOEID", sesionSOEID, true));
                }catch(Exception ex){}

                //set timezone information
                try
                {
                    this.ReportViewer.ServerReport.SetParameters(new ReportParameter("UTCOffsetMinutes", Request.QueryString["utcOffsetMinutes"], true));
                    this.ReportViewer.ServerReport.SetParameters(new ReportParameter("UserCurrentDateTime", Request.QueryString["userCurrentDateTime"], true));
                }
                catch (Exception ex) { }

                if (this.Request.QueryString.HasKeys())
                {
                    foreach (string s in this.Request.QueryString.Keys)
                    {
                        //if (s.StartsWith("parameter", StringComparison.InvariantCultureIgnoreCase)) 
                        //{ 
                        try
                        {
                            if (s != "reportPath" && s != "reportName" && s != "iframeID")
                            {
                                ReportViewer.ServerReport.SetParameters(new ReportParameter(s, Request.QueryString[s], true));
                            }
                        }
                        catch{}
                    }
                }

                GlobalUtil.CreateReportLog(Request.QueryString["reportPath"]);

                //if (System.Web.HttpContext.Current.Session["CreatedLog"] == null)
                //{
                    //Session_Util.CreateReportLog(Request.QueryString["reportName"], 29);
                    //System.Web.HttpContext.Current.Session["CreatedLog"] = true;
                //}
                //else
                //{
                //    System.Web.HttpContext.Current.Session["CreatedLog"] = null;
                //}

                //this.ReportViewer.ServerReport.Refresh();
            }
        }

        protected void ReportViewer_Back(object sender, BackEventArgs e)
        {
            ServerReport sr = (ServerReport)e.ParentReport;
            sr.Refresh();
        }

        protected void ReportViewer_Drillthrough(object sender, DrillthroughEventArgs e)
        {
            if (e.Report.IsDrillthroughReport)
            {
                ServerReport sr = (ServerReport)e.Report;
                sr.Refresh();
            }
        }
    }
}