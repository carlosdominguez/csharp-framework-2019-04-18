//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Automation.Web.Models.EC
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblEC_Business
    {
        public tblEC_Business()
        {
            this.tblEC_BusinessFiles = new HashSet<tblEC_BusinessFiles>();
            this.tblEC_EquationBusinessLift = new HashSet<tblEC_EquationBusinessLift>();
            this.tblEC_SubBusiness = new HashSet<tblEC_SubBusiness>();
        }
    
        public int ID { get; set; }
        public string Name { get; set; }
        public bool Multiple { get; set; }
        public string BU { get; set; }
        public Nullable<int> EditCheckType { get; set; }
    
        public virtual ICollection<tblEC_BusinessFiles> tblEC_BusinessFiles { get; set; }
        public virtual ICollection<tblEC_EquationBusinessLift> tblEC_EquationBusinessLift { get; set; }
        public virtual ICollection<tblEC_SubBusiness> tblEC_SubBusiness { get; set; }
    }
}
