//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Automation.Web.Models.EC
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblEC_EquationRule
    {
        public int ID { get; set; }
        public int EquationID { get; set; }
        public Nullable<bool> IsLeft { get; set; }
        public Nullable<bool> IsSum { get; set; }
        public Nullable<int> ShortID { get; set; }
        public Nullable<int> IceCodeID { get; set; }
        public Nullable<int> Prior { get; set; }
        public string AMT_FLG { get; set; }
        public string SET_TYPE { get; set; }
    
        public virtual tblEC_FileShortNameDescr tblEC_FileShortNameDescr { get; set; }
        public virtual tblEC_IceCodeMapping tblEC_IceCodeMapping { get; set; }
    }
}
