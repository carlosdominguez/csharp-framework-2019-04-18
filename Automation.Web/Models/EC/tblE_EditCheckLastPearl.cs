//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Automation.Web.Models.EC
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblE_EditCheckLastPearl
    {
        public int ID { get; set; }
        public string YEAR { get; set; }
        public string PERIOD { get; set; }
        public string TYPEACCOUNT { get; set; }
        public string FRSBU { get; set; }
        public string LVID { get; set; }
        public string AFFILIATE { get; set; }
        public string GOC { get; set; }
        public string FDLACCOUNT { get; set; }
        public string FDLACCOUNTDES { get; set; }
        public string AMOUNT { get; set; }
    }
}
