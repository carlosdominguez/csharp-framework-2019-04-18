//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Automation.Web.Models.EC
{
    using System;
    
    public partial class SP_EditCheck_GetGridBusinessFRS_Result
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Lift { get; set; }
        public int Pass { get; set; }
        public int Fail { get; set; }
        public int Total { get; set; }
        public int LiftQ { get; set; }
        public int PassQ { get; set; }
        public int FailQ { get; set; }
        public int TotalQ { get; set; }
        public bool Complete { get; set; }
        public bool CompleteQ { get; set; }
        public System.DateTime Date { get; set; }
        public System.DateTime DateQC { get; set; }
        public string Created { get; set; }
        public string CreatedQC { get; set; }
    }
}
