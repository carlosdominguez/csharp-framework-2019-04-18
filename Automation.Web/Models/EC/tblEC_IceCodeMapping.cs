//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Automation.Web.Models.EC
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblEC_IceCodeMapping
    {
        public tblEC_IceCodeMapping()
        {
            this.tblEC_EquationRule = new HashSet<tblEC_EquationRule>();
            this.tblEC_FileData = new HashSet<tblEC_FileData>();
            this.tblEC_AffiliateIceCode = new HashSet<tblEC_AffiliateIceCode>();
        }
    
        public int ID { get; set; }
        public string Short { get; set; }
        public string IceCode { get; set; }
    
        public virtual ICollection<tblEC_EquationRule> tblEC_EquationRule { get; set; }
        public virtual ICollection<tblEC_FileData> tblEC_FileData { get; set; }
        public virtual ICollection<tblEC_AffiliateIceCode> tblEC_AffiliateIceCode { get; set; }
    }
}
