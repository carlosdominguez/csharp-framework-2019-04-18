//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Automation.Web.Models.EC
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblE_EditProduct
    {
        public tblE_EditProduct()
        {
            this.tblE_EditCheckRule = new HashSet<tblE_EditCheckRule>();
        }
    
        public int ID { get; set; }
        public string Product { get; set; }
    
        public virtual ICollection<tblE_EditCheckRule> tblE_EditCheckRule { get; set; }
    }
}
