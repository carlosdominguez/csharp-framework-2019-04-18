//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Automation.Web.Models.EC
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblEC_Validation
    {
        public tblEC_Validation()
        {
            this.tblEC_RunRules = new HashSet<tblEC_RunRules>();
        }
    
        public int ID { get; set; }
        public string Validation { get; set; }
    
        public virtual ICollection<tblEC_RunRules> tblEC_RunRules { get; set; }
    }
}
