//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Automation.Web.Models.EC
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblEC_FileColumns
    {
        public int ID { get; set; }
        public int FileID { get; set; }
        public string ColumnName { get; set; }
        public string Type { get; set; }
    
        public virtual tblEC_File tblEC_File { get; set; }
    }
}
