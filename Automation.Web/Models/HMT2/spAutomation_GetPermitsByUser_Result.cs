//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Automation.Web.Models.HMT2
{
    using System;
    
    public partial class spAutomation_GetPermitsByUser_Result
    {
        public int ID { get; set; }
        public Nullable<int> ParentID { get; set; }
        public Nullable<decimal> NumOrder { get; set; }
        public string ClassIcon { get; set; }
        public string Category { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
    }
}
