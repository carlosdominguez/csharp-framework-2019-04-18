//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Automation.Web.Models.HMT2
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblR_Status
    {
        public tblR_Status()
        {
            this.tblR_Role = new HashSet<tblR_Role>();
            this.tblR_RoleIDRoleType = new HashSet<tblR_RoleIDRoleType>();
        }
    
        public int ID { get; set; }
        public string Status { get; set; }
    
        public virtual ICollection<tblR_Role> tblR_Role { get; set; }
        public virtual ICollection<tblR_RoleIDRoleType> tblR_RoleIDRoleType { get; set; }
    }
}
