//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Automation.Web.Models.HMT2
{
    using System;
    using System.Collections.Generic;
    
    public partial class VWGhost_tblCenter
    {
        public string ManagedGeographyID { get; set; }
        public byte ID { get; set; }
        public string Name { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public decimal StandardCost { get; set; }
        public bool IsActive { get; set; }
        public string Code { get; set; }
    }
}
