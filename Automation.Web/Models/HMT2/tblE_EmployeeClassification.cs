//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Automation.Web.Models.HMT2
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblE_EmployeeClassification
    {
        public tblE_EmployeeClassification()
        {
            this.tblE_EmployeeData = new HashSet<tblE_EmployeeData>();
        }
    
        public int ID { get; set; }
        public string ClassificationLetter { get; set; }
        public string Description { get; set; }
        public bool FTEEligible { get; set; }
    
        public virtual ICollection<tblE_EmployeeData> tblE_EmployeeData { get; set; }
    }
}
