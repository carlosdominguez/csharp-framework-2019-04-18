//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Automation.Web.Models.HMT2
{
    using System;
    using System.Collections.Generic;
    
    public partial class VWR_LVID
    {
        public string ID { get; set; }
        public string Type { get; set; }
        public string OrgGroup { get; set; }
        public string LegalForm { get; set; }
        public string EffectiveDate { get; set; }
        public string RSSD { get; set; }
        public string SSB { get; set; }
        public string DBA { get; set; }
        public string DateInvestment { get; set; }
        public string DateIncorporation { get; set; }
        public string DispositionDate { get; set; }
        public string IncorporationCountry { get; set; }
        public string EIN { get; set; }
        public string RegO { get; set; }
        public string Tax { get; set; }
        public string Name { get; set; }
        public string Domicile { get; set; }
        public string Voting { get; set; }
        public string Equity { get; set; }
        public string ActivityCode { get; set; }
        public string EffectiveStatus { get; set; }
        public string CountryDomicile { get; set; }
        public string Country { get; set; }
    }
}
