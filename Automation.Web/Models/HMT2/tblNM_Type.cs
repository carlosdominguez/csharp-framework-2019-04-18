//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Automation.Web.Models.HMT2
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblNM_Type
    {
        public tblNM_Type()
        {
            this.tblNM_Charter = new HashSet<tblNM_Charter>();
        }
    
        public int ID { get; set; }
        public string Type { get; set; }
    
        public virtual ICollection<tblNM_Charter> tblNM_Charter { get; set; }
    }
}
