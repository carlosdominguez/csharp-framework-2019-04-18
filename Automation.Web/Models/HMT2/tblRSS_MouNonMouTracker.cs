//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Automation.Web.Models.HMT2
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblRSS_MouNonMouTracker
    {
        public int ID { get; set; }
        public Nullable<int> MONTH { get; set; }
        public Nullable<int> YEAR { get; set; }
        public string MOUCode { get; set; }
        public string CENTER { get; set; }
        public Nullable<bool> ISDS { get; set; }
        public Nullable<int> JAN { get; set; }
        public Nullable<int> FEB { get; set; }
        public Nullable<int> MAR { get; set; }
        public Nullable<int> APR { get; set; }
        public Nullable<int> MAY { get; set; }
        public Nullable<int> JUN { get; set; }
        public Nullable<int> JUL { get; set; }
        public Nullable<int> AUG { get; set; }
        public Nullable<int> SEP { get; set; }
        public Nullable<int> OCT { get; set; }
        public Nullable<int> NOV { get; set; }
        public Nullable<int> DEC { get; set; }
    }
}
