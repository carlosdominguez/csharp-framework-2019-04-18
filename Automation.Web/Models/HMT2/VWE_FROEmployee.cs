//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Automation.Web.Models.HMT2
{
    using System;
    using System.Collections.Generic;
    
    public partial class VWE_FROEmployee
    {
        public long ID { get; set; }
        public string GEID { get; set; }
        public string SOEID { get; set; }
        public Nullable<long> RITSID { get; set; }
        public string FCI_ID { get; set; }
        public string PRIMARY_ID { get; set; }
        public string LAST_NAME { get; set; }
        public string FIRST_NAME { get; set; }
        public string MIDDLE_NAME { get; set; }
        public string PREF_FIRST_NAME { get; set; }
        public string NAME_PREFIX { get; set; }
        public string NAME { get; set; }
        public string WORK_PHONE { get; set; }
        public string ALTN_PHONE { get; set; }
        public string PAGER_PHONE { get; set; }
        public string FAX { get; set; }
        public string EMAIL_ADDR { get; set; }
        public string LOCATION { get; set; }
        public string TAX_LOCATION_CD { get; set; }
        public string WORK_ADDRESS1 { get; set; }
        public string WORK_ADDRESS2 { get; set; }
        public string WORK_ADDRESS3 { get; set; }
        public string WORK_ADDRESS4 { get; set; }
        public string WORK_CITY { get; set; }
        public string WORK_STATE { get; set; }
        public string WORK_POSTAL { get; set; }
        public string WORK_COUNTRY { get; set; }
        public string CNTRY_DESCR { get; set; }
        public string WORK_BUILDING { get; set; }
        public string WORK_FLOOR { get; set; }
        public string GH_BLDG { get; set; }
        public string BLDG_ZONE { get; set; }
        public string REGION { get; set; }
        public string RPT_REGION { get; set; }
        public string ACCT_CD { get; set; }
        public string GOC { get; set; }
        public string CBOH_CODE { get; set; }
        public string DEPTID { get; set; }
        public string DEPTNAME { get; set; }
        public string L02_DESCR { get; set; }
        public string L03_DESCR { get; set; }
        public string SOURCE1 { get; set; }
        public string EMPL_STATUS { get; set; }
        public Nullable<System.DateTime> TERMINATION_DT { get; set; }
        public string EMPL_CLASS { get; set; }
        public string JOB_FAMILY { get; set; }
        public string JOBFAM_NAME { get; set; }
        public string JOB_FUNCTION { get; set; }
        public string JOBFUNC_NAME { get; set; }
        public string JOBCODE { get; set; }
        public string JOBTITLE { get; set; }
        public string OFFCR_TITLE_CD { get; set; }
        public string OFFCR_TTL_DESC { get; set; }
        public string COMPANY { get; set; }
        public string COMPANY_NAME { get; set; }
        public string VENDOR_ID { get; set; }
        public string VENDOR { get; set; }
        public Nullable<System.DateTime> APPOINT_END_DT { get; set; }
        public Nullable<System.DateTime> LOAD_DTTM { get; set; }
        public string TSA_IND { get; set; }
        public Nullable<long> DirectManagerID { get; set; }
        public string DirectManager_GEID { get; set; }
        public Nullable<long> SupervisorID { get; set; }
        public string Supervisor_GEID { get; set; }
        public Nullable<int> ManagedSegmentID { get; set; }
        public string Managed_Segment { get; set; }
        public string Grade { get; set; }
        public Nullable<System.DateTime> HireDate { get; set; }
        public Nullable<System.DateTime> TransferDate { get; set; }
        public Nullable<byte> HierarchyLevel { get; set; }
        public System.DateTime CreatedDate { get; set; }
    }
}
