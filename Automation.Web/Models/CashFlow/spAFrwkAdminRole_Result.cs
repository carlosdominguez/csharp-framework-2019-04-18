//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Automation.Web.Models.CashFlow
{
    using System;
    
    public partial class spAFrwkAdminRole_Result
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string EERSMarketplaceRoleID { get; set; }
        public string EERSFunctionCode { get; set; }
        public string EERSFunctionDescription { get; set; }
        public Nullable<bool> EERSIgnore { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
