//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Automation.Web.Models.CashFlow
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblCF_M_MonthYearQuater
    {
        public int ID { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public int Quater { get; set; }
        public string MonthYear { get; set; }
    }
}
