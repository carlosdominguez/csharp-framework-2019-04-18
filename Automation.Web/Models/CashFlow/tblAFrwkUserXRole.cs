//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Automation.Web.Models.CashFlow
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblAFrwkUserXRole
    {
        public int ID { get; set; }
        public string SOEID { get; set; }
        public Nullable<int> IDRole { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
    
        public virtual tblAFrwkRole tblAFrwkRole { get; set; }
    }
}
