//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Automation.Web.Models.BCL
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblProjectMetadata
    {
        public tblProjectMetadata()
        {
            this.tblRequestMetadatas = new HashSet<tblRequestMetadata>();
        }
    
        public short ID { get; set; }
        public short ProjectID { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public byte Order { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public bool IsRequired { get; set; }
        public short DataTypeID { get; set; }
    
        public virtual tblDataType tblDataType { get; set; }
        public virtual tblProject tblProject { get; set; }
        public virtual ICollection<tblRequestMetadata> tblRequestMetadatas { get; set; }
    }
}
