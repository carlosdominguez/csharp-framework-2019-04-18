//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Automation.Web.Models.BCL
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblServer
    {
        public tblServer()
        {
            this.tblRequestReferences = new HashSet<tblRequestReference>();
        }
    
        public short ID { get; set; }
        public string Name { get; set; }
        public short ProjectID { get; set; }
        public string Path { get; set; }
    
        public virtual tblProject tblProject { get; set; }
        public virtual ICollection<tblRequestReference> tblRequestReferences { get; set; }
    }
}
