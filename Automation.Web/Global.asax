﻿<%@ Application Language="C#" %>

<script runat="server">

    protected void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup
        Automation.Web.RouteConfig.RegisterRoutes(System.Web.Routing.RouteTable.Routes);
    }

    protected void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }

    protected void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

    }

    protected void Application_EndRequest(object sender, EventArgs e)
    {
        //Save exception if the server has a error
        if (Server.GetLastError() != null)
        {
            Automation.Core.Class.GlobalUtil.SaveExceptionInDB(HttpContext.Current.Server.GetLastError());
        }
    }

    protected void Session_Start(object sender, EventArgs e)
    {
        Automation.Core.Class.GlobalUtil.SessionStart();
    }

    protected void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.
    }

    protected void Application_BeginRequest(object sender, EventArgs e)
    {
        // Bug fix for MS SSRS Blank.gif 500 server error missing parameter IterationId
        // https://connect.microsoft.com/VisualStudio/feedback/details/556989/
        if (HttpContext.Current.Request.Url.PathAndQuery.StartsWith("/Reserved.ReportViewerWebControl.axd") &&
         !String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["ResourceStreamID"]) &&
            HttpContext.Current.Request.QueryString["ResourceStreamID"].ToLower().Equals("blank.gif"))
        {
            Context.RewritePath(String.Concat(HttpContext.Current.Request.Url.PathAndQuery, "&IterationId=0"));
        }
    }

    // This method is called when the page is rendered 3 times (_LayoutMaster.cshtml, _PartialTopBar.cshtml, PageNotFound.cshtml)
    protected void Application_AcquireRequestState(object sender, EventArgs e)
    {
        //We use this event because "Application_BeginRequest" dont have Session Data
        Automation.Core.Class.GlobalUtil.CheckIfSOEIDOrAppKeyWasChaged();
    }
    
    protected void Application_PreSendRequestHeaders(object sender, EventArgs e)
    {
    }
       
</script>
            