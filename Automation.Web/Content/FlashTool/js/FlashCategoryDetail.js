/// <reference path="../../Shared/plugins/util/global.js" />
_showLoadingFullPage({
    msg: "Loading Segments",
    idLoading: "FlashDetail"
});
//Global variables to set: priorActual and flashTotal
var priorActual;
var flashTotal;
var bussinessMonth;
var bussinessYear;

//FN: to load all segments
function fnLoadFlashCustomSegments() {
    $.jqxGridApi.create({
        showTo: "#flashSegmentsGrid",
        options: {
            width: "100%",
            height: "500px",
            autoheight: false,
            autorowheight: false,
            //selectionmode: "multiplerows",
            showfilterrow: true,
            sortable: true,
            editable: true
            //'singlerow', 'multiplerows', 'checkbox', 'multiplerowsadvanced' or 'multiplerowsextended'.
        },
        sp: {
            Name: "[dbo].[spFlashGetAllFlashSegments]",
            Params: []
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set"
        },
        groups: [],
        columns: [
            { name: 'ID', type: 'int', hidden: true },
            { name: 'FlashSegmentName', text: 'Segment Name', width: '55%', type: 'string', editable: false},
            { name: 'CreatedBy', text: 'Created By', width: '15%', type: 'string', editable: false, cellsalign: 'center', align: 'center' },
            { name: 'CreatedDate', text: 'Creation Date', width: '30%', type: 'string', cellsformat: 'D', cellsalign: 'center', align: 'center' },
        ],
        ready: function () {
            //Select row and add all the data in the queryString
            $("#flashSegmentsGrid").on('rowselect', function (event) {
                $("#flashbtn").prop("disabled", false);
                //$("#bridgeBtn").prop("disabled", false);
                $("#sendParameter").attr("href", _getViewVar("SubAppPath") + "/FlashTool/FlashCategoryDetail?ID=" + event.args.row.ID + "&name=" + event.args.row.FlashSegmentName);
                $("#sendBridgeParameter").attr("href", _getViewVar("SubAppPath") + "/FlashTool/FlashBridge?ID=" + event.args.row.ID + "&name=" + event.args.row.FlashSegmentName);
            });

        }
    });
}
//FN: list all segments
function fnGoToSegments() {
   
    var htmlContentModal = 'Choose at least one segment';
    var formHtml =
                '<div class="content-body">' +
                        '<div id="sidebar">' +
                            '<a id="sendParameter">' +
                                '<button type="button" id="flashbtn" class="btnDelete btn btn-info top15 left15 bottom15 pull-right">' +
                                    '<i class="fa fa-external-link"></i> Flash' +
                                '</button>' +
                            '</a>' +
                        '</div>' +
                    '<div id="main">' +
                        '<div class="center">' +
                            '<div id="flashSegmentsGrid"></div>' +
                        '</div>' +
                    '</div>'+
                '</div>';
    fnLoadFlashCustomSegments();
    _showModal({
        modalId: "modalUpload",
        width: '50%',
        //buttons: [{
        //    name: "<i class='fa fa-save'></i> OK",
        //    class: "btn-success",
        //    closeModalOnClick: true,
        //    onClick: function ($modal) {
        //        //Validate required Comment
        //        window.location.href = _getViewVar("SubAppPath") + '/FlashTool/FlashCategory';
        //    }
        //}],
        addCloseButton: false,
        title: "Choose at least one segment",
        contentHtml: formHtml,
        onReady: function ($modal) { }
    });

}

$(document).ready(function () {
    getDate("m", function (pmonth) {
        getDate("y", function (pyear) {
            bussinessMonth = pmonth;
            bussinessYear = pyear;

            runPageScript();
        });
    });
    
    function runPageScript() {
        //Load Flash with parameters month and id
        fnLoadFlashFile();
        // initialize the input fields.
        $("#editComment").click(function () {
            showModalAddCommentEdit();
        });        //Save flash numbers        $("#SaveFlash").click(function () {
            $("#Grid").jqxGrid('clearfilters');
            fnSaveFlash();
        });        //Get diff numbers        $("#diff").click(function () {
            fnShowDiffMoM();
        });        //Get current view GOCS        $("#gocList").click(function () {
            fnGetGocs();
        });        //Get ELR to excel        $("#ELR").click(function () {
            fnFullElr();
        });
        //GET GDW to excel
        $("#currentGDW").click(function () {
            fnGetCurrentGDW();
        });
        //Get process FTE to excel
        $("#processFTE").click(function () {
            fnPrevFTE();
        });

        //Get current view to excel
        $("#getExcelViewSegment").click(function () {
            fnGetSegmentToExcel();
        });

        //Get prior actual to escel
        $("#getPriorActual").click(function () {
            fnGetPriorActual();
        });

        //Check if parameter ID is empty
        console.log(getParameterByName('ID'));
        if (typeof getParameterByName('ID') === 'undefined' || !getParameterByName('ID')) {
            console.log('Entre');
            fnGoToSegments();
        }
    }

});

//check only edited rows to save
var editedRows = new Array();
var editedIndex = {};

//Fn to get value of QS
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

//Load flashview
function fnLoadFlashFile() {
    $("#nameOfSegment").html("Flash Segment: <br />" + getParameterByName('name'));
    $("#goToBridge").attr("href", _getViewVar("SubAppPath") + "/FlashTool/FlashBridge?ID=" + getParameterByName('ID') + "&name=" + getParameterByName('name'));
    var lEditableCells = function (index, value, defaultvalue, column, rowdata) {
        var value = defaultvalue ? '' : 0;
        //total = total ? 'Nan' : 0;

        if (value) {
            return '<center><div style="background-color:lightBlue; line-height: 25px;;"><span><b>' + value + '</b></span></div></center>';
        }
        //} else {
        //    return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #008000;">' + value + '</span>';
        //}
        return "<center><div style='background-color:lightBlue; line-height: 25px;'><span><b>" + value + "</b></span></div></center>";
    }
    //add color to cells
    var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties, data) {

        if (columnfield == 'Accrual') {
            if (value == "") {
                value = 0;
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(value, "d2"); + '</b></span></div></center>';
            } else {
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(value, "d2"); + '</b></span></div></center>';
            }
        }
        if (columnfield == 'UnpostedCanada') {
            if (value == "") {
                value = 0;
                return '<center><div style="background-color:lightBlue; line-height: 25px;;"><span><b>' + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(value, "d2"); + '</b></span></div></center>';
            } else {
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(value, "d2"); + '</b></span></div></center>';
            }
        }
        if (columnfield == 'APS') {
            if (value == "") {
                value = 0;
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(value, "d2"); + '</b></span></div></center>';
            } else {
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(value, "d2"); + '</b</span></div></center>';
            }
        }
        if (columnfield == 'PriorActual') {
            //console.log(data);
            if (value == "") {
                value = 0;
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(value, "d2") + '</b></span></div>';
            } else {
                //if (value < 0) {
                //    return '<div style="line-height: 25px; text-align: right;"><span>(' + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(value , "d") + '</b>)</span></div>';
                //} else {
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(value, "d2") + '</b></span></div>';
                //}
            }
           
        }

    }
    var groupsrenderer = function (text, group, expanded) {
        return "" + group + "";
    }
    $("#Grid").jqxTooltip();
    if (typeof getParameterByName('ID') === 'undefined' || !getParameterByName('ID')) {
        console.log('vacio');
    } else {
        _hideMenu();

        _callProcedure({
            loadingMsgType: "fullLoading",
            loadingMsg: "Getting data...",
            name: "[dbo].[spFlashGetSegmentsByFlashLevel]",
            params: [
                    { Name: "@pIdSegment", Value: getParameterByName('ID') },
                    { Name: "@vCurrentMonth", Value: bussinessMonth }
            ],
            success: {
                fn: function (responseList) {

                    $.jqxGridApi.create({
                        showTo: "#Grid",
                        options: {
                            //for comments or descriptions
                            height: "500",
                            editable: true,
                            filterable: true,
                            sortable: true,
                            autorowheight: false,
                            autowidth: true,
                            showfilterrow: true,
                            showstatusbar: true,
                            statusbarheight: 25,
                            //selectionmode: 'singlecell',
                            showaggregates: true,
                            groupable: true,
                            sortable: true,
                            sortcolumn: 'Order',
                            sortdirection: 'asc',
                            altrows: true,                            resizable: true,


                        },

                        source: {
                            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                            dataBinding: "Large Data Set Local",
                            rows: responseList
                        },

                        //groups: ['FlashAccount'],
                        columns: [

                            {
                                name: 'ExpenseCategory', datafield: 'SubCategory', text: 'Expense Category', width: '200px', type: 'string', pinned: true,
                                filtertype: 'checkedlist', cellsalign: 'Left', align: 'center', editable: false, cellsalign: 'left', align: 'center',
                                groupsrenderer: groupsrenderer,

                            },
                            {
                                name: 'FlashAccount', groupable: false, text: 'Expense Account', width: '200px', type: 'string', filtertype: 'checkedlist', pinned: true,
                                editable: false, cellsalign: 'Left', align: 'center',

                            },
                            {
                                name: 'PriorActual', datafield: 'PriorActual', text: 'Prior Actual', width: '190px', type: 'number', filtertype: 'input', cellsformat: 'd2',
                                editable: false, cellsalign: 'right', align: 'Center', cellsrenderer: cellsrenderer,

                                aggregates: [{
                                    'Total':
                                    function (aggregatedValue, currentValue, column, record) {
                                        var total = $("#Grid").jqxGrid('getcolumnaggregateddata', 'PriorActual', ['sum']);
                                        sum = total.sum
                                        priorActual = sum;
                                        return sum

                                    }
                                }],


                            },
                            ////{ name: 'Seg', width: '80px', type: 'string', filtertype: 'input', editable: false, aggregates: ['sum'] },
                            {
                                name: 'today', datafield: 'today', text: 'ELR (P2P)', width: '150px', type: 'number', filtertype: 'input', cellsalign: 'right', align: 'center',
                                cellsformat: 'd2', editable: false,
                                aggregates: [{
                                    'Total':
                                    function (aggregatedValue, currentValue, column, record) {
                                        var total = $("#Grid").jqxGrid('getcolumnaggregateddata', 'today', ['sum']);
                                        sum = total.sum
                                        return sum
                                    }
                                }],
                                cellsrenderer:
                                function (index, value, defaultvalue, column, rowdata) {
                                    //var total = parseFloat(rowdata.PriorActual) - parseFloat(rowdata.yesterday);
                                    if (defaultvalue === "") {
                                        defaultvalue = 0;
                                        var total = defaultvalue;
                                        return "<div style='margin: 4px;' class='jqx-right-align'>" + total + "</div>";
                                    }

                                }
                            },
                            {
                                name: 'yesterday', datafield: 'yesterday', text: 'Yesterday ELR', width: '180px', groupable: true, type: 'number', filtertype: 'input',
                                cellsalign: 'center', align: 'center', cellsformat: 'd2', editable: false,
                                aggregates: [{
                                    'Total':
                                    function (aggregatedValue, currentValue, column, record) {
                                        var total = $("#Grid").jqxGrid('getcolumnaggregateddata', 'yesterday', ['sum']);
                                        sum = total.sum
                                        return sum
                                    }
                                }],
                            },
                            {
                                name: 'NewActivity', datafield: 'NewActivity', width: '190px', text: 'New Activity', groupable: true, type: 'number', filtertype: 'input',
                                cellsalign: 'center', align: 'center', editable: false, cellsformat: 'd2',
                                cellsrenderer:
                                //Get new activity
                                function (index, datafield, value, defaultvalue, column, rowdata) {
                                    if (rowdata.today == null || rowdata.today === "") {
                                        rowdata.today = 0;
                                    }
                                    if (rowdata.yesterday == null || rowdata.yesterday === "") {
                                        rowdata.yesterday = 0;
                                    }

                                    var total = parseFloat(rowdata.today) - parseFloat(rowdata.yesterday);
                                    if (rowdata.NewActivity != total) {
                                        $("#Grid").jqxGrid('setcellvalue', rowdata.uid, "NewActivity", total);
                                    }
                                    //Base de datos no se valido isNull
                                    return "<div style='margin: 4px;' class='jqx-right-align'>" + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(total, "d2"); + "</div>";
                                },

                                aggregates: [{

                                    'Total':
                                    function (aggregatedValue, currentValue, column, record) {
                                        var total = $("#Grid").jqxGrid('getcolumnaggregateddata', 'NewActivity', ['sum']);
                                        sum = total.sum
                                        return sum
                                    }
                                }],
                            },
                            {
                                name: 'Accrual', width: '120px', groupable: true, type: 'number', filtertype: 'input', cellsalign: 'center', align: 'center',
                                cellsrenderer: cellsrenderer, cellsformat: 'd2',
                                aggregates: [{

                                    'Total':
                                    function (aggregatedValue, currentValue, column, record) {
                                        var total = $("#Grid").jqxGrid('getcolumnaggregateddata', 'Accrual', ['sum']);
                                        sum = total.sum
                                        return sum
                                    }
                                }],
                            },
                            { name: 'check', text: 'Flat to Prior', width: '90px', type: 'string', columntype: 'checkbox', cellsalign: 'center', align: 'center' },
                            {
                                name: 'UnpostedCanada', text: 'Unposted ', width: '120px', groupable: true, type: 'number', filtertype: 'input', cellsalign: 'center', align: 'center',
                                cellsrenderer: cellsrenderer, cellsformat: 'd2',
                                aggregates: [{

                                    'Total':
                                    function (aggregatedValue, currentValue, column, record) {
                                        var total = $("#Grid").jqxGrid('getcolumnaggregateddata', 'UnpostedCanada', ['sum']);
                                        sum = total.sum
                                        return sum
                                    }
                                }],
                            },
                            {
                                name: 'APS', width: '120px', groupable: true, type: 'number', filtertype: 'input', cellsalign: 'center', align: 'center', cellsformat: 'd2', editable: false,
                                aggregates: [{
                                    'Total':
                                    function (aggregatedValue, currentValue, column, record) {
                                        var total = $("#Grid").jqxGrid('getcolumnaggregateddata', 'APS', ['sum']);
                                        sum = total.sum
                                        return sum
                                    }
                                }],
                            },
                            {
                                name: 'BAW', width: '120px', groupable: true, type: 'number', filtertype: 'input', cellsalign: 'center', align: 'center', cellsformat: 'd2', editable: false,
                                aggregates: [{
                                    'Total':
                                    function (aggregatedValue, currentValue, column, record) {
                                        var total = $("#Grid").jqxGrid('getcolumnaggregateddata', 'BAW', ['sum']);
                                        sum = total.sum
                                        return sum
                                    }
                                }],
                            },

                            ////listo elr+ var sum=(accrual + unposted + aps)
                            {
                                name: 'Month', text: 'Flash', width: '120px', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center', editable: false, cellsformat: 'd2',
                                cellsrenderer:
                                function (index, datafield, value, defaultvalue, column, rowdata) {
                                    if (rowdata.today == null || rowdata.today === "") {
                                        rowdata.today = 0;
                                    }
                                    if (rowdata.Accrual == null || rowdata.Accrual === "") {
                                        rowdata.Accrual = 0;
                                    }
                                    if (rowdata.UnpostedCanada == null || rowdata.UnpostedCanada === "") {
                                        rowdata.UnpostedCanada = 0;
                                    }
                                    if (rowdata.APS == null || rowdata.APS === "") {
                                        rowdata.APS = 0;
                                    }
                                    if (rowdata.BAW == null || rowdata.BAW === "") {
                                        rowdata.BAW = 0;
                                    }
                                    var total = parseFloat(rowdata.today) + parseFloat(rowdata.Accrual) + parseFloat(rowdata.UnpostedCanada) + parseFloat(rowdata.APS) + parseFloat(rowdata.BAW);
                                    //totalFlash = total;
                                    // rowdata.Month = total;
                                    // $("#Grid").jqxGrid('updaterow', rowdata.uid, rowdata);

                                    if (rowdata.Month != total) {
                                        $("#Grid").jqxGrid('setcellvalue', rowdata.uid, "Month", total);
                                        flashTotal = total;

                                    }


                                    return "<div style='margin: 4px;' class='jqx-right-align'>" + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(total, "d2"); + "</div>";
                                },
                                aggregates: [{
                                    'Total':
                                    function (aggregatedValue, currentValue, column, record) {
                                        var total = $("#Grid").jqxGrid('getcolumnaggregateddata', 'Month', ['sum']);
                                        sum = total.sum;
                                        flashTotal = sum;
                                        return sum
                                    }

                                }],

                            },
                            {
                                name: 'momVariance', datafield: 'momVariance', text: 'MoM Variance', width: '110px', type: 'string', filtertype: 'input', cellsalign: 'center',
                                align: 'center', editable: false,
                                aggregates: ['sum'], cellsformat: 'd2',                                //Get MoM // we have to recalculate all the numbers
                                cellsrenderer:
                                function (index, datafield, value, defaultvalue, column, rowdata) {
                                    if (rowdata.today == null || rowdata.today === "") {
                                        rowdata.today = 0;
                                    }
                                    if (rowdata.Accrual == null || rowdata.Accrual === "") {
                                        rowdata.Accrual = 0;
                                    }
                                    if (rowdata.UnpostedCanada == null || rowdata.UnpostedCanada === "") {
                                        rowdata.UnpostedCanada = 0;
                                    }
                                    if (rowdata.APS == null || rowdata.APS === "") {
                                        rowdata.APS = 0;
                                    }
                                    if (rowdata.BAW == null || rowdata.BAW === "") {
                                        rowdata.BAW = 0;
                                    }
                                    var lMonth = parseFloat(rowdata.today) + parseFloat(rowdata.Accrual) + parseFloat(rowdata.UnpostedCanada) + parseFloat(rowdata.APS) + parseFloat(rowdata.BAW);
                                    var total = parseFloat(lMonth) - parseFloat(rowdata.PriorActual);
                                    if (rowdata.momVariance != total) {
                                        $("#Grid").jqxGrid('setcellvalue', rowdata.uid, "momVariance", total);
                                        total = total;

                                    }
                                    //total = total || 0;
                                    return "<div style='margin: 4px;' class='jqx-right-align'>" + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(total, "d2"); + "</div>";
                                },
                                aggregates: [{
                                    'Total':
                                            // we have to recalculate all the numbers
                                            function (aggregatedValue, currentValue, column, rowdata) {
                                                if (rowdata.today == null || rowdata.today === "") {
                                                    rowdata.today = 0;
                                                }
                                                if (rowdata.Accrual == null || rowdata.Accrual === "") {
                                                    rowdata.Accrual = 0;
                                                }
                                                if (rowdata.UnpostedCanada == null || rowdata.UnpostedCanada === "") {
                                                    rowdata.UnpostedCanada = 0;
                                                }
                                                if (rowdata.APS == null || rowdata.APS === "") {
                                                    rowdata.APS = 0;
                                                }
                                                if (rowdata.BAW == null || rowdata.BAW === "") {
                                                    rowdata.BAW = 0;
                                                }
                                                var lMonth = parseFloat(rowdata.today) + parseFloat(rowdata.Accrual) + parseFloat(rowdata.UnpostedCanada) + parseFloat(rowdata.APS) + parseFloat(rowdata.BAW);
                                                var total = parseFloat(lMonth) - parseFloat(rowdata.PriorActual);
                                                total = total || 0;
                                                //var aggregatedValue = isNaN(parseInt(aggregatedValue)) ? 0 : parseInt(aggregatedValue)
                                                //console.log(aggregatedValue);
                                                if (isNaN(aggregatedValue)) {
                                                    aggregatedValue = 0;
                                                }
                                                var lShowTotal = aggregatedValue + total;
                                                return lShowTotal;//$.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(lShowTotal, "c2");
                                            }
                                }]

                            },

                            ////listo flash menos previous month tiene que ser primero flash --ToDo
                            {
                                name: 'Comment', width: '80px', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center', editable: false, cellsrenderer: function (rowIndex, datafield, value) {
                                    var dataRecord = $("#Grid").jqxGrid('getrowdata', rowIndex);
                                    //console.log(dataRecord.Comment);
                                    var htmlComment = "<div style='padding: 10px; font-size: 15px; max-height: 500px; overflow-y:scroll; width: 520px;'>";
                                    htmlComment += "<b>Comment:</b> " + dataRecord.Comment + " <br>";
                                    htmlComment += "</div>";
                                    var htmlResult = "";

                                    if (dataRecord.Comment) {
                                        htmlResult +=
                                            '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + htmlComment + '" data-title="Comments History" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                            '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png"/> ' +
                                            '</div>';
                                    }
                                    return htmlResult;
                                }
                            },
                            {
                                name: 'IDComment', width: '190px', type: 'number', filtertype: 'input', align: 'center', editable: false, cellsalign: 'center'
                                , hidden: 'true'
                            },
                            ////{ name: 'One Timer', width: '110px', type: 'string', filtertype: 'input',columntype: 'checkbox' }
                        ],
                        ready: function () {
                            $("#Grid").bind('cellvaluechanged', function (event) {
                                // event arguments.
                                var args = event.args;
                                // column data field.
                                var datafield = event.args.datafield;
                                // row's bound index.
                                var rowBoundIndex = args.rowindex;
                                // new cell value.
                                var value = args.newvalue;
                                // old cell value.
                                var oldvalue = args.oldvalue;
                                //var allData = $("#Grid").jqxGrid('getrowdata', rowBoundIndex);
                                //editedRows.push({ ExpenseCategory: allData.ExpenseCategory, FlashAccount: allData.FlashAccount, PriorActual: allData.PriorActual, Accrual: allData.Accrual, Unposted: allData.UnpostedCanada });

                                if (!editedIndex[rowBoundIndex]) {
                                    editedIndex[rowBoundIndex] = true;
                                }

                            });

                            $('#Grid').jqxGrid('addgroup', 'ExpenseCategory');
                            //Order by Column Order
                            $("#Grid").jqxGrid('sortby', 'Order', 'asc');
                            //On cell check. //FLAT TO PRIOR
                            $("#Grid").on('cellvaluechanged', function (event) {
                                if (event.args.datafield === "check") {
                                    console.log('check ' + event.args);
                                    var dataRecord = $("#Grid").jqxGrid('getrowdata', event.args.rowindex);
                                    if (event.args.newvalue) {
                                        console.log('check');
                                        if (!dataRecord.PriorActual) {
                                            dataRecord.PriorActual = 0;
                                        }
                                        if (!dataRecord.today) {
                                            dataRecord.today = 0;
                                        }
                                        if (!dataRecord.APS) {
                                            dataRecord.APS = 0;
                                        }
                                        if (!dataRecord.BAW) {
                                            dataRecord.BAW = 0;
                                        }

                                        //dataRecord.PriorActual = dataRecord.PriorActual ? 'undefined' : 0;
                                        dataRecord.UnpostedCanada = ((dataRecord.PriorActual) - (dataRecord.today) - (dataRecord.APS) - (dataRecord.BAW));
                                        event.preventDefault();
                                        event.stopImmediatePropagation();
                                        event.stopPropagation();
                                    } else {
                                        dataRecord.UnpostedCanada = 0;
                                        event.preventDefault();
                                        event.stopImmediatePropagation();
                                        event.stopPropagation();
                                    }
                                    $("#Grid").jqxGrid('updaterow', event.args.rowindex, dataRecord);
                                }
                                event.preventDefault();
                                event.stopImmediatePropagation();
                                event.stopPropagation();
                                return false;
                            });

                            //Add popup of information
                            $("#Grid").on("rowclick", function (event) {
                                _GLOBAL_SETTINGS.tooltipsPopovers();
                            });
                            console.log('Complete Main Flash VIEW');
                            setTimeout(function () { fnDisplayPrevFTE() }, 750);

                        }
                    });
                }
            },
            ignoreErrorMsg: true,
            error: function (xhr, textStatus, thrownError) {
                console.log(xhr, textStatus, thrownError);
            }
        });

        
        //Get prev FTEPREVGRID
        function fnDisplayPrevFTE() {
            $.jqxGridApi.create({
                showTo: "#FtePrevGrid",
                options: {
                    //for comments or descriptions
                    height: "300",
                    editable: true,
                    filterable: true,
                    sortable: true,
                    autorowheight: false,
                    showfilterrow: true,
                    showstatusbar: true,
                    statusbarheight: 25,
                    //selectionmode: 'singlecell',
                    showaggregates: true,
                    groupable: true,
                    sortable: true,
                    sortcolumn: 'Order',
                    sortdirection: 'asc',
                    altrows: true,


                },

                sp: {
                    Name: "[dbo].[spFlashGetPrevMonthGDWToFlash]",
                    Params: [
                            { Name: "@vCurrentMonth", Value: bussinessMonth }
                            , { Name: "@pIdSegment", Value: getParameterByName('ID') },
                    ]
                },
                source: {
                    // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                    dataBinding: "Large Data Set",
                    //rows: data
                },
                //groups: ['FlashAccount'],
                columns: [
                    {
                        name: 'SegmentName', text: 'Segment Name', width: '33%', type: 'string', filtertype: 'input', editable: false,
                        cellsalign: 'center', align: 'center'
                    },
                    {
                        name: 'Description', text: 'Description', width: '33%', type: 'string', filtertype: 'input', editable: false,
                        cellsalign: 'center', align: 'center'
                    },
                    //{ name: 'P2PAccount', datafield: 'account', text: 'P2P account', type: 'number', editable: false},
                    {
                        name: 'PrevMonthTotal', text: 'Total', width: '33%', type: 'string',
                        cellsalign: 'Left', align: 'center', editable: false, cellsalign: 'left', align: 'center',
                        aggregates: [{
                            '<b id="PrevMonthTotal">Total</b>':
                            function (aggregatedValue, currentValue, column, record) {
                                var total = $("#FtePrevGrid").jqxGrid('getcolumnaggregateddata', 'PrevMonthTotal', ['sum']);
                                sum = total.sum
                                return sum
                            }
                        }],

                    },
                    {
                        name: 'Date', groupable: false, text: 'Date', width: '200px', type: 'string', filtertype: 'input',
                        editable: false, cellsalign: 'Left', align: 'center', hidden: 'True'

                    },
                ],
                ready: function () {
                    console.log('complete display fnDisplayPrevFTE()');
                    setTimeout(function () { fnDisplayFTE() }, 250);

                }
            })
        }
        //Get current FTE
        function fnDisplayFTE() {
            $.jqxGridApi.create({
                showTo: "#FteGrid",
                options: {
                    //for comments or descriptions
                    height: "300",
                    editable: true,
                    filterable: true,
                    sortable: true,
                    autorowheight: false,
                    showfilterrow: true,
                    showstatusbar: true,
                    statusbarheight: 25,
                    //selectionmode: 'singlecell',
                    showaggregates: true,
                    groupable: true,
                    sortable: true,
                    sortcolumn: 'Order',
                    sortdirection: 'asc',
                    altrows: true,


                },

                sp: {
                    Name: "[dbo].[spFlashGetGDWBySegment]",
                    Params: [
                            { Name: "@vCurrentMonth", Value: bussinessMonth }
                            , { Name: "@pIdSegment", Value: getParameterByName('ID') },
                    ]
                },
                source: {
                    // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                    dataBinding: "Large Data Set",
                    //rows: data
                },
                //groups: ['FlashAccount'],
                columns: [
                    {
                        name: 'SegmentName', text: 'Segment Name', width: '33%', type: 'string', filtertype: 'input', editable: false,
                        cellsalign: 'center', align: 'center'
                    },
                    {
                        name: 'Description', text: 'Description', width: '33%', type: 'string', filtertype: 'input', editable: false,
                        cellsalign: 'center', align: 'center'
                    },
                    //{ name: 'P2PAccount', datafield: 'account', text: 'P2P account', type: 'number', editable: false},
                    {
                        name: 'Total', text: 'Total', width: '33%', type: 'string',
                        cellsalign: 'Left', align: 'center', editable: false, cellsalign: 'left', align: 'center',
                        aggregates: [{
                            '<b id="TotalCurrentMonth">Total</b>':
                            function (aggregatedValue, currentValue, column, record) {
                                var total = $("#FteGrid").jqxGrid('getcolumnaggregateddata', 'Total', ['sum']);
                                sum = total.sum
                                return sum
                            }
                        }],

                    },
                    {
                        name: 'Date', groupable: false, text: 'Date', width: '150px', type: 'string', filtertype: 'input',
                        editable: false, cellsalign: 'Left', align: 'center', hidden: 'True'

                    },
                ],
                ready: function () {
                    console.log('complete display current FTE: fnDisplayFTE()');
                }
            })
        }
        

        
       
    }
    

}
//FN: save to flash numbers to use in bridge view
function fnSaveFlash() {
    //check which row is modified in flash detail
    var indexRows = Object.keys(editedIndex);
    var rowData;
    for (var i = 0; i < indexRows.length; i++) {
        rowData = $("#Grid").jqxGrid('getrowdata', indexRows[i]);
        fnToSave(rowData);
    }

    //save row modified
    function fnToSave(rowData) {
        if (rowData) {
            var lPriorActual = rowData["PriorActual"];
            var lAccrual = typeof rowData["Accrual"] === 'undefined' ? 0 : rowData["Accrual"];
            var lUnposted = typeof rowData["UnpostedCanada"] === 'undefined' ? 0 : rowData["UnpostedCanada"];
            if (rowData["Accrual"] === '') {
                lAccrual = 0;
            }
            if (rowData["UnpostedCanada"] === '') {
                lUnposted = 0;
            }
           
            var ExpenseCategory = rowData["ExpenseCategory"]
            var FlashAccount = rowData["FlashAccount"]
            //Save to consolidated
            fnSaveConsolidated(lPriorActual, getParameterByName('name'), ExpenseCategory, FlashAccount, lAccrual, lUnposted);
        } else {
            fnLoadFlashFile();
        }
    }
  
 
}

// save to consolidated
function fnSaveConsolidated(lPriorActual, MSDESCR, ExpenseCategory, FlashAccount, lAccrual, lUnposted, onSuccess) {
    //get flash total and prior actual
    this.totalFlash = flashTotal;
    this.priorActual = priorActual
    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Saving data...",
        name: "[dbo].[spFlashSaveBridgeConsolidated]",
        params: [
            { "Name": "@pPriorActual", "Value": lPriorActual },

            { "Name": "@pSegmentId", "Value": getParameterByName('ID') },
            { "Name": "@pMSDesccr", "Value": MSDESCR },

            { "Name": "@pPriorActualTotalSegment", "Value": this.priorActual},
            { "Name": "@pFlashExpenseCategory", "Value": ExpenseCategory },
            { "Name": "@pFlashExpenseAccount", "Value": FlashAccount },
    
            { "Name": "@pAccrual", "Value": parseFloat(lAccrual) },
            { "Name": "@pUnposted", "Value": parseFloat(lUnposted) },

            { "Name": "@pFlashTotalSegment", "Value": this.totalFlash },
            { "Name": "@pMonth", "Value": bussinessMonth },
            { "Name": "@pYear", "Value": bussinessYear },
            { "Name": "@pSessionSOEID", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                _showNotification("success", "Data was added successfully.", "AlertAddComment");
                //onSuccess();
            }
        }
    });

}
//FN: save comments and editable cells
function showModalAddCommentEdit() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#Grid', true);
    if (rowData) {
        var lPriorActual = rowData["PriorActual"];

        var lAccrual = typeof rowData["Accrual"] === 'undefined' ? 0 : rowData["Accrual"];
        var lUnposted = typeof rowData["UnpostedCanada"] === 'undefined' ? 0 : rowData["UnpostedCanada"];
        var lComment = rowData["Comment"];
        var htmlContentModal = '';
        htmlContentModal += "<b>Expense Category: </b> " + rowData["ExpenseCategory"] + " <br/>";
        htmlContentModal += "<b>Description: </b>" + rowData["FlashAccount"] + "<br/>";
        htmlContentModal += "<b>Accrual: </b>" + lAccrual + "<br/>";
        htmlContentModal += "<b>Unposted: </b>" + lUnposted + "<br/>";
        htmlContentModal += "<b>Comments: </b><br/>";
        htmlContentModal += "<textarea class='form-control txtDetailComment' placeholder='(optional)' style='height:100px !important;'>" + lComment + "</textarea>";

        _showModal({
            modalId: "modalUpload",
            width: '70%',
            buttons: [{
                name: "<i class='fa fa-save'></i> Save",
                class: "btn-success",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    //Add Comments
                    lComment = $(".txtDetailComment").val();
                    //FN: to save data in db editable cells
                    addDetailComment(lPriorActual, getParameterByName('name'), rowData["PriorActual"], rowData["FlashAccount"], rowData["ExpenseCategory"], lAccrual, lUnposted, lComment, function () {
                        _showNotification("success", "Flash data was save successfully", "AlertReject");
                        rowData["Accrual"] = lAccrual;
                        rowData["UnpostedCanada"] = lUnposted;
                        rowData["Comment"] = lComment;
                        $('#Grid').jqxGrid('updaterow', rowData.boundindex, rowData);
                        //Close Modal
                        $modal.find(".close").click();
                    });
                }
            }],
            addCloseButton: true,
            title: "Add Comment",
            contentHtml: htmlContentModal,
            onReady: function ($modal) { }
        });
    }

}
//ADD editable cells to db
function addDetailComment(lPriorActual, MSDESCR, PriorActual, FlashAccount, ExpenseCategory, lAccrual, lUnposted, Comment, onSuccess) {
    var lTotalPrior = priorActual;
    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Adding extra file comments...",
        name: "[dbo].[spFlashInsertComment]",
        params: [
            { "Name": "@pAction", "Value": "Insert" },

            { "Name": "@pPriorActual", "Value": lPriorActual },

            { "Name": "@pSegmentId", "Value": getParameterByName('ID') },
            { "Name": "@pMSDesccr", "Value": MSDESCR },
            { "Name": "@pFlashExpenseAccount", "Value": FlashAccount },
            { "Name": "@pFlashExpenseCategory", "Value": ExpenseCategory },
            { "Name": "@pAccrual", "Value": parseFloat(lAccrual) },
            { "Name": "@pUnposted", "Value": parseFloat(lUnposted) },
            { "Name": "@pComment", "Value": Comment},
            { "Name": "@pMonth", "Value": bussinessMonth },
            { "Name": "@pYear", "Value": bussinessYear },
            { "Name": "@pSessionSOEID", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                //Return New ID
                _showNotification("success", "Data was added successfully.", "AlertAddComment"); 
                onSuccess();
            }
        }
    });
   
}
//Show diff month
function fnShowDiffMoM() {
    var PrevM = document.getElementById("PrevMonthTotal");
    var CurM = document.getElementById("TotalCurrentMonth");
    console.log(CurM - PrevM);
}
//Download Excel ELR
function fnFullElr() {

  _downloadExcel({
      spName: "[spFlashGetElRTotalBySegmentToExcel]",
      spParams: [
            { "Name": "@pIdSegment", "Value": getParameterByName('ID') },
            { "Name": "@vCurrentMonth", "Value": bussinessMonth }
      ],
      filename: "ELRList-" + getParameterByName('name')+ "-" + fnGetDate(),
      success: {
          msg: "Please wait, generating ELR report..."
      }
  });      
}
//Download GOCS to excel
function fnGetGocs() {
    _downloadExcel({
        spName: "[spFlashGetGOClistBySegmentToExcel ]",
        spParams: [
              { "Name": "@pIdSegment", "Value": getParameterByName('ID') },
              { "Name": "@vCurrentMonth", "Value": bussinessMonth }
        ],
        filename: "GOCList-" + getParameterByName('name') + "-" + fnGetDate(),
        success: {
            msg: "Please wait, generating GOC report..."
        }
    });
}
//Download GDW to excel

function fnGetCurrentGDW() {
    _downloadExcel({
        spName: "[spFlashGetGDWBySegmentExcelBySegment]",
        spParams: [
              { "Name": "@pIdSegment", "Value": getParameterByName('ID') },
              { "Name": "@vCurrentMonth", "Value": bussinessMonth }
        ],
        filename: "GDWList-" + getParameterByName('name') + "-" + fnGetDate(),
        success: {
            msg: "Please wait, GDW report..."
        }
    });
}
//Download prev FTE to excel
function fnPrevFTE() {
    _downloadExcel({
        spName: "[dbo].[spFlashGetPrevMonthGDWToExcel]",
        spParams: [
              { "Name": "@pIdSegment", "Value": getParameterByName('ID') },
              { "Name": "@vCurrentMonth", "Value": bussinessMonth }
        ],
        filename: "GDWList-" + getParameterByName('name') + "-" + fnGetDate(),
        success: {
            msg: "Please wait, GDW report..."
        }
    });
}
//Download Prior actual to excel
function fnGetPriorActual() {
    _downloadExcel({
        spName: "[dbo].[spFlashGetPriorActualBySegmentToExcel]",
        spParams: [
              { "Name": "@pIdSegment", "Value": getParameterByName('ID') },
              { "Name": "@vCurrentMonth", "Value": bussinessMonth }
        ],
        filename: "PriorActual-" + getParameterByName('name') + "-" + fnGetDate(),
        success: {
            msg: "Please wait, Prior Actual report..."
        }
    });
}
//Download current view to excel
function fnGetSegmentToExcel() {
    $('#Grid').jqxGrid('hidecolumn', 'check');
    var lSegment = getParameterByName('name');
    $("#Grid").jqxGrid("exportdata", "csv", "CurrentFlashView");
    $('#Grid').jqxGrid('showcolumn', 'check');

}

//show loaders
_execOnAjaxComplete(function () {
    setTimeout(function () {
        $(".ProgressHolder").fadeOut(1200, function () {
            _hideLoadingFullPage({
                idLoading: "FlashDetail"
            });
        });
    }, 300);
});
