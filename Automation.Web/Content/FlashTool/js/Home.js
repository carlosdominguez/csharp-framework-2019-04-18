$(document).ready(function () {
    function addData() {

        var urlFeeds = _getViewVar("SubAppPath") + '/FlashTool/TemplateStatus/';
        var $aLinkFlashFeed = $("#user-menu").find("a[href='" + urlFeeds + "']");
        $aLinkFlashFeed.attr("data-step", "1");
        $aLinkFlashFeed.attr("data-intro", "Manually you can upload all the excel files to mapping");

        var flashSegments = _getViewVar("SubAppPath") + '/FlashTool/FlashCustomSegmentsNames/';
        var $aLinkFlashSegments = $("#user-menu").find("a[href='" + flashSegments + "']").parent().parent().parent();
        $aLinkFlashSegments.attr("data-step", "2");
        $aLinkFlashSegments.attr("data-intro", "You can create or edit new segments name");

        var listSegments = _getViewVar("SubAppPath") + '/FlashTool/FlashCategory/';
        var $aLinkListSegmentsSegments = $("#user-menu").find("a[href='" + listSegments + "']");
        $aLinkListSegmentsSegments.attr("data-step", "3");
        $aLinkListSegmentsSegments.attr("data-intro", "You can see all the segments created by the users");

        var flashDetail = _getViewVar("SubAppPath") + '/FlashTool/FlashCategoryDetail/';
        var $aLinkFlashDetail = $("#user-menu").find("a[href='" + flashDetail + "']");
        $aLinkFlashDetail.attr("data-step", "4");
        $aLinkFlashDetail.attr("data-intro", "You can view the flash detail by segment * you have to choose at least one segment");

        var bridge = _getViewVar("SubAppPath") + '/FlashTool/FlashBridge/';
        var $aLinkBridge = $("#user-menu").find("a[href='" + bridge + "']");
        $aLinkBridge.attr("data-step", "5");
        $aLinkBridge.attr("data-intro", "You can check by segment, prior Run Rates, Add new onetimers or runrates and check your balance check");

        var consolidated = _getViewVar("SubAppPath") + '/FlashTool/FlashBridgeConsolidate/';
        var $aLinkConsolidated = $("#user-menu").find("a[href='" + consolidated + "']");
        $aLinkConsolidated.attr("data-step", "6");
        $aLinkConsolidated.attr("data-intro", "Check the CTI Summary and flash bridge to normalized");

        var fpa = _getViewVar("SubAppPath") + '/FlashTool/FlashFpa/';
        var $aLinkFpa = $("#user-menu").find("a[href='" + fpa + "']");
        $aLinkFpa.attr("data-step", "7");
        $aLinkFpa.attr("data-intro", "FP&A view by segment");

        var actualsMonitoring = _getViewVar("SubAppPath") + '/FlashTool/FlashCurrentActual/';
        var $aLinkActualsMonitoring = $("#user-menu").find("a[href='" + actualsMonitoring + "']");
        $aLinkActualsMonitoring.attr("data-step", "8");
        $aLinkActualsMonitoring.attr("data-intro", "You can see daily data to check your prioractual vrs current prior, only in BD2 to BD10");

    }
    setTimeout(function () { addData(); }, 1000);
        
});
