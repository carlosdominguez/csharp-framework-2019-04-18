_showLoadingFullPage({
    msg: "Loading Create New Segments",
    idLoading: "FlashSegments"
});
$(document).ready(function () {
    //Show Segments
    $("#showSegments").show();
    //Save Segments
    $("#saveSelectedSegments").click(function () {
        var checkedValues = $('input:checkbox:checked').map(function () {
            return this.value;
        }).get();
        checkedValues.length == 0 ? alert('you must check at least one') : fnSaveSelectedSegments(checkedValues);//replace alert for modal
        console.log(checkedValues);
        
    });
    //save custom name
    $("#saveSegmentName").click(function () {
        if ($('#lastId').length) {
            $('#lastId').remove();
        }
        var selText = $('#newSegmentName').val();
        selText == '' ? alert('You must Fill this value') : fnSaveCustomSegments(selText);
        console.log(selText);
        
    });
    //select levels to customize
    $(".dropdown-menu li a").click(function () {
        var selText = $(this).text();
        $(this).parents('.btn-group').find('.dropdown-toggle').html(selText + ' <span class="caret"></span>');
        //clean div for new data
        $("#column").empty();
        fnCustomShowSegmentsizeSegment(selText);
    });
    //Function to search into html
    $("#lSearch").keyup(function () {
        console.log("Handler for .keyup() called.");
        var input, filter, ul, li, a, i;
        input = document.getElementById('lSearch');
        filter = input.value.toUpperCase();
        ul = document.getElementById("column");
        li = ul.getElementsByTagName('li');

        // Loop through all list items, and hide those who don't match the search query
        for (i = 0; i < li.length; i++) {
            label = li[i].getElementsByTagName("label")[0];
            if (label.innerHTML.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    });
    
});
this.Id;
//fn to save new segments name
function fnSaveCustomSegments(lCustomName) {
    console.log(lCustomName);

    //va en el success
    
    //fnCustomShowSegmentsizeSegment(value);
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Saving Custom Name...",
        name: "[dbo].[spFlashCreateFlashSegment]",
        params: [
            { "Name": "@pFlashSegmentName", "Value": lCustomName },
            { "Name": "@pCreatedBy", "Value": _getSOEID() },  
        ],
        success: {
            //showTo: $(idMainJqxGrid).parent(),
            msg: "Name was save successfully.",
            fn: function (idToReturn) {
                var lId = idToReturn[0].Return;
                lId == 'EXIST' ? alert('Name already exist') : false;
                
                //ToDO: Global Variable
                var $LastIdInput = $('<input>').attr({
                    type: 'hidden',
                    id: 'lastId',
                    name: 'lastId',
                    value: lId
                })
                //var $CurrentIdInput = $('<input>').attr({
                //    type: 'hidden',
                //    id: 'CurrenId',
                //    name: 'CurrenId',
                //    value: lId
                //})
                $("#chklist").append($LastIdInput);
                //$("#chklist").append($CurrentIdInput);
                this.Id = lId;
                return this.Id;
                //fnCustomShowSegmentsizeSegment(value);
                
                
            }
        }
    });
}
//fn to show the segments with parameter level
function fnCustomShowSegmentsizeSegment(value) {
    var $getLevel = $('<input>').attr({
        type: 'hidden',
        id: 'levelId',
        name: 'levelId',
        value: value
    })
    $("#chklist").append($getLevel);
    console.log(value);
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Listing Segments.",
        name: "[dbo].[spGetManagementSegmentByLevel]",
        params: [
            { "Name": "@pLevel", "Value": value }
        ],
        //Show message only for the last 
        success: {
            showTo: $(column),
            msg: "Data load.",
            fn: function (responseList) {
                    //Function to show segments into checkboxs
                    function addCheckbox(responseList) {
                        //console.log(responseList);
                        var container = $('#chklist');
                        var inputs = container.find('input');
                        var divs = $("#chklist");
                        var col = 0;
                        for (var i = 0, len = responseList.length; i < len; i++) {
                            i % 2 ? setColumns() : setColumnsRest();
                            //fn to show columns with module and the others
                            col++
                            if (col === len) {
                                $("#chklist").show("slow");
                            }
                        }
                        //checkbox columns
                        function setColumns() {
                            var $div = $("<li>", { "class": "col-md-3" });
                            $('<input />', { type: 'checkbox', id: responseList[i].Description, value: responseList[i].ID }).appendTo($div);
                            $('<label />', { 'for': 'cb', text: responseList[i].Description }).appendTo($div);
                            setTimeout(function () {
                                $("#column").append($div);
                                   
                            }, 100);
                        }
                        //checkbox columns
                        function setColumnsRest() {
                            var $div = $("<li>", { "class": "col-md-3" });
                            $('<input />', { type: 'checkbox', id: responseList[i].Description, value: responseList[i].ID }).appendTo($div);
                            $('<label />', { 'for': 'cb', text: responseList[i].Description }).appendTo($div);
                            setTimeout(function () {
                                $("#column").append($div);

                            }, 100);
                        }
                        
                   
                    }
                //Add Checkbox
                addCheckbox(responseList);     
            }
        }
    });  
}
//fn to save segments
function fnSaveSelectedSegments(lSegmentsIds) {
    var lSegmentList = lSegmentsIds.join(", ")
    var lLastId = document.getElementById("lastId").value;
    var lgetLevel = document.getElementById("levelId").value
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Saving Segments...",
        name: "[dbo].[spFlashMapManagementSegmentByFlash]",
        params: [
            { "Name": "@pFlashSegmentID", "Value": lLastId },
            { "Name": "@pManagementSegmentsList", "Value": lSegmentList },
            { "Name": "@pCreatedBy", "Value": _getSOEID() } ,
            { "Name": "@pLevel", "Value": lgetLevel }
        ],
        success: {
            //showTo: $(idMainJqxGrid).parent(),
            msg: "Name was save successfully.",
            fn: function () {
                _showModal({
                    width: "75%",
                    title: "Segments Save Successfully",
                    contentHtml: "Segments Save Successfully",
                    buttons: [{
                        name: "Ok",
                        class: "btn-success",
                        closeModalOnClick: true,
                    }],
                    addCloseButton: true,
                    onReady: function () {
                    }
                });
                //$('#lastId').remove();
                $("#column").empty();
                //$("#showSegments").hide();
                $("#saveSegmentName").attr('btnCustomize', 'btn btn-success');

            }
        }
    });  
}
//On Ajax Complete 
_execOnAjaxComplete(function () {
    setTimeout(function () {
        $(".ProgressHolder").fadeOut(900, function () {
            _hideLoadingFullPage({
                idLoading: "FlashSegments"
            });
        });
    }, 200);
});

