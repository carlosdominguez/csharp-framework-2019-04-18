_showLoadingFullPage({
    msg: "Loading Flash Feeds status & Reports",
    idLoading: "FlashStatus"
});
var _uploadParameters = {};

$(document).ready(function () {
    loadFilesTables();
   
    //$("#contenttabletblFileTable").append('<button type="submit" class="btn btn-success">Save</button>');
    //On click upload files
   
});
/**
 * FlashAPSFULL****
 */
function loadFilesTables() {
   
   
    
    $("#fullMappingToExcel").click(function () {
        //alert('hola');
        var lSelectFullFile;
        $("select")
        .change(function () {
            var str = "";
            //$(".fullMapping select option:selected").each(function () {
            //    str += $(this).val() + " ";
            //});
            str = $("#fullMapping option:selected").val();
            lSelectFullFile = str;
        })
        .trigger("change");

        console.log(lSelectFullFile);
        function fnDisclaimer() {
            var htmlContentModal = '';
            htmlContentModal += "<b>If you don't see specific data, you would have to process it in flash detail.</b> ";
            _showModal({
                modalId: "modalUpload",
                width: '70%',
                buttons: [{
                    name: "<i id='okBtn' class='fa fa-save'></i> OK",
                    class: "btn-success",
                    closeModalOnClick: false,
                    onClick: function ($modal) {
                        //Add Comments
                        fnGetFullMappingExcel(lSelectFullFile.trim());
                        $modal.find(".close").click();
                    }
                }],
                addCloseButton: false,
                title: "Download full mappings",
                contentHtml: htmlContentModal,
                onReady: function ($modal) {
                }
            });

        }
        if (!lSelectFullFile) {

        } else {
            fnDisclaimer();
        }

    });
    //Dinamic full mapping depend of lSelectFile in Front end
    function fnGetFullMappingExcel(lSelectFile) {
        _downloadExcel({
            sql: "[dbo].[spFlashGetFullMappingExcel"+lSelectFile+"]",
            filename: lSelectFile + "_FULLMAPPINGReport",
            success: {
                msg: "Generating report... Please Wait, this operation may take some time to complete.",
            },
        });
         $('body').delegate('.closeButton', 'click', function () {
             // do your stuff
         });
        // $modal.find(".close").click();
    }
}

_execOnAjaxComplete(function () {
    setTimeout(function () {
        $(".ProgressHolder").fadeOut(900, function () {
            _hideLoadingFullPage({
                idLoading: "FlashStatus"
            });
        });
    }, 200);
});
