_showLoadingFullPage({
    msg: "Loading List Segments",
    idLoading: "FlashList"
});
$(document).ready(function () {
    $(".btnDelete").click(function () {
        fnDisableSegment();
    });
    fnLoadFlashCustomSegments();
});

//fn to disable the selected segments
function fnDisableSegment() {
    var objRowSelected = $.jqxGridApi.getOneSelectedRow("#flashEditSegmentsGrid", true);
    //fn return object by name an rowId
    var lFnGetDataFromGrid = function () {
        var lRowsSelected = $("#flashEditSegmentsGrid").jqxGrid('selectedrowindexes');
        var lSegmentsName = new Array();
        var lSelectedRecords = new Array();
        for (var i = 0; i < lRowsSelected.length; i++) {
            var lRow = $("#flashEditSegmentsGrid").jqxGrid('getrowdata', lRowsSelected[i]);
            lSelectedRecords[lSelectedRecords.length] = lRow.ID;
            lSegmentsName[lSegmentsName.length] = '<li>' + lRow.FlashSegmentName + '</li>'
        }
        return {
            SegmentsName: lSegmentsName,
            SelectedRecords: lSelectedRecords
        };

    }
    
    var fnSegmentsChoose = function () {
        var lSegments = lFnGetDataFromGrid().SegmentsName;
        return lSegments.join(" ");
    }
    
    var lRowsIds = lFnGetDataFromGrid().SelectedRecords.join(", ");
    
    if (objRowSelected) {
        var htmlContentModal = "<b><ol>" + fnSegmentsChoose(); + "</ol></b>";
        var src = "/FlashTool/FlashEditSegments/";
        _showModal({
            width: '35%',
            modalId: "modalDel",
            addCloseButton: true,
            buttons: [{
                name: "Delete",
                class: "btn-danger",
                onClick: function () {
                    //Save change to get the ID
                    _callProcedure({
                        loadingMsgType: "fullLoading",
                        loadingMsg: "Deleting Segments '" + lRowsIds + "'...",
                        name: "[dbo].[spFlashDisableFlashSegment]",
                        params: [
                            { "Name": "@pFlashSegmentID", "Value": lRowsIds }
                            //{ "Name": "@SessionSOEID", "Value": _getSOEID() },
                           
                        ],
                        success: {
                            fn: function (response) {
                                _showNotification("success", "The Segment '" + fnSegmentsChoose() + "' was deleted successfully.");
                                //Fn To reload de data
                                fnLoadFlashCustomSegments();
                            }
                        }
                    });
                }
            }],
            title: "Are you sure you want to delete this Segments?",
            contentHtml: htmlContentModal
        });
    }
}
//Fn to Load all the flash Segments
function fnLoadFlashCustomSegments() {
    
    $.jqxGridApi.create({
        showTo: "#flashEditSegmentsGrid",
        options: {
            //for comments or descriptions
            width: "100%",
            height: "500px",
            autoheight: false,
            autorowheight: false,
            autoheight: true,
            autorowheight: false,
            selectionmode: "multiplerows",
            showfilterrow: true,
            sortable: true,
            editable: true
            //'singlerow', 'multiplerows', 'checkbox', 'multiplerowsadvanced' or 'multiplerowsextended'.
        },
        sp: {
            Name: "[dbo].[spFlashGetAllFlashSegments]",
            Params: [ ]
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set"
        },
        groups: [],
        columns: [
            { name: 'ID', type: 'int', hidden: true },
            { name: 'FlashSegmentName', text: 'Segment Name', width: '55%', type: 'string', editable: false },
            { name: 'CreatedBy', text: 'Created By', width: '15%', type: 'string', editable: false, cellsalign: 'center', align: 'center' },
            { name: 'CreatedDate', text: 'Creation Date', width: '30%', type: 'string', cellsformat: 'D', cellsalign: 'center', align: 'center' },
        ],
        ready: function () {
            $("#flashEditSegmentsGrid").on('rowselect', function (event) {
                $("#sendParameter").attr("href", _getViewVar("SubAppPath") + "/FlashTool/FlashEditSegments?ID=" + event.args.row.ID +"&name="+ event.args.row.FlashSegmentName);
            });

        }
    });
}

_execOnAjaxComplete(function () {
    setTimeout(function () {
        $(".ProgressHolder").fadeOut(900, function () {
            _hideLoadingFullPage({
                idLoading: "FlashList"
            });
        });
    }, 200);
});

