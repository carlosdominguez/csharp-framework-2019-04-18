_showLoadingFullPage({
    msg: "Loading Bridge",
    idLoading: "FlashBridge"
});

//TODO: Put in a global JS
//Load Segments name
function fnLoadFlashCustomSegments() {
    $.jqxGridApi.create({
        showTo: "#flashSegmentsGrid",
        options: {
            width: "100%",
            height: "500px",
            autoheight: false,
            autorowheight: false,
            //selectionmode: "multiplerows",
            showfilterrow: true,
            sortable: true,
            editable: true
            //'singlerow', 'multiplerows', 'checkbox', 'multiplerowsadvanced' or 'multiplerowsextended'.
        },
        sp: {
            Name: "[dbo].[spFlashGetAllFlashSegments]",
            Params: []
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set"
        },
        groups: [],
        columns: [
            { name: 'ID', type: 'int', hidden: true },
            { name: 'FlashSegmentName', text: 'Segment Name', width: '55%', type: 'string', editable: false },
            { name: 'CreatedBy', text: 'Created By', width: '15%', type: 'string', editable: false, cellsalign: 'center', align: 'center' },
            { name: 'CreatedDate', text: 'Creation Date', width: '30%', type: 'string', cellsformat: 'D', cellsalign: 'center', align: 'center' },
        ],
        ready: function () {
            $("#flashSegmentsGrid").on('rowselect', function (event) {
                $("#flashbtn").prop("disabled", false);
                $("#bridgeBtn").prop("disabled", false);
                //$("#sendParameter").attr("href", _getViewVar("SubAppPath") + "/FlashTool/FlashCategoryDetail?ID=" + event.args.row.ID + "&name=" + event.args.row.FlashSegmentName);
                $("#sendBridgeParameter").attr("href", _getViewVar("SubAppPath") + "/FlashTool/FlashBridge?ID=" + event.args.row.ID + "&name=" + event.args.row.FlashSegmentName);
            });

        }
    });
}
//Go to segments
function fnGoToSegments() {

    var htmlContentModal = 'Choose at least one segment';
    var formHtml =
                '<div class="content-body">' +
                    '<div class="center">' +
                        '<div id="flashSegmentsGrid"></div>' +
                        '<a id="sendBridgeParameter">' +
                            '<button type="button" id="bridgeBtn" class="btnCustomize btn btn-primary bottom15 pull-right"><i class="fa fa-external-link"></i> Bridge</button>' +
                        '</a>' +
                    '</div>' +
                '</div>';
    fnLoadFlashCustomSegments();
    _showModal({
        modalId: "modalUpload",
        width: '40%',
        //buttons: [{
        //    name: "<i class='fa fa-save'></i> OK",
        //    class: "btn-success",
        //    closeModalOnClick: true,
        //    onClick: function ($modal) {
        //        //Validate required Comment
        //        window.location.href = _getViewVar("SubAppPath") + '/FlashTool/FlashCategory';
        //    }
        //}],
        addCloseButton: false,
        title: "Message: ",
        contentHtml: formHtml,
        onReady: function ($modal) { }
    });

}
var priorMonth;
var currentMonth;
var flashTotal = 0;
var ExpenseAccount;
var lExpectedCurrentMonth;
var bridgeType;
var oneTimmers;
var lTotalOneTimers;
//Fn to set BridgeType
function fnGetBridgeType() {
    _callProcedure({
        loadingMsgType: "Loading Expense Account",
        loadingMsg: "Loading...",
        name: "[dbo].[spFlashGetBridgeType]",
        //ToDo: Preguntar a CaRLOS sin o con -params
        params: [
        ],
        success: {
            fn: function (responseList) {
                bridgeType = responseList;
            }
        },
    });
}
//FN to show type of bridge
if (typeof getParameterByName('ID') === 'undefined' || !getParameterByName('ID')) {
    console.log('vacio');
} else {
    setTimeout(function () {
        fnGetBridgeType();
    }, 100);
}
//FN to get expense account
function fnGetExpenseAccounts() {
        _callProcedure({
            loadingMsgType: "Loading Expense Account",
            loadingMsg: "Loading...",
            name: "[dbo].[spFlashBridgeGetExpenseAccountsByFlashSegment]",
            params: [

            ],
            success: {
                fn: function (responseList) {
                    ExpenseAccount = responseList;
                    loadFilesTables();
                }
            },
        });
    }
//FN to show expense account
$(document).ready(function () {
    if (typeof getParameterByName('ID') === 'undefined' || !getParameterByName('ID')) {
        console.log('vacio');
    } else {
        //fnLoadTotalCurrentMonthOneTimmers();
        fnLoadOneTimers();
        //FN to get expense account
        setTimeout(function () {
            fnGetExpenseAccounts();
        }, 800);  
    }
    $("#bridgeBtn").prop("disabled", true);
    $("#nameOfSegment").html("Flash Segment: <br />" + getParameterByName('name'));
    $("#SaveBridge").click(function () {
        showModalAddBridgeData();
    });

    $("#delete").click(function () {
        //Delete Row in grid
        fnDeleteRow();
    });
});

//FN to show month
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

console.log(getParameterByName('ID'));
if (typeof getParameterByName('ID') === 'undefined' || !getParameterByName('ID')) {
    console.log('Entre');
    fnGoToSegments();
}
//Get Last date
var fnGetlastdate = function () {
    var now = new Date();
    currentMonth = now.getMonth() + 1;
    priorMonth = now.getMonth() + 1;
    var prevMonth;
    priorMonth = priorMonth - 1;
    if (priorMonth == -1 || priorMonth == 0) {
        switch (priorMonth) {
            case 0:
                return prevMonth = 1
                break;
            case -1:
                return prevMonth = 11
                break;
        }
    } else {
        return priorMonth
    }
}
//Get number of month
function getMonth() {
    var now = new Date();
    currentMonth = now.getMonth() + 1;
    priorMonth = now.getMonth() + 1;
    var currentYear = (new Date).getFullYear();
    var currentMonth = GetMonthName((new Date).getMonth());
    var lastMonth = GetMonthName((now.getMonth() - 1));
    console.log(lastMonth);
    $('#month').text('Flash Total ' /*+lastMonth*/);
    $('#ExpectedActualMonth').text('Expected ' + currentMonth);
    //Get Month Name
    function GetMonthName(monthNumber) {
        var lMonthNumber;
        monthNumber = monthNumber < 0 ? 11 : monthNumber;
        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        lMonthNumber = months.indexOf(monthNumber);
        console.log(lMonthNumber);
        return months[monthNumber];
    }
}
getMonth();
//FN if one time go to balance check
function fnChkOneTimmers(p) {
    if (lTotalOneTimers != p) {
        lTotalOneTimers = p;
        console.log('# ' + p);
        fnBalanceCheck();
    }
    
}
//ExpenseAccount1();
function fnLoadOneTimers() {
    $.jqxGridApi.create({
        showTo: "#tblFileTableOneTimers",
        options: {
            //for comments or descriptions
            showfilterrow: true,
            sortable: true,
            editable: true,
            selectionmode: 'singlecell',
            showstatusbar: true,
            statusbarheight: 25,
            showaggregates: true,            edittype: 'custom',
            selectionmode: "singlerow",
            resizable: true
            //'singlerow', 'multiplerows', 'checkbox', 'multiplerowsadvanced' or 'multiplerowsextended'.

        },

        sp: {
            Name: "[dbo].[spFlashGetOneTimers]",
            Params: [
                    { Name: "@pIdSegment", Value: getParameterByName('ID') },
                    { Name: "@pMonth", Value: getDate('m') },
            ]
        },

        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set",
        },
        columns: [
            { name: 'oneTimerId', text: 'oneTimerId', type: 'string', width: '20%', cellsalign: 'center', align: 'center', hidden: 'True' },
            {
                //si no es cero show alert revise por que no da 0, unexplained variance should be 0
                name: 'oneTimerMoM', text: 'MoM', type: 'number', width: '30%', cellsalign: 'left', align: 'left',
                cellsformat: 'd2',

                aggregates: [{
                    '<b>Total OneTimers:</b>':
                    function (aggregatedValue, currentValue, column, record) {
                        var total = $("#tblFileTableOneTimers").jqxGrid('getcolumnaggregateddata', 'oneTimerMoM', ['sum']);
                        sum = total.sum
                        if (isNaN(sum)) {
                            sum = 0;
                            return sum;
                        }
                        lTotalOneTimers = sum;
                        return sum
                    }
                }],
            },
            { name: 'ExpenseAccount', text: 'ExpenseAccount', type: 'string', width: '30%', cellsalign: 'center', align: 'center' },
            { name: 'oneTimerDescription', text: 'Description', type: 'string', width: '20%', cellsalign: 'center', align: 'center' },
            { name: 'oneTimerMonth', text: 'Month', type: 'string', width: '20%', cellsalign: 'center', align: 'center' },
        ],


        ready: function () {
            var formatter = new Intl.NumberFormat('en-US', {
                /*
                style: 'currency',
                currency: 'USD',
                */
                minimumFractionDigits: 2,
                // the default value for minimumFractionDigits depends on the currency
                // and is usually already 2
            });
            if (isNaN(lTotalOneTimers)) {
                oneTimmers = 0;
                lTotalOneTimers = 0;
            }
            $('#priorOneTimmers').text(formatter.format(lTotalOneTimers));
           
           
        }

    });

}
function loadFilesTables() {
    $.jqxGridApi.create({
        showTo: "#tblFileTable",
        options: {
            showfilterrow: true,
            sortable: true,
            editable: true,
            selectionmode: 'singlecell',
            showstatusbar: true,
            statusbarheight: 25,
            showaggregates: true,            edittype: 'custom',
            selectionmode: "singlerow"          
        },   
        sp: {
            Name: "[dbo].[spFlashGetBridgeByFlashSegment]",
            Params: [
                    { Name: "@pIdSegment", Value: getParameterByName('ID') },
                    { Name: "@pMonth", Value: getDate('m') }
            ]
        },
        
        source: {
            dataBinding: "Large Data Set",
        },
        columns: [

            //one timmer van aqui 
            //one timmer del mes anterior
            { name: 'bridgeId', type: 'number', hidden: true },
            //suma de atodas las varianzas esta en el correo
            {
                //si no es cero show alert revise por que no da 0, unexplained variance should be 0
                name: 'bridgeMoM', text: 'MoM', type: 'number', width: '30%', cellsalign: 'center', align: 'center',
                cellsformat: 'd2',
                aggregates: [{
                    '<b id="oneTimers">Total</b>':
                    function (aggregatedValue, currentValue, column, record) {
                        var total = $("#tblFileTable").jqxGrid('getcolumnaggregateddata', 'bridgeMoM', ['sum']);
                        
                        sum = total.sum
                        oneTimmers = sum;
                        
                        if (isNaN(sum)) {
                            sum = 0;
                            oneTimmers = sum;
                            return sum;
                        }

                        //fnChkOneTimmers(oneTimmers);
                        return sum
                    }
                }],
            },

            //formula flash - prevMonth
     
            { name: 'bridgeDescription', text: 'Description', width: '20%', type: 'string', filtertype: 'input', filterable: true, cellsalign: 'center', align: 'center' },
            //{ name: 'Date', text: 'Account', width: '19%', type: 'string', filtertype: 'input' },
            //todo lo que tengo en flash detail que agarre los nietos expensed account /// intellisence
            { name: 'bridgeExpenseAccountId', text: 'ExpenseAccountId', width: '20%', type: 'numeric', hidden: true },
            //{ name: 'Date', text: 'Account', width: '19%', type: 'string', filtertype: 'input' },
             {//poner todas las cuentas
                 name: 'ExpenseAccountId', text: 'Expense Account', columntype: 'dropdownlist', type: 'string', width: '30%',
                 cellsalign: 'center', align: 'center',
                 list: {
                     array: ExpenseAccount,
                     datafield: 'ExpenseAccountId',
                     displayfield: 'ExpenseAccount',
                     text: 'Expense Account',
                     autoComplete: true,
                     createeditor: function (row, value, editor) {
                         $.jqxGridApi.createEditor('dropdownlist', 'ExpenseAccountId', row, value, editor);
                         //$("#Grid").jqxGrid('setcellvalue', rowdata.uid, "ExpenseAccId", value);
                     }
                 }
             },
            
            {
                name: 'bridgeTypeId', text: 'One Timer / Run Rate', columntype: 'dropdownlist', type: 'string', width: '20%',
                cellsalign: 'center', align: 'center',
                list: {
                    array: bridgeType,
                    datafield: 'bridgeTypeId',
                    displayfield: 'bridgeType',
                    text: 'Bridge',
                    autoComplete: true,
                    createeditor: function (row, value, editor) {
                        $.jqxGridApi.createEditor('dropdownlist', 'bridgeTypeId', row, value, editor);
                    }
                }
            },
            //"month - year"
        ],
        
        
        ready: function () {
            if (typeof getParameterByName('ID') === 'undefined' || !getParameterByName('ID')) {
                console.log('vacio');
            } else {
                getPriorActual();
            }
        }

    });
    //Add new row to the grid
    $("#addrowbutton").one('click', function (event) {
        $("#tblFileTable").jqxGrid('addrow', null, {});
        return (false);
    })
}
//fn show modal to check data to save in db
function showModalAddBridgeData() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#tblFileTable', true);
    if (rowData) {
        var bridgeMoM = typeof rowData["bridgeMoM"] === 'undefined' ? 0 : rowData["bridgeMoM"];
        var bridgeDescription = typeof rowData["bridgeDescription"] === 'undefined' ? 0 : rowData["bridgeDescription"];
        var ExpenseAccountId = rowData["bridgeExpenseAccountId"] || rowData.ExpenseAccountId;
        var ExpenseAccount = rowData["ExpenseAccount"];
        var bridgeTypeId = typeof rowData["bridgeTypeId"] === 'undefined' ? 0 : rowData["bridgeTypeId"];
        var bridgeType = rowData["bridgeType"];
        var bridgeId = rowData["bridgeId"];
        console.log(bridgeId);
        if (!bridgeId) {
            bridgeId = '0';
        }
        var htmlContentModal = '';
        htmlContentModal += "<b>MoM: </b> " + bridgeMoM + " <br/>";
        htmlContentModal += "<b>Description: </b>" + bridgeDescription + "<br/>";
        htmlContentModal += "<b>Expense Account: </b>" + ExpenseAccount + "<br/>";
        htmlContentModal += "<b>Action Month: </b>" + bridgeType + "<br/>";
        htmlContentModal += "<b>Comments: </b><br/>";
        htmlContentModal += "<textarea disabled class='form-control txtDetailComment' placeholder='(optional)' style='height:100px !important;'>" + bridgeDescription + "</textarea>";

        _showModal({
            modalId: "modalUpload",
            width: '70%',
            buttons: [{
                name: "<i class='fa fa-save'></i> Save",
                class: "btn-success",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    //FN: to save data to db
                    addDetailToTblBridge(getParameterByName('ID')
                                        , bridgeId
                                        , bridgeMoM
                                        , bridgeDescription
                                        , ExpenseAccountId
                                        , bridgeTypeId,
                                        function () {
                                            _showNotification("success"
                                                , "Bridge data was save successfully"
                                                , "AlertReject"); 
                                            //Close Modal
                                            fnBalanceCheck();
                                            $modal.find(".close").click();
                                        });
                }
            }],
            addCloseButton: true,
            title: "Add Bridge Detail",
            contentHtml: htmlContentModal,
            onReady: function ($modal) { }
        });
    }

}
//FN: to save data to db
function addDetailToTblBridge(segmentId, bridgeId, bridgeMoM, bridgeDescription, ExpenseAccountId, bridgeTypeId, onSuccess) {
    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Adding to bridge...",
        name: "[dbo].[spFlashInsertBridgeDetail]",
        params: [
            { "Name": "@pAction", "Value": "Insert" },
            { "Name": "@pSegmentId", "Value": segmentId },
            { "Name": "@pBridgeId", "Value": bridgeId },
            { "Name": "@pBridgeMoM", "Value": bridgeMoM },
            { "Name": "@pBridgeDescription", "Value": bridgeDescription },
            { "Name": "@pExpenseAccountId", "Value": ExpenseAccountId },
            { "Name": "@pBridgeTypeId", "Value": bridgeTypeId },
            { "Name": "@pMonth", "Value": getDate('m') },
            { "Name": "@pYear", "Value": getDate('y') },
            { "Name": "@pSessionSOEID", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                //Return New ID
                _showNotification("success", "Data was added successfully.", "AlertAddComment");
                loadFilesTables();
                onSuccess();
                //insertToFlashBridge();
                //
            }
        }
    });

}
//FN: to delete Row
function fnDeleteRow() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#tblFileTable', true);
    if (rowData) {
        //var lIdComment = rowData["IDComment"];
        var bridgeMoM = typeof rowData["bridgeMoM"] === 'undefined' ? 0 : rowData["bridgeMoM"];
        var bridgeDescription = typeof rowData["bridgeDescription"] === 'undefined' ? 0 : rowData["bridgeDescription"];
        var ExpenseAccountId = rowData["bridgeExpenseAccountId"] || rowData.ExpenseAccountId;
        //var ExpenseAccountId = 
        var ExpenseAccount = rowData["ExpenseAccount"];
        var bridgeTypeId = typeof rowData["bridgeTypeId"] === 'undefined' ? 0 : rowData["bridgeTypeId"];
        var bridgeType = rowData["bridgeType"];
        var bridgeId = rowData["bridgeId"];

        _callProcedure({
            loadingMsgType: "topBar",
            loadingMsg: "Deleting...",
            name: "[dbo].[spFlashDeleteBridgeDetail]",
            params: [
                { "Name": "@pAction", "Value": "Insert" },
                { "Name": "@pSegmentId", "Value": getParameterByName('ID') },
                { "Name": "@pBridgeId", "Value": bridgeId },
                { "Name": "@pBridgeMoM", "Value": bridgeMoM },
                { "Name": "@pBridgeDescription", "Value": bridgeDescription },
                { "Name": "@pExpenseAccountId", "Value": ExpenseAccountId },
                { "Name": "@pBridgeTypeId", "Value": bridgeTypeId },
                { "Name": "@pMonth", "Value": getDate('m') },
                { "Name": "@pYear", "Value": getDate('y') },
                { "Name": "@pSessionSOEID", "Value": _getSOEID() }
            ],
            success: {
                fn: function (responseList) {
                    $("#tblFileTable").jqxGrid('deleterow', rowData["uid"]);
                    //Return New ID
                    loadFilesTables();
                    _showNotification("success", "Row was deleted successfully.", "AlertAddComment");
                    //onSuccess();
                    //insertToFlashBridge();
                    //
                }
            }
        });
    }
}

var PriorActual;
//FN: to get prior actual
function getPriorActual(fnOnSuccess) {
    _callProcedure({
        loadingMsgType: "Loading Prior Actual total",
        loadingMsg: "Calculating...",
        name: "[dbo].[spFlashGetTotalByFlashSegment]",
        params: [
            { Name: '@pIdSegment', Value: getParameterByName('ID') },
            { Name: '@pMonth', Value: getDate('m') }

        ],
        success: {
            //Set Prior actual from flash detail
            fn: function (responseList) {
                var result = 1;
                if (responseList.length != 0) {
                    result = Number(responseList[0].PriorActual);
                    PriorActual = result;
                    //Format pearlActual
                    var formatter = new Intl.NumberFormat('en-US', {
                        /*
                        style: 'currency',
                        currency: 'USD',
                        */
                        minimumFractionDigits: 2,
                        // the default value for minimumFractionDigits depends on the currency
                        // and is usually already 2
                    });
                    //Show prior actual
                    $('#totalPriorActual').text(formatter.format(result));
                    //$('#ExpectedActualMonth').text(formatter.format(e));          
                    console.log('oneTimmers ' + oneTimmers);
                    //IF NAN set 0
                    if (isNaN(lTotalOneTimers)) {
                        lTotalOneTimers = 0;
                    }
                    //Set Current Month
                    lExpectedCurrentMonth = PriorActual - lTotalOneTimers;
                    console.log(lExpectedCurrentMonth);
                    //Show current month in ID
                    $('.FlashExpectedTotal').text(formatter.format(lExpectedCurrentMonth))
                    setTimeout(function () { fnBalanceCheck(); }, 2000);
                } else {
                    PriorActual = 0;
                    lExpectedCurrentMonth = PriorActual - lTotalOneTimers;
                    console.log('In Else' + lExpectedCurrentMonth);
                    setTimeout(function () { fnBalanceCheck(); }, 2000);
                }
                if (fnOnSuccess) {
                    fnOnSuccess(result);
                }
            }
        },
    });
}
//If balanceCheck is negative show blinker
function blinker() {
    $('.BalanceCheck').fadeOut(1800);
    $('.BalanceCheck').fadeIn(1800);
}
function StopBlinker() {
    $('.BalanceCheck').fadeOut(function () {
        $(this).remove();
    });
    //$('.BalanceCheck').append(lBalanceCheck);

}
setInterval(blinker, 1000);
//Calculate balanceCheck
function fnBalanceCheck() {
    var totalBridge = $("#tblFileTable").jqxGrid('getcolumnaggregateddata', 'bridgeMoM', ['sum']);
    totalBridge.sum = totalBridge.sum || 0;
    console.log(totalBridge);
    var lDate = new Date();
    var firstDay = new Date(lDate.getFullYear(), lDate.getMonth(), 1);
    var lastDayOfTheMonth = new Date(lDate.getFullYear(), lDate.getMonth() + 1, 0);
    var lBalanceCheck = lExpectedCurrentMonth - flashTotal + totalBridge.sum;  //- totalOneTimmers  ///algo menos el expected pprior actual - onetimmers  flash total menos items 
    lBalanceCheck = Number(lBalanceCheck).toFixed(8).replace(/\.?0+$/, "");
    lBalanceCheck = Math.round(lBalanceCheck);
    var formatter = new Intl.NumberFormat('en-US', {
        /*
        style: 'currency',
        currency: 'USD',
        */
        minimumFractionDigits: 2,
        // the default value for minimumFractionDigits depends on the currency
        // and is usually already 2
    });
    if (isNaN(lBalanceCheck)) {
        lBalanceCheck = 0;
        return lBalanceCheck;
    }
    var lCheckDate = lDate - lastDayOfTheMonth;
    
    console.log(lCheckDate);
    if (lCheckDate = 0) {
        alert('Is the end of the month, you should check your onetimmers');
    }
    if (lBalanceCheck < 0) {
        //$("#SaveBridge").prop("disabled", true);

        if (lBalanceCheck === 0) {
            $('.BalanceCheck').removeClass('bg-primary').addClass('bg-info');
            $('.BalanceCheck').text(formatter.format(lBalanceCheck))
        }else{
            $('.BalanceCheck').removeClass('bg-primary').addClass('bg-danger');
            $('.BalanceCheck').text(formatter.format(lBalanceCheck))
        }       

        //alert('Please check your one timmers');
        //blinker();
    } else {
        if (lBalanceCheck == -0) {
            $('.BalanceCheck').removeClass('bg-primary').addClass('bg-info');
            $('.BalanceCheck').text(formatter.format(lBalanceCheck))
        } else {
            $('.BalanceCheck').removeClass('bg-primary').addClass('bg-success');
            $('.BalanceCheck').text(formatter.format(lBalanceCheck))
        }
        
        //$("#SaveBridge").prop("disabled", false);
    }
    console.log(lBalanceCheck);
}
//Get total by segment from flash detail
function fnGetFlashTotal(fnOnSuccess) {
    _callProcedure({
        loadingMsgType: "Loading Flash total",
        loadingMsg: "Calculating...",
        name: "[dbo].[spFlashGetFlashTotal]",
        params: [
            { Name: '@pIdSegment', Value: getParameterByName('ID') },
            { Name: '@pMonth', Value: getDate('m') }
        ],
        success: {
            fn: function (responseList) {

                var formatter = new Intl.NumberFormat('en-US', {
                    /*
                    style: 'currency',
                    currency: 'USD',
                    */
                    minimumFractionDigits: 2,
                    // the default value for minimumFractionDigits depends on the currency
                    // and is usually already 2
                });
                var result = 1;
                if (responseList.length != 0) {
                    result = Number(responseList[0].FLASHTOTAL);
                    flashTotal = result;
                    $('.FlashTotal').text(formatter.format(result));
                } else {
                    result = 0;
                    $('.FlashTotal').text(formatter.format(result));
                }
                if (fnOnSuccess) {
                    fnOnSuccess(result);
                }
            }
        },
    });
}
//If segment is undefined
if (typeof getParameterByName('ID') === 'undefined' || !getParameterByName('ID')) {
    console.log('vacio');
} else {
    fnGetFlashTotal();
}
//Show loader
_execOnAjaxComplete(function () {
    $(".ProgressHolder").fadeOut(1900, function () {
        // Animation complete.
    });
});

