_showLoadingFullPage({
    msg: "Loading Actuals Monitoring",
    idLoading: "FlashCurrentActual"
});

//TODO: Put in a global JS
var businessDay;
var BDStart;
var BDEnd;
//Check BD
function fnCheckBD() {
    _callProcedure({
        loadingMsgType: "Loading business day",
        loadingMsg: "Loading...",
        name: "[dbo].[spFlashCheckBusinessDay]",
        params: [

        ],
        success: {
            fn: function (responseList) {
                businessDay = responseList;
                fnCheckBDFromArray(businessDay);
            }
        },
    });
}
//Check bussines days
function fnCheckBDFromArray(businessDay) {
    var currentDate = fnGetDate();
    businessDay.map(function (obj, index) {
        if (obj.BusinessDay == "1") {
            BDStart = obj.WorkDay.split(' ')[0];
        }
        if (obj.BusinessDay == "10") {
            BDEnd = obj.WorkDay.split(' ')[0];
        }

    }).filter(isFinite)
    console.log(BDStart)
    console.log(BDEnd);
    if (Date.parse(currentDate) > Date.parse(BDEnd) && Date.parse(currentDate) < Date.parse(BDStart)) {
        _showAlert({
            id: "MessageBDValidation",
            showTo: $("#flashSegmentsGrid").parent(),
            type: 'info',
            title: "Message",
            content: "Data is only processed between BD1 and BD10"
        });

    } else {
        _showAlert({
            id: "MessageBDValidation",
            showTo: $("#flashSegmentsGrid").parent(),
            type: 'error',
            title: "Message",
            content: "Data is only processed between BD1 and BD10"
        });
        //document.getElementById("actualBtn").disabled = true;
    }
}
$(document).ready(function () {
    //Save comments
    $("#editComment").click(function () {
        showModalAddCommentEdit();
    });    //Save number for actuals monitoring    $("#SaveMonitoring").click(function () {
        fnSaveActualsMonitoring();
    });
    //Load Monitoring
    LoadActualsMonitoring();
    //Export view to excel
    $("#exportToExcel").click(function () {
        exportToExcel();
        //$("#ActualsMonitoring").jqxGrid("exportdata", "csv", "Variance Summary" + "-" + fnGetDate());
    });

    //$("#contenttabletblFileTable").append('<button type="submit" class="btn btn-success">Save</button>');
});
//Global variables to set: priorActual and flashTotal
var editedRows = new Array();
var editedIndex = {};

function exportToExcel() {
    _downloadExcel({
        spName: "[dbo].[spFlashGetActualsMonitoringToExcel]",
        spParams: [
            //{ "Name": "@pIdSegment", "Value": getParameterByName('ID') },
            //{ "Name": "@vCurrentMonth", "Value": getDate('m') }
        ],
        filename: "PearlActualsReport" + "-" + fnGetDate(),
        success: {
            msg: "Please wait, generating report..."
        }
    });
}
//FN: to get data from query string
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
//Load monitoring grid
function LoadActualsMonitoring() {
    $("#nameOfSegment").html(getParameterByName('name'));

    var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties, data) {

        if (columnfield == 'FinalFlash') {
            if (value == "") {
                value = 0;
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(value, "d2"); + '</span></div>';
            } else {
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(value, "d2"); + '</span></div>';
            }
        }
        if (columnfield == 'PreviousDayActual') {
            if (value == "") {
                value = 0;
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(value, "d2"); + '</span></div>';
            } else {
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(value, "d2"); + '</span></div>';
            }
        }
        if (columnfield == 'ActualsPearlToday') {
            if (value == "") {
                value = 0;
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(value, "d2"); + '</span></div>';
            } else {
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(value, "d2"); + '</span></div>';
            }
        }
        if (columnfield == 'PendingEntries') {
            if (value == "") {
                value = 0;
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(value, "d2"); + '</b></span></div></center>';
            } else {
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(value, "d2"); + '</b</span></div></center>';
            }
        }
        if (columnfield == 'yesterdayPrior') {
            //console.log(data);
            if (value == "") {
                value = 0;
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(value, "d2") + '</b></span></div>';
            } else {
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(value, "d2") + '</b></span></div>';
            }
        }
        if (columnfield == 'PriorActual') {
            //console.log(data);
            if (value == "") {
                value = 0;
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(value, "d2") + '</b></span></div>';
            } else {
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(value, "d2") + '</b></span></div>';
            }
        }

    }
    $.jqxGridApi.create({
        showTo: "#ActualsMonitoring",
        options: {
            sortable: true,
            editable: true,
            showstatusbar: true,
            statusbarheight: 25,
            showaggregates: true,
            showfilterrow: true,
            sortable: true,
            editable: true

        },
        sp: {
            Name: "[dbo].[spFlashGetActualsMonitoring]",
            Params: [
                    //{ Name: "@vCurrentMonth", Value: getDate('m') },
                    //{ Name: "@vCurrentMonth", Value: getDate('y') }

            ]
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set"

        },
        /**
        Prueba
        **/
        /// <Summary>
        /// Esto es una prueba del tool de docs
        /// </summary>
        columns: [
            {
                name: 'SegmentId', text: 'SegmentID', width: '25%', type: 'string', filtertype: 'input', filterable: true, cellsalign: 'center', align: 'center',
                editable: false, hidden: true,
            },
            //columna segment ID
             {
                 name: 'SegmentDescription', text: 'Segment Description', width: '25%', type: 'string', filtertype: 'input', filterable: true, cellsalign: 'center', align: 'center',
                 editable: false, pinned: true,
             },
             {
                 name: 'FinalFlash', text: 'Final Flash', width: '15%', type: 'string', filtertype: 'input', filterable: true, cellsalign: 'right', align: 'Center',
                 editable: false, cellsrenderer: cellsrenderer,
                 cellsformat: 'd2', aggregates: [{
                     'Total':
                     function (aggregatedValue, currentValue, column, record) {
                         var total = $("#ActualsMonitoring").jqxGrid('getcolumnaggregateddata', 'FinalFlash', ['sum']);
                         sum = total.sum
                         this.priorActual = sum;
                         return sum;
                     }
                 }],
             },
             //actual del mes anterior esta en el flash detail
            {
                name: 'PreviousDayActual', datafield: 'Previous Day Actual', text: 'Previous Day Actual', width: '15%', type: 'number', filtertype: 'input',
                editable: false, cellsalign: 'right', align: 'Center', cellsrenderer: cellsrenderer,
                cellsformat: 'd2', aggregates: [{
                    'Total':
                    function (aggregatedValue, currentValue, column, record) {
                        var total = $("#ActualsMonitoring").jqxGrid('getcolumnaggregateddata', 'PreviousDayActual', ['sum']);
                        sum = total.sum
                        this.priorActual = sum;
                        return sum;
                    }
                }],


            },
             //Actual del dia anterior este esta en pearl--- bd2 primer dia va a estar en blanco
             {
                 name: 'ActualsPearlToday', text: 'Actuals Pearl Today', width: '16%', type: 'string', filtertype: 'input', filterable: true, cellsalign: 'right', align: 'center',
                 editable: false, cellsrenderer: cellsrenderer, cellsformat: 'd2',
                 aggregates: [{
                     'Total':
                     function (aggregatedValue, currentValue, column, record) {
                         var total = $("#ActualsMonitoring").jqxGrid('getcolumnaggregateddata', 'ActualsPearlToday', ['sum']);
                         sum = total.sum
                         this.priorActual = sum;
                         return sum;
                     }
                 }],
             },
              //editable
             {
                 name: 'PendingEntries', text: 'Pending', width: '13%', type: 'Number', filtertype: 'input', filterable: true, cellsalign: 'right', align: 'center',
                 cellsrenderer: cellsrenderer, cellsformat: 'd2',
                 aggregates: [{
                     'Total':
                             function (aggregatedValue, currentValue, column, rowdata) {
                                 if (rowdata.PendingEntries == null || rowdata.PendingEntries === "") {
                                     rowdata.PendingEntries = 0;
                                 }
                                 //var lMonth = parseFloat(rowdata.today) + parseFloat(rowdata.Accrual) + parseFloat(rowdata.UnpostedCanada) + parseFloat(rowdata.APS);
                                 var total = parseFloat(rowdata.PendingEntries);
                                 total = total || 0;

                                 //console.log(rowdata);
                                 //var aggregatedValue = isNaN(parseInt(aggregatedValue)) ? 0 : parseInt(aggregatedValue)
                                 //console.log(aggregatedValue);
                                 if (isNaN(aggregatedValue)) {
                                     aggregatedValue = 0;
                                 }
                                 var lShowTotal = aggregatedValue + total;
                                 return lShowTotal;//$.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(lShowTotal, "d2");
                             }
                 }]
             },
              //ExpectedActuals = actualPearl + PendingEntries es igual al expected
             {
                 name: 'expectedActuals', datafield: 'Expected Actuals', text: 'Expected Actuals', width: '17%', type: 'string', filtertype: 'input', cellsalign: 'right', align: 'center',
                 editable: false, aggregates: ['sum'], cellsformat: 'd2', cellsrenderer:
                 function (index, datafield, value, defaultvalue, column, rowdata) {
                     if (rowdata.ActualsPearlToday == null || rowdata.ActualsPearlToday === "") {
                         rowdata.ActualsPearlToday = 0;
                     }
                     if (rowdata.PendingEntries == null || rowdata.PendingEntries === "") {
                         rowdata.PendingEntries = 0;
                     }
                     //var lMonth = parseFloat(rowdata.today) + parseFloat(rowdata.Accrual) + parseFloat(rowdata.UnpostedCanada) + parseFloat(rowdata.APS);
                     var total = parseFloat(rowdata.ActualsPearlToday) + parseFloat(rowdata.PendingEntries);
                     total = total || 0;
                     return "<div style='margin: 4px;' class='jqx-right-align'>" + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(total, "d2"); + "</div>";
                 },
                 aggregates: [{
                     'Total':
                             function (aggregatedValue, currentValue, column, rowdata) {
                                 if (rowdata.ActualsPearlToday == null || rowdata.ActualsPearlToday === "") {
                                     rowdata.ActualsPearlToday = 0;
                                 }
                                 if (rowdata.PendingEntries == null || rowdata.PendingEntries === "") {
                                     rowdata.PendingEntries = 0;
                                 }
                                 //var lMonth = parseFloat(rowdata.today) + parseFloat(rowdata.Accrual) + parseFloat(rowdata.UnpostedCanada) + parseFloat(rowdata.APS);
                                 var total = parseFloat(rowdata.ActualsPearlToday) + parseFloat(rowdata.PendingEntries);
                                 total = total || 0;

                                 //console.log(rowdata);
                                 //var aggregatedValue = isNaN(parseInt(aggregatedValue)) ? 0 : parseInt(aggregatedValue)
                                 //console.log(aggregatedValue);
                                 if (isNaN(aggregatedValue)) {
                                     aggregatedValue = 0;
                                 }
                                 var lShowTotal = aggregatedValue + total;
                                 return lShowTotal;//$.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(lShowTotal, "d2");
                             }
                 }]
             },
              //expected - currentflash (# de flash)
             {
                 name: 'IDPearlActualVsFlash', text: 'I/(D) Pearl Actual vs Flash', width: '13%', type: 'string', filtertype: 'input', filterable: true, cellsalign: 'right', align: 'center',
                 aggregates: ['sum'], cellsformat: 'd2', editable: false,
                 cellsrenderer:
                 function (index, datafield, value, defaultvalue, column, rowdata) {

                     if (rowdata.ActualsPearlToday == null || rowdata.ActualsPearlToday === "") {
                         rowdata.ActualsPearlToday = 0;
                     }
                     if (rowdata.PendingEntries == null || rowdata.PendingEntries === "") {
                         rowdata.PendingEntries = 0;
                     }
                     //var lMonth = parseFloat(rowdata.today) + parseFloat(rowdata.Accrual) + parseFloat(rowdata.UnpostedCanada) + parseFloat(rowdata.APS);
                     var ExpectedActuals = parseFloat(rowdata.ActualsPearlToday) + parseFloat(rowdata.PendingEntries);


                     var total = parseFloat(ExpectedActuals) - parseFloat(rowdata.FinalFlash);
                     //if (rowdata.IDPreviousDayActuals != total) {
                     //    $("#ActualsMonitoring").jqxGrid('setcellvalue', rowdata.uid, "IDPreviousDayActuals", total);
                     //}
                     total = total || 0;
                     return "<div style='margin: 4px;' class='jqx-right-align'>" + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(total, "d2"); + "</div>";
                 },
                 aggregates: [{
                     'Total':
                             function (aggregatedValue, currentValue, column, rowdata) {
                                 if (rowdata.ActualsPearlToday == null || rowdata.ActualsPearlToday === "") {
                                     rowdata.ActualsPearlToday = 0;
                                 }
                                 if (rowdata.PendingEntries == null || rowdata.PendingEntries === "") {
                                     rowdata.PendingEntries = 0;
                                 }
                                 //var lMonth = parseFloat(rowdata.today) + parseFloat(rowdata.Accrual) + parseFloat(rowdata.UnpostedCanada) + parseFloat(rowdata.APS);
                                 var ExpectedActuals = parseFloat(rowdata.ActualsPearlToday) + parseFloat(rowdata.PendingEntries);


                                 var total = parseFloat(ExpectedActuals) - parseFloat(rowdata.FinalFlash);

                                 //console.log(rowdata);
                                 //var aggregatedValue = isNaN(parseInt(aggregatedValue)) ? 0 : parseInt(aggregatedValue)
                                 //console.log(aggregatedValue);
                                 if (isNaN(aggregatedValue)) {
                                     aggregatedValue = 0;
                                 }
                                 var lShowTotal = aggregatedValue + total;
                                 return lShowTotal;//$.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(lShowTotal, "d2");
                             }
                 }]
             },
             ////reporte de balance de pearl de el dia se corre a medio dia
             //{
             //    name: 'CurrentPrior', text: 'Current', width: '19%', type: 'string', filtertype: 'input', filterable: true, cellsalign: 'right', align: 'center',
             //    editable: false, cellsrenderer: cellsrenderer,
             //    cellsformat: 'd2', aggregates: [{
             //        'Total':
             //        function (aggregatedValue, currentValue, column, record) {
             //            var total = $("#ActualsMonitoring").jqxGrid('getcolumnaggregateddata', 'CurrentPrior', ['sum']);
             //            sum = total.sum
             //            this.priorActual = sum;
             //            return sum;
             //        }
             //    }],
             //},
             //fourmula = current - yesterday
             {
                 name: 'IDPreviousDayActuals', text: 'DoD Variance', width: '17%', type: 'Number', filtertype: 'input', filterable: true, cellsalign: 'right', align: 'center',
                 editable: false, cellsformat: 'd2', aggregates: ['sum'],
                 cellsrenderer:
                    function (index, datafield, value, defaultvalue, column, rowdata) {
                        if (rowdata.ActualsPearlToday == null || rowdata.ActualsPearlToday === "") {
                            rowdata.ActualsPearlToday = 0;
                        }
                        if (rowdata.PreviousDayActual == null || rowdata.yestePreviousDayActualrdayPrior === "" || rowdata.PreviousDayActual === null) {
                            rowdata.PreviousDayActual = 0;
                        }
                        //console.log('Current' + rowdata.CurrentPrior);
                        //console.log('yesterday' + rowdata.yesterdayPrior);
                        var total = parseFloat(rowdata.ActualsPearlToday) - parseFloat(rowdata.PreviousDayActual);
                        //console.log(rowdata);

                        if (rowdata.IDPreviousDayActuals != total) {
                            //console.log(rowdata);
                            //$("#ActualsMonitoring").jqxGrid('setcellvalue', rowdata.uid, "IDPreviousDayActuals", total);
                        }
                        //Base de datos no se valido isNull
                        return "<div style='margin: 4px;' class='jqx-right-align'>" + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(total, "d2"); + "</div>";
                    },
                 aggregates: [{
                     'Total':
                             function (aggregatedValue, currentValue, column, rowdata) {
                                 if (rowdata.ActualsPearlToday == null || rowdata.ActualsPearlToday === "") {
                                     rowdata.ActualsPearlToday = 0;
                                 }
                                 if (rowdata.PreviousDayActual == null || rowdata.PreviousDayActual === "" || rowdata.PreviousDayActual === null) {
                                     rowdata.PreviousDayActual = 0;
                                 }
                                 var total = parseFloat(rowdata.ActualsPearlToday) - parseFloat(rowdata.PreviousDayActual);
                                 //console.log(rowdata);
                                 //var aggregatedValue = isNaN(parseInt(aggregatedValue)) ? 0 : parseInt(aggregatedValue)
                                 //console.log(aggregatedValue);
                                 if (isNaN(aggregatedValue)) {
                                     aggregatedValue = 0;
                                 }
                                 var lShowTotal = aggregatedValue + total;
                                 return lShowTotal;//$.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(lShowTotal, "d2");
                             }
                 }]
             },



            {
                name: 'Comment', width: '80px', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center', editable: false, cellsrenderer: function (rowIndex, datafield, value) {
                    var dataRecord = $("#ActualsMonitoring").jqxGrid('getrowdata', rowIndex);
                    //console.log(dataRecord.Comment);
                    var htmlComment = "<div style='padding: 10px; font-size: 15px; max-height: 500px; overflow-y:scroll; width: 520px;'>";
                    htmlComment += "<b>Comment:</b> " + dataRecord.Comment + " <br>";
                    htmlComment += "</div>";
                    var htmlResult = "";

                    if (dataRecord.Comment) {
                        htmlResult +=
                            '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + htmlComment + '" data-title="Comments History" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                            '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png"/> ' +
                            '</div>';
                    }
                    return htmlResult;
                }
            },
        ],


        ready: function () {
            $("#ActualsMonitoring").bind('cellvaluechanged', function (event) {
                // event arguments.
                var args = event.args;
                // column data field.
                var datafield = event.args.datafield;
                // row's bound index.
                var rowBoundIndex = args.rowindex;
                // new cell value.
                var value = args.newvalue;
                // old cell value.
                var oldvalue = args.oldvalue;
                //var allData = $("#Grid").jqxGrid('getrowdata', rowBoundIndex);
                //editedRows.push({ ExpenseCategory: allData.ExpenseCategory, FlashAccount: allData.FlashAccount, PriorActual: allData.PriorActual, Accrual: allData.Accrual, Unposted: allData.UnpostedCanada });

                if (!editedIndex[rowBoundIndex]) {
                    editedIndex[rowBoundIndex] = true;
                }

            });
            $("#ActualsMonitoring").on("rowclick", function (event) {
                _GLOBAL_SETTINGS.tooltipsPopovers();
            });
        }

    });
}


_showLoadingFullPage({
    msg: "Loading Actuals Monitoring"
});
//FN: to save Actuals monitoring
function fnSaveActualsMonitoring() {
    var indexRows = Object.keys(editedIndex);
    var rowData;
    for (var i = 0; i < indexRows.length; i++) {
        rowData = $("#ActualsMonitoring").jqxGrid('getrowdata', indexRows[i]);
        fnSaveMonitoring(rowData);
    }
    //FN: to save to db
    function fnSaveMonitoring() {

        var rowData = $.jqxGridApi.getOneSelectedRow('#ActualsMonitoring', true);
        if (rowData) {
            var lPending = typeof rowData["PendingEntries"] === 'undefined' ? 0 : rowData["PendingEntries"];
            var lSegmentDescription = rowData["SegmentDescription"];
            var lSegmentID = rowData["SegmentId"];
            var lComments = typeof rowData["Comment"] === 'undefined' ? ' ' : ' ';
            var htmlContentModal = '';
            htmlContentModal += "<b>Do you want to save this record? </b> ";
            _showModal({
                modalId: "modalUpload",
                width: '70%',
                buttons: [{
                    name: "<i class='fa fa-save'></i> Save",
                    class: "btn-success",
                    closeModalOnClick: false,
                    onClick: function ($modal) {
                        //Add Comments
                        fnSaveMonitoringToDB(lSegmentDescription, lPending, lSegmentID, lComments, function () {
                            _showNotification("success", "Actuals Monitoring data was save successfully", "AlertReject");
                            //Close Modal
                            $modal.find(".close").click();
                        });
                    }
                }],
                addCloseButton: true,
                title: "Save Flash template",
                contentHtml: htmlContentModal,
                onReady: function ($modal) { }
            });
        } else {
            fnLoadFlashFile();
        }

    }
}
//Save data to db 
function fnSaveMonitoringToDB(MSDESCR, lPending, lSegmentID, lComments, onSuccess) {

    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Saving data...",
        name: "[dbo].[spFlashSaveActualsMonitoring]",
        params: [
            { "Name": "@pSegmentId", "Value": lSegmentID },
            { "Name": "@pMSDesccr", "Value": MSDESCR },
            { "Name": "@pPending", "Value": lPending },
            { "Name": "@pComments", "Value": lComments },
            { "Name": "@pMonth", "Value": getDate('m') },
            { "Name": "@pYear", "Value": getDate('y') },
            { "Name": "@pSessionSOEID", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                _showNotification("success", "Data was added successfully.", "AlertAddComment");
                onSuccess();
            }
        }
    });

}
//Show modal to save data COMMENTS
function showModalAddCommentEdit() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#ActualsMonitoring', true);
    if (rowData) {
        var lPending = typeof rowData["PendingEntries"] === 'undefined' ? 0 : rowData["PendingEntries"];
        var lSegmentDescription = rowData["SegmentDescription"];
        var lSegmentID = rowData["SegmentId"];
        var lComment = rowData["Comment"];
        if (lComment == 'undefined') {
            lComment = '';
        }
        var htmlContentModal = '';
        htmlContentModal += "<b>Description: </b>" + rowData["SegmentDescription"] + "<br/>";
        htmlContentModal += "<b>Pending: </b>" + lPending + "<br/>";
        htmlContentModal += "<b>Comments: </b><br/>";
        htmlContentModal += "<textarea class='form-control txtDetailComment' placeholder='(optional)' style='height:100px !important;'>" + lComment + "</textarea>";

        _showModal({
            modalId: "modalUpload",
            width: '70%',
            buttons: [{
                name: "<i class='fa fa-save'></i> Save",
                class: "btn-success",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    //Add Comments
                    lComment = $(".txtDetailComment").val();

                    addDetailComment(lSegmentDescription, lPending, lSegmentID, lComment, function () {
                        _showNotification("success", "Actuals monitoring data was save successfully", "AlertReject");
                        rowData["PendingEntries"] = lPending;
                        rowData["Comment"] = lComment;
                        $('#ActualsMonitoring').jqxGrid('updaterow', rowData.boundindex, rowData);
                        //Close Modal
                        $modal.find(".close").click();
                    });
                }
            }],
            addCloseButton: true,
            title: "Add Comment",
            contentHtml: htmlContentModal,
            onReady: function ($modal) { }
        });
    }

}
//Add data to db COMMENTS
function addDetailComment(lSegmentDescription, lPending, lSegmentID, Comment, onSuccess) {

    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Adding extra file comments...",
        name: "[dbo].[spFlashInsertActualsMonitoringComment]",
        params: [
            { "Name": "@pAction", "Value": "Insert" },
            { "Name": "@pSegmentId", "Value": lSegmentID },
            { "Name": "@pMSDesccr", "Value": lSegmentDescription },
            { "Name": "@pPending", "Value": lPending },
            { "Name": "@pComment", "Value": Comment },
            { "Name": "@pMonth", "Value": getDate('m') },
            { "Name": "@pYear", "Value": getDate('y') },
            { "Name": "@pSessionSOEID", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                //Return New ID
                _showNotification("success", "Data was added successfully.", "AlertAddComment");

                //$modal.find(".closeModal-modalUpload").click();
                //insertToFlashBridge();
                //
                onSuccess();
            }
        }
    });

}
_execOnAjaxComplete(function () {
    setTimeout(function () {
        $(".ProgressHolder").fadeOut(900, function () {
            _hideLoadingFullPage({
                idLoading: "FlashCurrentActual"
            });
        });
    }, 200);
});


