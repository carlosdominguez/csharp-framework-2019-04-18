﻿_showLoadingFullPage({
    msg: "Loading Delete Dashboard",
    idLoading: "FlashDeleteData"
});
$(document).ready(function () {
    $(".trigger-btn").click(function () {
        var deleteData = $(this).data('id');
        $(".modal-footer #deleteId").val(deleteData);
    })
    
    $("#deleteData").click(function () {
        var deleteData = document.getElementById('deleteId').value
        switch(deleteData) {
            case 'setup':
                console.log("setUp");
                fnDeleteFlashSetUpData();
                break;
            case 'daily':
                console.log("Daily");
                fnDeleteFlashDailyData();
                break;
            case 'elr':
                console.log("elr");
                fnDeleteELRData();
                break;
            case 'actuals':
                console.log("actuals");
                fnDeletePearlActuals()
                break;
            default:
                console.log("nothing to delete :)");
        }
    });
    
    $("#deleteDailyData").click(function () {
        fnDeleteFlashDailyData();
    });
})

function fnDeleteFlashSetUpData() {
    _showLoadingFullPage({
        msg: "Loading Delete Set up Data",
        idLoading: "FlashDeleteSetupData"
    });
    _callProcedure({
        loadingMsgType: "Deleting setup data",
        loadingMsg: "Loading...",
        name: "[dbo].[spFlashDeleteSetUpData]",
        params: [
            { "Name": "@vCurrentMonth", "Value": getDate('m') },
            { "Name": "@SOEIDUploader", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                console.log(responseList);
                _execOnAjaxComplete(function () {
                    setTimeout(function () {
                        $(".ProgressHolder").fadeOut(900, function () {
                            _hideLoadingFullPage({
                                idLoading: "FlashDeleteSetupData"
                            });
                        });
                    }, 200);
                });
                $("#btnSuccess").click();
            }
        },
    });
}

function fnDeleteFlashDailyData() {
    _showLoadingFullPage({
        msg: "Loading Delete Daily Data",
        idLoading: "FlashDeleteDailyData"
    });
    _callProcedure({
        loadingMsgType: "Deleting daily data",
        loadingMsg: "Loading...",
        name: "[dbo].[spFlashDeleteDataApsBaw]",
        params: [
            { "Name": "@vCurrentMonth", "Value": getDate('m') },
            { "Name": "@SOEIDUploader", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                _execOnAjaxComplete(function () {
                    setTimeout(function () {
                        $(".ProgressHolder").fadeOut(900, function () {
                            _hideLoadingFullPage({
                                idLoading: "FlashDeleteDailyData"
                            });
                        });
                    }, 200);
                });
                console.log(responseList);
                $("#btnSuccess").click();
            }
        },
    });
}

function fnLoadFlashDatesElrFile() {

    _callProcedure({
        loadingMsgType: "List ElR Dates",
        loadingMsg: "Loading...",
        name: "[dbo].[spFlashListDatesElrData]",
        params: [
            { "Name": "@vCurrentMonth", "Value": getDate('m') },
        ],
        success: {
            fn: function (response) {
                //fnCreateSelect(responseList);
                var len = response.length;

                $("#sel_user").empty();
                for( var i = 0; i<len; i++){
                    var id = response[i]['LoadDate'].split(" ");
                    var name = response[i]['LoadDate'];
                    
                    $("#sel_user").append("<option value='" + id[0] + "'>" + id[0] + "</option>");
                }
            }
        },
    });
   
}
fnLoadFlashDatesElrFile();
function fnDeleteELRData() {
    _showLoadingFullPage({
        msg: "Flash Delete ELR Data",
        idLoading: "FlashDeleteELRData"
    });
    var lValueSelected = $('#sel_user').val();
    console.log(lValueSelected);
    
    _callProcedure({
        loadingMsgType: "Deleting P2P ELR data",
        loadingMsg: "Loading...",
        name: "[dbo].[spFlashDeleteELRData]",
        params: [
            { "Name": "@vLoadDate", "Value": lValueSelected },
            { "Name": "@vCurrentMonth", "Value": getDate('m') },
            { "Name": "@SOEIDUploader", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                _execOnAjaxComplete(function () {
                    setTimeout(function () {
                        $(".ProgressHolder").fadeOut(900, function () {
                            _hideLoadingFullPage({
                                idLoading: "FlashDeleteELRData"
                            });
                        });
                    }, 200);
                });
                console.log(responseList);
                $("#btnSuccess").click();
            }
        },
    });

}
function fnLoadFlashDatePearlActuals() {

    _callProcedure({
        loadingMsgType: "List ElR Dates",
        loadingMsg: "Loading...",
        name: "[dbo].[spFlashListDatesPearlActualsData]",
        params: [
            { "Name": "@vCurrentMonth", "Value": getDate('m') },
        ],
        success: {
            fn: function (response) {
                //fnCreateSelect(responseList);
                var len = response.length;

                $("#pearlActualsDates").empty();
                for (var i = 0; i < len; i++) {
                    var id = response[i]['LoadDate'].split(" ");
                    var name = response[i]['LoadDate'];

                    $("#pearlActualsDates").append("<option value='" + id[0] + "'>" + id[0] + "</option>");
                }
            }
        },
    });
}
setTimeout(function () {
    fnLoadFlashDatePearlActuals();
}, 800);

function fnDeletePearlActuals() {
    _showLoadingFullPage({
        msg: "Flash Delete Pearl Actual Data",
        idLoading: "FlashDeletePearlData"
    });
    var lDateSelected = $('#pearlActualsDates').val();
    console.log(lDateSelected);
    _callProcedure({
        loadingMsgType: "Deleting Pearl Actuals data",
        loadingMsg: "Loading...",
        name: "[dbo].[spFlashDeletePearlActualsData]",
        params: [
            { "Name": "@vLoadDate", "Value": lDateSelected },
            { "Name": "@vCurrentMonth", "Value": getDate('m') },
            { "Name": "@SOEIDUploader", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                console.log(responseList);
                _execOnAjaxComplete(function () {
                    setTimeout(function () {
                        $(".ProgressHolder").fadeOut(900, function () {
                            _hideLoadingFullPage({
                                idLoading: "FlashDeletePearlData"
                            });
                        });
                    }, 200);
                });
                $("#btnSuccess").click();
            }
        },
    });
}

_execOnAjaxComplete(function () {
    setTimeout(function () {
        $(".ProgressHolder").fadeOut(900, function () {
            _hideLoadingFullPage({
                idLoading: "FlashDeleteData"
            });
        });
    }, 200);
});