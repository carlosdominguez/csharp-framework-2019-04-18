﻿var gBussinesMonth;
var gBussinesYear;


//Fn to get Date
function fnGetDate() {
    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();
    //if (dateObj.getDay() == 1) {
    //    lYesterday = day - 3;
    //    console.log(year + "-" + month + "-" + lYesterday);
    //    return gTodayDate = year + "-" + month + "-" + lYesterday;
    //}

    return gTodayDate = year + "-" + month + "-" + day;
}
function getDate(lVal, onSuccess) {
    var date = fnGetDate();
    var sql = "SELECT * FROM [dbo].[fnFlashGetMonthAndYearByCorporateCalendar] " + "('" + date + "')";
    var async = false;
    var result = "";

    if (onSuccess) {
        async = true;
    }

    //Load process information
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Processed date",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': _toJSON(sql) },
        type: "POST",
        async: async,
        success: function (resultList) {
            //console.log(resultList[0]);
            var html = '';

            $.each(resultList, function (key, value) {
                gBussinesMonth = value.Month;
                gBussinesYear = value.Year;

            });

            if (lVal == 'm') {
                result = gBussinesMonth
            }
            if (lVal == 'y') {
                result = gBussinesYear
            }

            if (onSuccess) {
                onSuccess(result);
            } 
            
        }
    });
   
    if (typeof onSuccess == "undefined") {
        return result;
    }
}

//FN to get date //yy or //mm
