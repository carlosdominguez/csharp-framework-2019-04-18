﻿/// <reference path="../../Shared/plugins/util/global.js" />

//Document ready
$(document).ready(function () {
    loadTableIncomplete();
});

var accessAdmin = "INCOME_AND_OTHER_ADMIN";
var accessSuperAdmin = "INCOME_AND_OTHER_SUPER_ADMIN";
var accessPreparer = "INCOME_AND_OTHER_DEFAULT_USER";
var accessReviewer = "INCOME_AND_OTHER_MANAGER";
var userAccess = _getViewVar("UserInfo").Roles.replace(",", "");



function returnButtons(row) {
    var listButtons = [
        {
            name: "Delete",
            class: "btn-danger",
            onClick: function ($modal) {
                if (userAccess == accessReviewer) {
                    if ($("#txtNewComments").val().length > 20) {
                        _callServer({
                            url: '/IncomeAndOther/ReviewerDelete',
                            data: { 'pjson': JSON.stringify(row), 'comments': $("#txtNewComments").val() },
                            type: "post",
                            success: function (savingStatus) {
                                _showNotification("success", "Data was saved successfully.");
                                reloadTablePrepare();
                            }
                        });
                    } else {
                        _showNotification("error", "Please enter a comment");
                    }
                } else {
                    _showNotification("error", "Only reviewers can delete filings");
                }

                //alert($modal.find("#txtlogID").val());
            }
        },
        {
            name: "Approve",
            class: "btn-success",
            onClick: function ($modal) {

                if (userAccess == accessReviewer || userAccess == accessSuperAdmin) {
                    if (row.statusPreparedSOEID.toUpperCase() != _getSOEID().toUpperCase()) {

                        _callServer({
                            url: '/IncomeAndOther/ReviewerSave',
                            data: { 'pjson': JSON.stringify(row), 'comments': $("#txtNewComments").val(), 'accounts': $("#spinner").val(), 'duedate': $("#txtfilingDueDate").val() },
                            type: "post",
                            success: function (savingStatus) {
                                _showNotification("success", "Data was saved successfully.");
                                reloadTablePrepare();
                                reloadTableCompleted();
                            }
                        });
                    } else {
                        _showNotification("error", "You cannot review something you prepared");
                    }

                } else {
                    _showNotification("error", "You don't have access to review filings");
                }
            }
        },
        {
            name: "Save Changes",
            class: "btn-info",
            onClick: function ($modal) {
                _callServer({
                    url: '/IncomeAndOther/ReviewerEdit',
                    data: { 'pjson': JSON.stringify(row), 'comments': $("#txtNewComments").val(), 'accounts': $("#spinner").val(), 'duedate': $("#txtfilingDueDate").val() },
                    type: "post",
                    success: function (savingStatus) {
                        _showNotification("success", "Data was saved successfully.");
                        reloadTablePrepare();
                    }
                });
                //alert($modal.find("#txtlogID").val());
            }
        }];


    removeButtonsPreparer(listButtons);

    return listButtons;
}




function removeButtonsPreparer(listButtons) {


    if (userAccess == "INCOME_AND_OTHER_DEFAULT_USER") {
        var searchTerm = "Delete",
        index = -1;
        for (var i = 0, len = listButtons.length; i < len; i++) {
            if (listButtons[i].name === searchTerm) {
                index = i;
                break;
            }
        }

        listButtons.splice(index, 1);

        var searchTerm1 = "Approve",
        index1 = -1;
        for (var i = 0, len = listButtons.length; i < len; i++) {
            if (listButtons[i].name === searchTerm1) {
                index1 = i;
                break;
            }
        }
        listButtons.splice(index1, 1);
    }

}

//#region Load tables by status
function loadTableIncomplete() {
    _showLoadingFullPage({ msg: "Loading Calendar..." });

    reloadTableIncomplete();
    reloadTablePrepare();
    reloadTableCompleted();
};

function reloadTableIncomplete() {
    $.jqxGridApi.create({
        showTo: "#tblCalendarIncomplete",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_CalendarListingByMonthYear]",
            Params: [
                { Name: "@year", Value: new Date().getFullYear() },
                { Name: "@month", Value: "1,2,3,4,5,6,7,8,9,10,11,12" },
                { Name: "@statusPrepared", Value: 0 },
                { Name: "@status", Value: 0 }
            ]
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'logID', type: 'string', text:'Log ID', width: '10%' },
            { name: 'entityID', type: 'string', hidden: true },
            { name: 'entityName', type: 'string', text: 'Entity Name', width: '25%' },
            { name: 'function', type: 'string', width: '15%' },
            { name: 'frequency', type: 'string', width: '10%' },
            { name: 'reviewer', type: 'string', width: '10%' },
            { name: 'preparer', type: 'string', width: '10%' },
            { name: 'filingDueDate', text: 'Filing Due Date', type: 'date', width: '20%', cellsformat: "M/d/yyyy" },
            { name: 'filingID', type: 'string', hidden: true },
            { name: 'status', type: 'string', hidden: true },
            { name: 'statusPrepared', type: 'string', hidden: true },
            { name: 'statusDesc', type: 'string', hidden: true },
            { name: 'preparedDdate', text: 'Prepared Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'reviewedDate', text: 'Reviewed Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'commentFlag', type: 'string', hidden: true },
            { name: 'counts', type: 'string', hidden: true },
            { name: 'statusDate', text: 'Status Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'statusSOEID', type: 'string', hidden: true },
            { name: 'statusPreparedDate', text: 'Status Prepared Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'statusPreparedSOEID', type: 'string', hidden: true },
            { name: 'comments', type: 'string', hidden: true },
            { name: 'ttiCode', type: 'string', hidden: true },
            { name: 'EIN', type: 'string', hidden: true },
            { name: 'state', type: 'string', hidden: true },
            { name: 'city', type: 'string', hidden: true },
            { name: 'branch', type: 'string', hidden: true },
            { name: 'dueDay', type: 'string', hidden: true },
            { name: 'dueMonth', type: 'string', hidden: true }
        ],

        ready: function () {
            _hideLoadingFullPage();
            $('#tblCalendarIncomplete').on('rowdoubleclick', function (event) {
                // event.args.rowindex is a bound index.
                if (typeof event.args.group == "undefined") {
                    var row = event.args.rowindex;
                    var datarow = $("#tblCalendarIncomplete").jqxGrid('getrowdata', row);
                    showIncompleModal(datarow);
                }
            });
        }
    });
}

function reloadTablePrepare() {
    $.jqxGridApi.create({
        showTo: "#tblCalendarPrepared",
        options: {
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_CalendarListingByMonthYear]",
            Params: [
                { Name: "@year", Value: new Date().getFullYear() },
                { Name: "@month", Value: "1,2,3,4,5,6,7,8,9,10,11,12" },
                { Name: "@statusPrepared", Value: 1 },
                { Name: "@status", Value: 0 }
            ]
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'logID', type: 'string', text: 'Log ID', width: '10%' },
            { name: 'entityID', type: 'string', hidden: true },
            { name: 'entityName', type: 'string', text: 'Entity Name', width: '25%' },
            { name: 'function', type: 'string', width: '15%' },
            { name: 'frequency', type: 'string', width: '10%' },
            { name: 'reviewer', type: 'string', width: '10%' },
            { name: 'preparer', type: 'string', width: '10%' },
            { name: 'filingDueDate', text: 'Filing Due Date', type: 'date', width: '20%', cellsformat: "M/d/yyyy" },
            { name: 'filingID', type: 'string', hidden: true },
            { name: 'status', type: 'string', hidden: true },
            { name: 'statusPrepared', type: 'string', hidden: true },
            { name: 'statusDesc', type: 'string', hidden: true },
            { name: 'preparedDdate', text: 'Prepared Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'reviewedDate', text: 'Reviewed Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'commentFlag', type: 'string', hidden: true },
            { name: 'counts', type: 'string', hidden: true },
            { name: 'statusDate', text: 'Status Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'statusSOEID', type: 'string', hidden: true },
            { name: 'statusPreparedDate', text: 'Status Prepared Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'statusPreparedSOEID', type: 'string', hidden: true },
            { name: 'comments', type: 'string', hidden: true },
            { name: 'ttiCode', type: 'string', hidden: true },
            { name: 'EIN', type: 'string', hidden: true },
            { name: 'state', type: 'string', hidden: true },
            { name: 'city', type: 'string', hidden: true },
            { name: 'branch', type: 'string', hidden: true },
            { name: 'dueDay', type: 'string', hidden: true },
            { name: 'dueMonth', type: 'string', hidden: true }
        ],
        ready: function () {
            _hideLoadingFullPage();
            $('#tblCalendarPrepared').on('rowdoubleclick', function (event) {
                // event.args.rowindex is a bound index.
                if (typeof event.args.group == "undefined") {
                    var row = event.args.rowindex;
                    var datarow = $("#tblCalendarPrepared").jqxGrid('getrowdata', row);
                    showPrepareModal(datarow);
                }
            });
        }
    });
}

function reloadTableCompleted() {
    $.jqxGridApi.create({
        showTo: "#tblCalendarComplete",
        options: {
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_CalendarListingByMonthYear]",
            Params: [
                { Name: "@year", Value: new Date().getFullYear() },
                { Name: "@month", Value: "1,2,3,4,5,6,7,8,9,10,11,12" },
                { Name: "@statusPrepared", Value: 1 },
                { Name: "@status", Value: 1 }
            ]
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'logID', type: 'string', text: 'Log ID', width: '10%' },
            { name: 'entityID', type: 'string', hidden: true },
            { name: 'entityName', type: 'string', text: 'Entity Name', width: '25%' },
            { name: 'function', type: 'string', width: '15%' },
            { name: 'frequency', type: 'string', width: '10%' },
            { name: 'reviewer', type: 'string', width: '10%' },
            { name: 'preparer', type: 'string', width: '10%' },
            { name: 'filingDueDate', text: 'Filing Due Date', type: 'date', width: '20%', cellsformat: "M/d/yyyy" },
            { name: 'filingID', type: 'string', hidden: true },
            { name: 'status', type: 'string', hidden: true },
            { name: 'statusPrepared', type: 'string', hidden: true },
            { name: 'statusDesc', type: 'string', hidden: true },
            { name: 'preparedDdate', text: 'Prepared Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'reviewedDate', text: 'Reviewed Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'commentFlag', type: 'string', hidden: true },
            { name: 'counts', type: 'string', hidden: true },
            { name: 'statusDate', text: 'Status Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'statusSOEID', type: 'string', hidden: true },
            { name: 'statusPreparedDate', text: 'Status Prepared Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'statusPreparedSOEID', type: 'string', hidden: true },
            { name: 'comments', type: 'string', hidden: true },
            { name: 'ttiCode', type: 'string', hidden: true },
            { name: 'EIN', type: 'string', hidden: true },
            { name: 'state', type: 'string', hidden: true },
            { name: 'city', type: 'string', hidden: true },
            { name: 'branch', type: 'string', hidden: true },
            { name: 'dueDay', type: 'string', hidden: true },
            { name: 'dueMonth', type: 'string', hidden: true }
        ],
        ready: function () {
            _hideLoadingFullPage()
            $('#tblCalendarComplete').on('rowdoubleclick', function (event) {
                if (typeof event.args.group == "undefined") {
                    // event.args.rowindex is a bound index.
                    var row = event.args.rowindex;
                    var datarow = $("#tblCalendarComplete").jqxGrid('getrowdata', row);
                    showCompleteModal(datarow);
                }
            });
        }
    });
}

//#endregion Load tables by status

//#region Click tables

//Incomplete
function showIncompleModal(row) {
    //var row = $.jqxGridApi.getOneSelectedRow("#tblCalendarIncomplete", true);


    _showModal({
        title: "<h3>Filing Calendar - Log ID:" + row.logID + " - Entity: " + row.entityName + "</h3>",
        width: "98%",
        contentAjaxUrl: "/IncomeAndOther/ModalTemplate",
        contentAjaxParams: {
            logID: row.logID,
            entityID: row.entityID,
            entityName: row.entityName,
            function1: row.function,
            frequency: row.frequency,
            reviewer: row.reviewer,
            preparer: row.preparer,
            filingDueDate: _formatDate(new Date(row.filingDueDate), 'MM/dd/yyyy'),
            filingID: row.filingID,
            status: row.status,
            statusPrepared: row.statusPrepared,
            statusDesc: row.statusDesc,
            preparedDdate: _formatDate(new Date(row.preparedDdate), 'MM/dd/yyyy'),
            reviewedDate: _formatDate(new Date(row.reviewedDate), 'MM/dd/yyyy'),
            commentFlag: row.commentFlag,
            counts: row.counts,
            statusDate: _formatDate(new Date(row.statusDate), 'MM/dd/yyyy'),
            statusSOEID: row.statusSOEID,
            statusPreparedDate: _formatDate(new Date(row.statusPreparedDate), 'MM/dd/yyyy'),
            statusPreparedSOEID: row.statusPreparedSOEID,
            comments: row.comments,
            ttiCode: row.ttiCode,
            EIN: row.EIN,
            state: row.state,
            city: row.city,
            branch: row.branch,
            dueDay: row.dueDay,
            dueMonth: row.dueMonth
        },
        buttons: [{
            name: "Delete",
            class: "btn-danger",
            onClick: function ($modal) {
                if (userAccess == accessReviewer) {
                    if ($("#txtNewComments").val().length > 20) {
                        _callServer({
                            url: '/IncomeAndOther/PreparerDelete',
                            data: { 'pjson': JSON.stringify(row), 'comments': $("#txtNewComments").val() },
                            type: "post",
                            success: function (savingStatus) {
                                _showNotification("success", "Data was saved successfully.");
                                reloadTableIncomplete();
                            }
                        });
                    } else {
                        _showNotification("error", "Please enter a comment");
                    }
                } else {
                    _showNotification("error", "Only reviewers can delete filings");
                }


            }
        },
        {
            name: "Prepare",
            class: "btn-success",
            onClick: function ($modal) {
                if (userAccess == accessPreparer || userAccess == accessReviewer || userAccess == accessSuperAdmin) {
                    _callServer({
                        url: '/IncomeAndOther/PreparerSave',
                        data: { 'pjson': JSON.stringify(row), 'comments': $("#txtNewComments").val() },
                        type: "post",
                        success: function (savingStatus) {
                            _showNotification("success", "Data was saved successfully.");
                            reloadTableIncomplete();
                            reloadTablePrepare();
                        }
                    });
                } else {
                    _showNotification("error", "You don't have access to prepare.");
                }
            }
        },

        {
            name: "Save Changes",
            class: "btn-info",
            onClick: function ($modal) {
                _callServer({
                    url: '/IncomeAndOther/PreparerEdit',
                    data: { 'pjson': JSON.stringify(row), 'comments': $("#txtNewComments").val(), 'accounts': $("#spinner").val() },
                    type: "post",
                    success: function (savingStatus) {
                        _showNotification("success", "Data was saved successfully.");
                        reloadTableIncomplete();
                    }
                });
                //alert($modal.find("#txtlogID").val());
            }
        }],
        onReady: function ($modal) {
            loadDropDownGlobalProcess(row.function);
            $modal.find("#txtfilingDueDate").prop('disabled', true);


            $modal.find("#spinner").spinner({
                min: 0,
                max: 500,
                step: 1,
                start: 1000,
                numberFormat: "C"
            });
            //$modal.find("#spinner").val("20");


            //GENERATE SLIDER
            //_GLOBAL_SETTINGS.initSlider();

            //$("#slider").slider({
            //    orientation: "horizontal",
            //    range: "min",
            //    max: 255,
            //    value: 127,
            //    slide: function () {
            //        console.log("Slider event", $("#slider").slider("value"));
            //    },
            //    change: function () {
            //        console.log("Change event", $("#slider").slider("value"));
            //    }
            //});
            //$("#slider").slider("value", 235);

        }
    });


}
//Prepared
function showPrepareModal(row) {
    // var row = $.jqxGridApi.getOneSelectedRow("#tblCalendarPrepared", true);
    _showModal({
        title: "<h3>Filing Calendar - Log ID:" + row.logID + " - Entity: " + row.entityName + "</h3>",
        width: "98%",
        contentAjaxUrl: "/IncomeAndOther/ModalTemplate",
        contentAjaxParams: {
            logID: row.logID,
            entityID: row.entityID,
            entityName: row.entityName,
            function1: row.function,
            frequency: row.frequency,
            reviewer: row.reviewer,
            preparer: row.preparer,
            filingDueDate: _formatDate(new Date(row.filingDueDate), 'MM/dd/yyyy'),
            filingID: row.filingID,
            status: row.status,
            statusPrepared: row.statusPrepared,
            statusDesc: row.statusDesc,
            preparedDdate: _formatDate(new Date(row.preparedDdate), 'MM/dd/yyyy'),
            reviewedDate: _formatDate(new Date(row.reviewedDate), 'MM/dd/yyyy'),
            commentFlag: row.commentFlag,
            counts: row.counts,
            statusDate: _formatDate(new Date(row.statusDate), 'MM/dd/yyyy'),
            statusSOEID: row.statusSOEID,
            statusPreparedDate: _formatDate(new Date(row.statusPreparedDate), 'MM/dd/yyyy'),
            statusPreparedSOEID: row.statusPreparedSOEID,
            comments: row.comments,
            ttiCode: row.ttiCode,
            EIN: row.EIN,
            state: row.state,
            city: row.city,
            branch: row.branch,
            dueDay: row.dueDay,
            dueMonth: row.dueMonth
        },
        buttons: returnButtons(row),
        onReady: function ($modal) {
            loadDropDownGlobalProcess(row.function);
            $modal.find("#spinner").spinner({
                min: 0,
                max: 500,
                step: 1,
                start: 1000,
                numberFormat: "C"
            });
        }
    });
}
//Completed
function showCompleteModal(row) {
    //var row = $.jqxGridApi.getOneSelectedRow("#tblCalendarComplete", true);
    _showModal({
        title: "<h3>Filing Calendar - Log ID:" + row.logID + " - Entity: " + row.entityName + "</h3>",
        width: "98%",
        contentAjaxUrl: "/IncomeAndOther/ModalTemplate",
        contentAjaxParams: {
            logID: row.logID,
            entityID: row.entityID,
            entityName: row.entityName,
            function1: row.function,
            frequency: row.frequency,
            reviewer: row.reviewer,
            preparer: row.preparer,
            filingDueDate: _formatDate(new Date(row.filingDueDate), 'MM/dd/yyyy'),
            filingID: row.filingID,
            status: row.status,
            statusPrepared: row.statusPrepared,
            statusDesc: row.statusDesc,
            preparedDdate: _formatDate(new Date(row.preparedDdate), 'MM/dd/yyyy'),
            reviewedDate: _formatDate(new Date(row.reviewedDate), 'MM/dd/yyyy'),
            commentFlag: row.commentFlag,
            counts: row.counts,
            statusDate: _formatDate(new Date(row.statusDate), 'MM/dd/yyyy'),
            statusSOEID: row.statusSOEID,
            statusPreparedDate: _formatDate(new Date(row.statusPreparedDate), 'MM/dd/yyyy'),
            statusPreparedSOEID: row.statusPreparedSOEID,
            comments: row.comments,
            ttiCode: row.ttiCode,
            EIN: row.EIN,
            state: row.state,
            city: row.city,
            branch: row.branch,
            dueDay: row.dueDay,
            dueMonth: row.dueMonth
        },
        buttons: [{
            name: "Save Changes",
            class: "btn-info",
            onClick: function ($modal) {
                _callServer({
                    url: '/IncomeAndOther/CompletedEdit',
                    data: { 'pjson': JSON.stringify(row), 'comments': $("#txtNewComments").val() },
                    type: "post",
                    success: function (savingStatus) {
                        _showNotification("success", "Data was saved successfully.");
                        reloadTableCompleted();
                    }
                });
                //alert($modal.find("#txtlogID").val());
            }
        }],
        onReady: function ($modal) {
            loadDropDownGlobalProcess(row.function);
            $modal.find("#txtfilingDueDate").prop('disabled', true);
            $modal.find("#txtaccounts").prop('disabled', true);

            $modal.find("#spinner").spinner({
                min: 0,
                max: 500,
                step: 1,
                start: 1000,
                numberFormat: "C"
            });
            // $modal.find("#spinner").val("20");
        }
    });
}

//#endregion Double click tables

//#region Show loading message
function loadTableComplete() {
    _showLoadingFullPage({ msg: "Loading Calendar..." });
};
//#endregion Show loading message

//#region Populate function type droplist
function loadDropDownGlobalProcess(selectedFunction) {
    //Set default value  
    _callServer({
        loadingMsg: "Loading Global Process...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT '0' AS [funcID], '-- Select --' AS [function] UNION SELECT REPLACE(CAST([funcID] AS VARCHAR(36)),'-','') as [funcID],[function] FROM [IncomeAndOtherQD].[dbo].[FunctionType] where active = 1 ORDER BY [function] ASC") },
        type: "post",
        success: function (resultList) {
            $("#ddlFunction").contents().remove();

            //Render Global Process Options
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $("#ddlFunction").append($('<option>', { value: objFunction.funcID, text: objFunction.function, selected: (objFunction.function.toLowerCase() == selectedFunction.toLowerCase()) }));
            }

            //if (params.fnSuccess) {
            //    params.fnSuccess(resultList, selectedFunction);
            //}
        }
    });
};
//#endregion Populate function type droplist

//#region Validate User
function ValidateUser() {
    alert(_getSOEID());

};
//#endregion Validate User