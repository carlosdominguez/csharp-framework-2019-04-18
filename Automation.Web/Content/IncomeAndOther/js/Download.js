﻿//Document ready
$(document).ready(function () {
    loadTableDownloads();
});



function loadTableDownloads() {
    $.jqxGridApi.create({
        showTo: "#tblFilesDownload",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_TheFirmFiles]",
            Params: [
                { Name: "@soeid", Value: "data" }
            ]
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'documentID', type: 'string',  hidden: true},
            { name: 'docLocation', type: 'string', hidden: true },
            { name: 'docName', type: 'string', width: '40%' },
            { name: 'expirationDate', type: 'string', width: '30%' },
            { name: 'documentTypeDescription', type: 'string', width: '30%' }
           
        ],

        ready: function () {
            _hideLoadingFullPage();
            $('#tblFilesDownload').on('rowclick', function (event) {
                // event.args.rowindex is a bound index.
                if (typeof event.args.group == "undefined") {
                    var row = event.args.rowindex;
                    var datarow = $("#tblFilesDownload").jqxGrid('getrowdata', row);


                    $('#linkFile').attr("href", datarow.docLocation + "\\" + datarow.docName);



                }
            });
        }
    });
}


//#region Show loading message
function loadTableComplete() {
    _showLoadingFullPage({ msg: "Loading Calendar..." });
};
//#endregion Show loading message


$("#linkFile").click(function (e) {

    var getselectedrowindexes = $('#tblFilesDownload').jqxGrid('getselectedrowindexes');
    var selectedRowData = $('#tblFilesDownload').jqxGrid('getrowdata', getselectedrowindexes[0]);

    var $btn = $(this);

    e.preventDefault();

    _callServer({
        url: '/IncomeAndOther/DownloadPDF',
        data: {
            'thefirmpath': selectedRowData.docLocation,
            'fileName': selectedRowData.docName
        },
        type: "post",
        success: function (urlToDownload) {
            window.open(_replaceAll("~", "", urlToDownload), '_blank');
        }
    });
    
});

