﻿
//#region DOCUMENT READY
$(document).ready(function () {
    $("#newDateInput").jqxDateTimeInput({ template: "summer", width: "100%", height: 34, formatString: "MM/dd/yyyy" });
    loadDropDownlistsNewPreparer();
    loadDropDownlistsNewReviewer();

});
//#endregion

//#region CLICK LOCK
$("#btnLock").click(function () {
    //$("#btnSearch").prop('disabled', true);
    //var disabled = $("#btnSearch").is(":disabled");
    //alert(disabled);




});
//#endregion

//#region CLICK UPDATE
$("#btnUpdate").click(function () {

    var resultIDs = " WHERE filingID IN (";
    var rows = $('#tblFilingSearch').jqxGrid('getrows');
    var arrayLength = rows.length;

    var date = $("#newDateInput").jqxDateTimeInput('getDate');
    var formattedDate = $.jqx.dataFormat.formatdate(date, 'd');


    for (var i = 0; i < arrayLength; i++) {
        resultIDs += "'" + rows[i].filingID + "'" + ",";
    }

    resultIDs = resultIDs.substring(0, resultIDs.length - 1);
    resultIDs += ");";

    _callServer({
        url: '/IncomeAndOther/UpdateFilingMass',
        data: {
            'listFilingIDs': resultIDs,
            //'newReviewerSOEID': $("#ddlNewReviewer :selected").val(),
            //'newPreparerSOEID': $("#ddlNewPreparer :selected").val(),
            'newFilingDueDate': formattedDate,
            'firstValues': rows[0].filingDueDate,
            'firstFilingID': rows[0].filingID
        },
        type: "post",
        success: function (savingStatus) {
            _showNotification("success", "Data was saved successfully.");
            loadTableSearch();
         
        }
    });

});


//#region CLICK DELETE
$("#btnDelete").click(function () {

    var resultIDs = " WHERE filingID IN (";
    var rows = $('#tblFilingSearch').jqxGrid('getrows');
    var arrayLength = rows.length;

    var date = $("#newDateInput").jqxDateTimeInput('getDate');
    var formattedDate = $.jqx.dataFormat.formatdate(date, 'd');


    for (var i = 0; i < arrayLength; i++) {
        resultIDs += "'" + rows[i].filingID + "'" + ",";
    }

    resultIDs = resultIDs.substring(0, resultIDs.length - 1);
    resultIDs += ");";

    _callServer({
        url: '/IncomeAndOther/DeleteFilingMass',
        data: {
            'listFilingIDs': resultIDs
        },
        type: "post",
        success: function (savingStatus) {
            _showNotification("success", "Data was saved successfully.");
            loadTableSearch();

        }
    });

});




//#region LOAD DROPLISTS
function loadDropDownlistsNewPreparer() {
    _showLoadingFullPage({ msg: "Loading Preparers..." });
    _callServer({
        loadingMsg: "Loading Data ...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT  '0' as SOEID,  '--Select--' as Name  UNION SELECT DISTINCT [SOEID] as SOEID ,[Name] as Name FROM [dbo].[tblAutomation_User] WHERE Roles = 'INCOME_AND_OTHER_DEFAULT_USER' order by Name") },
        type: "post",
        success: function (resultList) {
            $("#ddlNewPreparer").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $("#ddlNewPreparer").append($('<option>', { value: objFunction.SOEID, text: objFunction.Name, selected: (objFunction.Name == "0") }));
            }

            _hideLoadingFullPage();
        }
    });
};

function loadDropDownlistsNewReviewer() {
    _showLoadingFullPage({ msg: "Loading Reviewers..." });
    _callServer({
        loadingMsg: "Loading Data ...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT '0' as SOEID, '--Select--'  as Name  UNION SELECT DISTINCT [SOEID] as SOEID ,[Name] as Name FROM [dbo].[tblAutomation_User] WHERE Roles = 'INCOME_AND_OTHER_MANAGER' order by Name") },
        type: "post",
        success: function (resultList) {
            $("#ddlNewReviewer").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $("#ddlNewReviewer").append($('<option>', { value: objFunction.SOEID, text: objFunction.Name, selected: (objFunction.Name == "0") }));
            }
            _hideLoadingFullPage();
        }
    });
};
//#endregion