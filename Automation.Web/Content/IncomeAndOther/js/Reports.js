﻿
loadDropDownReports();
loadDropDownCalendarType();


//#region DOCUMENT READY
$(document).ready(function () {
    $("#dateInputFrom").jqxDateTimeInput({ template: "summer", width: "100%", height: 34, formatString: "MM/dd/yyyy" });
    $("#dateInputTo").jqxDateTimeInput({ template: "summer", width: "100%", height: 34, formatString: "MM/dd/yyyy" });
});
//#endregion


//Download Excel
$("#btnExportExcel").click(function () {
    downloadQueryToExcel();
});


function downloadQueryToExcel() {

    var dateFrom = $("#dateInputFrom").jqxDateTimeInput('getDate');
    var formattedDateFrom = $.jqx.dataFormat.formatdate(dateFrom, 'd');

    var dateTo = $("#dateInputTo").jqxDateTimeInput('getDate');
    var formattedDateTo = $.jqx.dataFormat.formatdate(dateTo, 'd');

    var storeProcedure = $("#ddlReport :selected").val();

    var calendarType = $("#ddlCalendarType :selected").val();

    var reportName = $("#ddlReport :selected").text();
    var sql = "";

    if (storeProcedure == "[dbo].[NSP_Reports_AuditTrail] ") {
        sql = storeProcedure + " '" + formattedDateFrom + "', '" + formattedDateTo + "'";
    } else {
        sql = storeProcedure + " '" + formattedDateFrom + "', '" + formattedDateTo + "', '" + calendarType + "'";
    }


        storeProcedure + " '" + formattedDateFrom + "', '" + formattedDateTo + "', '" + calendarType + "'";

    _downloadExcel({
        sql: sql,
        filename: _createCustomID() + "_Income&Other_Report_" + reportName + ".xls",
        success: {
            msg: "Generating report... Please Wait, this operation may take some time to complete."
        }
    });
};



function loadDropDownReports() {
    //Set default value  
    _callServer({
        loadingMsg: "Loading Reports...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT * FROM (SELECT 'All' AS [reportSP], '-- Select --' AS [reportName] UNION SELECT [reportSP],[reportName] FROM [dbo].[Reports]) as a order by reportName") },
        type: "post",
        success: function (resultList) {
            $("#ddlReport").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $("#ddlReport").append($('<option>', { value: objFunction.reportSP, text: objFunction.reportName, selected: (objFunction.reportSP == "0") }));
            }
        }
    });
};

function loadDropDownCalendarType() {
    //Set default value  
    _callServer({
        loadingMsg: "Loading Reports...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT  ' All' as ID, ' All' as VAL  UNION SELECT  'Default' as ID, 'Default' as VAL  UNION SELECT  'TrustReturnPackage' as ID, 'TrustReturnPackage' as VAL UNION SELECT  'AppointmentsPackage' as ID, 'AppointmentsPackage' as VAL  UNION SELECT  'TaxPackage' as ID, 'TaxPackage' as VAL") },
        type: "post",
        success: function (resultList) {
            $("#ddlCalendarType").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $("#ddlCalendarType").append($('<option>', { value: objFunction.VAL, text: objFunction.ID, selected: (objFunction.VAL == "0") }));
            }
        }
    });
};