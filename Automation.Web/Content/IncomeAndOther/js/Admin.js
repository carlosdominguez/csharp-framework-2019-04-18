﻿//#region DOCUMENT READY
$(document).ready(function () {
    loadAllTable();
    // initialize jqxGrid
    $("#jqxdropdownbutton").jqxDropDownButton({ template: "summer", width: "100%", height: 30 });
    $("#dateInput").jqxDateTimeInput({ template: "summer", width: "100%", height: 30, formatString: "MM/dd/yyyy" });

    $("#dueDateCorpTax").jqxDateTimeInput({ template: "summer", width: "100%", height: 30, formatString: "MM/dd/yyyy" });

    $("#divYEUpdates").hide();
    

});
//#endregion

//#region LOAD MESSAGE
function loadAllTable() {
    _showLoadingFullPage({ msg: "Loading Admin Data..." });
    loadTableEntityMaster();
    loadTableFunctionType();
    loadTableAuditTrail();
    loadDropDownEntities();
    loadDropDownEntities1();
    loadTableBusinessCalendar();
};
//#endregion

//#region LOAD ALL TABLES
function loadTableComplete() {
    _showLoadingFullPage({ msg: "Loading Calendar..." });
};
//#endregion

//#region USER VARIABLES
var accessAdmin = "INCOME_AND_OTHER_ADMIN";
var accessSuperAdmin = "INCOME_AND_OTHER_SUPER_ADMIN";
var accessPreparer = "INCOME_AND_OTHER_DEFAULT_USER";
var accessReviewer = "INCOME_AND_OTHER_MANAGER";
var userAccess = _getViewVar("UserInfo").Roles.replace(",", "");
//#endregion

//#region VALIDATE USER
function ValidateUser() {
    alert(_getSOEID());

};
//#endregion

//#region ENTITY
function loadTableEntityMaster() {
    $.jqxGridApi.create({
        showTo: "#tblEntityMaster",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_EntityMasterListing]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
			{ name: 'logID', type: 'string', width: '10%' },
			{ name: 'entityID', type: 'string', hidden: true },
			{ name: 'entityName', type: 'string', width: '20%' },
			{ name: 'function', type: 'string', width: '10%' },
			{ name: 'frequency', type: 'string', width: '10%' },
			{ name: 'dueDay', type: 'string', width: '10%' },
			{ name: 'dueMonth', type: 'string', width: '10%' },
			{ name: 'active', type: 'string', width: '10%' },
			{ name: 'counts', type: 'string', width: '10%' },
			{ name: 'ttiCode', type: 'string', width: '10%' }
        ],

        ready: function () {
            _hideLoadingFullPage();
            $('#tblEntityMaster').on('rowdoubleclick', function (event) {
                // event.args.rowindex is a bound index.
                if (typeof event.args.group == "undefined") {
                    var row = event.args.rowindex;
                    var datarow = $("#tblEntityMaster").jqxGrid('getrowdata', row);
                    showEntitiMasterModal(datarow);
                }
            });
        }
    });
}

//Entities Master
function showEntitiMasterModal(row) {
    if (row == "NewEntity") {
        _showModal({
            title: "<h3>Entity Master</h3>",
            width: "82%",
            contentAjaxUrl: "/IncomeAndOther/ModalEntities",
            buttons: [
            {
                name: "Save",
                class: "btn-info",
                closeModalOnClick: true,
                onClick: function ($modal) {

                    //get data from modal
                    var myObject = new Object();
                    myObject.logID = $modal.find("#txtlogID").val();
                    myObject.entityName = $modal.find("#txtentityName").val();
                    myObject.function = $modal.find("#ddlFunction :selected").text();
                    myObject.frequency = $modal.find("#ddlFrequency :selected").text();
                    myObject.dueDay = $modal.find("#txtdueDay").val();
                    myObject.dueMonth = $modal.find("#txtdueMonth").val();
                    myObject.active = $modal.find("#ddlStatus").val();
                    myObject.counts = $modal.find("#txtCounts").val();
                    myObject.ttiCode = $modal.find("#txtttiCode").val();

                    var entityNew = JSON.stringify(myObject);


                    ///////////////////
                    if ($modal.find("#ddlFunction :selected").text() != "-- Select --" && $modal.find("#ddlStatus").val() != "NA"
                        && $modal.find("#txtlogID").val() != "" && $modal.find("#txtentityName").val() != ""
                        && $modal.find("#txtdueDay").val() != "" && $modal.find("#txtdueMonth").val() != ""
                        && $modal.find("#ddlFrequency :selected").text() != "-- Select --") {
                        _callServer({
                            url: '/IncomeAndOther/EntityEdit',
                            closeModalOnClick: false,
                            data: { 'pjson': entityNew, 'entityid': "NewEntity" },
                            type: "post",
                            success: function (savingStatus) {
                                loadTableEntityMaster();
                                _showNotification("success", "Data was saved successfully.");
                            }
                        });
                    } else {
                        _showNotification("error", "Please select a function or status.");
                    }
                }
            }],
            onReady: function ($modal) {
                loadDropDownFunction("-- Select --");
                loadDropDownEntityStatus("NA");
                loadDropDownFrequency("-- Select --");

            }
        });
    } else {
        _showModal({
            title: "<h3>Entity Master</h3>",
            width: "82%",
            contentAjaxUrl: "/IncomeAndOther/ModalEntities",
            contentAjaxParams: {
                logID: row.logID,
                entityID: row.entityID,
                entityName: row.entityName,
                function: row.function,
                frequency: row.frequency,
                dueDay: row.dueDay,
                dueMonth: row.dueMonth,
                active: row.active,
                counts: row.counts,
                preparer: row.preparer,
                reviewer: row.reviewer,
                ttiCode: row.ttiCode,
                EIN: row.EIN,
                state: row.state,
                city: row.city,
                branch: row.branch
            },
            buttons: [
            {
                name: "Save",
                class: "btn-info",
                closeModalOnClick: true,
                onClick: function ($modal) {

                    //get data from modal
                    var myObject = new Object();
                    myObject.logID = $modal.find("#txtlogID").val();
                    myObject.entityName = $modal.find("#txtentityName").val();
                    myObject.function = $modal.find("#ddlFunction :selected").text();
                    myObject.frequency = $modal.find("#ddlFrequency :selected").text();
                    myObject.dueDay = $modal.find("#txtdueDay").val();
                    myObject.dueMonth = $modal.find("#txtdueMonth").val();
                    myObject.active = $modal.find("#ddlStatus").val();
                    myObject.counts = $modal.find("#txtCounts").val();
                    myObject.preparer = $modal.find("#txtpreparer").val();
                    myObject.reviewer = $modal.find("#txtreviewer").val();
                    myObject.ttiCode = $modal.find("#txtttiCode").val();
                    //myObject.EIN = $modal.find("#txtEIN").val();
                    //myObject.state = $modal.find("#txtstate").val();
                    //myObject.city = $modal.find("#txtcity").val();
                    //myObject.branch = $modal.find("#txtbranch").val();

                    var entityNew = JSON.stringify(myObject);


                    ///////////////////
                    if ($modal.find("#ddlFunction :selected").text() != "-- Select --" && $modal.find("#ddlStatus").val() != "NA") {
                        _callServer({
                            url: '/IncomeAndOther/EntityEdit',
                            closeModalOnClick: false,
                            data: { 'pjson': entityNew, 'entityid': row.entityID },
                            type: "post",
                            success: function (savingStatus) {
                                loadTableEntityMaster();
                                _showNotification("success", "Data was saved successfully.");
                            }
                        });
                    } else {
                        _showNotification("error", "Please select a function or status.");
                    }
                }
            }],
            onReady: function ($modal) {
                loadDropDownFunction(row.function);
                loadDropDownEntityStatus(row.active);
                loadDropDownFrequency(row.frequency);
                $modal.find("#txtlogID").attr('disabled', 'disabled');
                $modal.find("#ddlFunction").attr('disabled', 'disabled');
            }
        });
    }

}

function loadDropDownFunction(selectedFunction) {
    //Set default value  
    _callServer({
        loadingMsg: "Loading Global Process...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT '0' AS [funcID], '-- Select --' AS [function] UNION SELECT REPLACE(CAST([funcID] AS VARCHAR(36)),'-','') as [funcID],[function] FROM [IncomeAndOtherQD].[dbo].[FunctionType] where active = 1 ORDER BY [function] ASC") },
        type: "post",
        success: function (resultList) {
            $("#ddlFunction").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $("#ddlFunction").append($('<option>', { value: objFunction.funcID, text: objFunction.function, selected: (objFunction.function.toLowerCase() == selectedFunction.toLowerCase()) }));
            }
        }
    });
};

function loadDropDownEntityStatus(status) {
    //Set default value  
    _callServer({
        loadingMsg: "Loading Global Process...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("select 'NA' as [value] , '-- Select --' as [status] UNION select 'True' as [value],'Active' as [status] UNION select 'False' as [value], 'Inactive' as [status]") },
        type: "post",
        success: function (resultList) {
            $("#ddlStatus").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $("#ddlStatus").append($('<option>', { value: objFunction.value, text: objFunction.status, selected: (objFunction.value.toLowerCase() == status.toLowerCase()) }));
            }
        }
    });
};

function loadDropDownFrequency(selectedFreq) {
    //Set default value  
    _callServer({
        loadingMsg: "Loading Global Process...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT '0' AS ID, '-- Select --' AS Val UNION SELECT DISTINCT [frequency] as ID, [frequency] as Val FROM [IncomeAndOtherQD].[dbo].[EntityMaster]") },
        type: "post",
        success: function (resultList) {
            $("#ddlFrequency").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $("#ddlFrequency").append($('<option>', { value: objFunction.ID, text: objFunction.Val, selected: (objFunction.Val == selectedFreq) }));
            }
        }
    });
};


$("#btnNewEntity").click(function () {
    showEntitiMasterModal("NewEntity");
});

//#endregion

//#region FUNCTION TYPE
function loadTableFunctionType() {
    $.jqxGridApi.create({
        showTo: "#tblFunctionType",
        options: {
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_FunctionTypeListing]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
			{ name: 'funcID', type: 'string', width: '10%', hidden: true },
			{ name: 'function', type: 'string', width: '50%' },
			{ name: 'active', type: 'string', width: '50%' }
        ],
        ready: function () {
            _hideLoadingFullPage()
            $('#tblFunctionType').on('rowdoubleclick', function (event) {
                // event.args.rowindex is a bound index.
                if (typeof event.args.group == "undefined") {
                    var row = event.args.rowindex;
                    var datarow = $("#tblFunctionType").jqxGrid('getrowdata', row);
                    showFunctionTypeModal(datarow);
                }
            });
        }
    });
}

//Function Type
function showFunctionTypeModal(row) {
    if (row == "NewFunction") {
        _showModal({
            title: "<h3>Function Type</h3>",
            width: "40%",
            contentAjaxUrl: "/IncomeAndOther/ModalFunction",
            buttons: [
            {
                name: "Save Changes",
                class: "btn-info",
                onClick: function ($modal) {


                    if ($("#txtfunction").val() != "") {
                        _callServer({
                            url: '/IncomeAndOther/FunctionEdit',
                            data: { 'functionId': 'NewFunction', 'functionName': $("#txtfunction").val(), 'status': $("#jqxcheckbox").val() },
                            type: "post",
                            success: function (savingStatus) {
                                _showNotification("success", "Data was saved successfully.");
                                loadTableFunctionType();
                            }
                        });
                    } else {
                        _showNotification("error", "Please enter a function name.");
                    }


                }
            }],
            onReady: function ($modal) {

                //#region CREATE CHECKBOX & ASSIGN VALUE
                $("#jqxcheckbox").jqxCheckBox({
                    width: 120,
                    height: 25
                });
                //
                if (row.active == "True") {
                    $("#jqxcheckbox").jqxCheckBox({ checked: true });
                } else {
                    $("#jqxcheckbox").jqxCheckBox({ checked: false });
                }
                //#endregion 


            }
        });
    } else {
        _showModal({
            title: "<h3>Function Type</h3>",
            width: "40%",
            contentAjaxUrl: "/IncomeAndOther/ModalFunction",
            contentAjaxParams: {
                funcID: row.funcID,
                function1: row.function,
                active: row.active
            },
            buttons: [
            {
                name: "Save Changes",
                class: "btn-info",
                onClick: function ($modal) {
                    _callServer({
                        url: '/IncomeAndOther/FunctionEdit',
                        data: { 'functionId': row.funcID, 'functionName': $("#txtfunction").val(), 'status': $("#jqxcheckbox").val() },
                        type: "post",
                        success: function (savingStatus) {
                            _showNotification("success", "Data was saved successfully.");
                            loadTableFunctionType();
                        }
                    });
                }
            }],
            onReady: function ($modal) {

                //#region CREATE CHECKBOX & ASSIGN VALUE
                $("#jqxcheckbox").jqxCheckBox({
                    width: 120,
                    height: 25
                });
                //
                if (row.active == "True") {
                    $("#jqxcheckbox").jqxCheckBox({ checked: true });
                } else {
                    $("#jqxcheckbox").jqxCheckBox({ checked: false });
                }
                //#endregion 


            }
        });
    }
}

//Add new function
$("#btnNewFunction").click(function () {
    showFunctionTypeModal("NewFunction");
});


//#endregion

//#region CALENDAR FILING

//Add new filing calendar
$("#btnSaveFilingCalendar").click(function () {

    var date = $("#dateInput").jqxDateTimeInput('getDate');
    var formattedDate = $.jqx.dataFormat.formatdate(date, 'd');

    var selectedEntityID = $("#selectedEntityID").attr("identity");
    var selectedCalendar = $("#ddlCalendarType option:selected").val();

    //RETURN PACKAGE ITEMS------------------------------------------------------
    var date4 = $("#dueDateCorpTax").jqxDateTimeInput('getDate');
    var dueDateCorpTax = $.jqx.dataFormat.formatdate(date4, 'd');
    //---------------------------------------------------------------------------


    if (selectedEntityID != "" && $("#txtfunction").val() != "" && selectedCalendar != "--Select--") {
        _callServer({
            url: '/IncomeAndOther/SaveFilingCalendar',
            data: {
               'identity': selectedEntityID, 'filingDueDate': formattedDate, 'calendarType': selectedCalendar, 'dateDueCorpTax':dueDateCorpTax
            },
            type: "post",
            success: function (savingStatus) {
                _showNotification("success", "Data was saved successfully.");
                loadTableFunctionType();
                $("#dateInput").val("");
                $("#selectedEntityID").text("");
                $("#dueDateCorpTax").val("");
                $("#ddlCalendarType")[0].selectedIndex = 0;

            }
        });
    } else {
        _showNotification("error", "Please enter a function name and Calendar type.");
    }
});

function loadDropDownEntities1() {
    $.jqxGridApi.create({
        showTo: "#tblEntityMasterDrop",
        options: {
            //for comments or descriptions
            height: "300",
            width: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_EntityMasterListing]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'entityID', type: 'string', hidden: true },
			{ name: 'logID', type: 'string', width: '20%' },
			{ name: 'entityName', type: 'string', width: '40%' },
			{ name: 'function', type: 'string', width: '20%' },
			{ name: 'frequency', type: 'string', width: '20%' }
        ],

        ready: function () {
            _hideLoadingFullPage();
            $('#tblEntityMasterDrop').on('rowdoubleclick', function (event) {
                // event.args.rowindex is a bound index.
                if (typeof event.args.group == "undefined") {
                    var row = event.args.rowindex;
                    var datarow = $("#tblEntityMasterDrop").jqxGrid('getrowdata', row);
                    // showEntitiMasterModal(datarow);
                }
            });
        }
    });
}

$("#tblEntityMasterDrop").on('rowselect', function (event) {
    var args = event.args;
    var row = $("#tblEntityMasterDrop").jqxGrid('getrowdata', args.rowindex);
    var dropDownContent = '<div id="selectedEntityID" style="position: relative; margin-left: 3px; margin-top: 5px;" idEntity="' + row.entityID + '" >' + row['entityName'] + ' ' + row['function'] + '</div>';
    $("#jqxdropdownbutton").jqxDropDownButton('setContent', dropDownContent);


});

$("#tblEntityMasterDrop").jqxGrid('selectrow', 0);

function loadDropDownEntities() {
    //Set default value  
    _callServer({
        loadingMsg: "Loading Global Process...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify(
				"Select * FROM (SELECT '0' AS [entityID], '-- Select --' AS [entityName] UNION " +
				"SELECT  REPLACE(CAST([entityID]   AS VARCHAR(200)),'-','') as [entityID] ," +
				"(REPLACE(CAST([entityName] AS VARCHAR(36)),'-','') + ' (Log: '+ " +
				"REPLACE(CAST([logID] AS VARCHAR(36)),'-','') +  + ' Freq: '+ " +
				"REPLACE(CAST([frequency] AS VARCHAR(36)),'-','') +  ' FCN: ' + " +
				"REPLACE(CAST([function] AS VARCHAR(36)),'-','') " +
				")as [entityName] FROM [IncomeAndOtherQD].[dbo].[EntityMaster] WHERE active = 1 ) a order by entityName"
				)
        },
        type: "post",
        success: function (resultList) {
            $("#ddlEntities").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $("#ddlEntities").append($('<option>', { value: objFunction.entityID, text: objFunction.entityName, selected: (objFunction.entityID.toLowerCase() == "0") }));
            }
        }
    });
};

$("#ddlCalendarType").change(function () {
    var selectedValue = $("#ddlCalendarType option:selected").val();
    if (selectedValue == "Default" || selectedValue == "--Select--") {
        $('#divReturnPackage').hide();
    } else {
        $('#divReturnPackage').show();
    }
});

//#endregion

//#region AUDIT TRAIL
function loadTableAuditTrail() {
    $.jqxGridApi.create({
        showTo: "#tblAuditTrail",
        options: {
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_AuditTrailListing]",
            Params: [
				{ Name: "@year", Value: new Date().getFullYear() },
				{ Name: "@month", Value: '1' }

            ]
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
			{ name: 'Year', type: 'string', width: '10%' },
			{ name: 'auditTimestamp', text: 'Time stamp', type: 'date', cellsformat: "M/d/yyyy", width: '10%' },
			{ name: 'auditAction', type: 'string', text: 'Action', width: '10%' },
			{ name: 'objectName', text: 'Field', type: 'string', width: '10%' },
            { name: 'logID', text: 'logID', type: 'string', width: '10%' },
            { name: 'entityName', text: 'EntityName', type: 'string', width: '10%' },
            { name: 'function', text: 'Function', type: 'string', width: '10%' },
            { name: 'frequency', text: '', type: 'string', width: '10%' },
			{ name: 'oldValue', type: 'string', width: '10%' },
			{ name: 'newValue', type: 'string', width: '10%' },
			{ name: 'SOEID', type: 'string', width: '10%' },
			{ name: 'FieldId', type: 'string', width: '10%' }
			//{ name: 'auditTrailID', type: 'string', width: '10%' }
        ],
        ready: function () {
            _hideLoadingFullPage()
            $('#tblAuditTrail').on('rowdoubleclick', function (event) {
                if (typeof event.args.group == "undefined") {
                    // event.args.rowindex is a bound index.
                    var row = event.args.rowindex;
                    var datarow = $("#tblAuditTrail").jqxGrid('getrowdata', row);
                    showCompleteModal(datarow);
                }
            });
        }
    });
}

function loadDropDownYears(selectedFunction) {
    //Set default value  
    _callServer({
        loadingMsg: "Loading Global Process...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT'-- Select --' as [Year] UNION  SELECT DISTINCT  CAST(DATEPART(YEAR,[auditTimestamp]) AS VARCHAR(36)) as [Year] FROM [IncomeAndOtherQD].[dbo].[AuditTrail] ") },
        type: "post",
        success: function (resultList) {
            $("#ddlFunction").contents().remove();

            //Render Global Process Options
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $("#ddlFunction").append($('<option>', { value: objFunction.funcID, text: objFunction.function, selected: (objFunction.function.toLowerCase() == selectedFunction.toLowerCase()) }));
            }

            //if (params.fnSuccess) {
            //    params.fnSuccess(resultList, selectedFunction);
            //}
        }
    });
};
//#endregion

//#region YEAR END
//Year End
function showYearEndModal(row) {
    //var row = $.jqxGridApi.getOneSelectedRow("#tblCalendarComplete", true);
    _showModal({
        title: "<h3>Filing Calendar - Log ID:" + row.logID + " - Entity: " + row.entityName + "</h3>",
        width: "98%",
        contentAjaxUrl: "/IncomeAndOther/ModalTemplate",
        contentAjaxParams: {
            logID: row.logID,
            entityID: row.entityID,
            entityName: row.entityName,
            function1: row.function,
            frequency: row.frequency,
            reviewer: row.reviewer,
            preparer: row.preparer,
            filingDueDate: _formatDate(new Date(row.filingDueDate), 'MM/dd/yyyy'),
            filingID: row.filingID,
            status: row.status,
            statusPrepared: row.statusPrepared,
            statusDesc: row.statusDesc,
            preparedDdate: _formatDate(new Date(row.preparedDdate), 'MMM-dd-yyyy'),
            reviewedDate: _formatDate(new Date(row.reviewedDate), 'MMM-dd-yyyy'),
            commentFlag: row.commentFlag,
            counts: row.counts,
            statusDate: row.statusDate,
            statusSOEID: row.statusSOEID,
            statusPreparedDate: row.statusPreparedDate,
            statusPreparedSOEID: row.statusPreparedSOEID,
            comments: row.comments,
            ttiCode: row.ttiCode,
            EIN: row.EIN,
            state: row.state,
            city: row.city,
            branch: row.branch,
            dueDay: row.dueDay,
            dueMonth: row.dueMonth
        },
        buttons: [{
            name: "Save Changes",
            class: "btn-info",
            onClick: function ($modal) {
                _callServer({
                    url: '/IncomeAndOther/CompletedEdit',
                    data: { 'pjson': JSON.stringify(row), 'comments': $("#txtNewComments").val() },
                    type: "post",
                    success: function (savingStatus) {
                        _showNotification("success", "Data was saved successfully.");
                        reloadTableCompleted();
                    }
                });
                //alert($modal.find("#txtlogID").val());
            }
        }],
        onReady: function ($modal) {
            loadDropDownGlobalProcess(row.function);
            $modal.find("#txtfilingDueDate").prop('disabled', true);
            $modal.find("#txtaccounts").prop('disabled', true);

            $modal.find("#spinner").spinner({
                min: 0,
                max: 500,
                step: 1,
                start: 1000,
                numberFormat: "C"
            });
            // $modal.find("#spinner").val("20");
        }
    });
}

$("#btnGenerateNewCalendar").click(function () {
    var value = "something";
    //if ($("#txtfunction").val() != "") {
        _callServer({
            url: '/IncomeAndOther/GenerateNewFilingCalendar',
            data: { 'value': value },
            type: "post",
            success: function (savingStatus) {
                _showNotification("success", "Filing Calendar Updated Successfully.");
                loadTableFunctionType();
            }
        });
    //} else {
    //    _showNotification("error", "Please enter a function name.");
    //}
});

function loadTableBusinessCalendar() {
    $.jqxGridApi.create({
        showTo: "#tblBusinessCalendar",
        options: {
            //for comments or descriptions
            height: "250",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_BusinessCalendar]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
        { name: 'bdID', type: 'string', hidden: true },
        { name: 'businessDay', type: 'string', width: '20%' },
        { name: 'reportingYear', type: 'string', width: '20%' },
        { name: 'reportingMonth', type: 'string', width: '20%' },
        { name: 'reportingMonthName', type: 'string', width: '20%' },
        { name: 'BDDate', type: 'string', width: '10%' },
        { name: 'active', type: 'string', width: '10%' }
        ],

        ready: function () {
            _hideLoadingFullPage();
        }
    });
}


$("#btnActivateNewCalendar").click(function () {
    var activeYear = $("#ddlDueYearActivate :selected").val();
    if (activeYear != "9999") {
    _callServer({
        url: '/IncomeAndOther/ActivateYear',
        data: { 'activeYear': activeYear },
        type: "post",
        success: function (savingStatus) {
            _showNotification("success", "Filing Calendar Year changed to: " + activeYear + "");
        }
    });
    } else {
        _showNotification("error", "Please select a year to activate.");
    }
});


$("#btnLockYE").click(function () {
    var pass = "yeTaxes2017"

    if ($("#txtYEPassword").val() == pass) {
        $("#divYEUpdates").show();
    } else {
        _showNotification("error", "Wrong password");
    }

    
    //var activeYear = $("#ddlDueYearActivate :selected").val();
    //if (activeYear != "9999") {
    //} else {
    //   
    //}
});


//#endregion






