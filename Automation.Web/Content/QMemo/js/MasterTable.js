/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="QMemoGlobal.js" />

//Hide Menu
_hideMenu();

$(document).ready(function () {
    //Group By
    $("#cboGroupBy").change(function () {
        loadTableGroupBy();

        //QMemo Accounts
        //if ($("#cboGroupBy").val() == "QMemoAccountMapped" || $("#cboGroupBy").val() == "QMemoAccountNoMapped") {
        //    $("#btnViewManualTemplateDetails").show();
        //} else {
        //    //Hide View Manual Template Balance Detail on FDL Account grouping
        //    $("#btnViewManualTemplateDetails").hide();
        //}

        //Full Suite Custom Filters Download 
        if ($("#cboGroupBy").val() == "QMemoAccountMapped" ||
            $("#cboGroupBy").val() == "QMemoAccountNoMapped" ||
            $("#cboGroupBy").val() == "QMemoAccountDetailPaging" ||
            $("#cboGroupBy").val() == "GLAccount" ||
            $("#cboGroupBy").val() == "FullSuitePaging" ||
            $("#cboGroupBy").val() == "FullSuiteNewGLAccounts") {

            $("#btnViewMappedDetails").hide();

        } else {
            $("#btnViewMappedDetails").show();
        }

    });
    //$("#cboGroupBy").select2();

    //Load Master table summary
    $("#cboPeriod").change(function () {
        loadTableGroupBy();
    });
    //$("#cboPeriod").select2();

    //Load Mapped details
    $("#btnViewMappedDetails").click(function () {
        loadTableMappedBalanceDetails();
    });

    //Load Manual Template details
    $("#btnViewManualTemplateDetails").click(function () {
        loadTableManualTempBalanceDetails();
    });

    //Export to excel create manually
    $("#btnExportMasterTable").click(function () {
        downloadMasterTableSummary();
    });

    $("#btnExportMasterTableDetail").click(function () {
        downloadFullSuiteDetail();
    });

    //Load Period
    loadPeriod(function () {

        //Load Business
        _loadBusiness(function () {
            //Load last quarter data
            loadTableGroupBy();

            //Load last full suite file information
            loadLastFullSuiteFileInfo();
        });
    });
});

function loadPeriod(fnOnSuccess) {
    var sql =
        "WITH TDATA AS( " +
        "    SELECT DISTINCT  " +
        "        CONVERT(VARCHAR,[Year]) + '-Q' + CONVERT(VARCHAR,[Quarter]) AS [Period],  " +
        "        [Year],  " +
        "        [Quarter]  " +
        "    FROM  " +
        "        [dbo].[tblM_MappingPeriod] " +
        "    WHERE  " +
        "        [IsDeleted] = 0 " +
        ")  " +
        "SELECT [Period] FROM TDATA ORDER BY [Year], [Quarter] ";

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loadin Periods Information...",
        url: '/Shared/ExecQuery',
        data: { 'pjsonSql': JSON.stringify(sql) },
        type: "post",
        success: function (resultList) {
            for (var i = 0; i < resultList.length; i++) {
                var objItem = resultList[i];

                $("#cboPeriod").append($('<option>', { value: objItem.Period, text: objItem.Period, selected: (i == resultList.length - 1) }));
            }

            if (fnOnSuccess) {
                fnOnSuccess();
            }
        }
    });
}

function hasFilters() {
    var hasFilter = false;

    //QMemo Filter
    if ($("#HFQMemoFilter").val()) {
        hasFilter = true;
    }

    //Business Filter
    if ($("#HFBusinessIDFilter").val()) {
        hasFilter = true;
    }

    return hasFilter;
}

function getFilterGroup(ptype) {
    var filterValue = "";
    var filterGroup = null;

    if (ptype == "Year") {
        filterValue = getSelectedYear();
        filterValue = filterValue ? filterValue : 0;
    }

    if (ptype == "Quarter") {
        filterValue = getSelectedQuarter();
        filterValue = filterValue ? filterValue : 0;
    }

    if (filterValue != null) {
        filterGroup = new $.jqx.filter();
        var filter_or_operator = 1;
        var filterCondition = 'equal';
        var filter = filterGroup.createfilter('stringfilter', filterValue, filterCondition);
        filterGroup.addfilter(filter_or_operator, filter);
    }

    return filterGroup;
};

function loadTableMappedBalanceDetails() {
    var $jqxGrid = $("#tbl-master-table");
    var rowIndex = $jqxGrid.jqxGrid('selectedrowindex');
    var dataRow = $jqxGrid.jqxGrid('getrowdata', rowIndex);

    if (dataRow) {
        var sql =
        "SELECT TOP 5000 " +
	    "     [QMEMO_ID] " +
        "    ,[QMEMO_ACCOUNT] " +
        "    ,[LV_ID] " +
        "    ,[FRS_BSUNIT_ID] " +
        "    ,[GOC] " +
        "    ,[GL_ACCT_ID] " +
        "    ,[FDL_ACCT_ID] " +
        "    ,[ENTRPS_PROD_ID] " +
        "    ,[SRC_SYS_ID] " +
        "    ,[SUB_GL_FEED_ID] " +
        "    ,[CPRT_GOC] " +
        "    ,[AFFL_LV_ID] " +
        "    ,[AFFL_FRS_BSUNIT_ID] " +
        "    ,[ICE_CODE] " +
        "    ,[CUST_ID] " +
        "    ,[TRAN_CCY_CD] " +
        "    ,[GAAP_TYP_CD] " +
        "    ,[BALANCE] " +
        "    ,[BUSINESS_ID] " +
        "    ,[BUSINESS_NAME] " +
        //"    ,ISNULL([BUSINESS_NAME], '" + defaultBusiness.BusinessName + "') BUSINESS_NAME " +
        "    ,[MANAGED_SEGMENT] " +
        "    ,[YEAR] " +
        "    ,[QUARTER] " +
        "    ,[ACTG_PRD_NO] " +
        "    ,[FULL_KEY] " +
	    "    ,[MANAGED_SEGMENT_DESC] AS MANAGED_SEGMENT_DESCRIPTION " +
        "FROM " +
        "   [dbo].[VW_MasterTableMapping] " +
        "WHERE " +
        "   [YEAR] = " + getSelectedYear() + " AND " +
        "   [QUARTER] = " + getSelectedQuarter() + " AND " +
        "   [BUSINESS_ID] = " + dataRow.BusinessID + " AND " +

        (($("#cboGroupBy").val() == "QMemoAccountMapped" || $("#cboGroupBy").val() == "QMemoAccountNoMapped") ? " QMEMO_ID = " + dataRow.QMemoID + " " : " [MT_FDL_ACCT_ID] = " + dataRow.FDLAccountID) + " ";

        //console.log(sql);

        $.jqxGridApi.create({
            showTo: "#tbl-master-table-details",
            options: {
                //for comments or descriptions
                height: "400",
                autoheight: false,
                autorowheight: false,
                selectionmode: "singlerow",
                showfilterrow: true,
                sortable: true,

                //Clear div of ShowTo, some times filters aren't working when another grid is created in same div
                cleardiv: true
            },
            sp: {
                SQL: sql
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
                dataBinding: "Large Data Set"
            },
            columns: [
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                { name: 'QMEMO_ACCOUNT', text: 'QMemo Account', width: '110px', type: 'string', filtertype: 'checkedlist' },
                { name: 'BUSINESS_NAME', text: 'Business', width: '150px', type: 'string', filtertype: 'checkedlist' },
                { name: 'MANAGED_SEGMENT', text: 'Managed Segment', width: '116px', type: 'string', filtertype: 'checkedlist' },
                { name: 'MANAGED_SEGMENT_DESCRIPTION', text: 'Managed Segment Description', width: '200px', type: 'string', filtertype: 'checkedlist' },
                { name: 'LV_ID', text: 'Legal Vehicle ID', type: 'string', filtertype: 'checkedlist' },
                { name: 'FRS_BSUNIT_ID', text: 'FRS Business Unit ID', type: 'string', filtertype: 'checkedlist' },
                { name: 'GOC', text: 'GOC', type: 'string', filtertype: 'checkedlist' },
                { name: 'GL_ACCT_ID', text: 'General Ledger Account ID', type: 'string', filtertype: 'checkedlist' },
                { name: 'FDL_ACCT_ID', text: 'FDL Account ID', type: 'string', filtertype: 'checkedlist' },
                { name: 'ENTRPS_PROD_ID', text: 'Enterprise Product ID', type: 'string', filtertype: 'checkedlist' },
                { name: 'SRC_SYS_ID', text: 'Source System ID', type: 'string', filtertype: 'checkedlist' },
                { name: 'SUB_GL_FEED_ID', text: 'Original Journal Sub GL Feed ID', type: 'string', filtertype: 'checkedlist' },
                { name: 'CPRT_GOC', text: 'CounterParty GOC', type: 'string', filtertype: 'checkedlist' },
                { name: 'AFFL_LV_ID', text: 'Affiliate Legal Vehicle ID', type: 'string', filtertype: 'checkedlist' },
                { name: 'AFFL_FRS_BSUNIT_ID', text: 'Affiliate FRS Business Unit ID', type: 'string', filtertype: 'checkedlist' },
                { name: 'ICE_CODE', text: 'ICE Code', type: 'string', filtertype: 'checkedlist' },
                { name: 'CUST_ID', text: 'Customer ID', type: 'string', filtertype: 'checkedlist' },
                { name: 'TRAN_CCY_CD', text: 'Transaction Currency Code', type: 'string', filtertype: 'checkedlist' },
                { name: 'GAAP_TYP_CD', text: 'GAAP Type Code', type: 'string', filtertype: 'checkedlist' },
                { name: 'BALANCE', text: 'Quarter Balance', type: 'float', filtertype: 'number', cellsformat: 'd' }
            ]
        });

        $("#master-detail-content").show();
    } else {
        $.showAlert({
            showTo: $jqxGrid.parent(),
            type: 'error',
            content: "Please select at least one row",
            animateScrollTop: false
        });
    }

}

function loadTableManualTempBalanceDetails() {
    var $jqxGrid = $("#tbl-master-table");
    var rowIndex = $jqxGrid.jqxGrid('selectedrowindex');
    var dataRow = $jqxGrid.jqxGrid('getrowdata', rowIndex);

    if (dataRow) {
        var sql =
            " SELECT " +
            "     [QMEMO_ID] " +
            "    ,[QMEMO_ACCOUNT] " +
            "    ,[FRS_BSUNIT_ID] " +
            "    ,[SOURCE] " +
            "    ,[BUSINESS_ID] " +
            "    ,[BUSINESS_NAME] " +
            "    ,[YEAR] " +
            "    ,[QUARTER] " +
            "    ,[BALANCE] " +
            "    ,[FILE_CREATED_DATE] " +
            "    ,[FILE_CREATED_BY] " +
            " FROM [dbo].[VW_ManualTemplate] " +
            " WHERE QMEMO_ID = " + dataRow.QMemoID + " AND BUSINESS_ID = " + dataRow.BusinessID + " AND YEAR = " + getSelectedYear() + " AND QUARTER = " + getSelectedQuarter() + " ";

        //console.log(sql);

        $.jqxGridApi.create({
            showTo: "#tbl-master-table-details",
            options: {
                //for comments or descriptions
                height: "400",
                autoheight: false,
                autorowheight: false,
                selectionmode: "singlerow",
                showfilterrow: true,
                sortable: true,
                editable: true
            },
            sp: {
                SQL: sql
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
                dataBinding: "Large Data Set"
            },
            columns: [
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                { name: 'FRS_BSUNIT_ID', text: 'FRS Business Unit ID', width: '15%', type: 'string', filtertype: 'checkedlist' },
                { name: 'QMEMO_ACCOUNT', text: 'QMemo Account', width: '20%', type: 'string', filtertype: 'checkedlist' },
                { name: 'BUSINESS_NAME', text: 'Business', width: '10%', type: 'string', filtertype: 'checkedlist' },
                { name: 'SOURCE', text: 'Source', type: 'string', width: '15%', filtertype: 'checkedlist' },
                { name: 'BALANCE', text: 'Balance', type: 'float', width: '15%', filtertype: 'number', cellsformat: 'd' },
                { name: 'FILE_CREATED_BY', text: 'Uploaded By', type: 'date', width: '10%' },
                { name: 'FILE_CREATED_DATE', text: 'Uploaded Date', type: 'string', width: '15%', cellsformat: 'MMM d yyyy hh:mm tt' }
            ]
        });

        $("#master-detail-content").show();

        //Scroll down
        $('html, body').animate({
            scrollTop: '600px'
        }, 800);
    } else {
        $.showAlert({
            showTo: $jqxGrid.parent(),
            type: 'error',
            content: "Please select at least one row",
            animateScrollTop: false
        });
    }
}

function getSelectedYear() {
    var selVal = $("#cboPeriod").val();
    //return selVal;
    return selVal.split("-")[0];
}

function getSelectedQuarter() {
    var selVal = $("#cboPeriod").val();
    //return selVal;
    return selVal.split("-")[1].replace("Q", "");
}

function downloadFullSuiteMapDetail(pqmemoID, pqmemo, pbusinessID, pbusiness, onlyMapped) {
    var spName;
    var spParams;
    var filename;
    var where = 'WHERE ( YEAR = ' + getSelectedYear() + ') AND ( QUARTER = ' + getSelectedQuarter() + ') AND ( QMEMO_ACCOUNT_ID = ' + pqmemoID + ') AND (BUSINESS_ID = ' + pbusinessID + ')';

    if (typeof onlyMapped == "undefined") {
        onlyMapped = "1";
    }

    spName = "[SP_FSQuarter_MTFullSuiteMapDetailPagingExcel]";
    spParams = [
        { "Name": "@Year", "Value": getSelectedYear() },
        { "Name": "@Quarter", "Value": getSelectedQuarter() },
        { "Name": "@StrWhere", "Value": _encodeSkipSideminder(where) },
        { "Name": "@StartIndexRow", "Value": 0 },
        { "Name": "@EndIndexRow", "Value": 9999999 },
        { "Name": "@BusinessIDs", Value: _getStrBusinessIDByUser() },
        { "Name": "@OnlyMapped", "Value": onlyMapped }
    ];

    pbusiness = _replaceAll(" ", "", pbusiness);
    pbusiness = _replaceAll("-", "", pbusiness);

    filename = "FullSuiteDetails_" + getSelectedYear() + "Q" + getSelectedQuarter() + "_" + pqmemo + "_" + pbusiness + "_" + _createCustomID(false) + ".xls";

    _callProcedure({
        name: spName,
        params: spParams,
        response: true,
        download: true,
        filename: filename,
        success: {
            msg: "Generating report... Please Wait, this operation may take some time to complete."
        }
    });
}

function downloadQMemoByBusiness(pqmemoID, pqmemo, pbusinessID, pbusiness, onlyMapped) {
    var spName;
    var spParams;
    var filename;
    var where = 'WHERE ( YEAR = ' + getSelectedYear() + ') AND ( QUARTER = ' + getSelectedQuarter() + ') AND ( QMEMO_ACCOUNT_ID = ' + pqmemoID + ') AND (BUSINESS_ID = ' + pbusinessID + ')';

    if (typeof onlyMapped == "undefined") {
        onlyMapped = "1";
    }

    spName = "[SP_FSQuarter_MTQMemoXBusinessDetailPagingExcel]";
    spParams = [
        { "Name": "@Year", "Value": getSelectedYear() },
        { "Name": "@Quarter", "Value": getSelectedQuarter() },
        { "Name": "@StrWhere", "Value": _encodeSkipSideminder(where) },
        { "Name": "@StartIndexRow", "Value": 0 },
        { "Name": "@EndIndexRow", "Value": 9999999 },
        { "Name": "@BusinessIDs", Value: _getStrBusinessIDByUser() },
        { "Name": "@OnlyMapped", "Value": onlyMapped }
    ];

    pbusiness = _replaceAll(" ", "", pbusiness);
    pbusiness = _replaceAll("-", "", pbusiness);

    filename = "MappedBalanceDetails_" + getSelectedYear() + "Q" + getSelectedQuarter() + "_" + pqmemo + "_" + pbusiness + "_" + _createCustomID(false) + ".xls";

    _callProcedure({
        name: spName,
        params: spParams,
        response: true,
        download: true,
        filename: filename,
        success: {
            msg: "Generating report... Please Wait, this operation may take some time to complete."
        }
    });
}

function downloadMasterTableSummary() {
    var spName;
    var spParams;
    var filename;
    var filterData = $.jqxGridApi.getFilter("#tbl-master-table");

    //QMemo Accounts by Business
    if ($("#cboGroupBy").val() == "QMemoAccountMapped" || $("#cboGroupBy").val() == "QMemoAccountNoMapped") {
        spName = "[dbo].[SP_FSQuarter_MTQMemoXBusinessExcel]";
        spParams = [
            { "Name": "@Year", "Value": getSelectedYear() },
            { "Name": "@Quarter", "Value": getSelectedQuarter() },
            { "Name": "@BusinessIDs", Value: _getStrBusinessIDByUser() },
            { "Name": "@OnlyMapped", "Value": ($("#cboGroupBy").val() == "QMemoAccountMapped" ? "1" : "0") }
        ];
        filename = "QMemoAccountsSummary_" + getSelectedYear() + "Q" + getSelectedQuarter() + "_" + _createCustomID(false) + "";
    }

    //QMemo Accounts Download with Filters
    if ($("#cboGroupBy").val() == "QMemoAccountDetailPaging") {
        spName = "[SP_FSQuarter_MTQMemoXBusinessDetailPagingExcel]";
        spParams = [
            { "Name": "@Year", "Value": getSelectedYear() },
            { "Name": "@Quarter", "Value": getSelectedQuarter() },
            { "Name": "@StrWhere", "Value": filterData.Where },
            { "Name": "@StrOrderBy", "Value": filterData.Sort },
            { "Name": "@StartIndexRow", "Value": 0 },
            { "Name": "@EndIndexRow", "Value": 9999999 },
            { "Name": "@BusinessIDs", Value: _getStrBusinessIDByUser() }
        ];
        filename = "MappedBalanceDetails_" + getSelectedYear() + "Q" + getSelectedQuarter() + "_" + _createCustomID(false) + "";
    }

    //Full Suite by FDL Accounts
    if ($("#cboGroupBy").val() == "FDLAccount") {
        spName = "[SP_FSQuarter_MTFullSuiteByFDLAccountExcel]";
        spParams = [
            { "Name": "@Year", "Value": getSelectedYear() },
            { "Name": "@Quarter", "Value": getSelectedQuarter() },
            { "Name": "@BusinessIDs", Value: _getStrBusinessIDByUser() }
        ];
        filename = "FullSuiteByFDLAccounts_" + getSelectedYear() + "Q" + getSelectedQuarter() + "_" + _createCustomID(false) + "";
    }

    //Full Suite by GL Accounts
    if ($("#cboGroupBy").val() == "GLAccount") {
        spName = "[SP_FSQuarter_MTFullSuiteByGLAccountExcel]";
        spParams = [
            { "Name": "@Year", "Value": getSelectedYear() },
            { "Name": "@Quarter", "Value": getSelectedQuarter() },
            { "Name": "@BusinessIDs", Value: _getStrBusinessIDByUser() }
        ];
        filename = "FullSuiteByGLAccounts_" + getSelectedYear() + "Q" + getSelectedQuarter() + "_" + _createCustomID(false) + "";
    }

    //Full Suite Download with Filters
    if ($("#cboGroupBy").val() == "FullSuitePaging") {
        spName = "[SP_FSQuarter_MTFullSuiteDetailPagingExcel]";
        spParams = [
            { "Name": "@Year", "Value": getSelectedYear() },
            { "Name": "@Quarter", "Value": getSelectedQuarter() },
            { "Name": "@StrWhere", "Value": filterData.Where },
            { "Name": "@StrOrderBy", "Value": filterData.Sort },
            { "Name": "@StartIndexRow", "Value": 0 },
            { "Name": "@EndIndexRow", "Value": 9999999 },
            { "Name": "@BusinessIDs", Value: _getStrBusinessIDByUser() }
        ];
        filename = "FullSuiteData_" + getSelectedYear() + "Q" + getSelectedQuarter() + "_" + _createCustomID(false) + "";
    }

    //Full Suite new GL Accounts
    if ($("#cboGroupBy").val() == "FullSuiteNewGLAccounts") {
        

        spName = "[SP_FSQuarter_MTFullSuiteNewGLAccountExcel]";
        spParams = [
            { "Name": "@Year", "Value": getSelectedYear() },
            { "Name": "@Quarter", "Value": getSelectedQuarter() },
            { "Name": "@BusinessIDs", Value: _getStrBusinessIDByUser() }
        ];
        filename = "FullSuiteNewGLAccounts_" + getSelectedYear() + "Q" + getSelectedQuarter() + "_" + _createCustomID(false) + "";
    }

    _downloadExcel({
        sql: _getSqlProcedure(spName, spParams),
        filename: filename,
        success: {
            msg: "Please Wait, this operation may take some time to complete. Generating report..."
        }
    });

    //_callProcedure({
    //    name: spName,
    //    params: spParams,
    //    response: true,
    //    download: true,
    //    filename: filename,
    //    success: {
    //        msg: "Generating report... Please Wait, this operation may take some time to complete."
    //    }
    //});
};

function downloadFullSuiteDetail() {
    var dataRecord = $.jqxGridApi.getOneSelectedRow("#tbl-master-table", true);
    if (dataRecord) {
        _callProcedure({
            name: $("#cboGroupBy").val() == "QMemoAccount" ? "[dbo].[SP_Mapping_ExcelMasterTableXQMemoDetail]" : "[SP_Mapping_ExcelMasterTableXFDLAccountDetail]",
            params: [
                { "Name": "@Year", "Value": getSelectedYear() },
                { "Name": "@Quarter", "Value": getSelectedQuarter() },
                { "Name": ($("#cboGroupBy").val() == "QMemoAccount" ? "@QMemoID" : "@FDLAccountID"), "Value": ($("#cboGroupBy").val() == "QMemoAccount" ? dataRecord.QMemoID : dataRecord.FDLAccountID) },
                { "Name": "@BusinessID", "Value": dataRecord.BusinessID }
            ],
            response: true,
            download: true,
            filename: _createCustomID() + "_MasterTable_" + getSelectedYear() + "-Q" + getSelectedQuarter() + "_" + ($("#cboGroupBy").val() == "QMemoAccount" ? dataRecord.QMemoAccount : dataRecord.FDLAccount) + "_" + dataRecord.Business + ".xls",
            success: {
                msg: "Generating report... Please Wait, this operation may take some time to complete."
            }
        });
    }
};

function loadLastFullSuiteFileInfo() {
    _callProcedure({
        response: true,
        name: "[dbo].[SP_Mapping_LastFullSuiteFileInfo]",
        params: [],
        showBeforeSendMsg: false,
        success: {
            fn: function (responseList) {
                if (responseList.length == 1) {
                    var data = responseList[0];
                    console.log(data);
                    $("#headerTitle").text("Master table - Last Full Suite File: Year " + data["Year"] + ", Accounting Period " + data["Month"] + ", Quarter " + data["Quarter"] + " on " + _formatDate(new Date(data["CreatedDate"]), "MMM d yyyy hh:mm TT"));
                }
            }
        }
    });
};

//-----------------------------------------------------------------------
// VIEW SUMMARY BY
//-----------------------------------------------------------------------
function loadTableGroupBy() {
    var gridConfig;

    //QMemo Accounts by Business
    if ($("#cboGroupBy").val() == "QMemoAccountMapped") {
        gridConfig = getConfigViewQMemoAccountsByBusiness(1);
    }

    //QMemo Accounts by Business
    if ($("#cboGroupBy").val() == "QMemoAccountNoMapped") {
        gridConfig = getConfigViewQMemoAccountsByBusiness(0);
    }

    //QMemo Accounts Download with Filters
    if ($("#cboGroupBy").val() == "QMemoAccountDetailPaging") {
        gridConfig = getConfigViewQMemoAccountsDownloadWithFilters();
    }

    //Full Suite by FDL Accounts
    if ($("#cboGroupBy").val() == "FDLAccount") {
        gridConfig = getConfigViewFullSuiteByFDLAccounts();
    }

    //Full Suite by GL Accounts
    if ($("#cboGroupBy").val() == "GLAccount") {
        gridConfig = getConfigViewFullSuiteByGLAccounts();
    }

    //Full Suite Download with Filters
    if ($("#cboGroupBy").val() == "FullSuitePaging") {
        gridConfig = getConfigViewFullSuiteDownloadWithFilters();
    }

    //Full Suite new GL Accounts
    if ($("#cboGroupBy").val() == "FullSuiteNewGLAccounts") {
        gridConfig = getConfigViewFullSuiteNewGLAccounts();
    }

    $.jqxGridApi.create({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Table Information...",
        showTo: "#tbl-master-table",
        options: {
            //for comments or descriptions
            height: hasFilters() ? "400" : "550",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: true,
            sortable: true,
            editable: true,
            //Clear div of ShowTo, some times filters aren't working when another grid is created in same div
            cleardiv: true
        },
        sp: {
            Name: gridConfig.spName,
            Params: gridConfig.spParams
        },
        source: gridConfig.source,
        //type: string - text - number - int - float - date - time 
        //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
        //cellsformat: ddd, MMM dd, yyyy h:mm tt, c2
        columns: gridConfig.columns,
        ready: gridConfig.fnReady
    });
}

//QMemo Accounts by Business Mapped!
function getConfigViewQMemoAccountsByBusiness(onlyMapped) {
    return {
        columns: [
            { name: 'QMemoID', type: 'int', hidden: true },
            { name: 'QMemoAccount', text: 'QMemo Account', type: 'string', width: '15%', filtertype: 'input', cellsalign: 'center', align: 'center' },
            { name: 'BusinessID', type: 'int', hidden: true },
            { name: 'Business', text: 'Business', type: 'string', width: '15%', filtertype: 'checkedlist', cellsalign: 'center', align: 'center' },
            { name: 'FullSuiteMappedBalance', text: 'Mapped Balance', type: 'float', sortable: true, width: '13%', filtertype: 'number', cellsformat: 'd' },
            {
                name: '...ViewMappedBalanceDetail', text: '', computedcolumn: true, text: '', type: 'html', width: '25px', editable: false, filterable: false, cellsrenderer: function (rowIndex, datafield, value) {
                    var result = '';
                    var dataRecord = $("#tbl-master-table").jqxGrid('getrowdata', rowIndex);

                    if ((dataRecord.FullSuiteMappedBalance != 0) || (dataRecord.FullSuiteMappedBalance == 0 && dataRecord.ManualTemplateBalance == 0)) {
                        var balance = numeral(dataRecord.FullSuiteMappedBalance).format('(0,0.0000)');
                        var fsRows = numeral(dataRecord.FullSuiteRows).format('0,0');
                        var actions = "<div style='padding: 10px; font-size: 15px;'>";

                        actions += "<b>QMemo Account:</b> " + dataRecord.QMemoAccount + " <br>";
                        actions += "<b>Business:</b> " + dataRecord.Business + " <br>";
                        actions += "<b>Mapped Balance:</b> " + balance + " <br><br>";
                        //actions += "<b>Full Suite Mapped Rows:</b> " + fsRows + " <br><br>";

                        actions += "<b>QMemo Mapping Table Details:</b><br>";
                        actions += "<a target='_blank' href='MasterTableView?ptype=QMemoXBusinessDetailPaging&pyear=" + getSelectedYear() + "&pquarter=" + getSelectedQuarter() + "&pqmemoAccountID=" + dataRecord.QMemoID + "&pqmemoAccount=" + dataRecord.QMemoAccount + "&pbusinessID=" + dataRecord.BusinessID + "&pbusiness=" + dataRecord.Business + "&ponlyMapped=" + onlyMapped + "&pbalance=" + balance + "' style='line-height: 25px;'>View Details</a><br>";
                        actions += "<a href='#' onclick='downloadQMemoByBusiness(" + dataRecord.QMemoID + ", &#34;" + dataRecord.QMemoAccount + "&#34;, " + dataRecord.BusinessID + ", &#34;" + dataRecord.Business + "&#34;, " + onlyMapped + ")' style='line-height: 25px;'>Download to Excel</a><br><br>";

                        actions += "<b>Full Suite File (" + fsRows + " Rows): </b><br>";
                        actions += "<a target='_blank' href='MasterTableView?ptype=FullSuiteMapDetailPaging&pyear=" + getSelectedYear() + "&pquarter=" + getSelectedQuarter() + "&pqmemoAccountID=" + dataRecord.QMemoID + "&pqmemoAccount=" + dataRecord.QMemoAccount + "&pbusinessID=" + dataRecord.BusinessID + "&pbusiness=" + dataRecord.Business + "&ponlyMapped=" + onlyMapped + "&pbalance=" + balance + "' style='line-height: 25px;'>View Details</a><br>";
                        actions += "<a href='#' onclick='downloadFullSuiteMapDetail(" + dataRecord.QMemoID + ", &#34;" + dataRecord.QMemoAccount + "&#34;, " + dataRecord.BusinessID + ", &#34;" + dataRecord.Business + "&#34;, " + onlyMapped + ")' style='line-height: 25px;'>Download to Excel</a><br><br>";

                        actions += "</div>";

                        result =

                        //'<span rel="tooltip" data-color-class="primary" data-animate=" animated fadeIn" data-toggle="tooltip" data-original-title="Upload Descriptions File" data-placement="right"><img class="qtipTooltip" title="" style="margin-left: 2px; margin-top: 3px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/QMemo/images/mt-actions.png"/></span>';

                        '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="right" data-content="' + actions + '" data-title="Actions" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin: 2px;"> ' +
                        '       <img style="margin-left: 2px; margin-top: 3px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/QMemo/images/mt-actions.png"/> ' +
                        '</div>';

                        //'<span rel="tooltip" data-color-class="primary" data-animate=" animated fadeIn" data-toggle="tooltip" data-original-title="' + actions + '" data-placement="right"> ' +
                        //'   <div style="text-align: center; cursor: pointer;" class="view-btn" > ' +
                        //'       <img style="margin-left: 2px; margin-top: 3px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/QMemo/images/mt-actions.png"/> ' +
                        //'   </div>' +
                        //'</span>';

                        //result = '<div style="text-align: center; cursor: pointer;" class="view-btn" ><img class="qtipTooltip" title="' + actions + '" style="margin-left: 2px; margin-top: 3px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/QMemo/images/mt-actions.png"/></div>';
                        //result = '<img src="App_Themes/Base/images/mt-actions.png" style="height: 20px;padding: 2px;cursor: pointer;">';
                    } else {
                        result = '';
                    }

                    return result;
                }
            },


            { name: 'ManualTemplateBalance', text: 'Offline Template Balance', type: 'float', sortable: true, width: '13%', filtertype: 'number', cellsformat: 'd' },

            {
                name: '...ViewOfflineTemplateDetail', text: '', computedcolumn: true, text: '', type: 'html', width: '22px', filterable: false, cellsrenderer: function (rowIndex, datafield, value) {
                    var result = '';
                    var dataRecord = $("#tbl-master-table").jqxGrid('getrowdata', rowIndex);

                    if (dataRecord.ManualTemplateBalance != 0) {
                        var balance = numeral(dataRecord.ManualTemplateBalance).format('(0,0.0000)');
                        var fsRows = numeral(dataRecord.FullSuiteRows).format('0,0');
                        var actions = "<div style='padding: 10px; font-size: 15px;'>";

                        actions += "<b>QMemo Account:</b> " + dataRecord.QMemoAccount + " <br>";
                        actions += "<b>Business:</b> " + dataRecord.Business + " <br>";
                        actions += "<b>Offline Temp Balance:</b> " + balance + " <br>";
                        actions += "<b>Offline Temp Rows:</b> " + fsRows + " <br><br>";

                        actions += "<a href='#' onclick='loadTableManualTempBalanceDetails();' style='line-height: 25px;'>View Details</a><br>";
                        actions += "<a href='#' style='line-height: 25px;'>Download to Excel</a><br>";

                        actions += "</div>";

                        result =
                        '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="right" data-content="' + actions + '" data-title="Actions" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                        '       <img style="margin-left: 2px; margin-top: 3px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/QMemo/images/mt-actions.png"/> ' +
                        '</div>';

                        //result = '<div style="text-align: center;" class="view-btn" ><img class="qtipTooltip" title="' + actions + '" style="margin-left: 2px; margin-top: 3px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/QMemo/images/mt-actions.png"/></div>';
                        //result = '<img src="App_Themes/Base/images/mt-actions.png" style="height: 20px;padding: 2px;cursor: pointer;">';
                    } else {
                        result = '';
                    }

                    return result;
                }
            },

            { name: 'TotalBalance', text: 'Total Balance', type: 'float', sortable: true, width: '12%', filtertype: 'number', cellsformat: 'd' },
            { name: 'PreviousQuarterBalance', text: 'Previous Balance', type: 'float', sortable: true, width: '12%', filtertype: 'number', cellsformat: 'd' },
            { name: 'QoQVariance', text: 'QoQ Variance', type: 'float', sortable: true, width: '12%', filtertype: 'number', cellsformat: 'd' },
            { name: 'FullSuiteRows', text: 'Rows', width: '100px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' }
        ],
        spName: "[dbo].[SP_FSQuarter_MTQMemoXBusiness]",
        spParams: [
            { Name: "@Year", Value: getSelectedYear() },
            { Name: "@Quarter", Value: getSelectedQuarter() },
            { Name: "@BusinessIDs", Value: _getStrBusinessIDByUser() },
            { Name: "@OnlyMapped", Value: onlyMapped }
        ],
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        fnReady: function () {
            var $jqxGrid = $("#tbl-master-table");

            _GLOBAL_SETTINGS.tooltipsPopovers();
            $jqxGrid.on("rowclick", function (event) {
                _GLOBAL_SETTINGS.tooltipsPopovers();
            });

            //Validate query string filters "QM_MasterTable.aspx?pqmemo=QM80065200&pbusiness=USCCB"
            var hasFilter = false;

            //QMemo Filter
            if ($("#HFQMemoFilter").val()) {
                var qmemoFilterGroup = getFilterGroup("QMemoAccount");
                $jqxGrid.jqxGrid('addfilter', "QMemoAccount", qmemoFilterGroup);
                hasFilter = true;
            }

            //Business Filter
            if ($("#HFBusinessIDFilter").val()) {
                var businessFilterGroup = getFilterGroup("BusinessID");
                $jqxGrid.jqxGrid('addfilter', "BusinessID", businessFilterGroup);
                hasFilter = true;
            }

            // apply the filters.
            if (hasFilter) {
                $jqxGrid.jqxGrid('applyfilters');
            }

            //Select first row by default
            var rows = $jqxGrid.jqxGrid("getrows");
            if (hasFilters() && rows.length > 0) {
                var defaulRow = rows[0];
                $jqxGrid.jqxGrid('selectrow', defaulRow.uid);

                //Load mapped details
                $("#btnViewMappedDetails").click();
            }

            function getFilterGroup(ptype) {
                var filterValue = "";
                var filterGroup = null;

                if (ptype == "QMemoAccount") {
                    filterValue = $("#HFQMemoFilter").val();
                }

                if (ptype == "BusinessID") {
                    filterValue = $("#HFBusinessIDFilter").val();
                }

                if (filterValue != null) {
                    filterGroup = new $.jqx.filter();
                    var filter_or_operator = 1;
                    var filterCondition = 'equal';
                    var filter = filterGroup.createfilter('stringfilter', filterValue, filterCondition);
                    filterGroup.addfilter(filter_or_operator, filter);
                }

                return filterGroup;
            };
        }
    };
};

//QMemo Accounts Download with Filters
function getConfigViewQMemoAccountsDownloadWithFilters() {
    var columns = [];
    var spName = "";
    var source;
    var fnReady;

    //Set columns
    columns.push({ name: "YEAR", type: "int", hidden: true });
    columns.push({ name: "QUARTER", type: "int", hidden: true });
    columns.push({ name: "BUSINESS_NAME", text: "Business", type: 'string', filtertype: "input" });
    columns.push({ name: "QMEMO_ACCOUNT", text: "QMemo Account", type: 'string', filtertype: "input" });
    columns.push({ name: "QMEMO_ACCOUNT_DESC", text: "QMemo Account Desc", type: 'string', width: '180px', filtertype: "input" });
    columns.push({ name: "GL_ACCT_ID", text: "GL Account", type: 'string', filtertype: "input" });
    columns.push({ name: "GL_ACCT_DESC", text: "GL Account Desc", type: 'string', width: '180px', filtertype: "input" });
    columns.push({ name: "FDL_ACCT_ID", text: "FDL Account", type: 'string', filtertype: "input" });
    columns.push({ name: "FDL_ACCT_DESC", text: "FDL Account Desc", type: 'string', width: '180px', filtertype: "input" });
    columns.push({ name: "GOC", text: "GOC", type: 'string', filtertype: "input" });
    columns.push({ name: "ENTRPS_PROD_ID", text: "Enterprice Product", type: 'string', filtertype: "input" });
    columns.push({ name: "ENTRPS_PROD_DESC", text: "Enterprice Product Desc", type: 'string', filtertype: "input" });
    columns.push({ name: "CUST_ID", text: "Customer ID", type: 'string', filtertype: "input" });
    columns.push({ name: "SUB_GL_FEED_ID", text: "SubGL Feed ID", type: 'string', filtertype: "input" });
    columns.push({ name: "AFFL_FRS_BSUNIT_ID", text: "Affiliate BU", type: 'string', filtertype: "input" });
    columns.push({ name: "ICE_CODE", text: "ICE Code", type: 'string', filtertype: "input" });
    columns.push({ name: "BALANCE", text: "Balance", type: 'float', filtertype: 'number', cellsformat: 'd' });

    //Set default filter by Year and Quarter
    var indexYear = _findIndexByProperty(columns, "name", "YEAR");
    var indexQuarter = _findIndexByProperty(columns, "name", "QUARTER");
    columns[indexYear].filter = getFilterGroup("Year");
    columns[indexQuarter].filter = getFilterGroup("Quarter");

    //Set Procedure
    spName = "[dbo].[SP_FSQuarter_MTQMemoXBusinessDetailPaging]";

    //Set Source
    source = {
        // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
        dataBinding: "Server Paging",
        pagesize: 200
    };

    //Set on ready fuction
    fnReady = function () { };

    return {
        columns: columns,
        spName: spName,
        spParams: [
            { Name: "@Year", Value: getSelectedYear() },
            { Name: "@Quarter", Value: getSelectedQuarter() },
            { Name: "@BusinessIDs", Value: _getStrBusinessIDByUser() }
        ],
        source: source,
        fnReady: fnReady
    };
};

//Full Suite by FDL Accounts
function getConfigViewFullSuiteByFDLAccounts() {
    return {
        columns: [
            { name: 'FDLAccountID', text: 'FDL Account', type: 'string', width: '100px', filtertype: 'input' },
            { name: 'FDLAccount', text: 'FDL Account Desc', type: 'string', width: '500px', filtertype: 'input' },
            { name: 'BusinessID', type: 'float', hidden: true },
            { name: 'Business', text: 'Business', type: 'string', width: '15%', filtertype: 'checkedlist' },
            { name: 'PrevQuarterToDateBalance', text: 'Previous QTD', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' },
            { name: 'CurrentQuarterToDateBalance', text: 'QTD', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' },
            { name: 'PrevQuarterToDateAverage', text: 'Previous QTD Average', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' },
            { name: 'CurrentQuarterToDateAverage', text: 'QTD Average', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' },
            { name: 'PrevYearToDateBalance', text: 'Previous YTD', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' },
            { name: 'CurrentYearToDateBalance', text: 'YTD', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' },
            { name: 'VarianceYearToDateBalance', text: 'QoQ YTD Variance', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' },
            { name: 'FullSuiteRows', text: 'Full Suite Rows', width: '100px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' }
        ],
        spName: "[dbo].[SP_FSQuarter_MTFullSuiteByFDLAccount]",
        spParams: [
            { Name: "@Year", Value: getSelectedYear() },
            { Name: "@Quarter", Value: getSelectedQuarter() },
            { Name: "@BusinessIDs", Value: _getStrBusinessIDByUser() }
        ],
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        fnReady: function () { }
    };
};

//Full Suite by GL Accounts
function getConfigViewFullSuiteByGLAccounts() {
    return {
        columns: [
            { name: 'BUSINESS_NAME', text: 'Business', type: 'string', filtertype: 'checkedlist' },
            { name: 'FDL_ACCOUNT_ID', text: 'FDL Account', type: 'string', filtertype: 'input' },
            { name: 'FDL_ACCOUNT_DESC', text: 'FDL Account Desc', width: '250px', type: 'string', filtertype: 'input' },
            { name: 'GL_ACCOUNT_ID', text: 'GL Account', type: 'string', filtertype: 'input' },
            { name: 'GL_ACCOUNT_DESC', text: 'GL Account Desc', width: '250px', type: 'string', filtertype: 'input' },
            { name: 'ENTRPS_PROD_ID', text: 'Enterprise Product', type: 'string', filtertype: 'input' },
            { name: 'ENTRPS_PROD_DESC', text: 'Enterprise Product Desc', width: '250px', type: 'string', filtertype: 'input' },
            { name: 'SUB_GL_FEED_ID', text: 'Sub GL Feed ID', type: 'string', filtertype: 'input' },
            { name: 'ICE_CODE', text: 'Ice Code', type: 'string', filtertype: 'input' },
            { name: 'PRIOR_BALANCE', text: 'Prior Quarter Balance', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' },
            { name: 'BALANCE', text: 'Balance', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' },
            { name: 'PRIOR_AVERAGE', text: 'Prior Quarterly Average', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' },
            { name: 'AVERAGE', text: 'Quarterly Average', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' }
        ],
        spName: "[dbo].[SP_FSQuarter_MTFullSuiteByGLAccount]",
        spParams: [
            { Name: "@Year", Value: getSelectedYear() },
            { Name: "@Quarter", Value: getSelectedQuarter() },
            { Name: "@BusinessIDs", Value: _getStrBusinessIDByUser() }
        ],
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        fnReady: function () { }
    };
};

//Full Suite Download with Filters
function getConfigViewFullSuiteDownloadWithFilters() {
    var columns = [];
    var spName = "";
    var source;
    var fnReady;

    //Set columns
    //columns.push({ name: "MT.ID", text: "MasterTableID", type: "int", filtertype: "number" });
    columns.push({ name: "YEAR", type: "int", hidden: true });
    columns.push({ name: "QUARTER", type: "int", hidden: true });
    columns.push({ name: "FRS_BSUNIT_ID", text: "BU", type: "int", filtertype: "number" });
    //columns.push({ name: "ISNULL(BS.ID,0)", text: "BusinessID", type: "int", filtertype: "number" });
    columns.push({ name: "BUSINESS_NAME", text: "Business", type: 'string', filtertype: "input" });

    columns.push({ name: "MANAGED_SEGMENT", text: "Managed Segment", type: 'int', filtertype: "number" });
    columns.push({ name: "MANAGED_SEGMENT_DESC", text: "Managed Segment Desc", type: 'string', filtertype: "input" });

    columns.push({ name: "LV_ID", text: "Legal Vehicle", type: 'string', filtertype: "input" });

    columns.push({ name: "GOC", text: "GOC", type: 'string', filtertype: "input" });
    columns.push({ name: "GL_ACCT_ID", text: "GL Account", type: 'string', filtertype: "input" });
    columns.push({ name: "GL_ACCT_DESC", text: "GL Account Desc", type: 'string', width: '180px', filtertype: "input" });

    //columns.push({ name: "MT.FDLAccountID", text: "BD FDL Account ID", type: 'int', filtertype: "number" });
    columns.push({ name: "FDL_ACCT_ID", text: "FDL Account", type: 'string', filtertype: "input" });
    columns.push({ name: "FDL_ACCT_DESC", text: "FDL Account Desc", type: 'string', filtertype: "input" });

    columns.push({ name: "ENTRPS_PROD_ID", text: "Enterprice Product", type: 'string', filtertype: "input" });
    columns.push({ name: "ENTRPS_PROD_DESC", text: "Enterprice Product Desc", type: 'string', filtertype: "input" });

    columns.push({ name: "SRC_SYS_ID", text: "Source System", type: 'string', filtertype: "input" });
    columns.push({ name: "SUB_GL_FEED_ID", text: "Journal Sub GL Feed", type: 'string', filtertype: "input" });
    columns.push({ name: "CPRT_GOC", text: "Counter Party GOC", type: 'string', filtertype: "input" });
    columns.push({ name: "AFFL_LV_ID", text: "Affiliate Legal Vehicle", type: 'string', filtertype: "input" });
    columns.push({ name: "AFFL_FRS_BSUNIT_ID", text: "Affiliate BU", type: 'string', filtertype: "input" });
    columns.push({ name: "ICE_CODE", text: "ICE Code", type: 'string', filtertype: "input" });
    columns.push({ name: "CUST_ID", text: "Customer ID", type: 'string', filtertype: "input" });
    //columns.push({ name: "T.Code", text: "Transaction Currency Code", type: 'string', filtertype: "input" });
    columns.push({ name: "GAAP_TYP_CD", text: "GAAP Type Code", type: 'string', filtertype: "input" });
    columns.push({ name: "ACTG_PRD_NO", text: "Account Period Number", type: 'string', filterable: false });
    columns.push({ name: "BSYTD_ISQTD_BALANCE", text: "Balance", type: 'float', filtertype: 'number', cellsformat: 'd' });
    columns.push({ name: "FULL_KEY", text: "Key", type: 'string', filtertype: "input" });

    //Set default filter by Year and Quarter
    var indexYear = _findIndexByProperty(columns, "name", "YEAR");
    var indexQuarter = _findIndexByProperty(columns, "name", "QUARTER");
    columns[indexYear].filter = getFilterGroup("Year");
    columns[indexQuarter].filter = getFilterGroup("Quarter");

    //Set Procedure
    spName = "[dbo].[SP_FSQuarter_MTFullSuiteDetailPaging]";

    //Set Source
    source = {
        // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
        dataBinding: "Server Paging",
        pagesize: 200
    };

    //Set on ready fuction
    fnReady = function () { };

    return {
        columns: columns,
        spName: spName,
        spParams: [
            { Name: "@Year", Value: getSelectedYear() },
            { Name: "@Quarter", Value: getSelectedQuarter() },
            { Name: "@BusinessIDs", Value: _getStrBusinessIDByUser() }
        ],
        source: source,
        fnReady: fnReady
    };
};

//Full Suite new GL Accounts
function getConfigViewFullSuiteNewGLAccounts() {
    return {
        columns: [
            { name: 'GL_ACCOUNT_ID', text: 'GL Account', width: '90px', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center' },
            { name: "GL_ACCOUNT_DESC", text: "GL Account Desc", type: 'string', width: '250px', filtertype: "input" },
            { name: 'ENTRPS_PROD_ID', text: 'Enterprise Product', width: '100px', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center' },
            { name: "ENTRPS_PROD_DESC", text: "Enterprice Product Desc", type: 'string', width: '250px', filtertype: "input" },
            { name: 'CUSTOMER_ID', text: 'Customer ID', width: '100px', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center' },
            { name: 'FULL_KEY', text: 'Full Key', width: '200px', type: 'string', filtertype: 'input' },
            { name: 'BALANCE', text: 'Balance', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' },
            { name: 'ROWS', text: 'Full Suite Rows', width: '100px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd', cellsalign: 'center', align: 'center' }
        ],
        spName: "[dbo].[SP_FSQuarter_MTFullSuiteNewGLAccount]",
        spParams: [
            { Name: "@Year", Value: getSelectedYear() },
            { Name: "@Quarter", Value: getSelectedQuarter() },
            { Name: "@BusinessIDs", Value: _getStrBusinessIDByUser() }
        ],
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        fnReady: function () { }
    };
};











/*
///====================================
$(document).ready(function () {
    //Group By
    $("#cboGroupBy").change(function () {
        loadTableGroupBy();

        //QMemo Accounts
        if ($("#cboGroupBy").val() == "QMemoAccount") {
            $("#btnViewManualTemplateDetails").show();
        } else {
            //Hide View Manual Template Balance Detail on FDL Account grouping
            $("#btnViewManualTemplateDetails").hide();
        }

        //Full Suite Custom Filters Download 
        if ($("#cboGroupBy").val() == "QMemoAccountDetailPaging" ||
            $("#cboGroupBy").val() == "GLAccount" ||
            $("#cboGroupBy").val() == "FullSuitePaging" ||
            $("#cboGroupBy").val() == "FullSuiteNewGLAccounts") {

            $("#btnViewMappedDetails").hide();

        } else {
            $("#btnViewMappedDetails").show();
        }

    });

    //Load Master table summary
    $("#cboPeriod").change(function () {
        loadTableGroupBy();
    });

    //Load Mapped details
    $("#btnViewMappedDetails").click(function () {
        loadTableMappedBalanceDetails();
    });

    //Load Manual Template details
    $("#btnViewManualTemplateDetails").click(function () {
        loadTableManualTempBalanceDetails();
    });

    //Export to excel create manually
    $("#btnExportMasterTable").click(function () {
        downloadMasterTableSummary();
    });

    $("#btnExportMasterTableDetail").click(function () {
        downloadFullSuiteDetail();
    });

    //Load Period
    loadPeriod(function () {
        //Load last quarter data
        loadTableGroupBy();

        //Load last full suite file information
        loadLastFullSuiteFileInfo();
    });
});

function hasFilters() {
    var hasFilter = false;

    //QMemo Filter
    if ($("#HFQMemoFilter").val()) {
        hasFilter = true;
    }

    //Business Filter
    if ($("#HFBusinessIDFilter").val()) {
        hasFilter = true;
    }

    return hasFilter;
}



function getFilterGroup(ptype) {
    var filterValue = "";
    var filterGroup = null;

    if (ptype == "Year") {
        filterValue = getSelectedYear();
        filterValue = filterValue ? filterValue : 0;
    }

    if (ptype == "Quarter") {
        filterValue = getSelectedQuarter();
        filterValue = filterValue ? filterValue : 0;
    }

    if (filterValue != null) {
        filterGroup = new $.jqx.filter();
        var filter_or_operator = 1;
        var filterCondition = 'equal';
        var filter = filterGroup.createfilter('stringfilter', filterValue, filterCondition);
        filterGroup.addfilter(filter_or_operator, filter);
    }

    return filterGroup;
};

function loadTableMappedBalanceDetails() {
    var $jqxGrid = $("#tbl-master-table");
    var rowIndex = $jqxGrid.jqxGrid('selectedrowindex');
    var dataRow = $jqxGrid.jqxGrid('getrowdata', rowIndex);

    if (dataRow) {
        var sql =
        "SELECT TOP 5000 " +
	    "     [QMEMO_ID] " +
        "    ,[QMEMO_ACCOUNT] " +
        "    ,[LV_ID] " +
        "    ,[FRS_BSUNIT_ID] " +
        "    ,[GOC] " +
        "    ,[GL_ACCT_ID] " +
        "    ,[FDL_ACCT_ID] " +
        "    ,[ENTRPS_PROD_ID] " +
        "    ,[SRC_SYS_ID] " +
        "    ,[SUB_GL_FEED_ID] " +
        "    ,[CPRT_GOC] " +
        "    ,[AFFL_LV_ID] " +
        "    ,[AFFL_FRS_BSUNIT_ID] " +
        "    ,[ICE_CODE] " +
        "    ,[CUST_ID] " +
        "    ,[TRAN_CCY_CD] " +
        "    ,[GAAP_TYP_CD] " +
        "    ,[BALANCE] " +
        "    ,[BUSINESS_ID] " +
        "    ,[BUSINESS_NAME] " +
        //"    ,ISNULL([BUSINESS_NAME], '" + defaultBusiness.BusinessName + "') BUSINESS_NAME " +
        "    ,[MANAGED_SEGMENT] " +
        "    ,[YEAR] " +
        "    ,[QUARTER] " +
        "    ,[ACTG_PRD_NO] " +
        "    ,[FULL_KEY] " +
	    "    ,[MANAGED_SEGMENT_DESC] AS MANAGED_SEGMENT_DESCRIPTION " +
        "FROM " +
        "   [dbo].[VW_MasterTableMapping] " +
        "WHERE " +
        "   [YEAR] = " + getSelectedYear() + " AND " +
        "   [QUARTER] = " + getSelectedQuarter() + " AND " +
        "   [BUSINESS_ID] = " + dataRow.BusinessID + " AND " +

        (($("#cboGroupBy").val() == "QMemoAccount") ? " QMEMO_ID = " + dataRow.QMemoID + " " : " [MT_FDL_ACCT_ID] = " + dataRow.FDLAccountID) + " ";

        //console.log(sql);

        $.jqxGridApi.create({
            showTo: "#tbl-master-table-details",
            options: {
                //for comments or descriptions
                height: "400",
                autoheight: false,
                autorowheight: false,
                selectionmode: "singlerow",
                showfilterrow: true,
                sortable: true,

                //Clear div of ShowTo, some times filters aren't working when another grid is created in same div
                cleardiv: true
            },
            sp: {
                SQL: sql
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
                dataBinding: "Large Data Set"
            },
            columns: [
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                { name: 'QMEMO_ACCOUNT', text: 'QMemo Account', width: '110px', type: 'string', filtertype: 'checkedlist' },
                { name: 'BUSINESS_NAME', text: 'Business', width: '150px', type: 'string', filtertype: 'checkedlist' },
                { name: 'MANAGED_SEGMENT', text: 'Managed Segment', width: '116px', type: 'string', filtertype: 'checkedlist' },
                { name: 'MANAGED_SEGMENT_DESCRIPTION', text: 'Managed Segment Description', width: '200px', type: 'string', filtertype: 'checkedlist' },
                { name: 'LV_ID', text: 'Legal Vehicle ID', type: 'string', filtertype: 'checkedlist' },
                { name: 'FRS_BSUNIT_ID', text: 'FRS Business Unit ID', type: 'string', filtertype: 'checkedlist' },
                { name: 'GOC', text: 'GOC', type: 'string', filtertype: 'checkedlist' },
                { name: 'GL_ACCT_ID', text: 'General Ledger Account ID', type: 'string', filtertype: 'checkedlist' },
                { name: 'FDL_ACCT_ID', text: 'FDL Account ID', type: 'string', filtertype: 'checkedlist' },
                { name: 'ENTRPS_PROD_ID', text: 'Enterprise Product ID', type: 'string', filtertype: 'checkedlist' },
                { name: 'SRC_SYS_ID', text: 'Source System ID', type: 'string', filtertype: 'checkedlist' },
                { name: 'SUB_GL_FEED_ID', text: 'Original Journal Sub GL Feed ID', type: 'string', filtertype: 'checkedlist' },
                { name: 'CPRT_GOC', text: 'CounterParty GOC', type: 'string', filtertype: 'checkedlist' },
                { name: 'AFFL_LV_ID', text: 'Affiliate Legal Vehicle ID', type: 'string', filtertype: 'checkedlist' },
                { name: 'AFFL_FRS_BSUNIT_ID', text: 'Affiliate FRS Business Unit ID', type: 'string', filtertype: 'checkedlist' },
                { name: 'ICE_CODE', text: 'ICE Code', type: 'string', filtertype: 'checkedlist' },
                { name: 'CUST_ID', text: 'Customer ID', type: 'string', filtertype: 'checkedlist' },
                { name: 'TRAN_CCY_CD', text: 'Transaction Currency Code', type: 'string', filtertype: 'checkedlist' },
                { name: 'GAAP_TYP_CD', text: 'GAAP Type Code', type: 'string', filtertype: 'checkedlist' },
                { name: 'BALANCE', text: 'Quarter Balance', type: 'float', filtertype: 'number', cellsformat: 'd' }
            ]
        });

        $("#master-detail-content").show();
    } else {
        $.showAlert({
            showTo: $jqxGrid.parent(),
            type: 'error',
            content: "Please select at least one row",
            animateScrollTop: false
        });
    }

}

function loadTableManualTempBalanceDetails() {
    var $jqxGrid = $("#tbl-master-table");
    var rowIndex = $jqxGrid.jqxGrid('selectedrowindex');
    var dataRow = $jqxGrid.jqxGrid('getrowdata', rowIndex);

    if (dataRow) {
        var sql =
            " SELECT " +
            "     [QMEMO_ID] " +
            "    ,[QMEMO_ACCOUNT] " +
            "    ,[FRS_BSUNIT_ID] " +
            "    ,[SOURCE] " +
            "    ,[BUSINESS_ID] " +
            "    ,[BUSINESS_NAME] " +
            "    ,[YEAR] " +
            "    ,[QUARTER] " +
            "    ,[BALANCE] " +
            " FROM [dbo].[VW_ManualTemplate] " +
            " WHERE QMEMO_ID = " + dataRow.QMemoID + " AND BUSINESS_ID = " + dataRow.BusinessID + " AND YEAR = " + getSelectedYear() + " AND QUARTER = " + getSelectedQuarter() + " ";

        //console.log(sql);

        $.jqxGridApi.create({
            showTo: "#tbl-master-table-details",
            options: {
                //for comments or descriptions
                height: "400",
                autoheight: false,
                autorowheight: false,
                selectionmode: "singlerow",
                showfilterrow: true,
                sortable: true
            },
            sp: {
                SQL: sql
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
                dataBinding: "Large Data Set"
            },
            columns: [
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                { name: 'FRS_BSUNIT_ID', text: 'FRS Business Unit ID', width: '15%', type: 'string', filtertype: 'checkedlist' },
                { name: 'QMEMO_ACCOUNT', text: 'QMemo Account', width: '20%', type: 'string', filtertype: 'checkedlist' },
                { name: 'BUSINESS_NAME', text: 'Business', width: '20%', type: 'string', filtertype: 'checkedlist' },
                { name: 'SOURCE', text: 'Source', type: 'string', width: '30%', filtertype: 'checkedlist' },
                { name: 'BALANCE', text: 'Balance', type: 'float', width: '15%', filtertype: 'number', cellsformat: 'd' }
            ]
        });

        $("#master-detail-content").show();
    } else {
        $.showAlert({
            showTo: $jqxGrid.parent(),
            type: 'error',
            content: "Please select at least one row",
            animateScrollTop: false
        });
    }
}

function getSelectedYear() {
    var selVal = $("#cboPeriod").val();
    return selVal.split("-")[0];
}

function getSelectedQuarter() {
    var selVal = $("#cboPeriod").val();
    return selVal.split("-")[1].replace("Q", "");
}

function downloadMasterTableSummary() {
    var spName;
    var spParams;
    var filename;

    //QMemo Accounts by Business
    if ($("#cboGroupBy").val() == "QMemoAccount") {
        spName = "[dbo].[SP_Mapping_ExcelMasterTableXQMemoSummary]";
        spParams = [
            { "Name": "@Year", "Value": getSelectedYear() },
            { "Name": "@Quarter", "Value": getSelectedQuarter() },
            { "Name": "@BusinessIDs", Value: _getStrBusinessIDByUser() }
        ];
        filename = _createCustomID() + "_MasterTable_" + getSelectedYear() + "-Q" + getSelectedQuarter() + "_QMemoAccounts.xls";
    }

    //QMemo Accounts Download with Filters
    if ($("#cboGroupBy").val() == "QMemoAccountDetailPaging") {
        var filterData = _getFiltersJqxGrid("#tbl-master-table");

        spName = "[SP_QM_ExcelQMemoBalancesDetailPaging]";
        spParams = [
            { "Name": "@StrWhere", "Value": _encodeSkipSideminder(filterData.Where) },
            { "Name": "@StrOrderBy", "Value": _encodeSkipSideminder(filterData.Sort) },
            { "Name": "@StartIndexRow", "Value": 0 },
            { "Name": "@EndIndexRow", "Value": 9999999 },
            { "Name": "@BusinessIDs", Value: _getStrBusinessIDByUser() }
        ];
        filename = _createCustomID() + "_FullSuiteData_" + getSelectedYear() + "-Q" + getSelectedQuarter() + ".xls";
    }

    //Full Suite by FDL Accounts
    if ($("#cboGroupBy").val() == "FDLAccount") {
        spName = "[SP_Mapping_ExcelMasterTableXFDLAccountSummary]";
        spParams = [
            { "Name": "@Year", "Value": getSelectedYear() },
            { "Name": "@Quarter", "Value": getSelectedQuarter() },
            { "Name": "@BusinessIDs", Value: _getStrBusinessIDByUser() }
        ];
        filename = _createCustomID() + "_MasterTable_" + getSelectedYear() + "-Q" + getSelectedQuarter() + "_FDLAccounts.xls";
    }

    //Full Suite by GL Accounts
    if ($("#cboGroupBy").val() == "GLAccount") {
        spName = "[SP_QM_ExcelFullSuiteXGLAccountSummary]";
        spParams = [
            { "Name": "@Year", "Value": getSelectedYear() },
            { "Name": "@Quarter", "Value": getSelectedQuarter() },
            { "Name": "@BusinessIDs", Value: _getStrBusinessIDByUser() }
        ];
        filename = _createCustomID() + "_MasterTable_" + getSelectedYear() + "-Q" + getSelectedQuarter() + "_GLAccounts.xls";
    }

    //Full Suite Download with Filters
    if ($("#cboGroupBy").val() == "FullSuitePaging") {
        var filterData = _getFiltersJqxGrid("#tbl-master-table");

        spName = "[SP_QM_ExcelFullSuiteListPaging]";
        spParams = [
            { "Name": "@StrWhere", "Value": _encodeSkipSideminder(filterData.Where) },
            { "Name": "@StrOrderBy", "Value": _encodeSkipSideminder(filterData.Sort) },
            { "Name": "@StartIndexRow", "Value": 0 },
            { "Name": "@EndIndexRow", "Value": 9999999 },
            { "Name": "@BusinessIDs", Value: _getStrBusinessIDByUser() }
        ];
        filename = _createCustomID() + "_FullSuiteData_" + getSelectedYear() + "-Q" + getSelectedQuarter() + ".xls";
    }

    //Full Suite new GL Accounts
    if ($("#cboGroupBy").val() == "FullSuiteNewGLAccounts") {
        var filterData = _getFiltersJqxGrid("#tbl-master-table");

        spName = "[SP_QM_FullSuiteXNewGLAccounts]";
        spParams = [
            { "Name": "@Year", "Value": getSelectedYear() },
            { "Name": "@Quarter", "Value": getSelectedQuarter() },
            { "Name": "@BusinessIDs", Value: _getStrBusinessIDByUser() }
        ];
        filename = _createCustomID() + "_FullSuiteData_NewGLAccounts_" + getSelectedYear() + "-Q" + getSelectedQuarter() + ".xls";
    }

    _callProcedure({
        name: spName,
        params: spParams,
        response: true,
        download: true,
        filename: filename,
        success: {
            msg: "Generating report... Please Wait, this operation may take some time to complete."
        }
    });
};

function downloadFullSuiteDetail() {
    var dataRecord = $.jqxGridApi.getOneSelectedRow("#tbl-master-table", true);
    if (dataRecord) {
        _callProcedure({
            name: $("#cboGroupBy").val() == "QMemoAccount" ? "[dbo].[SP_Mapping_ExcelMasterTableXQMemoDetail]" : "[SP_Mapping_ExcelMasterTableXFDLAccountDetail]",
            params: [
                { "Name": "@Year", "Value": getSelectedYear() },
                { "Name": "@Quarter", "Value": getSelectedQuarter() },
                { "Name": ($("#cboGroupBy").val() == "QMemoAccount" ? "@QMemoID" : "@FDLAccountID"), "Value": ($("#cboGroupBy").val() == "QMemoAccount" ? dataRecord.QMemoID : dataRecord.FDLAccountID) },
                { "Name": "@BusinessID", "Value": dataRecord.BusinessID }
            ],
            response: true,
            download: true,
            filename: _createCustomID() + "_MasterTable_" + getSelectedYear() + "-Q" + getSelectedQuarter() + "_" + ($("#cboGroupBy").val() == "QMemoAccount" ? dataRecord.QMemoAccount : dataRecord.FDLAccount) + "_" + dataRecord.Business + ".xls",
            success: {
                msg: "Generating report... Please Wait, this operation may take some time to complete."
            }
        });
    }
};

function loadLastFullSuiteFileInfo() {
    _callProcedure({
        response: true,
        name: "[dbo].[SP_Mapping_LastFullSuiteFileInfo]",
        params: [],
        showBeforeSendMsg: false,
        success: {
            fn: function (responseList) {
                if (responseList.length == 1) {
                    var data = responseList[0];
                    console.log(data);
                    $("#headerTitle").text("Master table - Last Full Suite File: Year " + data["Year"] + ", Accounting Period " + data["Month"] + ", Quarter " + data["Quarter"] + " on " + _formatDate(new Date(data["CreatedDate"]), "MMM d yyyy hh:mm TT"));
                }
            }
        }
    });
};

//-----------------------------------------------------------------------
// VIEW SUMMARY BY
//-----------------------------------------------------------------------
function loadTableGroupBy() {
    var gridConfig;

    //QMemo Accounts by Business
    if ($("#cboGroupBy").val() == "QMemoAccount") {
        gridConfig = getConfigViewQMemoAccountsByBusiness();
    }

    //QMemo Accounts Download with Filters
    if ($("#cboGroupBy").val() == "QMemoAccountDetailPaging") {
        gridConfig = getConfigViewQMemoAccountsDownloadWithFilters();
    }

    //Full Suite by FDL Accounts
    if ($("#cboGroupBy").val() == "FDLAccount") {
        gridConfig = getConfigViewFullSuiteByFDLAccounts();
    }

    //Full Suite by GL Accounts
    if ($("#cboGroupBy").val() == "GLAccount") {
        gridConfig = getConfigViewFullSuiteByGLAccounts();
    }

    //Full Suite Download with Filters
    if ($("#cboGroupBy").val() == "FullSuitePaging") {
        gridConfig = getConfigViewFullSuiteDownloadWithFilters();
    }

    //Full Suite new GL Accounts
    if ($("#cboGroupBy").val() == "FullSuiteNewGLAccounts") {
        gridConfig = getConfigViewFullSuiteNewGLAccounts();
    }

    $.jqxGridApi.create({
        showTo: "#tbl-master-table",
        options: {
            //for comments or descriptions
            height: hasFilters() ? "200" : "400",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: true,
            sortable: true,
            editable: true,
            //Clear div of ShowTo, some times filters aren't working when another grid is created in same div
            cleardiv: true
        },
        sp: {
            Name: gridConfig.spName,
            Params: gridConfig.spParams
        },
        source: gridConfig.source,
        //type: string - text - number - int - float - date - time 
        //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
        //cellsformat: ddd, MMM dd, yyyy h:mm tt, c2
        columns: gridConfig.columns,
        ready: gridConfig.fnReady
    });
}

//QMemo Accounts by Business
function getConfigViewQMemoAccountsByBusiness() {
    return {
        columns: [
            { name: 'QMemoID', type: 'float', hidden: true },
            { name: 'QMemoAccount', text: 'QMemo Account', type: 'string', width: '15%', filtertype: 'input' },
            { name: 'BusinessID', type: 'float', hidden: true },
            { name: 'Business', text: 'Business', type: 'string', width: '15%', filtertype: 'checkedlist' },
            { name: 'FullSuiteMappedBalance', text: 'Mapped Balance', type: 'float', sortable: true, width: '13%', filtertype: 'number', cellsformat: 'd' },
            { name: 'ManualTemplateBalance', text: 'Offline Template Balance', type: 'float', sortable: true, width: '13%', filtertype: 'number', cellsformat: 'd' },
            { name: 'TotalBalance', text: 'Total Balance', type: 'float', sortable: true, width: '12%', filtertype: 'number', cellsformat: 'd' },
            { name: 'PreviousQuarterBalance', text: 'Previous Balance', type: 'float', sortable: true, width: '12%', filtertype: 'number', cellsformat: 'd' },
            { name: 'QoQVariance', text: 'QoQ Variance', type: 'float', sortable: true, width: '12%', filtertype: 'number', cellsformat: 'd' },
            { name: 'FullSuiteRows', text: 'Rows', width: '100px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' }
        ],
        spName: "[dbo].[SP_Mapping_MasterTableXQMemoSummary]",
        spParams: [
            { Name: "@Year", Value: getSelectedYear() },
            { Name: "@Quarter", Value: getSelectedQuarter() },
            { Name: "@BusinessIDs", Value: _getStrBusinessIDByUser() }
        ],
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        fnReady: function () {
            var $jqxGrid = $("#tbl-master-table");

            //Validate query string filters "QM_MasterTable.aspx?pqmemo=QM80065200&pbusiness=USCCB"
            var hasFilter = false;

            //QMemo Filter
            if ($("#HFQMemoFilter").val()) {
                var qmemoFilterGroup = getFilterGroup("QMemoAccount");
                $jqxGrid.jqxGrid('addfilter', "QMemoAccount", qmemoFilterGroup);
                hasFilter = true;
            }

            //Business Filter
            if ($("#HFBusinessIDFilter").val()) {
                var businessFilterGroup = getFilterGroup("BusinessID");
                $jqxGrid.jqxGrid('addfilter', "BusinessID", businessFilterGroup);
                hasFilter = true;
            }

            // apply the filters.
            if (hasFilter) {
                $jqxGrid.jqxGrid('applyfilters');
            }

            //Select first row by default
            var rows = $jqxGrid.jqxGrid("getrows");
            if (hasFilters() && rows.length > 0) {
                var defaulRow = rows[0];
                $jqxGrid.jqxGrid('selectrow', defaulRow.uid);

                //Load mapped details
                $("#btnViewMappedDetails").click();
            }

            function getFilterGroup(ptype) {
                var filterValue = "";
                var filterGroup = null;

                if (ptype == "QMemoAccount") {
                    filterValue = $("#HFQMemoFilter").val();
                }

                if (ptype == "BusinessID") {
                    filterValue = $("#HFBusinessIDFilter").val();
                }

                if (filterValue != null) {
                    filterGroup = new $.jqx.filter();
                    var filter_or_operator = 1;
                    var filterCondition = 'equal';
                    var filter = filterGroup.createfilter('stringfilter', filterValue, filterCondition);
                    filterGroup.addfilter(filter_or_operator, filter);
                }

                return filterGroup;
            };
        }
    };
};

//QMemo Accounts Download with Filters
function getConfigViewQMemoAccountsDownloadWithFilters() {
    var columns = [];
    var spName = "";
    var source;
    var fnReady;

    //Set columns
    columns.push({ name: "YEAR", type: "int", hidden: true });
    columns.push({ name: "QUARTER", type: "int", hidden: true });
    columns.push({ name: "BUSINESS_NAME", text: "Business", type: 'string', filtertype: "input" });
    columns.push({ name: "QMEMO_ACCOUNT", text: "QMemo Account", type: 'string', filtertype: "input" });
    columns.push({ name: "QMEMO_ACCOUNT_DESC", text: "QMemo Account Desc", type: 'string', width: '180px', filtertype: "input" });
    columns.push({ name: "GL_ACCT_ID", text: "GL Account", type: 'string', filtertype: "input" });
    columns.push({ name: "GL_ACCT_DESC", text: "GL Account Desc", type: 'string', width: '180px', filtertype: "input" });
    columns.push({ name: "FDL_ACCT_ID", text: "FDL Account", type: 'string', filtertype: "input" });
    columns.push({ name: "FDL_ACCT_DESC", text: "FDL Account Desc", type: 'string', width: '180px', filtertype: "input" });
    columns.push({ name: "GOC", text: "GOC", type: 'string', filtertype: "input" });
    columns.push({ name: "ENTRPS_PROD_ID", text: "Enterprice Product", type: 'string', filtertype: "input" });
    columns.push({ name: "ENTRPS_PROD_DESC", text: "Enterprice Product Desc", type: 'string', filtertype: "input" });
    columns.push({ name: "CUST_ID", text: "Customer ID", type: 'string', filtertype: "input" });
    columns.push({ name: "SUB_GL_FEED_ID", text: "SubGL Feed ID", type: 'string', filtertype: "input" });
    columns.push({ name: "AFFL_FRS_BSUNIT_ID", text: "Affiliate BU", type: 'string', filtertype: "input" });
    columns.push({ name: "ICE_CODE", text: "ICE Code", type: 'string', filtertype: "input" });
    columns.push({ name: "BALANCE", text: "Balance", type: 'float', filtertype: 'number', cellsformat: 'd' });

    //Set default filter by Year and Quarter
    var indexYear = _findIndexByProperty(columns, "name", "YEAR");
    var indexQuarter = _findIndexByProperty(columns, "name", "QUARTER");
    columns[indexYear].filter = getFilterGroup("Year");
    columns[indexQuarter].filter = getFilterGroup("Quarter");

    //Set Procedure
    spName = "[dbo].[SP_QM_QMemoBalancesDetailPaging]";

    //Set Source
    source = {
        // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
        dataBinding: "Server Paging",
        pagesize: 200
    };

    //Set on ready fuction
    fnReady = function () { };

    return {
        columns: columns,
        spName: spName,
        spParams: [
            { Name: "@BusinessIDs", Value: _getStrBusinessIDByUser() }
        ],
        source: source,
        fnReady: fnReady
    };
};

//Full Suite by FDL Accounts
function getConfigViewFullSuiteByFDLAccounts() {
    return {
        columns: [
            { name: 'FDLAccountID', type: 'float', hidden: true },
            { name: 'FDLAccount', text: 'FDL Account', type: 'string', width: '15%', filtertype: 'input' },
            { name: 'BusinessID', type: 'float', hidden: true },
            { name: 'Business', text: 'Business', type: 'string', width: '15%', filtertype: 'checkedlist' },
            { name: 'PrevQuarterToDateBalance', text: 'Previous QTD', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' },
            { name: 'CurrentQuarterToDateBalance', text: 'QTD', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' },
            { name: 'PrevQuarterToDateAverage', text: 'Previous QTD Average', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' },
            { name: 'CurrentQuarterToDateAverage', text: 'QTD Average', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' },
            { name: 'PrevYearToDateBalance', text: 'Previous YTD', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' },
            { name: 'CurrentYearToDateBalance', text: 'YTD', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' },
            { name: 'VarianceYearToDateBalance', text: 'QoQ YTD Variance', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' },
            { name: 'FullSuiteRows', text: 'Rows', width: '100px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' }
        ],
        spName: "[dbo].[SP_Mapping_MasterTableXFDLAccountSummary]",
        spParams: [
            { Name: "@Year", Value: getSelectedYear() },
            { Name: "@Quarter", Value: getSelectedQuarter() },
            { Name: "@BusinessIDs", Value: _getStrBusinessIDByUser() }
        ],
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        fnReady: function () { }
    };
};

//Full Suite by GL Accounts
function getConfigViewFullSuiteByGLAccounts() {
    return {
        columns: [
            { name: 'BUSINESS_NAME', text: 'Business', type: 'string', filtertype: 'checkedlist' },
            { name: 'FDL_ACCOUNT_ID', text: 'FDL Account', type: 'string', filtertype: 'input' },
            { name: 'FDL_ACCOUNT_DESC', text: 'FDL Account Desc', width: '250px', type: 'string', filtertype: 'input' },
            { name: 'GL_ACCOUNT_ID', text: 'GL Account', type: 'string', filtertype: 'input' },
            { name: 'GL_ACCOUNT_DESC', text: 'GL Account Desc', width: '250px', type: 'string', filtertype: 'input' },
            { name: 'ENTRPS_PROD_ID', text: 'Enterprise Product', type: 'string', filtertype: 'input' },
            { name: 'ENTRPS_PROD_DESC', text: 'Enterprise Product Desc', width: '250px', type: 'string', filtertype: 'input' },
            { name: 'SUB_GL_FEED_ID', text: 'Sub GL Feed ID', type: 'string', filtertype: 'input' },
            { name: 'ICE_CODE', text: 'Ice Code', type: 'string', filtertype: 'input' },
            { name: 'PRIOR_BALANCE', text: 'Prior Quarter Balance', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' },
            { name: 'BALANCE', text: 'Balance', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' },
            { name: 'PRIOR_AVERAGE', text: 'Prior Quarterly Average', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' },
            { name: 'AVERAGE', text: 'Quarterly Average', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' }
        ],
        spName: "[dbo].[SP_QM_FullSuiteXGLAccountSummary]",
        spParams: [
            { Name: "@Year", Value: getSelectedYear() },
            { Name: "@Quarter", Value: getSelectedQuarter() },
            { Name: "@BusinessIDs", Value: _getStrBusinessIDByUser() }
        ],
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        fnReady: function () { }
    };
};

//Full Suite Download with Filters
function getConfigViewFullSuiteDownloadWithFilters() {
    var columns = [];
    var spName = "";
    var source;
    var fnReady;

    //Set columns
    //columns.push({ name: "MT.ID", text: "MasterTableID", type: "int", filtertype: "number" });
    columns.push({ name: "MT.FiscalYear", type: "int", hidden: true });
    columns.push({ name: "MT.Quarter", type: "int", hidden: true });
    columns.push({ name: "B.FRS_BSUNIT_ID", text: "BU", type: "int", filtertype: "number" });
    //columns.push({ name: "ISNULL(BS.ID,0)", text: "BusinessID", type: "int", filtertype: "number" });
    columns.push({ name: "ISNULL(BS.Name,'TBD')", text: "Business", type: 'string', filtertype: "input" });

    columns.push({ name: "BM.ManagedSegment", text: "Managed Segment", type: 'int', filtertype: "number" });
    columns.push({ name: "BM.ManagedSegmentDesc", text: "Managed Segment Desc", type: 'string', filtertype: "input" });

    columns.push({ name: "L.LV_ID", text: "Legal Vehicle", type: 'string', filtertype: "input" });

    columns.push({ name: "MT.GOC", text: "GOC", type: 'string', filtertype: "input" });
    columns.push({ name: "GL.GL_ACCT_ID", text: "GL Account", type: 'string', filtertype: "input" });
    columns.push({ name: "GL.Description", text: "GL Account Desc", type: 'string', width: '180px', filtertype: "input" });

    //columns.push({ name: "MT.FDLAccountID", text: "BD FDL Account ID", type: 'int', filtertype: "number" });
    columns.push({ name: "FA.FDL_ACCT_ID", text: "FDL Account", type: 'string', filtertype: "input" });
    columns.push({ name: "FA.Description", text: "FDL Account Desc", type: 'string', filtertype: "input" });

    columns.push({ name: "E.ENTRPS_PROD_ID", text: "Enterprice Product", type: 'string', filtertype: "input" });
    columns.push({ name: "E.Description", text: "Enterprice Product Desc", type: 'string', filtertype: "input" });

    columns.push({ name: "S.SRC_SYS_ID", text: "Source System", type: 'string', filtertype: "input" });
    columns.push({ name: "JS.SUB_GL_FEED_ID", text: "Journal Sub GL Feed", type: 'string', filtertype: "input" });
    columns.push({ name: "MT.CounterPartyGOC", text: "Counter Party GOC", type: 'string', filtertype: "input" });
    columns.push({ name: "AL.LV_ID", text: "Affiliate Legal Vehicle", type: 'string', filtertype: "input" });
    columns.push({ name: "BU.FRS_BSUNIT_ID", text: "Affiliate BU", type: 'string', filtertype: "input" });
    columns.push({ name: "BU.ICE_CODE", text: "ICE Code", type: 'string', filtertype: "input" });
    columns.push({ name: "C.CUST_ID", text: "Customer ID", type: 'string', filtertype: "input" });
    //columns.push({ name: "T.Code", text: "Transaction Currency Code", type: 'string', filtertype: "input" });
    columns.push({ name: "G.Code", text: "GAAP Type Code", type: 'string', filtertype: "input" });
    columns.push({ name: "MT.AccountPeriodNumber", text: "Account Period Number", type: 'string', filterable: false });
    columns.push({ name: "MT.BSYTD_ISQTD_BalAmout", text: "Balance", type: 'float', filtertype: 'number', cellsformat: 'd' });
    columns.push({ name: "(GL.GL_ACCT_ID+' '+E.ENTRPS_PROD_ID+' '+C.CUST_ID)", text: "Key", type: 'string', filtertype: "input" });

    //Set default filter by Year and Quarter
    var indexYear = _findIndexByProperty(columns, "name", "MT.FiscalYear");
    var indexQuarter = _findIndexByProperty(columns, "name", "MT.Quarter");
    columns[indexYear].filter = getFilterGroup("Year");
    columns[indexQuarter].filter = getFilterGroup("Quarter");

    //Set Procedure
    spName = "[dbo].[SP_QM_FullSuiteListPaging]";

    //Set Source
    source = {
        // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
        dataBinding: "Server Paging",
        pagesize: 200
    };

    //Set on ready fuction
    fnReady = function () { };

    return {
        columns: columns,
        spName: spName,
        spParams: [
            { Name: "@BusinessIDs", Value: _getStrBusinessIDByUser() }
        ],
        source: source,
        fnReady: fnReady
    };
};

//Full Suite new GL Accounts
function getConfigViewFullSuiteNewGLAccounts() {
    return {
        columns: [
            { name: 'GL_ACCT_ID', text: 'GL Account', width: '15%', type: 'string', filtertype: 'input' },
            { name: 'ENTRPS_PROD_ID', text: 'Enterprise Product', width: '15%', type: 'string', filtertype: 'input' },
            { name: 'CUST_ID', text: 'Customer ID', width: '15%', type: 'string', filtertype: 'input' },
            { name: 'FULL_KEY', text: 'Full Key', width: '32%', type: 'string', filtertype: 'input' },
            { name: 'BALANCE', text: 'Balance', width: '180px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' },
            { name: 'ROWS', text: 'Full Suite Rows', width: '100px', type: 'float', sortable: true, filtertype: 'number', cellsformat: 'd' }
        ],
        spName: "[dbo].[SP_QM_FullSuiteXNewGLAccounts]",
        spParams: [
            { Name: "@Year", Value: getSelectedYear() },
            { Name: "@Quarter", Value: getSelectedQuarter() },
            { Name: "@BusinessIDs", Value: _getStrBusinessIDByUser() }
        ],
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        fnReady: function () { }
    };
};
*/