﻿/// <reference path="../../Shared/plugins/util/global.js" />

var _BUSINESS = null;
var _IS_FULLSUITE_FILE_PROCESSING = false;
var _IGNORE_FULL_SUITE_FILE = false;

function _getBusiness() {
    return _BUSINESS;
};

function _isAdminRole() {
    return (_userHaveRole("QMEMO_ADMIN") || _userHaveRole("QMEMO_SUPER_ADMIN"));
}

function _getArrayBusinessByUser() {
    var arrayBusiness = [];
    //Admin have access to all business
    if (_isAdminRole()) {
        var businessList = _getBusiness();
        for (var i = 0; i < businessList.length; i++) {
            arrayBusiness.push({
                BusinessCode: businessList[i]["BusinessCode"],
                BusinessID: businessList[i]["BusinessID"],
                BusinessName: businessList[i]["BusinessName"],
                ManualTempManagedSeg: businessList[i]["ManualTempManagedSeg"],
                NeedFullSuiteData: businessList[i]["NeedFullSuiteData"],
            });
        }
    } else {
        var strRoles = _getUserInfo().Roles;
        var tempBusiness = null;

        strRoles = _replaceAll("QMEMO_ADMIN,", "", strRoles);
        strRoles = _replaceAll("QMEMO_SUPER_ADMIN,", "", strRoles);
        strRoles = _replaceAll("QMEMO_UPLOAD_EDIT_CHECK,", "", strRoles);
        strRoles = _replaceAll("QMEMO_", "", strRoles);
        strRoles = _replaceAll("_ADMIN", "", strRoles);
        strRoles = _replaceAll("_ANALYST", "", strRoles);
        strRoles = _replaceAll("_READ_ONLY", "", strRoles);

        arrayBusinessCode = strRoles.split(",");

        for (var i = 0; i < arrayBusinessCode.length; i++) {
            tempBusiness = _getBusinessByCode(arrayBusinessCode[i]);
            if (tempBusiness) {
                arrayBusiness.push({
                    BusinessCode: tempBusiness["BusinessCode"],
                    BusinessID: tempBusiness["BusinessID"],
                    BusinessName: tempBusiness["BusinessName"],
                    ManualTempManagedSeg: tempBusiness["ManualTempManagedSeg"],
                    NeedFullSuiteData: tempBusiness["NeedFullSuiteData"],
                });
            }
        }
    }
    return arrayBusiness;
}

function _getStrBusinessIDByUser(addNoMappedBusiness) {
    var arrayBusiness = _getArrayBusinessByUser();
    var strBusinessIDs = "";

    for (var i = 0; i < arrayBusiness.length; i++) {
        strBusinessIDs += arrayBusiness[i]["BusinessID"] + ",";
    }

    //Remove last char
    if (strBusinessIDs) {
        strBusinessIDs = strBusinessIDs.substring(0, strBusinessIDs.length - 1);
    }

    //Is used in Master Table Page, to show Business ID = 0
    if (_isAdminRole()) {
        strBusinessIDs = strBusinessIDs + ",0"
    }

    //if (addNoMappedBusiness) {
    //    if (strBusinessIDs) {
    //        strBusinessIDs + ",0"
    //    } else {
    //        strBusinessIDs + "0"
    //    }
    //}

    return strBusinessIDs;
}

function _checkFullSuiteFileIsProcessing() {
    //Note: _ajaxIsComplete() call only when all ajax request are complete to avoid many call
    if (_IS_FULLSUITE_FILE_PROCESSING == false && _ajaxIsComplete() && _IGNORE_FULL_SUITE_FILE == false) {
        _callProcedure({
            response: true,
            showBeforeSendMsg: false,
            name: "[dbo].[SP_Mapping_IsFullSuiteFileProcessing]",
            params: [],
            success: {
                fn: function (response) {
                    if (response.length > 0) {
                        var idProcessRunning = response[0]["ProcessRunningID"];
                        _IS_FULLSUITE_FILE_PROCESSING = true;

                        _showAlert({
                            id: "fullSuiteProgress",
                            type: 'info',
                            title: "<div id='titleProcessRunning'>Full Suite is updating (Validating period)...</div>",
                            content: "Please wait, until this process is completed... <br/><br/><div class='progress progress-success progress-striped active'><div class='bar' id='progressValue' style='width: 0%; line-height: 20px;'></div></div> <a href='#' id='viewProcessDetail'> See Details </a> ",
                            animateScrollTop: true
                        });

                        //View process detail
                        $("#viewProcessDetail").click(function () {
                            //Create popup with the process detail

                            _showModal({
                                title: "<div id='popupTitleProcessRunning'>" + $("#titleProcessRunning").text() + "</div>",
                                width: "80%",
                                contentHtml: "<div id='tbl-process-running-detail'></div>",
                                onReady: function ($modal) {
                                    $.jqxGridApi.create({
                                        showTo: "#tbl-process-running-detail",
                                        options: {
                                            //for comments or descriptions
                                            height: "400",
                                            autoheight: false,
                                            autorowheight: false,
                                            selectionmode: "singlerow",
                                            showfilterrow: false,
                                            sortable: true,
                                            editable: true
                                        },
                                        sp: {
                                            Name: "[dbo].[SP_Mapping_ProcessRunningDetail]",
                                            Params: [
                                                { Name: "@ProcessRunningID", Value: idProcessRunning }
                                            ]
                                        },
                                        source: {
                                            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
                                            dataBinding: "Large Data Set"
                                        },
                                        columns: [
                                            //type: string - text - number - int - float - date - time 
                                            //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                                            //cellsformat: ddd, MMM dd, yyyy h:mm tt
                                            { name: 'IsCompleted', type: 'number', hidden: true },
                                            { name: 'Name', text: 'Name', width: '60%', type: 'string', filtertype: 'input', editable: false },
                                            { name: 'Baseline', text: 'Baseline', width: '10%', type: 'string', filtertype: 'input', editable: false },
                                            { name: 'FormatTime', text: 'Time [min:sec:mil]', width: '15%', type: 'string', filtertype: 'input', editable: false },
                                            { name: 'Status', text: 'Status', width: '15%', type: 'string', filtertype: 'input', editable: false }
                                            //{
                                            //    name: 'Status', computedcolumn: true, text: 'Status', type: 'html', width: 100, cellsrenderer: function (rowIndex, dataField, value) {
                                            //        var dataRecord = $("#tbl-process-running-detail").jqxGrid('getrowdata', rowIndex);
                                            //        var hmlView = "Pending...";

                                            //        //Validate Business Access for the user
                                            //        if (_userHaveBusinessID(dataRecord.IsCompleted)) {
                                            //            //hmlView = '<div style="text-align: center;" class="view-btn" ><img onclick="addOrEditSelectedCase(&#39;Edit&#39;)" style="margin-left: 2px; margin-top: 3px;" height="20" width="20" src="App_Themes/Base/images/edit.png"/></div>';
                                            //            var hmlView = "Completed";
                                            //        }

                                            //        return hmlView;
                                            //    }
                                            //}

                                        ]
                                    });
                                }
                            });
                        });

                        //Create interval to refresh progress bar
                        var lastCallProcessRunningStatusDone = true;
                        var intervalProgressUpdate = setInterval(function () {
                            if (!idProcessRunning) {
                                _IS_FULLSUITE_FILE_PROCESSING = false;
                                if (intervalProgressUpdate) {
                                    clearInterval(intervalProgressUpdate);
                                }
                            } else {
                                if (lastCallProcessRunningStatusDone == true) {
                                    lastCallProcessRunningStatusDone = false;
                                    _callProcedure({
                                        response: true,
                                        loadingMsgType: "topBar",
                                        loadingMsg: "Checking Full Suite Status...",
                                        name: "[dbo].[SP_Mapping_GetProcessRunningStatus]",
                                        params: [
                                            { Name: "@ProcessRunningID", Value: idProcessRunning }
                                        ],
                                        success: {
                                            fn: function (response) {
                                                lastCallProcessRunningStatusDone = true;
                                                if (response.length > 0) {
                                                    var progressValue = response[0]["ProcessPercentage"];
                                                    var year = response[0]["Year"];
                                                    var month = response[0]["Month"];
                                                    var quarter = response[0]["Quarter"];

                                                    $("#progressValue").css("width", progressValue + "%");
                                                    $("#progressValue").text(progressValue + " % Completed");

                                                    //update title
                                                    if (year) {
                                                        $("#titleProcessRunning").text("Full Suite data is updating (Year " + year + ", Accounting Period " + month + ", Quarter " + quarter + ")...");
                                                        $("#popupTitleProcessRunning").text("Full Suite data is updating (Year " + year + ", Accounting Period " + month + ", Quarter " + quarter + ") " + progressValue + " % Completed");
                                                    }

                                                    //Refresh data details
                                                    if ($("#tbl-process-running-detail").length == 1) {
                                                        $.jqxGridApi.localStorageFindById("#tbl-process-running-detail").fnReload();
                                                    }

                                                    if (progressValue == 100) {
                                                        _showAlert({
                                                            id: "fullSuiteProgress",
                                                            type: 'success',
                                                            title: "<div id='titleProcessRunning'>Full Suite data is updating (Validating period) ✓</div>",
                                                            content: "<span id='subTitleProcessRunning'>Please wait, until this process is completed...</span> <br/><br/><div class='progress progress-success progress-striped active'><div class='bar' id='progressValue' style='width: 100%; line-height: 20px;'></div></div> ",
                                                            animateScrollTop: true
                                                        });
                                                        $("#titleProcessRunning").text("Full Suite data updated (Year " + year + ", Accounting Period " + month + ", Quarter " + quarter + ") ✓");
                                                        $("#subTitleProcessRunning").html("Full Suite data was successfully saved please refresh the page <b>(Press F5)</b> to see new balance mapped.");

                                                        _IS_FULLSUITE_FILE_PROCESSING = false;
                                                        clearInterval(intervalProgressUpdate);
                                                    }
                                                }

                                            }
                                        }
                                    });
                                }

                            }
                        }, 5000);
                    }
                }
            }
        });
    }
}

function _loadBusiness(onSuccess) {
    if (_BUSINESS == null) {
        _callProcedure({
            loadingMsgType: "fullLoading",
            loadingMsg: "Loadin Business...",
            response: true,
            name: "[dbo].[SP_FSQuarter_BusinessList]",
            params: [],
            success: {
                fn: function (responseBusinessList) {
                    _BUSINESS = responseBusinessList;
                    if (onSuccess) {
                        onSuccess(_BUSINESS);
                    }
                }
            }
        });
    } else {
        if (onSuccess) {
            onSuccess(_BUSINESS);
        }
    }
}

//Load Business
_loadBusiness();

//Check Full Suite File
var intervalCheckFullSuiteFile = setInterval(function () {
    //_console("checkFullSuiteFileIsProcessing");

    //Checking if some full suite file is processing
    _checkFullSuiteFileIsProcessing();
}, 30000);