//@ sourceURL=ProcessList.js
/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="SupportDocProdCtrlGlobal.js" />

$(document).ready(function () {

    //---------------------------------------------------
    //SUPPORT DOCUMENTATION PRODUCT AND CONTROL FUNCTIONS
    //---------------------------------------------------
    //Hide Menu Only when is a Responsible View
    _hideMenu();

    //Set default value for Process Date
    if ($("#HFProcessDate").val()) {
        $("#processDate").val(moment($("#HFProcessDate").val()).format('MMM DD, YYYY'));
    } else {
        var mommentProcessDate = moment().add(-1, 'd');
        var lastDay = mommentProcessDate.format('dddd');
        if (lastDay == "Saturday") {
            mommentProcessDate = mommentProcessDate.add(-1, 'd');
        }
        if (lastDay == "Sunday") {
            mommentProcessDate = mommentProcessDate.add(-2, 'd');
        }
        $("#processDate").val(mommentProcessDate.format('MMM DD, YYYY'));
    }
    
    //Create Datepicker and add Change Date event
    $("#processDate").datepicker().on('changeDate', function (ev) {
        loadTableProcessFiles();
    });

    //Set default value for Frequency
    if ($("#HFIDFrequency").val()) {
        $("#selFrequency").val($("#HFIDFrequency").val());
    }

    //On Change Frequency
    $("#selFrequency").change(function () {
        var selFrequency = $("#selFrequency").find("option:selected").val();

        //Daily
        if (selFrequency == "1") {
            $(".fieldSelYear").hide();
            $(".fieldSelMonth").hide();
            $(".fieldSelProcessDate").show();
        }

        //Montly
        if (selFrequency == "2") {
            $(".fieldSelYear").show();
            $(".fieldSelMonth").show();
            $(".fieldSelProcessDate").hide();
        }

        //Refresh filters Maker and Checker by Frequency
        loadViewType();

        //Refresh Process Files table
        loadTableProcessFiles();
    });
    
    //Set default value for Year
    if ($("#HFYear").val()) {
        $("#selYear").val($("#HFYear").val());
    }

    //Set default value for Month
    if ($("#HFMonth").val()) {
        $("#selMonth").val($("#HFMonth").val());
    } else {
        $("#selMonth").val(moment().format('MMMM'));
    }

    //On Change Month Field
    $("#selMonth").change(function () {
        loadTableProcessFiles();
    });
    
    //On click upload document
    $(".btnUploadDoc").click(function () {
        uploadDoc();
    });

    //On Click upload Extra Documents
    $(".btnUploadExtraDoc").click(function () {
        uploadExtraDoc();
    });

    //On click approve document
    $(".btnApprove").click(function () {
        approveDoc();
    });

    //On click reject document
    $(".btnReject").click(function () {
        rejectDoc();
    });

    //On Click Send Email
    $(".btnSendEmail").click(function () {
        var typeNotification = $(".selTypeNotification").val();

        if (typeNotification == "Stakeholder") {
            showModalSendEmailStakeholder();
        } else {
            showModalSendEmailMaker(typeNotification);
        }
    });

    //On Change Select of Type of Notification
    $(".selTypeNotification").change(function () {
        var typeNotification = $(".selTypeNotification").val();

        if (typeNotification == "Stakeholder") {
            //Show only approved process files
            $("#txtStatusByChecker").val("Approved");

            //Reload Table
            loadTableProcessFiles();
        } else {
            //Get old value
            var oldTypeNotification = $(".selTypeNotification").attr("oldValue");

            //Remove filter approved files
            $("#txtStatusByChecker").val("");

            //Refresh table only when was filtered by approved item by stakeholder
            if (oldTypeNotification == "Stakeholder") {
                //Reload Table
                loadTableProcessFiles();
            }
        }

        $(".selTypeNotification").attr("oldValue", typeNotification);
    });

    //On Click Copy Link to clipboard
    $(".btnCopyLinkToClipboard").click(function () {
        copyLinkToClipboard();
    });

    //On Click Add Comment 
    $(".btnAddComment").click(function () {
        showModalAddComment();
    });

    //On Click Open Discussions
    $(".btnOpenDiscussions").click(function () {
        showModalDiscussionList();
    });

    //On Click Toggle Select All
    $(".btnToggleSelectAll").click(function () {
        _toggleSelectAll("#tblProcessFiles");
    });

    //On Click Download File
    $(".btnDownloadFile").click(function (e) {
        var $btn = $(this);

        var rowData = $.jqxGridApi.getOneSelectedRow('#tblProcessFiles', true);
        if (_contains($btn.attr("href"), "Could not find file")) {
            e.preventDefault();
            _showNotification("error", "Process File '" + rowData.FileNamePatternText + "' is not received.", "AlertNotReceived");
        } else {
            if (!rowData || $btn.attr("disabled")) {
                e.preventDefault();
            } else {
                //Add Audit Record
                addDetailAuditLog({
                    type: "Download",
                    idPFDetails: rowData["IDPFDetail"],
                    fileStatus: rowData["FileStatus"],
                    statusByChecker: rowData["StatusByChecker"],
                    comment: "",
                    onSuccess: function (idPFDetailAuditLog) { }
                });
            }
        }
    });

    //On Click Open in Excel
    $(".btnOpenInExcel").click(function () {
        openInExcel();
    });

    //On Click Delete
    $(".btnDelete").click(function () {
        deleteProcessFile();
    });

    //On click refresh
    $(".btnRefresh").click(function () {
        //Trigger Change Frequency to load table
        $("#selFrequency").change();
    });

    //Load View by Maker, Checker, Customer, Email or All
    loadViewType();

    //Trigger Change Frequency to load table
    $("#selFrequency").change();

    //---------------------------------------------------
    //SUPPORT DOCUMENTATION FUNCTIONS
    //---------------------------------------------------

    //On click export to excel
    $(".btnExportToExcel").click(function () {
        exportToExcel();
    });
});

//---------------------------------------------------
//SUPPORT DOCUMENTATION PRODUCT AND CONTROL FUNCTIONS
//---------------------------------------------------
function loadTableProcessFiles() {
    if (isValidFilters()) {
        getProcessFilesByFilters({
            onSuccess: function (processList) {
                loadTable(processList);
            }
        });
    }

    function loadTable(processList) {
        var idTable = "#tblProcessFiles";
        var columnDefinitions = [];

        //Add Select Column only when need to send Email
        if ($("#HFViewType").val() == "Email") {
            columnDefinitions.push({
                name: 'IsSelected',
                text: "Select",
                type: 'bool',
                columntype: 'checkbox',
                width: '50px',
                pinned: true,
                filterable: false
            });

            //Add selected value to array
            //for (var i = 0; i < processList.length; i++) {
            //    processList[i].IsSelected = true;
            //}
        } else {
            $(".btnToggleSelectAll").hide();
        }

        //Add Columns
        columnDefinitions.push({ name: 'IDPFDetail', type: 'number', hidden: 'true' });
        columnDefinitions.push({ name: 'IDProcessFile', type: 'number', hidden: 'true' });
        columnDefinitions.push({ name: 'IDProcess', type: 'number', hidden: 'true' });
        columnDefinitions.push({ name: 'IDFrequency', type: 'number', hidden: 'true' });
        columnDefinitions.push({ name: 'CountryCode', type: 'string', hidden: 'true' });
        columnDefinitions.push({ name: 'CheckersJson', type: 'string', hidden: 'true' });
        columnDefinitions.push({ name: 'MakersJson', type: 'string', hidden: 'true' });
        columnDefinitions.push({ name: 'StakeholdersJson', type: 'string', hidden: 'true' });
        columnDefinitions.push({ name: 'AuditLogsCount', type: 'number', hidden: 'true' });
        columnDefinitions.push({ name: 'AuditLogsJson', type: 'string', hidden: 'true' });
        columnDefinitions.push({ name: 'AuditLogFilesCount', type: 'number', hidden: 'true' });
        columnDefinitions.push({ name: 'AuditLogFilesJson', type: 'string', hidden: 'true' });
        columnDefinitions.push({ name: 'NotificationEmailCount', type: 'number', hidden: 'true' });
        columnDefinitions.push({ name: 'NotificationEmailsJson', type: 'string', hidden: 'true' });
        columnDefinitions.push({ name: 'FileFullPath', type: 'string', hidden: 'true' });
        columnDefinitions.push({ name: 'FilePath', type: 'string', hidden: 'true' });
        columnDefinitions.push({ name: 'FileExt', type: 'string', hidden: 'true' });
        columnDefinitions.push({ name: 'FileNamePatternText', type: 'string', hidden: 'true' });
        columnDefinitions.push({ name: 'FolderFullPath', type: 'string', hidden: 'true' });
        columnDefinitions.push({ name: 'FolderPath', type: 'string', hidden: 'true' });
        columnDefinitions.push({
            name: 'FileStatus', text: 'File Status', width: '140px', type: 'string', pinned: true, filtertype: 'checkedlist', editable: false, filterable: true, cellsrenderer: function (rowIndex, datafield, value) {
                var dataRecord = $("#tblProcessFiles").jqxGrid('getrowdata', rowIndex);
                var htmlResult = '';

                //Add Status
                switch (dataRecord.FileStatus) {
                    case "Received":
                        htmlResult += '<span class="badge badge-md badge-info" style="margin-top: 5px; margin-left: 5px;"> Received </span>';
                        break;

                    case "Not Received":
                        htmlResult += '<span class="badge badge-md badge-warning" style="margin-top: 5px; margin-left: 5px;"> Not Received </span>';
                        break;

                    default:
                        htmlResult += '<span class="badge badge-md badge-warning" style="margin-top: 5px; margin-left: 5px;">' + dataRecord.FileStatus + '</span>';
                        break;
                }

                //Add History of comments
                if (dataRecord.AuditLogsCount > 0) {
                    var historyRows = JSON.parse(dataRecord.AuditLogsJson);
                    if (historyRows.length > 0) {
                        var historyInfoHtml = "<div style='padding: 10px; font-size: 15px; max-height: 500px; overflow-y:scroll; width: 520px;'>";

                        for (var i = 0; i < historyRows.length; i++) {
                            var tempData = historyRows[i];

                            if (tempData.Type == "Escalation" ||
                                tempData.Type == "File Rejected" || 
                                tempData.Type == "File Deleted") {
                                historyInfoHtml += "<b style='color:#f44336;'>" + tempData.Type + "</b><br>";
                            } else {
                                historyInfoHtml += "<b style='color:#4caf50;'>" + tempData.Type + "</b><br>";
                            }
                            
                            historyInfoHtml += "<b>User:</b> (" + tempData.UserRole + ") " + tempData.CreatedByName + " [" + tempData.CreatedBySOEID + "]<br>";
                            //historyInfoHtml += "<b>File Status:</b> " + tempData.FileStatus + "<br>";
                            //historyInfoHtml += "<b>Status By Checker:</b> " + tempData.StatusByChecker + "<br>";

                            if (tempData.Comment) {
                                historyInfoHtml += "<b>Date:</b> " + tempData.CreatedDate + "<br>";
                                historyInfoHtml += (tempData.Type == "Comment" ? "" : "<b>Comment:</b> ") + "<pre style='margin: 0 0 0px;'>" + _escapeToHTML(tempData.Comment) + "</pre>";
                            } else {
                                historyInfoHtml += "<b>Date:</b> " + tempData.CreatedDate + "<br>";
                            }

                            //Add Files
                            if (dataRecord.AuditLogFilesCount > 0) {
                                var auditLogFilesRows = JSON.parse(dataRecord.AuditLogFilesJson);


                                //Validate files of current auditLog in for loop
                                var auditLogFiles = _findAllObjByProperty(auditLogFilesRows, "IDPFDetailAuditLog", tempData.IDPFDetailAuditLog);

                                if (auditLogFiles.length > 0) {
                                    historyInfoHtml += "<b>Files:</b> <br>";
                                }

                                for (var j = 0; j < auditLogFiles.length; j++) {
                                    historyInfoHtml += " - " + auditLogFiles[j].FileName + "." + auditLogFiles[j].FileExt + "<br>";
                                }
                            }

                            //Add Sent to Emails
                            if (dataRecord.NotificationEmailCount > 0) {
                                var notificationEmailRows = JSON.parse(dataRecord.NotificationEmailsJson);

                                //Validate files of current auditLog in for loop
                                var notificationEmails = _findAllObjByProperty(notificationEmailRows, "IDPFNotification", tempData.IDPFNotification);

                                if (notificationEmails.length > 0) {
                                    historyInfoHtml += "<b>Email Sent to:</b> <br>";
                                }

                                for (var j = 0; j < notificationEmails.length; j++) {
                                    if (notificationEmails[j].SOEID) {
                                        historyInfoHtml += " - " + notificationEmails[j].Name + " [" + notificationEmails[j].SOEID + "]<br>";
                                    } else {
                                        historyInfoHtml += " - " + _escapeToHTML(notificationEmails[j].Email) + "<br>";
                                    }
                                }
                            }

                            historyInfoHtml += "<br>";
                        }

                        historyInfoHtml += "</div>";

                        htmlResult +=
                            '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="right" data-content="' + historyInfoHtml + '" data-title="History" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                            '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png"/> ' +
                            '</div>';
                    }
                }

                return htmlResult;
            }
        });
        columnDefinitions.push({
            name: 'StatusByChecker', text: 'Status By Checker', width: '140px', type: 'string', pinned: true, filtertype: 'checkedlist', editable: false, filterable: true, cellsrenderer: function (rowIndex, datafield, value) {
                var dataRecord = $("#tblProcessFiles").jqxGrid('getrowdata', rowIndex);
                var htmlResult = '';

                switch (dataRecord.StatusByChecker) {
                    case "Approved":
                        htmlResult = '<span class="badge badge-md badge-success" style="margin-top: 5px; margin-left: 5px;"> Approved </span>';
                        break;

                    case "Rejected":
                        htmlResult = '<span class="badge badge-md badge-danger" style="margin-top: 5px; margin-left: 5px;"> Rejected </span>';
                        break;

                    default:
                        htmlResult = '<span class="badge badge-md badge-warning" style="margin-top: 5px; margin-left: 5px;">' + dataRecord.StatusByChecker + '</span>';
                        break;
                }

                return htmlResult;
            }
        });
        columnDefinitions.push({ name: 'Process', text: 'Process', filtertype: 'checkedlist', type: 'string', width: '20%' });
        columnDefinitions.push({ name: 'Country', text: 'Country', filtertype: 'checkedlist', type: 'string', width: '10%' });
        columnDefinitions.push({ name: 'FileName', text: 'File Name', filtertype: 'input', type: 'string', width: '30%' });
        columnDefinitions.push({
            name: 'CheckersCount', text: 'Checkers', width: '60px', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                var dataRecord = $(idTable).jqxGrid('getrowdata', rowIndex);
                var htmlResult = '<span style="line-height:31px;margin-left:12px;">' + dataRecord.CheckersCount + '</span>';

                //Add Checkers
                if (dataRecord.CheckersJson) {
                    var rows = JSON.parse(dataRecord.CheckersJson);
                    if (rows.length > 0) {
                        var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                        for (var i = 0; i < rows.length; i++) {
                            var tempData = rows[i];
                            infoHtml += " " + tempData.Name + " (" + tempData.SOEID + ")<br>";
                        }

                        infoHtml += "</div>";

                        htmlResult +=
                            '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                            '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/info.png"/> ' +
                            '</div>';
                    }
                }

                return htmlResult;
            }
        });
        columnDefinitions.push({
            name: 'MakersCount', text: 'Makers', width: '60px', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                var dataRecord = $(idTable).jqxGrid('getrowdata', rowIndex);
                var htmlResult = '<span style="line-height:31px;margin-left:12px;">' + dataRecord.MakersCount + '</span>';

                //Add Makers
                if (dataRecord.MakersJson) {
                    var rows = JSON.parse(dataRecord.MakersJson);
                    if (rows.length > 0) {
                        var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                        for (var i = 0; i < rows.length; i++) {
                            var tempData = rows[i];
                            infoHtml += " " + tempData.Name + " (" + tempData.SOEID + ")<br>";
                        }

                        infoHtml += "</div>";

                        htmlResult +=
                            '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                            '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/info.png"/> ' +
                            '</div>';
                    }
                }

                return htmlResult;
            }
        });
        columnDefinitions.push({
            name: 'StakeholdersCount', text: 'Stakeholders', width: '60px', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                var dataRecord = $(idTable).jqxGrid('getrowdata', rowIndex);
                var htmlResult = '<span style="line-height:31px;margin-left:12px;">' + dataRecord.StakeholdersCount + '</span>';

                //Add Emails
                if (dataRecord.StakeholdersJson) {
                    var rows = JSON.parse(dataRecord.StakeholdersJson);
                    if (rows.length > 0) {
                        var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                        for (var i = 0; i < rows.length; i++) {
                            var tempData = rows[i];
                            infoHtml += " " + _htmlEncode(tempData.Email) + "<br>";
                        }

                        infoHtml += "</div>";

                        htmlResult +=
                            '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                            '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/info.png"/> ' +
                            '</div>';
                    }
                }

                return htmlResult;
            }
        });

        $.jqxGridApi.create({
            showTo: idTable,
            options: {
                //for comments or descriptions
                height: "450",
                autoheight: false,
                autorowheight: false,
                selectionmode: "singlerow",
                showfilterrow: true,
                sortable: true,
                editable: true,
                groupable: true
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                dataBinding: "Large Data Set Local",
                rows: processList
            },
            //type: string - text - number - int - float - date - time 
            //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
            //cellsformat: ddd, MMM dd, yyyy h:mm tt
            groups: ['Country'],
            columns: columnDefinitions,
            ready: function () {
                
                var $jqxGrid = $(idTable);

                //Add Popup to see auditLogs
                _GLOBAL_SETTINGS.tooltipsPopovers();
                $jqxGrid.on("rowclick", function (event) {
                    //Show tooltips
                    _GLOBAL_SETTINGS.tooltipsPopovers();

                    //"If" Fix bug: On click Group Row (Server Error in '/SD' Application. Empty file name is not legal.)
                    if (event.args.row.bounddata.FileNamePatternText) {

                        //Download to Server for stakeholder selected process file
                        if ($("#HFViewType").val() == "Stakeholder") {

                            //"If" Fix bug: Server Error in '/SD' Application. Could not find file '\\sjovnasfro0003.wlb.lac.nsroot.net\froqt0029\ICG_PCU_LATAM_3\P&L\Guatemala\2018\01. Jan\01-19\GT_PnL (01-19-2018).xlsx'.
                            if (event.args.row.bounddata.FileStatus == "Received") {

                                $(".btnDownloadFile").attr("disabled", "disabled");

                                //Download File in server
                                _callServer({
                                    loadingMsg: "Checking file...",
                                    url: '/SupportDocProdCtrl/DownloadFile',
                                    data: {
                                        'shareFolderFullPathFile': _encodeSkipSideminder(event.args.row.bounddata.FileFullPath),
                                        'fileName': _encodeSkipSideminder(event.args.row.bounddata.FileNamePatternText)
                                    },
                                    type: "post",
                                    success: function (serverPath) {
                                        serverPath = _getViewVar("SubAppPath") + _replaceAll("~", "", serverPath);
                                        $(".btnDownloadFile").attr("href", serverPath);
                                        $(".btnDownloadFile").removeAttr("disabled");

                                        //Show message to user
                                        _showNotification("success", "'" + event.args.row.bounddata.FileNamePatternText + "' is ready click in 'Download' button to view the process file.", "AlertDownloadFile");
                                    }
                                });
                            } else {
                                //Show message to user
                                _showNotification("error", "Process file '" + event.args.row.bounddata.FileNamePatternText + "' is not received.", "AlertNotReceivedFile");

                                //Disable download button
                                $(".btnDownloadFile").attr("disabled", "disabled");
                            }
                        } 
                    }
                });

                // expand all groups.
                $jqxGrid.jqxGrid('expandallgroups');

                //Set Select All 
                $(".btnToggleSelectAll").html('<i class="fa fa-square-o"></i> Select All ');
                $(".btnToggleSelectAll").attr("updateSelectTo", "1");
            }
        });
    }
}

function deleteProcessFile() {
    //Delete 
    var objRowSelected = $.jqxGridApi.getOneSelectedRow("#tblProcessFiles", true);
    if (objRowSelected) {

        if (objRowSelected["FileStatus"] == "Received") {
            var htmlContentModal = "";
            htmlContentModal += "<b>Country: </b>" + objRowSelected['Country'] + "<br/>";
            htmlContentModal += "<b>Process: </b>" + objRowSelected['Process'] + "<br/>";
            htmlContentModal += "<b>File: </b>" + objRowSelected['FileNamePatternText'] + "<br/>";
            htmlContentModal += "<b>Location: </b>" + objRowSelected['FileFullPath'] + "<br/>";

            _showModal({
                width: '40%',
                modalId: "modalDelRow",
                addCloseButton: true,
                buttons: [{
                    name: "Delete",
                    class: "btn-danger",
                    closeModalOnClick: false,
                    onClick: function ($modal) {
                        _callServer({
                            loadingMsgType: "topBar",
                            loadingMsg: "Deleting file '" + objRowSelected['FileNamePatternText'] + "'...",
                            url: '/SupportDocProdCtrl/DeleteFile',
                            data: {
                                pathToDelete: _encodeSkipSideminder(objRowSelected['FileFullPath'])
                            },
                            success: function (respond) {
                                if (respond == "Deleted") {
                                    //Add Audit Log to selected row
                                    addDetailAuditLog({
                                        type: "File Deleted",
                                        idPFDetails: objRowSelected["IDPFDetail"],
                                        fileStatus: "Not Received",
                                        statusByChecker: objRowSelected["StatusByChecker"],
                                        comment: objRowSelected['FileFullPath'],
                                        onSuccess: function (idPFDetailAuditLog) {
                                            //Refresh Audit Logs and File Status
                                            refreshRows([objRowSelected], function () {
                                                //Show Alert
                                                _showNotification("success", "The file '" + objRowSelected['FileNamePatternText'] + "' was deleted successful.", "AlertDeleted");

                                                //Close Modal
                                                $modal.find(".close").click();
                                            });
                                        }
                                    });
                                }
                            }
                        });
                    }
                }],
                title: "Are you sure you want to delete this file?",
                contentHtml: htmlContentModal
            });
        } else {
            _showNotification("error", "You cannot delete the file '" + objRowSelected['FileNamePatternText'] + "' because the file is not received.", "AlertIsNotReceived");
        }
    }
}

function getProcessFilesByFilters(customOptions) {
    //Get obj Moment by Frequency
    var objMoment = getObjMomentByFrequency();

    //Default Options.
    var options = {
        loadingMsgType: "fullLoading",
        checkFileStatus: true,
        filters: {
            idPFDetails: $("#HFIDPFDetail").val(),
            idFrequency: $("#selFrequency").find("option:selected").val(),
            checkerSOEID: $("#selCheckers").find("option:selected").val(),
            makerSOEID: $("#selMakers").find("option:selected").val(),
            statusByChecker: $("#txtStatusByChecker").val()
        },
        onSuccess: function () { }
    };

    //Add @ProcessDate
    if (options.filters.idFrequency == "1") {
        options.filters.processDate = objMoment.format("YYYY-MM-DD");
    }

    //Add @Year, @Month
    if (options.filters.idFrequency == "2") {
        options.filters.year = objMoment.format("YYYY");
        options.filters.month = objMoment.format("MM");
    }

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    //Get data of process files to update Status "Received" "Not Received"
    loadProcessFiles(function (processFilesList) {
        
        if (options.checkFileStatus) {

            //Check if exists files in Share folder
            _checkProcessFilesExists({
                loadingMsgType: options.loadingMsgType,
                year: objMoment.format("YYYY"),
                month: objMoment.format("MM"),
                processDate: objMoment.format("YYYY-MM-DD"),
                processFiles: processFilesList,
                onSuccess: function () {

                    //Get updated process files with Status of "Received" and "Not Received"
                    loadProcessFiles(function (processFilesUpdated) {
                        options.onSuccess(processFilesUpdated);
                    });
                }
            });
        } else {
            options.onSuccess(processFilesList);
        }
    });

    function loadProcessFiles(fnOnSuccess) {
        var spParams = [];

        //Filter by @IDPFDetails
        if (options.filters.idPFDetails) {
            spParams.push({ "Name": "@IDPFDetails", "Value": options.filters.idPFDetails });
        }

        //Filter by @IDFrequency
        if (options.filters.idFrequency) {
            spParams.push({ "Name": "@IDFrequency", "Value": options.filters.idFrequency });
        }

        //Filter by @CheckerSOEID
        if (options.filters.checkerSOEID) {
            spParams.push({ "Name": "@CheckerSOEID", "Value": (options.filters.checkerSOEID ? options.filters.checkerSOEID : "") });
        }

        //Filter by @MakerSOEID
        if (options.filters.makerSOEID) {
            spParams.push({ "Name": "@MakerSOEID", "Value": (options.filters.makerSOEID ? options.filters.makerSOEID : "") });
        }

        //Filter by @ProcessDate
        if (options.filters.processDate) {
            spParams.push({ "Name": "@ProcessDate", "Value": options.filters.processDate });
        }

        //Filter by @Year
        if (options.filters.year) {
            spParams.push({ "Name": "@Year", "Value": options.filters.year });
        }

        //Filter by @Month
        if (options.filters.month) {
            spParams.push({ "Name": "@Month", "Value": options.filters.month });
        }
        
        //Filter by @StatusByChecker
        if (options.filters.statusByChecker) {
            spParams.push({ "Name": "@StatusByChecker", "Value": options.filters.statusByChecker });
        }

        _callProcedure({
            loadingMsgType: options.loadingMsgType,
            loadingMsg: "Getting process files...",
            name: "[dbo].[spSDocProdCtrlListProcessFileDetail]",
            params: spParams,
            success: {
                fn: function (processFilesList) {
                    //Generate FolderPatternText, FileNamePatternText, FolderFullPath and FileFullPath
                    processFilesList = generateProcessFilePaths(processFilesList);

                    fnOnSuccess(processFilesList);
                }
            }
        });
    }
}

function loadSelectOfPermits(PermitType) {
    var idFrequency = $("#selFrequency").find("option:selected").val();
    var sql = "                                                                                     \
        SELECT                                                                                      \
            ''		AS [SOEID],                                                                     \
	        'All'	AS [NAME]                                                                       \
            UNION                                                                                   \
        SELECT DISTINCT                                                                             \
            E.SOEID,                                                                                \
	        E.NAME                                                                                  \
        FROM                                                                                        \
            [dbo].[tblSDocProdCtrlProcessFileXPermit] PFP                                           \
                INNER JOIN [dbo].[tblSDocProdCtrlProcessFile] PF ON PFP.[IDProcessFile] = PF.[ID]   \
                INNER JOIN [Automation].[dbo].[tblEmployee] E WITH(READUNCOMMITTED) ON PFP.SOEID = E.SOEID                \
        WHERE                                                                                       \
            PFP.[Type] = '" + PermitType + "' AND PF.[IDFrequency] = " + idFrequency + "            \
        ORDER BY                                                                                    \
            (2) ASC                                                                                 ";

    _callServer({
        loadingMsg: "Loading " + PermitType + "s...",
        url: '/Shared/ExecQuery',
        data: { 'pjsonSql': _toJSON(sql) },
        type: "post",
        success: function (resultList) {
            $("#sel" + PermitType + "s").contents().remove();

            //Add Options
            for (var i = 0; i < resultList.length; i++) {
                var objTemp = resultList[i];
                var optionText = (i == 0 ? objTemp.NAME : '(' + objTemp.SOEID + ') ' + objTemp.NAME);
                $("#sel" + PermitType + "s").append($('<option>', {
                    value: objTemp.SOEID,
                    text: optionText
                }));
            }

            //Add On Change Event
            $("#sel" + PermitType + "s").unbind("change");
            $("#sel" + PermitType + "s").change(function () {
                loadTableProcessFiles();
            });

            //Create Select2
            $("#sel" + PermitType + "s").select2();
        }
    });
};

function loadViewType() {
    switch ($("#HFViewType").val()) {
        case "All":
            //Select of Makers
            loadSelectOfPermits("Maker");

            //Select of Checkers
            loadSelectOfPermits("Checker");
            break;

        case "Maker":
            //Select of Checkers
            loadSelectOfPermits("Checker");

            //Select of Makers
            $("#selMakers").append($('<option>', {
                value: _getUserInfo().SOEID,
                text: "(" + _getUserInfo().SOEID + ") " + _getUserInfo().Name
            }));
            $("#selMakers").attr("disabled", true);
            break;

        case "Checker":
            //Select of Makers
            loadSelectOfPermits("Maker");

            //Select of Checkers
            $("#selCheckers").append($('<option>', {
                value: _getUserInfo().SOEID,
                text: "(" + _getUserInfo().SOEID + ") " + _getUserInfo().Name
            }));
            $("#selCheckers").attr("disabled", true);
            break;
        
        case "Email":
            if (_userHaveRole("SUPPORTDOCPRODCTRL_ADMIN") || _userHaveRole("SUPPORTDOCPRODCTRL_SUPER_ADMIN")) {
                //Select of Makers
                loadSelectOfPermits("Maker");

                //Select of Checkers
                loadSelectOfPermits("Checker");
            } else {
                //Select of Makers
                loadSelectOfPermits("Maker");

                //Select of Checkers
                $("#selCheckers").append($('<option>', {
                    value: _getUserInfo().SOEID,
                    text: "(" + _getUserInfo().SOEID + ") " + _getUserInfo().Name
                }));
                $("#selCheckers").attr("disabled", true);
            }
            break;

        case "Stakeholder":
            $(".formGroupSelMaker").hide();
            $(".formGroupSelChecker").hide();

            //Show only approved process files
            $("#txtStatusByChecker").val("Approved");
            break;
    }
}

function getObjMomentByFrequency() {
    //Get Frequency
    var selFrequency = $("#selFrequency").find("option:selected").val();

    //Moment Obj
    var objMoment;

    //Daily
    if (selFrequency == "1") {
        var processDate = $("#processDate").val();

        //Get Moment Day
        objMoment = moment(processDate, 'MMM DD, YYYY');
    }

    //Montly
    if (selFrequency == "2") {
        var selectedYear = $("#selYear").val();
        var selectedMonth = $("#selMonth").val();

        //Get Moment Month
        objMoment = moment(selectedYear + "-" + selectedMonth, "YYYY-MMMM");
    }

    return objMoment;
}

function generateProcessFilePaths(processList) {
    //Moment Obj
    var objMoment = getObjMomentByFrequency();

    //Generate Paths
    for (var i = 0; i < processList.length; i++) {
        //Generate FolderPatternText for all process
        processList[i].FolderPatternText = _generatePattern(processList[i].FolderPattern, processList[i], objMoment);

        //Generate FileNamePatternText for all process
        processList[i].FileNamePatternText = _generatePattern(processList[i].FileNamePattern, processList[i], objMoment);

        //Generate FolderFullPath
        processList[i].FolderFullPath = processList[i].ShareFolderPath + processList[i].FolderPatternText;

        //Generate FileFullPath
        processList[i].FileFullPath = processList[i].ShareFolderPath + processList[i].FolderPatternText + processList[i].FileNamePatternText;

        //Generate FolderPath
        processList[i].FolderPath = processList[i].FolderPatternText;

        //Generate FilePath
        processList[i].FilePath = processList[i].FolderPatternText + processList[i].FileNamePatternText;
    }

    return processList;
}

function getUserRoleAuditLog() {
    var result = "User";

    switch ($("#HFViewType").val()) {
        case "All":
            result = "Administrator";
            break;

        case "Maker":
            result = "Maker";
            break;

        case "Checker":
            result = "Checker";
            break;

        case "Email":
            if (_userHaveRole("SUPPORTDOCPRODCTRL_ADMIN") || _userHaveRole("SUPPORTDOCPRODCTRL_SUPER_ADMIN")) {
                result = "Administrator";
            } else {
                result = "Checker";
            }
            break;

        case "Stakeholder":
            result = "Stakeholder";
            break;
    }

    return result;
}

function sendEmail(customOptions) {
    var options = {
        comment: "",
        idPFDetail: "",
        idPFDetailAuditLog: "",
        typeNotification: "",       //Comment | File Rejected
        onSuccess: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    //Add notification record
    addNotification({
        type: options.typeNotification,
        comment: options.comment,
        onSuccess: function (idPFNotification) {
            var momentDate = getObjMomentByFrequency();
            var frequencyDate = momentDate.format("MMM DD, YYYY");

            //When Montly
            if ($("#selFrequency").val() == "2") {
                frequencyDate = momentDate.format("MMMM, YYYY");
            }

            if (options.idPFDetailAuditLog) {
                callProcedureSendEmail(options.idPFDetailAuditLog);
            } else {
                //Add Audit Log to process files
                addDetailAuditLog({
                    type: options.typeNotification,
                    idPFNotification: idPFNotification,
                    idPFDetails: options.idPFDetail,
                    comment: options.comment,
                    onSuccess: function (idPFDetailAuditLog) {
                        callProcedureSendEmail(idPFDetailAuditLog);
                    }
                });
            }

            function callProcedureSendEmail(idPFDetailAuditLog) {
                _callProcedure({
                    loadingMsgType: "topBar",
                    loadingMsg: "Sending " + options.typeNotification + " Email...",
                    name: "[dbo].[spSDocProdCtrlSendEmail]",
                    params: [
                        { "Name": "@ServerURL", "Value": _getViewVar("ServerURL") },
                        { "Name": "@IDPFNotification", "Value": idPFNotification },
                        { "Name": "@IDPFDetailAuditLog", "Value": idPFDetailAuditLog },
                        { "Name": "@IDPFDetail", "Value": options.idPFDetail },
                        { "Name": "@IDFrequency", "Value": $("#selFrequency").find(":selected").val() },
                        { "Name": "@Year", "Value": $("#selYear").val() },
                        { "Name": "@Month", "Value": $("#selMonth").val() },
                        { "Name": "@ProcessDate", "Value": momentDate.format("YYYY-MM-DD") },
                        { "Name": "@Frequency", "Value": $("#selFrequency").find(":selected").text() },
                        { "Name": "@FrequencyDate", "Value": frequencyDate },
                        { "Name": "@Comment", "Value": options.comment },
                        { "Name": "@TypeNotification", "Value": options.typeNotification }
                    ],
                    success: {
                        fn: function (responseList) {
                            if (options.onSuccess) {
                                options.onSuccess();
                            }
                        }
                    }
                });
            }
        }
    });
}

function showModalAddComment() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#tblProcessFiles', true);
    if (rowData) {
        var fileName = rowData["FileNamePatternText"];
        var htmlContentModal = '';
        htmlContentModal += "<b>Process: </b> " + rowData["Process"] + " <br/>";
        htmlContentModal += "<b>File Name: </b> <span class='txtFileName'>" + fileName + "</span><br/>";
        htmlContentModal += "<b>File Location: </b> <span class='txtFileFullPath'>" + rowData["FileFullPath"] + "</span> <br/><br/>";
        htmlContentModal += "Comments:";
        htmlContentModal += "<textarea class='form-control txtDetailAuditLog' placeholder='(Required)' style='height:100px !important;'></textarea>";

        _showModal({
            modalId: "modalUpload",
            width: '70%',
            buttons: [{
                name: "<i class='fa fa-save'></i> Save",
                class: "btn-success",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    //Validate required Comment
                    if ($(".txtDetailAuditLog").val()) {

                        addDetailAuditLog({
                            type: "Comment",
                            idPFDetails: rowData["IDPFDetail"],
                            fileStatus: rowData["FileStatus"],
                            statusByChecker: rowData["StatusByChecker"],
                            comment: $(".txtDetailAuditLog").val(),
                            onSuccess: function (idPFDetailAuditLog) {

                                //Send rejected email
                                sendEmail({
                                    comment: $(".txtDetailAuditLog").val(),
                                    idPFDetail: rowData.IDPFDetail,
                                    idPFDetailAuditLog: idPFDetailAuditLog,
                                    typeNotification: "Comment",       //Comment | File Rejected
                                    onSuccess: function () {
                                        //Refresh Audit Logs and File Status
                                        refreshRows([rowData], function () {
                                            //Show notification
                                            _showNotification("success", "Comment was added successfully.", "AlertAddComment");

                                            //Close Modal
                                            $modal.find(".close").click();
                                        });
                                    }
                                });
                            }
                        });
                    } else {
                        _showAlert({
                            id: "CommentValidation",
                            showTo: $modal.find(".modal-body"),
                            content: "Please add comments."
                        });
                    }
                }
            }],
            addCloseButton: true,
            title: "Add Comment",
            contentHtml: htmlContentModal,
            onReady: function ($modal) { }
        });
    }

}

function showModalDiscussionList() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#tblProcessFiles', true);
    if (rowData) {
        var idDiscussionTable = "tblDiscussionList";
        var htmlContentModal = '';
        htmlContentModal += 
            '<div class="row"> ' +
            '    <div class="col-md-12"> ' +
            '        <ul class="nav nav-tabs"> ' +
            '            <li class="active"> ' +
            '                <a href="#tab-discussion-list" data-toggle="tab" aria-expanded="true"> ' +
            '                    <i class="fa fa-cog"></i> Discussion List ' +
            '                </a> ' +
            '            </li> ' +
            '            <li class="" id="tab-label-my-issues" > ' +
            '                <a href="#tab-discussion-comments" data-toggle="tab" aria-expanded="false"> ' +
            '                    <i class="fa fa-cog"></i> Discussion Comments ' +
            '                </a> ' +
            '            </li> ' +
            '        </ul> ' +
            '        <div class="tab-content"> ' +
            '            <div class="tab-pane fade active in" id="tab-discussion-list"> ' +
            '               <button type="button" class="btnAdd btn btn-success top15 left15 bottom15 pull-right"><i class="fa fa-asterisk"></i> Add</button> ' +
            '               <button type="button" class="btnClose btn btn-info top15 left15 bottom15 pull-right"><i class="fa fa-asterisk"></i> Close</button> ' +
            '               <button type="button" class="btnViewComments btn btn-info top15 left15 bottom15 pull-right"><i class="fa fa-asterisk"></i> View Comments</button> ' +
            '               <button type="button" class="btnEdit btn btn-info top15 left15 bottom15 pull-right"><i class="fa fa-asterisk"></i> Edit</button> ' +
            '               <button type="button" class="btnDelete btn btn-danger  top15 left15 bottom15 pull-right"><i class="fa fa-remove"></i> Delete</button> ' +
            '               <div id="' + idDiscussionTable + '" class="clearfix"></div> ' +
            '            </div> ' +
            '            <div class="tab-pane fade in" id="tab-discussion-comments"> ' +
            '                <div id="comments-container"></div> ' +
            '            </div> ' +
            '        </div> ' +
            '    </div> ' +
            '</div>';

        _showModal({
            modalId: "modalUpload",
            width: '70%',
            addCloseButton: true,
            title: "Discussions List",
            contentHtml: htmlContentModal,
            onReady: function ($modal) {
                //Add row 
                $modal.find(".btnAdd").click(function () {
                    addRow();
                });

                //Edit row 
                $modal.find(".btnEdit").click(function () {
                    editRow();
                });

                //Delete selected row
                $modal.find(".btnDelete").click(function () {
                    deleteRow();
                });

                //Load table 
                loadTable();

                //Load comments
                loadCommentsPluggin($modal);
            }
        });

        function loadCommentsPluggin($modal) {
            var saveComment = function (data) {

                // Convert pings to human readable format
                $(data.pings).each(function (index, id) {
                    var user = usersArray.filter(function (user) { return user.id == id })[0];
                    data.content = data.content.replace('@' + id, '@' + user.fullname);
                });

                return data;
            }

            $modal.find('#comments-container').comments({
                profilePictureURL: '/Content/SupportDocProdCtrl/img/user-icon.png',
                currentUserId:1,
                roundProfilePictures: true,
                textareaRows: 1,
                enableAttachments: false,
                enableHashtags: false,
                enablePinging: true,
                getUsers: function (success, error) {
                    setTimeout(function () {
                        success(usersArray);
                    }, 500);
                },
                getComments: function (success, error) {
                    setTimeout(function () {
                        success(commentsArray);
                    }, 500);
                },
                postComment: function (data, success, error) {
                    setTimeout(function () {
                        success(saveComment(data));
                    }, 500);
                },
                putComment: function (data, success, error) {
                    setTimeout(function () {
                        success(saveComment(data));
                    }, 500);
                },
                deleteComment: function (data, success, error) {
                    setTimeout(function () {
                        success();
                    }, 500);
                },
                /*upvoteComment: function (data, success, error) {
                    setTimeout(function () {
                        success(data);
                    }, 500);
                },
                uploadAttachments: function (dataArray, success, error) {
                    setTimeout(function () {
                        success(dataArray);
                    }, 500);
                },*/
            });
        }

        function loadTable() {
            $.jqxGridApi.create({
                showTo: "#" + idDiscussionTable,
                options: {
                    //for comments or descriptions
                    height: "250",
                    autoheight: false,
                    autorowheight: false,
                    selectionmode: "singlerow",
                    showfilterrow: true,
                    sortable: true,
                    editable: true,
                    groupable: false
                },
                sp: {
                    Name: "[dbo].[spSDocProdCtrlAdminDiscussion]",
                    Params: [
                        { Name: "@Action", Value: "List" }
                    ]
                },
                source: {
                    // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
                    dataBinding: "Large Data Set"
                },
                columns: [
                    //type: string - text - number - int - float - date - time 
                    //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                    //cellsformat: ddd, MMM dd, yyyy h:mm tt
                    { name: 'ID', type: 'number', hidden: true },
                    {
                        name: 'Status', text: 'Status', width: '10%', type: 'string', pinned: true, filtertype: 'checkedlist', editable: false, filterable: true, cellsalign: 'center', align: 'center',
                        cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                            var htmlResult = '';

                            switch (rowData.Status) {
                                case "Closed":
                                    htmlResult = '<span class="badge badge-md badge-success" style="margin-top: 5px; margin-left: 5px;"> Closed </span>';
                                    break;

                                case "Open":
                                    htmlResult = '<span class="badge badge-md badge-warning" style="margin-top: 5px; margin-left: 5px;"> Open </span>';
                                    break;
                            }

                            return htmlResult;
                        }
                    },
                    { name: 'Title', text: 'Title', width: '80%', type: 'string', filtertype: 'input' },
                    {
                        name: 'Comments', text: 'Comments', width: '10%', type: 'string', filtertype: 'checkedlist', editable: false, filterable: true,
                        cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                            var htmlResult = '<span class="badge badge-md badge-info" style="margin-top: 5px; margin-left: 15px;"> 4 </span>';
                            return htmlResult;
                        }
                    }
                ],
                ready: function () {}
            });
        }

        function addRow() {
            var idTable = "tblDiscussionList";
            var $jqxGrid = $("#" + idTable);
            var datarow = {
                ID: 0,
                Name: ''
            };
            var commit = $jqxGrid.jqxGrid('addrow', null, datarow);

            //Missing status addition which should be Open by default and also need to ask for the user the title for the conversation.
            //Missing insert statement to the DB.


            _showNotification("success", "An empty row was added in the table.");
        }

        function deleteRow() {
            //Delete 
            var objRowSelected = $.jqxGridApi.getOneSelectedRow("#" + idTable, true);
            if (objRowSelected) {
                var htmlContentModal = "";
                htmlContentModal += "<b>Process: </b>" + objRowSelected['Name'] + "<br/>";

                _showModal({
                    width: '40%',
                    modalId: "modalDelRow",
                    addCloseButton: true,
                    buttons: [{
                        name: "Delete",
                        class: "btn-danger",
                        onClick: function () {
                            if (objRowSelected.ID != 0) {
                                _callProcedure({
                                    loadingMsgType: "fullLoading",
                                    loadingMsg: "Saving process...",
                                    name: "[dbo].[spSDocProdCtrlAdminProcess]",
                                    params: [
                                        { "Name": "@Action", "Value": "Delete" },
                                        { "Name": "@IDProcess", "Value": objRowSelected.ID },
                                        { "Name": "@Name", "Value": objRowSelected.Name },
                                        { "Name": "@SessionSOEID", "Value": _getSOEID() }
                                    ],
                                    //Show message only for the last 
                                    success: {
                                        showTo: $("#" + idTable).parent(),
                                        msg: "Row was deleted successfully.",
                                        fn: function () {
                                            var reloadData = setInterval(function () {
                                                //console.log("Reloading data from #tblReasons");
                                                //$.jqxGridApi.localStorageFindById("#" + idTable).fnReload();
                                                //$("#" + idTable).jqxGrid('updateBoundData');

                                                loadTable();

                                                //Remove changes
                                                $.jqxGridApi.rowsChangedFindById("#" + idTable).rows = [];

                                                clearInterval(reloadData);
                                            }, 500);
                                        }
                                    }
                                });
                            } else {
                                $("#" + idTable).jqxGrid('deleterow', objRowSelected.uid);
                            }
                        }
                    }],
                    title: "Are you sure you want to delete this row?",
                    contentHtml: htmlContentModal
                });
            }
        }

        function saveTable() {
            var rowsChanged = $.jqxGridApi.rowsChangedFindById("#" + idTable).rows;

            //Validate changes
            if (rowsChanged.length > 0) {
                var fnSuccess = {
                    showTo: $("#" + idTable).parent(),
                    msg: "The data was saved successfully.",
                    fn: function () {
                        var reloadData = setInterval(function () {
                            //console.log("Reloading data from #tblReasons");
                            //$.jqxGridApi.localStorageFindById("#" + idTable).fnReload();
                            //$("#" + idTable).jqxGrid('updateBoundData');

                            loadTable();

                            //Remove changes
                            $.jqxGridApi.rowsChangedFindById("#" + idTable).rows = [];

                            clearInterval(reloadData);
                        }, 1500);
                    }
                };

                for (var i = 0; i < rowsChanged.length; i++) {
                    var objRow = rowsChanged[i];
                    var action = "Edit";

                    if (objRow.ID == 0) {
                        var action = "Insert";
                    }

                    _callProcedure({
                        loadingMsgType: "fullLoading",
                        loadingMsg: "Saving process...",
                        name: "[dbo].[spSDocProdCtrlAdminProcess]",
                        params: [
                            { "Name": "@Action", "Value": action },
                            { "Name": "@IDProcess", "Value": objRow.ID },
                            { "Name": "@Name", "Value": objRow.Name },
                            { "Name": "@SessionSOEID", "Value": _getSOEID() }
                        ],
                        //Show message only for the last 
                        success: (i == rowsChanged.length - 1) ? fnSuccess : null
                    });
                }
            } else {
                _showAlert({
                    showTo: $("#" + idTable).parent(),
                    type: 'error',
                    title: "Message",
                    content: "No changes detected."
                });
            }
        }
    }
}

function showModalSendEmailNotification(typeNotification) {
    var processFiles = _getCheckedRows('#tblProcessFiles');

    if (processFiles.length > 0) {
        var emailNotificationGroups = [];

        //Fuction to find Notification Group
        function getNotificationGroup(objProcessFile) {
            var notificationGroup = {
                manager: [],
                makers: [],
                processFiles: []
            };
            for (var i = 0; i < emailNotificationGroups.length; i++) {
                var tempGroup = emailNotificationGroups[i];



            }

            return notificationGroup;
        }

        
        //Validate similar contacts for each processfile
        for (var i = 0; i < processFiles.length; i++) {
            var tempMakers = JSON.parse(processFiles[i].MakersJson);
            for (var j = 0; j < tempMakers.length; j++) {
                var objTempMaker = tempMakers[j];

                //First process files
                if (i == 0) {
                    //Add Makers
                    notificationGroup.makers.push({
                        SOEID: objTempMaker["SOEID"],
                        Name: objTempMaker["Name"],
                        Email: objTempMaker["Email"]
                    });

                    //First Manager
                    if (j == 0) {
                        //Add Manager
                        notificationGroup.manager = {
                            SOEID: objTempMaker["ManagerSOEID"],
                            Name: objTempMaker["ManagerName"],
                            Email: objTempMaker["ManagerEmail"]
                        };
                    } else {

                    }
                } else {
                    //Other process files
                }

                


                //Add Manager

            }
        }
    } else {
        _showNotification("error", "No process files selected", "SelectProcessFile");
    }
}

function showModalSendEmailMaker(typeNotification) {
    var idPFDetails = [];
    var contactSOEIDUnique = {};
    var managerSOEIDUnique = {};
    var contactToSendEmail = [];
    var processFiles = _getCheckedRows('#tblProcessFiles');
    var htmlContentModal = "";

    if (processFiles.length > 0) {
        htmlContentModal += "<p>(" + processFiles.length + ") Process Files selected. </p>";
        htmlContentModal += "<textarea class='form-control txtDetailAuditLog' placeholder='Add comments (Optional)' style='height:80px !important;'></textarea>";

        if (typeNotification == "Reminder") {
            htmlContentModal += "<br><p>This notification will be sent to the following Makers: </p>";
        }

        if (typeNotification == "Escalation") {
            htmlContentModal += "<br><p>This notification will be sent to the following Managers and Makers: </p>";
        }

        //Get selected process files
        var tempMakers;
        for (var i = 0; i < processFiles.length; i++) {
            //Add Process file detail ID
            idPFDetails.push(processFiles[i]["IDPFDetail"]);

            if (processFiles[i]["MakersCount"] > 0) {
                tempMakers = JSON.parse(processFiles[i]["MakersJson"]);

                //Add Makers of selected process file
                for (var j = 0; j < tempMakers.length; j++) {

                    //Validate unique SOEIDs
                    if (!_contains(tempMakers[j]["Name"], "NOT FOUND")) {
                        if (!contactSOEIDUnique[tempMakers[j]["SOEID"]]) {

                            if (typeNotification == "Reminder") {
                                htmlContentModal += " - " + tempMakers[j]["Name"] + " [" + tempMakers[j]["SOEID"] + "]<br/>";
                            }

                            contactToSendEmail.push(tempMakers[j]);
                            contactSOEIDUnique[tempMakers[j]["SOEID"]] = "1";

                            //Add Manager
                            managerSOEIDUnique[tempMakers[j]["ManagerSOEID"]] = {
                                ManagerName: tempMakers[j]["ManagerName"]
                            };
                        }
                    }
                }
            }
        }

        //Add Managers when is Escalation
        if (typeNotification == "Escalation") {
            var managerSOEIDs = Object.keys(managerSOEIDUnique);
            for (var i = 0; i < managerSOEIDs.length; i++) {
                var managerEmployees = _findAllObjByProperty(contactToSendEmail, "ManagerSOEID", managerSOEIDs[i]);

                htmlContentModal += " (Manager) " + managerSOEIDUnique[managerSOEIDs[i]].ManagerName + " [" + managerSOEIDs[i] + "]:<br/>";

                for (var n = 0; n < managerEmployees.length; n++) {
                    htmlContentModal += " - " + managerEmployees[n]["Name"] + " [" + managerEmployees[n]["SOEID"] + "]<br/>";
                }

                htmlContentModal += "<br/>";
            }
        }

        htmlContentModal += "<br/><p>If you need remove some one from the list please contact your administrator.</p>";

        _showModal({
            width: '50%',
            modalId: "modalSendReminder",
            addCloseButton: true,
            buttons: [{
                name: "Send notification",
                class: "btn-success",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    var momentDate = getObjMomentByFrequency();
                    var commentReminder = ($(".txtDetailAuditLog").val() ? $(".txtDetailAuditLog").val() : "");
                    var frequencyDate = momentDate.format("MMM DD, YYYY");

                    //When Montly
                    if ($("#selFrequency").val() == "2") {
                        frequencyDate = momentDate.format("MMMM, YYYY");
                    }

                    //Add notification record
                    addNotification({
                        type: typeNotification,
                        comment: commentReminder,
                        onSuccess: function (idPFNotification) {

                            //Add Audit Log to process files
                            addDetailAuditLog({
                                type: typeNotification,
                                idPFNotification: idPFNotification,
                                idPFDetails: idPFDetails.join(","),
                                comment: commentReminder,
                                onSuccess: function (idPFDetailAuditLog) {
                                    sendEmailReminderToMakers();
                                }
                            });

                            function sendEmailReminderToMakers() {
                                var counterMakersProcessed = 0;

                                //Send email foreach maker    
                                for (var k = 0; k < contactToSendEmail.length; k++) {
                                    var tempContact = contactToSendEmail[k];

                                    _callProcedure({
                                        loadingMsgType: "fullLoading",
                                        loadingMsg: "Sending " + typeNotification + " Email to '" + tempContact["Name"] + "'...",
                                        name: "[dbo].[spSDocProdCtrlSendEmailMaker]",
                                        params: [
                                            { "Name": "@ServerURL", "Value": _getViewVar("ServerURL") },
                                            { "Name": "@IDPFNotification", "Value": idPFNotification },
                                            { "Name": "@MakerSOEID", "Value": tempContact["SOEID"] },
                                            { "Name": "@MakerName", "Value": tempContact["Name"] },
                                            { "Name": "@MakerEmail", "Value": tempContact["Email"] },
                                            { "Name": "@IDPFDetails", "Value": idPFDetails.join(",") },
                                            { "Name": "@IDFrequency", "Value": $("#selFrequency").find(":selected").val() },
                                            { "Name": "@Year", "Value": $("#selYear").val() },
                                            { "Name": "@Month", "Value": $("#selMonth").val() },
                                            { "Name": "@ProcessDate", "Value": momentDate.format("YYYY-MM-DD") },
                                            { "Name": "@Frequency", "Value": $("#selFrequency").find(":selected").text() },
                                            { "Name": "@FrequencyDate", "Value": frequencyDate },
                                            { "Name": "@Comment", "Value": commentReminder },
                                            { "Name": "@TypeNotification", "Value": typeNotification }
                                        ],
                                        success: {
                                            fn: function (responseList) {
                                                counterMakersProcessed++;
                                                callRefreshRows();
                                            }
                                        }
                                    });
                                }

                                function callRefreshRows() {
                                    //Call refresh row until send last email to makers
                                    if (counterMakersProcessed == contactToSendEmail.length) {
                                        refreshRows(processFiles, function () {
                                            //Show notification
                                            _showNotification("success", typeNotification + " was sent successfully.", "Alert" + typeNotification);

                                            //Close Modal
                                            $modal.find(".close").click();
                                        });
                                    }
                                }
                            }
                        }
                    });
                }
            }],
            title: "Are you sure you want to send '" + typeNotification + "' notification?",
            contentHtml: htmlContentModal
        });
    } else {
        _showNotification("error", "No process files selected", "SelectProcessFile");
    }
}

function setFileName(idFile) {
    $('#fine-uploader-manual-trigger').fineUploader('setName', idFile, $(".txtFileName").text());

    //Remove alert
    $("#AlertChangeFileName").remove();
}

function showModalSendEmailStakeholder() {
    var idPFDetails = [];
    var contactEmailUnique = {};
    var contactToSendEmail = [];
    var processFiles = _getCheckedRows('#tblProcessFiles');
    var htmlContentModal = "";

    if (processFiles.length > 0) {
        htmlContentModal += "<p>(" + processFiles.length + ") Process Files selected. </p>";
        htmlContentModal += "<textarea class='form-control txtDetailAuditLog' placeholder='Add comments (Optional)' style='height:80px !important;'></textarea>";
        //htmlContentModal += "<br><p>This notification will be sent to the following Stakeholders: </p>";
    
        //Get selected process files
        var tempStakeholders;
        for (var i = 0; i < processFiles.length; i++) {
            //Add Process file detail ID
            idPFDetails.push(processFiles[i]["IDPFDetail"]);

            if (processFiles[i]["StakeholdersCount"] > 0) {
                //htmlContentModal += processFiles[i]["FileNamePatternText"] + "<br/>";
                tempStakeholders = JSON.parse(processFiles[i]["StakeholdersJson"]);

                //Add Stakeholders of selected process file
                for (var j = 0; j < tempStakeholders.length; j++) {

                    //Validate unique SOEIDs
                    if (!contactEmailUnique[tempStakeholders[j]["Email"]]) {
                        //htmlContentModal += " - " + _escapeToHTML(tempStakeholders[j]["Email"]) + "<br/>";
                        contactToSendEmail.push(tempStakeholders[j]);
                        contactEmailUnique[tempStakeholders[j]["Email"]] = "1";
                    }
                }

                //htmlContentModal += "<br/>";
            }
        }

        //htmlContentModal += "<br/><p>If you need remove some one from the list please contact your administrator.</p>";

        _showModal({
            width: '50%',
            modalId: "modalSendStakeholder",
            addCloseButton: true,
            buttons: [{
                name: "Send notification",
                class: "btn-success",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    var momentDate = getObjMomentByFrequency();
                    var commentStakeholder = ($(".txtDetailAuditLog").val() ? $(".txtDetailAuditLog").val() : "");
                    var frequencyDate = momentDate.format("MMM DD, YYYY");

                    //When Montly
                    if ($("#selFrequency").val() == "2") {
                        frequencyDate = momentDate.format("MMMM, YYYY");
                    }

                    //Add notification record
                    addNotification({
                        type: "Stakeholder",
                        comment: commentStakeholder,
                        onSuccess: function (idPFNotification) {

                            //Add Audit Log to process files
                            addDetailAuditLog({
                                type: "Stakeholder Notification",
                                idPFNotification: idPFNotification,
                                idPFDetails: idPFDetails.join(","),
                                comment: commentStakeholder,
                                onSuccess: function (idPFDetailAuditLog) {
                                    sendEmailToStakeholders();
                                }
                            });

                            function sendEmailToStakeholders() {
                                var counterProcessFilesProcessed = 0;

                                //Send email foreach process file    
                                for (var k = 0; k < processFiles.length; k++) {
                                    var tempProcessFile = processFiles[k];

                                    _callProcedure({
                                        loadingMsgType: "fullLoading",
                                        loadingMsg: "Sending Email for '" + tempProcessFile["FileNamePatternText"] + "'...",
                                        name: "[dbo].[spSDocProdCtrlSendEmailStakeholder]",
                                        params: [
                                            { "Name": "@ServerURL", "Value": _getViewVar("ServerURL") },
                                            { "Name": "@IDPFNotification", "Value": idPFNotification },
                                            { "Name": "@IDPFDetail", "Value": tempProcessFile["IDPFDetail"] },
                                            { "Name": "@IDFrequency", "Value": $("#selFrequency").find(":selected").val() },
                                            { "Name": "@Year", "Value": $("#selYear").val() },
                                            { "Name": "@Month", "Value": $("#selMonth").val() },
                                            { "Name": "@ProcessDate", "Value": momentDate.format("YYYY-MM-DD") },
                                            { "Name": "@Frequency", "Value": $("#selFrequency").find(":selected").text() },
                                            { "Name": "@FrequencyDate", "Value": frequencyDate },
                                            { "Name": "@Comment", "Value": commentStakeholder }
                                        ],
                                        success: {
                                            fn: function (responseList) {
                                                counterProcessFilesProcessed++;
                                                callRefreshRows();
                                            }
                                        }
                                    });
                                }

                                function callRefreshRows() {
                                    //Call refresh row until send last email to makers
                                    if (counterProcessFilesProcessed == processFiles.length) {
                                        refreshRows(processFiles, function () {
                                            //Show notification
                                            _showNotification("success", "Emails to stakeholders were sent successfully.", "AlertStakeholder");

                                            //Close Modal
                                            $modal.find(".close").click();
                                        });
                                    }
                                }
                            }
                        }
                    });
                }
            }],
            title: "Are you sure you want to send 'Stakeholder' notification?",
            contentHtml: htmlContentModal
        });
    } else {
        _showNotification("error", "No process files selected", "SelectProcessFile");
    }
}

function sendEmailStakeholder(onSuccessSendEmail) {
    var rowData = $.jqxGridApi.getOneSelectedRow('#tblProcessFiles', true);
    if (rowData) {
        var momentDate = getObjMomentByFrequency();
        var commentStakeholder = "";
        var frequencyDate = momentDate.format("MMM DD, YYYY");

        //When Montly
        if ($("#selFrequency").val() == "2") {
            frequencyDate = momentDate.format("MMMM, YYYY");
        }

        //Add notification record
        addNotification({
            type: "Stakeholder",
            comment: commentStakeholder,
            onSuccess: function (idPFNotification) {

                //Add Audit Log to process files
                addDetailAuditLog({
                    type: "Stakeholder Notification",
                    idPFNotification: idPFNotification,
                    idPFDetails: rowData["IDPFDetail"],
                    fileStatus: rowData["FileStatus"],
                    statusByChecker: rowData["StatusByChecker"],
                    comment: commentStakeholder,
                    onSuccess: function (idPFDetailAuditLog) {

                        _callProcedure({
                            loadingMsgType: "fullLoading",
                            loadingMsg: "Sending Email for '" + rowData["FileNamePatternText"] + "'...",
                            name: "[dbo].[spSDocProdCtrlSendEmailStakeholder]",
                            params: [
                                { "Name": "@ServerURL", "Value": _getViewVar("ServerURL") },
                                { "Name": "@IDPFNotification", "Value": idPFNotification },
                                { "Name": "@IDPFDetail", "Value": rowData["IDPFDetail"] },
                                { "Name": "@IDFrequency", "Value": $("#selFrequency").find(":selected").val() },
                                { "Name": "@Year", "Value": $("#selYear").val() },
                                { "Name": "@Month", "Value": $("#selMonth").val() },
                                { "Name": "@ProcessDate", "Value": momentDate.format("YYYY-MM-DD") },
                                { "Name": "@Frequency", "Value": $("#selFrequency").find(":selected").text() },
                                { "Name": "@FrequencyDate", "Value": frequencyDate },
                                { "Name": "@Comment", "Value": commentStakeholder }
                            ],
                            success: {
                                fn: function (responseList) {

                                    refreshRows([rowData], function () {
                                        //Show notification
                                        _showNotification("success", "Emails to stakeholders were sent successfully.", "AlertStakeholder");

                                        if (onSuccessSendEmail) {
                                            onSuccessSendEmail();
                                        }
                                    });
                                }
                            }
                        });

                    }
                });
            }
        });
    }
}

function uploadDoc() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#tblProcessFiles', true);
    if (rowData) {
        var fileName = rowData["FileNamePatternText"];
        var htmlContentModal = '';
        htmlContentModal += "<b>Process: </b> " + rowData["Process"] + " <br/>";
        htmlContentModal += "<b>File Name: </b> <span class='txtFileName'>" + fileName + "</span><br/>";
        htmlContentModal += "<b>Location to upload: </b> <span class='txtFileFullPath'>" + rowData["FileFullPath"] + "</span> <br/><br/>";
        htmlContentModal += "<div id='fine-uploader-manual-trigger'></div><br/>";
        htmlContentModal += "Comments:";
        htmlContentModal += "<textarea class='form-control txtFileComment' placeholder='(Optional)' style='height:100px !important;'></textarea>";

        //Set File Upload Parameters
        function setFileParams() {
            var fileComment = ($(".txtFileComment").val() ? $(".txtFileComment").val() : "");

            $('#fine-uploader-manual-trigger').fineUploader('setParams', {
                'jsonProcessFile': _toJSON(rowData),
                'filePrefix': "",
                'comment': fileComment,
                'userRole': getUserRoleAuditLog(),
                'isExtraFile': "0"
            });
        }

        _showModal({
            width: '70%',
            modalId: "modalUpload",
            buttons: [{
                name: "<i class='fa fa-cloud-upload'></i> Upload",
                class: "btn-success",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    //console.log($modal);

                    //Check if exist at least 1 file
                    var filesToUpload = $('#fine-uploader-manual-trigger').fineUploader('getUploads');
                    if (filesToUpload.length > 0) {
                        //Validate if the file has the correct name
                        var currentFileName = $(".qq-upload-file-selector.qq-upload-file.qq-editable").attr("title");
                        if (currentFileName == fileName) {
                            //Set parameters
                            setFileParams();

                            //Upload files
                            $('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
                        } else {
                            _showAlert({
                                id: "AlertChangeFileName",
                                showTo: $modal.find(".modal-body"),
                                content: "Please change your file name from <b>" + currentFileName + "</b> to <b>" + fileName + "</b>, click on 'Set Name' button to set the correct file name."
                            });
                        }
                    } else {
                        _showAlert({
                            id: "AlertSelectFile",
                            showTo: $modal.find(".modal-body"),
                            content: "Please select a file."
                        });
                    }
                }
            }],
            addCloseButton: true,
            title: "Upload File",
            contentHtml: htmlContentModal,
            onReady: function ($modal) {
                //Init Fine Uploader
                initFileUploader(rowData, $modal, setFileParams);

                //Show Overwrite Warning
                if (rowData["FileStatus"] == "Received") {
                    _showAlert({
                        id: "AlertOverwrite",
                        type: "warning",
                        showTo: $modal.find(".modal-body"),
                        content: "The file already exist and will be overwritten."
                    });
                }
            }
        });
    }
}

function uploadExtraDoc() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#tblProcessFiles', true);
    if (rowData) {
        var fileName = rowData["FileNamePatternText"];
        var htmlContentModal = '';
        htmlContentModal += "<b>Process: </b> " + rowData["Process"] + " <br/>";
        htmlContentModal += "<b>File Name: </b> <span class='txtFileName'>" + fileName + "</span><br/>";
        htmlContentModal += "<b>Location to upload: </b> " + rowData["FolderFullPath"] + "<br/><br/>";
        htmlContentModal += "<div id='fine-uploader-manual-trigger'></div><br/>";
        htmlContentModal += "Comments:";
        htmlContentModal += "<textarea class='form-control txtDetailAuditLog' placeholder='(Required)' style='height:100px !important;'></textarea>";

        //Set File Upload Parameters
        function setFileParams(idPFDetailAuditLog) {
            rowData["IDPFDetailAuditLog"] = (idPFDetailAuditLog ? idPFDetailAuditLog : 0);

            $('#fine-uploader-manual-trigger').fineUploader('setParams', {
                'jsonProcessFile': _toJSON(rowData),
                'filePrefix': "(Extra) ",
                'comment': $(".txtFileComment").val(),
                'userRole': getUserRoleAuditLog(),
                'isExtraFile': "1"
            });
        }

        _showModal({
            modalId: "modalUpload",
            width: '70%',
            buttons: [{
                name: "<i class='fa fa-cloud-upload'></i> Upload",
                class: "btn-success",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    //Validate required Comment
                    if ($(".txtDetailAuditLog").val()) {
                        
                        //Add Comments
                        addDetailAuditLog({
                            type: "Extra Files",
                            idPFDetails: rowData["IDPFDetail"],
                            fileStatus: rowData["FileStatus"],
                            statusByChecker: rowData["StatusByChecker"],
                            comment: $(".txtDetailAuditLog").val(),
                            onSuccess: function (idPFDetailAuditLog) {
                                //Set parameters
                                setFileParams(idPFDetailAuditLog);

                                //Upload files
                                $('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
                            }
                        });
                    } else {
                        _showAlert({
                            id: "CommentValidation",
                            showTo: $modal.find(".modal-body"),
                            content: "Please add comments about the additional files."
                        });
                    }
                }
            }],
            addCloseButton: true,
            title: "Upload Extra Files",
            contentHtml: htmlContentModal,
            onReady: function ($modal) {
                initFileUploaderExtra(rowData, $modal, setFileParams);
            }
        });
    }

}

function addDetailAuditLog(customOptions) {
    var options = {
        type: "",
        idPFNotification: "",
        idPFDetails: "",
        fileStatus: "",
        statusByChecker: "",
        comment: "",
        onSuccess: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    var spParams = [
        { "Name": "@Action", "Value": "Insert" },
        { "Name": "@Type", "Value": options.type },
        { "Name": "@IDPFDetails", "Value": options.idPFDetails },
        { "Name": "@FileStatus", "Value": options.fileStatus },
        { "Name": "@StatusByChecker", "Value": options.statusByChecker },
        { "Name": "@Comment", "Value": options.comment },
        { "Name": "@UserRole", "Value": getUserRoleAuditLog() },
        { "Name": "@SessionSOEID", "Value": _getSOEID() }
    ];

    if (options.idPFNotification) {
        spParams.push({ "Name": "@IDPFNotification", "Value": options.idPFNotification });
    }

    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Adding comments...",
        name: "[dbo].[spSDocProdCtrlAdminPFDetailAuditLog]",
        params: spParams,
        success: {
            fn: function (responseList) {
                //Return New ID
                options.onSuccess(responseList[0].ID);
            }
        }
    });
}

function addNotification(customOptions) {
    var options = {
        type: "",
        comment: "",
        onSuccess: function(){}
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Adding notification record...",
        name: "[dbo].[spSDocProdCtrlAdminPFNotification]",
        params: [
            { "Name": "@Action", "Value": "Insert" },
            { "Name": "@Type", "Value": options.type },
            { "Name": "@Comment", "Value": options.comment },
            { "Name": "@SessionSOEID", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                //Return New ID
                options.onSuccess(responseList[0].ID);
            }
        }
    });
}

function isValidFilters() {
    var result = true;

    //Get Frequency
    var selFrequency = $("#selFrequency").find("option:selected").val();

    //Day Field
    if (selFrequency == "Daily") {
        var processDate = $("#processDate").val();
        if (!processDate) {
            result = false;
            _showNotification("error", "Please select a Day", "SelectDay");
        }
    }

    //Month Field
    if (selFrequency == "Monthly") {
        var $optSelected = $("#selMonth").find("option:selected");

        if (!$optSelected.val()) {
            result = false;
            _showNotification("error", "Please select a Month", "SelectMonth");
        }
    }

    return result;
}

function rejectDoc() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#tblProcessFiles', true);
    if (rowData) {

        if (rowData["StatusByChecker"] != "Rejected") {

            if (rowData["FileStatus"] == "Received") {

                //Fix: Hide PO Select
                $(".select2-container").css("z-index", "0");

                var fileName = rowData["FileNamePatternText"];
                var htmlContentModal = '';
                htmlContentModal += "<b>Process: </b> " + rowData["Process"] + " <br/>";
                htmlContentModal += "<b>File Name: </b> <span class='txtFileName'>" + fileName + "</span><br/>";
                htmlContentModal += "<b>Uploaded Location: </b> " + rowData["FolderFullPath"] + "<br/><br/>";
                htmlContentModal += "<b>Comment: </b> <br/><textarea id='txtCommentReject' placeholder='(Required)' style='width:100%;height: 130px;'></textarea><br/>";

                _showModal({
                    width: '50%',
                    modalId: "modalDelApp",
                    addCloseButton: true,
                    buttons: [{
                        name: "Reject",
                        class: "btn-danger",
                        closeModalOnClick: false,
                        onClick: function ($modal) {
                            //Validate required Comment
                            if ($("#txtCommentReject").val()) {

                                //Update status to Rejected.
                                _callProcedure({
                                    loadingMsgType: "topBar",
                                    loadingMsg: "Rejecting process file...",
                                    name: "[dbo].[spSDocProdCtrlAdminPFDetail]",
                                    params: [
                                        { "Name": "@IDPFDetail", "Value": rowData.IDPFDetail },
                                        { "Name": "@AuditLogType", "Value": 'File Rejected' },
                                        { "Name": "@StatusByChecker", "Value": 'Rejected' },
                                        { "Name": "@Comment", "Value": $("#txtCommentReject").val() },
                                        { "Name": "@UserRole", "Value": getUserRoleAuditLog() },
                                        { "Name": "@ServerURL", "Value": _getViewVar("ServerURL") },
                                        { "Name": "@SessionSOEID", "Value": _getSOEID() }
                                    ],
                                    success: {
                                        fn: function (responseList) {

                                            //Send rejected email
                                            sendEmail({
                                                comment: $("#txtCommentReject").val(),
                                                idPFDetail: rowData.IDPFDetail,
                                                typeNotification: "File Rejected",       //Comment | File Rejected
                                                onSuccess: function () {
                                                    //Refresh Comments and File Status
                                                    refreshRows([rowData], function () {
                                                        //Show notification
                                                        _showNotification("success", "Process File '" + rowData["FileNamePatternText"] + "' was rejected successfully and notified to Makers.", "AlertReject");

                                                        //Close Modal
                                                        $modal.find(".close").click();
                                                    });
                                                }
                                            });
                                        }
                                    }
                                });
                            } else {
                                _showAlert({
                                    id: "CommentValidation",
                                    showTo: $modal.find(".modal-body"),
                                    content: "Comment cannot be empty."
                                });
                            }
                        }
                    }],
                    title: "Are you sure you want to reject this row?",
                    contentHtml: htmlContentModal
                });
            } else {
                _showNotification("error", "You cannot reject this row because the file is not received.", "AlertIsNotReceived");
            }
        } else {
            _showNotification("error", rowData["FileNamePatternText"] + " is already Rejected", "AlertIsRejected");
        }
    }
}

function approveDoc() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#tblProcessFiles', true);
    if (rowData) {

        if (rowData["StatusByChecker"] != "Approved") {

            if (rowData["FileStatus"] == "Received") {

                //Fix: Hide PO Select
                $(".select2-container").css("z-index", "0");

                var fileName = rowData["FileNamePatternText"];
                var htmlContentModal = '';
                htmlContentModal += "<b>Process: </b> " + rowData["Process"] + " <br/>";
                htmlContentModal += "<b>File Name: </b> <span class='txtFileName'>" + fileName + "</span><br/>";
                htmlContentModal += "<b>Uploaded Location: </b> " + rowData["FolderFullPath"] + "<br/><br/>";
                htmlContentModal += "<b>Comment: </b> <br/><textarea id='txtCommentApprove' placeholder='(Optional)' style='width:100%;height: 130px;'></textarea><br/>";

                _showModal({
                    width: '50%',
                    modalId: "modalApprove",
                    addCloseButton: true,
                    buttons: [{
                        name: "Approve",
                        class: "btn-success",
                        closeModalOnClick: false,
                        onClick: function ($modal) {
                            var commentApproved = ($("#txtCommentApprove").val() ? $("#txtCommentApprove").val() : "");

                            //Save comment
                            _callProcedure({
                                loadingMsgType: "topBar",
                                loadingMsg: "Approving process file...",
                                name: "[dbo].[spSDocProdCtrlAdminPFDetail]",
                                params: [
                                    { "Name": "@IDPFDetail", "Value": rowData.IDPFDetail },
                                    { "Name": "@AuditLogType", "Value": 'File Approved' },
                                    { "Name": "@StatusByChecker", "Value": 'Approved' },
                                    { "Name": "@Comment", "Value": commentApproved },
                                    { "Name": "@UserRole", "Value": getUserRoleAuditLog() },
                                    { "Name": "@ServerURL", "Value": _getViewVar("ServerURL") },
                                    { "Name": "@SessionSOEID", "Value": _getSOEID() }
                                ],
                                success: {
                                    fn: function (responseList) {
                                        //Show notification
                                        _showNotification("success", "Process File '" + rowData["FileNamePatternText"] + "' was approved successfully.", "AlertApproved");

                                        //Send Email
                                        sendEmailStakeholder(function () {

                                            //Close Modal
                                            $modal.find(".close").click();
                                        });
                                    }
                                }
                            });
                        }
                    }],
                    title: "Are you sure you want to approve this row?",
                    contentHtml: htmlContentModal
                });

            } else {
                _showNotification("error", "You cannot approve this row because the file is not received.", "AlertIsNotReceived");
            }
        } else {
            _showNotification("error", rowData["FileNamePatternText"] + " is already Approved", "AlertIsApproved");
        }
    }
}

function createIfNotExistsFolderPath(options) {
    _callServer({
        loadingMsgType: "topBar",
        loadingMsg: "Checking folder full path...",
        url: '/SupportDocProdCtrl/CreateIfNotExistsFolderPath',
        data: {
            typeAuthentication: _getTypeAuthentication(),
            user: _encodeSkipSideminder(_getWindowsUser()),
            pass: _getWindowsPassword(),
            folderPath: _encodeSkipSideminder(options.folderPath)
        },
        success: function (respond) {
            if (options.onSuccess) {
                options.onSuccess(respond);
            }
        }
    });
}

function openInExcel() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#tblProcessFiles', true);
    if (rowData) {
        var Excel = new ActiveXObject("Excel.Application");
        Excel.Visible = true;
        Excel.Workbooks.Open(rowData.FileFullPath);
    }
}

function copyLinkToClipboard() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#tblProcessFiles', true);
    if (rowData) {
        _copyToClipboard(rowData.FolderFullPath);
        _showNotification("success", "Link: '" + rowData.FolderFullPath + "' was copied succesfully to clipboard.", "CopyLink");

        createIfNotExistsFolderPath({
            folderPath: rowData.FolderFullPath,
            onSuccess: function (response) {
                console.log(response);
            }
        });

        if ($("#HFViewType").val() == "Stakeholder") {
            //Add Audit Record
            addDetailAuditLog({
                type: "Link Copied",
                idPFDetails: rowData["IDPFDetail"],
                fileStatus: rowData["FileStatus"],
                statusByChecker: rowData["StatusByChecker"],
                comment: "",
                onSuccess: function (idPFDetailAuditLog) { }
            });
        }
    }
}

function exportToExcel() {
    var spName = "[dbo].[spSDocPeriodListPagingExcel]";
    var spParams = [
        { "Name": "@StrWhere", "Value": "WHERE ( [Year] = " + $("#selPeriod").find("option:selected").attr("year") + " AND [Month] = " + $("#selPeriod").find("option:selected").attr("month") + " AND ([NotificationStatus] ='" + $("#HFNotificationStatus").val() + "') AND [ResponsilbleSOEID] LIKE '%" + $("#HFSOEIDResponsible").val() + "%')" },
        { "Name": "@EndIndexRow", "Value": 100000 }
    ];
    var filename = "SupportDocToolPendingDocs - " + $("#selPeriod").find("option:selected").val();

    _downloadExcel({
        sql: _getSqlProcedure(spName, spParams),
        filename: filename,
        success: {
            msg: "Please Wait. Generating report..."
        }
    });
}

function refreshRows(rowsToUpdate, onSuccess) {
    var idPFDetails = [];

    //Get IDPFDetail
    for (var i = 0; i < rowsToUpdate.length; i++) {
        idPFDetails.push(rowsToUpdate[i]["IDPFDetail"]);
    }

    //Refresh Comments and File Status
    getProcessFilesByFilters({
        checkFileStatus: true,
        loadingMsgType: "topBar",
        onSuccess: function (processList) {
            var idsUpdated = [];
            var rowsUpdated = [];

            for (var i = 0; i < rowsToUpdate.length; i++) {
                var objProcessFileUpdated = _findOneObjByProperty(processList, "IDPFDetail", rowsToUpdate[i]["IDPFDetail"]);

                //Refresh FileStatus in selected row
                rowsToUpdate[i]["FileStatus"] = objProcessFileUpdated.FileStatus;

                //Refresh StatusByChecker in selected row
                rowsToUpdate[i]["StatusByChecker"] = objProcessFileUpdated.StatusByChecker;

                //Refresh NotificationEmailCount in selected row
                rowsToUpdate[i]["NotificationEmailCount"] = objProcessFileUpdated.NotificationEmailCount;

                //Refresh NotificationEmailsJson in selected row
                rowsToUpdate[i]["NotificationEmailsJson"] = objProcessFileUpdated.NotificationEmailsJson;

                //Refresh AuditLogFilesCount in selected row
                rowsToUpdate[i]["AuditLogFilesCount"] = objProcessFileUpdated.AuditLogFilesCount;

                //Refresh AuditLogFilesJson in selected row
                rowsToUpdate[i]["AuditLogFilesJson"] = objProcessFileUpdated.AuditLogFilesJson;

                //Refresh AuditLogsCount in selected row
                rowsToUpdate[i]["AuditLogsCount"] = objProcessFileUpdated.AuditLogsCount;

                //Refresh AuditLogsJson in selected row
                rowsToUpdate[i]["AuditLogsJson"] = objProcessFileUpdated.AuditLogsJson;

                idsUpdated.push(rowsToUpdate[i].uid);
                rowsUpdated.push(rowsToUpdate[i]);
            }

            //Update Data in Table
            $('#tblProcessFiles').jqxGrid('updaterow', idsUpdated, rowsUpdated);

            // expand all groups.
            $('#tblProcessFiles').jqxGrid('expandallgroups');

            if (onSuccess) {
                onSuccess();
            }
        },
        filters: {
            idPFDetails: idPFDetails.join(",")
        }
    });
}

function initFileUploader(rowData, $modal, pfnSetFileParams) {
    $('#fine-uploader-manual-trigger').fineUploader({
        template: 'qq-template-manual-trigger',
        request: {
            endpoint: _getViewVar("SubAppPath") + '/SupportDocProdCtrl/Upload'
        },
        thumbnails: {
            placeholders: {
                waitingPath: _getViewVar("SubAppPath") + '/Content/Shared/plugins/fine-uploader/placeholders/waiting-generic.png',
                notAvailablePath: _getViewVar("SubAppPath") + '/Content/Shared/plugins/fine-uploader/placeholders/not_available-generic.png'
            }
        },
        callbacks: {
            onAllComplete: function (succeeded, failed) {
                console.log(succeeded, failed);
                if (failed.length == 0) {
                    //Add Detail AuditLog
                    addDetailAuditLog({
                        type: "File Uploaded",
                        idPFDetails: rowData["IDPFDetail"],
                        fileStatus: "Received",
                        statusByChecker: rowData["StatusByChecker"],
                        comment: ($(".txtFileComment").val() ? $(".txtFileComment").val() : ""),
                        onSuccess: function (idPFDetailAuditLog) {
                            //Send rejected email
                            sendEmail({
                                comment: ($(".txtFileComment").val() ? $(".txtFileComment").val() : ""),
                                idPFDetail: rowData["IDPFDetail"],
                                idPFDetailAuditLog: idPFDetailAuditLog,
                                typeNotification: "File Uploaded",       //Comment | File Rejected
                                onSuccess: function () {
                                    //Refresh Comments and File Status
                                    refreshRows([rowData], function () {
                                        //Show notification
                                        _showNotification("success", "The file was uploaded successfully.", "AlertFileUploaded");

                                        //Close Modal
                                        $modal.find(".close").click();
                                    });
                                }
                            });
                        }
                    });
                }
            },
            onError: function (id, name, errorReason, xhrOrXdr) {
                if (name && !_contains(errorReason, "has an invalid extension")) {
                    _showDetailAlert({
                        showTo: ".modal-body",
                        title: "Message",
                        shortMsg: "An error ocurred with the file '" + rowData["FileNamePatternText"] + "'.",
                        longMsg: "<pre>" + errorReason + "</pre>",
                        type: "Error",
                        viewLabel: "View Details"
                    });
                }
            },
            onSubmit: function (id, name) {
                pfnSetFileParams();
            },
            onManualRetry: function (id, name) {
                pfnSetFileParams();
            }
        },
        validation: {
            allowedExtensions: [rowData["FileExt"]],
            itemLimit: 1
            //sizeLimit: 51200 // 50 kB = 50 * 1024 bytes
        },
        autoUpload: false
    });
}

function initFileUploaderExtra(rowData, $modal, pfnSetFileParams) {
    $('#fine-uploader-manual-trigger').fineUploader({
        template: 'qq-template-manual-trigger-extra',
        request: {
            endpoint: _getViewVar("SubAppPath") + '/SupportDocProdCtrl/Upload'
        },
        thumbnails: {
            placeholders: {
                waitingPath: _getViewVar("SubAppPath") + '/Content/Shared/plugins/fine-uploader/placeholders/waiting-generic.png',
                notAvailablePath: _getViewVar("SubAppPath") + '/Content/Shared/plugins/fine-uploader/placeholders/not_available-generic.png'
            }
        },
        callbacks: {
            onAllComplete: function (succeeded, failed) {
                console.log(succeeded, failed);
                if (failed.length == 0) {
                    //Refresh Comments and File Status
                    refreshRows([rowData], function () {
                        //Show notification
                        _showNotification("success", "Files were uploaded successfully.", "AlertFileUploaded");

                        //Close Modal
                        $modal.find(".close").click();
                    });

                    //Refresh Table
                    //loadTableProcessFiles();

                    //Create Log
                    //_insertAuditLog({
                    //    Action: "Uploaded Doc",
                    //    Description: (plocationToSave + pfileName + '.' + pext)
                    //});
                }
            },
            onError: function (id, name, errorReason, xhrOrXdr) {
                if (name && !_contains(errorReason, "has an invalid extension")) {
                    _showDetailAlert({
                        showTo: ".modal-body",
                        title: "Message",
                        shortMsg: "An error ocurred with the file '" + rowData["FileNamePatternText"] + "'.",
                        longMsg: "<pre>" + errorReason + "</pre>",
                        type: "Error",
                        viewLabel: "View Details"
                    });
                }
            },
            onSubmit: function (id, name) {
                pfnSetFileParams();
            },
            onManualRetry: function (id, name) {
                pfnSetFileParams();
            }
        },
        autoUpload: false
    });
}