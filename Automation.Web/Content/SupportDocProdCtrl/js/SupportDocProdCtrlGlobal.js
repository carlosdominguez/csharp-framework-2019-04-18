/// <reference path="../../Shared/plugins/util/global.js" />

function _generatePattern(pattern, objProcess, objMoment) {
    //Replace {Process}
    if (_contains(pattern, "{Process}")) {
        pattern = _replaceAll("{Process}", objProcess.Process, pattern);
    }

    //Replace {Country}
    if (_contains(pattern, "{Country}")) {
        pattern = _replaceAll("{Country}", objProcess.Country, pattern);
    }

    //Replace {FileName}
    if (_contains(pattern, "{FileName}")) {
        pattern = _replaceAll("{FileName}", objProcess.FileName, pattern);
    }

    //Replace {FileExt}
    if (_contains(pattern, "{FileExt}")) {
        pattern = _replaceAll("{FileExt}", objProcess.FileExt, pattern);
    }

    //Replace {Year}
    if (_contains(pattern, "{Year}")) {
        pattern = _replaceAll("{Year}", objMoment.format('YYYY'), pattern);
    }

    //Replace {MM. MMM} eg. "11. Nov"
    if (_contains(pattern, "{MM. MMM}")) {
        pattern = _replaceAll("{MM. MMM}", objMoment.format('MM. MMM'), pattern);
    }

    //Replace {MM-DD} eg. "11-14"
    if (_contains(pattern, "{MM-DD}")) {
        pattern = _replaceAll("{MM-DD}", objMoment.format('MM-DD'), pattern);
    }

    //Replace {MM-DD-YYYY} eg. "11-15-2017"
    if (_contains(pattern, "{MM-DD-YYYY}")) {
        pattern = _replaceAll("{MM-DD-YYYY}", objMoment.format('MM-DD-YYYY'), pattern);
    }
    
    //Replace {MM-YYYY} eg. "11-2017"
    if (_contains(pattern, "{MM-YYYY}")) {
        pattern = _replaceAll("{MM-YYYY}", objMoment.format('MM-YYYY'), pattern);
    }

    //Replace {MMM-DD-YYYY} eg. "Nov-15-2017"
    if (_contains(pattern, "{MMM-DD-YYYY}")) {
        pattern = _replaceAll("{MMM-DD-YYYY}", objMoment.format('MMM-DD-YYYY'), pattern);
    }

    //Replace {MMM-YYYY} eg. "Nov-2017"
    if (_contains(pattern, "{MMM-YYYY}")) {
        pattern = _replaceAll("{MMM-YYYY}", objMoment.format('MMM-YYYY'), pattern);
    }

    return pattern;
}

function _loadProcessFilesTable(idTable) {
    $.jqxGridApi.create({
        showTo: idTable,
        options: {
            //for comments or descriptions
            height: "500",
            autoheight: false,
            autorowheight: false,
            showfilterrow: true,
            sortable: true,
            editable: true,
            groupable: true,
            //'singlerow', 'multiplerows', 'checkbox', 'multiplerowsadvanced' or 'multiplerowsextended'.
            selectionmode: "singlerow"
        },
        sp: {
            Name: "[dbo].[spSDocProdCtrlAdminProcessFile]",
            Params: [
                { Name: "@Action", Value: 'List' }
            ]
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set"
        },
        groups: ['Country'],
        columns: [
            //type: string - text - number - int - float - date - time 
            //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
            //cellsformat: ddd, MMM dd, yyyy h:mm tt
            //{ name: 'FolderPattern', text: 'Folder Pattern', type: 'string', width: '200px' },
            { name: 'IDProcessFile', type: 'number', hidden: 'true' },
            { name: 'IDProcess', type: 'number', hidden: 'true' },
            { name: 'IDFrequency', type: 'number', hidden: 'true' },
            { name: 'CountryCode', type: 'string', hidden: 'true' },
            { name: 'FileExt', type: 'string', hidden: 'true' },
            { name: 'CheckersJson', type: 'string', hidden: 'true' },
            { name: 'MakersJson', type: 'string', hidden: 'true' },
            { name: 'EmailsJson', type: 'string', hidden: 'true' },
            {
                name: 'IsSelected',
                text: "Select",
                type: 'bool',
                columntype: 'checkbox',
                width: '50px',
                pinned: true,
                filterable: false
            },
            { name: 'Process', text: 'Process', filtertype: 'checkedlist', type: 'string', width: '20%' },
            { name: 'Country', text: 'Country', filtertype: 'checkedlist', type: 'string', width: '10%' },
            { name: 'Frequency', text: 'Frequency', filtertype: 'checkedlist', type: 'string', width: '10%' },
            { name: 'FileName', text: 'File Name', filtertype: 'input', type: 'string', width: '30%' },
            {
                name: 'CheckersCount', text: 'Checkers', width: '8%', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                    var dataRecord = $(idTable).jqxGrid('getrowdata', rowIndex);
                    var htmlResult = '<span style="line-height:31px;margin-left:12px;">' + dataRecord.CheckersCount + '</span>';

                    //Add Checkers
                    if (dataRecord.CheckersJson) {
                        var rows = JSON.parse(dataRecord.CheckersJson);
                        if (rows.length > 0) {
                            var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                            for (var i = 0; i < rows.length; i++) {
                                var tempData = rows[i];
                                infoHtml += " " + tempData.Name + " (" + tempData.SOEID + ")<br>";
                            }

                            infoHtml += "</div>";

                            htmlResult +=
                                '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png"/> ' +
                                '</div>';
                        }
                    }

                    return htmlResult;
                }
            },
            {
                name: 'MakersCount', text: 'Makers', width: '8%', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                    var dataRecord = $(idTable).jqxGrid('getrowdata', rowIndex);
                    var htmlResult = '<span style="line-height:31px;margin-left:12px;">' + dataRecord.MakersCount + '</span>';

                    //Add Makers
                    if (dataRecord.MakersJson) {
                        var rows = JSON.parse(dataRecord.MakersJson);
                        if (rows.length > 0) {
                            var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                            for (var i = 0; i < rows.length; i++) {
                                var tempData = rows[i];
                                infoHtml += " " + tempData.Name + " (" + tempData.SOEID + ")<br>";
                            }

                            infoHtml += "</div>";

                            htmlResult +=
                                '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png"/> ' +
                                '</div>';
                        }
                    }

                    return htmlResult;
                }
            },
            {
                name: 'EmailsCount', text: 'Stakeholders', width: '8%', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                    var dataRecord = $(idTable).jqxGrid('getrowdata', rowIndex);
                    var htmlResult = '<span style="line-height:31px;margin-left:12px;">' + dataRecord.EmailsCount + '</span>';

                    //Add Emails
                    if (dataRecord.EmailsJson) {
                        var rows = JSON.parse(dataRecord.EmailsJson);
                        if (rows.length > 0) {
                            var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                            for (var i = 0; i < rows.length; i++) {
                                var tempData = rows[i];
                                infoHtml += " " + _htmlEncode(tempData.Email) + "<br>";
                            }

                            infoHtml += "</div>";

                            htmlResult +=
                                '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png"/> ' +
                                '</div>';
                        }
                    }

                    return htmlResult;
                }
            }
        ],
        ready: function () {
            //Add popup of information
            $(idTable).on("rowclick", function (event) {
                _GLOBAL_SETTINGS.tooltipsPopovers();

                //$('#grid').jqxGrid('selectrow', 10);
            });

            $(idTable).on('cellvaluechanged', function (event) {

                //Autoselect row on click Checkbox
                if (event.args.datafield == "IsSelected") {
                    if (args.newvalue) {
                        $(idTable).jqxGrid('selectrow', args.rowindex);
                    } else {
                        $(idTable).jqxGrid('unselectrow', args.rowindex);
                    }
                }

            });

            //On select row update edit button
            $(idTable).on('rowselect', function (event) {
                $(".btnEditProcessFile").attr("href", _getViewVar("SubAppPath") + "/SupportDocProdCtrl/AdminProcessFile?pidProcessFile=" + event.args.row.IDProcessFile);
            });

            // expand all groups.
            $(idTable).jqxGrid('expandallgroups');
        }
    });
}

function _toggleSelectAll(idTable) {
    var rows = $(idTable).jqxGrid("getRows");
    var idsToUpdate = [];
    var rowsToUpdate = [];
    var isSelected = ($(".btnToggleSelectAll").attr("updateSelectTo") == "1");

    for (var i = 0; i < rows.length; i++) {
        rows[i]["IsSelected"] = isSelected;
        idsToUpdate.push(rows[i].uid);
        rowsToUpdate.push(rows[i]);
    }

    if (isSelected) {
        $(".btnToggleSelectAll").html('<i class="fa fa-check-square-o"></i> Unselect All ');
        $(".btnToggleSelectAll").attr("updateSelectTo", "0");
    } else {
        $(".btnToggleSelectAll").html('<i class="fa fa-square-o"></i> Select All ');
        $(".btnToggleSelectAll").attr("updateSelectTo", "1");
    }

    //Update Data in Table
    $(idTable).jqxGrid('updaterow', idsToUpdate, rowsToUpdate);

    // expand all groups.
    $(idTable).jqxGrid('expandallgroups');
}

function _getCheckedRows(idTable) {
    var selectedRows = [];
    var processFiles = $(idTable).jqxGrid("getRows");

    for (var i = 0; i < processFiles.length; i++) {
        if (processFiles[i]["IsSelected"]) {
            selectedRows.push(processFiles[i]);
        }
    }

    return selectedRows;
}

function _loadProcessFileAdmin(customOptions) {
    //Default Options.
    var options = {
        showTo: "",
        idProcessFile: "",
        action: "Insert"
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Process File Form...",
        url: "/SupportDocProdCtrl/PartialViewAdminProcessFile?" + $.param({
            pidProcessFile: options.idProcessFile,
            paction: options.action
        }),
        success: function (htmlResponse) {
            $(options.showTo).contents().remove();
            $(options.showTo).html(htmlResponse);
        }
    });
}

function _checkProcessFilesExists(customOptions) {
    //Process Files to Post
    var processFilesToPost = [];

    //Default Options.
    var options = {
        loadingMsgType: "fullLoading",
        year: 0,
        month: 0,
        processDate: "",
        processFiles: [],
        onSuccess: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    //Remove unnecesary properties
    for (var i = 0; i < options.processFiles.length; i++) {
        processFilesToPost.push({
            IDPFDetail: options.processFiles[i].IDPFDetail,
            Year: options.year,
            Month: options.month,
            ProcessDate: options.processDate,
            IDProcessFile: options.processFiles[i].IDProcessFile,
            FileFullPath: options.processFiles[i].FileFullPath,
            FilePath: options.processFiles[i].FilePath,
            FolderPath: options.processFiles[i].FolderPath,
            StatusByChecker: options.processFiles[i].StatusByChecker,
            FileStatus: options.processFiles[i].FileStatus
        });
    }

    //Call Server
    _callServer({
        type: "POST",
        loadingMsgType: options.loadingMsgType,
        loadingMsg: "Checking process files in share folder...",
        url: '/SupportDocProdCtrl/CheckProcessFiles',
        data: {
            jsonProcessFiles: _toJSON(processFilesToPost),
            typeAuthentication: _getTypeAuthentication(),
            user: _encodeSkipSideminder(_getWindowsUser()),
            pass: _getWindowsPassword()
        },
        success: function (resultMsg) {
            if (options.onSuccess) {
                //_showNotification("info", resultMsg, "CheckedFilesAlert");
                options.onSuccess();
            }
        }
    });
}

function _insertAuditLog(options) {
    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Creating audit log...",
        name: "[dbo].[spSDocAuditAdmin]",
        params: [
            { "Name": "@TypeAction", "Value": "Insert" },
            { "Name": "@AuditAction", "Value": options.Action },
            { "Name": "@Description", "Value": options.Description },
            { "Name": "@CreatedBy", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                if (options.onSuccess) {
                    options.onSuccess();
                }
            }
        }
    });
}

//------------------------------------------------------
//Authentication methods
//------------------------------------------------------
function _setWindowsUser(user) {
    localStorage.setItem("WindowsUser", user);
}

function _getWindowsUser() {
    var userID = "";
    if (localStorage.getItem("WindowsUser")) {
        userID = localStorage.getItem("WindowsUser");
    }

    return userID;
}

function _setWindowsPassword(pass) {
    localStorage.setItem("WindowsPassword", _encodeAsciiString(pass));
}

function _getWindowsPassword(decodePass) {
    var pass = "";
    if (localStorage.getItem("WindowsPassword")) {
        pass = localStorage.getItem("WindowsPassword");

        if (decodePass) {
            pass = _decodeAsciiString(pass);
        }
    }

    return pass;
}

function _setTypeAuthentication(auth) {
    localStorage.setItem("TypeAuthentication", auth);
}

function _getTypeAuthentication() {
    var auth = "ToolAuthentication";
    if (localStorage.getItem("TypeAuthentication")) {
        auth = localStorage.getItem("TypeAuthentication");
    }

    return auth;
}
