/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="SupportDocProdCtrlGlobal.js" />

var regionList = {};

$(document).ready(function () {
    //Load table
    _loadProcessFilesTable("#tblProcessFiles");

    //On Click Edit Process File
    $(".btnEditProcessFile").click(function () {
        editProcessFile();
    });

    //On Click New Process File
    $(".btnNewProcessFile").click(function () {
        newProcessFile();
    });

    //On Click Delete Process File
    $(".btnDeleteProcessFile").click(function () {
        deleteProcessFile();
    });

    //On Click Toggle Select All
    $(".btnToggleSelectAll").click(function () {
        _toggleSelectAll("#tblProcessFiles");
    });

    //On Click Download Audit Report
    $(".btnExportToExcel").click(function () {
        downloadToExcelProcessList();
    });

});

function downloadToExcelProcessList() {
    _downloadExcel({
        spName: "[dbo].[spSDocProdCtrlListProcessFile]",
        spParams: [],
        filename: "ProcessFiles_" + moment().format('MMM-DD-YYYY-HH-MM'),
        success: {
            msg: "Please wait, generating report..."
        }
    });
}

function newProcessFile() {
    //Show tab with edit form
    $("#tab-label-process-file").show();

    //Load form to add or edit an process file
    _loadProcessFileAdmin({
        showTo: "#adminProcessFileContent",
        idProcessFile: 0,
        action: "Insert"
    });

    //Go to the Form Tab
    $("#tab-label-process-file").find("a").click();

    //Update tab label
    $("#lblProcessFileForm").html("New Process File");
}

function editProcessFile() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#tblProcessFiles', true);
    if (rowData) {
        //Show tab with edit form
        $("#tab-label-process-file").show();

        //Load form to add or edit an process file
        _loadProcessFileAdmin({
            showTo: "#adminProcessFileContent",
            idProcessFile: rowData.IDProcessFile,
            action: "Edit"
        });

        //Go to the Form Tab
        $("#tab-label-process-file").find("a").click();

        //Update tab label
        $("#lblProcessFileForm").html("Edit Process File");
    }
}

function deleteProcessFile() {
    //Delete 
    var objRowSelected = $.jqxGridApi.getOneSelectedRow('#tblProcessFiles', true);
    if (objRowSelected) {
        var htmlContentModal = "";
        htmlContentModal += "<b>Process: </b>" + objRowSelected['Process'] + "<br/>";
        htmlContentModal += "<b>File Name: </b>" + objRowSelected['FileName'] + "<br/>";

        _showModal({
            width: '40%',
            modalId: "modalDelRow",
            addCloseButton: true,
            buttons: [{
                name: "Delete",
                class: "btn-danger",
                onClick: function () {
                    if (objRowSelected.IDProcessFile != 0) {
                        _callProcedure({
                            loadingMsgType: "fullLoading",
                            loadingMsg: "Deleting row...",
                            name: "[dbo].[spSDocProdCtrlAdminProcessFile]",
                            params: [
                                { "Name": "@Action", "Value": "Delete" },
                                { "Name": "@IDProcessFile", "Value": objRowSelected.IDProcessFile },
                                { "Name": "@SessionSOEID", "Value": _getSOEID() }
                            ],
                            //Show message only for the last 
                            success: {
                                showTo: $('#tblProcessFiles').parent(),
                                msg: "Row was deleted successfully.",
                                fn: function () {
                                    var reloadData = setInterval(function () {
                                        //Load table
                                        _loadProcessFilesTable("#tblProcessFiles");

                                        //Remove changes
                                        $.jqxGridApi.rowsChangedFindById('#tblProcessFiles').rows = [];

                                        clearInterval(reloadData);
                                    }, 500);
                                }
                            }
                        });
                    } else {
                        $("#tblProcessFiles").jqxGrid('deleterow', objRowSelected.uid);
                    }
                }
            }],
            title: "Are you sure you want to delete this row?",
            contentHtml: htmlContentModal
        });
    }
}