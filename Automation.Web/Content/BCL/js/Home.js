var _GlobalVars = {
    Project: [],
    Approver: [],
    Requestor: [],
    user: []
};

var viewModel = {
    Project: ko.observable(),
    Approver: ko.observable(),
    Requestor: ko.observable(),
    visibleButton: ko.observable()
};

var ProjectSelected;
var ApproverSelected;
var RequestorSelected;

viewModel.ProjectSelected = ko.dependentObservable({
    read: viewModel.Project,
    write: function (result) {
        ProjectSelected = result;
        if (typeof ProjectSelected === 'undefined' || !ProjectSelected
            || typeof ApproverSelected === 'undefined' || !ApproverSelected
            || typeof RequestorSelected === 'undefined' || !RequestorSelected
            ) {
            viewModel.visibleButton(false);
        } else {
            viewModel.visibleButton(true);
        }
    }
});
viewModel.ApproverSelected = ko.dependentObservable({
    read: viewModel.Approver,
    write: function (result) {
        ApproverSelected = result;
        if (typeof ProjectSelected === 'undefined' || !ProjectSelected
            || typeof ApproverSelected === 'undefined' || !ApproverSelected
            || typeof RequestorSelected === 'undefined' || !RequestorSelected
            ) {
            viewModel.visibleButton(false);
        } else {
            viewModel.visibleButton(true);
        }
    }
});
viewModel.RequestorSelected = ko.dependentObservable({
    read: viewModel.Requestor,
    write: function (result) {
        RequestorSelected = result;
        if (typeof ProjectSelected === 'undefined' || !ProjectSelected
            || typeof ApproverSelected === 'undefined' || !ApproverSelected
            || typeof RequestorSelected === 'undefined' || !RequestorSelected
            ) {
            viewModel.visibleButton(false);
        } else {
            viewModel.visibleButton(true);
        }
    }
});


$(document).ready(function () {

    var listOfParams2 = {
        ID: _getSOEID()
    }

    


    BindJsonAlias("get_Project", "Project", "BCL", listOfParams2, function () {
        viewModel.Project(_GlobalVars["Project"]);

        BindJsonAlias("get_Checker", "Approver", "BCL", listOfParams2, function () {
            viewModel.Approver(_GlobalVars["Approver"]);


            BindJsonAlias("get_Maker", "Requestor", "BCL", listOfParams2, function () {
                viewModel.Requestor(_GlobalVars["Requestor"]);

                viewModel.visibleButton(false);
                ko.applyBindings(viewModel);


                document.getElementById("cbRequestor").value = _GlobalVars["Requestor"][0].SOEID;
                viewModel.RequestorSelected(_GlobalVars["Requestor"][0].SOEID);
            });
        });
    });

    //_callServer({
    //    loadingMsgType: "fullLoading",
    //    loadingMsg: "Loading data...",
    //    url: '/BCL/proc_AddUsers' ,
    //    type: "post",
    //    data: {
    //        'data': JSON.stringify(listOfParams2)
    //    },
    //    success: function (json) {

    //    }
    //});

});


function AddRequest()
{
    if(ProjectSelected == "5"){
        var url = '/BCL/GenesisOptimaAdjustment/0/' + RequestorSelected + '/' + ApproverSelected;
        window.location.href = url;
    }
    if (ProjectSelected == "6") {
        var url = '/BCL/BSSAdjustment/0/' + RequestorSelected + '/' + ApproverSelected;
        window.location.href = url;
    }
}
