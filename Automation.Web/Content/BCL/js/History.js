﻿var _GlobalVars = {
    pendingForApprove: [],
    Approver: [],
    gridMetaData: [],
    gridPath: [],
    gridAttestation: [],
}


var viewModel = {
    pendingForApprove: ko.observableArray(),
    Approver: ko.observable(),
    gridMetaData: ko.observableArray(),
    gridPath: ko.observableArray(),
    gridAttestation: ko.observableArray()
};

var ApproverSelected;
var ProjectSelected;

viewModel.gridPendingToApprove = new KOGridModel({
    data: viewModel.pendingForApprove,
    columns: [
        //{ header: "Actions", data: "ID", dataTemplate: 'divActions' }

         { header: "Project Name", data: "ProjectName" },
         { header: "Request ID", data: "RequestID" },
         { header: "Status", data: "RequestStatus" },
         { header: "Requester", data: "RequesterID" },
         { header: "Created Date", data: "CreatedDate" },
         { header: "Approver", data: "ApproverID" },
         { header: "Metadata", template: "divMetaData" },
         { header: "Adjustment ID", data: "AdjustmentID" },
         { header: "Functions", template: 'divAction' }
    ],
    pageSize: 10
});


viewModel.gridAttestation1 = new KOGridModel({
    data: viewModel.gridAttestation,
    columns: [
        //{ header: "Actions", data: "ID", dataTemplate: 'divActions' }
         { header: "Attestation", template: "divAttestation" },
         { header: "Checker Status", data: "CheckerStatus" },
         { header: "Maker", data: "MakerStatus" },
         { header: "Requester", data: "MakerStatus" },
         { header: "Comments", data: "Comments" },
         { header: "Latest Maker Comments", data: "LatestMakerComments" },
         { header: "Latest Checker Comments", data: "LatestCheckerComments" }
    ],
    pageSize: 10
});

viewModel.ApproverSelected = ko.dependentObservable({
    read: viewModel.Approver,
    write: function (result) {
        ApproverSelected = result;
        if ( typeof ApproverSelected === 'undefined' || !ApproverSelected) {
            
        } else {
            
        }
    }
});


$(document).ready(function () {
    //$('.usd_input').mask('00000.00');
    var listOfParams = {
        SOEID: _getSOEID()
    }

    _hideMenu();

     var listOfParams2 = {
         ID: _getSOEID()
    }


    BindJsonAlias("myHistory", "pendingForApprove", "BCL", listOfParams, function () {
        viewModel.pendingForApprove(_GlobalVars["pendingForApprove"]);

        BindJsonAlias("get_Checker", "Approver", "BCL", listOfParams2, function () {
            viewModel.Approver(_GlobalVars["Approver"]);
        });

        ko.applyBindings(viewModel);

    });





});

var selectedID;
function ReviewRequest(item) {
    if (item.RequestStatus == "Pending for approval/validation") {
        selectedID = item.RequestID;;
        $("#changeApprover").modal();
    }
}

function ChangePath(item) {
    if (item.RequestStatus == "Pending for approval/validation") {
        selectedID = item.RequestID;;
        $("#changepath").modal();
    }
}
function CancelProject(item) {
    if (item.RequestStatus == "Pending for approval/validation") {
        selectedID = item.RequestID;;

        var listOfParams = {
            ID: selectedID,
            SOEID: _getSOEID()
        }

        BindJsonAlias("cancelRequest", "pendingForApprove", "BCL", listOfParams, function () {
            viewModel.pendingForApprove(_GlobalVars["pendingForApprove"]);
        });
    }
}
function View(item) {
    selectedID = item.RequestID;

    var listOfParams = {
        ID: selectedID,
        SOEID: _getSOEID()
    }

    BindJsonAlias("getRequestMetaData", "gridMetaData", "BCL", listOfParams, function () {
        viewModel.gridMetaData(_GlobalVars["gridMetaData"]);
    });

    BindJsonAlias("getPathList", "gridPath", "BCL", listOfParams, function () {
        viewModel.gridPath(_GlobalVars["gridPath"]);
    });

    BindJsonAlias("getDataToShow", "gridAttestation", "BCL", listOfParams, function () {
        viewModel.gridAttestation(_GlobalVars["gridAttestation"]);
    });



    $("#viewForm").modal();
}


function changeApprover()
{

    if (typeof ApproverSelected === 'undefined' || !ApproverSelected) {
        _showNotification("error", "Select one Approver.")
    } else {
        var listOfParams = {
            ID: selectedID,
            SOEID: _getSOEID(),
            newApprover: $('#cbApprover').val()
        }

        BindJsonAlias("changeApprover", "pendingForApprove", "BCL", listOfParams, function () {
            viewModel.pendingForApprove(_GlobalVars["pendingForApprove"]);
        });
    }

    

}


function addNewPath()
{

    if (typeof $('#txtNewPath').val() === 'undefined' || !$('#txtNewPath').val()) {
        _showNotification("error", "Add a new path.")
    } else {
        var listOfParams = {
            ID: selectedID,
            SOEID: _getSOEID(),
            newPath: $('#txtNewPath').val()
        }

        BindJsonAlias("changePath", "pendingForApprove", "BCL", listOfParams, function () {
            viewModel.pendingForApprove(_GlobalVars["pendingForApprove"]);
        });
    }
}