﻿var _GlobalVars = {
    pathAdded: [],
    checkerCorrect: [],
    finall: [],
    RequestMetaData: []
};

var viewModel = {
    pathAdded: ko.observableArray(),
    visibleMaker: ko.observable(),
    visibleChecker: ko.observable()
};

var count;

viewModel.removePath = function (Name) {
    self.pathAdded.remove(function (Path) {
        return Path.Path == Name;
    });

}

viewModel.pathDelete = ko.dependentObservable({
    read: viewModel.pathAdded,
    write: function (path) {
       
        var index = _GlobalVars["pathAdded"].findIndex(function (o) {
            return o.Path === path.Path;
        })
        if (index !== -1) _GlobalVars["pathAdded"].splice(index, 1);

        

        viewModel.pathAdded(_GlobalVars["pathAdded"]);

    },
    owner: viewModel
});


$(document).ready(function () {
    _hideMenu();


    var listOfParams2 = {
        ID: _getSOEID()
    }

    if ($('#hfMaker').val().toUpperCase() == $('#hfApprover').val().toUpperCase()) {
        var url = /BCL/;
        window.location.href = url;
    }

    if($('#hfType').val() == 0)
    {
        viewModel.visibleMaker(true);
        viewModel.visibleChecker(false);

        if ($('#hfMaker').val().toUpperCase() != _getSOEID().toUpperCase()) {
            var url = /BCL/;
            window.location.href = url;
        }

        



        var listOfParams3 = {
            ID: $('#hfApprover').val()
        }
        BindJsonAlias("get_CheckerCorrect", "checkerCorrect", "BCL", listOfParams3, function () {
            
            if (_GlobalVars["checkerCorrect"].length == 0)
            {
                var url = /BCL/;
                window.location.href = url;
            }
         
        });
    }
    else
    {
        viewModel.visibleMaker(false);
        viewModel.visibleChecker(true);

        var listOfParams3 = {
            ID: $('#hfType').val()
        }
        //getMETADATA

        //check is ok
        var listOfParams5 = {
            requestId: $('#hfType').val(),
            requesterID: $('#hfMaker').val(),
            approverID: $('#hfApprover').val(),
            type: 5
        }

        BindJsonAlias("checkIfIsOKOptima", "isOK", "BCL", listOfParams5, function () {
            if (_GlobalVars["isOK"].length == 0) {
                var url = /BCL/;
                window.location.href = url;
            }
        });


        BindJsonAlias("getPathList", "pathAdded", "BCL", listOfParams3, function () {
            viewModel.pathAdded(_GlobalVars["pathAdded"]);
        });

        BindJsonAlias("getRequestMetaData", "RequestMetaData", "BCL", listOfParams3, function () {

            $('#txtAdjutstmetn').disabled = true;
            $('#txtAdjustmentID').disabled = true;
            $('#txtAdjustmentContact').disabled = true;
            $('#ctl00_ContentPlaceHolder1_gridMetaData2_ctl03_ddl').disabled = true;
            $('#txtMTC').disabled = true;
            $('#txtReasonCode').disabled = true;
            document.getElementById("txtAdjutstmetn").readOnly = true;
            document.getElementById("txtAdjustmentID").readOnly = true;
            document.getElementById("txtAdjustmentContact").readOnly = true;
            document.getElementById("ctl00_ContentPlaceHolder1_gridMetaData2_ctl03_ddl").readOnly = true;
            document.getElementById("txtMTC").readOnly = true;
            document.getElementById("txtReasonCode").readOnly = true;
            $.each(_GlobalVars["RequestMetaData"], function (key, value) {
 
                if (value.ProjectMetadataID == 29)
                {
                    $('#txtAdjutstmetn').val(value.Value);

                }
                if (value.ProjectMetadataID == 30) {
                    $('#txtAdjustmentID').val(value.Value);
                }
                if (value.ProjectMetadataID == 31) {
                    $('#txtAdjustmentContact').val(value.Value);
                }
                if (value.ProjectMetadataID == 32) {
                    $('#ctl00_ContentPlaceHolder1_gridMetaData2_ctl03_ddl').val(value.Value);
                    $('#txtReasonCode').val(value.Value);
                }
                if (value.ProjectMetadataID == 33) {
                    $('#txtMTC').val(value.Value);
                }


            });

        });


        //check if id is equal approver maker

        if ($('#hfApprover').val() == _getSOEID()) {

        }
        if ($('#hfMaker').val() == _getSOEID()) {

        }

    }
    


    ko.applyBindings(viewModel);
});


function AddPath()
{
    var textPath = $('#txtPath').val();

    if (textPath != "")
    {
        var column = {
            "Path": textPath
        }

        _GlobalVars["pathAdded"].push(column);

        viewModel.pathAdded(_GlobalVars["pathAdded"]);
       

        $('#txtPath').val("");
    }

}

function SendRequest()
{
    count = 0;
    validateText('txtAdjutstmetn');
    validateText('txtAdjustmentID');
    //validateText('txtAdjustmentContact');
   // validateText('ctl00_ContentPlaceHolder1_gridMetaData2_ctl03_ddl');
    validateText('txtMTC');

    validatePath();

    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl02_Maker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl03_Maker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl04_Maker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl05_Maker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl06_Maker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl07_Maker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl08_Maker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl09_Maker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl10_Maker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl11_Maker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl12_Maker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl13_Maker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl14_Maker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl15_Maker');

    if(count == 0)
    {
        $("#sendRequest").disabled = true;

        var listOfParams2 = {
            ID: _getSOEID(),
            projectID : 5,
            requesterID: $('#hfMaker').val(),
            approverID: $('#hfApprover').val(),       
            IDAdjustment:29,     
            IDAdjustmentID :30,    
            IDAdjustmentContact:31,
            IDReason:32,           
            IDMTC:33,       
            Adjustment: $('#txtAdjutstmetn').val(),       
            AdjustmentID: $('#txtAdjustmentID').val(),
            AdjustmentContact: $('#txtAdjustmentContact').val() ,
            Reason: $('#ctl00_ContentPlaceHolder1_gridMetaData2_ctl03_ddl').val()     ,      
            MTC: $('#txtMTC').val()  ,            
            Reference: _GlobalVars["pathAdded"][0].Path,
            stepID1: 69,
            stepID2: 70,
            stepID3: 71,
            stepID4: 72,
            stepID5: 73,
            stepID6: 74,
            stepID7: 75,
            stepID8: 76,
            stepID9: 77,
            stepID10: 78,
            stepID11: 79,
            stepID12: 80,
            stepID13: 81,
            stepID14: 82,
            statusStepID1 :$('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl02_Maker').val(),
            statusStepID2 :$('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl03_Maker').val(),
            statusStepID3 :$('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl04_Maker').val(),
            statusStepID4 :$('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl05_Maker').val(),
            statusStepID5 :$('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl06_Maker').val(),
            statusStepID6 :$('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl07_Maker').val(),
            statusStepID7 :$('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl08_Maker').val(),
            statusStepID8 :$('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl09_Maker').val(),
            statusStepID9 :$('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl10_Maker').val(),
            statusStepID10:$('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl11_Maker').val(),
            statusStepID11:$('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl12_Maker').val(),
            statusStepID12:$('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl13_Maker').val(),
            statusStepID13:$('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl14_Maker').val(),
            statusStepID14:$('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl15_Maker').val(),
            descStepID1: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl02_commentConSheet').val(),
            descStepID2: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl03_commentConSheet').val(),
            descStepID3: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl04_commentConSheet').val(),
            descStepID4: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl05_commentConSheet').val(),
            descStepID5: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl06_commentConSheet').val(),
            descStepID6: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl07_commentConSheet').val(),
            descStepID7: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl08_commentConSheet').val(),
            descStepID8: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl09_commentConSheet').val(),
            descStepID9: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl10_commentConSheet').val(),
            descStepID10: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl11_commentConSheet').val(),
            descStepID11: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl12_commentConSheet').val(),
            descStepID12: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl13_commentConSheet').val(),
            descStepID13: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl14_commentConSheet').val(),
            descStepID14: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl15_commentConSheet').val(),
            statusID :0

        }

        BindJsonAlias("addRequest", "finall", "BCL", listOfParams2, function () {
            var url = /BCL/;
            window.location.href = url;
        });


    }
    else {
        _showNotification("error", "There are erros in the form.")
        $("#sendRequest").disabled = false;
    }

}
function validateText(idInput)
{
    var idtext = '#' + idInput;
    if ($(idtext).val() == "") {
        document.getElementById(idInput).style.borderColor = "#FF0000";
        count = count + 1;
    } else {
        document.getElementById(idInput).style.borderColor = "#e1e1e1";
    }
}

function validateCB(idInput) {
    var idtext = '#' + idInput;
    if ($(idtext).val() == "0") {
        document.getElementById(idInput).style.borderColor = "#FF0000";
        count = count + 1;
    } else {
        document.getElementById(idInput).style.borderColor = "#e1e1e1";
    }
}

function validatePath() {

    if (_GlobalVars["pathAdded"].length == "0") {
        document.getElementById('txtPath').style.borderColor = "#FF0000";
        count = count + 1;
    } else {
        document.getElementById('txtPath').style.borderColor = "#e1e1e1";
    }
}

function SendRequestChecker() {
    count = 0;

    validateCB('ctl00_ContentPlaceHolder1_gridEnrichment_ctl02_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridEnrichment_ctl03_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridEnrichment_ctl04_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridEnrichment_ctl05_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridEnrichment_ctl06_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridEnrichment_ctl07_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridEnrichment_ctl08_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridEnrichment_ctl09_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridEnrichment_ctl10_Checker');
 

    if (count == 0) {
        $("#sendRequest").disabled = true;

        var listOfParams2 = {
            ID: _getSOEID(),
            requestId: $('#hfType').val(),
            requesterID: $('#hfMaker').val(),
            approverID: $('#hfApprover').val(),
            stepID1: 83,
            stepID2: 84,
            stepID3: 85,
            stepID4: 86,
            stepID5: 87,
            stepID6: 88,
            stepID7: 89,
            stepID8: 90,
            stepID9: 91,
            statusStepID1: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl02_Checker').val(),
            statusStepID2: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl03_Checker').val(),
            statusStepID3: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl04_Checker').val(),
            statusStepID4: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl05_Checker').val(),
            statusStepID5: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl06_Checker').val(),
            statusStepID6: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl07_Checker').val(),
            statusStepID7: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl08_Checker').val(),
            statusStepID8: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl09_Checker').val(),
            statusStepID9: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl10_Checker').val(),

            descStepID1: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl02_commentConSheet').val(),
            descStepID2: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl03_commentConSheet').val(),
            descStepID3: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl04_commentConSheet').val(),
            descStepID4: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl05_commentConSheet').val(),
            descStepID5: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl06_commentConSheet').val(),
            descStepID6: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl07_commentConSheet').val(),
            descStepID7: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl08_commentConSheet').val(),
            descStepID8: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl08_commentConSheet').val(),
            descStepID9: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl10_commentConSheet').val(),

            statusID: 1

        }

        BindJsonAlias("addRequestChecker", "finall", "BCL", listOfParams2, function () {
            var url = /BCL/;
            window.location.href = url;
        });


    }
    else {
        _showNotification("error", "There are erros in the form.")
        $("#sendRequest").disabled = false;
    }

}

function RejectRequest()
{
    count = 0;

    validateCB('ctl00_ContentPlaceHolder1_gridEnrichment_ctl02_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridEnrichment_ctl03_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridEnrichment_ctl04_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridEnrichment_ctl05_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridEnrichment_ctl06_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridEnrichment_ctl07_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridEnrichment_ctl08_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridEnrichment_ctl09_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridEnrichment_ctl10_Checker');


    if (count == 0) {
        $("#sendRequest").disabled = true;

        var listOfParams2 = {
            ID: _getSOEID(),
            requestId: $('#hfType').val(),
            requesterID: $('#hfMaker').val(),
            approverID: $('#hfApprover').val(),
            stepID1: 83,
            stepID2: 84,
            stepID3: 85,
            stepID4: 86,
            stepID5: 87,
            stepID6: 88,
            stepID7: 89,
            stepID8: 90,
            stepID9: 91,
            statusStepID1: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl02_Checker').val(),
            statusStepID2: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl03_Checker').val(),
            statusStepID3: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl04_Checker').val(),
            statusStepID4: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl05_Checker').val(),
            statusStepID5: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl06_Checker').val(),
            statusStepID6: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl07_Checker').val(),
            statusStepID7: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl08_Checker').val(),
            statusStepID8: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl09_Checker').val(),
            statusStepID9: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl10_Checker').val(),

            descStepID1: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl02_commentConSheet').val(),
            descStepID2: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl03_commentConSheet').val(),
            descStepID3: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl04_commentConSheet').val(),
            descStepID4: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl05_commentConSheet').val(),
            descStepID5: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl06_commentConSheet').val(),
            descStepID6: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl07_commentConSheet').val(),
            descStepID7: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl08_commentConSheet').val(),
            descStepID8: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl09_commentConSheet').val(),
            descStepID9: $('#ctl00_ContentPlaceHolder1_gridEnrichment_ctl10_commentConSheet').val(),

            statusID: 3

        }

        BindJsonAlias("rejectRequestChecker", "finall", "BCL", listOfParams2, function () {
            var url = /BCL/;
            window.location.href = url;
        });


    }
    else {
        _showNotification("error", "There are erros in the form.")
        $("#sendRequest").disabled = false;
    }

}