﻿var formatter = new Intl.NumberFormat('en-US', {

    style: 'currency',
    currency: 'USD',

    minimumFractionDigits: 2,
    // the default value for minimumFractionDigits depends on the currency
    // and is usually already 2
});

function setTwoNumberDecimal(el) {
    el.value = parseFloat(el.value).toFixed(2);
};


function renderComboBox(div, textDefault, data, pMultiple, arrSelected) {

    $(div).find('option').remove().end();

    var response = _GlobalVars[data];

    $.each(response, function (index, element) {
        $(div).append($('<option>', {
            value: element.ID,
            text: element.Value
        }));

    })

    $(div).multiselect({
        click: function (event, ui) {
            if (ui.checked) {
                _GlobalVars[arrSelected].push(ui.value);
            } else {
                _GlobalVars[arrSelected] = jQuery.grep(_GlobalVars[arrSelected], function (value) {
                    return value != ui.value;
                });
            }
        },
        selectedText: function (numChecked, numTotal, checkedItems) {
            return numChecked + ' of ' + numTotal + ' checked';
        },
        checkAll: function (e) {
            _GlobalVars[arrSelected] = []
            $.each(e.target.childNodes, function (index, value) {
                _GlobalVars[arrSelected].push(e.target.childNodes[index].value);
            });

        },
        uncheckAll: function (e) {
            _GlobalVars[arrSelected] = []
        }
    });


    $(div).multiselect({
        multiple: pMultiple,
        noneSelectedText: textDefault
    }).multiselectfilter();

    //$(div).multiselect().multiselectfilter();
    $(div).multiselect("refresh");
    $(div).css('width', '100px');

}


function BindJson(procedure, varName, listOfParams, onSuccess) {

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/BCL/' + procedure,
        type: "post",
       
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {

            _GlobalVars[varName] = jQuery.parseJSON(json);

            if (onSuccess) {
                onSuccess();
            }
        }
    });
}


function BindJsonAlias(procedure, varName, alias, listOfParams, onSuccess) {

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/' + alias + '/' + procedure,
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {

            _GlobalVars[varName] = jQuery.parseJSON(json);

            if (onSuccess) {
                onSuccess();
            }
        }
    });
}


function renderComboBoxSingle(div, textDefault, data, pMultiple, arrSelected) {

    $(div).find('option').remove().end();

    var response = _GlobalVars[data];

    $.each(response, function (index, element) {
        $(div).append($('<option>', {
            value: element.ID,
            text: element.Value
        }));

    })

    $(div).multiselect({
        click: function (event, ui) {
            if (ui.checked) {
                _GlobalVars[arrSelected] = ui.value;
            } else {
                _GlobalVars[arrSelected] = jQuery.grep(_GlobalVars[arrSelected], function (value) {
                    return value != ui.value;
                });
            }
            viewQMemoListData();
        },
        selectedText: function (numChecked, numTotal, checkedItems) {
            return numChecked + ' of ' + numTotal + ' checked';
        }
    });


    $(div).multiselect({
        multiple: pMultiple,
        noneSelectedText: textDefault
    }).multiselectfilter();

    //$(div).multiselect().multiselectfilter();
    $(div).multiselect("refresh");
    $(div).css('width', '100px');


}

function handleNumber(event, mask) {
    /* numeric mask with pre, post, minus sign, dots and comma as decimal separator
        {}: positive integer
        {10}: positive integer max 10 digit
        {,3}: positive float max 3 decimal
        {10,3}: positive float max 7 digit and 3 decimal
        {null,null}: positive integer
        {10,null}: positive integer max 10 digit
        {null,3}: positive float max 3 decimal
        {-}: positive or negative integer
        {-10}: positive or negative integer max 10 digit
        {-,3}: positive or negative float max 3 decimal
        {-10,3}: positive or negative float max 7 digit and 3 decimal
    */
    with (event) {
        stopPropagation()
        preventDefault()
        if (!charCode) return
        var c = String.fromCharCode(charCode)
        if (c.match(/[^-\d,]/)) return
        with (target) {
            var txt = value.substring(0, selectionStart) + c + value.substr(selectionEnd)
            var pos = selectionStart + 1
        }
    }
    var dot = count(txt, /\./, pos)
    txt = txt.replace(/[^-\d,]/g, '')

    var mask = mask.match(/^(\D*)\{(-)?(\d*|null)?(?:,(\d+|null))?\}(\D*)$/); if (!mask) return // meglio exception?
    var sign = !!mask[2], decimals = +mask[4], integers = Math.max(0, +mask[3] - (decimals || 0))
    if (!txt.match('^' + (!sign ? '' : '-?') + '\\d*' + (!decimals ? '' : '(,\\d*)?') + '$')) return

    txt = txt.split(',')
    if (integers && txt[0] && count(txt[0], /\d/) > integers) return
    if (decimals && txt[1] && txt[1].length > decimals) return
    txt[0] = txt[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.')

    with (event.target) {
        value = mask[1] + txt.join(',') + mask[5]
        selectionStart = selectionEnd = pos + (pos == 1 ? mask[1].length : count(value, /\./, pos) - dot)
    }

    function count(str, c, e) {
        e = e || str.length
        for (var n = 0, i = 0; i < e; i += 1) if (str.charAt(i).match(c)) n += 1
        return n
    }
}