﻿var _GlobalVars = {
    pathAdded: [],
    checkerCorrect: [],
    finall: [],
    RequestMetaData: [],
    stepData: [],
    isOK: []
};

var viewModel = {
    pathAdded: ko.observableArray(),
    visibleMaker: ko.observable(),

    visibleChecker: ko.observable()
};

var count;

viewModel.removePath = function (Name) {
    self.pathAdded.remove(function (Path) {
        return Path.Path == Name;
    });

}

viewModel.pathDelete = ko.dependentObservable({
    read: viewModel.pathAdded,
    write: function (path) {

        var index = _GlobalVars["pathAdded"].findIndex(function (o) {
            return o.Path === path.Path;
        })
        if (index !== -1) _GlobalVars["pathAdded"].splice(index, 1);



        viewModel.pathAdded(_GlobalVars["pathAdded"]);

    },
    owner: viewModel
});


$(document).ready(function () {
    _hideMenu();


    var listOfParams2 = {
        ID: _getSOEID()
    }

    if ($('#hfMaker').val().toUpperCase() == $('#hfApprover').val().toUpperCase()) {
        var url = /BCL/;
        window.location.href = url;
    }

    if ($('#hfType').val() == 0) {
        viewModel.visibleMaker(true);
        viewModel.visibleChecker(false);

        if ($('#hfMaker').val().toUpperCase() != _getSOEID().toUpperCase()) {
            var url = /BCL/;
            window.location.href = url;
        }

        //check approver 

        var listOfParams3 = {
            ID: $('#hfApprover').val()
        }
        BindJsonAlias("get_CheckerCorrect", "checkerCorrect", "BCL", listOfParams3, function () {

            if (_GlobalVars["checkerCorrect"].length == 0) {
                var url = /BCL/;
                window.location.href = url;
            }

        });
    }
    else {
        viewModel.visibleMaker(false);
        viewModel.visibleChecker(true);
        
        if ($('#hfApprover').val().toUpperCase() != _getSOEID().toUpperCase()) {
            var url = /BCL/;
            window.location.href = url;
        }


        //check is ok
        var listOfParams5 = {
            requestId: $('#hfType').val(),
            requesterID: $('#hfMaker').val(),
            approverID: $('#hfApprover').val(),
            type: 6
        }

        BindJsonAlias("checkIfIsOKOptima", "isOK", "BCL", listOfParams5, function () {
            if (_GlobalVars["isOK"].length == 0) {
                var url = /BCL/;
                window.location.href = url;
            }
        });

        var listOfParams3 = {
            ID: $('#hfType').val()
        }
        //getMETADATA
        
        var listOfParams4 = {
            requestId: $('#hfType').val(),
            requesterID: "",
            approverID: ""

        }

        BindJsonAlias("getPathList", "pathAdded", "BCL", listOfParams3, function () {
            viewModel.pathAdded(_GlobalVars["pathAdded"]);
        });

        BindJsonAlias("getStepData", "stepData", "BCL", listOfParams4, function () {
            $.each(_GlobalVars["stepData"], function (key, value) {

                if (value.StepID == 92) {
                    $('#txtStep1').val(value.Name);
                    $('#desctxtStep1').val(value.Description);
                }
                if (value.StepID == 93) {
                    $('#txtStep2').val(value.Name);
                    $('#desctxtStep2').val(value.Description);
                }
                if (value.StepID == 94) {
                    $('#txtStep3').val(value.Name);
                    $('#desctxtStep3').val(value.Description);
                }
                if (value.StepID == 95) {
                    $('#txtStep4').val(value.Name);
                    $('#desctxtStep4').val(value.Description);
                }
                if (value.StepID == 96) {
                    $('#txtStep5').val(value.Name);
                    $('#desctxtStep5').val(value.Description);
                }
                if (value.StepID == 97) {
                    $('#txtStep6').val(value.Name);
                    $('#desctxtStep6').val(value.Description);
                }
                if (value.StepID == 98) {
                    $('#txtStep7').val(value.Name);
                    $('#desctxtStep7').val(value.Description);
                }


            });
        });


        BindJsonAlias("getRequestMetaData", "RequestMetaData", "BCL", listOfParams3, function () {

            $('#txtAdjutstmetn').disabled = true;
            $('#txtAdjustmentID').disabled = true;
            $('#txtAdjustmentContact').disabled = true;
            $('#ctl00_ContentPlaceHolder1_gridMetaData2_ctl03_ddl').disabled = true;
            $('#txtMTC').disabled = true;
            $('#txtReasonCode').disabled = true;
            document.getElementById("txtAdjutstmetn").readOnly = true;
            document.getElementById("txtAdjustmentID").readOnly = true;
            document.getElementById("txtAdjustmentContact").readOnly = true;
            document.getElementById("ctl00_ContentPlaceHolder1_gridMetaData2_ctl03_ddl").readOnly = true;
            document.getElementById("txtMTC").readOnly = true;
            document.getElementById("txtReasonCode").readOnly = true;
            $.each(_GlobalVars["RequestMetaData"], function (key, value) {

                if (value.ProjectMetadataID == 34) {
                    $('#txtAdjutstmetn').val(value.Value);

                }
                if (value.ProjectMetadataID == 35) {
                    $('#txtAdjustmentID').val(value.Value);
                }
                if (value.ProjectMetadataID == 36) {
                    $('#txtAdjustmentContact').val(value.Value);
                }
                if (value.ProjectMetadataID == 37) {
                    $('#ctl00_ContentPlaceHolder1_gridMetaData2_ctl03_ddl').val(value.Value);
                    $('#txtReasonCode').val(value.Value);
                }
                if (value.ProjectMetadataID == 38) {
                    $('#txtMTC').val(value.Value);
                }


            });

        });


        //check if id is equal approver maker

        if ($('#hfApprover').val() == _getSOEID()) {

        }
        if ($('#hfMaker').val() == _getSOEID()) {

        }

    }



    ko.applyBindings(viewModel);
});


function AddPath() {
    var textPath = $('#txtPath').val();

    if (textPath != "") {
        var column = {
            "Path": textPath
        }

        _GlobalVars["pathAdded"].push(column);

        viewModel.pathAdded(_GlobalVars["pathAdded"]);


        $('#txtPath').val("");
    }

}

function SendRequest() {
    count = 0;
    validateText('txtAdjutstmetn');
    validateText('txtAdjustmentID');
    //validateText('txtAdjustmentContact');
    // validateText('ctl00_ContentPlaceHolder1_gridMetaData2_ctl03_ddl');
    validateText('txtMTC');

    validatePath();

    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl02_Maker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl03_Maker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl04_Maker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl05_Maker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl06_Maker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl07_Maker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl08_Maker');


    if (count == 0) {
        $("#sendRequest").disabled = true;

        var listOfParams2 = {
            ID: _getSOEID(),
            projectID: 6,
            requesterID: $('#hfMaker').val(),
            approverID: $('#hfApprover').val(),
            IDAdjustment: 34,
            IDAdjustmentID: 35,
            IDAdjustmentContact: 36,
            IDReason: 37,
            IDMTC: 38,
            Adjustment: $('#txtAdjutstmetn').val(),
            AdjustmentID: $('#txtAdjustmentID').val(),
            AdjustmentContact: $('#txtAdjustmentContact').val(),
            Reason: $('#ctl00_ContentPlaceHolder1_gridMetaData2_ctl03_ddl').val(),
            MTC: $('#txtMTC').val(),
            Reference: _GlobalVars["pathAdded"][0].Path,
            stepID1: 92,
            stepID2: 93,
            stepID3: 94,
            stepID4: 95,
            stepID5: 96,
            stepID6: 97,
            stepID7: 98,
            statusStepID1: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl02_Maker').val(),
            statusStepID2: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl03_Maker').val(),
            statusStepID3: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl04_Maker').val(),
            statusStepID4: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl05_Maker').val(),
            statusStepID5: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl06_Maker').val(),
            statusStepID6: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl07_Maker').val(),
            statusStepID7: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl08_Maker').val(),
            descStepID1: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl02_commentConSheet').val(),
            descStepID2: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl03_commentConSheet').val(),
            descStepID3: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl04_commentConSheet').val(),
            descStepID4: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl05_commentConSheet').val(),
            descStepID5: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl06_commentConSheet').val(),
            descStepID6: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl07_commentConSheet').val(),
            descStepID7: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl08_commentConSheet').val(),
            statusID: 0

        }

        BindJsonAlias("addRequestBSS", "finall", "BCL", listOfParams2, function () {
            var url = /BCL/;
            window.location.href = url;
        });


    }
    else {
        _showNotification("error", "There are erros in the form.")
        $("#sendRequest").disabled = false;
    }

}
function validateText(idInput) {
    var idtext = '#' + idInput;
    if ($(idtext).val() == "") {
        document.getElementById(idInput).style.borderColor = "#FF0000";
        count = count + 1;
    } else {
        document.getElementById(idInput).style.borderColor = "#e1e1e1";
    }
}

function validateCB(idInput) {
    var idtext = '#' + idInput;
    if ($(idtext).val() == "0") {
        document.getElementById(idInput).style.borderColor = "#FF0000";
        count = count + 1;
    } else {
        document.getElementById(idInput).style.borderColor = "#e1e1e1";
    }
}

function validatePath() {

    if (_GlobalVars["pathAdded"].length == "0") {
        document.getElementById('txtPath').style.borderColor = "#FF0000";
        count = count + 1;
    } else {
        document.getElementById('txtPath').style.borderColor = "#e1e1e1";
    }
}

function SendRequestChecker() {
    count = 0;

    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl02_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl03_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl04_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl05_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl06_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl07_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl08_Checker');



    if (count == 0) {
        $("#sendRequest").disabled = true;

        var listOfParams2 = {
            ID: _getSOEID(),
            requestId: $('#hfType').val(),
            requesterID: $('#hfMaker').val(),
            approverID: $('#hfApprover').val(),
            stepID1: 92,
            stepID2: 93,
            stepID3: 94,
            stepID4: 95,
            stepID5: 96,
            stepID6: 97,
            stepID7: 98,
            statusStepID1: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl02_Checker').val(),
            statusStepID2: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl03_Checker').val(),
            statusStepID3: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl04_Checker').val(),
            statusStepID4: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl05_Checker').val(),
            statusStepID5: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl06_Checker').val(),
            statusStepID6: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl07_Checker').val(),
            statusStepID7: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl08_Checker').val(),
            descStepID1: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl02_commentConSheet').val(),
            descStepID2: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl03_commentConSheet').val(),
            descStepID3: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl04_commentConSheet').val(),
            descStepID4: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl05_commentConSheet').val(),
            descStepID5: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl06_commentConSheet').val(),
            descStepID6: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl07_commentConSheet').val(),
            descStepID7: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl08_commentConSheet').val(),


            statusID: 1

        }

        BindJsonAlias("addRequestCheckerBSS", "finall", "BCL", listOfParams2, function () {
            var url = /BCL/;
            window.location.href = url;
        });


    }
    else {
        _showNotification("error", "There are erros in the form.")
        $("#sendRequest").disabled = false;
    }

}

function RejectRequest() {
    count = 0;

    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl02_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl03_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl04_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl05_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl06_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl07_Checker');
    validateCB('ctl00_ContentPlaceHolder1_gridControlSheet_ctl08_Checker');


    if (count == 0) {
        $("#sendRequest").disabled = true;

        var listOfParams2 = {
            ID: _getSOEID(),
            requestId: $('#hfType').val(),
            requesterID: $('#hfMaker').val(),
            approverID: $('#hfApprover').val(),
            stepID1: 92,
            stepID2: 93,
            stepID3: 94,
            stepID4: 95,
            stepID5: 96,
            stepID6: 97,
            stepID7: 98,
            statusStepID1: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl02_Checker').val(),
            statusStepID2: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl03_Checker').val(),
            statusStepID3: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl04_Checker').val(),
            statusStepID4: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl05_Checker').val(),
            statusStepID5: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl06_Checker').val(),
            statusStepID6: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl07_Checker').val(),
            statusStepID7: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl08_Checker').val(),
            descStepID1: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl02_commentConSheet').val(),
            descStepID2: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl03_commentConSheet').val(),
            descStepID3: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl04_commentConSheet').val(),
            descStepID4: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl05_commentConSheet').val(),
            descStepID5: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl06_commentConSheet').val(),
            descStepID6: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl07_commentConSheet').val(),
            descStepID7: $('#ctl00_ContentPlaceHolder1_gridControlSheet_ctl08_commentConSheet').val(),
            statusID: 3

        }

        BindJsonAlias("rejectRequestCheckerBSS", "finall", "BCL", listOfParams2, function () {
            var url = /BCL/;
            window.location.href = url;
        });


    }
    else {
        _showNotification("error", "There are erros in the form.")
        $("#sendRequest").disabled = false;
    }

}