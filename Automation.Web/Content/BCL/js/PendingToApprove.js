﻿var _GlobalVars = {
    pendingForApprove: []
}


var viewModel = {
    pendingForApprove: ko.observableArray()
};

viewModel.gridPendingToApprove = new KOGridModel({
    data: viewModel.pendingForApprove,
    columns: [
        //{ header: "Actions", data: "ID", dataTemplate: 'divActions' }

         { header: "Project Name", data: "ProjectName" },
         { header: "Request ID", data: "RequestID" },
         { header: "Status", data: "RequestStatus" },
         { header: "Requester", data: "RequesterID" },
         { header: "Created Date", data: "CreatedDate" },
         { header: "Approver", data: "ApproverID" },
         { header: "Metadata", template: "divMetaData" },
         { header: "Adjustment ID", data: "AdjustmentID" },
         { header: "", template: 'divAction' }
         ],
    pageSize: 50
});


$(document).ready(function () {
    //$('.usd_input').mask('00000.00');
    var listOfParams = {
        SOEID: _getSOEID()
    }

    _hideMenu();


    BindJsonAlias("pendingForApprove", "pendingForApprove", "BCL", listOfParams, function () {
        viewModel.pendingForApprove(_GlobalVars["pendingForApprove"]);

        ko.applyBindings(viewModel);

    });

    


   
});


function ReviewRequest(item)
{
    var c = item;
    if (item.ProjectID == 5)
    {
        var url = '/BCL/GenesisOptimaAdjustment/' + item.RequestID + '/' + item.RequesterID + '/' + item.ApproverID;

    }
    if (item.ProjectID == 6) {
        var url = '/BCL/BSSAdjustment/' + item.RequestID + '/' + item.RequesterID + '/' + item.ApproverID;

    }


    window.location.href = url;
}