﻿
//#regionDOCUMENT READY
$(document).ready(function () {

    //--load droplist tables
    $("#jqxdropdownbutton_LVID").jqxDropDownButton({ template: "summer", width: "100%", height: 30 });
    $("#jqxdropdownbutton_LOGID").jqxDropDownButton({ template: "summer", width: "100%", height: 30 });

    loadDropDownLVIDs("");
    loadJurisdictionLocation();

    //--load initial search droplists
    initDropListLoads();

    //--initialize datetimepickers
    $("#txt_dueDateFrom").datepicker();
    $("#txt_dueDateTo").datepicker();

    //
    $(".js-example-basic-multiple").select2();

    $("#btnExportExcel").click(function () {
        downloadSearchToExcel();
    });



});
//#endregion

//#region LOAD DROPLIST TABLES
function loadDropDownLVIDs(data) {
    $.jqxGridApi.create({
        showTo: "#tblLVID",
        options: {
            //for comments or descriptions
            height: "600",
            width: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_ADMIN_LVID]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'idLVID', type: 'string', hidden: true },
            { name: 'lvid', type: 'string', width: '20%' },
            { name: 'legalEntityName', type: 'string', width: '40%' },
            { name: 'idLegalEntity', type: 'string', hidden: true },
            { name: 'corpCode', type: 'string', width: '20%' },
            { name: 'idCorpCode', type: 'string', hidden: true },
            { name: 'fein', type: 'string', width: '20%' },
            { name: 'idFEIN', type: 'string', hidden: true }
        ],

        ready: function () {
            _hideLoadingFullPage();
            $('#tblLVID').on('rowselect', function (event) {
                // event.args.rowindex is a bound index.
                if (typeof event.args.group == "undefined") {
                    var args = event.args;
                    var row = $("#tblLVID").jqxGrid('getrowdata', args.rowindex);
                    var dropDownContent = '<div id="selectedLVIDID" style="position: relative; width:100%; margin-left: 3px; margin-top: 5px;" idLVID="' + row.idLVID + '" >' + row['lvid'] + ' - ' + row['legalEntityName'] + '</div>';
                    $("#jqxdropdownbutton_LVID").jqxDropDownButton('setContent', dropDownContent);

                    //$('#txt_CorpCode').val(row['corpCode']);
                    //$('#txt_LegalEntity').val(row['legalEntityName']);
                    //$('#txtFEIN').val(row['fein']);

                    $('#drp_corpcode').val(row['idCorpCode']);
                    $('#drp_fein').val(row['idFEIN']);
                    $('#drp_legalentity').val(row['idLegalEntity']);
                }
            });
        }
    });
}

function loadJurisdictionLocation() {
    $.jqxGridApi.create({
        showTo: "#tblLOGID",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_JL_JURISDICTION]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'idJurisdictionLocation', text: "Log ID", type: 'string', width: '10%' },
            { name: 'responsableSoeid', text: "RESP SOEID", type: 'string', hidden: true },
            { name: 'idLVID', type: 'string', hidden: true },
            { name: 'lvid', type: 'string', text: "LVID", width: '10%' },
            { name: 'idLegalEntity', type: 'string', hidden: true },
            { name: 'legalEntityName', type: 'string', text: "LE", width: '30%' },
            { name: 'idCorpCode', type: 'string', hidden: true },
            { name: 'corpCode', type: 'string', text: "CORP CODE", hidden: true },
            { name: 'idFEIN', type: 'string', hidden: true },
            { name: 'fein', type: 'string', text: "FEIN", hidden: true },
            { name: 'idType', type: 'string', hidden: true },
            { name: 'type', type: 'string', text: "TYPE", hidden: true },
            { name: 'idState', type: 'string', hidden: true },
            { name: 'stateDivSub', type: 'string', text: "STATE", width: '20%' },
            { name: 'idAccount', type: 'string', hidden: true },
            { name: 'account', type: 'string', text: "ACCOUNT", hidden: true },
            { name: 'frequency', type: 'string', text: "FREQUENCY", width: '20%' },
            { name: 'idJurisdiction', type: 'string', hidden: true },
            { name: 'jurisdiction', type: 'string', text: "JURISDICTION", width: '20%' },
            { name: 'monthDue', type: 'string', text: "MONTH", hidden: true },
            { name: 'dueDay', type: 'string', text: "DUE DAY", hidden: true },
            { name: 'effectiveDate', type: 'string', text: "EFF DATE", hidden: true },
            { name: 'ctCorp', type: 'string', text: "CT CORP", hidden: true },
            { name: 'P2PLVID', type: 'string', text: "P2P LVID", hidden: true },
            { name: 'goc', type: 'string', text: "GOC", hidden: true },
            { name: 'idP2PGocsLvid', type: 'string', hidden: true },
            { name: 'vendorSupplier', type: 'string', text: "VEND SUP", hidden: true },
            { name: 'vendorSiteCode', type: 'string', text: "VEND SITE", hidden: true },
            { name: 'idCommodityCode', type: 'string', hidden: true },
            { name: 'commodityCode', type: 'string', text: "COMMODITY", hidden: true },
            { name: 'ckAch', type: 'string', text: "ACH ? ", hidden: true },
            { name: 'active', type: 'string', text: "ACTIVE ?", hidden: true },
            { name: 'idP2PVendorSite', type: 'string', hidden: true },
            { name: 'vendorSite', type: 'string', text: "P2P Vendor", hidden: true }
        ],

        ready: function () {
            _hideLoadingFullPage();
            $('#tblLOGID').on('rowselect', function (event) {
                // event.args.rowindex is a bound index.
                if (typeof event.args.group == "undefined") {
                    var args = event.args;
                    var row = $("#tblLOGID").jqxGrid('getrowdata', args.rowindex);
                    var dropDownContent = '<div id="selectedLOGID" style="position: relative; width:100%; margin-left: 3px; margin-top: 5px;" idLOGID="' + row.idJurisdictionLocation + '" >' + row['idJurisdictionLocation'] + ' - ' + row['legalEntityName'] + '</div>';
                    $("#jqxdropdownbutton_LOGID").jqxDropDownButton('setContent', dropDownContent);

                    $('#drp_corpcode').val(row['idCorpCode']);
                    $('#drp_fein').val(row['idFEIN']);
                    $('#drp_legalentity').val(row['idLegalEntity']);
                    $('#drp_state').val(row['idState']);
                    $('#drp_jurisdiction').val(row['idJurisdiction']);
                    $('#drp_account').val(row['idAccount']);
                    $('#drp_frequency').val(row['frequency']);
                    $('#drp_responsable').val(row['responsableSoeid']);


                    $('#drp_type').val(row['idType']).trigger('change');


                    var rows1 = $('#tblLVID').jqxGrid('getrows');
                    for (var y = 0; y < rows1.length; y++) {
                        if (row.idLVID == rows1[y].idLVID) {
                            $("#tblLVID").jqxGrid('selectrow', y);
                        }
                    }



                }
            });
        }
    });
}
//#endregion

//#region INICIALIZE - LOAD - VARIABLEs --> DROPLIST
//#region VARIABLES DROPLIST
var qry_corpcode, qry_fein, qry_legalentity, qry_state, qry_jurisdiction, qry_account, qry_frequency, qry_responsable, qry_type;
qry_corpcode = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT [idCorpCode] as [id],[corpCode] as [text] FROM [SAU].[dbo].[tblAdmin_CorpCode] where [active] = 1;";
qry_fein = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT  [idFEIN] as [id],[fein] as [text] FROM [SAU].[dbo].[tblAdmin_FEIN] where [active] = 1;";
qry_legalentity = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT [idLegalEntity] as [id],[legalEntityName] as [text] FROM [SAU].[dbo].[tblAdmin_LegalEntity] WHERE [active] = 1";
qry_state = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT [idState] as id ,[stateDivSub] as [text] FROM [SAU].[dbo].[tblAdmin_StateDivSub] WHERE active = 1 ";
qry_jurisdiction = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT [idJurisdiction] as id,[jurisdiction] as [text] FROM [SAU].[dbo].[tblAdmin_Jurisdiction] where active = 1 ";
qry_account = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT [idAccount] as id ,[account] as [text]  FROM [SAU].[dbo].[tblAdmin_Account] where active = 1";
qry_frequency = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT 'Annually' as id,'Annually' as [text] UNION SELECT 'Monthly' as id,'Monthly' as [text] UNION SELECT 'Quarter1' as id,'Quarter1' as [text] UNION SELECT 'Quarter2' as id,'Quarter2' as [text] UNION SELECT 'NA' as id,'NA' as [text] UNION SELECT 'Semi' as id,'Semi' as [text] ;";
qry_responsable = "SELECT * FROM (SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT  [SOEID] as id ,[Name] as [text] FROM [SAU].[dbo].[tblAutomation_User] WHERE IsDeleted = 0 ) as a order by [text];";
qry_type = "SELECT [idType] as id,[description] as [text] FROM [SAU].[dbo].[tblAdmin_Type] where active = 1 order by description";
qry_rejectionReason = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT [rejectionReason] as id ,[rejectionReason] as [text] FROM [tblAdmin_RejectionReason] where active = 1";
//#endregion

//#region LOAD DROPLISTS
function loadDropDowns(selectQuery, selectedId, dropList) {
    //Set default value  
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Log ID Details...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify(selectQuery) },
        type: "post",
        success: function (resultList) {
            $("#" + dropList + "").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $("#" + dropList + "").append($('<option>', { value: objFunction.id, text: objFunction.text, selected: (objFunction.id == selectedId) }));
            }
        }
    });
};

//#endregion

//#region INICIATE DROPLISTS
function initDropListLoads() {
    loadDropDowns(qry_corpcode, "0", "drp_corpcode");
    loadDropDowns(qry_fein, "0", "drp_fein");
    loadDropDowns(qry_legalentity, "0", "drp_legalentity");
    loadDropDowns(qry_state, "0", "drp_state");
    loadDropDowns(qry_jurisdiction, "0", "drp_jurisdiction");
    loadDropDowns(qry_account, "0", "drp_account");
    loadDropDowns(qry_frequency, "0", "drp_frequency");
    loadDropDowns(qry_responsable, "0", "drp_responsable");
    loadDropDowns(qry_type, "0", "drp_type");

}
//#endregion
//#endregion

//#region CHECKBOX SELECT ALL
$("#ckh_type_selectAll").click(function () {
    if ($("#ckh_type_selectAll").is(':checked')) {
        $("#drp_type > option").prop("selected", "selected");
        $("#drp_type").trigger("change");
    } else {
        $("#drp_type > option").removeAttr("selected");
        $("#drp_type").trigger("change");
    }
});

//#endregion

//#region CLEAN SEARCH BUTTON
$("#btnClearFields").click(function () {
    cleanSearch();
});

function cleanSearch() {
    loadDropDownLVIDs("");
    loadJurisdictionLocation();
    $("#jqxdropdownbutton_LOGID").val("");
    $("#jqxdropdownbutton_LVID").val("");
    $("#drp_corpcode").val("0");

    $("#drp_fein").val("0");
    $("#drp_legalentity").val("0");
    $("#drp_state").val("0");
    $("#drp_jurisdiction").val("0");
    $("#drp_account").val("0");
    $("#drp_frequency").val("0");
    $("#drp_responsable").val("0");

    $("#txt_dueDateFrom").val("");
    $("#txt_dueDateTo").val("");

    $("#drp_type > option").removeAttr("selected");
    $("#drp_type").trigger("change");

    $("#ckh_type_selectAll").prop("checked", false);
}


function chageStatus(filingId) {
    _callServer({
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT [filingStatus] FROM [tblFilings] WHERE idFiling = " + filingId + "") },
        type: "post",
        success: function (resultList) {
            var status = resultList[0].filingStatus;
            $("#txt_status_" + filingId + "").val(status);
        }
    });

}
//#endregion

//#region LOAD JURISDICTION SEARCH
//LOAD JURISDICTION TABLEs
function loadJurisdictionLocationResults() {
    $.jqxGridApi.create({
        showTo: "#tbl_JurisdictionLocationResults",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_JL_JURISDICTION_SEARCH]",
            Params: [
                { Name: "@logid", Value: validateEmpty($("#selectedLOGID").attr("idLOGID")) },
                { Name: "@lvid", Value: validateEmpty($("#selectedLVIDID").attr("idLVID")) },
                { Name: "@corpcodeid", Value: validateEmpty($("#drp_corpcode :selected").val()) },
                { Name: "@feinid", Value: validateEmpty($("#drp_fein :selected").val()) },
                { Name: "@legaentityid", Value: validateEmpty($("#drp_legalentity :selected").val()) },
                { Name: "@stateid", Value: validateEmpty($("#drp_state :selected").val()) },
                { Name: "@jurisdictionid", Value: validateEmpty($("#drp_jurisdiction :selected").val()) },
                { Name: "@accountid", Value: validateEmpty($("#drp_account :selected").val()) },
                { Name: "@frequencyid", Value: validateEmpty($("#drp_frequency :selected").val()) },
                { Name: "@responsablesoeid", Value: validateEmpty($("#drp_responsable :selected").val()) },
                { Name: "@datefrom", Value: validateEmpty($("#txt_dueDateFrom").val()) },
                { Name: "@dateto", Value: validateEmpty($("#txt_dueDateTo").val()) },
                { Name: "@typeid", Value: validateEmpty($("#drp_type").select2("val")) }
            ]
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'ckAch', type: 'string', text: "ACH", width: '5%' },
            { name: 'idFiling', type: 'string', hidden: true },
            { name: 'idJurisdictionLocation', text: "Log ID", type: 'int', width: '5%' },
            { name: 'oldLog', text: "Old Log", type: 'string', hidden:true },
            { name: 'type', type: 'string', text: "TYPE", width: '5%' },
            { name: 'frequency', type: 'string', text: "FREQUENCY", width: '10%' },
            { name: 'preparerSOEID', text: "RESP SOEID", type: 'string', width: '10%' },
            { name: 'legalEntityName', type: 'string', text: "LE", width: '10%' },
            { name: 'corpCode', type: 'string', text: "CORP CODE", width: '10%' },
            { name: 'fein', type: 'string', text: "FEIN", width: '10%' },
            { name: 'account', type: 'string', text: "ACCOUNT", width: '5%' },
            { name: 'stateDivSub', type: 'string', text: "STATE", width: '5%' },
            { name: 'jurisdiction', type: 'string', text: "JURISDICTION", width: '10%' },
            { name: 'dueDate', type: 'date', text: "DUE DATE", width: '10%', cellsformat: "M/d/yyyy" },
            { name: 'lvid', type: 'string', text: "LVID", width: '10%' },
            { name: 'commodityCode', type: 'string', text: "COMMODITY", width: '10%' },
            { name: 'totalAmountPaid', type: 'string', text: "Total", width: '10%' }

        ],

        ready: function () {
            //COUNT TOTAL IN TABLE
            var rowCount = $("#tbl_JurisdictionLocationResults").jqxGrid('getrows').length;
            $("#tableRowCount").text('Row count: ' + rowCount);

            $('#tbl_JurisdictionLocationResults').on('rowdoubleclick', function (event) {
                if (typeof event.args.group == "undefined") {
                    //_showLoadingFullPage();
                    var row = event.args.rowindex;
                    var datarow = $("#tbl_JurisdictionLocationResults").jqxGrid('getrowdata', row);
                    showModal(datarow);
                }
            });
        }
    });
}

//SEARCH BUTTON BY CRITERIA
$("#btnSearch").click(function () {
    loadJurisdictionLocationResults();
});

//VALIDATE EMPTY FIELDS 
function validateEmpty(value) {
    var val;
    if (value == null || value == "") {
        val = '0';
    } else {
        val = value.toString();
    }
    return val;
}



function downloadSearchToExcel() {

    var searchVariables = "";
    searchVariables =
        " '" + validateEmpty($("#selectedLOGID").attr("idLOGID")) + "', " +
        " '" + validateEmpty($("#selectedLVIDID").attr("idLVID")) + "', " +
        " '" + validateEmpty($("#drp_corpcode :selected").val()) + "', " +
        " '" + validateEmpty($("#drp_fein :selected").val()) + "', " +
        " '" + validateEmpty($("#drp_legalentity :selected").val()) + "', " +
        " '" + validateEmpty($("#drp_state :selected").val()) + "', " +
        " '" + validateEmpty($("#drp_jurisdiction :selected").val()) + "', " +
        " '" + validateEmpty($("#drp_account :selected").val()) + "', " +
        " '" + validateEmpty($("#drp_frequency :selected").val()) + "', " +
        " '" + validateEmpty($("#drp_responsable :selected").val()) + "', " +
        " '" + validateEmpty($("#txt_dueDateFrom").val()) + "', " +
        " '" + validateEmpty($("#txt_dueDateTo").val()) + "', " +
        " '" + validateEmpty($("#drp_type").select2("val")) + "' ";


    var storeProcedure = "NSP_LT_JL_JURISDICTION_SEARCH";

    var sql = "";
    sql = storeProcedure + " " + searchVariables;
    _downloadExcel({
        sql: sql,
        filename: "_Filings_" + _createCustomID() + ".xls",
        success: {
            msg: "Generating report... Please Wait, this operation may take some time to complete."
        }
    });
};


//#endregion

//#region SHOW DASHBOARD MODAL
function showModal(row) {
    _showModal({
        modalId: "ModalJurisdiction",
        title: "JURISDICTION - LogID:" + row.idJurisdictionLocation,
        width: "98%",
        contentAjaxUrl: "/SalesAndUse/ModalFiling",
        contentAjaxParams: {
            idJurisdictionLocation: row.idJurisdictionLocation,
            type: row.type,
            stateDivSub: row.stateDivSub,
            jurisdiction: row.jurisdiction,
            lvid: row.lvid,
            legalEntityName: row.legalEntityName,
            corpCode: row.corpCode,
            fein: row.fein,
            frequency: row.frequency,
            commodity: row.commodityCode,
            totalAmount: row.totalAmountPaid
        },
        onReady: function ($modal) {
            //GET LAST 12 FILINGS FOR THE JURISDICTION
            var filingQuery = "SELECT * FROM (SELECT TOP 12 f.[idFiling],f.[idJurisdictionLocation],f.[filingStatus],f.[filingType],CONVERT(CHAR(10),f.[dueDate], 101) as [dueDate],f.[preparerSOEID],f.[preparedDate],f.[reviewerSOEID],f.[reviewedDate],f.[mailerSOEID],f.[mailedDate] ,f.[paymentApproval],f.[checkReturn],CASE  WHEN f.mainConfirmationFile = 0 THEN 'NO' ELSE 'YES' END AS mainConfirmationFile,CONVERT(DECIMAL(12,2),f.[totalAmountPaid]) AS [totalAmountPaid], CONVERT(DECIMAL(12,2),f.[taxSavings]) AS [taxSavings],(CONVERT(DECIMAL(12,2),f.[totalAmountPaid])-CONVERT(DECIMAL(12,2),f.[taxSavings])) as TotalToPay ,f.[active],f.[dueMonth] ,f.[dueDay],f.[dueYear],CASE  WHEN f.reviewerSOEID IS NULL THEN 'No reviewer' ELSE reviewerSOEID END AS LastReviewedBy,CASE  WHEN f.[paymentCheck] = 0 THEN 'NO' ELSE 'YES' END AS paymentCheck,CASE  WHEN f.[exportFlatFile] = 0 THEN 'NO' ELSE 'YES' END AS exportFlatFile ,CASE  WHEN jl.[ckAch] = 0 THEN 'NO' ELSE 'YES' END AS ckAch FROM [SAU].[dbo].[tblFilings] f JOIN  [dbo].[tblJurisdictionLocation] jl ON f.[idJurisdictionLocation] = jl.[idJurisdictionLocation] WHERE f.idJurisdictionLocation = " + row.idJurisdictionLocation + " AND f.[dueYear] = '" + row.dueDate.getFullYear() + "' order by f.[dueDate] DESC ) as a"

            _callServer({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading Filings Details...",
                url: '/Ajax/ExecQuery',
                data: { 'pjsonSql': JSON.stringify(filingQuery) },
                type: "post",
                success: function (resultList) {
                    //FOR LOAD ROWS
                    $("#filingsDetails").empty();

                    for (var i = 0; i < resultList.length; i++) {
                        loadFilingRows(resultList, i, row);
                    }
                    //EDIT ON CLICK
                    $modal.find('.edi').click(function (e) { clickEdit(e); });
                    //$modal.find('.edi').on('click', e => clickEdit(e));
                    //CLOSE

                    $('#close').on('click', function () { $('#editpopup').fadeOut(); });

                    //PAYMENT ON CLICK
                    //$('.payment').on('click', e => clickPayment(e));
                    $modal.find('.payment').click(function (e) { clickPayment(e); });
                    //CLOSE
                    $('#closePayment').on('click', function () { $('#paymentpopup').fadeOut(); });

                    //PAYMENT ON CLICK
                    //$('.review').on('click', e => clickReview(e));
                    $modal.find('.review').click(function (e) { clickReview(e); });
                    //CLOSE
                    $('#closeReview').on('click', function () { $('#reviewpopup').fadeOut(); });

                    //CHEKLIST ON CLICK
                    //$('.checklist').on('click', e => clickChecklist(e));
                    $modal.find('.checklist').click(function (e) { clickChecklist(e); });
                    //CLOSE
                    $('#closeChecklist').on('click', function () { $('#checklistpopup').fadeOut(); });

                    //MAIL ON CLICK
                    //$('.mail').on('click', e => clickMail(e));
                    $modal.find('.mail').click(function (e) { clickMail(e); });
                    //CLOSE
                    $('#closeMail').on('click', function () { $('#mailpopup').fadeOut(); });

                    //COMMENTS ON CLICK
                    $modal.find('.comment').click(function (e) { clickComment(e); });
                    //CLOSE
                    $('#closeComment').on('click', function () { $('#commentspopup').fadeOut(); });

                    //COMMENTS ON CLICK
                    $modal.find('.coversheet').click(function (e) { clickCoverSheet(e); });
                    //CLOSE
                    $('#closeCoversheet').on('click', function () { $('#coversheetpopup').fadeOut(); });

                }
            });
        },
        class: " compactModal"
    });
}


function loadFilingRows(resultList, i, row) {
    //CREATE VARIABLE WITH LISTED DIVS
    //#region Variable

    var singlerow = '        \
                                <div> \
                                    <div class="row" style=\"margin-top:0px; height: 33px; margin-top: 4px;\" id="row_@filingUID"> \
                                        <div class=\"col-md-2 nopaddingCols compactInput\"> \
                                            <div class=\"form-group \"> \
                                                <div class=\"col-md-3 nopaddingCols\"> \
                                                    <div style="margin-top:1px;"> \
                                                        <span class="fa fa-angle-double-right" id= "spanArrow__@filingUID" style="color: #4CAF50;font-size: 18px;font-weight: bold;"></span> \
                                                        <button id=\"btn_edit_@filingUID\" filingid="@filingUID" type=\"button\" class=\"btn btn-xs tn-default edi\" data-target=\"#idFilingEdit_\"> \<span class=\"fa fa-cog compactInput\" style=\"margin-top:5px;\"> \</span> \</button> \
                                                    </div> \
                                                </div> \
                                                <div class=\"col-md-9 nopaddingCols\"> \
                                                    <div> \
                                                        <input id=\"txt_status_@filingUID\" disabled type=\"text\" class=\"compactInput form-control\" placeholder=\"Status\" style="height: 27px !important;"> \
                                                    </div> \
                                                </div> \
                                            </div> \
                                        </div> \
                                        <div class=\"col-md-1 nopaddingCols compactInput\"> \
                                            <div class=\"form-group \"> \
                                                <div> \
                                                    <input type=\"text\" id=\"txt_duedate_@filingUID\" disabled class=\"form-control compactInput\" placeholder=\"Due Date\" style="height: 27px !important;"> \
                                                </div> \
                                            </div> \
                                        </div> \
                                        <div class=\"col-md-1 nopaddingCols compactInput\"> \
                                            <div class=\"form-group \"> \
                                                <div> \
                                                    <input type=\"text\" id=\"txt_total_@filingUID\" disabled class=\"form-control compactInput\" placeholder=\"Total\" style="height: 27px !important;"> \
                                                </div> \
                                            </div> \
                                        </div> \
                                        <div class=\"col-md-1 nopaddingCols compactInput\"> \
                                            <div class=\"form-group \"> \
                                                <div> \
                                                    <input type=\"text\" id=\"txt_preparer_@filingUID\" disabled class=\"form-control compactInput\" placeholder=\"Prepared By\" style="height: 27px !important;"> \
                                                </div> \
                                            </div> \
                                        </div> \
                                        <div class=\"col-md-1 nopaddingCols compactInput\"> \
                                            <div class=\"form-group \"> \
                                                <div class=\"col-md-6 nopaddingCols\"> \
                                                    <div> \
                                                        <button id=\"btn_payment_@filingUID\" filingid="@filingUID" type=\"button\" class=\"btn btn-xs btn-success center-block payment\"> \<span class=\"fa fa-dollar\" style=\"margin-top:5px;\" title=\"Payment\"> \</span> \</button> \
                                                    </div> \
                                                </div> \
                                                <div class=\"col-md-6 nopaddingCols\"> \
                                                    <div> \
                                                        <input type=\"text\" id=\"txt_available_@filingUID\" disabled class=\"form-control compactInput\" placeholder=\"\" style="height: 27px !important;"> \
                                                    </div> \
                                                </div> \
                                            </div> \
                                        </div> \
                                        <div class=\"col-md-1 nopaddingCols compactInput\"> \
                                            <div class=\"form-group \"> \
                                                <div class=\"col-md-4 nopaddingCols\"> \
                                                    <div> \
                                                        <button id=\"btn_review_@filingUID\" filingid="@filingUID" type=\"button\" class=\"btn btn-xs btn-primary review\"> \<span class=\"fa fa-thumbs-o-up\" style=\"margin-top:5px;\" title=\"Review\"> \</span> \</button> \
                                                    </div> \
                                                </div> \
                                                <div class=\"col-md-8 nopaddingCols\"> \
                                                    <div> \
                                                        <input type=\"text\" id=\"txt_p2p_@filingUID\" disabled class=\"form-control compactInput\" placeholder=\"P2P\" style="height: 27px !important;"> \
                                                    </div> \
                                                </div> \
                                            </div> \
                                        </div> \
                                        <div class=\"col-md-1 nopaddingCols compactInput\"> \
                                            <div class=\"form-group \"> \
                                                <div> \
                                                    <input type=\"text\" id=\"txt_lastreviewdby_@filingUID\" disabled class=\"form-control compactInput\" placeholder=\"Last Reviewed By\" style="height: 27px !important;"> \
                                                </div> \
                                            </div> \
                                        </div> \
                                        <div class=\"col-md-1 nopaddingCols compactInput\"> \
                                            <div class=\"form-group \"> \
                                                <div class=\"col-md-4 nopaddingCols\"> \
                                                    <div> \
                                                        <button id=\"btn_mail_@filingUID\" filingid="@filingUID" type=\"button\" class=\"btn btn-xs btn-warning mail\"> \<span class=\"fa fa-envelope-o\" style=\"margin-top:5px;\" title=\"Mail\"> \</span> \</button> \
                                                    </div> \
                                                </div> \
                                                <div class=\"col-md-8 nopaddingCols\"> \
                                                    <div> \
                                                        <input type=\"text\" id=\"txt_mailed_@filingUID\" disabled class=\"form-control compactInput\" placeholder=\"Mailed\" style="height: 27px !important;"> \
                                                    </div> \
                                                </div> \
                                            </div> \
                                        </div> \
                                        <div class=\"col-md-1 nopaddingCols compactInput\"> \
                                            <div class=\"form-group \"> \
                                                <div> \
                                                    <input type=\"text\" id=\"txt_ach_@filingUID\" disabled class=\"form-control compactInput\" placeholder=\"ACH\" style="height: 27px !important;"> \
                                                </div> \
                                            </div> \
                                        </div> \
                                        <div class=\"col-md-2 nopaddingCols compactInput inlineButtons\"> \
                                            <div class=\"form-group \"> \
                                                <div class=\"col-md-3 nopadding inlineButtons\"> \
                                                    <div> \
                                                        <button id=\"btn_comment_@filingUID\"  filingid="@filingUID" type=\"button\" class=\"btn btn-xs btn-info comment\"> \<span class=\"fa fa-comment\" style=\"margin-top:5px;\" title=\"Comment\"> \</span> \</button> \
                                                    </div> \
                                                </div> \
                                                <div class=\"col-md-3 nopadding inlineButtons\"> \
                                                    <div> \
                                                        <button id=\"btn_checklist_@filingUID\" filingid="@filingUID" type=\"button\" class=\"btn btn-xs btn-success checklist\"> \<span class=\"fa fa-list-ol\" style=\"margin-top:5px;\" title=\"Checklist\"> \</span> \</button> \
                                                    </div> \
                                                </div> \
                                                <div class=\"col-md-3 nopadding inlineButtons\"> \
                                                    <div> \
                                                        <button id=\"btn_prntcoversheet_@filingUID\" filingid="@filingUID" type=\"button\" class=\"btn btn-xs btn-primary coversheet\"> \<span class=\"fa fa-print\" style=\"margin-top:5px;\" title=\"Print Coversheet\"> \</span> \</button> \
                                                    </div> \
                                                </div> \
                                            </div> \
                                        </div> \
                                    </div> \
                                </div> ';
    //#endregion

    var objFunction = resultList[i];
    singlerow = singlerow.replace(new RegExp('@filingUID', 'g'), objFunction.idFiling);
    $("#filingsDetails").append(singlerow);

    $("#txt_status_" + objFunction.idFiling + "").val(objFunction.filingStatus);
    $("#txt_duedate_" + objFunction.idFiling + "").val(objFunction.dueDate);

    //$("#txt_total_" + objFunction.idFiling + "").val(objFunction.totalAmountPaid);

    var total = parseFloat(objFunction.totalAmountPaid) - parseFloat(objFunction.taxSavings);

    var totalDashboarInit = new Cleave("#txt_total_" + objFunction.idFiling + "", {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });


    totalDashboarInit.setRawValue(objFunction.TotalToPay);

    $("#txt_preparer_" + objFunction.idFiling + "").val(objFunction.preparerSOEID);
    $("#txt_available_" + objFunction.idFiling + "").val(objFunction.paymentCheck);
    $("#txt_p2p_" + objFunction.idFiling + "").val(objFunction.exportFlatFile);
    $("#txt_lastreviewdby_" + objFunction.idFiling + "").val(objFunction.LastReviewedBy);
    $("#txt_mailed_" + objFunction.idFiling + "").val(objFunction.mainConfirmationFile);
    $("#txt_ach_" + objFunction.idFiling + "").val(objFunction.ckAch);
    
    
    //class="fa fa-angle-double-right"
    //style="color: #4CAF50;font-size: 18px;font-weight: bold;"

    if (
        (((new Date(objFunction.dueDate)).getMonth()) == (new Date()).getMonth())
        &&
        (((new Date(objFunction.dueDate)).getYear()) == (new Date()).getYear())

        ) {
        $("#spanArrow__" + objFunction.idFiling + "").show();
        $("#row_" + objFunction.idFiling + "").css("border-style","double");
        

    } else {
        $("#spanArrow__" + objFunction.idFiling + "").hide();
        $("#row_" + objFunction.idFiling + "").css("border-style", "hidden");
    }



}
//#endregion

//#region EDIT MAIN BUTTON
function clickEdit(event) {
    var totalDue = new Cleave('#txt_totaldue', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });

    var totalSavings = new Cleave('#txt_taxsavings', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });



    $("#editpopup").fadeIn(1000, function () {

    });

    var filingid = $("#" + event.currentTarget.id).attr("filingid");
    $("#txt_duedate2").datepicker();
    //clean popup
    $("#drp_filing_type").val("");
    $("#txt_filingId").val("");
    $("#txt_duedate2").val("");
    $("#txt_totaldue").val("");
    $("#txt_taxsavings").val("");
    $("#txt_preparedBy").val("");
    $("#txt_status").val("");
    $("#txt_comments_edit").val("");
    $("#txt_newComment").val("");
    $("#lblMessageEdit").text("");

    //load filing data
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Filing Details...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT [idFiling],[idJurisdictionLocation],[filingStatus],[filingType],[dueDate],[preparerSOEID],[preparedDate],[reviewerSOEID],[reviewedDate],[mailerSOEID],[mailedDate],[paymentApproval],[checkReturn] ,[mainConfirmationFile],[totalAmountPaid],[taxSavings] ,[active],[dueMonth] ,[dueDay],[dueYear] ,[paymentCheck] ,[exportFlatFile] ,[paymentMethod] FROM [SAU].[dbo].[tblFilings] WHERE [idFiling] = " + filingid + "") },
        type: "post",
        success: function (resultList) {
            var objFunction = resultList[0];
            if (objFunction.filingType == "") {
                $("#drp_filing_type").val("0");
            } else {
                $("#drp_filing_type").val(objFunction.filingType);
            }

            $("#txt_filingId").val(objFunction.idFiling);
            $("#txt_duedate2").val(objFunction.dueDate.split(' ')[0]);
            //$("#txt_totaldue").val(objFunction.totalAmountPaid);
            //$("#txt_taxsavings").val(objFunction.taxSavings);
            $("#txt_preparedBy").val(objFunction.preparerSOEID);
            $("#txt_status").val(objFunction.filingStatus);



            totalDue.setRawValue(objFunction.totalAmountPaid);
            totalSavings.setRawValue(objFunction.taxSavings);


            if (objFunction.filingStatus != "PendingToPrepare" &&
                objFunction.filingStatus != "Rejected" &&
                objFunction.filingStatus != "ChecklistReady" &&
                objFunction.filingStatus != "FilingEdited" &&
                objFunction.filingStatus != "PaymentCompleted") {
                
                $("#btn_edit_save").prop("disabled", true);
            } else {
                $("#btn_edit_save").prop("disabled", false);
            }

        }
    });

    //load comments
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Comments...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT [idComment],[id],[stage],[comment],[createdBy],[createdDate] FROM [tblComments] WHERE id = " + filingid + " AND stage = 'edit' order by createdDate DESC;") },
        type: "post",
        success: function (resultList) {
            var comments = "";
            for (var i = 0; i < resultList.length; i++) {
                var objFunction1 = resultList[i];
                comments += "Comment by: " + objFunction1.createdBy + " on " + objFunction1.createdDate.split(' ')[0] + " - " + objFunction1.comment + "\n";

            }
            $("#txt_comments_edit").val(comments);
        }
    });




}

function filingEditSave() {
    // SET VARIABLES 
    if (_getUserInfo().Roles.includes("PREPARER") || _getUserInfo().Roles.includes("ADMIN")) {
        var myObject = new Object();
        myObject.filingid = $('#txt_filingId').val();
        myObject.duedate = $('#txt_duedate2').val();
        myObject.filingType = $("#drp_filing_type option:selected").val();
        myObject.totaldue = $('#txt_totaldue').val().replace(/,/g, "");
        myObject.taxsavings = $('#txt_taxsavings').val().replace(/,/g, "");
        myObject.newcomment = $('#txt_newComment').val();
        myObject.status = $('#txt_status').val();


        //--validate complete fields
        var validations = true;
        if (myObject.duedate == "" || myObject.filingType == "0" || myObject.totaldue == "" || myObject.taxsavings == "") {
            validations = false;
        }

        if (validations) {

            if (parseFloat(myObject.taxsavings) >= 0) {
                var filingMainValues = JSON.stringify(myObject);

                _callServer({
                    url: '/SalesAndUse/FilingEditSave',
                    closeModalOnClick: false,
                    data: { 'pjson': filingMainValues },
                    type: "post",
                    success: function (savingStatus) {
                        $('#editpopup').fadeOut();
                        chageStatus(myObject.filingid);

                        var total = parseFloat(myObject.totaldue) - parseFloat(myObject.taxsavings);

                        //$("#txt_total_" + myObject.filingid).val(total);

                        var totalDashboar = new Cleave("#txt_total_" + myObject.filingid, {
                            numeral: true,
                            numeralThousandsGroupStyle: 'thousand'
                        });

                        totalDashboar.setRawValue(total);


                        _showNotification("success", "Filing updated successfully.");
                        $("#lblMessageEdit").text("**Filing updated successfully.**");
                        $("#lblMessageEdit").css("color", "#2E7D32");
                    }
                });
            }
            else {
                _showNotification("error", "The tax savings amount most be positive");
                $("#lblMessageEdit").text("**The tax savings amount most be positive.**");
                $("#lblMessageEdit").css('color', 'red');
            }

            
        } else {
            _showNotification("error", "Please complete all the required fields.");
            $("#lblMessageEdit").text("**Please complete all the required fields.**");
            $("#lblMessageEdit").css('color', 'red');

        }
    } else {
        $("#lblMessageEdit").text("**Only preparers can edit filings.**");
        $("#lblMessageEdit").css('color', 'red');
    }


}

//#endregion

//#region PAYMENT MAIN BUTTON
var idP2P, idVendor;

function clickPayment(event) {
    $("#paymentpopup").fadeIn(1000, function () {
    });

    //#region clean payment fields
    $("#payment_duedate").text("");
    $("#payment_totaldue").text("");
    $("#payment_taxsavings").text("");
    $("#payment_totalremain").text("");
    $("#payment_totalpayments").text("");


    $("#drp_paymentCategory").val("BAU");
    $("#drp_paymentMethod").val("0");
    $("#txt_paymentAmount").val("");

    $("#jqxdropdownbutton_payment_p2plvid").val("");
    $("#jqxdropdownbutton_payment_vendorsite").val("");
    $("#txt_paymentp2plvid2").val("");
    $("#txt_paymentGOC").val("");
    $("#txt_paymentAccount").val("");
    $("#txt_payment_comments").val("");

    $('#btn_payment_add_main').prop('disabled', false);
    $('#btn_payment_edit_main').prop('disabled', true);
    $('#btn_payment_submitPayment').prop('disabled', true);


    $("#txt_newPaymentComment").val("");
    $("#payment_filingid").val("");
    $("#lblPaymentEdit").text("");
    //#endregion

    var paymentAmount = new Cleave("#txt_paymentAmount", {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });

    var filingid = $("#" + event.currentTarget.id).attr("filingid");

    loadMainPaymentInfo2(filingid);

    $("#payment_filingid").val(filingid);
    $("#payment_filingid").text(filingid);

    //load drop tables
    $("#jqxdropdownbutton_payment_p2plvid").jqxDropDownButton({ template: "summer", width: "100%", height: 30 });
    $("#jqxdropdownbutton_payment_vendorsite").jqxDropDownButton({ template: "summer", width: "100%", height: 30 });

    loadDropDownPaymentP2PLvid($("#lvidtext").text().slice($("#lvidtext").text().indexOf(':') + 3));
    console.log($("#lvidtext").text().slice($("#lvidtext").text().indexOf(':')+3));
    loadDropDownPaymentVendorSite();

    loadMainPaymentInfo(filingid);


    $("#jqxdropdownbutton_payment_vendorsite").jqxDropDownButton('open');
    $("#jqxdropdownbutton_payment_p2plvid").jqxDropDownButton('open');


}

function loadMainPaymentInfo(filingid) {
    //load filing data
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Filing Details...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT [idFiling],[dueDate],[totalAmountPaid],[taxSavings],[filingStatus] FROM [SAU].[dbo].[tblFilings] WHERE [idFiling] = " + filingid + "") },
        type: "post",
        success: function (resultList) {
            var objFunction = resultList[0];
            $("#payment_duedate").text(objFunction.dueDate.split(' ')[0]);
            $("#payment_totaldue").text(objFunction.totalAmountPaid);
            $("#payment_taxsavings").text(objFunction.taxSavings);

            total = parseFloat(objFunction.totalAmountPaid) - parseFloat(objFunction.taxSavings);
            //calculate TotalRemain

            if (objFunction.filingStatus != "PendingToPrepare" &&
                objFunction.filingStatus != "Rejected" &&
                objFunction.filingStatus != "ChecklistReady" &&
                objFunction.filingStatus != "FilingEdited" &&
                objFunction.filingStatus != "PaymentCompleted") {
                $("#btn_payment_add_main").prop("disabled", true);
            } else {
                $("#btn_payment_add_main").prop("disabled", false);
                $('#btn_payment_submitPayment').prop('disabled', false);

            }



            loadPaymentLines(filingid, total, objFunction.filingStatus);


        }
    });
}

function loadMainPaymentInfo2(filingid) {
    //load filing data
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Filing Details...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT [idP2PGocsLvid],[idP2PVendorSite], CASE ckACH WHEN 0 THEN 'NO' WHEN 1 THEN 'YES' END as  ckACH, jl.frequency FROM [SAU].[dbo].[tblFilings] f JOIN [dbo].[tblJurisdictionLocation] jl ON jl.[idJurisdictionLocation] = f.[idJurisdictionLocation] WHERE f.[idFiling] =  " + filingid + "") },
        type: "post",
        success: function (resultList) {
            var objFunction = resultList[0];

            idP2P = objFunction.idP2PGocsLvid;
            console.log(idP2P + 'Second')
            idVendor = objFunction.idP2PVendorSite;

            $("#payment_achnot").text(objFunction.ckACH);

            var qry_paymentmethod = "SELECT id,text FROM (SELECT '0' AS [id], '-- Select --' AS [text], 0 as [order] UNION SELECT [idPaymentMethod]AS [id],[paymentMethod] AS [text], [order]  FROM [SAU].[dbo].[tblAdmin_PaymentMethod] where (ach = 0 AND active = 1) OR [idPaymentMethod] = 1) as A order by [order]"
            if (objFunction.ckACH == "YES") {
                qry_paymentmethod = "SELECT id,text FROM (SELECT '0' AS [id], '-- Select --' AS [text], 0 as [order] UNION SELECT [idPaymentMethod]AS [id],[paymentMethod] AS [text], [order]  FROM [SAU].[dbo].[tblAdmin_PaymentMethod] where (ach = 1 AND active = 1)) as A order by [order]";
            }
            loadDropDowns(qry_paymentmethod, "0", "drp_paymentMethod");




            if (objFunction.frequency == "Annually" || objFunction.frequency == "Semi") {
                $("#paymentpopup").removeClass("modalAutoCenter");
                $("#paymentpopup").addClass("modalAutoCenterAnnual");
            } else {
                $("#paymentpopup").removeClass("modalAutoCenterAnnual");
                $("#paymentpopup").addClass("modalAutoCenter");
            }
        }
    });
}

function loadPaymentLines(filingid, total, status) {
    //load payment lines
    var paymenttotal = 0;

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Payment Line Details...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify("SELECT idPayment,idFiling,[ExportedToFlatFile],l.lvid,p.idP2PGocLvid,p2.goc ,a.account,p.idVendorSite,v.vendorSite,v.description,paymentCategory,pm.paymentMethod,CONVERT(DECIMAL(10,2),[paymentAmount]) AS paymentAmount,paymentStatus FROM tblPayments p LEFT JOIN tblAdmin_P2PLvidGoc p2 ON p.idP2PGocLvid = p2.idP2PGocsLvid LEFT JOIN tblAdmin_P2PVendorSite v ON v.idVendorSite = p.idVendorSite LEFT JOIN tblAdmin_PaymentMethod pm ON pm.idPaymentMethod = p.idPaymentMethod LEFT JOIN [tblAdmin_P2PAccount] a ON p2.idAccount = a.[idP2PAccount] LEFT JOIN tblAdmin_LVID l ON p2.idLvid = l.idLVID WHERE p.idFiling = " + filingid + ""
                )
        },
        type: "post",
        success: function (resultList) {

            $("#divPaymentList").empty();

            for (var i = 0; i < resultList.length; i++) {
                loadPaymentRows(resultList, i);
                paymenttotal = parseFloat(paymenttotal) +  parseFloat(resultList[i].paymentAmount);
            }
            //$('.paymentedit').on('click', e => clickPaymentEdit(e));

            $('.paymentedit').click(function (e) {
                clickPaymentEdit(e);
            });

            $('.paymentdelete').click(function (e) {
                clickPaymentDelete(e);
            });

            $('.paymentdelete').each(function () {
                var $btn = $(this);
                var exportTedToFlatFile = $btn.attr("exportedToFlatFile");

                if (status != "PendingToPrepare" &&
                    status != "Rejected" &&
                    status != "ChecklistReady" &&
                    status != "FilingEdited" &&
                    status != "PaymentCompleted") {

                    $btn.attr("disabled", true);
                } else {
                    $btn.attr("disabled", false);
                }

                if (exportTedToFlatFile == "True") {
                    $btn.attr("disabled", true);
                }
            });


            




            //set total - payment total - remaining


            $("#payment_totalpayments").text(parseFloat(parseFloat(paymenttotal)).toFixed(2));
            $("#payment_totalremain").text(parseFloat(parseFloat(total) - parseFloat(paymenttotal)).toFixed(2));
        }
    });

}

function loadDropDownPaymentP2PLvid(lv) {
    $.jqxGridApi.create({
        showTo: "#tblPaymentP2PLvid",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_AC_FILING_P2PLVID]",
            Params: [
                { Name: "@p2pGocLid", Value: lv }
            ]
            //Name: "[dbo].[NSP_LT_ADMIN_P2PLVID]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'idP2PGocsLvid', type: 'string', hidden: true },
            { name: 'lvid', type: 'string', width: '33%' },
            { name: 'goc', type: 'string', width: '33%' },
            { name: 'account', type: 'string', width: '33%' }
        ],

        ready: function () {
            _hideLoadingFullPage();

            $('#tblPaymentP2PLvid').on('rowselect', function (event) {
                // event.args.rowindex is a bound index.
                if (typeof event.args.group == "undefined") {
                    var args = event.args;
                    var row = $("#tblPaymentP2PLvid").jqxGrid('getrowdata', args.rowindex);
                    var dropDownContent = '<div id="selectedPaymentP2PLVID" style="position: relative; width:100%; margin-left: 3px; margin-top: 5px;" idP2PLVID="' + row.idP2PGocsLvid + '" >' + row['lvid'] + ' - ' + row['goc'] + ' - ' + row['account'] + '</div>';
                    $("#jqxdropdownbutton_payment_p2plvid").jqxDropDownButton('setContent', dropDownContent);
                    $('#txt_paymentp2plvid2').val(row['lvid']);
                    $('#txt_paymentGOC').val(row['goc']);
                    $('#txt_paymentAccount').val(row['account']);


                }
            });


            //LOOP !! 


            var rows1 = $('#tblPaymentP2PLvid').jqxGrid('getrows');
            for (var y = 0; y < rows1.length; y++) {
                if (idP2P == rows1[y].idP2PGocsLvid) {
                    $("#tblPaymentP2PLvid").jqxGrid('selectrow', rows1[y].uid);

                }
            }


            setTimeout(function () {
                $("#jqxdropdownbutton_payment_p2plvid").jqxDropDownButton('close');
            }, 10);

        }
    });
}

function loadDropDownPaymentVendorSite() {
    $.jqxGridApi.create({
        showTo: "#tblPaymentVendorSite",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_ADMIN_VENDOR_SITE]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'idVendorSite', type: 'string', width: '25%' },
            { name: 'vendorSite', type: 'string', width: '25%' },
            { name: 'description', type: 'string', width: '75%' }
        ],

        ready: function () {
            _hideLoadingFullPage();
            $('#tblPaymentVendorSite').on('rowselect', function (event) {
                // event.args.rowindex is a bound index.
                if (typeof event.args.group == "undefined") {
                    var args = event.args;
                    var row = $("#tblPaymentVendorSite").jqxGrid('getrowdata', args.rowindex);
                    var dropDownContent = '<div id="selectedPaymentVendorSite" style="position: relative; width:100%; margin-left: 3px; margin-top: 5px;" paymentVendorSite="' + row.idVendorSite + '" >' + row['vendorSite'] + ' - ' + row['description'] + '</div>';
                    $("#jqxdropdownbutton_payment_vendorsite").jqxDropDownButton('setContent', dropDownContent);


                }
            });


            var rows1 = $('#tblPaymentVendorSite').jqxGrid('getrows');
            for (var y = 0; y < rows1.length; y++) {
                if (idVendor == rows1[y].idVendorSite) {
                    $("#tblPaymentVendorSite").jqxGrid('selectrow', rows1[y].uid);

                }
            }

            setTimeout(function () {
                $("#jqxdropdownbutton_payment_vendorsite").jqxDropDownButton('close');
            }, 10);
        }
    });
}

function loadPaymentRows(resultList, i) {
    //#region DIV
    var singlerow = '<div class=\"row\" style=\"margin-top:10px; text-align:center;\"> \
                            <div class=\"col-md-1 nopaddingCols compactInputPayment\"> \
                                <div class=\"form-group \"> \
                                    <button id=\"btn_payment_@paymentid\" paymentid1=\"@paymentid\" type=\"button\" class=\"btn btn-xs btn-default paymentedit\"> <span class=\"fa fa-cog\" style=\"margin-top:5px;\"> </span> </button> \
                                </div> \
                            </div> \
                            <div class=\"col-md-2 nopaddingCols compactInputPayment\"> \
                                <div class=\"form-group \"> \
                                      <input type=\"text\" id=\"txt_payment_category_@paymentid\" disabled class=\"form-control compactInputPayment\" placeholder=\"Category\">  \
                                </div> \
                            </div>  \
                            <div class=\"col-md-2 nopaddingCols compactInputPayment\"> \
                                <div class=\"form-group \"> \
                                       <input type=\"text\" id=\"txt_payment_method_@paymentid\" disabled class=\"form-control compactInputPayment\" placeholder=\"Payment Method\">  \
                                </div> \
                            </div> \
                            <div class=\"col-md-2 nopaddingCols compactInputPayment\"> \
                                <div class=\"form-group \"> \
                                    <div class="input-group"> \
                                        <span class="input-group-addon">$</span> \
                                        <input type=\"text\" id=\"txt_payment_amount_@paymentid\" disabled class=\"form-control compactInputPayment\" placeholder=\"Amount\"> \
                                    </div> \
                                </div> \
                            </div> \
                            <div class=\"col-md-1 nopaddingCols compactInputPayment\"> \
                                <div class=\"form-group \"> \
                                      <input type=\"text\" id=\"txt_payment_P2PLvid_@paymentid\" disabled class=\"form-control compactInputPayment\" placeholder=\"P2P LVID\">  \
                                </div> \
                            </div> \
                            <div class=\"col-md-1 nopaddingCols compactInputPayment\"> \
                                <div class=\"form-group \"> \
                                       <input type=\"text\" id=\"txt_payment_P2PAccount_@paymentid\" disabled class=\"form-control compactInputPayment\" placeholder=\"P2P Account\">  \
                                </div> \
                            </div> \
                            <div class=\"col-md-1 nopaddingCols compactInputPayment\"> \
                                <div class=\"form-group \"> \
                                       <input type=\"text\" id=\"txt_payment_P2PGoc_@paymentid\" disabled class=\"form-control compactInputPayment\" placeholder=\"P2P GOC\">  \
                                </div> \
                            </div> \
                            <div class=\"col-md-1 nopaddingCols compactInputPayment\"> \
                                <div class=\"form-group \"> \
                                         <input type=\"text\" id=\"txt_payment_Vendor_@paymentid\" disabled class=\"form-control compactInputPayment\" placeholder=\"Vendor\">  \
                                </div> \
                            </div> \
                            <div class=\"col-md-1 nopaddingCols compactInputPayment\"> \
                            <div class=\"col-md-7 nopaddingCols compactInputPayment\"> \
                                <div class=\"form-group \"> \
                                     <input type=\"text\" id=\"txt_payment_sitecode_@paymentid\" disabled class=\"form-control compactInputPayment\" placeholder=\"Site Code\">  \
                                </div> \
                            </div> \
                            <div class=\"col-md-5 nopaddingCols compactInputPayment\"> \
                                <div class=\"form-group \"> '
    var objFunction = resultList[i];

    singlerow += '<button id=\"btn_payment_delete_@paymentid\"  exportedToFlatFile=\"' + objFunction.ExportedToFlatFile + '\" disabled=\"disabled\" paymentid2=\"@paymentid\" idFiling2 = \"@filingid2\" type=\"button\" class=\"btn btn-xs btn-danger paymentdelete\"> <span class=\"fa fa-remove\" style=\"margin-top:5px;\"> </span> </button> \
                            </div> \
                            </div> \
                        </div> \
                    </div>';

    //#endregion

    
    singlerow = singlerow.replace(new RegExp('@paymentid', 'g'), objFunction.idPayment);
    singlerow = singlerow.replace(new RegExp('@filingid2', 'g'), objFunction.idFiling);

    $("#divPaymentList").append(singlerow);

    $("#txt_payment_category_" + objFunction.idPayment + "").val(objFunction.paymentCategory);
    $("#txt_payment_method_" + objFunction.idPayment + "").val(objFunction.paymentMethod);

    //$("#txt_payment_amount_" + objFunction.idPayment + "").val(objFunction.paymentAmount);

    var cl_paymentAmount = new Cleave("#txt_payment_amount_" + objFunction.idPayment + "", {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });

    cl_paymentAmount.setRawValue(objFunction.paymentAmount);

    //$("#txt_payment_sentP2P_" + objFunction.idPayment + "").val(objFunction.preparerSOEID);
    $("#txt_payment_P2PLvid_" + objFunction.idPayment + "").val(objFunction.lvid);
    $("#txt_payment_P2PGoc_" + objFunction.idPayment + "").val(objFunction.goc);
    $("#txt_payment_P2PAccount_" + objFunction.idPayment + "").val(objFunction.account);
    $("#txt_payment_Vendor_" + objFunction.idPayment + "").val(objFunction.description);
    $("#txt_payment_sitecode_" + objFunction.idPayment + "").val(objFunction.vendorSite);



}

function clickPaymentEdit(event) {
    var paymentid = $("#" + event.currentTarget.id).attr("paymentid1");
    $('#btn_payment_add_main').prop('disabled', true);

    //load filing data
    _callServer({
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT [idPayment],p.[idFiling],f.[filingStatus],[idP2PGocLvid],[idVendorSite],[paymentCategory],[idPaymentMethod],[paymentAmount],[paymentStatus],[exportedToFlatFile] ,[exportedToFlatFileDate],[createdBy] ,[createdDate],[paymentReadyToExport] FROM [SAU].[dbo].[tblPayments] p JOIN [dbo].[tblFilings] f ON f.[idFiling] = p.[idFiling] WHERE idPayment =  " + paymentid + "") },
        type: "post",
        success: function (resultList) {
            var objFunction = resultList[0];
            $("#txt_paymentId").val(objFunction.idPayment);
            $("#drp_paymentCategory").val(objFunction.paymentCategory);

            $("#drp_paymentMethod").val(objFunction.idPaymentMethod);


            var cl_paymentAmount1 = new Cleave("#txt_paymentAmount", {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand'
            });

            cl_paymentAmount1.setRawValue(objFunction.paymentAmount);


            //select P2P LVID
            var rowslvid = $('#tblPaymentP2PLvid').jqxGrid('getrows');
            for (var y = 0; y < rowslvid.length; y++) {
                if (objFunction.idP2PGocLvid == rowslvid[y].idP2PGocsLvid) {
                    $("#tblPaymentP2PLvid").jqxGrid('selectrow', y);
                }
            }

            //select vendor
            var rowsvendor = $('#tblPaymentVendorSite').jqxGrid('getrows');
            for (var y = 0; y < rowsvendor.length; y++) {
                if (objFunction.idVendorSite == rowsvendor[y].idVendorSite) {
                    $("#tblPaymentVendorSite").jqxGrid('selectrow', y);
                }
            }

            //validations
            if (objFunction.filingStatus != "ChecklistReady" && objFunction.filingStatus != "Rejected") {
                $('#btn_payment_edit_main').prop('disabled', true);
            } else {
                $('#btn_payment_edit_main').prop('disabled', false);
            }


            //load payment comments
            _callServer({
                url: '/Ajax/ExecQuery',
                data: { 'pjsonSql': JSON.stringify("SELECT [idComment],[id],[stage],[comment],[createdBy],[createdDate] FROM [tblComments] WHERE id = " + paymentid + " AND stage = 'payment' order by createdDate DESC;") },
                type: "post",
                success: function (resultList) {
                    var comments = "";
                    for (var i = 0; i < resultList.length; i++) {
                        var objFunction1 = resultList[i];
                        comments += "Comment by: " + objFunction1.createdBy + " on " + objFunction1.createdDate.split(' ')[0] + " - " + objFunction1.comment + "\n";

                    }
                    $("#txt_payment_comments").val(comments);
                }
            });
        }
    });
}

function clickPaymentDelete(event) {
    var paymentid = $("#" + event.currentTarget.id).attr("paymentid2");
    var filingid = $("#" + event.currentTarget.id).attr("idFiling2");

    if (_getUserInfo().Roles.includes("PREPARER") || _getUserInfo().Roles.includes("ADMIN")) {
        var myObject = new Object();
        myObject.paymentId = paymentid;

        var filingMainValues = JSON.stringify(myObject);

        _callServer({
            url: '/SalesAndUse/FilingPaymentDelete',
            closeModalOnClick: false,
            data: { 'pjson': filingMainValues },
            type: "post",
            success: function (savingStatus) {
                //s loadMainPaymentInfo(myObject.filingId);
                loadMainPaymentInfo(filingid);
                _showNotification("success", "Payment deleted successfully.");
                $("#lblPaymentEdit").text("");
            }
        });

    }

}

function clearPayments() {
    $("#drp_paymentCategory").val("0");
    $("#drp_paymentMethod").val("0");
    $("#txt_paymentAmount").val("");

    $("#jqxdropdownbutton_payment_p2plvid").val("");
    $("#jqxdropdownbutton_payment_vendorsite").val("");
    $("#txt_paymentp2plvid2").val("");
    $("#txt_paymentGOC").val("");
    $("#txt_paymentAccount").val("");
    $("#txt_payment_comments").val("");

    $('#btn_payment_add_main').prop('disabled', false);
    $('#btn_payment_edit_main').prop('disabled', true);

    $("#txt_newPaymentComment").val("");
    $("#payment_filingid").val("");
    $("#txt_paymentId").val("");
}

function clickPaymentAdd() {

    if (_getUserInfo().Roles.includes("PREPARER") || _getUserInfo().Roles.includes("ADMIN")) {
        var myObject = new Object();
        myObject.paymentCategory = $("#drp_paymentCategory option:selected").val();
        myObject.paymentMethod = $("#drp_paymentMethod option:selected").val();
        //myObject.paymentAmount = $("#txt_paymentAmount").val();

        myObject.paymentAmount = $('#txt_paymentAmount').val().replace(/,/g, "");
        myObject.paymentid = $('#txt_paymentId').val();
        myObject.comments = $('#txt_newPaymentComment').val();


        myObject.filingId = $('#payment_filingid').text();

        if ($("#selectedPaymentP2PLVID").attr("idP2PLVID") == null) {
            myObject.p2pLvid = 0;
        } else {
            myObject.p2pLvid = $("#selectedPaymentP2PLVID").attr("idP2PLVID");
        }

        if ($("#selectedPaymentVendorSite").attr("paymentVendorSite") == null) {
            myObject.vendor = 0;
        } else {
            myObject.vendor = $("#selectedPaymentVendorSite").attr("paymentVendorSite");
        }

        var filingMainValues = JSON.stringify(myObject);

        //Validate remaining amount
        //if (parseFloat(myObject.paymentAmount) > parseFloat($("#payment_totalremain").text())) {
        //    $("#lblPaymentEdit").text("**The payment amount is higher than the available amount**");
        //    $("#lblPaymentEdit").css('color', 'red');
        //} else {

            var validations = true;

            if (myObject.paymentCategory == "" ||
                myObject.paymentMethod == "0" ||
                myObject.paymentAmount == "" ) {
                validations = false;
            }

            if ($("#payment_achnot").text == 'YES' && (
                myObject.p2pLvid == "" ||
                myObject.vendor == "")) {
                validations = false;
            } 


            //ADD PAYMENT TO FILING
            if (validations) {
                _callServer({
                    url: '/SalesAndUse/FilingPaymentAdd',
                    closeModalOnClick: false,
                    data: { 'pjson': filingMainValues },
                    type: "post",
                    success: function (savingStatus) {
                        loadMainPaymentInfo(myObject.filingId);
                        _showNotification("success", "Payment added successfully.");
                        $("#lblPaymentEdit").text("");
                        $("#drp_paymentMethod").val("0");
                        $("#txt_paymentAmount").val("");
                    }
                });
            } else {

                $("#lblPaymentEdit").text("**Please complete all the required fields.**");
                $("#lblPaymentEdit").css('color', 'red');
            }

        //}
    } else {
        $("#lblPaymentEdit").text("**Only preparers can add payments.**");
        $("#lblPaymentEdit").css('color', 'red');
    }


}

function clickPaymentUpdate() {
    if (_getUserInfo().Roles.includes("PREPARER") || _getUserInfo().Roles.includes("ADMIN")) {
        var myObject = new Object();
        myObject.paymentCategory = $("#drp_paymentCategory option:selected").val();
        myObject.paymentMethod = $("#drp_paymentMethod option:selected").val();
        //myObject.paymentAmount = $("#txt_paymentAmount").val();

        myObject.paymentAmount = $('#txt_paymentAmount').val().replace(/,/g, "");

        myObject.paymentid = $('#txt_paymentId').val();
        myObject.p2pLvid = $("#selectedPaymentP2PLVID").attr("idP2PLVID");
        myObject.vendor = $("#selectedPaymentVendorSite").attr("paymentVendorSite");
        myObject.comments = $('#txt_newPaymentComment').val();
        myObject.filingId = $('#payment_filingid').text();

        var filingMainValues = JSON.stringify(myObject);
        //Missing fields validations

        //Validate remaining amount
        //if (parseFloat(myObject.paymentAmount) > parseFloat($("#payment_totalremain").text())) {
        //    $("#lblPaymentEdit").text("**The payment amount is higher than the available amount**");
        //    $("#lblPaymentEdit").css('color', 'red');
        //} else {

            var validations = true;

            if (myObject.paymentCategory == "" ||
                myObject.paymentMethod == "0" ||
                myObject.paymentAmount == "" ||
                myObject.p2pLvid == "" ||
                myObject.vendor == "") {
                validations = false;
            }

            //ADD PAYMENT TO FILING
            if (validations) {
                _callServer({
                    url: '/SalesAndUse/FilingPaymentUpdate',
                    closeModalOnClick: false,
                    data: { 'pjson': filingMainValues },
                    type: "post",
                    success: function (savingStatus) {
                        loadMainPaymentInfo(myObject.filingId);
                        _showNotification("success", "Payment updated successfully.");
                        $("#lblPaymentEdit").text("");
                        $("#drp_paymentMethod").val("0");
                        $("#txt_paymentAmount").val("");
                    }
                });
            } else {

                $("#lblPaymentEdit").text("**Please complete all the required fields.**");
                $("#lblPaymentEdit").css('color', 'red');
            }
        //}

    } else {
        $("#lblPaymentEdit").text("**Only preparers can edit payments.**");
        $("#lblPaymentEdit").css('color', 'red');
    }
}

function clickSubmitPayment() {

    if (_getUserInfo().Roles.includes("PREPARER") || _getUserInfo().Roles.includes("ADMIN")) {
        var myObject = new Object();
        myObject.filingId = $('#payment_filingid').text();

        var filingMainValues = JSON.stringify(myObject);
        //ADD PAYMENT TO FILING

        var remainTotal = $('#payment_totalremain').text();

        if (parseFloat(remainTotal) == 0) {
            _callServer({
                url: '/SalesAndUse/FilingPaymentSubmit',
                closeModalOnClick: false,
                data: { 'pjson': filingMainValues },
                type: "post",
                success: function (savingStatus) {
                    $('#paymentpopup').fadeOut();
                    chageStatus(myObject.filingId);
                    _showNotification("success", "Payment updated successfully.");
                }
            });
        } else {
            $("#lblPaymentEdit").text("**Please complete the payments.**");
            $("#lblPaymentEdit").css('color', 'red');
        }


    } else {
        $("#lblPaymentEdit").text("**Only preparers can submit payments.**");
        $("#lblPaymentEdit").css('color', 'red');
    }

}

function paymentMethodChange() {

    var selected = $("#drp_paymentMethod option:selected").val();
    //Zero Return
    if (selected == 1) {
        $("#txt_paymentAmount").val("0");
        $("#txt_paymentAmount").attr('disabled', 'disabled');
    } else {
        $("#txt_paymentAmount").val("");
        $("#txt_paymentAmount").removeAttr('disabled');

    }


}
//#endregion

//#region REVIEW MAIN BUTTON
function clickReview(event) {
    $("#reviewpopup").fadeIn(1000, function () {
    });

    loadDropDowns(qry_rejectionReason, "0", "drp_reviewRejectedReason")

    $("#lblSaveMessage").text("");

    var filingid = $("#" + event.currentTarget.id).attr("filingid");

    $("#review_filingid").val(filingid);
    $("#review_filingid").text(filingid);

    loadMainReviewInfo(filingid);

    //load filing data
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Filing Details...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT [idFiling] ,[filingStatus],[reviewerSOEID] ,[reviewedDate] ,[rejectedBy],[rejectedReason],[idCommodityCode] FROM [tblFilings] f JOIN [dbo].[tblJurisdictionLocation] jl ON jl.[idJurisdictionLocation] = f.[idJurisdictionLocation] WHERE [idFiling] = " + filingid + "") },
        type: "post",
        success: function (resultList) {
            var objFunction = resultList[0];

            if (objFunction.filingStatus == "PaymentCompleted" || objFunction.filingStatus == "Rejected" || objFunction.filingStatus == "ExportedToFlatFile" || objFunction.filingStatus == "ReadyToExport" || objFunction.filingStatus == "Mailed") {

                $("#btnReviewSave").prop("disabled", false);
                if (objFunction.filingStatus == "Rejected") {
                    $("#divReviewerReject").show();
                    $("#drp_reviewRejectedBy").val(objFunction.rejectedBy);
                    $("#drp_reviewRejectedReason").val(objFunction.rejectedReason);
                }
            } else {
                $("#divReviewerReject").hide();
                $("#btnReviewSave").prop("disabled", true);
            }
        }
    });


    //load comments
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Rejection Comments...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT [idComment],[id],[stage],[comment],[createdBy],[createdDate] FROM [tblComments] WHERE id = " + filingid + " AND stage = 'reject' order by createdDate DESC;") },
        type: "post",
        success: function (resultList) {
            var comments = "";
            for (var i = 0; i < resultList.length; i++) {
                var objFunction1 = resultList[i];
                comments += "Comment by: " + objFunction1.createdBy + " on " + objFunction1.createdDate.split(' ')[0] + " - " + objFunction1.comment + "\n";

            }
            $("#txt_review_rejectedDescription").val(comments);
        }
    });
}

function reviewReasonChange() {
    if ($("#drp_reviewStatus").val() == "Approved") {
        $("#divReviewerReject").hide();
    } else {
        $("#divReviewerReject").show();
        $("#drp_reviewRejectedReason").val("0");
        $("#drp_reviewRejectedBy").val("0");
        $("#txt_review_rejectedDescription").val("");
    }


}

function loadMainReviewInfo(filingid) {
    //load filing data
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Filing Details...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT [idFiling],[dueDate],[totalAmountPaid],[taxSavings],[filingStatus] FROM [SAU].[dbo].[tblFilings] WHERE [idFiling] = " + filingid + "") },
        type: "post",
        success: function (resultList) {
            var objFunction = resultList[0];
            $("#lbl_rv_total").text(objFunction.totalAmountPaid);

        }
    });


}

function saveReview() {
    if (_getUserInfo().Roles.includes("REVIEWER") || _getUserInfo().Roles.includes("ADMIN")) {
        //set variables
        var myObject = new Object();
        myObject.status = $("#drp_reviewStatus option:selected").val();
        myObject.rejectedBy = $("#drp_reviewRejectedBy option:selected").val();
        myObject.rejectedReason = $("#drp_reviewRejectedReason option:selected").text();
        myObject.rejectedDescription = $('#txt_review_rejectedDescription').val();
        myObject.filingId = $('#review_filingid').text();
        myObject.commodityCode = $("#lbl_rv_commodity").text();
        myObject.total = $("#lbl_rv_total").text();

        var validation = true;

        //validate rejection details
        if (myObject.status == "Rejected") {
            if (myObject.rejectedBy == "0" ||
                myObject.rejectedReason == "0" ||
                myObject.rejectedDescription == "") {
                validation = false;
            }
        }

        //save changes
        if (validation) {
            //validate doa
            _callServer({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading DOA...",
                url: '/Ajax/ExecQuery',
                data: { 'pjsonSql': JSON.stringify("SELECT [SOEID],cc.[commodityCode],[approvalAmount] FROM [SAU].[dbo].[tblAdmin_DOA] do JOIN [dbo].[tblAdmin_CommodityCode] cc ON  do.idCommodityCode = cc.idCommodityCode WHERE SOEID = '" + _getSOEID() + "' AND cc.[commodityCode]='" + myObject.commodityCode + "'") },
                type: "post",
                success: function (resultList) {
                    var objFunction = resultList[0];
                    if (parseFloat(objFunction.approvalAmount) >= parseFloat(myObject.total)) {
                        var filingMainValues = JSON.stringify(myObject);

                        _callServer({
                            url: '/SalesAndUse/FilingReviewSave',
                            closeModalOnClick: false,
                            data: { 'pjson': filingMainValues },
                            type: "post",
                            success: function (savingStatus) {
                                $('#reviewpopup').fadeOut();
                                chageStatus(myObject.filingId);

                                $("#txt_lastreviewdby_" + myObject.filingId).val(_getSOEID());

                                _showNotification("success", "Filing reviewed successfully.");
                            }
                        });
                    } else {
                        _showNotification("error", "Not enough delegation amount.");
                        $("#lblSaveMessage").text("**Not enough delegation amount.**");
                        $("#lblSaveMessage").css('color', 'red');
                    }
                }
            });
        } else {
            $("#lblSaveMessage").text("**Please complete the rejection details.**");
        }
    } else {
        $("#lblSaveMessage").text("**Only reviewers can approve/reject.**");
        $("#lblSaveMessage").css('color', 'red');
    }
}
//#endregion

//#region MAIL MAIN BUTTON
function clickMail(event) {
    var filingid = $("#" + event.currentTarget.id).attr("filingid");
    $("#lblMessageMail").val("");
    $("#mailpopup").fadeIn(1000, function () {
    });

    $("#txt_mailedDate").datepicker();

    $("#mail_filingid").val(filingid);
    $("#mail_filingid").text(filingid);
    //load filing data
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Filing Details...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT [idFiling],[filingStatus],[mailerSOEID],[mailedDate],[mainConfirmationFile],[ckAch] FROM [tblFilings] f JOIN [dbo].[tblJurisdictionLocation] j ON j.[idJurisdictionLocation] = f.[idJurisdictionLocation] WHERE f.[idFiling] = " + filingid + "") },
        type: "post",
        success: function (resultList) {
            $("#chk_confirmationFile").prop("checked", false);
            $("#txt_mailedDate").val("");
            $("#txt_mail_comments").val("");



            var objFunction = resultList[0];

            if ((objFunction.filingStatus == "ExportedToFlatFile" && objFunction.ckAch == "True")
                || 
                ((objFunction.filingStatus == "ReadyToExport" || objFunction.filingStatus == "ExportedToFlatFile") && objFunction.ckAch == 'False')) {
                $("#btn_mail_save").prop("disabled", false);
            } else {
                $("#btn_mail_save").prop("disabled", true);
                
            }

            //Validate Reviewer
            if (_getUserInfo().Roles.includes("REVIEWER")) {
                $("#btn_mail_save").prop("disabled", false);
            }

            


            $("#txt_filingId").val(objFunction.idFiling);

            var mailedby = "SELECT '0' AS [id], '-- Select mailed by --' AS [text] UNION SELECT u.[SOEID] as id, (u.[SOEID] + ' - ' +[Name]) as [text] FROM [tblAutomation_UserXRole] ur JOIN [tblAutomation_User] u ON u.[SOEID] = ur.[SOEID] where IDRole IN (5,6,7);";

            if (objFunction.mailerSOEID == "") {

                loadDropDowns(mailedby, '0', "drp_mailedBy");
            } else {
                loadDropDowns(mailedby, objFunction.mailerSOEID, "drp_mailedBy");
            }

            $("#txt_mailedDate").val(objFunction.mailedDate);


            if (objFunction.mainConfirmationFile == "True") {
                $("#chk_confirmationFile").prop("checked", true);
            } else {
                $("#chk_confirmationFile").prop("checked", false);
            }

            //load comments
            _callServer({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading Comments...",
                url: '/Ajax/ExecQuery',
                data: { 'pjsonSql': JSON.stringify("SELECT [idComment],[id],[stage],[comment],[createdBy],[createdDate] FROM [tblComments] WHERE id = " + filingid + " AND stage = 'mail' order by createdDate DESC;") },
                type: "post",
                success: function (resultList) {
                    var comments = "";
                    for (var i = 0; i < resultList.length; i++) {
                        var objFunction1 = resultList[i];
                        comments += "Comment by: " + objFunction1.createdBy + " on " + objFunction1.createdDate.split(' ')[0] + " - " + objFunction1.comment + "\n";

                    }
                    $("#txt_mail_comments").val(comments);
                }
            });


        }
    });
}

function saveMail() {
    //set variables
    if (_getUserInfo().Roles.includes("MAILER") || _getUserInfo().Roles.includes("ADMIN") || _getUserInfo().Roles.includes("PREPARER")) {
        var myObject = new Object();
        myObject.mailedDate = $("#txt_mailedDate").val();
        myObject.mailedBy = $("#drp_mailedBy option:selected").val();


        if ($("#chk_confirmationFile").is(':checked'))
            myObject.confirmationFile = 1;
        else
            myObject.confirmationFile = 0;

        myObject.comments = $('#txt_mail_comments').val();
        myObject.filingId = $('#mail_filingid').text();

        var validations = true;

        if (myObject.mailedDate == "" ||
            myObject.mailedBy == "0" ||
            myObject.confirmationFile == "") {
            validations = false;
        }

        if (validations) {
            var filingMainValues = JSON.stringify(myObject);

            _callServer({
                url: '/SalesAndUse/FilingMailSave',
                closeModalOnClick: false,
                data: { 'pjson': filingMainValues },
                type: "post",
                success: function (savingStatus) {
                    if(savingStatus == "Success"){
                        $('#mailpopup').fadeOut();
                        chageStatus(myObject.filingId);
                        $("#txt_mailed_" + myObject.filingId).val("YES");
                        _showNotification("success", "Filing mailed successfully.");
                    }else{
                        _showNotification("error", savingStatus);
                    }
                }
            });
        } else {

            $("#lblMessageMail").text("**Please complete all the required fields.**");
            $("#lblMessageMail").css('color', 'red');
        }
    } else {
        $("#lblMessageMail").text("**Only mailers can manage mails**");
        $("#lblMessageMail").css('color', 'red');
    }
}
//#endregion

//#region COMMENTS MAIN BUTTON
function clickComment(event) {
    $("#commentspopup").fadeIn(1000, function () {

    });

    var filingid = $("#" + event.currentTarget.id).attr("filingid");
    $("#comment_filingid").val(filingid);
    $("#comment_filingid").text(filingid);

    //load payment comments
    _callServer({
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT  (UPPER([stage]) + ' - ' + UPPER([createdBy]) + ' - ' + CONVERT(VARCHAR(19),[createdDate]) + ' - ' + [comment]) as comment FROM [SAU].[dbo].[tblComments] where idFiling = " + filingid + " order by createdDate DESC;") },
        type: "post",
        success: function (resultList) {
            var comments = "";
            for (var i = 0; i < resultList.length; i++) {
                var objFunction1 = resultList[i];
                comments += objFunction1.comment + "\n";

            }
            $("#txt_comments_comments").val(comments);
        }
    });

}
//#endregion

//#region CHECKLIST MAIN BUTTON
function clickChecklist(event) {
    $("#checklistpopup").fadeIn(1000, function () {
    });

    var filingid = $("#" + event.currentTarget.id).attr("filingid");

    $("#checklist_filingid").val(filingid);
    $("#checklist_filingid").text(filingid);


        if ($("#lblChecklistFrequency").text() == "Annually" || $("#lblChecklistFrequency").text() == "Semi") {
            $("#checklistpopup").removeClass("modalAutoCenter");
            $("#checklistpopup").addClass("modalAutoCenterAnnual");
        } else {
            $("#checklistpopup").removeClass("modalAutoCenterAnnual");
            $("#checklistpopup").addClass("modalAutoCenter");
        }


    

    var myObject = new Object();
    myObject.filingid = filingid;
    var filingMainValues = JSON.stringify(myObject);

    //CREATE MISSING CHECKLIST ITEMS
    _callServer({
        url: '/SalesAndUse/FilingChecklistValidate',
        closeModalOnClick: false,
        data: { 'pjson': filingMainValues },
        type: "post",
        success: function (savingStatus) {

            //LOAD CHECKLIST ITEMS
            var checklistItems = "SELECT [idFilingChecklist],fc.[idFiling],[filingStatus],[checklistStatus],[comment],[createdBy],[createdDate],[checklist],[order] FROM [tblFilingChecklist] fc LEFT JOIN [dbo].[tblComments] c ON c.id = fc.idFilingChecklist JOIN [dbo].[tblAdmin_Checklist] ch ON ch.[idChecklist] = fc.[idChecklist] JOIN [dbo].[tblFilings] ff ON ff.idFiling = fc.idFiling WHERE fc.idFiling = " + filingid + " and ch.Active = 1 ORDER BY [order]";
            _callServer({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading Checklist Details...",
                url: '/Ajax/ExecQuery',
                data: { 'pjsonSql': JSON.stringify(checklistItems) },
                type: "post",
                success: function (resultList) {
                    //FOR LOAD ROWS
                    $("#divChecklistList").empty();
                    for (var i = 0; i < resultList.length; i++) {
                        loadChecklistRows(resultList, i);
                    }

                    $('.checklistChange').change(function (e) { clickChecklistSave(e); });
                    $('.checklistSave').click(function (e) { clickChecklistSave(e); });
                }
            });
        }
    });
}

function loadChecklistRows(resultList, i) {
    //CREATE VARIABLE WITH LISTED DIVS
    //#region Variable

    var singlerow = '        \
                <div class="row" style="margin-top:5px;" > \
                    <div class="col-md-6 "> \
                        <div class="col-md-1"> \
                               <button id="btn_checklist_edit_@checklistID" @disabled checklistid="@checklistID" type="button" class="btn btn-xs btn-default checklistSave"> <span class="fa fa-check-square" style="margin-top:5px;"> </span> </button> \
                        </div> \
                        <div class="col-md-11 "> \
                            <label class="control-label">' + resultList[i].checklist + '</label> \
                            </div> \
                    </div> \
                    <div class="col-md-6 "> \
                        <div class="col-md-1 "> \
                            <select class="combobox" id="cbmChecklist_@checklistID" onchange="clickChecklistSave(event)" checklistid="@checklistID" @disabled> \
                              <option value="NO">NO</option> \
                              <option value="YES">YES</option> \
                              <option value="NA">NA</option> \
                            </select> \
                        </div> \
                        <div class="col-md-11 "> \
                              <textarea class="form-control" rows="1" id="txtChecklistComment_@checklistID" onfocusout="clickChecklistSave(event)" checklistid="@checklistID"></textarea> \
                        </div> \
                    </div> \
                </div>';
    //#endregion

    var objFunction = resultList[i];
    singlerow = singlerow.replace(new RegExp('@checklistID', 'g'), objFunction.idFilingChecklist);

    if ((objFunction.filingStatus != "PendingToPrepare" &&
                objFunction.filingStatus != "Rejected" &&
                objFunction.filingStatus != "ChecklistReady" &&
                objFunction.filingStatus != "FilingEdited" &&
                objFunction.filingStatus != "PaymentCompleted") || (!_getUserInfo().Roles.includes("PREPARER") && !_getUserInfo().Roles.includes("ADMIN"))) {
        singlerow = singlerow.replace(new RegExp('@disabled', 'g'), "disabled");
        $("#btn_payment_submitChecklist").prop("disabled", true);     

    } else {
        singlerow = singlerow.replace(new RegExp('@disabled', 'g'), "");
        $("#btn_payment_submitChecklist").prop("disabled", false);
    }

    $("#divChecklistList").append(singlerow);

    $("#cbmChecklist_" + objFunction.idFilingChecklist + "").val(objFunction.checklistStatus);


    $("#txtChecklistComment_" + objFunction.idFilingChecklist + "").val(objFunction.comment);


    //$("#txt_ach_" + objFunction.idFiling + "").val(objFunction.);

}

function clickChecklistSave(event) {

    var checklistid = $("#" + event.currentTarget.id).attr("checklistid");
    var myObject = new Object();

    myObject.checklistid = checklistid;
    myObject.checklistcomment = $("#txtChecklistComment_" + checklistid + "").val();
    myObject.checkliststatus = $("#cbmChecklist_" + checklistid + "").val();


    var checklistMainValues = JSON.stringify(myObject);
    //Missing fields validations


    //ADD PAYMENT TO FILING

    _callServer({
        url: '/SalesAndUse/ChecklistSave',
        closeModalOnClick: false,
        data: { 'pjson': checklistMainValues },
        type: "post",
        success: function (savingStatus) {

            _callServer({
                url: '/Ajax/ExecQuery',
                data: { 'pjsonSql': JSON.stringify("SELECT [idFiling] FROM [tblFilingChecklist] where idFilingChecklist = " + checklistid + "") },
                type: "post",
                success: function (resultList) {
                    var idFiling = resultList[0].idFiling;
                    chageStatus(idFiling);
                }
            });
            _showNotification("success", "Checklist updated successfully.");
            //$("#lblChecklistSaveMessage").text("Checklist updated successfully.");


        }
    });

}

function clickSubmitChecklist() {


    if (_getUserInfo().Roles.includes("PREPARER") || _getUserInfo().Roles.includes("ADMIN")) {
        var myObject = new Object();
        myObject.filingid = $('#checklist_filingid').text();
        var filingMainValues = JSON.stringify(myObject);

        _callServer({
            url: '/SalesAndUse/FilingChecklistSubmit',
            closeModalOnClick: false,
            data: { 'pjson': filingMainValues },
            type: "post",
            success: function (savingStatus) {
                $('#checklistpopup').fadeOut();
                chageStatus(myObject.filingid);
                _showNotification("success", "Checklist submitted successfully.");
            }
        });

    } else {
        $("#lblChecklistSaveMessage").text("**Only preparers can edit filings.**");
        $("#lblChecklistSaveMessage").css('color', 'red');
    }




}


//#endregion

//#region COVESHEET MAIN BUTTON
function clickCoverSheet(event) {
    $("#coversheetpopup").fadeIn(1000, function () {
    });

    var filingid = $("#" + event.currentTarget.id).attr("filingid");

    //LOAD APPROVERS USERS
    loadCoversheetUsersInfo(filingid, function () {
        var queryMainData = "SELECT filingStatus,tblFilings.idJurisdictionLocation,CONVERT(CHAR(10),dueDate, 101) as dueDate,DATEPART(YEAR,dueDate) as FYear,[type],corpCode,lvid,legalEntityName,fein as idFEIN,stateDivSub,jurisdiction, \
                        frequency,DATENAME(MONTH,  DateAdd(M,-1,dueDate)) as dueMonth,filingType,dueMonth as filingPeriod,mailerSOEID,CONVERT(CHAR(10),mailedDate, 101) as mailedDate,account FROM tblFilings INNER JOIN \
                        tblJurisdictionLocation ON tblFilings.idJurisdictionLocation = tblJurisdictionLocation.idJurisdictionLocation INNER JOIN \
                        tblAdmin_Account ON tblJurisdictionLocation.idAccount = tblAdmin_Account.idAccount INNER JOIN \
                        tblAdmin_Jurisdiction ON tblJurisdictionLocation.idJurisdiction = tblAdmin_Jurisdiction.idJurisdiction INNER JOIN \
                        tblAdmin_LVID ON tblJurisdictionLocation.idLVID = tblAdmin_LVID.idLVID INNER JOIN \
                        tblAdmin_CorpCode ON tblAdmin_LVID.idCorpCode = tblAdmin_CorpCode.idCorpCode INNER JOIN \
                        tblAdmin_LegalEntity ON tblAdmin_LVID.idLegalEntity = tblAdmin_LegalEntity.idLegalEntity INNER JOIN \
                        tblAdmin_Type ON tblJurisdictionLocation.idType = tblAdmin_Type.idType INNER JOIN \
                        tblAdmin_FEIN ON tblAdmin_LVID.idFEIN = tblAdmin_FEIN.idFEIN INNER JOIN \
                        tblAdmin_StateDivSub ON tblJurisdictionLocation.idState = tblAdmin_StateDivSub.idState WHERE [idFiling] = "+ filingid + "";

        //load filing data
        _callServer({
            loadingMsgType: "fullLoading",
            loadingMsg: "Loading Filing Details...",
            url: '/Ajax/ExecQuery',
            data: { 'pjsonSql': JSON.stringify(queryMainData) },
            type: "post",
            success: function (resultList) {
                var objFunction = resultList[0];

                $("#txt_pdf_status").val(objFunction.filingStatus);
                $("#txt_pdf_logID").val(objFunction.idJurisdictionLocation);
                $("#txt_pdf_dueDate").val(objFunction.dueDate);
                $("#lblCSType").val(objFunction.type);
                $("#lblCSCorp").val(objFunction.corpCode);
                $("#lblCSLVID").val(objFunction.lvid);
                $("#lblCSLE").val(objFunction.legalEntityName);
                $("#lblCSLicense").val("");
                $("#lblCSFEIN").val(objFunction.idFEIN);
                $("#lblCSState").val(objFunction.stateDivSub);
                $("#lblCSJurisdiction").val(objFunction.jurisdiction);
                $("#lblCSFrequency").val(objFunction.frequency);
                $("#lblCSFilingPeriod").val(objFunction.dueMonth);
                $("#lblCSLicense").val(objFunction.account);


                $("#txt_pdf_filingtype").val(objFunction.filingType);
                $("#txt_pdf_filingperiod").val(objFunction.FYear);
                $("#txt_pdf_frequency").val(objFunction.frequency);
                $("#txt_pdf_mailed").val(objFunction.mailedDate);


                //LOAD CHECKLIST ITEMS
                var checklistItems = "SELECT [idFilingChecklist],fc.[idFiling],[filingStatus],[checklistStatus],[comment],[createdBy],[createdDate],[checklist],[order] FROM [tblFilingChecklist] fc LEFT JOIN [dbo].[tblComments] c ON c.id = fc.idFilingChecklist JOIN [dbo].[tblAdmin_Checklist] ch ON ch.[idChecklist] = fc.[idChecklist] JOIN [dbo].[tblFilings] ff ON ff.idFiling = fc.idFiling WHERE fc.idFiling = " + filingid + " and ch.Active = 1 ORDER BY [order]";
                _callServer({
                    loadingMsgType: "fullLoading",
                    loadingMsg: "Loading Checklist Details...",
                    url: '/Ajax/ExecQuery',
                    data: { 'pjsonSql': JSON.stringify(checklistItems) },
                    type: "post",
                    success: function (resultList) {
                        //FOR LOAD ROWS
                        $("#div_pdf_checklist_items").empty();
                        for (var i = 0; i < resultList.length; i++) {
                            loadChecklistRowsCover(resultList, i);
                        }

                        loadPaymentLinesCS(filingid);



                        //LOAD COMMENTS
                        var commentsQuery = "SELECT DISTINCT * FROM (SELECT[idComment],CONVERT(VARCHAR(19),c.[createdDate]) as createdDate,u.[Name] as Name ,title as Entitlement,UPPER([stage]) as STAGE,[comment] \
                                FROM [SAU].[dbo].[tblComments] c JOIN [dbo].[tblAutomation_User] u ON u.SOEID = c.createdBy JOIN [dbo].[tblAutomation_UserXRole] \
                                ur ON ur.SOEID = u.SOEID JOIN [dbo].[tblAutomation_Role] r ON r.[ID] = ur.[IDRole]  JOIN [dbo].[tblTaxUserInfo] tx ON tx.SOEID = c.createdBy WHERE [idFiling] = " + filingid + " AND [stage] != 'Checklist') as s ORDER BY s.createdDate DESC";
                        _callServer({
                            url: '/Ajax/ExecQuery',
                            data: { 'pjsonSql': JSON.stringify(commentsQuery) },
                            type: "post",
                            success: function (resultList) {
                                $("#div_pdf_comments_items").empty();
                                for (var i = 0; i < resultList.length; i++) {
                                    loadCommentsRowsCover(resultList, i);
                                }

                                var today = new Date();

                                var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                                _generatePDFFromHtml("#coversheetpopup", "filing_" + filingid + "_" + date + ".pdf");

                                //set timeout of 3 seconds to download and generate pdf

                                //FONG 
                                setTimeout(function () {
                                    $("#coversheetpopup").fadeOut(1000, function () { });
                                }, 6000)
                            }
                        });
                    }
                });
            }
        });
    });

    
}

function loadCoversheetUsersInfo(filingid, onSuccess) {
    //LOAD PREPARER
    var preparerQuery = "SELECT TOP 1 [preparerSOEID],au.Name, u.title, u.phone,CONVERT(date, f.preparedDate) as preparedDate FROM [SAU].[dbo].[tblFilings] f JOIN [dbo].[tblTaxUserInfo] u ON f.preparerSOEID = u.SOEID JOIN [dbo].[tblAutomation_User] au ON au.SOEID = f.preparerSOEID where f.idFiling = " + filingid + "";
    var totalAjaxToComplete = 3;
    var ajaxCounter = 0;

    _callServer({
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify(preparerQuery) },
        type: "post",
        success: function (resultList) {
            $("#lbl_coversheet_preparer_name").text("-");
            $("#lbl_coversheet_preparer_title").text("-");
            $("#lbl_coversheet_preparer_phone").text("-");
            $("#lbl_coversheet_preparer_date").text("-");
            if(resultList.length > 0){
                var objFunction = resultList[0];
                var d = new Date(objFunction.preparedDate);

                $("#lbl_coversheet_preparer_name").text(objFunction.Name);
                $("#lbl_coversheet_preparer_title").text(objFunction.title);
                $("#lbl_coversheet_preparer_phone").text(objFunction.phone);
                $("#lbl_coversheet_preparer_date").text(d.toLocaleDateString());
            }

            ajaxCounter++;
            if (ajaxCounter == totalAjaxToComplete) {
                if (onSuccess) {
                    onSuccess();
                }
            }
        }
    });

    //LOAD REVIEWER
    var reviewerQuery = "SELECT TOP 1 reviewerSOEID,au.Name, u.title, u.phone,CONVERT(date, f.reviewedDate) as reviewedDate FROM [SAU].[dbo].[tblFilings] f JOIN [dbo].[tblTaxUserInfo] u ON f.reviewerSOEID = u.SOEID JOIN [dbo].[tblAutomation_User] au ON au.SOEID = f.reviewerSOEID where f.idFiling = " + filingid + "";
    _callServer({
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify(reviewerQuery) },
        type: "post",
        success: function (resultList) {
            $("#lbl_coversheet_reviewer_name").text("-");
            $("#lbl_coversheet_reviewer_title").text("-");
            $("#lbl_coversheet_reviewer_phone").text("-");
            $("#lbl_coversheet_reviewer_date").text("-");
            if (resultList.length > 0) {
                var objFunction = resultList[0];
                var d = new Date(objFunction.reviewedDate);
               
                $("#lbl_coversheet_reviewer_name").text(objFunction.Name);
                $("#lbl_coversheet_reviewer_title").text(objFunction.title);
                $("#lbl_coversheet_reviewer_phone").text(objFunction.phone);
                $("#lbl_coversheet_reviewer_date").text(d.toLocaleDateString());
            }

            ajaxCounter++;
            if (ajaxCounter == totalAjaxToComplete) {
                if (onSuccess) {
                    onSuccess();
                }
            }
        }
    });

    //LOAD MAILER
    var mailerQuery = "SELECT TOP 1 mailerSOEID,au.Name, u.title, u.phone,CONVERT(date, mailedDate) as mailedDate FROM [SAU].[dbo].[tblFilings] f JOIN [dbo].[tblTaxUserInfo] u ON f.mailerSOEID = u.SOEID JOIN [dbo].[tblAutomation_User] au ON au.SOEID = f.mailerSOEID where f.idFiling = " + filingid + "";
    _callServer({
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify(mailerQuery) },
        type: "post",
        success: function (resultList) {
            $("#lbl_coversheet_mailer_name").text("-");
            $("#lbl_coversheet_mailer_title").text("-");
            $("#lbl_coversheet_mailer_phone").text("-");
            $("#lbl_coversheet_mailer_date").text("-");

            if(resultList.length > 0){
                var objFunction = resultList[0];
                var d = new Date(objFunction.mailedDate);
                
                $("#lbl_coversheet_mailer_name").text(objFunction.Name);
                $("#lbl_coversheet_mailer_title").text(objFunction.title);
                $("#lbl_coversheet_mailer_phone").text(objFunction.phone);
                $("#lbl_coversheet_mailer_date").text(d.toLocaleDateString());
            }

            ajaxCounter++;
            if (ajaxCounter == totalAjaxToComplete) {
                if (onSuccess) {
                    onSuccess();
                }
            }
        }
    });

}

function loadChecklistRowsCover(resultList, i) {
    //CREATE VARIABLE WITH LISTED DIVS
    //#region Variable

    var singlerow = '        \
                <div class="row" style="margin-top:5px;" > \
                    <div class="col-md-6 "> \
                        <div class="col-md-1"> \
                        </div> \
                        <div class="col-md-11 "> \
                            <label class="control-label pdfFontColor">' + resultList[i].checklist + '</label> \
                            </div> \
                    </div> \
                    <div class="col-md-6 "> \
                        <div class="col-md-1 "> \
                            <label class="pdfFontColor" id="lbl_chk_Coverchecklist_@checklistID"></label> \
                        </div> \
                        <div class="col-md-11 "> \
                              <label class="form-control" rows="1" id="txtCoverChecklistComment_@checklistID"></label> \
                        </div> \
                    </div> \
                </div>';
    //#endregion

    var objFunction = resultList[i];
    singlerow = singlerow.replace(new RegExp('@checklistID', 'g'), objFunction.idFilingChecklist);

    $("#div_pdf_checklist_items").append(singlerow);

    $("#lbl_chk_Coverchecklist_" + objFunction.idFilingChecklist + "").text(objFunction.checklistStatus);

    $("#txtCoverChecklistComment_" + objFunction.idFilingChecklist + "").html(objFunction.comment);
}

function loadPaymentLinesCS(filingid) {
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Payment Line Details...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify("SELECT idPayment,p.idFiling,l.lvid,p.idP2PGocLvid,p2.goc ,[vendorSupplier],[vendorSiteCode],a.account,p.idVendorSite,v.vendorSite,v.description,paymentCategory,pm.paymentMethod,CONVERT(DECIMAL(10,2),[paymentAmount]) AS paymentAmount,paymentStatus,jl.[ckAch] FROM tblPayments p LEFT JOIN tblAdmin_P2PLvidGoc p2 ON p.idP2PGocLvid = p2.idP2PGocsLvid LEFT JOIN tblAdmin_P2PVendorSite v ON v.idVendorSite = p.idVendorSite LEFT JOIN tblAdmin_PaymentMethod pm ON pm.idPaymentMethod = p.idPaymentMethod LEFT JOIN tblAdmin_P2PAccount a ON p2.idAccount = a.idP2PAccount LEFT JOIN tblAdmin_LVID l ON p2.idLvid = l.idLVID LEFT JOIN [dbo].[tblFilings] f ON f.idFiling = p.idFiling LEFT JOIN tblJurisdictionLocation jl ON jl.[idJurisdictionLocation] = f.[idJurisdictionLocation]  WHERE p.idFiling = " + filingid + ""
                )
        },
        type: "post",
        success: function (resultList) {

            $("#div_pdf_payment_items").empty();
            for (var i = 0; i < resultList.length; i++) {
                loadPaymentRowsCS(resultList, i);
            }

            loadPaymentTotalCS(filingid);

        }
    });
}

function loadPaymentTotalCS(filingid) {
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Payment Line Details...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify("SELECT SUM(CONVERT(DECIMAL(10,2),[paymentAmount])) totalAmount FROM [SAU].[dbo].[tblPayments] WHERE idFiling = " + filingid + ""
                )
        },
        type: "post",
        success: function (resultList) {
            var objFunction = resultList[0];
            var paymentLineTotal = new Cleave("#lblTotalDueFee", {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand'
            });
            paymentLineTotal.setRawValue(objFunction.totalAmount);          
        }
    });
}

function loadPaymentRowsCS(resultList, i) {
    //#region DIV
    var singlerow = '<div class=\"row\" style=\"margin-top:10px; text-align:center;\"> \
                            <div class=\"col-md-1 nopaddingCols compactInput2\"> \
                                <div class=\"form-group \"> \
                                      <input type=\"text\" id=\"txt_CS_payment_category_@paymentid\" disabled class=\"form-control pdfFontColor compactInput2\" placeholder=\"Category\">  \
                                </div> \
                            </div>  \
                            <div class=\"col-md-2 nopaddingCols compactInput2\"> \
                                <div class=\"form-group \"> \
                                       <input type=\"text\" id=\"txt_CS_payment_method_@paymentid\" disabled class=\"form-control pdfFontColor compactInput2\" placeholder=\"Payment Method\">  \
                                </div> \
                            </div> \
                            <div class=\"col-md-2 nopaddingCols compactInput2\"> \
                                <div class=\"form-group \"> \
                                    <div class="input-group"> \
                                        <span class="input-group-addon">$</span> \
                                        <input style=\"text-align: right;\" type=\"text\" id=\"txt_CS_payment_amount_@paymentid\" disabled class=\"form-control pdfFontColor compactInput2\" placeholder=\"Amount\">  \
                                    </div> \
                                </div> \
                            </div> \
                            <div class=\"col-md-1 nopaddingCols compactInput2\"> \
                                <div class=\"form-group \"> \
                                      <input type=\"text\" id=\"txt_CS_payment_P2PLvid_@paymentid\" disabled class=\"form-control pdfFontColor compactInput2\" placeholder=\"P2P LVID\">  \
                                </div> \
                            </div> \
                            <div class=\"col-md-2 nopaddingCols compactInput2\"> \
                                <div class=\"form-group \"> \
                                       <input type=\"text\" id=\"txt_CS_payment_P2PAccount_@paymentid\" disabled class=\"form-control pdfFontColor compactInput2\" placeholder=\"P2P Account\">  \
                                </div> \
                            </div> \
                            <div class=\"col-md-1 nopaddingCols compactInput2\"> \
                                <div class=\"form-group \"> \
                                       <input type=\"text\" id=\"txt_CS_payment_P2PGoc_@paymentid\" disabled class=\"form-control pdfFontColor compactInput2\" placeholder=\"P2P GOC\">  \
                                </div> \
                            </div> \
                            <div class=\"col-md-1 nopaddingCols compactInput2\"> \
                                <div class=\"form-group \"> \
                                         <input type=\"text\" id=\"txt_CS_payment_Vendor_@paymentid\" disabled class=\"form-control pdfFontColor compactInput2\" placeholder=\"Vendor\">  \
                                </div> \
                            </div> \
                            <div class=\"col-md-2 nopaddingCols compactInput2\"> \
                                <div class=\"form-group \"> \
                                    <input type=\"text\" id=\"txt_CS_payment_sitecode_@paymentid\" disabled class=\"form-control pdfFontColor compactInput2\" placeholder=\"Site Code\">  \
                                </div> \
                            </div> \
                        </div>';
    //#endregion

    var objFunction = resultList[i];
    singlerow = singlerow.replace(new RegExp('@paymentid', 'g'), objFunction.idPayment);
    $("#div_pdf_payment_items").append(singlerow);

    $("#txt_CS_payment_category_" + objFunction.idPayment + "").val(objFunction.paymentCategory);
    $("#txt_CS_payment_method_" + objFunction.idPayment + "").val(objFunction.paymentMethod);

    //$("#txt_CS_payment_amount_" + objFunction.idPayment + "").val(objFunction.paymentAmount);
    var paymentLine = new Cleave("#txt_CS_payment_amount_" + objFunction.idPayment + "", {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });

    paymentLine.setRawValue(objFunction.paymentAmount);


    $("#txt_CS_payment_P2PLvid_" + objFunction.idPayment + "").val(objFunction.lvid);
    $("#txt_CS_payment_P2PGoc_" + objFunction.idPayment + "").val(objFunction.goc);
    $("#txt_CS_payment_P2PAccount_" + objFunction.idPayment + "").val(objFunction.account);
    if (objFunction.ckAch == "True") {//ACH
        $("#txt_CS_payment_Vendor_" + objFunction.idPayment + "").val(objFunction.description);
        $("#txt_CS_payment_sitecode_" + objFunction.idPayment + "").val(objFunction.vendorSite);
    }else{//CHCKCK
        $("#txt_CS_payment_Vendor_" + objFunction.idPayment + "").val(objFunction.vendorSupplier);
        $("#txt_CS_payment_sitecode_" + objFunction.idPayment + "").val(objFunction.vendorSiteCode);
    }
    


}

function loadCommentsRowsCover(resultList, i) {
    //CREATE VARIABLE WITH LISTED DIVS
    //#region Variable

    var singlerow = '        \
                <div class="row" > \
                    <div class="col-md-2 nopaddingColsComments compactInput"> \
                            <label id=\"txt_CS_CS_comment_date_@commentid\" class="control-label pdfFontColor"></label> \
                        </div> \
                        <div class="col-md-2 nopaddingColsComments compactInput"> \
                            <label id=\"txt_CS_CS_comment_name_@commentid\" class="control-label pdfFontColor"></label> \
                        </div> \
                        <div class="col-md-2 nopaddingColsComments compactInput"> \
                            <label  id=\"txt_CS_CS_comment_entitlement_@commentid\" class="control-label pdfFontColor"></label> \
                        </div> \
                        <div class="col-md-1 nopaddingColsComments compactInput"> \
                            <label id=\"txt_CS_CS_comment_stage_@commentid\" class="control-label pdfFontColor"></label> \
                        </div> \
                        <div class="col-md-5 nopaddingColsComments compactInput"> \
                            <label id=\"txt_CS_CS_comment_comment_@commentid\" class="control-label pdfFontColor"></label> \
                        </div> \
                </div>';
    //#endregion

    var objFunction = resultList[i];
    singlerow = singlerow.replace(new RegExp('@commentid', 'g'), objFunction.idComment);
    $("#div_pdf_comments_items").append(singlerow);
    $("#txt_CS_CS_comment_date_" + objFunction.idComment + "").text(objFunction.createdDate);
    $("#txt_CS_CS_comment_name_" + objFunction.idComment + "").text(objFunction.Name);
    $("#txt_CS_CS_comment_entitlement_" + objFunction.idComment + "").text(objFunction.Entitlement);
    $("#txt_CS_CS_comment_stage_" + objFunction.idComment + "").text(objFunction.STAGE);
    $("#txt_CS_CS_comment_comment_" + objFunction.idComment + "").text(objFunction.comment);



}
//#endregion

//function isNumberKey(evt, obj) {
//    var charCode = (evt.which) ? evt.which : event.keyCode
//    var value = obj.value;

//    if (charCode == 45 && value == "")
//    { return true; }
//    var dotcontains = value.indexOf(".") != -1;
//    if (dotcontains)
//        if (charCode == 46) return false;
//    if (charCode == 46) return true;
//    if (charCode > 31 && (charCode < 48 || charCode > 57))
//        return false;
//    return true;
//}

