﻿$(document).ready(function () {
    initUpload();
    var qry = "SELECT '0' AS [id],	'-- Period --' AS [text] UNION SELECT CONVERT(VARCHAR(50),[ImportMonth]) + '-'+ CONVERT(VARCHAR(50),[ImportYear]) AS [id], CONVERT(VARCHAR(50),[ImportMonth]) + '-' + CONVERT(VARCHAR(50),[ImportYear]) AS [text] FROM [SAU].[dbo].[tblVertex_HistoryExportELA] GROUP BY CONVERT(VARCHAR(50),[ImportMonth]) + '-'+ CONVERT(VARCHAR(50),[ImportYear]), CONVERT(VARCHAR(50),[ImportMonth]) + '-'+ CONVERT(VARCHAR(50),[ImportYear])"
    loadDropDowns2(qry, "0", "drop_Periods");
    loadWorkingTable();
    loadApprovingTable()
    loadExportTable();
    loadNonExportTable();
    loadImportLogTable()
});

function initUpload() {
    var fnClickUpload = function () {
        if ($("#txt_ImportMonth").val() == "" || $("#txt_ImportYear").val() == "") {
            _showNotification("error", "Empty field found. Please select all the necessary details.");
        } else {
            $('#fine-uploader-manual-trigger').fineUploader('setParams', {
                'ImpMonth': $("#txt_ImportMonth").val(),
                'ImpYear': $("#txt_ImportYear").val()
            });

            $('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
        }
    };

    $('#fine-uploader-manual-trigger').fineUploader({
        template: 'qq-template-manual-trigger',
        request: {
            endpoint: '/SalesAndUse/UploadELA'
        },
        thumbnails: {
            placeholders: {
                waitingPath: '/Content/Shared/plugins/fine-uploader/placeholders/waiting-generic.png',
                notAvailablePath: '/Content/Shared/plugins/fine-uploader/placeholders/not_available-generic.png'
            }
        },
        callbacks: {
            onComplete: function (id, name, resonseJSON, xhr) {
                console.log(id, name, resonseJSON, xhr)
            },
            onAllComplete: function (succeeded, failed) {
                //console.log(succeeded, failed, this);
                if (failed.length == 0) {
                    _showAlert({
                        type: "success",
                        title: "Message",
                        content: "The file was uploaded successfully"
                    });
                    $("#txt_ImportMonth").val("");
                    $("#txt_ImportYear").val("");
                    $('#fine-uploader-manual-trigger').fineUploader('reset');

                    $('#trigger-upload').click(fnClickUpload);
                    //$('section.box .actions .box_toggle').click();
                    loadWorkingTable();
                    loadExportTable();
                    loadNonExportTable();
                    loadImportLogTable();
                    loadApprovingTable();
                }
            },
            onError: function (id, name, errorReason, xhrOrXdr) {
                if (name) {
                    _showDetailAlert({
                        title: "Message",
                        shortMsg: "An error ocurred with the file '" + name + "'.",
                        longMsg: errorReason,
                        type: "Error",
                        viewLabel: "View Details"
                    });
                }
            }
        },
        validation: {
            allowedExtensions: ['xls','xlsx'],
            itemLimit: 1
        },
        autoUpload: false
    });
    $('#trigger-upload').click(fnClickUpload);
}

function loadDropDowns2(selectQuery, selectedId, dropList) {
    var $select;

    if (typeof dropList == "string") {
        $select = $("#" + dropList + "");
    } else {
        $select = dropList;
    }
        //Set default value  
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Log ID Details...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify(selectQuery) },
        type: "post",
        success: function (resultList) {
            $select.contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $select.append($('<option>', { value: objFunction.id, text: objFunction.text, selected: (objFunction.id == selectedId) }));
            }
        }
    });
};

function loadImportLogTable() {
    $.jqxGridApi.create({
        showTo: "#tblImportTable",
        options: {
            //for comments or descriptions
            height: "400",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_VF_LT_TABLE_IMPORT]",
            Params: [{ Name: "@AppID", Value: "ELA" }
            ]
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'ImportID', type: 'int', text: "Import ID", hidden: true },
            { name: 'FileName', type: 'string', text: "File Name", width: '40%' },
            { name: 'FileLines', type: 'int', text: "File # Lines", width: '10%' },
            { name: 'ImportLines', type: 'int', text: "Imported # Lines", width: '13%' },
            { name: 'Month', type: 'int', text: "Month", width: '5%' },
            { name: 'Year', type: 'int', text: "Year", width: '5%' },
            { name: 'JournalGeneration', type: 'int', text: "Journal", width: '7%' },
            { name: 'ResponsableUser', type: 'string', text: "Responsable", width: '10%' },
            { name: 'DateStamp', type: 'datetime', text: "Import Date", width: '10%' }
        ],

        ready: function () {
            var rowCount = $("#tblImportTable").jqxGrid('getrows').length;
            $("#TableImport").text('Row count: ' + rowCount);
            _hideLoadingFullPage();
        }
    });
};

function loadWorkingTable() {
    $.jqxGridApi.create({
        showTo: "#tblWorkingTable",
        options: {
            //for comments or descriptions
            height: "400",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_VF_LT_TABLE_WORKING]",
            Params: [
                { Name: "@AppID", Value: "ELA" }
            ]
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'ELAWorkingID', type: 'string', hidden: true },
            { name: 'LVID', type: 'string', text: "LVID", width: '7%' },
            { name: 'CustomerGOC', type: 'string', text: "Costumer GOC", width: '10%' },
            { name: 'TaxDue', type: 'string', text: "Tax Due", width: '7%' },
            { name: 'ActualCost', type: 'string', text: "Actual Cost", width: '7%' },
            { name: 'City', type: 'string', text: "City", width: '13%' },
            { name: 'State', type: 'string', text: "State", width: '5%' },
            { name: 'ZipCode', type: 'string', text: "Zip Code", width: '7%' },
            { name: 'GOC_EXPCODE', type: 'string', hidden: true },
            { name: 'IsTaxIncluded', type: 'float', hidden: true },
            { name: 'InvoiceDescription', type: 'string', hidden: true },
            { name: 'DestinationTable', type: 'string', hidden: true },
            { name: 'ImportMonth', type: 'int', text: "Import Month", width: '7%' },
            { name: 'ImportYear', type: 'int', text: "Import Year", width: '7%' },
            { name: 'ReviewReason', type: 'string', text: "Review Reason", width: '20%' },
            { name: 'Remarks', type: 'string', hidden: true },
            { name: 'Scenario', type: 'string', hidden: true }
        ],

        ready: function () {
            var rowCount = $("#tblWorkingTable").jqxGrid('getrows').length;
            $("#TableWorkingImport").text('Row count: ' + rowCount);

            $('#tblWorkingTable').on('rowdoubleclick', function (event) {
                if (typeof event.args.group == "undefined") {
                    //_showLoadingFullPage();
                    var selectedRow;
                    selectedRow = $.jqxGridApi.getOneSelectedRow("#tblWorkingTable", true);
                    generateForm(selectedRow, "Working");
                }
            });

            _hideLoadingFullPage();
        }
    });
};

function loadApprovingTable() {
    $.jqxGridApi.create({
        showTo: "#tblApprovalTable",
        options: {
            //for comments or descriptions
            height: "400",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_VF_LT_TABLE_APPROVING]",
            Params: [
            { Name: "@AppID", Value: "ELA" }
            ]
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'ELAWorkingID', type: 'string', hidden: true },
            { name: 'LVID', type: 'string', text: "LVID", width: '7%' },
            { name: 'CustomerGOC', type: 'string', text: "Costumer GOC", width: '10%' },
            { name: 'TaxDue', type: 'string', text: "Tax Due", width: '7%' },
            { name: 'ActualCost', type: 'string', text: "Actual Cost", width: '7%' },
            { name: 'City', type: 'string', text: "City", width: '13%' },
            { name: 'State', type: 'string', text: "State", width: '5%' },
            { name: 'ZipCode', type: 'string', text: "Zip Code", width: '7%' },
            { name: 'GOC_EXPCODE', type: 'string', hidden: true },
            { name: 'IsTaxIncluded', type: 'float', hidden: true },
            { name: 'InvoiceDescription', type: 'string', hidden: true },
            { name: 'Imp', type: 'string', hidden: true },
            { name: 'ImportMonth', type: 'int', text: "Import Month", width: '7%' },
            { name: 'ImportYear', type: 'int', text: "Import Year", width: '7%' },
            { name: 'ReviewReason', type: 'string', text: "Review Reason", width: '20%' },
            { name: 'Remarks', type: 'string', hidden: true },
            { name: 'Scenario', type: 'string', hidden: true }
        ],

        ready: function () {
            var rowCount = $("#tblApprovalTable").jqxGrid('getrows').length;
            $("#TableApprovalImport").text('Row count: ' + rowCount);

            $('#tblApprovalTable').on('rowdoubleclick', function (event) {
                if (typeof event.args.group == "undefined") {
                    //_showLoadingFullPage();
                    var selectedRow;
                    selectedRow = $.jqxGridApi.getOneSelectedRow("#tblApprovalTable", true);
                    generateForm(selectedRow, "Approving");
                }
            });
            _hideLoadingFullPage();
        }
    });
};

function loadExportTable() {
    $.jqxGridApi.create({
        showTo: "#tblExportTable",
        options: {
            //for comments or descriptions
            height: "400",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_VF_LT_TABLE_EXPORT]",
            Params: [
            { Name: "@AppID", Value: "ELA" }
            ]
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'ELAWorkingID', type: 'string', hidden: true },
            { name: 'LVID', type: 'string', text: "LVID", width: '7%' },
            { name: 'CustomerGOC', type: 'string', text: "Costumer GOC", width: '10%' },
            { name: 'TaxDue', type: 'string', text: "Tax Due", width: '7%' },
            { name: 'ActualCost', type: 'string', text: "Actual Cost", width: '7%' },
            { name: 'City', type: 'string', text: "City", width: '13%' },
            { name: 'State', type: 'string', text: "State", width: '5%' },
            { name: 'ZipCode', type: 'string', text: "Zip Code", width: '7%' },
            { name: 'GOC_EXPCODE', type: 'string', hidden: true },
            { name: 'IsTaxIncluded', type: 'float', hidden: true },
            { name: 'InvoiceDescription', type: 'string', hidden: true },
            { name: 'Imp', type: 'string', hidden: true },
            { name: 'ImportMonth', type: 'int', text: "Import Month", width: '7%' },
            { name: 'ImportYear', type: 'int', text: "Import Year", width: '7%' },
            { name: 'ReviewReason', type: 'string', text: "Review Reason", width: '20%' },
            { name: 'Remarks', type: 'string', hidden: true },
            { name: 'Scenario', type: 'string', hidden: true }
        ],

        ready: function () {
            var rowCount = $("#tblExportTable").jqxGrid('getrows').length;
            $("#TableExportImport").text('Row count: ' + rowCount);
            _hideLoadingFullPage();
        }
    });
};

function loadNonExportTable() {
    $.jqxGridApi.create({
        showTo: "#tblNonExportTable",
        options: {
            //for comments or descriptions
            height: "400",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_VF_LT_TABLE_NON_EXPORT]",
            Params: [
            { Name: "@AppID", Value: "ELA" }
            ]
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'ELAWorkingID', type: 'string', hidden: true },
            { name: 'LVID', type: 'string', text: "LVID", width: '7%' },
            { name: 'CustomerGOC', type: 'string', text: "Costumer GOC", width: '10%' },
            { name: 'TaxDue', type: 'string', text: "Tax Due", width: '7%' },
            { name: 'ActualCost', type: 'string', text: "Actual Cost", width: '7%' },
            { name: 'City', type: 'string', text: "City", width: '13%' },
            { name: 'State', type: 'string', text: "State", width: '5%' },
            { name: 'ZipCode', type: 'string', text: "Zip Code", width: '7%' },
            { name: 'GOC_EXPCODE', type: 'string', hidden: true },
            { name: 'IsTaxIncluded', type: 'float', hidden: true },
            { name: 'InvoiceDescription', type: 'string', hidden: true },
            { name: 'Imp', type: 'string', hidden: true },
            { name: 'ImportMonth', type: 'int', text: "Import Month", width: '7%' },
            { name: 'ImportYear', type: 'int', text: "Import Year", width: '7%' },
            { name: 'ReviewReason', type: 'string', text: "Review Reason", width: '20%' },
            { name: 'Remarks', type: 'string', hidden: true },
            { name: 'Scenario', type: 'string', hidden: true }
        ],

        ready: function () {
            var rowCount = $("#tblNonExportTable").jqxGrid('getrows').length;
            $("#TableNonExportImport").text('Row count: ' + rowCount);
            _hideLoadingFullPage();
        }
    });
};

function generateForm(selectedRow, table) {
    var InvID, sp, actionButtons;
    InvID = selectedRow.ELAWorkingID;

    var htmlForm =
    '    <div class="content-body">                                                                        ' +
    '        <div class="row" style="margin-top:10px;">                                                    ' +
    '            <div class="col-lg-12">                                                                   ' +
    '                <div class="input-group-lg compactModal col-lg-12">                                    ' +
    '                   <label class="control-label">LVID</label>                                          ' +
    '                   <select class="form-control selectpicker" id="drop_LVID"></select>                 ' +
    '                </div>                                                                                ' +                                                                        
    '            </div>                                                                                    ' +
    '        </div>                                                                                        ' +
    '        <div class="row" style="margin-top:10px;">                                                    ' +
    '            <div class="col-lg-12">                                                                   ' +
    '                <div class="col-lg-6">                                                                ' +
    '                    <div class="input-group">                                                         ' +
    '                        <span class="input-group-addon">Customer GOC</span>                         ' +
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.CustomerGOC + '">' +
    '                    </div>                                                                            ' +
    '                </div>                                                                                ' +
    '                <div class="col-lg-6">                                                                ' +
    '                    <div class="input-group">                                                         ' +
    '                        <span class="input-group-addon">Tax Due</span>                           ' +
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.TaxDue + '">' +
    '                    </div>                                                                            ' +
    '                </div>                                                                                ' +
    '                                                                                                      ' +
    '            </div>                                                                                    ' +
    '        </div>                                                                                        ' +
    '        <div class="row" style="margin-top:10px;">                                                    ' +
    '            <div class="col-lg-12">                                                                   ' +
    '                <div class="col-lg-6">                                                                ' +
    '                    <div class="input-group">                                                         ' +
    '                        <span class="input-group-addon">City</span>                         ' +
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.City + '">' +
    '                    </div>                                                                            ' +
    '                </div>                                                                                ' +
    '                <div class="col-lg-6">                                                                ' +
    '                    <div class="input-group">                                                         ' +
    '                        <span class="input-group-addon">State</span>                         ' +
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.State + '">' +
    '                    </div>                                                                            ' +
    '                </div>                                                                                ' +
    '            </div>                                                                                    ' +
    '        </div>                                                                                        ' +
    '        <div class="row" style="margin-top:10px;">                                                    ' +
    '            <div class="col-lg-12">                                                                   ' +
    '                <div class="col-lg-6">                                                                ' +
    '                    <div class="input-group">                                                         ' +
    '                        <span class="input-group-addon">Country</span>                            ' +
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.Country + '">' +
    '                    </div>                                                                            ' +
    '                </div>                                                                                ' +
    '                <div class="col-lg-6">                                                                ' +
    '                    <div class="input-group">                                                         ' +
    '                        <span class="input-group-addon">Zip Code</span>                           ' +
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.ZipCode + '">' +
    '                    </div>                                                                            ' +
    '                </div>                                                                                ' +
    '            </div>                                                                                    ' +
    '        </div>                                                                                        ' +
    '        <div class="row" style="margin-top:10px;">                                                    ' +
    '            <div class="col-lg-12">                                                                   ' +
    '                <div class="col-lg-6">                                                                ' +
    '                    <div class="input-group">                                                         ' +
    '                        <span class="input-group-addon">GOC Expense Code</span>                            ' +
    '                        <input id="txtGOC_Expense" type="text" class="form-control pull-right" value = "' + selectedRow.GOC_EXPCODE + '">' +
    '                    </div>                                                                            ' +
    '                </div>                                                                                ' +
    '                <div class="col-lg-6">                                                                ' +
    '                    <div class="input-group">                                                         ' +
    '                        <span class="input-group-addon">Is Tax included</span>                                   ' +
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.IsTaxIncluded + '">' +
    '                    </div>                                                                            ' +
    '                </div>                                                                                ' +
    '            </div>                                                                                    ' +
    '        </div>                                                                                        ' +
    '        <div class="row" style="margin-top:10px;">                                                    ' +
    '            <div class="col-lg-12">                                                                   ' +
    '                <div class="col-lg-6">                                                                ' +
    '                    <div class="input-group">                                                         ' +
    '                        <span class="input-group-addon">Reason for Review</span>                                ' +
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.ReviewReason + '">' +
    '                    </div>                                                                            ' +
    '                </div>                                                                                ' +
    '                <div class="col-lg-6">                                                                ' +
    '                    <div class="input-group">                                                         ' +
    '                        <span class="input-group-addon">Invoice Description</span>                                    ' +
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.InvoiceDescription + '">' +
    '                    </div>                                                                            ' +
    '                </div>                                                                                ' +
    '            </div>                                                                                    ' +
    '        </div>                                                                                        ' +
    '        <div class="row" style="margin-top:10px;">                                                    ' +
    '            <div class="col-lg-12">                                                                   ' +
    '                <div class="col-lg-6">                                                                ' +
    '                    <div class="input-group">                                                         ' +
    '                        <span class="input-group-addon">Remarks</span>                              ' +
    '                        <input id="txtRemarks" type="text" class="form-control pull-right" value = "' + selectedRow.Remarks + '">' +
    '                    </div>                                                                            ' +
    '                </div>                                                                                ' +
    '            </div>                                                                                    ' +
    '        </div>                                                                                        ' +                                                                                                                                                                             
    '        <div class="row" style="margin-top:10px;">                                                    ' +
    '            <div class="input-group-lg compactModal col-lg-2">                                        ' +
    '                <label class="control-label">Import?</label>                                          ' +
    '                <select class="form-control selectpicker" id="drop_Import">                           ' +
    '                    <option value="R">R</option>                                                      ' +
    '                    <option value="Y">Y</option>                                                      ' +
    '                    <option value="N">N</option>                                                      ' +
    '                 </select>                                                                            ' +
    '            </div>                                                                                    ' +                                                                             
    '        </div>                                                                                        ' +
    '    </div>                                                                                            ';

    if (table == "Working") {

        var actionButtons = [{
            name: "Save",
            class: "btn-success",
            closeModalOnClick: true,
            onClick: function ($modal) {
                spparams = [
                    { Name: "@ELAWorkingID", Value: InvID },
                    { Name: "@LVID", Value: $modal.find("#drop_LVID :selected").val() },
                    { Name: "@DestTable", Value: $modal.find("#drop_Import :selected").text() },
                    { Name: "@GOCExpenseCode", Value: $modal.find("#txtGOC_Expense").val() },
                    { Name: "@Remarks", Value: $modal.find("#txtRemarks").val() }
                ];
                updatePendingLines('[dbo].[NSP_VF_AC_UPDATE_WORKING_ELA]', spparams);
            }
        }]
    } else {
        var actionButtons = [{
            name: "Working",
            class: "btn-warning",
            closeModalOnClick: true,
            onClick: function ($modal) {
                spparams = [
                    { Name: "@ELAWorkingID", Value: InvID },
                    { Name: "@DestinationTable", Value: 'R' }
                ];
                updatePendingLines('[dbo].[NSP_VF_AC_UPDATE_APPROVING_ELA]', spparams);
                loadWorkingTable();
            }
        }, {
            name: "Export",
            class: "btn-success",
            closeModalOnClick: true,
            onClick: function ($modal) {
                spparams = [
                    { Name: "@ELAWorkingID", Value: InvID },
                    { Name: "@DestinationTable", Value: 'E' }
                ];
                updatePendingLines('[dbo].[NSP_VF_AC_UPDATE_APPROVING_ELA]', spparams);
                loadExportTable();
            }
        }, {
            name: "Non Export",
            class: "btn-danger",
            closeModalOnClick: true,
            onClick: function ($modal) {
                spparams = [
                    { Name: "@ELAWorkingID", Value: InvID },
                    { Name: "@DestinationTable", Value: 'N' }
                ];
                updatePendingLines('[dbo].[NSP_VF_AC_UPDATE_APPROVING_ELA]', spparams);
                loadNonExportTable();
            }
        }]
    }

    _showModal({
        width: "75%",
        title: "ELA Item Line",
        contentHtml: htmlForm,
        buttons: actionButtons,
        addCloseButton: true,
        onReady: function ($modal) {
            var qry;
            qry = " SELECT '0' AS [id],	'-- Select --' AS [text] UNION SELECT [LVID] [id], [LVID] +' - '+ [LegalEntityName] [text] FROM [dbo].[tblVertex_LVID]"
            if (table == "Working") {
                if (selectedRow.LVID == "") {
                    loadDropDowns2(qry, "0", $modal.find("#drop_LVID"),table);
                } else {
                    loadDropDowns2(qry, selectedRow.LVID, $modal.find("#drop_LVID"));
                }
            } else {
                loadDropDowns2(qry, selectedRow.LVID, $modal.find("#drop_LVID"));
                $modal.find("#drop_LVID").prop('disabled', true);
                $modal.find("#txtGOC_Expense").prop('disabled', true);
                $modal.find("#txtRemarks").prop('disabled', true);
                $modal.find("#drop_Import").prop('disabled', true);
            }
        }
    });
}

function updatePendingLines(sp, params) {
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Updating Line..",
        name: sp,
        params: params,
        success: {
            fn: function (responseList) {
                //_showAlert({
                //    type: "success",
                //    title: "Message",
                //    content: "Line Updated"
                //});
                _showNotification("success", "Line Updated");
                loadWorkingTable();
                loadApprovingTable();
            }
        }
    });
}

function downloadTables(sp, tblName) {
    _downloadExcel({
        spName: sp,
        spParams: [{ Name: "@AppID", Value: "ELA" }],
        filename: "ELA_Table_" + tblName + "_" + Date.now(),
        success: {
            msg: "Please wait, generating Excel..."
        }
    });
}

$("#btnjournal").click(function () {
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Validating Journal Generation..",
        name: "[dbo].[NSP_VF_JOURNAL_VALIDATION]",
        params: [{ Name: '@AppID', Value: 'ELA' }],
        success: {
            fn: function (responseList) {
                if (responseList[0].Value == 1) {
                    _showNotification("error", responseList[0].Message);
                } else {
                    var messagelog = '';
                    if (responseList[1].Value == 1) {
                        messageLog = responseList[1].Message;
                    } else {
                        messageLog = "Generate New Journal Entry";
                    }
                    var htmlContentModal = "<b>" + messageLog + "<br/>";
                    _showModal({
                        width: '35%',
                        modalId: "modalDel",
                        addCloseButton: true,
                        buttons: [{
                            name: "Yes",
                            class: "btn-danger",
                            onClick: function () {
                                _callProcedure({
                                    loadingMsgType: "fullLoading",
                                    loadingMsg: "Generating Journal..",
                                    name: "[dbo].[NSP_VF_JOURNAL_GENERATION]",
                                    params: [{ Name: "@overWrite", Value: responseList[1].Value },
                                             { Name: "@AppID", Value: 'ELA' }],
                                    success: {
                                        fn: function (responseList) {

                                            _showAlert({
                                                type: "success",
                                                title: "Message",
                                                content: "Journal Generated"
                                            });

                                            _downloadExcel({
                                                spName: "[dbo].[NSP_VF_LT_TOTAL_NEW] ",
                                                spParams: [{ Name: "@AppID", Value: 'ELA' }],
                                                filename: "ELA_Journal_" + responseList[0].Period,
                                                pathToSave: "\\\\gcodfs\\GCO_Data_Grp\\ART\\Tax\\_Sales & Use Tax\\GRB & ELA\\ELA\\ELA VertexFeeds\\",
                                                success: {
                                                    msg: "Please wait, Downloading Journal..."
                                                }
                                            });
                                            loadImportLogTable();
                                            var qry = "SELECT '0' AS [id],	'-- Period --' AS [text] UNION SELECT CONVERT(VARCHAR(50),[ImportMonth]) + '-'+ CONVERT(VARCHAR(50),[ImportYear]) AS [id], CONVERT(VARCHAR(50),[ImportMonth]) + '-' + CONVERT(VARCHAR(50),[ImportYear]) AS [text] FROM [SAU].[dbo].[tblVertex_HistoryExportELA] GROUP BY CONVERT(VARCHAR(50),[ImportMonth]) + '-'+ CONVERT(VARCHAR(50),[ImportYear]), CONVERT(VARCHAR(50),[ImportMonth]) + '-'+ CONVERT(VARCHAR(50),[ImportYear])"
                                            loadDropDowns2(qry, "0", "drop_Periods")
                                        }
                                    }
                                });
                            }
                        }],
                        title: "Are you sure you want to generate this Journal?",
                        contentHtml: htmlContentModal
                    });
                }
            }
        }
    });
});

$("#btnWorkingTable_Download").click(function () {
    downloadTables("[dbo].[NSP_VF_LT_TABLE_WORKING]", "Working");
});

$("#btnApprovalTable_Download").click(function () {
    downloadTables("[dbo].[NSP_VF_LT_TABLE_APPROVING]", "Approving");
});

$("#btnExportTable_Download").click(function () {
    downloadTables("[dbo].[NSP_VF_LT_TABLE_EXPORT]", "Export");
});

$("#btnNonExportTable_Download").click(function () {
    downloadTables("[dbo].[NSP_VF_LT_TABLE_NON_EXPORT]", "NonExport");
});

$("#btnDownImport").click(function () {
    if ($("#drop_Periods :selected").text() != "-- Period --") {
        var period = $("#drop_Periods :selected").text().split("-")
        _downloadExcel({
            spName: "[dbo].[NSP_VF_LT_DOWNLOAD_IMPORTED]",
            spParams: [
                { Name: "@ImportMonth", Value: period[0] },
                { Name: "@ImportYear", Value: period[1] },
                { Name: "@AppID", Value: 'ELA' }
            ],
            filename: "DownLoad_ELA_Import_" + $("#drop_Periods :selected").text(),
            success: {
                msg: "Please wait, generating Excel..."
            }
        });
    } else {
        _showNotification("error", "Select a period");
    }
    //downloadTables("[dbo].[NSP_VF_LT_NON_EXPORT_TABLE]", "NonExport");
});

$("#btnDownJournal").click(function () {
    if ($("#drop_Periods :selected").text() != "-- Period --") {
        var period = $("#drop_Periods :selected").text().split("-")
        _downloadExcel({
            spName: "[dbo].[NSP_VF_LT_DOWNLOAD_TOTAL_NEW]",
            spParams: [
                { Name: "@ImportMonth", Value: period[0] },
                { Name: "@ImportYear", Value: period[1] },
                { Name: "@AppID", Value: 'ELA' }
            ],
            filename: "DownLoad_Journal_" + $("#drop_Periods :selected").text(),
            success: {
                msg: "Please wait, generating Excel..."
            }
        });
    } else {
        _showNotification("error", "Select a period");
    }
    //downloadTables("[dbo].[NSP_VF_LT_NON_EXPORT_TABLE]", "NonExport");
});

$("#btnVertexFile").click(function () {
    downloadTables("[dbo].[NSP_VF_LT_DOWNLOAD_VERTEX_FILE]", "VertexFile");
});