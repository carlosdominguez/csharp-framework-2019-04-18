﻿/// <reference path="../../Shared/plugins/util/global.js" />

$(document).ready(function () {
    //loadFileUploader();
    loadWorkingTable();
    loadApprovingTable()
    loadExportTable();
    loadNonExportTable();
    loadImportLogTable();
    initUpload();
    var qry = "SELECT '0' AS [id],	'-- Period --' AS [text] UNION SELECT CONVERT(VARCHAR(50),[ImportMonth]) + '-'+ CONVERT(VARCHAR(50),[ImportYear]) AS [id], CONVERT(VARCHAR(50),[ImportMonth]) + '-' + CONVERT(VARCHAR(50),[ImportYear]) AS [text] FROM [SAU].[dbo].[tblVertex_HistoryExport] GROUP BY CONVERT(VARCHAR(50),[ImportMonth]) + '-'+ CONVERT(VARCHAR(50),[ImportYear]), CONVERT(VARCHAR(50),[ImportMonth]) + '-'+ CONVERT(VARCHAR(50),[ImportYear])"
    loadDropDowns2(qry, "0", "drop_Periods")
});

//LOAD METHODS
function loadImportLogTable() {
    $.jqxGridApi.create({
        showTo: "#tblImportTable",
        options: {
            //for comments or descriptions
            height: "400",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_VF_LT_TABLE_IMPORT]",
            Params: [
            { Name: "@AppID", Value: "P2P" }
            ]
        },
        
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'ImportID', type: 'int', text: "Import ID", hidden: true },
            { name: 'FileName', type: 'string', text: "File Name", width: '40%' },
            { name: 'FileLines', type: 'int', text: "File # Lines", width: '10%' },
            { name: 'ImportLines', type: 'int', text: "Imported # Lines", width: '13%' },
            { name: 'Month', type: 'int', text: "Month", width: '5%' },
            { name: 'Year', type: 'int', text: "Year", width: '5%' },
            { name: 'JournalGeneration', type: 'int', text: "Journal", width: '7%' },
            { name: 'ResponsableUser', type: 'string', text: "Responsable", width: '10%' },
            { name: 'DateStamp', type: 'datetime', text: "Import Date", width: '10%' }
        ],

        ready: function () {
            var rowCount = $("#tblImportTable").jqxGrid('getrows').length;
            $("#TableImport").text('Row count: ' + rowCount);
            _hideLoadingFullPage();
        }
    });
};

function loadWorkingTable() {
    $.jqxGridApi.create({
        showTo: "#tblWorkingTable",
        options: {
            //for comments or descriptions
            height: "400",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_VF_LT_TABLE_WORKING]",
            Params: [
                { Name: "@AppID", Value: "P2P" }
            ]
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'P2PWorkingID', type: 'string', hidden: true },
            { name: 'InvoiceID', type: 'string', hidden: true },
            { name: 'VendorNumber', type: 'int', text: "Vendor #", width: '7%' },
            { name: 'VendorName', type: 'string', text: "Vendor Name", width: '15%' },
            { name: 'InvoiceNumber', type: 'int', text: "Invoice #", width: '7%' },
            { name: 'InvoiceDate', type: 'date', text: "InvoiceDate", width: '7%', cellsformat: "M/d/yyyy"},
            { name: 'InvoiceAmount', type: 'float', text: "InvoiceAmount", width: '10%' },
            { name: 'CommodityName', type: 'string', hidden:true },
            { name: 'VertexCity', type: 'string', text: "City", width: '5%' },
            { name: 'VertexState', type: 'string', text: "State", width: '5%' },
            { name: 'VertexRateFromCode', type: 'float', hidden:true},
            { name: 'LVID', type: 'string', text: "LVID", width: '5%' },
            { name: 'Account', type: 'string', text: "Account", width: '10%' },
            { name: 'GOC', type: 'string', text: "GOC", width: '10%' },
            { name: 'LineType', type: 'string', hidden:true},
            { name: 'DistributionAmount', type: 'float', text: "Dist Amount", width: '10%' },
            { name: 'PONumber', type: 'string', hidden: true },
            { name: 'RefLineNumber', type: 'string', hidden: true },
            { name: 'CompanyName', type: 'string', hidden: true },
            { name: 'TaxDue', type: 'float', text: "TaxDue", width: '10%' },
            { name: 'AccountType', type: 'string', hidden: true },
            { name: 'InvoiceDistributionDescription', type: 'string', hidden: true },
            { name: 'Accrue', type: 'string', hidden: true },
            { name: 'AccrueTaxReason', type: 'string', hidden: true },
            { name: 'Import', type: 'string', hidden: true },
            { name: 'ImportReason', type: 'string', hidden: true }
        ],

        ready: function () {
            var rowCount = $("#tblWorkingTable").jqxGrid('getrows').length;
            $("#TableWorkingImport").text('Row count: ' + rowCount);

            $('#tblWorkingTable').on('rowdoubleclick', function (event) {
                if (typeof event.args.group == "undefined") {
                    //_showLoadingFullPage();
                    var selectedRow;
                    selectedRow = $.jqxGridApi.getOneSelectedRow("#tblWorkingTable", true);
                    generateForm(selectedRow, "Working");
                }
            });

            _hideLoadingFullPage();
        }
    });
};

function loadApprovingTable() {
    $.jqxGridApi.create({
        showTo: "#tblApprovalTable",
        options: {
            //for comments or descriptions
            height: "400",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_VF_LT_TABLE_APPROVING]",
            Params: [
            { Name: "@AppID", Value: "P2P" }
            ]
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'P2PWorkingID', type: 'string', hidden: true },
            { name: 'InvoiceID', type: 'string', hidden: true },
            { name: 'VendorNumber', type: 'int', text: "Vendor #", width: '7%' },
            { name: 'VendorName', type: 'string', text: "Vendor Name", width: '15%' },
            { name: 'InvoiceNumber', type: 'int', text: "Invoice #", width: '7%' },
            { name: 'InvoiceDate', type: 'date', text: "InvoiceDate", width: '7%', cellsformat: "M/d/yyyy" },
            { name: 'InvoiceAmount', type: 'float', text: "InvoiceAmount", width: '10%' },
            { name: 'CommodityName', type: 'string', hidden: true },
            { name: 'VertexCity', type: 'string', text: "City", width: '5%' },
            { name: 'VertexState', type: 'string', text: "State", width: '5%' },
            { name: 'VertexRateFromCode', type: 'float', hidden: true },
            { name: 'LVID', type: 'string', text: "LVID", width: '5%' },
            { name: 'Account', type: 'string', text: "Account", width: '10%' },
            { name: 'GOC', type: 'string', text: "GOC", width: '10%' },
            { name: 'LineType', type: 'string', hidden: true },
            { name: 'DistributionAmount', type: 'float', text: "Dist Amount", width: '10%' },
            { name: 'PONumber', type: 'string', hidden: true },
            { name: 'RefLineNumber', type: 'string', hidden: true },
            { name: 'CompanyName', type: 'string', hidden: true },
            { name: 'TaxDue', type: 'float', text: "TaxDue", width: '10%' },
            { name: 'AccountType', type: 'string', hidden: true },
            { name: 'InvoiceDistributionDescription', type: 'string', hidden: true },
            { name: 'Accrue', type: 'string', hidden: true },
            { name: 'AccrueTaxReason', type: 'string', hidden: true },
            { name: 'Import', type: 'string', hidden: true },
            { name: 'ImportReason', type: 'string', hidden: true }
        ],

        ready: function () {
            var rowCount = $("#tblApprovalTable").jqxGrid('getrows').length;
            $("#TableApprovalImport").text('Row count: ' + rowCount);

            $('#tblApprovalTable').on('rowdoubleclick', function (event) {
                if (typeof event.args.group == "undefined") {
                    //_showLoadingFullPage();
                    var selectedRow;
                    selectedRow = $.jqxGridApi.getOneSelectedRow("#tblApprovalTable", true);
                    generateForm(selectedRow, "Approving");
                }
            });
            _hideLoadingFullPage();
        }
    });
};

function loadExportTable() {
    $.jqxGridApi.create({
        showTo: "#tblExportTable",
        options: {
            //for comments or descriptions
            height: "400",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_VF_LT_TABLE_EXPORT]",
            Params: [
            { Name: "@AppID", Value: "P2P" }
            ]
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'P2PWorkingID', type: 'string', hidden: true },
            { name: 'InvoiceID', type: 'string', hidden: true },
            { name: 'VendorNumber', type: 'int', text: "Vendor #", width: '7%' },
            { name: 'VendorName', type: 'string', text: "Vendor Name", width: '15%' },
            { name: 'InvoiceNumber', type: 'int', text: "Invoice #", width: '7%' },
            { name: 'InvoiceDate', type: 'date', text: "InvoiceDate", width: '7%', cellsformat: "M/d/yyyy" },
            { name: 'InvoiceAmount', type: 'float', text: "InvoiceAmount", width: '10%' },
            { name: 'CommodityName', type: 'string', hidden: true },
            { name: 'VertexCity', type: 'string', text: "City", width: '5%' },
            { name: 'VertexState', type: 'string', text: "State", width: '5%' },
            { name: 'VertexRateFromCode', type: 'float', hidden: true },
            { name: 'LVID', type: 'string', text: "LVID", width: '5%' },
            { name: 'Account', type: 'string', text: "Account", width: '10%' },
            { name: 'GOC', type: 'string', text: "GOC", width: '10%' },
            { name: 'LineType', type: 'string', hidden: true },
            { name: 'DistributionAmount', type: 'float', text: "Dist Amount", width: '10%' },
            { name: 'PONumber', type: 'string', hidden: true },
            { name: 'RefLineNumber', type: 'string', hidden: true },
            { name: 'CompanyName', type: 'string', hidden: true },
            { name: 'TaxDue', type: 'float', text: "TaxDue", width: '10%' },
            { name: 'AccountType', type: 'string', hidden: true },
            { name: 'InvoiceDistributionDescription', type: 'string', hidden: true },
            { name: 'Accrue', type: 'string', hidden: true },
            { name: 'AccrueTaxReason', type: 'string', hidden: true },
            { name: 'Import', type: 'string', hidden: true },
            { name: 'ImportReason', type: 'string', hidden: true }
        ],

        ready: function () {
            var rowCount = $("#tblExportTable").jqxGrid('getrows').length;
            $("#TableExportImport").text('Row count: ' + rowCount);
            _hideLoadingFullPage();
        }
    });
};

function loadNonExportTable() {
    $.jqxGridApi.create({
        showTo: "#tblNonExportTable",
        options: {
            //for comments or descriptions
            height: "400",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_VF_LT_TABLE_NON_EXPORT]",
            Params: [
            { Name: "@AppID", Value: "P2P" }
            ]
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'P2PWorkingID', type: 'string', hidden: true },
            { name: 'InvoiceID', type: 'string', hidden: true },
            { name: 'VendorNumber', type: 'int', text: "Vendor #", width: '7%' },
            { name: 'VendorName', type: 'string', text: "Vendor Name", width: '15%' },
            { name: 'InvoiceNumber', type: 'int', text: "Invoice #", width: '7%' },
            { name: 'InvoiceDate', type: 'date', text: "InvoiceDate", width: '7%', cellsformat: "M/d/yyyy" },
            { name: 'InvoiceAmount', type: 'float', text: "InvoiceAmount", width: '10%' },
            { name: 'CommodityName', type: 'string', hidden: true },
            { name: 'VertexCity', type: 'string', text: "City", width: '5%' },
            { name: 'VertexState', type: 'string', text: "State", width: '5%' },
            { name: 'VertexRateFromCode', type: 'float', hidden: true },
            { name: 'LVID', type: 'string', text: "LVID", width: '5%' },
            { name: 'Account', type: 'string', text: "Account", width: '10%' },
            { name: 'GOC', type: 'string', text: "GOC", width: '10%' },
            { name: 'LineType', type: 'string', hidden: true },
            { name: 'DistributionAmount', type: 'float', text: "Dist Amount", width: '10%' },
            { name: 'PONumber', type: 'string', hidden: true },
            { name: 'RefLineNumber', type: 'string', hidden: true },
            { name: 'CompanyName', type: 'string', hidden: true },
            { name: 'TaxDue', type: 'float', text: "TaxDue", width: '10%' },
            { name: 'AccountType', type: 'string', hidden: true },
            { name: 'InvoiceDistributionDescription', type: 'string', hidden: true },
            { name: 'Accrue', type: 'string', hidden: true },
            { name: 'AccrueTaxReason', type: 'string', hidden: true },
            { name: 'Import', type: 'string', hidden: true },
            { name: 'ImportReason', type: 'string', hidden: true }
        ],

        ready: function () {
            var rowCount = $("#tblNonExportTable").jqxGrid('getrows').length;
            $("#TableNonExportImport").text('Row count: ' + rowCount);
            _hideLoadingFullPage();
        }
    });
};

function initUpload() {
    var fnClickUpload = function () {
        if ($("#txt_ImportMonth").val() == "" || $("#txt_ImportYear").val() == "") {
            _showNotification("error", "Empty field found. Please select all the necessary details.");
        } else {
            $('#fine-uploader-manual-trigger').fineUploader('setParams', {
                'ImpMonth': $("#txt_ImportMonth").val(),
                'ImpYear': $("#txt_ImportYear").val()
            });

            $('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
        }
    };

    $('#fine-uploader-manual-trigger').fineUploader({
        template: 'qq-template-manual-trigger',
        request: {
            endpoint: '/SalesAndUse/UploadVertex'
        },
        thumbnails: {
            placeholders: {
                waitingPath: '/Content/Shared/plugins/fine-uploader/placeholders/waiting-generic.png',
                notAvailablePath: '/Content/Shared/plugins/fine-uploader/placeholders/not_available-generic.png'
            }
        },
        callbacks: {
            onComplete: function (id, name, resonseJSON, xhr) {
                console.log(id, name, resonseJSON, xhr)
            },
            onAllComplete: function (succeeded, failed) {
                //console.log(succeeded, failed, this);
                if (failed.length == 0) {
                    _showAlert({
                        type: "success",
                        title: "Message",
                        content: "The file was uploaded successfully"
                    });
                    $("#txt_ImportMonth").val("");
                    $("#txt_ImportYear").val("");
                    $('#fine-uploader-manual-trigger').fineUploader('reset');

                    $('#trigger-upload').click(fnClickUpload);
                    //$('section.box .actions .box_toggle').click();
                    loadWorkingTable();
                    loadExportTable();
                    loadNonExportTable();
                    loadImportLogTable();
                    loadApprovingTable();
                }
            },
            onError: function (id, name, errorReason, xhrOrXdr) {
                if (name) {
                    _showDetailAlert({
                        title: "Message",
                        shortMsg: "An error ocurred with the file '" + name + "'.",
                        longMsg: errorReason,
                        type: "Error",
                        viewLabel: "View Details"
                    });
                }
            }
        },
        validation: {
            allowedExtensions: ['xls','xlsx'],
            itemLimit: 1
        },
        autoUpload: false
    });
    $('#trigger-upload').click(fnClickUpload);
}

function generateForm(selectedRow, table) {
    var InvID,sp,actionButtons;
    InvID = selectedRow.P2PWorkingID;

    var htmlForm =                                                                                                                                                                       
    '    <div class="content-body">                                                                        '+
    '        <div class="row" style="margin-top:10px;">                                                    '+
    '            <div class="col-lg-12">                                                                   '+
    '                <div class="col-lg-6">                                                                '+
    '                    <div class="input-group">                                                         '+
    '                        <span class="input-group-addon">Vendor Name</span>                            '+
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.VendorName + '">'+
    '                    </div>                                                                            '+
    '                </div>                                                                                '+
    '                <div class="col-lg-6">                                                                '+
    '                    <div class="input-group">                                                         '+
    '                        <span class="input-group-addon">Vendor Number</span>                          '+
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.VendorNumber + '">' +
    '                    </div>                                                                            '+
    '                </div>                                                                                '+
    '            </div>                                                                                    '+
    '        </div>                                                                                        '+
    '        <div class="row" style="margin-top:10px;">                                                    '+
    '            <div class="col-lg-12">                                                                   '+
    '                <div class="col-lg-6">                                                                '+
    '                    <div class="input-group">                                                         '+
    '                        <span class="input-group-addon">Invoice Number</span>                         '+
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.InvoiceNumber + '">' +
    '                    </div>                                                                            '+
    '                </div>                                                                                '+
    '                <div class="col-lg-6">                                                                '+
    '                    <div class="input-group">                                                         '+
    '                        <span class="input-group-addon">Invoice Date</span>                           '+
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.InvoiceDate + '">' +
    '                    </div>                                                                            '+
    '                </div>                                                                                '+
    '                                                                                                      '+
    '            </div>                                                                                    '+
    '        </div>                                                                                        '+
    '        <div class="row" style="margin-top:10px;">                                                    '+
    '            <div class="col-lg-12">                                                                   '+
    '                <div class="col-lg-6">                                                                '+
    '                    <div class="input-group">                                                         '+
    '                        <span class="input-group-addon">Invoice Amount</span>                         '+
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.InvoiceAmount + '">' +
    '                    </div>                                                                            '+
    '                </div>                                                                                '+
    '                <div class="col-lg-6">                                                                '+
    '                    <div class="input-group">                                                         '+
    '                        <span class="input-group-addon">Commodity Name</span>                         '+
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.CommodityName + '">' +
    '                    </div>                                                                            '+
    '                </div>                                                                                '+
    '            </div>                                                                                    '+
    '        </div>                                                                                        '+
    '        <div class="row" style="margin-top:10px;">                                                    '+
    '            <div class="col-lg-12">                                                                   '+
    '                <div class="col-lg-6">                                                                '+
    '                    <div class="input-group">                                                         '+
    '                        <span class="input-group-addon">Vertex City</span>                            '+
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.VertexCity + '">' +
    '                    </div>                                                                            '+
    '                </div>                                                                                '+
    '                <div class="col-lg-6">                                                                '+
    '                    <div class="input-group">                                                         '+
    '                        <span class="input-group-addon">Vertex State</span>                           '+
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.VertexState + '">' +
    '                    </div>                                                                            '+
    '                </div>                                                                                '+
    '            </div>                                                                                    '+
    '        </div>                                                                                        '+
    '        <div class="row" style="margin-top:10px;">                                                    '+
    '            <div class="col-lg-12">                                                                   '+
    '                <div class="col-lg-6">                                                                '+
    '                    <div class="input-group">                                                         '+
    '                        <span class="input-group-addon">Vertex Rate</span>                            '+
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.VertexRateFromCode + '">' +
    '                    </div>                                                                            '+
    '                </div>                                                                                '+
    '                <div class="col-lg-6">                                                                '+
    '                    <div class="input-group">                                                         '+
    '                        <span class="input-group-addon">LVID</span>                                   '+
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.LVID + '">' +
    '                    </div>                                                                            '+
    '                </div>                                                                                '+
    '            </div>                                                                                    '+
    '        </div>                                                                                        '+
    '        <div class="row" style="margin-top:10px;">                                                    '+
    '            <div class="col-lg-12">                                                                   '+
    '                <div class="col-lg-6">                                                                '+
    '                    <div class="input-group">                                                         '+
    '                        <span class="input-group-addon">Account</span>                                '+
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.Account + '">' +
    '                    </div>                                                                            '+
    '                </div>                                                                                '+
    '                <div class="col-lg-6">                                                                '+
    '                    <div class="input-group">                                                         '+
    '                        <span class="input-group-addon">GOC</span>                                    '+
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.GOC + '">' +
    '                    </div>                                                                            '+
    '                </div>                                                                                '+
    '            </div>                                                                                    '+
    '        </div>                                                                                        '+
    '        <div class="row" style="margin-top:10px;">                                                    '+
    '            <div class="col-lg-12">                                                                   '+
    '                <div class="col-lg-6">                                                                '+
    '                    <div class="input-group">                                                         '+
    '                        <span class="input-group-addon">Line Type</span>                              '+
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.LineType + '">' +
    '                    </div>                                                                            '+
    '                </div>                                                                                '+
    '                <div class="col-lg-6">                                                                '+
    '                    <div class="input-group">                                                         '+
    '                        <span class="input-group-addon">Distribution Amount</span>                    '+
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.DistributionAmount + '">' +
    '                    </div>                                                                            '+
    '                </div>                                                                                '+
    '            </div>                                                                                    '+
    '        </div>                                                                                        '+
    '        <div class="row" style="margin-top:10px;">                                                    '+
    '            <div class="col-lg-12">                                                                   '+
    '                <div class="col-lg-6">                                                                '+
    '                    <div class="input-group">                                                         '+
    '                        <span class="input-group-addon">PO Number</span>                              '+
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.PONumber + '">' +
    '                    </div>                                                                            '+
    '                </div>                                                                                '+
    '                <div class="col-lg-6">                                                                '+
    '                    <div class="input-group">                                                         '+
    '                        <span class="input-group-addon">Reference Line Number</span>                  '+
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.RefLineNumber + '">' +
    '                    </div>                                                                            '+
    '                </div>                                                                                '+
    '            </div>                                                                                    '+
    '        </div>                                                                                        '+
    '        <div class="row" style="margin-top:10px;">                                                    '+
    '            <div class="col-lg-12">                                                                   '+
    '                <div class="col-lg-6">                                                                '+
    '                    <div class="input-group">                                                         '+
    '                        <span class="input-group-addon">Company Name</span>                           '+
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.CompanyName + '">' +
    '                    </div>                                                                            '+
    '                </div>                                                                                '+
    '                <div class="col-lg-6">                                                                '+
    '                    <div class="input-group">                                                         '+
    '                        <span class="input-group-addon">Tax Due</span>                                '+
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.TaxDue + '">' +
    '                    </div>                                                                            '+
    '                </div>                                                                                '+
    '            </div>                                                                                    '+
    '        </div>                                                                                        '+
    '        <div class="row" style="margin-top:10px;">                                                    '+
    '            <div class="col-lg-12">                                                                   '+
    '                <div class="col-lg-2">                                                                '+
    '                    <div class="input-group">                                                         '+
    '                        <span class="input-group-addon">Account Type</span>                           '+
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.AccountType + '">' +
    '                    </div>                                                                            '+
    '                </div>                                                                                '+
    '                <div class="col-lg-10">                                                               '+
    '                    <div class="input-group">                                                         '+
    '                        <span class="input-group-addon">Invoice Dist Desc</span>                      '+
    '                        <input type="text" class="form-control pull-right" disabled="disabled" value = "' + selectedRow.InvoiceDistributionDescription + '">' +
    '                    </div>                                                                            '+
    '                </div>                                                                                '+
    '            </div>                                                                                    '+
    '        </div>                                                                                        '+
    '        <div class="row" style="margin-top:10px;">                                                    '+
    '            <div class="input-group-lg compactModal col-lg-2">                                        '+
    '                <label class="control-label">Accrue</label>                                           '+
    '                <select class="form-control selectpicker" id="drop_Accrue">                           '+
    '                    <option value="R">R</option>                                                       '+
    '                    <option value="Y">Y</option>                                                       '+
    '                    <option value="N">N</option>                                                       '+
    '                 </select>                                                                            '+
    '            </div>                                                                                    '+
    '            <div class="input-group-lg compactModal col-lg-4">                                        '+
    '                <label class="control-label">Accrue Tax Reason</label>                                '+
    '                <select class="form-control selectpicker" id="drop_AccrueReason"></select>                            '+
    '            </div>                                                                                    '+
    '            <div class="input-group-lg compactModal col-lg-2">                                        '+
    '                <label class="control-label">Import</label>                                           '+
    '                <select class="form-control selectpicker" id="drop_Import">                           '+
    '                    <option value="R">R</option>                                                       '+
    '                    <option value="Y">Y</option>                                                       '+
    '                    <option value="N">N</option>                                                       '+
    '                 </select>                                                                            '+
    '            </div>                                                                                    '+
    '            <div class="input-group-lg compactModal col-lg-4">                                        '+
    '                <label class="control-label">Impor Reason</label>                                     '+
    '                <select class="form-control selectpicker" id="drop_ImportReason"></select>                            '+
    '            </div>                                                                                    '+
    '        </div>                                                                                        '+
    '    </div>                                                                                            ';                                            

    if (table == "Working") {

        var actionButtons = [{
            name: "Save",
            class: "btn-success",
            closeModalOnClick: true,
            onClick: function ($modal) {
                spparams = [
                    { Name: "@P2PWorkingID", Value: InvID },
                    { Name: "@Accrue", Value: $modal.find("#drop_Accrue").val() },
                    { Name: "@AccrueTaxReason", Value: $modal.find("#drop_AccrueReason :selected").text() },
                    { Name: "@Import", Value: $modal.find("#drop_Import").val() },
                    { Name: "@ImportReason", Value: $modal.find("#drop_ImportReason :selected").text() }
                ];
                updatePendingLines('[dbo].[NSP_VF_AC_UPDATE_WORKING]', spparams);
            }
        }]
    } else {
        var actionButtons = [{
            name: "Working",
            class: "btn-warning",
            closeModalOnClick: true,
            onClick: function ($modal) {
                spparams = [
                    { Name: "@P2PWorkingID", Value: InvID },
                    { Name: "@DestinationTable", Value: 'W' }
                ];
                updatePendingLines('[dbo].[NSP_VF_AC_UPDATE_APPROVING]', spparams);
                loadWorkingTable();
            }
        },{
            name: "Export",
            class: "btn-success",
            closeModalOnClick: true,
            onClick: function ($modal) {
                spparams = [
                    { Name: "@P2PWorkingID", Value: InvID },
                    { Name: "@DestinationTable", Value: 'E' }
                ];
                updatePendingLines('[dbo].[NSP_VF_AC_UPDATE_APPROVING]', spparams);
                loadExportTable();
            }
        }, {
            name: "Non Export",
            class: "btn-danger",
            closeModalOnClick: true,
            onClick: function ($modal) {
                spparams = [
                    { Name: "@P2PWorkingID", Value: InvID },
                    { Name: "@DestinationTable", Value: 'N' }
                ];
                updatePendingLines('[dbo].[NSP_VF_AC_UPDATE_APPROVING]', spparams);
                loadNonExportTable();
            }
        }]
    }

    _showModal({
        width: "75%",
        title: "P2P Item Line",
        contentHtml: htmlForm,
        buttons: actionButtons,
        addCloseButton: true,
        onReady: function ($modal) {
            var qry, qry2;

            qry = "SELECT DISTINCT [AccrueTaxReasonID] [id], [AccrueTaxReason] [text] FROM [dbo].[tblVertex_AccrueTaxReason] WHERE Active = 1 ORDER BY AccrueTaxReason ASC;";
            loadDropDowns(qry, selectedRow.AccrueTaxReason, $modal.find("#drop_AccrueReason"), table);
            qry2 = "SELECT [ImportReasonID] [id],[ImportReason] [text] FROM [dbo].[tblVertex_ImportReason] WHERE [Active] = 1 ORDER BY [ImportReason] ASC;";
            loadDropDowns(qry, selectedRow.ImportReason, $modal.find("#drop_ImportReason"), table);
            $("#drop_Accrue").val(selectedRow.Accrue);
            $("#drop_Import").val(selectedRow.Import);
        }
    });
}

function loadDropDowns(selectQuery, selectedId, dropList, table) {
    var $select, App = false, aid = "0";

    if (typeof dropList == "string") {
        $select = $("#" + dropList + "");
    } else {
        $select = dropList;
    }

    if (table == "Working") {
        $select.contents().remove();
        if (selectedId == 'Review') {
            $select.append($('<option>', { value: "0", text: selectedId }));
        } else {
            App = true;
        }
    } else {
        App = true;
        $select.contents().remove();
    }
    //Set default value  
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Log ID Details...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify(selectQuery) },
        type: "post",
        success: function (resultList) {
            //$select.contents().remove();
            //$select.append($('<option>', { value: val, text: "Review" }));
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                if (App) {
                    if (objFunction.text == selectedId) {
                        aid = objFunction.id;
                    }
                }
                $select.append($('<option>', { value: objFunction.id, text: objFunction.text, selected: (objFunction.id == aid) }));
            }
        }
    });
};

function loadDropDowns2(selectQuery, selectedId, dropList) {
    //Set default value  
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Log ID Details...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify(selectQuery) },
        type: "post",
        success: function (resultList) {
            $("#" + dropList + "").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $("#" + dropList + "").append($('<option>', { value: objFunction.id, text: objFunction.text, selected: (objFunction.id == selectedId) }));
            }
        }
    });
};

function updatePendingLines(sp,params) {
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Updating Line..",
        name: sp,
        params: params,
        success: {
            fn: function (responseList) {
                //_showAlert({
                //    type: "success",
                //    title: "Message",
                //    content: "Line Updated"
                //});
                _showNotification("success", "Line Updated");
                loadWorkingTable();
                loadApprovingTable();
            }
        }
    });
}

function downloadTables(sp,tblName) {
    _downloadExcel({
        spName: sp,
        spParams: [{Name: "@AppID", Value:"P2P"}],
        filename: "P2P_Table_" + tblName + "_" + Date.now() ,
        success: {
            msg: "Please wait, generating Excel..."
        }
    });
}

$("#btnjournal").click(function () {
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Validating log identifer..",
        name: "[dbo].[NSP_VF_JOURNAL_VALIDATION]",
        params: [{Name:'@AppID', Value:'P2P'}],
        success: {
            fn: function (responseList) {
                if (responseList[0].Value == 1) {
                    _showNotification("error", responseList[0].Message);
                } else {
                    var messagelog = '';
                    if (responseList[1].Value == 1) {
                        messageLog = responseList[1].Message;
                    } else {
                        messageLog = "Generate New Journal Entry";
                    }
                    var htmlContentModal = "<b>" + messageLog +"<br/>";
                    _showModal({
                        width: '35%',
                        modalId: "modalDel",
                        addCloseButton: true,
                        buttons: [{
                            name: "Yes",
                            class: "btn-danger",
                            onClick: function () {
                                _callProcedure({
                                    loadingMsgType: "fullLoading",
                                    loadingMsg: "Generating Journal..",
                                    name: "[dbo].[NSP_VF_JOURNAL_GENERATION]",
                                    params: [{ Name: "@overWrite", Value: responseList[1].Value },
                                             { Name: "@AppID", Value: 'P2P' }],
                                    success: {
                                        fn: function (responseList) {
                                            
                                            _showAlert({
                                                type: "success",
                                                title: "Message",
                                                content: "Journal Generated"
                                            });

                                            _downloadExcel({
                                                spName: "[dbo].[NSP_VF_LT_TOTAL_NEW] ",
                                                spParams: [{ Name: "@AppID", Value: 'P2P' }],
                                                filename: "P2P_Journal_" + responseList[0].Period,
                                                pathToSave: "\\\\gcodfs\\GCO_Data_Grp\\ART\\Tax\\_Sales & Use Tax\\Vertex Feeds\\Journal Files\\Journals Vertex\\",
                                                success: {
                                                    msg: "Please wait, GDW report..."
                                                }
                                            });
                                            loadImportLogTable();
                                            var qry = "SELECT '0' AS [id],	'-- Period --' AS [text] UNION SELECT CONVERT(VARCHAR(50),[ImportMonth]) + '-'+ CONVERT(VARCHAR(50),[ImportYear]) AS [id], CONVERT(VARCHAR(50),[ImportMonth]) + '-' + CONVERT(VARCHAR(50),[ImportYear]) AS [text] FROM [SAU].[dbo].[tblVertex_HistoryExport] GROUP BY CONVERT(VARCHAR(50),[ImportMonth]) + '-'+ CONVERT(VARCHAR(50),[ImportYear]), CONVERT(VARCHAR(50),[ImportMonth]) + '-'+ CONVERT(VARCHAR(50),[ImportYear])"
                                            loadDropDowns2(qry, "0", "drop_Periods")
                                        }
                                    }
                                });
                            }
                        }],
                        title: "Are you sure you want to generate this Journal?",
                        contentHtml: htmlContentModal
                    });
                }
            }
        }
    });




    //var htmlContentModal = "<b>This action will generate the vertex journal<br/>";
    //_showModal({
    //    width: '35%',
    //    modalId: "modalDel",
    //    addCloseButton: true,
    //    buttons: [{
    //        name: "Yes",
    //        class: "btn-danger",
    //        onClick: function () {
    //            //_callProcedure({
    //            //    loadingMsgType: "fullLoading",
    //            //    loadingMsg: "Updating Docs..",
    //            //    name: "[dbo].[spFirmDeleteDocument]",
    //            //    params: [
    //            //        { Name: '@taxDocumentID', Value: taxID },
    //            //        { Name: '@documentID', Value: docID },
    //            //        { Name: '@SOEID', Value: _getUserInfo().SOEID }
    //            //    ],
    //            //    success: {
    //            //        fn: function (responseList) {
    //            //            //console.log(responseList[0].docLocation + '\\' + $("#txt_DocumentName").val());
    //            //            _callServer({
    //            //                loadingMsgType: "fullLoading",
    //            //                loadingMsg: "Checking documents in Share Folder...",
    //            //                url: '/TheFirm/DeleteFile',
    //            //                data: {
    //            //                    thefirmpath: _encodeSkipSideminder(responseList[0].docLocation + '\\' + $("#txt_DocumentName").val())
    //            //                },
    //            //                success: function (resultMsg) {
    //            //                    _showNotification("success", "File have been deleted");
    //            //                    //$("#tbl_SearchResult").jqxGrid('clear');
    //            //                    $("#btnSearch").trigger("click");
    //            //                }
    //            //            });

    //            //        }
    //            //    },
    //            //});
    //        }
    //    }],
    //    title: "Are you sure you want to generate this Journal?",
    //    contentHtml: htmlContentModal
    //});





    //_callProcedure({
    //    loadingMsgType: "fullLoading",
    //    loadingMsg: "Validating log identifer..",
    //    name: "[dbo].[NSP_VF_JOURNALGENERATION]",
    //    params: [
    //    ],
    //    success: {
    //        fn: function (responseList) {
    //            _showNotification("success", "Journal Generated");
    //            loadImportLogTable();
    //        }
    //    }
    //});
});

$("#btnWorkingTable_Download").click(function(){
    downloadTables("[dbo].[NSP_VF_LT_TABLE_WORKING]", "Working");
});

$("#btnApprovalTable_Download").click(function () {
    downloadTables("[dbo].[NSP_VF_LT_TABLE_APPROVING]", "Approving");
});

$("#btnExportTable_Download").click(function () {
    downloadTables("[dbo].[NSP_VF_LT_TABLE_EXPORT]", "Export");
});

$("#btnNonExportTable_Download").click(function () {
    downloadTables("[dbo].[NSP_VF_LT_TABLE_NON_EXPORT]", "NonExport");
});

$("#btnDownImport").click(function () {
    if ($("#drop_Periods :selected").text() != "-- Period --") {
        var period = $("#drop_Periods :selected").text().split("-")
        _downloadExcel({
            spName: "[dbo].[NSP_VF_LT_DOWNLOAD_IMPORTED]",
            spParams: [
                { Name: "@ImportMonth", Value: period[0] },
                { Name: "@ImportYear", Value: period[1] },
                { Name: "@AppID", Value: 'P2P' }
            ],
            filename: "DownLoad_P2P_Import_" + $("#drop_Periods :selected").text(),
            success: {
                msg: "Please wait, generating Excel..."
            }
        });
    } else {
        _showNotification("error", "Select a period");
    }
    //downloadTables("[dbo].[NSP_VF_LT_NON_EXPORT_TABLE]", "NonExport");
});

$("#btnDownJournal").click(function () {
    if ($("#drop_Periods :selected").text() != "-- Period --") {
        var period = $("#drop_Periods :selected").text().split("-")
        _downloadExcel({
            spName: "[dbo].[NSP_VF_LT_DOWNLOAD_TOTAL_NEW]",
            spParams: [
                { Name: "@ImportMonth", Value: period[0] },
                { Name: "@ImportYear", Value: period[1] },
                { Name: "@AppID", Value: 'P2P' }
            ],
            filename: "DownLoad_Journal_" + $("#drop_Periods :selected").text(),
            success: {
                msg: "Please wait, generating Excel..."
            }
        });
    } else {
        _showNotification("error", "Select a period");
    }
    //downloadTables("[dbo].[NSP_VF_LT_NON_EXPORT_TABLE]", "NonExport");
});

$("#btnVertexFile").click(function () {
    downloadTables("[dbo].[NSP_VF_LT_DOWNLOAD_VERTEX_FILE]", "VertexFile");
});