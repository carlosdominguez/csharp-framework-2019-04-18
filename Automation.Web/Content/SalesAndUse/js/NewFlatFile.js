﻿//#regionDOCUMENT READY

var header = "";
var inline = "";
var footer = "";


$(document).ready(function () {

});
//#endregion

//#endregion

//#region NEW FLAT FILE MODAL
function showNewFlatFileModal() {

    _showModal({
        modalId: "ModalJurisdiction",
        title: "NEW FLAT FILE",
        width: "98%",
        contentAjaxUrl: "/SalesAndUse/ModalNewFlatFile",
        contentAjaxParams: {
            idJurisdictionLocation: ""
        },
        buttons: [{
            name: "CREATE FLAT FILE",
            class: "btn-info",
            onClick: function ($modal) {


                if ($("#txt_flatfile_previewheader").val() == "") {
                    _showNotification("error", "No pending filings.");
                } else {
                    
                    var ffDownload = header + inline + footer;
                    var ffName = $("#txt_flatfileName").val();


                    var hours = $("#txt_flatfileName").val().split("_");
                    var ffHours = hours[4];


                    //DOWNLOAD TXT
                    download(ffName, ffDownload);



                    var myObject = new Object();

                    myObject.ffName = ffName;
                    myObject.ffHours = ffHours;

                    var flatfile = JSON.stringify(myObject);

                    //INSERT NEW INFORMATION
                    _callServer({
                        url: '/SalesAndUse/NewFlatFile',
                        closeModalOnClick: false,
                        data: { 'pjson': flatfile },
                        type: "post",
                        success: function (savingStatus) {
                            _showNotification("success", "Flat file successfully generated.");
                            loadFlatFileGeneral();

                            //sendAutomaticEmailSQLSP();

                        }
                    });
                }
            }
        }],
        onReady: function ($modal) {
            $("#txt_flatfileDate").val(new Date());

            if (_getUserInfo().Roles.includes('FLATFILE')) {

                $("#ModalJurisdiction_btn_0").prop("disabled", false);
            } else {
                $("#ModalJurisdiction_btn_0").prop("disabled", true);
            }
            
            loadFlatFilesPreliminary();
            




        },
        class: " compactModal"
    });
}

function line30(objFunction1, header20, h2) {
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Preview Header...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("EXECUTE NSP_AC_FLATFILE_NFFI30 " + objFunction1.idFiling + "") },
        type: "post",
        success: function (resultList30) {
            $("#txt_flatfile_preview").val(header20);
            inline += h2 + '\r\n';
            for (var i = 0; i < resultList30.length; i++) {
                var objFunction30 = resultList30[i];
                $("#txt_flatfile_preview").val($("#txt_flatfile_preview").val() + objFunction30.header30 + '\r\n');
                inline += objFunction30.header30 + '\r\n';
            }
        }
    });

}
//#endregion


function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

function sendAutomaticEmailSQLSP() {
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Sending Email to Treasury...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("EXECUTE NSP_AC_TREASURY_EMAIL_FLATFILE " + _getSOEID() + "") },
        type: "post",
        success: function (resultList30) {
            //IF EMAIL WAS SENT
        }
    });



}

function loadFlatFilesPreliminary() {
    //Set default value  

    var date1 = new Date();
    Date.prototype.yyyymmdd = function () {
        var mm = this.getMonth() + 1; // getMonth() is zero-based
        var dd = this.getDate();

        return [this.getFullYear(),
                (mm > 9 ? '' : '0') + mm,
                (dd > 9 ? '' : '0') + dd
        ].join('');
    };


    currentHours = date1.getHours();
    currentHours = ("0" + currentHours).slice(-2);

    currentMinutes = date1.getMinutes();
    currentMinutes = ("0" + currentMinutes).slice(-2);

    currentSeconds = date1.getSeconds();
    currentSeconds = ("0" + currentSeconds).slice(-2);







    var rundate = "APFROACH_US_FROACH_" + date1.yyyymmdd() + '_' + currentHours + currentMinutes + currentSeconds + "_INV_FLAT.TXT";
    var runHours = "" + currentHours + currentMinutes + currentSeconds;
    var query = "EXEC NSP_AC_FLATFILE_NEWFF_PRELIMINAR '" + _getSOEID() + "','" + rundate + "','" + runHours + "'";

    $("#txt_flatfileName").val(rundate);

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Generating preliminary Flat File Details...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify(query) },
        type: "post",
        success: function (resultList) {
       
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                
                var fields = objFunction.Return.split('NEWL%');

                $("#txt_flatfile_previewheader").text(fields[0]);
                $("#txt_flatfile_previewheader").val(fields[0]);
                header = fields[0] + '\r\n';

                $("#txt_flatfile_previewfooter").text(fields[fields.length-1]);
                $("#txt_flatfile_previewfooter").val(fields[fields.length - 1]);
                footer = fields[fields.length - 1];

                fields.shift();
                fields.pop();


                var textarea = document.getElementById("txt_flatfile_preview");
                textarea.value = fields.join("\r\n");
                inline = fields.join("\r\n");
                inline = inline + '\r\n';

               
                
                





            }
        }
    });
};