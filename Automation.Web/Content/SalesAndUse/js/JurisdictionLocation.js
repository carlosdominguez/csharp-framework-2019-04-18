/// <reference path="../../Shared/plugins/util/global.js" />


//#regionDOCUMENT READY
$(document).ready(function () {
    loadJurisdictionLocation();

});
//#endregion

//#region LOAD JURISDICTION TABLE s
function loadJurisdictionLocation() {
    $.jqxGridApi.create({
        showTo: "#tbl_JurisdictionLocation",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_JL_JURISDICTION]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'idJurisdictionLocation', text: "Log ID", type: 'string', width: '4%' },
            { name: 'responsableSoeid', text: "RESP SOEID", type: 'string', width: '10%' },
            { name: 'idLVID', type: 'string', hidden: true },
            { name: 'lvid', type: 'string', text: "LVID", width: '5%' },
            { name: 'idLegalEntity', type: 'string', hidden: true },
            { name: 'legalEntityName', type: 'string', text: "LE", width: '10%' },
            { name: 'idCorpCode', type: 'string', hidden: true },
            { name: 'corpCode', type: 'string', text: "CORP CODE", width: '5%' },
            { name: 'idFEIN', type: 'string', hidden: true },
            { name: 'fein', type: 'string', text: "FEIN", width: '5%' },
            { name: 'idType', type: 'string', hidden: true },
            { name: 'type', type: 'string', text: "TYPE", width: '5%' },
            { name: 'idState', type: 'string', hidden: true },
            { name: 'stateDivSub', type: 'string', text: "STATE", width: '5%' },
            { name: 'idAccount', type: 'string', hidden: true },
            { name: 'account', type: 'string', text: "ACCOUNT", width: '10%' },
            { name: 'frequency', type: 'string', text: "FREQUENCY", width: '5%' },
            { name: 'idJurisdiction', type: 'string', hidden: true },
            { name: 'jurisdiction', type: 'string', text: "JURISDICTION", width: '10%' },
            { name: 'monthDue', type: 'string', text: "MONTH", width: '5%' },
            { name: 'dueDay', type: 'string', text: "DUE DAY", width: '5%' },
            { name: 'effectiveDate', type: 'string', text: "EFF DATE", width: '10%' },
            { name: 'ctCorp', type: 'string', text: "CT CORP", width: '5%' },
            { name: 'P2PLVID', type: 'string', text: "P2P LVID", width: '5%' },
            { name: 'goc', type: 'string', text: "GOC", width: '10%' },
            { name: 'idP2PGocsLvid', type: 'string', hidden: true },
            { name: 'vendorSupplier', type: 'string', text: "VEND SUP", width: '10%' },
            { name: 'vendorSiteCode', type: 'string', text: "VEND SITE", width: '10%' },
            { name: 'idCommodityCode', type: 'string', hidden: true },
            { name: 'commodityCode', type: 'string', text: "COMMODITY", width: '10%' },
            { name: 'ckAch', type: 'string', text: "ACH ? ", width: '5%' },
            { name: 'active', type: 'string', text: "ACTIVE ?", width: '5%' },
            { name: 'idP2PVendorSite', type: 'string', hidden: true },
            { name: 'vendorSite', type: 'string', text: "P2P Vendor", width: '5%' }
        ],

        ready: function () {

            if (_getUserInfo().Roles.includes('REVIEWER')) {
                $("#btnJurisdiction_AddNew").prop("disabled", false);
                $("#ModalJurisdiction_btn_0").prop("disabled", false);
            } else {
                $("#btnJurisdiction_AddNew").prop("disabled", true);
                $("#ModalJurisdiction_btn_0").prop("disabled", true);
            }

            _hideLoadingFullPage();
            $('#tbl_JurisdictionLocation').on('rowdoubleclick', function (event) {
                if (typeof event.args.group == "undefined") {
                    //_showLoadingFullPage();
                    var row = event.args.rowindex;
                    var datarow = $("#tbl_JurisdictionLocation").jqxGrid('getrowdata', row);
                    showModal(datarow);
                }
            });
        }
    });
}

//#endregion

//#region LOAD JURISDICTION MODAL
function showModal(row) {


    if (row == "NewJurisdiction") {
        _showModal({
            modalId: "ModalJurisdiction",
            title: "JURISDICTION",
            width: "98%",
            contentAjaxUrl: "/SalesAndUse/ModalJurisdictionLocation",
            contentAjaxParams: {
                idJurisdictionLocation: "",
                responsableSoeid: "", idLVID: "", lvid: "", idLegalEntity: "",
                legalEntityName: "", idCorpCode: "", corpCode: "",
                idFEIN: "", fein: "", idType: "", type: "",
                idState: "", stateDivSub: "", idAccount: "", account: "",
                frequency: "", idJurisdiction: "", jurisdiction: "", monthDue: "",
                dueDay: "", effectiveDate: "", ctCorp: "", P2PLVID: "",
                goc: "", idP2PGocsLvid: "", vendorSupplier: "", vendorSiteCode: "",
                idCommodityCode: "", commodityCode: "", active: ""
            },
            buttons: [{
                name: "Save Changes",
                class: "btn-info",
                onClick: function ($modal) {
                    //SAVE A NEW LOGID

                    //-------CREATE OBJECT-----------------------------------------------------------
                    var myObject = new Object();
                    myObject.lvidId = $("#selectedLVIDID").attr("idLVID");
                    myObject.p2plvid = $("#selectedP2PLVIDID").attr("idP2PLVID");
                    myObject.idState = $("#drp_state option:selected").val();
                    myObject.idAccount = $("#drp_account option:selected").val();
                    myObject.idJurisdiction = $("#drp_jurisdiction option:selected").val();
                    myObject.frequency = $("#drp_frequency option:selected").val();
                    myObject.responsableSoeid = $("#drp_responsable option:selected").val();
                    myObject.idType = $("#drp_type option:selected").val();
                    myObject.idCommodityCode = $("#drp_commodity option:selected").val();
                    myObject.monthDue = $("#drp_monthDue option:selected").val();
                    myObject.effectiveDate = $('#txt_effectiveDate').val();



                    myObject.dueDay = $("#drp_dueDate option:selected").val();







                   



                    if ($("#chk_active").is(':checked')) {
                        myObject.active = "1";
                    } else {
                        myObject.active = "0";
                    }


                    if ($("#chk_ach").is(':checked')) {
                        myObject.ckAch = "1";
                    } else {
                        myObject.ckAch = "0";
                    }

                    myObject.vendorSiteCode = $('#txt_P2PVendorSiteCode').val();
                    myObject.vendorSupplier = $('#txt_P2PVendorNumber').val();


                    if ($("#drp_vendor option:selected").val() == "0") {
                        myObject.idP2PVendorSite = "0";
                    } else {
                        myObject.idP2PVendorSite = $("#drp_vendor option:selected").val();
                    }                    
                    //-------------------------------------------------------------------------------


                    //-------VALIDATE FIELDS---------------------------------------------------------


                    var myValidations = validateCompleteForm(myObject)


                    if (myValidations.result) {

                        var jurisdiction = JSON.stringify(myObject);

                        //INSERT NEW INFORMATION
                        _callServer({
                            url: '/SalesAndUse/JurisdictionInsert',
                            closeModalOnClick: false,
                            data: { 'pjson': jurisdiction },
                            type: "post",
                            success: function (savingStatus) {
                                loadJurisdictionLocation();
                                _showNotification("success", "Jurisdiction saved successfully.");
                            }
                        });
                    } else {
                        _showNotification("error", myValidations.message);
                    }
                }
            }],
            onReady: function ($modal) {

                if (_getUserInfo().Roles.includes('REVIEWER')) {
                    $("#btnJurisdiction_AddNew").prop("disabled", false);
                    $("#ModalJurisdiction_btn_0").prop("disabled", false);
                } else {
                    $("#btnJurisdiction_AddNew").prop("disabled", true);
                    $("#ModalJurisdiction_btn_0").prop("disabled", true);
                }


                loadDropDowns(qry_state, '0', "drp_state");
                loadDropDowns(qry_account, '0', "drp_account");
                loadDropDowns(qry_jurisdiction, '0', "drp_jurisdiction");
                loadDropDowns(qry_frequency, '0', "drp_frequency");
                loadDropDowns(qry_responsable, '0', "drp_responsable");
                loadDropDowns(qry_type, '0', "drp_type");
                loadDropDowns(qry_commodity, '0', "drp_commodity");

                //loadDropDowns(qry_duemonth, '0', "drp_monthDue");

                loadDropDowns(qry_dueday, '0', "drp_dueDate");
                loadDropDowns(qry_p2pvendor, '0', "drp_vendor");

                //CREATE DROPLIST TABLE BUTTON
                $("#jqxdropdownbutton_LVID").jqxDropDownButton({ template: "summer", width: "100%", height: 30 });
                $("#jqxdropdownbutton_P2P_LVID").jqxDropDownButton({ template: "summer", width: "100%", height: 30 });

                $("#txt_effectiveDate").datepicker();


                loadDropDownLVIDs("0");
                loadDropDownP2PLVIDs("0");

            },
            class: " compactModal"
        });

    } else {
        _showModal({
            modalId: "ModalJurisdiction",
            title: "JURISDICTION - LogID:" + row.idJurisdictionLocation,
            width: "98%",
            contentAjaxUrl: "/SalesAndUse/ModalJurisdictionLocation",
            contentAjaxParams: {
                idJurisdictionLocation: row.idJurisdictionLocation,
                responsableSoeid: row.responsableSoeid,
                idLVID: row.idLVID, lvid: row.lvid,
                idLegalEntity: row.idLegalEntity,
                legalEntityName: row.legalEntityName,
                idCorpCode: row.idCorpCode,
                corpCode: row.corpCode,
                idFEIN: row.idFEIN,
                fein: row.fein,
                idType: row.idType,
                type: row.type,
                idState: row.idState,
                stateDivSub: row.stateDivSub,
                idAccount: row.idAccount,
                account: row.account,
                frequency: row.frequency,
                idJurisdiction: row.idJurisdiction,
                jurisdiction: row.jurisdiction,
                monthDue: row.monthDue,
                dueDay: row.dueDay,
                effectiveDate: _formatDate(new Date(row.effectiveDate), 'MM/dd/yyyy'),
                ctCorp: row.ctCorp,
                P2PLVID: row.P2PLVID,
                goc: row.goc,
                idP2PGocsLvid: row.idP2PGocsLvid,
                vendorSupplier: row.vendorSupplier,
                vendorSiteCode: row.vendorSiteCode,
                idCommodityCode: row.idCommodityCode,
                commodityCode: row.commodityCode,
                active: row.active
            },
            buttons: [{
                name: "Save Changes",
                class: "btn-info",
                onClick: function ($modal) {
                    //UPDATE A EXISTING LOG ID
                    //-------CREATE OBJECT-----------------------------------------------------------
                    var myObject = new Object();

                    myObject.jurisdictionId = row.idJurisdictionLocation;
                    myObject.lvidId = $("#selectedLVIDID").attr("idLVID");
                    myObject.p2plvid = $("#selectedP2PLVIDID").attr("idP2PLVID");
                    myObject.idState = $("#drp_state option:selected").val();
                    myObject.idAccount = $("#drp_account option:selected").val();
                    myObject.idJurisdiction = $("#drp_jurisdiction option:selected").val();
                    myObject.frequency = $("#drp_frequency option:selected").val();
                    myObject.responsableSoeid = $("#drp_responsable option:selected").val();
                    myObject.idType = $("#drp_type option:selected").val();
                    myObject.idCommodityCode = $("#drp_commodity option:selected").val();
                    myObject.monthDue = $("#drp_monthDue option:selected").val();
                    myObject.dueDay = $("#drp_dueDate option:selected").val();
                    myObject.effectiveDate = $('#txt_effectiveDate').val();



                    if ($("#chk_active").is(':checked')) {
                        myObject.active = "1";
                    } else {
                        myObject.active = "0";
                    }


                    if ($("#chk_ach").is(':checked')) {
                        myObject.ckAch = "1";
                    } else {
                        myObject.ckAch = "0";
                    }

                    myObject.vendorSiteCode = $('#txt_P2PVendorSiteCode').val();
                    myObject.vendorSupplier = $('#txt_P2PVendorNumber').val();


                    if ($("#drp_vendor option:selected").val() == "0") {
                        myObject.idP2PVendorSite = "0";
                    } else {
                        myObject.idP2PVendorSite = $("#drp_vendor option:selected").val();
                    }

                    //-------------------------------------------------------------------------------

                    //-------VALIDATE UPDATE FIELDS--------------------------------------------------


                    var myValidations = validateCompleteForm(myObject)


                    if (myValidations.result) {


                        //--CREATE UPDATE QUERY





                        var jurisdiction = JSON.stringify(myObject);

                        //INSERT NEW INFORMATION
                        _callServer({
                            url: '/SalesAndUse/JurisdictionUpdate',
                            closeModalOnClick: false,
                            data: { 'pjson': jurisdiction },
                            type: "post",
                            success: function (savingStatus) {
                                loadJurisdictionLocation();
                                _showNotification("success", "Jurisdiction updated successfully.");
                            }
                        });
                    } else {
                        _showNotification("error", myValidations.message);
                    }
                }
            }],
            onReady: function ($modal) {

                if (_getUserInfo().Roles.includes('REVIEWER')) {
                    $("#btnJurisdiction_AddNew").prop("disabled", false);
                    $("#ModalJurisdiction_btn_0").prop("disabled", false);
                } else {
                    $("#btnJurisdiction_AddNew").prop("disabled", true);
                    $("#ModalJurisdiction_btn_0").prop("disabled", true);
                }


                loadDropDowns(qry_state, row.idState, "drp_state");
                loadDropDowns(qry_account, row.idAccount, "drp_account");
                loadDropDowns(qry_jurisdiction, row.idJurisdiction, "drp_jurisdiction");
                loadDropDowns(qry_frequency, row.frequency, "drp_frequency");
                loadDropDowns(qry_responsable, row.responsableSoeid, "drp_responsable");
                loadDropDowns(qry_type, row.idType, "drp_type");
                loadDropDowns(qry_commodity, row.idCommodityCode, "drp_commodity");
                
                loadDropDowns(qry_dueday, row.dueDay, "drp_dueDate");
                loadDropDowns(qry_p2pvendor, row.idP2PVendorSite, "drp_vendor");

                //SET MONTH
                loadMonthsPreload(row.frequency, row.monthDue);

                //CREATE DROPLIST TABLE BUTTON
                $("#jqxdropdownbutton_LVID").jqxDropDownButton({ template: "summer", width: "100%", height: 30 });
                $("#jqxdropdownbutton_P2P_LVID").jqxDropDownButton({ template: "summer", width: "100%", height: 30 });

                $("#txt_effectiveDate").datepicker();

                if (row.ckAch == "True") {
                    $('#chk_ach').prop('checked', true);
                    $('#divNoACH').hide();
                    $('#divACH').show();
                } else {
                    $('#chk_ach').prop('checked', false);
                    $('#divNoACH').show();
                    $('#divACH').hide();
                }


                if (row.active == "True") {
                    $('#chk_active').prop('checked', true);
                } else {
                    $('#chk_active').prop('checked', false);
                }



                loadDropDownLVIDs(row.idLVID);
                loadDropDownP2PLVIDs(row.idP2PGocsLvid);

                $modal.find("#txt_effectiveDate").val(_formatDate(new Date(row.effectiveDate), 'MMM dd, yyyy'));
                $modal.find("#txt_P2PVendorSiteCode").val(row.vendorSupplier);
                $modal.find("#txt_P2PVendorNumber").val(row.vendorSiteCode);


            },
            class: " compactModal"
        });
    }



}
//#endregion

//#region VARIABLES DROPLIST
var qry_state, qry_account, qry_jurisdiction, qry_frequency, qry_responsable, qry_type, qry_commodity, qry_duemonth, qry_dueday, qry_p2pvendor;

//STATE
qry_state = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT [idState] as id ,[stateDivSub] as [text] FROM [SAU].[dbo].[tblAdmin_StateDivSub] WHERE active = 1 ";
//ACCOUNT
qry_account = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT [idAccount] as id ,[account] as [text]  FROM [SAU].[dbo].[tblAdmin_Account] where active = 1";
//JURISDICTION
qry_jurisdiction = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT [idJurisdiction] as id,[jurisdiction] as [text] FROM [SAU].[dbo].[tblAdmin_Jurisdiction] where active = 1 ";
//FREQUENCY
qry_frequency = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT 'Annually' as id,'Annually' as [text] UNION SELECT 'Monthly' as id,'Monthly' as [text] UNION SELECT 'Quarter1' as id,'Quarter1' as [text] UNION SELECT 'Quarter2' as id,'Quarter2' as [text] UNION SELECT 'Semi' as id,'Semi' as [text] UNION SELECT 'NA' as id,'NA' as [text];";
//RESPONSABLE
qry_responsable = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT  [SOEID] as id ,[Name] as [text] FROM [SAU].[dbo].[tblAutomation_User];";
//TYPE
qry_type = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT [idType] as id,[type] as [text] FROM [SAU].[dbo].[tblAdmin_Type] where active = 1 ";
//COMMODITYCODE
qry_commodity = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT [idCommodityCode] as id,[commodityCode] as [text] FROM [SAU].[dbo].[tblAdmin_CommodityCode] where active = 1;";
//DUEDAY
qry_dueday = "SELECT [id],[text] FROM (SELECT '0' AS [id], '-- Select --' AS [text], 0 as [order] UNION SELECT [dueDay] as id, [dueDay] as text, [order] FROM [SAU].[dbo].[tblAdmin_DueDay]) as a ORDER BY a.[order];";
//P2PVENDOR
qry_p2pvendor = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT [idVendorSite] as id ,[vendorSite] as text FROM [SAU].[dbo].[tblAdmin_P2PVendorSite] where active = 1;";


//DUEMONTH
qry_duemonth_Anual = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT [monthDue] as [id],[description] AS [text] FROM [SAU].[dbo].[tblAdmin_MonthDue] where category = 'Annual' AND active = 1;";

qry_duemonth_Semi = "SELECT [monthDue] as [id],[description] AS [text] FROM [SAU].[dbo].[tblAdmin_MonthDue] where category = 'Semi' AND active = 1;";


qry_duemonth_Monthly = "SELECT [monthDue] as [id],[description] AS [text] FROM [SAU].[dbo].[tblAdmin_MonthDue] where category = 'Monthly' AND active = 1 ";

qry_duemonth_Q1 = "SELECT [monthDue] as [id],[description] AS [text],[order] as [order] FROM [SAU].[dbo].[tblAdmin_MonthDue] where category = 'Quarter1' AND active = 1 ";

qry_duemonth_Q2 = "SELECT [monthDue] as [id],[description] AS [text],[order] as [order] FROM [SAU].[dbo].[tblAdmin_MonthDue] where category = 'Quarter2' AND active = 1 ";

qry_duemonth_NA = "SELECT 'NA' as [id],'NA' as [text]";

//#endregion

//#region LOAD DROPLISTS
function loadDropDowns(selectQuery, selectedId, dropList) {
    //Set default value  
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Log ID Details...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify(selectQuery) },
        type: "post",
        success: function (resultList) {
            $("#" + dropList + "").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $("#" + dropList + "").append($('<option>', { value: objFunction.id, text: objFunction.text, selected: (objFunction.id == selectedId) }));
            }
        }
    });
};

//#endregion

//#region LOAD DROPLIST TABLES
function loadDropDownLVIDs(data) {
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading LVID...",
        name: "[dbo].[NSP_LT_ADMIN_LVID]",
        params: [],
        success: {
            fn: function (responseList) {
                loadTable(responseList);

                //SET CURRENT VALUE
                if (data) {
                    var resultList = $.grep(responseList, function (v) {
                        return v.idLVID == data;
                    });

                    if (resultList.length > 0) {
                        var row = resultList[0];

                        var dropDownContent = '<div id="selectedLVIDID" style="position: relative; width:100%; margin-left: 3px; margin-top: 5px;" idLVID="' + row.idLVID + '" >' + row['lvid'] + ' - ' + row['legalEntityName'] + '</div>';
                        $("#jqxdropdownbutton_LVID").jqxDropDownButton('setContent', dropDownContent);

                        $('#txt_CorpCode').val(row['corpCode']);
                        $('#txt_LegalEntity').val(row['legalEntityName']);
                        $('#txtFEIN').val(row['fein']);
                    }
                }
            }
        }
    })

    function loadTable(list) {
        $.jqxGridApi.create({
            showTo: "#tblLVID",
            options: {
                //for comments or descriptions
                height: "600",
                autoheight: false,
                autorowheight: false,
                selectionmode: "singlerow",
                showfilterrow: false,
                sortable: true,
                groupable: true,
                showfilterrow: true
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                dataBinding: "Large Data Set Local",
                rows: list
            },
            group: [],
            columns: [
                { name: 'idLVID', type: 'string', hidden: true },
                { name: 'lvid', type: 'string', width: '20%' },
                { name: 'legalEntityName', type: 'string', width: '40%' },
                { name: 'corpCode', type: 'string', width: '20%' },
                { name: 'fein', type: 'string', width: '20%' }
            ],

            ready: function () {
                _hideLoadingFullPage();
                $('#tblLVID').on('rowselect', function (event) {
                    // event.args.rowindex is a bound index.
                    if (typeof event.args.group == "undefined") {
                        var args = event.args;
                        var row = $("#tblLVID").jqxGrid('getrowdata', args.rowindex);
                        var dropDownContent = '<div id="selectedLVIDID" style="position: relative; width:100%; margin-left: 3px; margin-top: 5px;" idLVID="' + row.idLVID + '" >' + row['lvid'] + ' - ' + row['legalEntityName'] + '</div>';
                        $("#jqxdropdownbutton_LVID").jqxDropDownButton('setContent', dropDownContent);

                        $('#txt_CorpCode').val(row['corpCode']);
                        $('#txt_LegalEntity').val(row['legalEntityName']);
                        $('#txtFEIN').val(row['fein']);

                    }
                });

                //var rows1 = $('#tblLVID').jqxGrid('getrows');
                //for (var y = 0; y < rows1.length; y++) {
                //    if (data == rows1[y].idLVID) {
                //        $("#tblLVID").jqxGrid('selectrow', y);
                //    }
                //}


            }
        });
    }

    
}

function loadDropDownP2PLVIDs(data) {
    //////////////////////
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading P2PLVID...",
        name: "[dbo].[NSP_LT_ADMIN_P2PLVID]",
        params: [],
        success: {
            fn: function (responseList) {
                loadTable(responseList);

                //SET CURRENT VALUE
                if (data) {
                    var resultList = $.grep(responseList, function (v) {
                        return v.idP2PGocsLvid == data;
                    });

                    if (resultList.length > 0) {
                        var row = resultList[0];
                        var dropDownContent = '<div id="selectedP2PLVIDID" style="position: relative; width:100%; margin-left: 3px; margin-top: 5px;" idP2PLVID="' + row.idP2PGocsLvid + '" >' + row['lvid'] + ' - ' + row['account'] + '</div>';
                        $("#jqxdropdownbutton_P2P_LVID").jqxDropDownButton('setContent', dropDownContent);

                        $('#txtP2PAccount').val(row['account']);
                        $('#txt_Goc').val(row['goc']);
                    }
                }
            }
        }
    })

    function loadTable(list) {
        $.jqxGridApi.create({
            showTo: "#tblP2PLVID",
            options: {
                //for comments or descriptions
                height: "600",
                autoheight: false,
                autorowheight: false,
                selectionmode: "singlerow",
                showfilterrow: false,
                sortable: true,
                groupable: true,
                showfilterrow: true
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                dataBinding: "Large Data Set Local",
                rows: list
            },
            group: [],
            columns: [
            { name: 'idP2PGocsLvid', type: 'string', hidden: true },
            { name: 'lvid', type: 'string', width: '10%' },
            { name: 'goc', type: 'string', width: '15%' },
            { name: 'account', type: 'string', width: '15%' },
            { name: 'corpCode', type: 'string', width: '10%' },
            { name: 'fein', type: 'string', width: '10%' },
            { name: 'companyName', type: 'string', width: '10%' },
            { name: 'legalEntityName', type: 'string', width: '20%' },
            { name: 'active', type: 'string', width: '20%' }
            ],

            ready: function () {
                _hideLoadingFullPage();
                $('#tblP2PLVID').on('rowselect', function (event) {
                    // event.args.rowindex is a bound index.
                    if (typeof event.args.group == "undefined") {
                        var args = event.args;
                        var row = $("#tblP2PLVID").jqxGrid('getrowdata', args.rowindex);
                        var dropDownContent = '<div id="selectedP2PLVIDID" style="position: relative; width:100%; margin-left: 3px; margin-top: 5px;" idP2PLVID="' + row.idP2PGocsLvid + '" >' + row['lvid'] + ' - ' + row['account'] + '</div>';
                        $("#jqxdropdownbutton_P2P_LVID").jqxDropDownButton('setContent', dropDownContent);

                        $('#txtP2PAccount').val(row['account']);
                        $('#txt_Goc').val(row['goc']);

                    }
                });

                //var rows1 = $('#tblLVID').jqxGrid('getrows');
                //for (var y = 0; y < rows1.length; y++) {
                //    if (data == rows1[y].idLVID) {
                //        $("#tblLVID").jqxGrid('selectrow', y);
                //    }
                //}


            }
        });
    }

}
//#endregion

//#region CHECKBOX ACH CHANGE
function chkACH() {
    if ($('#chk_ach').is(':checked')) {
        $('#divNoACH').hide();
        $('#divACH').show();
    } else {
        $('#divNoACH').show();
        $('#divACH').hide();
    }
}
//#endregion

//#region ADD NEW JURISDICTION 
$("#btnJurisdiction_AddNew").click(function () {
    showModal("NewJurisdiction");

});
//#endregion

//#region VALIDATE FORM COMPLETE
function validateCompleteForm(myJurisdiction) {
    var myReturn = new Object();
    myReturn.result = true;
    myReturn.message = "The following fields are required:<br/>";

    if (myJurisdiction.lvidId == undefined) { myReturn.result = false; myReturn.message += "<span class=\"badge\">LVID</span>"; }

    if (myJurisdiction.p2plvid == undefined) { myReturn.result = false; myReturn.message += "<span class=\"badge\">P2P LVID</span>"; }
    if (myJurisdiction.idState == "0") { myReturn.result = false; myReturn.message += "<span class=\"badge\">STATE</span>"; }
    if (myJurisdiction.idAccount == "0") { myReturn.result = false; myReturn.message += "<span class=\"badge\">ACCOUNT</span>"; }
    if (myJurisdiction.idJurisdiction == "0") { myReturn.result = false; myReturn.message += "<span class=\"badge\">JURISDICTION</span>"; }
    if (myJurisdiction.frequency == "0") { myReturn.result = false; myReturn.message += "<span class=\"badge\">FREQUENCY</span>"; }
    if (myJurisdiction.responsableSoeid == "0") { myReturn.result = false; myReturn.message += "<span class=\"badge\">SOEID</span>"; }
    if (myJurisdiction.idCommodityCode == "0") { myReturn.result = false; myReturn.message += "<span class=\"badge\">COMMODITY CODE</span>"; }
    if (myJurisdiction.monthDue == "0") { myReturn.result = false; myReturn.message += "<span class=\"badge\">DUE MONTH</span>"; }
    if (myJurisdiction.dueDay == "0") { myReturn.result = false; myReturn.message += "<span class=\"badge\">DUE DAY</span>"; }
    if (myJurisdiction.effectiveDate == "") { myReturn.result = false; myReturn.message += "<span class=\"badge\">EFFECTIVE DATE</span>"; }

    if (myJurisdiction.ckAch == "1") {
        if (myJurisdiction.idP2PVendorSite == "0") { myReturn.result = false; myReturn.message += "<span class=\"badge\">VENDOR SITE</span>"; }
    } else {
        if (myJurisdiction.vendorSiteCode == "") { myReturn.result = false; myReturn.message += "<span class=\"badge\">VENDOR SITE</span>"; }
        if (myJurisdiction.vendorSupplier == "") { myReturn.result = false; myReturn.message += "<span class=\"badge\">VENDOR SUPPLIER</span>"; }
    }

    return myReturn;
}


//#endregion



function loadMonthsPreload (selectedValue, selectedMonth) {
    var selectQuery = "";
    switch (selectedValue) {
        case "Semi":
            selectQuery = qry_duemonth_Semi;

            break;
        case "Annually":
            selectQuery = qry_duemonth_Anual;

            break;
        case "Monthly":
            selectQuery = qry_duemonth_Monthly;
            break;
        case "Quarter1":
            selectQuery = qry_duemonth_Q1;
            break;
        case "Quarter2":
            selectQuery = qry_duemonth_Q2;
            break;
        case "NA":
            selectQuery = qry_duemonth_NA;
            break;
    }

    loadDropDowns(selectQuery, selectedMonth, "drp_monthDue");
}

function loadMonths() {
    var selectedValue = $('#drp_frequency').val();
    var selectQuery = "";
    switch (selectedValue) {
        case "Semi":
            selectQuery = qry_duemonth_Semi;

            break;
        case "Annually":
            selectQuery = qry_duemonth_Anual;

            break;
        case "Monthly":
            selectQuery = qry_duemonth_Monthly;
            break;
        case "Quarter1":
            selectQuery = qry_duemonth_Q1;
            break;
        case "Quarter2":
            selectQuery = qry_duemonth_Q2;
            break;
        case "NA":
            selectQuery = qry_duemonth_NA;
            break;
    }

    loadDropDowns(selectQuery, selectedValue, "drp_monthDue");
}

function validateLastDayOfMonth(month, day, year) {


}

$("#btnJurisdiction_Download").click(function () {
    _downloadExcel({
        sql: '[dbo].[NSP_LT_JL_JURISDICTION] ',
        filename: "_JurisdictionLocation_" + _createCustomID() + ".xls",
        success: {
            msg: "Generating report... Please Wait, this operation may take some time to complete."
        }
    });
})