﻿//#regionDOCUMENT READY
$(document).ready(function () {
    loadPolice();

});
//#endregion

//#region LOAD JURISDICTION TABLE s
function loadPolice() {
    $.jqxGridApi.create({
        showTo: "#tbl_Police",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            editable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_POLICE]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'SOEID', text: "SOEID", type: 'string', width: '10%', editable: false },
            { name: 'Name', text: "Employee", type: 'string', width: '20%', editable: false },
            { name: 'Status', text: "Status", type: 'string', width: '10%', editable: false },
            { name: 'Approval', text: "AP", type: 'string', width: '7%', editable: false },
            { name: 'ApprovedDate', text: "ApprovalDate", type: 'date', width: '10%', editable: false },
            { name: 'ApprovedBy', text: "ApprovedBy", type: 'string', width: '10%', editable: false },
            { name: 'EmailReference', text: "Email Reference", type: 'string', width: '15%', editable: true },
             { name: 'IQueryGroup', text: "IQuery Group", type: 'string', width: '20%', editable: true }

        ],

        ready: function () {
            $('#tbl_Police').on('rowdoubleclick', function (event) {
                if (typeof event.args.group == "undefined") {
                    //_showLoadingFullPage();
                    var row = event.args.rowindex;
                    var datarow = $("#tbl_Police").jqxGrid('getrowdata', row);
                    showModal(datarow);
                }
            });
        }
    });
}


function showModal(row) {

    _showModal({
        modalId: "ModalPolice",
        title: "Police",
        width: "60%",
        contentAjaxUrl: "/IQueryMetrics/ModalPolice",
        contentAjaxParams: {
            SOEID: row.SOEID,
            Name: row.Name,
            Status: row.Status,
            Approval: row.Approval,
            ApprovedDate: row.ApprovedDate,
            ApprovedBy: row.ApprovedBy,
            EmailReference: row.EmailReference,
            IQueryGroup: row.IQueryGroup
        },
        buttons: [{
            name: "Save Changes",
            class: "btn-info",
            onClick: function ($modal) {
                ////SAVE A NEW LOGID

                ////-------CREATE OBJECT-----------------------------------------------------------
                //var myObject = new Object();
                //myObject.lvidId = $("#selectedLVIDID").attr("idLVID");
                //myObject.p2plvid = $("#selectedP2PLVIDID").attr("idP2PLVID");
                //myObject.idState = $("#drp_state option:selected").val();
                //myObject.idAccount = $("#drp_account option:selected").val();
                //myObject.idJurisdiction = $("#drp_jurisdiction option:selected").val();
                //myObject.frequency = $("#drp_frequency option:selected").val();
                //myObject.responsableSoeid = $("#drp_responsable option:selected").val();
                //myObject.idType = $("#drp_type option:selected").val();
                //myObject.idCommodityCode = $("#drp_commodity option:selected").val();
                //myObject.monthDue = $("#drp_monthDue option:selected").val();
                //myObject.effectiveDate = $('#txt_effectiveDate').val();



                //myObject.dueDay = $("#drp_dueDate option:selected").val();


                //if ($("#chk_active").is(':checked')) {
                //    myObject.active = "1";
                //} else {
                //    myObject.active = "0";
                //}


                //if ($("#chk_ach").is(':checked')) {
                //    myObject.ckAch = "1";
                //} else {
                //    myObject.ckAch = "0";
                //}

                //myObject.vendorSiteCode = $('#txt_P2PVendorSiteCode').val();
                //myObject.vendorSupplier = $('#txt_P2PVendorNumber').val();


                //if ($("#drp_vendor option:selected").val() == "0") {
                //    myObject.idP2PVendorSite = "0";
                //} else {
                //    myObject.idP2PVendorSite = $("#drp_vendor option:selected").val();
                //}
                ////-------------------------------------------------------------------------------


                ////-------VALIDATE FIELDS---------------------------------------------------------


                //var myValidations = validateCompleteForm(myObject)


                //if (myValidations.result) {

                //    var jurisdiction = JSON.stringify(myObject);

                //    //INSERT NEW INFORMATION
                //    _callServer({
                //        url: '/SalesAndUse/JurisdictionInsert',
                //        closeModalOnClick: false,
                //        data: { 'pjson': jurisdiction },
                //        type: "post",
                //        success: function (savingStatus) {
                //            loadJurisdictionLocation();
                //            _showNotification("success", "Jurisdiction saved successfully.");
                //        }
                //    });
                //} else {
                //    _showNotification("error", myValidations.message);
                //}
            }
        }],
        onReady: function ($modal) {

            //if (_getUserInfo().Roles.includes('REVIEWER')) {
            //    $("#btnJurisdiction_AddNew").prop("disabled", false);
            //    $("#ModalJurisdiction_btn_0").prop("disabled", false);
            //} else {
            //    $("#btnJurisdiction_AddNew").prop("disabled", true);
            //    $("#ModalJurisdiction_btn_0").prop("disabled", true);
            //}


            //loadDropDowns(qry_state, '0', "drp_state");
            //loadDropDowns(qry_account, '0', "drp_account");
            //loadDropDowns(qry_jurisdiction, '0', "drp_jurisdiction");
            //loadDropDowns(qry_frequency, '0', "drp_frequency");
            //loadDropDowns(qry_responsable, '0', "drp_responsable");
            //loadDropDowns(qry_type, '0', "drp_type");
            //loadDropDowns(qry_commodity, '0', "drp_commodity");

            ////loadDropDowns(qry_duemonth, '0', "drp_monthDue");

            //loadDropDowns(qry_dueday, '0', "drp_dueDate");
            //loadDropDowns(qry_p2pvendor, '0', "drp_vendor");

            ////CREATE DROPLIST TABLE BUTTON
            //$("#jqxdropdownbutton_LVID").jqxDropDownButton({ template: "summer", width: "100%", height: 30 });
            //$("#jqxdropdownbutton_P2P_LVID").jqxDropDownButton({ template: "summer", width: "100%", height: 30 });

            //$("#txt_effectiveDate").datepicker();


            //loadDropDownLVIDs("0");
            //loadDropDownP2PLVIDs("0");

        },
        class: " compactModal"
    });



}


//#endregion