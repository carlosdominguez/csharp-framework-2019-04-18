﻿//#region DOCUMENT READY
$(document).ready(function () {
    $("#dateInputFrom").jqxDateTimeInput({ template: "summer", width: "100%", height: 34, formatString: "MM/dd/yyyy" });
    $("#dateInputTo").jqxDateTimeInput({ template: "summer", width: "100%", height: 34, formatString: "MM/dd/yyyy" });
    $("#txt_dueDateFrom").datepicker();
    $("#txt_dueDateFrom1").datepicker();
    loadReports();
});
//#endregion

function loadReports() {
    $.jqxGridApi.create({
        showTo: "#tblReports",
        options: {
            //for comments or descriptions
            height: "250",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: false,
            showfilterrow: true,
            editable: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_REPORTS]",
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'IsSelected', text: "Select", type: 'bool', columntype: 'checkbox', width: '50px', pinned: true, filterable: false },
            { name: 'reportID', type: 'string', hidden: true,  },
            { name: 'reportName', type: 'string', text: 'Report Name', width: '50%', editable: false },
            { name: 'reportSP', type: 'string', text: 'SP Related', width: '45%', editable: false },
            { name: 'active', type: 'string', hidden: true },
        ],
        ready: function () {
            _hideLoadingFullPage();
        }
    });
}

$("#btnExportExcel").click(function () {
    downloadQueryToExcel();
});


function downloadQueryToExcel() {
    var processFiles = _getCheckedRows('#tblReports');
    var dateFrom = $("#dateInputFrom").jqxDateTimeInput('getDate');
    var formattedDateFrom = $.jqx.dataFormat.formatdate(dateFrom, 'd');
    var dateTo = $("#dateInputTo").jqxDateTimeInput('getDate');
    var formattedDateTo = $.jqx.dataFormat.formatdate(dateTo, 'd');

    //var storeProcedure = $("#ddlReport :selected").val();
    
    for (var i = 0; i < processFiles.length; i++) {
        var sql = "";
        sql = processFiles[i].reportSP + " '" + formattedDateFrom + "', '" + formattedDateTo + "'";
        _downloadExcel({
            multiplesFiles: true,
            sql: sql,
            filename: "_IQuery_" + processFiles[i].reportName + "_" + _createCustomID() + ".xls",
            success: {
                msg: "Generating report " + processFiles[i].reportName + " ... Please Wait, this operation may take some time to complete."
            }
        });
    }
};

function _getCheckedRows(idTable) {
    var selectedRows = [];
    var processFiles = $(idTable).jqxGrid("getRows");

    for (var i = 0; i < processFiles.length; i++) {
        if (processFiles[i]["IsSelected"]) {
            selectedRows.push(processFiles[i]);
        }
    }

    return selectedRows;
}