/// <reference path="../../Shared/plugins/util/global.js" />
var _regionList = [];

function _getRegionsByCurrentUser(customOptions) {

    //Default Options.
    var options = {
        // RegionAdmin | ProofOwner | Responsible | AccountOwner | EscalationDB8 | EscalationDB10 | EscalationDB12
        accessBy: "Responsible",
        onSuccess: null
    };

    //Do merge with customOptions and options
    $.extend(true, options, customOptions);

    var sql =
        "SELECT \n" +
        "    DR.[ID] AS [IDRegion], \n" +
	    "    DR.[Region] \n" +
        "FROM  \n" +
        "    [dbo].[tblSDocRegionXUser] RU \n" +
        "        INNER JOIN [dbo].[tblSDocRegion] DR ON RU.[IDRegion] = DR.[ID] \n" +
        "WHERE  \n" +
        "    RU.[IsDeleted] = 0 AND RU.[UserSOEID] = '" + _getSOEID() + "' \n" +
        "UNION \n" +
        "SELECT DISTINCT \n" +
        "    DR.[ID] AS [IDRegion], \n" +
	    "    DR.[Region] \n" +
        "FROM \n" +
        "    [dbo].[tblSDocInfo] DI \n" +
        "        INNER JOIN [dbo].[tblSDocPeriod] DP ON DI.[ID] = DP.[IDDoc] \n" +
        "        INNER JOIN [dbo].[tblSDocRegion] DR ON DI.[IDRegion] = DR.[ID] \n" +
        "WHERE \n" +
        "   DI.[IsDeleted] = 0 AND DP.[IsDeleted] = 0 AND DR.[IsDeleted] = 0 ";

    if (options.accessBy == "ProofOwner") {
        sql += " AND DI.[ProofOwnerSOEID] LIKE '%" + _getSOEID() + "%'";
    }

    if (options.accessBy == "Responsible") {
        sql += " AND DI.[ResponsibleSOEID] LIKE '%" + _getSOEID() + "%'";
    }

    if (options.accessBy == "RegionAdmin") {
        var regionsOfUser = [];
        for (var i = 0; i < _regionList.length; i++) {
            regionsOfUser.push({
                IDRegion: _regionList[i].IDRegion,
                Region: _regionList[i].Region
            });
        }

        if (options.onSuccess) {
            options.onSuccess(regionsOfUser);
        }
    } else {
        _callServer({
            loadingMsgType: "fullLoading",
            loadingMsg: "Loading regions of user...",
            url: '/Shared/ExecQuery',
            data: { 'pjsonSql': _toJSON(sql) },
            type: "post",
            success: function (regionsOfUser) {
                if (options.onSuccess) {
                    options.onSuccess(regionsOfUser);
                }
            }
        });
    }

    
}

function _loadSelectOfRegion(customOptions) {

    //Default Options.
    var options = {
        // RegionAdmin | ProofOwner | Responsible | AccountOwner | EscalationDB8 | EscalationDB10 | EscalationDB12
        accessBy: "Responsible", 
        onSuccess: null
    };

    //Do merge with customOptions and options
    $.extend(true, options, customOptions);

    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Getting regions...",
        name: "[dbo].[spSDocAdminRegion]",
        params: [
            { "Name": "@Action", "Value": "List" }
        ],
        success: {
            fn: function (responseList) {
                //Filter by RegionAdmin
                if (options.accessBy == "RegionAdmin") {
                    var currentUserSOEID = _getSOEID();
                    for (var i = 0; i < responseList.length; i++) {
                        var objTempRegion = responseList[i];
                        var tempUsersOfRegion = JSON.parse(objTempRegion.UserJson);
                        var objUserInRegion = _findOneObjByProperty(tempUsersOfRegion, "UserSOEID", currentUserSOEID);
                        if (objUserInRegion) {
                            _regionList.push(objTempRegion);
                        }
                    }
                } else {
                    //Add region list to validate upload of excel template
                    _regionList = responseList;
                }
                
                //Get regions of current user
                _getRegionsByCurrentUser({
                    accessBy: options.accessBy,
                    onSuccess: function (regionsOfUser) {
                        var idRegionSelected = 0;
                        if (regionsOfUser.length > 0) {
                            idRegionSelected = regionsOfUser[0].IDRegion;
                        }

                        //Add Default Option
                        $("#selRegion").append($('<option>', {
                            value: "0",
                            text: "-- Select --"
                        }));

                        //Add Options
                        for (var i = 0; i < _regionList.length; i++) {
                            var objTemp = _regionList[i];
                            var regionDisabled = true;

                            //Validate if user have access to the region
                            if (regionsOfUser.length > 0) {
                                var objTempRegionAccess = _findOneObjByProperty(regionsOfUser, "IDRegion", objTemp.IDRegion);
                                if (objTempRegionAccess) {
                                    regionDisabled = false;
                                }
                            }

                            var optionAttrs = {
                                value: objTemp.IDRegion,
                                text: objTemp.Region,
                                selected: (objTemp.IDRegion == idRegionSelected)
                            };

                            //if (regionDisabled) {
                            //    optionAttrs.disabled = true;
                            //}

                            if (regionDisabled == false) {
                                $("#selRegion").append($('<option>', optionAttrs));
                            }
                        }

                        //Show message if user don't have any region permits
                        if (_regionList.length == 0 && regionsOfUser.length == 0) {
                            _showAlert({
                                type: 'error',
                                content: "You don't have any region assigned please contact the system administrator.",
                                animateScrollTop: true
                            });
                        }

                        //Show alert to user if regions not exists
                        //if (_regionList.length == 0) {
                        //    _showAlert({
                        //        type: 'error',
                        //        content: "Please go to 'Regions & Emails' in the menu and add one Region as User Admin at least.",
                        //        animateScrollTop: true
                        //    });
                        //} else {
                            
                        //}

                        //Load Periods
                        _loadSelectOfPeriodsByRegion({
                            idRegion: $("#selRegion").val(),
                            onSuccess: function () {
                                if (options.onSuccess) {
                                    options.onSuccess(_regionList);
                                }
                            }
                        });
                    }
                });
            }
        }
    });
}

function _loadSelectOfPeriodsByRegion(customOptions) {

    //Default Options.
    var options = {
        idRegion: 0,
        idSelectedPeriod: 0,
        onSuccess: null
    };

    //Do merge with customOptions and options
    $.extend(true, options, customOptions);

    if (options.idRegion && options.idRegion != "0") {
        var sql =
        " SELECT \n " +
        "     FORMAT(CONVERT(DATE, CONVERT(VARCHAR, [Year]) + '/' + CONVERT(VARCHAR, [Month]) + '/01'), 'yyyyMM')		 AS [id], \n " +
	    "     [Year]																									 AS [year],  \n " +
	    "     [Month]																									 AS [monthId],  \n " +
	    "     FORMAT(CONVERT(DATE, CONVERT(VARCHAR, [Year]) + '/' + CONVERT(VARCHAR, [Month]) + '/01'), 'MMMM')		     AS [monthName], \n " +
	    "     FORMAT(CONVERT(DATE, CONVERT(VARCHAR, [Year]) + '/' + CONVERT(VARCHAR, [Month]) + '/01'), 'MMMM yyyy')     AS [period], \n " +
	    "     COUNT(1)																								     AS [rows] \n " +
        " FROM  \n " +
        "     [dbo].[tblSDocPeriod] DP \n " +
        "         INNER JOIN [dbo].[tblSDocInfo] D ON DP.[IDDoc] = D.[ID] \n " +
        " WHERE \n " +
        "     D.[IsDeleted] = 0 AND \n " +
        "     DP.[IsDeleted] = 0 AND \n " +
        "     D.[IDRegion] = " + options.idRegion + " \n " +
        " GROUP BY \n " +
        "     DP.[Year], DP.[Month] \n " +
        " ORDER BY \n " +
        "     DP.[Year], DP.[Month]";

        _callServer({
            loadingMsgType: "fullLoading",
            loadingMsg: "Loading Periods Records...",
            url: '/Shared/ExecQuery',
            data: { 'pjsonSql': _toJSON(sql) },
            type: "post",
            success: function (monthListDB) {

                //Get default months by current month
                var monthDiff = 4;
                if (monthListDB.length > 0) {
                    var tempMonthDiff = moment().diff(moment(monthListDB[0].period, "MMMM YYYY"), 'months', false);
                    if (tempMonthDiff > monthDiff) {
                        monthDiff = tempMonthDiff;
                    }
                }

                var defaultMonths = _loadMonths({
                    cantBackMonths: monthDiff,
                    cantForwardMonths: 2
                });

                //Do a merge of months
                for (var i = 0; i < monthListDB.length; i++) {
                    var tempMonth = _findOneObjByProperty(defaultMonths, "id", monthListDB[i].id);
                    if (tempMonth) {
                        tempMonth.rows = monthListDB[i].rows;
                    } else {
                        defaultMonths.push(monthListDB[i]);
                    }
                }

                //Sort array by id
                defaultMonths.sort(function (a, b) {
                    if (parseFloat(a.id) < parseFloat(b.id)) {
                        return 1;
                    }
                    if (parseFloat(a.id) > parseFloat(b.id)) {
                        return -1;
                    }
                    return 0;
                });

                //Remove old options
                $("#selPeriod").contents().remove();
                var currentMonthId = moment().format('YYYYMM');
                if(options.idSelectedPeriod){
                    currentMonthId = moment(options.idSelectedPeriod, "MMMM YYYY").format('YYYYMM');
                }

                //Create options in select
                for (var i = 0; i < defaultMonths.length; i++) {
                    var objTemp = defaultMonths[i];

                    $("#selPeriod").append($('<option>', {
                        year: objTemp.year,
                        month: objTemp.monthId,
                        value: objTemp.period,
                        text: objTemp.year + " " + objTemp.monthName + ' (' + objTemp.rows + ' records)',
                        selected: (objTemp.id == currentMonthId)
                    }));
                }

                if (options.onSuccess) {
                    options.onSuccess();
                }
            }
        });
    } else {
        $("#selPeriod").append($('<option>', {
            value: 0,
            text: "-- Select Region --"
        }));

        if (options.onSuccess) {
            options.onSuccess();
        }
    }
}

function _loadDocumentTableList(customOptions) {
    var filters = _getCurrentFilters();

    //Default Options.
    var options = {
        grouped: false,
        onSuccess: null
    };

    //Do merge with customOptions and options
    $.extend(true, options, customOptions);

    //Add Filters to table
    var strWhere = "WHERE 1 = 0";
    if (filters.IDRegion && filters.IDRegion != "0" && filters.Year && filters.Month) {
        strWhere =
            "WHERE " +
            "   [IDRegion] = " + filters.IDRegion + " AND " +
            "   [Year] = " + filters.Year + " AND " +
            "   [Month] = " + filters.Month;
    }
    if (filters.ProofOwnerSOEID) {
        strWhere += " AND [ProofOwnerSOEID] LIKE '%" + filters.ProofOwnerSOEID + "%'";
    }
    if (filters.ResponsibleSOEID) {
        strWhere += " AND [ResponsibleSOEID] LIKE '%" + filters.ResponsibleSOEID + "%'";
    }
    if (filters.AccountOwnerSOEID) {
        strWhere += " AND [AccountOwnerSOEID] LIKE '%" + filters.AccountOwnerSOEID + "%'";
    }
    if (filters.FileStatus) {
        strWhere += " AND [FileStatus] LIKE '" + filters.FileStatus + "'";
    }
    if (filters.StatusByPO) {
        strWhere += " AND [StatusByPO] LIKE '" + filters.StatusByPO + "'";
    }
    
    var selRegion = $("#selRegion").find("option:selected").text();
    var groupColoumns = [];

    //if (options.grouped) {
    //    groupColoumns = ['CategoryLevel1', 'FileName'];
    //    //if (_contains(selRegion, "MEXICO")) {
    //    //    groupColoumns = ['CategoryLevel1', 'CategoryLevel3', 'FileName'];
    //    //}
    //}

    $.jqxGridApi.create({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        showTo: "#tblDocs",
        options: {
            //for comments or descriptions
            height: "500",
            autoheight: false,
            autorowheight: false,
            showfilterrow: true,
            sortable: true,
            editable: true,
            groupable: true,
            //'singlerow', 'multiplerows', 'checkbox', 'multiplerowsadvanced' or 'multiplerowsextended'.
            selectionmode: "singlerow"
        },
        sp: {
            Name: "[dbo].[spSDocPeriodListPaging]",
            Params: [
                { Name: "@StrWhere", Value: strWhere },
                { Name: "@EndIndexRow", Value: 99000000 }
            ],
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set"
        },
        groups: groupColoumns,
        columns: [
            //type: string - text - number - int - float - date - time 
            //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
            //cellsformat: ddd, MMM dd, yyyy h:mm tt
            { name: 'IDDoc', type: 'number', hidden: true },
            { name: 'IDDocPeriod', type: 'number', hidden: true },
            { name: 'JsonHistory', type: 'string', hidden: true },
            { name: 'IDRegion', type: 'number', hidden: true },
            { name: 'FileFullPath', type: 'string', hidden: true },
            { name: 'ResponsibleDesc', type: 'string', hidden: true },
            {
                name: 'FileStatus', text: 'File Status', width: '145px', type: 'string', pinned: true, filtertype: 'checkedlist', editable: false, filterable: true, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                    var dataRecord = $("#tblDocs").jqxGrid('getrowdata', rowIndex);
                    var htmlResult = '';
                    
                    //Add Status
                    switch (dataRecord.FileStatus) {
                        case "Received":
                            htmlResult += '<span class="badge badge-md badge-info" style="margin-top: 5px; margin-left: 5px;"> Received </span>';
                            break;

                        case "Not Received":
                            htmlResult += '<span class="badge badge-md badge-warning" style="margin-top: 5px; margin-left: 5px;"> Not Received </span>';
                            break;

                        default:
                            htmlResult += '<span class="badge badge-md badge-warning" style="margin-top: 5px; margin-left: 5px;">' + dataRecord.FileStatus + '</span>';
                            break;
                    }

                    //Add History
                    if (dataRecord.JsonHistory) {
                        var historyRows = JSON.parse(dataRecord.JsonHistory);
                        if (historyRows.length > 0) {
                            var historyInfoHtml = "<div style='padding: 10px; font-size: 15px; max-height: 500px; overflow-y:scroll; width: 520px;'>";

                            for (var i = 0; i < historyRows.length; i++) {
                                var tempData = historyRows[i];

                                historyInfoHtml += "<b>User:</b> (" + tempData.CreatedBySOEID + ") " + tempData.CreatedByName + "<br>";
                                historyInfoHtml += "<b>File Status:</b> " + tempData.FileStatus + "<br>";
                                historyInfoHtml += "<b>Status By PO:</b> " + tempData.StatusByPO + "<br>";

                                if (tempData.Comment) {
                                    historyInfoHtml += "<b>Date:</b> " + tempData.CreatedDate + "<br>";
                                    historyInfoHtml += "<b>Comment:</b> " + tempData.Comment + " <br><br>";
                                } else {
                                    historyInfoHtml += "<b>Date:</b> " + tempData.CreatedDate + " (EDT Eastern Daylight Time)<br><br>";
                                }
                            }

                            historyInfoHtml += "</div>";

                            htmlResult +=
                                '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="right" data-content="' + historyInfoHtml + '" data-title="History Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png"/> ' +
                                '</div>';
                        }
                    }

                    return htmlResult;
                }
            },
            {
                name: 'StatusByPO', text: 'Status By PO', width: '100px', type: 'string', pinned: true, filtertype: 'checkedlist', editable: false, filterable: true, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                    var dataRecord = $("#tblDocs").jqxGrid('getrowdata', rowIndex);
                    var htmlResult = '';

                    switch (dataRecord.StatusByPO) {
                        case "Approved":
                            htmlResult = '<span class="badge badge-md badge-success" style="margin-top: 5px; margin-left: 5px;"> Approved </span>';
                            break;

                        case "Rejected":
                            htmlResult = '<span class="badge badge-md badge-danger" style="margin-top: 5px; margin-left: 5px;"> Rejected </span>';
                            break;

                        default:
                            htmlResult = '<span class="badge badge-md badge-warning" style="margin-top: 5px; margin-left: 5px;">' + dataRecord.StatusByPO + '</span>';
                            break;
                    }

                    return htmlResult;
                }
            },
            { name: 'CategoryLevel1', text: 'CategoryLevel1', type: 'string', width: '135px', filtertype: 'checkedlist' },
            { name: 'CategoryLevel2', text: 'CategoryLevel2', type: 'string', width: '135px', filtertype: 'checkedlist' },
            { name: 'CategoryLevel3', text: 'CategoryLevel3', type: 'string', width: '135px', filtertype: 'checkedlist' },
            { name: 'FileName', text: 'File Name', width: '300px', type: 'string', filtertype: 'checkedlist',editable :false },
            { name: 'Extension', text: 'Ext', width: '80px', type: 'string', filtertype: 'checkedlist', editable: false },
            { name: 'FullKey', text: 'Full Key', width: '360px', type: 'string', filtertype: 'input' },
            { name: "Balance", text: "Balance", width: '110px', type: 'float', filtertype: 'number', cellsformat: 'd' },
            { name: 'FileFolderPath', text: 'Folder Link', width: '1200px', type: 'string', filtertype: 'input' },
            { name: 'ProofOwnerSOEID', text: 'Proof Owner SOEID', width: '150px', type: 'string', filtertype: 'input' },
            { name: 'ProofOwnerBackupSOEID', text: 'Proof Owner Backup SOEID', width: '150px', type: 'string', filtertype: 'input' },
            { name: 'ResponsibleSOEID', text: 'Responsible SOEID', width: '150px', type: 'string', filtertype: 'input' },
            { name: 'AccountOwnerSOEID', text: 'Account Owner SOEID', width: '150px', type: 'string', filtertype: 'input' },
            { name: 'Year', text: 'Year', width: '70px', type: 'string', filtertype: 'input' },
            { name: 'Month', text: 'Month', width: '70px', type: 'string', filtertype: 'input' }
        ],
        ready: function () {
            //On select row event
            //$("#tblMakerCheckerIssues").on('rowselect', function (event) {
            //    $(".btnViewIssue").attr("href", _getViewVar("SubAppPath") + "/MEIL/MakerChecker/AdminIssue?pissueID=" + event.args.row.IssueID);
            //});
            
            var $jqxGrid = $("#tblDocs");

            //Add Popup to see comments
            _GLOBAL_SETTINGS.tooltipsPopovers();
            $jqxGrid.on("rowclick", function (event) {
                _GLOBAL_SETTINGS.tooltipsPopovers();
            });
        }
    });
}

function _getCurrentFilters() {
    var filters = {
        IDRegion: null,
        Year: null,
        Month: null,
        ProofOwnerSOEID: null,
        ResponsibleSOEID: null,
        AccountOwnerSOEID: null,
        FileStatus: null,
        StatusByPO: null
    };
    var idRegion = $("#selRegion").val();
    var year = $("#selPeriod").find("option:selected").attr("year");
    var month = $("#selPeriod").find("option:selected").attr("month");
    var fileStatus = $("#selFileStatus").val();
    var statusByPO = $("#selStatusByPO").val();

    //Get Region Filter
    if (typeof idRegion == "undefined" || idRegion == null) {
        idRegion = "0";
    }
    filters["IDRegion"] = idRegion;
    
    //Get Year Filter
    filters["Year"] = year;

    //Get Month Filter
    filters["Month"] = month;

    //Get Proof Owner SOEID Filter
    filters["ProofOwnerSOEID"] = $("#selPO select").val();

    //Get Responsible SOEID Filter
    filters["ResponsibleSOEID"] = $("#selResponsible select").val();

    //Get AccountOwner SOEID Filter
    if ($("#HFViewType").val() == "AccountOwner") {
        filters["AccountOwnerSOEID"] = _getSOEID();
    }

    //Get File Status Filter
    if (fileStatus != "All") {
        filters["FileStatus"] = fileStatus;
    }

    //Get Status By PO Filter
    if (statusByPO != "All") {
        filters["StatusByPO"] = statusByPO;
    }

    if (!filters["Year"]) {
        filters["Year"] = "0";
    }

    if (!filters["Month"]) {
        filters["Month"] = "0";
    }

    return filters;
}

function _checkFileExistsInShareDrive(customOptions) {

    //Default Options.
    var options = {
        onSuccess: null
    };

    //Do merge with customOptions and options
    $.extend(true, options, customOptions);

    //Get current filters
    var filters = _getCurrentFilters();

    //When is DocumentEscalation Check only current docs for PO
    if (_contains(window.location.href, "DocumentEscalation")) {
        filters.ProofOwnerSOEID = _getSOEID();
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Checking documents in Share Folder...",
        url: '/SupportDoc/CheckFilesInShareDrive',
        data: {
            proofOwnerSOEID: filters.ProofOwnerSOEID,
            responsibleSOEID: filters.ResponsibleSOEID,
            idRegion: filters.IDRegion,
            year: filters.Year,
            month: filters.Month,
            typeAuthentication: _getTypeAuthentication(),
            user: _encodeSkipSideminder(_getWindowsUser()),
            pass: _getWindowsPassword()
        },
        success: function (resultMsg) {
            if (options.onSuccess) {
                _showNotification("info", resultMsg, "CheckNotification");

                options.onSuccess();
            }
        }
    });

    //Faster option
    //_callProcedure({
    //    loadingMsgType: "fullLoading",
    //    loadingMsg: "Checking documents in Share Folder...",
    //    name: "[dbo].[spSDocCheckFileExists]",
    //    params: [
    //        { "Name": "@SessionSOEID", "Value": _getSOEID() },
    //        { "Name": "@Year", "Value": $("#selPeriod").find("option:selected").attr("year") },
    //        { "Name": "@Month", "Value": $("#selPeriod").find("option:selected").attr("month") }
    //    ],
    //    success: {
    //        fn: function (responseList) {
    //            if (fnOnSuccess) {
    //                fnOnSuccess();
    //            }
    //        }
    //    }
    //});
}

function _insertAuditLog(options) {
    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Creating audit log...",
        name: "[dbo].[spSDocAuditAdmin]",
        params: [
            { "Name": "@TypeAction", "Value": "Insert" },
            { "Name": "@AuditAction", "Value": options.Action },
            { "Name": "@Description", "Value": options.Description },
            { "Name": "@CreatedBy", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                if (options.onSuccess) {
                    options.onSuccess();
                }
            }
        }
    });
}

//------------------------------------------------------
//Authentication methods
//------------------------------------------------------
function _setWindowsUser(user) {
    localStorage.setItem("WindowsUser", user);
}

function _getWindowsUser() {
    var userID = "";
    if (localStorage.getItem("WindowsUser")) {
        userID = localStorage.getItem("WindowsUser");
    }

    return userID;
}

function _setWindowsPassword(pass) {
    localStorage.setItem("WindowsPassword", _encodeAsciiString(pass));
}

function _getWindowsPassword(decodePass) {
    var pass = "";
    if (localStorage.getItem("WindowsPassword")) {
        pass = localStorage.getItem("WindowsPassword");

        if (decodePass) {
            pass = _decodeAsciiString(pass);
        }
    }

    return pass;
}

function _setTypeAuthentication(auth) {
    localStorage.setItem("TypeAuthentication", auth);
}

function _getTypeAuthentication() {
    var auth = "ToolAuthentication";
    if (localStorage.getItem("TypeAuthentication")) {
        auth = localStorage.getItem("TypeAuthentication");
    }

    return auth;
}
