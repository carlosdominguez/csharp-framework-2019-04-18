/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="SupportDocGlobal.js" />
$(document).ready(function () {

    //Load regions information
    _loadSelectOfRegion({
        accessBy: "RegionAdmin",
        onSuccess: function () {
            //Load table
            _loadDocumentTableList({
                grouped: true
            });

            //Update Delete Text button
            updateBtnDeleteText();
        }
    });

    //On Change Region
    $("#selRegion").change(function () {
        // Load Periods
        var idRegion = $("#selRegion").val();
        _loadSelectOfPeriodsByRegion({
            idRegion: idRegion,
            onSuccess: function () {
                //Load table
                _loadDocumentTableList({
                    grouped: true
                });

                //Update Delete Text button
                updateBtnDeleteText();
            }
        });
    });

    //On Change Period
    $("#selPeriod").change(function () {
        //Load table
        _loadDocumentTableList({
            grouped: true
        });

        //Update Delete Text button
        updateBtnDeleteText();
    });

    //On Click Download Template
    $(".btnDownloadTemplate").click(function () {
        var idRegion = $("#selRegion").val();
        var regionName = $("#selRegion").find("option:selected").text();
        var year = $("#selPeriod").find("option:selected").attr("year");
        var month = $("#selPeriod").find("option:selected").attr("month");

        if (idRegion && idRegion != "0") {
            if (year && month) {
                var strWhere = "WHERE [IDRegion] = " + idRegion + " AND [Year] = " + year + " AND [Month] = " + month;

                _downloadExcel({
                    spName: "[AccountOwnerShip].[dbo].[spSDocPeriodListPagingExcel]",
                    spParams: [
                        { "Name": "@StrWhere", "Value": strWhere },
                        { "Name": "@EndIndexRow", "Value": 99000000 },
                    ],
                    filename: "SuportDocTemplate_" + regionName + "_" + moment().format('YYYY-MM-DD'),
                    sheetname: "TOUPLOAD",
                    success: {
                        msg: "Please wait, generating report..."
                    }
                });
            }else{
                _showAlert({
                    id: "UploadActivityMessage",
                    type: "error",
                    content: "Please select Period"
                });
            }
            
        } else {
            _showAlert({
                id: "UploadActivityMessage",
                type: "error",
                content: "Please select Region"
            });
        }
    });

    //On Click Delete Data
    $(".btnDeleteData").click(function () {
        deleteDataByRegionPeriod();
    });

    //Create to import excel
    XLSX.createJsXlsx({
        idElement: "js-xlsx-plugin",
        fileInput: {
            dropText: "Drop an Excel file here...",
            hideDrop: false,
            inputText: "... or click here to select a file"
        },
        onSuccess: function (excelSource) {

            isValidExcel({
                excelSource: excelSource,
                onSuccess: function (excelSourceValidated) {
                    _callServer({
                        loadingMsgType: "fullLoading",
                        loadingMsg: "Uploading data...",
                        url: '/SupportDoc/UploadDocData',
                        data: {
                            'pjsonData': JSON.stringify(excelSourceValidated),
                            'pidRegion': $("#selRegion").val(),
                            'pyear': $("#selPeriod").find("option:selected").attr("year"),
                            'pmonth': $("#selPeriod").find("option:selected").attr("month")
                        },
                        type: "post",
                        success: function (json) {
                            //Clear input file
                            $(".jsxlsx-input-file").val("");

                            //Check if file exist
                            _checkFileExistsInShareDrive({
                                onSuccess: function () {
                                    // Load Periods
                                    var idRegion = $("#selRegion").val();
                                    var idSelectedPeriod = $("#selPeriod").val();
                                    _loadSelectOfPeriodsByRegion({
                                        idRegion: idRegion,
                                        idSelectedPeriod: idSelectedPeriod,
                                        onSuccess: function () {
                                            //Load table
                                            _loadDocumentTableList({
                                                grouped: true
                                            });

                                            //Update Delete Text button
                                            updateBtnDeleteText();
                                        }
                                    });
                                }
                            });
                        },
                        error: function () {
                            //Clear input file
                            $(".jsxlsx-input-file").val("");
                        }
                    });
                },
                onError: function (excelSourceValidated) {

                    //Clear input file
                    $(".jsxlsx-input-file").val("");
                }
            });
        }
    });

});

function deleteDataByRegionPeriod() {
    var selRegionName = $("#selRegion").find("option:selected").text();
    var selPeriod = $("#selPeriod").find("option:selected").text();
    var htmlContentModal = "<b>Region: </b>" + selRegionName + "<br/>";
    htmlContentModal += "<b>Period: </b>" + selPeriod + "<br/>";

    _showModal({
        width: '35%',
        modalId: "modalDelApp",
        addCloseButton: true,
        buttons: [{
            name: "Delete",
            class: "btn-danger",
            onClick: function () {
                var filters = _getCurrentFilters();

                _callProcedure({
                    loadingMsgType: "topBar",
                    loadingMsg: "Deleting data...",
                    name: "[dbo].[spSDocDeleteDocPeriod]",
                    params: [
                        { "Name": "@IDRegion", "Value": filters.IDRegion },
                        { "Name": "@Year", "Value": filters.Year },
                        { "Name": "@Month", "Value": filters.Month },
                        { "Name": "@SessionSOEID", "Value": _getSOEID() }
                    ],
                    success: {
                        fn: function (responseList) {
                            //Refresh data
                            var idRegion = $("#selRegion").val();
                            var idSelectedPeriod = $("#selPeriod").val();
                            _loadSelectOfPeriodsByRegion({
                                idRegion: idRegion,
                                idSelectedPeriod: idSelectedPeriod,
                                onSuccess: function () {
                                    //Load table
                                    _loadDocumentTableList({
                                        grouped: true
                                    });

                                    //Update Delete Text button
                                    updateBtnDeleteText();
                                }
                            });

                            //Show user message
                            _showNotification("success", "Data was deleted successful for '" + selRegionName + " " + selPeriod + "'.");
                        }
                    }
                });
            }
        }],
        title: "Are you sure you want to delete the data?",
        contentHtml: htmlContentModal
    });
    
}

function updateBtnDeleteText() {
    var selRegionName = $("#selRegion").find("option:selected").text();
    var selPeriod = $("#selPeriod").find("option:selected").text();

    if (_contains(selPeriod, "(0 records)")) {
        $(".btnDeleteData").hide();
    } else {
        var btnDeleteText = "Delete data for '" + selRegionName + " " + selPeriod + "'";
        $(".btnDeleteText").html(btnDeleteText);
        $(".btnDeleteData").show();
    }
}

function isValidExcel(customOptions) {

    //Default Options.
    var options = {
        excelSource: [],
        onSuccess: null,
        onError: null
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    var excelSource = options.excelSource;
    var isValid = true;
    var sheetName = "TOUPLOAD";
    var allowedColumns = [
        { "Region": true },
        { "FullKey": true },
        { "Balance": true },
        { "NotificationStatus": true },
        { "FileName": true },
        { "Extension": true },
        { "ReconciliationType": true },
        { "CategoryLevel1": true },
        { "CategoryLevel2": true },
        { "CategoryLevel3": true },
        { "ProofOwnerDesc": true },
        { "ProofOwnerSOEID": true },
        { "ProofOwnerBackupDesc": true },
        { "ProofOwnerBackupSOEID": true },
        { "ResponsibleDesc": true },
        { "ResponsibleSOEID": true },
        { "AccountOwnerDesc": true },
        { "AccountOwnerSOEID": true },
        { "Escalation1": true },
        { "Escalation2": true },
        { "Escalation3": true }
    ];

    //Validate if Sheet with name sheetName exists
    if (!excelSource[sheetName]) {
        _showAlert({
            id: "UploadActivityMessage",
            type: 'error',
            content: "Please review the Excel file that exists the Sheet Name '<b>" + sheetName + "</b>' with the data to upload.",
            animateScrollTop: true
        });

        isValid = false;
    }

    //Validate columns
    if (isValid) {
        var excelColumnsKeys = Object.keys(excelSource[sheetName][0]);
        var missingCols = [];
        var extraCols = [];

        //Validate if the colums allowed are defined in the excel data
        for (var i = 0; i < allowedColumns.length; i++) {
            var allowedColumExist = false;
            var objAllowCol = allowedColumns[i];
            var keysAllowColNames = Object.keys(objAllowCol);
            var defaultKeyAllowColName = "";

            //When is Escalation1, Escalation2, Escalation3 ignore because sometimes
            //comes empty in the array of excel but exist in the excel file
            if (objAllowCol.Ignore && objAllowCol.Ignore == 1) {
                allowedColumExist = true;
            } else {
                for (var j = 0; j < keysAllowColNames.length && allowedColumExist == false; j++) {
                    var keyAllowColName = keysAllowColNames[j];

                    //Set default Key
                    if (objAllowCol[keyAllowColName]) {
                        defaultKeyAllowColName = keyAllowColName;
                    }

                    //Look if allowed column exist in excel columns
                    for (var k = 0; k < excelColumnsKeys.length; k++) {
                        var excelCol = excelColumnsKeys[k];

                        if (excelCol.toLocaleLowerCase() == keyAllowColName.toLocaleLowerCase()) {
                            allowedColumExist = true;
                            break;
                        }
                    }
                }
            }

            if (allowedColumExist == false) {
                missingCols.push(defaultKeyAllowColName);
            }
        }

        //Validate if the colums of the excel are allowed colums
        for (var i = 0; i < excelColumnsKeys.length; i++) {
            var excelCol = excelColumnsKeys[i];
            var excelColAllowed = false;

            for (var k = 0; k < allowedColumns.length && excelColAllowed == false; k++) {
                var objAllowCol = allowedColumns[k];
                var keysAllowColNames = Object.keys(objAllowCol);

                for (var j = 0; j < keysAllowColNames.length; j++) {
                    var keyAllowColName = keysAllowColNames[j];

                    if (excelCol.toLocaleLowerCase() == keyAllowColName.toLocaleLowerCase()) {
                        excelColAllowed = true;
                        break;
                    }
                }
            }

            if (excelColAllowed == false) {
                extraCols.push(excelCol);
            }
        }

        if (extraCols.length > 0 || missingCols.length > 0) {
            var msg = "<b>Excel file must have the following column names:</b> <br>";
            var flagCloseUl;
            var groupAllowColsEvery = 7;

            for (var i = 0; i < allowedColumns.length; i++) {
                var objAllowCol = allowedColumns[i];
                var keysAllowColNames = Object.keys(objAllowCol);

                for (var j = 0; j < keysAllowColNames.length; j++) {
                    var keyAllowColName = keysAllowColNames[j];

                    if ((i % groupAllowColsEvery) == 0) {
                        msg += "<ul class='col-md-4'>";
                        flagCloseUl = false;
                    }

                    //Set default Key
                    if (objAllowCol[keyAllowColName] === true) {
                        msg += "<li>" + (i + 1) + ":" + keyAllowColName + "</li>";
                    }

                    if (((i + 1) % groupAllowColsEvery) == 0) {
                        msg += "</ul>";
                        flagCloseUl = true;
                    }
                }
            }

            if (flagCloseUl == false) {
                msg += "</ul>";
            }

            msg += "<div class='clearfix'></div><br>";
            if (extraCols.length > 0) {
                msg += "<b>Please remove the following extra columns in the Excel file (review hidden colums): </b><br>";
                msg += "<ul class='col-md-4'>";
                for (var i = 0; i < extraCols.length; i++) {
                    msg += "<li>" + extraCols[i] + "</li>";
                }
                msg += "</ul>";
                msg += "<div class='clearfix'></div><br>";
            }

            if (missingCols.length > 0) {
                msg += "<b>Please add the following columns in the Excel file: </b><br>";
                msg += "<ul class='col-md-4'>";
                for (var i = 0; i < missingCols.length; i++) {
                    msg += "<li>" + missingCols[i] + "</li>";
                }
                msg += "</ul>";
                msg += "<div class='clearfix'></div>";
            }

            _showAlert({
                id: "UploadActivityMessage",
                type: 'error',
                content: msg,
                animateScrollTop: true
            });
            isValid = false;
        }
    }

    //Validate Excel Data
    if (isValid) {
        var excelRows = excelSource[sheetName];
        var selectedRegionName = $("#selRegion").find("option:selected").text();
        var columnsRequired = [
            "FullKey",
            "Balance",
            "NotificationStatus",
            "FileName",
            "Extension",
            "ReconciliationType",
            "CategoryLevel1"
        ];

        //Clean and validate excel data
        validateExcelData({
            excelRows: excelRows,
            columnsRequired: columnsRequired,
            columnsOptional: [
                "CategoryLevel2",
                "CategoryLevel3"
            ],
            columnsSOEID: [
                "ProofOwnerSOEID",
                "ProofOwnerBackupSOEID",
                "ResponsibleSOEID",
                "AccountOwnerSOEID",
                "Escalation1",
                "Escalation2",
                "Escalation3"
            ],
            onCleanDataRow: function(objExcelRow){
                //Excel columns
                var excelCols = Object.keys(objExcelRow);

                //Clean data in excel
                for (var i = 0; i < excelCols.length; i++) {
                    var tempColName = excelCols[i];

                    //Convert Balance to number
                    if (tempColName == "Balance") {
                        //Fix negative values with ()
                        if (_contains(objExcelRow[tempColName], "(")) {
                            objExcelRow[tempColName] = _replaceAll("(", "-", objExcelRow[tempColName]);
                            objExcelRow[tempColName] = _replaceAll(")", "", objExcelRow[tempColName]);
                        }

                        //Fix comma in numbers (,)
                        objExcelRow[tempColName] = _replaceAll(",", "", objExcelRow[tempColName]);

                        //Fix Parse exponential numbers "-3.11625E+11"
                        objExcelRow[tempColName] = parseFloat(objExcelRow[tempColName]);

                        //Fix "NaN" values
                        if (isNaN(objExcelRow[tempColName])) {
                            objExcelRow[tempColName] = "0";
                        }
                    }

                    //Fix issue with invalid characters (\ / : * ? " < > |) in File Name
                    if (tempColName == "FileName" ||
                        tempColName == "CategoryLevel1" || 
                        tempColName == "CategoryLevel2" ||
                        tempColName == "CategoryLevel3") {
                        objExcelRow[tempColName] = _replaceAll("\\", "", objExcelRow[tempColName]);
                        objExcelRow[tempColName] = _replaceAll("/", "", objExcelRow[tempColName]);
                        objExcelRow[tempColName] = _replaceAll(":", "", objExcelRow[tempColName]);
                        objExcelRow[tempColName] = _replaceAll("*", "", objExcelRow[tempColName]);
                        objExcelRow[tempColName] = _replaceAll("?", "", objExcelRow[tempColName]);
                        objExcelRow[tempColName] = _replaceAll('"', '', objExcelRow[tempColName]);
                        objExcelRow[tempColName] = _replaceAll("<", "", objExcelRow[tempColName]);
                        objExcelRow[tempColName] = _replaceAll(">", "", objExcelRow[tempColName]);
                        objExcelRow[tempColName] = _replaceAll("|", "", objExcelRow[tempColName]);
                    }

                    //Fix uppercase extentions 
                    if (tempColName == "Extension") {
                        objExcelRow[tempColName] = objExcelRow[tempColName].toLowerCase();
                    }

                    //Remove enters characters
                    objExcelRow[tempColName] = _replaceAll("\r", "", objExcelRow[tempColName]);
                    objExcelRow[tempColName] = _replaceAll("\n", "", objExcelRow[tempColName]);

                    //Remove tabs characters
                    objExcelRow[tempColName] = _replaceAll("\t", "", objExcelRow[tempColName]);

                    //Trim all data
                    objExcelRow[tempColName] = $.trim(objExcelRow[tempColName]);
                }

                return objExcelRow;
            },
            onValidateRow: function (excelRowNumber, objTempRow) {
                var result = "";

                //Validate Region
                if (objTempRow["Region"]) {
                    if (objTempRow["Region"] != selectedRegionName) {
                        result += "<li>Row (" + excelRowNumber + "): Column [Region] value '" + objTempRow["Region"] + "' is incorrect please update to selected region '" + selectedRegionName + "' </li>";
                    }
                }

                //Validate NotificationStatus Column
                if (objTempRow["NotificationStatus"]) {
                    if (objTempRow["NotificationStatus"] != "Required" &&
                        objTempRow["NotificationStatus"] != "Not Required") {
                        result += "<li>Row (" + excelRowNumber + "): Column [NotificationStatus] value '" + objTempRow["NotificationStatus"] + "' is incorrect please use only 'Required' or 'Not Required' </li>";
                    }
                }

                return result;
            },
            onSuccess: options.onSuccess,
            onError: options.onError
        });

        function validateExcelData(customOptions) {
            //Global Variables to validate
            var objUniqueGOCsInExcel = {};
            var objUniqueSOEIDsInExcel = {};
            var strGOCList = "";
            var strSOEIDList = "";
            var isValid = true;
            var htmlMsg = "";

            //Default Options.
            var options = {
                excelRows: [],
                columnsRequired: [],
                columnsOptional: [],
                columnsGOC: [],
                columnsSOEID: [],
                columnsToDelete: [],
                onCleanDataRow: function (objExcelRow) { },
                //onValidateRow: function(excelRowNumber, objTempRow) {},
                onSuccess: function () { },
                onError: function () { }
            };

            //Hacer un merge con las opciones que nos enviaron y las default.
            $.extend(true, options, customOptions);

            //Get all GOCs & SOEIDs Information to validate
            var excelRowNumber;
            var objTempRow;
            var optionalColsAllRowsEmpty = {};

            //Set by default optional columns all rows are empty = true;
            for (var b = 0; b < options.columnsOptional.length; b++) {
                optionalColsAllRowsEmpty[options.columnsOptional[b]] = true;
            }

            for (var i = 0; i < options.excelRows.length; i++) {
                excelRowNumber = i + 2;

                // Run custom clean data functions
                if (options.onCleanDataRow) {
                    options.excelRows[i] = options.onCleanDataRow(options.excelRows[i]);
                }

                //Get Row Data
                objTempRow = options.excelRows[i];

                // Validate Optional Empty cols for CategoryLevel1, CategoryLevel2 or CategoryLevel3
                for (var n = 0; n < options.columnsOptional.length; n++) {
                    if (options.excelRows[i][options.columnsOptional[n]]) {
                        optionalColsAllRowsEmpty[options.columnsOptional[n]] = false;
                    }
                }

                // Delete columns
                for (var s = 0; s < options.columnsToDelete.length; s++) {
                    var tempColumnToDelete = options.columnsToDelete[s];

                    //Remove Columns
                    delete options.excelRows[s][tempColumnToDelete];
                }

                // Get GOCs excel rows
                for (var j = 0; j < options.columnsGOC.length; j++) {
                    var tempColumnGOC = options.columnsGOC[j];
                    if (objTempRow[tempColumnGOC]) {
                        if (objTempRow[tempColumnGOC].length <= 10) {
                            if (typeof objUniqueGOCsInExcel[objTempRow[tempColumnGOC]] == "undefined") {
                                objUniqueGOCsInExcel[objTempRow[tempColumnGOC]] = excelRowNumber + "";
                                strGOCList += objTempRow[tempColumnGOC] + ";";
                            } else {
                                objUniqueGOCsInExcel[objTempRow[tempColumnGOC]] += (";" + excelRowNumber)
                            }
                        }
                    }
                }

                // Get SOEIDs in excel
                for (var k = 0; k < options.columnsSOEID.length; k++) {
                    var tempColumnSOEID = options.columnsSOEID[k];
                    if (objTempRow[tempColumnSOEID]) {
                        //Fix issue comma at start or the end of string ";fd36131; ac69360; mr76138;"
                        var tempSOEIDsToValidate = objTempRow[tempColumnSOEID].split(";");
                        var tempSOEIDsValidArray = [];
                        for (var w = 0; w < tempSOEIDsToValidate.length; w++) {
                            if (tempSOEIDsToValidate[w]) {
                                tempSOEIDsValidArray.push(tempSOEIDsToValidate[w]);
                            }
                        }
                        objTempRow[tempColumnSOEID] = tempSOEIDsValidArray.join(";");
                        
                        //Fix issue with spaces "AL78008; CD25867"
                        objTempRow[tempColumnSOEID] = _replaceAll(" ", "", objTempRow[tempColumnSOEID]);

                        //Split SOEIDs with CD25867;CD23548 Format
                        var arraySOEIDs = objTempRow[tempColumnSOEID].split(";");
                        for (var l = 0; l < arraySOEIDs.length; l++) {
                            var tempSOEID = arraySOEIDs[l].toUpperCase();
                            if (typeof objUniqueSOEIDsInExcel[tempSOEID] == "undefined") {
                                objUniqueSOEIDsInExcel[tempSOEID] = excelRowNumber + "";
                                strSOEIDList += tempSOEID + ";";
                            } else {
                                objUniqueSOEIDsInExcel[tempSOEID] += (";" + excelRowNumber)
                            }
                        }
                    }
                }
            }

            // Add Optional columns with data to be required col
            for (var l = 0; l < options.columnsOptional.length; l++) {
                if (optionalColsAllRowsEmpty[options.columnsOptional[l]] == false) {
                    options.columnsRequired.push(options.columnsOptional[l]);
                }
            }

            //Do validation in database and show user errors
            sendGOCToValidate(function (resultListGOCs) {
                sendSOEIDToValidate(function (resultListSOEIDs) {
                    
                    //Validate Excel Data
                    for (var i = 0; i < options.excelRows.length; i++) {
                        var objGOCValidation = null;
                        var objSOEIDValidation = null;

                        excelRowNumber = i + 2;
                        objTempRow = options.excelRows[i];

                        // Validation for GOC Columns
                        for (var j = 0; j < options.columnsGOC.length; j++) {
                            var tempColumnGOC = options.columnsGOC[j];

                            if (objTempRow[tempColumnGOC]) {
                                if (objTempRow[tempColumnGOC].length > 10) {
                                    htmlMsg += "<li>Row (" + excelRowNumber + "): Column [" + tempColumnGOC + "] the max text length is 10 characters your value '" + objTempRow[tempColumnGOC] + "' has " + objTempRow[tempColumnGOC].length + " of length </li>";
                                } else {
                                    objGOCValidation = _findOneObjByProperty(resultListGOCs, "GOC", objTempRow[tempColumnGOC]);

                                    if (objGOCValidation.Exists == "0") {
                                        htmlMsg += "<li>Row (" + excelRowNumber + "): Column [" + tempColumnGOC + "] your value '" + objTempRow[tempColumnGOC] + "' not exists in our GOCs records. </li>";
                                    } else {
                                        if (objGOCValidation.ExistsGPL == "0" && objGOCValidation.ExistsRPL == "0") {
                                            htmlMsg += "<li>Row (" + excelRowNumber + "): Column [" + tempColumnGOC + "] your value '" + objTempRow[tempColumnGOC] + "' don't have GPL and RPL Assigned, please validate if it is a FRSS GOC or use other FRSS GOC. </li>";
                                        } else {
                                            if (objGOCValidation.ExistsGPL == "0") {
                                                htmlMsg += "<li>Row (" + excelRowNumber + "): Column [" + tempColumnGOC + "] your value '" + objTempRow[tempColumnGOC] + "' don't have GPL Assigned, please validate if it is a FRSS GOC or use other FRSS GOC. </li>";
                                            }

                                            if (objGOCValidation.ExistsRPL == "0") {
                                                htmlMsg += "<li>Row (" + excelRowNumber + "): Column [" + tempColumnGOC + "] your value '" + objTempRow[tempColumnGOC] + "' don't have RPL Assigned, please validate if it is a FRSS GOC or use other FRSS GOC. </li>";
                                            }
                                        }
                                    }
                                }
                            } else {
                                htmlMsg += "<li>Row (" + excelRowNumber + "): Column [" + tempColumnGOC + "] value is missing </li>";
                            }
                        }
                        
                        // Validation for SOEID Columns
                        for (var k = 0; k < options.columnsSOEID.length; k++) {
                            var tempColumnSOEID = options.columnsSOEID[k];
                            if (objTempRow[tempColumnSOEID]) {

                                //Split SOEIDs with CD25867;CD23548 Format
                                var arraySOEIDs = objTempRow[tempColumnSOEID].split(";");

                                for (var h = 0; h < arraySOEIDs.length; h++) {
                                    var tempSOEID = arraySOEIDs[h];

                                    objSOEIDValidation = _findOneObjByProperty(resultListSOEIDs, "SOEID", tempSOEID.toUpperCase());
                                    if (objSOEIDValidation.Exists == "0") {
                                        htmlMsg += "<li>Row (" + excelRowNumber + "): Column [" + tempColumnSOEID + "] your value '" + tempSOEID + "' not exists in our Global Directory records. </li>";
                                    }
                                }
                            } else {
                                htmlMsg += "<li>Row (" + excelRowNumber + "): Column [" + tempColumnSOEID + "] value is missing </li>";
                            }
                        }

                        // Validation for Required Columns
                        for (var m = 0; m < options.columnsRequired.length; m++) {
                            var tempColumnRequired = options.columnsRequired[m];
                            if (!objTempRow[tempColumnRequired]) {
                                htmlMsg += "<li>Row (" + excelRowNumber + "): Column [" + tempColumnRequired + "] value is missing </li>";
                            }
                        }

                        // Run custom validations
                        if (options.onValidateRow) {
                            htmlMsg += options.onValidateRow(excelRowNumber, objTempRow);
                        }
                    }

                    if (htmlMsg) {
                        htmlMsg = "<ul>" + htmlMsg + "</ul>";

                        _showAlert({
                            id: "UploadExcelMessage",
                            type: 'error',
                            content: "<b>Please review the following error in the data of your Excel:</b>" + htmlMsg,
                            animateScrollTop: true
                        });

                        isValid = false;
                    }

                    if (isValid) {
                        options.onSuccess(options.excelRows);
                    } else {
                        options.onError(options.excelRows);
                    }
                });
            });

            //Validate if exists GOCs in Database
            function sendGOCToValidate(onSuccess) {
                //Sql for validate GOCs
                var sqlValidateGOCs =
                    "SELECT \n" +
                    "   TG.[GOC], \n" +
                    "   CASE ISNULL(G.[ID], '') \n" +
                    "       WHEN '' THEN 0 \n" +
                    "       ELSE 1 \n" +
                    "   END AS [Exists], \n" +
                    "   CASE ISNULL(MS.[ManagerID], 0) \n" +
                    "       WHEN 0 THEN 0 \n" +
                    "       ELSE 1 \n" +
                    "   END AS [ExistsGPL], \n" +
                    "   CASE ISNULL(MSMG.[ManagerID], 0) \n" +
                    "       WHEN 0 THEN 0 \n" +
                    "       ELSE 1 \n" +
                    "   END AS [ExistsRPL] \n" +
                    "FROM ( \n" +
                    "    SELECT [Val] AS [GOC] \n" +
                    "    FROM [Automation].[dbo].[func_Split]('" + strGOCList + "', ';', 1) \n" +
                    ") TG \n" +
                    "LEFT JOIN [Automation].[dbo].[tblGlobalOrganizationCode] G ON G.[ID] = TG.[GOC] " +
                    " \n " +
                    "--Get GPL \n " +
                    "LEFT JOIN [Automation].[dbo].[tblManagedSegment] MS WITH(READUNCOMMITTED) ON G.ManagedSegmentID = MS.ID \n " +
                    " \n " +
                    "--Get RPL \n " +
                    "LEFT JOIN [Automation].[dbo].[tblManagedSegmentManagedGeography] MSMG WITH(READUNCOMMITTED) ON G.ManagedSegmentID = MSMG.ManagedSegmentID AND G.ManagedGeographyID = MSMG.ManagedGeographyID   \n ";

                //Validate GOCs
                _callServer({
                    loadingMsgType: "fullLoading",
                    loadingMsg: "Validating GOCs...",
                    url: '/Ajax/ExecQuery',
                    data: { 'pjsonSql': _toJSON(sqlValidateGOCs) },
                    type: "POST",
                    success: function (resultListGOCs) {
                        onSuccess(resultListGOCs);
                    }
                });
            }

            //Validate if exists SOEIDs in Database
            function sendSOEIDToValidate(onSuccess) {
                //Sql for validate SOEIDs
                var sqlValidateSOEIDs =
                    "SELECT \n" +
                    "   UPPER(TG.[SOEID])           AS [SOEID], \n" +
                    "   CASE ISNULL(E.[SOEID], '') \n" +
                    "       WHEN '' THEN 0 \n" +
                    "       ELSE 1 \n" +
                    "   END                         AS [Exists] \n" +
                    "FROM ( \n" +
                    "    SELECT [Val] AS [SOEID] \n" +
                    "    FROM [Automation].[dbo].[func_Split]('" + strSOEIDList + "', ';', 1) \n" +
                    ") TG \n" +
                    "LEFT JOIN [Automation].[dbo].[tblEmployee] E WITH(READUNCOMMITTED) ON E.[SOEID] = TG.[SOEID] ";

                //Validate GOCs
                _callServer({
                    loadingMsgType: "fullLoading",
                    loadingMsg: "Validating SOEIDs...",
                    url: '/Ajax/ExecQuery',
                    data: { 'pjsonSql': _toJSON(sqlValidateSOEIDs) },
                    type: "POST",
                    success: function (resultListSOEIDs) {
                        onSuccess(resultListSOEIDs);
                    }
                });
            }
        }
    } else {
        options.onError();
    }
}