/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="SupportDocGlobal.js" />
var strFiltersTable = "Year;Month;ProofOwnerSOEID;FileStatus;StatusByPO";

$(document).ready(function () {

    //Hide left Menu
    _hideMenu();

    // Load POs
    if ($("#HFViewType").val() == "ProofOwner") {
        _createSelectSOEID({
            id: "#selPO",
            selSOEID: _getSOEID(),
            disabled: true
        });

        $("#lblRoleUser").html(" Proof Owner");
    } else {
        _createSelectSOEID({
            id: "#selPO",
            selSOEID: ""
        });
    }

    // Load Responsibles
    if ($("#HFViewType").val() == "Responsible") {
        _createSelectSOEID({
            id: "#selResponsible",
            selSOEID: _getSOEID(),
            disabled: true
        });

        $("#lblRoleUser").html(" Responsible");
    } else {
        _createSelectSOEID({
            id: "#selResponsible",
            selSOEID: ""
        });
    }

    // Load Admins
    if ($("#HFViewType").val() == "RegionAdmin") {
        $("#lblRoleUser").html(" Region Admin");
    }

    //Load regions information
    _loadSelectOfRegion({
        accessBy: $("#HFViewType").val(),
        onSuccess: function () {
            //Check File status
            _checkFileExistsInShareDrive({
                onSuccess: function () {
                    //Load table
                    _loadDocumentTableList();
                }
            });
        }
    });

    //On Change Region
    $("#selRegion").change(function () {
        // Load Periods
        var idRegion = $("#selRegion").val();
        _loadSelectOfPeriodsByRegion({
            idRegion: idRegion,
            onSuccess: function () {
                //Check File status
                _checkFileExistsInShareDrive({
                    onSuccess: function () {
                        //Load table
                        _loadDocumentTableList();
                    }
                });
            }
        });
    });

    //On Change Period
    $("#selPeriod").change(function () {
        //Check File status
        _checkFileExistsInShareDrive({
            onSuccess: function () {
                //Load table
                _loadDocumentTableList();
            }
        });
    });

    //On Change File Status
    $("#selFileStatus").change(function () {
        //Load table
        _loadDocumentTableList(_getCurrentFilters(strFiltersTable));
    });

    //On Change Status By PO
    $("#selStatusByPO").change(function () {
        //Load table
        _loadDocumentTableList(_getCurrentFilters(strFiltersTable));
    });

    //On click reload table
    $(".btnRefresh").click(function () {
        //Check File status
        _checkFileExistsInShareDrive({
            onSuccess: function () {
                //Load table
                _loadDocumentTableList(_getCurrentFilters(strFiltersTable));
            }
        });
    });

    //On click approve document
    $(".btnApprove").click(function () {
        approveDoc();
    });

    //On click reject document
    $(".btnReject").click(function () {
        rejectDoc();
    });

    //On click download document
    $(".btnDownloadDoc").click(function (e) {
        downloadDoc(e);
    });

    //On Export Approved documents
    $(".btnExportApprovedDocs").click(function (e) {
        exportApproved();
    });

    //On click upload document
    $(".btnUploadDoc").click(function () {
        uploadDoc();
    });

    //On click export to excel
    $(".btnExportToExcel").click(function () {
        exportToExcel();
    });

    //On Click Copy Link to clipboard
    $(".btnCopyLinkToClipboard").click(function () {
        copyLinkToClipboard();
    });

    //Fix trigger change on creation of selects
    _execOnAjaxComplete(function () {
        //On Change PO
        $("#selPO select").change(function () {
            //Load table
            _loadDocumentTableList();
        });

        //On Change Responsible
        $("#selResponsible select").change(function () {
            //Load table
            _loadDocumentTableList();
        });
    });
});

function copyLinkToClipboard() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#tblDocs', true);
    if (rowData) {
        _copyToClipboard(rowData.FileFolderPath);
        _showNotification("success", "Link: '" + rowData.FileFolderPath + "' was copied succesfully to clipboard.", "CopyLink");

        createIfNotExistsFolderPath({
            folderPath: rowData.FileFolderPath,
            onSuccess: function (response) {
                console.log(response);
            }
        });
    }
}

function createIfNotExistsFolderPath(options) {
    _callServer({
        loadingMsgType: "topBar",
        loadingMsg: "Checking folder full path...",
        url: '/SupportDocProdCtrl/CreateIfNotExistsFolderPath',
        data: {
            typeAuthentication: _getTypeAuthentication(),
            user: _encodeSkipSideminder(_getWindowsUser()),
            pass: _getWindowsPassword(),
            folderPath: _encodeSkipSideminder(options.folderPath)
        },
        success: function (respond) {
            if (options.onSuccess) {
                options.onSuccess(respond);
            }
        }
    });
}

function rejectDoc() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#tblDocs', true);
    if (rowData) {

        //Fix: Hide PO Select
        $(".select2-container").css("z-index", "0");

        var htmlContentModal = "<b>Name: </b> <br/>" + rowData.FileName + "<br/>";
        htmlContentModal += "<b>Path: </b> <br/>" + rowData.FileFullPath + "<br/>";
        htmlContentModal += "<b>Comment: </b> <br/><textarea id='txtCommentReject' style='width:100%;height: 130px;'></textarea><br/>";

        _showModal({
            width: '35%',
            modalId: "modalDelApp",
            addCloseButton: true,
            buttons: [{
                name: "Reject",
                class: "btn-danger",
                onClick: function () {

                    var commentReject = $("#txtCommentReject").val();

                    _callProcedure({
                        loadingMsgType: "topBar",
                        loadingMsg: "Rejecting document...",
                        name: "[dbo].[spSDocAdminStatus]",
                        params: [
                            { "Name": "@ServerURL", "Value": _getViewVar("ServerURL") },
                            { "Name": "@SessionSOEID", "Value": _getSOEID() },
                            { "Name": "@IDDocPeriod", "Value": rowData.IDDocPeriod },
                            { "Name": "@StatusByPO", "Value": 'Rejected' },
                            { "Name": "@Comment", "Value": commentReject }
                        ],
                        success: {
                            fn: function (responseList) {
                                //Create Log
                                _insertAuditLog({
                                    Action: "Rejected Doc",
                                    Description: rowData.FileFullPath
                                });

                                //Update all rows in the grid
                                //var allRows = $.jqxGridApi.getAllRows('#tblDocs');
                                var allRows = $('#tblDocs').jqxGrid('getRows');
                                for (var i = 0; i < allRows.length; i++) {
                                    var objTempRow = allRows[i];

                                    if (objTempRow && objTempRow.FileName == rowData.FileName && objTempRow.Extension == rowData.Extension) {
                                        objTempRow.StatusByPO = 'Rejected';

                                        objHistory = {
                                            IDDocPeriod: rowData.IDDocPeriod,
                                            FileStatus: rowData.FileStatus,
                                            StatusByPO: 'Rejected',
                                            Comment: commentReject,
                                            CreatedByName: _getUserInfo().Name,
                                            CreatedBySOEID: _getUserInfo().SOEID,
                                            CreatedDate: moment().format('MMM DD YYYY h:mmA')
                                        };

                                        var history = JSON.parse(objTempRow.JsonHistory);
                                        history.push(objHistory);

                                        objTempRow.JsonHistory = JSON.stringify(history);

                                        //Set new value
                                        $('#tblDocs').jqxGrid('updaterow', objTempRow.boundindex, objTempRow);
                                    }
                                }

                                //Create Log of Rejected Notification
                                _insertAuditLog({
                                    Action: "Rejected Notification",
                                    Description: rowData.ResponsibleDesc + " (" + rowData.ResponsibleSOEID + ")"
                                });

                                _showNotification("success", "Document '" + rowData.FileName + "." + rowData.Extension + "' was rejected successfully and notified to " + rowData.ResponsibleDesc + " (" + rowData.ResponsibleSOEID + ").")
                            }
                        }
                    });
                }
            }],
            title: "Are you sure you want to reject this row?",
            contentHtml: htmlContentModal
        });
    }
}

function approveDoc() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#tblDocs', true);
    if (rowData) {

        //Approve only documents with status "Pending" or "Rejected"
        if (rowData.StatusByPO != "Approved") {
            //Fix: Hide PO Select
            $(".select2-container").css("z-index", "0");

            var htmlContentModal = "<b>Name: </b>" + rowData.FileName + "<br/>";
            htmlContentModal += "<b>Path: </b>" + rowData.FileFullPath + "<br/>";

            _showModal({
                width: '35%',
                modalId: "modalDelApp",
                addCloseButton: true,
                buttons: [{
                    name: "Approved",
                    class: "btn-success",
                    onClick: function () {
                        _callProcedure({
                            loadingMsgType: "topBar",
                            loadingMsg: "Approving document...",
                            name: "[dbo].[spSDocAdminStatus]",
                            params: [
                                { "Name": "@ServerURL", "Value": _getViewVar("ServerURL") },
                                { "Name": "@SessionSOEID", "Value": _getSOEID() },
                                { "Name": "@IDDocPeriod", "Value": rowData.IDDocPeriod },
                                { "Name": "@StatusByPO", "Value": 'Approved' },
                                { "Name": "@Comment", "Value": '' }
                            ],
                            success: {
                                fn: function (responseList) {
                                    //Create Log
                                    _insertAuditLog({
                                        Action: "Approved Doc",
                                        Description: rowData.FileFullPath
                                    });

                                    //Update all rows in the grid
                                    //var allRows = $.jqxGridApi.getAllRows('#tblDocs');
                                    var allRows = $('#tblDocs').jqxGrid('getRows');
                                    for (var i = 0; i < allRows.length; i++) {
                                        var objTempRow = allRows[i];

                                        if (objTempRow && objTempRow.FileName == rowData.FileName && objTempRow.Extension == rowData.Extension) {
                                            objTempRow.StatusByPO = 'Approved';

                                            objHistory = {
                                                IDDocPeriod: rowData.IDDocPeriod,
                                                FileStatus: rowData.FileStatus,
                                                StatusByPO: 'Approved',
                                                Comment: '',
                                                CreatedByName: _getUserInfo().Name,
                                                CreatedBySOEID: _getUserInfo().SOEID,
                                                CreatedDate: moment().format('MMM DD YYYY h:mmA')
                                            };

                                            var history = JSON.parse(objTempRow.JsonHistory);
                                            history.push(objHistory);

                                            objTempRow.JsonHistory = JSON.stringify(history);

                                            //Set new value
                                            $('#tblDocs').jqxGrid('updaterow', objTempRow.boundindex, objTempRow);
                                        }
                                    }

                                    _showNotification("success", "Document '" + rowData.FileName + "." + rowData.Extension + "' was approved successfully.")
                                }
                            }
                        });
                    }
                }],
                title: "Are you sure you want to approve this row?",
                contentHtml: htmlContentModal
            });
        } else {
            _showAlert({
                id: "ApprovedAlert",
                showTo: $(".content-body"),
                type: "info",
                title: "Message",
                content: "The selected document '" + rowData.FileName + "." + rowData.Extension + "' is already approved.",
                animateScrollTop: true
            });
        }
    }
}

function downloadDoc(e) {
    e.preventDefault();

    var rowData = $.jqxGridApi.getOneSelectedRow('#tblDocs', true);
    if (rowData) {
        if (rowData.FileStatus == "Received") {

            _showNotification("success", "Downloading document '" + rowData.FileName + "' please wait...");

            setTimeout(function () {
                window.location.href = _getViewVar("SubAppPath") + "/SupportDoc/DownloadDoc/?" + $.param({
                    'docShareFolderFilePath': _encodeSkipSideminder(rowData.FileFullPath),
                    'fileName': _encodeSkipSideminder(rowData.FileName + '.' + rowData.Extension),
                    'ext': rowData.Extension,
                    'typeAuthentication': _getTypeAuthentication(),
                    'user': _encodeSkipSideminder(_getWindowsUser()),
                    'pass': _getWindowsPassword()
                });

                //Create Log
                _insertAuditLog({
                    Action: "Download File",
                    Description: rowData.FileFullPath
                });
            }, 1000);

            //_callServer({
            //    loadingMsgType: "topBar",
            //    loadingMsg: "Downloading document '" + rowData.FileName + "' please wait...",
            //    url: '/SupportDoc/DownloadDoc',
            //    data: {
            //        'docShareFolderFilePath': rowData.FileFullPath,
            //        'fileName': rowData.FileName + '.' + rowData.Extension
            //    },
            //    type: "post",
            //    success: function (urlToDownload) {
            //        if (urlToDownload) {
            //            window.open(_replaceAll("~", "", urlToDownload), '_blank');
            //        } else {
            //            _showNotification("error", "An error occurred while downloading the file.");
            //        }
            //    }
            //});
        } else {
            _showNotification("error", "File not exists in '" + rowData.FileFullPath + "'");
        }
    }
}

function exportApproved() {
    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Getting Approved Documents...",
        name: "[dbo].[spSDocPeriodListPaging]",
        params: [
            { "Name": "@StrWhere", "Value": "WHERE ( [Year] = " + $("#selPeriod").find("option:selected").attr("year") + " AND [Month] = " + $("#selPeriod").find("option:selected").attr("month") + " AND [FileStatus] = 'Received' AND [StatusByPO] = 'Approved')" },
            { "Name": "@EndIndexRow", "Value": 100000 }
        ],
        success: {
            fn: function (responseList) {
                
                if (responseList.length > 0) {
                    var distinctDocs = {};
                    var distinctRegionPaths = {};

                    //Fix: Hide PO Select, cannot move to function _showModal because it can hide select2 in modals
                    $(".select2-container").css("z-index", "0");

                    var htmlContentModal = "<b>Approved Documents:</b><pre>";

                    //Add Path of documents to export
                    for (var i = 0; i < responseList.length; i++) {
                        var objRowTemp = responseList[i];

                        //Only if not exists document to avoid duplicates
                        if (!distinctDocs[objRowTemp.FileFullPath]) {
                            htmlContentModal += objRowTemp.FileFullPath + " \n";
                            distinctDocs[objRowTemp.FileFullPath] = true;
                        }
                        
                        //Only if not exists path of region to export approved
                        if (!distinctRegionPaths[objRowTemp.RegionFolderPath]) {
                            distinctRegionPaths[objRowTemp.RegionFolderPath] = true;
                        }
                    }
                    htmlContentModal += "</pre>";

                    //Add Path of region to export
                    htmlContentModal += "<b>Export To:</b><pre>";
                    var regionPaths = Object.keys(distinctRegionPaths);
                    for (var i = 0; i < regionPaths.length; i++) {
                        htmlContentModal += regionPaths[i] + "Approved\\";
                    }
                    htmlContentModal += "</pre>";

                    _showModal({
                        width: '50%',
                        modalId: "modalDelApp",
                        addCloseButton: true,
                        buttons: [{
                            name: "Export",
                            class: "btn-success",
                            onClick: function () {

                                _callServer({
                                    loadingMsgType: "fullLoading",
                                    loadingMsg: "Exporting documents...",
                                    url: '/SupportDoc/ExportDocuments',
                                    data: {
                                        year: $("#selPeriod").find("option:selected").attr("year"),
                                        month: $("#selPeriod").find("option:selected").attr("month"),
                                        typeAuthentication: _getTypeAuthentication(),
                                        user: _encodeSkipSideminder(_getWindowsUser()),
                                        pass: _getWindowsPassword()
                                    },
                                    success: function (responseData) {
                                        if (responseData.success == "1") {
                                            //Create Log
                                            _insertAuditLog({
                                                Action: "Exported Approved Docs",
                                                Description: responseData.msg
                                            });

                                            htmlContentModal = "<b>Documents Copied:</b><pre>" + responseData.msg + "</pre>";

                                            _showModal({
                                                width: '50%',
                                                modalId: "modalConfirmApp",
                                                addCloseButton: true,
                                                buttons: [{
                                                    name: "Ok",
                                                    class: "btn-success",
                                                    onClick: function () {}
                                                }],
                                                title: "Documents were exported successfully",
                                                contentHtml: htmlContentModal
                                            });
                                        } else {
                                            _showNotification("error", responseData.msg);
                                        }
                                    }
                                });
                            }
                        }],
                        title: "Are you sure you want to export the following approved documents?",
                        contentHtml: htmlContentModal
                    });
                } else {
                    _showNotification("error", "No approved documents found");
                }
            }
        }
    });
}

function exportToExcel() {
    var filters = _getCurrentFilters();
    var spName = "[dbo].[spSDocPeriodListPagingResponsibleExcel]";
    var strWhere =
        "WHERE " +
        "   [IDRegion] = " + filters.IDRegion + " AND " +
        "   [Year] = " + filters.Year + " AND " +
        "   [Month] = " + filters.Month + " AND " +
        "   [ResponsibleSOEID] LIKE '%" + filters.ResponsibleSOEID + "%' ";

    var spParams = [
        { "Name": "@StrWhere", "Value": strWhere },
        { "Name": "@EndIndexRow", "Value": 99000000 }
    ];
    var filename = "SupportDocToolPendingDocs - " + $("#selPeriod").find("option:selected").val();

    _downloadExcel({
        sql: _getSqlProcedure(spName, spParams),
        filename: filename,
        success: {
            msg: "Please Wait. Generating report..."
        }
    });
}

function uploadDoc() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#tblDocs', true);
    if (rowData) {
        var htmlContentModal = '';
        htmlContentModal += "<b>Full Key: </b> " + rowData["FullKey"] + " <br/>";
        htmlContentModal += "<b>File Name: </b> <span class='txtFileName'>" + rowData["FileName"] + "." + rowData["Extension"] +"</span> <br/>";
        htmlContentModal += "<b>Location to upload: </b> " + rowData["FileFolderPath"] + " <br/><br/>";
        htmlContentModal += "<div id='fine-uploader-manual-trigger'></div>";

        _showModal({
            width: '70%',
            modalId: "modalUpload",
            addCloseButton: true,
            title: "Upload Document",
            contentHtml: htmlContentModal,
            onReady: function ($modal) {
                initFileUploader(rowData["Extension"], rowData["FileName"], rowData["FileFolderPath"]);
            }
        });
    }

}

function setFileName(idFile) {
    $('#fine-uploader-manual-trigger').fineUploader('setName', idFile, $(".txtFileName").text());

    //Remove alert
    $("#AlertChangeFileName").remove();
}

function initFileUploader(pext, pfileName, plocationToSave) {
    $('#fine-uploader-manual-trigger').fineUploader({
        template: 'qq-template-manual-trigger',
        request: {
            endpoint: _getViewVar("SubAppPath") + '/SupportDoc/Upload'
        },
        thumbnails: {
            placeholders: {
                waitingPath: _getViewVar("SubAppPath") + '/Content/Shared/plugins/fine-uploader/placeholders/waiting-generic.png',
                notAvailablePath: _getViewVar("SubAppPath") + '/Content/Shared/plugins/fine-uploader/placeholders/not_available-generic.png'
            }
        },
        callbacks: {
            onAllComplete: function (succeeded, failed) {
                console.log(succeeded, failed);
                if (failed.length == 0) {
                    _showAlert({
                        showTo: ".modal-body",
                        type: "success",
                        title: "Message",
                        content: "The file was uploaded successfully.",
                        animateScrollTop: false
                    });

                    //Refresh Table
                    _loadDocumentTableList();

                    //Create Log
                    _insertAuditLog({
                        Action: "Uploaded Doc",
                        Description: (plocationToSave + pfileName + '.' + pext)
                    });
                }
            },
            onError: function (id, name, errorReason, xhrOrXdr) {
                if (name && !_contains(errorReason, "has an invalid extension")) {
                    _showDetailAlert({
                        showTo: ".modal-body",
                        title: "Message",
                        shortMsg: "An error ocurred with the file '" + name + "'.",
                        longMsg: "<pre>" + errorReason + "</pre>",
                        type: "Error",
                        viewLabel: "View Details"
                    });
                }
            },
            onSubmit: function (id, name) {
                //var file = this.getFile(id);
                //validated = validate(file);

                //if (validated) {
                //    return true;
                //} else {
                //    return false;
                //}
                var filters = _getCurrentFilters();

                //Add parameter of locationToSave
                $('#fine-uploader-manual-trigger').fineUploader('setParams', {
                    'idRegion': filters.IDRegion,
                    'year': filters.Year,
                    'month': filters.Month,
                    'locationToSave': plocationToSave
                });
            },
            onManualRetry: function (id, name) {
                var filters = _getCurrentFilters();

                //Add parameter of locationToSave
                $('#fine-uploader-manual-trigger').fineUploader('setParams', {
                    'idRegion': filters.IDRegion,
                    'year': filters.Year,
                    'month': filters.Month,
                    'locationToSave': plocationToSave
                });
            }
        },
        validation: {
            allowedExtensions: [pext],
            itemLimit: 1
            //sizeLimit: 51200 // 50 kB = 50 * 1024 bytes
        },
        autoUpload: false
    });

    //Validation before send files
    $('#trigger-upload').click(function () {

        //Validate if the file has the correct name
        if ($(".qq-upload-file-selector.qq-upload-file.qq-editable").attr("title") != pfileName + '.' + pext) {
            _showAlert({
                id: "AlertChangeFileName",
                showTo: ".modal-body",
                content: "Please change your file name of your document to <b>" + pfileName + "</b>, click on 'Set Name' button to set the correct file name."
            });
        } else {
            $('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
        }

        //if ($('[name^="iCheck-TypeUpload"]:checked').length > 0) {
        //    $('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
        //} else {
        //    _showAlert({
        //        content: "Please select the type of the file <b>'GLMS Transcript'</b> or <b>'Udemy Training Catalog'</b>."
        //    });
        //}
    });
}