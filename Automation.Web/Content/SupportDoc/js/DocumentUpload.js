/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="SupportDocGlobal.js" />

$(document).ready(function () {

    //Hide Menu Only when is a Responsible View
    _hideMenu();

    //Load regions information
    _loadSelectOfRegion({
        accessBy: "Responsible"
    });

    //On Change Region
    $("#selRegion").change(function () {
        // Load Periods
        var idRegion = $("#selRegion").val();
        _loadSelectOfPeriodsByRegion(idRegion, function () {
            //Load table
            _loadDocumentTableList(_getCurrentFilters(strFiltersTable));
        });
    });

    //On click reload table
    $(".btnRefresh").click(function () {
        checkFileExistByResponsible(function () {
            //Load pending documents by responsible
            loadDocumentByResponsible();
        });
    });

    //On Change Period
    $("#selPeriod").change(function () {
        $(".btnRefresh").click();
    });

    $(".btnRefresh").click();
});

function exportToExcel() {
    var spName = "[dbo].[spSDocPeriodListPagingExcel]";
    var spParams = [
        { "Name": "@StrWhere", "Value": "WHERE ( [Year] = " + $("#selPeriod").find("option:selected").attr("year") + " AND [Month] = " + $("#selPeriod").find("option:selected").attr("month") + " AND ([NotificationStatus] ='" + $("#HFNotificationStatus").val() + "') AND [ResponsilbleSOEID] LIKE '%" + $("#HFSOEIDResponsible").val() + "%')" },
        { "Name": "@EndIndexRow", "Value": 100000 }
    ];
    var filename = "SupportDocToolPendingDocs - " + $("#selPeriod").find("option:selected").val();

    _downloadExcel({
        sql: _getSqlProcedure(spName, spParams),
        filename: filename,
        success: {
            msg: "Please Wait. Generating report..."
        }
    });
}

function uploadDoc() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#tblDocs', true);
    if (rowData) {
        var htmlContentModal = '';
        htmlContentModal += "<b>Full Key: </b> " + rowData["FullKey"] + " <br/>";
        htmlContentModal += "<b>File Name: </b> " + rowData["FileName"] + " <br/>";
        htmlContentModal += "<b>File Extension: </b> " + rowData["Extension"] + " <br/>";
        htmlContentModal += "<b>Location to upload: </b> " + rowData["FileFolderPath"] + " <br/><br/>";
        htmlContentModal += "<div id='fine-uploader-manual-trigger'></div>";

        _showModal({
            width: '50%',
            modalId: "modalUpload",
            addCloseButton: true,
            title: "Upload Document",
            contentHtml: htmlContentModal,
            onReady: function ($modal) {
                initFileUploader(rowData["Extension"], rowData["FileName"], rowData["FileFolderPath"]);
            }
        });
    }
    
}

function initFileUploader(pext, pfileName, plocationToSave) {
    $('#fine-uploader-manual-trigger').fineUploader({
        template: 'qq-template-manual-trigger',
        request: {
            endpoint: _getViewVar("SubAppPath") + '/SupportDoc/Upload'
        },
        thumbnails: {
            placeholders: {
                waitingPath: _getViewVar("SubAppPath") + '/Content/Shared/plugins/fine-uploader/placeholders/waiting-generic.png',
                notAvailablePath: _getViewVar("SubAppPath") + '/Content/Shared/plugins/fine-uploader/placeholders/not_available-generic.png'
            }
        },
        callbacks: {
            onAllComplete: function (succeeded, failed) {
                console.log(succeeded, failed);
                if (failed.length == 0) {
                    _showAlert({
                        showTo: ".modal-body",
                        type: "success",
                        title: "Message",
                        content: "The file was uploaded successfully.",
                        animateScrollTop: false
                    });

                    //Refresh Table
                    loadDocumentByResponsible();

                    //Create Log
                    _insertAuditLog({
                        Action: "Uploaded Doc",
                        Description: (plocationToSave + pfileName + '.' + pext)
                    });
                }
            },
            onError: function (id, name, errorReason, xhrOrXdr) {
                if (name && !_contains(errorReason, "has an invalid extension")) {
                    _showDetailAlert({
                        showTo: ".modal-body",
                        title: "Message",
                        shortMsg: "An error ocurred with the file '" + name + "'.",
                        longMsg: "<pre>" + errorReason + "</pre>",
                        type: "Error",
                        viewLabel: "View Details"
                    });
                }
            },
            onSubmit: function (id, name) {
                //var file = this.getFile(id);
                //validated = validate(file);

                //if (validated) {
                //    return true;
                //} else {
                //    return false;
                //}

                //Add parameter of locationToSave
                $('#fine-uploader-manual-trigger').fineUploader('setParams', {
                    'year': $("#selPeriod").find("option:selected").attr("year"),
                    'month': $("#selPeriod").find("option:selected").attr("month"),
                    'locationToSave': plocationToSave
                });
            },
            onManualRetry: function (id, name) {
                //Add parameter of locationToSave
                $('#fine-uploader-manual-trigger').fineUploader('setParams', {
                    'year': $("#selPeriod").find("option:selected").attr("year"),
                    'month': $("#selPeriod").find("option:selected").attr("month"),
                    'locationToSave': plocationToSave
                });
            }
        },
        validation: {
            allowedExtensions: [pext],
            itemLimit: 1
            //sizeLimit: 51200 // 50 kB = 50 * 1024 bytes
        },
        autoUpload: false
    });

    //Validation before send files
    $('#trigger-upload').click(function () {

        //Validate if the file has the correct name
        if ($(".qq-upload-file-selector.qq-upload-file.qq-editable").attr("title") != pfileName + '.' + pext) {
            _showAlert({
                showTo: ".modal-body",
                content: "Please change the name of your document to '<b>" + pfileName + "</b>'."
            });
        } else {
            $('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
        }

        //if ($('[name^="iCheck-TypeUpload"]:checked').length > 0) {
        //    $('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
        //} else {
        //    _showAlert({
        //        content: "Please select the type of the file <b>'GLMS Transcript'</b> or <b>'Udemy Training Catalog'</b>."
        //    });
        //}
    });
}