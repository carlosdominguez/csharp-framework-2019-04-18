/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="SupportDocGlobal.js" />

$(document).ready(function () {

    //Load regions information
    _loadSelectOfRegion({
        accessBy: "ProofOwner",
        onSuccess: function () {
            //Check File status
            _checkFileExistsInShareDrive({
                onSuccess: function () {
                    //Load table
                    loadTablePendingDocsByResponsible();
                }
            });
        }
    });

    //On Change Region
    $("#selRegion").change(function () {
        // Load Periods
        var idRegion = $("#selRegion").val();
        _loadSelectOfPeriodsByRegion({
            idRegion: idRegion,
            onSuccess: function () {
                //Check File status
                _checkFileExistsInShareDrive({
                    onSuccess: function () {
                        //Load table
                        loadTablePendingDocsByResponsible();
                    }
                });
            }
        });
    });

    //On Change Period
    $("#selPeriod").change(function () {
        //Check File status
        _checkFileExistsInShareDrive({
            onSuccess: function () {
                //Load table
                loadTablePendingDocsByResponsible();
            }
        });
    });

    //On Change Status
    $("#selStatus").change(function () {
        //Required
        if ($("#selStatus").val() == "Required") {
            //Show
            $(".btnBD8").show();
            $(".btnBD10").show();
            $(".btnBD12").show();

            // Hide
            $(".btnEscalation").hide();
        }

        //Not Required
        if ($("#selStatus").val() == "No Required" || $("#selStatus").val() == "Required Except") {
            //Show
            $(".btnEscalation").show();
            
            // Hide
            $(".btnBD8").hide();
            $(".btnBD10").hide();
            $(".btnBD12").hide();
        }

        //Check File status
        _checkFileExistsInShareDrive({
            onSuccess: function () {
                //Load table
                loadTablePendingDocsByResponsible();
            }
        });
    });

    //On Click Send Notification buttons
    $(".btnSendNotification").click(function () {
        var $btn = $(this);
        sendNotification($btn.attr("typeNotification"), $btn.attr("contacts"));
    });

    //Check File status
    //_checkFileExistsInShareDrive(function () {
        //Show and Hide buttons and load Table of Pending Documents
        //$("#selStatus").change();
    //});
});

function sendNotification(typeNotification, typeContacts) {
    var htmlContentModal = "<p>This communication will be delivered to contacts as provided in the template under corresponding";
    var contacts = typeContacts.split(",");
    for (var i = 0; i < contacts.length; i++) {
        htmlContentModal += " <b>" + contacts[i] + "</b> field";
    }
    htmlContentModal += " </p>";

    _showModal({
        width: '35%',
        modalId: "modalDelApp",
        addCloseButton: true,
        buttons: [{
            name: "Send notification",
            class: "btn-success",
            onClick: function () {
                var filters = _getCurrentFilters();

                _callProcedure({
                    loadingMsgType: "fullLoading",
                    loadingMsg: "Sending email notifications '" + typeNotification + "' for " + $("#selPeriod").find("option:selected").val() + "...",
                    name: "[dbo].[spSDocEmailEscalationStart]",
                    params: [
                        { "Name": "@SessionSOEID", "Value": _getSOEID() },
                        { "Name": "@ServerURL", "Value": _getViewVar("ServerURL") },
                        { "Name": "@TypeNotification", "Value": typeNotification },
                        { "Name": "@ContactsToSend", "Value": typeContacts },
                        { "Name": "@NotificationStatus", "Value": $("#selStatus").val() },
                        { "Name": "@IDRegion", "Value": filters.IDRegion },
                        { "Name": "@Year", "Value": filters.Year },
                        { "Name": "@Month", "Value": filters.Month }
                    ],
                    success: {
                        fn: function (responseList) {
                            //Create Log
                            _insertAuditLog({
                                Action: "Email Notification",
                                Description: "Notification '" + typeNotification + "' for " + $("#selPeriod").find("option:selected").val() + " was sent"
                            });

                            _showNotification("success", "Notification '" + typeNotification + "' for " + $("#selPeriod").find("option:selected").val() + " was sent successfully")
                        }
                    }
                });
            }
        }],
        title: "Are you sure you want to send '" + typeNotification + "' notification?",
        contentHtml: htmlContentModal
    });
}

function loadTablePendingDocsByResponsible() {
    var filters = _getCurrentFilters();
    var sql =
        "SELECT [ResponsibleSOEID], [ResponsibleDesc], [PendingDocs] \
         FROM [dbo].[fnSDocGetPendingByResponsible](" + filters.IDRegion + ", " + filters.Year + ", " + filters.Month + ", '" + $("#selStatus").val() + "') \
         ORDER BY [PendingDocs] DESC";

    $.jqxGridApi.create({
        showTo: "#tblResponsibleDocs",
        options: {
            //for comments or descriptions
            height: "400",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: true,
            sortable: true,
            editable: true
        },
        sp: {
            SQL: sql
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        columns: [ 
            { name: 'ResponsibleSOEID', text: 'Responsibles SOEID', width: '20%', type: 'string', filtertype: 'input' },
            { name: 'ResponsibleDesc', text: 'Responsibles Desc', width: '60%', type: 'string', filtertype: 'input' },
            { name: 'PendingDocs', text: 'Pending Docs', width: '20%', type: 'string', filtertype: 'input' }
        ]
    });
}