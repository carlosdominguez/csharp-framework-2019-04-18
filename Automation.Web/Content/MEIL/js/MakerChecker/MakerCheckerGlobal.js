﻿/// <reference path="../../../Shared/plugins/util/global.js" />

function _loadIssueAdmin(customOptions) {
    //Default Options.
    var options = {
        showTo: "",
        issueID: "",
        action: "New"
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Issue Administration...",
        url: "/MEIL/MakerChecker/PartialViewAdminIssue?" + $.param({
            pissueID: options.issueID,
            paction: options.action
        }),
        success: function (htmlResponse) {
            $(options.showTo).contents().remove();
            $(options.showTo).html(htmlResponse);
        }
    });
}