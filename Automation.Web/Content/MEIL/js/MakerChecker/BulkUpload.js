﻿var bulkExcelId;
var _GlobalVars = {
    //Cambiar variable
    Lista: []
};
var viewModel = {
    //Cambiar variable
    Lista: ko.observable(),
    BulkUploadById: ko.observableArray()

};
viewModel.gridLogModel = new KOGridModel({
    data: viewModel.Lista,
    columns: [
        //Columnas Cambiar
        { header: "Actions", data: "IdLogBulkUpload", dataTemplate: 'divActions' },
        { header: "Created By ", data: "BulkUploadSoeid" },
        { header: "Count of Issues", data: "BulkUploadRows" },
        { header: "Date of upload", data: "BulkUploadDate" }
        
        //{ header: "Start Time", data: "StartTime" }
    ],
    pageSize: 4
});

viewModel.gridShowBulkUpload = new KOGridModel({
    data: viewModel.BulkUploadById,
    columns: [
        //Columnas Cambiar

        //{ header: "Issue Id", data: "IssueID" },
        { header: "", template: 'loquequiera' },
        { header: "Accountable Person", data: "AccountablePerson" },
        { header: "Action Taken", data: "ActionTaken" },
        { header: "Country", data: "Country" },
        { header: "Date of Occurrence", data: "DateOfOccurrence" },
        { header: "Process Area", data: "ProcessArea" },
        { header: "Reason", data: "Reason" },
        { header: "Reported By", data: "ReportedBy" },
        { header: "ReportedByRole", data: "ReportedByRole" },
        { header: "Reporting Period", data: "ReportingPeriod" },
        { header: "Resolution Date", data: "ResolutionDate" },
        { header: "Role", data: "Role" },
        { header: "Short Description of Error", data: "ShortDescriptionOfError" },
        { header: "Status", data: "Status" },

        //{ header: "Start Time", data: "StartTime" }
    ],
    pageSize: 4
});
function fnShowIssues(parent) {
    
    //document.getElementById('showListOfIssuesForBulkID').click();
    $('#showListOfIssuesForBulkID').modal();
    var listOfParams = {
        IDBulkUpload: parent.value
    }
    bulkExcelId = listOfParams.IDBulkUpload;
    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Loading ...",
        name: "[dbo].[ListBulkUploadExcelByID]",
        params: [
            { "Name": "@IdLogBulkUpload", "Value": listOfParams.IDBulkUpload }
        ],
        success: {
            fn: function (responseList) {
                //var viewIssueID = {
                //    dataTest: ko.observable(responseList)
                //}
                //ko.cleanNode(viewModel);
                //ko.applyBindings(viewIssueID);
            
               
                viewModel.BulkUploadById([])
                //var url = 'MEIL/MakerChecker/AdminIssue?pissueID=' + responseList.value;
                responseList.forEach(function (item, index, array) {
                    var column = {
                            "IssueID"                   : item.IssueID,         
                            "AccountablePerson"         : ko.observable(item.AccountablePerson),
                            "ActionTaken"               : ko.observable(item.ActionTaken),  
                            "Country"                   : ko.observable(item.Country),           
                            "DateOfOccurrence"          : ko.observable(item.DateOfOccurrence),  
                            "ProcessArea"               : ko.observable(item.ProcessArea),       
                            "Reason"                    : ko.observable(item.Reason),            
                            "ReportedBy"                : ko.observable(item.ReportedBy),        
                            "ReportedByRole"            : ko.observable(item.ReportedByRole),    
                            "ReportingPeriod"           : ko.observable(item.ReportingPeriod),   
                            "ResolutionDate"            : ko.observable(item.ResolutionDate),    
                            "Role"                      : ko.observable(item.Role),              
                            "ShortDescriptionOfError"   : ko.observable(item.ShortDescriptionOfError),
                            "Status"                    : ko.observable(item.Status)
                    }                    
                    viewModel.BulkUploadById.push(column);        
                });
            }
        }
    });

    
}

function downloadToExcel() {
    _downloadExcel({
        spName: "[dbo].[ListBulkUploadExcelByIDToExcel]",
        spParams: [
            { "Name": "@IdLogBulkUpload", "Value": bulkExcelId }
        ],
        filename: "BulkIssueList-" + moment().format('MMM-DD-YYYY-HH-MM'),
        success: {
            msg: "Please wait, generating report..."
        }
    });
}
function fnGoToIssue(parent) {
    var list = {
        issueID: parent.value
    }
    var url = '/MC/MEIL/MakerChecker/AdminIssue?pissueID=' + parent.value;
}
function generateMakerCheckerID(cont) {
    return createCustomMCID(cont);
}

function startObjectsIntro() {
    var intro = introJs();
    intro.setOptions();
    intro.start().onbeforechange(function () {

        if (intro._currentStep == "4") {
            $('#showListOfIssuesForBulkID').modal();

        }
    });
}

function createCustomMCID(cont) {
    var myDate = new Date();
    var id = "";
    var dbIDLen = 16;

    if (typeof addMilliseconds == "undefined") {
        addMilliseconds = true;
    }

    var uniqueId = ((Math.floor(Math.random() * 999)) + "") + (cont + "");
    var lastPartOfId = uniqueId + "";
    var startPartOfId = "";

    startPartOfId += myDate.getFullYear();
    startPartOfId += (myDate.getMonth() + 1) < 10 ? '0' + (myDate.getMonth() + 1) : (myDate.getMonth() + 1);
    startPartOfId += myDate.getDate() < 10 ? '0' + myDate.getDate() : myDate.getDate();
    //startPartOfId += myDate.getMilliseconds() < 10 ? '0' + myDate.getMilliseconds() : myDate.getMilliseconds();
    //startPartOfId += myDate.getUTCMilliseconds() + cont;
    startPartOfId += myDate.getUTCMilliseconds() + "";

    var startPartOfIdLen = dbIDLen - lastPartOfId.length;
    id = startPartOfId.substring(0, startPartOfIdLen) + lastPartOfId;
    id = id.substring(id.length - 13);

    //id += myDate.getHours() < 10 ? '0' + myDate.getHours() : myDate.getHours();
    //id += myDate.getMinutes() < 10 ? '0' + myDate.getMinutes() : myDate.getMinutes();

    return "MC" + id + "I";
}

//function _createCustomID() {
//    var myDate = new Date();
//    var id = "";
//    var date = new Date();
//    var components = [
//        //date.getFullYear(),
//        date.getYear(),
//        //date.getMonth(),
//        //date.getDate(),
//        //date.getHours(),
//        date.getMilliseconds(),
//        Math.floor(Math.random() * 899999 + 100000) 
//    ];
//    var id = components.join("");
//    return id;
//}
var ProcessAreaID;
$(document).ready(function () {
    ko.applyBindings(viewModel);
    fnListIssues();

    //On Click Donwload
    $("#downloadBulkIssuess").click(function () {
        downloadToExcel();
    });
    //On Click Load Table Button
    $(".btnLoadTable").click(function () {
        //Load Table
        loadProcessActivityTable();
    });

    //On Click View Info
    $(".btnViewInfo").click(function () {
        var selectedRow = $.jqxGridApi.getOneSelectedRow("#tblProcessActivities", true);
        if (selectedRow) {
            _openModalProcessActivityInfo({
                filters: {
                    source: selectedRow.SR,
                    idProcessActivity: selectedRow.ID
                },
                onReady: function () { }
            });
        }
    });

    //On Click Download BulkUploadTemplate
    $(".btnDownloadBulkTemplate").click(function () {
        window.open("/MC/MEIL/MakerChecker/GetExcelTemplate/");
    });

    //On Click Delete CPR Activities
    $(".btnDeleteCPRActivities").click(function () {
        deleteCPRActivities();
    });
    function fnListIssues() {
        _callProcedure({
            loadingMsgType: "topBar",
            loadingMsg: "Loading ...",
            name: "[dbo].[LISTBulkUploadExcel]",
            params: [
                    { "Name": "@SOEID", "Value": _getSOEID() }
            ],
            success: {
                fn: function (responseList) {
                    viewModel.Lista(responseList);
                    //_showNotification("success", "Data was added successfully.", "AlertAddComment");
                    //onSuccess();
                }
            }
        });
    }

    //Create to import excel
    XLSX.createJsXlsx({
        idElement: "js-xlsx-plugin",
        fileInput: {
            dropText: "Drop an Excel file here...",
            hideDrop: false,
            inputText: "... or click here to select a file"
        },
        onSuccess: function (excelSource) {
            //console.log(excelSource);

            isValidExcel({
                excelSource: excelSource,
                onSuccess: function (excelSourceValidated) {
                    //console.log($(".jqxDropDownProcess").find(".fieldIDProcess").attr("value"), excelSource["TOUPLOAD"]);

                    _callServer({
                        loadingMsgType: "fullLoading",
                        loadingMsg: "Uploading data...",
                        url: '/MEIL/Makerchecker/BulkUploadExcel',
                        data: {
                            'pjsonData': JSON.stringify(excelSourceValidated["Template"]),
                            'pidProcess': $(".jqxDropDownProcess").find(".fieldIDProcess").attr("value")
                        },
                        type: "post",
                        success: function (json) {
                            //Clear input file
                            $(".jsxlsx-input-file").val("");
                            fnListIssues();
                            
                            //Check if file exist
                            _showAlert({
                                id: "UploadActivityMessage",
                                type: 'success',
                                content: "Data was uploaded successfully.",
                                animateScrollTop: true
                            });

                            //Refresh Grid
                            //loadProcessActivityTable();
                        },
                        error: function () {
                            //Clear input file
                            $(".jsxlsx-input-file").val("");
                        }
                    });
                },
                onError: function (excelSourceValidated) {

                    //Clear input file
                    $(".jsxlsx-input-file").val("");
                }
            });

            //Clear input file
            $(".jsxlsx-input-file").val("");
        }

    });
    
    //Upload Fields
    if ($("#HFViewType").val() == "Upload") {
        // initialize jqxDropDownProcess
        $(".jqxDropDownProcess").jqxDropDownButton({
            width: "100%", height: 34
        });

        //Set "--Select Process --" in dropdown.
        var dropDownContent = '<div class="fieldIDProcess" value="0" style="padding: 8px;">-- Select --</div>';
        $(".jqxDropDownProcess").jqxDropDownButton('setContent', dropDownContent);

        // on click clear parent 
        $(".btnClearParent").click(function () {
            //Clear Parent Data
            $(".jqxDropDownProcess").jqxDropDownButton('setContent', '<div class="fieldIDProcess" value="0" style="padding: 8px;">-- Select --</div>');

            //Refresh Process Activities
            loadProcessActivityTable();
        });
        
    }
});


function isValidExcel(defaultOptions) {

    //Default Options.
    var option = {
        excelSource: [],
        onSuccess: null,
        onError: null
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, option, defaultOptions);

    var excelSource = option.excelSource;
    var isValid = true;
    var sheetName = "Template";
    var allowedColumns = [
        //Add all valid columns e. g. some times is "Country" but the real name need to be "Region"
        //{ "Country": false, "Region": true },
        {"IssueID": true },
        { "Accountable Person": true },
        { "Role": true },
        { "Process Area": true },
        { "Date of Occurrence": true },
        { "Reporting Period": true },
        { "Country": true },
        { "Reason": true },
        { "Short Description of Error": true },
        { "Resolution Date": true },
        { "Status": true },
        { "Action Taken": true },
        { "Reported By": true },
        { "ReportedByRole": true },
    ];

    //Validate if Sheet with name sheetName exists
    if (!excelSource[sheetName]) {
        _showAlert({
            id: "UploadActivityMessage",
            type: 'error',
            content: "Please review the Excel file that exists the Sheet Name '<b>" + sheetName + "</b>' with the data to upload.",
            animateScrollTop: true
        });

        isValid = false;
    }
  
    //Validate columns
    if (isValid) {
        var excelColumnsKeys = Object.keys(excelSource[sheetName][0]);
        var missingCols = [];
        var extraCols = [];

        //Validate if the colums allowed are defined in the excel data
        for (var i = 0; i < allowedColumns.length; i++) {
            var allowedColumExist = false;
            var objAllowCol = allowedColumns[i];
            var keysAllowColNames = Object.keys(objAllowCol);
            var defaultKeyAllowColName = "";

            //comes empty in the array of excel but exist in the excel file
            if (objAllowCol.Ignore && objAllowCol.Ignore == 1) {
                allowedColumExist = true;
            } else {
                for (var j = 0; j < keysAllowColNames.length && allowedColumExist == false; j++) {
                    var keyAllowColName = keysAllowColNames[j];

                    //Set default Key
                    if (objAllowCol[keyAllowColName]) {
                        defaultKeyAllowColName = keyAllowColName;
                    }

                    //Look if allowed column exist in excel columns
                    for (var k = 0; k < excelColumnsKeys.length; k++) {
                        var excelCol = excelColumnsKeys[k];

                        if (excelCol.toLocaleLowerCase() == keyAllowColName.toLocaleLowerCase()) {
                            allowedColumExist = true;
                            break;
                        }
                    }
                }
            }

            //if (allowedColumExist == false) {
            //    missingCols.push(defaultKeyAllowColName);
            //}
        }

        //Validate if the colums of the excel are allowed colums
        for (var i = 0; i < excelColumnsKeys.length; i++) {
            var excelCol = excelColumnsKeys[i];
            var excelColAllowed = false;

            for (var k = 0; k < allowedColumns.length && excelColAllowed == false; k++) {
                var objAllowCol = allowedColumns[k];
                var keysAllowColNames = Object.keys(objAllowCol);

                for (var j = 0; j < keysAllowColNames.length; j++) {
                    var keyAllowColName = keysAllowColNames[j];

                    if (excelCol.toLocaleLowerCase() == keyAllowColName.toLocaleLowerCase()) {
                        excelColAllowed = true;
                        break;
                    }
                }
            }

            //if (excelColAllowed == false) {
            //    extraCols.push(excelCol);
            //}
        }
        //Create IssueID col, and delete from missingCols to continue with the valdation
        if (missingCols.length > 0 && missingCols == "IssueID"){
            allowedColumns.push("IssueID");
            //Delete from missingCols
            missingCols.splice(0);
        }
       
        if (extraCols.length > 0 || missingCols.length > 0) {

            var msg = "<b>Excel file must have the following column names:</b> <br>";
            var flagCloseUl;

            for (var i = 0; i < allowedColumns.length; i++) {
                var objAllowCol = allowedColumns[i];
                var keysAllowColNames = Object.keys(objAllowCol);

                for (var j = 0; j < keysAllowColNames.length; j++) {
                    var keyAllowColName = keysAllowColNames[j];

                    if (i == 0 || i == 6 || i == 12) {
                        msg += "<ul class='col-md-4'>";
                        flagCloseUl = false;
                    }

                    //Set default Key
                    if (objAllowCol[keyAllowColName] === true) {
                        msg += "<li>" + keyAllowColName + "</li>";
                    }

                    if (i == 5 || i == 11 || i == 17) {
                        msg += "</ul>";
                        flagCloseUl = true;
                    }
                }
            }

            if (flagCloseUl == false) {
                msg += "</ul>";
            }

            msg += "<div class='clearfix'></div><br>";
            if (extraCols.length > 0) {
                msg += "<b>Please remove the following extra columns in the Excel file (review hidden colums): </b><br>";
                msg += "<ul class='col-md-4'>";
                for (var i = 0; i < extraCols.length; i++) {
                    msg += "<li>" + extraCols[i] + "</li>";
                }
                msg += "</ul>";
                msg += "<div class='clearfix'></div><br>";
            }

            if (missingCols.length > 0) {
                msg += "<b>Please add the following columns in the Excel file: </b><br>";
                msg += "<ul class='col-md-4'>";
                for (var i = 0; i < missingCols.length; i++) {
                    msg += "<li>" + missingCols[i] + "</li>";
                }
                msg += "</ul>";
                msg += "<div class='clearfix'></div>";
            }

            _showAlert({
                id: "UploadActivityMessage",
                type: 'error',
                content: msg,
                animateScrollTop: true
            });
            isValid = false;
        }
    }

    //Validate BulkUpload Data
    if (isValid) {
        var excelIssuesList = excelSource[sheetName];
        var htmlMsg = "";
        var objUniqueSOEIDsInExcel = {};
        var strSOEIDList = "";
       
        var tempDate = "";
        var arrayDate = null;

        var tempSOEID = "";
        var arraySOEIDs = null;

        var objUniqueProcessAreaInExcel = {};
        var arrayProcessArea = null;
        var tempProcessArea = "";
        var strProcessArea = "";

        var objUniqueCountryInExcel = {};
        var arrayCountry = null;
        var tempCountry = "";
        var strCountry = "";
        
        var objUniqueMakerCheckerReason = {};
        var arrayMakerCheckerReason = null;
        var tempMakerCheckerReason = "";
        var strMakerCheckerReason = "";

        var excelRowNumber;
        var objIssueRow;

        
        //Correct formatted date
        //for (var i = 0; i < excelIssuesList.length; i++) {
        //    excelRowNumber = i + 2;
        //    objIssueRow = excelIssuesList[i];

        //    if (objIssueRow["Date of Occurrence"]) {
        //        //Accountable Person
        //        arrayDate = objIssueRow["Date of Occurrence"].split(";");
        //        for (var l = 0; l < arrayDate.length; l++) {
        //            objIssueRow["Date of Occurrence"] = moment(arrayDate[l], 'DD/MM/YYYY').format('MM-DD-YYYY');
        //        }
        //    }
        //}
        //Validate if SOEIDs Exists
        for (var i = 0; i < excelIssuesList.length; i++) {
            excelRowNumber = i + 2;
            objIssueRow = excelIssuesList[i];
            
            if (objIssueRow["Accountable Person"]) {
                //Accountable Person
                arraySOEIDs = objIssueRow["Accountable Person"].split(";");
                for (var l = 0; l < arraySOEIDs.length; l++) {
                    tempSOEID = arraySOEIDs[l].toUpperCase();
                    if (typeof objUniqueSOEIDsInExcel[tempSOEID] == "undefined") {
                        objUniqueSOEIDsInExcel[tempSOEID] = excelRowNumber + "";
                        strSOEIDList += tempSOEID + ";";
                    } else {
                        objUniqueSOEIDsInExcel[tempSOEID] += (";" + excelRowNumber)
                    }
                }
            }

            // Get Process Area List
            if (objIssueRow["Process Area"]) {
                objIssueRow["Process Area"] = objIssueRow["Process Area"].split("-");
                objIssueRow["Process Area"] = objIssueRow["Process Area"][1];
                //Process Area
                arrayProcessArea = objIssueRow["Process Area"].split(";");
               
                console.log(arrayProcessArea);
                for (var l = 0; l < arrayProcessArea.length; l++) {
                    tempProcessArea = arrayProcessArea[l];
                    if (typeof objUniqueProcessAreaInExcel[tempProcessArea] == "undefined") {
                        objUniqueProcessAreaInExcel[tempProcessArea] = excelRowNumber + "";
                        strProcessArea += tempProcessArea + ";";

                    } else {
                        objUniqueProcessAreaInExcel[tempProcessArea] += (";" + excelRowNumber)
                    }
                }
            }

            // Get Country
            if (objIssueRow["Country"]) {
                //Country
                arrayCountry = objIssueRow["Country"].split(";");
                for (var l = 0; l < arrayCountry.length; l++) {
                    tempCountry = arrayCountry[l];
                    if (typeof objUniqueCountryInExcel[tempCountry] == "undefined") {
                        objUniqueCountryInExcel[tempCountry] = excelRowNumber + "";
                        strCountry += tempCountry + ";";

                    } else {
                        objUniqueCountryInExcel[tempCountry] += (";" + excelRowNumber)
                    }
                }
            }

            // Get makerChekerReason
            if (objIssueRow["Reason"]) {
                //Country
                arrayMakerCheckerReason = objIssueRow["Reason"].split(";");
                for (var l = 0; l < arrayMakerCheckerReason.length; l++) {
                    tempMakerCheckerReason = arrayMakerCheckerReason[l];
                    if (typeof objUniqueMakerCheckerReason[tempMakerCheckerReason] == "undefined") {
                        objUniqueMakerCheckerReason[tempMakerCheckerReason] = excelRowNumber + "";
                        strMakerCheckerReason += tempMakerCheckerReason + ";";

                    } else {
                        objUniqueMakerCheckerReason[tempMakerCheckerReason] += (";" + excelRowNumber)
                    }
                }
            }

        }
        var sqlValidateCountry =
            "SELECT \n" +
            "ISNULL(P.[dsc_Country], 0)	AS [IDCountry]  \n" +
            "FROM (   \n" +
            "    SELECT [Val] AS [IdCountry]\n" +
            "    FROM [Automation].[dbo].[func_Split]('" + strCountry + "', ';', 1)\n" +
            "    ) TG \n" +
            "LEFT JOIN [dbo].[tlkpCountry]  P ON (P.[dsc_Country]  = TG.[IdCountry] )\n" +
            "WHERE  [dsc_Country] <> '- Regional'   ";

        //Sql for validate SOEIDs
        var sqlValidateSOEIDs =
            "SELECT \n" +
            "   TG.[SOEID], \n" +
            "   CASE ISNULL(E.[SOEID], '') \n" +
            "       WHEN '' THEN 0 \n" +
            "       ELSE 1 \n" +
            "   END AS [Exists] \n" +
            "FROM ( \n" +
            "    SELECT [Val] AS [SOEID] \n" +
            "    FROM [Automation].[dbo].[func_Split]('" + strSOEIDList + "', ';', 1) \n" +
            ") TG \n" +
            "LEFT JOIN [Automation].[dbo].[tblEmployee] E WITH(READUNCOMMITTED) ON E.[SOEID] = TG.[SOEID] ";

        //Sql for validate process area
        var sqlValidateProcessArea =
            "SELECT\n" +
            "    TG.[ExcelProcessName],\n" +
            "    ISNULL(P.[ID], 0) AS [IDProcess] \n" +
            "FROM ( \n" +
            "        SELECT [Val] AS [ExcelProcessName] \n" +
            "        FROM [Automation].[dbo].[func_Split]('" + strProcessArea + "', ';', 1) \n" +
            "    ) TG \n" +
            "    LEFT JOIN [dbo].[tblProcess] P ON (P.[Name] = TG.[ExcelProcessName])";

        var sqlValidateMakerCheckerReason =
            "SELECT  \n" +
            "P.[Description]," +
            "ISNULL(P.[ID], 0)	AS [MakerCheckerReasonID]  \n" +
            "FROM (  \n" +
            "    SELECT [Val] AS [IdMakerCheckerReason]  \n" +
            "    FROM [Automation].[dbo].[func_Split]('" + strMakerCheckerReason + "', ';', 1)  \n" +
            ") TG  \n" +
            "    LEFT JOIN [dbo].[tblMakerCheckerReason] P ON(P.[Description] = TG.IdMakerCheckerReason)  \n" +
            "WHERE  \n" +
            "    [IsDeleted] = 0 ";

       
       

        //Validate bulk Soeid
        _callServer({
            loadingMsgType: "fullLoading",
            loadingMsg: "Validating SOEIDs...",
            url: '/Ajax/ExecQuery',
            data: { 'pjsonSql': _toJSON(sqlValidateSOEIDs) },
            type: "POST",
            success: function (resultListSOEIDs) {
                //Validate Excel Data
                for (var i = 0; i < excelIssuesList.length; i++) {
                    var objSOEIDValidation = null;

                    excelRowNumber = i + 2;
                    objIssueRow = excelIssuesList[i];

                    // Validation for SOEIDs
                    if (objIssueRow["Accountable Person"]) {

                        //Accountable Person
                        arraySOEIDs = objIssueRow["Accountable Person"].split(";");

                        for (var h = 0; h < arraySOEIDs.length; h++) {
                            tempSOEID = arraySOEIDs[h].toUpperCase();

                            objSOEIDValidation = _findOneObjByProperty(resultListSOEIDs, "SOEID", tempSOEID);
                            if (objSOEIDValidation.Exists == "0") {
                                htmlMsg += "<li>Row (" + excelRowNumber + "): Column [Accountable Person] your value '" + tempSOEID + "' not exists in our Global Directory records. </li>";
                            }
                        }
                    } else {
                        htmlMsg += "<li>Row (" + excelRowNumber + "): Column [Accountable Person] value is missing </li>";
                    }

                    // Validation for Role
                    if (!objIssueRow["Role"]) {
                        htmlMsg += "<li>Row (" + excelRowNumber + "): Column [Role] value is missing </li>";
                    }

                    // Validation for Process Area
                    if (!objIssueRow["Process Area"]) {
                        htmlMsg += "<li>Row (" + excelRowNumber + "): Column [Process Area] value is missing </li>";
                    }

                    // Validation for Date of Occurrence
                    if (!objIssueRow["Date of Occurrence"]) {
                        htmlMsg += "<li>Row (" + excelRowNumber + "): Column [Date of Occurrence] value is missing </li>";
                    }

                    // Validation for Reporting Period
                    if (!objIssueRow["Reporting Period"]) {
                        htmlMsg += "<li>Row (" + excelRowNumber + "): Column [Reporting Period] value is missing </li>";
                    }

                    // Validation for Country
                    if (!objIssueRow["Country"]) {
                        htmlMsg += "<li>Row (" + excelRowNumber + "): Column [Country] value is missing </li>";
                    }

                    // Validation for Reason
                    if (!objIssueRow["Reason"]) {
                        htmlMsg += "<li>Row (" + excelRowNumber + "): Column [Reason] value is missing </li>";
                    }

                    // Validation for Short Description of Error
                    if (!objIssueRow["Short Description of Error"]) {
                        htmlMsg += "<li>Row (" + excelRowNumber + "): Column [Short Description of Error] value is missing </li>";
                    }

                    // Validation for Short Description of Error
                    if (!objIssueRow["Reported By"]) {
                        htmlMsg += "<li>Row (" + excelRowNumber + "): Column [Reported By] value is missing </li>";
                    }

                    // Validation for Short Description of Error
                    if (!objIssueRow["ReportedByRole"]) {
                        htmlMsg += "<li>Row (" + excelRowNumber + "): Column [ReportedByRole] value is missing </li>";
                    }
                }

                if (htmlMsg) {
                    htmlMsg = "<ul>" + htmlMsg + "</ul>";

                    _showAlert({
                        id: "UploadActivityMessage",
                        type: 'error',
                        content: "<b>Please review the following error in the data of your Excel:</b>" + htmlMsg,
                        animateScrollTop: true
                    });

                    isValid = false;
                }

                //Add dinamically ID to excelIssuesList
                //objIssueRow.IssueID = generateMakerCheckerID();
                fnSetIssueID();
                //IssueId
                function fnSetIssueID() {
                    //setTimeout(function () {  
                       
                        for (var i = 0; i < excelSource[sheetName].length; i++) {
                            var issueID = generateMakerCheckerID(i);
                            //ADD IssueIDColumn
                            excelSource[sheetName][i].IssueID = issueID;

                            console.log(excelSource[sheetName][i].IssueID);
                        }
                    //}, 90);
                }

                //Validate bulk makerCheckerReason
                _callServer({
                    loadingMsgType: "fullLoading",
                    loadingMsg: "Validating Maker Checker Reason...",
                    url: '/Ajax/ExecQuery',
                    data: { 'pjsonSql': _toJSON(sqlValidateMakerCheckerReason) },
                    type: "POST",
                    success: function (resultListMakerCheckerReasonId) {
                        MakerCheckerReasonID = resultListMakerCheckerReasonId;
                        SetMakerCheckerReasonId(MakerCheckerReasonID);
                        //makerCheckerReason
                        function SetMakerCheckerReasonId(MakerCheckerReasonID) {
                            for (var i = 0; i < excelSource[sheetName].length; i++) {
                                //ADD IDProcess
                                var objTempProcess = _findOneObjByProperty(MakerCheckerReasonID, "Description", excelSource[sheetName][i]["Reason"]);
                                excelSource[sheetName][i].ReasonMakerCheckerID = objTempProcess.MakerCheckerReasonID;
                            }

                        }

                        //Validate bulk Country
                        _callServer({
                            loadingMsgType: "fullLoading",
                            loadingMsg: "Validating ProcessArea...",
                            url: '/Ajax/ExecQuery',
                            data: { 'pjsonSql': _toJSON(sqlValidateCountry) },
                            type: "POST",
                            success: function (resultListCountryId) {
                                CountryID = resultListCountryId;
                                SetCountryId(CountryID);
                                //Country
                                function SetCountryId(CountryID) {
                                    for (var i = 0; i < excelSource[sheetName].length; i++) {
                                        //ADD IDProcess
                                        var objTempProcess = _findOneObjByProperty(CountryID, "IDCountry", excelSource[sheetName][i]["Country"]);
                                        excelSource[sheetName][i].CountryId = objTempProcess.IDCountry;
                                    }
                                }
                                //Validate bulk ProcessArea
                                _callServer({
                                    loadingMsgType: "fullLoading",
                                    loadingMsg: "Validating ProcessArea...",
                                    url: '/Ajax/ExecQuery',
                                    data: { 'pjsonSql': _toJSON(sqlValidateProcessArea) },
                                    type: "POST",
                                    success: function (resultListProcessArea) {
                                        ProcessAreaID = resultListProcessArea;
                                        SetProcessAreaId(ProcessAreaID)
                                        //ProcessArea
                                        function SetProcessAreaId(ProcessAreaID) {

                                            for (var i = 0; i < excelSource[sheetName].length; i++) {

                                                var objTempProcess = _findOneObjByProperty(ProcessAreaID, "ExcelProcessName", excelSource[sheetName][i]["Process Area"]);
                                                excelSource[sheetName][i].ProcessAreaId = objTempProcess.IDProcess;
                                            }

                                        }

                                        if (isValid) {
                                            option.onSuccess(excelSource);
                                        } else {
                                            option.onError(excelSource);
                                        }
                                    }

                                });

                            }

                        });

                    }

                });
            },
            error: function () {
                option.onError(excelSource);
            }


        });
        
    }

    function isValidGOCs(arrayGOCs) {

    }
}


function fnListIssue(item) {
    var location = window.location.origin;
    var link = ("href", _getViewVar("SubAppPath") + "/MEIL/MakerChecker/AdminIssue?pissueID=" + item.IssueID);
    window.location.href = link;
}