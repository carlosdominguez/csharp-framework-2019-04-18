/// <reference path="../../../Shared/plugins/util/global.js" />
/// <reference path="MakerCheckerGlobal.js" />
$(document).ready(function () {
    var action = "New";
    var issueID = $("#HFAdminIssueID").val();

    //Get Issue ID
    if (issueID) {
        action = "Edit";

        //Set page title
        _setPageTitle("Control Execution Finding");
    } else {

        //Set page title
        _setPageTitle("New Control Execution Finding");
    }

    //Load form to add or edit an issue
    _loadIssueAdmin({
        showTo: "#adminIssueContent",
        issueID: issueID,
        action: action
    });

    //Load table of issues
    loadMakerCheckerIssueList();

    //Hide Menu
    _hideMenu();

});

function loadMakerCheckerIssueList() {
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Issues...",
        name: "[dbo].[spMakerCheckerListIssues]",
        params: [
            { "Name": "@CreatedBy", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                loadTable(responseList);
            }
        }
    });
    
    function loadTable(dataList) {
        $.jqxGridApi.create({
            showTo: "#tblMakerCheckerIssues",
            options: {
                //for comments or descriptions
                height: "450",
                autoheight: false,
                autorowheight: false,
                showfilterrow: true,
                sortable: true,
                editable: true,
                selectionmode: "singlerow"
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                dataBinding: "Large Data Set Local",
                rows: dataList
            },
            groups: [],
            columns: [
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                { name: 'CreatedDate', text: 'Created Date', width: '130px', type: 'date', filtertype: 'date', cellsformat: 'MMM dd, yyyy' },
                { name: 'IssueID', text: 'Issue ID', width: '160px', type: 'string', filtertype: 'input' },
                { name: 'AccountablePersonSOEID', text: 'Accountable Person', width: '150px', type: 'string', filtertype: 'checkedlist' },
                { name: 'ManagerAccountableSOEID', text: 'Manager Accountable', width: '150px', type: 'string', filtertype: 'checkedlist' },
                { name: 'AccountablePersonRole', text: 'Role', width: '110px', type: 'string', filtertype: 'checkedlist' },
                { name: 'Process', text: 'Process Area', width: '315px', type: 'string', filtertype: 'checkedlist' },
                { name: 'Country', text: 'Country', width: '135px', type: 'string', filtertype: 'checkedlist' },
                { name: 'Reason', text: 'Reason', width: '290px', type: 'string', filtertype: 'checkedlist' },
                { name: 'DateOfOccurrence', text: 'Date Of Ocurrence', width: '130px', type: 'date', filtertype: 'date', cellsformat: 'MMM dd, yyyy' },
                { name: 'ReportingPeriod', text: 'Reporting Period', width: '120px', type: 'string', filtertype: 'checkedlist' },
                { name: 'Description', text: 'Short Description', width: '500px', type: 'string', filtertype: 'input' }
            ],
            ready: function () {
                //On select row event
                $("#tblMakerCheckerIssues").on('rowselect', function (event) {
                    $(".btnViewIssue").attr("href", _getViewVar("SubAppPath") + "/MEIL/MakerChecker/AdminIssue?pissueID=" + event.args.row.IssueID);
                });
            }
        });
    }
}