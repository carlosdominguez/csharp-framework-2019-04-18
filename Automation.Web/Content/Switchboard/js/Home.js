$(document).ready(function () {
    getGridTaskToday();

    $("#dateInput").jqxDateTimeInput({ width: '300px', height: '25px' });

    var source = new Array();
    for (i = 0; i < 10; i++) {
        var movie = 'avatar.png';
        var title = 'Avatar';
        var year = 2009;
        switch (i) {
            case 1:
                movie = 'Complete';
                title = 'Complete';
                year = 2006;
                break;
        }
  
    }
    // Create a jqxComboBox
    $("#jqxStatus").jqxComboBox({ source: source, selectedIndex: 0, width: '250', height: '25px' });

});


function getGridTaskToday()
{
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/Switchboard/getSwitchboardData',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                        { name: 'EventID', type: 'string' }
                      , { name: 'NodeID', type: 'string' }
                      , { name: 'GOC', type: 'string' }
                      , { name: 'EventName', type: 'string' }
                      , { name: 'StatusID', type: 'string' }
                      , { name: 'StatusName', type: 'string' }
                      , { name: 'Frequence', type: 'string' }
                      , { name: 'Month', type: 'string' }
                      , { name: 'BusinessDay', type: 'string' }
                      , { name: 'BusinessDayString', type: 'string' }
                      , { name: 'Date', type: 'string' }
                      , { name: 'StartTime', type: 'string' }
                      , { name: 'Duration', type: 'string' }
                      , { name: 'EndTime', type: 'string' }
                      , { name: 'Timezone', type: 'string' }
                      , { name: 'DateUTC', type: 'string' }
                      , { name: 'Taxonomy', type: 'string' }
                      , { name: 'ProcessName', type: 'string' }
                      , { name: 'System', type: 'string' }
                      , { name: 'MCA', type: 'string' }
                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            var imgActions = function (row, datafield, value) {

                //var rowData = $('#jqxGridSwitchboard').jqxGrid('getrowdata', row);


                return '<table style="margin-top:5px;" ><tr><td><i class="fa fa-check-square-o" style="font-size:26px; color:blue;" title="Complete"></i></br></td><td style="width:8px;"></td><td><i class="fa fa-trash-o" style="font-size:26px; color:gray;"  title="Cancel"></i></td><td style="width:8px;"></td><td><i class="fa fa-clock-o" style="font-size:26px; color:red;"  title="Delay"></i></td></tr><tr><td><i class="fa fa-envelope" style="font-size:26px; color:blue;"  title="Email Chain"></i></td><td style="width:8px;"></td><td><i class="fa fa-unlock" style="font-size:26px; color:gray;"  title="UnLock"></i></td><td style="width:8px;"></td><td><i class="fa fa-volume-down" style="font-size:26px; color:red;"  title="Open MCA"></i></td></tr></table>';
                
            }

            var StartDate = function (row, datafield, value) {

                var rowData = $('#jqxGridSwitchboard').jqxGrid('getrowdata', row);

                var date = new Date(rowData.DateUTC);
                return '</br><center>' + date + '</center>';

            }

            var endDate = function (row, datafield, value) {

                var rowData = $('#jqxGridSwitchboard').jqxGrid('getrowdata', row);

                var date = new Date(rowData.DateUTC);
                var minutes = rowData.Duration;

                date.setTime(date.getTime() + minutes * 60 * 1000);

                return '</br><center>' + date + '</center>';

            }



            $('#jqxGridSwitchboard').jqxGrid(
           {
               source: dataAdapter,
               theme: 'energyblue',
               width: '100%',
               selectionmode: 'multiplecellsadvanced',
               autoheight: true,
               //rowheight: 30,
               rowsheight: 57,
               pagesize: 10,
               pageable: true,
               //filterable: true,
               //showfilterrow: true,
               columnsresize: true,
               selectionmode: 'singlecell',
               enablebrowserselection: true,
               columns: [
                { text: 'Actions', width: 93, editable: false, cellsrenderer: imgActions },
                 
                { text: 'Status', dataField: 'StatusName', width: '7%', editable: false },
                { text: 'Task', dataField: 'EventName',  width: '20%', editable: false },
                { text: 'Frequence', dataField: 'Frequence',  width: '7%', editable: false },
                { text: 'Business Day', dataField: 'BusinessDayString', width: '7%', editable: false },
                { text: 'Start Date Time', width: 170, editable: false, cellsrenderer: StartDate },
                { text: 'End Date Time', width:170, editable: false, cellsrenderer: endDate },
                { text: 'Duration', dataField: 'Duration', width: '5%', editable: false },
                { text: 'GOC', dataField: 'GOC',  width: '7%', editable: false },
                { text: 'Process Name', dataField: 'ProcessName',  width: '7%', editable: false },
                { text: 'System', dataField: 'System',  width: '7%', editable: false },
                { text: 'MCA', dataField: 'MCA',  width: '7%', editable: false }



               ]

           });

        }
    });



}