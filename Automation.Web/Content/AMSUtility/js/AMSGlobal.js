﻿function _dropDownFill(selectQuery, selectedId, dropList, List){
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Validating Journal Generation..",
        name: selectQuery,
        params: [{ Name: '@ListRequest', Value: List },
                 { Name: '@Param', Value: '' }],
        success: {
            fn: function (responseList) {
                $("#" + dropList + "").contents().remove();
                for (var i = 0; i < responseList.length; i++) {
                    var objFunction = responseList[i];
                    $("#" + dropList + "").append($('<option>', { value: objFunction.ID, text: objFunction.Text, selected: (objFunction.ID == selectedId) }));
                }
            }
        }
    });
}