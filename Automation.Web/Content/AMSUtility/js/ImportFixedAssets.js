/// <reference path="AMSGlobal.js" />

$(document).ready(function () {
    initUpload();
    _dropDownFill('[dbo].[spAMSLT_Tables]', '0', 'drop_State', 'ActiveStates');
    loadImportTracker();
});

function initUpload() {
    var fnClickUpload = function () {
        if ($("#drop_State :selected").val() == "0") {
            _showNotification("error", "Empty field found. Please select a state.");
        } else {
            var validation = 30;

            _callProcedure({
                loadingMsgType: "fullLoading",
                loadingMsg: "Validating Journal Generation..",
                name: "[dbo].[spAMSLT_Tables]",
                params: [{ Name: '@ListRequest', Value: "FAImportValidation" },
                        { Name: '@Param', Value: $("#drop_State :selected").val() }],
                async:true,
                success: {
                    fn: function (responseList) {
                        validation = responseList[0].Validation

                        if (validation > 0) {
                            var htmlContentModal = "<b>App Name:</b> Are you sure you want to overwrite the assets assigned to the following state " + $("#drop_State :selected").val() + " ?<br/>";
                            _showModal({
                                width: '35%',
                                modalId: "modalDelApp",
                                addCloseButton: true,
                                buttons: [{
                                    name: "Delete",
                                    class: "btn-danger",
                                    onClick: function () {
                                        $('#fine-uploader-manual-trigger').fineUploader('setParams', {
                                            'State': $("#drop_State :selected").val(),
                                            'Overwrite': "1"
                                        });

                                        $('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
                                    }
                                }],
                                title: "WARNING",
                                contentHtml: htmlContentModal
                            });
                        } else {
                            $('#fine-uploader-manual-trigger').fineUploader('setParams', {
                                'State': $("#drop_State :selected").val(),
                                'Overwrite': "0"
                            });

                            $('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
                        }
                    }
                }
            });

                 
        }
    };

    $('#fine-uploader-manual-trigger').fineUploader({
        template: 'qq-template-manual-trigger',
        request: {
            endpoint: '/AMSUtility/UploadAssets'
        },
        thumbnails: {
            placeholders: {
                waitingPath: '/Content/Shared/plugins/fine-uploader/placeholders/waiting-generic.png',
                notAvailablePath: '/Content/Shared/plugins/fine-uploader/placeholders/not_available-generic.png'
            }
        },
        callbacks: {
            onComplete: function (id, name, resonseJSON, xhr) {
                console.log(id, name, resonseJSON, xhr)
            },
            onAllComplete: function (succeeded, failed) {
                //console.log(succeeded, failed, this);
                if (failed.length == 0) {
                    _showAlert({
                        type: "success",
                        title: "Message",
                        content: "The file was uploaded successfully"
                    });
                    loadImportTracker();

                    $('#fine-uploader-manual-trigger').fineUploader('reset');
                    $('#trigger-upload').click(fnClickUpload);

                }
            },
            onError: function (id, name, errorReason, xhrOrXdr) {
                if (name) {
                    _showDetailAlert({
                        title: "Message",
                        shortMsg: "An error ocurred with the file '" + name + "'.",
                        longMsg: errorReason,
                        type: "Error",
                        viewLabel: "View Details"
                    });
                }
            }
        },
        validation: {
            allowedExtensions: ['xls', 'xlsx'],
            itemLimit: 1
        },
        autoUpload: false
    });
    $('#trigger-upload').click(fnClickUpload);
}

function loadImportTracker() {
    $.jqxGridApi.create({
        showTo: "#tblImportTracker",
        options: {
            //for comments or descriptions
            height: "400",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[spAMSLT_Tables]",
            Params: [
                { Name: "@ListRequest", Value: "AuditTrailFA" },
                { Name: '@Param', Value: "" }
            ]
        },

        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'AuditID', type: 'int', text: "Import ID", width: '7%', align: 'center' },
            { name: 'ActionType', type: 'String', text: "Action Type", width: '8%', align: 'center' },
            { name: 'Module', type: 'String', text: "Module", width: '10%', align: 'center' },
            { name: 'ModifiedObject', type: 'String', text: "ModifiedObject", width: '13%', align: 'center' },
            { name: 'OldValue', type: 'String', text: "OldValue", width: '20%', align: 'center' },
            { name: 'NewValue', type: 'String', text: "NewValue", width: '20%', align: 'center' },
            { name: 'SOEID', type: 'String', text: "SOEID", width: '10%', align: 'center' },
            { name: 'ActionDate', type: 'String', text: "ActionDate", width: '12%', align: 'center' }
        ],

        ready: function () {
            var rowCount = $("#tblImportTracker").jqxGrid('getrows').length;
            $("#TableImport").text('Row count: ' + rowCount);
            _hideLoadingFullPage();
        }
    });
};