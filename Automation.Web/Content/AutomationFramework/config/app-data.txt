{
	"Permits": [
		{
			"ID": "12",
			"ParentID": "",
			"Category": "Main",
			"ClassIcon": "fa-dashboard",
			"Name": "Home",
			"Path": "/",
			"AccessCommentHtml": "",
			"NumOrder": "1",
			"FlagIsGlobal": "1"
		},
		{
			"ID": "1",
			"ParentID": "",
			"Category": "Main",
			"ClassIcon": "fa-list",
			"Name": "Apps Doc Checker",
			"Path": "/AutomationFramework/AppList",
			"AccessCommentHtml": "",
			"NumOrder": "2",
			"FlagIsGlobal": "1"
		},
		{
			"ID": "10",
			"ParentID": "",
			"Category": "Main",
			"ClassIcon": "fa-list",
			"Name": "Automation DB",
			"Path": "",
			"AccessCommentHtml": "",
			"NumOrder": "3",
			"FlagIsGlobal": "1"
		},
		{
			"ID": "3",
			"ParentID": "10",
			"Category": "Main",
			"ClassIcon": "fa-calendar",
			"Name": "Coorporate Calendar",
			"Path": "/AutomationFramework/CoorporateCalendar",
			"AccessCommentHtml": "",
			"NumOrder": "3.1",
			"FlagIsGlobal": "1"
		},
		{
			"ID": "5",
			"ParentID": "10",
			"Category": "Main",
			"ClassIcon": "fa-briefcase",
			"Name": "GPL MS Responsible",
			"Path": "/AutomationFramework/GPLResponsible",
			"AccessCommentHtml": "",
			"NumOrder": "3.2",
			"FlagIsGlobal": "1"
		},
		{
			"ID": "4",
			"ParentID": "10",
			"Category": "Main",
			"ClassIcon": "fa-briefcase",
			"Name": "RPL MS / MG Responsible",
			"Path": "/AutomationFramework/RPLResponsible",
			"AccessCommentHtml": "",
			"NumOrder": "3.3",
			"FlagIsGlobal": "1"
		},
		{
			"ID": "6",
			"ParentID": "10",
			"Category": "Main",
			"ClassIcon": "fa-briefcase",
			"Name": "GOC Responsible",
			"Path": "/AutomationFramework/GOCResponsible",
			"AccessCommentHtml": "",
			"NumOrder": "3.4",
			"FlagIsGlobal": "1"
		},
		{
			"ID": "2",
			"ParentID": "10",
			"Category": "Main",
			"ClassIcon": "fa-sitemap",
			"Name": "Process Tree - MCA",
			"Path": "/AutomationFramework/ProcessTree",
			"AccessCommentHtml": "",
			"NumOrder": "3.5",
			"FlagIsGlobal": "1"
		},
		{
			"ID": "8",
			"ParentID": "10",
			"Category": "Main",
			"ClassIcon": "fa-globe",
			"Name": "Countries & Region",
			"Path": "/AutomationFramework/CountryRegion",
			"AccessCommentHtml": "",
			"NumOrder": "3.6",
			"FlagIsGlobal": "1"
		},
		{
			"ID": "11",
			"ParentID": "",
			"Category": "Main",
			"ClassIcon": "fa-group",
			"Name": "EERS",
			"Path": "",
			"AccessCommentHtml": "",
			"NumOrder": "4",
			"FlagIsGlobal": "1"
		},
		{
			"ID": "7",
			"ParentID": "11",
			"Category": "Main",
			"ClassIcon": "fa-lock",
			"Name": "EERS Roles",
			"Path": "/AutomationFramework/EERS Roles",
			"AccessCommentHtml": "",
			"NumOrder": "4.1",
			"FlagIsGlobal": "1"
		},
		{
			"ID": "9",
			"ParentID": "11",
			"Category": "Main",
			"ClassIcon": "fa-user",
			"Name": "EERS Users",
			"Path": "/AutomationFramework/EERSUser",
			"AccessCommentHtml": "",
			"NumOrder": "4.2",
			"FlagIsGlobal": "1"
		}
	],
	"Reports": [],
	"Roles": [],
	"RolesXPermits": [],
	"RolesXReports": [],
	"Users": [],
	"UsersXRoles": []
}