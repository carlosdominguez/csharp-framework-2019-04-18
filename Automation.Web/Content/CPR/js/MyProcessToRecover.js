/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="../../Shared/plugins/citi-process-tree/process-tree-v1.0.0.js" />
/// <reference path="CPRGlobal.js" />
var objCommunication = null;

$(document).ready(function () {
    //Load Process Activity Table
    loadProcessActivityTable();

    //On Click Add Comment
    $(".btnAddComment").click(function () {
        addCommentToProcessActivity();
    });

    //On Click View Info
    $(".btnViewInfo").click(function () {
        var selectedRow = $.jqxGridApi.getOneSelectedRow("#tblProcessActivities", true);
        if (selectedRow) {
            _openModalProcessActivityInfo({
                filters: {
                    source: selectedRow.SR,
                    idProcessActivity: selectedRow.ID
                },
                onReady: function () { }
            });
        }
    });

    //Load Communication Information
    var idCommunication = $("#HFIDCommunication").val();
    if (idCommunication) {
        _getDBCommunicationByID({
            idCommunication: idCommunication,
            onReady: function (objCommunicationResult) {
                objCommunication = objCommunicationResult;

                $(".lblRecover").html(objCommunication.LocationName + " (" + objCommunication.LocationType + ")");
                $(".lblImpactedDates").html(objCommunication.ImpactedDateRangeText);

                if (objCommunication.IsTest == "True") {
                    $("#btnAddCommentLabel").html("Add Comment & Attest");
                } else {
                    $("#btnAddCommentLabel").html("Add Comment & Status");
                }

                //Load Comments
                //Add History of comments
                var historyRows = JSON.parse(objCommunication.JsonAuditLog);
                var htmlResult = "Not found";
                if (historyRows.length > 0) {
                    historyInfoHtml = "<div style='padding: 10px; font-size: 15px; max-height: 500px; overflow-y:scroll; width: 520px;'>";

                    for (var i = 0; i < historyRows.length; i++) {
                        var tempData = historyRows[i];

                        historyInfoHtml += "<b style='color:#4caf50;'>" + tempData.Type + "</b><br>";

                        historyInfoHtml += "<b>User:</b> " + tempData.CreatedByName + " [" + tempData.CreatedBySOEID + "]<br>";
                        historyInfoHtml += "<b>Date:</b> " + tempData.CreatedDate + "<br>";
                        historyInfoHtml += "<b>Comment:</b> <pre style='margin: 0 0 0px;'>" + _escapeToHTML(tempData.Comment) + "</pre>";

                        historyInfoHtml += "<br>";
                    }

                    historyInfoHtml += "</div>";

                    htmlResult =
                        '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="right" data-content="' + historyInfoHtml + '" data-title="History" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                        '  View (' + historyRows.length + ') '
                        '</div>';
                }
                $(".lblAuditLogs").html(htmlResult);

                //Load Tooltip Plugin
                _GLOBAL_SETTINGS.tooltipsPopovers();
            }
        });
    } else {
        $("#btnAddCommentLabel").html("Add Comment & Status");
    }
});

function addCommentToProcessActivity() {
    var selectedRow = $.jqxGridApi.getOneSelectedRow('#tblProcessActivities', true);
    if (selectedRow) {

        var htmlContentModal = '';

        htmlContentModal +=
            '        <div class="col-md-12">  ' +
            '            <b>Status: </b>  ' +
            '        </div>  ' +
            '        <div class="col-md-12">  ' +
            '            <select class="form-control selStatus" name="selStatus" id="selStatus"> ' +
            '                <option value="" selected="true">-- Select --</option> ';

        if(objCommunication){
            if (objCommunication.IsTest == "True") {
                htmlContentModal +=
                    '        <option value="Approved">Attest & Approve Data</option> ';
            } else {
                htmlContentModal +=
                    '        <option value="Completed">Completed</option> ' +
                    '        <option value="Working">Working on it</option> ';
            }
        } else {
            htmlContentModal +=
                    '        <option value="Approved">Attest & Approve Data</option> ';
        }
         
        htmlContentModal +=
            
            '                <option value="Missing Information">Missing Information</option> ' +
            '                <option value="Access Issue">Access Issue</option> ' +
            '                <option value="Not my process">It\'s not my process</option> ' +
            '                <option value="Other Issues">Other Issues</option> ' +
            '            </select> ' +
            '        </div><br><br><br>';

        htmlContentModal +=
            '        <div class="col-md-12">  ' +
            '            <b>Comment: </b>  ' +
            '        </div>  ' +
            '        <div class="col-md-12">  ' +
            '            <textarea id="txtComment" placeholder="(Required) e.g. I have problems with the links provided." style="width:100%;height: 130px;"></textarea> ' +
            '        </div>';

        _showModal({
            width: '50%',
            modalId: "modalAddCommentActivity",
            addCloseButton: true,
            buttons: [{
                name: "Add Comments",
                class: "btn-success",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    var status = $modal.find("#selStatus").val();
                    var comment = $modal.find("#txtComment").val();
                    
                    //Validate require Status
                    if (status) {

                        //Validate required Comment
                        if (comment) {

                            //Get Activity Info from DB to calculate User relation in Activity
                            _getBDObjProcessActivityInfo({
                                filters: {
                                    source: selectedRow.SR,
                                    idProcessActivity: selectedRow.ID
                                },
                                onReady: function (objActivityInfo) {
                                    _callProcedure({
                                        loadingMsgType: "fullLoading",
                                        loadingMsg: "Adding comments to process activity...",
                                        name: "[dbo].[spCPRAFrwkProcessActivityXAuditLogAdmin]",
                                        params: [
                                            { "Name": "@Action", "Value": "Insert" },
                                            { "Name": "@Type", "Value": 'Comment' },
                                            { "Name": "@IDProcessActivity", "Value": selectedRow.ID },
                                            { "Name": "@Source", "Value": selectedRow.SR },

                                            { "Name": "@IDCommunication", "Value": ($("#HFIDCommunication").val() ? $("#HFIDCommunication").val() : '0') },

                                            { "Name": "@Status", "Value": status },
                                            { "Name": "@Comment", "Value": comment },

                                            { "Name": "@RoleInActivity", "Value": getUserRoleInActivity(objActivityInfo) },
                                            { "Name": "SessionSOEID", "Value": _getSOEID() }
                                        ],
                                        //Show message only for the last 
                                        success: {
                                            fn: function () {
                                                //Refresh Table
                                                loadProcessActivityTable();

                                                //Show Message
                                                _showAlert({
                                                    id: "AddComments",
                                                    type: "success",
                                                    content: "Comment was added successfully."
                                                });

                                                //Close Modal
                                                $modal.find(".close").click();
                                            }
                                        }
                                    });
                                }
                            });

                        } else {
                            _showNotification("error", "Comment cannot be empty.", "CommunicationError", false);
                        }
                    } else {
                        _showNotification("error", "Please select the Status.", "CommunicationError", false);
                    }
                }
            }],
            title: "Add Process Activity Comment:",
            contentHtml: htmlContentModal
        });

       
    }
}

function getUserRoleInActivity(objActivityInfo) {
    var userRoleInActivityArray = [];
    var userSOEID = _getSOEID();
    //FRSS Head
    //GPL
    if (_containsIgnoreCase(userSOEID, objActivityInfo.GPL_SOEID)) {
        userRoleInActivityArray.push("GPL");
    }

    //RPL
    if (_containsIgnoreCase(userSOEID, objActivityInfo.RPL_SOEID)) {
        userRoleInActivityArray.push("RPL");
    }

    //Owner
    var ownerList = JSON.parse(objActivityInfo.OWNER_JSON_USERS);
    for (var i = 0; i < ownerList.length; i++) {
        if (_containsIgnoreCase(userSOEID, ownerList[i].SOEID)) {
            userRoleInActivityArray.push("Owner");
            break;
        }
    }

    //Owner Manager
    for (var i = 0; i < ownerList.length; i++) {
        if (_containsIgnoreCase(userSOEID, ownerList[i].ManagerSOEID)) {
            userRoleInActivityArray.push("Owner Manager");
            break;
        }
    }

    //Maker
    var makerList = JSON.parse(objActivityInfo.MAKER_JSON_USERS);
    for (var i = 0; i < makerList.length; i++) {
        if (_containsIgnoreCase(userSOEID, makerList[i].SOEID)) {
            userRoleInActivityArray.push("Maker");
            break;
        }
    }

    //Maker Manager
    for (var i = 0; i < makerList.length; i++) {
        if (_containsIgnoreCase(userSOEID, makerList[i].ManagerSOEID)) {
            userRoleInActivityArray.push("Maker Manager");
            break;
        }
    }

    //Checker
    if (objActivityInfo.CHECKER_JSON_USERS) {
        var checkerList = JSON.parse(objActivityInfo.CHECKER_JSON_USERS);
        for (var i = 0; i < checkerList.length; i++) {
            if (_containsIgnoreCase(userSOEID, checkerList[i].SOEID)) {
                userRoleInActivityArray.push("Checker");
                break;
            }
        }

        //Checker Manager
        for (var i = 0; i < checkerList.length; i++) {
            if (_containsIgnoreCase(userSOEID, checkerList[i].ManagerSOEID)) {
                userRoleInActivityArray.push("Checker Manager");
                break;
            }
        }
    }
    
    //WAD Owner & WAD Owner Manager CPR
    if (objActivityInfo.SOURCE == "CPR") {
        //WAD Owner CPR
        var WADOwnerList = JSON.parse(objActivityInfo.WAD_OWNER_JSON_USERS);
        for (var i = 0; i < WADOwnerList.length; i++) {
            if (_containsIgnoreCase(userSOEID, WADOwnerList[i].SOEID)) {
                userRoleInActivityArray.push("WAD Owner");
                break;
            }
        }

        //WAD Owner Manager CPR
        for (var i = 0; i < WADOwnerList.length; i++) {
            if (_containsIgnoreCase(userSOEID, WADOwnerList[i].ManagerSOEID)) {
                userRoleInActivityArray.push("WAD Owner Manager");
                break;
            }
        }
    }

    //WAD Owner & WAD Owner Manager eDCFC
    if (objActivityInfo.SOURCE == "eDCFC") {
        //WAD Owner eDCFC
        if (_containsIgnoreCase(userSOEID, objActivityInfo.WAD_OWNER_SOEID)) {
            userRoleInActivityArray.push("WAD Owner");
        }

        //WAD Owner Manager eDCFC
        if (_containsIgnoreCase(userSOEID, objActivityInfo.WAD_OWNER_MANAGER_SOEID)) {
            userRoleInActivityArray.push("WAD Owner Manager");
        }
    }

    //WAD Maker & WAD Maker Manager CPR
    if (objActivityInfo.SOURCE == "CPR") {
        //WAD Maker CPR
        var WADMakerList = JSON.parse(objActivityInfo.WAD_MAKER_JSON_USERS);
        for (var i = 0; i < WADMakerList.length; i++) {
            if (_containsIgnoreCase(userSOEID, WADMakerList[i].SOEID)) {
                userRoleInActivityArray.push("WAD Maker");
                break;
            }
        }

        //WAD Maker Manager CPR
        for (var i = 0; i < WADMakerList.length; i++) {
            if (_containsIgnoreCase(userSOEID, WADMakerList[i].ManagerSOEID)) {
                userRoleInActivityArray.push("WAD Maker Manager");
                break;
            }
        }
    }

    //WAD Maker & WAD Maker Manager eDCFC
    if (objActivityInfo.SOURCE == "eDCFC") {
        //WAD Maker eDCFC
        if (_containsIgnoreCase(userSOEID, objActivityInfo.WAD_MAKER_SOEID)) {
            userRoleInActivityArray.push("WAD Maker");
        }

        //WAD Maker Manager eDCFC
        if (_containsIgnoreCase(userSOEID, objActivityInfo.WAD_MAKER_MANAGER_SOEID)) {
            userRoleInActivityArray.push("WAD Maker Manager");
        }
    }

    //WAD Checker & WAD Checker Manager CPR
    if (objActivityInfo.SOURCE == "CPR") {
        if (objActivityInfo.WAD_CHECKER_JSON_USERS) {
            //WAD Checker CPR
            var WADCheckerList = JSON.parse(objActivityInfo.WAD_CHECKER_JSON_USERS);
            for (var i = 0; i < WADCheckerList.length; i++) {
                if (_containsIgnoreCase(userSOEID, WADCheckerList[i].SOEID)) {
                    userRoleInActivityArray.push("WAD Checker");
                    break;
                }
            }

            //WAD Checker Manager CPR
            for (var i = 0; i < WADCheckerList.length; i++) {
                if (_containsIgnoreCase(userSOEID, WADCheckerList[i].ManagerSOEID)) {
                    userRoleInActivityArray.push("WAD Checker Manager");
                    break;
                }
            }
        }
    }

    //WAD Checker & WAD Checker Manager eDCFC
    if (objActivityInfo.SOURCE == "eDCFC") {
        //WAD Checker eDCFC
        if (_containsIgnoreCase(userSOEID, objActivityInfo.WAD_CHECKER_SOEID)) {
            userRoleInActivityArray.push("WAD Checker");
        }

        //WAD Checker Manager eDCFC
        if (_containsIgnoreCase(userSOEID, objActivityInfo.WAD_CHECKER_MANAGER_SOEID)) {
            userRoleInActivityArray.push("WAD Checker Manager");
        }
    }

    return userRoleInActivityArray.join(", ");
}

function loadProcessActivityTable() {
    var filters = {};

    if ($("#HFIDCommunication").val()) {
        filters.idCommunication = $("#HFIDCommunication").val();
        filters.communicationEmployeeSOEID = _getSOEID();
    }else{
        filters.employeeSOEID = _getSOEID();
    }

    _loadProcessActivityTable({
        idTable: "#tblProcessActivities",
        filters: filters,
        onReady: function(eDCFCAcitivityList){
            //Add count of activities to recover
            $("#lblProcessActivityCount").html("Activities to recover (" + eDCFCAcitivityList.length + ")");
        }
    });
}