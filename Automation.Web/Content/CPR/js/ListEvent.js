/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="../../Shared/plugins/citi-process-tree/process-tree-v1.0.0.js" />
/// <reference path="CPRGlobal.js" />

$(document).ready(function () {
    //Load Table of Communications
    loadCommunicationsTable();

    //On Click View Info
    $(".btnViewInfo").click(function () {
        viewCommunicationInfo();
    });

    //On Click Extend Event
    $(".btnExtendEvent").click(function () {
        extendEvent();
    });

    //On Click Additional Communication
    $(".btnadditionalCommunication").click(function () {
        additionalCommunication();
    });

    //On Click Finish Communication
    $(".btnFinishEvent").click(function () {
        finishCommunication();
    });
});


//Functionality
function additionalCommunication() {
    var selectedRow = $.jqxGridApi.getOneSelectedRow('#tblCommunications', true);
    if (selectedRow) {

        if (selectedRow["Status"] == "In Progress") {

            var htmlContentModal = '';
            htmlContentModal += "<b>Comment: </b> <br/><textarea id='txtComment' placeholder='(Required) e.g. Please follow the Risk & Control instructions.' style='width:100%;height: 130px;'></textarea><br/>";

            _showModal({
                width: '50%',
                modalId: "modalDelApp",
                addCloseButton: true,
                buttons: [{
                    name: "Send Notification",
                    class: "btn-success",
                    closeModalOnClick: false,
                    onClick: function ($modal) {
                        var comment = $modal.find("#txtComment").val();

                        //Validate required Comment
                        if (comment) {

                            _callProcedure({
                                loadingMsgType: "fullLoading",
                                loadingMsg: "Sending additional communication...",
                                name: "[dbo].[spCPRAFrwkCommunicationAdmin]",
                                params: [
                                    { "Name": "@Action", "Value": "Additional Communication" },
                                    { "Name": "@IDCommunication", "Value": selectedRow.ID },

                                    { "Name": "@ImpactedDateStart", "Value": moment(selectedRow.ImpactedDateStart).format("YYYY-MM-DD") },
                                    { "Name": "@ImpactedDateEnd", "Value": moment(selectedRow.ImpactedDateEnd).format("YYYY-MM-DD") },
                                    { "Name": "@ImpactedDateRangeText", "Value": selectedRow.ImpactedDateRangeText },

                                    { "Name": "@Comment", "Value": comment },

                                    { "Name": "@ServerURL", "Value": _getViewVar("ServerURL") },
                                    { "Name": "SessionSOEID", "Value": _getSOEID() }
                                ],
                                //Show message only for the last 
                                success: {
                                    fn: function () {
                                        //Refresh Table
                                        loadCommunicationsTable();

                                        //Show Communication
                                        _showAlert({
                                            id: "AdditionalCommunication",
                                            type: "success",
                                            content: "Notification was sent successfully."
                                        });

                                        //Close Modal
                                        $modal.find(".close").click();
                                    }
                                }
                            });

                        } else {
                            _showNotification("error", "Comment cannot be empty.", "CommunicationError", false);
                        }
                    }
                }],
                title: "Please add Additional Communication Comment:",
                contentHtml: htmlContentModal
            });

        } else {
            _showNotification("error", "Please select In Progress communication.", "AlertOnlyInProgress");
        }
    }
}

function finishCommunication() {
    var selectedRow = $.jqxGridApi.getOneSelectedRow('#tblCommunications', true);
    if (selectedRow) {

        if (selectedRow["Status"] == "In Progress") {

            var htmlContentModal = '';
            htmlContentModal += "<b>Comment: </b> <br/><textarea id='txtComment' placeholder='(Required) e.g. The center was restored.' style='width:100%;height: 130px;'></textarea><br/>";

            _showModal({
                width: '50%',
                modalId: "modalDelApp",
                addCloseButton: true,
                buttons: [{
                    name: "Send Notification",
                    class: "btn-success",
                    closeModalOnClick: false,
                    onClick: function ($modal) {
                        var comment = $modal.find("#txtComment").val();

                        //Validate required Comment
                        if (comment) {

                            _callProcedure({
                                loadingMsgType: "fullLoading",
                                loadingMsg: "Sending finish event communication...",
                                name: "[dbo].[spCPRAFrwkCommunicationAdmin]",
                                params: [
                                    { "Name": "@Action", "Value": "Finish Event" },
                                    { "Name": "@IDCommunication", "Value": selectedRow.ID },

                                    { "Name": "@ImpactedDateStart", "Value": moment(selectedRow.ImpactedDateStart).format("YYYY-MM-DD") },
                                    { "Name": "@ImpactedDateEnd", "Value": moment(selectedRow.ImpactedDateEnd).format("YYYY-MM-DD") },
                                    { "Name": "@ImpactedDateRangeText", "Value": selectedRow.ImpactedDateRangeText },

                                    { "Name": "@Status", "Value": 'Finished' },
                                    { "Name": "@Comment", "Value": comment },

                                    { "Name": "@ServerURL", "Value": _getViewVar("ServerURL") },
                                    { "Name": "SessionSOEID", "Value": _getSOEID() }
                                ],
                                //Show message only for the last 
                                success: {
                                    fn: function () {
                                        //Refresh Table
                                        loadCommunicationsTable();

                                        //Show Communication
                                        _showAlert({
                                            id: "FinishEvent",
                                            type: "success",
                                            content: "Notification was sent successfully."
                                        });

                                        //Close Modal
                                        $modal.find(".close").click();
                                    }
                                }
                            });

                        } else {
                            _showNotification("error", "Comment cannot be empty.", "CommunicationError", false);
                        }
                    }
                }],
                title: "Please add Finish Communication Comment:",
                contentHtml: htmlContentModal
            });

        } else {
            _showNotification("error", "Only In Progress Events can be finished.", "AlertOnlyInProgress");
        }
    }
}

function extendEvent() {
    var selectedRow = $.jqxGridApi.getOneSelectedRow("#tblCommunications", true);
    if (selectedRow) {
        _openModalCommunicationInfo({
            title: "Extend Impacted Dates of Event",
            fields: {
                impactedDates: {
                    editable: true
                },
                comment: {
                    editable: true
                }
            },
            buttons: [{
                name: "Send Extend Update",
                class: "btn-success",
                onClick: function ($modalExtendEvent) {
                    var impactedDateStart = $modalExtendEvent.find("#containerImpactedDates").attr("impactedStartDate");
                    var impactedDateEnd = $modalExtendEvent.find("#containerImpactedDates").attr("impactedEndDate");
                    var impactedDateRangeText = $modalExtendEvent.find("#containerImpactedDates").attr("impactedDateRange");
                    var comment = $modalExtendEvent.find("#txtComment").val();

                    if (comment) {
                        if (impactedDateEnd != moment(selectedRow.ImpactedDateEnd).format("YYYY-MM-DD")) {
                            _callProcedure({
                                loadingMsgType: "fullLoading",
                                loadingMsg: "Sending Extend Update...",
                                name: "[dbo].[spCPRAFrwkCommunicationAdmin]",
                                params: [
                                    { "Name": "@Action", "Value": "Extend Event" },
                                    { "Name": "@IDCommunication", "Value": selectedRow.ID },

                                    { "Name": "@ImpactedDateStart", "Value": impactedDateStart },
                                    { "Name": "@ImpactedDateEnd", "Value": impactedDateEnd },
                                    { "Name": "@ImpactedDateRangeText", "Value": impactedDateRangeText },

                                    { "Name": "@Status", "Value": 'In Progress' },
                                    { "Name": "@Comment", "Value": comment },

                                    { "Name": "@ServerURL", "Value": _getViewVar("ServerURL") },
                                    { "Name": "SessionSOEID", "Value": _getSOEID() }
                                ],
                                //Show message only for the last 
                                success: {
                                    fn: function () {

                                        loadCommunicationsTable();

                                        _showAlert({
                                            id: "EventExtend",
                                            type: "success",
                                            content: "Notification was sent successfully."
                                        });

                                        //setTimeout(function () {
                                        //    // Redirect to Finish WAD Event
                                        //    window.location.href = _fixFrameworkURL('/CPR/ListEvent?pviewType=FinishEvent');
                                        //}, 1500);
                                    }
                                }
                            });
                        }else{
                            _showNotification("error", "Please change the impatected dates", "CommunicationError", false);
                            return false;
                        }
                    } else {
                        _showNotification("error", "Please add comments", "CommunicationError", false);
                        return false;
                    }
                }
            }],
            objCommunication: {
                isTestEvent: (selectedRow.IsTest == "True" ? true : false),
                locationTypetoRecover: selectedRow.LocationType,
                impactedDateStart: moment(selectedRow.ImpactedDateStart).format("YYYY-MM-DD"),
                impactedDateEnd: moment(selectedRow.ImpactedDateEnd).format("YYYY-MM-DD"),
                impactedDateRangeText: selectedRow.ImpactedDateRangeText,
                idLocation: selectedRow.LocationActivitiesCount,
                locationName: selectedRow.LocationName,
                locationActivitiesCount: selectedRow.LocationActivitiesCount,
                communicationDate: moment(selectedRow.CreatedDate).format("YYYY-MM-DD"),
                typeOfEvent: selectedRow.TypeOfEvent,
                localRecovery: selectedRow.LocalRecovery,
                recoveryAction: selectedRow.RecoveryAction,
                comment: selectedRow.Comment,
                sendToFRSSHead: selectedRow.SendToFRSSHead,
                sendToGPL: selectedRow.SendToGPL,
                sendToRPL: selectedRow.SendToRPL,
                sendToOwner: selectedRow.SendToOwner,
                sendToOwnerManager: selectedRow.SendToOwnerManager,
                sendToMaker: selectedRow.SendToMaker,
                sendToMakerManager: selectedRow.SendToMakerManager,
                sendToChecker: selectedRow.SendToChecker,
                sendToCheckerManager: selectedRow.SendToCheckerManager,
                sendToWADOwner: selectedRow.SendToWADOwner,
                sendToWADOwnerManager: selectedRow.SendToWADOwnerManager,
                sendToWADMaker: selectedRow.SendToWADMaker,
                sendToWADMakerManager: selectedRow.SendToWADMakerManager,
                sendToWADChecker: selectedRow.SendToWADChecker,
                sendToWADCheckerManager: selectedRow.SendToWADCheckerManager,
                testEmail: selectedRow.TestEmail
            }
        });
    }
}

function viewCommunicationInfo() {
    var selectedRow = $.jqxGridApi.getOneSelectedRow("#tblCommunications", true);
    if (selectedRow) {
        _openModalCommunicationInfo({
            objCommunication: {
                isTestEvent: (selectedRow.IsTest == "True" ? true : false),
                locationTypetoRecover: selectedRow.LocationType,
                impactedDateRangeText: selectedRow.ImpactedDateRangeText,
                idLocation: selectedRow.LocationActivitiesCount,
                locationName: selectedRow.LocationName,
                locationActivitiesCount: selectedRow.LocationActivitiesCount,
                communicationDate: moment(selectedRow.CreatedDate).format("YYYY-MM-DD"),
                typeOfEvent: selectedRow.TypeOfEvent,
                localRecovery: selectedRow.LocalRecovery,
                recoveryAction: selectedRow.RecoveryAction,
                comment: selectedRow.Comment,
                sendToFRSSHead: selectedRow.SendToFRSSHead,
                sendToGPL: selectedRow.SendToGPL,
                sendToRPL: selectedRow.SendToRPL,
                sendToOwner: selectedRow.SendToOwner,
                sendToOwnerManager: selectedRow.SendToOwnerManager,
                sendToMaker: selectedRow.SendToMaker,
                sendToMakerManager: selectedRow.SendToMakerManager,
                sendToChecker: selectedRow.SendToChecker,
                sendToCheckerManager: selectedRow.SendToCheckerManager,
                sendToWADOwner: selectedRow.SendToWADOwner,
                sendToWADOwnerManager: selectedRow.SendToWADOwnerManager,
                sendToWADMaker: selectedRow.SendToWADMaker,
                sendToWADMakerManager: selectedRow.SendToWADMakerManager,
                sendToWADChecker: selectedRow.SendToWADChecker,
                sendToWADCheckerManager: selectedRow.SendToWADCheckerManager,
                testEmail: selectedRow.TestEmail
            }
        });
    }
}

function loadCommunicationsTable () {
    getDBCommunications({
        onReady: function (communicationList) {
            var idTable = "#tblCommunications";
            console.log(communicationList);
            //Create table
            $.jqxGridApi.create({
                showTo: idTable,
                options: {
                    //for comments or descriptions
                    height: (($(window).height() < 700) ? "380" : "500"),
                    autoheight: false,
                    autorowheight: false,
                    showfilterrow: true,
                    sortable: true,
                    editable: true,
                    selectionmode: "singlerow"
                },
                source: {
                    // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                    dataBinding: "Large Data Set Local",
                    rows: communicationList
                },
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                columns: [
                    { name: 'ID', type: 'number', hidden: true },
                    { name: 'JsonAuditLog', type: 'string', hidden: true },
                    { name: 'LocationType', type: 'string', hidden: true },
                    { name: 'LocalRecovery', type: 'string', hidden: true },
                    { name: 'TestEmail', type: 'string', hidden: true },
                    { name: 'CreatedBy', type: 'string', hidden: true },
                    { name: 'ImpactedDateStart', type: 'date', hidden: true },
                    { name: 'ImpactedDateEnd', type: 'date', hidden: true },
                    {
                        name: 'Status', text: 'Status', width: '130px', type: 'string', pinned: true, filtertype: 'checkedlist', editable: false, filterable: true, cellsalign: 'center', align: 'center',
                        cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                            var htmlResult = '';

                            //Add Status
                            switch (rowData.Status) {
                                case "In Progress":
                                    htmlResult += '<span class="badge badge-md badge-warning" style="margin-top: 5px; margin-left: 5px;"> In Progress </span>';
                                    break;

                                case "Finished":
                                    htmlResult += '<span class="badge badge-md badge-success" style="margin-top: 5px; margin-left: 5px;"> Finished </span>';
                                    break;
                            }

                            //Add History of comments
                            var historyRows = JSON.parse(rowData.JsonAuditLog);
                            if (historyRows.length > 0) {
                                var historyInfoHtml = "<div style='padding: 10px; font-size: 15px; max-height: 500px; overflow-y:scroll; width: 520px;'>";

                                for (var i = 0; i < historyRows.length; i++) {
                                    var tempData = historyRows[i];

                                    historyInfoHtml += "<b style='color:#4caf50;'>" + tempData.Type + "</b><br>";

                                    historyInfoHtml += "<b>User:</b> " + tempData.CreatedByName + " [" + tempData.CreatedBySOEID + "]<br>";
                                    historyInfoHtml += "<b>Date:</b> " + tempData.CreatedDate + "<br>";
                                    historyInfoHtml += "<b>Comment:</b> <pre style='margin: 0 0 0px;'>" + _escapeToHTML(tempData.Comment) + "</pre>";
                                    
                                    historyInfoHtml += "<br>";
                                }

                                historyInfoHtml += "</div>";

                                htmlResult +=
                                    '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="right" data-content="' + historyInfoHtml + '" data-title="History" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                    '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png"/> ' +
                                    '</div>';
                            }
                            

                            return htmlResult;
                        }
                    },
                    {
                        name: 'IsTest', text: 'Is Attestation?', width: '100px', type: 'string', filtertype: 'checkedlist', editable: false, filterable: true, cellsalign: 'center', align: 'center',
                        cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                            var htmlResult = '';

                            //Add Status
                            switch (rowData.IsTest) {
                                case "False":
                                    var $cell = $(cellHtml);
                                    $cell.html("No");
                                    htmlResult += $cell[0].outerHTML;
                                    break;

                                case "True":
                                    htmlResult += '<span class="badge badge-md badge-success" style="margin-top: 5px; margin-left: 32px;"> Yes </span>';
                                    break;
                            }

                            return htmlResult;
                        }
                    },
                    {
                        name: 'LocationName', text: 'Recover', width: '200px', type: 'string', filtertype: 'checkedlist', editable: false, filterable: true,
                        cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                            var $cell = $(cellHtml);
                            $cell.html(rowData.LocationName + " (" + rowData.LocationType + ")");

                            return $cell[0].outerHTML;
                        }
                    },
                    //{ name: 'Recover', text: 'Recover', width: '200px', type: 'string', filtertype: 'input' },
                    { name: 'CreatedDate', text: 'Sent Date', width: '110px', type: 'date', filtertype: 'input', cellsformat: 'MMM dd, yyyy' },
                    {
                        name: 'CreatedByName', text: 'Send By', width: '270px', type: 'string', filtertype: 'checkedlist', filterable: true,
                        cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                            var $cell = $(cellHtml);
                            $cell.html(rowData.CreatedByName + " [" + rowData.CreatedBy + "]");

                            return $cell[0].outerHTML;
                        }
                    },
                    { name: 'ImpactedDateRangeText', text: 'Impacted Dates', width: '310px', type: 'string', filtertype: 'input' },
                    { name: 'LocationActivitiesCount', text: 'eDCFC Activities', width: '120px', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center' },
                    { name: 'TypeOfEvent', text: 'Type Of Event', width: '100px', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center' },
                    { name: 'RecoveryAction', text: 'Recovery Action', width: '150px', type: 'string', filtertype: 'input'},
                    { name: 'Comment', text: 'Event Comment', width: '300px', type: 'string', filtertype: 'input' },
                    { name: 'FinishComment', text: 'Finish Comment', width: '300px', type: 'string', filtertype: 'input' },
                    { name: 'SendToFRSSHead', text: 'To FRSS Head', width: '110px', type: 'bool', columntype: 'checkbox', filtertype: 'checkedlist' },
                    { name: 'SendToGPL', text: 'To GPL', width: '60px', type: 'bool', columntype: 'checkbox', filtertype: 'checkedlist' },
                    { name: 'SendToRPL', text: 'To RPL', width: '60px', type: 'bool', columntype: 'checkbox', filtertype: 'checkedlist' },
                    { name: 'SendToOwner', text: 'To Owner', width: '80px', type: 'bool', columntype: 'checkbox', filtertype: 'checkedlist' },
                    { name: 'SendToOwnerManager', text: 'To Owner Manager', width: '140px', type: 'bool', columntype: 'checkbox', filtertype: 'checkedlist' },
                    { name: 'SendToMaker', text: 'To Maker', width: '80px', type: 'bool', columntype: 'checkbox', filtertype: 'checkedlist' },
                    { name: 'SendToMakerManager', text: 'To Maker Manager', width: '140px', type: 'bool', columntype: 'checkbox', filtertype: 'checkedlist' },
                    { name: 'SendToChecker', text: 'To Checker', width: '80px', type: 'bool', columntype: 'checkbox', filtertype: 'checkedlist' },
                    { name: 'SendToCheckerManager', text: 'To Checker Manager', width: '140px', type: 'bool', columntype: 'checkbox', filtertype: 'checkedlist' },
                    { name: 'SendToWADOwner', text: 'To WAD Owner', width: '110px', type: 'bool', columntype: 'checkbox', filtertype: 'checkedlist' },
                    { name: 'SendToWADOwnerManager', text: 'To WAD Owner Manager', width: '170px', type: 'bool', columntype: 'checkbox', filtertype: 'checkedlist' },
                    { name: 'SendToWADMaker', text: 'To WAD Maker', width: '110px', type: 'bool', columntype: 'checkbox', filtertype: 'checkedlist' },
                    { name: 'SendToWADMakerManager', text: 'To WAD Maker Manager', width: '170px', type: 'bool', columntype: 'checkbox', filtertype: 'checkedlist' },
                    { name: 'SendToWADChecker', text: 'To WAD Checker', width: '120px', type: 'bool', columntype: 'checkbox', filtertype: 'checkedlist' },
                    { name: 'SendToWADCheckerManager', text: 'To WAD Checker Manager', width: '180px', type: 'bool', columntype: 'checkbox', filtertype: 'checkedlist' }
                ],
                ready: function () {
                    $(idTable).on("rowclick", function (event) {
                        //Show tooltips
                        _GLOBAL_SETTINGS.tooltipsPopovers();
                    });
                }
            });
        }
    });
}

//BD Data
function getDBCommunications (customOptions) {
    //Default Options.
    var options = {
        onReady: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    if (options.onReady) {

        //Load competency information
        _callProcedure({
            loadingMsgType: "fullLoading",
            loadingMsg: "Getting communications information...",
            name: "[dbo].[spCPRAFrwkCommunicationAdmin]",
            params: [
                { "Name": "@Action", "Value": 'List' }
            ],
            success: {
                fn: function (responseList) {
                    options.onReady(responseList);
                }
            }
        });
    }
}