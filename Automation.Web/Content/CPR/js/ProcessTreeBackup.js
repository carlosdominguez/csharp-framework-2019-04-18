/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="../../Shared/plugins/citi-process-tree/process-tree-v1.0.0.js" />

var criticalCategoryList;
var entityToLeadTypeList;

$(document).ready(function () {
    //Add Edit event
    $('.btnEditRow').click(function () {
        openModalProcessAdmin("Edit");
    });

    //Add click event
    $('.btnAddRow').click(function () {
        openModalProcessAdmin("Add");
    });

    //Refresh click event
    $(".btnRefresh").click(function () {
        loadTableProcess();
    });

    //Delete click event
    $('.btnDeleteRow').click(function () {
        var objRowSelected = $.jqxGridApi.getOneSelectedRow('#tblProcess', true);
        if (objRowSelected) {
            var htmlContentModal = "";
            htmlContentModal += "<b>Level: </b>" + objRowSelected['Category'] + "<br/>";
            htmlContentModal += "<b>Name: </b>" + objRowSelected['Name'] + "<br/>";

            _showModal({
                width: '40%',
                modalId: "modalDelRow",
                addCloseButton: true,
                buttons: [{
                    name: "Delete",
                    class: "btn-danger",
                    onClick: function () {
                        //Delete row in in DB
                        _callProcedure({
                            response: true,
                            name: "[Automation].[dbo].[spAFrwkProcessAdmin]",
                            params: [
                                { "Name": "@Action", "Value": 'Delete' },
                                { "Name": "@IDProcess", "Value": objRowSelected.ID },
                                { "Name": "@SessionSOEID", "Value": _getSOEID() }
                            ],
                            success: {
                                fn: function (responseID) {
                                    //show success alert
                                    _showNotification("success", "The row (" + objRowSelected.Name + ") was deleted successfully.", "AlertDeleted");
                                    
                                    //refresh table
                                    loadTableProcess();

                                    //Refresh filters process
                                    _refreshFilterProcessByChange($("#filter-process"), objRowSelected.Level);
                                }
                            }
                        });
                    }
                }],
                title: "Are you sure you want to delete this row?",
                contentHtml: htmlContentModal
            });
        }
    });

    //On click export excel
    $(".btnExportExcel").click(function () {
        _downloadExcel({
            spName: "[Automation].[dbo].[spAFrwkProcessListExcel] ",
            spParams: [],
            filename: "ProcessTree-" + moment().format('MMM-DD-YYYY-HH-MM'),
            success: {
                msg: "Please wait, generating report..."
            }
        });
    });

    //Load Filter Process
    var $filterContainer = $("#filter-process");
    _loadFilterProcess("selProcessL2", 2, null, $filterContainer);
    $("#selProcessL2").change(function () {
        
        //Clear Process L4
        $("#selProcessL4").val("0");
        $("#selProcessL4").contents().remove();

        //Clear Process L5
        $("#selProcessL5").val("0");
        $("#selProcessL5").contents().remove();

        //Load Process L3
        _loadFilterProcess("selProcessL3", 3, true, $filterContainer);

        //Load Table
        loadTableProcess();
    });

    $("#selProcessL3").change(function () {
        
        //Clear Process L5
        $("#selProcessL5").val("0");
        $("#selProcessL5").contents().remove();

        //Load Process L4
        _loadFilterProcess("selProcessL4", 4, true, $filterContainer);

        //Load Table
        loadTableProcess();
    });

    $("#selProcessL4").change(function () {

        //Load Process L5
        _loadFilterProcess("selProcessL5", 5, true, $filterContainer);

        //Load Table
        loadTableProcess();
    });

    $("#selProcessL5").change(function () {
        //Load Table
        loadTableProcess();
    });

    //Hide menu
    _hideMenu();

    //Get Critical Category Data
    _getProcessCriticalCategoryData({
        onReady: function (responseCriticalCategoryList) {
            criticalCategoryList = responseCriticalCategoryList;

            //Load Table
            setTimeout(function () {
                loadTableProcess();
            }, 400);
        }
    });

    //Get Entity To Lead Type Data
    _getProcessEntityToLeadTypeData({
        onReady: function (responseEntityToLeadTypeList) {
            entityToLeadTypeList = responseEntityToLeadTypeList;
        }
    });
});

function openModalProcessAdmin(typeAction) {
    var selectedRow;
    var resultList;
    var parentLevel;
    var parentName;
    var idTableRPL = 'tblRPLs_' + _createCustomID();
    var idTableLead = 'tblLeads_' + _createCustomID();
    var idTableResources = 'tblResources_' + _createCustomID();

    // initialize jqxGridParent
    var idTableProcessParent = "jqxGridParent_" + _createCustomID();

    // Get last version of Process List
    _getProcessData({
        showAll: true,
        onReady: function (processListTable) {
            loadModal(processListTable);
        }
    });

    function loadModal(processListTable) {

        //Calculate Max ID
        var maxID = 0;
        for (var i = 0; i < processListTable.length; i++) {
            if (processListTable[i].Key && parseInt(processListTable[i].Key) > maxID) {
                maxID = parseInt(processListTable[i].Key);
            }
        }

        if (typeAction == "Edit") {
            selectedRow = $.jqxGridApi.getOneSelectedRow("#tblProcess", true);
            if (selectedRow) {
                if (selectedRow.IDParent > 0) {
                    parentLevel = 0;
                    parentName = ' Not found or Deleted ';
                    resultList = $.grep(processListTable, function (v) {
                        return v.ID == selectedRow.IDParent;
                    });

                    if (resultList.length > 0) {
                        parentLevel = resultList[0].Level;
                        parentName = resultList[0].Name;
                    }

                    selectedRow.SelParent = (resultList[0].Key ? (resultList[0].Key + ": ") : "") + 'L' + parentLevel + ' - ' + parentName;
                } else {
                    selectedRow.SelParent = '-- No Parent --';
                }
            }
        }

        if (typeAction == "Add") {
            selectedRow = {
                ID: 0,
                Level: 1,
                Key: '',
                Name: '',
                IDParent: 0,
                CantChilds: 0,
                SelParent: '-- No Parent --'
            }

            var defaultParentRow = $.jqxGridApi.getOneSelectedRow("#tblProcess");
            if (defaultParentRow) {
                parentLevel = defaultParentRow.Level;
                parentName = defaultParentRow.Name;

                selectedRow.IDParent = defaultParentRow.ID;
                selectedRow.SelParent = (defaultParentRow.Key ? (defaultParentRow.Key + ": ") : "") + 'L' + parentLevel + ' - ' + parentName;;
                selectedRow.Level = parentLevel + 1;
            }
        }

        if (selectedRow) {
            _showModal({
                width: '95%',
                modalId: "SelectProcess",
                addCloseButton: true,
                buttons: [{
                    name: "Save",
                    class: "btn-success",
                    closeModalOnClick: false,
                    onClick: function ($modal) {
                        modalOnSave($modal);
                    }
                }],
                title: "Process Information",
                onReady: function ($modal) {
                    modalOnReady($modal);
                },
                contentHtml: getModalHtml(selectedRow)
            });
        }

        function getModalHtml(selectedRow) {
            var htmlFormEdit =
                '<ul class="nav nav-tabs transparent left-aligned"> ' +
                '    <li class="active"> ' +
                '        <a href="#tab-process-info" data-toggle="tab"> ' +
                '            <i class="fa fa-th"></i> <span class="lblProcessInfo">Process Info</span> ' +
                '        </a> ' +
                '    </li> ' +
                '    <li> ' +
                '        <a href="#tab-contacts" data-toggle="tab"> ' +
                '            <i class="fa fa-users"></i> <span class="lblContacts">Contacts</span> ' +
                '        </a> ' +
                '    </li> ' +
                //'    <li> ' +
                //'        <a href="#tab-rpls" data-toggle="tab"> ' +
                //'            <i class="fa fa-user"></i> <span class="lblRPLs">RPL</span> ' +
                //'        </a> ' +
                //'    </li> ' +
                //'    <li> ' +
                //'        <a href="#tab-leads" data-toggle="tab"> ' +
                //'            <i class="fa fa-user"></i> <span class="lblLeads">Lead</span> ' +
                //'        </a> ' +
                //'    </li> ' +
                '    <li> ' +
                '        <a href="#tab-resources" data-toggle="tab"> ' +
                '            <i class="fa fa-link"></i> <span class="lblResources">Blueworks / Resources</span> ' +
                '        </a> ' +
                '    </li> ' +
                '    <li> ' +
                '        <a href="#tab-mca-controls" data-toggle="tab"> ' +
                '            <i class="fa fa-th-large"></i> <span class="lblMCAControls">MCA Controls</span> ' +
                '        </a> ' +
                '    </li> ' +
                '    <li> ' +
                '        <a href="#tab-schedule" data-toggle="tab"> ' +
                '            <i class="fa fa-calendar"></i> <span class="lblSchedule">Schedule</span> ' +
                '        </a> ' +
                '    </li> ' +
                //'    <li> ' +
                //'        <a href="#tab-edcfc-checklist" data-toggle="tab"> ' +
                //'            <i class="fa fa-table"></i> <span class="lbleDCFC">eDCFC</span> ' +
                //'        </a> ' +
                //'    </li> ' +
                //'    <li> ' +
                //'        <a href="#tab-css-university" data-toggle="tab"> ' +
                //'            <i class="fa fa-list-ul"></i> <span class="lblCSSUniversity">CSS University</span> ' +
                //'        </a> ' +
                //'    </li> ' +
                //'    <li> ' +
                //'        <a href="#tab-imt-issues" data-toggle="tab"> ' +
                //'            <i class="fa fa-list-ul"></i> <span class="lblIMT">IMT Issues</span> ' +
                //'        </a> ' +
                //'    </li> ' +
                '    <li> ' +
                '        <a href="#tab-linked-systems" data-toggle="tab"> ' +
                '            <i class="fa fa-cubes"></i> <span class="lblLinkedSystem">Linked systems</span> ' +
                '        </a> ' +
                '    </li> ' +
                '    <li> ' +
                '        <a href="#tab-cob" data-toggle="tab"> ' +
                '            <i class="fa fa-ambulance"></i> <span class="lblCOB">COB</span> ' +
                '        </a> ' +
                '    </li> ' +
                '</ul> ' +
                '<div class="tab-content"> ' +
                '    <div class="tab-pane fade in active" id="tab-process-info"> ' +
                '        <div class="tab-process-info-container"> ' +

                '           <input type="hidden" value="' + selectedRow.ID + '" class="fieldID" > ' +

                '           <div class="form-group" > ' +
                '               <label for="fieldName" class="col-sm-2 control-label"> ' +
                '                   Unique ID ' +
                '               </label> ' +
                '               <div class="col-sm-8"> ' +
                '                   <input type="text" placeholder="Next ID: ' + (maxID + 1) + '" value="' + selectedRow.Key + '" class="fieldKey form-control" > ' +
                '               </div> ' +
                '           </div> ' +

                '           <div class="form-group form-process"> ' +
                '               <label for="fieldName" class="col-sm-2 control-label"> ' +
                '                   Parent ' +
                '               </label> ' +
                '               <div class="col-sm-8"> ' +
                '                  <div class="jqxDropDownParent" style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;font-size: 14px;"> ' +
                '                      <div style="border-color: transparent;" id="' + idTableProcessParent + '"></div> ' +
                '                  </div> ' +
                '                  <button type="button" class="btn btn-xs btnClearParent btn-danger" style="position: absolute;right: -20px;top: 1px;"> ' +
                '                      <span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> ' +
                '                  </button> ' +
                '               </div> ' +
                '           </div> ' +

                '           <div class="form-group"> ' +
                '               <label for="fieldName" class="col-sm-2 control-label"> ' +
                '                   Process Level ' +
                '               </label> ' +
                '               <div class="col-sm-8"> ' +
                '                  <span class="fieldLevel" style="padding-left: 11px;line-height: 30px;" value="' + selectedRow.Level + '">L' + selectedRow.Level + '</span>' +
                '               </div> ' +
                '           </div> ' +

                '           <div class="form-group"> ' +
                '               <label for="fieldName" class="col-sm-2 control-label"> ' +
                '                   Process Name ' +
                '               </label> ' +
                '               <div class="col-sm-8"> ' +
                '                   <textarea rows="2" class="fieldName form-control" placeholder="e.g. Checker Review">' + selectedRow.Name + '</textarea>' +
                '               </div> ' +
                '           </div> ' +

                '           <div class="form-group"> ' +
                '               <label for="fieldCriticalCategory" class="col-sm-2 control-label"> ' +
                '                   Critical Category ' +
                '               </label> ' +
                '               <div class="col-sm-8"> ' +
                '                  <select name="fieldCriticalCategory" class="form-control fieldCriticalCategory"></select> ' +
                '               </div> ' +
                '           </div> ' +

                '        </div> ' +
                '    </div> ' +
                '    <div class="tab-pane fade in" id="tab-contacts"> ' +
                '        <div class="tab-contacts-container"> ' +

                '           <div class="form-group"> ' +
                '               <label for="fieldGPL" class="col-sm-2 control-label"> ' +
                '                   Select GPL ' +
                '               </label> ' +
                '               <div class="col-sm-8"> ' +
                '                  <div class="selGPL"></div> ' +
                '                  <label class="msgGPLValidation" style="display:none; color: #004786; font-size: 12px; "> ' +
                '                       Only Product (L2 Process) can edit GPL ' +
                '                  </label> ' +
                '               </div> ' +
                '           </div> ' +


                '           <div class="form-group"> ' +
                '               <label for="fieldRPL" class="col-sm-2 control-label"> ' +
                '                   RPLs ' +
                '               </label> ' +
                '               <div class="col-sm-8"> ' +

                '                   <div class="tab-rpls-container"> ' +
                '                       <div style="width: 30%;float: right;"> ' +
                '                           <button type="button" class="btnNewRPL btn btn-success top15 left15 bottom15 pull-right"><i class="fa fa-plus-square-o"></i> </button> ' +
                '                           <button type="button" class="btnDeleteRPL btn btn-danger  top15 left15 bottom15 pull-right"><i class="glyphicon glyphicon-remove-sign"></i> </button> ' +
                '                       </div> ' +
                '                       <div style="width: 40%;float: right;padding-top: 15px;"> ' +
                '                           <div class="selRPL"></div> ' +
                '                       </div> ' +
                '                       <div style="width: 20%;float: right;padding-top: 15px;"> ' +
                '                           <select name="selRPLEntityToLead" class="form-control selRPLEntityToLead"></select> ' +
                '                           <a href="#" class="btnManageEntityToLead" selEntityToLeadType=".selRPLEntityToLeadType"><i class="fa fa-edit"></i> Manage Options</a> ' +
                '                       </div> ' +
                '                       <div style="width: 10%;float: right;padding-top: 15px;"> ' +
                '                           <select name="selRPLEntityToLeadType" onChangeRefresh=".selRPLEntityToLead" class="form-control selRPLEntityToLeadType"></select> ' +
                '                       </div> ' +
                '                   </div> ' +
                '                   <div id="' + idTableRPL + '" class="clearfix"></div> ' +

                '               </div> ' +
                '           </div> ' +


                '           <div class="form-group"> ' +
                '               <label for="fieldLead" class="col-sm-2 control-label"> ' +
                '                   Leads / Managers ' +
                '               </label> ' +
                '               <div class="col-sm-8"> ' +

                '                   <div class="tab-leads-container"> ' +
                '                       <div style="width: 30%;float: right;"> ' +
                '                           <button type="button" class="btnNewLead btn btn-success top15 left15 bottom15 pull-right"><i class="fa fa-plus-square-o"></i> </button> ' +
                '                           <button type="button" class="btnDeleteLead btn btn-danger  top15 left15 bottom15 pull-right"><i class="glyphicon glyphicon-remove-sign"></i> </button> ' +
                '                       </div> ' +
                '                       <div style="width: 40%;float: right;padding-top: 15px;"> ' +
                '                           <div class="selLead"></div> ' +
                '                       </div> ' +
                '                       <div style="width: 20%;float: right;padding-top: 15px;"> ' +
                '                           <select name="selLeadEntityToLead" class="form-control selLeadEntityToLead"></select> ' +
                '                           <a href="#" class="btnManageEntityToLead" selEntityToLeadType=".selLeadEntityToLeadType"><i class="fa fa-edit"></i> Manage Options</a> ' +
                '                       </div> ' +
                '                       <div style="width: 10%;float: right;padding-top: 15px;"> ' +
                '                           <select name="selLeadEntityToLeadType" onChangeRefresh=".selLeadEntityToLead" class="form-control selLeadEntityToLeadType"></select> ' +
                '                       </div> ' +
                '                   </div> ' +
                '                   <div id="' + idTableLead + '" class="clearfix"></div> ' +

                '               </div> ' +
                '           </div> ' +


                '        </div> ' +
                '    </div> ' +
                //'    <div class="tab-pane fade in" id="tab-rpls"> ' +
                
                //'    </div> ' +
                //'    <div class="tab-pane fade in" id="tab-leads"> ' +
                
                //'    </div> ' +
                '    <div class="tab-pane fade in" id="tab-resources"> ' +
                '        <div style="width: 15%;float: right;"> ' +
                '            <button type="button" class="btnDeleteResource btn btn-danger  top15 left15 bottom15 pull-right"><i class="glyphicon glyphicon-remove-sign"></i> </button> ' +
                '            <button type="button" class="btnNewResource btn btn-success top15 left15 bottom15 pull-right"><i class="fa fa-plus-square-o"></i> </button> ' +
                '        </div> ' +
                '        <div style="width: 40%;float: right;padding-top: 15px;"> ' +
                '            <input class="form-control txtResourceDescription" placeholder="Description " /> ' +
                '        </div> ' +
                '        <div style="width: 45%;float: right;padding-top: 15px;"> ' +
                '            <input class="form-control txtResourceLink" placeholder="E.g. https://www.blueworkslive.com/scr/processes/2b38832e08#map" /> ' +
                '        </div> ' +
                '        <div id="' + idTableResources + '" class="clearfix"></div> ' +
                '    </div> ' +
                '</div> ';

            htmlFormEdit =
                "<div class='form-horizontal'>" +
                "   <div class='row'>" +
                "       <div class='col-sm-12 form-process-container'>" + htmlFormEdit + "</div>" +
                "   </div>" +
                "</div>";

            return htmlFormEdit;
        }

        function modalOnReady($modal) {
            //Create Critical Category Options
            //$modal.find(".fieldCriticalCategory").append($('<option>', { value: "", text: "" }));
            var optionWasSelected = false;
            for (var i = 0; i < criticalCategoryList.length; i++) {
                var objCriticalCategory = criticalCategoryList[i];

                $modal.find(".fieldCriticalCategory").append($('<option>', {
                    selected: (objCriticalCategory.IDCriticalCategory == selectedRow.IDCriticalCategory),
                    value: objCriticalCategory.IDCriticalCategory,
                    text: objCriticalCategory.CriticalCategoryName
                }));

                if (objCriticalCategory.IDCriticalCategory == selectedRow.IDCriticalCategory) {
                    optionWasSelected = true;
                }
            }

            //Select Default Option "ID:3 Non Critical Process"
            //if (!optionWasSelected && selectedRow.CantChilds == 0) {
                //$modal.find(".fieldCriticalCategory").find("option[value=3]").prop("selected", true);
            //}

            //Load Selects of Entitiy To Lead Types
            for (var i = 0; i < entityToLeadTypeList.length; i++) {
                var objEntityToLeadType = entityToLeadTypeList[i];

                $modal.find(".selRPLEntityToLeadType").append($('<option>', {
                    EntityToLeadTypeName: objEntityToLeadType.EntityToLeadTypeName,
                    value: objEntityToLeadType.IDEntityToLeadType,
                    text: objEntityToLeadType.EntityToLeadTypeName
                }));

                $modal.find(".selLeadEntityToLeadType").append($('<option>', {
                    EntityToLeadTypeName: objEntityToLeadType.EntityToLeadTypeName,
                    value: objEntityToLeadType.IDEntityToLeadType,
                    text: objEntityToLeadType.EntityToLeadTypeName
                }));
            }

            //On Change EntityToLeadType load options of Entities To Lead
            $modal.find(".selRPLEntityToLeadType,.selLeadEntityToLeadType").change(function () {
                var idType = $(this).find(":selected").val();
                var type = $(this).find(":selected").attr("EntityToLeadTypeName");
                var selToRefresh = $(this).attr("onChangeRefresh");

                _getProcessEntityToLeadDataByIDType({
                    idType: idType,
                    type: type,
                    onReady: function (responseEntityToLeadList) {
                        //Clear old values
                        $modal.find(selToRefresh).contents().remove();

                        //Add New Values
                        for (var i = 0; i < responseEntityToLeadList.length; i++) {
                            var objEntityToLead = responseEntityToLeadList[i];

                            $modal.find(selToRefresh).append($('<option>', {
                                selected: (i == 0),
                                EntityToLeadName: objEntityToLead.EntityToLeadName,
                                value: objEntityToLead.IDEntityToLead,
                                text: objEntityToLead.EntityToLeadName
                            }));
                        }
                    }
                });
            });
            $modal.find(".selRPLEntityToLeadType,.selLeadEntityToLeadType").change();

            // On Click Manage Entities To Lead
            $(".btnManageEntityToLead").click(function (e) {
                e.preventDefault();
                var selType = $(this).attr("selEntityToLeadType");
                var idType = $modal.find(selType).find(":selected").val();
                var type = $modal.find(selType).find(":selected").attr("EntityToLeadTypeName");

                openModalManageEntityToLead({
                    idType: idType,
                    type: type,
                    onSave: function () {
                        //Load new changes
                        $modal.find(selType).change();
                    }
                });
            });

            // Creat GPL Select
            _createSelectSOEID({
                id: $modal.find(".selGPL"),
                selSOEID: selectedRow.GPLSOEID
                //disabled: (selectedRow.Level != 2)
            });

            // Creat RPL Select
            _createSelectSOEID({
                id: $modal.find(".selRPL")
            });

            // Creat Lead Select
            _createSelectSOEID({
                id: $modal.find(".selLead")
            });

            // initialize jqxDropDownParent
            $modal.find(".jqxDropDownParent").jqxDropDownButton({
                width: "100%", height: 34
            });

            //Set "--Select Parent --" in dropdown.
            var dropDownContent = '<div class="fieldIDParent" value="' + selectedRow.IDParent + '" style="padding: 8px;">' + selectedRow.SelParent + '</div>';
            $modal.find(".jqxDropDownParent").jqxDropDownButton('setContent', dropDownContent);

            _createProcessTable({
                idTable: "#" + idTableProcessParent,
                simple: true,
                processList: processListTable,
                onReady: function () {
                    var $jqxGrid = $("#" + idTableProcessParent);

                    //On select row event
                    $jqxGrid.on('cellclick', function (event) {
                        var rowSelectEvent = setInterval(function () {

                            var args = event.args;
                            if ($jqxGrid.attr("ClickedRowBtnChild") != args.rowindex) {
                                var row = $jqxGrid.jqxGrid('getrowdata', args.rowindex);

                                var dropDownContent = '<div class="fieldIDParent" value="' + row.ID + '" style="padding: 8px;">' + (row.Key ? (row.Key + ": ") : "") + 'L' + row.Level + ' ' + row.Name + '</div>';
                                $modal.find(".jqxDropDownParent").jqxDropDownButton('setContent', dropDownContent);
                                $modal.find(".jqxDropDownParent").jqxDropDownButton('close');

                                //Update child process Level
                                var newLevel = (row.Level + 1);
                                $modal.find(".fieldLevel").html("L" + newLevel);
                                $modal.find(".fieldLevel").attr("value", newLevel);

                                //Update Alerts
                                loadAlertValidations(newLevel);
                            } else {
                                $jqxGrid.attr("ClickedRowBtnChild", "0");
                            }

                            clearInterval(rowSelectEvent);
                        }, 200);
                        //Close DropDown
                        //$(".jqxDropDownParent").jqxDropDownButton('close');
                    });
                }
            });

            //Load RPL Information
            loadTableOfRPLs(selectedRow.ID, idTableRPL, $modal);

            //Load Managers Information
            loadTableOfLeads(selectedRow.ID, idTableLead, $modal);

            //Load Resources Information
            loadTableOfResources(selectedRow.ID, idTableResources, $modal);

            // on click clear parent 
            $modal.find(".btnClearParent").click(function () {
                $modal.find(".jqxDropDownParent").jqxDropDownButton('setContent', '<div class="fieldIDParent" value="0" style="padding: 8px;">-- No Parent --</div>');

                //Update child process Level
                $modal.find(".fieldLevel").html("L1");
                $modal.find(".fieldLevel").attr("value", "1");
            });

            // Load alert validations in screen
            function loadAlertValidations(processLevel) {

                if (processLevel != 2) {
                    $modal.find(".selGPL select").attr("disabled", true);
                    $modal.find(".msgGPLValidation").show();
                } else {
                    $modal.find(".selGPL select").removeAttr("disabled");
                    $modal.find(".msgGPLValidation").hide();
                }

                if (processLevel != 3) {
                    $modal.find(".tab-rpls-container").hide();
                    _showAlert({
                        id: "RPLAlert",
                        showTo: $modal.find(".tab-rpls-container").parent(),
                        type: 'Disabled',
                        title: "Message",
                        content: "Only Menu of Services (L3 Process) can edit RPL"
                    });
                } else {
                    $modal.find(".tab-rpls-container").show();
                    $modal.find(".tab-rpls-container").parent().find("#RPLAlert").remove();
                }

                if (processLevel != 4) {
                    $modal.find(".tab-leads-container").hide();
                    _showAlert({
                        id: "LeadAlert",
                        showTo: $modal.find(".tab-leads-container").parent(),
                        type: 'Disabled',
                        title: "Message",
                        content: "Only Sub-Process (L4) can edit Lead / Manager"
                    });
                } else {
                    $modal.find(".tab-leads-container").show();
                    $modal.find(".tab-leads-container").parent().find("#LeadAlert").remove();
                }
            }
            loadAlertValidations(selectedRow.Level);

        }

        function modalOnSave($modal) {
            var action = 'Add';
            var idProcess = $modal.find(".fieldID").val();
            var idParent = $modal.find(".fieldIDParent").attr("value");
            var level = $modal.find(".fieldLevel").attr("value");
            var idCriticalCategory = $modal.find(".fieldCriticalCategory").val();
            var gplSOEID = $modal.find(".selGPL select").val();
            var category = "L" + level;
            var key = $modal.find(".fieldKey").val();
            var name = $modal.find(".fieldName").val();

            //Check if we need update
            if (idProcess > 0) {
                action = 'Update';
            }

            //Get RPLs
            var rplRows = $("#" + idTableRPL).jqxGrid("getRows");
            var strListRPLs = "Skip";
            if (selectedRow.Level == 3) {
                strListRPLs = generateString(rplRows, ["IDEntityToLead", "RPLSOEID"], "~", "|");
            }

            //Get Leads
            var leadRows = $("#" + idTableLead).jqxGrid("getRows");
            var strListLeads = "Skip";
            if (selectedRow.Level == 4) {
                strListLeads = generateString(leadRows, ["IDEntityToLead", "LeadSOEID"], "~", "|");
            }

            //Get Resources
            var resourceRows = $("#" + idTableResources).jqxGrid("getRows");
            var strListResources = generateString(resourceRows, ["Link", "Description"], "~", "|");

            if (validateProcess()) {
                //Save data in DB
                _callProcedure({
                    response: true,
                    name: "[Automation].[dbo].[spAFrwkProcessAdmin]",
                    params: [
                        { "Name": "@Action", "Value": action },
                        { "Name": "@IDProcess", "Value": idProcess },
                        { "Name": "@IDParent", "Value": idParent },
                        { "Name": "@IDCriticalCategory", "Value": idCriticalCategory },
                        { "Name": "@GPLSOEID", "Value": gplSOEID },
                        { "Name": "@StrListRPLs", "Value": strListRPLs },
                        { "Name": "@StrListLeads", "Value": strListLeads },
                        { "Name": "@StrListResources", "Value": strListResources },
                        { "Name": "@Level", "Value": level },
                        { "Name": "@Category", "Value": category },
                        { "Name": "@Key", "Value": key },
                        { "Name": "@Name", "Value": $.trim(name) },
                        { "Name": "@SessionSOEID", "Value": _getSOEID() }
                    ],
                    success: {
                        fn: function (responseID) {
                            if (responseID.length > 0) {
                                //Update ID process
                                if (action == 'Add') {
                                    $modal.find(".fieldID").val(responseID[0].IDProcess);
                                }

                                //show success alert
                                _showNotification("success", "The information (L" + level + " " + name + ") was saved successfully.", "AlertProcess");

                                //Close popup
                                $modal.find(".close").click();

                                //refresh table
                                loadTableProcess();

                                //Refresh filters process
                                _refreshFilterProcessByChange($("#filter-process"), level);
                            }
                        }
                    }
                });
            }

            //Validate Data
            function validateProcess() {
                var result = true;

                //if (!key) {
                //    _showNotification("error", "Unique ID cannot be empty.", "KeyAlert", false);
                //    result = false;
                //}

                if (!name) {
                    _showNotification("error", "Process Name cannot be empty.", "NameAlert", false);
                    result = false;
                }

                if (result && validateExistsProcessKey(idProcess, key)) {
                    //Notification is trigger in validateExistsProcessKey
                    result = false;
                }

                if (validateParentToChildRecursiveLoop(idProcess, idParent)) {
                    _showNotification("error", "The process (L" + level + " " + name + ") cannot be under theirs own childs.", "UnderOwnChildsAlert", false);
                    result = false;
                }

                return result;
            }

            //Validate Unique ID for Process Key
            function validateExistsProcessKey(idProcess, key) {
                var result = false;

                if (key) {
                    var objProcessWithSameKey = _findOneObjByProperty(processListTable, "Key", key);

                    if (objProcessWithSameKey && objProcessWithSameKey.ID != idProcess) {
                        _showNotification("error", "Unique ID '" + key + "' already exists for 'L" + objProcessWithSameKey.Level + " " + objProcessWithSameKey.Name + "'.", "SameKeyAlert", false);
                        result = true;
                    }
                }

                return result;
            }

            //Avoid parent childs recursive loop infinite
            function validateParentToChildRecursiveLoop(idProcess, idProcessToBeParent) {
                var result = false;
                var resultList = $.grep(processListTable, function (v) {
                    return v.ID == idProcessToBeParent;
                });

                if (resultList.length > 0) {
                    var objProcess = resultList[0];

                    if (objProcess.IDParent > 0) {
                        if (idProcess != objProcess.IDParent && idProcess != objProcess.ID) {
                            result = validateParentToChildRecursiveLoop(idProcess, objProcess.IDParent);
                        } else {
                            result = true;
                        }
                    }
                }

                return result;
            }
        }

        function openModalManageEntityToLead(customOptions) {
            //Default Options.
            var options = {
                idType: "",
                type: "Entity",
                onSave: function () { }
            };

            //Hacer un merge con las opciones que nos enviaron y las default.
            $.extend(true, options, customOptions);

            var idTable = "tblManageEntityToLead_" + _createCustomID();
            var htmlContentModal =
                '<div class="row"> ' +
                '    <div class="col-md-12"> ' +
                '        <button type="button" class="btnAddNew btn btn-success top15 left15 bottom15 pull-right"><i class="fa fa-asterisk"></i> Add</button> ' +
                '        <button type="button" class="btnDelete btn btn-danger top15 left15 bottom15 pull-right"><i class="fa fa-remove"></i> Delete</button> ' +
                '    </div> ' +
                '    <div class="col-md-12"> ' +
                '        <div id="' + idTable + '"></div> ' +
                '    </div> ' +
                '</div> ';

            _showModal({
                modalId: "modalManageEntityToLead",
                width: '95%',
                addCloseButton: true,
                buttons: [{
                    name: "<i class='fa fa-save'></i> Save",
                    class: "btn-success",
                    closeModalOnClick: false,
                    onClick: function ($modal) {
                        saveTable();
                    }
                }],
                title: "Manage " + options.type,
                contentHtml: htmlContentModal,
                onReady: function ($modal) {
                    //Add new row 
                    $modal.find(".btnAddNew").click(function () {
                        addNewRow();
                    });

                    //Delete selected row 
                    $modal.find(".btnDelete").click(function () {
                        deleteRow();
                    });

                    //Load table 
                    loadTable();
                }
            });

            function loadTable() {
                $.jqxGridApi.create({
                    showTo: "#" + idTable,
                    options: {
                        //for comments or descriptions
                        height: "350",
                        autoheight: false,
                        autorowheight: false,
                        selectionmode: "singlerow",
                        showfilterrow: true,
                        sortable: true,
                        editable: true,
                        groupable: false
                    },
                    sp: {
                        Name: "[Automation].[dbo].[spAFrwkProcessAdminEntityToLead]",
                        Params: [
                            { Name: "@Action", Value: "List" },
                            { Name: "@IDType", Value: options.idType }
                        ]
                    },
                    source: {
                        // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
                        dataBinding: "Large Data Set"
                    },
                    columns: [
                        //type: string - text - number - int - float - date - time 
                        //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                        //cellsformat: ddd, MMM dd, yyyy h:mm tt
                        { name: 'ID', type: 'number', hidden: true },
                        { name: 'Name', text: options.type, width: '99%', type: 'string', filtertype: 'input' }
                    ],
                    ready: function () {}
                });
            }

            function addNewRow() {
                var $jqxGrid = $("#" + idTable);
                var datarow = {
                    ID: 0,
                    Name: 'New ' + options.type + ' (Double click to update)'
                };
                var commit = $jqxGrid.jqxGrid('addrow', null, datarow);

                _showNotification("success", "An empty row was added in the table.", "NewEntityToLead");
            }

            function deleteRow() {
                //Delete 
                var objRowSelected = $.jqxGridApi.getOneSelectedRow("#" + idTable, true);
                if (objRowSelected) {
                    var htmlContentModal = "";
                    htmlContentModal += "<b>" + options.type + ": </b>" + objRowSelected['Name'] + "<br/>";

                    _showModal({
                        width: '40%',
                        modalId: "modalDelRow",
                        addCloseButton: true,
                        buttons: [{
                            name: "Delete",
                            class: "btn-danger",
                            onClick: function () {
                                if (objRowSelected.ID != 0) {
                                    _callProcedure({
                                        loadingMsgType: "fullLoading",
                                        loadingMsg: "Deleting " + options.type + "...",
                                        name: "[Automation].[dbo].[spAFrwkProcessAdminEntityToLead]",
                                        params: [
                                            { "Name": "@Action", "Value": "Delete" },
                                            { "Name": "@IDEntityToLead", "Value": objRowSelected.ID },
                                            { "Name": "@SessionSOEID", "Value": _getSOEID() }
                                        ],
                                        //Show message only for the last 
                                        success: {
                                            showTo: $("#" + idTable).parent(),
                                            msg: "Row was deleted successfully.",
                                            fn: function () {
                                                var reloadData = setInterval(function () {
                                                    //console.log("Reloading data from #tblReasons");
                                                    //$.jqxGridApi.localStorageFindById("#" + idTable).fnReload();
                                                    //$("#" + idTable).jqxGrid('updateBoundData');

                                                    //Refresh Table
                                                    loadTable();

                                                    //Remove changes
                                                    $.jqxGridApi.rowsChangedFindById("#" + idTable).rows = [];

                                                    //Trigger onSave event
                                                    if (options.onSave) {
                                                        options.onSave();
                                                    }

                                                    clearInterval(reloadData);
                                                }, 500);
                                            }
                                        }
                                    });
                                } else {
                                    $("#" + idTable).jqxGrid('deleterow', objRowSelected.uid);
                                }
                            }
                        }],
                        title: "Are you sure you want to delete this row?",
                        contentHtml: htmlContentModal
                    });
                }
            }

            function saveTable() {
                var rowsChanged = $.jqxGridApi.rowsChangedFindById("#" + idTable).rows;

                //Validate changes
                if (rowsChanged.length > 0) {
                    var fnSuccess = {
                        showTo: $("#" + idTable).parent(),
                        msg: "The data was saved successfully.",
                        fn: function () {
                            var reloadData = setInterval(function () {
                                //console.log("Reloading data from #tblReasons");
                                //$.jqxGridApi.localStorageFindById("#" + idTable).fnReload();
                                //$("#" + idTable).jqxGrid('updateBoundData');

                                loadTable();

                                //Remove changes
                                $.jqxGridApi.rowsChangedFindById("#" + idTable).rows = [];

                                //Trigger onSave event
                                if (options.onSave) {
                                    options.onSave();
                                }

                                clearInterval(reloadData);
                            }, 1500);
                        }
                    };

                    for (var i = 0; i < rowsChanged.length; i++) {
                        var objRow = rowsChanged[i];
                        var action = "Edit";

                        if (objRow.ID == 0) {
                            var action = "Insert";
                        }

                        _callProcedure({
                            loadingMsgType: "fullLoading",
                            loadingMsg: "Saving " + options.type + " options...",
                            name: "[Automation].[dbo].[spAFrwkProcessAdminEntityToLead]",
                            params: [
                                { "Name": "@Action", "Value": action },
                                { "Name": "@IDType", "Value": options.idType },
                                { "Name": "@IDEntityToLead", "Value": objRow.ID },
                                { "Name": "@Name", "Value": objRow.Name },
                                { "Name": "@SessionSOEID", "Value": _getSOEID() }
                            ],
                            //Show message only for the last 
                            success: (i == rowsChanged.length - 1) ? fnSuccess : null
                        });
                    }
                } else {
                    _showAlert({
                        showTo: $("#" + idTable).parent(),
                        type: 'error',
                        title: "Message",
                        content: "No changes detected."
                    });
                }
            }
        }
    }
}

function generateString(listRows, listCols, rowDelimiter, colDelimiter) {
    var result = "";
    for (var i = 0; i < listRows.length; i++) {
        var objRow = listRows[i];

        if (listCols.length == 1) {
            result += objRow[colName];
        } else {
            for (var j = 0; j < listCols.length; j++) {
                var colName = listCols[j];

                //When is last col
                if (j == (listCols.length - 1)) {
                    result += objRow[colName];
                } else {
                    result += objRow[colName] + colDelimiter;
                }
                
            }
        }

        //Avoid last row to add rowDelimiter
        if (i != (listRows.length - 1)) {
            result += rowDelimiter;
        }
    }

    return result;
}

function loadTableOfRPLs(IDProcess, PIDTable, $modal) {
    var idTable = "#" + PIDTable;

    //Add new row 
    $modal.find(".btnNewRPL").click(function () {
        newRow();
    });

    //Delete selected row 
    $modal.find(".btnDeleteRPL").click(function () {
        deleteRow();
    });

    function loadTableRPLs() {
        $.jqxGridApi.create({
            showTo: idTable,
            options: {
                //for comments or descriptions
                height: "200",
                autoheight: false,
                autorowheight: false,
                selectionmode: "singlerow",
                showfilterrow: true,
                sortable: true,
                editable: true,
                groupable: false
            },
            sp: {
                Name: "[Automation].[dbo].[spAFrwkAdminProcessRPL]",
                Params: [
                    { Name: "@Action", Value: "List" },
                    { Name: "@IDProcess", Value: IDProcess }
                ],
                OnDataLoaded: function (responseList) {
                    $modal.find(".lblRPLs").html("RPL (" + responseList.length + ")");
                }
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
                dataBinding: "Large Data Set"
            },
            columns: [
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                { name: 'IDProcessXRPL', type: 'number', hidden: true },
                { name: 'IDEntityToLead', type: 'number', hidden: true },
                { name: 'RPLSOEID', type: 'string', hidden: true },
                { name: 'EntityToLeadName', text: 'To Lead', width: '30%', type: 'string', filtertype: 'input' },
                { name: 'RPLName', text: 'Name', width: '70%', type: 'string', filtertype: 'input' }
            ],
            ready: function () {

            }
        });
    }

    function newRow() {
        var $jqxGrid = $(idTable);
        var inputRPLSOEID = $modal.find(".selRPL select").find(":selected").val();
        var inputRPLName = $modal.find(".selRPL select").find(":selected").attr("Name");
        var idEntityToLead = $modal.find(".selRPLEntityToLead").val();
        var entityToLeadName = $modal.find(".selRPLEntityToLead").find(":selected").attr("EntityToLeadName");

        //Validate if the user fill all field
        if (inputRPLSOEID && idEntityToLead) {

            //Validate if soeid not exist in current list
            //var soeidsRows = $(idTable).jqxGrid("getRows");
            //var existsRPL = _findAllObjByProperty(soeidsRows, "RPLSOEID", inputRPLSOEID);
            //if (existsRPL.length == 0) {

            var datarow = {
                IDProcessXRPL: 0,
                IDEntityToLead: idEntityToLead,
                RPLSOEID: inputRPLSOEID,
                RPLName: inputRPLName,
                EntityToLeadName: entityToLeadName
            };
            var commit = $jqxGrid.jqxGrid('addrow', null, datarow);

            //Refresh RPLs Counter
            $modal.find(".lblRPLs").html("RPL (" + $jqxGrid.jqxGrid("getRows").length + ")");

            //} else {
            //    _showAlert({
            //        showTo: $(idTable).parent(),
            //        type: 'error',
            //        title: "Message",
            //        content: "'" + inputRPLName + "' already exist in RPL."
            //    });
            //}
        } else {
            var entityToLeadTypeName = $modal.find(".selRPLEntityToLeadType").find(":selected").attr("EntityToLeadTypeName");
            _showAlert({
                showTo: $(idTable).parent(),
                type: 'error',
                title: "Message",
                content: "'" + entityToLeadTypeName + " option' or 'RPL SOEID' is empty."
            });
        }
    }

    function deleteRow() {
        //Delete 
        var objRowSelected = $.jqxGridApi.getOneSelectedRow(idTable, true);
        if (objRowSelected) {
            var entityToLeadTypeName = $modal.find(".selRPLEntityToLeadType").find(":selected").attr("EntityToLeadTypeName");
            var htmlContentModal = "";
            htmlContentModal += "<b>" +entityToLeadTypeName + ": </b>" + objRowSelected['EntityToLeadName'] + "<br/>";
            htmlContentModal += "<b>RPL: </b>" + objRowSelected['RPLName'] + "<br/>";

            _showModal({
                width: '40%',
                modalId: "modalDelRow",
                addCloseButton: true,
                buttons: [{
                    name: "Delete",
                    class: "btn-danger",
                    onClick: function () {
                        //if (objRowSelected.IDProcessXRPL != 0) {
                        //    _callProcedure({
                        //        loadingMsgType: "fullLoading",
                        //        loadingMsg: "Deleting row...",
                        //        name: "[Automation].[dbo].[spAFrwkAdminProcessRPL]",
                        //        params: [
                        //            { "Name": "@Action", "Value": "Delete" },
                        //            { "Name": "@IDProcessXRPL", "Value": objRowSelected.IDProcessXRPL },
                        //            { "Name": "@SessionSOEID", "Value": _getSOEID() }
                        //        ],
                        //        //Show message only for the last 
                        //        success: {
                        //            showTo: $(idTable).parent(),
                        //            msg: "Row was deleted successfully.",
                        //            fn: function () {
                        //                var reloadData = setInterval(function () {
                        //                    //Reload table
                        //                    loadTableRPLs();

                        //                    //Remove changes
                        //                    $.jqxGridApi.rowsChangedFindById(idTable).rows = [];

                        //                    clearInterval(reloadData);
                        //                }, 500);
                        //            }
                        //        }
                        //    });
                        //} else {
                            $(idTable).jqxGrid('deleterow', objRowSelected.uid);
                        //}
                    }
                }],
                title: "Are you sure you want to delete this row?",
                contentHtml: htmlContentModal
            });
        }
    }

    //Load table 
    loadTableRPLs();
}

function loadTableOfLeads(IDProcess, PIDTable, $modal) {
    var idTable = "#" + PIDTable;

    //Add new row 
    $modal.find(".btnNewLead").click(function () {
        newRow();
    });

    //Delete selected row 
    $modal.find(".btnDeleteLead").click(function () {
        deleteRow();
    });

    function loadTableLeads() {
        $.jqxGridApi.create({
            showTo: idTable,
            options: {
                //for comments or descriptions
                height: "200",
                autoheight: false,
                autorowheight: false,
                selectionmode: "singlerow",
                showfilterrow: true,
                sortable: true,
                editable: true,
                groupable: false
            },
            sp: {
                Name: "[Automation].[dbo].[spAFrwkAdminProcessLead]",
                Params: [
                    { Name: "@Action", Value: "List" },
                    { Name: "@IDProcess", Value: IDProcess }
                ],
                OnDataLoaded: function (responseList) {
                    $modal.find(".lblLeads").html("Lead (" + responseList.length + ")");
                }
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
                dataBinding: "Large Data Set"
            },
            columns: [
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                { name: 'IDProcessXLead', type: 'number', hidden: true },
                { name: 'IDEntityToLead', type: 'number', hidden: true },
                { name: 'LeadSOEID', type: 'string', hidden: true },
                { name: 'EntityToLeadName', text: 'To Lead', width: '30%', type: 'string', filtertype: 'input' },
                { name: 'LeadName', text: 'Name', width: '70%', type: 'string', filtertype: 'input' }
            ],
            ready: function () {

            }
        });
    }

    function newRow() {
        var $jqxGrid = $(idTable);
        var inputLeadSOEID = $modal.find(".selLead select").find(":selected").val();
        var inputLeadName = $modal.find(".selLead select").find(":selected").attr("Name");
        var idEntityToLead = $modal.find(".selLeadEntityToLead").val();
        var entityToLeadName = $modal.find(".selLeadEntityToLead").find(":selected").attr("EntityToLeadName");

        //Validate if the user fill all field
        if (inputLeadSOEID && idEntityToLead) {

            //Validate if soeid not exist in current list
            //var soeidsRows = $(idTable).jqxGrid("getRows");
            //var existsLead = _findAllObjByProperty(soeidsRows, "LeadSOEID", inputLeadSOEID);
            //if (existsLead.length == 0) {

            var datarow = {
                IDProcessXLead: 0,
                IDEntityToLead: idEntityToLead,
                LeadSOEID: inputLeadSOEID,
                LeadName: inputLeadName,
                EntityToLeadName: entityToLeadName
            };
            var commit = $jqxGrid.jqxGrid('addrow', null, datarow);

            //Refresh Leads Counter
            $modal.find(".lblLeads").html("Lead (" + $jqxGrid.jqxGrid("getRows").length + ")");

            //} else {
            //    _showAlert({
            //        showTo: $(idTable).parent(),
            //        type: 'error',
            //        title: "Message",
            //        content: "'" + inputLeadName + "' already exist in Lead."
            //    });
            //}
        } else {
            var entityToLeadTypeName = $modal.find(".selLeadEntityToLeadType").find(":selected").attr("EntityToLeadTypeName");
            _showAlert({
                showTo: $(idTable).parent(),
                type: 'error',
                title: "Message",
                content: "'" + entityToLeadTypeName + " option' or 'Lead SOEID' is empty."
            });
        }
    }

    function deleteRow() {
        //Delete 
        var objRowSelected = $.jqxGridApi.getOneSelectedRow(idTable, true);
        if (objRowSelected) {
            var entityToLeadTypeName = $modal.find(".selLeadEntityToLeadType").find(":selected").attr("EntityToLeadTypeName");
            var htmlContentModal = "";
            htmlContentModal += "<b>" + entityToLeadTypeName + ": </b>" + objRowSelected['EntityToLeadName'] + "<br/>";
            htmlContentModal += "<b>Lead: </b>" + objRowSelected['LeadName'] + "<br/>";

            _showModal({
                width: '40%',
                modalId: "modalDelRow",
                addCloseButton: true,
                buttons: [{
                    name: "Delete",
                    class: "btn-danger",
                    onClick: function () {
                        //if (objRowSelected.IDProcessXLead != 0) {
                        //    _callProcedure({
                        //        loadingMsgType: "fullLoading",
                        //        loadingMsg: "Deleting row...",
                        //        name: "[Automation].[dbo].[spAFrwkAdminProcessLead]",
                        //        params: [
                        //            { "Name": "@Action", "Value": "Delete" },
                        //            { "Name": "@IDProcessXLead", "Value": objRowSelected.IDProcessXLead },
                        //            { "Name": "@SessionSOEID", "Value": _getSOEID() }
                        //        ],
                        //        //Show message only for the last 
                        //        success: {
                        //            showTo: $(idTable).parent(),
                        //            msg: "Row was deleted successfully.",
                        //            fn: function () {
                        //                var reloadData = setInterval(function () {
                        //                    //Reload table
                        //                    loadTableLeads();

                        //                    //Remove changes
                        //                    $.jqxGridApi.rowsChangedFindById(idTable).rows = [];

                        //                    clearInterval(reloadData);
                        //                }, 500);
                        //            }
                        //        }
                        //    });
                        //} else {
                        $(idTable).jqxGrid('deleterow', objRowSelected.uid);
                        //}
                    }
                }],
                title: "Are you sure you want to delete this row?",
                contentHtml: htmlContentModal
            });
        }
    }

    //Load table 
    loadTableLeads();
}

function loadTableOfResources(IDProcess, PIDTable, $modal) {
    var idTable = "#" + PIDTable;

    //Add new row 
    $modal.find(".btnNewResource").click(function () {
        newRow();
    });

    //Delete selected row 
    $modal.find(".btnDeleteResource").click(function () {
        deleteRow();
    });

    function loadTableResources() {
        $.jqxGridApi.create({
            showTo: idTable,
            options: {
                //for comments or descriptions
                height: "200",
                autoheight: false,
                autorowheight: false,
                selectionmode: "singlerow",
                showfilterrow: true,
                sortable: true,
                editable: true,
                groupable: false
            },
            sp: {
                Name: "[Automation].[dbo].[spAFrwkAdminProcessResource]",
                Params: [
                    { Name: "@Action", Value: "List" },
                    { Name: "@IDProcess", Value: IDProcess }
                ],
                OnDataLoaded: function (responseList) {
                    $modal.find(".lblResources").html("Blueworks / Resources (" + responseList.length + ")");
                }
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
                dataBinding: "Large Data Set"
            },
            columns: [
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                { name: 'IDProcessXResource', type: 'number', hidden: true },
                { name: 'Link', text: 'Link', width: '45%', type: 'string', filtertype: 'input' },
                { name: 'Description', text: 'Description', width: '55%', type: 'string', filtertype: 'input' }
            ],
            ready: function () {

            }
        });
    }

    function newRow() {
        var $jqxGrid = $(idTable);
        var inputResourceLink = $modal.find(".txtResourceLink").val();
        var inputResourceDescription = $modal.find(".txtResourceDescription").val();

        //Validate if exists the link and the description
        if (inputResourceLink && inputResourceDescription) {

            //Validate if soeid not exist in current list
            //var soeidsRows = $(idTable).jqxGrid("getRows");
            //var existsResource = _findAllObjByProperty(soeidsRows, "ResourceSOEID", inputResourceSOEID);
            //if (existsResource.length == 0) {

            var datarow = {
                IDProcessXResource: 0,
                Link: inputResourceLink,
                Description: inputResourceDescription
            };
            var commit = $jqxGrid.jqxGrid('addrow', null, datarow);

            //Refresh Resources Counter
            $modal.find(".lblResources").html("Blueworks / Resources (" + $jqxGrid.jqxGrid("getRows").length + ")");

            //} else {
            //    _showAlert({
            //        showTo: $(idTable).parent(),
            //        type: 'error',
            //        title: "Message",
            //        content: "'" + inputResourceLink + "' already exist in Resource."
            //    });
            //}
        } else {
            _showAlert({
                showTo: $(idTable).parent(),
                type: 'error',
                title: "Message",
                content: "Link or Description is empty."
            });
        }
    }

    function deleteRow() {
        //Delete 
        var objRowSelected = $.jqxGridApi.getOneSelectedRow(idTable, true);
        if (objRowSelected) {
            var htmlContentModal = "";
            htmlContentModal += "<b>Resource: </b>" + objRowSelected['ResourceLink'] + "<br/>";

            _showModal({
                width: '40%',
                modalId: "modalDelRow",
                addCloseButton: true,
                buttons: [{
                    name: "Delete",
                    class: "btn-danger",
                    onClick: function () {
                        //if (objRowSelected.IDProcessXResource != 0) {
                        //    _callProcedure({
                        //        loadingMsgType: "fullLoading",
                        //        loadingMsg: "Deleting row...",
                        //        name: "[Automation].[dbo].[spAFrwkAdminProcessResource]",
                        //        params: [
                        //            { "Name": "@Action", "Value": "Delete" },
                        //            { "Name": "@IDProcessXResource", "Value": objRowSelected.IDProcessXResource },
                        //            { "Name": "@SessionSOEID", "Value": _getSOEID() }
                        //        ],
                        //        //Show message only for the last 
                        //        success: {
                        //            showTo: $(idTable).parent(),
                        //            msg: "Row was deleted successfully.",
                        //            fn: function () {
                        //                var reloadData = setInterval(function () {
                        //                    //Reload table
                        //                    loadTableResources();

                        //                    //Remove changes
                        //                    $.jqxGridApi.rowsChangedFindById(idTable).rows = [];

                        //                    clearInterval(reloadData);
                        //                }, 500);
                        //            }
                        //        }
                        //    });
                        //} else {
                            $(idTable).jqxGrid('deleterow', objRowSelected.uid);
                        //}
                    }
                }],
                title: "Are you sure you want to delete this row?",
                contentHtml: htmlContentModal
            });
        }
    }

    //Load table 
    loadTableResources();
}

function loadTableProcess() {

    _getProcessData({
        $filterContainer: $("#filter-process"),
        onReady: function (resultList) {
            _createProcessTable({
                simple: false,
                useCollapseCache: true,
                idTable: "#tblProcess",
                processList: resultList,
                criticalCategoryList: criticalCategoryList,
                onSavedProcess: function (objProcessSaved) {
                    //Refresh filters process
                    _refreshFilterProcessByChange($("#filter-process"), objProcessSaved.Level);
                }
            });
        }
    });
}