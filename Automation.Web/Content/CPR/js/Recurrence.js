/// <reference path="../../Shared/plugins/util/global.js" />
var businessDayList = ['BD-14','BD-13','BD-12','BD-11','BD-10','BD-9','BD-8','BD-7','BD-6','BD-5','BD-4','BD-3','BD-2','BD-1','BD1','BD2','BD3','BD4','BD5','BD6','BD7','BD8','BD9','BD10','BD11','BD12','BD13','BD14','BD15','BD16','BD17','BD18','BD19','BD20','BD21'];
var monthList = ['January', 'February', 'March', 'April', 'May', 'June ', 'July', 'August', 'September', 'October', 'November', 'December'];
var monthDayList = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];
var maxDateCorporateCalendar = "";

$(document).ready(function () {
    //Hide Menu
    _hideMenu();

    //Create Field Business Days / Calendar Days for Repeat Montly
    createFieldCalendarBusinessDays($("#containerRepeatMonthly"));

    //Create Field Business Days / Calendar Days for Repeat Quarterly => Months
    createFieldCalendarBusinessDays($("#containerFieldRepeatQuarterlyMonthVsBD"));

    //Create Field Business Days / Calendar Days for Repeat Yearly => Months
    createFieldCalendarBusinessDays($("#containerFieldRepeatYearlyMonthVsBD"));

    //Generate Months Select Option, when user click in Yearly
    generateMonths("#fieldRepeatYearlySelectMonth");

    //On Click Copy Code
    $(".btnCopyCode").click(function () {
        if (isValidData()) {
            var recurrenceCode = $("#lblRecurrenceCode").html();
            _copyToClipboard(recurrenceCode);
            _showNotification("success", "Reference Code '" + recurrenceCode + "' was copied succesfully to clipboard.", "CopyCode");

            generateRecurrenceDatesInDB(recurrenceCode);
        } else {
            _showAlert({
                type: "error",
                content: "Please validate that you selected at least one option for '" + getRepeat() + "'."
            });
        }
    });

    //On Change field Repeat
    function onChangeFieldRepeat() {
        var repeatValue = getRepeat();

        //Hide all
        $("#containerRepeatDaily").hide();
        $("#containerRepeatWeekly").hide();
        $("#containerRepeatMonthly").hide();
        $("#containerRepeatQuarterly").hide();
        $("#containerRepeatYearly").hide();

        //Show selected value
        switch (repeatValue) {
            case "Daily":
                $("#containerRepeatDaily").show();
                break;

            case "Weekly":
                $("#containerRepeatWeekly").show();
                break;

            case "Monthly":
                $("#containerRepeatMonthly").show();
                break;

            case "Quarterly":
                $("#containerRepeatQuarterly").show();
                break;

            case "Yearly":
                $("#containerRepeatYearly").show();
                break;
        }

        // Refresh Code and Preview
        refreshData();
    }
    $("#fieldRepeat input").on('ifChecked', onChangeFieldRepeat);
    $("#fieldRepeat input").on('ifUnchecked', onChangeFieldRepeat);

    //On change Daily Options
    function onChangeDailyOptions() {
        // Refresh Code and Preview
        refreshData();
    }
    $("#fieldRepeatDaily input").on('ifChecked', onChangeDailyOptions);
    $("#fieldRepeatDaily input").on('ifUnchecked', onChangeDailyOptions);

    //On change Weekly Options
    function onChangeWeeklyOptions() {
        // Refresh Code and Preview
        refreshData();
    }
    $("#fieldRepeatWeekly input").on('ifChecked', onChangeWeeklyOptions);
    $("#fieldRepeatWeekly input").on('ifUnchecked', onChangeWeeklyOptions);

    //On change Monthly Options
    function onChangeMonthlyOptions() {
        // Refresh Code and Preview
        refreshData();
    }
    $("#containerRepeatMonthly .fieldCalendarDBsType input").on('ifChecked', onChangeMonthlyOptions);
    $("#containerRepeatMonthly .fieldCalendarDBsType input").on('ifUnchecked', onChangeMonthlyOptions);
    $("#containerRepeatMonthly .containerFieldBusinessDays input").on('ifChecked', onChangeMonthlyOptions);
    $("#containerRepeatMonthly .containerFieldBusinessDays input").on('ifUnchecked', onChangeMonthlyOptions);
    $("#containerRepeatMonthly .containerFieldCalendarDays input").on('ifChecked', onChangeMonthlyOptions);
    $("#containerRepeatMonthly .containerFieldCalendarDays input").on('ifUnchecked', onChangeMonthlyOptions);

    //On Change Quarterly Options
    function onChangeQuarterlyOptions() {
        // Refresh Code and Preview
        refreshData();
    }
    $("#fieldRepeatQuarterlyMonths input").on('ifChecked', onChangeMonthlyOptions);
    $("#fieldRepeatQuarterlyMonths input").on('ifUnchecked', onChangeMonthlyOptions);
    $("#containerFieldRepeatQuarterlyMonthVsBD .fieldCalendarDBsType input").on('ifChecked', onChangeMonthlyOptions);
    $("#containerFieldRepeatQuarterlyMonthVsBD .fieldCalendarDBsType input").on('ifUnchecked', onChangeMonthlyOptions);
    $("#containerFieldRepeatQuarterlyMonthVsBD .containerFieldBusinessDays input").on('ifChecked', onChangeMonthlyOptions);
    $("#containerFieldRepeatQuarterlyMonthVsBD .containerFieldBusinessDays input").on('ifUnchecked', onChangeMonthlyOptions);
    $("#containerFieldRepeatQuarterlyMonthVsBD .containerFieldCalendarDays input").on('ifChecked', onChangeMonthlyOptions);
    $("#containerFieldRepeatQuarterlyMonthVsBD .containerFieldCalendarDays input").on('ifUnchecked', onChangeMonthlyOptions);

    //On Change Yearly Options
    function onchangeYearlyOptions() {
        // Refresh Code and Preview
        refreshData();
    }
    $("#fieldRepeatYearlySelectMonth").change(refreshData);
    $("#containerFieldRepeatYearlyMonthVsBD .fieldCalendarDBsType input").on('ifChecked', onChangeMonthlyOptions);
    $("#containerFieldRepeatYearlyMonthVsBD .fieldCalendarDBsType input").on('ifUnchecked', onChangeMonthlyOptions);
    $("#containerFieldRepeatYearlyMonthVsBD .containerFieldBusinessDays input").on('ifChecked', onChangeMonthlyOptions);
    $("#containerFieldRepeatYearlyMonthVsBD .containerFieldBusinessDays input").on('ifUnchecked', onChangeMonthlyOptions);
    $("#containerFieldRepeatYearlyMonthVsBD .containerFieldCalendarDays input").on('ifChecked', onChangeMonthlyOptions);
    $("#containerFieldRepeatYearlyMonthVsBD .containerFieldCalendarDays input").on('ifUnchecked', onChangeMonthlyOptions);

    //Load Max Date Corporate Calendar
    loadMaxDateCorporateCalendar();

    // Refresh Code and Preview
    refreshData();
});

function isValidData() {
    var result = true;
    var type;
    var repeatValue = getRepeat();
    var inputSelectedOptions = [];

    //Show selected value
    switch (repeatValue) {
        case "Daily":
            return true;
            break;

        case "Weekly":
            inputSelectedOptions = $("#fieldRepeatWeekly").find("input:checked");
            break;

        case "Monthly":
            type = $("#containerRepeatMonthly .fieldCalendarDBsType").find("input:checked").val();
            if (type == "BD") {
                inputSelectedOptions = $("#containerRepeatMonthly .containerFieldBusinessDays").find("input:checked");
            }
            if (type == "Days") {
                inputSelectedOptions = $("#containerRepeatMonthly .containerFieldCalendarDays").find("input:checked");
            }
            break;

        case "Quarterly":
            var inputQuarterMonthsList = $("#fieldRepeatQuarterlyMonths").find("input:checked");
            if (inputQuarterMonthsList.length > 0) {
                type = $("#containerFieldRepeatQuarterlyMonthVsBD .fieldCalendarDBsType").find("input:checked").val();
                if (type == "BD") {
                    inputSelectedOptions = $("#containerFieldRepeatQuarterlyMonthVsBD .containerFieldBusinessDays").find("input:checked");
                }
                if (type == "Days") {
                    inputSelectedOptions = $("#containerFieldRepeatQuarterlyMonthVsBD .containerFieldCalendarDays").find("input:checked");
                }
            }
            break;

        case "Yearly":
            type = $("#containerFieldRepeatYearlyMonthVsBD .fieldCalendarDBsType").find("input:checked").val();
            if (type == "BD") {
                inputSelectedOptions = $("#containerFieldRepeatYearlyMonthVsBD .containerFieldBusinessDays").find("input:checked");
            }
            if (type == "Days") {
                inputSelectedOptions = $("#containerFieldRepeatYearlyMonthVsBD .containerFieldCalendarDays").find("input:checked");
            }
            break;
    }

    if (inputSelectedOptions.length == 0) {
        result = false;
    }

    return result;
}

function generateRecurrenceDatesInDB(recurrenceCode) {

    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Saving dates for recurrence...",
        name: "[dbo].[spCPRAFrwkRecurrenceAdmin]",
        params: [
            { "Name": "@Action", "Value": "Insert" },
            { "Name": "@Code", "Value": recurrenceCode },
            { "Name": "@Type", "Value": getRepeatTypeByCode(recurrenceCode) },
            { "Name": "@SessionSOEID", "Value": _getSOEID() },
        ],
        success: {
            fn: function (recurrenceList) {
                var recurrencesToSave = [];
                var objRecurrenceReady = {};

                //Check all recurrence that needs to generate dates
                for (var i = 0; i < recurrenceList.length; i++) {
                    if (needToGenerate(recurrenceList[i])) {
                        objRecurrenceReady[recurrenceList[i].ID] = false;
                    }
                }

                //Generate Dates
                for (var i = 0; i < recurrenceList.length; i++) {
                    var objRecurrence = recurrenceList[i];

                    //console.log(objRecurrence.Code + " Procesing...");

                    if (needToGenerate(objRecurrence)) {
                        generateRecurrenceDates({
                            idRecurrence: objRecurrence.ID,
                            recurrenceCode: objRecurrence.Code,
                            onSuccess: function (resultList, typeDaysOrBDs, idRecurrence, recurrenceCode) {

                                for (var j = 0; j < resultList.length; j++) {
                                    recurrencesToSave.push({
                                        IDR: idRecurrence,
                                        D: resultList[j].Date
                                    });
                                }
                                objRecurrenceReady[idRecurrence] = true;

                                //console.log(objRecurrence.Code + " Done!");

                                saveRecurrencesToDB();
                            }
                        });
                    }
                }

                //Save recurrences
                function saveRecurrencesToDB() {
                    var keys = Object.keys(objRecurrenceReady);
                    var isReadyToSave = true;
                    for (var i = 0; i < keys.length; i++) {
                        if (objRecurrenceReady[keys[i]] == false) {
                            isReadyToSave = false;
                            break;
                        }
                    }

                    //Save Data
                    if (isReadyToSave) {
                        //console.log(recurrencesToSave, "Saving data...");

                        _callServer({
                            loadingMsgType: "topBar",
                            loadingMsg: "Saving Recurrence Dates...",
                            url: '/CPR/UploadRecurrence',
                            data: {
                                'pjsonData': JSON.stringify(recurrencesToSave)
                            },
                            type: "post",
                            success: function (json) {
                                //Check if file exist
                                _showAlert({
                                    id: "SaveRecurrence",
                                    type: 'success',
                                    content: "Recurrense was saved and code was copied succesfully to clipboard.",
                                    animateScrollTop: true
                                });
                            }
                        });
                    }

                    //console.log(objRecurrenceReady);
                }

                //Check if recurrence needs to generate dates
                function needToGenerate(objRecurrence) {
                    var result = false;

                    if (!objRecurrence.MaxDate) {
                        result = true;
                    }

                    return result;
                }
            }
        }
    });

}

function generateRecurrenceDates(customOptions) {

    //Default Options.
    var options = {
        idRecurrence: 0,
        recurrenceCode: generateRecurrenceCode(),
        dateFormat: "ddd, MMM DD, YYYY",
        occurrencesToGenerate: 400,
        currentYear: moment().format("YYYY"),
        onSuccess: null
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    var repeatValue = getRepeatValueByCode(options.recurrenceCode);
    var momentStartDate = moment(options.currentYear + "-01-01").add(-1, 'days');
    var momentEndDate;
    
    if (maxDateCorporateCalendar) {
        momentEndDate = moment(maxDateCorporateCalendar);
    } else {
        momentEndDate = moment(options.currentYear + "-12-31");
    }
    
    var dateResultList = [];

    //Show selected value
    switch (repeatValue) {
        case "Daily":
            getDaily();
            break;

        case "Weekly":
            getWeekly();
            break;

        case "Monthly":
            getMonthly();
            break;

        case "Quarterly":
            getQuarterly();
            break;

        case "Yearly":
            getYearly();
            break;
    }

    function getDaily() {
        var repeatOption = getRepeatOptionByCode(options.recurrenceCode);
        var recurrence;
        var momentRecurrenceList;

        if (repeatOption) {
            if (repeatOption == "AllDays") {
                recurrence = moment(momentStartDate).recur(momentEndDate).every(1).day();
            }

            if (repeatOption == "Workdays") {
                recurrence = moment(momentStartDate).recur(momentEndDate).every(["Mo", "Tu", "We", "Th", "Fr"]).daysOfWeek();
            }

            momentRecurrenceList = recurrence.next(options.occurrencesToGenerate);
            for (var i = 0; i < momentRecurrenceList.length; i++) {
                addItem(momentRecurrenceList[i], "Days");
            }
        }

        //Return Result
        options.onSuccess(dateResultList, "Days", options.idRecurrence, options.recurrenceCode);
    }

    function getWeekly() {
        var selectedDayList = getRepeatOptionByCode(options.recurrenceCode);
        var recurrence;
        var momentRecurrenceList;

        if (selectedDayList.length > 0) {
            recurrence = moment(momentStartDate).recur(momentEndDate).every(selectedDayList).daysOfWeek();

            momentRecurrenceList = recurrence.next(options.occurrencesToGenerate);
            for (var i = 0; i < momentRecurrenceList.length; i++) {
                addItem(momentRecurrenceList[i], "Days");
            }
        }

        //Return Result
        options.onSuccess(dateResultList, "Days", options.idRecurrence, options.recurrenceCode);
    }

    function getMonthly() {
        var type = getRepeatTypeByCode(options.recurrenceCode);
        var selectedDayList = getRepeatOptionByCode(options.recurrenceCode);
        var inputDayList;
        var recurrence;
        var momentRecurrenceList;

        if (type == "BD") {
            if (selectedDayList.length > 0) {
                //Sql for validate GOCs
                var sqlValidateGOCs =
                    "SELECT \n" +
                    "    [Year], \n" +
                    "    [Month], \n" +
                    "    [BusinessDay], \n" +
                    "    CONVERT(VARCHAR(10), [WorkDay], 120) AS [WorkDay] \n" +
                    "FROM \n" +
                    "    [Automation].[dbo].[tblCorporateCalendar] \n" +
                    "WHERE \n" +
                    "    [Year] = " + options.currentYear + " AND \n" +
                    "    [BusinessDay] IN (" + selectedDayList.join(",") + ") ";

                //Get Business Days Result
                _callServer({
                    loadingMsgType: "topBar",
                    loadingMsg: "Getting Business Days...",
                    url: '/Ajax/ExecQuery',
                    data: { 'pjsonSql': _toJSON(sqlValidateGOCs) },
                    type: "POST",
                    success: function (resultList) {
                        for (var i = 0; i < resultList.length; i++) {
                            addItem(resultList[i], type);
                        }

                        //Return Result
                        options.onSuccess(dateResultList, "BD", options.idRecurrence, options.recurrenceCode);
                    }
                });

            } else {
                //Return Result
                options.onSuccess(dateResultList, "BD", options.idRecurrence, options.recurrenceCode);
            }
        }

        if (type == "Days") {
            if (selectedDayList.length > 0) {
                recurrence = moment(momentStartDate).recur(momentEndDate).every(selectedDayList).daysOfMonth();

                momentRecurrenceList = recurrence.next(options.occurrencesToGenerate);
                for (var i = 0; i < momentRecurrenceList.length; i++) {
                    addItem(momentRecurrenceList[i], type);
                }
            }

            //Return Result
            options.onSuccess(dateResultList, "Days", options.idRecurrence, options.recurrenceCode);
        }
    }

    function getQuarterly() {
        var type = getRepeatTypeByCode(options.recurrenceCode);
        var selectedMonthList = [];
        var selectedDayList = [];
        var recurrence;
        var momentRecurrenceList;

        //Get Months
        //----------------------------------------------------------
        //1st Month of Quarter (Jan, Apr, Jul, Oct)
        if (_contains(options.recurrenceCode, "1stM")) {
            selectedMonthList.push(1);
            selectedMonthList.push(4);
            selectedMonthList.push(7);
            selectedMonthList.push(10);
        }

        //2nd Month of Quarter (Feb, May, Aug, Nov)
        if (_contains(options.recurrenceCode, "2stM")) {
            selectedMonthList.push(2);
            selectedMonthList.push(5);
            selectedMonthList.push(8);
            selectedMonthList.push(11);
        }

        //3rd Month of Quarter (Mar, Jun, Sep, Dec)
        if (_contains(options.recurrenceCode, "3stM")) {
            selectedMonthList.push(3);
            selectedMonthList.push(6);
            selectedMonthList.push(9);
            selectedMonthList.push(12);
        }

        if (selectedMonthList.length > 0) {

            if (type == "BD") {
                selectedDayList = getRepeatOptionByCode(options.recurrenceCode);

                if (selectedDayList.length > 0) {
                    //Sql for validate GOCs
                    var sqlValidateGOCs =
                        "SELECT \n" +
                        "    [Year], \n" +
                        "    [Month], \n" +
                        "    [BusinessDay], \n" +
                        "    CONVERT(VARCHAR(10), [WorkDay], 120) AS [WorkDay] \n" +
                        "FROM \n" +
                        "    [Automation].[dbo].[tblCorporateCalendar] \n" +
                        "WHERE \n" +
                        "    [Year] = " + options.currentYear + " AND \n" +
                        "    [Month] IN (" + selectedMonthList.join(",") + ") AND \n" +
                        "    [BusinessDay] IN (" + selectedDayList.join(",") + ") ";

                    //Get Business Days Result
                    _callServer({
                        loadingMsgType: "topBar",
                        loadingMsg: "Getting Business Days...",
                        url: '/Ajax/ExecQuery',
                        data: { 'pjsonSql': _toJSON(sqlValidateGOCs) },
                        type: "POST",
                        success: function (resultList) {
                            for (var i = 0; i < resultList.length; i++) {
                                addItem(resultList[i], type);
                            }

                            //Return Result
                            options.onSuccess(dateResultList, "BD", options.idRecurrence, options.recurrenceCode);
                        }
                    });

                } else {
                    //Return Result
                    options.onSuccess(dateResultList, "BD", options.idRecurrence, options.recurrenceCode);
                }
            }

            if (type == "Days") {
                selectedDayList = getRepeatOptionByCode(options.recurrenceCode);

                if (selectedDayList.length > 0) {
                    //Fix January = 1 but Moment January = 0
                    var customSelectedMonthList = [];
                    for (var i = 0; i < selectedMonthList.length; i++) {
                        customSelectedMonthList.push(selectedMonthList[i] - 1);
                    }

                    recurrence = moment(momentStartDate).recur(momentEndDate)
                                    .every(selectedDayList).daysOfMonth()
                                    .every(customSelectedMonthList).monthsOfYear();

                    momentRecurrenceList = recurrence.next(options.occurrencesToGenerate);
                    for (var i = 0; i < momentRecurrenceList.length; i++) {
                        addItem(momentRecurrenceList[i], type);
                    }
                }

                //Return Result
                options.onSuccess(dateResultList, "Days", options.idRecurrence, options.recurrenceCode);
            }

        } else {
            //Return Result
            options.onSuccess(dateResultList, "Days", options.idRecurrence, options.recurrenceCode);
        }
        
    }

    function getYearly() {
        var type = getRepeatTypeByCode(options.recurrenceCode);
        var selectedMonthList = [];
        var selectedDayList = [];
        var recurrence;
        var momentRecurrenceList;

        //moment("12-25-1995", "MM-DD-YYYY");
        //Set Month of Year
        var strYearlyCode = _replaceAll(repeatValue, "", options.recurrenceCode);
        var selectedMonthOfYear = strYearlyCode.substr(0, 3);
        var objSelMonthMoment = moment(options.currentYear + "-" + selectedMonthOfYear + "-01", "YYYY-MMM-DD");
        selectedMonthList.push(objSelMonthMoment.format("M"));

        if (type == "BD") {
            selectedDayList = getRepeatOptionByCode(options.recurrenceCode);

            if (selectedDayList.length > 0) {
                //Sql for validate GOCs
                var sqlValidateGOCs =
                    "SELECT \n" +
                    "    [Year], \n" +
                    "    [Month], \n" +
                    "    [BusinessDay], \n" +
                    "    CONVERT(VARCHAR(10), [WorkDay], 120) AS [WorkDay] \n" +
                    "FROM \n" +
                    "    [Automation].[dbo].[tblCorporateCalendar] \n" +
                    "WHERE \n" +
                    "    [Year] = " + options.currentYear + " AND \n" +
                    "    [Month] IN (" + selectedMonthList.join(",") + ") AND \n" +
                    "    [BusinessDay] IN (" + selectedDayList.join(",") + ") ";

                //Get Business Days Result
                _callServer({
                    loadingMsgType: "topBar",
                    loadingMsg: "Getting Business Days...",
                    url: '/Ajax/ExecQuery',
                    data: { 'pjsonSql': _toJSON(sqlValidateGOCs) },
                    type: "POST",
                    success: function (resultList) {
                        for (var i = 0; i < resultList.length; i++) {
                            addItem(resultList[i], type);
                        }

                        //Return Result
                        options.onSuccess(dateResultList, "BD", options.idRecurrence, options.recurrenceCode);
                    }
                });
            } else {
                //Return Result
                options.onSuccess(dateResultList, "BD", options.idRecurrence, options.recurrenceCode);
            }
        }

        if (type == "Days") {
            selectedDayList = getRepeatOptionByCode(options.recurrenceCode);

            if (selectedDayList.length > 0) {
                //Fix January = 1 but Moment January = 0
                var customSelectedMonthList = [];
                for (var i = 0; i < selectedMonthList.length; i++) {
                    customSelectedMonthList.push(selectedMonthList[i] - 1);
                }

                recurrence = moment(momentStartDate).recur(momentEndDate)
                                .every(selectedDayList).daysOfMonth()
                                .every(customSelectedMonthList).monthsOfYear();

                momentRecurrenceList = recurrence.next(options.occurrencesToGenerate);
                for (var i = 0; i < momentRecurrenceList.length; i++) {
                    addItem(momentRecurrenceList[i], type);
                }
            }

            //Return Result
            options.onSuccess(dateResultList, "Days", options.idRecurrence, options.recurrenceCode);
        }
    }

    function addItem(objRecurrenceDate, type) {
        if (type == "Days") {
            var objDateMomment = objRecurrenceDate;
            dateResultList.push({
                Year: objDateMomment.format("YYYY"),
                MonthNumber: objDateMomment.format("M"),
                MonthName: objDateMomment.format("MMMM"),
                MonthCustomFormat: objDateMomment.format("MM - MMMM"),
                ObjDate: new Date(objDateMomment.year(), objDateMomment.month(), objDateMomment.date()),
                Date: objDateMomment.format("YYYY-MM-DD"),
                DateFormat: objDateMomment.format(options.dateFormat)
            });
        }

        if (type == "BD") {
            var objBD = objRecurrenceDate;
            var objDBMoment = moment(objBD.WorkDay);
            var objBDMontMoment = moment(objBD.Year + "-" + objBD.Month + "-01", "YYYY-M-DD");

            dateResultList.push({
                BDYear: objBD.Year,
                BDMonthNumber: objBD.Month,
                BDMonthName: objBDMontMoment.format("MMMM"),
                BDMonthCustomFormat: objBDMontMoment.format("MM - MMMM"),
                ObjDate: objDBMoment.toDate(),
                Date: objBD.WorkDay,
                DateFormat: objDBMoment.format(options.dateFormat),
                BD: "BD" + objBD.BusinessDay
            });
        }
    }
}

function createFieldCalendarBusinessDays($element) {
    var uniqueId = _createCustomID();
    var pluginHtml =
    '<div class="form-group row"> ' +
    '    <div class="col-md-12"> ' +
    '        <div class="form-control fieldCalendarDBsType"> ' +
    '            <ul class="list-unstyled"> ' +
    '                <li class="col-md-6"> ' +
    '                    <input type="radio" value="BD" name="chkMonthDaysBDsType_' + uniqueId + '" class="skin-flat-green" checked> ' +
    '                    <label class="icheck-label form-label">Business Days</label> ' +
    '                </li> ' +
    '                <li class="col-md-6"> ' +
    '                    <input type="radio" value="Days" name="chkMonthDaysBDsType_' + uniqueId + '" class="skin-flat-green" > ' +
    '                    <label class="icheck-label form-label">Calendar Days</label> ' +
    '                </li> ' +
    '                <div style="clear:both;"></div> ' +
    '            </ul> ' +
    '        </div> ' +
    '    </div> ' +
    '</div> ' +
    
    '<div class="form-group row containerFieldBusinessDays"> ' +
    '    <div class="col-md-12"> ' +
    '        <div class="form-control fieldBusinessDays"> ' +
    '            <ul class="list-unstyled"></ul> ' +
    '        </div> ' +
    '    </div> ' +
    '</div> ' +

    '<div class="form-group row containerFieldCalendarDays" style="display:none;"> ' +
    '    <div class="col-md-12"> ' +
    '        <div class="form-control fieldCalendarDays"> ' +
    '            <ul class="list-unstyled"></ul> ' +
    '        </div> ' +
    '    </div> ' +
    '</div> ';

    $element.append($(pluginHtml));

    //On Change Business Day / Calendar Days
    var $fieldCalendarBDsType = $element.find(".fieldCalendarDBsType");
    var $containerFieldBusinessDays = $element.find(".containerFieldBusinessDays");
    var $containerFieldCalendarDays = $element.find(".containerFieldCalendarDays");
    $fieldCalendarBDsType.find("input").on('ifChecked', function () {
        var fieldValue = $fieldCalendarBDsType.find("input:checked").val();

        //Hide all
        $containerFieldBusinessDays.hide();
        $containerFieldCalendarDays.hide();
        
        //Show selected value
        switch (fieldValue) {
            case "BD":
                $containerFieldBusinessDays.show();
                break;

            case "Days":
                $containerFieldCalendarDays.show();
                break;
        }
    });

    //Generate Business Days Checkboxs
    var $fieldBusinessDays = $element.find(".fieldBusinessDays ul");
    generateBusinessDays($fieldBusinessDays);

    //Generate Calendar Days Checkboxs
    var $fieldCalendarDays = $element.find(".fieldCalendarDays ul");
    generateCalendarDays($fieldCalendarDays);
}

function generateBusinessDays($element) {

    for (var i = 0; i < businessDayList.length; i++) {
        var tempBD = businessDayList[i];
        var $li = $(
            '<li class="col-md-4"> ' +
            '   <input type="checkbox" name="chkBusinessDays" value="' + _replaceAll("BD", "", tempBD) + '" class="skin-flat-green"> ' +
            '   <label class="icheck-label form-label">' + tempBD + '</label> ' +
            '</li>'
        );

        $element.append($li);
    }

    $element.append($('<div style="clear:both;"></div>'));
}

function generateCalendarDays($element) {

    for (var i = 0; i < monthDayList.length; i++) {
        var tempDay = monthDayList[i];
        var $li = $(
            '<li class="col-md-3"> ' +
            '   <input type="checkbox" name="chkCalendarDays" value="' + tempDay + '" class="skin-flat-green"> ' +
            '   <label class="icheck-label form-label" >' + tempDay + '</label> ' +
            '</li>'
        );

        $element.append($li);
    }

    $element.append($('<div style="clear:both;"></div>'));
}

function generateMonths(pselector) {
    var $select = $(pselector);

    //$(idSelect).find("option").remove();
    //$select.append($('<option>', {
    //    value: "0",
    //    text: "-- Select a User --"
    //}));

    //Load options in select
    for (var i = 0; i < monthList.length; i++) {
        var monthName = monthList[i];
        
        $select.append($('<option>', {
            value: monthName.substr(0, 3),
            text: monthName
        }));
    }
}

function getRepeatValueByCode(pcode) {
    var repeatValue = "";

    // Calculate Repeat Value
    if (_contains(pcode, "Daily")) {
        repeatValue = "Daily";
    }

    if (_contains(pcode, "Weekly")) {
        repeatValue = "Weekly";
    }

    if (_contains(pcode, "Monthly")) {
        repeatValue = "Monthly";
    }

    if (_contains(pcode, "Quarterly")) {
        repeatValue = "Quarterly";
    }

    if (_contains(pcode, "Yearly")) {
        repeatValue = "Yearly";
    }

    return repeatValue;
}

function getRepeatOptionByCode(pcode) {
    var repeatValue = getRepeatValueByCode(pcode);
    var repeatOption;

    //Show selected value
    switch (repeatValue) {
        case "Daily":
            if (_contains(pcode, "AllDays")) {
                repeatOption = "AllDays";
            }

            if (_contains(pcode, "Workdays")) {
                repeatOption = "Workdays";
            }
            break;

        case "Weekly":
            var strWeekDays = _replaceAll(repeatValue, "", pcode);
            repeatOption = strWeekDays.match(/.{1,2}/g);
            break;

        case "Monthly":
            var typeMonthly = getRepeatTypeByCode(pcode);
            if (typeMonthly) {
                var strMonthlyOptions = _replaceAll(repeatValue, "", pcode);
                strMonthlyOptions = _replaceAll((typeMonthly + "."), "", strMonthlyOptions);
                strMonthlyOptions = _replaceAll(typeMonthly, "", strMonthlyOptions);
                if (strMonthlyOptions) {
                    repeatOption = strMonthlyOptions.split(".");
                } else {
                    repeatOption = [];
                }
            }
            break;

        case "Quarterly":
            var typeQuarterly = getRepeatTypeByCode(pcode);
            if (typeQuarterly) {
                var strQuarterlyOptions = _replaceAll(repeatValue, "", pcode);
                strQuarterlyOptions = _replaceAll("1stM:", "", strQuarterlyOptions);
                strQuarterlyOptions = _replaceAll("2stM:", "", strQuarterlyOptions);
                strQuarterlyOptions = _replaceAll("3stM:", "", strQuarterlyOptions);
                strQuarterlyOptions = _replaceAll((typeQuarterly + "."), "", strQuarterlyOptions);
                strQuarterlyOptions = _replaceAll(typeQuarterly, "", strQuarterlyOptions);
                if (strQuarterlyOptions) {
                    repeatOption = strQuarterlyOptions.split(".");
                } else {
                    repeatOption = [];
                }
            }
            break;

        case "Yearly":
            var typeYearly = getRepeatTypeByCode(pcode);
            if (typeYearly) {
                var strYearlyOptions = _replaceAll(repeatValue, "", pcode);
                strYearlyOptions = _replaceAll(strYearlyOptions.substr(0, 4), "", strYearlyOptions);
                strYearlyOptions = _replaceAll((typeYearly + "."), "", strYearlyOptions);
                strYearlyOptions = _replaceAll(typeYearly, "", strYearlyOptions);
                if (strYearlyOptions) {
                    repeatOption = strYearlyOptions.split(".");
                } else {
                    repeatOption = [];
                }
            }
            break;
    }

    return repeatOption;
}

function getRepeatTypeByCode(pcode) {
    var repeatValue = getRepeatValueByCode(pcode);
    var type = "Days";
    var strOptions = _replaceAll(repeatValue, "", pcode);

    if (repeatValue == "Quarterly") {
        strOptions = _replaceAll("1stM:", "", strOptions);
        strOptions = _replaceAll("2stM:", "", strOptions);
        strOptions = _replaceAll("3stM:", "", strOptions);
    }

    if (repeatValue == "Yearly") {
        //Remove Jan:, Feb:, May:..
        strOptions = _replaceAll(strOptions.substr(0, 4), "", strOptions);
    }

    if (strOptions.substr(0, 2) == "BD") {
        type = "BD";
    }

    return type;
}

function getRepeat() {
    return $("#fieldRepeat").find("input:checked").val();
}

function getRepeatOption() {
    var repeatValue = getRepeat();
    var repeatOption = "";

    //Show selected value
    switch (repeatValue) {
        case "Daily":
            repeatOption = $("#fieldRepeatDaily").find("input:checked").val();
            break;

        case "Weekly":
            var inputDayList = $("#fieldRepeatWeekly").find("input:checked");

            for (var i = 0; i < inputDayList.length; i++) {
                repeatOption += inputDayList[i].value;

                //if ((i + 1) == inputDayList.length) {
                //    repeatOption += inputDayList[i].value;
                //} else {
                //    repeatOption += inputDayList[i].value + ".";
                //}
            }

            break;

        case "Monthly":
            var type = $("#containerRepeatMonthly .fieldCalendarDBsType").find("input:checked").val();
            if (type) {
                var inputDayList;

                if (type == "BD") {
                    inputDayList = $("#containerRepeatMonthly .containerFieldBusinessDays").find("input:checked");
                }

                if (type == "Days") {
                    inputDayList = $("#containerRepeatMonthly .containerFieldCalendarDays").find("input:checked");
                }

                repeatOption += type;
                for (var i = 0; i < inputDayList.length; i++) {
                    repeatOption += ("." + inputDayList[i].value);

                    //if ((i + 1) == inputDayList.length) {
                    //    repeatOption += inputDayList[i].value;
                    //} else {
                    //    repeatOption += inputDayList[i].value + ".";
                    //}
                }
            }
            break;

        case "Quarterly":
            var inputQuarterMonthsList = $("#fieldRepeatQuarterlyMonths").find("input:checked");
            for (var i = 0; i < inputQuarterMonthsList.length; i++) {
                repeatOption += (inputQuarterMonthsList[i].value + ":");
            }

            var type = $("#containerFieldRepeatQuarterlyMonthVsBD .fieldCalendarDBsType").find("input:checked").val();
            if (type) {
                var inputDayList;

                if (type == "BD") {
                    inputDayList = $("#containerFieldRepeatQuarterlyMonthVsBD .containerFieldBusinessDays").find("input:checked");
                }

                if (type == "Days") {
                    inputDayList = $("#containerFieldRepeatQuarterlyMonthVsBD .containerFieldCalendarDays").find("input:checked");
                }

                repeatOption += type;
                for (var i = 0; i < inputDayList.length; i++) {
                    repeatOption += ("." + inputDayList[i].value);

                    //if ((i + 1) == inputDayList.length) {
                    //    repeatOption += inputDayList[i].value;
                    //} else {
                    //    repeatOption += inputDayList[i].value + ".";
                    //}
                }
            }
            break;

        case "Yearly":
            repeatOption += $("#fieldRepeatYearlySelectMonth").val() + ":";

            var type = $("#containerFieldRepeatYearlyMonthVsBD .fieldCalendarDBsType").find("input:checked").val();
            if (type) {
                var inputDayList;

                if (type == "BD") {
                    inputDayList = $("#containerFieldRepeatYearlyMonthVsBD .containerFieldBusinessDays").find("input:checked");
                }

                if (type == "Days") {
                    inputDayList = $("#containerFieldRepeatYearlyMonthVsBD .containerFieldCalendarDays").find("input:checked");
                }

                repeatOption += type;
                for (var i = 0; i < inputDayList.length; i++) {
                    repeatOption += ("." + inputDayList[i].value);

                    //if ((i + 1) == inputDayList.length) {
                    //    repeatOption += inputDayList[i].value;
                    //} else {
                    //    repeatOption += inputDayList[i].value + ".";
                    //}
                }
            }
            break;
    }

    return repeatOption;
}

function getRepeatEndDate() {
    return $("#fieldRepeatEnd").find("input:checked").val();
}

function loadMaxDateCorporateCalendar() {
    //Sql for validate GOCs
    var sql =
        "SELECT CONVERT(VARCHAR(10), MAX([WorkDay]), 120)  AS [MaxDate] \n" +
        "FROM [Automation].[dbo].[tblCorporateCalendar] ";

    //Get Business Days Result
    _callServer({
        loadingMsgType: "topBar",
        loadingMsg: "Getting Max Date Corporate Calendar...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': _toJSON(sql) },
        type: "POST",
        success: function (resultList) {
            maxDateCorporateCalendar = resultList[0].MaxDate;
        }
    });
}

function refreshData() {
    // Set Recurrence Code
    $("#lblRecurrenceCode").html(generateRecurrenceCode());

    // Reload Recurrence Preview
    loadRecurrencePreview();
}

function loadRecurrencePreview() {
    generateRecurrenceDates({
        onSuccess: function (resultList, typeDaysOrBDs) {
            var columns = [];
            if (typeDaysOrBDs == "Days") {
                columns.push({ name: 'Year', text: 'Year', width: '25%', type: 'string', filtertype: 'checkedlist' });
                columns.push({ name: 'MonthCustomFormat', text: 'Month', width: '25%', type: 'string', filtertype: 'checkedlist' });
                columns.push({ name: 'Date', text: 'Short Date', width: '15%', type: 'date', filtertype: 'date', cellsformat: 'yyyy-MM-dd' });
                columns.push({ name: 'ObjDate', text: 'Long Date', width: '35%', type: 'date', filtertype: 'date', cellsformat: 'dddd, MMMM dd, yyyy' });
            }

            if (typeDaysOrBDs == "BD") {
                columns.push({ name: 'BDYear', text: 'Year', width: '20%', type: 'string', filtertype: 'checkedlist' });
                columns.push({ name: 'BDMonthCustomFormat', text: 'Month', width: '20%', type: 'string', filtertype: 'checkedlist' });
                columns.push({ name: 'BD', text: 'BD', width: '15%', type: 'string', filtertype: 'checkedlist' });
                columns.push({ name: 'Date', text: 'Short Date', width: '15%', type: 'date', filtertype: 'date', cellsformat: 'yyyy-MM-dd' });
                columns.push({ name: 'ObjDate', text: 'Long Date', width: '30%', type: 'date', filtertype: 'date', cellsformat: 'dddd, MMMM dd, yyyy' });
            }
            
            $.jqxGridApi.create({
                showTo: "#tblPreviewRecurrence",
                options: {
                    //for comments or descriptions
                    height: "450",
                    autoheight: false,
                    autorowheight: false,
                    showfilterrow: true,
                    sortable: true,
                    editable: true,
                    selectionmode: "singlerow"
                },
                source: {
                    // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                    dataBinding: "Large Data Set Local",
                    rows: resultList
                },
                groups: [],
                columns: columns,
                ready: function () { }
            });
        }
    });
}

function generateRecurrenceCode() {
    return getRepeat() + getRepeatOption();
}

function generateRecurrenceCode2() {
    var uniqueId = _createCustomID(true);
    var part1 = "";

    var id2 = _getSOEID();
    var part2 = id2.substring(0, 2);

    var selRecurrence = getRepeat();

    switch (selRecurrence) {
        case "Daily":
            part1 = uniqueId.substr(uniqueId.length - 8);
            break;

        case "Weekly":
            part1 = uniqueId.substr(uniqueId.length - 7);
            break;

        case "Monthly":
            part1 = uniqueId.substr(uniqueId.length - 6);
            break;

        case "Quarterly":
            part1 = uniqueId.substr(uniqueId.length - 4);
            break;

        case "Yearly":
            part1 = uniqueId.substr(uniqueId.length - 7);
            break;
    }

    return "R" + getRepeat().toUpperCase() + part1 + part2.toUpperCase();
}