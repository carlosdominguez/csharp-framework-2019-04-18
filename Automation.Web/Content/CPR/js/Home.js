$(document).ready(function () {
    // Create MCA Popup
    //_createMCASummaryLink({
    //    idContainer: "MCASelect",
    //    idLinkSummary: "linkMCA-" + _createCustomID(),
    //    imgMCAInfoPath: _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png',
    //    idMCAList: [431]
    //});

    //Show Communication Sub Menu
    _execOnAjaxComplete(function () {
        var $aComunication = $("#user-menu").find(".menu-parent");
        var $liCommunication = $aComunication.parent();
        $liCommunication.addClass("open");
        $liCommunication.find("ul").css("display", "block");
        $liCommunication.find(".arrow").addClass("open");
        $aComunication.css("background-color", "white");
    });
});
