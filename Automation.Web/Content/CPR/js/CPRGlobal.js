/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="../../Shared/plugins/citi-process-tree/process-tree-v1.0.0.js" />

$(document).ready(function () {
    _execOnAjaxComplete(function () {
        var tutorial = _getQueryStringParameter("ptutorial");
        switch (tutorial) {
            case "SendRealEventPart2":
                _loadTutorialSendRealEventPart2();
                break;

            case "SendAttestationEventPart2":
                _loadTutorialSendAttestationEventPart2();
                break;

            case "UploadCPRTemplatePart2":
                _loadTutorialUploadCPRTemplatePart2();
                break;

            case "ReviewActivitiesPart2":
                _loadTutorialReviewActivitiesPart2();
                break;
        }
    }); 
});

// Get Global Filters for Activities
function _getDefaultFilters() {
    return {
        GPLSOEID: "",
        RPLSOEID: "",
        employeeSOEID: "",
        idCommunication: "",
        communicationEmployeeSOEID: "",
        idProcessTaxonomy: "",
        idLocation: "",
        startDate: "",
        endDate: "",
        recoverableWAD: true
    };
}

// Get Global Columns Information to Select
function _getDefaultColsToSelect(){
    return {
        IDProcessTaxonomy:      { Show: true, Alias: 'A' },
        ProcessTaxonomyDesc:    { Show: true, Alias: 'B' },
        ProcessTaxonomyLevel:   { Show: true, Alias: 'C' },
        ManagedSegment:         { Show: true, Alias: 'D' },
        ManagedSegmentDesc:     { Show: true, Alias: 'E' },
        GOC:                    { Show: true, Alias: 'F' },
        GOCDesc:                { Show: true, Alias: 'G' },
        PrimaryMakerSOEID:      { Show: true, Alias: 'H' },
        PrimaryMakerName:       { Show: true, Alias: 'I' },
        PrimaryCheckerSOEID:    { Show: true, Alias: 'J' },
        PrimaryCheckerName:     { Show: true, Alias: 'K' },
        ActivityName:           { Show: true, Alias: 'L' },
        AuditStatus:            { Show: true, Alias: 'M' },
        JsonAuditLog:           { Show: true, Alias: 'N' }
    };
}

// Load Activities Information
function _loadProcessActivityTable(customOptions) {
    //Default Options.
    var options = {
        idTable: "#tblProcessActivities",
        filters: _getDefaultFilters(),
        onReady: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    _getDBListMergeActivities({
        filters: options.filters,
        onReady: function (processAcitivityList, cols) {
            var columns = [];

            // Add Hidden Columns
            columns.push({ name: 'ID', type: 'number', hidden: true });
            columns.push({ name: cols.IDProcessTaxonomy.Alias, type: 'number', hidden: true });
            columns.push({ name: cols.ProcessTaxonomyLevel.Alias, type: 'string', hidden: true });
            if (cols.JsonAuditLog.Show) {
                columns.push({ name: cols.JsonAuditLog.Alias, type: 'string', hidden: true });
            }

            // Add Source Column "eDCFC" or "CPR"
            var sourceWidth = "80px";
            if (cols.JsonAuditLog.Show) {
                sourceWidth = "110px";
            }
            columns.push({
                name: 'SR', text: 'Source', width: sourceWidth, type: 'string', pinned: true, filtertype: 'checkedlist', editable: false, filterable: true, cellsalign: 'center', align: 'center',
                cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                    var htmlResult = '';

                    //Add Status
                    switch (rowData.SR) {
                        case "CPR":
                            htmlResult += '<span class="badge badge-md badge-warning" style="margin-top: 5px; margin-left: 5px;"> CPR </span>';
                            break;

                        case "eDCFC":
                            htmlResult += '<span class="badge badge-md badge-success" style="margin-top: 5px; margin-left: 5px;"> eDCFC </span>';
                            break;
                    }

                    //Add History of comments
                    if (cols.JsonAuditLog.Show) {
                        var historyRows = JSON.parse(rowData[cols.JsonAuditLog.Alias]);
                        if (historyRows.length > 0) {
                            var historyInfoHtml = "<div style='padding: 10px; font-size: 15px; max-height: 500px; overflow-y:scroll; width: 520px;'>";

                            for (var i = 0; i < historyRows.length; i++) {
                                var tempData = historyRows[i];

                                historyInfoHtml += "<b style='color:#4caf50;'>" + tempData.Type + "</b><br>";

                                historyInfoHtml += "<b>User:</b> " + tempData.CreatedByName + " [" + tempData.CreatedBySOEID + "]<br>";
                                if (tempData.RoleInActivity) {
                                    historyInfoHtml += "<b>Role:</b> " + tempData.RoleInActivity + "<br>";
                                }
                                historyInfoHtml += "<b>Date:</b> " + tempData.CreatedDate + "<br>";
                                if (tempData.Status) {
                                    historyInfoHtml += "<b>Status:</b> " + tempData.Status + "<br>";
                                }
                                historyInfoHtml += "<b>Comment:</b> <pre style='margin: 0 0 0px;'>" + _escapeToHTML(tempData.Comment) + "</pre>";

                                historyInfoHtml += "<br>";
                            }

                            historyInfoHtml += "</div>";

                            htmlResult +=
                                '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="right" data-content="' + historyInfoHtml + '" data-title="History" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png"/> ' +
                                '</div>';
                        }
                    }

                    return htmlResult;
                }
            });
            
            // Add Recover Status "Completed" "Missing Information" etc...
            columns.push({
                name: cols.AuditStatus.Alias, text: 'WAD Status', columntype: 'dropdownlist', type: 'string', filtertype: 'checkedlist', width: '125px',
                cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                    var htmlResult = '';

                    //Add Status
                    switch (rowData[cols.AuditStatus.Alias]) {
                        case "Approved":
                            htmlResult += '<span class="badge badge-md badge-success" style="margin-top: 5px; margin-left: 5px;"> Approved </span>';
                            break;

                        case "Completed":
                            htmlResult += '<span class="badge badge-md badge-success" style="margin-top: 5px; margin-left: 5px;"> Completed </span>';
                            break;

                        case "Missing Information":
                            htmlResult += '<span class="badge badge-md badge-danger" style="margin-top: 5px; margin-left: 5px;"> Missing Information </span>';
                            break;

                        case "Access Issue":
                            htmlResult += '<span class="badge badge-md badge-danger" style="margin-top: 5px; margin-left: 5px;"> Access Issue </span>';
                            break;

                        case "Not my process":
                            htmlResult += '<span class="badge badge-md badge-danger" style="margin-top: 5px; margin-left: 5px;"> It\'s not my process </span>';
                            break;

                        case "Other Issues":
                            htmlResult += '<span class="badge badge-md badge-danger" style="margin-top: 5px; margin-left: 5px;"> Other Issues </span>';
                            break;

                        case "Working":
                            htmlResult += '<span class="badge badge-md badge-warning" style="margin-top: 5px; margin-left: 5px;"> Working </span>';
                            break;

                        case "Pending":
                            htmlResult += '<span class="badge badge-md badge-warning" style="margin-top: 5px; margin-left: 5px;"> Pending </span>';
                            break;
                    }

                    return htmlResult;
                }
            });
            
            // Show Activity Name
            if (cols.ActivityName.Show) {
                columns.push({ name: cols.ActivityName.Alias, text: 'Activity Name', width: '600px', type: 'string', filtertype: 'input' });
            }

            // Add Process Taxonomy Column
            columns.push({
                name: cols.ProcessTaxonomyDesc.Alias, text: 'Process Tree', width: '200px', type: 'html', filtertype: 'checkedlist',
                cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                    var levelText = rowData[cols.ProcessTaxonomyLevel.Alias];
                    var resultHTML = '';

                    if (levelText) {
                        var levelHtml;

                        switch (levelText) {
                            case 'L2':
                                levelHtml = '<span class="badge badge-info">' + levelText + '</span>';
                                break;
                            case 'L3':
                                levelHtml = '<span class="badge badge-purple">' + levelText + '</span>';
                                break;
                            case 'L4':
                                levelHtml = '<span class="badge badge-accent">' + levelText + '</span>';
                                break;
                            case 'L5':
                                levelHtml = '<span class="badge badge-primary">' + levelText + '</span>';
                                break;
                            case 'L6':
                                levelHtml = '<span class="badge badge-warning">' + levelText + '</span>';
                                break;
                            default:
                                levelHtml = levelText;
                                break;
                        }

                        resultHTML =
                            ' <span style="line-height: 28px; padding-left: 2px;" title="' + rowData[cols.ProcessTaxonomyDesc.Alias] + '" > ' + levelHtml + ' ' + rowData[cols.ProcessTaxonomyDesc.Alias] + ' </span>';

                    } else {
                        resultHTML = cellHtml;
                    }

                    return resultHTML;
                }
            });

            if(cols.ManagedSegment.Show){
                columns.push({ name: cols.ManagedSegment.Alias, text: 'MS', width: '70px', type: 'string', filtertype: 'checkedlist' });
            }

            if(cols.ManagedSegmentDesc.Show){
                columns.push({ name: cols.ManagedSegmentDesc.Alias, text: 'MS Desc', width: '200px', type: 'string', filtertype: 'checkedlist' });
            }

            if(cols.GOC.Show){
                columns.push({ name: cols.GOC.Alias, text: 'GOC', width: '120px', type: 'string', filtertype: 'checkedlist' });
            }

            if(cols.GOCDesc.Show){
                columns.push({ name: cols.GOCDesc.Alias, text: 'GOC Desc', width: '300px', type: 'string', filtertype: 'checkedlist' });
            }

            if(cols.PrimaryMakerSOEID.Show){
                columns.push({ name: cols.PrimaryMakerSOEID.Alias, text: 'Maker SOEID', width: '120px', type: 'string', filtertype: 'input' });
            }

            if(cols.PrimaryMakerName.Show){
                columns.push({ name: cols.PrimaryMakerName.Alias, text: 'Maker Name', width: '250px', type: 'string', filtertype: 'checkedlist' });
            }

            if (cols.PrimaryCheckerSOEID.Show) {
                columns.push({ name: cols.PrimaryCheckerSOEID.Alias, text: 'Checker SOEID', width: '120px', type: 'string', filtertype: 'input' });
            }

            if (cols.PrimaryCheckerName.Show) {
                columns.push({ name: cols.PrimaryCheckerName.Alias, text: 'Checker Name', width: '250px', type: 'string', filtertype: 'checkedlist' });
            }
                
            //Create table
            $.jqxGridApi.create({
                showTo: options.idTable,
                options: {
                    //for comments or descriptions
                    height: (($(window).height() < 700) ? "480" : "700"),
                    autoheight: false,
                    autorowheight: false,
                    showfilterrow: true,
                    sortable: true,
                    editable: true,
                    selectionmode: "singlerow"
                },
                source: {
                    // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                    dataBinding: "Large Data Set Local",
                    rows: processAcitivityList
                },
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                columns: columns,
                ready: function () {
                    $(options.idTable).on("rowclick", function (event) {
                        //Show tooltips
                        _GLOBAL_SETTINGS.tooltipsPopovers();
                    });

                    if (options.onReady) {
                        options.onReady(processAcitivityList);
                    }
                }
            });
        }
    });
}

// Show With Process Activity Information
function _openModalProcessActivityInfo(customOptions) {
    //Default Options.
    var options = {
        filters: {
            source: "eDCFC",
            idProcessActivity: ""
        },
        onReady: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    //Container HTML
    var objProcessTreePlugin = null;

    //Get Activity Info from DB
    _getBDObjProcessActivityInfo({
        filters: options.filters,
        onReady: function (objActivityInfo) {
            generateModal(objActivityInfo);
        }
    });

    function getUserIcon(ptype) {
        var result = "fa-user";

        if (ptype == "Group") {
            result = "fa-group";
        }

        return result;
    }

    function getDocumentList(pdocList){
        var htmlResult = '<ul class="list-group"> ';
        
        if(pdocList.length > 0){
            for (var i = 0; i < pdocList.length; i++) {
                var objDoc = pdocList[i];

                htmlResult +=
                    '<li class="list-group-item"> ' +
                    '    <div class="col-md-7"> ' +
                    '        <a target="_blank" href="' + objDoc.URL + '">' + objDoc.URL + '</a>' +
                    '    </div> ' +
                    '    <div class="col-md-5"> ' +
                    '        <span>By ' + objDoc.CreatedByName + ' [' + objDoc.CreatedBySOEID + '] on ' + objDoc.CreatedDate + '</span> ' +
                    '    </div> ' +
                    '    <div style="clear:both;"></div> ' +
                    '</li> ';
            }
        }else{
            htmlResult += '<li class="list-group-item"> No documents found </li> ';
        }

        htmlResult += '</ul>';

        return htmlResult;
    }

    function getUserListPopupHtml(puserList) {
        var htmlResult = "";
        var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

        for (var i = 0; i < puserList.length; i++) {
            var objTempUser = puserList[i];
            infoHtml += " " + objTempUser.Name + " [" + objTempUser.SOEID + "]<br>";
        }

        infoHtml += "</div>";
        htmlResult +=
            '<button type="button" class="btn btn-default" aria-label="Help" rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="top" data-content="' + infoHtml + '" data-title="Users" data-trigger="click" data-html="true" data-original-title="" title="" > ' +
            '   <span class="fa fa-eye"></span>' +
            '</button> ';

        return htmlResult;
    }

    function getUserField(customOptions) {
        //Default Options.
        var options = {
            fieldId: "",        // lblOwner
            fieldName: "",      // Owner
            fieldType: "User",  // User | Group
            displayName: "",
            displaySOEID: "",
            displayText: "",    // APS GRA Team (5)
            primaryName: "",    // Rodriguez Quesada, Rodolfo Gerardo
            primarySOEID: "",   // RR16467
            primaryIgnore: false,
            userList: []
        };

        //Hacer un merge con las opciones que nos enviaron y las default.
        $.extend(true, options, customOptions);

        var htmlResult = "";

        //When not exist User show not found
        if (options.userList.length == 0) {

            //For Normal User Field
            if (options.displayName) {
                if (options.displaySOEID) {
                    options.displayText = options.displayName + " [" + options.displaySOEID + "]";
                } else {
                    options.displayText = options.displayName;
                }
            } else {
                if (!options.displayText) {
                    options.displayText = "Not Assigned";
                }
            }
        }

        htmlResult +=
        //Open Tag Form Group
        '<!--' + options.fieldName + '--> ' +
        '<div class="form-group row"> ' +
        '    <label for="' + options.fieldId + '" class="col-md-4 col-form-label"> ' +
        '        ' + options.fieldName + ' ' +
        '    </label> ' +
        '    <div class="col-md-8"> ';

        //Open Tag Input Group only when is Group and more than 1 user
        if (options.fieldType == "Group" && options.userList.length > 1) {
            htmlResult +=
            '        <div class="input-group"> ';
        }

        //Open Tag Form Control
        htmlResult +=
            '            <div class="form-control ' + options.fieldId + '" name="' + options.fieldId + '" id="' + options.fieldId + '"> ' +
            '                <i class="fa ' + getUserIcon(options.fieldType) + '"></i> ' +
            '                <span>' + options.displayText;
        
        //Show PRUEBA MAKERS (2) only when is Group and more than 1 user
        if (options.fieldType == "Group" && options.userList.length > 1) {
            htmlResult += ' (' + options.userList.length + ' Users)';
        }

        //Close Tag Form Control
        htmlResult +=
            '               </span> ' +
            '            </div> ';

        //Show View User List Button only when is Group and more than 1 user
        if (options.fieldType == "Group" && options.userList.length > 1) {
            htmlResult += 
            '            <span class="input-group-btn"> ' +
            '                ' + getUserListPopupHtml(options.userList) +
            '            </span> ';
        }

        //Close Tag Input Group only when is Group and more than 1 user
        if (options.fieldType == "Group" && options.userList.length > 1) {
            htmlResult +=
            '        </div> ';
        }

        //Close Tag Form Group
        htmlResult +=
            '    </div> ' +
            '</div> ';

        // Show Primary Maker / Checker only when is a Group
        if (options.fieldType == "Group" && options.primaryIgnore != true) {
            var primaryDisplayText = "Not Assigned";

            if (options.primaryName) {
                primaryDisplayText = options.primaryName + ' [' + options.primarySOEID + ']';
            }

            htmlResult +=
                '<!--' + options.fieldName + 'Primary--> ' +
                '<div class="form-group row"> ' +
                '    <label for="' + options.fieldId + 'Primary" class="col-md-4 col-form-label"> ' +
                '        Primary ' + options.fieldName + ' ' +
                '    </label> ' +
                '    <div class="col-md-8"> ' +
                '        <div class="form-control ' + options.fieldId + 'Primary" name="' + options.fieldId + 'Primary" id="' + options.fieldId + 'Primary"> ' +
                '            <i class="fa fa-user"></i> ' +
                '            <span>' + primaryDisplayText + '</span> ' +
                '        </div> ' +
                '    </div> ' +
                '</div> ';
        }
            
        return htmlResult;
    }

    function generatePDFReport(objActivityInfo, $modal) {
        //Show full page Loading
        _showLoadingFullPage({
            idLoading: "ActivityInfoPDF",
            msg: "Generating PDF File.."
        });

        //Generate PDF File Name
        var pdfName = "";
        if (objActivityInfo.ACTIVITY_NAME.length > 20) {
            pdfName = objActivityInfo.ACTIVITY_NAME.slice(0, 10) + "__" + objActivityInfo.ACTIVITY_NAME.substr(objActivityInfo.ACTIVITY_NAME.length - 10) + " " + moment().format("YYYY-MM-DD");
        } else {
            pdfName = objActivityInfo.ACTIVITY_NAME + " " + moment().format("YYYY-MM-DD");
        }

        //Show Hidden fields
        $modal.find(".divReportHeader").show();
        $modal.find(".divReportDate").show();

        //Hide icons
        $modal.find(".fa").hide();

        _generatePDFFromHtml("#formActivityInfo", pdfName, function () {
            //Hide Hidden fields
            $modal.find(".divReportHeader").hide();
            $modal.find(".divReportDate").hide();

            //Show icons
            $modal.find(".fa").show();

            //Hide full page Loading
            _hideLoadingFullPage({
                idLoading: "ActivityInfoPDF"
            });
        });
    }

    function generateModal(objActivityInfo) {
        var htmlActivityInfo =
            '<button type="button" class="btnGeneratePDF btn btn-success left15 bottom15 pull-right" > ' +
            '    <i class="fa fa-file-pdf-o"></i> Generate PDF ' +
            '</button> ' +
            '<form id="formActivityInfo" class="form-horizontal" style="clear: both;"> ' +
            '    <!--Hidden Field Report Header--> ' +
            '    <div class="divReportHeader" style="display:none; background-color: #004786;height: 60px;margin-bottom: 15px;"> ' +
            '        <img src="' + _fixFrameworkURL('/Content/Shared/images/global_header_logo.png') + '" style="float: left;"> ' +
            '        <h3 style="color:white;padding-left: 88px;padding-top: 20px;">CoB Process Recovery</h3> ' +
            '    </div> ' +
            '    <h4>General Information';

        if (objActivityInfo.SOURCE == "eDCFC") {
            htmlActivityInfo += '<span class="badge badge-lg badge-success" style="margin-top: 5px; margin-left: 5px;"> eDCFC </span>';
        }

        if (objActivityInfo.SOURCE == "CPR") {
            htmlActivityInfo += '<span class="badge badge-lg badge-warning" style="margin-top: 5px; margin-left: 5px;"> CPR </span>';
        }

        htmlActivityInfo +=
            '    </h4><br> ' +
            '    <!--Hidden Field Report Date--> ' +
            '    <div class="form-group row divReportDate" style="display:none;"> ' +
            '        <label for="lblReportDate" class="col-md-2 col-form-label"> ' +
            '            Report Date ' +
            '        </label> ' +
            '        <div class="col-md-10"> ' +
            '            <div class="form-control lblReportDate" name="lblReportDate" id="lblReportDate"> ' +
            '                ' + moment().format('MMM D, YYYY') + ' ' +
            '            </div> ' +
            '        </div> ' +
            '    </div> ' +

            '    <!--Activity Name--> ' +
            '    <div class="form-group row"> ' +
            '        <label for="lblProcessActivityName" class="col-md-2 col-form-label"> ' +
            '            Process Activity ' +
            '        </label> ' +
            '        <div class="col-md-10"> ' +
            '            <div class="form-control lblProcessActivityName" name="lblProcessActivityName" id="lblProcessActivityName"> ' +
            '                ' + objActivityInfo.ACTIVITY_NAME + ' ' +
            '            </div> ' +
            '        </div> ' +
            '    </div> ' +
            '    <div class="row"> ' +
            '       <div class="col-md-6"> ' +

            '           <!--Managed Segment--> ' +
            '           <div class="form-group row"> ' +
            '               <label for="lblManagedSegment" class="col-md-4 col-form-label"> ' +
            '                   Managed Segment ' +
            '               </label> ' +
            '               <div class="col-md-8"> ' +
            '                   <div class="form-control lblManagedSegment" name="lblManagedSegment" id="lblManagedSegment"> ' +
            '                      ' + objActivityInfo.MANAGED_SEGMENT_DESCRIPTION + ' [' + objActivityInfo.MANAGED_SEGMENT + ']' +
            '                   </div> ' +
            '               </div> ' +
            '           </div> ' +

            //'           <!--Process Taxonomy--> ' +
            //'           <div class="form-group row"> ' +
            //'               <label for="lblProcessTaxonomy" class="col-md-4 col-form-label"> ' +
            //'                   Process Taxonomy ' +
            //'               </label> ' +
            //'               <div class="col-md-8"> ' +
            //'                   <div class="form-control lblProcessTaxonomy" name="lblProcessTaxonomy" id="lblProcessTaxonomy"> ' +
            //'                       L4 609. US Tax Form Validation - Non-Outsourced Model' +
            //'                   </div> ' +
            //'               </div> ' +
            //'           </div> ' +

            '        </div> ' +
            '        <div class="col-md-6"> ' +

            '           <!--GOC--> ' +
            '           <div class="form-group row"> ' +
            '               <label for="lblGOC" class="col-md-4 col-form-label"> ' +
            '                   GOC ' +
            '               </label> ' +
            '               <div class="col-md-8"> ' +
            '                   <div class="form-control lblGOC" name="lblGOC" id="lblGOC"> ' +
            '                      ' + objActivityInfo.GOC_DESCRIPTION + ' [' + objActivityInfo.GOC + ']' +
            '                   </div> ' +
            '               </div> ' +
            '           </div> ' +

            '        </div> ' +
            '    </div> ';

        var GPLText = "Not Assigned";
        if (objActivityInfo.GPL_NAME) {
            GPLText = objActivityInfo.GPL_NAME + ' [' + objActivityInfo.GPL_SOEID + ']';
        }

        htmlActivityInfo +=
            '    <h4>Contacts Information</h4> ' +
            '    <br> ' +
            '    <div class="row"> ' +
            '        <div class="col-md-6"> ' +

            '           <!--GPL--> ' +
            '           <div class="form-group row"> ' +
            '               <label for="lblGPL" class="col-md-4 col-form-label"> ' +
            '                   GPL ' +
            '               </label> ' +
            '               <div class="col-md-8"> ' +
            '                   <div class="form-control lblGPL" name="lblGPL" id="lblGPL"> ' +
            '                       <i class="fa fa-user"></i> ' +
            '                       <span>' + GPLText + '</span> ' +
            '                   </div> ' +
            '               </div> ' +
            '           </div> ';

        //Generate Owner Field
        objActivityInfo.OWNER_USERS = [];
        if (objActivityInfo.OWNER_JSON_USERS) {
            objActivityInfo.OWNER_USERS = JSON.parse(objActivityInfo.OWNER_JSON_USERS);
        }
        var htmlOwnerField = getUserField({
            fieldId: "lblOwner",
            fieldName: "Owner",
            fieldType: objActivityInfo.OWNER_TYPE,
            displayText:  objActivityInfo.OWNER_NAME,
            userList: objActivityInfo.OWNER_USERS,
            primaryIgnore: true
        });
        htmlActivityInfo += htmlOwnerField;

        //Generate Maker Field
        objActivityInfo.MAKER_USERS = [];
        if (objActivityInfo.MAKER_JSON_USERS) {
            objActivityInfo.MAKER_USERS = JSON.parse(objActivityInfo.MAKER_JSON_USERS);
        }
        var htmlMakerField = getUserField({
            fieldId: "lblMaker",
            fieldName: "Maker",
            fieldType: objActivityInfo.MAKER_TYPE,
            displayText: objActivityInfo.MAKER_NAME,
            userList: objActivityInfo.MAKER_USERS,
            primaryName: objActivityInfo.PRIMARY_MAKER_NAME,
            primarySOEID: objActivityInfo.PRIMARY_MAKER_SOEID,
            primaryIgnore: (objActivityInfo.SOURCE == "CPR")
        });
        htmlActivityInfo += htmlMakerField;

        //Generate Checker Field
        objActivityInfo.CHECKER_USERS = [];
        if (objActivityInfo.CHECKER_JSON_USERS) {
            objActivityInfo.CHECKER_USERS = JSON.parse(objActivityInfo.CHECKER_JSON_USERS);
        }
        var htmlCheckerField = getUserField({
            fieldId: "lblChecker",
            fieldName: "Checker",
            fieldType: objActivityInfo.CHECKER_TYPE,
            displayText: objActivityInfo.CHECKER_NAME,
            userList: objActivityInfo.CHECKER_USERS,
            primaryName: objActivityInfo.PRIMARY_CHECKER_NAME,
            primarySOEID: objActivityInfo.PRIMARY_CHECKER_SOEID,
            primaryIgnore: (objActivityInfo.SOURCE == "CPR")
        });
        htmlActivityInfo += htmlCheckerField;

        var RPLText = "Not Assigned";
        if (objActivityInfo.RPL_NAME) {
            RPLText = objActivityInfo.RPL_NAME + ' [' + objActivityInfo.RPL_SOEID + ']';
        }

        htmlActivityInfo +=
            '        </div> ' +
            '        <div class="col-md-6"> ' +

            '           <!--RPL--> ' +
            '           <div class="form-group row"> ' +
            '               <label for="lblRPL" class="col-md-4 col-form-label"> ' +
            '                   RPL ' +
            '               </label> ' +
            '               <div class="col-md-8"> ' +
            '                   <div class="form-control lblRPL" name="lblRPL" id="lblRPL"> ' +
            '                       <i class="fa fa-user"></i> ' +
            '                       <span>' + RPLText + '</span> ' +
            '                   </div> ' +
            '               </div> ' +
            '           </div> ';

        //Generate WAD Owner Field
        var htmlWADOwnerField = getUserField({
            fieldId: "lblWADOwner",
            fieldName: "WAD Owner",
            fieldType: "User",
            displayName: objActivityInfo.WAD_OWNER_NAME,
            displaySOEID: objActivityInfo.WAD_OWNER_SOEID,
            primaryIgnore: true
        });
        htmlActivityInfo += htmlWADOwnerField;

        //Generate WAD Maker Field
        var htmlWADMakerField = getUserField({
            fieldId: "lblWADMaker",
            fieldName: "WAD Maker",
            fieldType: "User",
            displayName: objActivityInfo.WAD_MAKER_NAME,
            displaySOEID: objActivityInfo.WAD_MAKER_SOEID,
            primaryIgnore: true
        });
        htmlActivityInfo += htmlWADMakerField;

        //Generate WAD Checker Field
        var htmlWADCheckerField = getUserField({
            fieldId: "lblWADChecker",
            fieldName: "WAD Checker",
            fieldType: "User",
            displayName: objActivityInfo.WAD_CHECKER_NAME,
            displaySOEID: objActivityInfo.WAD_CHECKER_SOEID,
            primaryIgnore: true
        });
        htmlActivityInfo += htmlWADCheckerField;

        htmlActivityInfo +=
            '        </div> ' +
            '    </div> ';

        objActivityInfo.DOCS = [];
        if (objActivityInfo.JSON_DOCS) {
            objActivityInfo.DOCS = JSON.parse(objActivityInfo.JSON_DOCS);
        }
        htmlActivityInfo +=
            '    <h4>Bluework & SOP / Resources Links </h4> ' +
            '    <br> ' +
            '    <div class="row"> ' +
            '        <div class="col-md-12"> ' +
            '           ' + getDocumentList(objActivityInfo.DOCS) +
            '        </div> ' +
            '    </div> ' +

            //'    <h4>Checklist & MCA Key Controls</h4> ' +
            //'    <br> ' +
            //'    <div class="row"> ' +
            //'        <div class="col-md-6"> ' +

            //'        </div> ' +
            //'        <div class="col-md-6"> ' +

            //'        </div> ' +
            //'    </div> ' +

            //'    <h4>Applications and EUCs</h4> ' +
            //'    <br> ' +
            //'    <div class="row"> ' +
            //'        <div class="col-md-6"> ' +

            //'        </div> ' +
            //'        <div class="col-md-6"> ' +

            //'        </div> ' +
            //'    </div> ' +

            '</form> ';

        var htmlContentModal = "<div id='popupContainerActivityInfo'>" + htmlActivityInfo + "</div>";

        _showModal({
            width: '90%',
            modalId: "ModalActivityInfo",
            addCloseButton: true,
            title: "Activity Information",
            onReady: function ($modal) {
                //Init popover user list
                _GLOBAL_SETTINGS.tooltipsPopovers();

                //On Click Generate PDF
                $modal.find(".btnGeneratePDF").click(function () {
                    generatePDFReport(objActivityInfo, $modal);
                });
            },
            contentHtml: htmlContentModal
        });
    }
}

// Show Modal Confirm Communication
function _openModalCommunicationInfo(customOptions) {
    //Default Options.
    var options = {
        title: "Communication Information",
        buttons: [],
        fields: {
            impactedDates: {
                editable: false
            },
            comment: {
                editable: false
            }
        },
        objCommunication: {
            isTestEvent: false,
            locationTypetoRecover: "",

            impactedDateStart: "",
            impactedDateEnd: "",
            impactedDateRangeText: "",

            idLocation: "",
            locationName: "",
            locationActivitiesCount: 0,

            idProcessActivities: [],
            communicationDate: "",
            typeOfEvent: "",
            localRecovery: "",
            recoveryAction: "",
            comment: "",
            sendToFRSSHead: false,
            sendToGPL: false,
            sendToRPL: false,
            sendToOwner: false,
            sendToOwnerManager: false,
            sendToMaker: false,
            sendToMakerManager: false,
            sendToChecker: false,
            sendToCheckerManager: false,
            sendToWADOwner: false,
            sendToWADOwnerManager: false,
            sendToWADMaker: false,
            sendToWADMakerManager: false,
            sendToWADChecker: false,
            sendToWADCheckerManager: false,
            testEmail: ""
        },
        onConfirm: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    var objCommunication = options.objCommunication;
    var htmlContentModal = "";

    htmlContentModal +=
        '<ul class="list-group">  ';

    if (objCommunication.isTestEvent) {
        htmlContentModal +=
            '    <li class="list-group-item">  ' +
            '        <div class="col-md-4">  ' +
            '            <span>Attestation Event: </span>  ' +
            '        </div>  ' +
            '        <div class="col-md-8">  ' +
            '            <span class="badge badge-md badge-success">Yes</span> ' +
            '        </div>  ' +
            '        <div style="clear:both;"></div>  ' +
            '    </li> ';
    }

    htmlContentModal +=
        '    <li class="list-group-item">  ' +
        '        <div class="col-md-4">  ' +
        '            <span>Activities to recover: </span>  ' +
        '        </div>  ' +
        '        <div class="col-md-8">  ' +
        '            <span class="badge badge-md badge-primary">' + objCommunication.locationActivitiesCount + '</span>  ' +
        '        </div>  ' +
        '        <div style="clear:both;"></div>  ' +
        '    </li> ' +

        '    <li class="list-group-item">  ' +
        '        <div class="col-md-4">  ' +
        '            <span>Communication Date <i class="fa fa-calendar"></i>: </span>  ' +
        '        </div>  ' +
        '        <div class="col-md-8">  ' +
        '            ' + moment(objCommunication.communicationDate).format("MMM DD, YYYY") + '  ' +
        '        </div>  ' +
        '        <div style="clear:both;"></div>  ' +
        '    </li> ';

    if (options.fields.impactedDates.editable) {
        htmlContentModal +=
            '    <li class="list-group-item">  ' +
            '        <div class="col-md-4">  ' +
            '            <span>Impacted Dates <i class="fa fa-calendar"></i>: </span>  ' +
            '        </div>  ' +
            '        <div class="col-md-8">  ' +
            '            <div impactedStartDate="' + objCommunication.impactedDateStart + '" impactedEndDate="' + objCommunication.impactedDateEnd + '" impactedDateRange="' + objCommunication.impactedDateRangeText + '" style="cursor:pointer; border-color: green;" class="form-control daterange daterange-text add-date-ranges containerImpactedDates" id="containerImpactedDates" data-format="MMMM D, YYYY" > ' +
            '                <i class="fa fa-calendar"></i> ' +
            '                <span >' + objCommunication.impactedDateRangeText + '</span> ' +
            '            </div> ' +
            '        </div>  ' +
            '        <div style="clear:both;"></div>  ' +
            '    </li> ';
    } else {
        htmlContentModal +=
            '    <li class="list-group-item">  ' +
            '        <div class="col-md-4">  ' +
            '            <span>Impacted Dates <i class="fa fa-calendar"></i>: </span>  ' +
            '        </div>  ' +
            '        <div class="col-md-8">  ' +
            '            ' + objCommunication.impactedDateRangeText +
            '        </div>  ' +
            '        <div style="clear:both;"></div>  ' +
            '    </li> ';
    }

    htmlContentModal +=
        '    <li class="list-group-item">  ' +
        '        <div class="col-md-4">  ' +
        '            <span>Recover: </span>  ' +
        '        </div>  ' +
        '        <div class="col-md-8">  ' +
        '            ' + objCommunication.locationName + ' (' + objCommunication.locationTypetoRecover + ')  ' +
        '        </div>  ' +
        '        <div style="clear:both;"></div>  ' +
        '    </li> ' +

        '    <li class="list-group-item">  ' +
        '        <div class="col-md-4">  ' +
        '            <span>Type of Event: </span>  ' +
        '        </div>  ' +
        '        <div class="col-md-8">  ' +
        '            ' + objCommunication.typeOfEvent +
        '        </div>  ' +
        '        <div style="clear:both;"></div>  ' +
        '    </li> ';

    if (objCommunication.typeOfEvent == "Local") {
        htmlContentModal +=
            '    <li class="list-group-item">  ' +
            '        <div class="col-md-4">  ' +
            '            <span>Local Recovery: </span>  ' +
            '        </div>  ' +
            '        <div class="col-md-8">  ' +
            '            ' + objCommunication.localRecovery +
            '        </div>  ' +
            '        <div style="clear:both;"></div>  ' +
            '    </li> ';
    }

    htmlContentModal +=
        '    <li class="list-group-item">  ' +
        '        <div class="col-md-4">  ' +
        '            <span>Recovery Action: </span>  ' +
        '        </div>  ' +
        '        <div class="col-md-8">  ' +
        '            ' + objCommunication.recoveryAction +
        '        </div>  ' +
        '        <div style="clear:both;"></div>  ' +
        '    </li> ';

    if (options.fields.comment.editable) {
        htmlContentModal +=
            '    <li class="list-group-item">  ' +
            '        <div class="col-md-4">  ' +
            '            <span>Comment: </span>  ' +
            '        </div>  ' +
            '        <div class="col-md-8">  ' +
            '            <textarea style="border-color: green;" class="form-control txtComment" placeholder="e. g. Please recover ASAP this critical process activities!" rows="5" name="txtComment" id="txtComment">' + objCommunication.comment + '</textarea> ' +
            '        </div>  ' +
            '        <div style="clear:both;"></div>  ' +
            '    </li> ';
    } else {
        htmlContentModal +=
            '    <li class="list-group-item">  ' +
            '        <div class="col-md-4">  ' +
            '            <span>Comment: </span>  ' +
            '        </div>  ' +
            '        <div class="col-md-8">  ' +
            '            ' + objCommunication.comment +
            '        </div>  ' +
            '        <div style="clear:both;"></div>  ' +
            '    </li> ';
    }
        
    htmlContentModal +=
        '    <li class="list-group-item">  ' +
        '        <div class="col-md-4">  ' +
        '            <span>Sent To: </span>  ' +
        '        </div>  ' +
        '        <div class="col-md-8">  ' +
        '           <ul class="list-unstyled">  ' +
        '               <li class="col-md-12" style="padding: 0;"><i class="fa fa-user"></i> FRSS Head <i class="fa ' + getContactCheckIcon(objCommunication.sendToFRSSHead) + '"></i></li>  ' +
        '           </ul>  ' +
        '           <ul class="list-unstyled">  ' +
        '               <li class="col-md-5" style="padding: 0;"><i class="fa fa-user"></i> GPL <i class="fa ' + getContactCheckIcon(objCommunication.sendToGPL) + '"></i></li>  ' +
        '               <li class="col-md-7" style="padding: 0;"><i class="fa fa-user"></i> RPL <i class="fa ' + getContactCheckIcon(objCommunication.sendToRPL) + '"></i></li>  ' +
        '           </ul>  ' +
        '           <ul class="list-unstyled">  ' +
        '               <li class="col-md-5" style="padding: 0;"><i class="fa fa-user"></i> Owner <i class="fa ' + getContactCheckIcon(objCommunication.sendToOwner) + '"></i></li>  ' +
        '               <li class="col-md-7" style="padding: 0;"><i class="fa fa-user"></i> Manager of Owner <i class="fa ' + getContactCheckIcon(objCommunication.sendToOwnerManager) + '"></i></li>  ' +
        '           </ul>  ' +
        '           <ul class="list-unstyled">  ' +
        '               <li class="col-md-5" style="padding: 0;"><i class="fa fa-user"></i> Maker <i class="fa ' + getContactCheckIcon(objCommunication.sendToMaker) + '"></i></li>  ' +
        '               <li class="col-md-7" style="padding: 0;"><i class="fa fa-user"></i> Manager of Maker <i class="fa ' + getContactCheckIcon(objCommunication.sendToMakerManager) + '"></i></li>  ' +
        '           </ul>  ' +
        '           <ul class="list-unstyled">  ' +
        '               <li class="col-md-5" style="padding: 0;"><i class="fa fa-user"></i> Checker <i class="fa ' + getContactCheckIcon(objCommunication.sendToChecker) + '"></i></li>  ' +
        '               <li class="col-md-7" style="padding: 0;"><i class="fa fa-user"></i> Manager of Checker <i class="fa ' + getContactCheckIcon(objCommunication.sendToCheckerManager) + '"></i></li>  ' +
        '           </ul>  ' +
        '           <ul class="list-unstyled">  ' +
        '               <li class="col-md-5" style="padding: 0;"><i class="fa fa-user"></i> WAD Owner <i class="fa ' + getContactCheckIcon(objCommunication.sendToWADOwner) + '"></i></li>  ' +
        '               <li class="col-md-7" style="padding: 0;"><i class="fa fa-user"></i> Manager of WAD Owner <i class="fa ' + getContactCheckIcon(objCommunication.sendToWADOwnerManager) + '"></i></li>  ' +
        '           </ul>  ' +
        '           <ul class="list-unstyled">  ' +
        '               <li class="col-md-5" style="padding: 0;"><i class="fa fa-user"></i> WAD Maker <i class="fa ' + getContactCheckIcon(objCommunication.sendToWADMaker) + '"></i></li>  ' +
        '               <li class="col-md-7" style="padding: 0;"><i class="fa fa-user"></i> Manager of WAD Maker <i class="fa ' + getContactCheckIcon(objCommunication.sendToWADMakerManager) + '"></i></li>  ' +
        '           </ul>  ' +
        '           <ul class="list-unstyled">  ' +
        '               <li class="col-md-5" style="padding: 0;"><i class="fa fa-user"></i> WAD Checker <i class="fa ' + getContactCheckIcon(objCommunication.sendToWADChecker) + '"></i></li>  ' +
        '               <li class="col-md-7" style="padding: 0;"><i class="fa fa-user"></i> Manager of WAD Checker <i class="fa ' + getContactCheckIcon(objCommunication.sendToWADCheckerManager) + '"></i></li>  ' +
        '           </ul>  ' +
        '           <div style="clear:both;"></div>  ' +
        '        </div>  ' +
        '        <div style="clear:both;"></div>  ' +
        '    </li> ';

    if (objCommunication.testEmail) {
        htmlContentModal +=
            '    <li class="list-group-item">  ' +
            '        <div class="col-md-4">  ' +
            '            <span>Test Email: </span>  ' +
            '        </div>  ' +
            '        <div class="col-md-8">  ' +
            '            <span class="badge badge-md badge-success">' + objCommunication.testEmail + '</span> (All notifications will be send only to this email) ' +
            '        </div>  ' +
            '        <div style="clear:both;"></div>  ' +
            '    </li> ';
    }

    htmlContentModal +=
        '</ul>';

    _showModal({
        width: '70%',
        modalId: "modalDelRow",
        addCloseButton: true,
        buttons: options.buttons,
        title: options.title,
        contentHtml: htmlContentModal,
        onReady: function ($modal) {
            if (options.fields.impactedDates.editable) {
                _loadImpactedDateRangePlugin($modal.find(".containerImpactedDates"));
            }
        }
    });

    function getContactCheckIcon(isSelected) {
        var result = "fa-close text-danger";

        if (isSelected) {
            result = "fa-check text-success";
        }

        return result;
    }
}

// Create plugin to select Impacted Date Range
function _loadImpactedDateRangePlugin ($elContainer) {
    //Set custom date to flag all dates
    var allDatesFlag = moment("1992-01-19");

    var ranges = {
        'All': [allDatesFlag, allDatesFlag],
        'Today': [moment(), moment()],
        'Next 3 Days': [moment(), moment().add(2, 'days')],
        'Next 15 Days': [moment(), moment().add(14, 'days')],
        'Next 30 Days': [moment(), moment().add(29, 'days')],
        'This Month': [moment().startOf('month'), moment().endOf('month')]
    };

    var $containerImpactedDates = $elContainer,
        options = {
            format: _getElementValueHelper($containerImpactedDates, 'format', 'MM/DD/YYYY'),
            timePicker: _getElementValueHelper($containerImpactedDates, 'timePicker', false),
            timePickerIncrement: _getElementValueHelper($containerImpactedDates, 'timePickerIncrement', false),
            separator: _getElementValueHelper($containerImpactedDates, 'separator', ' - '),
        },
        min_date = _getElementValueHelper($containerImpactedDates, 'minDate', ''),
        max_date = _getElementValueHelper($containerImpactedDates, 'maxDate', ''),
        start_date = _getElementValueHelper($containerImpactedDates, 'startDate', ''),
        end_date = _getElementValueHelper($containerImpactedDates, 'endDate', '');

    if ($containerImpactedDates.hasClass('add-date-ranges')) {
        options['ranges'] = ranges;
    }

    if (min_date.length) {
        options['minDate'] = min_date;
    }

    if (max_date.length) {
        options['maxDate'] = max_date;
    }

    if (start_date.length) {
        options['startDate'] = start_date;
    } else {
        options['startDate'] = allDatesFlag;
    }

    if (end_date.length) {
        options['endDate'] = end_date;
    } else {
        options['endDate'] = allDatesFlag;
    }

    //Create Date Range Picker
    $containerImpactedDates.daterangepicker(options, function (start, end) {
        var objDaterangepicker = this;
        var drp = $containerImpactedDates.data('daterangepicker');

        if ($containerImpactedDates.hasClass('daterange-text')) {
            //$containerImpactedDates.find('span').html(start.format(drp.format) + drp.separator + end.format(drp.format));
            if (start.format("YYYY-MM-DD") == allDatesFlag.format("YYYY-MM-DD")) {
                //Update Impacted Date Range
                $elContainer.attr("impactedStartDate", "");
                $elContainer.attr("impactedEndDate", "");
                $elContainer.attr("impactedDateRange", "");

                $containerImpactedDates.find('span').html("All");
            } else {
                //Update Impacted Date Range
                $elContainer.attr("impactedStartDate", start.format('YYYY-MM-DD'));
                $elContainer.attr("impactedEndDate", end.format('YYYY-MM-DD'));

                //When is Today avoid duplicate the same date "March 23, 2018 - March 23, 2018"
                if ($elContainer.attr("impactedStartDate") == $elContainer.attr("impactedEndDate")) {
                    $elContainer.attr("impactedDateRange", start.format('MMM D, YYYY') + " (" + objDaterangepicker.chosenLabel + ")" );
                } else {
                    $elContainer.attr("impactedDateRange", start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY') + " (" + objDaterangepicker.chosenLabel + ")");
                }

                $containerImpactedDates.find('span').html($elContainer.attr("impactedDateRange"));
            }

            //Update Location Activities Counter
            //$(".selToRecover").change();
        }
    });

    if (typeof options['ranges'] == 'object') {
        $containerImpactedDates.data('daterangepicker').container.removeClass('show-calendar');
    }
}

// Element Attribute Helper
function _getElementValueHelper($el, data_var, default_val) {
    if (typeof $el.data(data_var) != 'undefined') {
        return $el.data(data_var);
    }

    return default_val;
}

function _loadTutorialSendRealEventPart1() {
    //Clear others
    $("[data-step]").removeAttr("data-step");
    $("[data-intro]").removeAttr("data-intro");

    var urlStartEvent = _getViewVar("SubAppPath") + '/CPR/StartEvent';
    var $aLinkStartEvent = $("#user-menu").find("a[href='" + urlStartEvent + "']");
    $aLinkStartEvent.attr("data-step", "1");
    $aLinkStartEvent.attr("data-intro", "To send a Real Event please click here in <b>Start Event</b>");

    introJs().setOption('doneLabel', 'Continue').start().oncomplete(function () {
        window.location.href = _getViewVar("SubAppPath") + '/CPR/StartEvent?ptutorial=SendRealEventPart2';
    });

    //introJs().start();
}

function _loadTutorialSendRealEventPart2() {
    //Clear others
    $("[data-step]").removeAttr("data-step");
    $("[data-intro]").removeAttr("data-intro");

    //What needs to be recovered
    var $selToRecover = $("#selToRecover");
    var htmlSelToRecoverMsg =
        "Seconds you need to select <b>What needs to be recoverd</b> can be:" +
        "<ul> " + 
        "    <li><b>Main FRSS Center:</b> Use this option for principal centers. e.g. CSS Costa Rica, CSS Tampa, CSS Mumbai and CSS Manila.</li> " +
        "    <li><b>Other Locations:</b> Use this option for others locations of employees. e.g. Dublin, New York.</li> " +
        "</ul>"; 
    $selToRecover.attr("data-step", "2");
    $selToRecover.attr("data-intro", htmlSelToRecoverMsg);

    //Impacted Dates
    var $containerImpactedDates = $("#containerImpactedDates");
    var htmlContainerImpactedDates = "Then you need to select <b>Impacted Dates</b> for the disaster. e.g. A fire in a FRSS Center can prevent the operation of the process activities for 15 days. ";
    $containerImpactedDates.attr("data-step", "3");
    $containerImpactedDates.attr("data-intro", htmlContainerImpactedDates);

    if ($selToRecover.val() == "Center") {
        //FRSS Center Impacted
        var $selCenter = $("#selCenter");
        var htmlSelCenter = "Then you need to select <b>FRSS Center Impacted</b> for the disaster. e.g. CSS Costa Rica, CSS Tampa, CSS Mumbai and CSS Manila";
        $selCenter.attr("data-step", "4");
        $selCenter.attr("data-intro", htmlSelCenter);
    }

    if ($selToRecover.val() == "Other") {
        //Other FRSS Location Impacted
        var $selOtherLocation = $("#selOtherLocation");
        var htmlSelOtherLocation = "Then you need to select <b>Other FRSS Location Impacted</b> for the disaster. e.g. Dublin, New York";
        $selOtherLocation.attr("data-step", "4");
        $selOtherLocation.attr("data-intro", htmlSelOtherLocation);
    }

    //Process Activities
    var $linkGoTabActivityList = $("#linkGoTabActivityList").find("span");
    var htmlLinkGoTabActivityList = "The next step is review the impacted activities for the FRSS Center in disaster.";
    $linkGoTabActivityList.attr("data-step", "5");
    $linkGoTabActivityList.attr("data-intro", htmlLinkGoTabActivityList);

    //Send
    var $linkGoTabCommunication = $("#linkGoTabCommunication").find("span");
    var htmlLinkGoTabCommunication = "When all is ready, you can review the contacts that are going to receive the communication.";
    $linkGoTabCommunication.attr("data-step", "6");
    $linkGoTabCommunication.attr("data-intro", htmlLinkGoTabCommunication);

    introJs().start();
}

function _loadTutorialUploadCPRTemplatePart1() {
    //Clear others
    $("[data-step]").removeAttr("data-step");
    $("[data-intro]").removeAttr("data-intro");

    var urlCPRTemplate = _getViewVar("SubAppPath") + '/CPR/ListProcessActivity?pviewType=Upload';
    var $urlCPRTemplate = $("#user-menu").find("a[href='" + urlCPRTemplate + "']");
    $urlCPRTemplate.attr("data-step", "1");
    $urlCPRTemplate.attr("data-intro", "To upload process activities please click here in <b>Upload CPR Template</b>");

    introJs().setOption('doneLabel', 'Continue').start().oncomplete(function () {
        window.location.href = _getViewVar("SubAppPath") + '/CPR/ListProcessActivity?pviewType=Upload&ptutorial=UploadCPRTemplatePart2';
    });
}

function _loadTutorialUploadCPRTemplatePart2() {
    //Clear others
    $("[data-step]").removeAttr("data-step");
    $("[data-intro]").removeAttr("data-intro");

    //Process Trees
    var $selProcessTree = $(".fieldIDProcess");
    var htmlSelProcessTree = "Seconds you need to select the <b>FRSS Process</b> for the activites that you are going to upload";
    $selProcessTree.attr("data-step", "2");
    $selProcessTree.attr("data-intro", htmlSelProcessTree);

    //Upload CPR Process Template
    var $btnDownloadCPRTemplate = $(".btnDownloadCPRTemplate");
    var htmlBtnDownloadCPRTemplate = "Then click here to download the <b>Excel template</b> for the selected FRSS Process.";
    $btnDownloadCPRTemplate.attr("data-step", "3");
    $btnDownloadCPRTemplate.attr("data-intro", htmlBtnDownloadCPRTemplate);

    //Recurrence 
    var urlRecurrence = _getViewVar("SubAppPath") + '/CPR/Recurrence';
    var $urlRecurrence = $("#user-menu").find("a[href='" + urlRecurrence + "']");
    $urlRecurrence.attr("data-step", "4");
    $urlRecurrence.attr("data-intro", "A process activity has recurrence and you can create the <b>Recurrence Code</b> here for your CPR Excel Template.");

    //Upload CPR Process Template
    var $dropFileZone = $(".jsxlsx-drop");
    var htmlDropFieZone = "Finally you need to select or drop your Excel file that you previuos downloaded to upload the data.";
    $dropFileZone.attr("data-step", "5");
    $dropFileZone.attr("data-intro", htmlDropFieZone);

    //Upload CPR Process Template
    var $tblProcessActivities = $("#tblProcessActivities");
    var htmlTblProcessActivities = "Here you can view the list of activities for the selected FRSS Process.";
    $tblProcessActivities.attr("data-step", "6");
    $tblProcessActivities.attr("data-intro", htmlTblProcessActivities);

    introJs().start();
}

function _loadTutorialSendAttestationEventPart1() {
    //Clear others
    $("[data-step]").removeAttr("data-step");
    $("[data-intro]").removeAttr("data-intro");

    var urlStartEvent = _getViewVar("SubAppPath") + '/CPR/StartEvent';
    var $aLinkStartEvent = $("#user-menu").find("a[href='" + urlStartEvent + "']");
    $aLinkStartEvent.attr("data-step", "1");
    $aLinkStartEvent.attr("data-intro", "To send a Attestation Event please click here in <b>Start Event</b>");

    introJs().setOption('doneLabel', 'Continue').start().oncomplete(function () {
        window.location.href = _getViewVar("SubAppPath") + '/CPR/StartEvent?ptutorial=SendAttestationEventPart2';
    });

    //introJs().start();
}

function _loadTutorialSendAttestationEventPart2() {
    //Clear others
    $("[data-step]").removeAttr("data-step");
    $("[data-intro]").removeAttr("data-intro");

    //What needs to be recovered
    var $selToRecover = $("#selToRecover");
    var htmlSelToRecoverMsg =
        "Then you need to select <b>What needs to be attested</b> can be:" +
        "<ul> " +
        "    <li><b>Main FRSS Center:</b> Use this option for principal centers. e.g. CSS Costa Rica, CSS Tampa, CSS Mumbai and CSS Manila.</li> " +
        "    <li><b>Other Locations:</b> Use this option for others locations of employees. e.g. Dublin, New York.</li> " +
        "</ul>";
    $selToRecover.attr("data-step", "2");
    $selToRecover.attr("data-intro", htmlSelToRecoverMsg);

    //Impacted Dates
    var $containerImpactedDates = $("#containerImpactedDates");
    var htmlContainerImpactedDates = "Then you need to select <b>Impacted Dates</b> for the attestation to filter activities with the recurrence in the impacted dates. e.g. Select <b>15 Days</b> to get all the critical activities that need to be run between the impacted dates. ";
    $containerImpactedDates.attr("data-step", "3");
    $containerImpactedDates.attr("data-intro", htmlContainerImpactedDates);

    if ($selToRecover.val() == "Center") {
        //FRSS Center Impacted
        var $selCenter = $("#selCenter");
        var htmlSelCenter = "Then you need to select <b>FRSS Center Impacted</b> for the attestation. e.g. CSS Costa Rica, CSS Tampa, CSS Mumbai and CSS Manila";
        $selCenter.attr("data-step", "4");
        $selCenter.attr("data-intro", htmlSelCenter);
    }

    if ($selToRecover.val() == "Other") {
        //Other FRSS Location Impacted
        var $selOtherLocation = $("#selOtherLocation");
        var htmlSelOtherLocation = "Then you need to select <b>Other FRSS Location Impacted</b> for the attestation. e.g. Dublin, New York";
        $selOtherLocation.attr("data-step", "4");
        $selOtherLocation.attr("data-intro", htmlSelOtherLocation);
    }

    //Process Activities
    var $linkGoTabActivityList = $("#linkGoTabActivityList").find("span");
    var htmlLinkGoTabActivityList = "The next step is review the impacted activities for the FRSS Center in attestation.";
    $linkGoTabActivityList.attr("data-step", "5");
    $linkGoTabActivityList.attr("data-intro", htmlLinkGoTabActivityList);

    //Send
    var $linkGoTabCommunication = $("#linkGoTabCommunication").find("span");
    var htmlLinkGoTabCommunication = "When all is ready, you can review the contacts that are going to receive the attestation email.";
    $linkGoTabCommunication.attr("data-step", "6");
    $linkGoTabCommunication.attr("data-intro", htmlLinkGoTabCommunication);

    introJs().start();
}

function _loadTutorialReviewActivitiesPart1() {
    //Clear others
    $("[data-step]").removeAttr("data-step");
    $("[data-intro]").removeAttr("data-intro");

    var urlMyProcessToRecover = _getViewVar("SubAppPath") + '/CPR/MyProcessToRecover';
    var $linkMyProcessToRecover = $("#user-menu").find("a[href='" + urlMyProcessToRecover + "']");
    $linkMyProcessToRecover.attr("data-step", "1");
    $linkMyProcessToRecover.attr("data-intro", "To review your CPR & eDCFC process activities in which you are related please click here in <b>My Process to recover</b>");

    var urlListProcessActivities = _getViewVar("SubAppPath") + '/CPR/ListProcessActivity';
    var $linkListProcessActivities = $("#user-menu").find("a[href='" + urlListProcessActivities + "']");
    $linkListProcessActivities.attr("data-step", "2");
    $linkListProcessActivities.attr("data-intro", "To review all CPR & eDCFC process activities please click here in <b>Recovery Info</b>");

    introJs().setOption('doneLabel', 'Continue').start().oncomplete(function () {
        window.location.href = _getViewVar("SubAppPath") + '/CPR/ListProcessActivity?ptutorial=ReviewActivitiesPart2';
    });

    //introJs().start();
}

function _loadTutorialReviewActivitiesPart2() {
    //Clear others
    $("[data-step]").removeAttr("data-step");
    $("[data-intro]").removeAttr("data-intro");

    //GPL
    var $selGPL = $("#selGPL").parent();
    var htmlSelGPL = "Here you can filter all process activities for a specific <b>GPL</b>";
    $selGPL.attr("data-step", "2");
    $selGPL.attr("data-intro", htmlSelGPL);

    //RPL
    var $selRPL = $("#selRPL").parent();
    var htmlSelRPL = "Here you can filter all process activities for a specific <b>RPL</b>";
    $selRPL.attr("data-step", "3");
    $selRPL.attr("data-intro", htmlSelRPL);

    //RPL
    var $selAnalyst = $("#selAnalyst").parent();
    var htmlSelAnalyst = "Here you can filter all process activities for a specific <b>Analyst</b> can be Owner, Maker, Checker, WAD Owner, WAD Maker, WAD Checker and Managers.";
    $selAnalyst.attr("data-step", "4");
    $selAnalyst.attr("data-intro", htmlSelAnalyst);

    introJs().start();
}

//BD Data
function _getDBCommunicationByID(customOptions) {
    //Default Options.
    var options = {
        idCommunication: "",
        onReady: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    //Load Communication information
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Getting communication information...",
        name: "[dbo].[spCPRAFrwkCommunicationAdmin]",
        params: [
            { "Name": "@Action", "Value": 'ByID' },
            { "Name": "@IDCommunication", "Value": options.idCommunication }
        ],
        success: {
            fn: function (responseList) {
                options.onReady(responseList[0]);
            }
        }
    });
}

function _getDBListGPLs(customOptions) {
    //Default Options.
    var options = {
        onReady: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    var sql =
        ";WITH TEDCFCActivities AS ( \n " +
	    "    SELECT \n " +
		"        N.[ID] \n " +
	    "    FROM \n " +
		"        [TITAN].[dbo].[Node] N WITH(READUNCOMMITTED) \n " +
		"	        --Get Only Recoverable WAD \n " +
		"	        INNER JOIN [TITAN].[dbo].[NodeData] ND_RWAD WITH(READUNCOMMITTED) ON ND_RWAD.[NodeID] = N.[ID] AND ND_RWAD.[MetadataID] = 32 AND ND_RWAD.[Value] = 'True' \n " +
	    "    WHERE \n " +
		"        --Get only Active Activities \n " +
		"        N.[IsActive] = 1 \n " +
        " \n " +
		"        --Get only Control Activities \n " +
		"        AND N.[CategoryID] = 3 \n " +
        ") \n " +
        "SELECT DISTINCT \n " +
        "    ISNULL(GPL.SOEID, 'TBD')          AS [SOEID], \n " +
	    "    ISNULL(GPL.NAME, 'Not Assigned')  AS [NAME] \n " +
        "FROM \n " +
        "        [TITAN].[dbo].[Node] N \n " +
        " \n " +
        "    --Get metadata GOC \n " +
        "    LEFT JOIN [TITAN].[dbo].[NodeData] ND_GOC WITH(READUNCOMMITTED) ON ND_GOC.[NodeID] = N.[ID] AND ND_GOC.[MetadataID] = 26 AND ND_GOC.[Value] IS NOT NULL \n " +
        "    LEFT JOIN [Automation].[dbo].[tblGlobalOrganizationCode] G WITH(READUNCOMMITTED) ON ND_GOC.[Value] = G.ID \n " +
        " \n " +
        "    --Get GPL \n " +
        "    LEFT JOIN [Automation].[dbo].[tblManagedSegment] MS WITH(READUNCOMMITTED) ON G.ManagedSegmentID = MS.ID \n " +
        "    LEFT JOIN [Automation].[dbo].[tblEmployee] GPL WITH(READUNCOMMITTED) ON MS.[ManagerID] = GPL.[ID] \n " +
        "WHERE \n " +
        "    N.[ID] IN (SELECT T1.[ID] FROM TEDCFCActivities T1) \n " +
        "UNION \n " +
        "SELECT DISTINCT \n " +
	    "    ISNULL(GPL.SOEID, 'TBD')          AS [SOEID], \n " +
	    "    ISNULL(GPL.NAME, 'Not Assigned')  AS [NAME] \n " +
        "FROM  \n " +
	    "    [dbo].[tblCPRProcessActivity] PA \n " +
		"        --Get GPL \n " +
		"        LEFT JOIN [Automation].[dbo].[tblManagedSegment] MS WITH(READUNCOMMITTED) ON PA.[ManagedSegment] = MS.ID \n " +
		"        LEFT JOIN [Automation].[dbo].[tblEmployee] GPL WITH(READUNCOMMITTED) ON MS.[ManagerID] = GPL.[ID]  \n " +
        "WHERE \n " +
        "    PA.[IsDeleted] = 0 \n " +
        "ORDER BY \n " +
	    "    (2)";

    //Load process information
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading GPLs...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': _toJSON(sql) },
        type: "POST",
        success: function (resultList) {

            if (options.onReady) {
                options.onReady(resultList);
            }
        }
    });
}

function _getDBListRPLs(customOptions) {
    //Default Options.
    var options = {
        onReady: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    var sql =
        ";WITH TEDCFCActivities AS ( \n " +
	    "    SELECT \n " +
		"        N.[ID] \n " +
	    "    FROM \n " +
		"        [TITAN].[dbo].[Node] N WITH(READUNCOMMITTED) \n " +
		"	        --Get Only Recoverable WAD \n " +
		"	        INNER JOIN [TITAN].[dbo].[NodeData] ND_RWAD WITH(READUNCOMMITTED) ON ND_RWAD.[NodeID] = N.[ID] AND ND_RWAD.[MetadataID] = 32 AND ND_RWAD.[Value] = 'True' \n " +
	    "    WHERE \n " +
		"        --Get only Active Activities \n " +
		"        N.[IsActive] = 1 \n " +
        " \n " +
		"        --Get only Control Activities \n " +
		"        AND N.[CategoryID] = 3 \n " +
        ") \n " +
        "SELECT DISTINCT \n " +
	    "    ISNULL(RPL.[SOEID], 'TBD')            AS [SOEID], \n " +
	    "    ISNULL(RPL.[NAME], 'Not Assigned')    AS [NAME] \n " +
        "FROM  \n " +
	    "    [TITAN].[dbo].[Node] N  \n " +
		"        --Get metadata GOC \n " +
		"        LEFT JOIN [TITAN].[dbo].[NodeData] ND_GOC WITH(READUNCOMMITTED) ON ND_GOC.[NodeID] = N.[ID] AND ND_GOC.[MetadataID] = 26 AND ND_GOC.[Value] IS NOT NULL  \n " +
		"        LEFT JOIN [Automation].[dbo].[tblGlobalOrganizationCode] G WITH(READUNCOMMITTED) ON ND_GOC.[Value] = G.ID  \n " +
        " \n " +
		"        --Get RPL \n " +
		"        LEFT JOIN [Automation].[dbo].[tblManagedSegmentManagedGeography] MSMG WITH(READUNCOMMITTED) ON G.ManagedSegmentID = MSMG.ManagedSegmentID AND G.ManagedGeographyID = MSMG.ManagedGeographyID   \n " +
		"        LEFT JOIN [Automation].[dbo].[tblEmployee] RPL WITH(READUNCOMMITTED) ON MSMG.[ManagerID] = RPL.ID  \n " +
        "WHERE \n " +
	    "    N.[ID] IN (SELECT T1.[ID] FROM TEDCFCActivities T1) \n " +
        "UNION \n " +
        "SELECT DISTINCT \n " +
	    "    ISNULL(RPL.[SOEID], 'TBD')            AS [SOEID], \n " +
	    "    ISNULL(RPL.[NAME], 'Not Assigned')    AS [NAME] \n " +
        "FROM  \n " +
	    "    [dbo].[tblCPRProcessActivity] PA  \n " +
		"        --Get GOC \n " +
		"        LEFT JOIN [Automation].[dbo].[tblGlobalOrganizationCode] G WITH(READUNCOMMITTED) ON PA.[GOC] = G.ID \n " +
        " \n " +
		"        --Get RPL \n " +
		"        LEFT JOIN [Automation].[dbo].[tblManagedSegmentManagedGeography] MSMG WITH(READUNCOMMITTED) ON G.ManagedSegmentID = MSMG.ManagedSegmentID AND G.ManagedGeographyID = MSMG.ManagedGeographyID \n " +
		"        LEFT JOIN [Automation].[dbo].[tblEmployee] RPL WITH(READUNCOMMITTED) ON MSMG.[ManagerID] = RPL.ID \n " +
        "WHERE \n " +
	    "    PA.[IsDeleted] = 0 \n " +
        "ORDER BY \n " +
	    "    (2) ";

    //Load process information
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading RPLs...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': _toJSON(sql) },
        type: "POST",
        success: function (resultList) {

            if (options.onReady) {
                options.onReady(resultList);
            }
        }
    });
}

function _getDBListAnalysts(customOptions) {
    //Default Options.
    var options = {
        onReady: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    //Get Analyst
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading analysts from activities...",
        name: "[dbo].[spCPRAFrwkGetActivitiesAnalyst]",
        params: [],
        success: {
            fn: function (resultList) {
                options.onReady(resultList);
            }
        }
    });
}

function _getDBListEDCFCActivitySummary(customOptions) {
    //Default Options.
    var options = {
        filters: _getDefaultFilters(),
        colsToSel: _getDefaultColsToSelect(),
        onReady: function () { }
    };

    var sql = _getSQLDBListEDCFCActivitySummary(customOptions);

    //Load process information
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading eDCFC Process Activities...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': _toJSON(sql) },
        type: "POST",
        success: function (resultList) {

            if (options.onReady) {
                options.onReady(resultList, options.colsToSel);
            }
        }
    });
}

function _getDBListCPRActivitySummary(customOptions) {
    //Default Options.
    var options = {
        filters: _getDefaultFilters(),
        colsToSel: _getDefaultColsToSelect(),
        onReady: function () { }
    };

    var sql = _getSQLDBListCPRActivitySummary(customOptions);

    //Load process information
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading CPR Process Activities...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': _toJSON(sql) },
        type: "POST",
        success: function (resultList) {

            if (options.onReady) {
                options.onReady(resultList, options.colsToSel);
            }
        }
    });
}

function _getDBListMergeActivities(customOptions) {
    //Default Options.
    var options = {
        filters: _getDefaultFilters(),
        colsToSel: _getDefaultColsToSelect(),
        onReady: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    _getSQLDBListEDCFCActivitySummary({
        filters: options.filters,
        colsToSel: options.colsToSel,
        onReady: function (sqlEDCFC) {

            _getSQLDBListCPRActivitySummary({
                filters: options.filters,
                colsToSel: options.colsToSel,
                onReady: function (sqlCPR) {
                    var sql = sqlEDCFC + " \n UNION \n " + sqlCPR;

                    //Load process information
                    _callServer({
                        loadingMsgType: "fullLoading",
                        loadingMsg: "Loading Process Activities...",
                        url: '/Ajax/ExecQuery',
                        data: { 'pjsonSql': _toJSON(sql) },
                        type: "POST",
                        success: function (resultList) {

                            if (options.onReady) {
                                options.onReady(resultList, options.colsToSel);
                            }
                        }
                    });
                }
            });
        }
    });
}

function _getSQLDBListEDCFCActivitySummary(customOptions) {
    //Default Options.
    var options = {
        filters: _getDefaultFilters(),
        colsToSel: _getDefaultColsToSelect(),
        onReady: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    var strIDActivitiesOfUser = "0";

    //Load eDCFC activities related to the user
    if (options.filters.employeeSOEID) {
        _callProcedure({
            loadingMsgType: "fullLoading",
            loadingMsg: "Getting eDCFC activities of user...",
            name: "[dbo].[spCPRAFrwkeDCFCActivitiesByUser]",
            params: [
                { "Name": "@SOEID", "Value": options.filters.employeeSOEID }
            ],
            success: {
                fn: function (activityList) {

                    if (activityList.length > 0) {
                        for (var i = 0; i < activityList.length; i++) {
                            strIDActivitiesOfUser += ", " + activityList[i].ID;
                        }
                    }

                    createSQL();
                }
            }
        });
    } else {
        createSQL();
    }

    function createSQL() {
        //Create SQL, Faster way to get database data
        var sql =
            "SELECT \n";

        if (options.colsToSel.IDProcessTaxonomy.Show) {
            sql += "	0		AS [" + options.colsToSel.IDProcessTaxonomy.Alias + "], \n";
        }

        if (options.colsToSel.ProcessTaxonomyDesc.Show) {
            sql += "	'Not found'		AS [" + options.colsToSel.ProcessTaxonomyDesc.Alias + "], \n";
        }

        if (options.colsToSel.ProcessTaxonomyLevel.Show) {
            sql += "	''		AS [" + options.colsToSel.ProcessTaxonomyLevel.Alias + "], \n";
        }

        if (options.colsToSel.ManagedSegment.Show) {
            sql += "	MS.[ID]		AS [" + options.colsToSel.ManagedSegment.Alias + "], \n";
        }

        if (options.colsToSel.ManagedSegmentDesc.Show) {
            sql += "	MS.[Description]		AS [" + options.colsToSel.ManagedSegmentDesc.Alias + "], \n";
        }

        if (options.colsToSel.GOC.Show) {
            sql += "	ND_GOC.[Value]		AS [" + options.colsToSel.GOC.Alias + "], \n";
        }

        if (options.colsToSel.GOCDesc.Show) {
            sql += "	G.[Description]		AS [" + options.colsToSel.GOCDesc.Alias + "], \n";
        }

        if (options.colsToSel.ActivityName.Show) {
            sql += "	N.[Name]		AS [" + options.colsToSel.ActivityName.Alias + "], \n";
        }

        if (options.colsToSel.PrimaryMakerSOEID.Show) {
            sql += "	EPR.[SOEID]		AS [" + options.colsToSel.PrimaryMakerSOEID.Alias + "], \n";
        }

        if (options.colsToSel.PrimaryMakerName.Show) {
            sql += "	EPR.[Name]		AS [" + options.colsToSel.PrimaryMakerName.Alias + "], \n";
        }

        if (options.colsToSel.PrimaryCheckerSOEID.Show) {
            sql += "	EPC.[SOEID]		AS [" + options.colsToSel.PrimaryCheckerSOEID.Alias + "], \n";
        }

        if (options.colsToSel.PrimaryCheckerName.Show) {
            sql += "	EPC.[Name]		AS [" + options.colsToSel.PrimaryCheckerName.Alias + "], \n";
        }

        if (options.colsToSel.AuditStatus.Show) {
            sql += "	ISNULL(TAS.[Status], 'Pending')		AS [" + options.colsToSel.AuditStatus.Alias + "], \n";
        }

        if (options.colsToSel.JsonAuditLog.Show) {
            sql += "	[dbo].[fnCPRAFrwkGetJsonProcessActivityAuditLogs](N.[ID], 'eDCFC')		AS [" + options.colsToSel.JsonAuditLog.Alias + "], \n";
        }

        //Select always NodeID column
        sql += "	N.[ID]			AS [ID], \n";
        sql += "	'eDCFC'			AS [SR] \n";

        sql +=
            "FROM  \n" +
            "	[TITAN].[dbo].[Node] N WITH(READUNCOMMITTED) \n";

        //Add Join to Get Activities Only Recoverable WA
        if (options.filters.recoverableWAD == true) {
            sql +=
            "       --Get Only Recoverable WAD \n" +
            "       INNER JOIN [TITAN].[dbo].[NodeData] ND_RWAD WITH(READUNCOMMITTED) ON ND_RWAD.[NodeID] = N.[ID] AND ND_RWAD.[MetadataID] = 32 AND ND_RWAD.[Value] = 'True' \n" +
            "        \n";
        }

        //Add Join to Get Activities Only From Communication
        if (options.filters.idCommunication) {
            sql +=
            "       --Get Activities of Communication \n" +
            "       INNER JOIN [dbo].[tblCPRAFrwkCommunicationXActivity] COA WITH(READUNCOMMITTED) ON COA.[IDCommunication] = " + options.filters.idCommunication + " AND COA.[IDProcessActivity] = N.[ID] AND COA.[Source] = 'eDCFC' \n" +
            "        \n";
        }

        //Add Join to Get Activities beetween date range
        if (options.filters.startDate && options.filters.endDate) {
            sql +=
                "       --Get Activities beetween date rage \n" +
                "       INNER JOIN ( SELECT DISTINCT TE2.[NodeID] \n" +
                "                   FROM [TITAN].[dbo].[Event] TE2 WITH(READUNCOMMITTED) \n" +
                "                   WHERE TE2.[Date] BETWEEN '" + options.filters.startDate + "' AND '" + options.filters.endDate + "') AS TDR ON N.[ID] = TDR.[NodeID] \n";
        }

        //Add Join to Get Managed Segments and GOCs Information
        if (options.colsToSel.ManagedSegment.Show ||
           options.colsToSel.ManagedSegmentDesc.Show ||
           options.colsToSel.GOC.Show ||
           options.colsToSel.GOCDesc.Show) {

            sql +=
                "       --Get Only Activities with GOC \n" +
                "       INNER JOIN [TITAN].[dbo].[NodeData] ND_GOC WITH(READUNCOMMITTED) ON ND_GOC.[NodeID] = N.[ID] AND ND_GOC.[MetadataID] = 26 AND ND_GOC.[Value] IS NOT NULL \n" +
                "       LEFT JOIN [Automation].[dbo].[tblGlobalOrganizationCode] G WITH(READUNCOMMITTED) ON ND_GOC.[Value] = G.ID \n" +
                "        \n" +
                "       --Get ManagedSegment \n" +
                "       LEFT JOIN [Automation].[dbo].[tblManagedSegment] MS WITH(READUNCOMMITTED) ON G.ManagedSegmentID = MS.ID \n";
        }

        //Add Join to Get GPL
        if (options.filters.GPLSOEID) {
            sql +=
                "        --Get GPL  \n" +
                "       LEFT JOIN [Automation].[dbo].[tblEmployee] GPL WITH(READUNCOMMITTED) ON MS.[ManagerID] = GPL.[ID] \n" +
                "       \n";
        }

        //Add Join to Get RPL
        if (options.filters.RPLSOEID) {
            sql +=
                "       --Get RPL  \n" +
                "       LEFT JOIN [Automation].[dbo].[tblManagedSegmentManagedGeography] MSMG WITH(READUNCOMMITTED) ON G.ManagedSegmentID = MSMG.ManagedSegmentID AND G.ManagedGeographyID = MSMG.ManagedGeographyID   \n" + 
                "       LEFT JOIN [Automation].[dbo].[tblEmployee] RPL WITH(READUNCOMMITTED) ON MSMG.[ManagerID] = RPL.ID \n" +
                "        \n";
        }

        //Add Join to Get Primary Maker Information
        if (options.colsToSel.PrimaryMakerSOEID || options.colsToSel.PrimaryMakerName) {
            sql +=
                "       --If Activity have Group get Primary Maker \n" +
                "       LEFT JOIN [Automation].[dbo].[tblEmployee] EPR WITH(READUNCOMMITTED) ON N.[PrimaryResponsibleID] = EPR.ID \n";
        }

        //Add Join to Get Primary Checker Information
        if (options.colsToSel.PrimaryCheckerSOEID || options.colsToSel.PrimaryCheckerName) {
            sql +=
                "       --If Activity have Group get Primary Checker \n" +
                "       LEFT JOIN [Automation].[dbo].[tblEmployee] EPC WITH(READUNCOMMITTED) ON N.[PrimaryApproverID] = EPC.ID \n";
        }

        //Get Audit Status
        if (options.colsToSel.AuditStatus.Show) {
            sql +=
            "       --Get Audit Status \n" +
            "       OUTER APPLY ( \n" +
            "           SELECT TPAA.[Status] \n" +
            "           FROM [dbo].[tblCPRProcessActivityXAuditLog] TPAA \n" +
            "           WHERE TPAA.[IDProcessActivity] = N.[ID] AND TPAA.[IsLast] = 1 \n" +
            "       ) AS TAS \n" +
            "        \n";
        }

        sql +=
            "WHERE \n" +
            "     --Get only Active Activities \n" +
            "     N.[IsActive] = 1 \n" +
            "  \n" +
            "     --Get only Control Activities \n" +
            "     AND N.[CategoryID] = 3  \n" +
            "  \n";

        // Filter by GPL
        if (options.filters.GPLSOEID) {
            if (options.filters.GPLSOEID == "TBD") {
                options.filters.GPLSOEID = "";
            }

            sql +=
                "     --Filter by GPL \n" +
                "     AND ISNULL(GPL.SOEID, '') = '" + options.filters.GPLSOEID + "' \n" +
                "  \n";
        }

        // Filter by RPL
        if (options.filters.RPLSOEID) {
            if (options.filters.RPLSOEID == "TBD") {
                options.filters.RPLSOEID = "";
            }

            sql +=
                "     --Filter by RPL \n" +
                "     AND ISNULL(RPL.SOEID, '') = '" + options.filters.RPLSOEID + "' \n" +
                "  \n";
        }

        //Filter by Process Taxonomy
        if (options.filters.idProcessTaxonomy) {
            sql +=
                "     --Filter by Process Taxonomy \n" +
                "     AND 1 = 0 \n";

            //"     AND PA.[IDProcessTaxonomy] = " + options.filters.idProcessTaxonomy + " \n";
        }

        //Filter by Activities of specific employee for Communication
        if (options.filters.communicationEmployeeSOEID && options.filters.idCommunication) {
            sql +=
                "     --Filter Nodes by current User \n" +
                "     AND N.[ID] IN ( \n" +
                "         SELECT DISTINCT TCAC.[IDProcessActivity] \n" +
                "         FROM [dbo].[tblCPRAFrwkCommunicationXActivityXContact] TCAC  \n" +
                "         WHERE TCAC.[IDCommunication] = " + options.filters.idCommunication + " AND TCAC.[Source] = 'eDCFC' AND TCAC.[SOEID] = '" + options.filters.communicationEmployeeSOEID + "' \n" +
                "     ) \n";
        } else {
            //Filter by Activities of User
            if (options.filters.employeeSOEID) {
                sql +=
                    "     --Filter Nodes by current User \n" +
                    "     AND N.[ID] IN (" + strIDActivitiesOfUser + ") \n";
            }
        }

        //Filter by Location
        if (options.filters.idLocation) {
            sql +=
                "  \n" +
                "     --Get only Responsibles from Parent Location \n" +
                "     AND N.[PrimaryResponsibleID] IN ( \n" +
                "         SELECT  \n" +
                "             E.[ID] \n" +
                "         FROM  \n" +
                "             [Automation].[dbo].[tblEmployee] E WITH(READUNCOMMITTED) \n" +
                "                 --Get only FRSS Employees \n" +
                "                 --Commented because some activities have NON FRSS Employee maybe for missing update or position changed. \n" +
                "                 --INNER JOIN [Automation].[dbo].[tblProcessXManagedSegment] PM WITH(READUNCOMMITTED) ON PM.[IDProcess] = 1 AND E.[ManagedSegmentID] = PM.[ManagedSegment] \n" +
                "  \n" +
                "                 --Get Location \n" +
                "                 INNER JOIN [Automation].[dbo].[tblEmployeePhysicalLocationMap] EL WITH(READUNCOMMITTED) ON E.[WORK_COUNTRY] = EL.[WORK_COUNTRY] AND E.[WORK_CITY] = EL.[WORK_CITY] \n" +
                "         WHERE \n" +
                "             --Get only Active employees \n" +
                "             E.[EMPL_STATUS] = 'A' AND \n" +
                "  \n" +
                "             --Filter by Location \n" +
                "             EL.[ID] = " + options.filters.idLocation + " \n" +
                " ) ";
        }

        options.onReady(sql);
    }
}

function _getSQLDBListCPRActivitySummary(customOptions) {
    //Default Options.
    var options = {
        filters: _getDefaultFilters(),
        colsToSel: _getDefaultColsToSelect(),
        onReady: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    //Create SQL, Faster way to get database data
    var sql =
        "SELECT \n";

    if (options.colsToSel.IDProcessTaxonomy.Show) {
        sql += "	PT.[ID]		AS [" + options.colsToSel.IDProcessTaxonomy.Alias + "], \n";
    }

    if (options.colsToSel.ProcessTaxonomyDesc.Show) {
        sql += "	PT.[Name]		AS [" + options.colsToSel.ProcessTaxonomyDesc.Alias + "], \n";
    }

    if (options.colsToSel.ProcessTaxonomyLevel.Show) {
        sql += "	PT.[Category]		AS [" + options.colsToSel.ProcessTaxonomyLevel.Alias + "], \n";
    }

    if (options.colsToSel.ManagedSegment.Show) {
        sql += "	MS.[ID]		AS [" + options.colsToSel.ManagedSegment.Alias + "], \n";
    }

    if (options.colsToSel.ManagedSegmentDesc.Show) {
        sql += "	MS.[Description]		AS [" + options.colsToSel.ManagedSegmentDesc.Alias + "], \n";
    }

    if (options.colsToSel.GOC.Show) {
        sql += "	PA.[GOC]		AS [" + options.colsToSel.GOC.Alias + "], \n";
    }

    if (options.colsToSel.GOCDesc.Show) {
        sql += "	G.[Description]		AS [" + options.colsToSel.GOCDesc.Alias + "], \n";
    }

    if (options.colsToSel.ActivityName.Show) {
        sql += "	PA.[ActivityName]		AS [" + options.colsToSel.ActivityName.Alias + "], \n";
    }

    if (options.colsToSel.PrimaryMakerSOEID.Show) {
        sql += "	CACM.[SOEID]		AS [" + options.colsToSel.PrimaryMakerSOEID.Alias + "], \n";
    }

    if (options.colsToSel.PrimaryMakerName.Show) {
        sql += "	ISNULL(PM.[NAME], 'SOEID ' + CACM.[SOEID] + ' Not exists')		AS [" + options.colsToSel.PrimaryMakerName.Alias + "], \n";
    }

    if (options.colsToSel.PrimaryCheckerSOEID.Show) {
        sql += "	CACC.[SOEID]		AS [" + options.colsToSel.PrimaryCheckerSOEID.Alias + "], \n";
    }

    if (options.colsToSel.PrimaryCheckerName.Show) {
        sql += "	ISNULL(PC.[NAME], 'SOEID ' + CACC.[SOEID] + ' Not exists')		AS [" + options.colsToSel.PrimaryCheckerName.Alias + "], \n";
    }

    if (options.colsToSel.AuditStatus.Show) {
        sql += "	ISNULL(TAS.[Status], 'Pending')		AS [" + options.colsToSel.AuditStatus.Alias + "], \n";
    }

    if (options.colsToSel.JsonAuditLog.Show) {
        sql += "	[dbo].[fnCPRAFrwkGetJsonProcessActivityAuditLogs](PA.[ID], 'CPR')		AS [" + options.colsToSel.JsonAuditLog.Alias + "], \n";
    }

    //Select always NodeID column
    sql += "	PA.[ID]			AS [ID], \n";
    sql += "	'CPR'			AS [SR] \n";
    sql +=
        "FROM  \n" +
        "	[dbo].[tblCPRProcessActivity] PA WITH(READUNCOMMITTED) \n";

    //Add Join to Get Activities beetween date range
    if (options.filters.startDate && options.filters.endDate) {
        sql +=
            "       --Get Activities beetween date rage \n" +
            "        INNER JOIN [dbo].[tblCPRRecurrence] R ON PA.[RecurrenceCode] = R.[Code] \n" +
            "        INNER JOIN ( \n" +
			"            SELECT DISTINCT RD.[IDRecurrence] \n" +
			"            FROM [dbo].[tblCPRRecurrenceXDate] RD \n" +
			"            WHERE RD.[Date] BETWEEN '" + options.filters.startDate + "' AND '" + options.filters.endDate + "' \n" +
		    "        ) AS TDR ON R.[ID] = TDR.[IDRecurrence] \n";
    }

    //Add Join to Get Activities Only From Communication
    if (options.filters.idCommunication) {
        sql +=
        "       --Get Activities of Communication \n" +
        "       INNER JOIN [dbo].[tblCPRAFrwkCommunicationXActivity] COA WITH(READUNCOMMITTED) ON COA.[IDCommunication] = " + options.filters.idCommunication + " AND COA.[IDProcessActivity] = PA.[ID] AND COA.[Source] = 'CPR' \n" +
        "        \n";
    }

    //Add Join to Get Process Taxonomy Information
    if (options.colsToSel.IDProcessTaxonomy ||
        options.colsToSel.ProcessTaxonomyDesc ||
        options.colsToSel.ProcessTaxonomyLevel) {

        sql +=
            "       --Get Process Taxonomy Information \n" +
            "       LEFT JOIN [Automation].[dbo].[tblProcess] PT WITH(READUNCOMMITTED) ON PA.[IDProcessTaxonomy] = PT.[ID] \n";
    }

    //Add Join to Get Managed Segments and GOCs Information
    if (options.colsToSel.ManagedSegment ||
       options.colsToSel.ManagedSegmentDesc ||
       options.colsToSel.GOC ||
       options.colsToSel.GOCDesc) {

        sql +=
            "       --Get GOC \n" +
            "       LEFT JOIN [Automation].[dbo].[tblGlobalOrganizationCode] G WITH(READUNCOMMITTED) ON PA.[GOC] = G.ID  \n" +
            "        \n" +
            "       --Get ManagedSegment \n" +
            "       LEFT JOIN [Automation].[dbo].[tblManagedSegment] MS WITH(READUNCOMMITTED) ON PA.[ManagedSegment] = MS.ID \n";
    }

    //Add Join to Get GPL
    if (options.filters.GPLSOEID) {
        sql +=
            "        --Get GPL  \n" +
            "       LEFT JOIN [Automation].[dbo].[tblEmployee] GPL WITH(READUNCOMMITTED) ON MS.[ManagerID] = GPL.[ID] \n" +
            "       \n";
    }

    //Add Join to Get RPL
    if (options.filters.RPLSOEID) {
        sql +=
            "       --Get RPL  \n" +
            "       LEFT JOIN [Automation].[dbo].[tblManagedSegmentManagedGeography] MSMG WITH(READUNCOMMITTED) ON G.ManagedSegmentID = MSMG.ManagedSegmentID AND G.ManagedGeographyID = MSMG.ManagedGeographyID   \n" +
            "       LEFT JOIN [Automation].[dbo].[tblEmployee] RPL WITH(READUNCOMMITTED) ON MSMG.[ManagerID] = RPL.ID \n" +
            "        \n";
    }

    //Add Join to Get Primary Maker Information
    if (options.colsToSel.PrimaryMakerSOEID || options.colsToSel.PrimaryMakerName) {
        sql +=
            "       --If Get Primary Maker \n" +
            "       LEFT JOIN [dbo].[tblCPRProcessActivityXContact] CACM ON PA.[ID] = CACM.[IDProcessActivity] AND CACM.[IsDeleted] = 0 AND CACM.[IsFirst] = 1 AND CACM.[ContactType] = 'Maker' \n" +
            "       LEFT JOIN [Automation].[dbo].[tblEmployee] PM WITH(READUNCOMMITTED) ON CACM.[SOEID] = PM.[SOEID] \n";
    }

    //Add Join to Get Primary Checker Information
    if (options.colsToSel.PrimaryCheckerSOEID || options.colsToSel.PrimaryCheckerName) {
        sql +=
            "       --If Get Primary Checker \n" +
            "       LEFT JOIN [dbo].[tblCPRProcessActivityXContact] CACC ON PA.[ID] = CACC.[IDProcessActivity] AND CACC.[IsDeleted] = 0 AND CACC.[IsFirst] = 1 AND CACC.[ContactType] = 'Checker' \n" +
            "       LEFT JOIN [Automation].[dbo].[tblEmployee] PC WITH(READUNCOMMITTED) ON CACC.[SOEID] = PC.[SOEID] \n";
    }

    //Get Audit Status
    if (options.colsToSel.AuditStatus.Show) {
        sql +=
        "       --Get Audit Status \n" +
        "       OUTER APPLY ( \n" +
        "           SELECT TPAA.[Status] \n" +
        "           FROM [dbo].[tblCPRProcessActivityXAuditLog] TPAA \n" +
        "           WHERE TPAA.[IDProcessActivity] = PA.[ID] AND TPAA.[IsLast] = 1 \n" +
        "       ) AS TAS \n" +
        "        \n";
    }

    sql +=
        "WHERE \n" +
        "     --Get only Active Activities \n" +
        "     PA.[IsDeleted] = 0 \n" +
        "  \n";

    // Filter by GPL
    if (options.filters.GPLSOEID) {
        if (options.filters.GPLSOEID == "TBD") {
            options.filters.GPLSOEID = "";
        }

        sql +=
            "     --Filter by GPL \n" +
            "     AND ISNULL(GPL.SOEID, '') = '" + options.filters.GPLSOEID + "' \n" +
            "  \n";
    }

    // Filter by RPL
    if (options.filters.RPLSOEID) {
        if (options.filters.RPLSOEID == "TBD") {
            options.filters.RPLSOEID = "";
        }

        sql +=
            "     --Filter by RPL \n" +
            "     AND ISNULL(RPL.SOEID, '') = '" + options.filters.RPLSOEID + "' \n" +
            "  \n";
    }

    //Filter by Process Taxonomy
    if (options.filters.idProcessTaxonomy) {
        sql +=
            "     --Filter by Process Taxonomy \n" +
            "     AND PA.[IDProcessTaxonomy] = " + options.filters.idProcessTaxonomy + " \n";
    }

    //Filter by Activities of specific employee for Communication
    if (options.filters.communicationEmployeeSOEID && options.filters.idCommunication) {
        sql +=
            "     --Filter Nodes by current User \n" +
            "     AND PA.[ID] IN ( \n" +
            "         SELECT DISTINCT TCAC.[IDProcessActivity] \n" +
			"         FROM [dbo].[tblCPRAFrwkCommunicationXActivityXContact] TCAC  \n" +
			"         WHERE TCAC.[IDCommunication] = " + options.filters.idCommunication + " AND TCAC.[Source] = 'CPR' AND TCAC.[SOEID] = '" + options.filters.communicationEmployeeSOEID + "' \n" +
		    "     ) \n";
    } else {
        if (options.filters.employeeSOEID) {
            sql +=
                "     --Filter Nodes by current User \n" +
                "     AND PA.[ID] IN ( \n" +
                "         SELECT DISTINCT TPAC.[IDProcessActivity] \n" +
                "         FROM [dbo].[tblCPRProcessActivityXContact] TPAC  \n" +
                "               INNER JOIN [Automation].[dbo].[tblEmployee] TPACE WITH(READUNCOMMITTED) ON TPAC.[SOEID] = TPACE.[SOEID]   \n" +
                "               INNER JOIN [Automation].[dbo].[tblEmployee] TPACEM WITH(READUNCOMMITTED) ON TPACE.[DirectManagerID] = TPACEM.[ID]  \n" +
                "         WHERE (TPACE.[SOEID] = '" + options.filters.employeeSOEID + "' OR TPACEM.[SOEID] = '" + options.filters.employeeSOEID + "') AND TPAC.[IsDeleted] = 0 \n" +
                "     ) \n";
        }
    }

    //Filter by Location
    if (options.filters.idLocation) {
        sql +=
            "     --Filter by Physical Location \n" +
            "     AND PA.[IDPhysicalLocation] = " + options.filters.idLocation + " \n";
    }

    options.onReady(sql);
}

function _getBDObjProcessActivityInfo(customOptions) {
    //Default Options.
    var options = {
        filters: {
            source: "eDCFC",
            idProcessActivity: ""
        },
        onReady: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    if (options.filters.source == "eDCFC") {
        //Get Activity Info from DB for eDCFC
        _getBDObjEDCFCProcessActivityInfo({
            filters: options.filters,
            onReady: function (objActivityInfo) {
                options.onReady(objActivityInfo);
            }
        });
    }

    if (options.filters.source == "CPR") {
        //Get Activity Info from DB for CPR
        _getBDObjCPRProcessActivityInfo({
            filters: options.filters,
            onReady: function (objActivityInfo) {
                options.onReady(objActivityInfo);
            }
        });
    }
}

function _getBDObjCPRProcessActivityInfo(customOptions) {
    //Default Options.
    var options = {
        filters: {
            idProcessActivity: ""
        },
        onReady: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    //Create SQL, Faster way to get database data
    var sql =
        "SELECT \n" +
        "    'CPR'								    AS SOURCE, \n" +
        "    GPL.GEID								AS GPL_GEID, \n" +
	    "    GPL.SOEID								AS GPL_SOEID, \n" +
	    "    GPL.NAME								AS GPL_NAME, \n" +
	    "    GPL.EMAIL_ADDR							AS GPL_EMAIL, \n" +
	    "    GPL.WORK_PHONE							AS GPL_WORK_PHONE, \n" +
	    "    RPL.GEID								AS RPL_GEID, \n" +
	    "    RPL.SOEID								AS RPL_SOEID, \n" +
	    "    RPL.NAME								AS RPL_NAME, \n" +
	    "    RPL.EMAIL_ADDR							AS RPL_EMAIL, \n" +
	    "    RPL.WORK_PHONE							AS RPL_WORK_PHONE, \n" +
	    "    G.[ID]									AS GOC, \n" +
	    "    G.[Description]						AS GOC_DESCRIPTION, \n" +
	    "    G.[CountryID]							AS GOC_COUNTRY_ID, \n" +
	    "    MS.[ID]								AS MANAGED_SEGMENT, \n" +
	    "    MS.[Description]						AS MANAGED_SEGMENT_DESCRIPTION, \n" +
	    "    PA.[ID]								AS ACTIVITY_ID, \n" +
	    "    PA.[ActivityName]						AS ACTIVITY_NAME, \n" +
	    "    TOwner.[ID]							AS OWNER_ID, \n" +
	    "    TOwner.[Name]							AS OWNER_NAME, \n" +
	    "    TOwner.[Email]							AS OWNER_EMAIL, \n" +
	    "    TOwner.[JsonUsers]						AS OWNER_JSON_USERS, \n" +
	    "    TOwner.[UserCount]						AS OWNER_USER_COUNT, \n" +
	    "    TOwner.[Type]							AS OWNER_TYPE, \n" +
	    "    TMaker.[ID]							AS MAKER_ID, \n" +
	    "    TMaker.[Name]							AS MAKER_NAME, \n" +
	    "    TMaker.[Email]							AS MAKER_EMAIL, \n" +
	    "    TMaker.[JsonUsers]						AS MAKER_JSON_USERS, \n" +
	    "    TMaker.[UserCount]						AS MAKER_USER_COUNT, \n" +
	    "    TMaker.[Type]							AS MAKER_TYPE, \n" +
	    "    TChecker.[ID]							AS CHECKER_ID, \n" +
	    "    TChecker.[Name]						AS CHECKER_NAME, \n" +
	    "    TChecker.[Email]						AS CHECKER_EMAIL, \n" +
	    "    TChecker.[JsonUsers]					AS CHECKER_JSON_USERS, \n" +
	    "    TChecker.[UserCount]					AS CHECKER_USER_COUNT, \n" +
	    "    TChecker.[Type]						AS CHECKER_TYPE, \n" +
	    "    TWADOwner.[ID]							AS WAD_OWNER_ID, \n" +
	    "    TWADOwner.[Name]						AS WAD_OWNER_NAME, \n" +
	    "    TWADOwner.[Email]						AS WAD_OWNER_EMAIL, \n" +
	    "    TWADOwner.[JsonUsers]					AS WAD_OWNER_JSON_USERS, \n" +
	    "    TWADOwner.[UserCount]					AS WAD_OWNER_USER_COUNT, \n" +
	    "    TWADOwner.[Type]						AS WAD_OWNER_TYPE, \n" +
	    "    TWADMaker.[ID]							AS WAD_MAKER_ID, \n" +
	    "    TWADMaker.[Name]						AS WAD_MAKER_NAME, \n" +
	    "    TWADMaker.[Email]						AS WAD_MAKER_EMAIL, \n" +
	    "    TWADMaker.[JsonUsers]					AS WAD_MAKER_JSON_USERS, \n" +
	    "    TWADMaker.[UserCount]					AS WAD_MAKER_USER_COUNT, \n" +
	    "    TWADMaker.[Type]						AS WAD_MAKER_TYPE, \n" +
	    "    TWADChecker.[ID]						AS WAD_CHECKER_ID, \n" +
	    "    TWADChecker.[Name]						AS WAD_CHECKER_NAME, \n" +
	    "    TWADChecker.[Email]					AS WAD_CHECKER_EMAIL, \n" +
	    "    TWADChecker.[JsonUsers]				AS WAD_CHECKER_JSON_USERS, \n" +
	    "    TWADChecker.[UserCount]				AS WAD_CHECKER_USER_COUNT, \n" +
	    "    TWADChecker.[Type]						AS WAD_CHECKER_TYPE, \n" +
	    "    [dbo].[fnCPRAFrwkGetJsonDocs](PA.[ID])	AS JSON_DOCS \n" +
        "FROM  \n" +
        "    [dbo].[tblCPRProcessActivity] PA WITH(READUNCOMMITTED)  \n" +
        "    --Get GOC  \n" +
        "    LEFT JOIN [Automation].[dbo].[tblGlobalOrganizationCode] G WITH(READUNCOMMITTED) ON PA.[GOC] = G.ID \n" +
        " \n" +
        "    --Get GPL  \n" +
        "    LEFT JOIN [Automation].[dbo].[tblManagedSegment] MS WITH(READUNCOMMITTED) ON G.ManagedSegmentID = MS.ID  \n" +
        "    LEFT JOIN [Automation].[dbo].[tblEmployee] GPL WITH(READUNCOMMITTED) ON MS.[ManagerID] = GPL.[ID]  \n" +
        " \n" +
        "    --Get RPL  \n" +
        "    LEFT JOIN [Automation].[dbo].[tblManagedSegmentManagedGeography] MSMG WITH(READUNCOMMITTED) ON G.ManagedSegmentID = MSMG.ManagedSegmentID AND G.ManagedGeographyID = MSMG.ManagedGeographyID   \n" +
        "    LEFT JOIN [Automation].[dbo].[tblEmployee] RPL WITH(READUNCOMMITTED) ON MSMG.[ManagerID] = RPL.ID  \n" +
        " \n" +
        "    --Get Contact Info Owner \n" +
        "    OUTER APPLY( \n" +
        "        SELECT * FROM [dbo].[fnCPRAFrwkGetContactInfo](PA.[ID], 'Owner') \n" +
        "    ) AS TOwner \n" +
        " \n" +
        "    --Get Contact Info Maker \n" +
        "    OUTER APPLY( \n" +
        "        SELECT * FROM [dbo].[fnCPRAFrwkGetContactInfo](PA.[ID], 'Maker') \n" +
        "    ) AS TMaker \n" +
        " \n" +
        "    --Get Contact Info Checker \n" +
        "    OUTER APPLY( \n" +
        "        SELECT * FROM [dbo].[fnCPRAFrwkGetContactInfo](PA.[ID], 'Checker') \n" +
        "    ) AS TChecker \n" +
        " \n" +
        "    --Get Contact Info WAD Owner \n" +
        "    OUTER APPLY( \n" +
        "        SELECT * FROM [dbo].[fnCPRAFrwkGetContactInfo](PA.[ID], 'WAD Owner') \n" +
        "    ) AS TWADOwner \n" +
        " \n" +
        "    --Get Contact Info WAD Maker \n" +
        "    OUTER APPLY( \n" +
        "        SELECT * FROM [dbo].[fnCPRAFrwkGetContactInfo](PA.[ID], 'WAD Maker') \n" +
        "    ) AS TWADMaker \n" +
        " \n" +
        "    --Get Contact Info WAD Checker \n" +
        "    OUTER APPLY( \n" +
        "        SELECT * FROM [dbo].[fnCPRAFrwkGetContactInfo](PA.[ID], 'WAD Checker') \n" +
        "    ) AS TWADChecker \n" +
        " \n" +
        "WHERE  \n" +
        "    --Get only Active Activities  \n" +
        "    PA.[IsDeleted] = 0  \n" +
        " \n" +
        "    --Filter by Node ID \n" +
        "    AND PA.[ID] = " + options.filters.idProcessActivity + " \n";

    //Load process information
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading CPR Process Activity Summary...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': _toJSON(sql) },
        type: "POST",
        success: function (resultList) {

            if (options.onReady) {
                if (resultList.length == 1) {
                    options.onReady(resultList[0]);
                } else {
                    options.onReady(resultList);
                }
            }

        }
    });
}

function _getBDObjEDCFCProcessActivityInfo(customOptions) {
    //Default Options.
    var options = {
        filters: {
            idProcessActivity: ""
        },
        onReady: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    //Create SQL, Faster way to get database data
    var sql =
        "SELECT  \n" +
        "   'eDCFC'						AS SOURCE, \n" +
        "	GPL.GEID			        AS GPL_GEID, \n" +
        "	GPL.SOEID			        AS GPL_SOEID, \n" +
        "	GPL.NAME			        AS GPL_NAME, \n" +
        "	GPL.EMAIL_ADDR		        AS GPL_EMAIL, \n" +
        "	GPL.WORK_PHONE		        AS GPL_WORK_PHONE, \n" +
        "	RPL.GEID			        AS RPL_GEID, \n" +
        "	RPL.SOEID			        AS RPL_SOEID, \n" +
        "	RPL.NAME			        AS RPL_NAME, \n" +
        "	RPL.EMAIL_ADDR		        AS RPL_EMAIL, \n" +
        "	RPL.WORK_PHONE		        AS RPL_WORK_PHONE, \n" +
        "	G.[ID]				        AS GOC, \n" +
        "	G.[Description]		        AS GOC_DESCRIPTION, \n" +
        "	G.[CountryID]		        AS GOC_COUNTRY_ID, \n" +
        "	MS.[ID]				        AS MANAGED_SEGMENT, \n" +
        "	MS.[Description]	        AS MANAGED_SEGMENT_DESCRIPTION, \n" +
        "	N.[ID]				        AS ACTIVITY_ID, \n" +
        "	N.[Name]			        AS ACTIVITY_NAME, \n" +

        "	TOWNER.OWNER_ID, \n" +
        "	TOWNER.OWNER_NAME, \n" +
        "	TOWNER.OWNER_EMAIL, \n" +
        "	TOWNER.OWNER_JSON_USERS, \n" +
        "	TOWNER.OWNER_TYPE, \n" +

        "	TMAKER.MAKER_ID, \n" +
        "	TMAKER.MAKER_NAME, \n" +
        "	TMAKER.MAKER_EMAIL, \n" +
        "	TMAKER.MAKER_JSON_USERS, \n" +
        "	TMAKER.MAKER_TYPE, \n" +

        "	EPR.[SOEID]                                 AS PRIMARY_MAKER_SOEID, \n" +
        "	EPR.[Name]                                  AS PRIMARY_MAKER_NAME, \n" +

        "	TCHECKER.CHECKER_ID, \n" +
        "	TCHECKER.CHECKER_NAME, \n" +
        "	TCHECKER.CHECKER_EMAIL, \n" +
        "	TCHECKER.CHECKER_JSON_USERS, \n" +
        "	TCHECKER.CHECKER_TYPE, \n" +

        "	EPC.[SOEID]		                            AS PRIMARY_CHECKER_SOEID, \n" +
        "	EPC.[Name]		                            AS PRIMARY_CHECKER_NAME, \n" +

        "   WADOwner.[SOEID]			                AS WAD_OWNER_SOEID, \n" +
	    "   WADOwner.[Name]				                AS WAD_OWNER_NAME, \n" +
        "   WADOwnerM.[SOEID]			                AS WAD_OWNER_MANAGER_SOEID, \n" +
	    "   WADOwnerM.[Name]				            AS WAD_OWNER_MANAGER_NAME, \n" +

	    "   WADMaker.[SOEID]			                AS WAD_MAKER_SOEID, \n" +
	    "   WADMaker.[Name]				                AS WAD_MAKER_NAME, \n" +
        "   WADMakerM.[SOEID]			                AS WAD_MAKER_MANAGER_SOEID, \n" +
	    "   WADMakerM.[Name]				            AS WAD_MAKER_MANAGER_NAME, \n" +

	    "   WADChecker.[SOEID]			                AS WAD_CHECKER_SOEID, \n" +
	    "   WADChecker.[Name]			                AS WAD_CHECKER_NAME, \n" +
        "   WADCheckerM.[SOEID]			                AS WAD_CHECKER_MANAGER_SOEID, \n" +
	    "   WADCheckerM.[Name]			                AS WAD_CHECKER_MANAGER_NAME, \n" +

        "   [TITAN].[dbo].[func_Get_JsonDocs](N.[ID])   AS JSON_DOCS \n" +
        "FROM \n" +
        "    [TITAN].[dbo].[Node] N WITH(READUNCOMMITTED) \n" +
        "        LEFT JOIN [TITAN].[dbo].[NodeData] ND_GOC WITH(READUNCOMMITTED) ON ND_GOC.[NodeID] = N.[ID] AND ND_GOC.[MetadataID] = 26 AND ND_GOC.[Value] IS NOT NULL \n" +
        "        LEFT JOIN [Automation].[dbo].[tblGlobalOrganizationCode] G WITH(READUNCOMMITTED) ON ND_GOC.[Value] = G.ID \n" +
        " \n" +
        "        --Get GPL \n" +
        "        LEFT JOIN [Automation].[dbo].[tblManagedSegment] MS WITH(READUNCOMMITTED) ON G.ManagedSegmentID = MS.ID \n" +
        "        LEFT JOIN [Automation].[dbo].[tblEmployee] GPL WITH(READUNCOMMITTED) ON MS.[ManagerID] = GPL.[ID] \n" +
        " \n" +
        "        --Get RPL \n" +
        "        LEFT JOIN [Automation].[dbo].[tblManagedSegmentManagedGeography] MSMG WITH(READUNCOMMITTED) ON G.ManagedSegmentID = MSMG.ManagedSegmentID AND G.ManagedGeographyID = MSMG.ManagedGeographyID  \n" +
        "        LEFT JOIN [Automation].[dbo].[tblEmployee] RPL WITH(READUNCOMMITTED) ON MSMG.[ManagerID] = RPL.ID \n" +
        " \n" +
        "       --Get WAD Owner \n" +
        "       LEFT JOIN [TITAN].[dbo].[NodeData] ND_WADOwner WITH(READUNCOMMITTED) ON ND_WADOwner.[NodeID] = N.[ID] AND ND_WADOwner.[MetadataID] = 37 AND ND_WADOwner.[Value] IS NOT NULL \n" +
        "       LEFT JOIN [Automation].[dbo].[tblEmployee] WADOwner WITH(READUNCOMMITTED) ON ND_WADOwner.[Value] = WADOwner.ID \n" +
        "       LEFT JOIN [Automation].[dbo].[tblEmployee] WADOwnerM WITH(READUNCOMMITTED) ON WADOwner.[DirectManagerID] = WADOwnerM.ID \n" +
        " \n" +
        "       --Get WAD Primary Maker \n" +
        "       LEFT JOIN [TITAN].[dbo].[NodeData] ND_WADMaker WITH(READUNCOMMITTED) ON ND_WADMaker.[NodeID] = N.[ID] AND ND_WADMaker.[MetadataID] = 35 AND ND_WADMaker.[Value] IS NOT NULL \n" +
        "       LEFT JOIN [Automation].[dbo].[tblEmployee] WADMaker WITH(READUNCOMMITTED) ON ND_WADMaker.[Value] = WADMaker.ID \n" +
        "       LEFT JOIN [Automation].[dbo].[tblEmployee] WADMakerM WITH(READUNCOMMITTED) ON WADMaker.[DirectManagerID] = WADMakerM.ID \n" +
        " \n" +
        "       --Get WAD Primary Checker \n" +
        "       LEFT JOIN [TITAN].[dbo].[NodeData] ND_WADChecker WITH(READUNCOMMITTED) ON ND_WADChecker.[NodeID] = N.[ID] AND ND_WADChecker.[MetadataID] = 45 AND ND_WADChecker.[Value] IS NOT NULL \n" +
        "       LEFT JOIN [Automation].[dbo].[tblEmployee] WADChecker WITH(READUNCOMMITTED) ON ND_WADChecker.[Value] = WADChecker.ID \n" +
        "       LEFT JOIN [Automation].[dbo].[tblEmployee] WADCheckerM WITH(READUNCOMMITTED) ON WADChecker.[DirectManagerID] = WADCheckerM.ID \n" +
        " \n" +
        "        --Get Primary Maker \n" +
        "        LEFT JOIN [Automation].[dbo].[tblEmployee] EPR WITH(READUNCOMMITTED) ON N.[PrimaryResponsibleID] = EPR.ID \n" +
        " \n" +
        "        --Get Primary Checker \n" +
        "        LEFT JOIN [Automation].[dbo].[tblEmployee] EPC WITH(READUNCOMMITTED) ON N.[PrimaryApproverID] = EPC.ID \n" +
        " \n" +
        "        --Get Node Owner, can be some specific user or group \n" +
        "        --================================================================================== \n" +
        "        OUTER APPLY( \n" +
        "            SELECT  \n" +
        "                T1G.ID											    AS OWNER_ID, \n" +
        "                T1G.Name										    AS OWNER_NAME, \n" +
        "                ISNULL(T1G.DistributionList, 'DL NOT FOUND')       AS OWNER_EMAIL, \n" +
        "                [TITAN].[dbo].[func_Get_JsonUsersGroup](T1G.ID)    AS OWNER_JSON_USERS, \n" +
        "                'Group'										    AS OWNER_TYPE \n" +
        "            FROM \n" +
        "                [TITAN].[dbo].[Group] T1G WITH(READUNCOMMITTED) \n" +
        "            WHERE \n" +
        "                T1G.[IsActive] = 1 AND \n" +
        "                T1G.[ID] >= 90000000000 AND \n" +
        "                T1G.[ID] = N.[OwnerID] \n" +
        "            UNION \n" +
        "            SELECT \n" +
        "                E.ID										        AS OWNER_ID, \n" +
        "                --E.SOEID									        AS OWNER_SOEIDs, \n" +
        "                E.NAME + ' - [' + E.SOEID + ']'			        AS OWNER_NAME, \n" +
        "                E.EMAIL_ADDR								        AS OWNER_EMAIL, \n" +
        "                [TITAN].[dbo].[func_Get_JsonUsersGroup](E.ID)      AS OWNER_JSON_USERS, \n" +
        "                'User'										        AS OWNER_TYPE \n" +
        "            FROM \n" +
        "                [Automation].[dbo].[tblEmployee] E WITH(READUNCOMMITTED) \n" +
        "            WHERE \n" +
        "                E.[ID] = N.[OwnerID] \n" +
        "	    ) AS TOWNER \n" +
        " \n" +
        "        --Get Node Maker, can be some specific user or group \n" +
        "        --================================================================================== \n" +
        "        OUTER APPLY( \n" +
        "            SELECT  \n" +
        "                T1G.ID											    AS MAKER_ID, \n" +
        "                T1G.Name										    AS MAKER_NAME, \n" +
        "                ISNULL(T1G.DistributionList, 'DL NOT FOUND')       AS MAKER_EMAIL, \n" +
        "                [TITAN].[dbo].[func_Get_JsonUsersGroup](T1G.ID)    AS MAKER_JSON_USERS, \n" +
        "                'Group'										    AS MAKER_TYPE \n" +
        "            FROM \n" +
        "                [TITAN].[dbo].[Group] T1G WITH(READUNCOMMITTED) \n" +
        "            WHERE \n" +
        "                T1G.[IsActive] = 1 AND \n" +
        "                T1G.[ID] >= 90000000000 AND \n" +
        "                T1G.[ID] = N.[ResponsibleID] \n" +
        "            UNION \n" +
        "            SELECT \n" +
        "                E.ID										        AS MAKER_ID, \n" +
        "                --E.SOEID									        AS MAKER_SOEIDs, \n" +
        "                E.NAME + ' - [' + E.SOEID + ']'			        AS MAKER_NAME, \n" +
        "                E.EMAIL_ADDR								        AS MAKER_EMAIL, \n" +
        "                [TITAN].[dbo].[func_Get_JsonUsersGroup](E.ID)      AS MAKER_JSON_USERS, \n" +
        "                'User'										        AS MAKER_TYPE \n" +
        "            FROM \n" +
        "                [Automation].[dbo].[tblEmployee] E WITH(READUNCOMMITTED) \n" +
        "            WHERE \n" +
        "                E.[ID] = N.[ResponsibleID] \n" +
        "	    ) AS TMAKER \n" +
        " \n" +
        "        --Get Node Checker, can be some specific user or group \n" +
        "        --================================================================================== \n" +
        "        OUTER APPLY( \n" +
        "            SELECT  \n" +
        "                T1G.ID											    AS CHECKER_ID, \n" +
        "                T1G.Name										    AS CHECKER_NAME, \n" +
        "                ISNULL(T1G.DistributionList, 'DL NOT FOUND')       AS CHECKER_EMAIL, \n" +
        "                [TITAN].[dbo].[func_Get_JsonUsersGroup](T1G.ID)    AS CHECKER_JSON_USERS, \n" +
        "                'Group'										    AS CHECKER_TYPE \n" +
        "            FROM \n" +
        "                [TITAN].[dbo].[Group] T1G WITH(READUNCOMMITTED) \n" +
        "            WHERE \n" +
        "                T1G.[IsActive] = 1 AND \n" +
        "                T1G.[ID] >= 90000000000 AND \n" +
        "                T1G.[ID] = N.[ApproverID] \n" +
        "            UNION \n" +
        "            SELECT \n" +
        "                E.ID										        AS CHECKER_ID, \n" +
        "                --E.SOEID									        AS CHECKER_SOEIDs, \n" +
        "                E.NAME + ' - [' + E.SOEID + ']'			        AS CHECKER_NAME, \n" +
        "                E.EMAIL_ADDR								        AS CHECKER_EMAIL, \n" +
        "                [TITAN].[dbo].[func_Get_JsonUsersGroup](E.ID)      AS CHECKER_JSON_USERS, \n" +
        "                'User'										        AS CHECKER_TYPE \n" +
        "            FROM \n" +
        "                [Automation].[dbo].[tblEmployee] E WITH(READUNCOMMITTED) \n" +
        "            WHERE \n" +
        "                E.[ID] = N.[ApproverID] \n" +
        "	    ) AS TCHECKER \n" +
        " \n" +
        "WHERE \n" +
        "    --Filter by Node ID \n" +
        "    N.[ID] = " + options.filters.idProcessActivity + " \n";

    //Load process information
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading eDCFC Process Activity Summary...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': _toJSON(sql) },
        type: "POST",
        success: function (resultList) {

            if (options.onReady) {
                if (resultList.length == 1) {
                    options.onReady(resultList[0]);
                } else {
                    options.onReady(resultList);
                }
            }

        }
    });
}