/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="CPRGlobal.js" />
/// <reference path="../../Shared/plugins/citi-process-tree/process-tree-v1.0.0.js" />

$(document).ready(function () {
    //On Click Load Table Button
    $(".btnLoadTable").click(function () {
        //Load Table
        loadProcessActivityTable();
    });

    //On Click View Info
    $(".btnViewInfo").click(function () {
        var selectedRow = $.jqxGridApi.getOneSelectedRow("#tblProcessActivities", true);
        if (selectedRow) {
            _openModalProcessActivityInfo({
                filters: {
                    source: selectedRow.SR,
                    idProcessActivity: selectedRow.ID
                },
                onReady: function () { }
            });
        }
    });

    //On Click Download CPR Template
    $(".btnDownloadCPRTemplate").click(function () {
        var idProcessTaxonomy = $(".fieldIDProcess").attr("value");
        if (idProcessTaxonomy && idProcessTaxonomy != "0") {
            _downloadExcel({
                spName: "[eDCFC].[dbo].[spCPRAFrwkDownloadExcelProcessActivity]",
                spParams: [
                    { "Name": "@IDProcessTaxonomy", "Value": $(".fieldIDProcess").attr("value") }
                ],
                filename: "CPRTemplate-" + moment().format('MMM-DD-YYYY'),
                sheetname: "TOUPLOAD",
                success: {
                    msg: "Please wait, generating report..."
                }
            });
        } else {
            _showAlert({
                id: "UploadActivityMessage",
                type: "error",
                content: "Please select Process Tree"
            });
        }
    });

    //On Click Delete CPR Activities
    $(".btnDeleteCPRActivities").click(function () {
        deleteCPRActivities();
    });

    //Upload Fields
    if ($("#HFViewType").val() == "Upload") {
        // initialize jqxDropDownProcess
        $(".jqxDropDownProcess").jqxDropDownButton({
            width: "100%", height: 34
        });

        //Set "--Select Process --" in dropdown.
        var dropDownContent = '<div class="fieldIDProcess" value="0" style="padding: 8px;">-- Select --</div>';
        $(".jqxDropDownProcess").jqxDropDownButton('setContent', dropDownContent);

        // on click clear parent 
        $(".btnClearParent").click(function () {
            //Clear Parent Data
            $(".jqxDropDownProcess").jqxDropDownButton('setContent', '<div class="fieldIDProcess" value="0" style="padding: 8px;">-- Select --</div>');

            //Refresh Process Activities
            loadProcessActivityTable();
        });

        //Get Process Taxonomy Information
        _getProcessData({
            showAll: true,
            onReady: function (processListTable) {
                //Create Process Tree
                _createProcessTable({
                    idTable: "#jqxDropDownActivitiesToUpload",
                    simple: true,
                    processList: processListTable,
                    onReady: function () {
                        var $jqxGrid = $("#jqxDropDownActivitiesToUpload");

                        //On select row event
                        $jqxGrid.on('cellclick', function (event) {
                            var rowSelectEvent = setInterval(function () {

                                var args = event.args;
                                if ($jqxGrid.attr("ClickedRowBtnChild") != args.rowindex) {
                                    var row = $jqxGrid.jqxGrid('getrowdata', args.rowindex);

                                    var dropDownContent = '<div class="fieldIDProcess" value="' + row.ID + '" style="padding: 8px;">' + (row.Key ? (row.Key + ": ") : "") + 'L' + row.Level + ' ' + row.Name + '</div>';
                                    $(".jqxDropDownProcess").jqxDropDownButton('setContent', dropDownContent);
                                    $(".jqxDropDownProcess").jqxDropDownButton('close');

                                    //Update child process Level
                                    var newLevel = (row.Level + 1);
                                    $(".fieldLevel").html("L" + newLevel);
                                    $(".fieldLevel").attr("value", newLevel);

                                    //Refresh Process Activities
                                    loadProcessActivityTable();
                                } else {
                                    $jqxGrid.attr("ClickedRowBtnChild", "0");
                                }

                                clearInterval(rowSelectEvent);
                            }, 200);
                            //Close DropDown
                            //$(".jqxDropDownProcess").jqxDropDownButton('close');
                        });
                    }
                });
            }
        });

        //Create to import excel
        XLSX.createJsXlsx({
            idElement: "js-xlsx-plugin",
            fileInput: {
                dropText: "Drop an Excel file here...",
                hideDrop: false,
                inputText: "... or click here to select a file"
            },
            onSuccess: function (excelSource) {
                //console.log(excelSource);

                isValidExcel({
                    excelSource: excelSource,
                    onSuccess: function (excelSourceValidated) {
                        //console.log($(".jqxDropDownProcess").find(".fieldIDProcess").attr("value"), excelSource["TOUPLOAD"]);

                        _callServer({
                            loadingMsgType: "fullLoading",
                            loadingMsg: "Uploading data...",
                            url: '/CPR/UploadProcessActivity',
                            data: {
                                'pjsonData': JSON.stringify(excelSourceValidated["TOUPLOAD"]),
                                'pidProcess': $(".jqxDropDownProcess").find(".fieldIDProcess").attr("value")
                            },
                            type: "post",
                            success: function (json) {
                                //Clear input file
                                $(".jsxlsx-input-file").val("");

                                //Check if file exist
                                _showAlert({
                                    id: "UploadActivityMessage",
                                    type: 'success',
                                    content: "Data was uploaded successfully.",
                                    animateScrollTop: true
                                });

                                //Refresh Grid
                                loadProcessActivityTable();
                            },
                            error: function () {
                                //Clear input file
                                $(".jsxlsx-input-file").val("");
                            }
                        });
                    },
                    onError: function (excelSourceValidated) {

                        //Clear input file
                        $(".jsxlsx-input-file").val("");
                    }
                });

                //Clear input file
                $(".jsxlsx-input-file").val("");
            }
        });
    } else {

        //Load GPL Select
        loadSelectGPL();

        //Load RPL Select
        loadSelectRPL();

        //Load Analyst Select
        loadSelectAnalyst();
    }

    //Load Process Activity Table
    loadProcessActivityTable();
});

function loadSelectGPL() {
    //Clear Options
    $("#selGPL").contents().remove();

    _getDBListGPLs({
        onReady: function (resultList) {
            //Add Default Option
            $("#selGPL").append($('<option>', {
                value: "",
                text: "-- Select --"
            }));

            //Add Options
            for (var i = 0; i < resultList.length; i++) {
                var objTemp = resultList[i];
                var optionText = objTemp.NAME;

                if (objTemp.SOEID && objTemp.SOEID != "TBD") {
                    optionText += ' [' + objTemp.SOEID + ']';
                }

                $("#selGPL").append($('<option>', {
                    value: objTemp.SOEID,
                    text: optionText
                }));
            }

            //Add On Change Event
            $("#selGPL").change(function () {
                loadProcessActivityTable();
            });

            //Create Select2
            $("#selGPL").select2();
        }
    });
}

function loadSelectRPL() {
    //Clear Options
    $("#selRPL").contents().remove();

    _getDBListRPLs({
        onReady: function (resultList) {
            //Add Default Option
            $("#selRPL").append($('<option>', {
                value: "",
                text: "-- Select --"
            }));

            //Add Options
            for (var i = 0; i < resultList.length; i++) {
                var objTemp = resultList[i];
                var optionText = objTemp.NAME;

                if (objTemp.SOEID && objTemp.SOEID != "TBD") {
                    optionText += ' [' + objTemp.SOEID + ']';
                }

                $("#selRPL").append($('<option>', {
                    value: objTemp.SOEID,
                    text: optionText
                }));
            }

            //Add On Change Event
            $("#selRPL").change(function () {
                loadProcessActivityTable();
            });

            //Create Select2
            $("#selRPL").select2();
        }
    });
}

function loadSelectAnalyst() {
    //Clear Options
    $("#selAnalyst").contents().remove();

    _getDBListAnalysts({
        onReady: function (resultList) {
            //Add Default Option
            $("#selAnalyst").append($('<option>', {
                value: "",
                text: "-- Select --"
            }));

            //Add Options
            for (var i = 0; i < resultList.length; i++) {
                var objTemp = resultList[i];
                var optionText = objTemp.NAME + ' [' + objTemp.SOEID + ']';
                $("#selAnalyst").append($('<option>', {
                    value: objTemp.SOEID,
                    text: optionText
                }));
            }

            //Add On Change Event
            $("#selAnalyst").change(function () {
                loadProcessActivityTable();
            });

            //Create Select2
            $("#selAnalyst").select2();
        }
    });
}

function isValidExcel(defaultOptions) {

    //Default Options.
    var option = {
        excelSource: [],
        onSuccess: null,
        onError: null
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, option, defaultOptions);

    var excelSource = option.excelSource;
    var isValid = true;
    var sheetName = "TOUPLOAD";
    var allowedColumns = [
        //Add all valid columns e. g. some times is "Country" but the real name need to be "Region"
        //{ "Country": false, "Region": true },
        { "GOC ID": true },
        { "GOC Description": true },
        { "Process Activity Name": true },
        { "Owner SOEIDs": true },
        { "Owner Names": true },
        { "Maker SOEIDs": true },
        { "Maker Names": true },
        { "Checker SOEIDs": true },
        { "Checker Names": true },
        { "WAD Owner SOEIDs": true },
        { "WAD Owner Names": true },
        { "WAD Maker SOEIDs": true },
        { "WAD Maker Names": true },
        { "WAD Checker SOEIDs": true },
        { "WAD Checker Names": true },
        { "Recurrence Code": true },
        { "SOP / Bluework Link": true }
        //{ "EscalationDB10": true, Ignore: 1 },
        //{ "EscalationDB12": true, Ignore: 1 }
    ];

    //Validate if the user select the Process
    var idProcess = $(".jqxDropDownProcess").find(".fieldIDProcess").attr("value");
    if (!idProcess || idProcess == "0") {
        _showAlert({
            id: "UploadActivityMessage",
            type: 'error',
            content: "Please select the Main FRSS Process to upload the activities.",
            animateScrollTop: true
        });

        isValid = false;
    }

    //Validate if Sheet with name sheetName exists
    if (!excelSource[sheetName]) {
        _showAlert({
            id: "UploadActivityMessage",
            type: 'error',
            content: "Please review the Excel file that exists the Sheet Name '<b>" + sheetName + "</b>' with the data to upload.",
            animateScrollTop: true
        });

        isValid = false;
    }

    ////Validate if "Region" Column  exists
    //if (isValid && !excelSource[sheetName][0]["Region"]) {
    //    _showAlert({
    //        type: 'error',
    //        content: "Please review the Excel file that exists '<b>Region</b>' in the First Column.",
    //        animateScrollTop: true
    //    });
    //    isValid = false;
    //}

    ////Validate "Region" valid data
    //if (isValid) {
    //    for (var i = 0; i < excelSource[sheetName].length; i++) {
    //        if (!regionList[excelSource[sheetName][i]["Region"]]) {
    //            var strRegions = "";
    //            var regionsKeys = Object.keys(regionList);
    //            for (var i = 0; i < regionsKeys.length; i++) {
    //                strRegions += " - " + regionList[regionsKeys[i]].Name + " <br>";
    //            }
    //            _showAlert({
    //                type: 'error',
    //                content: "Please review the Excel file in the colum '<b>Region</b>' only the following options are allowed: <br> " + strRegions,
    //                animateScrollTop: true
    //            });
    //            isValid = false;
    //            break;
    //        }
    //    }
    //}
    
    //Validate columns
    if (isValid) {
        var excelColumnsKeys = Object.keys(excelSource[sheetName][0]);
        var missingCols = [];
        var extraCols = [];

        //Validate if the colums allowed are defined in the excel data
        for (var i = 0; i < allowedColumns.length; i++) {
            var allowedColumExist = false;
            var objAllowCol = allowedColumns[i];
            var keysAllowColNames = Object.keys(objAllowCol);
            var defaultKeyAllowColName = "";

            //When is EscalationDB8, EscalationDB10, EscalationDB12 ignore because sometimes
            //comes empty in the array of excel but exist in the excel file
            if (objAllowCol.Ignore && objAllowCol.Ignore == 1) {
                allowedColumExist = true;
            } else {
                for (var j = 0; j < keysAllowColNames.length && allowedColumExist == false; j++) {
                    var keyAllowColName = keysAllowColNames[j];

                    //Set default Key
                    if (objAllowCol[keyAllowColName]) {
                        defaultKeyAllowColName = keyAllowColName;
                    }

                    //Look if allowed column exist in excel columns
                    for (var k = 0; k < excelColumnsKeys.length; k++) {
                        var excelCol = excelColumnsKeys[k];

                        if (excelCol.toLocaleLowerCase() == keyAllowColName.toLocaleLowerCase()) {
                            allowedColumExist = true;
                            break;
                        }
                    }
                }
            }

            if (allowedColumExist == false) {
                missingCols.push(defaultKeyAllowColName);
            }
        }

        //Validate if the colums of the excel are allowed colums
        for (var i = 0; i < excelColumnsKeys.length; i++) {
            var excelCol = excelColumnsKeys[i];
            var excelColAllowed = false;

            for (var k = 0; k < allowedColumns.length && excelColAllowed == false; k++) {
                var objAllowCol = allowedColumns[k];
                var keysAllowColNames = Object.keys(objAllowCol);

                for (var j = 0; j < keysAllowColNames.length; j++) {
                    var keyAllowColName = keysAllowColNames[j];

                    if (excelCol.toLocaleLowerCase() == keyAllowColName.toLocaleLowerCase()) {
                        excelColAllowed = true;
                        break;
                    }
                }
            }

            if (excelColAllowed == false) {
                extraCols.push(excelCol);
            }
        }

        if (extraCols.length > 0 || missingCols.length > 0) {
            var msg = "<b>Excel file must have the following column names:</b> <br>";
            var flagCloseUl;

            for (var i = 0; i < allowedColumns.length; i++) {
                var objAllowCol = allowedColumns[i];
                var keysAllowColNames = Object.keys(objAllowCol);

                for (var j = 0; j < keysAllowColNames.length; j++) {
                    var keyAllowColName = keysAllowColNames[j];

                    if (i == 0 || i == 6 || i == 12) {
                        msg += "<ul class='col-md-4'>";
                        flagCloseUl = false;
                    }

                    //Set default Key
                    if (objAllowCol[keyAllowColName] === true) {
                        msg += "<li>" + keyAllowColName + "</li>";
                    }

                    if (i == 5 || i == 11 || i == 17) {
                        msg += "</ul>";
                        flagCloseUl = true;
                    }
                }
            }

            if (flagCloseUl == false) {
                msg += "</ul>";
            }

            msg += "<div class='clearfix'></div><br>";
            if (extraCols.length > 0) {
                msg += "<b>Please remove the following extra columns in the Excel file (review hidden colums): </b><br>";
                msg += "<ul class='col-md-4'>";
                for (var i = 0; i < extraCols.length; i++) {
                    msg += "<li>" + extraCols[i] + "</li>";
                }
                msg += "</ul>";
                msg += "<div class='clearfix'></div><br>";
            }

            if (missingCols.length > 0) {
                msg += "<b>Please add the following columns in the Excel file: </b><br>";
                msg += "<ul class='col-md-4'>";
                for (var i = 0; i < missingCols.length; i++) {
                    msg += "<li>" + missingCols[i] + "</li>";
                }
                msg += "</ul>";
                msg += "<div class='clearfix'></div>";
            }

            _showAlert({
                id: "UploadActivityMessage",
                type: 'error',
                content: msg,
                animateScrollTop: true
            });
            isValid = false;
        }
    }

    //Validate CPR Data
    if (isValid) {
        var activityList = excelSource[sheetName];
        var htmlMsg = "";
        var objUniqueGOCsInExcel = {};
        var objUniqueSOEIDsInExcel = {};
        var strGOCList = "";
        var strSOEIDList = "";
        var tempSOEID = "";
        var arrayOwnerSOEIDs = null;
        var arrayMakerSOEIDs = null;
        var arrayCheckerSOEIDs = null;
        var arrayWADOwnerSOEIDs = null;
        var arrayWADMakerSOEIDs = null;
        var arrayWADCheckerSOEIDs = null;
        var excelRowNumber;
        var objActivity;

        //Validate if GOCs & SOEIDs Exists
        for (var i = 0; i < activityList.length; i++) {
            excelRowNumber = i + 2;
            objActivity = activityList[i];

            // Get GOCs excel rows
            if (objActivity["GOC ID"]) {
                if (objActivity["GOC ID"].length <= 10) {
                    if (typeof objUniqueGOCsInExcel[objActivity["GOC ID"]] == "undefined") {
                        objUniqueGOCsInExcel[objActivity["GOC ID"]] = excelRowNumber + "";
                        strGOCList += objActivity["GOC ID"] + ";";
                    } else {
                        objUniqueGOCsInExcel[objActivity["GOC ID"]] += (";" + excelRowNumber )
                    }
                }
            }

            // Get SOEIDs of contacts
            if (objActivity["Owner SOEIDs"]) {
                //Owners
                arrayOwnerSOEIDs = objActivity["Owner SOEIDs"].split(";");
                for (var l = 0; l < arrayOwnerSOEIDs.length; l++) {
                    tempSOEID = arrayOwnerSOEIDs[l].toUpperCase();
                    if (typeof objUniqueSOEIDsInExcel[tempSOEID] == "undefined") {
                        objUniqueSOEIDsInExcel[tempSOEID] = excelRowNumber + "";
                        strSOEIDList += tempSOEID + ";";
                    } else {
                        objUniqueSOEIDsInExcel[tempSOEID] += (";" + excelRowNumber)
                    }
                }
            }

            if (objActivity["Maker SOEIDs"]) {
                //Makers
                arrayMakerSOEIDs = objActivity["Maker SOEIDs"].split(";");
                for (var l = 0; l < arrayMakerSOEIDs.length; l++) {
                    tempSOEID = arrayMakerSOEIDs[l].toUpperCase();
                    if (typeof objUniqueSOEIDsInExcel[tempSOEID] == "undefined") {
                        objUniqueSOEIDsInExcel[tempSOEID] = excelRowNumber + "";
                        strSOEIDList += tempSOEID + ";";
                    } else {
                        objUniqueSOEIDsInExcel[tempSOEID] += (";" + excelRowNumber)
                    }
                }
            }

            if (objActivity["Checker SOEIDs"]) {
                //Checkers
                arrayCheckerSOEIDs = objActivity["Checker SOEIDs"].split(";");
                for (var l = 0; l < arrayCheckerSOEIDs.length; l++) {
                    tempSOEID = arrayCheckerSOEIDs[l].toUpperCase();
                    if (typeof objUniqueSOEIDsInExcel[tempSOEID] == "undefined") {
                        objUniqueSOEIDsInExcel[tempSOEID] = excelRowNumber + "";
                        strSOEIDList += tempSOEID + ";";
                    } else {
                        objUniqueSOEIDsInExcel[tempSOEID] += (";" + excelRowNumber)
                    }
                }
            }

            if (objActivity["WAD Owner SOEIDs"]) {
                //WADOwners
                arrayWADOwnerSOEIDs = objActivity["WAD Owner SOEIDs"].split(";");
                for (var l = 0; l < arrayWADOwnerSOEIDs.length; l++) {
                    tempSOEID = arrayWADOwnerSOEIDs[l].toUpperCase();
                    if (typeof objUniqueSOEIDsInExcel[tempSOEID] == "undefined") {
                        objUniqueSOEIDsInExcel[tempSOEID] = excelRowNumber + "";
                        strSOEIDList += tempSOEID + ";";
                    } else {
                        objUniqueSOEIDsInExcel[tempSOEID] += (";" + excelRowNumber)
                    }
                }
            }

            if (objActivity["WAD Maker SOEIDs"]) {
                //WADMakers
                arrayWADMakerSOEIDs = objActivity["WAD Maker SOEIDs"].split(";");
                for (var l = 0; l < arrayWADMakerSOEIDs.length; l++) {
                    tempSOEID = arrayWADMakerSOEIDs[l].toUpperCase();
                    if (typeof objUniqueSOEIDsInExcel[tempSOEID] == "undefined") {
                        objUniqueSOEIDsInExcel[tempSOEID] = excelRowNumber + "";
                        strSOEIDList += tempSOEID + ";";
                    } else {
                        objUniqueSOEIDsInExcel[tempSOEID] += (";" + excelRowNumber)
                    }
                }
            }

            if (objActivity["WAD Checker SOEIDs"]) {
                //WADCheckers
                arrayWADCheckerSOEIDs = objActivity["WAD Checker SOEIDs"].split(";");
                for (var l = 0; l < arrayWADCheckerSOEIDs.length; l++) {
                    tempSOEID = arrayWADCheckerSOEIDs[l].toUpperCase();
                    if (typeof objUniqueSOEIDsInExcel[tempSOEID] == "undefined") {
                        objUniqueSOEIDsInExcel[tempSOEID] = excelRowNumber + "";
                        strSOEIDList += tempSOEID + ";";
                    } else {
                        objUniqueSOEIDsInExcel[tempSOEID] += (";" + excelRowNumber)
                    }
                }
            }

        }

        //Sql for validate GOCs
        var sqlValidateGOCs =
            "SELECT \n" +
            "   TG.[GOC], \n" +
            "   CASE ISNULL(G.[ID], '') \n" +
            "       WHEN '' THEN 0 \n" +
            "       ELSE 1 \n" +
            "   END AS [Exists], \n" +
            "   CASE ISNULL(MS.[ManagerID], 0) \n" +
            "       WHEN 0 THEN 0 \n" +
            "       ELSE 1 \n" +
            "   END AS [ExistsGPL], \n" +
            "   CASE ISNULL(MSMG.[ManagerID], 0) \n" +
            "       WHEN 0 THEN 0 \n" +
            "       ELSE 1 \n" +
            "   END AS [ExistsRPL] \n" +
            "FROM ( \n" +
            "    SELECT [Val] AS [GOC] \n" +
	        "    FROM [Automation].[dbo].[func_Split]('" + strGOCList + "', ';', 1) \n" +
            ") TG \n" +
            "LEFT JOIN [Automation].[dbo].[tblGlobalOrganizationCode] G ON G.[ID] = TG.[GOC] " +
            " \n " +
            "--Get GPL \n " +
            "LEFT JOIN [Automation].[dbo].[tblManagedSegment] MS WITH(READUNCOMMITTED) ON G.ManagedSegmentID = MS.ID \n " +
            " \n " +
            "--Get RPL \n " +
		    "LEFT JOIN [Automation].[dbo].[tblManagedSegmentManagedGeography] MSMG WITH(READUNCOMMITTED) ON G.ManagedSegmentID = MSMG.ManagedSegmentID AND G.ManagedGeographyID = MSMG.ManagedGeographyID   \n ";

        //Validate GOCs
        _callServer({
            loadingMsgType: "fullLoading",
            loadingMsg: "Validating GOCs...",
            url: '/Ajax/ExecQuery',
            data: { 'pjsonSql': _toJSON(sqlValidateGOCs) },
            type: "POST",
            success: function (resultListGOCs) {

                //Sql for validate SOEIDs
                var sqlValidateSOEIDs =
                    "SELECT \n" +
                    "   TG.[SOEID], \n" +
                    "   CASE ISNULL(E.[SOEID], '') \n" +
                    "       WHEN '' THEN 0 \n" +
                    "       ELSE 1 \n" +
                    "   END AS [Exists] \n" +
                    "FROM ( \n" +
                    "    SELECT [Val] AS [SOEID] \n" +
                    "    FROM [Automation].[dbo].[func_Split]('" + strSOEIDList + "', ';', 1) \n" +
                    ") TG \n" +
                    "LEFT JOIN [Automation].[dbo].[tblEmployee] E WITH(READUNCOMMITTED) ON E.[SOEID] = TG.[SOEID] ";

                //Validate GOCs
                _callServer({
                    loadingMsgType: "fullLoading",
                    loadingMsg: "Validating SOEIDs...",
                    url: '/Ajax/ExecQuery',
                    data: { 'pjsonSql': _toJSON(sqlValidateSOEIDs) },
                    type: "POST",
                    success: function (resultListSOEIDs) {
                        

                        //Validate Excel Data
                        for (var i = 0; i < activityList.length; i++) {
                            var objGOCValidation = null;
                            var objSOEIDValidation = null;

                            excelRowNumber = i + 2;
                            objActivity = activityList[i];

                            // Validation for CPR Action Column
                            //if (objActivity["CPR Action"]) {
                            //    // Check valid values "ADD" or "UPDATE"
                            //    if (_contains(objActivity["CPR Action"], "ADD") || _contains(objActivity["CPR Action"], "UPDATE")) {
                            //        // Validate "UPDATE:45" format
                            //        if (_contains(objActivity["CPR Action"], "UPDATE")) {
                            //            var splitResut = objActivity["CPR Action"].split(":");
                            //            if (splitResut.length > 1) {
                            //                var idCPR = splitResut[1];
                            //                //Check CPR ID must have a number
                            //                if (isNaN(idCPR)) {
                            //                    htmlMsg += "<li>Row (" + excelRowNumber + "): Column [CPR Action] value '" + objActivity["CPR Action"] + "' the correct format is UPDATE:NUMBER e.g. 'UPDATE:34' </li>";
                            //                }
                            //            } else {
                            //                htmlMsg += "<li>Row (" + excelRowNumber + "): Column [CPR Action] value '" + objActivity["CPR Action"] + "' the correct format is UPDATE:NUMBER e.g. 'UPDATE:34' </li>";
                            //            }
                            //        }
                            //    } else {
                            //        htmlMsg += "<li>Row (" + excelRowNumber + "): Column [CPR Action] value '" + objActivity["CPR Action"] + "' the correct values are ADD or UPDATE:NUMBER e.g. 'ADD' or 'UPDATE:34' </li>";
                            //    }
                            //} else {
                            //    htmlMsg += "<li>Row (" + excelRowNumber + "): Column [CPR Action] value is missing </li>";
                            //}

                            // Validation for GOC ID
                            if (objActivity["GOC ID"]) {
                                if (objActivity["GOC ID"].length > 10) {
                                    htmlMsg += "<li>Row (" + excelRowNumber + "): Column [GOC ID] the max text length is 10 characters your value '" + objActivity["GOC ID"] + "' has " + objActivity["GOC ID"].length + " of length </li>";
                                } else {
                                    objGOCValidation = _findOneObjByProperty(resultListGOCs, "GOC", objActivity["GOC ID"]);

                                    if (objGOCValidation.Exists == "0") {
                                        htmlMsg += "<li>Row (" + excelRowNumber + "): Column [GOC ID] your value '" + objActivity["GOC ID"] + "' not exists in our GOCs records. </li>";
                                    } else {
                                        if (objGOCValidation.ExistsGPL == "0" && objGOCValidation.ExistsRPL == "0") {
                                            htmlMsg += "<li>Row (" + excelRowNumber + "): Column [GOC ID] your value '" + objActivity["GOC ID"] + "' don't have GPL and RPL Assigned, please validate if it is a FRSS GOC or use other FRSS GOC. </li>";
                                        } else {
                                            if (objGOCValidation.ExistsGPL == "0") {
                                                htmlMsg += "<li>Row (" + excelRowNumber + "): Column [GOC ID] your value '" + objActivity["GOC ID"] + "' don't have GPL Assigned, please validate if it is a FRSS GOC or use other FRSS GOC. </li>";
                                            }

                                            if (objGOCValidation.ExistsRPL == "0") {
                                                htmlMsg += "<li>Row (" + excelRowNumber + "): Column [GOC ID] your value '" + objActivity["GOC ID"] + "' don't have RPL Assigned, please validate if it is a FRSS GOC or use other FRSS GOC. </li>";
                                            }
                                        }
                                    }
                                }
                            } else {
                                htmlMsg += "<li>Row (" + excelRowNumber + "): Column [GOC ID] value is missing </li>";
                            }

                            // Validation for Owner SOEIDs
                            if (objActivity["Owner SOEIDs"]) {
                                for (var h = 0; h < arrayOwnerSOEIDs.length; h++) {
                                    tempSOEID = arrayOwnerSOEIDs[h];

                                    objSOEIDValidation = _findOneObjByProperty(resultListSOEIDs, "SOEID", tempSOEID);
                                    if (objSOEIDValidation.Exists == "0") {
                                        htmlMsg += "<li>Row (" + excelRowNumber + "): Column [Owner SOEIDs] your value '" + tempSOEID + "' not exists in our Global Directory records. </li>";
                                    }
                                }
                            } else {
                                htmlMsg += "<li>Row (" + excelRowNumber + "): Column [Owner SOEIDs] value is missing </li>";
                            }

                            // Validation for Maker SOEIDs
                            if (objActivity["Maker SOEIDs"]) {
                                for (var h = 0; h < arrayMakerSOEIDs.length; h++) {
                                    tempSOEID = arrayMakerSOEIDs[h];

                                    objSOEIDValidation = _findOneObjByProperty(resultListSOEIDs, "SOEID", tempSOEID);
                                    if (objSOEIDValidation.Exists == "0") {
                                        htmlMsg += "<li>Row (" + excelRowNumber + "): Column [Maker SOEIDs] your value '" + tempSOEID + "' not exists in our Global Directory records. </li>";
                                    }
                                }
                            } else {
                                htmlMsg += "<li>Row (" + excelRowNumber + "): Column [Maker SOEIDs] value is missing </li>";
                            }

                            // Validation for Checker SOEIDs
                            if (objActivity["Checker SOEIDs"]) {
                                for (var h = 0; h < arrayCheckerSOEIDs.length; h++) {
                                    tempSOEID = arrayCheckerSOEIDs[h];

                                    objSOEIDValidation = _findOneObjByProperty(resultListSOEIDs, "SOEID", tempSOEID);
                                    if (objSOEIDValidation.Exists == "0") {
                                        htmlMsg += "<li>Row (" + excelRowNumber + "): Column [Checker SOEIDs] your value '" + tempSOEID + "' not exists in our Global Directory records. </li>";
                                    }
                                }
                            }

                            // Validation for WAD Owner SOEIDs
                            if (objActivity["WAD Owner SOEIDs"]) {
                                for (var h = 0; h < arrayWADOwnerSOEIDs.length; h++) {
                                    tempSOEID = arrayWADOwnerSOEIDs[h];

                                    objSOEIDValidation = _findOneObjByProperty(resultListSOEIDs, "SOEID", tempSOEID);
                                    if (objSOEIDValidation.Exists == "0") {
                                        htmlMsg += "<li>Row (" + excelRowNumber + "): Column [WAD Owner SOEIDs] your value '" + tempSOEID + "' not exists in our Global Directory records. </li>";
                                    }
                                }
                            } else {
                                htmlMsg += "<li>Row (" + excelRowNumber + "): Column [WAD Owner SOEIDs] value is missing </li>";
                            }

                            // Validation for WAD Maker SOEIDs
                            if (objActivity["WAD Maker SOEIDs"]) {
                                for (var h = 0; h < arrayWADMakerSOEIDs.length; h++) {
                                    tempSOEID = arrayWADMakerSOEIDs[h];

                                    objSOEIDValidation = _findOneObjByProperty(resultListSOEIDs, "SOEID", tempSOEID);
                                    if (objSOEIDValidation.Exists == "0") {
                                        htmlMsg += "<li>Row (" + excelRowNumber + "): Column [WAD Maker SOEIDs] your value '" + tempSOEID + "' not exists in our Global Directory records. </li>";
                                    }
                                }
                            } else {
                                htmlMsg += "<li>Row (" + excelRowNumber + "): Column [WAD Maker SOEIDs] value is missing </li>";
                            }

                            // Validation for WAD Checker SOEIDs
                            if (objActivity["WAD Checker SOEIDs"]) {
                                for (var h = 0; h < arrayWADCheckerSOEIDs.length; h++) {
                                    tempSOEID = arrayWADCheckerSOEIDs[h];

                                    objSOEIDValidation = _findOneObjByProperty(resultListSOEIDs, "SOEID", tempSOEID);
                                    if (objSOEIDValidation.Exists == "0") {
                                        htmlMsg += "<li>Row (" + excelRowNumber + "): Column [WAD Checker SOEIDs] your value '" + tempSOEID + "' not exists in our Global Directory records. </li>";
                                    }
                                }
                            }

                            // Validation for Recurrence Code
                            if (!objActivity["Recurrence Code"]) {
                                htmlMsg += "<li>Row (" + excelRowNumber + "): Column [Recurrence Code] value is missing </li>";
                            }
                        }

                        if (htmlMsg) {
                            htmlMsg = "<ul>" + htmlMsg + "</ul>";

                            _showAlert({
                                id: "UploadActivityMessage",
                                type: 'error',
                                content: "<b>Please review the following error in the data of your Excel:</b>" + htmlMsg,
                                animateScrollTop: true
                            });

                            isValid = false;
                        }

                        //Replace ADD = 0 and remove UPDATE:, delete unnecessary columns
                        if (isValid) {
                            for (var i = 0; i < excelSource[sheetName].length; i++) {
                                //Remove Columns
                                delete excelSource[sheetName][i]["GOC Description"];
                                delete excelSource[sheetName][i]["Owner Names"];
                                delete excelSource[sheetName][i]["Maker Names"];
                                delete excelSource[sheetName][i]["Checker Names"];
                                delete excelSource[sheetName][i]["WAD Owner Names"];
                                delete excelSource[sheetName][i]["WAD Maker Names"];
                                delete excelSource[sheetName][i]["WAD Checker Names"];

                                //CPR Action Replace values
                                //var tempAction = excelSource[sheetName][i]["CPR Action"];

                                //if (_contains(tempAction, "ADD")) {
                                //    excelSource[sheetName][i]["CPR Action"] = 0;
                                //}

                                //if (_contains(tempAction, "UPDATE")) {
                                //    excelSource[sheetName][i]["CPR Action"] = parseInt(_replaceAll("UPDATE:", "", tempAction));
                                //}
                            }
                        }

                        if (isValid) {
                            option.onSuccess(excelSource);
                        } else {
                            option.onError(excelSource);
                        }
                    }
                });
            },
            error: function () {
                option.onError(excelSource);
            }
        });
    }

    function isValidGOCs(arrayGOCs) {

    }
}

function deleteCPRActivities() {
    var idProcessTaxonomy = $(".fieldIDProcess").attr("value");
    
    if (idProcessTaxonomy && idProcessTaxonomy != "0") {
        var rows = $.jqxGridApi.getAllRows("#tblProcessActivities");
        if (rows.length > 0) {
            var idsArray = [];
            var processName = "";

            for (var i = 0; i < rows.length; i++) {
                var objActivity = rows[i];
                idsArray.push(objActivity.ID);

                processName = objActivity["B"];
            }

            //return idsArray.join(",");

            var htmlContentModal = "<b>Process: </b> " + processName + "<br/>";
            htmlContentModal += "<b>Activities to delete: </b> " + rows.length + "<br/>";

            _showModal({
                width: '35%',
                modalId: "modalDelCPRActivities",
                addCloseButton: true,
                buttons: [{
                    name: "Delete",
                    class: "btn-danger",
                    onClick: function () {

                        _callProcedure({
                            loadingMsgType: "topBar",
                            loadingMsg: "Deleting CPR Activities...",
                            name: "[dbo].[spCPRAFrwkProcessActivityAdmin]",
                            params: [
                                { "Name": "@Action", "Value": "Delete" },
                                { "Name": "@IDProcessTaxonomy", "Value": idProcessTaxonomy },
                                { "Name": "@IDsActivitiesCPR", "Value": idsArray.join(",") },
                                { "Name": "@SessionSOEID", "Value": _getSOEID() }
                            ],
                            success: {
                                fn: function () {
                                    //Show User Message
                                    _showAlert({
                                        id: "UploadActivityMessage",
                                        animateScrollTop: true,
                                        type: "success",
                                        content: "CPR Activities were deleted successfully."
                                    });

                                    //Refresh Table
                                    loadProcessActivityTable();
                                }
                            }
                        });
                    }
                }],
                title: "Are you sure you want to delete CPR Activities?",
                contentHtml: htmlContentModal
            });
        } else {
            _showAlert({
                id: "UploadActivityMessage",
                type: "error",
                content: "The selected process don't have CPR Activities."
            });
        }
    } else {
        _showAlert({
            id: "UploadActivityMessage",
            type: "error",
            content: "Please select Process Tree"
        });
    }
}

function loadProcessActivityTable() {
    var idProcessTaxonomy = $(".fieldIDProcess").attr("value");
    var GPLSOEID = $("#selGPL").val();
    var RPLSOEID = $("#selRPL").val();
    var AnalystSOEID = $("#selAnalyst").val();
    var filters = {};

    if (idProcessTaxonomy && idProcessTaxonomy != "0") {
        filters.idProcessTaxonomy = idProcessTaxonomy;
    }

    if (GPLSOEID) {
        filters.GPLSOEID = GPLSOEID;
    }

    if (RPLSOEID) {
        filters.RPLSOEID = RPLSOEID;
    }

    if (AnalystSOEID) {
        filters.employeeSOEID = AnalystSOEID;
    }

    _loadProcessActivityTable({
        idTable: "#tblProcessActivities",
        filters: filters,
        onReady: function(activityList){
            //Add count of activities to recover
            $(".lblActivityCount").html("Process Activities (" + activityList.length + ")");
        }
    });
}