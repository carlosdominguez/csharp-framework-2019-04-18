/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="../../Shared/plugins/citi-process-tree/process-tree-v1.0.0.js" />
/// <reference path="CPRGlobal.js" />

$(document).ready(function () {

    //Get Current Process List
    //_getProcessData({
    //    onReady: function (resultList) {
    //        processList = resultList;

    //        //Load Documents Scripts
    //        onDocumentReady();
    //    }
    //});

    //Hide menu
    //_hideMenu();

    //Load Wizard plugin
    $('#wizardSendCommunication').bootstrapWizard({
        'tabClass': 'nav nav-wizardSendCommunication',
        'debug': false,
        onShow: function (tab, navigation, index) {

            console.log('onShow - ' + tab + ' index: ' + index);
            switch (index) {
                case 1:
                    //LoadTabGeneralInformation();
                    //$('#EUCID').attr('tabindex', 1);
                    //localStorage.setItem('tabindex', 1);
                    break;
                case 2:

                    //LoadTabCriticality();
                    //$('#EUCID').attr('tabindex', 2);
                    //localStorage.setItem('tabindex', 2);
                    break;
                case 3:
                    //LoadTabDataGathering();
                    //$('#EUCID').attr('tabindex', 3);
                    //localStorage.setItem('tabindex', 3);
                    break;
                case 4:
                    //LoadTabRemediation();
                    //$('#EUCID').attr('tabindex', 4);
                    //localStorage.setItem('tabindex', 4);
                    break;
                default:
                    break;
            }
        },
        onNext: function (tab, navigation, index) {
            console.log('onNext' + ' - index: ' + index);
            switch (index) {
                case 1:
                    //var isValidGeneralInfoTab = validateRequiredGeneralInfo();
                    //if (isValidGeneralInfoTab) {
                    //    savetblEUC();
                    //    LoadTabCriticality();
                    //}
                    //return isValidGeneralInfoTab;
                    break;
                case 2:
                    //$('a[href="#euc-form-criticality-tab2"]').css('background', 'green');
                    //$('html, body').animate({
                    //    scrollTop: '0px'
                    //}, 800);
                    //_showNotification("success", "EUC Criticality - Business Important - was saved successfully.");
                    //var isValidCriticalityInfoTab = validateRequiredCriticality();
                    //if (isValidCriticalityInfoTab) {
                    //    savetblEUC_Criticality();
                    //    LoadTabDataGathering();
                    //}
                    //return isValidCriticalityInfoTab;
                    break;
                case 3:
                    //$('a[href="#euc-form-remediation-tab4"]').css('background', 'green');
                    //$('html, body').animate({
                    //    scrollTop: '0px'
                    //}, 800);
                    //_showNotification("success", "EUC Remediation was saved successfully.");
                    //validateRequiredDataGathering();
                    break;
                case 4:
                    //validateRequiredRemediation();
                    break;
                default:
                    break;
            }

        },
        onPrevious: function (tab, navigation, index) {
            console.log('onPrevious' + ' - index: ' + index);
        },
        onLast: function (tab, navigation, index) {
            console.log('onLast' + ' - index: ' + index);
        },
        onTabClick: function (tab, navigationBar, index, toIndex, toTab) {
            //console.log('onTabClick' + ' index: ' + index);
            var idToTab = $(toTab).find("a").attr("id");
            var enableNavigation = true;
            var msgError = "";

            switch (idToTab) {
                //When pass from 0 to other tab
                case "linkGoTabRecover":                    
                    break;

                case "linkGoTabActivityList":
                    if (!objTabRecover.tabWasLoaded) {
                        enableNavigation = false;
                    }

                    var objLocationSelected = objTabRecover.getObjLocationSelected();
                    if (objLocationSelected.total == 0) {
                        msgError = "No process activities found for the location and impacted dates selected.";
                        enableNavigation = false;
                    }
                    break;

                case "linkGoTabCommunication":

                    if (!objTabRecover.tabWasLoaded) {
                        enableNavigation = false;
                    }

                    if (!objTabProcessActivity.tabWasLoaded) {
                        enableNavigation = false;
                    }
                    
                    break;

                case "linkGoTabSummary":
                    
                    if (!objTabRecover.tabWasLoaded) {
                        enableNavigation = false;
                    }

                    if (!objTabProcessActivity.tabWasLoaded) {
                        enableNavigation = false;
                    }

                    if (!objTabSendCommunication.tabWasLoaded) {
                        enableNavigation = false;
                    }

                    //TODO: Once Summary done remove this line
                    msgError = "Please click in 'Review Communication' button first.";
                    enableNavigation = false;
                    break;
                default:
                    break;
            }

            if (enableNavigation == false) {
                if (!msgError) {
                    msgError = "You can not go to this section without completing the previous ones.";
                }

                _showNotification("error", msgError, "WizardValidation");
            }

            return enableNavigation;
        },
        onTabShow: function (tab, navigation, index) {
            console.log('onTabShow' + ' index: ' + index);

            if (index > 0) {
                $('#wizardSendCommunication').find('.form-wizard').children('li').eq(index - 1).addClass('complete');
            }

            switch (index) {
                case 0:
                    objTabRecover.loadTab();
                    break;
                case 1:
                    objTabProcessActivity.loadTab();
                    break;
                case 2:
                    objTabSendCommunication.loadTab();
                    break;
                case 3:
                    //LoadTabRemediation();
                    break;
                default:
                    break;
            }

            var $total = navigation.find('li').length;
            var $current = index + 1;
            var $percent = ($current / $total) * 100;
            $('#wizardSendCommunication .progress-bar').css({
                width: $percent + '%'
            });
        }
    });

    //Create Swich for Test Event
    var defaults = {
        color: '#4caf50',
        secondaryColor: '#dfdfdf',
        jackColor: '#fff',
        jackSecondaryColor: null,
        className: 'switchery',
        disabled: false,
        disabledOpacity: 0.5,
        speed: '0.5s',
        size: 'large'
    }
    var switchery = new Switchery($(".js-switch")[0], defaults);

    //On click Test Event
    $(".chkTestEvent").change(function () {
        //When is a Test Event
        if ($(this).is(":checked")) {
            //$("#lblTestEvent").html("Test Event");
            $("#lblTestEvent").html("Attestation Event");
            $(".containerSendCommunication").addClass("testEvent");
            $("#cprLogo").hide();
        } else {
            //When is a Real Event
            $("#lblTestEvent").html("Real Event");
            $(".containerSendCommunication").removeClass("testEvent");
            $("#cprLogo").show();
        }
    });
});

//------------------------------------------------
//Tab 1 Recover Logic
//------------------------------------------------
var objTabRecover = {
    //Variables
    tabWasLoaded: false,
    impactedStartDate: "",
    impactedEndDate: "",
    impactedDateRange: "",

    //Functionality
    loadTab: function () {
        if (objTabRecover.tabWasLoaded == false) {
            //On Click View Process List
            $(".btnGoActivitityList").click(function () {
                $("#linkGoTabActivityList").click();
            });

            //On change Select To Recover
            $(".selToRecover").change(function () {
                var optionToRecover = $(this).val();

                //Hide all
                $(".fieldContainer").hide();

                //Show selected Field
                switch (optionToRecover) {
                    case "Center":
                        objTabRecover.renderOptionSelCenter();
                        $(".fieldContainerCenter").show();
                        break;

                    case "Other":
                        objTabRecover.renderOptionSelOtherLocation();
                        $(".fieldContainerOtherLocation").show();
                        break;

                    case "Owner":
                        $(".fieldContainerOwner").show();
                        break;

                    case "Process":
                        $(".fieldContainerProcessToRecover").show();
                        break;
                }
            });
            $(".selToRecover").change();

            //Create Date Impacted Range
            objTabRecover.loadImpactedDateRangePlugin();

            //Set tab was loaded to avoid bind multiples times the click or change events.
            objTabRecover.tabWasLoaded = true;
        }
    },
    loadImpactedDateRangePlugin: function () {
        //Set custom date to flag all dates
        var allDatesFlag = moment("1992-01-19");
        var thisMonth = moment().format("MMMM") + ' Month';
        var defaultRange = '15 Days';
        var ranges = {
            'All': [allDatesFlag, allDatesFlag],
            'Today': [moment(), moment()],
            '3 Days': [moment(), moment().add(2, 'days')],
            '15 Days': [moment(), moment().add(14, 'days')],
            '30 Days': [moment(), moment().add(29, 'days')]
        };
        ranges[thisMonth] = [moment().startOf('month'), moment().endOf('month')];

        var $containerImpactedDates = $(".containerImpactedDates"),
            options = {
                format: _getElementValueHelper($containerImpactedDates, 'format', 'MM/DD/YYYY'),
                timePicker: _getElementValueHelper($containerImpactedDates, 'timePicker', false),
                timePickerIncrement: _getElementValueHelper($containerImpactedDates, 'timePickerIncrement', false),
                separator: _getElementValueHelper($containerImpactedDates, 'separator', ' - '),
            },
            min_date = _getElementValueHelper($containerImpactedDates, 'minDate', ''),
            max_date = _getElementValueHelper($containerImpactedDates, 'maxDate', ''),
            start_date = _getElementValueHelper($containerImpactedDates, 'startDate', ''),
            end_date = _getElementValueHelper($containerImpactedDates, 'endDate', '');

        if ($containerImpactedDates.hasClass('add-date-ranges')) {
            options['ranges'] = ranges;
        }

        if (min_date.length) {
            options['minDate'] = min_date;
        }

        if (max_date.length) {
            options['maxDate'] = max_date;
        }

        if (start_date.length) {
            options['startDate'] = start_date;
        } else {
            options['startDate'] = ranges[defaultRange][0]; //allDatesFlag;
        }

        if (end_date.length) {
            options['endDate'] = end_date;
        } else {
            options['endDate'] = ranges[defaultRange][1]; //allDatesFlag;
        }

        //Create Date Range Picker
        $containerImpactedDates.daterangepicker(options, function (start, end) {
            onChangeRangeDate(start, end);
        });
        onChangeRangeDate(ranges[defaultRange][0], ranges[defaultRange][1]);

        //On Change Datepicker
        function onChangeRangeDate(start, end) {
            var objDaterangepicker = $containerImpactedDates.data('daterangepicker');
            //var drp = $containerImpactedDates.data('daterangepicker');

            //If not exists set default
            if (!objDaterangepicker.chosenLabel) {
                objDaterangepicker.chosenLabel = defaultRange;
            }

            if ($containerImpactedDates.hasClass('daterange-text')) {
                //$containerImpactedDates.find('span').html(start.format(drp.format) + drp.separator + end.format(drp.format));
                if (start.format("YYYY-MM-DD") == allDatesFlag.format("YYYY-MM-DD")) {
                    //Update Impacted Date Range
                    objTabRecover.impactedStartDate = "";
                    objTabRecover.impactedEndDate = "";
                    objTabRecover.impactedDateRange = "";

                    $containerImpactedDates.find('span').html("All");
                } else {
                    //Update Impacted Date Range
                    objTabRecover.impactedStartDate = start.format('YYYY-MM-DD');
                    objTabRecover.impactedEndDate = end.format('YYYY-MM-DD');

                    //When is Today avoid duplicate the same date "March 23, 2018 - March 23, 2018"
                    if (objTabRecover.impactedStartDate == objTabRecover.impactedEndDate) {
                        objTabRecover.impactedDateRange = start.format('MMM D, YYYY') + " (" + objDaterangepicker.chosenLabel + ")";
                    } else {
                        objTabRecover.impactedDateRange = start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY') + " (" + objDaterangepicker.chosenLabel + ")";;
                    }

                    $containerImpactedDates.find('span').html(objTabRecover.impactedDateRange);
                }

                //Update Location Activities Counter
                $(".selToRecover").change();
            }
        }

        if (typeof options['ranges'] == 'object') {
            $containerImpactedDates.data('daterangepicker').container.removeClass('show-calendar');
        }
    },
    renderOptionSelCenter: function () {
        //Get data
        objTabRecover.getDBLocationSummary({
            type: "Center",
            onReady: function (centerList) {
                $(".selCenter").contents().remove();

                //Render Process Options
                for (var i = 0; i < centerList.length; i++) {
                    var objLocation = centerList[i];

                    $(".selCenter").append($('<option>', {
                        locationName: objLocation.LOCATION_NAME,
                        locationTotal: objLocation.TOTAL,
                        value: objLocation.LOCATION_ID,
                        text: objLocation.LOCATION_NAME + " (" + objLocation.TOTAL + ")"
                    }));
                }

                //Set not found Option
                if (centerList.length == 0) {
                    $(".selCenter").append($('<option>', {
                        locationName: "",
                        locationTotal: 0,
                        value: "0",
                        text: "Not Found"
                    }));
                }

                $(".selCenter").change(function () {
                    updateHeaderCommunicationSummary();
                });

                //Load Header Summary of Communication
                updateHeaderCommunicationSummary();

                //Create Select2
                //$(".selCenter").select2();
            }
        });
    },
    renderOptionSelOtherLocation: function () {
        //Get data
        objTabRecover.getDBLocationSummary({
            type: "Other",
            onReady: function (otherLocationList) {
                $(".selOtherLocation").contents().remove();

                //Render Process Options
                for (var i = 0; i < otherLocationList.length; i++) {
                    var objLocation = otherLocationList[i];

                    $(".selOtherLocation").append($('<option>', {
                        locationName: objLocation.LOCATION_NAME,
                        locationTotal: objLocation.TOTAL,
                        value: objLocation.LOCATION_ID,
                        text: objLocation.LOCATION_NAME + " (" + objLocation.TOTAL + ")"
                    }));
                }

                //Set not found Option
                if (otherLocationList.length == 0) {
                    $(".selOtherLocation").append($('<option>', {
                        locationName: "",
                        locationTotal: 0,
                        value: "0",
                        text: "Not Found"
                    }));
                }

                $(".selOtherLocation").change(function () {
                    updateHeaderCommunicationSummary();
                });

                //Load Header Summary of Communication
                updateHeaderCommunicationSummary();

                //Create Select2
                //$(".selCenter").select2();
            }
        });
    },
    getObjLocationSelected: function () {
        var optionToRecover = $(".selToRecover").val();
        var objLocation = null;

        //Get selected location
        switch (optionToRecover) {
            case "Center":
                objLocation = {
                    id: $(".selCenter").val(),
                    name: $(".selCenter").find(":selected").attr("locationName"),
                    total: $(".selCenter").find(":selected").attr("locationTotal")
                };
                break;

            case "Other":
                objLocation = {
                    id: $(".selOtherLocation").val(),
                    name: $(".selOtherLocation").find(":selected").attr("locationName"),
                    total: $(".selOtherLocation").find(":selected").attr("locationTotal")
                };
                break;
        }

        return objLocation;
    },
    
    //BD Data
    getDBLocationSummary: function (customOptions) {
        //Default Options.
        var options = {
            type: "Center",
            onReady: function () { }
        };

        //Hacer un merge con las opciones que nos enviaron y las default.
        $.extend(true, options, customOptions);

        //Get Center Summary with Total of eDCFC Activities
        var sql =
            "SELECT \n" +
            "    T1E.[ID]			AS [LOCATION_ID], \n" +
            "    T1E.[WORK_COUNTRY], \n" +
            "    T1E.[WORK_CITY], \n" +
            "    T1E.[LOCATION_TYPE], \n" +
            "    T1E.[LOCATION_NAME], \n" +
            "    TT2.[TOTAL_EDCFC] + TT3.[TOTAL_CPR]    AS [TOTAL] \n" +
            "FROM  \n" +
            "    [Automation].[dbo].[tblEmployeePhysicalLocationMap] T1E \n" +
            "    OUTER APPLY( \n" +
			"        SELECT \n" +
			"	        COUNT (1) AS [TOTAL_EDCFC] \n" +
			"        FROM  \n" +
			"	        [TITAN].[dbo].[Node] N \n" +
            "               --Get Only Recoverable WAD \n" +
            "               INNER JOIN [TITAN].[dbo].[NodeData] ND_RWAD ON ND_RWAD.[NodeID] = N.[ID] AND ND_RWAD.[MetadataID] = 32 AND ND_RWAD.[Value] = 'True' \n" +
            " \n" +
            "               --Get Only Activities with GOC \n" +
            "               INNER JOIN [TITAN].[dbo].[NodeData] ND_GOC ON ND_GOC.[NodeID] = N.[ID] AND ND_GOC.[MetadataID] = 26 AND ND_GOC.[Value] IS NOT NULL \n";

            //Add Join to Get Activities beetween date range
        if (objTabRecover.impactedStartDate && objTabRecover.impactedEndDate) {
            sql +=
                "       --Get Activities beetween date rage \n" +
                "       INNER JOIN ( SELECT DISTINCT TE2.[NodeID] \n" +
                "                   FROM [TITAN].[dbo].[Event] TE2 \n" +
                "                   WHERE TE2.[Date] BETWEEN '" + objTabRecover.impactedStartDate + "' AND '" + objTabRecover.impactedEndDate + "') AS TDR ON N.[ID] = TDR.[NodeID] \n";
        }

        sql +=
			"        WHERE \n" +
			"	        --Get only Active Activities \n" +
			"	        N.[IsActive] = 1  \n" +
            " " +
			"	        --Get only Control Activities \n" +
			"	        AND N.[CategoryID] = 3  \n" +
            " " +
			"	        --Get only Responsibles from Parent Location \n" +
			"	        AND N.[PrimaryResponsibleID] IN ( \n" +
			"		        SELECT  \n" +
			"			        E.[ID] \n" +
			"		        FROM  \n" +
			"			        [Automation].[dbo].[tblEmployee] E WITH(READUNCOMMITTED) \n" +
			"				        --Get only FRSS Employees \n" +
			"				        --Commented because some activities have NON FRSS Employee maybe for missing update or position changed. \n" +
			"				        --INNER JOIN [Automation].[dbo].[tblProcessXManagedSegment] PM ON PM.[IDProcess] = 1 AND E.[ManagedSegmentID] = PM.[ManagedSegment] \n" +
			"				 \n" +
			"				        --Get Location \n" +
			"				        INNER JOIN [Automation].[dbo].[tblEmployeePhysicalLocationMap] EL ON E.[WORK_COUNTRY] = EL.[WORK_COUNTRY] AND E.[WORK_CITY] = EL.[WORK_CITY] \n" +
			"		        WHERE \n" +
			"			        --Get only Active employees \n" +
			"			        E.[EMPL_STATUS] = 'A' AND \n" +
            " " +
			"			        --Filter by Parent Location \n" +
			"			        EL.[ID] = T1E.[ID] \n" +
			"	        ) \n" +
		    "    )	AS TT2 \n" +
            "    OUTER APPLY( \n" +
			"        SELECT \n" +
			"	        COUNT (1) AS [TOTAL_CPR] \n" +
			"        FROM  \n" +
			"	        [dbo].[tblCPRProcessActivity] PA \n";

        //Add Join to Get Activities beetween date range
        if (objTabRecover.impactedStartDate && objTabRecover.impactedEndDate) {
            sql +=
            "       --Get Activities beetween date rage \n" +
            "        INNER JOIN [dbo].[tblCPRRecurrence] R ON PA.[RecurrenceCode] = R.[Code] \n" +
            "        INNER JOIN ( \n" +
			"            SELECT DISTINCT RD.[IDRecurrence] \n" +
			"            FROM [dbo].[tblCPRRecurrenceXDate] RD \n" +
			"            WHERE RD.[Date] BETWEEN '" + objTabRecover.impactedStartDate + "' AND '" + objTabRecover.impactedEndDate + "' \n" +
		    "        ) AS TDR ON R.[ID] = TDR.[IDRecurrence] \n";
        }

        sql +=
			"        WHERE \n" +
            "           --Get only Active Activities \n" +
            "           PA.[IsDeleted] = 0 \n" +
            " \n" +
            "           --Filter by Physical Location \n" +
            "            AND PA.[IDPhysicalLocation] = T1E.[ID] \n" +
		    "    )	AS TT3 \n" +
            "WHERE  \n";

        if (options.type == "Center") {
            sql += " T1E.[LOCATION_TYPE] = '" + options.type + "' ";
        } else {
            sql += " T1E.[LOCATION_TYPE] = '" + options.type + "' AND (TT2.[TOTAL_EDCFC] + TT3.[TOTAL_CPR]) > 0 ";
        }   

        //Load process information
        _callServer({
            loadingMsgType: "fullLoading",
            loadingMsg: "Loading " + options.type + " Locations...",
            url: '/Ajax/ExecQuery',
            data: { 'pjsonSql': _toJSON(sql) },
            type: "POST",
            success: function (resultList) {

                if (options.onReady) {
                    options.onReady(resultList);
                }
            }
        });
    }
}

//------------------------------------------------
//Tab 2 Process Activities Logic
//------------------------------------------------
var objTabProcessActivity = {
    //Variables
    tabWasLoaded: false,

    //Functionality
    loadTab: function () {
        if (objTabProcessActivity.tabWasLoaded == false) {
            //On Click Go Communication
            $(".btnGoCommunication").click(function () {
                $("#linkGoTabCommunication").click();
            });

            //On Click View Info
            $(".btnViewInfo").click(function () {
                var selectedRow = $.jqxGridApi.getOneSelectedRow("#tblProcessActivities", true);
                if (selectedRow) {
                    _openModalProcessActivityInfo({
                        filters: {
                            source: selectedRow.SR,
                            idProcessActivity: selectedRow.ID
                        },
                        onReady: function () { }
                    });
                }
            });

            //Set tab was loaded to avoid bind multiples times the click or change events.
            objTabProcessActivity.tabWasLoaded = true;
        }

        //Load Header Summary of Communication
        updateHeaderCommunicationSummary();

        //Load eDCFC Activity Summary Table
        objTabProcessActivity.loadEDCFCActivityTable();
    },
    loadEDCFCActivityTable: function () {
        _loadProcessActivityTable({
            idTable: "#tblProcessActivities",
            filters: {
                idLocation: objTabRecover.getObjLocationSelected().id,
                startDate: objTabRecover.impactedStartDate,
                endDate: objTabRecover.impactedEndDate
            },
            onReady: function(eDCFCAcitivityList){
                //Add count of activities to recover
                $("#lblEDCFActivityCount").html("Activities to recover (" + eDCFCAcitivityList.length + ")");
            }
        });
    }

    //BD Data
}

//------------------------------------------------
//Tab 3 Send Communication
//------------------------------------------------
var objTabSendCommunication = {
    //Variables
    tabWasLoaded: false,

    //Functionality
    loadTab: function () {
        if (objTabSendCommunication.tabWasLoaded == false) {
            //Hide menu
            _hideMenu();

            //On Click Go Communication
            $(".btnSendCommunication").click(function () {
                sendCommunication();
                //$("#linkGoTabSummary").click();
            });

            //On Click Add My Email To Test
            $("#btnAddMyEmailToTest").click(function (e) {
                e.preventDefault();
                $("#txtTestEmail").val(_getUserInfo().Email + ";");
            });

            //Load Current Date
            $(".lblCommunicationDate").find("span").html(moment().format("MMM DD, YYYY"));

            //On Change Type of event
            $("#selTypeOfEvent").change(function () {
                if ($("#selTypeOfEvent").val() == "Local") {
                    //When is Local
                    $(".containerSelLocalRecovery").show();

                    //Remove WAD Contacts
                    $('.chk-wad-event').iCheck('uncheck');

                    //Check Local Contacts
                    $('.chk-local-event').iCheck('check');
                } else {
                    //When is WAD
                    $(".containerSelLocalRecovery").hide();
                    $(".selLocalRecovery").val("");

                    //Remove Local Contacts
                    $('.chk-local-event').iCheck('uncheck');

                    //Check WAD Contacts
                    $('.chk-wad-event').iCheck('check');
                }
            });

            //Set tab was loaded to avoid bind multiples times the click or change events.
            objTabSendCommunication.tabWasLoaded = true;
        }

    }

    //BD Data
}

// Validate Communication
function validateCommunication() {
    var objCommunication = getObjCommunication();
    var errorMsg = "";
    var tabCommunicationError = "";
    var result = true;

    // Validate Tab Communication

    if (!objCommunication.comment) {
        tabCommunicationError += " <i class='fa fa-info-circle'></i> Field 'Comment' is missing.<br />";
    }

    if (objCommunication.typeOfEvent == "Local" && !objCommunication.localRecovery) {
        tabCommunicationError += " <i class='fa fa-info-circle'></i> Field 'Local Recovery' is missing.<br />";
    }

    if (tabCommunicationError) {
        errorMsg += "Tab Communication:<br />";
        errorMsg += tabCommunicationError;
    }

    if (errorMsg) {
        _showNotification("error", errorMsg, "CommunicationError", false);

        result = false;
    }

    return result;
}

// Show Modal Confirm Communication
function showModalConfirmCommunication(customOptions) {

    var objCommunication = getObjCommunication();

    //Show Communication Info
    _openModalCommunicationInfo({
        title: "Are you sure you want to send the communication?",
        objCommunication: objCommunication,
        buttons: [{
            name: "Send Communication",
            class: "btn-success",
            onClick: function () {
                if (customOptions) {
                    customOptions.onConfirm();
                }
            }
        }]
    });
}

// Send Communication to all contacts
function sendCommunication() {
    //Get Communication Information 
    if (validateCommunication()) {
        showModalConfirmCommunication({
            onConfirm: function () {
                var objCommunication = getObjCommunication();

                _callProcedure({
                    loadingMsgType: "fullLoading",
                    loadingMsg: "Sending Communication...",
                    name: "[dbo].[spCPRAFrwkCommunicationAdmin]",
                    params: [
                        { "Name": "@Action", "Value": "Start Event" },
                        { "Name": "@IDsActivitiesEDCFC", "Value": getIDsActivities("eDCFC") },
                        { "Name": "@IDsActivitiesCPR", "Value": getIDsActivities("CPR") },
                        { "Name": "@IDLocation", "Value": objCommunication.idLocation },
                        { "Name": "@LocationName", "Value": objCommunication.locationName },
                        { "Name": "@LocationType", "Value": objCommunication.locationTypetoRecover },
                        { "Name": "@LocationActivitiesCount", "Value": objCommunication.locationActivitiesCount },
                        { "Name": "@ImpactedDateStart", "Value": objCommunication.impactedDateStart },
                        { "Name": "@ImpactedDateEnd", "Value": objCommunication.impactedDateEnd },
                        { "Name": "@ImpactedDateRangeText", "Value": objCommunication.impactedDateRangeText },
                        { "Name": "@TypeOfEvent", "Value": objCommunication.typeOfEvent },
                        { "Name": "@LocalRecovery", "Value": objCommunication.localRecovery },
                        { "Name": "@RecoveryAction", "Value": objCommunication.recoveryAction },
                        { "Name": "@Comment", "Value": objCommunication.comment },
                        { "Name": "@SendToFRSSHead", "Value": objCommunication.sendToFRSSHead },
                        { "Name": "@SendToGPL", "Value": objCommunication.sendToGPL },
                        { "Name": "@SendToRPL", "Value": objCommunication.sendToRPL },
                        { "Name": "@SendToOwner", "Value": objCommunication.sendToOwner },
                        { "Name": "@SendToOwnerManager", "Value": objCommunication.sendToOwnerManager },
                        { "Name": "@SendToMaker", "Value": objCommunication.sendToMaker },
                        { "Name": "@SendToMakerManager", "Value": objCommunication.sendToMakerManager },
                        { "Name": "@SendToChecker", "Value": objCommunication.sendToChecker },
                        { "Name": "@SendToCheckerManager", "Value": objCommunication.sendToCheckerManager },
                        { "Name": "@SendToWADOwner", "Value": objCommunication.sendToWADOwner },
                        { "Name": "@SendToWADOwnerManager", "Value": objCommunication.sendToWADOwnerManager },
                        { "Name": "@SendToWADMaker", "Value": objCommunication.sendToWADMaker },
                        { "Name": "@SendToWADMakerManager", "Value": objCommunication.sendToWADMakerManager },
                        { "Name": "@SendToWADChecker", "Value": objCommunication.sendToWADChecker },
                        { "Name": "@SendToWADCheckerManager", "Value": objCommunication.sendToWADCheckerManager },
                        { "Name": "@Status", "Value": "In Progress" },
                        { "Name": "@IsTest", "Value": objCommunication.isTestEvent },
                        { "Name": "@TestEmail", "Value": objCommunication.testEmail },
                        { "Name": "@IsDeleted", "Value": "0" },
                        { "Name": "@ServerURL", "Value": _getViewVar("ServerURL") },
                        { "Name": "SessionSOEID", "Value": _getSOEID() }
                    ],
                    //Show message only for the last 
                    success: {
                        fn: function () {
                            //Check if file exist
                            _showAlert({
                                type: 'success',
                                content: "Notification was sent successfully.",
                                animateScrollTop: true
                            });

                            setTimeout(function () {
                                // Redirect to Finish WAD Event
                                window.location.href = _fixFrameworkURL('/CPR/ListEvent?pviewType=FinishEvent');
                            }, 1500);
                        }
                    }
                });
            }
        });
    }
}

// Get list of IDs Activities
function getIDsActivities(psource) {
    var rows = $.jqxGridApi.getAllRows("#tblProcessActivities");
    var idsArray = [];

    for (var i = 0; i < rows.length; i++) {
        var objActivity = rows[i];

        if (objActivity.SR == psource) {
            idsArray.push(objActivity.ID);
        }
    }

    return idsArray.join(",");
}

// Get all communication settings
function getObjCommunication() {
    var selectedLocation = objTabRecover.getObjLocationSelected();
    var objCommunication = {
        isTestEvent: $(".chkTestEvent").is(":checked"),
        locationTypetoRecover: $(".selToRecover").val(),

        impactedDateStart: objTabRecover.impactedStartDate,
        impactedDateEnd: objTabRecover.impactedEndDate,
        impactedDateRangeText: (objTabRecover.impactedDateRange ? objTabRecover.impactedDateRange : "All"),

        idLocation: selectedLocation.id,
        locationName: selectedLocation.name,
        locationActivitiesCount: selectedLocation.total,

        idProcessActivities: [],
        communicationDate: moment().format("YYYY-MM-DD"),
        typeOfEvent: $(".selTypeOfEvent").val(),
        localRecovery: $(".selLocalRecovery").val(),
        recoveryAction: $(".selRecoveryAction").val(),
        comment: $(".txtComment").val(),
        sendToFRSSHead: $("#chk-frss-head").is(":checked"),
        sendToGPL: $("#chk-gpl").is(":checked"),
        sendToRPL: $("#chk-rpl").is(":checked"),
        sendToOwner: $("#chk-owner").is(":checked"),
        sendToOwnerManager: $("#chk-manager-owner").is(":checked"),
        sendToMaker: $("#chk-maker").is(":checked"),
        sendToMakerManager: $("#chk-manager-maker").is(":checked"),
        sendToChecker: $("#chk-checker").is(":checked"),
        sendToCheckerManager: $("#chk-manager-checker").is(":checked"),
        sendToWADOwner: $("#chk-wad-owner").is(":checked"),
        sendToWADOwnerManager: $("#chk-wad-manager-owner").is(":checked"),
        sendToWADMaker: $("#chk-wad-maker").is(":checked"),
        sendToWADMakerManager: $("#chk-wad-manager-maker").is(":checked"),
        sendToWADChecker: $("#chk-wad-checker").is(":checked"),
        sendToWADCheckerManager: $("#chk-wad-manager-checker").is(":checked"),
        testEmail: $("#txtTestEmail").val()
    };

    return objCommunication;
}

// Update Header Summary of Communication
function updateHeaderCommunicationSummary() {
    var objLocationSelected = objTabRecover.getObjLocationSelected();

    //Update Recover label
    $(".lblRecover").html(objLocationSelected.name);

    //Update Dates Impacted label
    if (objTabRecover.impactedStartDate && objTabRecover.impactedEndDate) {
        $(".containerLblDatesImpacted").show();
        $(".lblImpactedDates").html(objTabRecover.impactedDateRange);
    } else {
        $(".containerLblDatesImpacted").hide();
    }

    //Updat count eDCFC Activities
    $(".lblProcessActivityCount").html(objLocationSelected.total);
}

// =================================================
// START PENDING TO DELETE
var processList;

function loadTableProcess() {
    var idTable = "#tblProcess";

    //Get Current Process List
    _getProcessData({
        onReady: function (resultList) {
            loadTable(resultList);
        }
    });

    function loadTable(processList) {
        //Show only last level process
        var lastLevelProcessList = _findAllObjByProperty(processList, 'CantChilds', 0);

        //Create table
        $.jqxGridApi.create({
            showTo: idTable,
            options: {
                //for comments or descriptions
                height: (($(window).height() < 700) ? "480" : "700"),
                autoheight: false,
                autorowheight: false,
                showfilterrow: true,
                sortable: true,
                editable: true,
                selectionmode: "singlerow"
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                dataBinding: "Large Data Set Local",
                rows: lastLevelProcessList
            },
            //type: string - text - number - int - float - date - time 
            //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
            //cellsformat: ddd, MMM dd, yyyy h:mm tt
            columns: [
                { name: 'ID', type: 'string', hidden: true },
                { name: 'CantChilds', type: 'number', hidden: true },
                { name: 'IDParent', type: 'string', hidden: true },
                { name: 'Level', type: 'number', hidden: true },
                { name: 'Category', type: 'string', hidden: true },
                { name: 'GPLSOEID', type: 'string', hidden: true },
                {
                    name: 'Name', text: 'Name', width: '400px', type: 'html', filtertype: 'input', cellsrenderer: function (rowIndex, datafield, value) {
                        var resultHTML = '';
                        var dataRecord = $(idTable).jqxGrid('getrowdata', rowIndex);

                        var levelText = 'L' + dataRecord.Level;
                        var levelHtml;

                        switch (dataRecord.Level) {
                            case 2:
                                levelHtml = '<span class="badge badge-info">' + levelText + '</span>';
                                break;
                            case 3:
                                levelHtml = '<span class="badge badge-purple">' + levelText + '</span>';
                                break;
                            case 4:
                                levelHtml = '<span class="badge badge-accent">' + levelText + '</span>';
                                break;
                            case 5:
                                levelHtml = '<span class="badge badge-primary">' + levelText + '</span>';
                                break;
                            case 6:
                                levelHtml = '<span class="badge badge-warning">' + levelText + '</span>';
                                break;
                            default:
                                levelHtml = levelText;
                                break;
                        }

                        var processSummaryTitle = _getProcessTitleSummary(processList, dataRecord);
                        if (dataRecord.CantChilds > 0) {
                            resultHTML =
                                    ' <span style="line-height: 28px; padding-left: 2px;" title="' + processSummaryTitle + '" > ' + levelHtml + ' ' + value + ' (' + dataRecord.CantChilds + ')</span>';
                        } else {
                            resultHTML =
                                    ' <span style="line-height: 28px; padding-left: 2px;" title="' + processSummaryTitle + '" > ' + levelHtml + ' ' + value + ' (' + dataRecord.CantChilds + ')</span>';
                        }

                        return resultHTML;
                    }
                },
                { name: 'GPLName', text: 'GPL', type: 'string', filtertype: 'checkedlist', width: '180px' },
                { name: 'RPLJson', type: 'string', hidden: true },
                {
                    name: 'RPLCount', text: 'RPLs', width: '60px', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                        var dataRecord = $(idTable).jqxGrid('getrowdata', rowIndex);
                        var htmlResult = '<span style="line-height:31px;margin-left:12px;">' + (dataRecord.RPLCount > 0 ? dataRecord.RPLCount : "") + '</span>';

                        //Add RPLs
                        if (dataRecord.RPLJson) {
                            var rows = JSON.parse(dataRecord.RPLJson);
                            if (rows.length > 0) {
                                var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                                for (var i = 0; i < rows.length; i++) {
                                    var tempData = rows[i];
                                    infoHtml += tempData.EntityToLeadName + ": " + tempData.RPLName + " (" + tempData.RPLSOEID + ")<br>";
                                }

                                infoHtml += "</div>";

                                htmlResult +=
                                    '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="RPL Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                    '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/info.png"/> ' +
                                    '</div>';
                            }
                        }

                        return htmlResult;
                    }
                },
                { name: 'LeadJson', type: 'string', hidden: true },
                {
                    name: 'LeadCount', text: 'Leads', width: '60px', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                        var dataRecord = $(idTable).jqxGrid('getrowdata', rowIndex);
                        var htmlResult = '<span style="line-height:31px;margin-left:12px;">' + (dataRecord.LeadCount > 0 ? dataRecord.LeadCount : "") + '</span>';

                        //Add Leads
                        if (dataRecord.LeadJson) {
                            var rows = JSON.parse(dataRecord.LeadJson);
                            if (rows.length > 0) {
                                var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                                for (var i = 0; i < rows.length; i++) {
                                    var tempData = rows[i];
                                    infoHtml += tempData.EntityToLeadName + ": " + tempData.LeadName + " (" + tempData.LeadSOEID + ")<br>";
                                }

                                infoHtml += "</div>";

                                htmlResult +=
                                    '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="Lead Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                    '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/info.png"/> ' +
                                    '</div>';
                            }
                        }

                        return htmlResult;
                    }
                },
                { name: 'OwnerJson', type: 'string', hidden: true },
                {
                    name: 'OwnerCount', text: 'Owners', width: '60px', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                        var dataRecord = $(idTable).jqxGrid('getrowdata', rowIndex);
                        var htmlResult = '<span style="line-height:31px;margin-left:12px;">' + (dataRecord.OwnerCount > 0 ? dataRecord.OwnerCount : "") + '</span>';

                        //Add Owners
                        if (dataRecord.OwnerJson) {
                            var rows = JSON.parse(dataRecord.OwnerJson);
                            if (rows.length > 0) {
                                var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                                for (var i = 0; i < rows.length; i++) {
                                    var tempData = rows[i];
                                    infoHtml += tempData.EntityToOwnerName + ": " + tempData.OwnerName + " (" + tempData.OwnerSOEID + ")<br>";
                                }

                                infoHtml += "</div>";

                                htmlResult +=
                                    '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="Owner Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                    '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/info.png"/> ' +
                                    '</div>';
                            }
                        }

                        return htmlResult;
                    }
                },
                { name: 'WADContactJson', type: 'string', hidden: true },
                {
                    name: 'WADContactCount', text: 'WAD Contacts', width: '100px', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                        var dataRecord = $(idTable).jqxGrid('getrowdata', rowIndex);
                        var htmlResult = '<span style="line-height:31px;margin-left:12px;">' + (dataRecord.WADContactCount > 0 ? dataRecord.WADContactCount : "") + '</span>';

                        //Add WADContacts
                        if (dataRecord.WADContactJson) {
                            var rows = JSON.parse(dataRecord.WADContactJson);
                            if (rows.length > 0) {
                                var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                                for (var i = 0; i < rows.length; i++) {
                                    var tempData = rows[i];
                                    infoHtml += tempData.EntityToWADContactName + ": " + tempData.WADContactName + " (" + tempData.WADContactSOEID + ")<br>";
                                }

                                infoHtml += "</div>";

                                htmlResult +=
                                    '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="WADContact Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                    '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/info.png"/> ' +
                                    '</div>';
                            }
                        }

                        return htmlResult;
                    }
                },
                { name: 'ApplicationsJson', type: 'string', hidden: true },
                {
                    name: 'ApplicationsCount', text: 'Applications / EUCs', width: '140px', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                        var dataRecord = $(idTable).jqxGrid('getrowdata', rowIndex);
                        var htmlResult = '<span style="line-height:31px;margin-left:12px;">' + (dataRecord.ApplicationsCount > 0 ? dataRecord.ApplicationsCount : "") + '</span>';

                        //Add Applicationss
                        if (dataRecord.ApplicationsJson) {
                            var rows = JSON.parse(dataRecord.ApplicationsJson);
                            if (rows.length > 0) {
                                var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                                for (var i = 0; i < rows.length; i++) {
                                    var tempData = rows[i];
                                    infoHtml += tempData.EntityToApplicationsName + ": " + tempData.ApplicationsName + " (" + tempData.ApplicationsSOEID + ")<br>";
                                }

                                infoHtml += "</div>";

                                htmlResult +=
                                    '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="Applications Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                    '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/info.png"/> ' +
                                    '</div>';
                            }
                        }

                        return htmlResult;
                    }
                },
                { name: 'MCAControlsJson', type: 'string', hidden: true },
                {
                    name: 'MCAControlsCount', text: 'MCA Controls', width: '140px', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                        var dataRecord = $(idTable).jqxGrid('getrowdata', rowIndex);
                        var htmlResult = '<span style="line-height:31px;margin-left:12px;">' + (dataRecord.MCAControlsCount > 0 ? dataRecord.MCAControlsCount : "") + '</span>';

                        //Add MCAControlss
                        if (dataRecord.MCAControlsJson) {
                            var rows = JSON.parse(dataRecord.MCAControlsJson);
                            if (rows.length > 0) {
                                var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                                for (var i = 0; i < rows.length; i++) {
                                    var tempData = rows[i];
                                    infoHtml += tempData.EntityToMCAControlsName + ": " + tempData.MCAControlsName + " (" + tempData.MCAControlsSOEID + ")<br>";
                                }

                                infoHtml += "</div>";

                                htmlResult +=
                                    '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="MCAControls Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                    '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/info.png"/> ' +
                                    '</div>';
                            }
                        }

                        return htmlResult;
                    }
                },
                { name: 'ResourceJson', type: 'string', hidden: true },
                {
                    name: 'ResourceCount', text: 'Blueworks / Links', width: '140px', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                        var dataRecord = $(idTable).jqxGrid('getrowdata', rowIndex);
                        var htmlResult = '<span style="line-height:31px;margin-left:12px;">' + (dataRecord.ResourceCount > 0 ? dataRecord.ResourceCount : "") + '</span>';

                        //Add Resources
                        if (dataRecord.ResourceJson) {
                            var rows = JSON.parse(dataRecord.ResourceJson);
                            if (rows.length > 0) {
                                var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                                for (var i = 0; i < rows.length; i++) {
                                    var tempData = rows[i];
                                    infoHtml += "<a href='" + tempData.Link + "' target='_blank'>" + tempData.Link + "</a>" + (tempData.Description ? (": " + tempData.Description) : "") + "<br>";
                                }

                                infoHtml += "</div>";

                                htmlResult +=
                                    '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="Resource Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                    '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/info.png"/> ' +
                                    '</div>';
                            }
                        }

                        return htmlResult;
                    }
                },
                { name: 'Schedule', text: 'Schedule', type: 'string', filterable: false, width: '180px' }
            ],
            ready: function () {
                $(idTable).on("rowclick", function (event) {
                    //Show tooltips
                    _GLOBAL_SETTINGS.tooltipsPopovers();
                });
            }
        });
    }
}

function loadTabProcessList() {
    //Load Filter Process
    var $filterContainer = $("#filter-process");
    _loadFilterProcess("selProcessL2", 2, null, $filterContainer);
    $("#selProcessL2").change(function () {

        //Clear Process L4
        $("#selProcessL4").val("0");
        $("#selProcessL4").contents().remove();

        //Clear Process L5
        $("#selProcessL5").val("0");
        $("#selProcessL5").contents().remove();

        //Load Process L3
        _loadFilterProcess("selProcessL3", 3, true, $filterContainer);

        //Load Table
        loadTableProcess();
    });

    $("#selProcessL3").change(function () {

        //Clear Process L5
        $("#selProcessL5").val("0");
        $("#selProcessL5").contents().remove();

        //Load Process L4
        _loadFilterProcess("selProcessL4", 4, true, $filterContainer);

        //Load Table
        loadTableProcess();
    });

    $("#selProcessL4").change(function () {

        //Load Process L5
        _loadFilterProcess("selProcessL5", 5, true, $filterContainer);

        //Load Table
        loadTableProcess();
    });

    $("#selProcessL5").change(function () {
        //Load Table
        loadTableProcess();
    });

    //Load Table
    loadTableProcess();
}
// END PENDING TO DELETE
// =================================================