﻿/// <reference path="../../Shared/plugins/util/global.js" />

$(document).ready(function () {
    loadTableState();
    loadTableCity();
});

//Tables loading
function loadTableState() {
    $.jqxGridApi.create({
        showTo: "#tbl_StateList",
        options: {
            //for comments or descriptions
            height: "500",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_STATELIST]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
			{ name: 'StateID', type: 'string', hidden: true },
			{ name: 'Acrony', text: "Acrony", type: 'string', width: '20%' },
            { name: 'StateName', type: 'State Name', type: 'string', width: '60%' },
            { name: 'Active', type: 'integer', text: "Active", width: '20%' }
        ],

        ready: function () {
            
        }
    });
}

function loadTableCity() {
    $.jqxGridApi.create({
        showTo: "#tbl_CityList",
        options: {
            //for comments or descriptions
            height: "500",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_CITYLIST]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
			{ name: 'CityID', type: 'string', hidden: true },
			{ name: 'CityName', text: "City Name", type: 'string', width: '20%' },
            { name: 'StateID', type: 'string', hidden: true },
            { name: 'State', type: 'State Name', type: 'string', width: '60%' },
            { name: 'Active', type: 'integer', text: "Active", width: '20%' }
        ],

        ready: function () {

        }
    });
}

//Methods
function generateStateForm(Taction) {
    var Sid;
    var Sname;
    var Sacronym;
    var active;
    

    if (Taction == 'Edit') {
        var selectedRow = $.jqxGridApi.getOneSelectedRow("#tbl_StateList", true);

        Sid = selectedRow.StateID
        Sname = selectedRow.StateName;
        Sacronym = selectedRow.Acrony;
        active = selectedRow.Active;

    } else {
        Sid = 0;
        Sname = '';
        Sacronym = '';
        active = 'true';
    }

    var StateHtml =
        '<div class="row">' +
        '    <div class="col-md-12">' +
        '        <p>Please fill the state information</p>' +
        '        <label class="control-label" for="txtStateId">ID:</label>' +
        '        <label class="control-label" id="txtStateId">'+ Sid+ '</label>' +
        '        <div class="col-md-12">' +
        '            <label class="control-label" for="txtStateName">State Name</label>' +
        '            <input type="text" class="form-control" id="txtStateName" value="' + Sname + '">' +
        '        </div>' +
        '        <div class="col-md-6">' +
        '            <label class="control-label" for="txtStateAcronym">State acronym</label>' +
        '            <input type="text" class="form-control" id="txtStateAcronym" value="' + Sacronym + '">' +
        '        </div>' +
        '        <div class="col-md-6">' +
        '            <div class="form-group" style="margin-top: 15px;">' +
        '                <ul class="list-group ">' +
        '                    <li class="list-group-item" style="background-color:transparent !important; color:rgba(118, 118, 118, 1.0) !important; border-color: #e1e1e1;">' +
    '                        Is this State activated?' +
        '                        <div class="material-switch pull-right">' +
        '                            <input id="chkStateActive" name="chkLogidActive" type="checkbox" checked="' + active + '"/>' +
        '                            <label for="chkStateActive" class="label-success"></label>' +
        '                        </div>' +
        '                    </li>' +
        '                </ul>' +
        '            </div>' +
        '        </div>' +
        '    </div>' +
        '</div>';

    _showModal({
        width: "75%",
        title: "State Editor Form",
        contentHtml: StateHtml,
        buttons: [{
            name: "Save",
            class: "btn-success",
            closeModalOnClick: false,
            onClick: function ($modal) {
                var action = 'Add'
                var Sid = $modal.find("#txtStateId").text();
                var Sname = $modal.find("#txtStateName").val();
                var Sacronym = $modal.find("#txtStateAcronym").val();
                var active;
                switch ($modal.find("#chkStateActive").is(':checked')) {
                    case true: active = 1;
                        break;
                    case false: active = 0;
                        break;
                }

                if (Sname != "" && Sacronym != "") {
                    if (Sid > 0) {
                        action = 'Edit'
                    }

                    _callProcedure({
                        loadingMsgType: "fullLoading",
                        loadingMsg: "Validating State list..",
                        name: "NSP_AC_STATE",
                        params: [
                            { Name: '@Action', Value: action},
                            { Name: '@StateID', Value: Sid },
                            { Name: '@StateName', Value: Sname },
                            { Name: '@StateAcronym', Value: Sacronym },
                            { Name: '@Active', Value: active }
                        ],
                        success: {
                            fn: function (responseList) {
                                $modal.find(".close").click();
                            }
                        }
                    });
   
                    _showNotification("success", "State " + Sname + " has been saved", "AlertSave");
                    loadTableState();
                } else {
                    _showNotification("Error", "There is missing data", "AlertSave");
                }
            }
        }],
        addCloseButton: true,
        onReady: function ($modal) {

        }
    });
}

function generateCityForm(Taction) {
    var Cid;
    var Cname
    var Sid;
    var Sname;
    var active;

    if (Taction == 'Edit') {
        var selectedRow = $.jqxGridApi.getOneSelectedRow("#tbl_CityList", true);

        Cid = selectedRow.CityID;
        Cname = selectedRow.CityName;
        Sid = selectedRow.StateID;
        Sname = selectedRow.State;
        active = selectedRow.Active;
    } else {
        Cid = 0;
        Cname = '';
        Sid = 0;
        Sname = '';
        active = 'true';
    }

    var CityHtml =
        '<div class="row">' +
        '    <div class="col-md-12">' +
        '        <p>Please fill the city information</p>' +
        '        <label class="control-label" for="txtCityId">ID:</label>' +
        '        <label class="control-label" id="txtCityId">' + Cid + '</label>' +
        '        <label Style="display:none" class="control-label" id="txtStateId">' + Sid + '</label>' +
        '        <div class="col-md-12">' +
        '            <label class="control-label" for="txtStateName">City Name</label>' +
        '            <input type="text" class="form-control" id="txtCityName" value="' + Cname + '">' +
        '        </div>' +
       '         <div class="col-md-6">' +
        '           <label class="control-label" for="drp_States">Select a State</label>' +
        '           <select class="form-control selectpicker" id="drp_States"></select>' +
        '        </div>' +
        '        <div class="col-md-6">' +
        '            <div class="form-group" style="margin-top: 15px;">' +
        '                <ul class="list-group ">' +
        '                    <li class="list-group-item" style="background-color:transparent !important; color:rgba(118, 118, 118, 1.0) !important; border-color: #e1e1e1;">' +
    '                        Is this City activated?' +
        '                        <div class="material-switch pull-right">' +
        '                            <input id="chkCityActive" name="chkCityActive" type="checkbox" checked="' + active + '"/>' +
        '                            <label for="chkCityActive" class="label-success"></label>' +
        '                        </div>' +
        '                    </li>' +
        '                </ul>' +
        '            </div>' +
        '        </div>' +
        '    </div>' +
        '</div>';

    _showModal({
        width: "75%",
        title: "City Editor Form",
        contentHtml: CityHtml,
        buttons: [{
            name: "Save",
            class: "btn-success",
            closeModalOnClick: false,
            onClick: function ($modal) {
                var action = 'Add'
                var Cid = $modal.find("#txtCityId").text();
                var Cname = $modal.find("#txtCityName").val();
                var Sid = $modal.find("#drp_States").val();
                var Sname = $modal.find("#drp_States").text();
                var active;

                switch ($modal.find("#chkCityActive").is(':checked')) {
                    case true: active = 1;
                        break;
                    case false: active = 0;
                        break;
                }

                if (Sname != "" && Sacronym != "") {
                    if (Sid > 0) {
                        action = 'Edit'
                    }

                    _callProcedure({
                        loadingMsgType: "fullLoading",
                        loadingMsg: "Validating State list..",
                        name: "NSP_AC_STATE",
                        params: [
                            { Name: '@Action', Value: action },
                            { Name: '@StateID', Value: Sid },
                            { Name: '@StateName', Value: Sname },
                            { Name: '@StateAcronym', Value: Sacronym },
                            { Name: '@Active', Value: active }
                        ],
                        success: {
                            fn: function (responseList) {
                                $modal.find(".close").click();
                            }
                        }
                    });

                    _showNotification("success", "State " + Sname + " has been saved", "AlertSave");
                    loadTableState();
                } else {
                    _showNotification("Error", "There is missing data", "AlertSave");
                }
            }
        }],
        addCloseButton: true,
        onReady: function ($modal) {
            var Sid;

            Sid = $modal.find("#txtStateId").text();

            qry = "SELECT [StateID] AS [id],[Acrony] + ' - ' + [StateName] AS [text] FROM [dbo].[tblTMRE_AdmState] WHERE Active = 1;";
            
            loadDropDowns(qry, Sid, $modal.find("#drp_States"));
        }
    });
}

function loadDropDowns(selectQuery, selectedId, dropList) {
    var $select;

    if (typeof dropList == "string") {
        $select = $("#" + dropList + "");
    } else {
        $select = dropList;
    }

    //Set default value  
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Log ID Details...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify(selectQuery) },
        type: "post",
        success: function (resultList) {
            $select.contents().remove();
            $select.append($('<option>', { value: "0", text: "" }));
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $select.append($('<option>', { value: objFunction.id, text: objFunction.text, selected: (objFunction.id == selectedId) }));
            }
        }
    });
};

//Buttons actions
$("#btnAddNewState").click(function () {
    generateStateForm('Add');
});

$("#btnEditState").click(function () {
    generateStateForm('Edit');
});

$("#btnAddNewCity").click(function () {
    generateCityForm('Add');
});

$("#btnEditCity").click(function () {
    generateCityForm('Edit');
});