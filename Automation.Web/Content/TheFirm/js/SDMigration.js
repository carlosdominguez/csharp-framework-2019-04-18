﻿$(document).ready(function () {

});

$("#btnUpdate").click(function () {
    var htmlContentModal = "<b>This action will update the location for all the files recorded in the database<br/>";
    _showModal({
        width: '35%',
        modalId: "modalDel",
        addCloseButton: true,
        buttons: [{
            name: "Yes",
            class: "btn-danger",
            onClick: function () {
                _callProcedure({
                    loadingMsgType: "fullLoading",
                    loadingMsg: "Updating Docs..",
                    name: "[dbo].[spFirmUpdateSharedriveMassive]",
                    params: [
                        { Name: '@docLocation', Value: $("#txt_SDDirctory").val() }
                    ],
                    success: {
                        fn: function (responseList) {
                            _showNotification("success", "Files Location have been Updated");
                        }
                    },
                });
            }
        }],
        title: "Are you sure you want to update the sharedrive location?",
        contentHtml: htmlContentModal
    });
});