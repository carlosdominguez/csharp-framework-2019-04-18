﻿$(document).ready(function () {
    loadPartialViewDBTable();
});

function loadPartialViewDBTable() {
    _callServer({
        loadingMsg: "Loading table '" + _getViewVar("TableName") + "' information...",
        url: "/Configuration/PartialViewDBTable?" + $.param({
            ptbl: _getViewVar("TableName"),
            pcolKey: _getViewVar("ColumnKey")
        }),
        success: function (htmlResponse) {
            $("#content-db-table").html(htmlResponse);
        }
    });
};