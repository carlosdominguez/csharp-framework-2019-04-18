﻿/// <reference path="../Shared/plugins/util/global.js" />

$(document).ready(function () {
    //On click delete
    $(".btnDelete").click(function (e) {
        deleteDL();
    });

    //On click add dl
    $(".btnAdd").click(function (e) {
        addDL();
    });

    //On click Get email information
    $("#SAMAccountACInfo").click(function (e) {
        e.preventDefault();
        getSAMAccountACInformation();
    });

    //On click Get email information
    $("#EmailACInfo").click(function (e) {
        e.preventDefault();
        getEmailACInformation();
    });

    //On click get users of SAMAccountName from Input
    $("#ACUsersBySAMAccountNameInput").click(function (e) {
        e.preventDefault();
        getACUsersBySAMAccountNameInput();
    });

    //On click get users of DL from Input
    $("#ACUsersByDLInput").click(function (e) {
        e.preventDefault();
        getACUsersByDLInput();
    });

    //On click get users of DL
    $("#ACUsersByDL").click(function (e) {
        e.preventDefault();
        getACUsersByDL();
    });

    //On click get groups of User
    $("#ACGroupsBySOEID").click(function (e) {
        e.preventDefault();
        getACGroupsBySOEID();
    });

    //On click get groups of User
    $("#ACGroupsBySOEIDAM").click(function (e) {
        e.preventDefault();
        getACGroupsBySOEIDAM();
    });

    //On click Sync Selected DL
    $("#ACSyncSelected").click(function (e) {
        e.preventDefault();
        syncSelected(false);
    });

    //On click Sync Selected DL IGNORE ACWhenChanged
    $("#ACSyncSelectedForce").click(function (e) {
        e.preventDefault();
        syncSelected(true);
    });

    //On click Sync All DL
    $("#ACSyncAll").click(function (e) {
        e.preventDefault();
        syncAll();
    });

    //On click view contacts
    $(".btnViewContacts").click(function (e) {
        viewContacts();
    });

    //Update User DLs
    _updateCurrentUserDLs({
        onSuccess: function () {
            //Load table
            loadTableDLs();
        }
    });
    
});

function loadTableDLs() {
    $.jqxGridApi.create({
        showTo: "#tblDLs",
        options: {
            //for comments or descriptions
            height: "500",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: true,
            sortable: true,
            editable: true,
            groupable: false
        },
        sp: {
            Name: "[Automation].[dbo].[spAFrwkAdminDL]",
            Params: [
                { Name: "@Action", Value: "List" }
            ]
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        columns: [
            //type: string - text - number - int - float - date - time 
            //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
            //cellsformat: ddd, MMM dd, yyyy h:mm tt
            { name: 'ID', type: 'number', width: '50px', filtertype: 'input' },
            { name: 'Guid', type: 'string', width: '300px', filtertype: 'input' },
            { name: 'DisplayName', text: 'DisplayName', width: '350px', type: 'string', filtertype: 'input' },
            { name: 'Email', text: 'Email', width: '350px', type: 'string', filtertype: 'input' },
            { name: 'ManagedBy', text: 'Managed By', width: '90px', type: 'string', filtertype: 'checkedlist' },
            { name: 'UserCount', text: "Users", type: 'number', width: '60px', filtertype: 'input' },
            { name: 'DLCount', text: "DLs", type: 'number', width: '60px', filtertype: 'input' },
            { name: 'CreatedBy', text: 'Created By', width: '90px', type: 'string', filtertype: 'checkedlist' },
            { name: 'CreatedDate', text: 'Created Date', width: '100px', type: 'date', cellsformat: "yyyy-MM-dd", filtertype: 'input' },
            { name: 'ModifiedBy', text: 'Modified By', width: '90px', type: 'string', filtertype: 'checkedlist' },
            { name: 'ModifiedDate', text: 'Modified Date', width: '100px', type: 'date', cellsformat: "yyyy-MM-dd", filtertype: 'input' },
            { name: 'ACWhenChanged', text: 'AC When Changed', width: '130px', type: 'date', cellsformat: "yyyy-MM-dd", filtertype: 'input' }
        ],
        ready: function () { }
    });
}

function viewContacts() {
    var selectedRow = $.jqxGridApi.getOneSelectedRow("#tblDLs", true);
    if (selectedRow) {
        _openModalViewContactsOfDL({
            title: "Contacts of '" + selectedRow.DisplayName + "'",
            idDL: selectedRow.ID
        });
    }
}

function deleteDL() {
    var selectedRow = $.jqxGridApi.getOneSelectedRow("#tblDLs", true);
    if (selectedRow) {
        var htmlContentModal = "";
        htmlContentModal += "<b>DL Name: </b>" + selectedRow['DisplayName'] + "<br/>";

        _showModal({
            width: '40%',
            modalId: "modalDelRow",
            addCloseButton: true,
            buttons: [{
                name: "<i class='fa fa-trash-o'></i> Delete",
                class: "btn-danger",
                onClick: function () {
                    _callProcedure({
                        loadingMsgType: "topBar",
                        loadingMsg: "Loading labels",
                        name: "[Automation].[dbo].[spAFrwkAdminDL]",
                        params: [
                            { "Name": "@Action", "Value": "Delete" },
                            { "Name": "@IDDL", "Value": selectedRow.ID },
                            { "Name": "@SessionSOEID", "Value": _getSOEID() }
                        ],
                        success: function (response) {
                            //Show message
                            _showAlert({
                                id: "DeleteDL",
                                type: 'success',
                                title: "Message",
                                content: "DL '" + selectedRow.DisplayName + "' was deleted successfull"
                            });

                            //Refresh table
                            loadTableDLs();
                        }
                    });
                }
            }],
            title: "Are you sure you want to delete this row?",
            contentHtml: htmlContentModal
        });
    }
}

function addDL(){
    var emailDL = _getEmailFromDLSummary($("#txtDL").val());
    if(emailDL){
        _addAutomationIDDL({
            emailDL: emailDL,
            onSuccess: function (idDLCreated) {
                //Show user message
                _showAlert({
                    id: "AddDL",
                    type: 'success',
                    title: "Message",
                    content: "DL was created successfull with <b>ID: " + idDLCreated + "</b>"
                });

                //Refresh table
                loadTableDLs();
            }
        });
    } else {
        _showAlert({
            type: 'error',
            title: "Message",
            content: "Please enter the DL"
        });
    }
}

function syncAll() {

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Updating all DLs...",
        url: '/Configuration/DLManager/UpdateAllAutomationDL',
        data: {},
        type: "GET",
        success: function (countDLUpdated) {
            
            //Refresh Table
            loadTableDLs();

            //Show User Message
            _showAlert({
                type: 'success',
                title: "Message",
                content: countDLUpdated + " DLs where updated."
            });

        }
    });
}

function syncSelected(forced) {

    var selectedRow = $.jqxGridApi.getOneSelectedRow("#tblDLs", true);
    if (selectedRow) {
        var emailLastUpdate = selectedRow.ModifiedDate;
        if (!emailLastUpdate) {
            emailLastUpdate = selectedRow.CreatedDate;
        }
        emailLastUpdate = moment(emailLastUpdate).format('YYYY-MM-DD');

        _callServer({
            loadingMsgType: "fullLoading",
            loadingMsg: "Updating Selected DLs...",
            url: '/Configuration/DLManager/UpdateAutomationDL',
            data: {
                pemailGuid: selectedRow.Guid,
                pemailLastUpdate: emailLastUpdate,
                pforced: forced
            },
            type: "GET",
            success: function (countDLUpdated) {

                //Refresh Table
                loadTableDLs();

                //Show User Message
                _showAlert({
                    type: 'success',
                    title: "Message",
                    content: "DL '" + selectedRow.DisplayName + "' was updated successful with " + countDLUpdated + " DLs of dependency."
                });

            }
        });
    }
}

function getACUsersBySAMAccountNameInput() {
    var SAMAccountName = _getEmailFromDLSummary($("#txtDL").val());
    if (SAMAccountName) {
        var url = _getViewVar("SubAppPath") + "/Configuration/DLManager/GetACUsersBySAMAccountName/?" + $.param({
            pSAMAccountName: SAMAccountName,
            pappKey: _getViewVar("AppKey")
        });

        window.open(url);

    } else {
        _showAlert({
            type: 'error',
            title: "Message",
            content: "Please enter the SAMAccountName"
        });
    }
}

function getACUsersByDLInput() {
    var emailDL = _getEmailFromDLSummary($("#txtDL").val());
    if (emailDL) {
        var url = _getViewVar("SubAppPath") + "/Configuration/DLManager/GetACUsersByDL/?" + $.param({
            pemailDL: emailDL,
            pappKey: _getViewVar("AppKey")
        });

        window.open(url);

    } else {
        _showAlert({
            type: 'error',
            title: "Message",
            content: "Please enter the DL"
        });
    }
}

function getACUsersByDL() {
    var selectedRow = $.jqxGridApi.getOneSelectedRow("#tblDLs", true);

    if (selectedRow) {
        var url = _getViewVar("SubAppPath") + "/Configuration/DLManager/GetACUsersByDL/?" + $.param({
            pemailGuid: selectedRow.Guid,
            pappKey: _getViewVar("AppKey")
        });

        window.open(url);

    } 
}

function getACGroupsBySOEID() {
    var soeid = $("#txtDL").val();

    if (soeid) {
        var url = _getViewVar("SubAppPath") + "/Configuration/DLManager/GetACGroupsBySOEID/?" + $.param({
            pacsoeid: soeid,
            pappKey: _getViewVar("AppKey")
        });

        window.open(url);

        //_callServer({
        //    loadingMsgType: "fullLoading",
        //    loadingMsg: "Getting Active Directory Email Information...",
        //    url: '/Configuration/DLManager/GetAllPropertiesOfEmail',
        //    data: {
        //        pemail: $("#txtDL").val()
        //    },
        //    type: "GET",
        //    success: function (response) {
        //        console.log(response);
        //    }
        //});
    } else {
        _showAlert({
            type: 'error',
            title: "Message",
            content: "Please enter the employee SOEID"
        });
    }
}

function getACGroupsBySOEIDAM() {
    var soeid = $("#txtDL").val();

    if (soeid) {
        var url = _getViewVar("SubAppPath") + "/Configuration/DLManager/GetACGroupsBySOEIDAM/?" + $.param({
            pacsoeid: soeid,
            pappKey: _getViewVar("AppKey")
        });

        window.open(url);
    } else {
        _showAlert({
            type: 'error',
            title: "Message",
            content: "Please enter the employee SOEID"
        });
    }
}

function getEmailACInformation() {
    var email = $("#txtDL").val();

    if (email) {
        var url = _getViewVar("SubAppPath") + "/Configuration/DLManager/GetAllPropertiesOfEmail/?" + $.param({
            pemail: email,
            pappKey: _getViewVar("AppKey")
        });

        window.open(url);

        //_callServer({
        //    loadingMsgType: "fullLoading",
        //    loadingMsg: "Getting Active Directory Email Information...",
        //    url: '/Configuration/DLManager/GetAllPropertiesOfEmail',
        //    data: {
        //        pemail: $("#txtDL").val()
        //    },
        //    type: "GET",
        //    success: function (response) {
        //        console.log(response);
        //    }
        //});
    } else {
        _showAlert({
            type: 'error',
            title: "Message",
            content: "Please enter the email"
        });
    }
}

function getSAMAccountACInformation() {
    var paccount = $("#txtDL").val();

    if (paccount) {
        var url = _getViewVar("SubAppPath") + "/Configuration/DLManager/GetAllPropertiesBySAMAccount/?" + $.param({
            psAMAccount: paccount,
            pappKey: _getViewVar("AppKey")
        });

        window.open(url);
    } else {
        _showAlert({
            type: 'error',
            title: "Message",
            content: "Please enter the email"
        });
    }
}