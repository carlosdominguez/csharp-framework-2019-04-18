﻿$(document).ready(function () {
    //Add new row to the table
    $("#btnAddDBStructure").click(function () {
        addNewRowBDStructure();
    });

    //Delete selected row
    $("#btnDeleteBDStructure").click(function () {
        deleteRowDBStructure();
    });

    //Save information to json /Content/Sources/database-structure.txt
    $("#btnSaveDBStructure").click(function () {
        saveBDStructure();
    });

    //Reload Table
    $("#btnLoadDBStructureTable").click(function () {
        loadTableBDStructure();
    });

    //Load DB Structure table
    loadTableBDStructure();
});

function deleteRowDBStructure() {
    //Delete 
    var objRowSelected = $.jqxGridApi.getOneSelectedRow("#tblDBStructure", true);
    if (objRowSelected) {
        var htmlContentModal =
            "<b>Prefix:</b>" + objRowSelected.prefix + "<br/>" +
            "<b>Type:</b>" + objRowSelected.objectType + "<br/>" +
            "<b>Name:</b>" + objRowSelected.objectName + "<br/>";

        _showModal({
            modalId: "modalDelBDStructure",
            title: "Are you sure you want to delete this row?",
            contentHtml: htmlContentModal,
            buttons: [{
                name: "Confirm",
                class: "btn-danger",
                closeModalOnClick: true,
                onClick: function ($modal) {
                    $("#tblDBStructure").jqxGrid('deleterow', objRowSelected.uid);
                }
            }],
            addCloseButton: true
        });
    }
}

function addNewRowBDStructure() {
    var $jqxGrid = $("#tblDBStructure");
    var datarow = {
        numOrder: "0",
        isSaved: "0",
        key: "",
        prefix: "",
        objectType: "",
        objectName: "",
        existsQuery: "",
        createQuery: ""
    };
    var commit = $jqxGrid.jqxGrid('addrow', null, datarow);
}

function loadTableBDStructure() {
    _callServer({
        url: '/Configuration/DataBase/GetJsonStructure',
        data: { pType: _getViewVar("pType") },
        success: function (responseList) {
            loadTable(JSON.parse(responseList));
        }
    });

    function loadTable(webConfigList) {
        $.jqxGridApi.create({
            showTo: "#tblDBStructure",
            options: {
                //for comments or descriptions
                height: "700",
                autoheight: false,
                autorowheight: false,
                selectionmode: "singlerow",
                showfilterrow: true,
                sortable: true,
                editable: true,
                groupable: true
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                dataBinding: "Large Data Set Local",
                rows: webConfigList
            },
            groups: [],
            columns: [
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                { name: 'isSaved', text: 'Is Saved?', width: '5%', type: 'string', filtertype: 'checkedlist', editable: true },
                { name: 'numOrder', text: 'Create #', width: '5%', type: 'float', filtertype: 'checkedlist', editable: true },
                { name: 'delNumOrder', text: 'Delete #', width: '5%', type: 'float', filtertype: 'checkedlist', editable: true },
                { name: 'lockDelete', text: 'Lock Delete', width: '5%', type: 'string', filtertype: 'checkedlist', editable: true },
                { name: 'key', text: 'Key', width: '15%', type: 'string', filtertype: 'input', editable: true },
                { name: 'prefix', text: 'Prefix', width: '10%', type: 'string', filtertype: 'checkedlist', editable: true },
                { name: 'objectName', text: 'Name', width: '15%', type: 'string', filtertype: 'input', editable: true },
                { name: 'objectType', text: 'Type', width: '10%', type: 'string', filtertype: 'checkedlist', editable: true },
                { name: 'createQuery', text: 'Create Query', width: '10%', type: 'string', filtertype: 'input', editable: true },
                { name: 'deleteQuery', text: 'Delete Query', width: '10%', type: 'string', filtertype: 'input', editable: true },
                { name: 'existsQuery', text: 'Exists Query', width: '10%', type: 'string', filtertype: 'input', editable: true }

            ],
            ready: function () {

            }
        });

        $("#contentDBStructure").show();
    }
}

function compareByCreateNumOrder(a,b) {
    if (a.numOrder < b.numOrder) {
        return -1;
    }
    if (a.numOrder > b.numOrder) {
        return 1;
    }
    return 0;
}

function compareByDelNumOrder(a, b) {
    if (a.delNumOrder < b.delNumOrder) {
        return -1;
    }
    if (a.delNumOrder > b.delNumOrder) {
        return 1;
    }
    return 0;
}

function saveBDStructure() {
    var jqxRows = $.jqxGridApi.getAllRows("#tblDBStructure");
    var rowsToSave = [];

    //Reorder array by Delete orden number
    jqxRows.sort(compareByDelNumOrder);
    for (var i = 0; i < jqxRows.length; i++) {
        jqxRows[i].delNumOrder = (i + 1);
    }

    //Reorder array by Create orden number
    jqxRows.sort(compareByCreateNumOrder);

    for (var i = 0; i < jqxRows.length; i++) {
        rowsToSave.push({
            isSaved: jqxRows[i].isSaved,
            numOrder: (i + 1),
            delNumOrder: jqxRows[i].delNumOrder,
            lockDelete: jqxRows[i].lockDelete,
            key: jqxRows[i].key,
            prefix: jqxRows[i].prefix,
            objectName: jqxRows[i].objectName,
            objectType: jqxRows[i].objectType,
            //existsQuery: (function () {
            //    if (jqxRows[i].objectType == "Table" || jqxRows[i].objectType == "Procedure" || jqxRows[i].objectType == "Function") {
            //        return "ISNULL(OBJECT_ID('" + jqxRows[i].prefix + jqxRows[i].objectName + "'), 0)"
            //    }
            //    if (jqxRows[i].objectType == "Profile") {
            //        return "select count(1) from msdb.dbo.sysmail_profile sp where sp.name = '" + jqxRows[i].prefix + jqxRows[i].objectName + "'"
            //    }
            //})(),
            
            createQuery: jqxRows[i].createQuery,
            deleteQuery: jqxRows[i].deleteQuery,
            existsQuery: jqxRows[i].existsQuery
        });
    }

    _callServer({
        url: '/Configuration/DataBase/SaveJsonStructure',
        data: { 'pjson': _toJSON(rowsToSave), pType: _getViewVar("pType") },
        type: "post",
        success: function (savingStatus) {
            _showNotification("success", "Data was saved successfully.");
        }
    });
}