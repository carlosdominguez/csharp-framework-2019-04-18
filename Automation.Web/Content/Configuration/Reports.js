﻿/// <reference path="../Shared/plugins/util/global.js" />

$(document).ready(function () {
    _loadDBObjects(function () {
        loadReports();
    });
});

function loadReports() {
    var objListReports;

    //Create nestable List
    _loadReportsFromDB(function (reportsList) {
        objListReports = new _classNestableList({
            id: "#content-list-reports",
            entity: "Report",
            maxDepth: 1,
            buttons: {
                addNew: {}
            },
            onSave: function (newReports, action, objReport) {
                console.log(newReports, action, objReport);

                if (action == "Insert") {
                    addReport(objReport);
                }

                if (action == "Delete") {
                    deleteReport(objReport);
                }

                if (action == "Edit") {
                    editReport(objReport);
                }

                if (action == "Reorder") {
                    //Save all reports with new NumOrder
                    for (var i = 0; i < newReports.length; i++) {
                        editReport(newReports[i], false);
                    }

                    //Refresh Reports list when all is saved
                    _execOnAjaxComplete(function () {
                        //Update Menus in Framework
                        _refreshSession();

                        //Update List of reports
                        _loadReportsFromDB(function (reportsListDB) {
                            _showNotification("success", "Reports order was modified successfully", "ReorderReport");

                            objListReports.loadItemsList(reportsListDB);
                        });
                    });
                }
            },
            items: reportsList,

            //type: {string} {input-icon} {radio}
            columns: [
                { type: 'string', fieldKey: 'ID', showInList: false, showInForm: false },
                { type: 'string', fieldKey: 'Category', width: '200px', fieldPlaceHolder: 'eg. Main', valueTemplate: ':NumOrder - :Category', showInList: true, showInForm: true },
                { type: 'string', fieldKey: 'Name', width: '200px', fieldPlaceHolder: 'eg. EUC Home', valueTemplate: "<i class='fa fa-bar-chart'></i> _cropWord(:Name, 15)", showInList: true, showInForm: true },
                { type: 'string', fieldKey: 'Path', width: '350px', fieldPlaceHolder: 'eg. /EUCInventory/Pending P2P Payments', valueTemplate: "_cropWord(:Path, 45)", showInList: true, showInForm: true },
                { type: 'string', fieldKey: 'NumOrder', showInList: false, showInForm: false }
            ]
        });
    });

    function addReport(objReport) {
        var spName = _getDBObjectFullName("AdminReport");

        _callProcedure({
            loadingMsgType: "topBar",
            loadingMsg: "Saving new report '" + objReport.Name + "'...",
            name: "[dbo].[" + spName + "]",
            params: [
                { "Name": "@Action", "Value": "Insert" },
                { "Name": "@NumOrder", "Value": objReport.NumOrder },
                { "Name": "@Category", "Value": objReport.Category },
                { "Name": "@Name", "Value": objReport.Name },
                { "Name": "@Path", "Value": objReport.Path },
                { "Name": "@SessionSOEID", "Value": _getSOEID() }
            ],
            success: function () {
                _showNotification("success", "Report was saved successfully", "AddReport");

                //Update Menus in Framework
                _refreshSession();

                //Refresh Reports List from DB
                _loadReportsFromDB(function (reportsListDB) {
                    objListReports.loadItemsList(reportsListDB);
                });
            }
        });
    }

    function deleteReport(objReport) {
        var spName = _getDBObjectFullName("AdminReport");

        _callProcedure({
            loadingMsgType: "topBar",
            loadingMsg: "Deleting report '" + objReport.Name + "'...",
            name: "[dbo].[" + spName + "]",
            params: [
                { "Name": "@Action", "Value": "Delete" },
                { "Name": "@IDRow", "Value": objReport.ID },
                { "Name": "@SessionSOEID", "Value": _getSOEID() }
            ],
            success: function () {
                _showNotification("success", "Report was deleted successfully", "DeleteReport");

                //Update Menus in Framework
                _refreshSession();

                //Refresh Reports List from DB
                _loadReportsFromDB(function (reportsListDB) {
                    objListReports.loadItemsList(reportsListDB);
                });
            }
        });
    }
    
    function editReport(objReport, flagRefresh) {
        var spName = _getDBObjectFullName("AdminReport");

        _callProcedure({
            loadingMsgType: "topBar",
            loadingMsg: "Saving changes for report '" + objReport.Name + "'...",
            name: "[dbo].[" + spName + "]",
            params: [
                { "Name": "@Action", "Value": "Edit" },
                { "Name": "@IDRow", "Value": objReport.ID },
                { "Name": "@NumOrder", "Value": objReport.NumOrder },
                { "Name": "@Category", "Value": objReport.Category },
                { "Name": "@Name", "Value": objReport.Name },
                { "Name": "@Path", "Value": objReport.Path },
                { "Name": "@SessionSOEID", "Value": _getSOEID() }
            ],
            success: function () {
                if (typeof flagRefresh == 'undefined' || flagRefresh) {
                    _showNotification("success", "Report was modified successfully", "EditReport");

                    //Update Menus in Framework
                    _refreshSession();

                    //Refresh Reports List from DB
                    _loadReportsFromDB(function (reportsListDB) {
                        objListReports.loadItemsList(reportsListDB);
                    });
                }
            }
        });
    }
}