﻿
function renderComboBox(div, textDefault, data, pMultiple, arrSelected) {

    $(div).find('option').remove().end();

    var response = _GlobalVars[data];

    $.each(response, function (index, element) {
        $(div).append($('<option>', {
            value: element.ID,
            text: element.Value
        }));

    })

    $(div).multiselect({
        click: function (event, ui) {
            if (ui.checked) {
                _GlobalVars[arrSelected].push(ui.value);
            } else {
                _GlobalVars[arrSelected] = jQuery.grep(_GlobalVars[arrSelected], function (value) {
                    return value != ui.value;
                });
            }
        },
        selectedText: function (numChecked, numTotal, checkedItems) {
            return numChecked + ' of ' + numTotal + ' checked';
        },
        checkAll: function (e) {
            _GlobalVars[arrSelected] = []
            $.each(e.target.childNodes, function (index, value) {
                _GlobalVars[arrSelected].push(e.target.childNodes[index].value);
            });
                
        },
        uncheckAll: function (e) {
            _GlobalVars[arrSelected] = []
        }
    });


    $(div).multiselect({
        multiple: pMultiple,
        noneSelectedText: textDefault
    }).multiselectfilter();

    //$(div).multiselect().multiselectfilter();
    $(div).multiselect("refresh");
    $(div).css('width', '100px');

}


function BindJson(procedure, varName, listOfParams, onSuccess) {

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/EC/' + procedure,
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {

            _GlobalVars[varName] = jQuery.parseJSON(json);

            if (onSuccess) {
                onSuccess();
            }
        }
    });
}


function BindJsonAlias(procedure, varName,alias, listOfParams, onSuccess) {

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/' + alias + '/' + procedure,
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {

            _GlobalVars[varName] = jQuery.parseJSON(json);

            if (onSuccess) {
                onSuccess();
            }
        }
    });
}


function renderComboBoxSingle(div, textDefault, data, pMultiple, arrSelected) {

    $(div).find('option').remove().end();

    var response = _GlobalVars[data];

    $.each(response, function (index, element) {
        $(div).append($('<option>', {
            value: element.ID,
            text: element.Value
        }));

    })

    $(div).multiselect({
        click: function (event, ui) {
            if (ui.checked) {
                _GlobalVars[arrSelected] = ui.value;
            } else {
                _GlobalVars[arrSelected] = jQuery.grep(_GlobalVars[arrSelected], function (value) {
                    return value != ui.value;
                });
            }
            viewQMemoListData();
        },
        selectedText: function (numChecked, numTotal, checkedItems) {
            return numChecked + ' of ' + numTotal + ' checked';
        }
    });


    $(div).multiselect({
        multiple: pMultiple,
        noneSelectedText: textDefault
    }).multiselectfilter();

    //$(div).multiselect().multiselectfilter();
    $(div).multiselect("refresh");
    $(div).css('width', '100px');


}
