﻿var _GlobalVars = {
    Period: [],
    Bussiness: [],
    CompareTwo: [],
    CompareTwoDetail: [],
    arrCheckID: [],
    arrOperator: [],
    arrValidation: [],
    arrProduct: [],
    arrAccount: [],
    arrEditQuality: [],
    arrBU: [],
    arrBU2: [],
    Run: [],
    FilterEdidtQuality: []
};

var PeriodSelected;
var RunSelected;

var viewModel = {
    Period: ko.observable(),
    Bussiness: ko.observable(),
    CompareTwo: ko.observable(),
    CompareTwoDetails: ko.observableArray(),
    selectedModel: ko.observable(),
    B1: ko.observable(),
    B2: ko.observable(),
    TotalFind: ko.observable()
};


var arrCheckID = [];
var arrOperator = [];
var arrValidation = [];
var arrProduct = [];
var arrAccount = [];
var arrEditQuality = [];

function CompareTwoDetail(key, template, data) {
    this.key = key;
    this.template = ko.observable(template);
    this.data = data;
}


viewModel.periodSelect = ko.dependentObservable({
    read: viewModel.Period,
    write: function (Period) {
        PeriodSelected = Period;
        var listOfParams = {
            pPeriod: Period,
            pSOEID: _getSOEID()
        }
        BindJsonAlias("getBusiness", "Bussiness",'ComparisonEC', listOfParams, function () {
            $('#fieldsetFilter').hide();
            $('#fieldsetEditcheck').hide();
            $('#fieldsetBusiness').hide();

            $('#fieldsetBU').show();
            //viewModel.Bussiness(_GlobalVars["Bussiness"]);

            var listOfParams3 = {
                pID: 1,
                pPeriod: PeriodSelected
            }

            BindJsonAlias("procEC_GetFilterBU", "FilterBU", 'ComparisonEC', listOfParams3, function () {
                renderComboBox('#cbBU', 'FRS vs Genesis', "FilterBU", 1, "arrBU");
                renderComboBox('#cbBU2', 'RRI vs FRS', "FilterBU", 1, "arrBU2");

            });

            var listOfParams6 = {
                pID: 1,
                pPeriod: PeriodSelected,
                pType: "Product",
                pSOEID: _getSOEID()
            }

            BindJsonAlias("procEC_GetFilter", "FilterProduct", 'ComparisonEC', listOfParams6, function () {
                renderComboBox('#cbProduct', 'Select Product', "FilterProduct", 1, "arrProduct");
            });

            _GlobalVars["FilterEdidtQuality"][0] = { id: '0', Value: 'Edit Check' };
            _GlobalVars["FilterEdidtQuality"][1] = { id: '1', Value: 'Quality Check' };

            renderComboBox('#cbEditQuality', 'Select type', "FilterEdidtQuality", 1, "arrEditQuality");

        });
    },
    owner: viewModel
});


$(document).ready(function () {

    $('#fieldsetBU').hide();
    $('#fieldsetEditcheck').hide();
    $('#fieldsetFilter').hide();
    $('#fieldsetBusiness').hide();



    viewModel.CompareTwoDetailFilter = function (name) {
        return ko.utils.arrayFirst(this.CompareTwoDetails(), function (CompareTwoDetails) {
            return (CompareTwoDetails.name.replace(/\s+/, "").toLowerCase() === name.replace(/\s+/, "").toLowerCase());
        });
    };

    BindJsonAlias("getPeriod", "Period", 'ComparisonEC', "", function () {
        viewModel.Period = _GlobalVars["Period"];
        ko.applyBindings(viewModel);
    });


    $("#CablePopUp").on('shown.bs.modal', function () {
        ViewCable()
    });

    _hideMenu();
});

function nestedTable(tableName) {
    var $table = $(tableName);

    $table.find('.js-view-parents').on('click', function (e) {
        e.preventDefault();
        var $btn = $(e.target), $row = $btn.closest('tr'), $nextRow = $row.next('tr.expand-child');


        if ($nextRow.length) {
            $nextRow.toggle();
        } else {

            var IDS = $(this).context.id.replace('grid', '');
            var Grid = $(this).context.id.replace(',', '_');

            _GlobalVars[Grid] = [];

            var listOfParams = {
                pPeriod: PeriodSelected,
                pSOEID: _getSOEID(),
                pRunID: IDS
            }

            var newRow = '';


            BindJsonAlias("procEC_GetGridBusinessDetailCompareInside", Grid, 'ComparisonEC', listOfParams, function () {

                viewModel.CompareTwoDetails.push({ name: Grid, data: _GlobalVars[Grid] })
                newRow = '<tr class="expand-child" id="collapse' + Grid + '">' +
                          '<td colspan="12">' +
                          '<table id="testingLookAndFill' + Grid + '" class="display dataTable" style="width: 95%; ">' +
                            '<thead>' +
                            '<tr role="row">' +
                            '<th colspan="9" rowspan="1" style="background-color:#8B8B8B;color:white;">Left</th>' +
                            '<th colspan="9" rowspan="1" style="background-color:#8B8B8B;color:white;">Right</th>' +
                            '<tr role="row">' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 3%;">Sign</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Account</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Description</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Ice Code</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Prior</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">BU</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;"><span data-bind="text: B1" Left></span></th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;"><span data-bind="text: B2" Left></span></th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Variance</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Sign</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Account</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Description</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Ice Code</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Prior</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">BU</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;"><span data-bind="text: B1" Right></span></th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;"><span data-bind="text: B2" Right></span></th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Variance</th>' +
                            '</tr>' +
                            '</thead>' +
                            '<tbody data-bind="foreach: CompareTwoDetailFilter(&#39;' + Grid + '&#39;).data ">' +
                            '<tr role="row" class="odd">' +
                            '<td data-bind="text: LSing" style="font-size: 41px;"></td>' +
                            '<td data-bind="text: LAccount" style="background-color:white; font-size:X-SMALL;"></td>' +
                            '<td data-bind="text: LAccountDescr" style="overflow: auto;white-space:nowrap;max-width: 40px;"></td>' +
                            '<td data-bind="text: LIceCode" style="background-color:white; font-size:X-SMALL;"></td>' +
                            '<td data-bind="text: LPrior" style="background-color:white; font-size:X-SMALL;"></td>' +
                            '<td data-bind="text: BUL" style="background-color:white; font-size:X-SMALL;"></td>' +
                            '<td data-bind="text: LB1" style="background-color:white; font-size:X-SMALL;"></td>' +
                            '<td data-bind="text: LB2" style="background-color:white; font-size:X-SMALL;"></td>' +
                            '<td data-bind="text: LVariance" style="background-color:white; font-size:X-SMALL;"></td>' +
                            '<td data-bind="text: RSing" style="font-size: 41px;"></td>' +
                            '<td data-bind="text: RAccount" style="background-color:white; font-size:X-SMALL;"></td>' +
                            '<td data-bind="text: RAccountDescr" style="overflow: auto;white-space:nowrap;max-width: 40px;"></td>' +
                            '<td data-bind="text: RIceCode" style="background-color:white; font-size:X-SMALL;"></td>' +
                            '<td data-bind="text: RPrior" style="background-color:white; font-size:X-SMALL;"></td>' +
                            '<td data-bind="text: BUR" style="background-color:white; font-size:X-SMALL;"></td>' +
                            '<td data-bind="text: RB1" style="background-color:white; font-size:X-SMALL;"></td>' +
                            '<td data-bind="text: RB2" style="background-color:white; font-size:X-SMALL;"></td>' +
                            '<td data-bind="text: RVariance" style="background-color:white; font-size:X-SMALL;"></td>' +
                            '</tr>' +
                            '</tbody>' +
                            '</table>' +
                          '</table></td></tr>';
                $nextRow = $(newRow).insertAfter($row);
                /// $("#testingLookAndFill")[0] ASI FUNCIONA
                ko.applyBindings(viewModel, document.getElementById("testingLookAndFill" + Grid))


                //viewModel[Grid] = _GlobalVars[Grid];
            });


            // viewModel.CompareTwoIdo = "";


        }
    });


}

function ViewLastRun(ID) {

    RunSelected = ID;

    $('#fieldsetFilter').show();
    $('#fieldsetEditcheck').show();


    var listOfParams = {
        pPeriod: PeriodSelected,
        pSOEID: _getSOEID(),
        pRunID: ID
    }


    BindJsonAlias("procEC_GetGridBusinessDetailCompare", "CompareTwoDetail", 'ComparisonEC', listOfParams, function () {
        $('#BusinessTable').show();

        viewModel.CompareTwo(_GlobalVars["CompareTwoDetail"]);
        nestedTable('#EditCheckTwoSource');

        viewModel.TotalFind(_GlobalVars['CompareTwoDetail'].length);

    });

    var listOfParams0 = {
        pPeriod: PeriodSelected,
        pID: ID
    }

    BindJsonAlias("procEC_GetB1B2", "B1B2", 'ComparisonEC', listOfParams0, function () {

        viewModel.B1(_GlobalVars['B1B2'][0].SubBusinessName);
        viewModel.B2(_GlobalVars['B1B2'][1].SubBusinessName);

    });


    var listOfParams2 = {
        pID: ID,
        pPeriod: PeriodSelected,
        pType: "CheckID",
        pSOEID: _getSOEID()
    }

    BindJsonAlias("procEC_GetFilter", "FilterCheckID", 'ComparisonEC', listOfParams2, function () {
        renderComboBox('#cbCheckID', 'Select CheckID', "FilterCheckID", 1, "arrCheckID");
    });

    var listOfParams3 = {
        pID: ID,
        pPeriod: PeriodSelected,
        pType: "Account",
        pSOEID: _getSOEID()
    }

    BindJsonAlias("procEC_GetFilter", "FilterAccount", 'ComparisonEC', listOfParams3, function () {
        renderComboBox('#cbAccount', 'Select Account', "FilterAccount", 1, "arrAccount");
    });

    var listOfParams4 = {
        pID: ID,
        pPeriod: PeriodSelected,
        pType: "Operator",
        pSOEID: _getSOEID()
    }

    BindJsonAlias("procEC_GetFilter", "FilterOperator", 'ComparisonEC', listOfParams4, function () {
        renderComboBox('#cbOperator', 'Select Operator', "FilterOperator", 1, "arrOperator");
    });

    var listOfParams5 = {
        pID: ID,
        pPeriod: PeriodSelected,
        pType: "Validation",
        pSOEID: _getSOEID()
    }

    BindJsonAlias("procEC_GetFilter", "FilterValidation", 'ComparisonEC', listOfParams5, function () {
        renderComboBox('#cbValidation', 'Select Validation', "FilterValidation", 1, "arrValidation");
    });

   


}

function RunEditCheck() {

    $('#fieldsetFilter').show();
    $('#fieldsetEditcheck').show();


    var listOfParams = {
        pPeriod: PeriodSelected,
        pSOEID: _getSOEID(),
        pRunID: 1,
        pBU: _GlobalVars["arrBU"].toString(),
        pProduct: _GlobalVars["arrProduct"].toString(),
        pType: _GlobalVars["arrEditQuality"].toString(),
        pBU2: _GlobalVars["arrBU2"].toString()
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/EC/procEC_RunEditCheckMultiple',
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {

            var listOfParams2 = {
                pPeriod: PeriodSelected,
                pSOEID: _getSOEID()
            }
            BindJsonAlias("getBusiness", "Bussiness", 'ComparisonEC', listOfParams2, function () {
                $('#fieldsetFilter').hide();
                $('#fieldsetEditcheck').hide();
                $('#fieldsetBusiness').show();

                $('#fieldsetBU').show();
                viewModel.Bussiness(_GlobalVars["Bussiness"]);

                ViewLastRun(1);
            });

        }
    });





}


function Filter() {

    //need emac 6 vs 2015 +

    //var data = _GlobalVars["CompareTwoDetail"],
    //filterBy = { CheckID: _GlobalVars["arrCheckID"] },//, BALLOT_STATUS: ["PERM", "POLL"]
    //result = data.filter(o => Object.keys(filterBy).every(k => filterBy[k].some(f => o[k] === f)));

    //viewModel.CompareTwo(data);

    //arrCheckID: [],
    //arrOperator: [],
    //arrValidation: [],
    //arrAccount: []

    var listOfParams = {
        pPeriod: PeriodSelected,
        pSOEID: _getSOEID(),
        pRunID: RunSelected,
        pCHECKID: _GlobalVars["arrCheckID"].toString(),
        pOPERATOR: _GlobalVars["arrOperator"].toString(),
        pVALIDATION: _GlobalVars["arrValidation"].toString(),
        pProduct: _GlobalVars["arrProduct"].toString(),
        pACCOUNT: _GlobalVars["arrAccount"].toString()
    }


    BindJsonAlias("procEC_GetGridBusinessDetailCompareFilter", "CompareTwoDetail", 'ComparisonEC', listOfParams, function () {

        viewModel.CompareTwo(_GlobalVars["CompareTwoDetail"]);
        nestedTable('#EditCheckTwoSource');

        viewModel.TotalFind(_GlobalVars['CompareTwoDetail'].length);

    });


    //viewModel.CompareTwo(filterStore(_GlobalVars["CompareTwoDetail"], filter));


}

function Clear() {

    _GlobalVars["arrCheckID"] = [];
    _GlobalVars["arrOperator"] = [];
    _GlobalVars["arrValidation"] = [];
    _GlobalVars["arrAccount"] = [];
    _GlobalVars["arrProduct"] = [];

    ViewLastRun(RunSelected);

}

function Download() {


    var sql = 'procEC_GetGridBusinessDetailCompareExcel ';

    _downloadExcel({
        spName: sql,
        spParams: [
            { "Name": "@UserSoeId", "Value": _getSOEID() },
            { "Name": "@Period", "Value": PeriodSelected },
            { "Name": "@RunID", "Value": RunSelected },
            { "Name": "@CheckID", "Value": _GlobalVars["arrCheckID"].toString() },
	        { "Name": "@Operator", "Value": _GlobalVars["arrOperator"].toString() },
	        { "Name": "@Validation", "Value": _GlobalVars["arrValidation"].toString() },
	        { "Name": "@Account", "Value": _GlobalVars["arrAccount"].toString() }
        ],
        filename: "Edit Check Report",
        success: {
            msg: "Please wait, generating report..."
        }
    });


}

function ViewCable() {
    var QMemosCB;
    var BUCB;

    _GlobalVars["QMemosCB"] = _GlobalVars["FilterAccount"].filter(account => { if (account.Value.match(/QM.*/)) return account });

    var filterBy = _GlobalVars["arrBU"];



    _GlobalVars["QMemosBU"] = _GlobalVars["FilterBU"].filter(function (array_el) {
        return _GlobalVars["arrBU"].filter(function (anotherOne_el) {
            if (anotherOne_el == array_el.ID) return array_el;
        }).length == 0
    });


    renderComboBox('#cbSelBU', 'Select BU', "QMemosBU", 0, "QMemosBU");

    renderComboBox('#cbSelQM', 'Select QMemo', "QMemosCB", 0, "QMemosCB");


    //var data = _GlobalVars["CompareTwoDetail"],
    //filterBy = { CheckID: _GlobalVars["arrCheckID"] },//, BALLOT_STATUS: ["PERM", "POLL"]
    //result = data.filter(o => Object.keys(filterBy).every(k => filterBy[k].some(f => o[k] === f)));


}

function addCable() {


}