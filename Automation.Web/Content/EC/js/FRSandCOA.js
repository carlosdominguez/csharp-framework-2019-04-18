﻿var rowid;

$(document).ready(function () {
    _hideMenu();

    XLSX.createJsXlsx({
        idElement: "js-xlsx-plugin",
        fileInput: {
            dropText: "Drop an Excel file here...",
            hideDrop: false,
            inputText: "... or click here to select a file"
        },
        onSuccess: function (jsonData) {

            var data = jsonData["FRS AND COA Accounts"];

            var QMemos = data.filter(qmData => { if (qmData.ACCOUNT.match(/QM.*/)) return qmData });
            var IS = data.filter(qmData => { if (qmData.ACCOUNT.match(/IS.*/)) return qmData });
            var BS = data.filter(qmData => { if (qmData.ACCOUNT.match(/BS.*/)) return qmData });

            _callServer({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading data...",
                url: '/FRSandCOA/uploadReenFile',
                data: {
                    'data': JSON.stringify(QMemos)
                },
                type: "post",
                success: function (json) {
                    renderLog();
                }
            });
            _callServer({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading data...",
                url: '/FRSandCOA/uploadReenFile',
                data: {
                    'data': JSON.stringify(IS)
                },
                type: "post",
                success: function (json) {
                }
            });
            _callServer({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading data...",
                url: '/FRSandCOA/uploadReenFile',
                data: {
                    'data': JSON.stringify(BS)
                },
                type: "post",
                success: function (json) {
                }
            });
        }
    });


    renderLog();


});

function renderLog() {
    var listOfParams = {
        type: 'FRS and COA'
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/EC/getTableEC_Log',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {

            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                        { name: 'ID', type: 'string' },
                        { name: 'Name', type: 'string' },
                        { name: 'SOEID', type: 'string' },
                        { name: 'UploadDate', type: 'string' },
                        { name: 'YearUpload', type: 'string' },
                        { name: 'MonthUpload', type: 'string' },

                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            $('#jqxGridLastFile').jqxGrid(
           {
               source: dataAdapter,
               theme: 'blackberry',
               width: '100%',
               // autoheight: true,
               // autorowheight: true,
               pagesize: 10,
               pageable: true,
               filterable: true,
               showfilterrow: true,
               columnsresize: true,
               selectionmode: 'singlecell',
               enablebrowserselection: true,
               columns: [
                        { text: 'Name', dataField: 'Name', filtertype: 'input', editable: false },
                        { text: 'SOEID', dataField: 'SOEID', filtertype: 'input', editable: false },
                        { text: 'Upload Date', dataField: 'UploadDate', filtertype: 'input', editable: false },
                        { text: 'Year Upload', dataField: 'YearUpload', filtertype: 'input', editable: false },
                        { text: 'Month Upload', dataField: 'MonthUpload', filtertype: 'input', editable: false }
               ]

           });


        }
    });



}