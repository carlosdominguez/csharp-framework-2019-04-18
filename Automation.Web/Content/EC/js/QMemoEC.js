﻿var _GlobalVars = {
    Period: [],
    Bussiness: [],
    CompareTwo: [],
    CompareTwoDetail: [],
    arrCheckID: [],
    arrOperator: [],
    arrValidation: [],
    arrAccount: [],
    arrProduct: [],
    arrManagedSegment: [],
    arrBU: [],
    Run: [],
    cbSelBU: [],
    cbSelYear: [],
    cbSelMonth: [],
    cbSelQM: [],
    QMemoData: []
};

var PeriodSelected;
var RunSelected;

var viewModel = {
    Period: ko.observable(),
    Bussiness: ko.observable(),
    CompareTwo: ko.observable(),
    CompareTwoDetails: ko.observableArray(),
    selectedModel: ko.observable(),
    B1: ko.observable(),
    B2: ko.observable(),
    TotalFind: ko.observable(),
    QMemoData: ko.observableArray(),
    cbSelBU: ko.observableArray(),
    cbSelQM: ko.observableArray(),
};


var arrCheckID = [];
var arrOperator = [];
var arrValidation = [];
var arrAccount = [];
var cbProduct = [];
var cbManagedSegment = [];

var cbSelBU = [];
var cbSelYear = [];
var cbSelMonth = [];
var cbSelQM = [];

var SelectedQMFromRow = 0;

function CompareTwoDetail(key, template, data) {
    this.key = key;
    this.template = ko.observable(template);
    this.data = data;
}


viewModel.periodSelect = ko.dependentObservable({
    read: viewModel.Period,
    write: function (Period) {
        PeriodSelected = Period;
        var listOfParams = {
            pPeriod: Period,
            pSOEID: _getSOEID()
        }
        $('#fieldsetBusiness').show();

        BindJsonAlias("getBusiness", "Bussiness", "QMemoEC", listOfParams, function () {
            $('#fieldsetFilter').hide();
            $('#fieldsetEditcheck').hide();

            viewModel.Bussiness(_GlobalVars["Bussiness"]);
        });
    },
    owner: viewModel
});

viewModel.QMSelected = ko.dependentObservable({
    read: viewModel.Period,
    write: function (QMemo) {
        
        var listOfParams = {
            period: PeriodSelected,
            shortid: QMemo
        }


        BindJsonAlias("procEC_GetInfoQMemo", "QMemoData", "QMemoEC", listOfParams, function () {
            viewModel.QMemoData(_GlobalVars["QMemoData"]);
        });


    },
    owner: viewModel
});

viewModel.QMSelectedOnRowL = ko.dependentObservable({
    read: viewModel.Period,
    write: function (QMemo) {

        var QMemoSelected = _GlobalVars["FilterAccount"].filter(account => { if (account.Value.match(QMemo.LAccount)) return account });
        
    

        SelectedQMFromRow = QMemoSelected[0].ID;

    },
    owner: viewModel
});

viewModel.QMSelectedOnRowR = ko.dependentObservable({
    read: viewModel.Period,
    write: function (QMemo) {
        var QMemoSelected = _GlobalVars["FilterAccount"].filter(account => { if (account.Value.match(QMemo.RAccount)) return account });

        SelectedQMFromRow = QMemoSelected[0].ID;

    },
    owner: viewModel
});

$(document).ready(function () {

    $('#fieldsetBU').hide();
    $('#fieldsetEditcheck').hide();
    $('#fieldsetFilter').hide();
    $('#fieldsetBusiness').hide();

    viewModel.CompareTwoDetailFilter = function (name) {
        return ko.utils.arrayFirst(this.CompareTwoDetails(), function (CompareTwoDetails) {
            return (CompareTwoDetails.name.replace(/\s+/, "").toLowerCase() === name.replace(/\s+/, "").toLowerCase());
        });
    };

    BindJson("getPeriod", "Period", "", function () {
        viewModel.Period = _GlobalVars["Period"];
        ko.applyBindings(viewModel);
    });


    $("#CablePopUp").on('shown.bs.modal', function () {
        ViewCable(0)
    });

    _hideMenu();
});

function nestedTable(tableName) {
    var $table = $(tableName);

    $table.find('.js-view-parents').on('click', function (e) {
        e.preventDefault();
        var $btn = $(e.target), $row = $btn.closest('tr'), $nextRow = $row.next('tr.expand-child');


        if ($nextRow.length) {
            $nextRow.toggle();
        } else {

            var IDS = $(this).context.id.replace('grid', '');
            var Grid = $(this).context.id.replace(',', '_');

            _GlobalVars[Grid] = [];

            var listOfParams = {
                pPeriod: PeriodSelected,
                pSOEID: _getSOEID(),
                pRunID: IDS
            }

            var newRow = '';


            BindJsonAlias("procEC_GetGridBusinessDetailCompareInside", Grid, 'QMemoEC', listOfParams, function () {

                viewModel.CompareTwoDetails.push({ name: Grid, data: _GlobalVars[Grid] })
                newRow = '<tr class="expand-child" id="collapse' + Grid + '">' +
                          '<td colspan="12">' +
                          '<table id="testingLookAndFill' + Grid + '" class="display dataTable" style="width: 95%; ">' +
                            '<thead>' +
                            '<tr role="row">' +
                            '<th colspan="7" rowspan="1" style="background-color:#8B8B8B;color:white;">Left</th>' +
                            '<th colspan="7" rowspan="1" style="background-color:#8B8B8B;color:white;">Right</th>' +
                            '<tr role="row">' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 3%;">Sign</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Account</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Description</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Ice Code</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Prior</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">BU</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;"><span data-bind="text: B1" Left></span></th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Sign</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Account</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Description</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Ice Code</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Prior</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">BU</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;"><span data-bind="text: B1" Right></span></th>' +
                            '</tr>' +
                            '</thead>' +
                            '<tbody data-bind="foreach: CompareTwoDetailFilter(&#39;' + Grid + '&#39;).data ">' +
                            '<tr role="row" class="odd">' +
                            '<td data-bind="text: LSing" style="font-size: 41px;"></td>' +
                            '<td  style="background-color:white; font-size:X-SMALL;">' +  
                            '<span data-bind="text: LAccount"> </span>' + 
                            '<span><i class="fa fa-cloud-upload" style="font-size:x-large; color:#A1D044;margin-left: 5%; cursor:pointer;" data-toggle="modal" data-target="#CablePopUp" data-bind="click: $root.QMSelectedOnRowL, visible: LAccount.substring(0, 2) == &#34;QM&#34;  " " ></i></span> ' +
                            '</td>' +
                            '<td data-bind="text: LAccountDescr" style="overflow: auto;white-space:nowrap;max-width: 40px;"></td>' +
                            '<td data-bind="text: LIceCode" style="background-color:white; font-size:X-SMALL;"></td>' +
                            '<td data-bind="text: LPrior" style="background-color:white; font-size:X-SMALL;"></td>' +
                            '<td data-bind="text: BUL" style="background-color:white; font-size:X-SMALL;"></td>' +
                            '<td data-bind="text: LB1" style="background-color:white; font-size:X-SMALL;"></td>' +
                            '<td data-bind="text: RSing" style="font-size: 41px;"></td>' +
                            '<td  style="background-color:white; font-size:X-SMALL;">' +
                            '<span data-bind="text: RAccount"> </span>' +
                            '<span><i class="fa fa-cloud-upload" style="font-size:x-large; color:#A1D044;margin-left: 5%; cursor:pointer;" data-toggle="modal" data-target="#CablePopUp" data-bind="click: $root.QMSelectedOnRowR, visible: RAccount.substring(0, 2) == &#34;QM&#34;  " " ></i></span> ' +
                            '</td>' +
                            '<td data-bind="text: RAccountDescr" style="overflow: auto;white-space:nowrap;max-width: 40px;"></td>' +
                            '<td data-bind="text: RIceCode" style="background-color:white; font-size:X-SMALL;"></td>' +
                            '<td data-bind="text: RPrior" style="background-color:white; font-size:X-SMALL;"></td>' +
                            '<td data-bind="text: BUR" style="background-color:white; font-size:X-SMALL;"></td>' +
                            '<td data-bind="text: RB1" style="background-color:white; font-size:X-SMALL;"></td>' +
                            '</tr>' +
                            '</tbody>' +
                            '</table>' +
                          '</table></td></tr>';
                $nextRow = $(newRow).insertAfter($row);
                /// $("#testingLookAndFill")[0] ASI FUNCIONA
                ko.applyBindings(viewModel, document.getElementById("testingLookAndFill" + Grid))


                //viewModel[Grid] = _GlobalVars[Grid];
            });


            // viewModel.CompareTwoIdo = "";


        }
    });


}

function ViewLastRun(ID) {

    RunSelected = ID;

    $('#fieldsetFilter').show();
    $('#fieldsetEditcheck').show();


    var listOfParams = {
        pPeriod: PeriodSelected,
        pSOEID: _getSOEID(),
        pRunID: ID
    }


    BindJsonAlias("procEC_GetGridBusinessDetailCompare", "CompareTwoDetail",'QMemoEC', listOfParams, function () {
        $('#BusinessTable').show();

        viewModel.CompareTwo(_GlobalVars["CompareTwoDetail"]);
        nestedTable('#EditCheckTwoSource');

        viewModel.TotalFind(_GlobalVars['CompareTwoDetail'].length);

    });

    var listOfParams0 = {
        pPeriod: PeriodSelected,
        pID: ID
    }

    //BindJson("procEC_GetB1B2", "B1B2", listOfParams0, function () {

    //    viewModel.B1(_GlobalVars['B1B2'][0].SubBusinessName);
    //    viewModel.B2(_GlobalVars['B1B2'][1].SubBusinessName);

    //});


    var listOfParams2 = {
        pID: ID,
        pPeriod: PeriodSelected,
        pType: "CheckID",
        pSOEID: _getSOEID()
    }

    BindJson("procEC_GetFilter", "FilterCheckID", listOfParams2, function () {
        renderComboBox('#cbCheckID', 'Select CheckID', "FilterCheckID", 1, "arrCheckID");
    });

    var listOfParams3 = {
        pID: ID,
        pPeriod: PeriodSelected,
        pType: "Account",
        pSOEID: _getSOEID()
    }

    BindJson("procEC_GetFilter", "FilterAccount", listOfParams3, function () {
        renderComboBox('#cbAccount', 'Select Account', "FilterAccount", 1, "arrAccount");
    });

    var listOfParams4 = {
        pID: ID,
        pPeriod: PeriodSelected,
        pType: "Operator",
        pSOEID: _getSOEID()
    }

    BindJson("procEC_GetFilter", "FilterOperator", listOfParams4, function () {
        renderComboBox('#cbOperator', 'Select Operator', "FilterOperator", 1, "arrOperator");
    });

    var listOfParams5 = {
        pID: ID,
        pPeriod: PeriodSelected,
        pType: "Validation",
        pSOEID: _getSOEID()
    }

    BindJson("procEC_GetFilter", "FilterValidation", listOfParams5, function () {
        renderComboBox('#cbValidation', 'Select Validation', "FilterValidation", 1, "arrValidation");
    });



    var listOfParams6 = {
        pID: ID,
        pPeriod: PeriodSelected,
        pType: "Product2",
        pSOEID: _getSOEID()
    }

    BindJson("procEC_GetFilter", "FilterProduct", listOfParams6, function () {
        renderComboBox('#cbProduct', 'Select Product', "FilterProduct", 1, "arrProduct");
    });

    var listOfParams7 = {
        pID: ID,
        pPeriod: PeriodSelected,
        pType: "ManagedSegment",
        pSOEID: _getSOEID()
    }

    BindJson("procEC_GetFilter", "FilterManagedSegment", listOfParams7, function () {
        renderComboBox('#cbManagedSegment', 'Select MS', "FilterManagedSegment", 1, "arrManagedSegment");
    });


}

function RunEditCheck(ID) {

    $('#fieldsetFilter').show();
    $('#fieldsetEditcheck').show();


    var listOfParams = {
        pPeriod: PeriodSelected,
        pSOEID: _getSOEID(),
        pID: ID
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/QMemoEC/procEC_RunEditCheck',
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {
            _GlobalVars["Bussiness"] = jQuery.parseJSON(json);

            viewModel.Bussiness(_GlobalVars["Bussiness"]);

            ViewLastRun(ID);
            }

        });

}

function Filter() {

    //need emac 6 vs 2015 +

    //var data = _GlobalVars["CompareTwoDetail"],
    //filterBy = { CheckID: _GlobalVars["arrCheckID"] },//, BALLOT_STATUS: ["PERM", "POLL"]
    //result = data.filter(o => Object.keys(filterBy).every(k => filterBy[k].some(f => o[k] === f)));

    //viewModel.CompareTwo(data);

    //arrCheckID: [],
    //arrOperator: [],
    //arrValidation: [],
    //arrAccount: []

    var listOfParams = {
        pPeriod: PeriodSelected,
        pSOEID: _getSOEID(),
        pRunID: RunSelected,
        pCHECKID: _GlobalVars["arrCheckID"].toString(),
        pOPERATOR: _GlobalVars["arrOperator"].toString(),
        pVALIDATION: _GlobalVars["arrValidation"].toString(),
        pACCOUNT: _GlobalVars["arrAccount"].toString(),
        pMS: _GlobalVars["arrManagedSegment"].toString(),
        pProduct: _GlobalVars["arrProduct"].toString()
    }


    BindJsonAlias("procEC_GetGridBusinessDetailCompareFilter", "CompareTwoDetail","QMemoEC", listOfParams, function () {

        viewModel.CompareTwo(_GlobalVars["CompareTwoDetail"]);
        nestedTable('#EditCheckTwoSource');

        viewModel.TotalFind(_GlobalVars['CompareTwoDetail'].length);

    });


    //viewModel.CompareTwo(filterStore(_GlobalVars["CompareTwoDetail"], filter));


}

function Clear() {

    _GlobalVars["arrCheckID"] = [];
    _GlobalVars["arrOperator"] = [];
    _GlobalVars["arrValidation"] = [];
    _GlobalVars["arrAccount"] = [];

    ViewLastRun(RunSelected);

}

function Download() {

    var sql = 'procEC_GetGridBusinessDetailExcel ';

    _downloadExcel({
        spName: sql,
        spParams: [
            { "Name": "@UserSoeId", "Value": _getSOEID() },
            { "Name": "@Period", "Value": PeriodSelected },
            { "Name": "@RunID", "Value": RunSelected },
            { "Name": "@CheckID", "Value": _GlobalVars["arrCheckID"].toString() },
	        { "Name": "@Operator", "Value": _GlobalVars["arrOperator"].toString() },
	        { "Name": "@Validation", "Value": _GlobalVars["arrValidation"].toString() },
	        { "Name": "@Account", "Value": _GlobalVars["arrAccount"].toString() },
            { "Name": "@Product", "Value": "" },
            { "Name": "@ManagedSegment", "Value": "" }
        ],
        filename: "Edit Check Report",
        success: {
            msg: "Please wait, generating report..."
        }
    });


}

function ViewCable() {

    _GlobalVars["cbSelQM"] = _GlobalVars["FilterAccount"].filter(account => { if (account.Value.match(/QM.*/)) return account });

    viewModel.cbSelQM(_GlobalVars["cbSelQM"]);
    var listOfParams7 = {
        pName: _GlobalVars["Bussiness"][0].Name
    }

    BindJsonAlias("procEC_GetBusinessFilter", "cbSelBU", "QMemoEC", listOfParams7, function () {
        viewModel.cbSelBU(_GlobalVars["cbSelBU"]);
    });



    $('#PeriodText').val(PeriodSelected);


    if(SelectedQMFromRow > 0 )
    {

        viewModel.QMSelected(SelectedQMFromRow);

        $('#cbSelQM').val(SelectedQMFromRow);

        //var listOfParams = {
        //    period: PeriodSelected,
        //    shortid: SelectedQMFromRow
        //}


        //BindJsonAlias("procEC_GetInfoQMemo", "QMemoData", "QMemoEC", listOfParams, function () {
        //    viewModel.QMemoData(_GlobalVars["QMemoData"]);



        //});
    }

}

function addCable() {
    var listOfParams = {
         period : PeriodSelected,
         bu : '11273',
         account: $('#cbSelQM').val(),
         balance: $('#NewBalanceText').val(),
         business: $('#cbSelBU').val(),
         sOEID_UPLOADER  : _getSOEID(),
         fILE_PATH: 'ManualUpload'
    }


    BindJsonAlias("procEC_UploadManualTemplateAdjustment", "QMRun", "QMemoEC", listOfParams, function () {

        RunEditCheck(RunSelected);
        Filter();
        viewModel.QMSelected($('#cbSelQM').val());
    });

}

function viewAll()
{
    var listOfParams = {
        pPeriod: PeriodSelected,
        pSOEID: _getSOEID()
    }
    $('#fieldsetBusiness').show();

    BindJsonAlias("getBusiness", "Bussiness", "QMemoEC", listOfParams, function () {
        $('#fieldsetFilter').hide();
        $('#fieldsetEditcheck').hide();

        viewModel.Bussiness(_GlobalVars["Bussiness"]);
    });

}





