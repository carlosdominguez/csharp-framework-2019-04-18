﻿var rowid;

$(document).ready(function () {
    _hideMenu();

    XLSX.createJsXlsx({
        idElement: "js-xlsx-plugin",
        fileInput: {
            dropText: "Drop an Excel file here...",
            hideDrop: false,
            inputText: "... or click here to select a file"
        },
        onSuccess: function (jsonData) {

            var excelSource = jsonData;

            _callServer({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading data...",
                url: '/FRSvsGEN/uploadReenFile',
                data: {
                    'data': JSON.stringify(excelSource["FRS vs GN Report"])
                },
                type: "post",
                success: function (json) {
                    renderLog();
                }
            });
        }
    });

    renderLog();
    

});

function renderLog() {
    var listOfParams = {
        type: 'FRSSvsGEN'
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/EC/getTableEC_Log',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {

            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                        { name: 'ID', type: 'string' },
                        { name: 'Name', type: 'string' },
                        { name: 'SOEID', type: 'string' },
                        { name: 'UploadDate', type: 'string' },
                        { name: 'YearUpload', type: 'string' },
                        { name: 'MonthUpload', type: 'string' },

                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            $('#jqxGridLastFile').jqxGrid(
           {
               source: dataAdapter,
               theme: 'blackberry',
               width: '100%',
               // autoheight: true,
               // autorowheight: true,
               pagesize: 10,
               pageable: true,
               filterable: true,
               showfilterrow: true,
               columnsresize: true,
               selectionmode: 'singlecell',
               enablebrowserselection: true,
               columns: [
                        { text: 'Name', dataField: 'Name', filtertype: 'input', editable: false },
                        { text: 'SOEID', dataField: 'SOEID', filtertype: 'input', editable: false },
                        { text: 'Upload Date', dataField: 'UploadDate', filtertype: 'input', editable: false },
                        { text: 'Year Upload', dataField: 'YearUpload', filtertype: 'input', editable: false },
                        { text: 'Month Upload', dataField: 'MonthUpload', filtertype: 'input', editable: false }
               ]

           });


        }
    });



}