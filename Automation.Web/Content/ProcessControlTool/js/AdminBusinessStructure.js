/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="ProcessControlTool.js" />
var modalsWidth = "85%";
var teamList = null;

$(document).ready(function () {

    //Hide Menu
    _hideMenu();

    //On Click "New"
    $(".btnNewTeam").click(function () {
        openModalManageTeam("New");
    });

    //On Click "Edit"
    $(".btnEditTeam").click(function () {
        openModalManageTeam("Edit");
    });

    //On Click "Delete"
    $(".btnDeleteTeam").click(function () {
        deleteTeam();
    });

    //On click Checklist Manager
    $(".btnChecklistManager").click(function () {
        openModalManageChecklist();
    });

    //When all ajax are completed (_loadTimeZone();, _updateCurrentUserDLs();)
    _execOnAjaxComplete(function () {
        //Load Groups
        loadSelectOfMSGroup({
            onSuccess: function () {
                //Load table with all teams of selected group
                loadTeamsTable();

                //Hide Full Page Loading
                _hideLoadingFullPage({
                    id: "ActivityPageLoading"
                });
            }
        });
    });
});

function loadTeamsTable() {
    var idTable = "#tblTeams";
    //var teamList = getTeamList(idMSGroup);

    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Business Structure...",
        name: "[dbo].[spPCTManageTeam]",
        params: [
            { Name: "@Action", Value: "List Teams Tree" },
            { Name: "@IDMSGroup", Value: getObjMSGroup().IDMSGroup }
        ],
        success: function (responseTeamList) {
            teamList = responseTeamList;

            //Create table
            $.jqxGridApi.create({
                showTo: idTable,
                options: {
                    //for comments or descriptions
                    height: (($(window).height() < 700) ? "480" : "700"),
                    autoheight: false,
                    autorowheight: false,
                    showfilterrow: false,
                    sortable: false,
                    editable: true,
                    selectionmode: "singlerow"
                },
                source: {
                    // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                    dataBinding: "Large Data Set Local",
                    rows: teamList
                },
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                columns: [
                    { name: 'ID', type: 'string', hidden: true },
                    { name: 'TotalChilds', type: 'number', hidden: true },
                    { name: 'IDParentTeam', type: 'string', hidden: true },
                    { name: 'GOC', type: 'string', hidden: true },
                    { name: 'Level', type: 'number', hidden: true },
                    { name: 'IDLabel', type: 'number', hidden: true },
                    { name: 'LabelName', type: 'string', hidden: true },
                    { name: 'DLEmail', type: 'string', hidden: true },
                    { name: 'Tree', type: 'string', hidden: true },
                    { name: 'JsonContacts', type: 'string', hidden: 'true' },
                    {
                        name: 'Name', text: 'Name', width: '245px', pinned: true, type: 'html', filtertype: 'input',
                        cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                            var paddingXLevel = 35;
                            var padding = paddingXLevel * (rowData.Level - 1);
                            var badgeColorToUse = "badge-info";
                            var badgeColors = {
                                1: "badge-info",
                                2: "badge-purple",
                                3: "badge-accent",
                                4: "badge-primary"
                            };

                            if (padding == 0) {
                                padding = 2;
                            }

                            if (badgeColors[rowData.Level]) {
                                badgeColorToUse = badgeColors[rowData.Level];
                            }

                            var labelHtml = '<span class="badge ' + badgeColorToUse + '">' + rowData.LabelName + '</span>';
                            var resultHTML =
                                ' <span style="margin-left: ' + padding + 'px;line-height: 28px;" > ' +
                                    labelHtml + ' ' + value + ' (' + rowData.TotalChilds + ') ' +
                                '</span>';

                            return resultHTML;
                        }
                    },
                    { name: 'GOCDesc', text: 'GOC', width: '300px', type: 'string' },
                    { name: 'TotalActivities', text: 'Activities', width: '70px', type: 'string', cellsalign: 'center', align: 'center' },
                    {
                        name: 'CountContacts', text: 'Admins', width: '100px', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center',
                        cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                            var htmlResult = '<span style="line-height:31px;margin-left:25px;">' + rowData.CountContacts + '</span>';

                            //Add s
                            if (rowData.JsonContacts) {
                                var rows = JSON.parse(rowData.JsonContacts);
                                if (rows.length > 0) {
                                    var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                                    for (var i = 0; i < rows.length; i++) {
                                        var tempData = rows[i];
                                        infoHtml += " \n " + tempData.Type + ": " + tempData.Description + "<br>";
                                    }

                                    infoHtml += "</div>";

                                    htmlResult +=
                                        '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="User Admins Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                        '       <img style="margin-right: 25px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png"/> ' +
                                        '</div>';
                                }
                            }

                            return htmlResult;
                        }
                    },
                    //{ name: 'CFieldCount', text: 'Fields', width: '70px', type: 'string' },
                    { name: 'SharedFolderPath', text: 'Shared Folder', width: '850px', type: 'string' },
                    { name: 'FolderStructure', text: 'Folder Structure', width: '700px', type: 'string' },
                    { name: 'DLDisplayName', text: 'DL for Emails', width: '300px', type: 'string' }
                ],
                ready: function () {
                    //Add popup of information
                    $(idTable).on("rowclick", function (event) {
                        _GLOBAL_SETTINGS.tooltipsPopovers();
                    });
                }
            });
        }
    });
}

function getObjMSGroup() {
    var result = {
        IDMSGroup: 0,
        IDManagedSegment: 0
    };
    var $selOption = $("#selMSGroup").find("option:selected");
    if ($selOption.length > 0) {
        result = {
            IDMSGroup: $selOption.attr("value"),
            IDManagedSegment: $selOption.attr("managedSegment")
        }
    }

    return result;
}

function loadSelectOfMSGroup(customOptions) {
    //Default Options.
    var options = {
        onSuccess: null
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    var sql =
        " SELECT " +
        "     G.[ID], " +
        "     G.[Name], " +
        "     G.[IDManagedSegment] " +
        " FROM " +
		"     [dbo].[tblPCTMSGroupXAdminUser] GU " +
        "         INNER JOIN [dbo].[tblPCTMSGroup] G ON GU.[IDMSGroup] = G.[ID] " +
        " WHERE " +
		"     [IsDeleted] = 0 AND [SOEID] = '" + _getSOEID() + "' ";

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Business Group...",
        url: '/Shared/ExecQuery',
        data: { 'pjsonSql': _toJSON(sql) },
        type: "post",
        success: function (resultList) {
            //Remove current options
            $("#selMSGroup").contents().remove();

            //Add defaul option
            $("#selMSGroup").append($('<option>', {
                value: 0,
                text: "-- Select --"
            }));

            //Add Options
            for (var i = 0; i < resultList.length; i++) {
                var objTemp = resultList[i];
                $("#selMSGroup").append($('<option>', {
                    value: objTemp.ID,
                    text: objTemp.Name,
                    managedSegment: objTemp.IDManagedSegment,
                    selected: (i == 0)
                }));
            }

            //When not exists group assigned 
            if (resultList.length == 0) {
                
                //Hide Select of Group
                $(".selGroupContainer").hide();

                //Hide tab container
                $("#tab-team-list").hide();

                //Show Message to user
                _showAlert({
                    showTo: $("#tab-team-list").parent(),
                    type: 'error',
                    title: "Message",
                    content: "You don't have any business group assigned please contact the system administrator."
                });
            }

            //When exists only 1 group assigned
            if (resultList.length == 1) {

                //Hide Select of Group
                $(".selGroupContainer").hide();
            }

            //When exists more than 1 group assigned
            if (resultList.length > 1) {

                //On change load new group
                $("#selMSGroup").change(function () {
                    loadTeamsTable();
                });

            }

            //Trigger On Success
            if (options.onSuccess) {
                options.onSuccess();
            }
        }
    });
}

function openModalManageTeam(typeAction) {
    //typeAction: "New" || "Edit"

    //Create unique ID form Current Modal
    var idModalTeam = _createCustomID();
    var idTableAdminUsers = "#tblAdminUsers_" + idModalTeam;
    var idSelAdminUsers = "#selAdminUsers_" + idModalTeam;

    var objTeam = {
        Action: 'New',
        ID: 0,
        IDParentTeam: 0,
        IDLabel: 0,
        Name: "",
        Level: 1,
        DLDisplayName: "",
        DLEmail: "",
        DLSummary: "",
        GOC: "",
        GOCDesc: "",
        SharedFolderPath: "",
        FolderStructure: ""
    };

    if (typeAction == "Edit") {
        objTeam = $.jqxGridApi.getOneSelectedRow("#tblTeams", true);
    }

    if (typeAction == "New") {
        var objParentTeam = $.jqxGridApi.getOneSelectedRow("#tblTeams", false);
        if (objParentTeam) {
            objTeam.Level = objParentTeam.Level + 1;
            objTeam.IDParentTeam = objParentTeam.ID;
            objTeam.DLDisplayName = objParentTeam.DLDisplayName;
            objTeam.DLEmail = objParentTeam.DLEmail;
            objTeam.GOC = objParentTeam.GOC;
            objTeam.GOCDesc = objParentTeam.GOCDesc;
            objTeam.SharedFolderPath = objParentTeam.SharedFolderPath;
            objTeam.FolderStructure = objParentTeam.FolderStructure;
        }
    }

    //Only if exists objTeam
    if (objTeam) {

        //Set DL Summary  *CSS FRSS CR Re-engineering Team <dl.fro.cr.are.team@imcla.lac.nsroot.net>
        if (objTeam.DLDisplayName && objTeam.DLEmail) {
            objTeam.DLSummary = objTeam.DLDisplayName + " &lt;" + objTeam.DLEmail + "&gt;";
        }

        //Set IDMSGroup
        objTeam.IDMSGroup = getObjMSGroup().IDMSGroup;

        //Show Form of Team
        _showModal({
            width: modalsWidth,
            modalId: "TeamAdmin",
            addCloseButton: true,
            buttons: [{
                name: "<i class='fa fa-save'></i> Save",
                class: "btn-success",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    formOnSave($modal);
                }
            }],
            title: "Information",
            onReady: function ($modal) {
                formOnReady($modal);
            },
            contentHtml: formGetHtml()
        });
    }

    function formGetHtml() {
        //Create HTML
        var htmlForm =
            '<ul class="nav nav-tabs transparent left-aligned"> ' +
            '    <li class="active"> ' +
            '        <a href="#tab-team-info" data-toggle="tab"> ' +
            '            <i class="fa fa-table"></i> <span class="lblTeamInfo">Form</span> ' +
            '        </a> ' +
            '    </li> ' +

            '    <li> ' +
            '        <a href="#tab-admin-users" data-toggle="tab"> ' +
            '            <i class="fa fa-table"></i> <span class="lblAdminUsers">Admin Users</span> ' +
            '        </a> ' +
            '    </li> ' +

            '    <li style="display:none;"> ' +
            '        <a href="#tab-fields" data-toggle="tab"> ' +
            '            <i class="fa fa-table"></i> <span class="lblFields">Custom Fields</span> ' +
            '        </a> ' +
            '    </li> ' +

            '</ul> ' +

            '<div class="tab-content"> ' +

                //Start: Team Information (Tab Content)
            '    <div class="tab-pane fade in active" id="tab-team-info"> ' +
            '       <form class="form-horizontal formTeam"> ' +
            '          <div class="row"> ' +
            '              <div class="col-md-6"> ' +

                              //Select Label
            '                  <div class="form-group"> ' +
            '                      <label class="col-md-4 control-label"> ' +
            '                          <i class="fa fa-angle-double-right"></i> Label ' +
            '                      </label> ' +
            '                      <div class="col-md-6"> ' +
            '                          <select name="selLabel" class="form-control selLabel"></select> ' +
            '                      </div> ' +
            '                      <div class="col-md-2"> ' +
            '                          <button type="button" title="Manage Labels" class="btn btn-default btn-sm btnManageLabel"> ' +
            '                              <i class="fa fa-pencil-square-o"></i> ' +
            '                          </button> ' +
            '                      </div> ' +
            '                  </div> ' +

                               //Name
            '                  <div class="form-group"> ' +
            '                      <label class="col-md-4 control-label"> ' +
            '                          <i class="fa fa-angle-double-right"></i> Name ' +
            '                      </label> ' +
            '                      <div class="col-md-6"> ' +
            '                          <div class="form-control txtName" name="txtName" placeholder="Add Name..." contenteditable="true" >' + objTeam.Name + '</div> ' +
            '                      </div> ' +
            '                  </div> ' +

            '              </div> ' +
            '              <div class="col-md-6"> ' +

                              //Parent Team
            '                  <div class="form-group"> ' +
            '                      <label class="col-md-4 control-label"> ' +
            '                          <i class="fa fa-angle-double-right"></i> Parent ' +
            '                      </label> ' +
            '                      <div class="col-md-6"> ' +
            '                          <select name="selParentTeam" class="form-control selParentTeam"></select> ' +
            '                      </div> ' +
            '                      <div class="col-md-2"></div> ' +
            '                  </div> ' +

                               //Level
            '                  <div class="form-group"> ' +
            '                      <label class="col-md-4 control-label"> ' +
            '                          <i class="fa fa-angle-double-right"></i> Level ' +
            '                      </label> ' +
            '                      <div class="col-md-6"> ' +
            '                          <label class="form-control txtLevel">' + objTeam.Level + '</label> ' +
            '                      </div> ' +
            '                      <div class="col-md-2"></div> ' +
            '                  </div> ' +

            '              </div> ' +

            '              <div class="col-md-12"> ' +

                               //GOC
            '                   <div class="selGOC" id="selGOC_' + idModalTeam + '"></div> ' +

                               //DL for Emails
            '                   <div class="txtDLForSupport" id="txtDLForSupport_' + idModalTeam + '"></div> ' +

                               //Shared Folder
            '                   <div class="form-group"> ' +
            '                       <label class="col-md-2 control-label"> ' +
            '                           <i class="fa fa-angle-double-right"></i> Shared Folder ' +
            '                       </label> ' +
            '                       <div class="col-md-9"> ' +
            '                           <div class="form-control txtSharedFolderPath" name="txtSharedFolderPath" placeholder="e. g. \\\\sjovnasfro0003\\froqt0029\\ICG_PCU_LATAM_3" contenteditable="true" autocomplete="off" >' + objTeam.SharedFolderPath + '</div> ' +
            '                       </div> ' +
            '                      <div class="col-md-1"> ' +
            '                          <button type="button" title="Check Tool Access" class="btn btn-default btn-sm btnCheckAccess"> ' +
            '                              <i class="fa fa-unlock"></i> ' +
            '                          </button> ' +
            '                      </div> ' +
            '                   </div> ' +

                               //Folder Structure
            '                   <div class="form-group"> ' +
            '                       <label class="col-md-2 control-label"> ' +
            '                           <i class="fa fa-angle-double-right"></i> Folder Structure ' +
            '                       </label> ' +
            '                       <div class="col-md-9"> ' +
            '                           <div class="form-control txtFolderStructure" name="txtFolderStructure" placeholder="e. g. \\[YYYY]\\[MM]\\{ActivityName}\\{FileName}.{FileExt}" contenteditable="true" autocomplete="off" >' + objTeam.FolderStructure + '</div> ' +
            '                       </div> ' +
            '                   </div> ' +

            '              </div> ' +

            '              <div class="col-md-6"> ' +
            '              </div> ' +
            '              <div class="col-md-6"> ' +

                                //Copy on Childs
            '                  <div class="form-group"> ' +
            '                      <label class="col-md-4 control-label"> ' +
            '                          <i class="fa fa-angle-double-right"></i> Copy recursive' +
            '                      </label> ' +
            '                      <div class="col-md-6"> ' +
            '                           <div class="form-control"> ' +
            '                               <ul class="list-unstyled"> ' +
            '                                   <li class="col-md-6"> ' +
            '                                       <input type="radio" value="1" name="chkCopyRecursive_' + idModalTeam + '" class="skin-flat-green" > ' +
            '                                       <label class="icheck-label form-label">Yes</label> ' +
            '                                   </li> ' +
            '                                   <li class="col-md-6"> ' +
            '                                       <input type="radio" value="0" name="chkCopyRecursive_' + idModalTeam + '" class="skin-flat-green" checked> ' +
            '                                       <label class="icheck-label form-label">No</label> ' +
            '                                   </li> ' +
            '                                   <div style="clear:both;"></div> ' +
            '                               </ul> ' +
            '                           </div> ' +
            '                      </div> ' +
            '                  </div> ' +
            '              </div> ' +
            '          </div> ' +
            '       </form> ' +
            '   </div> ' +
                //End: Team Information (Tab Content)

                //Start: User Admins (Tab Content)
            '    <div class="tab-pane fade in" id="tab-admin-users"> ' +
            '        <div class="tab-users-container"> ' +

            //'           <form class="form-horizontal formAdminUsers"> ' +
            '              <div class="row"> ' +
            '                  <div class="col-md-12"> ' +
                                  //Select User by SOEID
            '                      <div class="form-group"> ' +
            '                          <label class="col-md-2 control-label"> ' +
            '                              <i class="fa fa-angle-double-right"></i> Add User ' +
            '                          </label> ' +
            '                          <div class="col-md-9"> ' +
            '                              <div class="selAdminUsers" id="selAdminUsers_' + idModalTeam + '"></div> ' +
            '                          </div> ' +
            '                          <div class="col-md-1"></div> ' +
            '                      </div> ' +

                                    //DL of Users
            '                       <div class="txtAdminUsersDL" id="txtAdminUsersDL_' + idModalTeam + '"></div> ' +
            '                 </div> ' +
            '              </div> ' +
            //'           </form> ' +

            '            <div style="width: 30%;float: right;"> ' +
            '                <button type="button" class="btnDeleteContact btn btn-danger  top15 left15 bottom15 pull-right"><i class="fa fa-trash-o"></i> Delete row</button> ' +
            '                <button type="button" class="btnNewContact btn btn-success top15 left15 bottom15 pull-right"><i class="fa fa-plus-square"></i> Add</button> ' +
            '            </div> ' +

            '        </div> ' +

            '        <div class="clearfix tblAdminUsers" id="tblAdminUsers_' + idModalTeam + '"></div> ' +
            '    </div> ' +
                //End: User Admins (Tab Content)

            '</div> ';

        return htmlForm;
    }

    function formOnSave($formContainer) {
        //Validate fields
        if ($formContainer.find(".formTeam").valid()) {
            //@Action Check if we need update
            if (objTeam.ID > 0) {
                objTeam.Action = 'Edit';
            }

            //@IDParentTeam
            objTeam.IDParentTeam = $formContainer.find(".selParentTeam").val();

            //@IDLabel
            objTeam.IDLabel = $formContainer.find(".selLabel").val();

            //@Level
            objTeam.Level = $formContainer.find(".txtLevel").text();

            //@Name
            objTeam.Name = _cleanString($formContainer.find(".txtName").text(), true);

            //Team name cannot contains " - ", by split by objActivity.TeamTree.split(" - ") on parse folder structure
            objTeam.Name = _replaceAll(" - ", " ", objTeam.Name);

            //@SharedFolderPath
            objTeam.SharedFolderPath = $formContainer.find(".txtSharedFolderPath").text();

            //@FolderStructure
            objTeam.FolderStructure = $formContainer.find(".txtFolderStructure").text();

            //@GOC
            var GOCSummary = $formContainer.find(".txtGOC").val();
            var GOCPart = "";
            var dataSplit = GOCSummary.split(" [");
            if(dataSplit.length > 1){
                GOCPart = dataSplit[1].replace("]", "");
            }else{
                if(dataSplit.length == 1){
                    GOCPart = GOCSummary;
                }
            }
            objTeam.GOC = GOCPart;

            //@StrListUsers
            var userRows = $(idTableAdminUsers).jqxGrid("getRows");

            //Only save level 1 rows
            rootContactRows = _findAllObjByProperty(userRows, "Level", "1");
            objTeam.StrListUsers = _generateString(rootContactRows, ["Type", "ID"], "~", "|");

            //DL for Emails
            var emailDL = _getEmailFromDLSummary($formContainer.find(".txtDLForSupport .inputDL").text());

            //Look for ID DL
            _getAutomationIDDL({
                emailDL: emailDL,
                onSuccess: function (idDL) {
                    //@IDDL
                    objTeam.IDDL = idDL

                    //@CopyRecursive 
                    objTeam.CopyRecursive = $("[name='chkCopyRecursive_" + idModalTeam + "']:checked").val();

                    //Save data in DB
                    _callProcedure({
                        response: true,
                        name: "[dbo].[spPCTManageTeam]",
                        params: [
                            { "Name": "@Action", "Value": objTeam.Action },
                            { "Name": "@IDMSGroup", "Value": objTeam.IDMSGroup },
                            { "Name": "@IDTeam", "Value": objTeam.ID },
                            { "Name": "@IDParentTeam", "Value": objTeam.IDParentTeam },
                            { "Name": "@IDLabel", "Value": objTeam.IDLabel },
                            { "Name": "@IDDL", "Value": objTeam.IDDL },
                            { "Name": "@Level", "Value": objTeam.Level },
                            { "Name": "@Name", "Value": objTeam.Name },
                            { "Name": "@SharedFolderPath", "Value": objTeam.SharedFolderPath },
                            { "Name": "@FolderStructure", "Value": objTeam.FolderStructure },
                            { "Name": "@GOC", "Value": objTeam.GOC },
                            { "Name": "@StrListUsers", "Value": objTeam.StrListUsers },
                            { "Name": "@CopyRecursive", "Value": objTeam.CopyRecursive },
                            { "Name": "@SessionSOEID", "Value": _getSOEID() }
                        ],
                        success: {
                            fn: function (response) {
                                //show success alert
                                _showNotification("success", "Data was saved successfully.", "AlertTeamSaved");

                                //Refresh table
                                loadTeamsTable();

                                //Close Modal
                                $formContainer.find(".close").click();
                            }
                        }
                    });
                }
            });
        }
    }

    function formOnReady($formContainer) {
        //Creacte Radio Buttons and Checkbox iCheck library
        _GLOBAL_SETTINGS.iCheck();

        //On Click Manage Labels
        $formContainer.find(".btnManageLabel").click(function () {
            openModalManageLabel({
                onChangesSaved: function () {
                    //Refresh Select of Labels
                    loadSelectOfLabel($formContainer);
                }
            });
        });

        //On click check Share Folder Access
        $formContainer.find(".btnCheckAccess").click(function () {
            checkAccess();
        });

        //Load GOC Selector
        loadSelectOfGOC();

        //Load DL Field
        loadFieldOfDLOfEmail();

        //Load Select of Labels
        loadSelectOfLabel();

        //Load Select of Parent Team
        loadSelectOfParentTeam();

        //Global.js Load div placeholders for content editable
        _loadDivContentEditablePlaceholders();

        //Load Admin Users
        loadTableOfAdminUsers();

        //Load validations for Team Form
        loadValidations();

        function loadValidations() {
            var rules = {};

            // Select Label
            rules["selLabel"] = {
                required: true,
                valueNotEquals: 0
            };

            // Team Name
            rules["txtName"] = {
                divEditableRequired: true
            };

            // GOC
            rules["txtGOC"] = {
                required: true
            };
            
            // Share Folder
            rules["txtSharedFolderPath"] = {
                divEditableRequired: true
            };

            // Folder Structure
            rules["txtFolderStructure"] = {
                divEditableRequired: true
            };

            _validateForm({
                form: $formContainer.find('.formTeam'),
                rules: rules
            });
        }

        function openModalManageLabel(customOptions) {
            //Default Options.
            var options = {
                onChangesSaved: null
            };

            //Hacer un merge con las opciones que nos enviaron y las default.
            $.extend(true, options, customOptions);

            var idTable = "tblManageLabel";
            var htmlListChecklist =
                '<div class="row" style="height: 400px;"> ' +
                '    <div class="col-md-12"> ' +
                '        <button type="button" class="btnNew btn btn-success top15 left15 bottom15 pull-right"><i class="fa fa-plus-square"></i> New</button> ' +
                '        <button type="button" class="btnDelete btn btn-danger top15 left15 bottom15 pull-right"><i class="fa fa-trash-o"></i> Delete</button> ' +
                '    </div> ' +
                '    <div class="col-md-12"> ' +
                '        <div id="' + idTable + '"></div> ' +
                '    </div> ' +
                '</div> ';

            _showModal({
                modalId: "modalManageLabel",
                width: modalsWidth,
                addCloseButton: true,
                buttons: [{
                    name: "<i class='fa fa-save'></i> Save",
                    class: "btn-success",
                    closeModalOnClick: false,
                    onClick: function ($modal) {
                        saveTable();
                    }
                }],
                title: "Manage Label",
                contentHtml: htmlListChecklist,
                onReady: function ($modal) {
                    //Add new row 
                    $modal.find(".btnNew").click(function () {
                        newRow();
                    });

                    //Delete selected row 
                    $modal.find(".btnDelete").click(function () {
                        deleteRow();
                    });

                    //Load table 
                    loadTable();
                }
            });

            function loadTable() {
                $.jqxGridApi.create({
                    showTo: "#" + idTable,
                    options: {
                        //for comments or descriptions
                        height: "250",
                        autoheight: false,
                        autorowheight: false,
                        selectionmode: "singlerow",
                        showfilterrow: true,
                        sortable: true,
                        editable: true,
                        groupable: false
                    },
                    sp: {
                        Name: "[dbo].[spPCTManageLabel]",
                        Params: [
                            { Name: "@Action", Value: "List" },
                            { Name: "@IDMSGroup", Value: getObjMSGroup().IDMSGroup }
                        ]
                    },
                    source: {
                        // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
                        dataBinding: "Large Data Set"
                    },
                    columns: [
                        //type: string - text - number - int - float - date - time 
                        //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                        //cellsformat: ddd, MMM dd, yyyy h:mm tt
                        { name: 'ID', type: 'number', hidden: true },
                        { name: 'Name', text: 'Name', width: '100%', type: 'string', filtertype: 'checkedlist' }
                    ],
                    ready: function () { }
                });
            }

            function newRow() {
                var $jqxGrid = $("#" + idTable);
                var datarow = {
                    ID: 0,
                    Name: 'Please add label name...'
                };
                var commit = $jqxGrid.jqxGrid('addrow', null, datarow);

                _showNotification("success", "An empty row was added in the table.", "NewLabelRow");
            }

            function deleteRow() {
                //Delete 
                var objRowSelected = $.jqxGridApi.getOneSelectedRow("#" + idTable, true);
                if (objRowSelected) {
                    var htmlListChecklist = "";
                    htmlListChecklist += "<b>Label name: </b>" + objRowSelected['Name'] + "<br/>";

                    _showModal({
                        width: '40%',
                        modalId: "modalDelRow",
                        addCloseButton: true,
                        buttons: [{
                            name: "<i class='fa fa-trash-o'></i> Delete",
                            class: "btn-danger",
                            onClick: function () {
                                //Call server to delete only if exists ID in database 
                                if (objRowSelected.ID != 0) {
                                    _callProcedure({
                                        loadingMsgType: "fullLoading",
                                        loadingMsg: "Deleting label...",
                                        name: "[dbo].[spPCTManageLabel]",
                                        params: [
                                            { "Name": "@Action", "Value": "Delete" },
                                            { "Name": "@IDLabel", "Value": objRowSelected.ID },
                                            { "Name": "@SessionSOEID", "Value": _getSOEID() }
                                        ],
                                        success: {
                                            showTo: $("#" + idTable).parent(),
                                            msg: "Label '" + objRowSelected['Name'] + "' was deleted successfully.",
                                            fn: function () {
                                                //Refresh changes
                                                loadTable();

                                                //Remove changes
                                                $.jqxGridApi.rowsChangedFindById("#" + idTable).rows = [];

                                                //Refresh caller
                                                if (options.onChangesSaved) {
                                                    options.onChangesSaved();
                                                }
                                            }
                                        }
                                    });
                                } else {
                                    $("#" + idTable).jqxGrid('deleterow', objRowSelected.uid);
                                }
                            }
                        }],
                        title: "Are you sure you want to delete this row?",
                        contentHtml: htmlListChecklist
                    });
                }
            }

            function saveTable() {
                var rowsChanged = $.jqxGridApi.rowsChangedFindById("#" + idTable).rows;

                //Data is valid
                if (isValidData()) {

                    //Function to show in the last saved item
                    var fnSuccess = {
                        showTo: $("#" + idTable).parent(),
                        msg: "Data was saved successfully.",
                        fn: function () {

                            //Refresh table until all ajax call are completed
                            _execOnAjaxComplete(function () {
                                //Refresh table
                                loadTable();

                                //Remove local cache of changes
                                $.jqxGridApi.rowsChangedFindById("#" + idTable).rows = [];

                                //Refresh caller
                                if (options.onChangesSaved) {
                                    options.onChangesSaved();
                                }
                            });
                        }
                    };

                    for (var i = 0; i < rowsChanged.length; i++) {
                        var objRow = rowsChanged[i];
                        var action = "Edit";

                        if (objRow.ID == 0) {
                            var action = "New";
                        }

                        _callProcedure({
                            loadingMsgType: "fullLoading",
                            loadingMsg: "Saving labels...",
                            name: "[dbo].[spPCTManageLabel]",
                            params: [
                                { "Name": "@Action", "Value": action },
                                { "Name": "@IDLabel", "Value": objRow.ID },
                                { "Name": "@IDMSGroup", "Value": getObjMSGroup().IDMSGroup },
                                { "Name": "@Name", "Value": objRow.Name },
                                { "Name": "@SessionSOEID", "Value": _getSOEID() }
                            ],
                            //Show message only for the last item
                            success: (i == rowsChanged.length - 1) ? fnSuccess : null
                        });
                    }
                }

                //Validate information to save
                function isValidData() {
                    var result = true;

                    //Check if exists changes
                    if (rowsChanged.length == 0) {
                        _showAlert({
                            id: "SaveDataError",
                            showTo: $("#" + idTable).parent(),
                            type: 'error',
                            title: "Message",
                            content: "No changes detected."
                        });

                        result = false;
                    }

                    //Check empty rows
                    for (var i = 0; i < rowsChanged.length; i++) {
                        var objRow = rowsChanged[i];

                        if (objRow.Name == "Please add label name...") {
                            _showAlert({
                                id: "SaveDataError",
                                showTo: $("#" + idTable).parent(),
                                type: 'error',
                                title: "Message",
                                content: "Cannot save empty row <b>Please add label name...</b>"
                            });
                            result = false;
                        }

                        if (!objRow.Name) {
                            _showAlert({
                                id: "SaveDataError",
                                showTo: $("#" + idTable).parent(),
                                type: 'error',
                                title: "Message",
                                content: "Cannot save empty row"
                            });
                            result = false;
                        }
                    }

                    return result;
                }
            }
        }

        function checkAccess() {

            if (!_divEditableIsEmpty($formContainer.find(".txtSharedFolderPath"))) {
                var shareFolderPath = $formContainer.find(".txtSharedFolderPath").text();
                _checkAccess(shareFolderPath);
            } else {
                _showNotification("error", "Share Folder is empty.", "DLError");
            }
        }

        function loadTableOfAdminUsers() {
            
            //Add new row 
            $formContainer.find(".btnNewContact ").click(function () {
                newRow();
            });
            //Delete selected row 
            $formContainer.find(".btnDeleteContact").click(function () {
                deleteRow();
            });
            //On click view admin users DL
            $formContainer.find(".btnAdminUsersDLViewContacts").click(function () {
                adminUsersDLViewContacts();
            });
            
            //Create Select of Admin Users
            _createSelectSOEID({
                id: idSelAdminUsers,
                selSOEID: ""
            });

            //Create Input of DL
            _createInputDL({
                id: "#txtAdminUsersDL_" + idModalTeam,
                labelText: "DL of Users"
            });

            //Load table 
            loadTableUsers();

            function loadTableUsers() {
                $.jqxGridApi.create({
                    showTo: idTableAdminUsers,
                    options: {
                        //for comments or descriptions
                        height: "200",
                        autoheight: false,
                        autorowheight: false,
                        selectionmode: "singlerow",
                        showfilterrow: true,
                        sortable: true,
                        editable: true,
                        groupable: false
                    },
                    sp: {
                        Name: "[dbo].[spPCTManageTeam]",
                        Params: [
                            { Name: "@Action", Value: "List Tree Contacts Admin of Team" },
                            { Name: "@IDMSGroup", Value: objTeam.IDMSGroup },
                            { Name: "@IDTeam", Value: objTeam.ID }
                        ],
                        OnDataLoaded: function (responseList) {
                            $formContainer.find(".lblAdminUsers").html("Admin Users (" + responseList.length + ")");
                        }
                    },
                    source: {
                        // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
                        dataBinding: "Large Data Set"
                    },
                    columns: [
                        //type: string - text - number - int - float - date - time 
                        //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                        //cellsformat: ddd, MMM dd, yyyy h:mm tt
                        { name: 'ID', type: 'string', hidden: true },
                        { name: 'Level', type: 'number', hidden: true },
                        { name: 'Type', text: 'Type', width: '15%', type: 'string', filtertype: 'checkedlist' },
                        {
                            name: 'Description', text: 'Description', width: '45%', type: 'html', filtertype: 'input',
                            cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                                var paddingXLevel = 35;
                                var padding = paddingXLevel * (rowData.Level - 1);
                                var badgeColorToUse = "badge-info";
                                var badgeColors = {
                                    1: "badge-info",
                                    2: "badge-purple",
                                    3: "badge-accent",
                                    4: "badge-primary"
                                };

                                if (padding == 0) {
                                    padding = 2;
                                }

                                if (badgeColors[rowData.Level]) {
                                    badgeColorToUse = badgeColors[rowData.Level];
                                }

                                var tagHtml = '<span class="badge ' + badgeColorToUse + '">L' + rowData.Level + '</span>';
                                var resultHTML =
                                    ' <span style="margin-left: ' + padding + 'px;line-height: 28px;" > ' +
                                        tagHtml + ' ' + value +
                                    '</span>';

                                return resultHTML;
                            }
                        },
                        { name: 'Email', text: 'Email', width: '40%', type: 'string', filtertype: 'input' }
                    ],
                    ready: function () { }
                });
            }

            function newRow() {
                var $jqxGrid = $(idTableAdminUsers);

                //User SOEID
                var inputUserSOEID = $formContainer.find(".selAdminUsers select").find(":selected").val();
                var inputUserName = $formContainer.find(".selAdminUsers select").find(":selected").attr("Name");

                //Validate if the user fill all field
                if (inputUserSOEID) {

                    //Validate if soeid not exist in current list
                    var gridRows = $jqxGrid.jqxGrid("getRows");
                    var existsRow = _findAllObjByProperty(gridRows, "ID", inputUserSOEID);
                    if (existsRow.length == 0) {
                        var datarow = {
                            ID: inputUserSOEID,
                            Level: 1,
                            Type: 'User',
                            Description: inputUserName + " [" + inputUserSOEID + "]",
                            Email: inputUserSOEID + "@citi.com"
                        };
                        var commit = $jqxGrid.jqxGrid('addrow', null, datarow);

                        //Refresh Users Counter
                        $formContainer.find(".lblAdminUsers").html("Admin Users (" + $jqxGrid.jqxGrid("getRows").length + ")");
                    } else {
                        _showAlert({
                            showTo: $jqxGrid.parent(),
                            type: 'error',
                            title: "Message",
                            content: "The user already exists."
                        });
                    }
                } else {

                    //DL for Admin
                    var $inputAdminUsersDL = $formContainer.find(".txtAdminUsersDL .inputDL");
                    if (!_divEditableIsEmpty($inputAdminUsersDL)) {
                        var inputDL = $inputAdminUsersDL.text();
                        var emailDL = _getEmailFromDLSummary(inputDL);

                        //Insert DL in database if not exists
                        _getAutomationIDDL({
                            emailDL: emailDL,
                            onSuccess: function (idDL) {

                                //Validate if soeid not exist in current list
                                var gridRows = $jqxGrid.jqxGrid("getRows");
                                var existsRow = _findAllObjByProperty(gridRows, "ID", idDL);
                                if (existsRow.length == 0) {

                                    var datarow = {
                                        ID: idDL,
                                        Level: 1,
                                        Type: 'DL',
                                        Description: $.trim(inputDL.split("<")[0]),
                                        Email: emailDL
                                    };
                                    var commit = $jqxGrid.jqxGrid('addrow', null, datarow);

                                    //Refresh RPLs Counter
                                    $formContainer.find(".lblAdminUsers").html("Admin Users (" + $jqxGrid.jqxGrid("getRows").length + ")");

                                } else {
                                    _showAlert({
                                        showTo: $jqxGrid.parent(),
                                        type: 'error',
                                        title: "Message",
                                        content: "The DL already exists."
                                    });
                                }

                            }
                        });
                    } else {
                        _showAlert({
                            showTo: $jqxGrid.parent(),
                            type: 'error',
                            title: "Message",
                            content: "Please select an user or enter DL."
                        });
                    }
                }
            }

            function deleteRow() {
                var $jqxGrid = $(idTableAdminUsers);

                //Delete 
                var objRowSelected = $.jqxGridApi.getOneSelectedRow(idTableAdminUsers, true);
                if (objRowSelected) {

                    var htmlListChecklist = "";

                    htmlListChecklist += "<b>Type: </b>" + objRowSelected['Type'] + "<br/>";
                    htmlListChecklist += "<b>Description: </b>" + objRowSelected['Description'] + "<br/>";

                    _showModal({
                        width: '40%',
                        modalId: "modalDelRow",
                        addCloseButton: true,
                        buttons: [{
                            name: "Delete",
                            class: "btn-danger",
                            onClick: function () {
                                //Delete row from grid
                                $jqxGrid.jqxGrid('deleterow', objRowSelected.uid);

                                //Refresh Users Counter
                                $formContainer.find(".lblAdminUsers").html("Admin Users (" + $jqxGrid.jqxGrid("getRows").length + ")");
                            }
                        }],
                        title: "Are you sure you want to delete this row?",
                        contentHtml: htmlListChecklist
                    });
                }
            }
        }

        function loadFieldOfDLOfEmail() {
            _createInputDL({
                id: "#txtDLForSupport_" + idModalTeam,
                dlSummary: objTeam.DLSummary,
                labeLText: "DL for Emails",
                validations: {
                    divEditableRequired : true
                }
            });
        }

        function loadSelectOfGOC() {
            _createSelectGOC({
                id: "#selGOC_" + idModalTeam,
                selelectedGOC: objTeam.GOCDesc,
                labelClass: "col-md-2",
                fieldClass: "col-md-9",
                actionClass: "col-md-1",
                placeholderMSCode: "e. g. " + getObjMSGroup().IDManagedSegment
            });
        }

        function loadSelectOfLabel() {

            _callProcedure({
                loadingMsgType: "topBar",
                loadingMsg: "Loading labels",
                name: "[dbo].[spPCTManageLabel]",
                params: [
                    { "Name": "@Action", "Value": "List" },
                    { "Name": "@IDMSGroup", "Value": getObjMSGroup().IDMSGroup }
                ],
                success: function (resultList) {
                    //Clear old values
                    $formContainer.find(".selLabel").contents().remove();

                    //Add defaul option
                    $formContainer.find(".selLabel").append($('<option>', {
                        value: 0,
                        text: "-- Select --"
                    }));

                    //Add Options
                    for (var i = 0; i < resultList.length; i++) {
                        var objTemp = resultList[i];
                        $formContainer.find(".selLabel").append($('<option>', {
                            value: objTemp.ID,
                            text: objTemp.Name,
                            selected: (objTemp.ID == objTeam.IDLabel)
                        }));
                    }
                }
            });
        }

        function loadSelectOfParentTeam() {
            //Clear old values
            $formContainer.find(".selParentTeam").contents().remove();

            //Add defaul option
            $formContainer.find(".selParentTeam").append($('<option>', {
                value: 0,
                level: 0,
                text: "-- No Parent --"
            }));

            //Add Options
            for (var i = 0; i < teamList.length; i++) {
                var objTempTeam = teamList[i];
                if (objTempTeam.ID != objTeam.ID) {
                    $formContainer.find(".selParentTeam").append($('<option>', {
                        value: objTempTeam.ID,
                        level: objTempTeam.Level,
                        text: generateCharByLevel(objTempTeam.Level, " ") + "(" + objTempTeam.LabelName + ") " + objTempTeam.Name,
                        selected: (objTempTeam.ID == objTeam.IDParentTeam)
                    }));
                }
            }

            //On Change select update Level
            $formContainer.find(".selParentTeam").change(function () {
                var selectedLevel = $formContainer.find(".selParentTeam").find("option:selected").attr("level");
                $formContainer.find(".txtLevel").html(parseInt(selectedLevel) + 1);
            });
            
            function generateCharByLevel(plevel, pchar) {
                var result = "";

                for (var i = 0; i < plevel; i++) {
                    result += pchar + "   -   ";
                }

                return result;
            }

            //_callProcedure({
            //    loadingMsgType: "topBar",
            //    loadingMsg: "Loading business structure...",
            //    name: "[dbo].[spPCTManageTeam]",
            //    params: [
            //        { "Name": "@Action", "Value": "List" },
            //        { "Name": "@IDMSGroup", "Value": getObjMSGroup().IDMSGroup }
            //    ],
            //    success: function (resultList) {
                    
            //    }
            //});
        }
    }
}

function openModalManageChecklist(customOptions) {
    var idModalChecklist = _createCustomID();
    var $modalChecklist = null;

    //Default Options.
    var options = {
        onChangesSaved: null
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);
    var idTable = "tblManageChecklist";

    //Show Modal to User
    _showModal({
        modalId: "modalManageChecklist",
        width: modalsWidth,
        addCloseButton: true,
        buttons: [],
        title: "Manage Check List",
        contentHtml: getHtmlModal(),
        onReady: function ($modal) {
            //Set Modal container
            $modalChecklist = $modal;

            //Add new row 
            $modal.find(".btnNew").click(function () {
                newChecklist();
            });

            //Add edit row 
            $modal.find(".btnEdit").click(function () {
                editChecklist();
            });

            //Delete selected row 
            $modal.find(".btnDelete").click(function () {
                deleteChecklist();
            });

            //Load table 
            loadChecklistTable();
        }
    });

    //Html Modal
    function getHtmlModal() {
        var html =
        ' <ul class="nav nav-tabs"> ' +
        '     <li class="active" id="tab-label-list-checklist"> ' +
        '         <a href="#tab-checklist-list" data-toggle="tab" aria-expanded="false"> ' +
        '             <i class="fa fa-cog"></i> List ' +
        '         </a> ' +
        '     </li> ' +
        '     <li class="" id="tab-label-checklist" style="display:none;"> ' +
        '         <a href="#tab-checklist-form" data-toggle="tab" aria-expanded="true"> ' +
        '             <i class="fa fa-cog"></i> <span id="lblChecklistForm">Check List Form</span> ' +
        '         </a> ' +
        '     </li> ' +
        ' </ul> ' +
        ' <div class="tab-content"> ' +
        '     <div class="tab-pane fade active in" id="tab-checklist-list"> ' +
        '        <button type="button" class="btnNew btn btn-success top15 left15 bottom15 pull-right"><i class="fa fa-plus-square"></i> New</button> ' +
        '        <button type="button" class="btnEdit btn btn-info top15 left15 bottom15 pull-right"><i class="fa fa-pencil-square-o"></i> Edit</button> ' +
        '        <button type="button" class="btnDelete btn btn-danger top15 left15 bottom15 pull-right"><i class="fa fa-trash-o"></i> Delete</button> ' +
        '        <div id="' + idTable + '" class="clearfix"></div> ' +
        '    </div> ' +
        '    <div class="tab-pane fade in" id="tab-checklist-form"> ' +
        '        <div id="adminChecklistContent"></div> ' +
        '    </div> ' +
        '</div> ';

        return html;
    }

    function loadChecklistTable() {
        $.jqxGridApi.create({
            showTo: "#" + idTable,
            options: {
                //for comments or descriptions
                height: "250",
                autoheight: false,
                autorowheight: false,
                selectionmode: "singlerow",
                showfilterrow: true,
                sortable: true,
                editable: true,
                groupable: false
            },
            sp: {
                Name: "[dbo].[spPCTManageChecklist]",
                Params: [
                    { Name: "@Action", Value: "List Check List Of MSGroup" },
                    { Name: "@IDMSGroup", Value: getObjMSGroup().IDMSGroup }
                ]
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
                dataBinding: "Large Data Set"
            },
            columns: [
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                { name: 'ID', type: 'number', hidden: true },
                { name: 'JsonChecklistSteps', type: 'string', hidden: 'true' },
                { name: 'Name', text: 'Name', width: '40%', type: 'string', filtertype: 'checkedlist' },
                { name: 'Description', text: 'Description', width: '50%', type: 'string', filtertype: 'input' },
                { name: 'CountChecklistSteps', text: 'Check Steps', width: '10%', type: 'number', filtertype: 'number' }
                
            ],
            ready: function () { }
        });
    }

    function newChecklist() {
        //Show tab with edit or add new form
        $modalChecklist.find("#tab-label-checklist").show();

        //Load form to add or edit an process file
        loadAdminChecklistForm();

        //Go to the Form Tab
        $modalChecklist.find("#tab-label-checklist").find("a").click();

        //Update tab label
        $modalChecklist.find("#lblChecklistForm").html("New Check List");
    }

    function editChecklist() {
        var objRowSelected = $.jqxGridApi.getOneSelectedRow("#" + idTable, true);
        if (objRowSelected) {
            //Show tab with edit or add new form
            $modalChecklist.find("#tab-label-checklist").show();

            //Load form to add or edit an process file
            loadAdminChecklistForm({
                objChecklist: objRowSelected
            });

            //Go to the Form Tab
            $modalChecklist.find("#tab-label-checklist").find("a").click();

            //Update tab label
            $modalChecklist.find("#lblChecklistForm").html("Edit Check List");
        }
    }

    function deleteChecklist() {
        //Delete 
        var objRowSelected = $.jqxGridApi.getOneSelectedRow("#" + idTable, true);
        if (objRowSelected) {
            var htmlListChecklist = "";
            htmlListChecklist += "<b>Checklist name: </b>" + objRowSelected['Name'] + "<br/>";

            _showModal({
                width: '40%',
                modalId: "modalDelRow",
                addCloseButton: true,
                buttons: [{
                    name: "<i class='fa fa-trash-o'></i> Delete",
                    class: "btn-danger",
                    onClick: function () {
                        //Call server to delete only if exists ID in database 
                        if (objRowSelected.ID != 0) {
                            _callProcedure({
                                loadingMsgType: "fullLoading",
                                loadingMsg: "Deleting label...",
                                name: "[dbo].[spPCTManageChecklist]",
                                params: [
                                    { "Name": "@Action", "Value": "Delete" },
                                    { "Name": "@IDChecklist", "Value": objRowSelected.ID },
                                    { "Name": "@SessionSOEID", "Value": _getSOEID() }
                                ],
                                success: {
                                    showTo: $("#" + idTable).parent(),
                                    msg: "Checklist '" + objRowSelected['Name'] + "' was deleted successfully.",
                                    fn: function () {
                                        //Refresh changes
                                        loadTable();

                                        //Remove changes
                                        $.jqxGridApi.rowsChangedFindById("#" + idTable).rows = [];

                                        //Refresh caller
                                        if (options.onChangesSaved) {
                                            options.onChangesSaved();
                                        }
                                    }
                                }
                            });
                        } else {
                            $("#" + idTable).jqxGrid('deleterow', objRowSelected.uid);
                        }
                    }
                }],
                title: "Are you sure you want to delete this row?",
                contentHtml: htmlListChecklist
            });
        }
    }

    function loadAdminChecklistForm(customOptions) {
        //Default Options.
        var options = {
            objChecklist: {
                ID: 0,
                Name: "",
                Description: "",
                JsonChecklistSteps: "[]"
            },
            onChangesSaved: null
        };

        //Hacer un merge con las opciones que nos enviaron y las default.
        $.extend(true, options, customOptions);

        //Add template item
        $modalChecklist.find("#adminChecklistContent").contents().remove();
        $modalChecklist.find("#adminChecklistContent").html(getHtmlAdminChecklistForm(options.objChecklist));

        //On Click Save Checklist
        $modalChecklist.find(".btnSave").click(function () {
            saveChecklist();
        });

        //Validate Form
        loadValidations();

        //Load checklist step table
        loadChecklistStepsTable({
            objChecklist: options.objChecklist,
            onSuccess: function () { }
        });

        function loadValidations() {
            var rules = {};

            // Checklist Name
            rules["txtName"] = {
                divEditableRequired: true
            };

            // Validate Description
            rules["txtDescription"] = {
                divEditableRequired: true
            };

            _validateForm({
                form: '#formChecklist_' + idModalChecklist,
                rules: rules
            });
        }

        function loadChecklistStepsTable(customOptions) {
            //Default Options.
            var options = {
                objChecklist: {},
                onSuccess: null
            };

            //Hacer un merge con las opciones que nos enviaron y las default.
            $.extend(true, options, customOptions);

            //When click add new shared filed
            $modalChecklist.find(".btnAddChecklistStep").click(function () {
                addNewRow();
            });

            //Load current shared drive files
            var checklisSteps = JSON.parse(options.objChecklist.JsonChecklistSteps);
            if (checklisSteps.length > 0) {
                for (var i = 0; i < checklisSteps.length; i++) {
                    addNewRow(checklisSteps[i]);
                }
            } else {
                //Load 1 by default
                addNewRow();
            }

            function addNewRow(pobjChecklistStep) {
                var $row = $(getHtmlTemplateRow(pobjChecklistStep));

                //On Click Delete
                $row.find(".btnDeleteChecklistStep").click(function () {
                    var idRowToDelete = $(this).attr("idRow");
                    deleteRow(idRowToDelete);
                });

                //Add row to table
                $modalChecklist.find(".containerRowsChecklistStep").append($row);

                //Auto Grow textarea
                _execOnObjectShows(".txtChecklistStepDesc", function () {
                    //$row.find(".txtChecklistStepDesc").trigger("onkeyup");
                    //$row.find(".txtChecklistStepGuidelines").trigger("onkeyup");

                    //Add autosize to textareas
                    $row.find("textarea").autosize();
                });
                
                //Add Validation rule
                $row.find(".txtChecklistStepNumber").rules("add", {
                    digits: true,
                    divEditableRequired: true
                });

                //Add Validation rule
                $row.find(".txtChecklistStepGroupName").rules("add", {
                    divEditableRequired: true
                });

                //Add Validation rule
                $row.find(".txtChecklistStepDesc").rules("add", {
                    required: true
                });

                //Global.js Load div placeholders for content editable
                _loadDivContentEditablePlaceholders();
            }

            function deleteRow(idRowToDelete) {
                var objChecklistStep = getObjChecklistStepByIDRow(idRowToDelete);
                if (objChecklistStep.ID != "0") {
                    var htmlContentModal = "";
                    htmlContentModal += "<b>Number: </b>" + objChecklistStep.Number + "<br/>";
                    htmlContentModal += "<b>Description: </b>" + objChecklistStep.Description + "<br/>";

                    _showModal({
                        width: '40%',
                        modalId: "modalDelRow",
                        addCloseButton: true,
                        buttons: [{
                            name: "<i class='fa fa-trash-o'></i> Delete",
                            class: "btn-danger",
                            onClick: function () {
                                doDelete();
                            }
                        }],
                        title: "Are you sure you want to delete this row?",
                        contentHtml: htmlContentModal
                    });
                } else {
                    doDelete();
                }

                function doDelete() {
                    if ($(".rowChecklistStep").length == 1) {
                        _showNotification("error", "Cannot delete the only one step for this checklist.", "ChecklistStepError");
                    } else {
                        if (objChecklistStep.ID != "0") {
                            //Whe is a saved DB Row
                            $("#" + idRowToDelete).addClass("rowChecklistStepDeleted");

                            //Hide Row
                            $("#" + idRowToDelete).hide();
                        } else {
                            //Is row is not saved remove HTML Row element
                            $("#" + idRowToDelete).remove();
                        }
                    }
                }
            }

            function getHtmlTemplateRow(pobjChecklistStep) {
                //Default Options.
                var objChecklistStep = {
                    ID: 0,
                    Number: $modalChecklist.find(".rowChecklistStep").length + 1,
                    GroupName: 'General',
                    Description: '',
                    Guidelines: ''
                };

                //Hacer un merge con las opciones que nos enviaron y las default.
                $.extend(true, objChecklistStep, pobjChecklistStep);

                var idControl = _createCustomID();
                var idRow = "rowChecklistStep_" + idControl;
                var idTxtChecklistStepNumber = "txtChecklistStepNumber_" + idControl;
                var idTxtChecklistStepGroupName = "txtChecklistStepGroupName_" + idControl;
                var idTxtChecklistStepDesc = "txtChecklistStepDesc_" + idControl;
                var idTxtChecklistStepGuidelines = "txtChecklistStepGuidelines_" + idControl;

                var html =
                    ' <tr class="rowChecklistStep" id="' + idRow + '" idDB="' + objChecklistStep.ID + '"> ' +
                    '     <td> ' +
                    '         <div class="form-control txtChecklistStepNumber" placeholder="Add step number..." name="' + idTxtChecklistStepNumber + '" id="' + idTxtChecklistStepNumber + '" contenteditable="true">' + objChecklistStep.Number + '</div> ' +
                    '     </td> ' +
                    '     <td> ' +
                    '         <div class="form-control txtChecklistStepGroupName" placeholder="Add step group name..." name="' + idTxtChecklistStepGroupName + '" id="' + idTxtChecklistStepGroupName + '" contenteditable="true">' + objChecklistStep.GroupName + '</div> ' +
                    '     </td> ' +
                    '     <td> ' +
                    '         <textarea class="form-control txtChecklistStepDesc" placeholder="Add step description..." name="' + idTxtChecklistStepDesc + '" id="' + idTxtChecklistStepDesc + '" >' + objChecklistStep.Description + '</textarea> ' +
                    '     </td> ' +
                    '     <td> ' +
                    '         <textarea class="form-control txtChecklistStepGuidelines" placeholder="Add step guidelines..." name="' + idTxtChecklistStepGuidelines + '" id="' + idTxtChecklistStepGuidelines + '" >' + objChecklistStep.Guidelines + '</textarea> ' +
                    '     </td> ' +
                    '     <td> ' +
                    '         <button type="button" class="btn btn-danger btn-sm btnDeleteChecklistStep" idRow="' + idRow + '"> ' +
                    '             <i class="fa fa-trash-o"></i> ' +
                    '         </button> ' +
                    '     </td> ' +
                    ' </tr> ';

                return html;
            }
        }

        function getObjChecklistStepByIDRow(idRow) {
            var id = $("#" + idRow).attr("idDB");
            var number = $("#" + idRow).find(".txtChecklistStepNumber").text();
            var groupName = $("#" + idRow).find(".txtChecklistStepGroupName").text();
            var description = $("#" + idRow).find(".txtChecklistStepDesc").val();
            var guidelines = $("#" + idRow).find(".txtChecklistStepGuidelines").val();

            return {
                ID: id,
                Number: number,
                GroupName: groupName,
                Description: description,
                Guidelines: guidelines
            }
        }

        function saveChecklistSteps(customOptions) {

            //Default Options.
            var options = {
                idChecklist: "0",
                onSuccess: null
            };

            //Do merge of customOptions with default options.
            $.extend(true, options, customOptions);

            //Delete files
            deleteChecklistSteps({
                onSuccess: function () {
                    //Save New or Updated ChecklistSteps
                    saveChecklistSteps({
                        onSuccess: function () {
                            options.onSuccess();
                        }
                    });
                }
            });

            function saveChecklistSteps(customSaveOptions) {

                //Default Options.
                var saveOptions = {
                    onSuccess: null
                };

                //Do merge of customOptions with default options.
                $.extend(true, saveOptions, customSaveOptions);

                //Function to show in the last saved item
                var fnSuccessLastItemSaved = {
                    fn: function () {

                        //Refresh table until all ajax call are completed
                        _execOnAjaxComplete(function () {

                            //Call onSuccess 
                            saveOptions.onSuccess();

                        });
                    }
                };

                //By each Shared ChecklistStep
                var $rowChecklistSteps = $(".rowChecklistStep:not(.rowChecklistStepDeleted)");
                if ($rowChecklistSteps.length > 0) {
                    $rowChecklistSteps.each(function (index, elementRow) {
                        var action = "New";
                        var objChecklistStep = getObjChecklistStepByIDRow($(elementRow).attr("id"));

                        //Whe exist in Database
                        if (objChecklistStep.ID != "0") {
                            action = "Edit";
                        }

                        //Save change to get the ID
                        _callProcedure({
                            loadingMsgType: "fullLoading",
                            loadingMsg: "Saving Check List Steps...",
                            name: "[dbo].[spPCTManageChecklistStep]",
                            params: [
                            { "Name": "@Action", "Value": action },
                            { "Name": "@IDChecklistStep", "Value": objChecklistStep.ID },
                            { "Name": "@IDChecklist", "Value": options.idChecklist },
                            { "Name": "@Number", "Value": objChecklistStep.Number },
                            { "Name": "@GroupName", "Value": objChecklistStep.GroupName },
                            { "Name": "@Description", "Value": objChecklistStep.Description },
                            { "Name": "@Guidelines", "Value": objChecklistStep.Guidelines },
                            { "Name": "@SessionSOEID", "Value": _getSOEID() }
                            ],
                            //Show message only for the last item
                            success: (index == ($rowChecklistSteps.length - 1)) ? fnSuccessLastItemSaved : null
                        });
                    });
                } else {
                    //Call onSuccess 
                    saveOptions.onSuccess();
                }
            }

            function deleteChecklistSteps(customDeleteOptions) {

                //Default Options.
                var deleteOptions = {
                    onSuccess: null
                };

                //Do merge of customOptions with default options.
                $.extend(true, deleteOptions, customDeleteOptions);

                //Function to show in the last deleted item
                var fnSuccessLastItemDeleted = {
                    fn: function () {

                        //Refresh table until all ajax call are completed
                        _execOnAjaxComplete(function () {

                            //Call onSuccess 
                            deleteOptions.onSuccess();

                        });
                    }
                };

                //By each Deleted 
                var $rowChecklistStepDeleted = $(".rowChecklistStepDeleted");
                if ($rowChecklistStepDeleted.length > 0) {
                    $rowChecklistStepDeleted.each(function (index, elementRow) {
                        var objChecklistStep = getObjChecklistStepByIDRow($(elementRow).attr("id"));

                        //Save change to get the ID
                        _callProcedure({
                            loadingMsgType: "fullLoading",
                            loadingMsg: "Saving Check List Steps...",
                            name: "[dbo].[spPCTManageChecklistStep]",
                            params: [
                            { "Name": "@Action", "Value": "Delete" },
                            { "Name": "@IDChecklistStep", "Value": objChecklistStep.ID },
                            { "Name": "@SessionSOEID", "Value": _getSOEID() }
                            ],
                            //Show message only for the last item
                            success: (index == ($rowChecklistStepDeleted.length - 1)) ? fnSuccessLastItemDeleted : null
                        });
                    });
                } else {
                    //Call onSuccess 
                    deleteOptions.onSuccess();
                }
            }
        }

        function saveChecklist() {
            if ($modalChecklist.find("#formChecklist_" + idModalChecklist).valid()) {
                var objChecklist = getObjChecklist();
                var action = "New";

                if (objChecklist.ID != 0) {
                    action = "Edit";
                }

                //Save change to get the ID
                _callProcedure({
                    loadingMsgType: "fullLoading",
                    loadingMsg: "Saving Checklist...",
                    name: "[dbo].[spPCTManageChecklist]",
                    params: [
                        { "Name": "@Action", "Value": action },
                        { "Name": "@IDChecklist", "Value": objChecklist.ID },
                        { "Name": "@IDMSGroup", "Value": getObjMSGroup().IDMSGroup },
                        { "Name": "@Name", "Value": objChecklist.Name },
                        { "Name": "@Description", "Value": objChecklist.Description },
                        { "Name": "@SessionSOEID", "Value": _getSOEID() }
                    ],
                    success: {
                        fn: function (response) {
                            if (response.length > 0) {
                                var objChecklistSaved = response[0];

                                //Save Check List Steps
                                saveChecklistSteps({
                                    idChecklist: objChecklistSaved.ID,
                                    onSuccess: function () {
                                        //Go to the Checklist List Tab
                                        $("#tab-label-list-checklist").find("a").click();

                                        //Close current Form Tab
                                        $("#tab-label-checklist").hide();
                                        $("#adminChecklistContent").contents().remove();

                                        //Show Message to user
                                        _showAlert({
                                            showTo: $modalChecklist.find(".modal-body"),
                                            id: "ChecklistAlert",
                                            type: 'success',
                                            title: "Message",
                                            content: "Checklist " + objChecklist.Name + " was saved successfully."
                                        });

                                        //Refresh Admin Checklist Page
                                        loadChecklistTable();
                                    }
                                });
                                
                            }
                        }
                    }
                });
            } else {
                _showAlert({
                    showTo: $modalChecklist.find(".modal-body"),
                    id: "SaveDataError",
                    type: 'error',
                    title: "Message",
                    content: "Please review errors."
                });
            }
        }

        function getObjChecklist() {
            var objChecklist = {
                ID: options.objChecklist.ID,
                Name: $modalChecklist.find(".txtName").text(),
                Description: $modalChecklist.find(".txtDescription").text()
            };

            return objChecklist;
        }

    }

    function getHtmlAdminChecklistForm(pobjChecklist) {
        var html =
        ' <form id="formChecklist_' + idModalChecklist + '" class="form-horizontal"> ' +
        '   <button type="button" class="btnSave btn btn-success left15 pull-right"><i class="fa fa-floppy-o"></i> Save Check List</button> ' +
        '   <div style="clear:both;"></div> ' +
        '   <div class="form-horizontal">' +

        '       <div class="module toggle-wrap"> ' +
        '           <div class="mod-header"> ' +
        '               <ul class="ops"></ul> ' +
        '               <h2 class="toggle-title"><i class="fa fa-th-large"></i> Details</h2> ' +
        '           </div> ' +
        '           <div class="mod-content"> ' +
        '               <div class="row"> ' +
        '                   <div class="col-md-12"> ' +

                                //Name
        '                       <div class="form-group"> ' +
        '                           <label class="col-md-2 control-label"> ' +
        '                               <i class="fa fa-angle-double-right"></i> Name ' +
        '                           </label> ' +
        '                           <div class="col-md-8"> ' +
        '                               <div class="form-control txtName" name="txtName" placeholder="Add check list name..." contenteditable="true" >' + pobjChecklist.Name + '</div> ' +
        '                           </div> ' +
        '                       </div> ' +

                                //Description
        '                       <div class="form-group"> ' +
        '                           <label class="col-md-2 control-label"> ' +
        '                               <i class="fa fa-angle-double-right"></i> Description ' +
        '                           </label> ' +
        '                           <div class="col-md-8"> ' +
        '                               <div class="form-control txtDescription" name="txtDescription" placeholder="Add check list description..." contenteditable="true" >' + pobjChecklist.Description + '</div> ' +
        '                           </div> ' +
        '                       </div> ' +

        '                   </div> ' +
        '               </div> ' +
        '           </div> ' +
        '       </div> ' +

        '       <div class="module toggle-wrap"> ' +
        '           <div class="mod-header"> ' +
        '               <ul class="ops"> ' +
        '                   <button type="button" class="btn btn-success btn-sm btnAddChecklistStep " style="margin-right: 14px;"> ' +
        '                       <i class="fa fa-plus-square-o"></i> ' +
        '                   </button> ' +
        '               </ul> ' +
        '               <h2 class="toggle-title"><i class="fa fa-files-o"></i> Check List Steps</h2> ' +
        '           </div> ' +
        '           <div class="mod-content"> ' +

        '               <table class="table table-hover"> ' +
        '                   <thead> ' +
        '                       <tr> ' +
        '                           <td width="5%">Number</td> ' +
        '                           <td width="15%">Group Name</td> ' +
        '                           <td width="40%">Check Description</td> ' +
        '                           <td width="40%">Additional guidelines</td> ' +
        '                       </tr> ' +
        '                   </thead> ' +
        '                   <tbody class="containerRowsChecklistStep"></tbody> ' +
        '               </table> ' +

        '           </div> ' +
        '       </div> ' +
        '   </div> ' +

        ' </form> ';

        return html;
    }

}

function deleteTeam() {
    var rowData = $.jqxGridApi.getOneSelectedRow("#tblTeams", true);
    if (rowData) {
        var htmlListChecklist = "";
        htmlListChecklist += "<b>Name: </b>" + rowData['Tree'] + "<br/>";
        htmlListChecklist += "<b>GOC: </b>" + rowData['GOCDesc'] + "<br/>";
        htmlListChecklist += "<b>Total Activities: </b>" + rowData['TotalActivities'] + "<br/>";

        _showModal({
            width: '40%',
            modalId: "modalDelRow",
            addCloseButton: true,
            buttons: [{
                name: "<i class='fa fa-trash-o'></i> Delete",
                class: "btn-danger",
                onClick: function () {
                    //Call server to delete only if exists ID in database 
                    if (rowData.ID != 0) {
                        _callProcedure({
                            loadingMsgType: "fullLoading",
                            loadingMsg: "Deleting '" + rowData['Tree'] + "'...",
                            name: "[dbo].[spPCTManageTeam]",
                            params: [
                                { "Name": "@Action", "Value": "Delete" },
                                { "Name": "@IDTeam", "Value": rowData.ID },
                                { "Name": "@SessionSOEID", "Value": _getSOEID() }
                            ],
                            success: {
                                showTo: $("#tblTeams").parent(),
                                msg: "'" + rowData['Tree'] + "' was deleted successfully.",
                                fn: function () {
                                    //Refresh changes
                                    loadTeamsTable();

                                    //Remove changes
                                    $.jqxGridApi.rowsChangedFindById("#tblTeams").rows = [];

                                }
                            }
                        });
                    } else {
                        $("#tblTeams").jqxGrid('deleterow', rowData.uid);
                    }
                }
            }],
            title: "Are you sure you want to delete this row?",
            contentHtml: htmlListChecklist
        });
    }
}