﻿/// <reference path="../Shared/plugins/util/global.js" />
/// <reference path="PCTGlobal.js" />

//Hide Menu
_hideMenu();

//Remove Page Title
$(".page-title").remove();

//Frequency List
var _frequencyList = [];

$(document).ready(function () {
    //On click delete
    $(".btnDelete").click(function (e) {
        deleteFrequency();
    });

    //On click generate recurrences
    $(".btnGenerateAll").click(function (e) {
        generateFrequency();
    });

    //On click view frequency
    $(".btnViewFrequency").click(function (e) {
        viewFrequency();
    });

    //Load Frequencies
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading frequencies...",
        name: "[dbo].[spPCTManageFrequency]",
        params: [
            { Name: "@Action", Value: "List All" },
            { Name: "@SessionSOEID", Value: _getSOEID() }
        ],
        success: {
            fn: function (resultList) {
                _frequencyList = resultList;

                //Set Status
                for (var i = 0; i < _frequencyList.length; i++) {
                    _frequencyList[i].Status = "Ok";
                }

                //Load table
                loadTableFrequency();
            }
        }
    });

    //When all ajax are completed (_loadTimeZone();, _updateCurrentUserDLs();, loadActivitySummaryOfUser();)
    _execOnAjaxComplete(function () {

        //Load timezone variables for frequency Manager
        _frequencyManager.init();

        //Hide Full Page Loading
        _hideLoadingFullPage({
            id: "ActivityPageLoading"
        });
    });
});

function loadTableFrequency() {
    $.jqxGridApi.create({
        showTo: "#tblFrequency",
        options: {
            //for comments or descriptions
            height: "500",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: true,
            sortable: true,
            editable: true,
            groupable: false
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set Local",
            rows: _frequencyList
        },
        columns: [
            //type: string - text - number - int - float - date - time 
            //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
            //cellsformat: ddd, MMM dd, yyyy h:mm tt
            { name: 'Status', pinned: true, cellsalign: 'center', align: 'center', type: 'string', width: '110px', filtertype: 'input' },
            { name: 'ID', type: 'number', width: '90px', filtertype: 'input' },
            { name: 'Code', type: 'string', width: '400px', filtertype: 'input' },
            { name: 'LastYearGenerated', text: 'Last Year', type: 'string', width: '80px', filtertype: 'input' },
            { name: 'CreatedBy', text: 'Created By', width: '90px', type: 'string', filtertype: 'checkedlist' },
            { name: 'CreatedDate', text: 'Created Date', width: '160px', type: 'date', cellsformat: "yyyy-MM-dd hh:mm tt", filtertype: 'input' },
            { name: 'ModifiedBy', text: 'Modified By', width: '90px', type: 'string', filtertype: 'checkedlist' },
            { name: 'ModifiedDate', text: 'Modified Date', width: '160px', type: 'date', cellsformat: "yyyy-MM-dd hh:mm tt", filtertype: 'input' }
        ],
        ready: function () { }
    });
}

function viewFrequency() {
    var selectedRow = $.jqxGridApi.getOneSelectedRow("#tblDLs", true);
    if (selectedRow) {
        _openModalViewContactsOfDL({
            title: "Contacts of '" + selectedRow.DisplayName + "'",
            idDL: selectedRow.ID
        });
    }
}

function deleteFrequency() {
    var selectedRow = $.jqxGridApi.getOneSelectedRow("#tblDLs", true);
    if (selectedRow) {
        var htmlContentModal = "";
        htmlContentModal += "<b>DL Name: </b>" + selectedRow['DisplayName'] + "<br/>";

        _showModal({
            width: '40%',
            modalId: "modalDelRow",
            addCloseButton: true,
            buttons: [{
                name: "<i class='fa fa-trash-o'></i> Delete",
                class: "btn-danger",
                onClick: function () {
                    _callProcedure({
                        loadingMsgType: "topBar",
                        loadingMsg: "Loading labels",
                        name: "[Automation].[dbo].[spAFrwkAdminDL]",
                        params: [
                            { "Name": "@Action", "Value": "Delete" },
                            { "Name": "@IDDL", "Value": selectedRow.ID },
                            { "Name": "@SessionSOEID", "Value": _getSOEID() }
                        ],
                        success: function (response) {
                            //Show message
                            _showAlert({
                                id: "DeleteDL",
                                type: 'success',
                                title: "Message",
                                content: "DL '" + selectedRow.DisplayName + "' was deleted successfull"
                            });

                            //Refresh table
                            loadTableDLs();
                        }
                    });
                }
            }],
            title: "Are you sure you want to delete this row?",
            contentHtml: htmlContentModal
        });
    }
}

function generateFrequency() {
    var yearToGenerate = $("#txtYear").val()
    if (yearToGenerate) {

        //Start generating
        generateFrequencyYearRecursive(yearToGenerate, 0);

    } else {
        _showAlert({
            type: 'error',
            title: "Message",
            content: "Please enter the Year to generate"
        });
    }
}

function generateFrequencyYearRecursive(yearToGenerate, indexFrequency) {
    if(_frequencyList.length - 1 >= indexFrequency){
        var objFrequency = _frequencyList[indexFrequency];

        //Set Status
        objFrequency.Status = "Processing...";

        //Refres Table
        loadTableFrequency();

        _frequencyManager.generateFrequencyYearDatesInDB({
            yearToGenerate: yearToGenerate,
            frequencyToGenerateList: [{
                ID: objFrequency.ID,
                Code: objFrequency.Code,
                Frequency: objFrequency.Frequency,
                Type: objFrequency.Type,
                LastYearGenerated: objFrequency.LastYearGenerated,
                CreatedBy: objFrequency.CreatedBy,
                CreatedDate: objFrequency.CreatedDate
            }],
            onSuccess: function (objFrequencyProcesed) {

                //Set Status
                objFrequency.Status = "Updated";

                //Refres Table
                loadTableFrequency();

                generateFrequencyYearRecursive(yearToGenerate, indexFrequency + 1);
            }
        });
    }
}