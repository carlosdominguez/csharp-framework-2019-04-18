/// <reference path="../../Shared/plugins/util/global.js" />
var _listTeamsOfUser = [];
var _listCFields = [];
var _frequencyManager = {
    businessDayList: ['BD-14', 'BD-13', 'BD-12', 'BD-11', 'BD-10', 'BD-9', 'BD-8', 'BD-7', 'BD-6', 'BD-5', 'BD-4', 'BD-3', 'BD-2', 'BD-1', 'BD1', 'BD2', 'BD3', 'BD4', 'BD5', 'BD6', 'BD7', 'BD8', 'BD9', 'BD10', 'BD11', 'BD12', 'BD13', 'BD14', 'BD15', 'BD16', 'BD17', 'BD18', 'BD19', 'BD20', 'BD21'],
    monthList: ['January', 'February', 'March', 'April', 'May', 'June ', 'July', 'August', 'September', 'October', 'November', 'December'],
    monthDayList: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'],
    maxDateCorporateCalendar: "",
    idModalFrequency: "",

    openModalSelectFrequency: function (customOptions) {
        //Default Options.
        var options = {
            frequencyCode: "",
            frequencyStartDate: moment().format('MMM DD, YYYY'),
            onSelect: null
        };

        //Merge default options
        $.extend(true, options, customOptions);

        //Init variables
        _frequencyManager.init();

        //Show Form of Select Frequency
        _showModal({
            width: "95%",
            modalId: "SelectFrequency",
            addCloseButton: true,
            buttons: [{
                name: "<i class='fa fa-crosshairs'></i> Select Frequency",
                class: "btn-info",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    var frequencyCode = $modal.find("#lblFrequencyCode").html();
                    _frequencyManager.generateFrequencyDatesInDB({
                        frequencyCode: frequencyCode,
                        onSuccess: function (objRecurrence) {
                            //Set current UTC Start Date Time
                            var frequencyStartDate = $("#frequencyStartDate_" + _frequencyManager.idModalFrequency).val();
                            var momentActivityStartDate = moment(frequencyStartDate + " 00:00", 'MMM DD, YYYY HH:mm');

                            var timeZoneInfo = _frequencyManager.getObjTimeZoneSelected();
                            var utcDateTime = moment.utc(momentActivityStartDate.format("YYYY-MM-DD HH:mm")).add(parseInt(timeZoneInfo.UTCOffsetMinutes * -1), 'minutes');

                            objRecurrence.UTCStartDateTime = utcDateTime.format("YYYY-MM-DD HH:mm");

                            //Trigger on Select to Caller Function
                            options.onSelect(objRecurrence);

                            //Close Modal
                            $modal.find(".close").click();
                        }
                    });
                }
            }],
            title: "Generate Frequency",
            onReady: function ($modal) {
                onReady($modal);
            },
            contentHtml: getHtml()
        });

        function getHtml() {
            var htmlFrequency =
                ' <div class="row"> ' +

                      //Form: Time Zone and SLA Time
                '     <div class="col-md-3"> ' +
                '         <h4 style="color: #004786;padding-bottom: 11px;">Settings</h4> ' +

                            //Frequency Time Zone
                '           <div class="form-group"> ' +
                '               <label class="col-md-12 control-label">Activity Date Time Zone</label> ' +
                '               <div class="col-md-12"> ' +
                '                   <select class="form-control selFrequencyTimeZone" id="selFrequencyTimeZone_' + _frequencyManager.idModalFrequency + '"></select>' +
                '               </div> ' +
                '           </div> ' +

                            //SLA Time
                '           <div class="form-group"> ' +
                '               <label class="col-md-12 control-label top15" >SLA Time</label> ' +
                '               <div class="col-md-12"> ' +
                '                   <select class="form-control selFrequencySLATime" id="selFrequencySLATime_' + _frequencyManager.idModalFrequency + '"></select> ' +
                '               </div> ' +
                '           </div> ' +

                            //Sel Activity Start Date
                '           <div class="form-group"> ' +
                '               <label class="col-md-12 control-label top15">Frequency Start Date:</label> ' +
                '               <div class="col-md-12"> ' +
                '                   <input type="text" value="' + options.frequencyStartDate + '" class="form-control" id="frequencyStartDate_' + _frequencyManager.idModalFrequency + '" /> ' +
                '               </div> ' +
                '           </div> ' +

                '     </div> ' +
                      //Form: Time Zone and SLA Time

                      //Start: Generate Frequency Code
                '     <div class="col-md-9" > ' +
                '         <div class="form-horizontal"> ' +
                '             <h4 style="color: #004786;">Frequency Code: <span id="lblFrequencyCode" class="badge badge-lg badge-success">R0000000</span></h4> ' +
                '             <div class="row"> ' +
                '                 <div class="col-md-5"> ' +
                                      //Start
                '                     <div class="form-group row" style="display:none;"> ' +
                '                         <label for="containerRepeat" class="col-md-2 col-form-label"> ' +
                '                             Start ' +
                '                         </label> ' +
                '                         <div class="col-md-10"> ' +
                '                             <input class="form-control" value="May 18, 2018" /> ' +
                '                         </div> ' +
                '                     </div> ' +

                                      //Repeat
                '                     <div class="form-group row" id="containerRepeat"> ' +
                '                         <label for="containerRepeat" class="col-md-2 col-form-label"> ' +
                '                             Repeat ' +
                '                         </label> ' +
                '                         <div class="col-md-10"> ' +
                '                             <div class="form-control" id="fieldRepeat"> ' +
                '                                 <ul class="list-unstyled"> ' +
                '                                     <li> ' +
                '                                         <input type="radio" id="flat-radio-daily" value="Daily" name="chkRepeat" class="skin-flat-green" checked> ' +
                '                                         <label class="icheck-label form-label" for="flat-radio-daily">Daily</label> ' +
                '                                     </li> ' +
                '                                     <li> ' +
                '                                         <input type="radio" id="flat-radio-weekly" value="Weekly" name="chkRepeat" class="skin-flat-green"> ' +
                '                                         <label class="icheck-label form-label" for="flat-radio-weekly">Weekly</label> ' +
                '                                     </li> ' +
                '                                     <li> ' +
                '                                         <input type="radio" id="flat-radio-monthly" value="Monthly" name="chkRepeat" class="skin-flat-green"> ' +
                '                                         <label class="icheck-label form-label" for="flat-radio-monthly">Monthly</label> ' +
                '                                     </li> ' +
                '                                     <li> ' +
                '                                         <input type="radio" id="flat-radio-quarterly" value="Quarterly" name="chkRepeat" class="skin-flat-green"> ' +
                '                                         <label class="icheck-label form-label" for="flat-radio-quarterly">Quarterly</label> ' +
                '                                     </li> ' +
                                                      //Hide Weekly Recurrence
                '                                     <li style="display:none;"> ' +
                '                                         <input type="radio" id="flat-radio-yearly" value="Yearly" name="chkRepeat" class="skin-flat-green"> ' +
                '                                         <label class="icheck-label form-label" for="flat-radio-yearly">Yearly</label> ' +
                '                                     </li> ' +
                '                                 </ul> ' +
                '                             </div> ' +
                '                         </div> ' +
                '                     </div> ' +
                '                 </div> ' +
                '                 <div class="col-md-7"> ' +
                                      //Repeat Daily
                '                     <div class="form-group row" id="containerRepeatDaily"> ' +
                '                         <div class="col-md-12"> ' +
                '                             <div class="form-control" id="fieldRepeatDaily"> ' +
                '                                 <ul class="list-unstyled"> ' +
                '                                     <li> ' +
                '                                         <input type="radio" value="AllDays" name="chkRepeatDaily" class="skin-flat-green" checked> ' +
                '                                         <label class="icheck-label form-label">All days</label> ' +
                '                                     </li> ' +
                '                                     <li> ' +
                '                                         <input type="radio" value="Workdays" name="chkRepeatDaily" class="skin-flat-green"> ' +
                '                                         <label class="icheck-label form-label">Workdays (Mon, Tue, Wed, Thu, Fri)</label> ' +
                '                                     </li> ' +
                '                                 </ul> ' +
                '                             </div> ' +
                '                         </div> ' +
                '                     </div> ' +
                                      //Repeat Weekly
                '                     <div class="form-group row" id="containerRepeatWeekly" style="display:none;"> ' +
                '                         <label for="fieldRepeatWeekly" class="col-md-2 col-form-label"> ' +
                '                             Days ' +
                '                         </label> ' +
                '                         <div class="col-md-10"> ' +
                '                             <div class="form-control" id="fieldRepeatWeekly"> ' +
                '                                 <ul class="list-unstyled"> ' +
                '                                     <li> ' +
                '                                         <input type="checkbox" name="chkRepeatWeekly" value="Mo" class="skin-flat-green" checked> ' +
                '                                         <label class="icheck-label form-label">Monday</label> ' +
                '                                     </li> ' +
                '                                     <li> ' +
                '                                         <input type="checkbox" name="chkRepeatWeekly" value="Tu" class="skin-flat-green" checked> ' +
                '                                         <label class="icheck-label form-label">Tuesday</label> ' +
                '                                     </li> ' +
                '                                     <li> ' +
                '                                         <input type="checkbox" name="chkRepeatWeekly" value="We" class="skin-flat-green" checked> ' +
                '                                         <label class="icheck-label form-label">Wednesday</label> ' +
                '                                     </li> ' +
                '                                     <li> ' +
                '                                         <input type="checkbox" name="chkRepeatWeekly" value="Th" class="skin-flat-green" checked> ' +
                '                                         <label class="icheck-label form-label">Thursday</label> ' +
                '                                     </li> ' +
                '                                     <li> ' +
                '                                         <input type="checkbox" name="chkRepeatWeekly" value="Fr" class="skin-flat-green" checked> ' +
                '                                         <label class="icheck-label form-label">Friday</label> ' +
                '                                     </li> ' +
                '                                     <li> ' +
                '                                         <input type="checkbox" name="chkRepeatWeekly" value="Sa" class="skin-flat-green" checked> ' +
                '                                         <label class="icheck-label form-label">Saturday</label> ' +
                '                                     </li> ' +
                '                                     <li> ' +
                '                                         <input type="checkbox" name="chkRepeatWeekly" value="Su" class="skin-flat-green" checked> ' +
                '                                         <label class="icheck-label form-label">Sunday</label> ' +
                '                                     </li> ' +
                '                                 </ul> ' +
                '                             </div> ' +
                '                         </div> ' +
                '                     </div> ' +

                                      //Repeat Monthly
                '                     <div id="containerRepeatMonthly" style="display:none;"></div> ' +

                                      //Repeat Quarterly
                '                     <div id="containerRepeatQuarterly" style="display:none;"> ' +
                '                         <div class="form-group row"> ' +
                '                             <div class="col-md-12"> ' +
                '                                 <div class="form-control" id="fieldRepeatQuarterlyMonths"> ' +
                '                                     <ul class="list-unstyled"> ' +
                '                                         <li> ' +
                '                                             <input type="checkbox" name="chkFieldRepeatQuarterlyMonths" value="1stM" class="skin-flat-green" checked> ' +
                '                                             <label class="icheck-label form-label">1st Month of Quarter (Jan, Apr, Jul, Oct)</label> ' +
                '                                         </li> ' +
                '                                         <li> ' +
                '                                             <input type="checkbox" name="chkFieldRepeatQuarterlyMonths" value="2stM" class="skin-flat-green" checked> ' +
                '                                             <label class="icheck-label form-label">2nd Month of Quarter (Feb, May, Aug, Nov)</label> ' +
                '                                         </li> ' +
                '                                         <li> ' +
                '                                             <input type="checkbox" name="chkFieldRepeatQuarterlyMonths" value="3stM" class="skin-flat-green" checked> ' +
                '                                             <label class="icheck-label form-label">3rd Month of Quarter (Mar, Jun, Sep, Dec)</label> ' +
                '                                         </li> ' +
                '                                     </ul> ' +
                '                                 </div> ' +
                '                             </div> ' +
                '                         </div> ' +
                '  ' +
                '                         <div id="containerFieldRepeatQuarterlyMonthVsBD"></div> ' +
                '                     </div> ' +

                                      //Repeat Yearly
                '                     <div id="containerRepeatYearly" style="display:none;"> ' +
                '                         <div class="form-group row"> ' +
                '                             <div class="col-md-12"> ' +
                '                                 <select class="form-control" id="fieldRepeatYearlySelectMonth"></select> ' +
                '                             </div> ' +
                '                         </div> ' +
                '                         <div id="containerFieldRepeatYearlyMonthVsBD"></div> ' +
                '                     </div> ' +
                '                 </div> ' +
                '                 <div class="col-md-4" style="display:none;"> ' +

                                      //Repeat End Config
                '                     <div class="form-group row" id="containerRepeatEnd"> ' +
                '                         <div class="col-md-12"> ' +
                '                             <div class="form-control" id="fieldRepeatEnd" > ' +
                '                                 <ul class="list-unstyled"> ' +
                '                                     <li> ' +
                '                                         <input type="radio" id="flat-radio-repeat-end-no-end" value="NoEnd" name="chkRepeatEnd" class="skin-flat-green" checked> ' +
                '                                         <label class="icheck-label form-label" for="flat-radio-repeat-end-no-end">No end date</label> ' +
                '                                     </li> ' +
                '                                     <li> ' +
                '                                         <input type="radio" id="flat-radio-repeat-end-after-occurrences" value="After" name="chkRepeatEnd" class="skin-flat-green"> ' +
                '                                         <label class="icheck-label form-label" for="flat-radio-repeat-end-after-occurrences">After <input value="1" style="width: 30px;" /> occurrences</label> ' +
                '                                     </li> ' +
                '                                     <li> ' +
                '                                         <input type="radio" id="flat-radio-repeat-end-date" name="chkRepeatEnd" value="On" class="skin-flat-green"> ' +
                '                                         <label class="icheck-label form-label" for="flat-radio-repeat-end-date">End on <input value="May 18, 2018" style="width: 120px;" /></label> ' +
                '                                     </li> ' +
                '                                 </ul> ' +
                '                             </div> ' +
                '                         </div> ' +
                '                     </div> ' +
                '                 </div> ' +
                '             </div> ' +
                '         </div> ' +
                '     </div> ' +
                      //End: Generate Frequency Code

                      //Start: Preview
                '     <div class="col-md-12"> ' +
                '         <h4 style="color: #004786;padding-bottom: 11px;">Preview</h4> ' +
                '         <div id="tblPreviewFrequency"></div> ' +
                '     </div> ' +
                      //End: Preview

                ' </div> ';

            return htmlFrequency;
        }

        function onReady($modal) {
            //Creacte Radio Buttons and Checkbox iCheck library
            _GLOBAL_SETTINGS.iCheck();

            //Create Field Business Days / Calendar Days for Repeat Montly
            _frequencyManager.createFieldCalendarBusinessDays($modal.find("#containerRepeatMonthly"));

            //Create Field Business Days / Calendar Days for Repeat Quarterly => Months
            _frequencyManager.createFieldCalendarBusinessDays($modal.find("#containerFieldRepeatQuarterlyMonthVsBD"));

            //Create Field Business Days / Calendar Days for Repeat Yearly => Months
            _frequencyManager.createFieldCalendarBusinessDays($modal.find("#containerFieldRepeatYearlyMonthVsBD"));

            //Generate Months Select Option, when user click in Yearly
            _frequencyManager.generateMonths("#fieldRepeatYearlySelectMonth");

            //On Change field Repeat
            function onChangeFieldRepeat() {
                var repeatValue = _frequencyManager.getRepeat();

                //Hide all
                $("#containerRepeatDaily").hide();
                $("#containerRepeatWeekly").hide();
                $("#containerRepeatMonthly").hide();
                $("#containerRepeatQuarterly").hide();
                $("#containerRepeatYearly").hide();

                //Show selected value
                switch (repeatValue) {
                    case "Daily":
                        $("#containerRepeatDaily").show();
                        break;

                    case "Weekly":
                        $("#containerRepeatWeekly").show();
                        break;

                    case "Monthly":
                        $("#containerRepeatMonthly").show();
                        break;

                    case "Quarterly":
                        $("#containerRepeatQuarterly").show();
                        break;

                    case "Yearly":
                        $("#containerRepeatYearly").show();
                        break;
                }

                // Refresh Code and Preview
                _frequencyManager.refreshData();
            }
            $modal.find("#fieldRepeat input").on('ifChecked', onChangeFieldRepeat);
            $modal.find("#fieldRepeat input").on('ifUnchecked', onChangeFieldRepeat);

            //On change Daily Options
            function onChangeDailyOptions() {
                // Refresh Code and Preview
                _frequencyManager.refreshData();
            }
            $modal.find("#fieldRepeatDaily input").on('ifChecked', onChangeDailyOptions);
            $modal.find("#fieldRepeatDaily input").on('ifUnchecked', onChangeDailyOptions);

            //On change Weekly Options
            function onChangeWeeklyOptions() {
                // Refresh Code and Preview
                _frequencyManager.refreshData();
            }
            $modal.find("#fieldRepeatWeekly input").on('ifChecked', onChangeWeeklyOptions);
            $modal.find("#fieldRepeatWeekly input").on('ifUnchecked', onChangeWeeklyOptions);

            //On change Monthly Options
            function onChangeMonthlyOptions() {
                // Refresh Code and Preview
                _frequencyManager.refreshData();
            }
            $modal.find("#containerRepeatMonthly .fieldCalendarDBsType input").on('ifChecked', onChangeMonthlyOptions);
            $modal.find("#containerRepeatMonthly .fieldCalendarDBsType input").on('ifUnchecked', onChangeMonthlyOptions);
            $modal.find("#containerRepeatMonthly .containerFieldBusinessDays input").on('ifChecked', onChangeMonthlyOptions);
            $modal.find("#containerRepeatMonthly .containerFieldBusinessDays input").on('ifUnchecked', onChangeMonthlyOptions);
            $modal.find("#containerRepeatMonthly .containerFieldCalendarDays input").on('ifChecked', onChangeMonthlyOptions);
            $modal.find("#containerRepeatMonthly .containerFieldCalendarDays input").on('ifUnchecked', onChangeMonthlyOptions);

            //On Change Quarterly Options
            function onChangeQuarterlyOptions() {
                // Refresh Code and Preview
                _frequencyManager.refreshData();
            }
            $modal.find("#fieldRepeatQuarterlyMonths input").on('ifChecked', onChangeMonthlyOptions);
            $modal.find("#fieldRepeatQuarterlyMonths input").on('ifUnchecked', onChangeMonthlyOptions);
            $modal.find("#containerFieldRepeatQuarterlyMonthVsBD .fieldCalendarDBsType input").on('ifChecked', onChangeMonthlyOptions);
            $modal.find("#containerFieldRepeatQuarterlyMonthVsBD .fieldCalendarDBsType input").on('ifUnchecked', onChangeMonthlyOptions);
            $modal.find("#containerFieldRepeatQuarterlyMonthVsBD .containerFieldBusinessDays input").on('ifChecked', onChangeMonthlyOptions);
            $modal.find("#containerFieldRepeatQuarterlyMonthVsBD .containerFieldBusinessDays input").on('ifUnchecked', onChangeMonthlyOptions);
            $modal.find("#containerFieldRepeatQuarterlyMonthVsBD .containerFieldCalendarDays input").on('ifChecked', onChangeMonthlyOptions);
            $modal.find("#containerFieldRepeatQuarterlyMonthVsBD .containerFieldCalendarDays input").on('ifUnchecked', onChangeMonthlyOptions);

            //On Change Yearly Options
            function onchangeYearlyOptions() {
                // Refresh Code and Preview
                _frequencyManager.refreshData();
            }
            $modal.find("#fieldRepeatYearlySelectMonth").change(_frequencyManager.refreshData);
            $modal.find("#containerFieldRepeatYearlyMonthVsBD .fieldCalendarDBsType input").on('ifChecked', onChangeMonthlyOptions);
            $modal.find("#containerFieldRepeatYearlyMonthVsBD .fieldCalendarDBsType input").on('ifUnchecked', onChangeMonthlyOptions);
            $modal.find("#containerFieldRepeatYearlyMonthVsBD .containerFieldBusinessDays input").on('ifChecked', onChangeMonthlyOptions);
            $modal.find("#containerFieldRepeatYearlyMonthVsBD .containerFieldBusinessDays input").on('ifUnchecked', onChangeMonthlyOptions);
            $modal.find("#containerFieldRepeatYearlyMonthVsBD .containerFieldCalendarDays input").on('ifChecked', onChangeMonthlyOptions);
            $modal.find("#containerFieldRepeatYearlyMonthVsBD .containerFieldCalendarDays input").on('ifUnchecked', onChangeMonthlyOptions);

            //Create Select Time Zone
            loadFrequencyTimeZone();

            //Create Select SLA Time
            loadFrequencySLATime();

            //Create Datepicker for Frequency Start Date and add Change Date event
            $modal.find("#frequencyStartDate_" + _frequencyManager.idModalFrequency).datepicker().on('changeDate', function (ev) {
                // Refresh Code and Preview
                _frequencyManager.refreshData();
            });

            //Load Max Date Corporate Calendar
            _frequencyManager.loadMaxDateCorporateCalendar();

            //If code exists
            if (options.frequencyCode) {
                _frequencyManager.loadFieldsByCode(options.frequencyCode);
            }

            // Refresh Code and Preview
            _frequencyManager.refreshData();

            function loadFrequencyTimeZone() {
                //Remove current options
                $modal.find(".selFrequencyTimeZone").contents().remove();

                //Find current offset of user
                var browserOffsetMinutes = moment().utcOffset();

                //Add Options
                for (var i = 0; i < _TIME_ZONES.length; i++) {
                    var objTemp = _TIME_ZONES[i];
                    $modal.find(".selFrequencyTimeZone").append($('<option>', {
                        value: objTemp.ID,
                        text: objTemp.TimeZone,
                        timeZone: objTemp.TimeZone,
                        utcOffset: objTemp.UTCOffset,
                        utcOffsetMinutes: objTemp.UTCOffsetMinutes,
                        selected: (objTemp.UTCOffsetMinutes == browserOffsetMinutes)
                    }));
                }

                //Refresh Preview on change
                $modal.find(".selFrequencyTimeZone").change(function () {
                    _frequencyManager.refreshData();
                });
            }

            function loadFrequencySLATime() {
                var timeList = [
                    { value: "00:00", text: "12:00 AM" }, { value: "00:30", text: "12:30 AM" },
                    { value: "01:00", text: "01:00 AM" }, { value: "01:30", text: "01:30 AM" },
                    { value: "02:00", text: "02:00 AM" }, { value: "02:30", text: "02:30 AM" },
                    { value: "03:00", text: "03:00 AM" }, { value: "03:30", text: "03:30 AM" },
                    { value: "04:00", text: "04:00 AM" }, { value: "04:30", text: "04:30 AM" },
                    { value: "05:00", text: "05:00 AM" }, { value: "05:30", text: "05:30 AM" },
                    { value: "06:00", text: "06:00 AM" }, { value: "06:30", text: "06:30 AM" },
                    { value: "07:00", text: "07:00 AM" }, { value: "07:30", text: "07:30 AM" },
                    { value: "08:00", text: "08:00 AM" }, { value: "08:30", text: "08:30 AM" },
                    { value: "09:00", text: "09:00 AM" }, { value: "09:30", text: "09:30 AM" },
                    { value: "10:00", text: "10:00 AM" }, { value: "10:30", text: "10:30 AM" },
                    { value: "11:00", text: "11:00 AM" }, { value: "11:30", text: "11:30 AM" },
                    { value: "12:00", text: "12:00 PM" }, { value: "12:30", text: "12:30 PM" },
                    { value: "13:00", text: "01:00 PM" }, { value: "13:30", text: "01:30 PM" },
                    { value: "14:00", text: "02:00 PM" }, { value: "14:30", text: "02:30 PM" },
                    { value: "15:00", text: "03:00 PM" }, { value: "15:30", text: "03:30 PM" },
                    { value: "16:00", text: "04:00 PM" }, { value: "16:30", text: "04:30 PM" },
                    { value: "17:00", text: "05:00 PM" }, { value: "17:30", text: "05:30 PM" },
                    { value: "18:00", text: "06:00 PM" }, { value: "18:30", text: "06:30 PM" },
                    { value: "19:00", text: "07:00 PM" }, { value: "19:30", text: "07:30 PM" },
                    { value: "20:00", text: "08:00 PM" }, { value: "20:30", text: "08:30 PM" },
                    { value: "21:00", text: "09:00 PM" }, { value: "21:30", text: "09:30 PM" },
                    { value: "22:00", text: "10:00 PM" }, { value: "22:30", text: "10:30 PM" },
                    { value: "23:00", text: "11:00 PM" }, { value: "23:30", text: "11:30 PM" }
                ];

                //Remove current options
                $modal.find(".selFrequencySLATime").contents().remove();

                //Add defaul option
                $modal.find(".selFrequencySLATime").append($('<option>', {
                    value: "23:59",
                    text: "-- No SLA Time --",
                    selected: true
                }));

                //Add Options
                for (var i = 0; i < timeList.length; i++) {
                    var objTemp = timeList[i];
                    $modal.find(".selFrequencySLATime").append($('<option>', {
                        value: objTemp.value,
                        text: objTemp.text
                    }));
                }

                //Refresh Preview on change
                $modal.find(".selFrequencySLATime").change(function () {
                    _frequencyManager.refreshData();
                });
            }
        }
    },

    loadFieldsByCode: function (pcode) {
        _showLoadingFullPage({
            id: "ActivityFrequency",
            msg: "Loading activity frequency...."
        });

        var cleanedCode = _frequencyManager.cleanCode(pcode);
        var timeZonePart = "";
        var SLATimePart = "";

        var splitData = pcode.split("~");
        if (splitData.length >= 3) {
            timeZonePart = splitData[0];
            SLATimePart = splitData[splitData.length - 1];
            if (SLATimePart == "NS") {
                SLATimePart = "23:59";
            }
        }

        var repeatValue = _frequencyManager.getRepeatValueByCode(pcode);
        var repeatOptions = _frequencyManager.getRepeatOptionByCode(pcode);
        var repeatType = _frequencyManager.getRepeatTypeByCode(pcode);

        //Set Activity Date Time Zone
        var objTimeZone = _findOneObjByProperty(_TIME_ZONES, "Abbreviation", timeZonePart);
        $("#selFrequencyTimeZone_" + _frequencyManager.idModalFrequency).val(objTimeZone.ID);

        //Set SLA Time
        $("#selFrequencySLATime_" + _frequencyManager.idModalFrequency).val(SLATimePart);

        //Set Frequency Start Date
        //$("#frequencyStartDate_" + _frequencyManager.idModalFrequency).datepicker('setDate', options.frequencyStartDate);

        //Set Repeat
        $("input[name='chkRepeat'][value='" + repeatValue + "']").iCheck('check');

        //Set Filters
        switch (repeatValue) {
            case "Daily":
                break;

            case "Weekly":
                break;

            case "Monthly":
                //Set Repeat Type
                $("#containerRepeatMonthly .fieldCalendarDBsType").find("input[value='" + repeatType + "']").iCheck('check');
                break;

            case "Quarterly":
                //Set Quarterly Options
                //Fix, issue with Quarterly freq CleanCode function to CleanedCode Variable
                if (_contains(cleanedCode, "1stM:")) {
                    $("#fieldRepeatQuarterlyMonths").find("input[value='1stM']").iCheck('check');
                }
                if (_contains(cleanedCode, "2stM:")) {
                    $("#fieldRepeatQuarterlyMonths").find("input[value='2stM']").iCheck('check');
                }
                if (_contains(cleanedCode, "3stM:")) {
                    $("#fieldRepeatQuarterlyMonths").find("input[value='3stM']").iCheck('check');
                }

                //Set Repeat Type
                $("#containerFieldRepeatQuarterlyMonthVsBD .fieldCalendarDBsType").find("input[value='" + repeatType + "']").iCheck('check');
                break;

            case "Yearly":
                //Set Month
                var strYearlyOptions = _replaceAll(repeatValue, "", cleanedCode);
                var selectedMonth = strYearlyOptions.substr(0, 4);
                $("#fieldRepeatYearlySelectMonth").val(selectedMonth);

                //Set Repeat Type
                $("#containerFieldRepeatYearlyMonthVsBD .fieldCalendarDBsType").find("input[value='" + repeatType + "']").iCheck('check');
                break;
        }

        //Set Repeat options
        for (var i = 0; i < repeatOptions.length; i++) {
            var repeatOptionValue = repeatOptions[i];

            switch (repeatValue) {
                case "Daily":
                    $("#fieldRepeatDaily").find("input[value='" + repeatOptionValue + "']").iCheck('check');
                    break;

                case "Weekly":
                    $("#fieldRepeatWeekly").find("input[value='" + repeatOptionValue + "']").iCheck('check');
                    break;

                case "Monthly":
                    if (repeatType == "BD") {
                        $("#containerRepeatMonthly .containerFieldBusinessDays").find("input[value='" + repeatOptionValue + "']").iCheck('check');
                    }

                    if (repeatType == "Days") {
                        $("#containerRepeatMonthly .containerFieldCalendarDays").find("input[value='" + repeatOptionValue + "']").iCheck('check');
                    }

                    break;

                case "Quarterly":
                    if (repeatType == "BD") {
                        $("#containerFieldRepeatQuarterlyMonthVsBD .containerFieldBusinessDays").find("input[value='" + repeatOptionValue + "']").iCheck('check');
                    }

                    if (repeatType == "Days") {
                        $("#containerFieldRepeatQuarterlyMonthVsBD .containerFieldCalendarDays").find("input[value='" + repeatOptionValue + "']").iCheck('check');
                    }
                    break;

                case "Yearly":
                    if (repeatType == "BD") {
                        $("#containerFieldRepeatYearlyMonthVsBD .containerFieldBusinessDays").find("input[value='" + repeatOptionValue + "']").iCheck('check');
                    }

                    if (repeatType == "Days") {
                        $("#containerFieldRepeatYearlyMonthVsBD .containerFieldCalendarDays").find("input[value='" + repeatOptionValue + "']").iCheck('check');
                    }
                    break;
            }
        }

        //On all call done
        _execOnAjaxComplete(function () {
            _hideLoadingFullPage({
                id: "ActivityFrequency"
            });
        });

    },

    isValidData: function () {
        var result = true;
        var type;
        var repeatValue = _frequencyManager.getRepeat();
        var inputSelectedOptions = [];

        //Show selected value
        switch (repeatValue) {
            case "Daily":
                return true;
                break;

            case "Weekly":
                inputSelectedOptions = $("#fieldRepeatWeekly").find("input:checked");
                break;

            case "Monthly":
                type = $("#containerRepeatMonthly .fieldCalendarDBsType").find("input:checked").val();
                if (type == "BD") {
                    inputSelectedOptions = $("#containerRepeatMonthly .containerFieldBusinessDays").find("input:checked");
                }
                if (type == "Days") {
                    inputSelectedOptions = $("#containerRepeatMonthly .containerFieldCalendarDays").find("input:checked");
                }
                break;

            case "Quarterly":
                var inputQuarterMonthsList = $("#fieldRepeatQuarterlyMonths").find("input:checked");
                if (inputQuarterMonthsList.length > 0) {
                    type = $("#containerFieldRepeatQuarterlyMonthVsBD .fieldCalendarDBsType").find("input:checked").val();
                    if (type == "BD") {
                        inputSelectedOptions = $("#containerFieldRepeatQuarterlyMonthVsBD .containerFieldBusinessDays").find("input:checked");
                    }
                    if (type == "Days") {
                        inputSelectedOptions = $("#containerFieldRepeatQuarterlyMonthVsBD .containerFieldCalendarDays").find("input:checked");
                    }
                }
                break;

            case "Yearly":
                type = $("#containerFieldRepeatYearlyMonthVsBD .fieldCalendarDBsType").find("input:checked").val();
                if (type == "BD") {
                    inputSelectedOptions = $("#containerFieldRepeatYearlyMonthVsBD .containerFieldBusinessDays").find("input:checked");
                }
                if (type == "Days") {
                    inputSelectedOptions = $("#containerFieldRepeatYearlyMonthVsBD .containerFieldCalendarDays").find("input:checked");
                }
                break;
        }

        if (inputSelectedOptions.length == 0) {
            result = false;
        }

        return result;
    },

    generateFrequencyYearDatesInDB: function (customGenerateOptions) {

        //Default Options.
        var generateOptions = {
            yearToGenerate: moment().utc().format("YYYY"),
            frequencyToGenerateList: [
                // [ID],
                // [Code],
			    // [Frequency],
			    // [Type],
			    // [LastYearGenerated],
			    // [CreatedBy],
			    // [CreatedDate] (2018-12-01)
            ],
            onSuccess: null
        };

        //Merge default options
        $.extend(true, generateOptions, customGenerateOptions);

        var yearToCheck = generateOptions.yearToGenerate;
        var frequencyToGenerateList = generateOptions.frequencyToGenerateList;
        var selectedFrequency = _findOneObjByProperty(frequencyToGenerateList, "Code", generateOptions.frequencyCode);
        var frequencysToSave = [];
        var objFrequencyReady = {};
        var isGoingToGeneratedDBDates = false;

        //Check all frequency that needs to generate dates
        for (var i = 0; i < frequencyToGenerateList.length; i++) {
            if (needToGenerate(frequencyToGenerateList[i])) {
                objFrequencyReady[frequencyToGenerateList[i].ID] = false;
                isGoingToGeneratedDBDates = true;
            }
        }

        //Generate Dates
        if (isGoingToGeneratedDBDates) {
            for (var i = 0; i < frequencyToGenerateList.length; i++) {
                var objFrequency = frequencyToGenerateList[i];

                //console.log(objFrequency.Code + " Procesing...");

                if (needToGenerate(objFrequency)) {
                    _frequencyManager.generateFrequencyDates({
                        yearToGenerate: yearToCheck,
                        idFrequency: objFrequency.ID,
                        frequencyCode: objFrequency.Code,
                        onSuccess: function (resultList, typeDaysOrBDs, idFrequency, frequencyCode) {

                            for (var j = 0; j < resultList.length; j++) {
                                frequencysToSave.push({
                                    IDR: idFrequency,
                                    D: resultList[j].UTCDateTime,
                                    Y: resultList[j].Year,
                                    M: resultList[j].MonthNumber,
                                    N: resultList[j].BD.replace("BD", "")
                                });
                            }
                            objFrequencyReady[idFrequency] = true;

                            //console.log(objFrequency.Code + " Done!");

                            saveFrequencysToDB();
                        }
                    });
                }
            }
        } else {
            if (generateOptions.onSuccess) {
                generateOptions.onSuccess(selectedFrequency);
            }
        }

        //Save frequencys
        function saveFrequencysToDB() {
            var keys = Object.keys(objFrequencyReady);
            var isReadyToSave = true;
            for (var i = 0; i < keys.length; i++) {
                if (objFrequencyReady[keys[i]] == false) {
                    isReadyToSave = false;
                    break;
                }
            }

            //Save Data
            if (isReadyToSave) {
                //console.log(frequencysToSave, "Saving data...");

                if (frequencysToSave && frequencysToSave.length > 0) {
                    _callServer({
                        loadingMsgType: "topBar",
                        loadingMsg: "Saving Frequency Dates...",
                        url: '/ProcessControlTool/UploadFrequency',
                        data: {
                            'pyearGenerated': yearToCheck,
                            'pjsonData': JSON.stringify(frequencysToSave)
                        },
                        type: "post",
                        success: function (response) {
                            if (generateOptions.onSuccess) {
                                generateOptions.onSuccess(selectedFrequency);
                            }

                            //Check if file exist
                            //_showAlert({
                            //    id: "SaveFrequency",
                            //    type: 'success',
                            //    content: "Recurrense was saved and code was copied succesfully to clipboard.",
                            //    animateScrollTop: true
                            //});
                        }
                    });
                } else {
                    if (generateOptions.onSuccess) {
                        generateOptions.onSuccess(selectedFrequency);
                    }
                }
            }

            //console.log(objFrequencyReady);
        }

        //Check if frequency needs to generate dates
        function needToGenerate(objFrequency) {
            //var result = false;

            //if (parseInt(objFrequency.LastYearGenerated) < parseInt(yearToCheck)) {
            //    result = true;
            //}

            //return result;
            return true;
        }

    },

    generateFrequencyDatesInDB: function (customGenerateOptions) {

        //Default Options.
        var generateOptions = {
            frequencyCode: "",
            onSuccess: null
        };

        //Merge default options
        $.extend(true, generateOptions, customGenerateOptions);

        var yearToCheck = moment().utc().format("YYYY");

        _callProcedure({
            loadingMsgType: "topBar",
            loadingMsg: "Check frequency '" + generateOptions.frequencyCode + "'...",
            name: "[dbo].[spPCTManageFrequency]",
            params: [
                { "Name": "@Action", "Value": "New" },
                { "Name": "@Code", "Value": generateOptions.frequencyCode },
                { "Name": "@Frequency", "Value": _frequencyManager.getRepeatValueByCode(generateOptions.frequencyCode) },
                { "Name": "@Type", "Value": _frequencyManager.getRepeatTypeByCode(generateOptions.frequencyCode) },
                { "Name": "@YearToCheck", "Value": yearToCheck },
                { "Name": "@SessionSOEID", "Value": _getSOEID() },
            ],
            success: {

                //When a new code is created database return list of frequency that needs to generate
                fn: function (frequencyToGenerateList) {
                    var selectedFrequency = _findOneObjByProperty(frequencyToGenerateList, "Code", generateOptions.frequencyCode);
                    var frequencysToSave = [];
                    var objFrequencyReady = {};
                    var isGoingToGeneratedDBDates = false;

                    //Check all frequency that needs to generate dates
                    for (var i = 0; i < frequencyToGenerateList.length; i++) {
                        if (needToGenerate(frequencyToGenerateList[i])) {
                            objFrequencyReady[frequencyToGenerateList[i].ID] = false;
                            isGoingToGeneratedDBDates = true;
                        }
                    }

                    //Generate Dates
                    if (isGoingToGeneratedDBDates) {
                        for (var i = 0; i < frequencyToGenerateList.length; i++) {
                            var objFrequency = frequencyToGenerateList[i];

                            //console.log(objFrequency.Code + " Procesing...");

                            if (needToGenerate(objFrequency)) {
                                _frequencyManager.generateFrequencyDates({
                                    yearToGenerate: yearToCheck,
                                    idFrequency: objFrequency.ID,
                                    frequencyCode: objFrequency.Code,
                                    onSuccess: function (resultList, typeDaysOrBDs, idFrequency, frequencyCode) {

                                        for (var j = 0; j < resultList.length; j++) {
                                            frequencysToSave.push({
                                                IDR: idFrequency,
                                                D: resultList[j].UTCDateTime,
                                                Y: resultList[j].Year,
                                                M: resultList[j].MonthNumber,
                                                N: resultList[j].BD.replace("BD", "")
                                            });
                                        }
                                        objFrequencyReady[idFrequency] = true;

                                        //console.log(objFrequency.Code + " Done!");

                                        saveFrequencysToDB();
                                    }
                                });
                            }
                        }
                    } else {
                        if (generateOptions.onSuccess) {
                            generateOptions.onSuccess(selectedFrequency);
                        }
                    }

                    //Save frequencys
                    function saveFrequencysToDB() {
                        var keys = Object.keys(objFrequencyReady);
                        var isReadyToSave = true;
                        for (var i = 0; i < keys.length; i++) {
                            if (objFrequencyReady[keys[i]] == false) {
                                isReadyToSave = false;
                                break;
                            }
                        }

                        //Save Data
                        if (isReadyToSave) {
                            //console.log(frequencysToSave, "Saving data...");

                            if (frequencysToSave && frequencysToSave.length > 0) {
                                _callServer({
                                    loadingMsgType: "fullLoading",
                                    loadingMsg: "Saving Frequency Dates...",
                                    url: '/ProcessControlTool/UploadFrequency',
                                    data: {
                                        'pyearGenerated': yearToCheck,
                                        'pjsonData': JSON.stringify(frequencysToSave)
                                    },
                                    type: "post",
                                    success: function (response) {
                                        if (generateOptions.onSuccess) {
                                            generateOptions.onSuccess(selectedFrequency);
                                        }

                                        //Check if file exist
                                        //_showAlert({
                                        //    id: "SaveFrequency",
                                        //    type: 'success',
                                        //    content: "Recurrense was saved and code was copied succesfully to clipboard.",
                                        //    animateScrollTop: true
                                        //});
                                    }
                                });
                            } else {
                                if (generateOptions.onSuccess) {
                                    generateOptions.onSuccess(selectedFrequency);
                                }
                            }
                        }

                        //console.log(objFrequencyReady);
                    }

                    //Check if frequency needs to generate dates
                    function needToGenerate(objFrequency) {
                        var result = false;

                        if (parseInt(objFrequency.LastYearGenerated) < parseInt(yearToCheck)) {
                            result = true;
                        }

                        return result;
                    }
                }
            }
        });

    },

    generateFrequencyDates: function (customOptions) {

        //Default Options.
        var options = {
            idFrequency: 0,
            frequencyCode: "",
            dateFormat: "YYYY-MM-DD HH:mm",
            //occurrencesToGenerate: 400,
            yearToGenerate: moment().format("YYYY"),
            slaTime: "",
            onSuccess: null
        };

        //Hacer un merge con las opciones que nos enviaron y las default.
        $.extend(true, options, customOptions);

        //Set default value
        if (!options.frequencyCode) {
            options.frequencyCode = _frequencyManager.generateFrequencyCode();
        }

        //Set SLATime
        options.slaTime = getSLATime();

        var repeatValue = _frequencyManager.getRepeatValueByCode(options.frequencyCode);
        var momentStartDate = moment(options.yearToGenerate + "-01-01 " + options.slaTime);
        var momentEndDate = moment(options.yearToGenerate + "-12-31 " + options.slaTime);

        //if (maxDateCorporateCalendar) {
        //    momentEndDate = moment(maxDateCorporateCalendar);
        //} else {
        //    momentEndDate = moment(options.yearToGenerate + "-12-31");
        //}

        var dateResultList = [];

        //Show selected value
        switch (repeatValue) {
            case "Daily":
                getDaily();
                break;

            case "Weekly":
                getWeekly();
                break;

            case "Monthly":
                getMonthly();
                break;

            case "Quarterly":
                getQuarterly();
                break;

            case "Yearly":
                getYearly();
                break;
        }

        function getDaily() {
            var repeatOption = _frequencyManager.getRepeatOptionByCode(options.frequencyCode);
            var frequency;
            var momentFrequencyList;

            if (repeatOption) {
                if (repeatOption == "AllDays") {
                    frequency = moment(momentStartDate).recur(momentEndDate).every(1).day();
                }

                if (repeatOption == "Workdays") {
                    frequency = moment(momentStartDate).recur(momentEndDate).every(["Mo", "Tu", "We", "Th", "Fr"]).daysOfWeek();
                }

                momentFrequencyList = frequency.all();
                //momentFrequencyList = frequency.next(options.occurrencesToGenerate);
                for (var i = 0; i < momentFrequencyList.length; i++) {
                    addItem(momentFrequencyList[i], "Days");
                }
            }

            //Return Result
            options.onSuccess(dateResultList, "Days", options.idFrequency, options.frequencyCode);
        }

        function getWeekly() {
            var selectedDayList = _frequencyManager.getRepeatOptionByCode(options.frequencyCode);
            var frequency;
            var momentFrequencyList;

            if (selectedDayList.length > 0) {
                frequency = moment(momentStartDate).recur(momentEndDate).every(selectedDayList).daysOfWeek();

                momentFrequencyList = frequency.all();
                //momentFrequencyList = frequency.next(options.occurrencesToGenerate);
                for (var i = 0; i < momentFrequencyList.length; i++) {
                    addItem(momentFrequencyList[i], "Days");
                }
            }

            //Return Result
            options.onSuccess(dateResultList, "Days", options.idFrequency, options.frequencyCode);
        }

        function getMonthly() {
            var type = _frequencyManager.getRepeatTypeByCode(options.frequencyCode);
            var selectedDayList = _frequencyManager.getRepeatOptionByCode(options.frequencyCode);
            var inputDayList;
            var frequency;
            var momentFrequencyList;

            if (type == "BD") {
                if (selectedDayList.length > 0) {
                    //Sql for BDs
                    var sqlBDs =
                        "SELECT \n" +
                        "    [Year], \n" +
                        "    [Month], \n" +
                        "    [BusinessDay], \n" +
                        "    CONVERT(VARCHAR(10), [WorkDay], 120) AS [WorkDay] \n" +
                        "FROM \n" +
                        "    [Automation].[dbo].[tblCorporateCalendar] \n" +
                        "WHERE \n" +
                        "    [Year] = " + options.yearToGenerate + " AND \n" +
                        "    [BusinessDay] IN (" + selectedDayList.join(",") + ") ";

                    //Get Business Days Result
                    _callServer({
                        loadingMsgType: "topBar",
                        loadingMsg: "Getting Business Days...",
                        url: '/Ajax/ExecQuery',
                        data: { 'pjsonSql': _toJSON(sqlBDs) },
                        type: "POST",
                        success: function (resultList) {
                            for (var i = 0; i < resultList.length; i++) {
                                addItem(resultList[i], type);
                            }

                            //Return Result
                            options.onSuccess(dateResultList, "BD", options.idFrequency, options.frequencyCode);
                        }
                    });

                } else {
                    //Return Result
                    options.onSuccess(dateResultList, "BD", options.idFrequency, options.frequencyCode);
                }
            }

            if (type == "Days") {
                if (selectedDayList.length > 0) {
                    frequency = moment(momentStartDate).recur(momentEndDate).every(selectedDayList).daysOfMonth();

                    momentFrequencyList = frequency.all();
                    //momentFrequencyList = frequency.next(options.occurrencesToGenerate);
                    for (var i = 0; i < momentFrequencyList.length; i++) {
                        addItem(momentFrequencyList[i], type);
                    }
                }

                //Return Result
                options.onSuccess(dateResultList, "Days", options.idFrequency, options.frequencyCode);
            }
        }

        function getQuarterly() {
            var type = _frequencyManager.getRepeatTypeByCode(options.frequencyCode);
            var selectedMonthList = [];
            var selectedDayList = [];
            var frequency;
            var momentFrequencyList;

            //Get Months
            //----------------------------------------------------------
            //1st Month of Quarter (Jan, Apr, Jul, Oct)
            if (_contains(options.frequencyCode, "1stM")) {
                selectedMonthList.push(1);
                selectedMonthList.push(4);
                selectedMonthList.push(7);
                selectedMonthList.push(10);
            }

            //2nd Month of Quarter (Feb, May, Aug, Nov)
            if (_contains(options.frequencyCode, "2stM")) {
                selectedMonthList.push(2);
                selectedMonthList.push(5);
                selectedMonthList.push(8);
                selectedMonthList.push(11);
            }

            //3rd Month of Quarter (Mar, Jun, Sep, Dec)
            if (_contains(options.frequencyCode, "3stM")) {
                selectedMonthList.push(3);
                selectedMonthList.push(6);
                selectedMonthList.push(9);
                selectedMonthList.push(12);
            }

            if (selectedMonthList.length > 0) {

                if (type == "BD") {
                    selectedDayList = _frequencyManager.getRepeatOptionByCode(options.frequencyCode);

                    if (selectedDayList.length > 0) {
                        //Sql for BDs
                        var sqlBDs =
                            "SELECT \n" +
                            "    [Year], \n" +
                            "    [Month], \n" +
                            "    [BusinessDay], \n" +
                            "    CONVERT(VARCHAR(10), [WorkDay], 120) AS [WorkDay] \n" +
                            "FROM \n" +
                            "    [Automation].[dbo].[tblCorporateCalendar] \n" +
                            "WHERE \n" +
                            "    [Year] = " + options.yearToGenerate + " AND \n" +
                            "    [Month] IN (" + selectedMonthList.join(",") + ") AND \n" +
                            "    [BusinessDay] IN (" + selectedDayList.join(",") + ") ";

                        //Get Business Days Result
                        _callServer({
                            loadingMsgType: "topBar",
                            loadingMsg: "Getting Business Days...",
                            url: '/Ajax/ExecQuery',
                            data: { 'pjsonSql': _toJSON(sqlBDs) },
                            type: "POST",
                            success: function (resultList) {
                                for (var i = 0; i < resultList.length; i++) {
                                    addItem(resultList[i], type);
                                }

                                //Return Result
                                options.onSuccess(dateResultList, "BD", options.idFrequency, options.frequencyCode);
                            }
                        });

                    } else {
                        //Return Result
                        options.onSuccess(dateResultList, "BD", options.idFrequency, options.frequencyCode);
                    }
                }

                if (type == "Days") {
                    selectedDayList = _frequencyManager.getRepeatOptionByCode(options.frequencyCode);

                    if (selectedDayList.length > 0) {
                        //Fix January = 1 but Moment January = 0
                        var customSelectedMonthList = [];
                        for (var i = 0; i < selectedMonthList.length; i++) {
                            customSelectedMonthList.push(selectedMonthList[i] - 1);
                        }

                        frequency = moment(momentStartDate).recur(momentEndDate)
                                        .every(selectedDayList).daysOfMonth()
                                        .every(customSelectedMonthList).monthsOfYear();

                        momentFrequencyList = frequency.all();
                        //momentFrequencyList = frequency.next(options.occurrencesToGenerate);
                        for (var i = 0; i < momentFrequencyList.length; i++) {
                            addItem(momentFrequencyList[i], type);
                        }
                    }

                    //Return Result
                    options.onSuccess(dateResultList, "Days", options.idFrequency, options.frequencyCode);
                }

            } else {
                //Return Result
                options.onSuccess(dateResultList, "Days", options.idFrequency, options.frequencyCode);
            }

        }

        function getYearly() {
            var frequencyCode = _frequencyManager.cleanCode(options.frequencyCode);
            var type = _frequencyManager.getRepeatTypeByCode(frequencyCode);
            var selectedMonthList = [];
            var selectedDayList = [];
            var frequency;
            var momentFrequencyList;

            //moment("12-25-1995", "MM-DD-YYYY");
            //Set Month of Year
            var strYearlyCode = _replaceAll(repeatValue, "", frequencyCode);
            var selectedMonthOfYear = strYearlyCode.substr(0, 3);
            var objSelMonthMoment = moment(options.yearToGenerate + "-" + selectedMonthOfYear + "-01", "YYYY-MMM-DD");
            selectedMonthList.push(objSelMonthMoment.format("M"));

            if (type == "BD") {
                selectedDayList = _frequencyManager.getRepeatOptionByCode(frequencyCode);

                if (selectedDayList.length > 0) {
                    //Sql for BDs
                    var sqlBDs =
                        "SELECT \n" +
                        "    [Year], \n" +
                        "    [Month], \n" +
                        "    [BusinessDay], \n" +
                        "    CONVERT(VARCHAR(10), [WorkDay], 120) AS [WorkDay] \n" +
                        "FROM \n" +
                        "    [Automation].[dbo].[tblCorporateCalendar] \n" +
                        "WHERE \n" +
                        "    [Year] = " + options.yearToGenerate + " AND \n" +
                        "    [Month] IN (" + selectedMonthList.join(",") + ") AND \n" +
                        "    [BusinessDay] IN (" + selectedDayList.join(",") + ") ";

                    //Get Business Days Result
                    _callServer({
                        loadingMsgType: "topBar",
                        loadingMsg: "Getting Business Days...",
                        url: '/Ajax/ExecQuery',
                        data: { 'pjsonSql': _toJSON(sqlBDs) },
                        type: "POST",
                        success: function (resultList) {
                            for (var i = 0; i < resultList.length; i++) {
                                addItem(resultList[i], type);
                            }

                            //Return Result
                            options.onSuccess(dateResultList, "BD", options.idFrequency, options.frequencyCode);
                        }
                    });
                } else {
                    //Return Result
                    options.onSuccess(dateResultList, "BD", options.idFrequency, options.frequencyCode);
                }
            }

            if (type == "Days") {
                selectedDayList = _frequencyManager.getRepeatOptionByCode(options.frequencyCode);

                if (selectedDayList.length > 0) {
                    //Fix January = 1 but Moment January = 0
                    var customSelectedMonthList = [];
                    for (var i = 0; i < selectedMonthList.length; i++) {
                        customSelectedMonthList.push(selectedMonthList[i] - 1);
                    }

                    frequency = moment(momentStartDate).recur(momentEndDate)
                                    .every(selectedDayList).daysOfMonth()
                                    .every(customSelectedMonthList).monthsOfYear();

                    momentFrequencyList = frequency.all();
                    //momentFrequencyList = frequency.next(options.occurrencesToGenerate);
                    for (var i = 0; i < momentFrequencyList.length; i++) {
                        addItem(momentFrequencyList[i], type);
                    }
                }

                //Return Result
                options.onSuccess(dateResultList, "Days", options.idFrequency, options.frequencyCode);
            }
        }

        function getTimeZoneCode() {
            var timeZoneCode = "";
            var splitData = options.frequencyCode.split("~");
            if (splitData.length >= 3) {
                timeZoneCode = splitData[0];
            } else {
                alert('Frequency code ' + pcode + ' don\'t have time zone');
            }

            return timeZoneCode;
        }

        function getSLATime() {
            var slaTime = "";
            var splitData = options.frequencyCode.split("~");
            if (splitData.length >= 3) {
                slaTime = splitData[splitData.length - 1];
                if (slaTime == "NS") {
                    slaTime = "23:59";
                }
            } else {
                alert('Frequency code ' + options.frequencyCode + ' don\'t have SLA Time');
            }

            return slaTime;
        }

        function getUTCMomentByTimeZone(strDateTimeSLA) {
            var objTimeZone = _findOneObjByProperty(_TIME_ZONES, "Abbreviation", getTimeZoneCode());

            //Return UTC by timezone
            return moment.utc(strDateTimeSLA).add((parseInt(objTimeZone.UTCOffsetMinutes) * -1), 'minutes');
        }

        function addItem(objFrequencyDate, type) {
            var strDateTimeSLA;

            if (type == "Days") {
                //Fix moment frequency get _isUTC = true;
                strDateTimeSLA = objFrequencyDate.format("YYYY-MM-DD") + " " + options.slaTime;
                var objDateMomment = moment(strDateTimeSLA);

                dateResultList.push({
                    Year: objDateMomment.format("YYYY"),
                    MonthNumber: objDateMomment.format("M"),
                    MonthName: objDateMomment.format("MMMM"),
                    MonthCustomFormat: objDateMomment.format("MM - MMMM"),
                    //ObjDate: new Date(objDateMomment.year(), objDateMomment.month(), objDateMomment.date()),
                    LocalDateMoment: objDateMomment,
                    Date: objDateMomment.format(options.dateFormat),
                    DateFormat: objDateMomment.format(options.dateFormat),
                    CostaRicaDateTime: getUTCMomentByTimeZone(strDateTimeSLA).add(_frequencyManager.utcOffsetMinCostaRica, 'minutes').format(options.dateFormat),
                    TampaDateTime: getUTCMomentByTimeZone(strDateTimeSLA).add(_frequencyManager.utcOffsetMinTampa, 'minutes').format(options.dateFormat),
                    MumbaiDateTime: getUTCMomentByTimeZone(strDateTimeSLA).add(_frequencyManager.utcOffsetMinMumbai, 'minutes').format(options.dateFormat),
                    ManilaDateTime: getUTCMomentByTimeZone(strDateTimeSLA).add(_frequencyManager.utcOffsetMinManila, 'minutes').format(options.dateFormat),
                    UTCDateTime: getUTCMomentByTimeZone(strDateTimeSLA).format(options.dateFormat),
                    BD: "0"
                });
            }

            if (type == "BD") {
                //Fix moment frequency get _isUTC = true;
                strDateTimeSLA = objFrequencyDate.WorkDay + " " + options.slaTime;

                var objBD = objFrequencyDate;
                var objDBMoment = moment(strDateTimeSLA);
                var objBDMontMoment = moment(objBD.Year + "-" + objBD.Month + "-01", "YYYY-M-DD");

                dateResultList.push({
                    Year: objBD.Year,
                    MonthNumber: objBD.Month,
                    MonthName: objBDMontMoment.format("MMMM"),
                    MonthCustomFormat: objBDMontMoment.format("MM - MMMM"),
                    //ObjDate: objDBMoment.toDate(),
                    LocalDateMoment: objDBMoment,
                    Date: objDBMoment.format(options.dateFormat),
                    DateFormat: objDBMoment.format(options.dateFormat),
                    CostaRicaDateTime: getUTCMomentByTimeZone(strDateTimeSLA).add(_frequencyManager.utcOffsetMinCostaRica, 'minutes').format(options.dateFormat),
                    TampaDateTime: getUTCMomentByTimeZone(strDateTimeSLA).add(_frequencyManager.utcOffsetMinTampa, 'minutes').format(options.dateFormat),
                    MumbaiDateTime: getUTCMomentByTimeZone(strDateTimeSLA).add(_frequencyManager.utcOffsetMinMumbai, 'minutes').format(options.dateFormat),
                    ManilaDateTime: getUTCMomentByTimeZone(strDateTimeSLA).add(_frequencyManager.utcOffsetMinManila, 'minutes').format(options.dateFormat),
                    UTCDateTime: getUTCMomentByTimeZone(strDateTimeSLA).format(options.dateFormat),
                    BD: "BD" + objBD.BusinessDay
                });
            }
        }
    },

    createFieldCalendarBusinessDays: function ($element) {
        var uniqueId = _createCustomID();
        var pluginHtml =
        '<div class="form-group row"> ' +
        '    <div class="col-md-12"> ' +
        '        <div class="form-control fieldCalendarDBsType"> ' +
        '            <ul class="list-unstyled"> ' +
        '                <li class="col-md-6"> ' +
        '                    <input type="radio" value="BD" name="chkMonthDaysBDsType_' + uniqueId + '" class="skin-flat-green" checked> ' +
        '                    <label class="icheck-label form-label">Business Days</label> ' +
        '                </li> ' +
        '                <li class="col-md-6"> ' +
        '                    <input type="radio" value="Days" name="chkMonthDaysBDsType_' + uniqueId + '" class="skin-flat-green" > ' +
        '                    <label class="icheck-label form-label">Calendar Days</label> ' +
        '                </li> ' +
        '                <div style="clear:both;"></div> ' +
        '            </ul> ' +
        '        </div> ' +
        '    </div> ' +
        '</div> ' +

        '<div class="form-group row containerFieldBusinessDays"> ' +
        '    <div class="col-md-12"> ' +
        '        <div class="form-control fieldBusinessDays"> ' +
        '            <ul class="list-unstyled"></ul> ' +
        '        </div> ' +
        '    </div> ' +
        '</div> ' +

        '<div class="form-group row containerFieldCalendarDays" style="display:none;"> ' +
        '    <div class="col-md-12"> ' +
        '        <div class="form-control fieldCalendarDays"> ' +
        '            <ul class="list-unstyled"></ul> ' +
        '        </div> ' +
        '    </div> ' +
        '</div> ';

        $element.append($(pluginHtml));

        //On Change Business Day / Calendar Days
        var $fieldCalendarBDsType = $element.find(".fieldCalendarDBsType");
        var $containerFieldBusinessDays = $element.find(".containerFieldBusinessDays");
        var $containerFieldCalendarDays = $element.find(".containerFieldCalendarDays");

        //Generate Business Days Checkboxs
        var $fieldBusinessDays = $element.find(".fieldBusinessDays ul");
        _frequencyManager.generateBusinessDays($fieldBusinessDays);

        //Generate Calendar Days Checkboxs
        var $fieldCalendarDays = $element.find(".fieldCalendarDays ul");
        _frequencyManager.generateCalendarDays($fieldCalendarDays);

        //Creacte Radio Buttons and Checkbox iCheck library
        _GLOBAL_SETTINGS.iCheck();
        $fieldCalendarBDsType.find("input").on('ifChecked', function () {
            var fieldValue = $fieldCalendarBDsType.find("input:checked").val();

            //Hide all
            $containerFieldBusinessDays.hide();
            $containerFieldCalendarDays.hide();

            //Show selected value
            switch (fieldValue) {
                case "BD":
                    $containerFieldBusinessDays.show();
                    break;

                case "Days":
                    $containerFieldCalendarDays.show();
                    break;
            }
        });

    },

    generateBusinessDays: function ($element) {

        for (var i = 0; i < _frequencyManager.businessDayList.length; i++) {
            var tempBD = _frequencyManager.businessDayList[i];
            var $li = $(
                '<li class="col-md-4"> ' +
                '   <input type="checkbox" name="chkBusinessDays" value="' + _replaceAll("BD", "", tempBD) + '" class="skin-flat-green"> ' +
                '   <label class="icheck-label form-label">' + tempBD + '</label> ' +
                '</li>'
            );

            $element.append($li);
        }

        $element.append($('<div style="clear:both;"></div>'));
    },

    generateCalendarDays: function ($element) {

        for (var i = 0; i < _frequencyManager.monthDayList.length; i++) {
            var tempDay = _frequencyManager.monthDayList[i];
            var $li = $(
                '<li class="col-md-3"> ' +
                '   <input type="checkbox" name="chkCalendarDays" value="' + tempDay + '" class="skin-flat-green"> ' +
                '   <label class="icheck-label form-label" >' + tempDay + '</label> ' +
                '</li>'
            );

            $element.append($li);
        }

        $element.append($('<div style="clear:both;"></div>'));
    },

    generateMonths: function (pselector) {
        var $select = $(pselector);

        //$(idSelect).find("option").remove();
        //$select.append($('<option>', {
        //    value: "0",
        //    text: "-- Select a User --"
        //}));

        //Load options in select
        for (var i = 0; i < _frequencyManager.monthList.length; i++) {
            var monthName = _frequencyManager.monthList[i];

            $select.append($('<option>', {
                value: monthName.substr(0, 3),
                text: monthName
            }));
        }
    },

    getRepeatValueByCode: function (pcode) {
        pcode = _frequencyManager.cleanCode(pcode);
        var repeatValue = "";

        // Calculate Repeat Value
        if (_contains(pcode, "Daily")) {
            repeatValue = "Daily";
        }

        if (_contains(pcode, "Weekly")) {
            repeatValue = "Weekly";
        }

        if (_contains(pcode, "Monthly")) {
            repeatValue = "Monthly";
        }

        if (_contains(pcode, "Quarterly")) {
            repeatValue = "Quarterly";
        }

        if (_contains(pcode, "Yearly")) {
            repeatValue = "Yearly";
        }

        return repeatValue;
    },

    getRepeatOptionByCode: function (pcode) {
        pcode = _frequencyManager.cleanCode(pcode);
        var repeatValue = _frequencyManager.getRepeatValueByCode(pcode);
        var repeatOption;

        //Show selected value
        switch (repeatValue) {
            case "Daily":
                if (_contains(pcode, "AllDays")) {
                    repeatOption = "AllDays";
                }

                if (_contains(pcode, "Workdays")) {
                    repeatOption = "Workdays";
                }
                break;

            case "Weekly":
                var strWeekDays = _replaceAll(repeatValue, "", pcode);
                repeatOption = strWeekDays.match(/.{1,2}/g);
                break;

            case "Monthly":
                var typeMonthly = _frequencyManager.getRepeatTypeByCode(pcode);
                if (typeMonthly) {
                    var strMonthlyOptions = _replaceAll(repeatValue, "", pcode);
                    strMonthlyOptions = _replaceAll((typeMonthly + "."), "", strMonthlyOptions);
                    strMonthlyOptions = _replaceAll(typeMonthly, "", strMonthlyOptions);
                    if (strMonthlyOptions) {
                        repeatOption = strMonthlyOptions.split(".");
                    } else {
                        repeatOption = [];
                    }
                }
                break;

            case "Quarterly":
                var typeQuarterly = _frequencyManager.getRepeatTypeByCode(pcode);
                if (typeQuarterly) {
                    var strQuarterlyOptions = _replaceAll(repeatValue, "", pcode);
                    strQuarterlyOptions = _replaceAll("1stM:", "", strQuarterlyOptions);
                    strQuarterlyOptions = _replaceAll("2stM:", "", strQuarterlyOptions);
                    strQuarterlyOptions = _replaceAll("3stM:", "", strQuarterlyOptions);
                    strQuarterlyOptions = _replaceAll((typeQuarterly + "."), "", strQuarterlyOptions);
                    strQuarterlyOptions = _replaceAll(typeQuarterly, "", strQuarterlyOptions);
                    if (strQuarterlyOptions) {
                        repeatOption = strQuarterlyOptions.split(".");
                    } else {
                        repeatOption = [];
                    }
                }
                break;

            case "Yearly":
                var typeYearly = _frequencyManager.getRepeatTypeByCode(pcode);
                if (typeYearly) {
                    var strYearlyOptions = _replaceAll(repeatValue, "", pcode);
                    strYearlyOptions = _replaceAll(strYearlyOptions.substr(0, 4), "", strYearlyOptions);
                    strYearlyOptions = _replaceAll((typeYearly + "."), "", strYearlyOptions);
                    strYearlyOptions = _replaceAll(typeYearly, "", strYearlyOptions);
                    if (strYearlyOptions) {
                        repeatOption = strYearlyOptions.split(".");
                    } else {
                        repeatOption = [];
                    }
                }
                break;
        }

        return repeatOption;
    },

    getRepeatTypeByCode: function (pcode) {
        pcode = _frequencyManager.cleanCode(pcode);
        var repeatValue = _frequencyManager.getRepeatValueByCode(pcode);
        var type = "Days";
        var strOptions = _replaceAll(repeatValue, "", pcode);

        if (repeatValue == "Quarterly") {
            strOptions = _replaceAll("1stM:", "", strOptions);
            strOptions = _replaceAll("2stM:", "", strOptions);
            strOptions = _replaceAll("3stM:", "", strOptions);
        }

        if (repeatValue == "Yearly") {
            //Remove Jan:, Feb:, May:..
            strOptions = _replaceAll(strOptions.substr(0, 4), "", strOptions);
        }

        if (strOptions.substr(0, 2) == "BD") {
            type = "BD";
        }

        return type;
    },

    getRepeat: function () {
        return $("#fieldRepeat").find("input:checked").val();
    },

    getRepeatOption: function () {
        var repeatValue = _frequencyManager.getRepeat();
        var repeatOption = "";

        //Show selected value
        switch (repeatValue) {
            case "Daily":
                repeatOption = $("#fieldRepeatDaily").find("input:checked").val();
                break;

            case "Weekly":
                var inputDayList = $("#fieldRepeatWeekly").find("input:checked");

                for (var i = 0; i < inputDayList.length; i++) {
                    repeatOption += inputDayList[i].value;

                    //if ((i + 1) == inputDayList.length) {
                    //    repeatOption += inputDayList[i].value;
                    //} else {
                    //    repeatOption += inputDayList[i].value + ".";
                    //}
                }

                break;

            case "Monthly":
                var type = $("#containerRepeatMonthly .fieldCalendarDBsType").find("input:checked").val();
                if (type) {
                    var inputDayList;

                    if (type == "BD") {
                        inputDayList = $("#containerRepeatMonthly .containerFieldBusinessDays").find("input:checked");
                    }

                    if (type == "Days") {
                        inputDayList = $("#containerRepeatMonthly .containerFieldCalendarDays").find("input:checked");
                    }

                    repeatOption += type;
                    for (var i = 0; i < inputDayList.length; i++) {
                        repeatOption += ("." + inputDayList[i].value);

                        //if ((i + 1) == inputDayList.length) {
                        //    repeatOption += inputDayList[i].value;
                        //} else {
                        //    repeatOption += inputDayList[i].value + ".";
                        //}
                    }
                }
                break;

            case "Quarterly":
                var inputQuarterMonthsList = $("#fieldRepeatQuarterlyMonths").find("input:checked");
                for (var i = 0; i < inputQuarterMonthsList.length; i++) {
                    repeatOption += (inputQuarterMonthsList[i].value + ":");
                }

                var type = $("#containerFieldRepeatQuarterlyMonthVsBD .fieldCalendarDBsType").find("input:checked").val();
                if (type) {
                    var inputDayList;

                    if (type == "BD") {
                        inputDayList = $("#containerFieldRepeatQuarterlyMonthVsBD .containerFieldBusinessDays").find("input:checked");
                    }

                    if (type == "Days") {
                        inputDayList = $("#containerFieldRepeatQuarterlyMonthVsBD .containerFieldCalendarDays").find("input:checked");
                    }

                    repeatOption += type;
                    for (var i = 0; i < inputDayList.length; i++) {
                        repeatOption += ("." + inputDayList[i].value);

                        //if ((i + 1) == inputDayList.length) {
                        //    repeatOption += inputDayList[i].value;
                        //} else {
                        //    repeatOption += inputDayList[i].value + ".";
                        //}
                    }
                }
                break;

            case "Yearly":
                repeatOption += $("#fieldRepeatYearlySelectMonth").val() + ":";

                var type = $("#containerFieldRepeatYearlyMonthVsBD .fieldCalendarDBsType").find("input:checked").val();
                if (type) {
                    var inputDayList;

                    if (type == "BD") {
                        inputDayList = $("#containerFieldRepeatYearlyMonthVsBD .containerFieldBusinessDays").find("input:checked");
                    }

                    if (type == "Days") {
                        inputDayList = $("#containerFieldRepeatYearlyMonthVsBD .containerFieldCalendarDays").find("input:checked");
                    }

                    repeatOption += type;
                    for (var i = 0; i < inputDayList.length; i++) {
                        repeatOption += ("." + inputDayList[i].value);

                        //if ((i + 1) == inputDayList.length) {
                        //    repeatOption += inputDayList[i].value;
                        //} else {
                        //    repeatOption += inputDayList[i].value + ".";
                        //}
                    }
                }
                break;
        }

        return repeatOption;
    },

    getRepeatEndDate: function () {
        return $("#fieldRepeatEnd").find("input:checked").val();
    },

    getObjTimeZoneSelected: function () {
        var $selOptionTimeZone = $("#selFrequencyTimeZone_" + _frequencyManager.idModalFrequency).find("option:selected");
        return _findOneObjByProperty(_TIME_ZONES, "ID", $selOptionTimeZone.val());
    },

    getSLATimeSelected: function () {
        var $selOptionSLATime = $("#selFrequencySLATime_" + _frequencyManager.idModalFrequency).find("option:selected");
        return $selOptionSLATime.val();
    },

    loadMaxDateCorporateCalendar: function () {
        //Sql for validate GOCs
        var sql =

            "SELECT \n" +
                //DateFormat: YYYY-MM-DD
            "   CONVERT(VARCHAR(10), MAX([WorkDay]), 120)  AS [MaxDate] \n" +
            "FROM \n" +
            "   [Automation].[dbo].[tblCorporateCalendar] ";

        //Get Business Days Result
        _callServer({
            loadingMsgType: "topBar",
            loadingMsg: "Getting Max Date Corporate Calendar...",
            url: '/Ajax/ExecQuery',
            data: { 'pjsonSql': _toJSON(sql) },
            type: "POST",
            success: function (resultList) {
                maxDateCorporateCalendar = resultList[0].MaxDate;
            }
        });
    },

    refreshData: function () {
        // Set Frequency Code
        $("#lblFrequencyCode").html(_frequencyManager.generateFrequencyCode());

        // Reload Frequency Preview
        _frequencyManager.loadFrequencyPreview();
    },

    loadFrequencyPreview: function () {
        _frequencyManager.generateFrequencyDates({
            onSuccess: function (resultListAllDates, typeDaysOrBDs) {
                var resultList = [];
                var frequencyStartDate = $("#frequencyStartDate_" + _frequencyManager.idModalFrequency).val();
                var momentActivityStartDate = moment(frequencyStartDate + " 00:00", 'MMM DD, YYYY HH:mm');

                //Filter list by Frequency Start Date
                for (var i = 0; i < resultListAllDates.length; i++) {
                    if (momentActivityStartDate.isSameOrBefore(resultListAllDates[i].LocalDateMoment)) {
                        resultList.push(resultListAllDates[i]);
                    }
                }

                //console.log(resultList);
                var columns = [];
                var slaTimeSelected = _frequencyManager.getSLATimeSelected();
                var dateTimeFormat = 'yyyy-MM-dd hh:mm tt';
                //if (slaTimeSelected == "23:59") {
                //    dateTimeFormat = 'yyyy-MM-dd';
                //}

                if (typeDaysOrBDs == "Days") {
                    //columns.push({ name: 'Year', text: 'Year', width: '25%', type: 'string', filtertype: 'checkedlist' });
                    //columns.push({ name: 'MonthCustomFormat', text: 'Month', width: '25%', type: 'string', filtertype: 'checkedlist' });
                    columns.push({ name: 'Date', text: 'Activity Date Time Zone', width: '20%', type: 'date', filtertype: 'range', cellsformat: dateTimeFormat, cellsalign: 'center', align: 'center' });
                    columns.push({ name: 'TampaDateTime', text: 'Center Tampa Date Time', width: '20%', type: 'date', filtertype: 'range', cellsformat: dateTimeFormat, cellsalign: 'center', align: 'center' });
                    columns.push({ name: 'MumbaiDateTime', text: 'Center Mumbai Date Time', width: '20%', type: 'date', filtertype: 'range', cellsformat: dateTimeFormat, cellsalign: 'center', align: 'center' });
                    columns.push({ name: 'ManilaDateTime', text: 'Center Manila Date Time', width: '20%', type: 'date', filtertype: 'range', cellsformat: dateTimeFormat, cellsalign: 'center', align: 'center' });
                    columns.push({ name: 'CostaRicaDateTime', text: 'Center Costa Rica Date Time', width: '20%', type: 'date', filtertype: 'range', cellsformat: dateTimeFormat, cellsalign: 'center', align: 'center' });

                    //columns.push({ name: 'UTCDateTime', text: 'UTC Date Time', width: '20%', type: 'date', filtertype: 'range', cellsformat: dateTimeFormat, cellsalign: 'center', align: 'center' });
                    //columns.push({ name: 'ObjDate', text: 'Long Date', width: '35%', type: 'date', filtertype: 'date', cellsformat: 'dddd, MMMM dd, yyyy' });
                }

                if (typeDaysOrBDs == "BD") {
                    //columns.push({ name: 'BDYear', text: 'Year', width: '20%', type: 'string', filtertype: 'checkedlist' });
                    //columns.push({ name: 'BDMonthCustomFormat', text: 'Month', width: '20%', type: 'string', filtertype: 'checkedlist' });
                    columns.push({ name: 'BD', text: 'BD', width: '10%', type: 'string', filtertype: 'checkedlist' });
                    columns.push({ name: 'Date', text: 'Local Date Time', width: '18%', type: 'date', filtertype: 'range', cellsformat: dateTimeFormat, cellsalign: 'center', align: 'center' });
                    columns.push({ name: 'TampaDateTime', text: 'Center Tampa Date Time', width: '18%', type: 'date', filtertype: 'range', cellsformat: dateTimeFormat, cellsalign: 'center', align: 'center' });
                    columns.push({ name: 'MumbaiDateTime', text: 'Center Mumbai Date Time', width: '18%', type: 'date', filtertype: 'range', cellsformat: dateTimeFormat, cellsalign: 'center', align: 'center' });
                    columns.push({ name: 'ManilaDateTime', text: 'Center Manila Date Time', width: '18%', type: 'date', filtertype: 'range', cellsformat: dateTimeFormat, cellsalign: 'center', align: 'center' });
                    columns.push({ name: 'CostaRicaDateTime', text: 'Center Costa Rica Date Time', width: '18%', type: 'date', filtertype: 'range', cellsformat: dateTimeFormat, cellsalign: 'center', align: 'center' });
                    //columns.push({ name: 'UTCDateTime', text: 'UTC Date Time', width: '18%', type: 'date', filtertype: 'range', cellsformat: dateTimeFormat, cellsalign: 'center', align: 'center' });
                    //columns.push({ name: 'ObjDate', text: 'Long Date', width: '30%', type: 'date', filtertype: 'date', cellsformat: 'dddd, MMMM dd, yyyy' });
                }

                $.jqxGridApi.create({
                    showTo: "#tblPreviewFrequency",
                    options: {
                        //for comments or descriptions
                        height: "370",
                        columnsresize: false,
                        autoheight: false,
                        autorowheight: false,
                        showfilterrow: true,
                        sortable: true,
                        editable: false,
                        selectionmode: "singlerow"
                    },
                    source: {
                        // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                        dataBinding: "Large Data Set Local",
                        rows: resultList
                    },
                    groups: [],
                    columns: columns,
                    ready: function () { }
                });
            }
        });
    },

    cleanCode: function (pcode) {
        //Remove from CODE the timezone and SLA

        var splitData = pcode.split("~");
        if (splitData.length >= 3) {
            var timeZonePart = splitData[0];
            var SLATimePart = splitData[splitData.length - 1];
            pcode = pcode.replace(timeZonePart + "~", "");
            pcode = pcode.replace("~" + SLATimePart, "");
        }

        return pcode;
    },

    generateFrequencyCode: function () {

        var timeZoneCode = _frequencyManager.getObjTimeZoneSelected().Abbreviation;
        var slaTime = _frequencyManager.getSLATimeSelected();
        if (slaTime == "23:59") {
            slaTime = "NS";
        }

        return timeZoneCode + "~" + _frequencyManager.getRepeat() + _frequencyManager.getRepeatOption() + "~" + slaTime;
    },

    init: function () {
        //Set idModal Frequency
        _frequencyManager.idModalFrequency = _createCustomID();

        //Set utcOffset by Centers
        _frequencyManager.objTimeZoneCostaRica = _findOneObjByProperty(_TIME_ZONES, "Abbreviation", "CST");   //Central America Standard Time
        _frequencyManager.utcOffsetMinCostaRica = parseInt(_frequencyManager.objTimeZoneCostaRica.UTCOffsetMinutes);

        _frequencyManager.objTimeZoneTampa = _findOneObjByProperty(_TIME_ZONES, "Abbreviation", "EDT");       //Eastern Daylight Savings Time
        _frequencyManager.utcOffsetMinTampa = parseInt(_frequencyManager.objTimeZoneTampa.UTCOffsetMinutes);

        _frequencyManager.objTimeZoneMumbai = _findOneObjByProperty(_TIME_ZONES, "Abbreviation", "IST");      //Indian Standard Time
        _frequencyManager.utcOffsetMinMumbai = parseInt(_frequencyManager.objTimeZoneMumbai.UTCOffsetMinutes);

        _frequencyManager.objTimeZoneManila = _findOneObjByProperty(_TIME_ZONES, "Abbreviation", "PHT");      //Philippine Time
        _frequencyManager.utcOffsetMinManila = parseInt(_frequencyManager.objTimeZoneManila.UTCOffsetMinutes);
    }
};

function _getTeamsOfUser(customOptions) {

    //Default Options.
    var options = {
        forceRefresh: false,
        onSuccess: null
    };

    //Do merge of customOptions with default options.
    $.extend(true, options, customOptions);

    if (_listTeamsOfUser.length > 0 && options.forceRefresh == false) {
        options.onSuccess(_listTeamsOfUser);
    } else {
        _callProcedure({
            loadingMsgType: "fullLoading",
            loadingMsg: "Loading Teams of user...",
            name: "[dbo].[spPCTManageTeam]",
            params: [
                { Name: "@Action", Value: "List Teams of User" },
                { Name: "@SessionSOEID", Value: _getSOEID() }
            ],
            success: function (resultList) {
                _listTeamsOfUser = resultList;
                options.onSuccess(_listTeamsOfUser);
            }
        });
    }
}

function _getCFields(customOptions) {

    //Default Options.
    var options = {
        action: 'List CFields Of Team',
        forceRefresh: false,
        contactSOEID: "",
        idTeam: 0,
        onSuccess: null
    };

    //Do merge of customOptions with default options.
    $.extend(true, options, customOptions);

    //When is contact
    if (options.contactSOEID) {
        options.action = 'List CFields Of Contact';
    } else {
        if (options.idTeam == 0) {
            options.action = 'List CFields Of MSGroup';
        }
    }

    if (_listCFields.length > 0 && options.forceRefresh == false) {
        options.onSuccess(_listCFields);
    } else {
        _callProcedure({
            loadingMsgType: "topBar",
            loadingMsg: "Loading custom fields",
            name: "[dbo].[spPCTManageCField]",
            params: [
                { "Name": "@Action", "Value": options.action },
                { "Name": "@ContactSOEID", "Value": options.contactSOEID },
                { "Name": "@IDTeam", "Value": options.idTeam },
                { "Name": "@SessionSOEID", "Value": _getSOEID() }
            ],
            success: function (resultList) {
                _listCFields = resultList;
                options.onSuccess(_listCFields);
            }
        });
    }
}

function _checkAccess(pathToTest) {
    if (pathToTest) {
        _callServer({
            loadingMsgType: "topBar",
            loadingMsg: "Checking access...",
            url: '/ProcessControlTool/CheckAccess',
            data: {
                typeAuthentication: _getTypeAuthentication(),
                user: _encodeSkipSideminder(_getWindowsUser()),
                pass: _getWindowsPassword(),
                pathToTest: _encodeSkipSideminder(pathToTest)
            },
            success: function (msg) {
                _showNotification("info", msg);
            }
        });
    } else {
        _showNotification("error", "Share folder path is empty", "CheckAccess");
    }
}

function _parseFolderStructure(pobjActivity) {

    //============================================
    //Available combinations
    //============================================
    //(dddd, MMM YYYY)\
    //@TeamName\
    //(YYYY)\
    //(MM)\
    //@TeamTree\
    //@LabelName\
    //@CFieldMCAProcess\
    //[RMCR - @CFieldMCAProcess]\
    //{@FrequencyType=='BD':@BD}\
    //[Minutes]\
    //@Frequency\
    //@FrequencyType\
    //@ActivityName\
    //{@Frequency=='Daily':(YYYY-MM-DD)}\
    //{@Frequency=='Monthly':(YYYY-MM)~(MMMM)}\
    //{@Frequency=='Quarterly':(YYYY-[Q]Q)}\
    //{@Frequency=='Yearly':(YYYY)~(MM-DD)}

    //Default Options.
    var objActivity = {
        ActivityName: "Financial Package",
        TeamName: "Taxes",
        TeamTree: "Costa Rica - LATAM - Taxes",
        TeamSharedFolderPath: '\\\\sjovnasfro0002.wlb.lac.nsroot.net\\froqt0018\\BSS_MEX_O&T\\Support Documentation\\DEV\\GLOBAL',
        TeamFolderStructure: "(dddd, MMM YYYY)\@TeamName\(YYYY)\(MM)\@TeamTree\@LabelName\@CFieldMCAProcess\{@FrequencyType=='BD':@BD}\[Minutes]\@Frequency\@FrequencyType\@ActivityName\{@Frequency=='Daily':(YYYY-MM-DD)}\{@Frequency=='Monthly':(YYYY-MM)~(MMMM)}\{@Frequency=='Quarterly':(YYYY-[Q]Q)}\{@Frequency=='Yearly':(YYYY)~(MM-DD)}",
        TeamLabelName: "Country",
        Frequency: "Monthly",
        FrequencyCode: "CST~DailyWorkdays~08:00",
        FrequencyUTCDateTimeSLA: "2018-10-16 12:34",
        FrequencyType: "BD", //"Days",
        FrequencyLocalYear: "2018",
        FrequencyLocalMonth: "10",
        FrequencyLocalDateTimeSLA: "2018-10-16 12:34",
        FrequencyBDNum: "-5",
        IDMCAProcess: "2",
        MCAProcessName: "CAA - Reporting - Standard",
        JsonSharedDriveFiles:
            ' [ ' +
	        '     { ' +
		    '         "Name": "principalfile", ' +
            '         "Ext": "xlsx", ' +
            '         "Description": "summary of all balances", ' +
            '         "FolderStructure": "@TeamStructure\\\\@FileName.@FileExt" ' +
            '     }, ' +
            '     { ' +
		    '         "Name": "minutes", ' +
		    '         "Ext": "msg", ' +
		    '         "Description": "minutes of the meeting", ' +
		    '         "FolderStructure": "@TeamStructure\\\\[Minutes]\\\\@FileName.@FileExt" ' +
            '     } ' +
            ' ]'
    };
    
    //Merge default options
    $.extend(true, objActivity, pobjActivity);

    // Activity Time Zone
    var activityMomentDateTime = moment(objActivity.FrequencyLocalDateTimeSLA);

    //Set BD Month and BD Year
    if (objActivity.FrequencyType == "BD") {
        activityMomentDateTime.month(parseInt(objActivity.FrequencyLocalMonth) - 1);
        activityMomentDateTime.year(objActivity.FrequencyLocalYear);
    }

    //Get Shared Drive Files
    var sharedFiles = JSON.parse(objActivity.JsonSharedDriveFiles);

    try {
        //calculate team structure
        var structureParts = objActivity.TeamFolderStructure.split("\\");
        var teamPathGenerated = "";
        for (var i = 0; i < structureParts.length; i++) {
            var tempPart = structureParts[i];

            //Calculate parts
            teamPathGenerated += evaluateTeamStructurePart(tempPart);

            //Validate expresions
            if (tempPart.length > 2) {
                var firstChar = tempPart.substr(0, 1);
                var lastChar = tempPart.substring(tempPart.length, tempPart.length - 1);

                //Remove firstChar and lastChar
                tempPart = tempPart.substr(1);;
                tempPart = tempPart.slice(0, -1);;

                //When is an expresion
                if (firstChar == "{" && lastChar == "}") {
                    var expresionParts = tempPart.split(":");
                    if (expresionParts.length == 2) {
                        var conditionPart = expresionParts[0];
                        var folderParts = expresionParts[1];

                        //Replace @FrequencyType
                        conditionPart = _replaceAll("@FrequencyType", "'" + objActivity.FrequencyType + "'", conditionPart);

                        //Replace @Frequency
                        conditionPart = _replaceAll("@Frequency", "'" + objActivity.Frequency + "'", conditionPart);

                        //Evaluate condition
                        if (eval(conditionPart)) {
                            var foldersToCreate = folderParts.split("~");
                            for (var f = 0; f < foldersToCreate.length; f++) {
                                teamPathGenerated += evaluateTeamStructurePart(foldersToCreate[f]);
                            }
                        }
                    }
                }
            }
        }

        var basePath = objActivity.TeamSharedFolderPath + teamPathGenerated;
        for (var i = 0; i < sharedFiles.length; i++) {
            var tempSharedFile = sharedFiles[i];
            sharedFiles[i].FolderPath = basePath;

            var fileStructureParts = tempSharedFile.FolderStructure.split("\\");
            var sharedFilePathGenerated = "";

            for (var s = 0; s < fileStructureParts.length; s++) {
                sharedFilePathGenerated += evaluateActivityStructurePart(fileStructureParts[s], tempSharedFile);
            }

            sharedFiles[i].FullPath = sharedFilePathGenerated;
            sharedFiles[i].FolderPath += "\\";
        }
        
        function evaluateActivityStructurePart(tempPart, objSharedFile) {
            var result = "";

            //When is empty set \
            if (tempPart == "") {
                result = "\\";
            }

            //When is @FileName.@FileExt
            if (!result && tempPart == "@FileName.@FileExt") {
                result = "\\" + objSharedFile.FileName + "." + objSharedFile.FileExt;
            }

            //When is @FileName.@FileExt
            if (!result && tempPart == "@TeamStructure") {
                result += objSharedFile.FolderPath;
            }

            //New folders 
            if (!result && tempPart.length > 2) {
                var firstChar = tempPart.substr(0, 1);
                var lastChar = tempPart.substring(tempPart.length, tempPart.length - 1);

                //Remove firstChar and lastChar
                tempPart = tempPart.substr(1);;
                tempPart = tempPart.slice(0, -1);;

                //When is a new folder
                if (firstChar == "[" && lastChar == "]") {
                    tempPart = replaceFolderNameVariables(tempPart);
                    tempPart = _cleanString(tempPart, true);
                    result = "\\" + tempPart;
                }
            }

            return result;
        }

        function evaluateTeamStructurePart(tempPart) {
            var result = "";

            //When is empty set \
            if (tempPart == "") {
                result = "\\";
            }

            //When is @TeamName
            if (!result && tempPart == "@TeamName") {
                result = "\\" + objActivity.TeamName;
            }

            //When is @TeamTree
            if (!result && tempPart == "@TeamTree") {
                var treeParts = objActivity.TeamTree.split(" - ");
                for (var t = 0; t < treeParts.length; t++) {
                    result += "\\" + treeParts[t];
                }
            }

            //When is @LabelName
            if (!result && tempPart == "@LabelName") {
                result = "\\" + objActivity.TeamLabelName;
            }

            //When is @Frequency
            if (!result && tempPart == "@Frequency") {
                result = "\\" + objActivity.Frequency;
            }

            //When is @FrequencyType
            if (!result && tempPart == "@FrequencyType") {
                result = "\\" + objActivity.FrequencyType;
            }

            //When is @BD
            if (!result && tempPart == "@BD") {
                if (objActivity.FrequencyBDNum) {
                    result = "\\BD" + objActivity.FrequencyBDNum;
                }
            }

            //When is @ActivityName
            if (!result && tempPart == "@ActivityName") {
                result = "\\" + objActivity.ActivityName;
            }

            //Whe is a @CField
            if (!result && _contains(tempPart, "@CField")) {
                tempPart = tempPart.replace("@CField", "");
                if (objActivity[tempPart + "Name"]) {
                    result = "\\" + objActivity[tempPart + "Name"];
                }
            }

            //Validate dates / new folders 
            if (!result && tempPart.length > 2) {
                var firstChar = tempPart.substr(0, 1);
                var lastChar = tempPart.substring(tempPart.length, tempPart.length - 1);

                //Remove firstChar and lastChar
                tempPart = tempPart.substr(1);;
                tempPart = tempPart.slice(0, -1);;

                //When is a date format
                if (firstChar == "(" && lastChar == ")") {
                    result = "\\" + getDateFormat(tempPart);
                }

                //When is a new folder
                if (firstChar == "[" && lastChar == "]") {
                    tempPart = replaceFolderNameVariables(tempPart);
                    tempPart = _cleanString(tempPart, true);
                    result = "\\" + tempPart;
                }
            }

            return result;
        }

        function replaceFolderNameVariables(pfolderName) {
            //When is @TeamName
            if (_contains(pfolderName, "@TeamName")) {
                pfolderName = _replaceAll("@TeamName", objActivity.TeamName, pfolderName);
            }

            //When is @TeamTree
            if (_contains(pfolderName, "@TeamTree")) {
                pfolderName = _replaceAll("@TeamTree", objActivity.TeamTree, pfolderName);
            }

            //When is @LabelName
            if (_contains(pfolderName, "@LabelName")) {
                pfolderName = _replaceAll("@LabelName", objActivity.TeamLabelName, pfolderName);
            }

            //When is @Frequency
            if (_contains(pfolderName, "@Frequency")) {
                pfolderName = _replaceAll("@Frequency", objActivity.Frequency, pfolderName);
            }

            //When is @FrequencyType
            if (_contains(pfolderName, "@FrequencyType")) {
                pfolderName = _replaceAll("@FrequencyType", objActivity.FrequencyType, pfolderName);
            }

            //When is @BD
            if (_contains(pfolderName, "@BD")) {
                if (objActivity.FrequencyBDNum) {
                    pfolderName = _replaceAll("@BD", objActivity.FrequencyBDNum, pfolderName);
                }
            }

            //When is @ActivityName
            if (_contains(pfolderName, "@ActivityName")) {
                pfolderName = _replaceAll("@ActivityName", objActivity.ActivityName, pfolderName);
            }

            //Whe is a @CField
            for (var i = 0; i < _listCFields.length; i++) {
                var tempCField = _listCFields[i];
                if (_contains(pfolderName, "@CField" + tempCField.Key)) {
                    if (objActivity[tempCField.Key + "Name"]) {
                        pfolderName = _replaceAll("@CField" + tempCField.Key, objActivity[tempCField.Key + "Name"], pfolderName);
                    }
                }
            }

            return pfolderName;
        }

        function getDateFormat(pdateFormat) {
            return activityMomentDateTime.format(pdateFormat);
        }

    } catch (e) {

        var htmlErrorMessage = "Folder Structure Path cannot be generated, please contact system administrator to fix the issue.<br /><br />";

        htmlErrorMessage += "<b>Shared Drive: </b>" + objActivity.TeamSharedFolderPath + " <br />";
        htmlErrorMessage += "<b>Team Folder Structure: </b>" + objActivity.TeamFolderStructure + " <br /><br />";

        for (var i = 0; i < sharedFiles.length; i++) {
            var tempSharedFile = sharedFiles[i];
            htmlErrorMessage += "<b>Shared File : </b>" + tempSharedFile.Name + "." + tempSharedFile.Ext + " <br />";
            htmlErrorMessage += "<b>Shared File Structure: </b>" + tempSharedFile.FolderStructure + " <br /><br />";
        }

        _showAlert({
            id: "FolderStructureError",
            type: 'error',
            title: "Message",
            content: htmlErrorMessage
        });
    }

    return sharedFiles;
}

function _getHtmlContactPopupByJson(jsonContacts) {
    var htmlResult = "";

    if (jsonContacts) {
        var rows = JSON.parse(jsonContacts);
        if (rows.length > 0) {
            var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

            for (var i = 0; i < rows.length; i++) {
                var tempData = rows[i];

                if (tempData.Type == "User") {
                    infoHtml += " \n "
                    infoHtml += ' &lt;a href=&quot;sip:' + tempData.Email + '&quot;&gt;&lt;img src=&quot;' + _getViewVar("SubAppPath") + '/Content/Shared/images/chat-skype.png&quot; height=&quot;26&quot; width=&quot;26&quot;&gt;&lt;/a&gt;';
                    infoHtml += ' &lt;a href=&quot;mailto:' + tempData.Email + '&quot;&gt;&lt;img src=&quot;' + _getViewVar("SubAppPath") + '/Content/Shared/images/email.png&quot; height=&quot;30&quot; width=&quot;30&quot;&gt;&lt;/a&gt; ';
                    infoHtml += tempData.Type + ": " + tempData.Description;
                    infoHtml += ' &lt;br&gt;';
                }

                if (tempData.Type == "DL") {
                    infoHtml += " \n ";
                    infoHtml += ' &lt;a href=&quot;mailto:' + tempData.Email + '&quot;&gt;&lt;img src=&quot;' + _getViewVar("SubAppPath") + '/Content/Shared/images/email.png&quot; height=&quot;30&quot; width=&quot;30&quot;&gt;&lt;/a&gt;';
                    infoHtml += tempData.Type + ": " + tempData.Description;
                    infoHtml += ' &lt;br&gt;';
                }

            }

            infoHtml += "</div>";

            htmlResult +=
                '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="Admins Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                '       <img style="margin-right: 25px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/info.png"/> ' +
                '</div>';
        }
    }

    return htmlResult;
}

function _getCheckedRows(idTable) {
    var selectedRows = [];
    var activities = $(idTable).jqxGrid("getRows");

    for (var i = 0; i < activities.length; i++) {
        if (activities[i]["IsSelected"]) {
            selectedRows.push(activities[i]);
        }
    }

    return selectedRows;
}

function _showModalActivitySettings(objActivity) {

    var formHtml =
    '<div class="row"> ' +
    '    <div class="col-md-12"> ' +
    '       <div class="pull-left"> ' +
    '           <p>Please check the setting that you need:</p> ' +
    '       </div> ' +
    '       <div style="clear:both;"></div> ' +
    '       <table class="table table-hover"> ' +
    '           <tbody id="tblActivitySettins"> ' +

    '                <tr> ' +
    '                    <td style="width: 84.5%;"></td> ' +
    '                    <td style="width: 15.5%;"> ' +
    '                       Check all <br /> ' +
    '                       <input type="checkbox" class="skin-square-green checkAllUserXRoles"> ' +
    '                    </td>' +
    '                </tr>' +

    '                <tr> ' +
    '                    <td style="width: 84.5%;"> ' +
    '                        <b>Activity Details</b>' +
    '                    </td> ' +
    '                    <td style="width: 15.5%;"></td>' +
    '                </tr>' +

    '                <tr> ' +
    '                    <td style="width: 84.5%;"> ' +
    '                        <i class="fa fa-cube"></i> - Add checklist for checker' +
    '                    </td> ' +
    '                    <td style="width: 15.5%;"> ' +
    '                        <input type="checkbox" class="skin-square-green chkIsEnabled">' +
    '                    </td>' +
    '                </tr>' +

    '                <tr> ' +
    '                    <td style="width: 84.5%;"> ' +
    '                        <i class="fa fa-cube"></i> - Add eDCFC Mapping' +
    '                    </td> ' +
    '                    <td style="width: 15.5%;"> ' +
    '                        <input type="checkbox" class="skin-square-green chkIsEnabled">' +
    '                    </td>' +
    '                </tr>' +

    '                <tr> ' +
    '                    <td style="width: 84.5%;"> ' +
    '                        <b>Share File Traking</b>' +
    '                    </td> ' +
    '                    <td style="width: 15.5%;"></td>' +
    '                </tr>' +

    '                <tr> ' +
    '                    <td style="width: 84.5%;"> ' +
    '                        <i class="fa fa-cube"></i> Track file in share folder' +
    '                    </td> ' +
    '                    <td style="width: 15.5%;"> ' +
    '                        <input type="checkbox" class="skin-square-green chkIsEnabled">' +
    '                    </td>' +
    '                </tr>' +

    '                <tr> ' +
    '                    <td style="width: 84.5%;"> ' +
    '                        <i class="fa fa-cube"></i> - Add additional files' +
    '                    </td> ' +
    '                    <td style="width: 15.5%;"> ' +
    '                        <input type="checkbox" class="skin-square-green chkIsEnabled">' +
    '                    </td>' +
    '                </tr>' +

    '                <tr> ' +
    '                    <td style="width: 84.5%;"> ' +
    '                        <i class="fa fa-cube"></i> - Add working file' +
    '                    </td> ' +
    '                    <td style="width: 15.5%;"> ' +
    '                        <input type="checkbox" class="skin-square-green chkIsEnabled">' +
    '                    </td>' +
    '                </tr>' +

    '                <tr> ' +
    '                    <td style="width: 84.5%;"> ' +
    '                        <b>Other Sections</b>' +
    '                    </td> ' +
    '                    <td style="width: 15.5%;"></td>' +
    '                </tr>' +

    '                <tr> ' +
    '                    <td style="width: 84.5%;"> ' +
    '                        <i class="fa fa-cube"></i> Add documentation links' +
    '                    </td> ' +
    '                    <td style="width: 15.5%;"> ' +
    '                        <input type="checkbox" class="skin-square-green chkIsEnabled">' +
    '                    </td>' +
    '                </tr>' +

    '                <tr> ' +
    '                    <td style="width: 84.5%;"> ' +
    '                        <i class="fa fa-cube"></i> Add COB Information' +
    '                    </td> ' +
    '                    <td style="width: 15.5%;"> ' +
    '                        <input type="checkbox" class="skin-square-green chkIsEnabled">' +
    '                    </td>' +
    '                </tr>' +

    '           </tbody> ' +
    '       </table> ' +
    '    </div> ' +
    '</div> ';

    _showModal({
        width: "50%",
        title: "Activity Settings",
        contentHtml: formHtml,
        buttons: [{
            name: "Save",
            class: "btn-success",
            closeModalOnClick: false,
            onClick: function ($modal) {

            }
        }],
        addCloseButton: true,
        onReady: function ($modal) {
            //Init Radio buttons
            _GLOBAL_SETTINGS.iCheck();
        }
    });
}

function _toggleSelectAll(idTable) {
    var rows = $(idTable).jqxGrid("getRows");
    var idsToUpdate = [];
    var rowsToUpdate = [];
    var isSelected = ($(".btnToggleSelectAll").attr("updateSelectTo") == "1");

    for (var i = 0; i < rows.length; i++) {
        rows[i]["IsSelected"] = isSelected;
        idsToUpdate.push(rows[i].uid);
        rowsToUpdate.push(rows[i]);
    }

    if (isSelected) {
        $(".btnToggleSelectAll").html('<i class="fa fa-check-square-o"></i> Unselect All ');
        $(".btnToggleSelectAll").attr("updateSelectTo", "0");
    } else {
        $(".btnToggleSelectAll").html('<i class="fa fa-square-o"></i> Select All ');
        $(".btnToggleSelectAll").attr("updateSelectTo", "1");
    }

    //Update Data in Table
    $(idTable).jqxGrid('updaterow', idsToUpdate, rowsToUpdate);

    // expand all groups.
    $(idTable).jqxGrid('expandallgroups');
}

function _checkActivitiesExists(customOptions) {
    //Activities to Post
    var activitiesToPost = [];

    //Default Options.
    var options = {
        loadingMsgType: "fullLoading",
        year: 0,
        month: 0,
        processDate: "",
        activities: [],
        onSuccess: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    //Remove unnecesary properties
    for (var i = 0; i < options.activities.length; i++) {
        activitiesToPost.push({
            IDPFDetail: options.activities[i].IDPFDetail,
            Year: options.year,
            Month: options.month,
            ActivityDate: options.processDate,
            IDActivity: options.activities[i].IDActivity,
            FileFullPath: options.activities[i].FileFullPath,
            FilePath: options.activities[i].FilePath,
            FolderPath: options.activities[i].FolderPath,
            StatusByChecker: options.activities[i].StatusByChecker,
            FileStatus: options.activities[i].FileStatus
        });
    }

    //Call Server
    _callServer({
        type: "POST",
        loadingMsgType: options.loadingMsgType,
        loadingMsg: "Checking process files in share folder...",
        url: '/ProcessControlTool/CheckActivities',
        data: {
            jsonActivities: _toJSON(activitiesToPost),
            typeAuthentication: _getTypeAuthentication(),
            user: _encodeSkipSideminder(_getWindowsUser()),
            pass: _getWindowsPassword()
        },
        success: function (resultMsg) {
            if (options.onSuccess) {
                //_showNotification("info", resultMsg, "CheckedFilesAlert");
                options.onSuccess();
            }
        }
    });
}

function _insertAuditLog(options) {
    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Creating audit log...",
        name: "[dbo].[spSDocAuditAdmin]",
        params: [
            { "Name": "@TypeAction", "Value": "Insert" },
            { "Name": "@AuditAction", "Value": options.Action },
            { "Name": "@Description", "Value": options.Description },
            { "Name": "@CreatedBy", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                if (options.onSuccess) {
                    options.onSuccess();
                }
            }
        }
    });
}

function _getSupportDocUserInfo(options) {
    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Creating audit log...",
        name: "[dbo].[spSDocAuditAdmin]",
        params: [
            { "Name": "@TypeAction", "Value": "Insert" },
            { "Name": "@AuditAction", "Value": options.Action },
            { "Name": "@Description", "Value": options.Description },
            { "Name": "@CreatedBy", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                if (options.onSuccess) {
                    options.onSuccess();
                }
            }
        }
    });
}

//------------------------------------------------------
//Authentication methods
//------------------------------------------------------
function _setWindowsUser(user) {
    localStorage.setItem("WindowsUser", user);
}

function _getWindowsUser() {
    var userID = "";
    if (localStorage.getItem("WindowsUser")) {
        userID = localStorage.getItem("WindowsUser");
    }

    return userID;
}

function _setWindowsPassword(pass) {
    localStorage.setItem("WindowsPassword", _encodeAsciiString(pass));
}

function _getWindowsPassword(decodePass) {
    var pass = "";
    if (localStorage.getItem("WindowsPassword")) {
        pass = localStorage.getItem("WindowsPassword");

        if (decodePass) {
            pass = _decodeAsciiString(pass);
        }
    }

    return pass;
}

function _setTypeAuthentication(auth) {
    localStorage.setItem("TypeAuthentication", auth);
}

function _getTypeAuthentication() {
    var auth = "ToolAuthentication";
    if (localStorage.getItem("TypeAuthentication")) {
        auth = localStorage.getItem("TypeAuthentication");
    }

    return auth;
}

//Show Full Page Loading | Hide Loading on AdminActivity.js or ActivityList.js
_showLoadingFullPage({
    id: "ActivityPageLoading",
    msg: "Loading Page...."
});

$(document).ready(function () {
    //console.log('ProcessControlTool: calling...');
    //Load Time Zones
    _loadTimeZone();

    //Update User DLs
    _updateCurrentUserDLs();

});
