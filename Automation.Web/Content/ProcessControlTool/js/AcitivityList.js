/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="ProcessControlTool.js" />

//Use to load activities of selected contact
var _lastContactSOEIDSelected = "";
var _contactView = "";
var _contactCFields = [];
var _userSummary = {};
var _activitiesLoaded = false;

//Hide Menu
_hideMenu();

//Remove Page Title
$(".page-title").remove();

//On document ready
$(document).ready(function () {

    //On Change Frequency
    $("#selFrequency").change(function () {
        var selFrequency = $("#selFrequency").find("option:selected").val();

        //Set cache to always load selected frequency by the user
        localStorage.setItem("ActivityFilterFrequency", selFrequency);

        //Daily
        if (selFrequency == "Daily") {
            onViewDaily();
        }

        //Weekly
        if (selFrequency == "Weekly") {
            onViewWeekly();
        }

        //Monthly
        if (selFrequency == "Monthly") {
            onViewMonthly();
        }

        //Quarterly
        if (selFrequency == "Quarterly") {
            onViewQuarterly();
        }

        //Refresh Activities table
        refreshPage();
    });

    //Load User Select
    if (_getQueryStringParameter("pviewType") == "All") {
        //When is View all activities
        _createSelectSOEID({
            id: "#selActivityUser",
            selSOEID: ""
        });

        //When is all activities show select
        $(".fieldSelActivityUser").show();
    } else {
        //Whe is view my activities, create Select of current user
        _createSelectSOEID({
            id: "#selActivityUser",
            selSOEID: _getSOEID(),
            disabled: true
        });

        //When is my activities hide select
        $(".fieldSelActivityUser").hide();
    }

    //On click in tab Maker / Checker / Stakeholder
    $("[contactView]").click(function () {
        _contactView = $(this).attr("contactView");
        localStorage.setItem("ActivityFilterContactView", _contactView);

        //Set start date of last activity pending
        //if (_userSummary[_contactView + 'MinActivityUTCDateTime']) {
        //    tempMoment = moment(_timeZoneConvertUTCToLocal(_userSummary[_contactView + 'MinActivityUTCDateTime']));
        //    updateDatepicker("#activityStartDate", tempMoment, false);
        //}

        refreshPage();
    });

    //On Click Copy Link to clipboard
    $(".btnCopyLinkToClipboard").click(function () {
        copyLinkToClipboard();
    });

    //On click download files
    $(".btnDownloadFiles").click(function () {
        downloadFiles();
    });

    //On click upload document
    $(".btnUploadFiles").click(function () {
        uploadFiles();
    });

    //On click Problem
    $(".btnProblem").click(function () {
        openModalProblem();
    });

    //On click add comment
    $(".btnAddComment").click(function () {
        openModalComment();
    });

    //On click review activity
    $(".btnReview").click(function () {
        openModalReview();
    });

    //On click approve activity
    $(".btnApprove").click(function () {
        openModalApproval("Approved");
    });

    //On click reject activity
    $(".btnReject").click(function () {
        openModalApproval("Rejected");
    });

    //On click send Reminder
    $(".btnSendEmailReminder").click(function () {
        openModalSendEmailReminderOrEscalation("Reminder");
    });

    //On click send Escalation
    $(".btnSendEmailEscalation").click(function () {
        openModalSendEmailReminderOrEscalation("Escalation");
    });

    //When all ajax are completed (_loadTimeZone();, _updateCurrentUserDLs();, loadActivitySummaryOfUser();)
    _execOnAjaxComplete(function () {

        //Run on Ready Scripts
        onFirstLoadOfPage();
        
    });
});

function onFirstLoadOfPage() {
    
    //load properties [MakerMinActivityUTCDateTime], [CheckerMinActivityUTCDateTime], [StakeholderMinActivityUTCDateTime]
    loadMinActivityUTCDateTimeOfUser({
        onSuccess: function () {
            //Load default values for filters and use MakerMinActivityUTCDateTime / CheckerMinActivityUTCDateTime 
            //to set default Start Date
            initFilterValues();

            //Get current filters
            var filters = getActivityFilters();

            //Refresh CFields and loadMinActivityUTCDateTimeOfUser() only when contact change
            var forceRefresh = false;
            if (_lastContactSOEIDSelected != filters.ContactSOEID) {
                forceRefresh = true;
                _lastContactSOEIDSelected = filters.ContactSOEID;
            }

            //Get information of current user like TotalMakers, TotalCheckers, TotalStakeholder
            loadActivitySummaryOfUser({
                forceRefresh: forceRefresh,
                loadMinActivityUTCDateTime: false,
                onSuccess: function () {

                    //Set current label name for Select of User
                    //$("#lblSelActivityUser").html(_contactView + " User:");

                    //Get custom fields of activities of user
                    _getCFields({
                        forceRefresh: forceRefresh,
                        contactSOEID: filters.ContactSOEID,
                        onSuccess: function (listCFields) {
                            _contactCFields = listCFields;

                            //Load table of Activities
                            loadTableActivities();

                            //Hide Full Page Loading
                            _hideLoadingFullPage({
                                id: "ActivityPageLoading"
                            });
                        }
                    });
                }
            });
        }
    });
}

function refreshPage() {

    //Refres Totals Summary
    loadActivitySummaryOfUser({
        onSuccess: function () {
            //Get current filters
            var filters = getActivityFilters();

            //Set current label name for Select of User
            //$("#lblSelActivityUser").html(_contactView + " User:");

            //Refresh CFields only when contact change
            var forceRefresh = false;
            if (_lastContactSOEIDSelected != filters.ContactSOEID) {
                forceRefresh = true;
                _lastContactSOEIDSelected = filters.ContactSOEID;
            }

            //Get custom fields of activities of user
            _getCFields({
                forceRefresh: forceRefresh,
                contactSOEID: filters.ContactSOEID,
                onSuccess: function (listCFields) {
                    _contactCFields = listCFields;

                    //Load table of Activities
                    loadTableActivities();
                }
            });
        }
    });
}

function initFilterValues() {
    //Set all Frequencies
    $("#selFrequency").find("option[value='All']").attr('selected', 'selected');

    //Set cache to always load selected frequency by the user
    localStorage.setItem("ActivityFilterFrequency", "All");

    //On Change user refresh data, adding here because outside is triggering change event
    $("#selActivityUser select").change(function () {
        refreshPage();
    });

    //Load FilterBy
    loadFilterBy();
}

function loadFilterBy() {

    //Create datepicker on firstLoad
    if ($("#selFilterBy").attr("flagWasInit") == "0") {

        //On Change Filter By
        $("#selFilterBy").change(function () {
            loadFilterBy();
        });

        //When is notification
        var putcDateTimeSLA = _getQueryStringParameter("putcDateTimeSLA");
        if (putcDateTimeSLA) {
            //Set default value "All"
            $("#selFilterBy").find("option[value='All']").attr('selected', 'selected');
        } else {
            //If exists value in localstorage
            var cacheFilterBy = localStorage.getItem("ActivityFilterBy");
            if (cacheFilterBy) {
                $("#selFilterBy").val(cacheFilterBy);
            } else {
                //Set default value "Month"
                $("#selFilterBy").find("option[value='Month']").attr('selected', 'selected');
            }
        }
        $("#selFilterBy").attr("flagWasInit", "1");
    } 

    //Get selected item
    var selFilterBy = $("#selFilterBy").find("option:selected").val();

    //Set cache to always load selected filter by of the user
    localStorage.setItem("ActivityFilterBy", selFilterBy);

    //All
    if (selFilterBy == "All") {
        onViewAll();
    }

    //Day
    if (selFilterBy == "Day") {
        onViewDaily();
    }

    //Month
    if (selFilterBy == "Month") {
        onViewMonthly();
    }

    //Quarter
    if (selFilterBy == "Quarter") {
        onViewQuarterly();
    }
}

function onViewAll() {

    if ($("#activityStartDate").attr("flagWasInit") == "0" &&
        $("#activityEndDate").attr("flagWasInit") == "0") {

        //Select Start Date: Set defaul value
        //$("#activityStartDate").val(moment(getMinActivityLocalDateTime()).format('MMM DD, YYYY'));

        //Select Start Date: Set defaul value
        var cacheStartDate = localStorage.getItem("ActivityFilterStartDate");
        if (cacheStartDate) {
            $("#activityStartDate").val(moment(cacheStartDate).format('MMM DD, YYYY'));
        } else {
            //Set default value for start of year
            $("#activityStartDate").val(moment().startOf('year').format('MMM DD, YYYY'));
        }

        //Create Datepicker for Activity Start Date and add Change Date event
        $("#activityStartDate").datepicker().on('changeDate', function (ev) {
            //Avoid multiples update to the table of activities
            var flagUpdateTable = "1";
            if (typeof $("#activityStartDate").attr("flagUpdateTable") != "undefined") {
                flagUpdateTable = $("#activityStartDate").attr("flagUpdateTable");
            }

            //Get Activity Start Date
            var activityStartDateInput = $("#activityStartDate").val();
            var momentActivityStartDate = moment(activityStartDateInput, 'MMM DD, YYYY');

            //Set cache to always load last Start Date selected by the user
            //NOTE: not saving start date to always show last pending activity
            //localStorage.setItem("ActivityFilterStartDate", momentActivityStartDate.format("YYYY-MM-DD"));

            if (flagUpdateTable == "1") {
                //Refresh Activities table
                refreshPage();
            } else {
                //Reset for next change
                $("#activityStartDate").attr("flagUpdateTable", "1");
            }
        });
        $("#activityStartDate").attr("flagWasInit", "1");

        //Select End Date: Set defaul value
        var cacheEndDate = localStorage.getItem("ActivityFilterEndDate");
        if (cacheEndDate) {
            $("#activityEndDate").val(moment(cacheEndDate).format('MMM DD, YYYY'));
        } else {
            //Set default value for end of month
            $("#activityEndDate").val(moment().endOf('year').format('MMM DD, YYYY'));
        }

        //Create Datepicker for Activity End Date and add Change Date event
        $("#activityEndDate").datepicker().on('changeDate', function (ev) {
            //Avoid multiples update to the table of activities
            var flagUpdateTable = "1";
            if (typeof $("#activityEndDate").attr("flagUpdateTable") != "undefined") {
                flagUpdateTable = $("#activityEndDate").attr("flagUpdateTable");
            }

            //Get Activity End Date
            var activityEndDateInput = $("#activityEndDate").val();
            var momentActivityEndDate = moment(activityEndDateInput, 'MMM DD, YYYY');

            //Set cache to always load last End Date selected by the user
            localStorage.setItem("ActivityFilterEndDate", momentActivityEndDate.format("YYYY-MM-DD"));

            if (flagUpdateTable == "1") {
                //Refresh Activities table
                refreshPage();
            } else {
                //Reset for next change
                $("#activityEndDate").attr("flagUpdateTable", "1");
            }
        });
        $("#activityEndDate").attr("flagWasInit", "1");

        //When exists Query String Parameters
        var putcDateTimeSLA = _getQueryStringParameter("putcDateTimeSLA");
        var pcontactView = _getQueryStringParameter("pcontactView");
        if (putcDateTimeSLA) {
            var momentLocalDateTime = _timeZoneConvertUTCToLocal(putcDateTimeSLA, false);

            //Set "All" Frequency
            $("#selFrequency").find("option").removeAttr('selected');
            $("#selFrequency").find("option[value='All']").attr('selected', 'selected');

            //Set Start Date
            $("#activityStartDate").val(momentLocalDateTime.format('MMM DD, YYYY'));

            //Set End Date
            $("#activityEndDate").val(momentLocalDateTime.format('MMM DD, YYYY'));

            //Set Query String Parameter of Contact View
            if (pcontactView) {
                _contactView = pcontactView;
            }
        }
    } else {
        //Set Filter By All
        //var lastPendingActivityDate = moment(getMinActivityLocalDateTime());
        //var endOfMonth = moment().endOf('month');

        updateDatepicker("#activityStartDate", moment().startOf('year'), false);
        updateDatepicker("#activityEndDate", moment().endOf('year'), false);
    }

    //Refresh only if table was loaded
    if (_activitiesLoaded) {
        refreshPage();
    }

    //Hide or show others filters
    $(".filterByAll").show();
    $(".filterByDay").hide();
    $(".filterByMonth").hide();
    $(".filterByQuarter").hide();
}

function onViewDaily() {
    
    //Create datepicker on firstLoad
    if ($("#activityDate").attr("flagWasInit") == "0") {

        //Set value
        $("#activityDate").val(moment().format('MMM DD, YYYY'));

        //Create Datepicker for Activity Date and add Change Date event
        $("#activityDate").datepicker().on('changeDate', function (ev) {
            //Set Filter for day selected
            setDay();
        });
        $("#activityDate").attr("flagWasInit", "1");

    }

    //Set Filter for day selected
    setDay();

    //Hide or show others filters
    $(".filterByAll").hide();
    $(".filterByDay").show();
    $(".filterByMonth").hide();
    $(".filterByQuarter").hide();

    function setDay() {
        //Set Filter By Day
        var currentDate = moment($("#activityDate").val(), "MMM DD, YYYY");
        updateDatepicker("#activityStartDate", currentDate, false);
        updateDatepicker("#activityEndDate", currentDate, false);

        //Refresh only if table was loaded
        if (_activitiesLoaded) {
            refreshPage();
        }
    }
}

function onViewMonthly() {

    //Add events on first load 
    if ($("#selActivityMonth").attr("flagWasInit") == "0") {

        $("#selActivityMonth").change(function () {
            setMonth();
        });
        $("#selActivityMonth").attr("flagWasInit", "1");

    } 

    //Set Filter By Month
    setMonth();
    
    //Hide or show others filters
    $(".filterByAll").hide();
    $(".filterByDay").hide();
    $(".filterByMonth").show();
    $(".filterByQuarter").hide();

    function setMonth() {
        var selectedMonth = $("#selActivityMonth").find("option:selected").val();
        
        //Set Selected Month Filter
        var startOfMonth = moment(selectedMonth, "YYYY-MM").startOf('month');
        var endOfMonth = moment(selectedMonth, "YYYY-MM").endOf('month');
        updateDatepicker("#activityStartDate", startOfMonth, false);
        updateDatepicker("#activityEndDate", endOfMonth, false);

        //Refresh only if table was loaded
        if (_activitiesLoaded) {
            refreshPage();
        }
    }
}

function onViewQuarterly() {
    //Add events on first load 
    if ($("#selActivityQuarter").attr("flagWasInit") == "0") {

        $("#selActivityQuarter").change(function () {
            setQuarter();
        });
        $("#selActivityQuarter").attr("flagWasInit", "1");

    }

    //Set Filter By Quarter
    setQuarter();

    //Hide or show others filters
    $(".filterByAll").hide();
    $(".filterByDay").hide();
    $(".filterByMonth").hide();
    $(".filterByQuarter").show();

    function setQuarter() {
        var selectedQuarter = $("#selActivityQuarter").find("option:selected").val();
        
        //Set Selected Month Filter
        var startOfQuarter = moment(selectedQuarter, "YYYY-MM").startOf('quarter');
        var endOfQuarter = moment(selectedQuarter, "YYYY-MM").endOf('quarter');
        updateDatepicker("#activityStartDate", startOfQuarter, false);
        updateDatepicker("#activityEndDate", endOfQuarter, false);

        //Refresh only if table was loaded
        if (_activitiesLoaded) {
            refreshPage();
        }
    }
}

function getMinActivityLocalDateTime() {
    var minMoment;
    var tempMoment = moment(_timeZoneGetSelectedInfo().UTCCurrentDateTimeFormat);

    if (_userSummary.MakerMinActivityUTCDateTime) {
        minMoment = moment(_timeZoneConvertUTCToLocal(_userSummary.MakerMinActivityUTCDateTime));
    }

    if (_userSummary.CheckerMinActivityUTCDateTime) {
        tempMoment = moment(_timeZoneConvertUTCToLocal(_userSummary.CheckerMinActivityUTCDateTime));

        if (minMoment.isAfter(tempMoment)) {
            minMoment = tempMoment;
        }
    }

    //if (_userSummary.StakeholderMinActivityUTCDateTime) {
    //    tempMoment = moment(_timeZoneConvertUTCToLocal(_userSummary.StakeholderMinActivityUTCDateTime));
    //}

    return minMoment.format("YYYY-MM-DD HH:mm");
}

function loadMinActivityUTCDateTimeOfUser(customOptions) {
    //Default Options.
    var options = {
        onSuccess: null
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    var filters = getActivityFilters();

    //Call Server
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Checking pending activities of user...",
        name: "[dbo].[spPCTManageActivityDetail]",
        params: [
            { "Name": "@Action", "Value": 'Get Min Activity Detail UTC DateTime by User' },
            { "Name": "@UTCOffsetMinutes", "Value": _timeZoneGetUTCOffsetMinutes() },
            { "Name": "@UserCurrentDateTime", "Value": _timeZoneGetCurrentDateTime() },
            { "Name": "@ContactSOEID", "Value": filters.ContactSOEID }
        ],
        success: {
            fn: function (resultList) {
                if (resultList.length > 0) {

                    //Add properties [MakerMinActivityUTCDateTime], [CheckerMinActivityUTCDateTime], [StakeholderMinActivityUTCDateTime]
                    $.extend(true, _userSummary, resultList[0]);

                    if (options.onSuccess) {
                        options.onSuccess();
                    }
                }
            }
        }
    });
}

function loadActivitySummaryOfUser(customOptions) {
    //Default Options.
    var options = {
        forceRefresh: false,
        loadMinActivityUTCDateTime: true,
        onSuccess: null
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    var filters = getActivityFilters();
    var action = 'Get Activity Detail Totals by User';

    if (!filters.ContactSOEID) {
        action = 'Get Activity Detail Totals by MSGroup';
    }

    //Call Server
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Checking activities of user...",
        name: "[dbo].[spPCTManageActivityDetail]",
        params: [
            { "Name": "@Action", "Value": action },
            //{ "Name": "@Action", "Value": 'Get Next Activity Detail Totals by User' },
            { "Name": "@FilterFrequency", "Value": filters.Frequency },
            { "Name": "@FilterActivityStartDate", "Value": filters.ActivityStartDate },
            { "Name": "@FilterActivityEndDate", "Value": filters.ActivityEndDate },
            { "Name": "@FilterContanctType", "Value": _contactView },
            { "Name": "@UTCOffsetMinutes", "Value": _timeZoneGetUTCOffsetMinutes() },
            { "Name": "@UserCurrentDateTime", "Value": _timeZoneGetCurrentDateTime() },
            { "Name": "@ContactSOEID", "Value": filters.ContactSOEID },
            { "Name": "@SessionSOEID", "Value": _getSOEID() }
        ],
        success: {
            fn: function (resultList) {
                if (resultList.length > 0) {
                    //Add properties [TotalMaker], [TotalChecker], [TotalStakeholder]
                    $.extend(true, _userSummary, resultList[0]);

                    //Calculate tabs Summary of Maker/ Checker and Stakeholder
                    var viewType = _getQueryStringParameter("pviewType");

                    //When user click "My Activities" set view type Contact (Maker/Checker/Stakeholder), "All Activities" viewType = "All"
                    if (!viewType) {
                        viewType = "Contact";
                    }

                    //Set Maker Summary
                    if (parseInt(_userSummary.TotalMaker) > 0) {
                        $('li[contactView="Maker"]').show();
                    } else {
                        //When is pviewType=All is Administrator only hide view for Contact (Maker, Checker or Stakeholder)
                        if (viewType == "Contact") {
                            $('li[contactView="Maker"]').hide();

                            //If the current View of the user don't have activities reset
                            if (_contactView == "Maker") {
                                _contactView = "";
                            }
                        }
                    }
                    $('#lblMakerSummary').html("Maker (" + _userSummary.TotalMaker + ")");

                    //Set Checker Summary
                    if (parseInt(_userSummary.TotalChecker) > 0) {
                        $('li[contactView="Checker"]').show();
                    } else {
                        //When is pviewType=All is Administrator only hide view for Contact (Maker, Checker or Stakeholder)
                        if (viewType == "Contact") {
                            $('li[contactView="Checker"]').hide();

                            //If the current View of the user don't have activities reset
                            if (_contactView == "Checker") {
                                _contactView = "";
                            }
                        }
                    }
                    $('#lblCheckerSummary').html("Checker (" + _userSummary.TotalChecker + ")");

                    //Set Stakeholder Summary
                    if (parseInt(_userSummary.TotalStakeholder) > 0) {
                        $('li[contactView="Stakeholder"]').show();
                    } else {
                        //When is pviewType=All is Administrator only hide view for Contact (Maker, Checker or Stakeholder)
                        if (viewType == "Contact") {
                            $('li[contactView="Stakeholder"]').hide();

                            //If the current View of the user don't have activities reset
                            if (_contactView == "Stakeholder") {
                                _contactView = "";
                            }
                        }
                    }
                    $('#lblStakeholderSummary').html("Stakeholder (" + _userSummary.TotalStakeholder + ")");

                    //Set tab active
                    if (!_contactView) {
                        //Set Cache Contact View
                        var cacheContactView = localStorage.getItem("ActivityFilterContactView");
                        if (parseInt(_userSummary['Total' + cacheContactView]) > 0) {
                            _contactView = cacheContactView;
                            $('[contactView="' + cacheContactView + '"]').addClass("active");
                            $("#tab-" + cacheContactView.toLocaleLowerCase() + "-list").addClass("fade");
                            $("#tab-" + cacheContactView.toLocaleLowerCase() + "-list").addClass("in");
                            $("#tab-" + cacheContactView.toLocaleLowerCase() + "-list").addClass("active");
                        } else {
                            //Set Default Value

                            if (parseInt(_userSummary.TotalMaker) > 0) {
                                _contactView = "Maker";
                                $('[contactView="Maker"]').addClass("active");
                                $("#tab-maker-list").addClass("fade");
                                $("#tab-maker-list").addClass("in");
                                $("#tab-maker-list").addClass("active");
                            }

                            if (!_contactView && parseInt(_userSummary.TotalChecker) > 0) {
                                _contactView = "Checker";
                                $('[contactView="Checker"]').addClass("active");
                                $("#tab-checker-list").addClass("fade");
                                $("#tab-checker-list").addClass("in");
                                $("#tab-checker-list").addClass("active");
                            }

                            if (!_contactView && parseInt(_userSummary.TotalStakeholder) > 0) {
                                _contactView = "Stakeholder";
                                $('[contactView="Stakeholder"]').addClass("active");
                                $("#tab-stakeholder-list").addClass("fade");
                                $("#tab-stakeholder-list").addClass("in");
                                $("#tab-stakeholder-list").addClass("active");
                            }

                            //Update Cache Value
                            localStorage.setItem("ActivityFilterContactView", _contactView);
                        }
                    } else {
                        if (parseInt(_userSummary['Total' + _contactView]) > 0) {
                            $('[contactView="' + _contactView + '"]').addClass("active");
                            $("#tab-" + _contactView.toLocaleLowerCase() + "-list").addClass("fade");
                            $("#tab-" + _contactView.toLocaleLowerCase() + "-list").addClass("in");
                            $("#tab-" + _contactView.toLocaleLowerCase() + "-list").addClass("active");
                        }
                    }

                    if (parseInt(_userSummary.TotalMaker) == 0 &&
                        parseInt(_userSummary.TotalChecker) == 0 &&
                        parseInt(_userSummary.TotalStakeholder) == 0) {

                        $('[contactView="Maker"]').removeClass("active");
                        $("#tab-maker-list").removeClass("active");

                        $('[contactView="Checker"]').removeClass("active");
                        $("#tab-checker-list").removeClass("active");

                        $('[contactView="Stakeholder"]').removeClass("active");
                        $("#tab-stakeholder-list").removeClass("active");

                        _showAlert({
                            id: "NoActivitiesAlert",
                            showTo: ".tab-content",
                            type: "info",
                            content: "No activities found."
                        });

                        //Hide Full Page Loading
                        _hideLoadingFullPage({
                            id: "ActivityPageLoading"
                        });

                        //fix issue options.onSuccess() set true in _activitiesLoaded and is not executed in this if only in else
                        _activitiesLoaded = true;
                    } else {
                        //Load Activity information only when exists activities for the current user
                        if (options.onSuccess) {
                            options.onSuccess();
                        }

                        //Remove alert
                        $("#NoActivitiesAlert").remove();
                    }
                }
            }
        }
    });
}

function updateDatepicker(idElement, newMomentValue, updateTableActivities) {
    //Set default value true
    if (typeof updateTableActivities == "undefined") {
        updateTableActivities = true;
    }

    //Set new value
    $(idElement).datepicker('setDate', newMomentValue.toDate());

    //Validate flagUpdateTable on Event Change Datepicker to update table of activities
    if (updateTableActivities == false) {

        //Avoid multiples update to the table of activities when trigger Update
        $(idElement).attr("flagUpdateTable", "0");
    } 
    
    //Update datepicker is going to trigger Change Event
    //$(idElement).datepicker('update', newMomentValue.toDate());
    $(idElement).datepicker('update');
}

function getPeriod(rowData) {
    //Get moment information
    var momentPeriod = moment(rowData.FrequencyLocalDateTimeSLA);
    var period = "";

    //Check if exists BD in frequencies
    if (rowData.FrequencyType == "BD") {
        //Set Sep-BD5 insted of 5
        momentPeriod = moment(rowData.FrequencyLocalYear + "-" + rowData.FrequencyLocalMonth, "YYYY-MM");
        var bdPeriod = momentPeriod.format("MMM-");
        period = bdPeriod + "BD" + rowData.FrequencyBDNum;
    }

    //Check if need add Quarter
    if (rowData.Frequency == "Quarterly") {
        period = period + momentPeriod.format("[-][Q]Q");
    }

    //Check if it a Daily
    if (rowData.Frequency == "Daily") {
        period = momentPeriod.format('MMM DD, YYYY');
    }

    return period;
}

function loadAuditLogOfActivity() {
    var rowData = $.jqxGridApi.getOneSelectedRow("#tbl" + _contactView + "Activities", true);
    if (rowData) {
        var shareDriveFilesPaths = _parseFolderStructure(rowData);
        var shareDriveFolderToCopy = shareDriveFilesPaths[0].FolderPath;
        var idActivityDetail = rowData.IDActivityDetail;
        var popoverSelector = ".containerAuditLog-" + idActivityDetail;
        var idForm = "formAuditLogInfo-" + idActivityDetail;
        var contentHtml =
            '<button type="button" class="btnGeneratePDF btn btn-success left15 bottom15 pull-right" > ' +
            '    <i class="fa fa-file-pdf-o"></i> Generate PDF ' +
            '</button> ' +
            '<div id="' + idForm + '" class="form-horizontal" style="clear: both;">' +

            '   <div class="divReportHeader" style="display:none; background-color: #004786;height: 60px;margin-bottom: 15px;"> ' +
            '       <img src="' + _fixFrameworkURL('/Content/Shared/images/global_header_logo.png') + '" style="float: left;"> ' +
            '       <h3 style="color:white;padding-left: 88px;padding-top: 20px;">Support Documentation</h3> ' +
            '   </div> ' +

            '   <!--Hidden Field Report Date--> ' +
            '   <div class="form-group row divReportDate" style="display:none; margin-bottom: 0px;"> ' +
            '       <label for="lblReportDate" class="col-md-2 col-form-label"> ' +
            '           <h5><b>Report Date</b></h5> ' +
            '       </label> ' +
            '       <div class="col-md-10"> ' +
            '           <div class="form-control lblReportDate" name="lblReportDate" id="lblReportDate"> ' +
            '               ' + moment().format('MMM D, YYYY') + ' ' +
            '           </div> ' +
            '       </div> ' +
            '   </div> ' +

            //Set ActivityPeriod
            '   <div class="form-group row divActivityPeriod" style="margin-bottom: 0px;"> ' +
            '       <label for="lblActivityPeriod" class="col-md-2 col-form-label"> ' +
            '           <h5><b>Activity Period</b></h5> ' +
            '       </label> ' +
            '       <div class="col-md-10"> ' +
            '           <div class="form-control lblActivityPeriod" name="lblActivityPeriod" id="lblActivityPeriod"> ' +
            '               ' + getPeriod(rowData) + '</b></h5> ' +
            '           </div> ' +
            '       </div> ' +
            '   </div> ' +

            //Set ActivityName
            '   <div class="form-group row divActivityName" style="margin-bottom: 0px;"> ' +
            '       <label for="lblActivityName" class="col-md-2 col-form-label"> ' +
            '           <h5><b>Activity Name</b></h5> ' +
            '       </label> ' +
            '       <div class="col-md-10"> ' +
            '           <div class="form-control lblActivityName" name="lblActivityName" id="lblActivityName"> ' +
            '               ' + rowData.ActivityName + ' ' +
            '           </div> ' +
            '       </div> ' +
            '   </div> ' +

            //Set TeamTreeName
            '   <div class="form-group row divTeamTreeName" style="margin-bottom: 0px;"> ' +
            '       <label for="lblTeamTreeName" class="col-md-2 col-form-label"> ' +
            '           <h5><b>' + rowData.TeamLabelName + '</b></h5> ' +
            '       </label> ' +
            '       <div class="col-md-10"> ' +
            '           <div class="form-control lblTeamTreeName" name="lblTeamTreeName" id="lblTeamTreeName"> ' +
            '               ' + rowData.TeamTree + ' ' +
            '           </div> ' +
            '       </div> ' +
            '   </div> ';

        //Add CFields
        for (var i = 0; i < _contactCFields.length; i++) {
            contentHtml +=
                //Set CFiled
                '   <div class="form-group row divCField" style="margin-bottom: 0px;"> ' +
                '       <label for="lblCField" class="col-md-2 col-form-label"> ' +
                '           <h5><b>' + _contactCFields[i].Description + '</b></h5> ' +
                '       </label> ' +
                '       <div class="col-md-10"> ' +
                '           <div class="form-control" > ' +
                '               ' + rowData[_contactCFields[i].Key + 'Name'] + ' ' +
                '           </div> ' +
                '       </div> ' +
                '   </div> ';
        }

        if (_contactView == "Maker" || _contactView == "Checker") {
            contentHtml +=
            //Set LocationSaved
            '   <div class="form-group row divLocationSaved" style="margin-bottom: 0px;"> ' +
            '       <label for="lblLocationSaved" class="col-md-2 col-form-label"> ' +
            '           <h5><b>Official Record</b></h5> ' +
            '       </label> ' +
            '       <div class="col-md-10"> ' +
            '           <div class="form-control lblLocationSaved" name="lblLocationSaved" id="lblLocationSaved"> ' +
            '               <a href="' + shareDriveFolderToCopy + '" target="_blank">' + shareDriveFolderToCopy + '</a> ' +
            '           </div> ' +
            '       </div> ' +
            '   </div> ';
        }

        contentHtml +=
            '   <br><div class="containerAuditLog-' + idActivityDetail + '">Loading...</div>' +
            '</div>';

        _showModal({
            width: '70%',
            modalId: "modalAuditLog",
            addCloseButton: true,
            title: "Audit Log Information",
            contentHtml: contentHtml,
            onReady: function ($modal) {
                $modal.find(".btnGeneratePDF").click(function () {
                    generateAuditLogPDF($modal);
                });
            }
        });
    }

    //Get information
    getAuditLogsByActivityDetail({
        idActivityDetail: idActivityDetail,
        onSuccess: function (auditLogs) {
            var auditLogHtml = "<div>";
            auditLogHtml += "<ul class='list-group'>"

            for (var i = 0; i < auditLogs.length; i++) {
                var tempData = auditLogs[i];

                auditLogHtml += "<li class='list-group-item row'>";

                if (tempData.Type == "Escalation" ||
                    tempData.Type == "Rejected" ||
                    tempData.Type == "Problem" ||
                    tempData.Type == "Files Deleted") {
                    auditLogHtml += "<b style='color:#f44336;'>" + tempData.Type + "</b><br>";
                } else {
                    auditLogHtml += "<b style='color:#4caf50;'>" + tempData.Type + "</b><br>";
                }

                auditLogHtml += "<b>User:</b> (" + tempData.UserRole + ") " + tempData.CreatedBy + "<br>";
                //auditLogHtml += "<b>File Status:</b> " + tempData.FileStatus + "<br>";
                //auditLogHtml += "<b>Status By Checker:</b> " + tempData.StatusByChecker + "<br>";

                //Add date
                var createdDateFormat = moment(tempData.CreatedDate).format("DD-MMM-YYYY hh:mm A");
                auditLogHtml += "<b>Date:</b> " + createdDateFormat + "<br>";

                //Add Comments
                if (tempData.Comment) {
                    auditLogHtml += (tempData.Type == "Comment" ? "" : "<b>Comment:</b> ") + "<pre style='margin: 0 0 0px;'>" + _escapeToHTML(tempData.Comment) + "</pre>";
                }

                //Add Files
                if (tempData.CountSharedFiles > 0) {
                    var auditLogFiles = JSON.parse(tempData.JsonSharedFiles);

                        auditLogHtml += "<b>Files:</b> <br>";

                    for (var j = 0; j < auditLogFiles.length; j++) {
                        auditLogHtml += " - " + auditLogFiles[j].FileName + "." + auditLogFiles[j].FileExt + "<br>";
                    }
                }

                //Add Checklist
                if (tempData.CountCheckSteps > 0) {
                    var auditLogCheckSteps = JSON.parse(tempData.JsonCheckSteps);

                    auditLogHtml += "<b>Check List '" + auditLogCheckSteps[0].CheckListName + "':</b> <br>";
                    auditLogHtml += "<ul class='list-group'>"

                    for (var j = 0; j < auditLogCheckSteps.length; j++) {
                        auditLogHtml += "<li class='list-group-item'>";
                        auditLogHtml += "   <div class='col-md-1'>#" + auditLogCheckSteps[j].Number + "</div>";
                        auditLogHtml += "   <div class='col-md-9'>" + auditLogCheckSteps[j].Description + "</div>";
                        auditLogHtml += "   <div class='col-md-2'>" + auditLogCheckSteps[j].Status + "</div>";
                        auditLogHtml += "   <div style='clear:both'></div>";
                        auditLogHtml += "</li>";
                    }

                    auditLogHtml += "</ul>"
                } else {
                    if (tempData.Type == "Approved" || tempData.Type == "Rejected") {
                        auditLogHtml += "<b>Check List:</b> Not Required <br>";
                    }
                }

                //Add Sent to Emails
                if (tempData.CountNotificationDetails > 0) {
                    var notificationEmails = JSON.parse(tempData.JsonNotificationDetails);

                    auditLogHtml += "<b>Email Sent to:</b> <br>";

                    for (var j = 0; j < notificationEmails.length; j++) {
                        auditLogHtml += " - " + notificationEmails[j].Type + ": " + notificationEmails[j].Description + "<br>";
                    }
                }

                auditLogHtml += "</li>";
            }

            auditLogHtml += "</ul>"
            auditLogHtml += "</div>";

            //Update html of Popover
            $(popoverSelector).html(auditLogHtml);
        }
    });

    function generateAuditLogPDF($modal) {
        //Show full page Loading
        _showLoadingFullPage({
            idLoading: "ActivityInfoPDF",
            msg: "Generating PDF File.."
        });

        //Generate PDF File Name
        var pdfName = "";
        if (rowData.ActivityName.length > 20) {
            pdfName = rowData.ActivityName.slice(0, 10) + "~" + rowData.ActivityName.substr(rowData.ActivityName.length - 10) + " " + moment().format("YYYY-MM-DD");
        } else {
            pdfName = rowData.ActivityName + " " + moment().format("YYYY-MM-DD");
        }

        //Show Hidden fields
        $modal.find(".divReportHeader").show();
        $modal.find(".divReportDate").show();

        //Hide icons
        $modal.find(".fa").hide();

        _generatePDFFromHtml("#" + idForm, pdfName, function () {
            //Hide Hidden fields
            $modal.find(".divReportHeader").hide();
            $modal.find(".divReportDate").hide();

            //Show icons
            $modal.find(".fa").show();

            //Hide full page Loading
            _hideLoadingFullPage({
                idLoading: "ActivityInfoPDF"
            });
        });
    }
}

function loadTableActivities() {

    getActivities({
        onSuccess: function (activityList) {
            var activitiesHaveBD = false;
            var columnDefinitions = [];
            var filter = getActivityFilters();
            var idTable = "#tbl" + _contactView + "Activities";
            var groupCols = ['TeamTree'];
            var groupLabelName = "Team Name";
            var groupColsCacheJson = localStorage.getItem("ActivityGroupedCols");
            if (groupColsCacheJson) {
                groupCols = JSON.parse(groupColsCacheJson);
            }

            //Set Group Label Name
            if (activityList.length > 0) {
                groupLabelName = activityList[0].TeamLabelName;
            }

            //Check if any activity have Business Day
            for (var i = 0; i < activityList.length; i++) {
                
                //Check if exists BD in frequencies
                if (activityList[i].FrequencyType == "BD") {
                    activitiesHaveBD = true;
                } 

                //Generate Period
                activityList[i].FrequencyPeriod = getPeriod(activityList[i]);
            }

            //Add Select Column only when need is Checker
            if (_contactView == "Checker") {
                columnDefinitions.push({
                    name: 'IsSelected',
                    text: "Select",
                    type: 'bool',
                    columntype: 'checkbox',
                    width: '50px',
                    pinned: true,
                    filterable: false
                });

                //Add selected value to array
                //for (var i = 0; i < processList.length; i++) {
                //    processList[i].IsSelected = true;
                //}
            } else {
                $(".btnToggleSelectAll").hide();
            }

            //Add Columns
            columnDefinitions.push({ name: 'IDActivityDetail', type: 'number', hidden: 'true' });
            columnDefinitions.push({ name: 'IDActivity', type: 'number', hidden: 'true' });
            columnDefinitions.push({ name: 'IDChecklistChecker', type: 'number', hidden: 'true' });
            columnDefinitions.push({ name: 'IDTeam', type: 'number', hidden: 'true' });
            columnDefinitions.push({ name: 'TeamName', type: 'string', hidden: 'true' });
            //columnDefinitions.push({ name: 'TeamTree', type: 'string', hidden: 'true' });
            columnDefinitions.push({ name: 'TeamSharedFolderPath', type: 'string', hidden: 'true' });
            columnDefinitions.push({ name: 'TeamFolderStructure', type: 'string', hidden: 'true' });
            columnDefinitions.push({ name: 'TeamLabelName', type: 'string', hidden: 'true' });

            columnDefinitions.push({ name: 'JsonSharedDriveFiles', type: 'string', hidden: 'true' });
            columnDefinitions.push({ name: 'JsonMakerContacts', type: 'string', hidden: 'true' });
            columnDefinitions.push({ name: 'JsonCheckerContacts', type: 'string', hidden: 'true' });
            columnDefinitions.push({ name: 'JsonStakeholderContacts', type: 'string', hidden: 'true' });
            columnDefinitions.push({ name: 'JsonAdminContacts', type: 'string', hidden: 'true' });
            columnDefinitions.push({ name: 'CountAuditLogs', type: 'number', hidden: 'true' });

            columnDefinitions.push({ name: 'ActivityDetailMakerBy', type: 'string', hidden: 'true' });
            columnDefinitions.push({ name: 'ActivityDetailCheckerBy', type: 'string', hidden: 'true' });

            columnDefinitions.push({ name: 'FrequencyCode', type: 'string', hidden: 'true' });
            columnDefinitions.push({ name: 'FrequencyType', type: 'string', hidden: 'true' });
            columnDefinitions.push({ name: 'FrequencyBDNum', type: 'number', hidden: 'true' });
            columnDefinitions.push({ name: 'FrequencyUTCDateTimeSLA', type: 'date', hidden: 'true' });
            columnDefinitions.push({ name: 'FrequencyLocalMonth', type: 'number', hidden: 'true' });
            columnDefinitions.push({ name: 'FrequencyLocalYear', type: 'number', hidden: 'true' });

            //Add dinamic CFields
            for (var i = 0; i < _contactCFields.length; i++) {
                columnDefinitions.push({ name: 'ID' + _contactCFields[i].Key, type: 'number', hidden: 'true' });
                columnDefinitions.push({ name: _contactCFields[i].Key + 'Name', type: 'string', hidden: 'true' });
            }

            columnDefinitions.push({ name: 'ActivityDetailUTCDateTimeCompleted', type: 'date', hidden: 'true' });
            columnDefinitions.push({
                name: 'ActivityDetailStatus', text: 'Activity Status', width: '165px', type: 'string', filtertype: 'checkedlist', editable: false, filterable: true,
                cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                    var htmlResult = '';
                    var badgeClass = "badge-warning";

                    //Add Status
                    switch (rowData.ActivityDetailStatus) {
                        case "Completed":
                            badgeClass = "badge-success";
                            break;

                        case "Rejected":
                            badgeClass = "badge-danger";
                            break;

                        case "Pending Checker":
                        case "Problem":
                            badgeClass = "badge-info";
                            break;

                        case "Pending Maker":
                            badgeClass = "badge-warning";
                            break;
                    }

                    htmlResult += '<span class="badge badge-md ' + badgeClass + '" style="margin-top: 5px; margin-left: 5px;">' + rowData.ActivityDetailStatus + '</span>';

                    //Add History of comments
                    if (rowData.CountAuditLogs > 0) {
                        var htmlAuditLog = "<div class='auditLogPopup-" + rowData.IDActivityDetail + "'>Loading...</div>";
                        htmlResult +=
                            '<div style="float: right; margin-left: 2px; cursor: pointer;"> ' +
                            '       <img onclick="loadAuditLogOfActivity()" style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png"/> ' +
                            '</div>';
                    }

                    return htmlResult;
                }
            });
            columnDefinitions.push({
                name: 'ActivityDetailSLAStatus', text: 'SLA Status', width: '90px', type: 'string', filtertype: 'checkedlist', editable: false, filterable: true,
                cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                    var htmlResult = '';
                    var badgeClass = "badge-warning";

                    switch (rowData.ActivityDetailSLAStatus) {
                        case "On Time":
                            badgeClass = "badge-warning";
                            break;

                        case "Late":
                            badgeClass = "badge-danger";
                            break;
                    }

                    if (rowData.ActivityDetailStatus == "Completed") {
                        badgeClass = "badge-success";
                    }

                    if (rowData.ActivityDetailStatus == "Problem") {
                        badgeClass = "badge-info";
                    }

                    htmlResult = '<span style="line-height: 30px; margin-left: 10px;"><span class="badge badge-md ' + badgeClass + '">' + rowData.ActivityDetailSLAStatus + '</span></span>';

                    return htmlResult;
                }
            });
            columnDefinitions.push({ name: 'FrequencyLocalDateTimeSLA', text: 'SLA Datetime', type: 'date', filtertype: 'range', editable: false, cellsformat: 'yyyy-MM-dd hh:mm tt', width: '160px' });
            columnDefinitions.push({ name: 'ActivityName', text: 'Activity Name', filtertype: 'input', type: 'string', width: '400px' });
            
            if (activitiesHaveBD) {
                columnDefinitions.push({
                    name: 'FrequencyPeriod', text: 'Period', width: '120px', type: 'string', filtertype: 'checkedlist', editable: false, filterable: true,
                    cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                        var htmlResult = '';
                        var badgeClass = "badge-info";

                        if (rowData.FrequencyPeriod) {
                            htmlResult = '<span style="line-height: 30px; margin-left: 10px;"><span class="badge badge-md ' + badgeClass + '">' + rowData.FrequencyPeriod + '</span></span>';
                        }

                        return htmlResult;
                    }
                });
            }
            
            //Whe "All" Frequencies show columns
            if (!filter.Frequency) {
                columnDefinitions.push({ name: 'Frequency', text: 'Frequency', filtertype: 'checkedlist', type: 'string', width: '80px' });
            } else {
                //When Frequency selected by de user hide column
                columnDefinitions.push({ name: 'Frequency', type: 'string', hidden: 'true' });
            }

            columnDefinitions.push({ name: 'TeamTree', text: groupLabelName, filtertype: 'checkedlist', type: 'string', width: '200px' });
            columnDefinitions.push({
                name: 'CountSharedDriveFiles', text: 'Activity Files', width: '90px', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center',
                cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                    var htmlResult = '<span style="line-height:31px;margin-left:25px;">' + rowData.CountSharedDriveFiles + '</span>';

                    //Add SharedDriveFiles
                    if (rowData.JsonSharedDriveFiles) {
                        var rows = JSON.parse(rowData.JsonSharedDriveFiles);
                        if (rows.length > 0) {
                            var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                            for (var i = 0; i < rows.length; i++) {
                                var tempData = rows[i];
                                infoHtml += " \n " + tempData.FileName + "." + tempData.FileExt + "<br>";
                            }

                            infoHtml += "</div>";

                            htmlResult +=
                                '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="Files Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                '       <img style="margin-right: 25px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/info.png"/> ' +
                                '</div>';
                        }
                    }

                    return htmlResult;
                }
            });
            columnDefinitions.push({
                name: 'CountMakerContacts', text: 'Makers', width: '90px', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center',
                cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                    var htmlResult = '<span style="line-height:31px;margin-left:25px;">' + rowData.CountMakerContacts + '</span>';

                    //Add Makers
                    htmlResult += _getHtmlContactPopupByJson(rowData.JsonMakerContacts);

                    return htmlResult;
                }
            });
            columnDefinitions.push({
                name: 'CountCheckerContacts', text: 'Checkers', width: '90px', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center',
                cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                    var htmlResult = '<span style="line-height:31px;margin-left:25px;">' + rowData.CountCheckerContacts + '</span>';

                    //Add Checkers
                    htmlResult += _getHtmlContactPopupByJson(rowData.JsonCheckerContacts);

                    return htmlResult;
                }
            });
            columnDefinitions.push({
                name: 'CountStakeholderContacts', text: 'Stakeholders', width: '90px', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center',
                cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                    var htmlResult = '<span style="line-height:31px;margin-left:25px;">' + rowData.CountStakeholderContacts + '</span>';

                    //Add Stakeholders
                    htmlResult += _getHtmlContactPopupByJson(rowData.JsonStakeholderContacts);

                    return htmlResult;
                }
            });
            columnDefinitions.push({
                name: 'CountAdminContacts', text: 'Admins', width: '90px', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center',
                cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                    var htmlResult = '<span style="line-height:31px;margin-left:25px;">' + rowData.CountAdminContacts + '</span>';

                    //Add Admins
                    htmlResult += _getHtmlContactPopupByJson(rowData.JsonAdminContacts);

                    return htmlResult;
                }
            });

            $.jqxGridApi.create({
                showTo: idTable,
                options: {
                    //for comments or descriptions
                    height: "500",
                    autoheight: false,
                    autorowheight: false,
                    selectionmode: "singlerow",
                    showfilterrow: true,
                    sortable: true,
                    editable: true,
                    groupable: true
                },
                source: {
                    // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                    dataBinding: "Large Data Set Local",
                    rows: activityList
                },
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                groups: groupCols,
                columns: columnDefinitions,
                ready: function () {
                    var $jqxGrid = $(idTable);

                    //Add Popup to see auditLogs
                    $jqxGrid.on("rowclick", function (event) {
                        //Get RowData
                        var rowClickedData = event.args.row.bounddata;

                        //Show tooltips
                        _GLOBAL_SETTINGS.tooltipsPopovers();

                        //Show Check List / Show normal Approve or Reject
                        if (rowClickedData.IDChecklistChecker) {
                            $(".btnApprove").hide();
                            $(".btnReject").hide();
                            $(".btnCopyLinkToClipboard").hide();
                            $(".btnReview").show();
                        } else {
                            $(".btnApprove").show();
                            $(".btnReject").show();
                            $(".btnCopyLinkToClipboard").show();
                            $(".btnReview").hide();
                        }
                    });

                    //Remember Group configuration of User
                    $jqxGrid.on("groupschanged", function (event) {
                        // groups array.
                        var groups = args.groups;

                        // Save in localstorage
                        localStorage.setItem("ActivityGroupedCols", JSON.stringify(groups));
                    });     

                    // expand all groups.
                    $jqxGrid.jqxGrid('expandallgroups');

                    // Use to avoid multiples loading of the table on first load of page
                    _activitiesLoaded = true;

                    //Set Select All 
                    //$(".btnToggleSelectAll").html('<i class="fa fa-square-o"></i> Select All ');
                    //$(".btnToggleSelectAll").attr("updateSelectTo", "1");
                }
            });
        }
    });
}

function getActivityFilters() {
    var filters = {
        Frequency: "",
        Year: null,
        Month: null,
        Quarter: null,
        ActivityStartDate: null,
        ActivityEndDate: null,
        ContactSOEID: "",
        SharedFilesStatus: "",
        CheckerStatus: "",
        SLAStatus: ""
    };

    //Get Frequency Filter
    var frequencyValue = $("#selFrequency").val();
    if (frequencyValue != "All") {
        filters["Frequency"] = frequencyValue;
    }

    //Get Year Filter
    filters["Year"] = $("#selYear").val();
    if (!filters["Year"]) {
        filters["Year"] = "0";
    }

    //Get Month Filter
    filters["Month"] = $("#selMonth").val();
    if (!filters["Month"]) {
        filters["Month"] = "0";
    }

    //Get Quarter Filter
    filters["Quarter"] = $("#selQuarter").val();
    if (!filters["Quarter"]) {
        filters["Quarter"] = "0";
    }

    //Get Activity Start Date
    var activityStartDateInput = $("#activityStartDate").val();
    var momentActivityStartDate = moment(activityStartDateInput, 'MMM DD, YYYY');
    filters["ActivityStartDate"] = momentActivityStartDate.format("YYYY-MM-DD");

    //Get Activity End Date
    var activityEndDateInput = $("#activityEndDate").val();
    var momentActivityEndDate = moment(activityEndDateInput, 'MMM DD, YYYY');
    filters["ActivityEndDate"] = momentActivityEndDate.format("YYYY-MM-DD");

    //Get Shared Files Status Filter
    var sharedFilesStatus = $("#selSharedFilesStatus").val();
    if (sharedFilesStatus != "All") {
        filters["SharedFilesStatus"] = sharedFilesStatus;
    } 

    //Get Checker Status Filter
    var checkerStatus = $("#selCheckerStatus").val();
    if (checkerStatus != "All") {
        filters["CheckerStatus"] = checkerStatus;
    }

    //Get SLA Status Filter
    var slaStatus = $("#selSLAStatus").val();
    if (slaStatus != "All") {
        filters["SLAStatus"] = slaStatus;
    }

    //Get User SOEID
    var contactSOEID = $("#selActivityUser select").val();
    if(contactSOEID){
        filters["ContactSOEID"] = contactSOEID;
    }
    
    return filters;
}

function getActivities(customOptions) {

    //Default Options.
    var options = {
        loadingMsgType: "fullLoading",
        onSuccess: null
    };

    //Do merge of customOptions with default options.
    $.extend(true, options, customOptions);

    var spParams = [];
    var filters = getActivityFilters();

    //Set Required Parameters

    if (filters.ContactSOEID) {
        spParams.push({ "Name": "@Action", "Value": 'List Activity Details by User' });
    } else {
        spParams.push({ "Name": "@Action", "Value": 'List Activity Details by MSGroup' });
    }
    
    //spParams.push({ "Name": "@Action", "Value": 'List Next Activity Details by User' });
    spParams.push({ "Name": "@FilterFrequency", "Value": filters.Frequency });
    spParams.push({ "Name": "@FilterActivityStartDate", "Value": filters.ActivityStartDate });
    spParams.push({ "Name": "@FilterActivityEndDate", "Value": filters.ActivityEndDate });
    spParams.push({ "Name": "@FilterContanctType", "Value": _contactView });
    spParams.push({ "Name": "@UTCOffsetMinutes", "Value": _timeZoneGetUTCOffsetMinutes() });
    spParams.push({ "Name": "@UserCurrentDateTime", "Value": _timeZoneGetCurrentDateTime() });
    spParams.push({ "Name": "@ContactSOEID", "Value": filters.ContactSOEID });
    spParams.push({ "Name": "@SessionSOEID", "Value": _getSOEID() });

    //TODO: Create frequency filters
    //Daily
    //if (filters.Frequency == "Daily") {
    //    spParams.push({ "Name": "@Action", "Value": 'List Activity Details by User' });
    //    spParams.push({ "Name": "@FilterFrequency", "Value": filters.Frequency });
    //    spParams.push({ "Name": "@FilterActivityStartDate", "Value": filters.ActivityStartDate });
    //    spParams.push({ "Name": "@FilterActivityEndDate", "Value": filters.ActivityEndDate });
    //}

    //Monthly
    //if (filters.Frequency == "Monthly") {
        
    //}

    //Quarterly
    //if (filters.Frequency == "Quarterly") {
     
    //}

    //Call Server
    _callProcedure({
        loadingMsgType: options.loadingMsgType,
        loadingMsg: "Getting activities...",
        name: "[dbo].[spPCTManageActivityDetail]",
        params: spParams,
        success: {
            fn: function (activityList) {
                if (options.onSuccess) {
                    options.onSuccess(activityList);
                }
            }
        }
    });
}

function copyLinkToClipboard() {
    var rowData = $.jqxGridApi.getOneSelectedRow("#tbl" + _contactView + "Activities", true);
    if (rowData) {
        var shareDriveFilesPaths = _parseFolderStructure(rowData);
        var shareDriveFolderToCopy = shareDriveFilesPaths[0].FolderPath;

        _copyToClipboard(shareDriveFolderToCopy);
        _showNotification("success", "Link: '" + shareDriveFolderToCopy + "' was copied succesfully to clipboard.", "CopyLink");

        createIfNotExistsFolderPath({
            folderPath: shareDriveFolderToCopy,
            onSuccess: function (response) {
                console.log(response);
            }
        });
    }
}

function createIfNotExistsFolderPath(options) {
    _callServer({
        loadingMsgType: "topBar",
        loadingMsg: "Checking folder full path...",
        url: '/ProcessControlTool/CreateIfNotExistsFolderPath',
        data: {
            typeAuthentication: _getTypeAuthentication(),
            user: _encodeSkipSideminder(_getWindowsUser()),
            pass: _getWindowsPassword(),
            folderPath: _encodeSkipSideminder(options.folderPath)
        },
        success: function (respond) {
            if (options.onSuccess) {
                options.onSuccess(respond);
            }
        }
    });
}

function openModalSendEmailReminderOrEscalation(typeNotification) {
    var idActivityDetails = [];
    var idAuditLogs = [];
    var contactSOEIDUnique = {};
    var managerSOEIDUnique = {};
    var contactToSendEmail = [];
    var activities = _getCheckedRows("#tbl" + _contactView + "Activities");
    var htmlContentModal = "";

    if (activities.length > 0) {
        htmlContentModal += "<p><b>(" + activities.length + ") Activities selected. </b></p>";
 
        if (typeNotification == "Reminder") {
            htmlContentModal += "<p>This notification will be sent to the following Makers: </p>";
        }

        if (typeNotification == "Escalation") {
            htmlContentModal += "<p>This notification will be sent to the following Managers and Makers: </p>";
        }

        //Get selected activities
        var tempMakers;
        for (var i = 0; i < activities.length; i++) {
            //Add Process file detail ID
            idActivityDetails.push(activities[i]["IDActivityDetail"]);

            if (activities[i]["CountMakerContacts"] > 0) {
                tempMakers = JSON.parse(activities[i]["JsonMakerContacts"]);

                //Remove DLs Users
                tempMakers = _findAllObjByProperty(tempMakers, "Type", "User");

                //Add Makers of selected activity
                for (var j = 0; j < tempMakers.length; j++) {

                    //Validate unique SOEIDs
                    if (!_contains(tempMakers[j]["Description"], "NOT FOUND")) {
                        if (!contactSOEIDUnique[tempMakers[j]["ID"]]) {

                            if (typeNotification == "Reminder") {
                                htmlContentModal += " - " + tempMakers[j]["Description"] + "<br/>";
                            }

                            contactToSendEmail.push(tempMakers[j]);
                            contactSOEIDUnique[tempMakers[j]["ID"]] = "1";

                            //Add Manager
                            managerSOEIDUnique[tempMakers[j]["ManagerSOEID"]] = {
                                ManagerName: tempMakers[j]["ManagerName"]
                            };
                        }
                    }
                }
            }
        }

        //Add Managers when is Escalation
        if (typeNotification == "Escalation") {
            var managerSOEIDs = Object.keys(managerSOEIDUnique);
            for (var i = 0; i < managerSOEIDs.length; i++) {
                var managerEmployees = _findAllObjByProperty(contactToSendEmail, "ManagerSOEID", managerSOEIDs[i]);

                htmlContentModal += " (Manager) " + managerSOEIDUnique[managerSOEIDs[i]].ManagerName + " [" + managerSOEIDs[i] + "]:<br/>";

                for (var n = 0; n < managerEmployees.length; n++) {
                    htmlContentModal += " - " + managerEmployees[n]["Description"] + "<br/>";
                }

                htmlContentModal += "<br/>";
            }
        }

        htmlContentModal += "<textarea class='form-control txtCommentNotification' placeholder='Add comment (Optional)' style='height:80px !important;'></textarea>";

        //htmlContentModal += "<br/><p>If you need remove some one from the list please contact your administrator.</p>";

        _showModal({
            width: '50%',
            modalId: "modalSendReminder",
            addCloseButton: true,
            buttons: [{
                name: "Send notification",
                class: "btn-success",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    var commentReminder = ($modal.find(".txtCommentNotification").val() ? $modal.find(".txtCommentNotification").val() : "");
                    
                    //Add to AuditLog for each Activity Selected
                    for (var i = 0; i < activities.length; i++) {

                        //Update Activity
                        addActivityDetail({
                            forIndex: i,
                            idActivityDetail: activities[i].IDActivityDetail,
                            idActivity: activities[i].IDActivity,
                            utcDateTimeSLA: moment(activities[i].FrequencyUTCDateTimeSLA).format("YYYY-MM-DD HH:mm"),
                            utcDateTimeCompleted: "",
                            status: activities[i].ActivityDetailStatus,
                            onSuccess: function (objActivityDetail, forIndex) {
                                activities[forIndex].IDActivityDetail = objActivityDetail.IDActivityDetail;

                                addAuditLog({
                                    idActivityDetail: objActivityDetail.IDActivityDetail,
                                    type: typeNotification + " Notification",
                                    status: objActivityDetail.ActivityDetailStatus,
                                    comment: commentReminder,
                                    userRole: getUserRoleAuditLog(),
                                    onSuccess: function (objAuditLogDB) {
                                        idAuditLogs.push(objAuditLogDB.ID);
                                    }
                                });
                            }
                        });
                    }

                    //On all Audit Log saved 
                    _execOnAjaxComplete(function () {
                        var counterMakersProcessed = 0;

                        //Send email foreach maker    
                        for (var k = 0; k < contactToSendEmail.length; k++) {
                            var tempContact = contactToSendEmail[k];

                            _callProcedure({
                                loadingMsgType: "fullLoading",
                                loadingMsg: "Sending " + typeNotification + " Email to '" + tempContact["Description"] + "'...",
                                name: "[dbo].[spPCTSendEmailMaker]",
                                params: [
                                    { "Name": "@IDAuditLogs", "Value": idAuditLogs.join(",") },
                                    { "Name": "@MakerSOEID", "Value": tempContact["ID"] },
                                    { "Name": "@MakerName", "Value": tempContact["Description"] },
                                    { "Name": "@MakerEmail", "Value": tempContact["Email"] },
                                    { "Name": "@TypeNotification", "Value": typeNotification },
                                    { "Name": "@ServerURL", "Value": _getViewVar("ServerURL") }
                                ],
                                success: {
                                    fn: function (responseList) {
                                        counterMakersProcessed++;
                                        callRefreshRows();
                                    }
                                }
                            });
                        }

                        function callRefreshRows() {
                            //Call refresh row until send last email to makers
                            if (counterMakersProcessed == contactToSendEmail.length) {

                                //Refresh all row selected
                                for (var i = 0; i < activities.length; i++) {
                                    refreshRow({
                                        rowData: activities[i],
                                        onSuccess: function () { }
                                    });
                                }

                                //On all rows updated
                                _execOnAjaxComplete(function () {
                                    //Close Modal
                                    $modal.find(".close").click();

                                    //Show user message
                                    _showAlert({
                                        id: "CommentSuccess",
                                        showTo: $("#tbl" + _contactView + "Activities").parent(),
                                        type: "success",
                                        content: typeNotification + " Notification was sent successfully."
                                    });
                                });
                            }
                        }
                    });
                }
            }],
            title: "Are you sure you want to send '" + typeNotification + "' notification?",
            contentHtml: htmlContentModal
        });
    } else {
        _showNotification("error", "No activities selected", "SelectProcessFile");
    }
}

function openModalProblem() {
    var rowData = $.jqxGridApi.getOneSelectedRow("#tbl" + _contactView + "Activities", true);
    if (rowData) {
        var shareDriveFilesPaths = _parseFolderStructure(rowData);
        var shareDriveFolderToCopy = shareDriveFilesPaths[0].FolderPath;
        var htmlContentModal = '';
        var bdPeriod = '';

        if (rowData["FrequencyType"] == "BD") {
            var momentBDPeriod = moment(rowData.FrequencyLocalYear + "-" + rowData.FrequencyLocalMonth, "YYYY-MM");
            bdPeriod = momentBDPeriod.format("MMM-") + "BD" + rowData.FrequencyBDNum;
            //htmlContentModal += "<b>Period: </b> " + bdPeriod + "<br>";
        }

        htmlContentModal += "<b>" + rowData["TeamLabelName"] + ": </b> " + rowData["TeamTree"] + " <br>";

        for (var i = 0; i < shareDriveFilesPaths.length; i++) {
            var tempFileName = shareDriveFilesPaths[i]["FileName"] + "." + shareDriveFilesPaths[i]["FileExt"];
            htmlContentModal += "<b>Activity File " + (i + 1) + ": </b> <span class='txtFileName'>" + tempFileName + "</span><br>";
        }

        htmlContentModal += "<b>Uploaded Location: </b> <span class='txtFolderPath'>" + shareDriveFolderToCopy + "</span> <br><br>";
        htmlContentModal += "<b>Comment: </b> <br/><textarea id='txtContactComment' placeholder='(Required)' style='width:100%;height: 130px;'></textarea><br/>";

        _showModal({
            width: '50%',
            modalId: "modalApproval",
            addCloseButton: true,
            buttons: [{
                name: '<i class="fa fa-save"></i> Save',
                class: 'btn-success',
                closeModalOnClick: false,
                onClick: function ($modal) {
                    var idAuditLog = 0;
                    var contactComment = $modal.find("#txtContactComment").val();
                    var utcDateTimeCompleted = "";
                    if (rowData.ActivityDetailUTCDateTimeCompleted) {
                        utcDateTimeCompleted = moment(rowData.ActivityDetailUTCDateTimeCompleted).format("YYYY-MM-DD HH:mm");
                    }

                    //When Comments is required
                    if (contactComment) {

                        //Update Activity
                        addActivityDetail({
                            idActivityDetail: rowData.IDActivityDetail,
                            idActivity: rowData.IDActivity,
                            utcDateTimeSLA: moment(rowData.FrequencyUTCDateTimeSLA).format("YYYY-MM-DD HH:mm"),
                            utcDateTimeCompleted: utcDateTimeCompleted,
                            status: "Problem",
                            onSuccess: function (objActivityDetail) {
                                //Update Activity Detail info to rowData
                                rowData.IDActivityDetail = objActivityDetail.IDActivityDetail;

                                //Add to AuditLog for Activity Files
                                addAuditLog({
                                    idActivityDetail: rowData.IDActivityDetail,
                                    type: "Problem",
                                    status: rowData.ActivityDetailStatus,
                                    comment: contactComment,
                                    userRole: getUserRoleAuditLog(),
                                    onSuccess: function (objAuditLogDB) {
                                        //Set idAuditLog
                                        idAuditLog = objAuditLogDB.ID;
                                    }
                                });
                            }
                        });

                        //On all saved close modal
                        _execOnAjaxComplete(function () {

                            //Send notification
                            sendEmail({
                                idAuditLog: idAuditLog,
                                typeNotification: "Comment",
                                onSuccess: function () {

                                    refreshRow({
                                        rowData: rowData,
                                        onSuccess: function () {

                                            //Close Modal
                                            $modal.find(".close").click();

                                            //Show user message
                                            _showAlert({
                                                id: "CommentSuccess",
                                                showTo: $("#tbl" + _contactView + "Activities").parent(),
                                                type: "success",
                                                content: "Comment for Activity '" + rowData.ActivityName + "' was saved successfully."
                                            });
                                        }
                                    });
                                }
                            });
                        });

                    } else {
                        _showAlert({
                            id: "CommentValidation",
                            showTo: $modal.find(".modal-body"),
                            type: "error",
                            content: "Comment cannot be empty."
                        });
                    }
                }
            }],
            title: "Report problem to activity",
            contentHtml: htmlContentModal
        });

    }
}

function openModalComment() {
    var rowData = $.jqxGridApi.getOneSelectedRow("#tbl" + _contactView + "Activities", true);
    if (rowData) {
        var shareDriveFilesPaths = _parseFolderStructure(rowData);
        var shareDriveFolderToCopy = shareDriveFilesPaths[0].FolderPath;
        var htmlContentModal = '';
        var bdPeriod = '';

        if (rowData["FrequencyType"] == "BD") {
            var momentBDPeriod = moment(rowData.FrequencyLocalYear + "-" + rowData.FrequencyLocalMonth, "YYYY-MM");
            bdPeriod = momentBDPeriod.format("MMM-") + "BD" + rowData.FrequencyBDNum;
            //htmlContentModal += "<b>Period: </b> " + bdPeriod + "<br>";
        }

        htmlContentModal += "<b>" + rowData["TeamLabelName"] + ": </b> " + rowData["TeamTree"] + " <br>";

        for (var i = 0; i < shareDriveFilesPaths.length; i++) {
            var tempFileName = shareDriveFilesPaths[i]["FileName"] + "." + shareDriveFilesPaths[i]["FileExt"];
            htmlContentModal += "<b>Activity File " + (i + 1) + ": </b> <span class='txtFileName'>" + tempFileName + "</span><br>";
        }

        htmlContentModal += "<b>Uploaded Location: </b> <span class='txtFolderPath'>" + shareDriveFolderToCopy + "</span> <br><br>";
        htmlContentModal += "<b>Comment: </b> <br/><textarea id='txtContactComment' placeholder='(Required)' style='width:100%;height: 130px;'></textarea><br/>";

        _showModal({
            width: '50%',
            modalId: "modalApproval",
            addCloseButton: true,
            buttons: [{
                name: '<i class="fa fa-save"></i> Save',
                class: 'btn-success',
                closeModalOnClick: false,
                onClick: function ($modal) {
                    var idAuditLog = 0;
                    var contactComment = $modal.find("#txtContactComment").val();
                    var utcDateTimeCompleted = "";
                    if (rowData.ActivityDetailUTCDateTimeCompleted) {
                        utcDateTimeCompleted = moment(rowData.ActivityDetailUTCDateTimeCompleted).format("YYYY-MM-DD HH:mm");
                    }
                    
                    //When Comments is required
                    if (contactComment) {

                        //Update Activity
                        addActivityDetail({
                            idActivityDetail: rowData.IDActivityDetail,
                            idActivity: rowData.IDActivity,
                            utcDateTimeSLA: moment(rowData.FrequencyUTCDateTimeSLA).format("YYYY-MM-DD HH:mm"),
                            utcDateTimeCompleted: utcDateTimeCompleted,
                            status: rowData.ActivityDetailStatus,
                            onSuccess: function (objActivityDetail) {
                                //Update Activity Detail info to rowData
                                rowData.IDActivityDetail = objActivityDetail.IDActivityDetail;

                                //Add to AuditLog for Activity Files
                                addAuditLog({
                                    idActivityDetail: rowData.IDActivityDetail,
                                    type: "Comment",
                                    status: rowData.ActivityDetailStatus,
                                    comment: contactComment,
                                    userRole: getUserRoleAuditLog(),
                                    onSuccess: function (objAuditLogDB) {
                                        //Set idAuditLog
                                        idAuditLog = objAuditLogDB.ID;
                                    }
                                });
                            }
                        });

                        //On all saved close modal
                        _execOnAjaxComplete(function () {

                            //Send notification
                            sendEmail({
                                idAuditLog: idAuditLog,
                                typeNotification: "Comment",
                                onSuccess: function () {

                                    refreshRow({
                                        rowData: rowData,
                                        onSuccess: function () {

                                            //Close Modal
                                            $modal.find(".close").click();

                                            //Show user message
                                            _showAlert({
                                                id: "CommentSuccess",
                                                showTo: $("#tbl" + _contactView + "Activities").parent(),
                                                type: "success",
                                                content: "Comment for Activity '" + rowData.ActivityName + "' was saved successfully."
                                            });
                                        }
                                    });
                                }
                            });
                        });

                    } else {
                        _showAlert({
                            id: "CommentValidation",
                            showTo: $modal.find(".modal-body"),
                            type: "error",
                            content: "Comment cannot be empty."
                        });
                    }
                }
            }],
            title: "Add Comment",
            contentHtml: htmlContentModal
        });

    }
}

function openModalApproval(approvalStatus) {
    var rowData = $.jqxGridApi.getOneSelectedRow("#tbl" + _contactView + "Activities", true);
    if (rowData) {

        //Declare values with Approved Status
        var idAuditLog = 0;
        var utcCompletionDateTime = moment().utc().format("YYYY-MM-DD HH:mm");
        var activityDetailStatus = "Completed";
        var typeNotification = "Stakeholder Notification";
        var btnName = "<i class='fa fa-check-square-o'></i> Approve";
        var btnClass = "btn-success";
        var commentPlaceholder = "(Optional)";
        var action = "approve";

        //Whe is rejected
        if (approvalStatus == "Rejected") {
            utcCompletionDateTime = "";
            activityDetailStatus = "Rejected";
            typeNotification = "Activity Rejected";
            btnName = "<i class='fa fa-times-circle'></i> Reject";
            btnClass = "btn-danger";
            commentPlaceholder = "(Required)";
            action = "reject";
        }

        if ((rowData["ActivityDetailStatus"] == "Pending Maker") ||
            (rowData["ActivityDetailStatus"] == "Problem") ||
            (rowData["ActivityDetailStatus"] == "Pending Checker") ||
            (rowData["ActivityDetailStatus"] == "Completed" && approvalStatus == "Rejected") ||
            (rowData["ActivityDetailStatus"] == "Rejected" && approvalStatus == "Approved")
        ) {

            if (rowData["ActivityDetailStatus"] == "Pending Checker" ||
                rowData["ActivityDetailStatus"] == "Completed" ||
                rowData["ActivityDetailStatus"] == "Rejected"
            ) {

                //Validate Checker different from Maker
                if (rowData["ActivityDetailMakerBy"].toLocaleLowerCase() != _getSOEID().toLocaleLowerCase()) {

                    var shareDriveFilesPaths = _parseFolderStructure(rowData);
                    var shareDriveFolderToCopy = shareDriveFilesPaths[0].FolderPath;

                    var htmlContentModal = '';
                    var bdPeriod = '';

                    if (rowData["FrequencyType"] == "BD") {
                        var momentBDPeriod = moment(rowData.FrequencyLocalYear + "-" + rowData.FrequencyLocalMonth, "YYYY-MM");
                        bdPeriod = momentBDPeriod.format("MMM-") + "BD" + rowData.FrequencyBDNum;
                        //htmlContentModal += "<b>Period: </b> " + bdPeriod + "<br>";
                    }

                    htmlContentModal += "<b>" + rowData["TeamLabelName"] + ": </b> " + rowData["TeamTree"] + " <br>";

                    for (var i = 0; i < shareDriveFilesPaths.length; i++) {
                        var tempFileName = shareDriveFilesPaths[i]["FileName"] + "." + shareDriveFilesPaths[i]["FileExt"];
                        htmlContentModal += "<b>Activity File " + (i + 1) + ": </b> <span class='txtFileName'>" + tempFileName + "</span><br>";
                    }

                    //htmlContentModal += '<button type="button" class="btnCopyLinkToClipboard btn btn-info pull-right"><i class="fa fa-clipboard"></i> Copy Link </button>';
                    htmlContentModal += "<b>Uploaded Location: </b> <span class='txtFolderPath'>" + shareDriveFolderToCopy + "</span> <br><br>";
                    htmlContentModal += "<b>Comment: </b> <br/><textarea id='txtCommentApprove' placeholder='" + commentPlaceholder + "' style='width:100%;height: 130px;'></textarea><br/>";
                
                    //Show modal to user
                    _showModal({
                        width: '50%',
                        modalId: "modalApproval",
                        addCloseButton: true,
                        buttons: [{
                            name: btnName,
                            class: btnClass,
                            closeModalOnClick: false,
                            onClick: function ($modal) {
                                var approvalComment = $modal.find("#txtCommentApprove").val();

                                //When is Rejected Comment is required
                                if (approvalComment || approvalStatus == "Approved") {

                                    //Update Activity
                                    addActivityDetail({
                                        idActivityDetail: rowData.IDActivityDetail,
                                        idActivity: rowData.IDActivity,
                                        utcDateTimeSLA: moment(rowData.FrequencyUTCDateTimeSLA).format("YYYY-MM-DD HH:mm"),
                                        utcDateTimeCompleted: utcCompletionDateTime,
                                        status: activityDetailStatus,
                                        onSuccess: function (objActivityDetail) {
                                            //Update Activity Detail info to rowData
                                            rowData.IDActivityDetail = objActivityDetail.IDActivityDetail;
                                            rowData.ActivityDetailUTCDateTimeCompleted = utcCompletionDateTime;
                                            rowData.ActivityDetailStatus = activityDetailStatus;

                                            //Add to AuditLog for Activity Files
                                            addAuditLog({
                                                idActivityDetail: rowData.IDActivityDetail,
                                                type: approvalStatus,
                                                status: rowData.ActivityDetailStatus,
                                                comment: approvalComment,
                                                userRole: getUserRoleAuditLog(),
                                                onSuccess: function (objAuditLogDB) {
                                                    //Set idAuditLog for Rejected Notification
                                                    idAuditLog = objAuditLogDB.ID;

                                                    //Add to AuditLog Stakeholder Notification
                                                    if (rowData.ActivityDetailStatus == "Completed") {
                                                        addAuditLog({
                                                            idActivityDetail: rowData.IDActivityDetail,
                                                            type: "Stakeholder Notification",
                                                            status: rowData.ActivityDetailStatus,
                                                            comment: "",
                                                            userRole: getUserRoleAuditLog(),
                                                            onSuccess: function (objAuditLogDB) {
                                                                //Set idAuditLog for Stakeholder Notification
                                                                idAuditLog = objAuditLogDB.ID;
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                    });

                                    //On all saved close modal
                                    _execOnAjaxComplete(function () {

                                        //Send notification
                                        sendEmail({
                                            idAuditLog: idAuditLog,
                                            typeNotification: typeNotification,
                                            onSuccess: function () {

                                                refreshRow({
                                                    rowData: rowData,
                                                    onSuccess: function () {

                                                        //Close Modal
                                                        $modal.find(".close").click();

                                                        //Show user message
                                                        _showAlert({
                                                            id: "ApprovalSuccess",
                                                            showTo: $("#tbl" + _contactView + "Activities").parent(),
                                                            type: "success",
                                                            content: "Activity '" + rowData.ActivityName + "' was '" + approvalStatus + "'."
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    });

                                } else {
                                    _showAlert({
                                        id: "CommentValidation",
                                        showTo: $modal.find(".modal-body"),
                                        type: "error",
                                        content: "Comment cannot be empty."
                                    });
                                }
                            }
                        }],
                        title: "Are you sure you want to " + action + " this row?",
                        contentHtml: htmlContentModal
                    });
                } else {
                    _showNotification("error", "The Checker of this activity needs to be different from the Maker.", "AlertSameMaker");
                }
            } else {
                _showNotification("error", "You cannot " + action + " this activity because is not completed by Maker.", "AlertPendingMaker");
            }
        } else {
            _showNotification("error", rowData["ActivityName"] + " is already " + approvalStatus + "", "AlertIsApproved");
        }
    }
}

function openModalReview() {
    var rowData = $.jqxGridApi.getOneSelectedRow("#tbl" + _contactView + "Activities", true);
    if (rowData) {
        

        if (rowData["ActivityDetailStatus"] == "Pending Checker" ||
            rowData["ActivityDetailStatus"] == "Completed" ||
            rowData["ActivityDetailStatus"] == "Rejected") {

            //Validate Checker different from Maker
            if (rowData["ActivityDetailMakerBy"].toLocaleLowerCase() != _getSOEID().toLocaleLowerCase()) {

                var shareDriveFilesPaths = _parseFolderStructure(rowData);
                var shareDriveFolderToCopy = shareDriveFilesPaths[0].FolderPath;

                var htmlContentModal = '';
                var bdPeriod = '';

                if (rowData["FrequencyType"] == "BD") {
                    var momentBDPeriod = moment(rowData.FrequencyLocalYear + "-" + rowData.FrequencyLocalMonth, "YYYY-MM");
                    bdPeriod = momentBDPeriod.format("MMM-") + "BD" + rowData.FrequencyBDNum;
                    //htmlContentModal += "<b>Period: </b> " + bdPeriod + "<br>";
                }

                htmlContentModal += "<b>" + rowData["TeamLabelName"] + ": </b> " + rowData["TeamTree"] + " <br>";

                for (var i = 0; i < shareDriveFilesPaths.length; i++) {
                    var tempFileName = shareDriveFilesPaths[i]["FileName"] + "." + shareDriveFilesPaths[i]["FileExt"];
                    htmlContentModal += "<b>Activity File " + (i + 1) + ": </b> <span class='txtFileName'>" + tempFileName + "</span><br>";
                }

                htmlContentModal += '<button type="button" class="btnCopyLinkChecklist btn btn-info pull-right"><i class="fa fa-clipboard"></i> Copy Link </button>';
                htmlContentModal += "<b>Uploaded Location: </b> <span class='txtFolderPath'>" + shareDriveFolderToCopy + "</span> <br><br>";
                htmlContentModal += '<div id="containerChecklist"></div>';

                //Show Modal to Upload Files of Activity
                _showModal({
                    width: '95%',
                    modalId: "modalUpload",
                    addCloseButton: true,
                    title: "Check List Activity '" + rowData["ActivityName"] + "' " + bdPeriod,
                    contentHtml: htmlContentModal,
                    onReady: function ($modal) {

                        //On click Copy Link
                        $modal.find(".btnCopyLinkChecklist").click(function () {
                            _copyToClipboard(shareDriveFolderToCopy);
                            _showNotification("success", "Link: '" + shareDriveFolderToCopy + "' was copied succesfully to clipboard.", "CopyLink");

                            createIfNotExistsFolderPath({
                                folderPath: shareDriveFolderToCopy,
                                onSuccess: function (response) {
                                    console.log(response);
                                }
                            });
                        });

                        //Load Checklist control in modal
                        loadControlChecklist(rowData.IDChecklistChecker, $modal);
                    }
                });

            }else{
                _showNotification("error", "The Checker of this activity needs to be different from the Maker.", "AlertSameMaker");
            }
        } else {
            _showNotification("error", "You cannot review this activity because is not completed by Maker.", "AlertPendingMaker");
        }
    }

    function onApproveOrReject($modal, checkListStatus) {
        //Declare values with Approved Status
        var idAuditLog = 0;
        var utcCompletionDateTime = moment().utc().format("YYYY-MM-DD HH:mm");
        var activityDetailStatus = "Completed";
        var typeNotification = "Stakeholder Notification";

        //When is Rejected
        if (checkListStatus == "Rejected") {
            utcCompletionDateTime = "";
            activityDetailStatus = "Rejected";
            typeNotification = "Activity Rejected";
        }

        //Update Activity
        addActivityDetail({
            idActivityDetail: rowData.IDActivityDetail,
            idActivity: rowData.IDActivity,
            utcDateTimeSLA: moment(rowData.FrequencyUTCDateTimeSLA).format("YYYY-MM-DD HH:mm"),
            utcDateTimeCompleted: utcCompletionDateTime,
            status: activityDetailStatus,
            onSuccess: function (objActivityDetail) {
                //Update Activity Detail info to rowData
                rowData.IDActivityDetail = objActivityDetail.IDActivityDetail;
                rowData.ActivityDetailUTCDateTimeCompleted = utcCompletionDateTime;
                rowData.ActivityDetailStatus = activityDetailStatus;

                //Add to AuditLog Approve or Reject Record
                addAuditLog({
                    idActivityDetail: rowData.IDActivityDetail,
                    type: checkListStatus,
                    status: rowData.ActivityDetailStatus,
                    comment: $modal.find("#txtCheckerComment").val(),
                    userRole: getUserRoleAuditLog(),
                    onSuccess: function (objAuditLogDB) {
                        //Set idAuditLog for Approved or Rejected
                        idAuditLog = objAuditLogDB.ID;

                        var strCheckSteps = "";
                        var checkSteps = [];
                        $modal.find(".checklistStepRow").each(function () {
                            var $tr = $(this);
                            checkSteps.push({
                                ID: $tr.attr("idDB"),
                                Status: $tr.find(".btnApproved").val()
                            });
                        });
                        strCheckSteps = _generateString(checkSteps, ['ID', 'Status'], "~", "|");

                        //Add Check Steps status
                        addAuditLogXCheckSteps({
                            idAuditLog: idAuditLog,
                            //strCheckSteps: "3|Approved~4|Rejected~6|Approved"
                            strCheckSteps: strCheckSteps,
                            onSuccess: function () {

                                //Add to AuditLog Stakeholder Notification
                                if (rowData.ActivityDetailStatus == "Completed") {
                                    //Add to AuditLog Stakeholder Notification
                                    addAuditLog({
                                        idActivityDetail: rowData.IDActivityDetail,
                                        type: "Stakeholder Notification",
                                        status: rowData.ActivityDetailStatus,
                                        comment: "",
                                        userRole: getUserRoleAuditLog(),
                                        onSuccess: function (objAuditLogDB) {
                                            //Set idAuditLog for Stakeholder Notification
                                            idAuditLog = objAuditLogDB.ID;
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
            }
        });

        //On all saved close modal
        _execOnAjaxComplete(function () {

            //Send notification
            sendEmail({
                idAuditLog: idAuditLog,
                typeNotification: typeNotification,
                onSuccess: function () {

                    refreshRow({
                        rowData: rowData,
                        onSuccess: function () {
                            var htmlIMTLink = "";
                            var typeMessage = "success";
                            var enviroment = _getViewVar("Enviroment");
                            var imtLink = "";

                            //Set IMT Link
                            switch (enviroment) {
                                case "Local":
                                case "DEV":
                                    imtLink = "https://eucsrvappdev002.nam.nsroot.net:101/MC/MEIL/MakerChecker/AdminIssue";
                                    break;

                                case "UAT":
                                    imtLink = "https://uat.issuelog.financeandrisk.citigroup.net/MC/MEIL/MakerChecker/AdminIssue";
                                    break;

                                case "PROD":
                                case "COB":
                                    imtLink = "https://issuelog.financeandrisk.citigroup.net/MC/MEIL/MakerChecker/AdminIssue";
                                    break;
                            }

                            //Close Modal
                            $modal.find(".close").click();

                            if (checkListStatus == "Rejected") {
                                htmlIMTLink = "<br><br>Click <a target='_blank' href='" + imtLink + "'>here</a> to log a Maker/Checker issue.";
                                typeMessage = "warning";
                            }

                            //Show user message
                            _showAlert({
                                id: "UploadSuccess",
                                showTo: $("#tbl" + _contactView + "Activities").parent(),
                                type: typeMessage,
                                content: "Activity '" + rowData.ActivityName + "' was '" + checkListStatus + "'." + htmlIMTLink
                            });
                        }
                    });
                }
            });
        });
    }

    function loadControlChecklist(idChecklist, $modal) {

        //Get Check List Steps
        getChecklistSteps({
            onSuccess: function (checkListSteps) {

                function updateAttestCompletionSummary() {
                    //Set approval status
                    var cantItems = $modal.find(".btnApproved").not(".btnCheckAll").not("[disabled]").length;
                    var cantApproved = 0;
                    var cantRejected = 0;
                    var cantItemsClicked = 0;
                    //var htlmFieldsRejecteds = "";
                    $modal.find(".btnApproved").not(".btnCheckAll").not("[disabled]").each(function () {
                        var $tempBtn = $(this);
                        if ($tempBtn.attr("value") == "Approved") {
                            cantApproved++;
                            cantItemsClicked++;
                        }

                        if ($tempBtn.attr("value") == "Rejected") {
                            cantRejected++;
                            cantItemsClicked++;

                            //htlmFieldsRejecteds += '<li style="margin-left: 15px;">' + $tempBtn.parent().parent().find(".field-label").text() + '</li>';
                        }
                    });

                    //Set reject summary
                    //if (htlmFieldsRejecteds) {
                    //    $("#rejectSummary").html("");
                    //    $("#rejectSummary").html("<b> Rejected Fields: </b>");
                    //    $("#rejectSummary").append($("<ul>" + htlmFieldsRejecteds + "<ul>"));
                    //} else {
                    //    $("#rejectSummary").html("");
                    //}

                    //Set approved status
                    if (cantItems == cantApproved) {
                        $modal.find("#approvalStatus").html('<span class="label label-success" style="font-size: 17px;">Attest Completion ' + cantItems + ' of ' + cantItems + '</span>');
                        $modal.find(".btnRejectActivity").hide();
                        $modal.find(".btnApproveActivity").show();
                        $modal.find(".btnApproveActivity").removeAttr("disabled");
                    } else {
                        if (cantRejected > 0) {
                            $modal.find("#approvalStatus").html('<span class="label label-danger" style="font-size: 17px;">Attest Completion ' + cantItemsClicked + ' of ' + cantItems + ' (' + cantRejected + ' Rejected)</span>');
                            $modal.find(".btnApproveActivity").hide();
                            $modal.find(".btnRejectActivity").show();
                        } else {
                            //Set pending status
                            if (cantItemsClicked < cantItems) {
                                $modal.find("#approvalStatus").html('<span class="label label-warning" style="font-size: 15px;">Attest Completion ' + cantItemsClicked + ' of ' + cantItems + '</span>');
                                $modal.find(".btnRejectActivity").hide();
                                $modal.find(".btnApproveActivity").attr("disabled", "disabled");
                            }
                        }
                    }
                }

                //Add html Checklist to modal
                $modal.find("#containerChecklist").html(getHtml(checkListSteps));

                //AutoSize for textarea
                $modal.find("#txtCheckerComment").autosize();

                //Add N/A toogle
                $modal.find(".btnNA").click(function () {
                    var $btn = $(this);
                    var $btnApprove = $btn.closest("tr").find(".btnApproved");

                    if (!$btn.attr("value") || $btn.attr("value") == "Empty") {
                        //Add N/A
                        $btn.html('<span style="color: green;margin: -6px;top: 0;" class="glyphicon glyphicon-ok" aria-hidden="true" style=""></span> ');
                        $btn.attr("value", "N/A");

                        //Set disabled approved button
                        $btnApprove.html('<span style="color: gray;margin: -6px;top: 0;" class="glyphicon glyphicon-ban-circle " aria-hidden="true"></span>');
                        $btnApprove.attr("value", "N/A");
                        $btnApprove.attr("disabled", "disabled");
                    } else {
                        if ($btn.attr("value") == "N/A") {
                            //Remove N/A
                            $btn.html('<span style="color: red;margin: -6px;top: 0;" class="glyphicon" aria-hidden="true" style=""></span> ');
                            $btn.attr("value", "");

                            //Set enable approved button
                            $btnApprove.html('<span></span>');
                            $btnApprove.attr("value", "");
                            $btnApprove.removeAttr("disabled");
                        }
                    }

                    //On Check all
                    if ($btn.hasClass("btnNACheckAll")) {
                        $modal.find(".btnNA").each(function () {
                            var $tempBtn = $(this);
                            var $tempBtnApprove = $tempBtn.closest("tr").find(".btnApproved");

                            $tempBtn.html($btn.html());
                            $tempBtn.attr("value", $btn.attr("value"));

                            $tempBtnApprove.html($btnApprove.html());
                            $tempBtnApprove.attr("value", $btnApprove.attr("value"));
                            if ($btnApprove.attr("disabled")) {
                                $tempBtnApprove.attr("disabled", "disabled");
                            } else {
                                $tempBtnApprove.removeAttr("disabled");
                            }
                        });
                    } else {
                        //Reset btnNACheckAll Status
                        $modal.find(".btnNACheckAll").html('<span></span>');
                    }

                    //Update Attest Completion 0 of 15
                    updateAttestCompletionSummary();
                });

                //Add Approved / Rejected toogle
                $modal.find(".btnApproved").click(function () {
                    var $btn = $(this);

                    if (!$btn.attr("value") || $btn.attr("value") == "Empty" || $btn.attr("value") == "Rejected") {
                        //Approved
                        $btn.html('<span style="color: green;margin: -6px;top: 0;" class="glyphicon glyphicon-ok" aria-hidden="true" style=""></span> ');
                        $btn.attr("value", "Approved");
                    } else {
                        if ($btn.attr("value") == "Approved") {
                            //Rejected
                            $btn.html('<span style="color: red;margin: -6px;top: 0;" class="glyphicon glyphicon-remove" aria-hidden="true" style=""></span> ');
                            $btn.attr("value", "Rejected");
                        }
                    }

                    //On Check all
                    if ($btn.hasClass("btnCheckAll")) {
                        $modal.find(".btnApproved").each(function () {
                            var $tempBtn = $(this);
                            $tempBtn.html($btn.html());
                            $tempBtn.attr("value", $btn.attr("value"));
                        });
                    } else {
                        //Reset btnCheckAll Status
                        $modal.find(".btnCheckAll").html('<span></span>');
                    }

                    //Update Attest Completion 0 of 15
                    updateAttestCompletionSummary();
                });

                //Start counter of approved items
                var cantItems = $modal.find(".btnApproved").not(".btnCheckAll").not("[disabled]").length;
                $modal.find("#approvalStatus").html('<span class="label label-warning" style="font-size: 15px;">Attest Completion 0 of ' + cantItems + '</span>');
                $modal.find(".btnRejectActivity").hide();
                $modal.find(".btnApproveActivity").attr("disabled", "disabled");

                //On Click Approve or Reject
                $modal.find(".btnApproveActivity,.btnRejectActivity").click(function () {
                    var checkListStatus = $(this).val();
                    onApproveOrReject($modal, checkListStatus);
                });
            }
        });
        
        function getHtml(checkListSteps) {
            var html =
                ' <div class="row"> ' +
                '     <div class="col-md-8"> ' +
                '         <div class="panel panel-primary"> ' +
                '             <div class="panel-heading"> ' +
                '                 <h3 class="panel-title">Information</h3> ' +
                '             </div> ' +
                '             <div class="panel-body"> ' +
                '                 <p>Please review the information for every check, and confirm sufficient or more info required.</p> ' +
                '             </div> ' +
                '             <table class="table table-responsive table-bordered table-striped"> ' +
                '                 <thead> ' +
                '                     <tr> ' +
                '                         <th width="5%" style="text-align: center;vertical-align: middle;">#</th> ' +
                '                         <th width="70%" style="vertical-align: middle;">Check Description</th> ' +
                '                         <th width="15%" style="text-align: center;"> ' +
                '                             Approval All <br>' +
                '                             <button type="button" class="btn btn-default btnApproved btnCheckAll" aria-label="Right Align" style="width: 26px;height: 26px;" value=""></button> ' +
                '                         </th> ' +
                '                         <th width="10%" style="text-align: center;"> ' +
                '                             N/A All <br>' +
                '                             <button type="button" class="btn btn-default btnNA btnNACheckAll" aria-label="Right Align" style="width: 26px;height: 26px;" value=""></button> ' +
                '                         </th> ' +
                '                     </tr> ' +
                '                 </thead> ' +
                '                 <tbody> ';

            for (var i = 0; i < checkListSteps.length; i++) {
                var tempCheckStep = checkListSteps[i];
                html +=
                '                     <tr class="checklistStepRow" idDB="' + tempCheckStep.ID + '"> ' +
                '                         <td style="text-align: center;vertical-align: middle;"><b>' + tempCheckStep.Number + '</b></td>' +
                '                         <td>' + tempCheckStep.Description;
                
                if(tempCheckStep.Guidelines){
                    html +=
                    '                          <br><br><b>Guidelines:</b><br>' + _replaceAll("\n", "<br>", tempCheckStep.Guidelines);
                }
                
                html +=
                '                         </td> ' +
                '                         <td style="text-align: center;vertical-align: middle;"> ' +
                '                              <button type="button" class="btn btn-default btnApproved" aria-label="Right Align" style="width: 26px;height: 26px;" value=""></button> ' +
                '                         </td> ' +
                '                         <td style="text-align: center;vertical-align: middle;"> ' +
                '                              <button type="button" class="btn btn-default btnNA" aria-label="Right Align" style="width: 26px;height: 26px;" value=""></button> ' +
                '                         </td> ' +
                '                    </tr> ';
            }

            html +=
                '                 </tbody> ' +
                '             </table> ' +
                '         </div> ' +

                '     </div> ' +
                '     <div class="col-md-4"> ' +
                '          <div class="panel panel-primary"> ' +
                '              <div class="panel-heading"> ' +
                '                  <h3 class="panel-title">Action</h3> ' +
                '              </div> ' +
                '              <div class="panel-body"> ' +
                '                  <p>As a Checker you need to approve this activity.</p> ' +
                '              </div> ' +
                '              <table class="table table-bordered table-striped"> ' +
                '                  <tbody> ' +
                '                      <tr> ' +
                '                          <td class="text-success"> ' +
                '                               <div id="approvalStatus" status="Pending"> ' +
                '                                   <span class="label label-warning" style="font-size: 15px;"> ' +
                '                                       Attest Completion 0 of 11 ' +
                '                                   </span> ' +
                '                               </div> ' +
                '                          </td> ' +
                '                      </tr> ' +
                '                      <tr> ' +
                '                          <th scope="row">Checker Comments:</th> ' +
                '                      </tr> ' +
                '                      <tr> ' +
                '                          <td class="text-success"> ' +
                '                              <textarea name="txtCheckerComment" id="txtCheckerComment" class="form-control" style="min-height: 170px !important;"></textarea> ' +
                '                          </td> ' +
                '                      </tr> ' +
                '                  </tbody> ' +
                '              </table> ' +
                
                '              <button type="button" value="Rejected" class="btnRejectActivity btn btn-danger top15 left15 bottom15 pull-right"><i class="fa fa-times-circle"></i> Reject Activity </button> ' +
                '              <button type="button" value="Approved" class="btnApproveActivity btn btn-success top15 left15 bottom15 pull-right"><i class="fa fa-check-square-o"></i> Approve Activity </button> ' +
                
                '          </div> ' +
                '     </div> ' + 
                ' </div> ';

            return html;
        }

        function getChecklistSteps(customOptions) {
            var options = {
                onSuccess: function () { }
            };

            //Hacer un merge con las opciones que nos enviaron y las default.
            $.extend(true, options, customOptions);

            _callProcedure({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading Check List...",
                name: "[dbo].[spPCTManageChecklistStep]",
                params: [
                    { "Name": "@Action", "Value": "List Steps Of Checklist" },
                    { "Name": "@IDChecklist", "Value": idChecklist }
                ],
                success: {
                    fn: function (responseList) {
                        options.onSuccess(responseList);
                    }
                }
            });
        }
    }
}

function uploadFiles() {
    var rowData = $.jqxGridApi.getOneSelectedRow("#tbl" + _contactView + "Activities", true);
    if (rowData) {
        var uploadersStatus = {
            ActivityFilesComplete: false,
            AdditionalFilesComplete: false
        };
        var shareDriveFilesPaths = _parseFolderStructure(rowData);
        var uploaderActivityFiles = [];
        var uploaderAdditionalFiles = [];
        var additionalFileFolderName = "Additional Files";
        var shareDriveFolderToCopy = shareDriveFilesPaths[0].FolderPath;
        var shareDriveFolderToCopyAdditional = shareDriveFolderToCopy + additionalFileFolderName + "\\"

        var htmlContentModal = '';
        var bdPeriod = '';

        if (rowData["FrequencyType"] == "BD") {
            var momentBDPeriod = moment(rowData.FrequencyLocalYear + "-" + rowData.FrequencyLocalMonth, "YYYY-MM");
            bdPeriod = momentBDPeriod.format("MMM-") + "BD" + rowData.FrequencyBDNum;
            //htmlContentModal += "<b>Period: </b> " + bdPeriod + "<br>";
        }

        htmlContentModal += "<b>" + rowData["TeamLabelName"] + ": </b> " + rowData["TeamTree"] + " <br>";

        for (var i = 0; i < shareDriveFilesPaths.length; i++) {
            var tempFileName = shareDriveFilesPaths[i]["FileName"] + "." + shareDriveFilesPaths[i]["FileExt"];
            htmlContentModal += "<b>Activity File " + (i + 1) + ": </b> <span class='txtFileName'>" + tempFileName + "</span><br>";
        }

        htmlContentModal += "<b>Location to upload: </b> <span class='txtFileFullPath'>" + shareDriveFolderToCopy + "</span> <br><br>";
        htmlContentModal += "<div class='fine-uploader-activity-files'></div><br>";
        htmlContentModal += "<div class='fine-uploader-additional-files'></div><br>";
        htmlContentModal += "Comments:";
        htmlContentModal += "<textarea class='form-control txtUploadComment' placeholder='(Optional)' style='height:100px !important;'></textarea>";

        //Show Modal to Upload Files of Activity
        _showModal({
            width: '95%',
            modalId: "modalUpload",
            buttons: [{
                name: "<i class='fa fa-cloud-upload'></i> Upload",
                class: "btn-success",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    //Get all files procesed by uploader plugin
                    var filesToUpload = $modal.find('.fine-uploader-activity-files').fineUploader('getUploads');

                    //Get only files with status submitted, and remove "rejected" status
                    filesToUpload = _findAllObjByProperty(filesToUpload, "status", "submitted");
                    
                    //Check if exist at least 1 file
                    if (filesToUpload.length > 0) {

                        //Validate if all Select have a valid options
                        var isValidSelects = true;
                        var isValidSelExtentions = true;
                        var errorMessage = ""; 
                        for (var i = 0; i < filesToUpload.length; i++) {
                            var objTempFile = filesToUpload[i];
                            var $sel = $modal.find("[qq-file-id='" + objTempFile.id + "']").find(".selSharedFilesToUpload");
                            if ($sel.val() == "0") {
                                isValidSelects = false;
                                errorMessage = "Please -- Select the activity file -- for your file '" + objTempFile.originalName + "'";
                                break;
                            } else {
                                //Validate extention selected
                                var validExt = $sel.find("option:selected").attr("fileExt");
                                var validFileName = $sel.find("option:selected").attr("fileName");
                                var currentFileExt = objTempFile.originalName.substring(objTempFile.originalName.length, objTempFile.originalName.length - validExt.length);
                                if (!_contains(currentFileExt, validExt)) {
                                    isValidSelExtentions = false;
                                    errorMessage = "Your file '" + objTempFile.originalName + "' needs to have the same extention with the selected activity file '" + validFileName + ".<b>" + validExt + "</b>'.";
                                    break;
                                }
                            }
                        }

                        //Check correct options for selects
                        if (isValidSelects) {

                            //Check valid extention mapping
                            if (isValidSelExtentions) {

                                //Check all files uploaded
                                if(uploaderActivityFiles.length == shareDriveFilesPaths.length){
                                    _showLoadingFullPage({
                                        idLoading: "Uploading",
                                        msg: "Uploading files..."
                                    });

                                    //Set parameters
                                    setFileParams();

                                    //Upload files and validate sucess in fuction onSuccessAllFilesUploaded
                                    if (uploaderActivityFiles.length > 0) {
                                        $('.fine-uploader-activity-files').fineUploader('uploadStoredFiles');
                                    } else {
                                        uploadersStatus.ActivityFilesComplete = true;
                                    }

                                    if (uploaderAdditionalFiles.length > 0) {
                                        $('.fine-uploader-additional-files').fineUploader('uploadStoredFiles');
                                    } else {
                                        uploadersStatus.AdditionalFilesComplete = true;
                                    }
                                }else{
                                    _showNotification("error", "Please select all files required for the activity.", "AlertSelectAllFiles");
                                }
                            } else {
                                _showNotification("error", errorMessage, "AlertSelectValidOption");
                            }
                        } else {
                            _showNotification("error", errorMessage, "AlertSelectValidOption");
                        }
                    } else {
                        _showNotification("error", "Please select a file.", "AlertSelectFile");
                    }
                }
            }],
            addCloseButton: true,
            title: "Upload Files Activity '" + rowData["ActivityName"] + "' " + bdPeriod,
            contentHtml: htmlContentModal,
            onReady: function ($modal) {
                //Init Fine Uploader
                initFileUploader($modal);

                //Show Overwrite Warning
                if (rowData["ActivityDetailStatus"] == "Pending Checker" || rowData["ActivityDetailStatus"] == "Completed") {
                    _showAlert({
                        id: "AlertOverwrite",
                        type: "warning",
                        showTo: $modal.find(".modal-body"),
                        content: "The file already exist and will be overwritten."
                    });
                }
            }
        });

        //When all files were uploaded
        function onSuccessAllFilesUploaded(typeUploader, $modal) {
            uploadersStatus[typeUploader] = true;

            //When all files were uploaded successfull
            if (uploadersStatus.ActivityFilesComplete && uploadersStatus.AdditionalFilesComplete) {

                //Hide uploading
                _hideLoadingFullPage({
                    idLoading: "Uploading"
                });

                var uploadComment = ($modal.find(".txtUploadComment").val() ? $modal.find(".txtUploadComment").val() : "");
                var idAuditLog = 0;

                //Add or Update current Activity Detail
                addActivityDetail({
                    idActivityDetail: rowData.IDActivityDetail,
                    idActivity: rowData.IDActivity,
                    utcDateTimeSLA: moment(rowData.FrequencyUTCDateTimeSLA).format("YYYY-MM-DD HH:mm"),
                    status: "Pending Checker",
                    onSuccess: function (objActivityDetail) {
                        //Update Activity Detail info to rowData
                        rowData.IDActivityDetail = objActivityDetail.IDActivityDetail;
                        rowData.ActivityDetailStatus = "Pending Checker";

                        //Add to AuditLog for Activity Files
                        addAuditLog({
                            idActivityDetail: rowData.IDActivityDetail,
                            type: "Files Uploaded",
                            status: rowData.ActivityDetailStatus,
                            comment: uploadComment,
                            userRole: getUserRoleAuditLog(),
                            onSuccess: function (objAuditLogDB) {
                                //Set new Audit Log
                                idAuditLog = objAuditLogDB.ID;

                                //Add Activity Files
                                for (var i = 0; i < uploaderActivityFiles.length; i++) {
                                    var tempActivityFile = uploaderActivityFiles[i];
                                    var idSharedFileSelected = $modal.find("[qq-file-id='" + tempActivityFile.id + "']").find(".selSharedFilesToUpload").val();
                                    var objSharedDriveFile = _findOneObjByProperty(shareDriveFilesPaths, "ID", idSharedFileSelected);

                                    //Add Audit Log x Share files
                                    addAuditLogXSharedDriveFile({
                                        idAuditLog: objAuditLogDB.ID,
                                        idSharedDriveFile: objSharedDriveFile.ID,
                                        fileFullPath: objSharedDriveFile.FullPath,
                                        fileName: objSharedDriveFile.FileName,
                                        fileExt: objSharedDriveFile.FileExt,
                                        isAdditionalFile: 0,
                                        onSuccess: function () { }
                                    });
                                }

                                //Add Additional Files
                                if (uploaderAdditionalFiles.length > 0) {
                                    for (var i = 0; i < uploaderAdditionalFiles.length; i++) {
                                        var tempAdditionalFile = uploaderAdditionalFiles[i];
                                        var tempFileExt = tempAdditionalFile.name.substr(tempAdditionalFile.name.lastIndexOf('.') + 1);
                                        var tempFileName = tempAdditionalFile.name.replace("." + tempFileExt, "");

                                        //Add Audit Log x Share files
                                        addAuditLogXSharedDriveFile({
                                            idAuditLog: objAuditLogDB.ID,
                                            idSharedDriveFile: 0,
                                            fileFullPath: shareDriveFolderToCopyAdditional + tempAdditionalFile.name,
                                            fileName: tempFileName,
                                            fileExt: tempFileExt,
                                            isAdditionalFile: 1,
                                            onSuccess: function () { }
                                        });
                                    }
                                }
                            }
                        });
                    }
                });

                //On all saved close modal
                _execOnAjaxComplete(function () {

                    //Send notification
                    sendEmail({
                        idAuditLog: idAuditLog,
                        typeNotification: "Files Uploaded",
                        onSuccess: function () {

                            refreshRow({
                                rowData: rowData,
                                onSuccess: function () {

                                    $modal.find(".close").click();

                                    //Show user message
                                    _showAlert({
                                        id: "UploadSuccess",
                                        showTo: $("#tbl" + _contactView + "Activities").parent(),
                                        type: "success",
                                        content: "Files were uploaded successfully."
                                    });
                                }
                            });
                        }
                    });
                });
            }
        }

        //Set File Upload Parameters
        function setFileParams() {
            $('.fine-uploader-activity-files').fineUploader('setParams', {
                'pshareDriveFiles': JSON.stringify(shareDriveFilesPaths),
                'pisAdditionalFile': "0",
                'padditionalFolderName': additionalFileFolderName
            });

            $('.fine-uploader-additional-files').fineUploader('setParams', {
                'pshareDriveFiles': JSON.stringify(shareDriveFilesPaths),
                'pisAdditionalFile': "1",
                'padditionalFolderName': additionalFileFolderName
            });
        }

        //Init File Uploader
        function initFileUploader($modal) {
            var shareDriveFilesPaths = _parseFolderStructure(rowData);
            var validExts = [];

            //Calculate valid extentions
            for (var i = 0; i < shareDriveFilesPaths.length; i++) {
                validExts.push(shareDriveFilesPaths[i].FileExt);
            }

            //Init Activity Files Uploader
            $('.fine-uploader-activity-files').fineUploader({
                template: 'qq-template-upload-activity-files',
                request: {
                    endpoint: _getViewVar("SubAppPath") + '/ProcessControlTool/UploadToShareDrive'
                },
                thumbnails: {
                    placeholders: {
                        waitingPath: _getViewVar("SubAppPath") + '/Content/Shared/plugins/fine-uploader/placeholders/waiting-generic.png',
                        notAvailablePath: _getViewVar("SubAppPath") + '/Content/Shared/plugins/fine-uploader/placeholders/not_available-generic.png'
                    }
                },
                callbacks: {
                    //On Server upload completed
                    onAllComplete: function (succeeded, failed) {
                        if (failed.length == 0) {
                            onSuccessAllFilesUploaded('ActivityFilesComplete', $modal);
                        }
                    },
                    //On Server error 
                    onError: function (id, name, errorReason, xhrOrXdr) {
                        if (_contains(errorReason, "Access to the path") && _contains(errorReason, "is denied")) {
                            _showDetailAlert({
                                id: "AlertErrorUpload",
                                showTo: ".modal-body",
                                title: "Message",
                                shortMsg: "The system don't have access to the share drive '" + shareDriveFolderToCopy + "' please contact to your system administrator.",
                                longMsg: "<pre>" + errorReason + "</pre>",
                                type: "Error",
                                viewLabel: "View Details"
                            });
                            //Show notification
                            $modal.find(".modal-header")[0].scrollIntoView();
                        } else {
                            if (name && !_contains(errorReason, "has an invalid extension")) {
                                _showDetailAlert({
                                    showTo: ".modal-body",
                                    title: "Message",
                                    shortMsg: "An error ocurred with the file '" + rowData["ActivityName"] + "'.",
                                    longMsg: "<pre>" + errorReason + "</pre>",
                                    type: "Error",
                                    viewLabel: "View Details"
                                });
                            }
                        }

                        //Hide uploading
                        _hideLoadingFullPage({
                            idLoading: "Uploading"
                        });
                    },
                    //When new file is added
                    onSubmitted: function (id, name) {
                        var $selSharedFiles = $modal.find("[qq-file-id='" + id + "']").find(".selSharedFilesToUpload");

                        //Add current file list to select
                        //Clear old values
                        $selSharedFiles.contents().remove();

                        //Add defaul option
                        $selSharedFiles.append($('<option>', {
                            value: 0,
                            text: "-- Select activity file --"
                        }));

                        //Add Options
                        for (var i = 0; i < shareDriveFilesPaths.length; i++) {
                            var objTemp = shareDriveFilesPaths[i];
                            $selSharedFiles.append($('<option>', {
                                value: objTemp.ID,
                                fileExt: objTemp.FileExt,
                                fileName: objTemp.FileName,
                                text: objTemp.FileName + "." + objTemp.FileExt
                            }));
                        }

                        //On Change Select Option set name
                        $selSharedFiles.change(function () {
                            var $selectedOption = $(this).find("option:selected");
                            if ($selectedOption.attr("value") != "0") {
                                var $qqUploadFile = $modal.find("[qq-file-id='" + id + "']").find(".qq-upload-file");
                                var oldFileName = $qqUploadFile.attr("oldFileName");
                                if (!oldFileName) {
                                    oldFileName = $('.fine-uploader-activity-files').fineUploader('getName', id);
                                    $qqUploadFile.attr("oldFileName", oldFileName);
                                }

                                var newFileName = $selectedOption.attr("fileName") + "." + $selectedOption.attr("fileExt");

                                //Set Shared File Name
                                $('.fine-uploader-activity-files').fineUploader('setName', id, newFileName);

                                //Set old name in title
                                $qqUploadFile.attr("title", oldFileName);
                            }
                        });

                        //Set parameters
                        setFileParams();

                        //Update files
                        //Get all files procesed by uploader plugin
                        var filesToUpload = $modal.find('.fine-uploader-activity-files').fineUploader('getUploads');

                        //Get only files with status submitted, and remove "rejected" status
                        filesToUpload = _findAllObjByProperty(filesToUpload, "status", "submitted");
                    
                        uploaderActivityFiles = filesToUpload;
                    },
                    //On Manual Retry
                    onManualRetry: function (id, name) {
                        setFileParams();
                    }
                },
                validation: {
                    allowedExtensions: validExts,
                    itemLimit: shareDriveFilesPaths.length
                    //sizeLimit: 51200 // 50 kB = 50 * 1024 bytes
                },
                autoUpload: false
            });

            //Init Additional Files Uploader
            $('.fine-uploader-additional-files').fineUploader({
                template: 'qq-template-upload-additional-files',
                request: {
                    endpoint: _getViewVar("SubAppPath") + '/ProcessControlTool/UploadToShareDrive'
                },
                thumbnails: {
                    placeholders: {
                        waitingPath: _getViewVar("SubAppPath") + '/Content/Shared/plugins/fine-uploader/placeholders/waiting-generic.png',
                        notAvailablePath: _getViewVar("SubAppPath") + '/Content/Shared/plugins/fine-uploader/placeholders/not_available-generic.png'
                    }
                },
                callbacks: {
                    onAllComplete: function (succeeded, failed) {
                        if (failed.length == 0) {
                            onSuccessAllFilesUploaded('AdditionalFilesComplete', $modal);
                        }
                    },
                    onError: function (id, name, errorReason, xhrOrXdr) {
                        if (_contains(errorReason, "Access to the path") && _contains(errorReason, "is denied")) {
                            _showDetailAlert({
                                id: "AlertErrorUpload",
                                showTo: ".modal-body",
                                title: "Message",
                                shortMsg: "The system don't have access to the share drive '" + shareDriveFolderToCopy + "' please contact to your system administrator.",
                                longMsg: "<pre>" + errorReason + "</pre>",
                                type: "Error",
                                viewLabel: "View Details"
                            });
                            //Show notification
                            $modal.find(".modal-header")[0].scrollIntoView();

                        } else {
                            if (name && !_contains(errorReason, "has an invalid extension")) {
                                _showDetailAlert({
                                    showTo: ".modal-body",
                                    title: "Message",
                                    shortMsg: "An error ocurred with the file '" + rowData["ActivityName"] + "'.",
                                    longMsg: "<pre>" + errorReason + "</pre>",
                                    type: "Error",
                                    viewLabel: "View Details"
                                });
                            }
                        }

                        //Hide uploading
                        _hideLoadingFullPage({
                            idLoading: "Uploading"
                        });
                    },

                    //When new file is added
                    onSubmitted: function (id, name) {
                        //Set parameters
                        setFileParams();

                        //Update files
                        //Get all files procesed by uploader plugin
                        var filesToUpload = $modal.find('.fine-uploader-additional-files').fineUploader('getUploads');

                        //Get only files with status submitted, and remove "rejected" status
                        filesToUpload = _findAllObjByProperty(filesToUpload, "status", "submitted");
                    
                        uploaderAdditionalFiles = filesToUpload;
                    },
                    onManualRetry: function (id, name) {
                        setFileParams();
                    }
                },
                validation: {},
                autoUpload: false
            });
        }
    }
}

function refreshRow(customOptions) {
    var options = {
        rowData: {},
        onSuccess: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    getActivityDetail({
        idActivityDetail: options.rowData.IDActivityDetail,
        onSuccess: function (objActivityDetail) {
            
            //Refresh ActivityDetailCheckerStatus in selected row
            options.rowData["ActivityDetailCheckerStatus"] = objActivityDetail.ActivityDetailCheckerStatus;

            //Refresh ActivityDetailSLAStatus in selected row
            options.rowData["ActivityDetailSLAStatus"] = objActivityDetail.ActivityDetailSLAStatus;

            //Refresh ActivityDetailStatus in selected row
            options.rowData["ActivityDetailStatus"] = objActivityDetail.ActivityDetailStatus;

            //Refresh CountAuditLogs in selected row
            options.rowData["CountAuditLogs"] = objActivityDetail.CountAuditLogs;

            //Refresh JsonAuditLogs in selected row
            options.rowData["JsonAuditLogs"] = objActivityDetail.JsonAuditLogs;

            //Update Data in Table
            $("#tbl" + _contactView + "Activities").jqxGrid('updaterow', options.rowData.uid, options.rowData);

            // expand all groups.
            $("#tbl" + _contactView + "Activities").jqxGrid('expandallgroups');

            if (options.onSuccess) {
                options.onSuccess();
            }
        }
    });
}

function getUserRoleAuditLog() {
    var result = _contactView;

    if (_getQueryStringParameter("pviewType") == "All") {
        result = "Administrator";
    }

    return result;
}

function getAuditLogsByActivityDetail(customOptions) {
    var options = {
        idActivityDetail: "0",
        onSuccess: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Getting Audit Logs...",
        name: "[dbo].[spPCTManageAuditLog]",
        params: [
            { "Name": "@Action", "Value": "List Audit Logs by Activity Detail" },
            { "Name": "@IDActivityDetail", "Value": options.idActivityDetail },
            { "Name": "@UTCOffsetMinutes", "Value": _timeZoneGetUTCOffsetMinutes() },
            { "Name": "@SessionSOEID", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                //Retur response
                options.onSuccess(responseList);
            }
        }
    });
}

function getActivityDetail(customOptions) {
    var options = {
        idActivityDetail: "0",
        onSuccess: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Getting Activity Detail...",
        name: "[dbo].[spPCTManageActivityDetail]",
        params: [
            { "Name": "@Action", "Value": "Get Activity Detail Info by ID" },
            { "Name": "@IDActivityDetail", "Value": options.idActivityDetail },
            { "Name": "@UTCOffsetMinutes", "Value": _timeZoneGetUTCOffsetMinutes() },
            { "Name": "@UserCurrentDateTime", "Value": _timeZoneGetCurrentDateTime() },
            { "Name": "@SessionSOEID", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                //Return Updated Activity Detail
                options.onSuccess(responseList[0]);
            }
        }
    });
}

function addActivityDetail(customOptions) {
    var action = "New";
    var options = {
        forIndex: 0,
        idActivityDetail: "0",
        idActivity: "0",
        utcDateTimeSLA: "",
        utcDateTimeCompleted: "",
        status: "",
        onSuccess: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    if (options.idActivityDetail != "0") {
        action = "Edit";
    }

    var spParams = [
        { "Name": "@Action", "Value": action },
        { "Name": "@IDActivityDetail", "Value": options.idActivityDetail },
        { "Name": "@IDActivity", "Value": options.idActivity },
        { "Name": "@UTCDateTimeSLA", "Value": options.utcDateTimeSLA },
        { "Name": "@ActivityDetailStatus", "Value": options.status },
        { "Name": "@UTCOffsetMinutes", "Value": _timeZoneGetUTCOffsetMinutes() },
        { "Name": "@UserCurrentDateTime", "Value": _timeZoneGetCurrentDateTime() },
        { "Name": "@SessionSOEID", "Value": _getSOEID() }
    ];

    if(options.utcDateTimeCompleted){
        spParams.push({ "Name": "@UTCDateTimeCompleted", "Value": options.utcDateTimeCompleted });
    }

    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Saving activity detail information...",
        name: "[dbo].[spPCTManageActivityDetail]",
        params: spParams,
        success: {
            fn: function (responseList) {
                //Return New Activity Detail
                options.onSuccess(responseList[0], options.forIndex);
            }
        }
    });
}

function addAuditLog(customOptions) {
    var options = {
        idActivityDetail: "0",
        type: "",
        status: "",
        comment: "",
        userRole: getUserRoleAuditLog(),
        onSuccess: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Saving activity detail information...",
        name: "[dbo].[spPCTManageAuditLog]",
        params: [
            { "Name": "@Action", "Value": "New" },
            { "Name": "@IDActivityDetail", "Value": options.idActivityDetail },
            { "Name": "@Type", "Value": options.type },
            { "Name": "@ActivityDetailStatus", "Value": options.status },
            { "Name": "@Comment", "Value": options.comment },
            { "Name": "@UserRole", "Value": options.userRole },
            { "Name": "@UTCOffsetMinutes", "Value": _timeZoneGetUTCOffsetMinutes() },
            { "Name": "@SessionSOEID", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                //Return New ID
                options.onSuccess(responseList[0]);
            }
        }
    });
}

function addAuditLogXCheckSteps(customOptions) {
    var options = {
        idAuditLog: "0",
        //strCheckSteps: "3|Approved~4|Rejected~6|Approved"
        strCheckSteps: "",
        onSuccess: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Saving Check List...",
        name: "[dbo].[spPCTManageAuditLogXChecklistStep]",
        params: [
            { "Name": "@Action", "Value": "New Audit Log Check Steps" },
            { "Name": "@IDAuditLog", "Value": options.idAuditLog },
            { "Name": "@StrCheckSteps", "Value": options.strCheckSteps },
            { "Name": "@SessionSOEID", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                options.onSuccess(responseList);
            }
        }
    });
}

function addAuditLogXSharedDriveFile(customOptions) {
    var options = {
        idAuditLog: "0",
        idSharedDriveFile: "0",
        fileFullPath: "",
        fileName: "",
        fileExt: "",
        isAdditionalFile: 0,
        onSuccess: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Saving shared files information...",
        name: "[dbo].[spPCTManageAuditLogXSharedFile]",
        params: [
            { "Name": "@Action", "Value": "New" },
            { "Name": "@IDAuditLog", "Value": options.idAuditLog },
            { "Name": "@IDSharedDriveFile", "Value": options.idSharedDriveFile },
            { "Name": "@FileFullPath", "Value": options.fileFullPath },
            { "Name": "@FileName", "Value": options.fileName },
            { "Name": "@FileExt", "Value": options.fileExt },
            { "Name": "@IsAdditionalFile", "Value": options.isAdditionalFile },
            { "Name": "@SessionSOEID", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                //Return New ID
                options.onSuccess(responseList[0]);
            }
        }
    });
}

function sendEmail(customOptions) {
    var options = {
        idAuditLog: "0",

        //'Files Uploaded', 'Activity Rejected', 'Comment', 'Stakeholder Notification'
        typeNotification: "",

        //'Maker', 'Checker', 'Stakeholder'
        contactType: _contactView,

        onSuccess: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    if (options.idAuditLog != "0") {
        _callProcedure({
            loadingMsgType: "fullLoading",
            loadingMsg: "Sending notification...",
            name: "[dbo].[spPCTSendEmail]",
            params: [
                { "Name": "@IDAuditLog", "Value": options.idAuditLog },
                { "Name": "@TypeNotification", "Value": options.typeNotification },
                { "Name": "@ContactType", "Value": options.contactType },
                { "Name": "@ServerURL", "Value": _getViewVar("ServerURL") }
            ],
            success: {
                fn: function () {
                    //Return New ID
                    options.onSuccess();
                }
            }
        });
    } else {
        alert('Error on sending notification IDAuditLog is 0');
    }
}

function downloadFiles() {
    var rowData = $.jqxGridApi.getOneSelectedRow("#tbl" + _contactView + "Activities", true);
    if (rowData) {
        if (rowData.ActivityDetailStatus == "Completed") {
            var shareDriveFiles = _parseFolderStructure(rowData);
            var todayDateFormat = _timeZoneGetSelectedInfo().CurrentDateTime.format("YYYY-MM-DD");

            for (var i = 0; i < shareDriveFiles.length; i++) {
                var tempFile = shareDriveFiles[i];
                var tempFileName = todayDateFormat + " - " + tempFile.FileName + "." + tempFile.FileExt;

                _downloadFile({
                    loadingMsg: "Downloading file '" + tempFile.FileName + "." + tempFile.FileExt + "'...",
                    urlCopyFrom: tempFile.FullPath,
                    fileName: tempFileName
                });
            }

            //When all files are donwloaded
            _execOnAjaxComplete(function () {
                if ($("#DownloadFileError").length == 0) {

                    //Add to AuditLog
                    addAuditLog({
                        idActivityDetail: rowData.IDActivityDetail,
                        type: "Files Downloaded",
                        status: rowData.ActivityDetailStatus,
                        comment: "",
                        userRole: getUserRoleAuditLog(),
                        onSuccess: function (objAuditLogDB) {
                            //objAuditLogDB.ID;

                            _showAlert({
                                id: "DownloadSuccess",
                                showTo: $("#tbl" + _contactView + "Activities").parent(),
                                type: "success",
                                content: "Files for Activity '" + rowData["ActivityName"] + "' were downloaded successfull."
                            });
                        }
                    });
                }
            });
        } else {
            _showNotification("error", "Activity '" + rowData["ActivityName"] + "' is not completed.", "AlertIsNotCompleted");
        }
    }
}