//@ sourceURL=PartialViewActivityForm.js
/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="ProcessControlTool.js" />

//Show Full Page Loading 
_showLoadingFullPage({
    id: "ActivityFormLoading",
    msg: "Loading Activity Form..."
});

$(document).ready(function () {
    // Load New Activity
    if ($("#HFAction").val() == "New") {
        loadActivityForm();
    }

    // Load Edit Activity
    if ($("#HFAction").val() == "Edit") {
        loadObjActivityByID({
            idActivity: $("#HFIDActivity").val(),
            onSuccess: function (objActivity) {
                loadActivityForm(objActivity);
            }
        });
    }

    //When all ajax are completed 
    _execOnAjaxComplete(function () {

        //Hide Full Page Loading
        _hideLoadingFullPage({
            id: "ActivityFormLoading"
        });
    });
});

function loadObjActivityByID(customOptions) {

    //Default Options.
    var options = {
        idActivity: 0,
        onSuccess: null
    };

    //Do merge of customOptions with default options.
    $.extend(true, options, customOptions);

    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Activity Information...",
        name: "[dbo].[spPCTManageActivity]",
        params: [
            { "Name": "@Action", "Value": 'Get Activity Info By ID' },
            { "Name": "@IDActivity", "Value": options.idActivity }
        ],
        success: {
            fn: function (responseList) {
                if (responseList.length > 0) {
                    options.onSuccess(responseList[0]);
                }
            }
        }
    });
}

function loadActivityForm(objEditActivity) {
    
    //Default Options.
    var objActivity = {
        ID: 0,
        IDTeam: $("#HFIDTeam").val(),
        IDFrequency: 0,
        IDChecklistChecker: 0,
        FrequencyUTCStartDateTime: '',
        FrequencyLocalStartDateTime: '',
        FrequencyCode: '',
        Name: '',
        GOC: '',
        GOCDesc: '',

        JsonEDCFCs: "[]",
        JsonSharedDriveFiles: "[]",

        JsonMakerContacts: "[]",
        StrListMakerContacts: '',

        JsonCheckerContacts: "[]",
        StrListCheckerContacts: '',

        JsonStakeholderContacts: "[]",
        StrListStakeholderContacts: ''
    };

    //Do merge of customOptions with default options.
    $.extend(true, objActivity, objEditActivity);

    //If not exists FrequencyUTCStartDateTime
    if (!objActivity.FrequencyUTCStartDateTime) {
        //Calculate default value
        var defaultFrequencyLocalDate = _timeZoneGetSelectedInfo().CurrentDateTime.format("YYYY-MM-DD 00:00");
        objActivity.FrequencyUTCStartDateTime = _timeZoneConvertLocalToUTC(defaultFrequencyLocalDate, true);
    }

    //Set FrequencyLocalStartDate
    objActivity.FrequencyLocalStartDateTime = _timeZoneConvertUTCToLocal(objActivity.FrequencyUTCStartDateTime, true);

    //Set Name
    _setDivEditableValue($("#txtName"), objActivity.Name);

    //Set Frequency ID / Code
    $("#lblActivityFrequency").attr("IDFrequency", objActivity.IDFrequency);
    $("#lblActivityFrequency").attr("FrequencyUTCStartDateTime", objActivity.FrequencyUTCStartDateTime);
    $("#lblActivityFrequency").html(objActivity.FrequencyCode);

    //On Click Change Frequency
    $(".btnChangeFrequency").click(function () {
        _frequencyManager.openModalSelectFrequency({
            frequencyCode: $("#lblActivityFrequency").text(),
            frequencyStartDate: moment(objActivity.FrequencyLocalStartDateTime).format('MMM DD, YYYY'),
            onSelect: function (objFrequency) {
                $("#lblActivityFrequency").attr("IDFrequency", objFrequency.ID);
                $("#lblActivityFrequency").attr("FrequencyUTCStartDateTime", objFrequency.UTCStartDateTime);

                _setDivEditableValue($("#lblActivityFrequency"), objFrequency.Code);
            }
        });
    });

    // On click Save
    $(".btnSave").click(function () {
        saveActivity();
    });

    // On click Activity Settings
    $(".btnActivitySettings").click(function () {
        //ProcessControlTool.js
        _showModalActivitySettings();
    });

    //Set Team GOC
    $("#lblGOC").html(objActivity.GOCDesc);

    //Select eDCFC
    _createSelectEDCFC({
        id: "#containerEDCFCMapping",
        label: "eDCFC Mapping",
        attrs: {
            name: "lblEDCFCsSummary"
        },
        jsonEDCFCs: objActivity.JsonEDCFCs,
        onSelect: function (strListContacts, newContacts, deletedContacts) {
            //Update container value, updated automatically before call onSelect in global.js
            //$("#containerMakerContacts").attr("StrListContact", strListContacts);
        }
    });

    //Select Maker
    _createSelectContact({
        id: "#containerMakerContacts",
        attrs: {
            name: "lblMakersSummary"
        },
        label: "Makers",
        spName: "[dbo].[spPCTManageActivity]",
        spParams: [
            { Name: "@Action", Value: "List Tree Contacts of Activity" },
            { Name: "@IDActivity", Value: objActivity.ID },
            { Name: "@ContactType", Value: 'Maker' }
        ],
        jsonContacts: objActivity.JsonMakerContacts,
        onSelect: function (strListContacts, newContacts, deletedContacts) {
            //Update container value, updated automatically before call onSelect in global.js
            //$("#containerMakerContacts").attr("StrListContact", strListContacts);
        }
    });

    //Select Checker
    _createSelectContact({
        id: "#containerCheckerContacts",
        attrs: {
            name: "lblCheckersSummary"
        },
        label: "Checkers",
        spName: "[dbo].[spPCTManageActivity]",
        spParams: [
            { Name: "@Action", Value: "List Tree Contacts of Activity" },
            { Name: "@IDActivity", Value: objActivity.ID },
            { Name: "@ContactType", Value: 'Checker' }
        ],
        jsonContacts: objActivity.JsonCheckerContacts,
        onSelect: function (strListContacts, newContacts, deletedContacts) {
            //Update container value, updated automatically before call onSelect in global.js
            //$("#containerCheckerContacts").attr("StrListContact", strListContacts);
        }
    });

    //Select Stakeholder
    _createSelectContact({
        id: "#containerStakeholderContacts",
        attrs: {
            name: "lblStakeholdersSummary"
        },
        label: "Stakeholders",
        spName: "[dbo].[spPCTManageActivity]",
        spParams: [
            { Name: "@Action", Value: "List Tree Contacts of Activity" },
            { Name: "@IDActivity", Value: objActivity.ID },
            { Name: "@ContactType", Value: 'Stakeholder' }
        ],
        jsonContacts: objActivity.JsonStakeholderContacts,
        onSelect: function (strListContacts, newContacts, deletedContacts) {
            //Update container value, updated automatically before call onSelect in global.js
            //$("#containerStakeholderContacts").attr("StrListContact", strListContacts);
        }
    });

    //Validate Form, run before loadSelectOfActivityTeam and loadSharedDriveFilesTable
    loadValidations();

    //Load Select of Team
    loadSelectOfActivityTeam({
        objActivity: objActivity,
        onSuccess: function () {}
    });

    //Render CFields after Select of Team was selected, NOTE: This is triggered on change event of $("#selActivityTeam").change()
    //renderCFields({
    //    objActivity: objActivity,
    //    onSuccess: function(){}
    //});


    //Load Select of Checklist, NOTE: This is triggered on change event of $("#selActivityTeam").change()
    //loadSelectOfChecklist({
    //    idChecklistSelected: objActivity.IDChecklistChecker
    //});

    //Global.js Load div placeholders for content editable
    _loadDivContentEditablePlaceholders();

    //Global.js Load on click module
    _loadModuleAccordion();

    //Load Shared Drive Files Table, NOTE: need to be after loadValidations().
    loadSharedDriveFilesTable({
        objActivity: objActivity,
        onSuccess: function () { }
    });

}

function loadValidations() {
    var rules = {};

    // Activity Name
    rules["txtName"] = {
        divEditableRequired: true
    };

    // Validate Frequency
    rules["lblActivityFrequency"] = {
        valueNotEquals: ""
    };

    //Validate Team Selected
    rules["selActivityTeam"] = {
        valueNotEquals: 0
    };

    //// Validate Makers
    //rules["lblMakersSummary"] = {
    //    valueNotEquals: "(0) Users and (0) DLs"
    //};

    //// Validate Checkers
    //rules["lblCheckersSummary"] = {
    //    valueNotEquals: "(0) Users and (0) DLs"
    //};

    //// Validate Stakeholders
    //rules["lblStakeholdersSummary"] = {
    //    valueNotEquals: "(0) Users and (0) DLs"
    //};

    _validateForm({
        form: '#formActivity',
        rules: rules
    });
}

function loadSharedDriveFilesTable(customOptions) {
    //Default Options.
    var options = {
        objActivity: {},
        onSuccess: null
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    //When click add new shared filed
    $(".btnAddFile").click(function () {
        addNewRow();
    });

    //Load current shared drive files
    var sharedDrivefiles = JSON.parse(options.objActivity.JsonSharedDriveFiles);
    if (sharedDrivefiles.length > 0) {
        for (var i = 0; i < sharedDrivefiles.length; i++) {
            addNewRow(sharedDrivefiles[i]);
        }
    } else {
        //Load 1 by default
        addNewRow();
    }

    function addNewRow(pobjSharedDriveFile) {
        var $row = $(getHtmlTemplateRow(pobjSharedDriveFile));

        //Select File Extention
        if (pobjSharedDriveFile) {
            $row.find(".selSharedFileExt").val(pobjSharedDriveFile.FileExt);
        }

        //On Click Delete
        $row.find(".btnDeleteFile").click(function () {
            var idRowToDelete = $(this).attr("idRow");
            deleteRow(idRowToDelete);
        });

        //Add row to table
        $(".containerRowsSharedFiles").append($row);

        //Add Validation rule
        $row.find(".txtSharedFileName").rules("add", {
            divEditableRequired: true
        });

        //Add Validation rule
        $row.find(".txtSharedFileDesc").rules("add", {
            divEditableRequired: true
        });

        //Add Validation rule
        $row.find(".txtSharedFileFolderStructure").rules("add", {
            divEditableRequired: true
        });

        //Global.js Load div placeholders for content editable
        _loadDivContentEditablePlaceholders();
    }

    function deleteRow(idRowToDelete) {
        var objSharedDriveFile = getObjSharedDriveFileByIDRow(idRowToDelete);
        if (objSharedDriveFile.ID != "0") {
            var htmlContentModal = "";
            htmlContentModal += "<b>File Name: </b>" + objSharedDriveFile.FileName + "<br/>";
            htmlContentModal += "<b>File Ext: </b>" + objSharedDriveFile.FileExt + "<br/>";
            htmlContentModal += "<b>Description: </b>" + objSharedDriveFile.Description + "<br/>";
            htmlContentModal += "<b>Folder Structure: </b>" + objSharedDriveFile.FolderStructure + "<br/>";

            _showModal({
                width: '40%',
                modalId: "modalDelRow",
                addCloseButton: true,
                buttons: [{
                    name: "<i class='fa fa-trash-o'></i> Delete",
                    class: "btn-danger",
                    onClick: function () {
                        doDelete();
                    }
                }],
                title: "Are you sure you want to delete this row?",
                contentHtml: htmlContentModal
            });
        } else {
            doDelete();
        }

        function doDelete(){
            if ($(".rowSharedFile").length == 1) {
                _showNotification("error", "Cannot delete the only one shared file for this activity.", "SharedFileError");
            } else {
                if (objSharedDriveFile.ID != "0") {
                    //Whe is a saved DB Row
                    $("#" + idRowToDelete).addClass("rowSharedFileDeleted");

                    //Hide Row
                    $("#" + idRowToDelete).hide();
                } else {
                    //Is row is not saved remove HTML Row element
                    $("#" + idRowToDelete).remove();
                }
            }
        }
    }

    function getHtmlTemplateRow(pobjSharedDriveFile) {
        //Default Options.
        var objSharedDriveFile = {
            ID: 0,
            IDWorkingFile: 0,
            FolderStructure: '@TeamStructure\\@FileName.@FileExt',
            FileName: '',
            FileExt: 'xlsx',
            Description: '',
            IsRequired: 1
        };

        //Hacer un merge con las opciones que nos enviaron y las default.
        $.extend(true, objSharedDriveFile, pobjSharedDriveFile);

        var idControl = _createCustomID();
        var idRow = "rowSharedFile_" + idControl;
        var idTxtSharedFileName = "txtSharedFileName_" + idControl;
        var idSelSharedFileExt = "selSharedFileExt_" + idControl;
        var idTxtSharedFileDesc = "txtSharedFileDesc_" + idControl;
        var idTxtSharedFileFolderStructure = "txtSharedFileFolderStructure_" + idControl;

        var html =
            ' <tr class="rowSharedFile" id="' + idRow + '" idDB="' + objSharedDriveFile.ID + '"> ' +
            '     <td> ' +
            '         <div class="form-control txtSharedFileName" placeholder="Add Name..." name="' + idTxtSharedFileName + '" id="' + idTxtSharedFileName + '" contenteditable="true">' + objSharedDriveFile.FileName + '</div> ' +
            '     </td> ' +
            '     <td> ' +
                      //TODO: Create a table to store all available extentions of files
            '         <select name="' + idSelSharedFileExt + '" id="' + idSelSharedFileExt + '" class="form-control selSharedFileExt"> ' +
            '             <option value="xlsx">.xlsx (Excel)</option> ' +
            '             <option value="docx">.docx (Word)</option> ' +
            '             <option value="pptx">.pptx (Power Point)</option> ' +
            '             <option value="zip">.zip (Compressed Archive)</option> ' +
            '             <option value="msg">.msg (Outlook Message)</option> ' +
            '             <option value="pdf">.pdf (PDF Document)</option> ' +
            '         </select> ' +
            '     </td> ' +
            '     <td> ' +
            '         <div class="form-control txtSharedFileDesc" placeholder="Add Description..." name="' + idTxtSharedFileDesc + '" id="' + idTxtSharedFileDesc + '" contenteditable="true">' + objSharedDriveFile.Description + '</div> ' +
            '     </td> ' +
            '     <td> ' +
            '         <div class="form-control txtSharedFileFolderStructure" placeholder="e. g. \[YYYY]\[MM]\{ActivityName}\{FileName}.{FileExt}" name="' + idTxtSharedFileFolderStructure + '" id="' + idTxtSharedFileFolderStructure + '" contenteditable="true" >' + objSharedDriveFile.FolderStructure + '</div> ' +
            '     </td> ' +
            '     <td> ' +
            '         <button type="button" class="btn btn-danger btn-sm btnDeleteFile" idRow="' + idRow + '"> ' +
            '             <i class="fa fa-trash-o"></i> ' +
            '         </button> ' +
            '     </td> ' +
            ' </tr> ';

        return html;
    }
}

function getObjSharedDriveFileByIDRow(idRow) {
    var id = $("#" + idRow).attr("idDB");
    var name = $("#" + idRow).find(".txtSharedFileName").text();
    var ext = $("#" + idRow).find(".selSharedFileExt").val();
    var description = $("#" + idRow).find(".txtSharedFileDesc").text();
    var folderStructure = $("#" + idRow).find(".txtSharedFileFolderStructure").text();

    return {
        ID: id,
        IDWorkingFile: 0,
        FolderStructure: folderStructure,
        FileName: _cleanString(name, true),
        FileExt: ext,
        Description: description,
        IsRequired: 1
    }
}

function getObjActivityTeamSelected() {
    var objTeamResult = {
        ID: 0,
        IDMSGroup: 0
    }

    var objTeamSelected = _findOneObjByProperty(_listTeamsOfUser, "ID", $("#selActivityTeam").val());
    if (objTeamSelected) {
        objTeamResult = objTeamSelected;
    }

    return objTeamResult;
}

function loadSelectOfChecklist(customOptions) {
    //Default Options.
    var options = {
        idMSGroup: "0",
        idChecklistSelected: "0",
        onSuccess: null
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    //Get checklist
    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Loading checklist...",
        name: "[dbo].[spPCTManageChecklist]",
        params: [
            { "Name": "@Action", "Value": "List Check List Of MSGroup" },
            { "Name": "@IDMSGroup", "Value": options.idMSGroup }
        ],
        success: function (resultList) {
            //Clear old values
            $("#selChecklist").contents().remove();

            //Add defaul option
            $("#selChecklist").append($('<option>', {
                value: 0,
                text: "-- No Required --"
            }));

            //Add Options
            for (var i = 0; i < resultList.length; i++) {
                var objTemp = resultList[i];
                $("#selChecklist").append($('<option>', {
                    value: objTemp.ID,
                    text: objTemp.Name,
                    selected: (objTemp.ID == options.idChecklistSelected)
                }));
            }

            //Add validation
            //$("#selChecklist").rules("add", {
            //    valueNotEquals: 0
            //});

            //Run on success
            if (options.onSuccess) {
                options.onSuccess();
            }
        }
    });
}

function loadSelectOfActivityTeam(customOptions) {
    //Default Options.
    var options = {
        objActivity: {},
        onSuccess: null
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    //Load teams of user
    _getTeamsOfUser({
        onSuccess: function (resultList) {
            //Default label name to select of Activity Team
            var labelSelActivityTeam = "Activity Team";
            var teamsToShow = [];
            var idTeamSelected = 0;

            //Calculate Teams to show
            var selectedTeam = _findOneObjByProperty(resultList, "ID", options.objActivity.IDTeam);

            //When selected team have childs show only childs
            if (parseInt(selectedTeam.TotalChilds) > 0) {
                teamsToShow = _findAllObjByProperty(resultList, "IDParentTeam", selectedTeam.ID);
                labelSelActivityTeam = selectedTeam.ChildsLabelName;
            } else {
                //When selected team have parent show childs parent
                if (selectedTeam.IDParentTeam) {
                    var objParentTeam = _findOneObjByProperty(resultList, "ID", selectedTeam.IDParentTeam);

                    //If user have access to Parent Team
                    if (objParentTeam) {
                        teamsToShow = _findAllObjByProperty(resultList, "IDParentTeam", objParentTeam.ID);
                        labelSelActivityTeam = objParentTeam.ChildsLabelName;
                    } else {
                        teamsToShow = [selectedTeam];
                        labelSelActivityTeam = selectedTeam.LabelName;
                    }

                    idTeamSelected = selectedTeam.ID;
                } else {
                    //Show all teams of the user
                    teamsToShow = resultList;
                    labelSelActivityTeam = selectedTeam.LabelName;
                }
            }

            //When is edit activity
            if ($("#HFAction").val() == "Edit") {
                idTeamSelected = options.objActivity.IDTeam;
            }

            //Clear current
            $("#selActivityTeam").contents().remove();

            //Add defaul option
            $("#selActivityTeam").append($('<option>', {
                value: 0,
                text: "-- Select --"
            }));

            //Add Options
            for (var i = 0; i < teamsToShow.length; i++) {
                var objTemp = teamsToShow[i];
                $("#selActivityTeam").append($('<option>', {
                    value: objTemp.ID,
                    text: objTemp.Name,
                    selected: objTemp.ID == idTeamSelected
                }));
            }

            //Set label of Activity Team
            $("#lblSelActivityTeam").html(labelSelActivityTeam + ":");

            //On Change update GOC of activity
            $("#selActivityTeam").change(function () {
                var objTeamSelected = _findOneObjByProperty(_listTeamsOfUser, "ID", $("#selActivityTeam").val());
                var GOCDesc = "";
                var shareDrivePath = "";
                var folderStructure = "";

                if (objTeamSelected) {
                    GOCDesc = objTeamSelected.GOCDesc;
                    shareDrivePath = objTeamSelected.SharedFolderPath;
                    folderStructure = objTeamSelected.FolderStructure;
                }

                //Render CFields for new team
                renderCFields({
                    forceRefresh: true,
                    objActivity: options.objActivity,
                    onSuccess: function () { }
                });

                //Select Checklist of team
                if (objTeamSelected) {
                    loadSelectOfChecklist({
                        idMSGroup: objTeamSelected.IDMSGroup,
                        idChecklistSelected: options.objActivity.IDChecklistChecker
                    });
                }

                //Update GOC
                $("#lblGOC").html(GOCDesc);

                //Update Share Drive Label
                $("#lblShareDrivePath").html(shareDrivePath);
            });

            //Set current GOC of activity
            $("#selActivityTeam").change();

            //Trigger On Success
            if (options.onSuccess) {
                options.onSuccess();
            }
        }
    });
}

function renderCFields(customOptions) {

    //Default Options.
    var options = {
        forceRefresh: false,
        objActivity: {},
        onSuccess: null
    };

    //Do merge of customOptions with default options.
    $.extend(true, options, customOptions);

    //Get CField by Team of Activity
    var idTeamSelected = $("#selActivityTeam").val();
    _getCFields({
        forceRefresh: options.forceRefresh,
        idTeam: idTeamSelected,
        onSuccess: function (cfieldList) {
            //Clean old data
            $("#cfieldContainer").contents().remove();

            //Render CFields
            for (var i = 0; i < cfieldList.length; i++) {
                renderOneCField(cfieldList[i]);
            }

            //Run onSuccess when all CFields are rendered
            _execOnAjaxComplete(function () {
                options.onSuccess();
            });
        }
    });

    function renderOneCField(objCField) {
        var idSelCField = 'selCField' + objCField.Key;
        var idBtnManageCField = 'btnManageCField' + objCField.Key;
        var fieldHtml =
        ' <div class="col-md-6"> ' +
        '     <div class="form-group"> ' +
        '         <label class="col-md-4 control-label"> ' +
        '             <i class="fa fa-angle-double-right"></i> ' + objCField.Name +
        '         </label> ' +
        '         <div class="col-md-6"> ' +
        '             <select id="' + idSelCField + '" name="' + idSelCField + '" class="form-control"></select> ' +
        '         </div> ' +
        '         <div class="col-md-2"> ' +
        '             <button type="button" class="btn btn-default btn-sm ' + idBtnManageCField + '"> ' +
        '                 <i class="fa fa-pencil"></i> ' +
        '             </button> ' +
        '         </div> ' +
        '     </div> ' +
        ' </div> ';

        //Add new field
        $("#cfieldContainer").append(fieldHtml);

        //Add Validation rule
        $("#cfieldContainer").find("#" + idSelCField).rules("add", {
            valueNotEquals: 0
        });

        //On Click Manage CField Options
        $("#cfieldContainer").find("." + idBtnManageCField).click(function () {
            openModalManageCFieldOptions();
        });

        //Load Field Options
        loadCFieldOptions();

        //Load options
        function loadCFieldOptions() {
            _callProcedure({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading " + objCField.Name + "...",
                name: "[dbo].[spPCTManageCFieldOption]",
                params: [
                    { "Name": "@Action", "Value": 'List' },
                    { "Name": "@CFieldKey", "Value": objCField.Key }
                ],
                success: {
                    fn: function (resultList) {
                        //Get Select
                        $selCField = $("#cfieldContainer").find("#" + idSelCField);

                        //Remove current options
                        $selCField.contents().remove();

                        //Add defaul option
                        $selCField.append($('<option>', {
                            value: 0,
                            text: "-- Select --",
                            selected: true
                        }));

                        //Add Options
                        for (var i = 0; i < resultList.length; i++) {
                            var objTemp = resultList[i];
                            $selCField.append($('<option>', {
                                value: objTemp.ID,
                                text: objTemp.Name
                            }));
                        }

                        //Select current objActivity value
                        if (options.objActivity['ID' + objCField.Key]) {
                            $selCField.val(options.objActivity['ID' + objCField.Key]);
                        }
                    }
                }
            });
        }

        //Manage CField Options
        function openModalManageCFieldOptions(customCallerOptions) {
            //Default Options.
            var localOptions = {
                onChangesSaved: function () {
                    //Refresh options
                    loadCFieldOptions();
                }
            };

            //Hacer un merge con las opciones que nos enviaron y las default.
            $.extend(true, localOptions, customCallerOptions);

            var idModalCFieldOption = _createCustomID();
            var idTableCFieldOptions = "tblManageCFieldOption_" + idModalCFieldOption;

            var htmlContentModal =
                '<div class="row"> ' +
                '    <div class="col-md-12"> ' +
                '        <button type="button" class="btnNew btn btn-success top15 left15 bottom15 pull-right"><i class="fa fa-plus-square"></i> New</button> ' +
                '        <button type="button" class="btnDelete btn btn-danger top15 left15 bottom15 pull-right"><i class="fa fa-trash-o"></i> Delete</button> ' +
                '    </div> ' +
                '    <div class="col-md-12"> ' +
                '        <div id="' + idTableCFieldOptions + '"></div> ' +
                '    </div> ' +
                '</div> ';

            _showModal({
                modalId: "modalManageCFieldOption",
                width: '70%',
                addCloseButton: true,
                buttons: [{
                    name: "<i class='fa fa-save'></i> Save",
                    class: "btn-success",
                    closeModalOnClick: false,
                    onClick: function ($modal) {
                        saveTable();
                    }
                }],
                title: "Manage " + objCField.Name,
                contentHtml: htmlContentModal,
                onReady: function ($modal) {
                    //Add new row to "#" + idTable
                    $modal.find(".btnNew").click(function () {
                        newRow();
                    });

                    //Delete selected row to "#" + idTable
                    $modal.find(".btnDelete").click(function () {
                        deleteRow();
                    });

                    //Load table 
                    loadTable();
                }
            });

            function loadTable() {
                $.jqxGridApi.create({
                    showTo: "#" + idTableCFieldOptions,
                    options: {
                        //for comments or descriptions
                        height: "250",
                        autoheight: false,
                        autorowheight: false,
                        selectionmode: "singlerow",
                        showfilterrow: true,
                        sortable: true,
                        editable: true,
                        groupable: false
                    },
                    sp: {
                        Name: "[dbo].[spPCTManageCFieldOption]",
                        Params: [
                            { "Name": "@Action", "Value": 'List' },
                            { "Name": "@CFieldKey", "Value": objCField.Key }
                        ]
                    },
                    source: {
                        // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
                        dataBinding: "Large Data Set"
                    },
                    columns: [
                        //type: string - text - number - int - float - date - time 
                        //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                        //cellsformat: ddd, MMM dd, yyyy h:mm tt
                        { name: 'ID', type: 'number', hidden: true },
                        { name: 'Name', text: objCField.Name, width: '100%', type: 'string', filtertype: 'input' }
                    ],
                    ready: function () {}
                });
            }

            function newRow() {
                var $jqxGrid = $("#" + idTableCFieldOptions);
                var datarow = {
                    ID: 0,
                    Name: 'Click here and type ' + objCField.Name + ' Name...'
                };
                var commit = $jqxGrid.jqxGrid('addrow', null, datarow);

                _showNotification("success", "An empty row was added in the table.", "NewCFieldOptionRow");
            }

            function deleteRow() {
                //Delete 
                var objRowSelected = $.jqxGridApi.getOneSelectedRow("#" + idTableCFieldOptions, true);
                if (objRowSelected) {
                    var htmlContentModal = "";
                    htmlContentModal += "<b>" + objCField.Name + ": </b>" + objRowSelected['Name'] + "<br/>";

                    _showModal({
                        width: '40%',
                        modalId: "modalDelRow",
                        addCloseButton: true,
                        buttons: [{
                            name: "<i class='fa fa-trash-o'></i> Delete",
                            class: "btn-danger",
                            onClick: function () {
                                //Call server to delete only if exists ID in database 
                                if (objRowSelected.ID != 0) {

                                    _callProcedure({
                                        loadingMsgType: "fullLoading",
                                        loadingMsg: "Deleting " + objCField.Name + "...",
                                        name: "[dbo].[spPCTManageCFieldOption]",
                                        params: [
                                            { "Name": "@Action", "Value": "Delete" },
                                            { "Name": "@CFieldKey", "Value": objCField.Key },
                                            { "Name": "@IDCFieldOption", "Value": objRowSelected.ID },
                                            { "Name": "@SessionSOEID", "Value": _getSOEID() }
                                        ],
                                        success: {
                                            showTo: $("#" + idTableCFieldOptions).parent(),
                                            msg: objCField.Name + " '" + objRowSelected['Name'] + "' was deleted successfully.",
                                            fn: function () {
                                                //Refresh changes
                                                loadTable();

                                                //Remove changes
                                                $.jqxGridApi.rowsChangedFindById("#" + idTableCFieldOptions).rows = [];

                                                //Refresh caller
                                                if (localOptions.onChangesSaved) {
                                                    localOptions.onChangesSaved();
                                                }
                                            }
                                        }
                                    });
                                } else {
                                    $("#" + idTableCFieldOptions).jqxGrid('deleterow', objRowSelected.uid);
                                }
                            }
                        }],
                        title: "Are you sure you want to delete this row?",
                        contentHtml: htmlContentModal
                    });
                }
            }

            function saveTable() {
                var rowsChanged = $.jqxGridApi.rowsChangedFindById("#" + idTableCFieldOptions).rows;

                //Data is valid
                if (isValidData()) {

                    //Function to show in the last saved item
                    var fnSuccess = {
                        showTo: $("#" + idTableCFieldOptions).parent(),
                        msg: "Data was saved successfully.",
                        fn: function () {

                            //Refresh table until all ajax call are completed
                            _execOnAjaxComplete(function () {
                                //Refresh table
                                loadTable();

                                //Remove local cache of changes
                                $.jqxGridApi.rowsChangedFindById("#" + idTableCFieldOptions).rows = [];

                                //Refresh caller
                                if (localOptions.onChangesSaved) {
                                    localOptions.onChangesSaved();
                                }
                            });
                        }
                    };

                    for (var i = 0; i < rowsChanged.length; i++) {
                        var objRow = rowsChanged[i];
                        var action = "Edit";

                        if (objRow.ID == 0) {
                            var action = "New";
                        }

                        _callProcedure({
                            loadingMsgType: "fullLoading",
                            loadingMsg: "Saving " + objCField.Name + "...",
                            name: "[dbo].[spPCTManageCFieldOption]",
                            params: [
                                { "Name": "@Action", "Value": action },
                                { "Name": "@CFieldKey", "Value": objCField.Key },
                                { "Name": "@IDCFieldOption", "Value": objRow.ID },
                                { "Name": "@Name", "Value": _cleanString(objRow.Name, true) },
                                { "Name": "@SessionSOEID", "Value": _getSOEID() }
                            ],
                            //Show message only for the last item
                            success: (i == rowsChanged.length - 1) ? fnSuccess : null
                        });
                    }
                }

                //Validate information to save
                function isValidData() {
                    var result = true;

                    //Check if exists changes
                    if (rowsChanged.length == 0) {
                        _showAlert({
                            id: "ManageCFieldOptionAlert",
                            showTo: $("#" + idTableCFieldOptions).parent(),
                            type: 'error',
                            title: "Message",
                            content: "No changes detected."
                        });

                        result = false;
                    }

                    //Check empty rows
                    for (var i = 0; i < rowsChanged.length; i++) {
                        var objRow = rowsChanged[i];

                        if (objRow.Name == 'Click here and type ' + objCField.Name + ' Name...') {
                            _showAlert({
                                id: "ManageCFieldOptionAlert",
                                showTo: $("#" + idTableCFieldOptions).parent(),
                                type: 'error',
                                title: "Message",
                                content: "Cannot save empty row <b>Click here and type " + objCField.Name + " Name...</b>"
                            });
                            result = false;
                        }

                        if (!objRow.Name) {
                            _showAlert({
                                id: "ManageCFieldOptionAlert",
                                showTo: $("#" + idTableCFieldOptions).parent(),
                                type: 'error',
                                title: "Message",
                                content: "Cannot save empty row"
                            });
                            result = false;
                        }
                    }

                    //Check Unique Values
                    var uniqueValues = {};
                    var uniqueValuesIsOk = true;
                    var lastDuplicateValue = "";
                    var allRows = $("#" + idTableCFieldOptions).jqxGrid("getRows");
                    for (var i = 0; i < allRows.length; i++) {
                        var objRow = allRows[i];

                        //Validate for unique values
                        if (!uniqueValues[objRow.Name]) {
                            uniqueValues[objRow.Name] = true;
                        } else {
                            uniqueValuesIsOk = false;
                            lastDuplicateValue = objRow.Name;
                        }
                    }
                    if (result == true) {
                        if (uniqueValuesIsOk == false) {
                            _showAlert({
                                id: "ManageCFieldOptionAlert",
                                showTo: $("#" + idTableCFieldOptions).parent(),
                                type: 'error',
                                title: "Message",
                                content: "Please remove duplicate values '" + lastDuplicateValue + "'"
                            });
                            result = false;
                        }
                    }

                    return result;
                }
            }
        }
    }
}

function getObjActivity() {
    var objActivity = {
        ID: $("#HFIDActivity").val(),
        IDTeam: $("#selActivityTeam").val(),
        IDFrequency: $("#lblActivityFrequency").attr("IDFrequency"),
        IDChecklistChecker: $("#selChecklist").val(),
        Name: _cleanString($("#txtName").text()),
        FrequencyUTCStartDateTime: $("#lblActivityFrequency").attr("FrequencyUTCStartDateTime"),
        StrListEDCFC: $("#containerEDCFCMapping").attr("StrListEDCFC"),
        StrListMakerContacts: $("#containerMakerContacts").attr("StrListContact"),
        StrListCheckerContacts: $("#containerCheckerContacts").attr("StrListContact"),
        StrListStakeholderContacts: $("#containerStakeholderContacts").attr("StrListContact")
    };

    return objActivity;
}

function generateString(data, property, character) {
    var result = "";
    for (var i = 0; i < data.length; i++) {
        result += data[i][property] + character;
    }

    if (data.length > 0) {
        result = result.substring(0, result.length - (character.length));
    }
    return result;
}

function saveSharedDriveFiles(customOptions) {

    //Default Options.
    var options = {
        idActivity: "0",
        onSuccess: null
    };

    //Do merge of customOptions with default options.
    $.extend(true, options, customOptions);

    //Delete files
    deleteFiles({
        onSuccess: function () {
            //Save New or Updated Files
            saveFiles({
                onSuccess: function () {
                    options.onSuccess();
                }
            });
        }
    });
    
    function saveFiles(customSaveOptions) {

        //Default Options.
        var saveOptions = {
            onSuccess: null
        };

        //Do merge of customOptions with default options.
        $.extend(true, saveOptions, customSaveOptions);

        //Function to show in the last saved item
        var fnSuccessLastItemSaved = {
            fn: function () {

                //Refresh table until all ajax call are completed
                _execOnAjaxComplete(function () {

                    //Call onSuccess 
                    saveOptions.onSuccess();

                });
            }
        };

        //By each Shared File
        var $rowSharedFiles = $(".rowSharedFile:not(.rowSharedFileDeleted)");
        if ($rowSharedFiles.length > 0) {
            $rowSharedFiles.each(function (index, elementRow) {
                var action = "New";
                var objSharedFile = getObjSharedDriveFileByIDRow($(elementRow).attr("id"));

                //Whe exist in Database
                if (objSharedFile.ID != "0") {
                    action = "Edit";
                }

                //Save change to get the ID
                _callProcedure({
                    loadingMsgType: "fullLoading",
                    loadingMsg: "Saving Shared Files of Activity...",
                    name: "[dbo].[spPCTManageSharedDriveFile]",
                    params: [
                    { "Name": "@Action", "Value": action },
                    { "Name": "@IDSharedDriveFile", "Value": objSharedFile.ID },
                    { "Name": "@IDActivity", "Value": options.idActivity },
                    { "Name": "@FileName", "Value": _cleanString(objSharedFile.FileName, true) },
                    { "Name": "@FileExt", "Value": objSharedFile.FileExt },
                    { "Name": "@Description", "Value": objSharedFile.Description },
                    { "Name": "@FolderStructure", "Value": objSharedFile.FolderStructure },
                    { "Name": "@SessionSOEID", "Value": _getSOEID() }
                ],
                    //Show message only for the last item
                    success: (index == ($rowSharedFiles.length - 1)) ? fnSuccessLastItemSaved : null
                });
            });
        } else {
            //Call onSuccess 
            saveOptions.onSuccess();
        } 
    }

    function deleteFiles(customDeleteOptions) {

        //Default Options.
        var deleteOptions = {
            onSuccess: null
        };

        //Do merge of customOptions with default options.
        $.extend(true, deleteOptions, customDeleteOptions);

        //Function to show in the last deleted item
        var fnSuccessLastItemDeleted = {
            fn: function () {

                //Refresh table until all ajax call are completed
                _execOnAjaxComplete(function () {

                    //Call onSuccess 
                    deleteOptions.onSuccess();

                });
            }
        };

        //By each Deleted 
        var $rowSharedFileDeleted = $(".rowSharedFileDeleted");
        if ($rowSharedFileDeleted.length > 0) {
            $rowSharedFileDeleted.each(function (index, elementRow) {
                var objSharedFile = getObjSharedDriveFileByIDRow($(elementRow).attr("id"));

                //Save change to get the ID
                _callProcedure({
                    loadingMsgType: "fullLoading",
                    loadingMsg: "Saving Shared Files of Activity...",
                    name: "[dbo].[spPCTManageSharedDriveFile]",
                    params: [
                    { "Name": "@Action", "Value": "Delete" },
                    { "Name": "@IDSharedDriveFile", "Value": objSharedFile.ID },
                    { "Name": "@SessionSOEID", "Value": _getSOEID() }
                ],
                    //Show message only for the last item
                    success: (index == ($rowSharedFileDeleted.length - 1)) ? fnSuccessLastItemDeleted : null
                });
            });
        } else {
            //Call onSuccess 
            deleteOptions.onSuccess();
        } 
    }
}

function saveCFieldValues(customOptions) {

    //Default Options.
    var options = {
        idActivity: "0",
        onSuccess: null
    };

    //Do merge of customOptions with default options.
    $.extend(true, options, customOptions);

    //Create base SQL
    var sql =
        " --Save CField Values of Activity \n " +
        " MERGE [dbo].[tblPCTActivityXCFieldValue] TTarget \n " +
        "     USING ( \n " +
        "         SELECT \n " +
        "             @sqlPartSelect \n " +
	    
        " ) AS TSource ON TTarget.[IDActivity] = @idActivity \n " +
	    
        " WHEN MATCHED THEN \n " +
	    "     UPDATE SET \n " +
	    "     @sqlPartUpdate \n " +
        
        " WHEN NOT MATCHED BY TARGET THEN \n " +
	    "     INSERT ( \n " +
        "         [IDActivity], \n " +
		"         @sqlPartInsertCols \n " +
	    "     ) \n " +
	    "     VALUES ( \n " +
        "         @idActivity, \n " +
		"         @sqlPartInsertValues \n " +
	    "     ); \n " +
        " SELECT CFV.* FROM [dbo].[tblPCTActivityXCFieldValue] CFV WHERE CFV.[IDActivity] = @idActivity ";

    _getCFields({
        idTeam: $("#selActivityTeam").val(),
        onSuccess: function (cfieldList) {
            //Save only if Activity have CFields
            if (cfieldList.length > 0) {
                var sqlPartSelect = "";
                var sqlPartUpdate = "";
                var sqlPartInsertCols = "";
                var sqlPartInsertValues = "";

                for (var i = 0; i < cfieldList.length; i++) {
                    var tempCField = cfieldList[0];
                    var cfieldValue = $("#selCField" + tempCField.Key).val();
                    sqlPartSelect += " " + cfieldValue + " AS [ID" + tempCField.Key + "], \n ";
                    sqlPartUpdate += " TTarget.[ID" + tempCField.Key + "] = TSource.[ID" + tempCField.Key + "], \n ";
                    sqlPartInsertCols += " [ID" + tempCField.Key + "], \n ";
                    sqlPartInsertValues += " TSource.[ID" + tempCField.Key + "], \n ";
                }

                //Remove last ", \n "
                sqlPartSelect = sqlPartSelect.slice(0, -4);
                sqlPartUpdate = sqlPartUpdate.slice(0, -4);
                sqlPartInsertCols = sqlPartInsertCols.slice(0, -4);
                sqlPartInsertValues = sqlPartInsertValues.slice(0, -4);

                //Replace Values in SQL
                sql = _replaceAll("@idActivity", options.idActivity, sql);
                sql = sql.replace("@sqlPartSelect", sqlPartSelect);
                sql = sql.replace("@sqlPartUpdate", sqlPartUpdate);
                sql = sql.replace("@sqlPartInsertCols", sqlPartInsertCols);
                sql = sql.replace("@sqlPartInsertValues", sqlPartInsertValues);

                //Save CField Values in database
                _callServer({
                    loadingMsgType: "fullLoading",
                    loadingMsg: "Saving Activity...",
                    url: '/Shared/ExecQuery',
                    data: { 'pjsonSql': _toJSON(sql) },
                    type: "post",
                    success: function (resultList) {
                        options.onSuccess();
                    }
                });
            } else {
                options.onSuccess();
            }
        }
    });
}

function saveActivity() {
    if ($("#formActivity").valid()) {
        var objActivity = getObjActivity();
        var action = "New";

        if ($("#HFAction").val() == "Edit") {
            action = "Edit";
        }

        //Save change to get the ID
        _callProcedure({
            loadingMsgType: "fullLoading",
            loadingMsg: "Saving Activity...",
            name: "[dbo].[spPCTManageActivity]",
            params: [
                { "Name": "@Action", "Value": action },
                { "Name": "@IDActivity", "Value": objActivity.ID },
                { "Name": "@IDTeam", "Value": objActivity.IDTeam },
                { "Name": "@IDFrequency", "Value": objActivity.IDFrequency },
                { "Name": "@IDChecklistChecker", "Value": objActivity.IDChecklistChecker },
                { "Name": "@Name", "Value": objActivity.Name },
                { "Name": "@FrequencyUTCStartDateTime", "Value": objActivity.FrequencyUTCStartDateTime },
                { "Name": "@StrListEDCFC", "Value": objActivity.StrListEDCFC },
                { "Name": "@StrListMakerContacts", "Value": objActivity.StrListMakerContacts },
                { "Name": "@StrListCheckerContacts", "Value": objActivity.StrListCheckerContacts },
                { "Name": "@StrListStakeholderContacts", "Value": objActivity.StrListStakeholderContacts },
                { "Name": "@SessionSOEID", "Value": _getSOEID() }
            ],
            success: {
                fn: function (response) {
                    if (response.length > 0) {
                        var objActivitySaved = response[0];

                        //Save CField Values of Activity
                        saveCFieldValues({
                            idActivity: objActivitySaved.ID,
                            onSuccess: function () {

                                //Save Share Drive Files
                                saveSharedDriveFiles({
                                    idActivity: objActivitySaved.ID,
                                    onSuccess: function () {

                                        //Show Message to user
                                        _showAlert({
                                            id: "ManageActivityAlert",
                                            type: 'success',
                                            title: "Message",
                                            content: "Activity " + objActivity.Name + " was saved successfully.",
                                            animateScrollTop: true
                                        });

                                        if (refreshAdminActivity) {
                                            //Refresh Admin Activity Page
                                            refreshAdminActivity();

                                            //Go to the Activity List Tab
                                            $("#tab-label-list-activity").find("a").click();

                                            //Close current Form Tab
                                            $("#tab-label-activity").hide();
                                            $("#adminActivityContent").contents().remove();

                                        }
                                    } 
                                });
                            }
                        });
                    }
                }
            }
        });
    } else {
        _showAlert({
            id: "ManageActivityAlert",
            type: 'error',
            title: "Message",
            content: "Please review errors.",
            animateScrollTop: true
        });
    }
}