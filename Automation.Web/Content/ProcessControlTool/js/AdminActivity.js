/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="ProcessControlTool.js" />

var regionList = {};

//Hide Menu
_hideMenu();

//On document ready
$(document).ready(function () {

    //On Click Edit Activity
    $(".btnEditActivity").click(function () {
        editActivity();
    });

    //On Click New Activity
    $(".btnNewActivity").click(function () {
        newActivity();
    });

    //On Click Delete Activity
    $(".btnDeleteActivity").click(function () {
        deleteActivity();
    });

    //TODO: On Click Toggle Select All
    //$(".btnToggleSelectAll").click(function () {
    //    _toggleSelectAll("#tblActivities");
    //});

    //TODO: On Click Copy Contacts
    //$(".btnCopyContacts").click(function () {
    //    copyContacts();
    //});

    //TODO: On Click Download Activities Report
    //$(".btnExportToExcel").click(function () {
    //    downloadToExcelActivityList();
    //});

    //On Click Check Frequency
    $(".btnCheckFrequency").click(function () {
        checkFrequency();
    });

    //When all ajax are completed (_loadTimeZone();, _updateCurrentUserDLs();)
    _execOnAjaxComplete(function () {
        //Load Teams
        loadSelectOfTeam({
            onSuccess: function () {
                
                //Validate if URL have parameter to edit directly /ProcessControlTool/AdminActivity?peditIDActivity=3
                var idActivityToEdit = _getQueryStringParameter("peditIDActivity");
                if (idActivityToEdit) {
                    editActivity(idActivityToEdit);
                } else {
                    //Load table with all activities of selected team
                    loadAdminActivitiesTable();
                }

                //Hide Full Page Loading
                _hideLoadingFullPage({
                    id: "ActivityPageLoading"
                });
            }
        });
    });
});

function refreshAdminActivity() {
    //Load Teams
    loadSelectOfTeam({
        forceRefresh: true,
        idTeamSelected: getObjTeamSelected().IDTeam,
        onSuccess: function () {
            //Load table with all activities of selected team
            loadAdminActivitiesTable();
        }
    });
}

function checkFrequency() {
    var selectedRow = $.jqxGridApi.getOneSelectedRow("#tblActivities", true);
    if (selectedRow) {
        var idModal = _createCustomID();
        var htmlContentModal =
            '<div class="row" style="height: 400px;"> ' +
            '    <div class="col-md-6"> ' +
                     //Start Date
            '        <div class="form-group"> ' +
            '            <label class="col-md-4 control-label"> ' +
            '                <i class="fa fa-asterisk iconasterisk"></i> Start Date ' +
            '            </label> ' +
            '            <div class="col-md-6"> ' +
            '                <input type="text" class="form-control txtStartDate" /> ' +
            '            </div> ' +
            '        </div> ' +
            '    </div> ' +
            '    <div class="col-md-6"> ' +
            
                     //End Date
            '        <div class="form-group"> ' +
            '            <label class="col-md-4 control-label"> ' +
            '                <i class="fa fa-asterisk iconasterisk"></i> End Date ' +
            '            </label> ' +
            '            <div class="col-md-6"> ' +
            '                <input type="text" class="form-control txtEndDate" /> ' +
            '            </div> ' +
            '        </div> ' +
            '    </div> ' +

            '    <div class="col-md-12" style="padding-top: 15px;"> ' +
            '        <div class="tblFrequencyDates" id="tblFrequencyDates_' + idModal + '"></div> ' +
            '    </div> ' +
            '</div> ';

        //Show user modal
        _showModal({
            modalId: "modalCheckFrequency",
            width: "90%",
            addCloseButton: true,
            title: "Activity Dates for " + selectedRow.Name + " " + selectedRow.FrequencyCode,
            contentHtml: htmlContentModal,
            onReady: function ($modal) {
                var currentDate = moment();
                var nextMonth = moment(currentDate).add(1, 'M');

                $modal.find(".txtStartDate").val(currentDate.format('MMM DD, YYYY'));
                $modal.find(".txtStartDate").datepicker();
                $modal.find(".txtStartDate").datepicker().on('changeDate', function (ev) {
                    //Load table 
                    loadTable($modal);
                });

                $modal.find(".txtEndDate").val(nextMonth.format('MMM DD, YYYY'));
                $modal.find(".txtEndDate").datepicker();
                $modal.find(".txtEndDate").datepicker().on('changeDate', function (ev) {
                    //Load table 
                    loadTable($modal);
                });

                //Load table 
                loadTable($modal);
            }
        });

        function loadTable($modal) {
            var idTable = "#tblFrequencyDates_" + idModal;

            //Get Start Date
            var startDateInput = $modal.find(".txtStartDate").val();
            var momentStartDate = moment(startDateInput, 'MMM DD, YYYY');
            var startDate = momentStartDate.format("YYYY-MM-DD");

            //Get End Date
            var endDateInput = $modal.find(".txtEndDate").val();
            var momentEndDate = moment(endDateInput, 'MMM DD, YYYY');
            var endDate = momentEndDate.format("YYYY-MM-DD");

            //Get ID Activity
            var idActivity = selectedRow.ID;

            //Create select of Frequencies
            var sqlFrequencies =
                " SELECT \n " +
                "     A.[ID], \n " +
                "     A.[IDTeam], \n " +
                "     A.[Name], \n " +
                "     TDates.[Type], \n " +
                "     TDates.[UTCDateTimeSLA], \n " +
                "     TDates.[UTCDateTimeCompleted], \n " +
                "     TDates.[LocalYear], \n " +
                "     TDates.[LocalMonth], \n " +
                "     TDates.[BDNum], \n " +
                "     TDates.[LocalDate], \n " +
                "     TDates.[LocalDateTimeSLA], \n " +
                "     TDates.[ActivityDetailStatus], \n " +
                "     TDates.[SLAStatus] \n " +
                " FROM  \n " +
                "     [dbo].[tblPCTActivity] A \n " +
                "         CROSS APPLY( \n " +
                "             SELECT *  \n " +
                "             FROM [dbo].[fnPCTActivityGetFrequencyDetails](A.[ID], " + _timeZoneGetUTCOffsetMinutes() + ", '" + _timeZoneGetCurrentDateTime() + "') \n " +
                "         ) TDates \n " +
                " WHERE  \n " +
                "     A.[ID] = " + idActivity + " AND \n " +
                "     TDates.[LocalDate] BETWEEN '" + startDate + "' AND '" + endDate + "' \n " +
                " ORDER BY \n " +
                "     TDates.[LocalDate] ASC ";

            if (startDate && endDate) {

                _callServer({
                    loadingMsgType: "topBar",
                    loadingMsg: "Loading Frequency Details...",
                    url: '/Shared/ExecQuery',
                    data: { 'pjsonSql': _toJSON(sqlFrequencies) },
                    type: "post",
                    success: function (resultList) {

                        var columns = [];
                        var dueDateWidth = "25%";

                        columns.push({ name: 'LocalYear', text: 'Year', width: '10%', type: 'string', filtertype: 'checkedlist' });
                        columns.push({ name: 'LocalMonth', text: 'Month', width: '10%', type: 'string', filtertype: 'checkedlist' });
                        
                        //Add BD Column
                        if (selectedRow.FrequencyType == "BD") {
                            dueDateWidth = "18%";
                            columns.push({ name: 'BDNum', text: 'BD', width: '7%', type: 'string', filtertype: 'checkedlist' });
                        }

                        columns.push({
                            name: 'ActivityDetailStatus', text: 'Status', width: '40%', type: 'string', filtertype: 'checkedlist', editable: false, filterable: true,
                            cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                                var htmlResult = '';
                                var badgeClass = "badge-warning";

                                //Add Status
                                switch (rowData.ActivityDetailStatus) {
                                    case "Completed":
                                        badgeClass = "badge-success";
                                        break;

                                    case "Pending Checker":
                                        badgeClass = "badge-info";
                                        break;

                                    case "Pending Maker":
                                        badgeClass = "badge-warning";
                                        break;
                                }

                                htmlResult += '<span class="badge badge-md ' + badgeClass + '" style="margin-top: 5px; margin-left: 5px;">' + rowData.ActivityDetailStatus + '</span>';

                                //Add History of comments
                                if (rowData.AuditLogsCount > 0) {
                                    var historyRows = JSON.parse(rowData.AuditLogsJson);
                                    if (historyRows.length > 0) {
                                        var historyInfoHtml = "<div style='padding: 10px; font-size: 15px; max-height: 500px; overflow-y:scroll; width: 520px;'>";

                                        for (var i = 0; i < historyRows.length; i++) {
                                            var tempData = historyRows[i];

                                            if (tempData.Type == "Escalation" ||
                                                tempData.Type == "File Rejected" ||
                                                tempData.Type == "File Deleted") {
                                                historyInfoHtml += "<b style='color:#f44336;'>" + tempData.Type + "</b><br>";
                                            } else {
                                                historyInfoHtml += "<b style='color:#4caf50;'>" + tempData.Type + "</b><br>";
                                            }

                                            historyInfoHtml += "<b>User:</b> (" + tempData.UserRole + ") " + tempData.CreatedByName + " [" + tempData.CreatedBySOEID + "]<br>";
                                            //historyInfoHtml += "<b>File Status:</b> " + tempData.FileStatus + "<br>";
                                            //historyInfoHtml += "<b>Status By Checker:</b> " + tempData.StatusByChecker + "<br>";

                                            if (tempData.Comment) {
                                                historyInfoHtml += "<b>Date:</b> " + tempData.CreatedDate + "<br>";
                                                historyInfoHtml += (tempData.Type == "Comment" ? "" : "<b>Comment:</b> ") + "<pre style='margin: 0 0 0px;'>" + _escapeToHTML(tempData.Comment) + "</pre>";
                                            } else {
                                                historyInfoHtml += "<b>Date:</b> " + tempData.CreatedDate + "<br>";
                                            }

                                            //Add Files
                                            if (rowData.AuditLogFilesCount > 0) {
                                                var auditLogFilesRows = JSON.parse(rowData.AuditLogFilesJson);


                                                //Validate files of current auditLog in for loop
                                                var auditLogFiles = _findAllObjByProperty(auditLogFilesRows, "IDPFDetailAuditLog", tempData.IDPFDetailAuditLog);

                                                if (auditLogFiles.length > 0) {
                                                    historyInfoHtml += "<b>Files:</b> <br>";
                                                }

                                                for (var j = 0; j < auditLogFiles.length; j++) {
                                                    historyInfoHtml += " - " + auditLogFiles[j].FileName + "." + auditLogFiles[j].FileExt + "<br>";
                                                }
                                            }

                                            //Add Sent to Emails
                                            if (rowData.NotificationEmailCount > 0) {
                                                var notificationEmailRows = JSON.parse(rowData.NotificationEmailsJson);

                                                //Validate files of current auditLog in for loop
                                                var notificationEmails = _findAllObjByProperty(notificationEmailRows, "IDPFNotification", tempData.IDPFNotification);

                                                if (notificationEmails.length > 0) {
                                                    historyInfoHtml += "<b>Email Sent to:</b> <br>";
                                                }

                                                for (var j = 0; j < notificationEmails.length; j++) {
                                                    if (notificationEmails[j].SOEID) {
                                                        historyInfoHtml += " - " + notificationEmails[j].Name + " [" + notificationEmails[j].SOEID + "]<br>";
                                                    } else {
                                                        historyInfoHtml += " - " + _escapeToHTML(notificationEmails[j].Email) + "<br>";
                                                    }
                                                }
                                            }

                                            historyInfoHtml += "<br>";
                                        }

                                        historyInfoHtml += "</div>";

                                        htmlResult +=
                                            '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="right" data-content="' + historyInfoHtml + '" data-title="History" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                            '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/info.png"/> ' +
                                            '</div>';
                                    }
                                }

                                return htmlResult;
                            }
                        });

                        columns.push({ name: 'LocalDateTimeSLA', text: 'SLA Datetime', type: 'date', cellsformat: 'yyyy-MM-dd hh:mm tt', width: dueDateWidth });
                        columns.push({
                            name: 'SLAStatus', text: 'SLA Status', width: '15%', type: 'string', filtertype: 'checkedlist', editable: false, filterable: true,
                            cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                                var htmlResult = '';
                                var badgeClass = "badge-warning";

                                switch (rowData.SLAStatus) {
                                    case "On Time":
                                        badgeClass = "badge-warning";
                                        break;

                                    case "Late":
                                        badgeClass = "badge-danger";
                                        break;
                                }
                                if (rowData.ActivityDetailStatus == "Completed") {
                                    badgeClass = "badge-success";
                                }

                                htmlResult = '<span style="line-height: 30px; margin-left: 10px;"><span class="badge badge-md ' + badgeClass + '">' + rowData.SLAStatus + '</span></span>';

                                return htmlResult;
                            }
                        });

                        $.jqxGridApi.create({
                            showTo: idTable,
                            options: {
                                height: "350",
                                autoheight: false,
                                autorowheight: false,
                                selectionmode: "singlerow",
                                showfilterrow: true,
                                sortable: true,
                                editable: true,
                                groupable: false
                            },
                            source: {
                                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                                dataBinding: "Large Data Set Local",
                                rows: resultList
                            },
                            //type: string - text - number - int - float - date - time 
                            //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                            //cellsformat: ddd, MMM dd, yyyy h:mm tt
                            columns: columns,
                            ready: function () {}
                        });
                    }
                });
            } else {
                _showAlert({
                    showTo: $modal.find(".modal-body"),
                    type: 'error',
                    title: "Message",
                    content: "Please select Start Date and End Date."
                });
            }
        }
    }
}

function getObjTeamSelected() {
    var result = {
        IDTeam: 0,
        IDMSGroup: 0,
        GroupLabelName: 'Group',
        LabelName: '',
        ChildsLabelName: '',
        TotalChilds: 0
    };
    var $selOption = $("#selTeam").find("option:selected");
    if ($selOption.length > 0) {
        var childsLabelName = $selOption.attr("childsLabelName");
        result.GroupLabelName = $selOption.attr("labelName");

        //If selected team have childs, set Group Label Name from Childs
        if (childsLabelName) {
            result.GroupLabelName = childsLabelName;
        }

        result.IDTeam = $selOption.attr("value");
        result.IDMSGroup = $selOption.attr("idMSGroup");
        result.LabelName = $selOption.attr("labelName");
        result.ChildsLabelName = $selOption.attr("childsLabelName");
        result.TotalChilds = $selOption.attr("totalChilds");
    }

    return result;
}

function loadSelectOfTeam(customOptions) {
    //Default Options.
    var options = {
        forceRefresh: false,
        idTeamSelected: "0",
        onSuccess: null
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    _getTeamsOfUser({
        forceRefresh: options.forceRefresh,
        onSuccess: function (resultList) {
            //Clear current
            $("#selTeam").contents().remove();

            //Check if user is admin of multiples MSGroups
            var adminMultipleMSGroups = false;
            if (resultList.length > 0) {
                var currentMSGroup = resultList[0].IDMSGroup;
                for (var i = 1; i < resultList.length; i++) {
                    if (resultList[i].IDMSGroup != currentMSGroup) {
                        adminMultipleMSGroups = true;
                        break;
                    }
                }
            }

            //Add defaul option
            $("#selTeam").append($('<option>', {
                value: 0,
                text: "-- Select --"
            }));

            //Add Options
            for (var i = 0; i < resultList.length; i++) {
                var objTemp = resultList[i];
                $("#selTeam").append($('<option>', {
                    value: objTemp.ID,
                    idMSGroup: objTemp.IDMSGroup,
                    totalChilds: objTemp.TotalChilds,
                    labelName: objTemp.LabelName,
                    childsLabelName: objTemp.ChildsLabelName,

                    //If user is admin of multiples groups show label "NAM ICG (Costa Rica) - CBNA (2)" if not show "CBNA (2)"
                    text: (adminMultipleMSGroups ? ("(" + objTemp.MSGroupName + ") ") : "") + objTemp.Tree + " (" + objTemp.TotalActivities + ")",
                    
                    //On firs load select first item, on refresh select the last item selected by the user
                    selected: (options.idTeamSelected == "0" ? (i == 0) : (objTemp.ID == options.idTeamSelected)) 
                }));
            }

            //When not exists team assigned 
            if (resultList.length == 0) {

                //Hide Select of Team
                $("#selTeamContainer").hide();

                //Hide tab activities of team container
                $("#tab-activity-list").hide();

                //Show Message to user
                _showAlert({
                    showTo: $("#tab-activity-list").parent(),
                    type: 'error',
                    title: "Message",
                    content: "You don't have any team assigned please contact the system administrator."
                });
            }

            //When exists only 1 team assigned
            if (resultList.length == 1) {

                //Hide Select of Team
                $("#selTeamContainer").hide();

                //Set team name
                $("#menu-permit-name").html("Manage Activities of " + resultList[0].Tree);
            }

            //When exists more than 1 team assigned
            if (resultList.length > 1) {

                //On change load new team
                $("#selTeam").change(function () {
                    loadAdminActivitiesTable();
                });

            }

            //Trigger On Success
            if (options.onSuccess) {
                options.onSuccess();
            }
        }
    });
}

function loadAdminActivitiesTable() {
    var idTable = "#tblActivities";
    var objTeamSelected = getObjTeamSelected();

    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Activities...",
        name: "[dbo].[spPCTManageActivity]",
        params: [
            { Name: "@Action", Value: "List Activities Of Team" },
            { Name: "@IDTeam", Value: objTeamSelected.IDTeam }
        ],
        success: function (activityList) {

            $.jqxGridApi.create({
                showTo: idTable,
                options: {
                    //for comments or descriptions
                    height: "500",
                    autoheight: false,
                    autorowheight: false,
                    showfilterrow: true,
                    sortable: true,
                    editable: true,
                    groupable: true,
                    //'singlerow', 'multiplerows', 'checkbox', 'multiplerowsadvanced' or 'multiplerowsextended'.
                    selectionmode: "singlerow"
                },
                source: {
                    // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                    dataBinding: "Large Data Set Local",
                    rows: activityList
                },
                groups: ['TeamName'],
                columns: [
                    //type: string - text - number - int - float - date - time 
                    //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                    //cellsformat: ddd, MMM dd, yyyy h:mm tt
                    //{ name: 'FolderPattern', text: 'Folder Pattern', type: 'string', width: '200px' },
                    { name: 'ID', type: 'number', hidden: 'true' },
                    { name: 'FrequencyType', type: 'string', hidden: 'true' },
                    { name: 'FrequencyCode', type: 'string', hidden: 'true' },
                    { name: 'JsonSharedDriveFiles', type: 'string', hidden: 'true' },
                    { name: 'JsonMakerContacts', type: 'string', hidden: 'true' },
                    { name: 'JsonCheckerContacts', type: 'string', hidden: 'true' },
                    { name: 'JsonStakeholderContacts', type: 'string', hidden: 'true' },
                    { name: 'IsSelected', text: "Select", type: 'bool', columntype: 'checkbox', width: '5%', pinned: true, filterable: false },
                    { name: 'Name', text: 'Name', filtertype: 'input', type: 'string', width: '35%' },
                    
                    { name: 'TeamName', text: objTeamSelected.GroupLabelName, filtertype: 'input', type: 'string', width: '9%' },
                    {
                        name: 'CountSharedDriveFiles', text: 'Drive Files', width: '9%', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center',
                        cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                            var htmlResult = '<span style="line-height:31px;margin-left:25px;">' + rowData.CountSharedDriveFiles + '</span>';

                            //Add SharedDriveFiles
                            if (rowData.JsonSharedDriveFiles) {
                                var rows = JSON.parse(rowData.JsonSharedDriveFiles);
                                if (rows.length > 0) {
                                    var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                                    for (var i = 0; i < rows.length; i++) {
                                        var tempData = rows[i];
                                        infoHtml += " \n " + tempData.FileName + "." + tempData.FileExt + "<br>";
                                    }

                                    infoHtml += "</div>";

                                    htmlResult +=
                                        '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="Files Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                        '       <img style="margin-right: 25px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png"/> ' +
                                        '</div>';
                                }
                            }

                            return htmlResult;
                        }
                    },
                    {
                        name: 'CountMakerContacts', text: 'Makers', width: '9%', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center',
                        cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                            var htmlResult = '<span style="line-height:31px;margin-left:25px;">' + rowData.CountMakerContacts + '</span>';

                            //Add Makers
                            if (rowData.JsonMakerContacts) {
                                var rows = JSON.parse(rowData.JsonMakerContacts);
                                if (rows.length > 0) {
                                    var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                                    for (var i = 0; i < rows.length; i++) {
                                        var tempData = rows[i];
                                        infoHtml += " \n " + tempData.Type + ": " + tempData.Description + "<br>";
                                    }

                                    infoHtml += "</div>";

                                    htmlResult +=
                                        '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="Makers Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                        '       <img style="margin-right: 25px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png"/> ' +
                                        '</div>';
                                }
                            }

                            return htmlResult;
                        }
                    },
                    {
                        name: 'CountCheckerContacts', text: 'Checkers', width: '9%', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center',
                        cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                            var htmlResult = '<span style="line-height:31px;margin-left:25px;">' + rowData.CountCheckerContacts + '</span>';

                            //Add Checkers
                            if (rowData.JsonCheckerContacts) {
                                var rows = JSON.parse(rowData.JsonCheckerContacts);
                                if (rows.length > 0) {
                                    var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                                    for (var i = 0; i < rows.length; i++) {
                                        var tempData = rows[i];
                                        infoHtml += " \n " + tempData.Type + ": " + tempData.Description + "<br>";
                                    }

                                    infoHtml += "</div>";

                                    htmlResult +=
                                        '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="Checkers Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                        '       <img style="margin-right: 25px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png"/> ' +
                                        '</div>';
                                }
                            }

                            return htmlResult;
                        }
                    },
                    {
                        name: 'CountStakeholderContacts', text: 'Stakeholders', width: '9%', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center',
                        cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                            var htmlResult = '<span style="line-height:31px;margin-left:25px;">' + rowData.CountStakeholderContacts + '</span>';

                            //Add Stakeholders
                            if (rowData.JsonStakeholderContacts) {
                                var rows = JSON.parse(rowData.JsonStakeholderContacts);
                                if (rows.length > 0) {
                                    var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                                    for (var i = 0; i < rows.length; i++) {
                                        var tempData = rows[i];
                                        infoHtml += " \n " + tempData.Type + ": " + tempData.Description + "<br>";
                                    }

                                    infoHtml += "</div>";

                                    htmlResult +=
                                        '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="Stakeholders Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                        '       <img style="margin-right: 25px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png"/> ' +
                                        '</div>';
                                }
                            }

                            return htmlResult;
                        }
                    },
                    { name: 'LastUpdatedInfo', text: 'Last Updated', filtertype: 'input', type: 'string', width: '15%' }
                ],
                ready: function () {
                    //Add popup of information
                    $(idTable).on("rowclick", function (event) {
                        _GLOBAL_SETTINGS.tooltipsPopovers();

                        //$('#grid').jqxGrid('selectrow', 10);
                    });

                    $(idTable).on('cellvaluechanged', function (event) {

                        //Autoselect row on click Checkbox
                        if (event.args.datafield == "IsSelected") {
                            if (args.newvalue) {
                                $(idTable).jqxGrid('selectrow', args.rowindex);
                            } else {
                                $(idTable).jqxGrid('unselectrow', args.rowindex);
                            }
                        }

                    });

                    // expand all groups.
                    $(idTable).jqxGrid('expandallgroups');
                }
            });
        }
    });
}

function downloadToExcelActivityList() {
    _downloadExcel({
        spName: "[dbo].[spPCTListActivity]",
        spParams: [],
        filename: "Activities_" + moment().format('MMM-DD-YYYY-HH-MM'),
        success: {
            msg: "Please wait, generating report..."
        }
    });
}

function newActivity() {
    //Show tab with edit form
    $("#tab-label-activity").show();

    //Load form to add or edit an process file
    loadPartialViewActivityForm({
        showTo: "#adminActivityContent",
        idActivity: 0,
        action: "New"
    });

    //Go to the Form Tab
    $("#tab-label-activity").find("a").click();

    //Update tab label
    $("#lblActivityForm").html("New Activity");
}

function editActivity(idActivityToEdit) {
    
    //If exists from querystring
    if (idActivityToEdit) {
        loadPartialView(idActivityToEdit);
    } else {
        var rowData = $.jqxGridApi.getOneSelectedRow('#tblActivities', true);
        if (rowData) {
            loadPartialView(rowData.ID);
        }
    }

    function loadPartialView(idActivity) {
        //Show tab with edit form
        $("#tab-label-activity").show();

        //Load form to add or edit an process file
        loadPartialViewActivityForm({
            showTo: "#adminActivityContent",
            idActivity: idActivity,
            action: "Edit"
        });

        //Go to the Form Tab
        $("#tab-label-activity").find("a").click();

        //Update tab label
        $("#lblActivityForm").html("Edit Activity");
    }
}

function loadPartialViewActivityForm(customOptions) {
    var selectedTeam = getObjTeamSelected();

    //Default Options.
    var options = {
        showTo: "",
        idTeam: selectedTeam.IDTeam,
        idActivity: "",
        action: "New"
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Activity Form...",
        url: "/ProcessControlTool/PartialViewActivityForm?" + $.param({
            pidTeam: options.idTeam,
            pidActivity: options.idActivity,
            paction: options.action
        }),
        success: function (htmlResponse) {
            $(options.showTo).contents().remove();
            $(options.showTo).html(htmlResponse);
        }
    });
}

function deleteActivity() {
    //Delete 
    var objRowSelected = $.jqxGridApi.getOneSelectedRow('#tblActivities', true);
    if (objRowSelected) {
        var htmlContentModal = "";
        htmlContentModal += "<b>Activity: </b>" + objRowSelected['Name'] + "<br/>";
        htmlContentModal += "<b>" + getObjTeamSelected().GroupLabelName + ": </b>" + objRowSelected['TeamName'] + "<br/>";

        _showModal({
            width: '40%',
            modalId: "modalDelRow",
            addCloseButton: true,
            buttons: [{
                name: "Delete",
                class: "btn-danger",
                onClick: function () {
                    if (objRowSelected.ID != 0) {
                        _callProcedure({
                            loadingMsgType: "fullLoading",
                            loadingMsg: "Deleting activity...",
                            name: "[dbo].[spPCTManageActivity]",
                            params: [
                                { "Name": "@Action", "Value": "Delete" },
                                { "Name": "@IDActivity", "Value": objRowSelected.ID },
                                { "Name": "@SessionSOEID", "Value": _getSOEID() }
                            ],
                            //Show message only for the last 
                            success: {
                                fn: function () {
                                    //Refresh table on ajax complete
                                    _execOnAjaxComplete(function () {

                                        //Show Message to user
                                        _showAlert({
                                            id: "ActivityDeleteAlert",
                                            type: 'success',
                                            title: "Message",
                                            content: "Activity " + objRowSelected['Name'] + " was deleted successfully.",
                                            animateScrollTop: true
                                        });

                                        //Refresh Admin Activity Page
                                        refreshAdminActivity();

                                        //Remove changes
                                        $.jqxGridApi.rowsChangedFindById('#tblActivities').rows = [];
                                    });
                                }
                            }
                        });
                    } else {
                        $("#tblActivities").jqxGrid('deleterow', objRowSelected.uid);
                    }
                }
            }],
            title: "Are you sure you want to delete this row?",
            contentHtml: htmlContentModal
        });
    }
}