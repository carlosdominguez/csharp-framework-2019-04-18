﻿var _GlobalVars = {
    Region: [],
    FileRegion: [],
    countryList: [],
    accountList: [],
    BUListDelimit: [],
    addKey: [],
    CloneaccountList: [],
    mappedData: []
};

var RegionSelected;
var CountrySelected;
var QuaterSelected;
var YearSelected;
var ListTypeAccount;

var SelectedCFAccount;


var CloneQuaterSelected;

var viewModel = {
    Region: ko.observable(),
    FileRegion: ko.observable(),
    viewAddAccount: ko.observable(),
    showColumnCountry: ko.observable(),
    countryList: ko.observable(),
    AccountName: ko.observable().extend({ required: true }),
    AccountDescription: ko.observable().extend({ required: true }),
    accountList: ko.observable(),
    cbQuater: ko.observable(),
    cbYear: ko.observable(),
    ListTypeAccount: ko.observable(),
    BUListDelimit: ko.observableArray(),
    ClonecbQuater: ko.observable(),
    CloneaccountList: ko.observableArray(),
    CloneviewAddAccount: ko.observable(),
    cloneCheckAll: ko.observable(),
    checkAllBU: ko.observable(),
    CFAccountSelected: ko.observable()
};

viewModel.ListTypeAccountSelect = ko.dependentObservable({
    read: viewModel.ListTypeAccount,
    write: function (type) {
        ListTypeAccount = type;

        if (typeof type === 'undefined' || !type) {
        } else {
        }
    },
    owner: viewModel
});

viewModel.gridDelimitedBU = new KOGridModel({
    data: viewModel.BUListDelimit,
    columns: [
        { header: "BU", data: "BU" }
       , { header: "Delimit", data: "BUexist", dataTemplate: 'tplCb' }
    ],
    pageSize: 5
});


viewModel.gridAccountList = new KOGridModel({
    data: viewModel.accountList,
    columns: [
        { header: "Account", data: "CFAccount" },
        { header: "Description", data: "Description" },
        { header: "Type ", data: "Name" },
        { header: "Region", data: "Region" },
        { header: "Country", data: "Country" },
        { header: "BU Selected", data: "BUS" },
        { header: "Actions", template: 'divActions' }
    ],
    pageSize: 4
});

viewModel.gridCloneMapping = new KOGridModel({
    data: viewModel.CloneaccountList,
    columns: [
        { header: "Account", data: "CFAccount" },
        { header: "Description", data: "Description" },
        { header: "Type ", data: "Name" },
        { header: "Region", data: "Region" },
        { header: "Country", data: "Country" },
        { header: "Actions", data: "CloneCheck", dataTemplate: 'tplCbClone' }
    ],
    pageSize: 4
});

viewModel.YearSelected = ko.dependentObservable({
    read: viewModel.FileRegion,
    write: function (Year) {
        YearSelected = Year;

        var listOfParams = {
            Region: RegionSelected,
            Quater: QuaterSelected
            //Year: YearSelected
        }

        if ((typeof QuaterSelected === 'undefined' || !QuaterSelected) || (typeof RegionSelected === 'undefined' || !RegionSelected) ) {
            viewModel.viewAddAccount(false);
        } else {
            viewModel.viewAddAccount(true);
            BindJsonAlias("getCF_Account", "accountList", 'MappingKeys', listOfParams, function () {
                viewModel.accountList(_GlobalVars["accountList"]);
            });
        }
    },
    owner: viewModel
});

viewModel.QuaterSelected = ko.dependentObservable({
    read: viewModel.FileRegion,
    write: function (Quater) {
        QuaterSelected = Quater;

        var listOfParams = {
            Region: RegionSelected,
            Quater: QuaterSelected
            //Year: YearSelected
        }

        if ((typeof QuaterSelected === 'undefined' || !QuaterSelected) || (typeof RegionSelected === 'undefined' || !RegionSelected) ) {
            viewModel.viewAddAccount(false);
        } else {
            viewModel.viewAddAccount(true);
            BindJsonAlias("getCF_Account", "accountList", 'MappingKeys', listOfParams, function () {
                viewModel.accountList(_GlobalVars["accountList"]);
            });
        }
    },
    owner: viewModel
});

viewModel.regionSelect = ko.dependentObservable({
    read: viewModel.Region,
    write: function (Region) {
        RegionSelected = Region;

        var listOfParams = {
            Region: RegionSelected,
            Quater: QuaterSelected
            //Year: YearSelected
        }

        if (typeof Region === 'undefined' || !Region) {
        } else {
            BindJsonAlias("getCountry", "countryList", 'RegionFileAdmin', listOfParams, function () {
                viewModel.countryList(_GlobalVars["countryList"]);
            });
        }

        if ((typeof QuaterSelected === 'undefined' || !QuaterSelected) || (typeof RegionSelected === 'undefined' || !RegionSelected) ) {
            viewModel.viewAddAccount(false);
        } else {
            viewModel.viewAddAccount(true);
            BindJsonAlias("getCF_Account", "accountList", 'MappingKeys', listOfParams, function () {
                viewModel.accountList(_GlobalVars["accountList"]);
            });
        }
    },
    owner: viewModel
});

viewModel.countrySelect = ko.dependentObservable({
    read: viewModel.countryList,
    write: function (Country) {
        
        //Function get files from region 

        if (typeof Country === 'undefined' || !Country) {
            CountrySelected = "";
        } else {
            CountrySelected = Country;
        }
    }
});


viewModel.CloneQuaterSelected = ko.dependentObservable({
    read: viewModel.FileRegion,
    write: function (Quater) {
        CloneQuaterSelected = Quater;

        var listOfParams = {
            Region: RegionSelected,
            Quater: CloneQuaterSelected
            //Year: YearSelected
        }

        if ((typeof CloneQuaterSelected === 'undefined' || !CloneQuaterSelected) || (typeof RegionSelected === 'undefined' || !RegionSelected)) {
            viewModel.CloneviewAddAccount(false);
        } else {
            viewModel.CloneviewAddAccount(true);
            BindJsonAlias("getCF_Account", "CloneaccountList", 'MappingKeys', listOfParams, function () {
             
                viewModel.cloneCheckAll(false);
                viewModel.CloneaccountList([]);

                _GlobalVars["CloneaccountList"].forEach(function (item, index, array) {

                    var column = {
                        "ID": item.ID,
                        "CFAccount": item.CFAccount,
                        "Description": ko.observable(item.Description),
                        "Region": ko.observable(item.Region),
                        "Country": ko.observable(item.Country),
                        "Name": ko.observable(item.Name),
                        "CloneCheck": ko.observable(false)
                    }

                    viewModel.CloneaccountList.push(column);
                });

            });
        }
    },
    owner: viewModel
});


$(document).ready(function () {
    _hideMenu();

   

    BindJsonAlias("getRegion", "Region", "RegionFileAdmin", "", function () {
        viewModel.Region(_GlobalVars["Region"]);
        viewModel.viewAddAccount(false);
        ko.applyBindings(viewModel);
        renderPreValues();

        
    });

    var listOfParams = {
        IDFile: 0
    }

    BindJsonAlias("getQuater", "cbQuater", "UploadFile", listOfParams, function () {
        viewModel.cbQuater(_GlobalVars["cbQuater"]);

        renderPreValues();
    });

    BindJsonAlias("getYear", "cbYear", "UploadFile", listOfParams, function () {
        viewModel.cbYear(_GlobalVars["cbYear"]);
        renderPreValues();
    });
    
    BindJsonAlias("getCF_TypesAccount", "ListTypeAccount", "MappingKeys", listOfParams, function () {
        viewModel.ListTypeAccount(_GlobalVars["ListTypeAccount"]);
        renderPreValues();
    });
    

    XLSX.createJsXlsx({
        allColsMode: true,
        idElement: "js-xlsx-plugin",
        fileInput: {
            dropText: "Drop an Excel file here...",
            hideDrop: false,
            inputText: "... or click here to select a file"
        },
        onSuccess: function (jsonData) {

            var excelSource = jsonData;

            var pQuaterSelected = QuaterSelected;

            var listOfParams = {
                data: JSON.stringify(excelSource[Object.keys(jsonData)[0]]),
                pQuaterSelected: pQuaterSelected
            }

            _callServer({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading data...",
                url: '/MappingKeys/uploadManualUpload',
                data: {
                    'data': JSON.stringify(listOfParams)
                },
                type: "post",
                success: function (json) {
                   //data refreshed
                }
            });
        }
    });












   
});

function renderPreValues()
{
    if (document.getElementById("hfRegion").value != "") {
        if (document.getElementById("hfYear").value != "") {
            if (document.getElementById("hfQuater").value != "") {

                document.getElementById("cbRegion").value = document.getElementById("hfRegion").value;
                viewModel.regionSelect(document.getElementById("hfRegion").value);

                document.getElementById("cbQuater").value = document.getElementById("hfYear").value;
                viewModel.QuaterSelected(document.getElementById("hfYear").value);

                //document.getElementById("cbYear").value = document.getElementById("hfYear").value;
                //viewModel.YearSelected(document.getElementById("hfYear").value);
            }
        }
    }
}
function addAccount()
{

    var error = "";
    var count = 1;
    var iscountry = false;

    if (typeof viewModel.AccountName() === 'undefined' || !viewModel.AccountName())
    {
        if (count = 1)
        {
            error = error + "Please add the name of the account";
        } else {
            error = error + ", add the name of the account";
        }
        count = count + 1;
        
    }
    if (typeof viewModel.AccountDescription() === 'undefined' || !viewModel.AccountDescription()) {
        if (count = 1) {
            error = error + "Please add the description of the account";
        } else {
            error = error + ", add the description of the account";
        }
        count = count + 1;
    }
    

    if (typeof ListTypeAccount === 'undefined' || !ListTypeAccount) {
        if (count = 1) {
            error = error + "Please add the type of the account";
        } else {
            error = error + ", add the type of the account";
        }
        count = count + 1;
    }
    if (viewModel.showColumnCountry() == true) {
        iscountry = true;
        if (typeof CountrySelected === 'undefined' || !CountrySelected) {
            if (count = 1) {
                error = error + "Please select one country";
            } else {
                error = error + ", select one country";
            }
            count = count + 1;
        }


    } else { iscountry = false; }

    if (error != "") {
        _showNotification('error', error);
    } else
    {
       

        var listOfParams = {
            Region: RegionSelected,
            CFAccount: viewModel.AccountName(),
            Description: viewModel.AccountDescription(),
            Country: CountrySelected,
            showColumnCountry: iscountry,
            Quater: QuaterSelected,
           // Year: YearSelected,
            Type: ListTypeAccount
        }



        BindJsonAlias("addCF_Account", "accountList", 'MappingKeys', listOfParams, function () {
            viewModel.accountList(_GlobalVars["accountList"]);
            _showNotification('success', "Account have been added.");
            $('#AccountPopUp').modal('hide');
            viewModel.AccountName("");
            Description: viewModel.AccountDescription("");
        });
       

    }
    


}


function AddKey(parent) {

    var listOfParams = {
        IDCFAccount: parent.ID
    }
    var url = '/CashFlow/CF/AddKey/' + parent.ID;

    window.location.href = url;
    
}

function DeleteKey(parent) {

    var listOfParams = {
        AccountID: parent.ID,
        Region: RegionSelected,
        Quater:QuaterSelected,       
        Year:YearSelected

    }
   
    BindJsonAlias("deleteCF_Account", "accountList", 'MappingKeys', listOfParams, function () {
        viewModel.accountList(_GlobalVars["accountList"]);
    });


}

function DelimitBU(parent) {

    SelectedCFAccount = parent.ID;

    $('#CFAccountBU').modal('show');
    
    viewModel.CFAccountSelected(parent.CFAccount);

    var listOfParams = {
        CFAccountID: parent.ID,
        Region: RegionSelected,
        Quater: QuaterSelected
       // Year: YearSelected
    }

    BindJsonAlias("getCF_CFAccountBU", "BUListDelimit", 'MappingKeys', listOfParams, function () {
        viewModel.BUListDelimit([]);

        _GlobalVars["BUListDelimit"].forEach(function (item, index, array) {
            var column = { "BU": item.BU, "BUexist": ko.observable(item.BUexist) }
            viewModel.BUListDelimit.push(column);
        });
    });


   

}

function saveBUExist()
{
    var exist;
    var bu;

    SelectedCFAccount;

    viewModel.BUListDelimit().forEach(function (item, index, array) {

        exist = item.BUexist();
        bu = item.BU;

        var listOfParams = {
            CFAccountID: SelectedCFAccount,
            BU: bu
        }


        if (exist) {
            _callServer({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading data...",
                url: '/MappingKeys/add_CFAccountBU',
                type: "post",
                data: {
                    'data': JSON.stringify(listOfParams)
                },
                success: function (json) {
                }
            });

        } else {
            _callServer({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading data...",
                url: '/MappingKeys/delete_CFAccountBU',
                type: "post",
                data: {
                    'data': JSON.stringify(listOfParams)
                },
                success: function (json) {
                }
            });
        }


    });


    var listOfParams = {
        Region: RegionSelected,
        Quater: QuaterSelected
        //Year: YearSelected
    }

        BindJsonAlias("getCF_Account", "accountList", 'MappingKeys', listOfParams, function () {
            viewModel.accountList(_GlobalVars["accountList"]);

            $('#CFAccountBU').modal('hide');
        });

}



function fntCheckAllBU() {

    if (viewModel.checkAllBU() == true) {

        viewModel.BUListDelimit([]);

        _GlobalVars["BUListDelimit"].forEach(function (item, index, array) {
            var column = { "BU": item.BU, "BUexist": ko.observable(true) }
            viewModel.BUListDelimit.push(column);
        });


    } else {
        viewModel.BUListDelimit([]);

        _GlobalVars["BUListDelimit"].forEach(function (item, index, array) {
            var column = { "BU": item.BU, "BUexist": ko.observable(false) }
            viewModel.BUListDelimit.push(column);
        });

    }


}



function fntCheckAllCopyMapping()
{

    if (viewModel.cloneCheckAll() == true) {

        viewModel.CloneaccountList([]);

        _GlobalVars["CloneaccountList"].forEach(function (item, index, array) {

            var column = {
                "ID": item.ID,
                "CFAccount": item.CFAccount,
                "Description": ko.observable(item.Description),
                "Region": ko.observable(item.Region),
                "Country": ko.observable(item.Country),
                "Name": ko.observable(item.Name),
                "CloneCheck": ko.observable(true)
            }

            viewModel.CloneaccountList.push(column);
        });
    } else
    {
        viewModel.CloneaccountList([]);
        _GlobalVars["CloneaccountList"].forEach(function (item, index, array) {

            var column = {
                "ID": item.ID,
                "CFAccount": item.CFAccount,
                "Description": ko.observable(item.Description),
                "Region": ko.observable(item.Region),
                "Country": ko.observable(item.Country),
                "Name": ko.observable(item.Name),
                "CloneCheck": ko.observable(false)
            }

            viewModel.CloneaccountList.push(column);
        });



    }


}

function CloneSelection()
{
    var listOfParams = {
        Region: RegionSelected,
        Quater: QuaterSelected,
        CloneRegion: RegionSelected,
        CloneQuater: CloneQuaterSelected,
        CloneaccountList: JSON.stringify(viewModel.CloneaccountList().filter(qmData => { if (qmData.CloneCheck() == true) return qmData }))
    }

    BindJsonAlias("CloneSelection", "accountList", 'MappingKeys', listOfParams, function () {
        viewModel.accountList(_GlobalVars["accountList"]);

        $('#CloneMapping').modal('hide');
    });




}

function tourguide()
{
    var enjoyhint_instance = new EnjoyHint({});

    var enjoyhint_script_steps = [
        {
            "next #menu-permit-name": 'Hello, I\'d like to tell you about how to add keys.<br> Click "Next" to proceed.'
        },
        {
            "next #cbRegion": 'Select the region for which you would like to add a key.<br> Click "Next" to proceed.'
        },
        {
            "next #cbQuater": 'Select the quarter for which you would like to add a key.<br> Click "Next" to proceed.'
        },
        {
            "next #btnAddAccount": 'Click to add a new account.<br> Click "Next" to proceed.'
        }
        ,
        {
            "next #txtName": 'Add the name of the Account.<br> Click "Next" to proceed.'
        }
        ,
        {
            "next #txtDescription": 'Add the description of the account.<br> Click "Next" to proceed.'
        },
        {
            "next #cbColumnTpye": 'Select the type of account.<br> Click "Next" to proceed.'
        },
        {
            "click #btnAddAccountPopUp": 'Click the button to add the account.'
        },
        {
            "next #tblMappedKeys": 'View your new account.<br> Click "Next" to proceed.'
        },
        {
            "next #btnDelimit": 'Delimit the BU that map this account.<br> Click "Next" to proceed.'
        },
        {
            "next #cbCheckAllBU": 'Click this checkbox if the account apply for all BUs, if you dont check any BU it is also going to apply for all the BUs.<br> Click "Next" to proceed.'
        },
        {
            "next #tblBUaLL": 'Find and Check the BUs that apply for this account.<br> Click "Next" to proceed.'
        },
        {
            "click #saveBUchanges": 'Save your changes.'
        },
        {
            "next #btnCloneMapping": 'Now click this button to clone keys.<br> Click "Next" to proceed.'
        },
        {
            "next #cbQuaterClone": 'Select the quarter to find the key you want to clone.<br> Click "Next" to proceed.'
        },
        {
            "next #tblCloneMapping": 'Check the keys you want to clone.<br> Click "Next" to proceed.'
        },
        {
            "click #btnCloneSelection": 'Click to clone the selected keys.'
        },
        {
            "next #bntDelete": 'Click this button if you need to delete the key.<br> Click "Next" to proceed.'
        },
        {
            "next #btnAddKey": 'Click this button to add the conditions of this key.'
        }

        
    ];

    enjoyhint_instance.set(enjoyhint_script_steps);

    enjoyhint_instance.run();


}

function downloadMappingData()
{

    var listOfParams = {
        REGION: RegionSelected,
        DATE: QuaterSelected
    }

    BindJsonAlias("get_mappedData", "mappedData", 'MappingKeys', listOfParams, function () {
       
        _downloadExcelJson({
            jsonData: JSON.stringify(_GlobalVars["mappedData"]),
            filename: "Mapped Data",
            success: {
                msg: "Please wait, generating report..."
            }
        });

    });

    

    

}


function downloadKeysData() {

    var listOfParams = {
        REGION: RegionSelected,
        DATE: QuaterSelected
    }

    BindJsonAlias("get_keysvalues", "keysData", 'MappingKeys', listOfParams, function () {

        _downloadExcelJson({
            jsonData: JSON.stringify(_GlobalVars["keysData"]),
            filename: "Mapped Data",
            success: {
                msg: "Please wait, generating report..."
            }
        });

    });





}