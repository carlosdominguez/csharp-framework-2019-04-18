﻿var _GlobalVars = {
    Region: [],
    Country: [],
    CountryBU: [],
    FileRegion: [],
    TypeColumn: [],
    ColumnOrder: [],
    FileColumn: [],
    AllColumnOrder: [],
    ColumnsEC: [],
    SelectedEC: []
};

var RegionSelected;
var CountrySelected;
var SelectedFile;
var ColumnTypeSelect;
var ColumnOrderSelect;
var FileSelected;

var FileSelectedViewCreate;
var FileSelectedViewModify;

var FileECSelect;
var FDLSelected;
var IceCodeSelected;
var MonthSelected;
var DescriptionSelected;
var colValue;


var viewModel = {
    Region: ko.observable(),
    Country: ko.observable(),
    CountryBU: ko.observable(),
    FileRegion: ko.observable(),
    TypeColumn: ko.observable(),
    ColumnOrder: ko.observable(),
    FileColumn: ko.observableArray(),
    ShowCreate: ko.observable(),
    ShowUpdate: ko.observable(),
    showOrderEdit: ko.observable(false),
    AllColumnOrder: ko.observable(),
    ColumnsEC: ko.observable()
};

viewModel.gridViewBU = new KOGridModel({
    data: viewModel.CountryBU,
    columns: [
        { header: "BU", data: "BU" },
        { header: "Actions", template: 'actionBUS' },
    ],
    pageSize: 10
});


viewModel.gridViewModel = new KOGridModel({
    data: viewModel.FileColumn,
    columns: [
        { header: "Order", data:"Order" },
        { headerTemplate: "headerOrder", template: "tempColumnOrder", visible: viewModel.showOrderEdit },
        { header: "Column Name", data: "ColumnName" },
        { headerTemplate: "headerOrder", template: "tempColumnName", visible: viewModel.showOrderEdit },
        { header: "Type", data: "ColumnType" },
        { header: "Is Table", data: 'IsTable', dataTemplate: 'tplCb' },
        { header: "Table Name", data: "IsTableName" },
        { header: "Actions", data:"ID", dataTemplate: 'divActions' },
    ],
    pageSize: 4
});

viewModel.CountryBUSelectedDelete = ko.dependentObservable({
    read: viewModel.CountryBU,
    write: function (CountryBU) {
        var listOfParams = {
            Country: CountrySelected,
            BU: CountryBU.BU
        }

        BindJsonAlias("DeleteCountryBU", "CountryBU", 'RegionFileAdmin', listOfParams, function () {
            viewModel.CountryBU(_GlobalVars["CountryBU"]);
        });

    },
    owner: viewModel
});

function CountryBUSelectedDelete(item)
{
    var listOfParams = {
        Country: CountrySelected,
        BU: item.BU
    }

    BindJsonAlias("DeleteCountryBU", "CountryBU", 'RegionFileAdmin', listOfParams, function () {
        viewModel.CountryBU(_GlobalVars["CountryBU"]);
    });
}

viewModel.countrySelect = ko.dependentObservable({
    read: viewModel.Country,
    write: function (Country) {
        CountrySelected = Country;

       
        //Function get files from region 

        if (typeof Country === 'undefined' || !Country) {
            $('#deleteCountryDiv').hide();
            $('#addBUDiv').hide();
            $('#BUListDiv').hide();
        } else {
            $('#deleteCountryDiv').show();
            $('#addBUDiv').show();
            $('#BUListDiv').show();

            var listOfParams = {
                Country: Country
            }

            BindJsonAlias("getCountryBU", "CountryBU", 'RegionFileAdmin', listOfParams, function () {
                viewModel.CountryBU(_GlobalVars["CountryBU"]);
            });
        }
    }
});

viewModel.regionSelect = ko.dependentObservable({
    read: viewModel.Region,
    write: function (Region) {
        RegionSelected = Region;

        var listOfParams = {
            pRegion: Region,
            pSOEID: _getSOEID()
        }
        //Function get files from region 
       

        if (typeof Region === 'undefined' || !Region) {
            $('#FileDiv').hide();
            $('#CountryDiv').hide();
            $('#BUListDiv').hide();
            $('#divRegion').hide();
            $('#EditCheckDiv').hide();
        } else
        {
            $('#FileDiv').show();
            $('#CountryDiv').show();
            $('#divRegion').show();
            $('#EditCheckDiv').show();
            var listOfParams = {
                Region: Region
            }

            getRegionFile();

            BindJsonAlias("getCountry", "Country", 'RegionFileAdmin', listOfParams, function () {
                viewModel.Country(_GlobalVars["Country"]);
            });

            getEditCheckFile();
        }
    },
    owner: viewModel
});

viewModel.FileDelete = ko.dependentObservable({
    read: viewModel.FileRegion,
    write: function (FileRegion) {
        var listOfParams = {
            ID: FileRegion.ID
        }

        BindJsonAlias("DeleteFile", "FileRegion", 'RegionFileAdmin', listOfParams, function () {
            viewModel.FileRegion(_GlobalVars["FileRegion"]);
        });

    },
    owner: viewModel
});

viewModel.FileSaveDefaultColumns = ko.dependentObservable({
    read: viewModel.FileRegion,
    write: function (FileRegion) {
        var listOfParams = {
            ID: FileRegion.ID,
            BU : FileRegion.AskBU,
            Country : FileRegion.AskCountry,
            MonthYear : FileRegion.AskMonthYear,
            Quater : FileRegion.AskQuater
        }

        BindJsonAlias("updateFile", "FileRegion", 'RegionFileAdmin', listOfParams, function () {
            viewModel.FileRegion(_GlobalVars["FileRegion"]);
        });

    },
    owner: viewModel
});

viewModel.FileAddColumns = ko.dependentObservable({
    read: viewModel.FileRegion,
    write: function (FileRegion) {

        FileSelected = FileRegion.ID;

        var listOfParams = {
            ID: FileRegion.ID
        }

        viewModel.ShowCreate(false);
        viewModel.ShowUpdate(false);

        FileSelectedViewCreate = FileRegion.IsCreated;
        FileSelectedViewModify = FileRegion.NeedUpdate;

        if(!FileRegion.IsCreated)
        {
            viewModel.ShowCreate(true);
        }else
        {
            if (FileRegion.NeedUpdate)
            {
                viewModel.ShowUpdate(true);
            }
        }

        BindJsonAlias("getColumnOrder", "ColumnOrder", 'RegionFileAdmin', listOfParams, function () {
            viewModel.ColumnOrder(_GlobalVars["ColumnOrder"]);
        });

        var listOfParams = {
            IDFile: FileRegion.ID
        }

        BindJsonAlias("getColumnFile", "FileColumn", 'RegionFileAdmin', listOfParams, function () {
            viewModel.FileColumn([])

            _GlobalVars["FileColumn"].forEach(function (item, index, array) {
                var column = { "ID":  item.ID , "ColumnName": ko.observable( item.ColumnName ), "IsTable": item.IsTable, "IsTableName": ko.observable( item.IsTableName ), "Order": ko.observable( item.Order ), "ColumnType": ko.observable( item.ColumnType) }
                viewModel.FileColumn.push(column);
            });
        });

    },
    owner: viewModel
});

viewModel.columnTypeSelect = ko.dependentObservable({
    read: viewModel.TypeColumn,
    write: function (TypeColumn) {
        ColumnTypeSelect = TypeColumn;
    },
    owner: viewModel
});

viewModel.colOrderSelect = ko.dependentObservable({
    read: viewModel.ColumnOrder,
    write: function (ColumnOrder) {
        ColumnOrderSelect = ColumnOrder;
    },
    owner: viewModel
});

viewModel.FileECSelect = ko.dependentObservable({
    read: viewModel.FileRegion,
    write: function (File) {
        FileECSelect = File;

        if (typeof FileECSelect === 'undefined' || !FileECSelect) {

            viewModel.ColumnsEC([]);

        } else {
            var listOfParams = {
                FileID: FileECSelect
            }

            BindJsonAlias("getCF_AllFileColumn", "ColumnsEC", 'RegionFileAdmin', listOfParams, function () {
                viewModel.ColumnsEC(_GlobalVars["ColumnsEC"])
            });
        }
    }
});

viewModel.SelectedFDL = ko.dependentObservable({
    read: viewModel.ColumnsEC,
    write: function (ID) {
        FDLSelected = ID;
    }
});

viewModel.SelectedFDLDesc = ko.dependentObservable({
    read: viewModel.ColumnsEC,
    write: function (ID) {
        DescriptionSelected = ID;
    }
});

viewModel.SelectedIceCod = ko.dependentObservable({
    read: viewModel.ColumnsEC,
    write: function (ID) {
        IceCodeSelected = ID;
    }
});
viewModel.SelectedMonth = ko.dependentObservable({
    read: viewModel.ColumnsEC,
    write: function (ID) {
        MonthSelected = ID;
    }
});

viewModel.SelectedValue = ko.dependentObservable({
    read: viewModel.ColumnsEC,
    write: function (ID) {
        colValue = ID;
    }
});


$(document).ready(function () {

    $('#FileDiv').hide();
    $('#CountryDiv').hide();
    $('#addBUDiv').hide();
    $('#BUListDiv').hide();



    BindJsonAlias("getRegion", "Region","RegionFileAdmin", "", function () {
        viewModel.Region(_GlobalVars["Region"]);
        ko.applyBindings(viewModel);

        BindJsonAlias("getColumnType", "TypeColumn", "RegionFileAdmin", "", function () {
            viewModel.TypeColumn(_GlobalVars["TypeColumn"]);
        });


        BindJsonAlias("getAllColumnsOrder", "AllColumnOrder", 'RegionFileAdmin', "", function () {
            viewModel.AllColumnOrder(_GlobalVars["AllColumnOrder"]);
        });

    });

   

    _hideMenu();
});


function addRegion()
{
    var listOfParams = {
        Region: $('#txtRegion').val()
    }

    BindJsonAlias("addRegion", "Region", 'RegionFileAdmin', listOfParams, function () {
        viewModel.Region(_GlobalVars["Region"]);

        $('#txtRegion').val("");
    });
}

function addCountry()
{
    var listOfParams = {
        Country: $('#txtCountry').val(),
        Region: RegionSelected
    }

    BindJsonAlias("addCountry", "Country", 'RegionFileAdmin', listOfParams, function () {
        viewModel.Country(_GlobalVars["Country"]);

        $('#txtCountry').val("");
    });
}

function DeleteCountry()
{
    var listOfParams = {
        Country: CountrySelected,
        Region: RegionSelected
    }

    BindJsonAlias("deleteCountry", "Country", 'RegionFileAdmin', listOfParams, function () {
        viewModel.Country(_GlobalVars["Country"]);
    });
}

function addCountryBU() {
    var listOfParams = {
        Country: CountrySelected,
        BU: $('#txtCountrybu').val(),
        Region: RegionSelected
    }

    BindJsonAlias("addCountryBU", "CountryBU", 'RegionFileAdmin', listOfParams, function () {
        viewModel.CountryBU(_GlobalVars["CountryBU"]);
        $('#txtCountrybu').val("");
    });
}

function getCountryBU() {
    var listOfParams = {
        Country: CountrySelected
    }

    BindJsonAlias("getCountryBU", "CountryBU", 'RegionFileAdmin', listOfParams, function () {
        viewModel.CountryBU(_GlobalVars["CountryBU"]);
    });
}

function addRegionFile() {
    var listOfParams = {
        Region: RegionSelected,
        Name: $('#txtNameFile').val(),
        UseCountry: document.getElementById("cbUseCountry").checked,
        UseBU: document.getElementById("cbUseBU").checked,
        UseMonthYear: document.getElementById("cbUseMonthYear").checked,
        UseQuater: document.getElementById("cbUseQuater").checked
    }

    BindJsonAlias("addFile", "FileRegion", 'RegionFileAdmin', listOfParams, function () {
        viewModel.FileRegion(_GlobalVars["FileRegion"]);
        //$('#txtCountrybu').val("");
    });
}

function getRegionFile() {
    var listOfParams = {
        Region: RegionSelected
    }

    BindJsonAlias("getFile", "FileRegion", 'RegionFileAdmin', listOfParams, function () {
        viewModel.FileRegion(_GlobalVars["FileRegion"]);
    });
}

function addColumnToFile()
{
    //find if exist
   // $('#txtNameColumn').val()


    var listOfParams = {
        IDFile: FileSelected,
        ColumnName: $('#txtNameColumn').val(),
        Type: $('#cbColumnTpye').val(),
        Order: $('#colOrderSelect').val(),
        IsTable: document.getElementById("cbUseOtherTable").checked,
        IsTableName: $('#txtIsTableName').val()
    }

    var listOfParams2 = {
        ID: FileSelected
    }

    BindJsonAlias("addColumnFile", "FileColumn", 'RegionFileAdmin', listOfParams, function () {

        viewModel.FileColumn([]);

        _GlobalVars["FileColumn"].forEach(function (item, index, array) {
            var column = { "ID": item.ID, "ColumnName": ko.observable(item.ColumnName), "IsTable": item.IsTable, "IsTableName": ko.observable(item.IsTableName), "Order": ko.observable(item.Order), "ColumnType": ko.observable(item.ColumnType) }
            viewModel.FileColumn.push(column);
        });

        BindJsonAlias("getColumnOrder", "ColumnOrder", 'RegionFileAdmin', listOfParams2, function () {
            viewModel.ColumnOrder(_GlobalVars["ColumnOrder"]);
        });

        $('#txtNameColumn').val("");
        $('#cbColumnTpye').val("");
        $('#colOrderSelect').val(0),
        document.getElementById("cbUseOtherTable").checked = false;
        $('#txtIsTableName').val("");

    });

    FileSelectedViewModify = true;

    if (!FileSelectedViewCreate) {
        viewModel.ShowCreate(true);
    } else {
        if (FileSelectedViewModify) {
            viewModel.ShowUpdate(true);
        }
    }



    getRegionFile();
}

function deleteColumnInFile(parent)
{

    var listOfParams = {
        IDColumn: parent.value,
        IDFile: FileSelected
    }

    BindJsonAlias("deleteColumnFile", "FileColumn", 'RegionFileAdmin', listOfParams, function () {
        viewModel.FileColumn([])

        _GlobalVars["FileColumn"].forEach(function (item, index, array) {
            var column = { "ID":  item.ID , "ColumnName": ko.observable( item.ColumnName ), "IsTable": item.IsTable, "IsTableName": ko.observable( item.IsTableName ), "Order": ko.observable( item.Order ), "ColumnType": ko.observable( item.ColumnType) }
            viewModel.FileColumn.push(column);
        });
    });

    getRegionFile();

    FileSelectedViewModify = true;

    if (!FileSelectedViewCreate) {
        viewModel.ShowCreate(true);
    } else {
        if (FileSelectedViewModify) {
            viewModel.ShowUpdate(true);
        }
    }
}

function AllColumnsToShow()
{

    var List = _GlobalVars["AllColumnOrder"];

    viewModel.FileColumn().forEach(function (item, index, array) {

        List = List.filter(p => { if (p.Order != item.Order()) return p });
    });

  
    viewModel.AllColumnOrder(List);
}

function createTable()
{
    var listOfParams = {
        Region: RegionSelected,
        FileID: FileSelected,
        SOEID: _getSOEID()
    }

    BindJsonAlias("createTableDynamic", "FileRegion", 'RegionFileAdmin', listOfParams, function () {
        viewModel.FileRegion(_GlobalVars["FileRegion"]);


        viewModel.ShowCreate(false);
        viewModel.ShowUpdate(false);
    });

}

function updateTable() {
    var listOfParams = {
        Region: RegionSelected,
        FileID: FileSelected,
        SOEID: _getSOEID()
    }

    BindJsonAlias("updateTableDynamic", "FileRegion", 'RegionFileAdmin', listOfParams, function () {
        viewModel.FileRegion(_GlobalVars["FileRegion"]);


        viewModel.ShowCreate(false);
        viewModel.ShowUpdate(false);
    });





}
function deleteRegion()
{
    
    var listOfParams = {
        Region: RegionSelected
    }

    BindJsonAlias("deleteRegions", "Region", 'RegionFileAdmin', listOfParams, function () {
        viewModel.Region(_GlobalVars["Region"]);
    });
}


function getEditCheckFile()
{
    var listOfParams = {
        RegionID: RegionSelected
    }

    BindJsonAlias("getCF_EditCheckFileSelected", "SelectedEC", 'RegionFileAdmin', listOfParams, function () {
        document.getElementById("cbECFile").value = _GlobalVars["SelectedEC"][0].FileID;
        viewModel.FileECSelect(_GlobalVars["SelectedEC"][0].FileID);

        document.getElementById("cbECFDL").value = _GlobalVars["SelectedEC"][0].colFDL;
        viewModel.SelectedFDL(_GlobalVars["SelectedEC"][0].colFDL);

        document.getElementById("cbECDesc").value = _GlobalVars["SelectedEC"][0].colDescription;
        viewModel.SelectedFDLDesc(_GlobalVars["SelectedEC"][0].colDescription);

        document.getElementById("cbECIceCode").value = _GlobalVars["SelectedEC"][0].colIceCode;
        viewModel.SelectedIceCod(_GlobalVars["SelectedEC"][0].colIceCode);

        document.getElementById("cbECMonth").value = _GlobalVars["SelectedEC"][0].colMonth;
        viewModel.SelectedValue(_GlobalVars["SelectedEC"][0].colMonth);
    });

    BindJsonAlias("getCF_EditCheckFileSelected", "SelectedEC", 'RegionFileAdmin', listOfParams, function () {
        

        document.getElementById("cbECFDL").value = _GlobalVars["SelectedEC"][0].colFDL;
        viewModel.SelectedFDL(_GlobalVars["SelectedEC"][0].colFDL);

        document.getElementById("cbECDesc").value = _GlobalVars["SelectedEC"][0].colDescription;
        viewModel.SelectedFDLDesc(_GlobalVars["SelectedEC"][0].colDescription);

        document.getElementById("cbECIceCode").value = _GlobalVars["SelectedEC"][0].colIceCode;
        viewModel.SelectedIceCod(_GlobalVars["SelectedEC"][0].colIceCode);

        document.getElementById("cbECMonth").value = _GlobalVars["SelectedEC"][0].colMonth;
        viewModel.SelectedMonth(_GlobalVars["SelectedEC"][0].colMonth);

        document.getElementById("cbECValue").value = _GlobalVars["SelectedEC"][0].colValue;
        viewModel.SelectedMonth(_GlobalVars["SelectedEC"][0].colValue);
    });
   
}

function insertEditCheckFile()
{
    var error = "";
    var count = 1;
    var iscountry = false;

    if (typeof FileECSelect === 'undefined' || !FileECSelect) {
        if (count = 1) {
            error = error + "Please select the file";
        } else {
            error = error + ", select the file";
        }
        count = count + 1;

    }

    if (typeof FDLSelected === 'undefined' || !FDLSelected) {
        if (count = 1) {
            error = error + "Please select the column of FDL";
        } else {
            error = error + ", select the column of FDL";
        }
        count = count + 1;

    }

    if (typeof IceCodeSelected === 'undefined' || !IceCodeSelected) {
        if (count = 1) {
            error = error + "Please select the column of Ice Code";
        } else {
            error = error + ", select the column of Ice Code";
        }
        count = count + 1;

    }

    if (typeof MonthSelected === 'undefined' || !MonthSelected) {
        if (count = 1) {
            error = error + "Please select the column of Month";
        } else {
            error = error + ", select the column of Month";
        }
        count = count + 1;

    }

    if (typeof DescriptionSelected === 'undefined' || !DescriptionSelected) {
        if (count = 1) {
            error = error + "Please select the column of Description";
        } else {
            error = error + ", select the column of Description";
        }
        count = count + 1;

    }
    if (typeof colValue === 'undefined' || !colValue) {
        if (count = 1) {
            error = error + "Please select the column of Value";
        } else {
            error = error + ", select the column of Value";
        }
        count = count + 1;

    }
    


    if (error != "") {
        _showNotification('error', error);
    } else {

        var listOfParams = {
            RegionID: RegionSelected,
            FileID: FileECSelect,
            colFDL: FDLSelected,
            colIceCode: IceCodeSelected,
            colMonth: MonthSelected,
            colDescription: DescriptionSelected,
            colValue: colValue
        }

        BindJsonAlias("saveCF_EditCheckFileSelected", "SelectedEC", 'RegionFileAdmin', listOfParams, function () {
            _showNotification('success', "Mapping edit check is saved.");
        });
    }
}