﻿var _GlobalVars = {
    Region: [],
    FileRegion: [],
    countryList: [],
    accountList: [],
    masterTable: [],
    comboboxBU: [],
    comboboxCountry: [],
    comboboxType: [],
    comboboxCFAccount: [],
    comboboxValueSelected: [],
    buFilter: [],
    accountFilter: [],
    accontTypeFilter: [],
    countryFilter: []
};
var MasterTableFilter;

var RegionSelected;
var CountrySelected;
var QuaterSelected;
var YearSelected;
var ListTypeAccount;

var comboboxBUSelected;
var comboboxCountrySelected;
var comboboxTypeSelected;
var comboboxCFAccountSelected;
var comboboxValueSelected;

var viewModel = {
    Region: ko.observable(),
    FileRegion: ko.observable(),
    viewAddAccount: ko.observable(),
    cbQuater: ko.observable(),
    cbYear: ko.observable(),
    masterTable: ko.observableArray(),
    comboboxBU: ko.observableArray(),
    comboboxCountry: ko.observableArray(),
    comboboxType: ko.observableArray(),
    comboboxCFAccount: ko.observableArray(),
    showMonthDivision: ko.observable(false),
    comboboxValueSel: ko.observableArray()
};

viewModel.comboboxBUSelected = ko.dependentObservable({
    read: viewModel.comboboxBU,
    write: function (item) {
        comboboxBUSelected = item;
        if (typeof comboboxBUSelected === 'undefined') {
            comboboxBUSelected = "0";
        }
    },
    owner: viewModel
});

viewModel.comboboxValueSelected = ko.dependentObservable({
    read: viewModel.comboboxValueSel,
    write: function (item) {
        comboboxValueSelected = item;
        if (typeof comboboxValueSelected === 'undefined') {
            comboboxValueSelected = "0";
        }
    },
    owner: viewModel
});

viewModel.comboboxCountrySelected = ko.dependentObservable({
    read: viewModel.comboboxCountry,
    write: function (item) {
        comboboxCountrySelected = item;
        if (typeof comboboxCountrySelected === 'undefined' ) {
            comboboxCountrySelected = "0";
        }
    },
    owner: viewModel
});

viewModel.comboboxTypeSelected = ko.dependentObservable({
    read: viewModel.comboboxType,
    write: function (item) {
        comboboxTypeSelected = item;
        if (typeof comboboxTypeSelected === 'undefined') {
            comboboxTypeSelected = "0";
        }
    },
    owner: viewModel
});

viewModel.comboboxCFAccountSelected = ko.dependentObservable({
    read: viewModel.comboboxCFAccount,
    write: function (item) {
        comboboxCFAccountSelected = item;
        if (typeof comboboxCFAccountSelected === 'undefined' ) {
            comboboxCFAccountSelected = "0";
        }
    },
    owner: viewModel
});



viewModel.QuaterSelected = ko.dependentObservable({
    read: viewModel.FileRegion,
    write: function (Quater) {
        QuaterSelected = Quater;

        var listOfParams = {
            Region: RegionSelected,
            Quater: QuaterSelected
        }

        if ((typeof QuaterSelected === 'undefined' || !QuaterSelected) || (typeof RegionSelected === 'undefined' || !RegionSelected) ) {
            viewModel.viewAddAccount(false);
        } else {
            viewModel.viewAddAccount(true);
            BindJsonAlias("getCF_MasterTableDetail", "masterTable", 'MasterTableCF', listOfParams, function () {
                viewModel.masterTable(_GlobalVars["masterTable"]);
                MasterTableFilter = _GlobalVars["masterTable"];
            });
            renderComboBox(listOfParams);

            
            _GlobalVars["comboboxValueSelected"] = [];

            _GlobalVars["comboboxValueSelected"][0] = { id: 1, value: 'Different than 0' };
            _GlobalVars["comboboxValueSelected"][1] = { id: 2, value: 'Greater than 0' };
            _GlobalVars["comboboxValueSelected"][2] = { id: 3, value: 'Less than 0' };

            viewModel.comboboxValueSel(_GlobalVars["comboboxValueSelected"]);
        }
    },
    owner: viewModel
});

viewModel.regionSelect = ko.dependentObservable({
    read: viewModel.Region,
    write: function (Region) {
        RegionSelected = Region;

        var listOfParams = {
            Region: RegionSelected,
            Quater: QuaterSelected
        }

        if ((typeof QuaterSelected === 'undefined' || !QuaterSelected) || (typeof RegionSelected === 'undefined' || !RegionSelected) ) {
            viewModel.viewAddAccount(false);
        } else {
            viewModel.viewAddAccount(true);
            BindJsonAlias("getCF_MasterTableDetail", "masterTable", 'MasterTableCF', listOfParams, function () {
                viewModel.masterTable(_GlobalVars["masterTable"]);
                MasterTableFilter = _GlobalVars["masterTable"];
            });
            renderComboBox(listOfParams);

            _GlobalVars["comboboxValueSelected"] = [];

            _GlobalVars["comboboxValueSelected"][0] = { id: 1, value: 'Different than 0' };
            _GlobalVars["comboboxValueSelected"][1] = { id: 2, value: 'Greater than 0' };
            _GlobalVars["comboboxValueSelected"][2] = { id: 3, value: 'Less than 0' };

            viewModel.comboboxValueSel(_GlobalVars["comboboxValueSelected"]);
        }
    },
    owner: viewModel
});

viewModel.gridMasterTable = new KOGridModel({
    data: viewModel.masterTable,
    columns: [
        //{ header: "Actions", data: "ID", dataTemplate: 'divActions' }
         { header: "Region", data: "Region" },
         { header: "BU", data: "BU" },
         { header: "Country", data: "Country" },
         { header: "CF Account", data: "CFAccount" },
         { header: "Description", data: "Description" },
         { header: "Type Account", data: "Name" },
         { header: "Quater", data: "Quater" },
         { header: "Year", data: "Year" },
         { header: "Past Quater", template: 'divLAST' },
         { header: "M1", template: 'divM1', visible: viewModel.showMonthDivision },
         { header: "M2", template: 'divM2', visible: viewModel.showMonthDivision },
         { header: "M3", template: 'divM3', visible: viewModel.showMonthDivision },
         { header: "Current Quater", template: 'divCurrent' },
         { header: "Variance", template: 'divVariance' }
    ],
    pageSize: 50
});

$(document).ready(function () {
    //$('.usd_input').mask('00000.00');

    BindJsonAlias("getRegion", "Region", "RegionFileAdmin", "", function () {
        viewModel.Region(_GlobalVars["Region"]);
        viewModel.viewAddAccount(false);
        ko.applyBindings(viewModel);

      
    });

    var listOfParams = {
        IDFile: 0
    }

    BindJsonAlias("getQuater", "cbQuater", "UploadFile", listOfParams, function () {
        viewModel.cbQuater(_GlobalVars["cbQuater"]);
    });

    //BindJsonAlias("getYear", "cbYear", "UploadFile", listOfParams, function () {
    //    viewModel.cbYear(_GlobalVars["cbYear"]);
    //});

   



    _hideMenu();
});

function setTwoNumberDecimal(event) {
    this.value = parseFloat(this.value).toFixed(2);
}

function runMapping()
{
    document.getElementById('divProgress').style.display = "block";
    document.getElementById('progressbar').style["width"] = "0%";

    if (_GlobalVars["accontTypeFilter"].length != 0)
    {
        var start = 0;
        var end = 0;

        var totalPB = _GlobalVars["accontTypeFilter"].length;

       

        if (_GlobalVars["buFilter"].length != 0)
        {
            var totalPB = totalPB * _GlobalVars["buFilter"].length;
        }
        else
        {
            var totalPB = totalPB * _GlobalVars["comboboxBU"].length;
            
        }

        var sumPD = 100 / totalPB;
        var ongoingPB = 0;

      

        goThruAccountType(0);

        function goThruAccountType(index)
        {
            var AccountType = _GlobalVars["accontTypeFilter"][index]

            if (index <= _GlobalVars["accontTypeFilter"].length - 1) {


                    if (_GlobalVars["buFilter"].length != 0) {

                        gothruBUFilter(0);

                        function gothruBUFilter(indexBU)
                        {
                            if (indexBU <= _GlobalVars["buFilter"].length - 1) {


                                var listOfParams = {
                                    Region: RegionSelected,
                                    Quater: QuaterSelected,
                                    //Year: YearSelected,
                                    User: _getSOEID(),
                                    BU: _GlobalVars["buFilter"][indexBU],
                                    Country: 0,
                                    Type: AccountType,
                                    CFAccount: 0
                                }

                                BindJsonAlias("getCF_MasterTableRunMapping", "masterTable", 'MasterTableCF', listOfParams, function () {
                                    viewModel.masterTable(_GlobalVars["masterTable"]);
                                    filterMasterTable();

                                    ongoingPB = ongoingPB + sumPD;
                                    document.getElementById('progressbar').style["width"] = ongoingPB + "%";

                                    gothruBUFilter(indexBU + 1);
                                });


                            } else {
                                document.getElementById('divProgress').style.display = "none";
                            }
                        }

                    }
                    else {
                     

                        gothruBUComboBox(0);

                        function gothruBUComboBox(indexBU) {

                            if (indexBU <= _GlobalVars["comboboxBU"].length - 1) {


                                var listOfParams = {
                                    Region: RegionSelected,
                                    Quater: QuaterSelected,
                                    //Year: YearSelected,
                                    User: _getSOEID(),
                                    BU: _GlobalVars["comboboxBU"][indexBU].BU,
                                    Country: 0,
                                    Type: AccountType,
                                    CFAccount: 0
                                }

                                BindJsonAlias("getCF_MasterTableRunMapping", "masterTable", 'MasterTableCF', listOfParams, function () {
                                    viewModel.masterTable(_GlobalVars["masterTable"]);
                                    filterMasterTable();

                                    ongoingPB = ongoingPB + sumPD;
                                    document.getElementById('progressbar').style["width"] = ongoingPB + "%";

                                    gothruBUComboBox(indexBU + 1);
                                });

                            } else {
                                document.getElementById('divProgress').style.display = "none";
                            }

                        }

                    }
                



                goThruAccountType(index + 1);
                ongoingPB = ongoingPB + sumPD;
                document.getElementById('progressbar').style["width"] = ongoingPB + "%";

            } else
            {
             //  
            }

        }



    }
    else {

    if (_GlobalVars["buFilter"].length != 0) {

        var start = 0;
        var end = 0


        var totalPB = _GlobalVars["buFilter"].length;
        var sumPD = 100/ totalPB  ;
        var ongoingPB = 0;

        document.getElementById('divProgress').style.display = "block";
        document.getElementById('progressbar').style["width"] = "0%";

        insertData(0);

        function insertData(index) {


            if (index <= totalPB - 1) {
              

                var listOfParams = {
                    Region: RegionSelected,
                    Quater: QuaterSelected,
                    //Year: YearSelected,
                    User: _getSOEID(),
                    BU: _GlobalVars["buFilter"][index],
                    Country: 0,
                    Type: 0,
                    CFAccount: 0
                }

                BindJsonAlias("getCF_MasterTableRunMapping", "masterTable", 'MasterTableCF', listOfParams, function () {
                    viewModel.masterTable(_GlobalVars["masterTable"]);
                    filterMasterTable();

                    ongoingPB = ongoingPB + sumPD;
                    document.getElementById('progressbar').style["width"] = ongoingPB + "%";

                    insertData(index + 1);
                });
                        
  
            } else {
                document.getElementById('divProgress').style.display = "none";
            }
        }



      //  _GlobalVars["buFilter"].forEach(function (element) {
           // MasterTableFilter = MasterTableFilter.filter(function (el) { return el.BU == element; })
        
            
     //   });
    }
    }


    //document.getElementById('divProgress').style.display = "none";
    //document.getElementById('progressbar').style["width"] = "0%";
}

function renderComboBox(listOfParams)
{
    

    BindJsonAlias("getCF_comboBoxBU", "comboboxBU", 'MasterTableCF', listOfParams, function () {
        //viewModel.comboboxBU(_GlobalVars["comboboxBU"]);
        renderComboBoxOnlyElementSendValueName('#cbBU', 'Choose..', _GlobalVars["comboboxBU"], 1, "buFilter","BU","ID");
    });

    BindJsonAlias("getCF_comboBoxCountry", "comboboxCountry", 'MasterTableCF', listOfParams, function () {
       // viewModel.comboboxCountry(_GlobalVars["comboboxCountry"]);
        renderComboBoxOnlyElementSendValueName('#cbCountry', 'Choose..', _GlobalVars["comboboxCountry"], 1, "countryFilter", "Country", "ID");
    });

    BindJsonAlias("getCF_comboBoxType", "comboboxType", 'MasterTableCF', listOfParams, function () {
        //viewModel.comboboxType(_GlobalVars["comboboxType"]);
        renderComboBoxOnlyElementSendValueName('#cbAccountType', 'Choose..', _GlobalVars["comboboxType"], 1, "accontTypeFilter", "Name", "ID");
    });

    BindJsonAlias("getCF_comboBoxCFAccount", "comboboxCFAccount", 'MasterTableCF', listOfParams, function () {
        //viewModel.comboboxCFAccount(_GlobalVars["comboboxCFAccount"]);
        renderComboBoxOnlyElementSendValueName('#cbAccount', 'Choose..', _GlobalVars["comboboxCFAccount"], 1, "accountFilter", "CFAccount", "ID");
    });



}

function filterMasterTable()
{

    MasterTableFilter = _GlobalVars["masterTable"];
    
    if (_GlobalVars["buFilter"].length != 0)
    {

        //_GlobalVars["buFilter"].forEach(function (element) {
        //    MasterTableFilter = MasterTableFilter.filter(function (el) { return el.BU == element; })
        //});

        MasterTableFilter = MasterTableFilter.filter(
        qmData => {
            if (_GlobalVars['buFilter'].includes(qmData['BU'].toString()))
                return qmData
        });

    }
    if (_GlobalVars["countryFilter"].length != 0)
    {

       // _GlobalVars["countryFilter"].forEach(function (element) {
       //     MasterTableFilter = MasterTableFilter.filter(function (el) { return el.BU == element; })
       // });

        MasterTableFilter = MasterTableFilter.filter(
       qmData => {
           if (_GlobalVars['countryFilter'].includes(qmData['countryID'].toString()))
               return qmData
       });

    }
    if (_GlobalVars["accontTypeFilter"].length != 0)
    {
        //_GlobalVars["accontTypeFilter"].forEach(function (element) {
        //    MasterTableFilter = MasterTableFilter.filter(function (el) { return el.BU == element; })
        //});

        MasterTableFilter = MasterTableFilter.filter(
       qmData => {
           if (_GlobalVars['accontTypeFilter'].includes(qmData['typeID'].toString()))
               return qmData
       });

    }
    if (_GlobalVars["accountFilter"].length != 0)
    {
        //_GlobalVars["accountFilter"].forEach(function (element) {
        //    MasterTableFilter = MasterTableFilter.filter(function (el) { return el.BU == element; })
        //});

        MasterTableFilter = MasterTableFilter.filter(
       qmData => {
           if (_GlobalVars['accountFilter'].includes(qmData['cfAccountID'].toString()))
               return qmData
       });
        
    }

  
    if (comboboxValueSelected != '0') {
        if (comboboxValueSelected == 1)
        {
            MasterTableFilter = MasterTableFilter.filter(
                function (el) {
                    return (el.CurrentQuater + el.ManualUpdate) != 0;
                })
        }
        if (comboboxValueSelected == 2) {
            MasterTableFilter = MasterTableFilter.filter(
                function (el) {
                    return (el.CurrentQuater + el.ManualUpdate) > 0;
                })
        }
        if (comboboxValueSelected == 3) {
            MasterTableFilter = MasterTableFilter.filter(
                function (el) {
                    return (el.CurrentQuater + el.ManualUpdate) < 0;
                })
        }
        
    }

    viewModel.masterTable(MasterTableFilter);


}

function clearMasterTable()
{
    viewModel.masterTable(_GlobalVars["masterTable"]);

}

function downloadData()
{
 
    _downloadExcelJson({
        jsonData: JSON.stringify(MasterTableFilter),
        filename: "MasterTable",
        success: {
            msg: "Please wait, generating report..."
        }
    });
    
} 


function uploadCable() {

    var url = '/CashFlow/CF/ManualUpload/';

    var win = window.open(url, '_blank');
    win.focus();


}


function downloadTXT()
{
    var listOfParams = {
        Region: RegionSelected,
        Quater: QuaterSelected
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/MasterTableCF/getCF_DdownloadInTXT',
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (data) {
            //download(json, 'json.txt', 'text/plain');
           
            var textData = '';
            var dateSelected = [];
            var dateYear;

            dateSelected =  viewModel.cbQuater().filter(function (el) { return el.ID == QuaterSelected; })
           

            if (dateSelected[0].Quater == 1)
            {
                dateYear = 'MAR' + dateSelected[0].Year;
            }
            if (dateSelected[0].Quater == 2) {
                dateYear = 'JUN' + dateSelected[0].Year;
            }
            if (dateSelected[0].Quater == 3) {
                dateYear = 'SEP' + dateSelected[0].Year;
            }
            if (dateSelected[0].Quater == 4) {
                dateYear = 'DEC' + dateSelected[0].Year;
            }


            var downloadDate = jQuery.parseJSON(data);

            if (_GlobalVars["buFilter"].length != 0) {

                downloadDate = downloadDate.filter(
                qmData => {
                    if (_GlobalVars['buFilter'].includes(qmData['BU'].toString()))
                        return qmData
                });
            }


            downloadDate.forEach(function (element) {
                
                var row = dateYear + '  ' + element.BU + '  ' + element.CFAccount + '  ' + element.CurrentQuater + '\r\n'

                textData = textData + row;
            });


            download(textData, 'CashFlowCable.txt', 'text/plain');

        }
    });
}

function download(content, fileName, contentType) {
    var a = document.createElement("a");
    var file = new Blob([content], { type: contentType });
    a.href = URL.createObjectURL(file);
    a.download = fileName;
    a.click();
}


function tourguide() {
    var enjoyhint_instance = new EnjoyHint({});

    var enjoyhint_script_steps = [
        {
            "next #menu-permit-name": 'Hello, I\'d like to tell you about the master table module and how to use it.<br> Click "Next" to proceed.'
        },
        {
            "next #cbRegion": 'Select the region you want to review.<br> Click "Next" to proceed.'
        },
        {
            "next #cbQuater": 'Select the Quarter you want to review.<br> Click "Next" to proceed.'
        },
        {
            "next #trFilterOption": 'Choose in the options to filter the data as you needed.<br> Click "Next" to proceed.'
        }
        ,
        {
            "next #btnRunData": 'Click this button if you need to reprocess the data. As recommendation if the data is properly filtered the action will take less to be executed.<br> Click "Next" to proceed.'
        }
        ,
        {
            "next #btnFilterValues": 'Click this button to filter the data.<br> Click "Next" to proceed.'
        },
        {
            "next #btnClearFilter": 'Need to remove the filter. Click this button.<br> Click "Next" to proceed.'
        },
        {
            "next #btnExcel": 'Need a report click this button and get the data in excel.<br> Click "Next" to proceed.'
        },
        {
            "next #cbShowMonthDivision": 'Click this button to view your data divided by months.<br> Click "Next" to proceed.'
        },
        {
            "next #gridPrincipal": 'Review all the data generated by the tool.<br> Click "Next" to proceed.'
        }

    ];

    enjoyhint_instance.set(enjoyhint_script_steps);

    enjoyhint_instance.run();


}