﻿var _GlobalVars = {
    Region: [],
    cbQuater: [],
    FilterEdidtQuality: [],
    FilterBU: [],
    EditCheck: [],
    SelectedBU: [],
    AccountFilter: [],
    CheckIDFilter: [],
    OperatorFilter: [],
    ValidationFilter: [],
    DetailEditCheck: [],
    DataSelected: [],
    IDeditCheckFilter: [],
    CheckValues: [],
    ManualUpdates: []
};

var myVar = setInterval(myTimer, 3000);

function myTimer() {
   
    _GlobalVars['CheckValues'].forEach(function (item, index, array) {

        recalculateData(item);

        array.splice(index, 1);
    });

}



var RegionSelected;
var QuaterSelected;



var viewModel = {
    Region: ko.observable(),
    cbQuater: ko.observable(),
    cbYear: ko.observable(),
    EditCheck: ko.observableArray(),
    TotalFind: ko.observable(0),
    expandGridRight: ko.observableArray(),
    expandGridLeft: ko.observableArray(),
    BUDIVvisible: ko.observable(false),
    FilterResultVisible: ko.observable(false)
};


viewModel.QuaterSelected = ko.dependentObservable({
    read: viewModel.cbQuater,
    write: function (Quater) {
        QuaterSelected = Quater;

        if ((typeof QuaterSelected === 'undefined' || !QuaterSelected) || (typeof RegionSelected === 'undefined' || !RegionSelected)) {

            viewModel.BUDIVvisible(false);
            viewModel.FilterResultVisible(false);

        } else {

            var listOfParams = {
                Date: QuaterSelected,
                pID: RegionSelected
            }

            BindJsonAlias("procEC_GetFilterBU", "FilterBU", 'ReviewReport', listOfParams, function () {
                renderComboBox('#cbBU', 'Select the BU', "FilterBU", 1, "SelectedBU");
            });
            BindJsonAlias("procCF_EditCheckReport", "EditCheck", 'ReviewReport', listOfParams, function () {

            });

            viewModel.BUDIVvisible(true);
        }
    },
    owner: viewModel
});

viewModel.regionSelect = ko.dependentObservable({
    read: viewModel.Region,
    write: function (Region) {
        RegionSelected = Region;

        if ((typeof QuaterSelected === 'undefined' || !QuaterSelected) || (typeof RegionSelected === 'undefined' || !RegionSelected)) {
            viewModel.BUDIVvisible(false);
            viewModel.FilterResultVisible(false);
        } else {
            var listOfParams = {
                Date: QuaterSelected,
                pID: RegionSelected
            }

            BindJsonAlias("procEC_GetFilterBU", "FilterBU", 'ReviewReport', listOfParams, function () {
                renderComboBox('#cbBU', 'Select the BU', "FilterBU", 1, "SelectedBU");
            });

            BindJsonAlias("procCF_EditCheckReport", "EditCheck", 'ReviewReport', listOfParams, function () {

            });
            viewModel.BUDIVvisible(true);
        }
    },
    owner: viewModel
});



$(document).ready(function () {
    //$('.usd_input').mask('00000.00');

    viewModel.expandGridRightFilter = function (name) {
        return ko.utils.arrayFirst(this.expandGridRight(), function (expandGridRight) {
            return (expandGridRight.name.replace(/\s+/, "").toLowerCase() === name.replace(/\s+/, "").toLowerCase());
        });
    };

    viewModel.expandGridLeftFilter = function (name) {
        return ko.utils.arrayFirst(this.expandGridLeft(), function (expandGridLeft) {
            return (expandGridLeft.name.replace(/\s+/, "").toLowerCase() === name.replace(/\s+/, "").toLowerCase());
        });
    };


    BindJsonAlias("getRegion", "Region", "RegionFileAdmin", "", function () {
        viewModel.Region(_GlobalVars["Region"]);
        ko.applyBindings(viewModel);
    });

    var listOfParams = {
        IDFile: 0
    }

    BindJsonAlias("getQuater", "cbQuater", "UploadFile", listOfParams, function () {
        viewModel.cbQuater(_GlobalVars["cbQuater"]);
    });
    var FilterEdidtQuality = [];

    FilterEdidtQuality[0] = 'Edit Check';
    FilterEdidtQuality[1] = 'Quality Check';

    renderComboBoxOnlyElement('#cbEditQuality', 'Choose..', FilterEdidtQuality, 1, "FilterEdidtQuality");



    _hideMenu();
});

function onComboBoxCheck() {
    var AccountFilter;
    var CheckIDFilter;
    var OperatorFilter;
    var ValidationFilter = [];


    var anotherOne = _GlobalVars["SelectedBU"];

    if (_GlobalVars["SelectedBU"].length > 0) {

        viewModel.FilterResultVisible(true);

        var filteredArray = _GlobalVars["EditCheck"].filter(
       qmData => {
           if (anotherOne.includes(qmData['BU'].toString()))
               return qmData
       });

        viewModel.EditCheck([]);

        filteredArray.forEach(function (item, index, array) {


            var column = {

                "Check_ID": ko.observable(item.Check_ID),
                "BU": ko.observable(item.BU),
                "Type": ko.observable(item.Type),
                "Product": ko.observable(item.Product),
                "Description": ko.observable(item.Description),
                "Account": ko.observable(item.Account),
                "IceCode": ko.observable(item.IceCode),
                "Sign": ko.observable(item.Sign),
                "Amount_CF_Tool": ko.observable(item.Amount_CF_Tool),
                "Amount_Manual": ko.observable(item.Amount_Manual),
                "EC_Status": ko.observable(item.EC_Status),
                "ACCID": ko.observable(item.ACCID)
            }

            viewModel.EditCheck.push(column);
        });

        viewModel.TotalFind(filteredArray.length);

       // viewModel.EditCheck(filteredArray);
        _GlobalVars["DataSelected"] = filteredArray;
 


        CheckIDFilter = filteredArray.map(item => item.CheckID).filter((value, index, self) => self.indexOf(value) === index);

        renderComboBoxOnlyElement('#cbCheckID', 'Choose..', CheckIDFilter, 1, "CheckIDFilter");

        OperatorFilter = filteredArray.map(item => item.Operator).filter((value, index, self) => self.indexOf(value) === index);

        renderComboBoxOnlyElement('#cbOperator', 'Choose..', OperatorFilter, 1, "OperatorFilter");

        ValidationFilter[0] = 'Pass';
        ValidationFilter[1] = 'Fail';

        renderComboBoxOnlyElement('#cbValidation', 'Choose..', ValidationFilter, 1, "ValidationFilter");

        //cbAccount = 
        var listOfParams = {
            Date: QuaterSelected,
            pID: RegionSelected
        }

        BindJsonAlias("procCF_EditCheckReport", "DetailEditCheck", 'ReviewReport', listOfParams, function () {

            AccountFilter = _GlobalVars["DetailEditCheck"].map(item => item.Account).filter((value, index, self) => self.indexOf(value) === index);

            renderComboBoxOnlyElement('#cbAccount', 'Choose..', AccountFilter, 1, "AccountFilter");
        });

    } else {
        viewModel.FilterResultVisible(false);
    }


}

function filterEditCheck() {
    var data = _GlobalVars["DataSelected"];

    //AccountFilter: [],
    //CheckIDFilter: [],
    //OperatorFilter: [],
    //ValidationFilter: [],
    //FilterEdidtQuality: [],

    //Account Filter

    if (_GlobalVars['AccountFilter'].length != 0) {
        _GlobalVars["IDeditCheckFilter"] = [];

        var AccountDetail = _GlobalVars["DetailEditCheck"].filter(
         qmData => {
             if (_GlobalVars['AccountFilter'].includes(qmData['Account'].toString()))
                 return qmData
         });

        var checksid = AccountDetail.map(item => item.CheckID).filter((value, index, self) => self.indexOf(value) === index);


        var newArr = [];

        for (var i = 0; i < checksid.length; i++) {
            _GlobalVars["IDeditCheckFilter"].push(checksid[i]);
        }


        data = data.filter(
        qmData => {
            if (_GlobalVars["IDeditCheckFilter"].includes(qmData['ID']))
                return qmData
        });
    }

    //CheckID Filter

    if (_GlobalVars['CheckIDFilter'].length != 0) {
        data = data.filter(
        qmData => {
            if (_GlobalVars['CheckIDFilter'].includes(qmData['CheckID'].toString()))
                return qmData
        });
    }
    //Operator Filter
    if (_GlobalVars['OperatorFilter'].length != 0) {
        data = data.filter(
       qmData => {
           if (_GlobalVars['OperatorFilter'].includes(qmData['Operator'].toString()))
               return qmData
       });
    }
    //Validation Filter



    if (_GlobalVars['ValidationFilter'].length != 0) {

        var ValidationArray = [];

        _GlobalVars['ValidationFilter'].forEach(function (element) {
            if (element == 'Pass') {
                ValidationArray.push('1');
            }
            if (element == 'Fail') {
                ValidationArray.push('0');
            }
        });

        data = data.filter(
       qmData => {
           if (ValidationArray.includes(qmData['Validation'].toString()))
               return qmData
       });
    }
    //Type Filter
    if (_GlobalVars['FilterEdidtQuality'].length != 0) {

        var ValidationType = [];

        _GlobalVars['FilterEdidtQuality'].forEach(function (element) {
            if (element == 'Edit Check') {
                ValidationType.push('E');
            }
            if (element == 'Quality Check') {
                ValidationType.push('Q');
            }
        });

        data = data.filter(
        qmData => {
            if (ValidationType.includes(qmData['Type'].toString()))
                return qmData
        });
    }


    viewModel.EditCheck(data);
    viewModel.TotalFind(data.length);

}



function runEditCheck() {

    var itemsProcessed = 0;
    _GlobalVars["SelectedBU"].forEach(function (element) {



        var listOfParams = {
            Date: QuaterSelected,
            pID: RegionSelected,
            BU: element
        }

        BindJsonAlias("procEC_RunEditCheck", "EditCheck", 'ReviewReport', listOfParams, function () {

        });
        itemsProcessed++;
        if (itemsProcessed === _GlobalVars["SelectedBU"].length) {
            onComboBoxCheck();

        }


    });



}


function onManualChange(checkid)
{

    var check = checkid.parent().parent().children()[0].innerHTML;

    _GlobalVars['CheckValues'].push(check);
    //setInterval(function () { recalculateData(check); }, 3000);

}





function recalculateData(check)
{

    var ChangeVariance = viewModel.EditCheck().filter(function (el) { return el.Check_ID() == check; })


    var CFACCOUNT = ChangeVariance.filter(function (el) { return el.Type() == "CF Balance"; });

    var sum = 0;

    CFACCOUNT.forEach(function (element) {

        var data1 = parseInt(element.Amount_Manual());
        var data2 = parseInt(element.Amount_CF_Tool());

        if (isNaN(data1))
        {
            data1 = 0;
        }
        if (isNaN(data2)) {
            data2 = 0;
        }

        if (element.Sign() === '+'){
            sum = sum + (data1 + data2);
        }else{
            sum = sum - (data1 + data2);
        }
    });

    var BB = ChangeVariance.filter(function (el) { return el.Type() == "Beginning Balance"; });

    var EB = ChangeVariance.filter(function (el) { return el.Type() == "Ending Balance"; });

    var Variance = ChangeVariance.filter(function (el) { return el.Type() == "Variance"; });

    var intBeginining = parseInt(BB[0].Amount_CF_Tool());
    var intEndBalance = parseInt(EB[0].Amount_CF_Tool());

    var rightBalance = (intBeginining + intEndBalance);
    var leftBalance = sum;

    var difference = Math.abs((rightBalance - leftBalance));

    //viewModel.EditCheck().filter(function (el) { return el.Check_ID() == check && el.Type() == "Variance"; }).Amount_CF_Tool(((parseInt(BB[0].Amount_CF_Tool()) + parseInt(EB[0].Amount_CF_Tool())) - sum));

    viewModel.EditCheck().filter(function (el) { return el.Check_ID() == check && el.Type() == "Variance"; })[0].Amount_CF_Tool(difference);

    

    if (difference == 0) {
        viewModel.EditCheck().filter(function (el) { return el.Check_ID() == check && el.Type() == "Variance"; })[0].EC_Status('Pass');
    } else
    {
        viewModel.EditCheck().filter(function (el) { return el.Check_ID() == check && el.Type() == "Variance"; })[0].EC_Status('Fail');
    }

}


function SaveManualInputs()
{
    var count = 0;
    var total = viewModel.EditCheck().filter(function (el) { return el.Type() == "CF Balance"; }).length;

    viewModel.EditCheck().filter(function (el) { return el.Type() == "CF Balance"; }).forEach(function (element) {

        count = count + 1;

        var listOfParams = {
            CheckID : element.Check_ID(),
            Product : element.Product(),
            Account : element.Account(),
            AmountManual: element.Amount_Manual(),
            BU: element.BU(),
            QuaterSelected: QuaterSelected,
            RegionID: RegionSelected,
            ACCID: element.ACCID()
        }

        BindJsonString("procCF_SaveManualUpdates", "ManualUpdates", 'ReviewReport', listOfParams, function () {
            //_showNotification('success', 'Manual Data saved');
        });

        if (total == count)
        {
            runEditCheckFromSave();

        }
    });


}



function runEditCheckFromSave()
{
    var CheckIDFilter = [];

    CheckIDFilter = viewModel.EditCheck().map(item => item.BU()).filter((value, index, self) => self.indexOf(value) === index);

    CheckIDFilter.forEach(function (element) {

        var listOfParams = {
            Date: QuaterSelected,
            BU: element,
            pID: RegionSelected
        }

        BindJsonString("procEC_RunEditCheck", "EditCheck", 'EditCheck', listOfParams, function () {


          _showNotification('success', 'Manual Data saved');
        });


    });


   


}


function downloadData() {
    var download = [];

    $.each(viewModel.EditCheck(), function (index, item) {

        var column = {

            "Check_ID":item.Check_ID(),
            "BU": item.BU(),
            "Type": item.Type(),
            "Product": item.Product(),
            "Account": item.Account(),
            "Description": item.Description(),
            "Sign": item.Sign(),
            "Amount_CF_Tool": item.Amount_CF_Tool(),
            "Amount_Manual": item.Amount_Manual(),
            "EC_Status": item.EC_Status()
        }

        download.push(column);
    });


    _downloadExcelJson({
        jsonData: JSON.stringify(download),
        filename: "EditCheckReport",
        success: {
            msg: "Please wait, generating report..."
        }
    });

}