﻿var _GlobalVars = {
    Account: [],
    BatchOrder: [],
    ActionOnResult: [],
    FileList: [],
    ActionOnColumn: [],
    ColumnSelected: [],
    ListRules: [],
    AllColumnList: [],
    TypeActionBaseColumn: [],
    getCF_AskYearBU: [],
    getValue1: [],
    getValue2: [],
    getValue3: [],
    getValue4: [],
    getValue5: [],
    getValue6: [],
    getValue7: [],
    DummyVal: []
};


var viewModel = {
    Account: ko.observable(),
    CFAccount: ko.observable(),
    BatchOrder: ko.observableArray(),
    ActionOnResult: ko.observableArray(),
    FileList: ko.observableArray(),
    ActionOnColumn: ko.observableArray(),
    ColumnSelected: ko.observableArray(),
    cbIsTableCheck: ko.observable(),
    ViewColumn: ko.observable(),
    cbIsNegative: ko.observable(),
    txtValue: ko.observable(),
    ListRules: ko.observableArray(),
    whereID: ko.observableArray(),
    whereBatch: ko.observableArray(),
    whereCFAccountID: ko.observableArray(),
    whereAction: ko.observableArray(),
    whereIsFromFile: ko.observableArray(),
    whereName: ko.observableArray(),
    whereColumnName: ko.observableArray(),
    whereActionColumn: ko.observableArray(),
    whereValue: ko.observableArray(),
    AllColumnList: ko.observableArray(),
    TypeActionBaseColumn: ko.observableArray(),
    txtClause: ko.observable(),
    txtMask: ko.observable(),
    ListClause: ko.observableArray(),
    AskMonth: ko.observable(),
    AskBU: ko.observable(),
    cbM1: ko.observable(false),
    cbM2: ko.observable(false),
    cbM3: ko.observable(false),
    cbCondi: ko.observable(false),
    cbCondiResult: ko.observable(false),
    cbLast: ko.observable(),
    getValue1: ko.observable(),
    getValue2: ko.observable(),
    getValue3: ko.observable(),
    getValue4: ko.observable(),
    getValue5: ko.observable(),
    getValue6: ko.observable(),
    getValue7: ko.observable(),
    toolTip1: ko.observable(),
    toolTip2: ko.observable(),
    toolTip3: ko.observable(),
    keyMask: ko.observable(),
    txtUpdateMask: ko.observable(),
    txtUpdateFile: ko.observable(),
    updateId: ko.observable(),
    updateConditional: ko.observable(false),
};

var KeyIDSelected;

var BatchOrderSel;
var ActionOnResultSelect;
var FileListSelect;
var ActionOnColumn;
var ColumnSelected;


var AllColumnListSelected;
var TypeActionBaseColumnSelected;

var AllColumnListSelectedMonth;
var AllColumnListSelectedYear;
var AllColumnListSelectedBU;

viewModel.AllColumnListSelected = ko.dependentObservable({
    read: viewModel.AllColumnList,
    write: function (column) {
        AllColumnListSelected = column;
        if (typeof column === 'undefined' || !column) {

            viewModel.TypeActionBaseColumn([]);
        } else {
            var listOfParams2 = {
                ColumnID: column
            }

            BindJsonAlias("getCF_TypeWhere", "TypeActionBaseColumn", "AddKey", listOfParams2, function () {
                viewModel.TypeActionBaseColumn(_GlobalVars["TypeActionBaseColumn"]);
            });

        }
    }
});

viewModel.TypeActionBaseColumnSelected = ko.dependentObservable({
    read: viewModel.TypeActionBaseColumn,
    write: function (action) {
        TypeActionBaseColumnSelected = action;
        if (typeof action === 'undefined' || !action) {
        } else {
        }
    }
});


viewModel.AddClause = function () {
    
    var error = "";
    var count = 1;

    if (typeof AllColumnListSelected === 'undefined' || !AllColumnListSelected) {
        if (count == 1) {
            error = error + "Please select the column affected";
        } else {
            error = error + ", select the column affected";
        }
        count = count + 1;
    }
    if (typeof TypeActionBaseColumnSelected === 'undefined' || !TypeActionBaseColumnSelected) {
        if (count == 1) {
            error = error + "Please select the type action";
        } else {
            error = error + ", select the type action";
        }
        count = count + 1;
    }

    if (typeof viewModel.txtClause() === 'undefined' || !viewModel.txtClause()) {
        if (count == 1) {
            error = error + "Please add the clause text";
        } else {
            error = error + ", add the clause text";
        }
        count = count + 1;
    }


    


    if (error != "") {
        _showNotification('error', error);
    } else {

        var listOfParams2 = {
            ID: viewModel.whereID(),
            ColumnID: AllColumnListSelected,
            ColumnActionID: TypeActionBaseColumnSelected,
            Value: viewModel.txtClause()
        }




        BindJsonAlias("addCF_WhereList", "ListClause", "AddKey", listOfParams2, function () {
            viewModel.ListClause(_GlobalVars["ListClause"]);
        });

        viewModel.txtClause("");
        _showNotification('success', "Where clause succesfully added");
    }
};

viewModel.AddKey = function () {
    var error = "";
    var count = 1;
    var iscountry = false;


        if (typeof FileListSelect === 'undefined' || !FileListSelect) {
            if (count == 1) {
                error = error + "Please select the file";
            } else {
                error = error + ", select the file";
            }
            count = count + 1;
        }
        if (typeof ActionOnColumn === 'undefined' || !ActionOnColumn) {
            if (count == 1) {
                error = error + "Please select the action on the column";
            } else {
                error = error + ", select the action on the column";
            }
            count = count + 1;
        }  
        if (typeof ColumnSelected === 'undefined' || !ColumnSelected) {
            if (count == 1) {
                error = error + "Please select the column of the file";
            } else {
                error = error + ", select  the column of the file";
            }
            count = count + 1;
        }
        if (typeof viewModel.txtMask() === 'undefined' || !viewModel.txtMask()) {
            if (count == 1) {
                error = error + "Please add the mask of the key";
            } else {
                error = error + ", add the mask text";
            }
            count = count + 1;
        }


    if (error != "") {
        _showNotification('error', error);
    } else {
        var listOfParams = {
                AccountID: $('#hfAccount').val(),
                FileID: FileListSelect,
                ColumnId: ColumnSelected,
                TypeActionColumn: ActionOnColumn,
                mask: viewModel.txtMask(),
                ActionOnResult: ActionOnResultSelect
       }

        viewModel.FileList([]);
        viewModel.ActionOnColumn([]);
        viewModel.ColumnSelected([]);

        viewModel.FileList(_GlobalVars["FileList"]);
        viewModel.ActionOnColumn(_GlobalVars["ActionOnColumn"]);
        viewModel.ColumnSelected(_GlobalVars["ColumnSelected"]);

        BindJsonAlias("add_CF_Rule", "ListRules", "AddKey", listOfParams, function () {
            viewModel.ListRules([]);

            _GlobalVars["ListRules"].forEach(function (item, index, array) {

                var column = {
                    "ID": item.ID,
                    "VarcharInKey": item.VarcharInKey,
                    "CFAccountID": item.CFAccountID,
                    "Name": item.Name,
                    "ColumnName": item.ColumnName,
                    "ActionColumn": item.ActionColumn,
                    "Mask": item.Mask,
                    "ActionOnResult": item.ActionOnResult,
                    "IsConditionalKey": item.IsConditionalKey
                    
                }
                viewModel.ListRules.push(column);
            });



            $('#AddKey').modal('hide');
            
        });



        _showNotification('success', "Key succesfully added");
    }

};

viewModel.updateKey = function () {
    var error = "";
    var count = 1;
    var iscountry = false;


    if (typeof FileListSelect === 'undefined' || !FileListSelect) {
        if (count == 1) {
            error = error + "Please select the file";
        } else {
            error = error + ", select the file";
        }
        count = count + 1;
    }
    if (typeof ActionOnColumn === 'undefined' || !ActionOnColumn) {
        if (count == 1) {
            error = error + "Please select the action on the column";
        } else {
            error = error + ", select the action on the column";
        }
        count = count + 1;
    }
    if (typeof ColumnSelected === 'undefined' || !ColumnSelected) {
        if (count == 1) {
            error = error + "Please select the column of the file";
        } else {
            error = error + ", select  the column of the file";
        }
        count = count + 1;
    }
    if (typeof viewModel.txtMask() === 'undefined' || !viewModel.txtMask()) {
        if (count == 1) {
            error = error + "Please add the mask of the key";
        } else {
            error = error + ", add the mask text";
        }
        count = count + 1;
    }


    if (error != "") {
        _showNotification('error', error);
    } else {
        var listOfParams = {
            AccountID: $('#hfAccount').val(),
            FileID: FileListSelect,
            ColumnId: ColumnSelected,
            TypeActionColumn: ActionOnColumn,
            mask: viewModel.txtMask(),
            ActionOnResult: ActionOnResultSelect
        }

        viewModel.FileList([]);
        viewModel.ActionOnColumn([]);
        viewModel.ColumnSelected([]);

        viewModel.FileList(_GlobalVars["FileList"]);
        viewModel.ActionOnColumn(_GlobalVars["ActionOnColumn"]);
        viewModel.ColumnSelected(_GlobalVars["ColumnSelected"]);

        BindJsonAlias("add_CF_Rule", "ListRules", "AddKey", listOfParams, function () {
            viewModel.ListRules([]);

            _GlobalVars["ListRules"].forEach(function (item, index, array) {

                var column = {
                    "ID": item.ID,
                    "VarcharInKey": item.VarcharInKey,
                    "CFAccountID": item.CFAccountID,
                    "Name": item.Name,
                    "ColumnName": item.ColumnName,
                    "ActionColumn": item.ActionColumn,
                    "Mask": item.Mask,
                    "ActionOnResult": item.ActionOnResult,
                    "IsConditionalKey": ko.observable(item.IsConditionalKey)

                }
                viewModel.ListRules.push(column);
            });



            $('#AddKey').modal('hide');

        });



        _showNotification('success', "Key succesfully added");
    }

};

viewModel.SaveMonthBU = function () {
    

};

viewModel.BatchOrderSelect = ko.dependentObservable({
    read: viewModel.BatchOrder,
    write: function (batch) {
        BatchOrderSel = batch;
        if (typeof batch === 'undefined' || !batch) {
        } else {
        }
    }
});

viewModel.ActionOnResultSelect = ko.dependentObservable({
    read: viewModel.ActionOnResult,
    write: function (action) {
        ActionOnResultSelect = action;
        if (typeof action === 'undefined' || !action) {
        } else {
        }
    }
});

viewModel.FileListSelect = ko.dependentObservable({
    read: viewModel.FileList,
    write: function (file) {
        FileListSelect = file;
        if (typeof file === 'undefined' || !file) {
            _GlobalVars["ColumnSelected"] = [];
            viewModel.ColumnSelected(_GlobalVars["ColumnSelected"]);
        } else {
            var listOfParams = {
                FileID: file
            }

            BindJsonAlias("getCF_FileColumn", "ColumnSelected", "AddKey", listOfParams, function () {
                viewModel.ColumnSelected(_GlobalVars["ColumnSelected"]);
            });
        }
    }
});

viewModel.ActionOnColumnSelect = ko.dependentObservable({
    read: viewModel.ActionOnColumn,
    write: function (action) {
        ActionOnColumn = action;
        if (typeof action === 'undefined' || !action) {
        } else {
        }
    }
});

viewModel.ColumnSelect = ko.dependentObservable({
    read: viewModel.ActionOnColumn,
    write: function (column) {
        ColumnSelected = column;
        if (typeof column === 'undefined' || !column) {
        } else {
        }
    }
});

viewModel.gridKeysList = new KOGridModel({
    data: viewModel.ListRules,
    columns: [
        { header: "Key", template: "divBtnKey" },
        { header: "File / Conditional Rule", data: "Name" },
        { header: "Column Selected / Result", data: "ColumnName" },
        { header: "Action On Column", data: "ActionColumn" },
        { header: "Action On Result", data: "ActionOnResult" },
        { header: "", template: 'divActions' },
    ],
    pageSize: 25
});

viewModel.gridWhereList = new KOGridModel({
    data: viewModel.ListClause,
    columns: [
        { header: "Column Name", data: "ColumnName" },
        { header: "Action on Result", data: "ActionName" },
        { header: "Value Search", data: "ValueSearch" },
        { header: "", template: 'divActionsList' },
    ],
    pageSize: 4
});

viewModel.AllColumnListSelectedMonth = ko.dependentObservable({
    read: viewModel.AllColumnList,
    write: function (column) {
        AllColumnListSelectedMonth = column; 
    }
});
viewModel.AllColumnListSelectedYear = ko.dependentObservable({
    read: viewModel.AllColumnList,
    write: function (column) {
        AllColumnListSelectedYear = column;
    }
});
viewModel.AllColumnListSelectedBU = ko.dependentObservable({
    read: viewModel.AllColumnList,
    write: function (column) {
        AllColumnListSelectedBU = column;
    }
});


$(document).ready(function () {

    document.body.style.zoom = "80%";

    var listOfParams = {
        AccountID: $('#hfAccount').val()
    }

    BindJsonAlias("getCF_AccountByID", "Account", "AddKey", listOfParams, function () {
        

       viewModel.Account(_GlobalVars["Account"]);

       ko.applyBindings(viewModel);


       BindJsonAlias("getCF_FileList", "FileList", "AddKey", listOfParams, function () {
           viewModel.FileList(_GlobalVars["FileList"]);
       });

       BindJsonAlias("getCF_ActionOnColumn", "ActionOnColumn", "AddKey", listOfParams, function () {
           viewModel.ActionOnColumn(_GlobalVars["ActionOnColumn"]);
       });

       BindJsonAlias("getCF_ActionOnResult", "ActionOnResult", "AddKey", listOfParams, function () {
           viewModel.ActionOnResult(_GlobalVars["ActionOnResult"]);
       });


       BindJsonAlias("get_CF_Rule", "ListRules", "AddKey", listOfParams, function () {

           viewModel.ListRules([]);

           _GlobalVars["ListRules"].forEach(function (item, index, array) {

               var column = {
                   "ID": item.ID,
                   "VarcharInKey": item.VarcharInKey,
                   "CFAccountID": item.CFAccountID,
                   "Name": item.Name,
                   "ColumnName": item.ColumnName,
                   "ActionColumn": item.ActionColumn,
                   "Mask": item.Mask,
                   "ActionOnResult": item.ActionOnResult,
                   "IsConditionalKey": ko.observable(item.IsConditionalKey)
               }

               viewModel.ListRules.push(column);
           });
           viewModel.ListRules(_GlobalVars["ListRules"]);

           getValuesRules();
          
       });


       viewModel.CFAccount(viewModel.Account()[0]['CFAccount'] + ' Q' + viewModel.Account()[0]['QuaterApply'] + ' ' + viewModel.Account()[0]['YearApply'] + ' ' + viewModel.Account()[0]['Region']);
    });
    
    _hideMenu();

    $("#AddKey").modal()

    $('#divBuildKey').collapse({
        toggle: true
    });
   
});

function goBack()
{
   
    var url = '/CashFlow/CF/MappingKeys/' + viewModel.Account()[0]['RegionID'] + '/' + viewModel.Account()[0]['dateID'] + '/' + viewModel.Account()[0]['QuaterApply'] + '' + viewModel.Account()[0]['YearApply'];

    window.location.href = url;


}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) ) {
        return false; 
    }
    return true;
}

function AddWhere(parent) {

    if (parent.IsConditionalKey == false) {

        var listOfParams = {
            IDCFAccount: parent.value
        }

        viewModel.whereID(parent.ID);
        viewModel.whereName(parent.Name);
        viewModel.whereColumnName(parent.ColumnName);
        viewModel.whereActionColumn(parent.ActionColumn);

        var listOfParams2 = {
            ID: parent.ID
        }

        KeyIDSelected = parent.ID;

        BindJsonAlias("getCF_AllFileColumn", "AllColumnList", "AddKey", listOfParams2, function () {

            viewModel.AllColumnList(_GlobalVars["AllColumnList"]);
        });

        BindJsonAlias("getCF_AskYearBU", "getCF_AskYearBU", "AddKey", listOfParams2, function () {
            viewModel.AskMonth(_GlobalVars["getCF_AskYearBU"][0]);
            viewModel.AskBU(_GlobalVars["getCF_AskYearBU"][0])

        });

        BindJsonAlias("getCF_WhereList", "ListClause", "AddKey", listOfParams2, function () {
            viewModel.ListClause(_GlobalVars["ListClause"]);
        });

        $("#AddWhereClause").modal();
    } else { }
}

function DeleteKey(parent) {

    var listOfParams = {
            AccountID: $('#hfAccount').val(),
            ID: parent.ID
            }


        BindJsonAlias("delete_CF_Rule", "ListRules", "AddKey", listOfParams, function () {

            viewModel.ListRules([]);

            _GlobalVars["ListRules"].forEach(function (item, index, array) {


                var column = {
                    "ID": item.ID,
                    "VarcharInKey": item.VarcharInKey,
                    "CFAccountID": item.CFAccountID,
                    "Name": item.Name,
                    "ColumnName": item.ColumnName,
                    "ActionColumn": item.ActionColumn,
                    "Mask": item.Mask,
                    "ActionOnResult": item.ActionOnResult,
                    "IsConditionalKey": ko.observable(item.IsConditionalKey)
                }

                viewModel.ListRules.push(column);
            });
            viewModel.ListRules(_GlobalVars["ListRules"]);
        });

}

function DeleteWhere(parent) {

    var listOfParams = {
        ID: parent.ID
    }
    var listOfParams2 = {
        ID: KeyIDSelected
    }

    BindJsonString("deleteCF_WhereList", "ListClause", "AddKey", listOfParams, function () {
       

        BindJsonAlias("getCF_WhereList", "ListClause", "AddKey", listOfParams2, function () {
            viewModel.ListClause(_GlobalVars["ListClause"]);
        });

    });


}

function tooltipValues()
{

    //var tooltip;
    ////toolTip1: ko.observable(),
    ////toolTip2: ko.observable(),
    ////toolTip3: ko.observable()

    //    tooltip = viewModel.getValue1();

    //    $.each(_GlobalVars["ListRules"], function (index, value) {
    //        tooltip = tooltip.replace(value.VarcharInKey, value.Mask);
    //    });

    //    viewModel.toolTip1(tooltip);
 
    //    tooltip = viewModel.getValue2();

    //    $.each(_GlobalVars["ListRules"], function (index, value) {

    //        tooltip = tooltip.replace(value.VarcharInKey, value.Mask);
    //    });

    //    viewModel.toolTip2(tooltip);


    //    tooltip = viewModel.getValue3();

    //    $.each(_GlobalVars["ListRules"], function (index, value) {

    //        tooltip = tooltip.replace(value.VarcharInKey, value.Mask);
    //    });
    //    viewModel.toolTip3(tooltip);

}


function getValue(list, pOrder)
{
    var valueToAdd;

    var tooltip = '';

    if (pOrder == 1) {

        valueToAdd = "";
        $.each(list, function (index, value) {
            valueToAdd = valueToAdd + value.Value;
        });

        viewModel.getValue1(valueToAdd);
    }

    if (pOrder == 2) {

        valueToAdd = "";
        $.each(list, function (index, value) {
            valueToAdd = valueToAdd + value.Value;
        });

        viewModel.getValue2(valueToAdd);

    }

    if (pOrder == 3) {

        valueToAdd = "";
        $.each(list, function (index, value) {
            valueToAdd = valueToAdd + value.Value;
        });

        viewModel.getValue3(valueToAdd);
    }

    if (pOrder == 4) {

        valueToAdd = "";
        $.each(list, function (index, value) {
            valueToAdd = valueToAdd + value.Value;
        });

        viewModel.getValue4(valueToAdd);
    }

    if (pOrder == 5) {

        valueToAdd = "";
        $.each(list, function (index, value) {
            valueToAdd = valueToAdd + value.Value;
        });

        viewModel.getValue5(valueToAdd);
    }

    if (pOrder == 6) {

        valueToAdd = "";
        $.each(list, function (index, value) {
            valueToAdd = valueToAdd + value.Value;
        });

        viewModel.getValue6(valueToAdd);
    }

    if (pOrder == 7) {

        valueToAdd = "";
        $.each(list, function (index, value) {
            valueToAdd = valueToAdd + value.Value;
        });

        viewModel.getValue7(valueToAdd);
    }

    tooltipValues();
}


function getValuesRules()
{
    var listOfParams = {
        ID: $('#hfAccount').val(),
        Type: 1
    }

    BindJsonAlias("getRuleBuildKey", "getValue1", "AddKey", listOfParams, function () {
        getValue(_GlobalVars["getValue1"], 1);

    });

    var listOfParams = {
        ID: $('#hfAccount').val(),
        Type: 2
    }

    BindJsonAlias("getRuleBuildKey", "getValue2", "AddKey", listOfParams, function () {
        getValue(_GlobalVars["getValue2"],2);
    });

    var listOfParams = {
        ID: $('#hfAccount').val(),
        Type: 3
    }

    BindJsonAlias("getRuleBuildKey", "getValue3", "AddKey", listOfParams, function () {
        getValue(_GlobalVars["getValue3"],3);
    });

    var listOfParams = {
        ID: $('#hfAccount').val(),
        Type: 4
    }

    BindJsonAlias("getRuleBuildKey", "getValue4", "AddKey", listOfParams, function () {
        getValue(_GlobalVars["getValue4"],4);
    });

    var listOfParams = {
        ID: $('#hfAccount').val(),
        Type: 5
    }

    BindJsonAlias("getRuleBuildKey", "getValue5", "AddKey", listOfParams, function () {
        getValue(_GlobalVars["getValue5"],5);
    });


}


function addValueFromGrid(parent)
{
    if (viewModel.cbM1() == true) {
        order = _GlobalVars.getValue1.length;
        var obj = { RuleID: $('#hfAccount').val(), StringOrder: order, Type: 1, Value: parent.VarcharInKey };

        _GlobalVars.getValue1.push(obj);

        getValue(_GlobalVars["getValue1"], 1);

    }
    if (viewModel.cbM2() == true) {
        order = _GlobalVars.getValue2.length;
        var obj = { RuleID: $('#hfAccount').val(), StringOrder: order, Type: 2, Value: parent.VarcharInKey };

        _GlobalVars.getValue2.push(obj);

        getValue(_GlobalVars["getValue2"], 2);
    }
    if (viewModel.cbM3() == true) {
        order = _GlobalVars.getValue3.length;
        var obj = { RuleID: $('#hfAccount').val(), StringOrder: order, Type: 3, Value: parent.VarcharInKey };

        _GlobalVars.getValue3.push(obj);

        getValue(_GlobalVars["getValue3"], 3);

    }

    if (viewModel.cbCondi() == true) {
        order = _GlobalVars.getValue6.length;
        var obj = { RuleID: $('#hfAccount').val(), StringOrder: order, Type: 6, Value: parent.VarcharInKey };

        _GlobalVars.getValue6.push(obj);

        getValue(_GlobalVars["getValue6"], 6);

    }

    if (viewModel.cbCondiResult() == true) {
        order = _GlobalVars.getValue7.length;
        var obj = { RuleID: $('#hfAccount').val(), StringOrder: order, Type: 7, Value: parent.VarcharInKey };

        _GlobalVars.getValue7.push(obj);

        getValue(_GlobalVars["getValue7"], 7);

    }

    tooltipValues();
}

function addKeyFunctionNumberValue(pType)
{
    if (pType == 1) {
        if (viewModel.cbM1() == true) {
            order = _GlobalVars.getValue1.length;
            var obj = { RuleID: $('#hfAccount').val(), StringOrder: order, Type: 1, Value: $('#txtNum1').val() };

            _GlobalVars.getValue1.push(obj);

            getValue(_GlobalVars["getValue1"], 1);

        }
        if (viewModel.cbM2() == true) {
            order = _GlobalVars.getValue2.length;
            var obj = { RuleID: $('#hfAccount').val(), StringOrder: order, Type: 2, Value: $('#txtNum1').val() };

            _GlobalVars.getValue2.push(obj);

            getValue(_GlobalVars["getValue2"], 2);
        }
        if (viewModel.cbM3() == true) {
            order = _GlobalVars.getValue3.length;
            var obj = { RuleID: $('#hfAccount').val(), StringOrder: order, Type: 3, Value: $('#txtNum1').val() };

            _GlobalVars.getValue3.push(obj);

            getValue(_GlobalVars["getValue3"], 3);
        }

        

        tooltipValues();
    }

    if (pType == 6) {
        if (viewModel.cbCondi() == true)
        {
            order = _GlobalVars.getValue6.length;
            var obj = { RuleID: $('#hfAccount').val(), StringOrder: order, Type: 6, Value: $('#txtNum6').val() };

            _GlobalVars.getValue6.push(obj);

            getValue(_GlobalVars["getValue6"], 6);

        }
        if (viewModel.cbCondiResult() == true) {
            order = _GlobalVars.getValue7.length;
            var obj = { RuleID: $('#hfAccount').val(), StringOrder: order, Type: 7, Value: $('#txtNum6').val() };

            _GlobalVars.getValue6.push(obj);

            getValue(_GlobalVars["getValue7"], 7);

        }
    }
    if (pType == 4) {
        order = _GlobalVars.getValue4.length;
        var obj = { RuleID: $('#hfAccount').val(), StringOrder: order, Type: 4, Value: $('#txtNum4').val() };

        _GlobalVars.getValue4.push(obj);

        getValue(_GlobalVars["getValue4"], 4);
    }

    if (pType == 5) {
        order = _GlobalVars.getValue5.length;
        var obj = { RuleID: $('#hfAccount').val(), StringOrder: order, Type: 5, Value: $('#txtNum5').val() };

        _GlobalVars.getValue5.push(obj);

        getValue(_GlobalVars["getValue5"], 5);
    }
}


function addKeyFunction(pType,pValue)
{
    var order;
    if (pType == 1)
    {
        if (viewModel.cbM1() == true) {
            order = _GlobalVars.getValue1.length;
            var obj = { RuleID: $('#hfAccount').val(), StringOrder: order, Type: 1, Value: pValue };

            _GlobalVars.getValue1.push(obj);

            getValue(_GlobalVars["getValue1"],1);
        }
        if (viewModel.cbM2() == true) {
            order = _GlobalVars.getValue2.length;
            var obj = { RuleID: $('#hfAccount').val(), StringOrder: order, Type: 2, Value: pValue };

            _GlobalVars.getValue2.push(obj);

            getValue(_GlobalVars["getValue2"],2);
        }
        if (viewModel.cbM3() == true) {
            order = _GlobalVars.getValue3.length;
            var obj = { RuleID: $('#hfAccount').val(), StringOrder: order, Type: 3, Value: pValue };

            _GlobalVars.getValue3.push(obj);

            getValue(_GlobalVars["getValue3"],3);
        }

        tooltipValues();
    }

    if (pType == 6) {
            
        if (viewModel.cbCondi() == true) {
                order = _GlobalVars.getValue6.length;
                var obj = { RuleID: $('#hfAccount').val(), StringOrder: order, Type: 6, Value: pValue };

                _GlobalVars.getValue6.push(obj);

                getValue(_GlobalVars["getValue6"], 6);

            }
        if (viewModel.cbCondiResult() == true) {
                order = _GlobalVars.getValue6.length;
                var obj = { RuleID: $('#hfAccount').val(), StringOrder: order, Type: 7, Value: pValue };

                _GlobalVars.getValue7.push(obj);

                getValue(_GlobalVars["getValue7"], 7);

            }
    }

    if (pType == 4) {
        order = _GlobalVars.getValue4.length;
        var obj = { RuleID: $('#hfAccount').val(), StringOrder: order, Type: 4, Value: pValue };

        _GlobalVars.getValue4.push(obj);
        getValue(_GlobalVars["getValue4"],4);

    }

    if (pType == 5) {
        order = _GlobalVars.getValue5.length;
        var obj = { RuleID: $('#hfAccount').val(), StringOrder: order, Type: 5, Value: pValue };

        _GlobalVars.getValue5.push(obj);
        getValue(_GlobalVars["getValue5"], 5);

    }
}

function removeKeyFunction(pType)
{
    var lenght = 0;
    if (pType == 1)
    {
        lenght = _GlobalVars["getValue1"].length - 1;
        _GlobalVars["getValue1"] = _GlobalVars["getValue1"].slice(0, lenght);
        getValue(_GlobalVars["getValue1"], 1);

        tooltipValues();
    }
    if (pType == 2) {
        lenght = _GlobalVars["getValue2"].length - 1;
        _GlobalVars["getValue2"] = _GlobalVars["getValue2"].slice(0, lenght);
        getValue(_GlobalVars["getValue2"], 2);

        tooltipValues();
    }
    if (pType == 3) {
        lenght = _GlobalVars["getValue3"].length - 1;
        _GlobalVars["getValue3"] = _GlobalVars["getValue3"].slice(0, lenght);
        getValue(_GlobalVars["getValue3"], 3);

        tooltipValues();
    }

    if (pType == 6) {
        lenght = _GlobalVars["getValue6"].length - 1;
        _GlobalVars["getValue6"] = _GlobalVars["getValue6"].slice(0, lenght);
        getValue(_GlobalVars["getValue6"], 6);

        tooltipValues();
    }

    if (pType == 7) {
        lenght = _GlobalVars["getValue6"].length - 1;
        _GlobalVars["getValue7"] = _GlobalVars["getValue7"].slice(0, lenght);
        getValue(_GlobalVars["getValue7"], 7);

        tooltipValues();
    }

    if (pType == 4) {
        lenght = _GlobalVars["getValue4"].length - 1;
        _GlobalVars["getValue4"] = _GlobalVars["getValue4"].slice(0, lenght);
        getValue(_GlobalVars["getValue4"], 4);
    }
    if (pType == 5) {
        lenght = _GlobalVars["getValue5"].length - 1;
        _GlobalVars["getValue5"] = _GlobalVars["getValue5"].slice(0, lenght);
        getValue(_GlobalVars["getValue5"], 5);
    }
    
}


function saveIndDataBase()
{

    var listOfParams = {
        ID: $('#hfAccount').val()
    }

    BindJsonString("removeRuleBuildKey", "DummyVal", "AddKey", listOfParams, function () {

        $.each(_GlobalVars["getValue1"], function (index, value) {
            var listOfParams = {
                ID: $('#hfAccount').val(),
                Type: 1,
                ORDER: value.StringOrder,
                Value: value.Value
            }
            BindJsonString("saveRuleBuildKey", "DummyVal", "AddKey", listOfParams, function () { });

        });

        $.each(_GlobalVars["getValue2"], function (index, value) {
            var listOfParams = {
                ID: $('#hfAccount').val(),
                Type: 2,
                ORDER: value.StringOrder,
                Value: value.Value
            }
            BindJsonString("saveRuleBuildKey", "DummyVal", "AddKey", listOfParams, function () { });
        });

        $.each(_GlobalVars["getValue3"], function (index, value) {
            var listOfParams = {
                ID: $('#hfAccount').val(),
                Type: 3,
                ORDER: value.StringOrder,
                Value: value.Value
            }
            BindJsonString("saveRuleBuildKey", "DummyVal", "AddKey", listOfParams, function () { });
        });

        $.each(_GlobalVars["getValue4"], function (index, value) {
            var listOfParams = {
                ID: $('#hfAccount').val(),
                Type: 4,
                ORDER: value.StringOrder,
                Value: value.Value
            }
            BindJsonString("saveRuleBuildKey", "DummyVal", "AddKey", listOfParams, function () { });
        });

        $.each(_GlobalVars["getValue5"], function (index, value) {
            var listOfParams = {
                ID: $('#hfAccount').val(),
                Type: 5,
                ORDER: value.StringOrder,
                Value: value.Value
            }
            BindJsonString("saveRuleBuildKey", "DummyVal", "AddKey", listOfParams, function () { });
        });




    });

    
        
    
}


function AddKeyConditional()
{

    var error = "";
    var count = 1;
    var iscountry = false;


    if (typeof viewModel.keyMask() === 'undefined' || !viewModel.keyMask()) {
        if (count == 1) {
            error = error + "Please add a mask";
        } else {
            error = error + ", add the mask";
        }
        count = count + 1;
    }
    if (typeof viewModel.getValue6() === 'undefined' || !viewModel.getValue6()) {
        if (count == 1) {
            error = error + "Please add the condition";
        } else {
            error = error + ", add the condition";
        }
        count = count + 1;
    }
    if (typeof viewModel.getValue7() === 'undefined' || !viewModel.getValue7()) {
        if (count == 1) {
            error = error + "Please add the result";
        } else {
            error = error + ", add the result";
        }
        count = count + 1;
    }


    if (error != "") {
        _showNotification('error', error);
    } else {
        var listOfParams = {
            AccountID: $('#hfAccount').val(),
            mask: viewModel.keyMask(),
            conditional: viewModel.getValue6(),
            FinalKey: viewModel.getValue7()

        }

        viewModel.keyMask([]);
        viewModel.getValue6([]);
        viewModel.getValue7([]);

        _GlobalVars["getValue6"] = [];
        _GlobalVars["getValue7"] = [];


        BindJsonAlias("add_CF_RuleConditional", "ListRules", "AddKey", listOfParams, function () {
            viewModel.ListRules([]);

            _GlobalVars["ListRules"].forEach(function (item, index, array) {

                var column = {
                    "ID": item.ID,
                    "VarcharInKey": item.VarcharInKey,
                    "CFAccountID": item.CFAccountID,
                    "Name": item.Name,
                    "ColumnName": item.ColumnName,
                    "ActionColumn": item.ActionColumn,
                    "Mask": item.Mask,
                    "ActionOnResult": item.ActionOnResult,
                    "IsConditionalKey": ko.observable(item.IsConditionalKey)

                }


                viewModel.ListRules.push(column);
            });



            $('#AddKey').modal('hide');

        });



        _showNotification('success', "Key succesfully added");
    }


}


function UpdateKeyModel(parent) {

    if (parent.IsConditionalKey == true)
    {

        $('#divConditionalKey').collapse({
            toggle: true
        });
        $('#divBuildKey').collapse({
            toggle: false
        });
        $('#divBuildFinalBalance').collapse({
            toggle: false
        });
        $('#divCondiFinalBalnce').collapse({
            toggle: false
        });

        viewModel.updateId(parent.ID);
        //viewModel.getValue6(parent.Name);
        //viewModel.getValue7(parent.ColumnName);
        viewModel.updateConditional(true);
        viewModel.keyMask(parent.Mask);


        var obj = { RuleID: $('#hfAccount').val(), StringOrder: 1, Type: 6, Value: parent.Name };

        _GlobalVars.getValue6.push(obj);

        getValue(_GlobalVars["getValue6"], 6);


        obj = { RuleID: $('#hfAccount').val(), StringOrder: 1, Type: 7, Value: parent.ColumnName };

        _GlobalVars.getValue7.push(obj);

        getValue(_GlobalVars["getValue7"], 7);


    }else{

        viewModel.updateConditional(false);

        viewModel.updateId(0);
        viewModel.getValue6([]);
        viewModel.getValue7([]);

        viewModel.keyMask([]);

    viewModel.txtUpdateMask(parent.Mask);
    viewModel.txtUpdateFile(parent.Name);
    viewModel.updateId(parent.ID);


        var listOfParams2 = {
            ID: parent.ID
        }

        BindJsonAlias("getCF_AllFileColumn", "AllColumnList", "AddKey", listOfParams2, function () {

            viewModel.AllColumnList(_GlobalVars["AllColumnList"]);
        });

        BindJsonAlias("getCF_AskYearBU", "getCF_AskYearBU", "AddKey", listOfParams2, function () {
            viewModel.AskMonth(_GlobalVars["getCF_AskYearBU"][0]);
            viewModel.AskBU(_GlobalVars["getCF_AskYearBU"][0])

        });

        BindJsonAlias("getCF_WhereList", "ListClause", "AddKey", listOfParams2, function () {
            viewModel.ListClause(_GlobalVars["ListClause"]);
        });

        $("#UpdateKeyDiv").modal();
    }
}


function cancelUpdate()
{
    viewModel.updateConditional(false);

    viewModel.updateId(0);
    viewModel.getValue6([]);
    viewModel.getValue7([]);

    viewModel.keyMask([]);
}


function updateKeyDataConditional()
{

    var error = "";
    var count = 1;
    var iscountry = false;


    if (typeof viewModel.keyMask() === 'undefined' || !viewModel.keyMask()) {
        if (count == 1) {
            error = error + "Please add a mask";
        } else {
            error = error + ", add the mask";
        }
        count = count + 1;
    }
    if (typeof viewModel.getValue6() === 'undefined' || !viewModel.getValue6()) {
        if (count == 1) {
            error = error + "Please add the condition";
        } else {
            error = error + ", add the condition";
        }
        count = count + 1;
    }
    if (typeof viewModel.getValue7() === 'undefined' || !viewModel.getValue7()) {
        if (count == 1) {
            error = error + "Please add the result";
        } else {
            error = error + ", add the result";
        }
        count = count + 1;
    }


    if (error != "") {
        _showNotification('error', error);
    } else {
        var listOfParams = {
            ID: viewModel.updateId(),
            mask: viewModel.keyMask(),
            conditional: viewModel.getValue6(),
            FinalKey: viewModel.getValue7()

        }

        viewModel.keyMask([]);
        viewModel.getValue6([]);
        viewModel.getValue7([]);

        _GlobalVars["getValue6"] = [];
        _GlobalVars["getValue7"] = [];


        BindJsonAlias("update_CF_RuleConditional", "ListRules", "AddKey", listOfParams, function () {
            viewModel.ListRules([]);

            _GlobalVars["ListRules"].forEach(function (item, index, array) {

                var column = {
                    "ID": item.ID,
                    "VarcharInKey": item.VarcharInKey,
                    "CFAccountID": item.CFAccountID,
                    "Name": item.Name,
                    "ColumnName": item.ColumnName,
                    "ActionColumn": item.ActionColumn,
                    "Mask": item.Mask,
                    "ActionOnResult": item.ActionOnResult,
                    "IsConditionalKey": ko.observable(item.IsConditionalKey)

                }
                viewModel.ListRules.push(column);
            });



            $('#AddKey').modal('hide');

        });


        cancelUpdate();
        _showNotification('success', "Key succesfully added");
    }




}

function updateKeyNotConditional()
{
    var error = "";
    var count = 1;
    var iscountry = false;

    if (typeof ActionOnColumn === 'undefined' || !ActionOnColumn) {
        if (count == 1) {
            error = error + "Please select the action on the column";
        } else {
            error = error + ", select the action on the column";
        }
        count = count + 1;
    }
    if (typeof AllColumnListSelected === 'undefined' || !AllColumnListSelected) {
        if (count == 1) {
            error = error + "Please select the column of the file";
        } else {
            error = error + ", select  the column of the file";
        }
        count = count + 1;
    }
    if (typeof viewModel.txtUpdateMask() === 'undefined' || !viewModel.txtUpdateMask()) {
        if (count == 1) {
            error = error + "Please add the mask of the key";
        } else {
            error = error + ", add the mask text";
        }
        count = count + 1;
    }


    if (error != "") {
        _showNotification('error', error);
    } else {
        var listOfParams = {
            ID: viewModel.updateId(),
            ColumnId: AllColumnListSelected,
            TypeActionColumn: ActionOnColumn,
            mask: viewModel.txtUpdateMask(),
            ActionOnResult: ActionOnResultSelect
        }

        viewModel.FileList([]);
        viewModel.ActionOnColumn([]);
        viewModel.ColumnSelected([]);

        viewModel.FileList(_GlobalVars["FileList"]);
        viewModel.ActionOnColumn(_GlobalVars["ActionOnColumn"]);
        viewModel.ColumnSelected(_GlobalVars["ColumnSelected"]);

        BindJsonAlias("update_CF_Rule", "ListRules", "AddKey", listOfParams, function () {
            viewModel.ListRules([]);

            _GlobalVars["ListRules"].forEach(function (item, index, array) {

                var column = {
                    "ID": item.ID,
                    "VarcharInKey": item.VarcharInKey,
                    "CFAccountID": item.CFAccountID,
                    "Name": item.Name,
                    "ColumnName": item.ColumnName,
                    "ActionColumn": item.ActionColumn,
                    "Mask": item.Mask,
                    "ActionOnResult": item.ActionOnResult,
                    "IsConditionalKey": ko.observable(item.IsConditionalKey)

                }
                viewModel.ListRules.push(column);
            });



            $('#AddKey').modal('hide');

        });


        $('#UpdateKeyDiv').modal('toggle');

        _showNotification('success', "Key succesfully added");
    }



}


function tourguide() {
    var enjoyhint_instance = new EnjoyHint({});

    var enjoyhint_script_steps = [
        {
            "next #btnAddNewKey": 'Hello, I\'d like to tell you about how to add conditions for this account please click this button.<br> Click "Next" to proceed.'
        },
        {
            "next #txtmask": 'Add a name(mask) for the condition you are about to enter.<br> Click "Next" to proceed.'
        },
        {
            "next #filechooose": 'Choose the main file of the key.<br> Click "Next" to proceed.'
        },
        {
            "next #cbaction": 'Choose the arithmetic operation for the selected column.<br> Click "Next" to proceed.'
        }
        ,
        {
            "next #cbColumn": 'Select the column which will be affected by the operation.<br> Click "Next" to proceed.'
        }
        ,
        {
            "next #actionOnColumn": 'Select if there is other rule action on the selected column.<br> Click "Next" to proceed.'
        },
        {
            "click #btnNewaddKey": 'Click "add key" button to add the new selection.<br> Click "Next" to proceed.'
        },
        {
            "next #listKeys": 'Review the new key.<br> Click "Next" to proceed.'
        },
        {
            "next #btnAddClause": 'Click this button to add conditions to the key.<br> Click "Next" to proceed.'
        },
        {
            "next #columnWhere": 'Select the column related to the condition you are about to enter.<br> Click "Next" to proceed.'
        },
        {
            "next #TypeWhere": 'Select the type of action (operators) between the column and the condition.<br> Click "Next" to proceed.'
        },
        {
            "next #txtClauseWhere": 'Write the value that will match the selected column.<br> Click "Next" to proceed.'
        },
        {
            "click #addNewClause": 'Save your new condition.'
        },
        {
            "next #tableClause": 'Review your new condition. Remenber you can add all the conditions that you need.<br> Click "Next" to proceed.'
        },
        {
            "next #btnDeleteClause": 'If you need to delete the condition please click this button.<br> Click "Next" to proceed.'
        },
        {
            "click #closeClauseData": 'Close the conditions popup.'
        },
        {
            "click #divBuildKey": 'This section is to build the mapping keys. Please check the month for which you want to apply the key (M3,M2 or M1)  <br> Click "Next" to proceed.'
        },
        {
            "click #btnSelectKey": 'Click on the key name (mask) you want to add.<br> Click "Next" to proceed.'
        },
        {
            "click #btnPluss": 'Click this button to add a sum action.'
        },
        {
            "click #btnSelectKey": 'Click on the key name (mask) you want to sum with the other key.<br> Click "Next" to proceed.'
        },
        {
            "click #BTNsavebuildyoukey": 'Save the created mapping.'
        },
        {
            "next #btnExpandFinalBalance": 'Click this buton to expand the build final balance.'
        },
        {
            "click #btnM1": 'Click on the button to add the balance of M1.'
        },
        {
            "click #btnSUMPLUSS": 'Click on sum to sum M1 with other value.'
        },
        {
            "click #btnM2": 'Click on the button to add the balance of M2.'
        },
        {
            "click #btnSUMPLUSS": 'Click on sum to sum (M1 + M2) with other value.'
        },
        {
            "click #btnM3": 'Click on the button to add the balance of M3.'
        },
        {
            "click #BTNsavebuildyoukey": 'Click on save button to save your new balance. Go to master table page and re Run your data and review your balance.'
        }


    ];

    enjoyhint_instance.set(enjoyhint_script_steps);

    enjoyhint_instance.run();


}