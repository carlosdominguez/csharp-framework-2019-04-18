﻿var _GlobalVars = {
    Region: [],
    FileRegion: [],
    cbBU: [],
    cbQuater: [],
    cbMonthYear: [],
    cbCountry: [],
    listLog: [],
    listLogByFile: [],
    listBU: []
};

var RegionSelected;
var FileSelected;
var IDFile;

var cbBUCount = 0;
var cbcountryCount = 0;
var cbMonthYearCount = 0;
var cbQuaterCount = 0;
var totalToViewUpload = 0;


var CountrySelected;
var MonthYearSelected;
var QuaterSelected;
var BUSelected;

var viewModel = {
    Region: ko.observable(),
    FileRegion: ko.observable(),
    AskBU: ko.observable(),
    AskCountry: ko.observable(),
    AskMonthYear: ko.observable(),
    AskQuater: ko.observable(),
    ViewUpload: ko.observable(),
    cbBU: ko.observable(),
    cbQuater: ko.observable(),
    cbMonthYear: ko.observable(),
    cbCountry: ko.observable(),
    listLog: ko.observable(),
    listLogByFile: ko.observable(),
    fileInput: ko.observable(),
    fileName: ko.observable(),
    someReader: new FileReader(),
    checkBU: ko.observable(),
    checkMonthYear: ko.observable(),
    checkCountry: ko.observable(),
    checkQuater: ko.observable()

};



viewModel.regionSelect = ko.dependentObservable({
    read: viewModel.Region,
    write: function (Region) {
        RegionSelected = Region;

        var listOfParams = {
            Region: Region
        }
        if (typeof Region === 'undefined' || !Region) {
            //$('#FileDiv').hide();
            $('#DivCbFile').hide();
            $('#DivCbDropDown').hide();
            $('#DivShowLog').hide();

        } else {
            //$('#FileDiv').show();
            BindJsonAlias("getFile", "FileRegion", 'RegionFileAdmin', listOfParams, function () {
                viewModel.FileRegion(_GlobalVars["FileRegion"]);

                BindJsonAlias("getBUbyRegion", "listBU", 'RegionFileAdmin', listOfParams, function () {
                    $('#DivCbFile').show();
                });
            });



        }
    },
    owner: viewModel
});

viewModel.fileSelected = ko.dependentObservable({
    read: viewModel.FileRegion,
    write: function (File) {


        if (typeof File === 'undefined' || !File) {
            $('#DivCbDropDown').hide();
            $('#DivShowLog').hide();
            $('#DivCheckBox').hide();
            totalToViewUpload = 0;
            IDFile = 0;
            viewModel.ViewUpload(false);
        } else {
            $('#DivCheckBox').show();
            $('#DivCbDropDown').show();
            $('#DivShowLog').show();

            FileSelected = _GlobalVars["FileRegion"].filter(file => { if (file.Name == File) return file });


            var count = 0;

            try {
                IDFile = FileSelected[0]["ID"];




                var listOfParams = {
                    IDFile: IDFile
                }

                if (FileSelected[0]["AskBU"]) {
                    BindJsonAlias("getBUfromFile", "cbBU", "UploadFile", listOfParams, function () {
                        viewModel.cbBU(_GlobalVars["cbBU"]);
                    });

                    count = count + 1;
                };
                if (FileSelected[0]["AskCountry"]) {

                    BindJsonAlias("getCountryfromFile", "cbCountry", "UploadFile", listOfParams, function () {
                        viewModel.cbCountry(_GlobalVars["cbCountry"]);
                    });

                    count = count + 1;
                };
                if (FileSelected[0]["AskMonthYear"]) {

                    BindJsonAlias("getMonthYear", "cbMonthYear", "UploadFile", listOfParams, function () {
                        viewModel.cbMonthYear(_GlobalVars["cbMonthYear"]);
                    });
                    count = count + 1;
                };
                if (FileSelected[0]["AskQuater"]) {

                    BindJsonAlias("getQuater", "cbQuater", "UploadFile", listOfParams, function () {
                        viewModel.cbQuater(_GlobalVars["cbQuater"]);
                    });
                    count = count + 1;
                };



                BindJsonAlias("getLogFile", "listLog", "UploadFile", listOfParams, function () {
                    viewModel.listLog(_GlobalVars["listLog"]);
                });

                BindJsonAlias("getLogFileOnly", "listLogByFile", "UploadFile", listOfParams, function () {
                    viewModel.listLogByFile(_GlobalVars["listLogByFile"]);
                });

                viewModel.AskBU(FileSelected[0]["AskBU"]);
                viewModel.AskCountry(FileSelected[0]["AskCountry"]);
                viewModel.AskMonthYear(FileSelected[0]["AskMonthYear"]);
                viewModel.AskQuater(FileSelected[0]["AskQuater"]);

                totalToViewUpload = count;

            }
            catch (err) {
                $('#DivCbDropDown').hide();
                $('#DivShowLog').hide();
                totalToViewUpload = 0;
                IDFile = 0;
                viewModel.ViewUpload(false);
            }
            viewUpload();
            //$('#FileDiv').show();
            //BindJsonAlias("getFile", "FileRegion", 'RegionFileAdmin', listOfParams, function () {
            //    viewModel.FileRegion(_GlobalVars["FileRegion"]);

            //    $('#DivCbFile').show();
            //});
        }
    },
    owner: viewModel
});

viewModel.BUSelected = ko.dependentObservable({
    read: viewModel.FileRegion,
    write: function (File) {
        BUSelected = File;
        viewUpload();

    },
    owner: viewModel
});

viewModel.CountrySelected = ko.dependentObservable({
    read: viewModel.FileRegion,
    write: function (File) {
        CountrySelected = File;
        viewUpload();

    },
    owner: viewModel
});

viewModel.MonthYearSelected = ko.dependentObservable({
    read: viewModel.FileRegion,
    write: function (File) {
        MonthYearSelected = File;
        viewUpload();

    },
    owner: viewModel
});

viewModel.QuaterSelected = ko.dependentObservable({
    read: viewModel.FileRegion,
    write: function (File) {
        QuaterSelected = File;
        viewUpload();

    },
    owner: viewModel
});

viewModel.gridLogModel = new KOGridModel({
    data: viewModel.listLog,
    columns: [
        { header: "Date Upload", data: "DateUpload" },
        { header: "Upload by", data: "SOEID" },
        { header: "Start Time", data: "StartTime" },
        { header: "End Time", data: "EndTime" },
        { header: "Month Upload", data: "MonthUpload" },
        { header: "Year Upload", data: "YearUpload" },
        { header: "Quarter Upload", data: "QuarterUpload" },
        { header: "Total Row", data: "TotalRow" },
        { header: "Country", data: "Country", visible: viewModel.AskCountry },
        { header: "BU", data: "BUID", visible: viewModel.AskBU },
        { header: "Month Year", data: "MonthYear", visible: viewModel.AskMonthYear },
        { header: "Quater", data: "Quater", visible: viewModel.AskQuater }
    ],
    pageSize: 4
});


viewModel.gridLogModelByFile = new KOGridModel({
    data: viewModel.listLogByFile,
    columns: [
        { header: "Upload by", data: "SOEID" },
        { header: "Month Upload", data: "MonthUpload" },
        { header: "Year Upload", data: "YearUpload" },
        { header: "Quarter Upload", data: "QuarterUpload" },
        { header: "Month Year", data: "MonthYear", visible: viewModel.AskMonthYear },
        { header: "Quater", data: "Quater", visible: viewModel.AskQuater }
    ],
    pageSize: 4
});


viewModel.incrementClickCounter = new function () {
    viewUpload();
}


$(document).ready(function () {

    BindJsonAlias("getRegion", "Region", "RegionFileAdmin", "", function () {
        viewModel.Region(_GlobalVars["Region"]);
        ko.applyBindings(viewModel);

        viewModel.ViewUpload(false);
    });

    XLSX.createJsXlsx({
        allColsMode: true,
        idElement: "js-xlsx-plugin",
        fileInput: {
            dropText: "Drop an Excel file here...",
            hideDrop: false,
            inputText: "... or click here to select a file"
        },
        onSuccess: function (excelSource) {

            //var excelSource = excelSource;

            var SOEID = _getSOEID();
            var FileID = viewModel.fileSelected()[0].ID;
            var Region = RegionSelected;

            var pCountrySelected = "0";
            var pMonthYearSelected = "0";
            var pQuaterSelected = "0";
            var pBUSelected = "0";


            if (viewModel.AskBU()) { pBUSelected = BUSelected; }
            if (viewModel.AskCountry()) { pCountrySelected = CountrySelected; }
            if (viewModel.AskMonthYear()) { pMonthYearSelected = MonthYearSelected; }
            if (viewModel.AskQuater()) { pQuaterSelected = QuaterSelected; }

            var letter = $('#buMultipleColumn').val();



            if (($("#MultipleBU").is(':checked') == true)) {


                var filterData = excelSource[Object.keys(excelSource)[0]].filter(qmData => { if (qmData[letter] != null) return qmData });

                var filterData = filterData.filter(qmData => { if (qmData[letter].match(/Business.*/) != null) return qmData });


                if (typeof filterData == 'undefined' || filterData.length == 0) {

                    document.getElementById('progressbar').style["width"] = "0px";

                    var totalPB = _GlobalVars["listBU"].length;
                    var sumPD = 100 / totalPB;
                    var ongoingPB = 0;


                    document.getElementById('divProgress').style.display = "block";
                    insertData2(0);

                    function insertData2(index) {

                        if (index <= totalPB - 1) {

                            bu = _GlobalVars["listBU"][index].BU;

                            filterData = excelSource[Object.keys(excelSource)[0]].filter(qmData => { if (qmData[letter] == bu) return qmData });
                            if (typeof filterData !== 'undefined' && filterData.length > 0) {
                                var listOfParams = {
                                    data: JSON.stringify(filterData),
                                    pCountrySelected: pCountrySelected,
                                    pMonthYearSelected: pMonthYearSelected,
                                    pQuaterSelected: pQuaterSelected,
                                    pBUSelected: bu,
                                    SOEID: SOEID,
                                    FileID: IDFile,
                                    Region: Region,
                                    letter: $('#buMultipleColumn').val(),
                                    keyword: "BU"
                                }

                                _callServer({
                                    loadingMsgType: "fullLoading",
                                    loadingMsg: "Loading data...",
                                    url: '/UploadFile/uploadFile',
                                    data: {
                                        'data': JSON.stringify(listOfParams)
                                    },
                                    type: "post",
                                    success: function (json) {
                                        // renderLog();

                                        ongoingPB = ongoingPB + sumPD;
                                        document.getElementById('progressbar').style["width"] = ongoingPB + "%";
                                        insertData2(index + 1);
                                    }
                                });
                            } else {
                                ongoingPB = ongoingPB + sumPD;
                                document.getElementById('progressbar').style["width"] = ongoingPB + "%";
                                insertData2(index + 1);
                            };



                        } else {
                            document.getElementById('divProgress').style.display = "none";
                            var listOfParams = {
                                IDFile: IDFile
                            }

                            BindJsonAlias("getLogFile", "listLog", "UploadFile", listOfParams, function () {
                                viewModel.listLog(_GlobalVars["listLog"]);
                            });

                            BindJsonAlias("getLogFileOnly", "listLogByFile", "UploadFile", listOfParams, function () {
                                viewModel.listLogByFile(_GlobalVars["listLogByFile"]);
                            });
                        }
                    }

                }
                else {
                    var filterData = excelSource[Object.keys(excelSource)[0]];

                    var columnscount = 0;

                    columnscount = Object.keys(filterData[0]).length + 1;


                    filterData = filterData.map(obj => ({
                        ID: obj.__rowNum__,
                        A: obj.A,
                        B: obj.B,
                        C: obj.C,
                        D: obj.D,
                        E: obj.E,
                        F: obj.F,
                        G: obj.G,
                        H: obj.H,
                        I: obj.I,
                        J: obj.J,
                        K: obj.K,
                        L: obj.L,
                        M: obj.M,
                        N: obj.N,
                        O: obj.O,
                        P: obj.P,
                        Q: obj.Q,
                        R: obj.R,
                        S: obj.S,
                        T: obj.T,
                        U: obj.U,
                        V: obj.V,
                        W: obj.W,
                        X: obj.X,
                        Y: obj.Y,
                        Z: obj.Z,
                        AA: obj.AA,
                        AB: obj.AB,
                        AC: obj.AC,
                        AD: obj.AD,
                        AE: obj.AE,
                        AF: obj.AF,
                        AG: obj.AG,
                        AH: obj.AH,
                        AI: obj.AI,
                        AJ: obj.AJ,
                        AK: obj.AK,
                        AL: obj.AL,
                        AM: obj.AM,
                        AN: obj.AN,
                        AO: obj.AO,
                        AP: obj.AP,
                        AQ: obj.AQ,
                        AR: obj.AR,
                        AS: obj.AS,
                        AT: obj.AT,
                        AU: obj.AU,
                        AV: obj.AV,
                        AW: obj.AW,
                        AX: obj.AX,
                        AY: obj.AY,
                        AZ: obj.AZ,
                        BA: obj.BA,
                        BB: obj.BB,
                        BC: obj.BC,
                        BD: obj.BD,
                        BE: obj.BE,
                        BF: obj.BF,
                        BG: obj.BG,
                        BH: obj.BH,
                        BI: obj.BI,
                        BJ: obj.BJ,
                        BK: obj.BK,
                        BL: obj.BL,
                        BM: obj.BM,
                        BN: obj.BN,
                        BO: obj.BO,
                        BP: obj.BP,
                        BQ: obj.BQ,
                        BR: obj.BR,
                        BS: obj.BS,
                        BT: obj.BT,
                        BU: obj.BU,
                        BV: obj.BV,
                        BW: obj.BW,
                        BX: obj.BX,
                        BY: obj.BY,
                        BZ: obj.BZ,
                        CA: obj.CA,
                        CB: obj.CB,
                        CC: obj.CC,
                        CD: obj.CD,
                        CE: obj.CE,
                        CF: obj.CF,
                        CG: obj.CG,
                        CH: obj.CH,
                        CI: obj.CI,
                        CJ: obj.CJ,
                        CK: obj.CK,
                        CL: obj.CL,
                        CM: obj.CM,
                        CN: obj.CN,
                        CO: obj.CO,
                        CP: obj.CP,
                        CQ: obj.CQ,
                        CR: obj.CR,
                        CS: obj.CS,
                        CT: obj.CT,
                        CU: obj.CU,
                        CV: obj.CV,
                        CW: obj.CW,
                        CX: obj.CX,
                        CY: obj.CY,
                        CZ: obj.CZ,
                        DA: obj.DA,
                        DB: obj.DB,
                        DC: obj.DC,
                        DD: obj.DD,
                        DE: obj.DE,
                        DF: obj.DF,
                        DG: obj.DG,
                        DH: obj.DH,
                        DI: obj.DI,
                        DJ: obj.DJ,
                        DK: obj.DK,
                        DL: obj.DL,
                        DM: obj.DM,
                        DN: obj.DN,
                        DO: obj.DO,
                        DP: obj.DP,
                        DQ: obj.DQ,
                        DR: obj.DR,
                        DS: obj.DS,
                        DT: obj.DT,
                        DU: obj.DU,
                        DV: obj.DV,
                        DW: obj.DW,
                        DX: obj.DX,
                        DY: obj.DY,
                        DZ: obj.DZ,
                        EA: obj.EA,
                        EB: obj.EB,
                        EC: obj.EC,
                        ED: obj.ED,
                        EE: obj.EE,
                        EF: obj.EF,
                        EG: obj.EG,
                        EH: obj.EH,
                        EI: obj.EI,
                        EJ: obj.EJ,
                        EK: obj.EK,
                        EL: obj.EL,
                        EM: obj.EM,
                        EN: obj.EN,
                        EO: obj.EO,
                        EP: obj.EP,
                        EQ: obj.EQ,
                        ER: obj.ER,
                        ES: obj.ES,
                        ET: obj.ET,
                        EU: obj.EU,
                        EV: obj.EV,
                        EW: obj.EW,
                        EX: obj.EX,
                        EY: obj.EY,
                        EZ: obj.EZ,
                        FA: obj.FA,
                        FB: obj.FB,
                        FC: obj.FC,
                        FD: obj.FD,
                        FE: obj.FE,
                        FF: obj.FF,
                        FG: obj.FG,
                        FH: obj.FH,
                        FI: obj.FI,
                        FJ: obj.FJ,
                        FK: obj.FK,
                        FL: obj.FL,
                        FM: obj.FM,
                        FN: obj.FN,
                        FO: obj.FO,
                        FP: obj.FP,
                        FQ: obj.FQ,
                        FR: obj.FR,
                        FS: obj.FS,
                        FT: obj.FT,
                        FU: obj.FU,
                        FV: obj.FV,
                        FW: obj.FW,
                        FX: obj.FX,
                        FY: obj.FY,
                        FZ: obj.FZ,
                        GA: obj.GA,
                        GB: obj.GB,
                        GC: obj.GC,
                        GD: obj.GD,
                        GE: obj.GE,
                        GF: obj.GF,
                        GG: obj.GG,
                        GH: obj.GH,
                        GI: obj.GI,
                        GJ: obj.GJ,
                        GK: obj.GK,
                        GL: obj.GL,
                        GM: obj.GM,
                        GN: obj.GN,
                        GO: obj.GO,
                        GP: obj.GP,
                        GQ: obj.GQ,
                        GR: obj.GR


                    }));

                    var filtered = filterData.filter(function (el) {
                        return el['A'] != null;
                    });


                    var businnesData = filtered.filter(qmData => { if (qmData[letter].match(/Business.*/) != null) return qmData });

                    var start = 0;
                    var end = 0


                    var totalPB = businnesData.length;
                    var sumPD = 100 / totalPB;
                    var ongoingPB = 0;

                    document.getElementById('divProgress').style.display = "block";
                    insertData(0);

                    function insertData(index) {


                        if (index <= totalPB - 1) {
                            start = businnesData[index].ID;
                            try {
                                end = businnesData[index + 1].ID;
                            }
                            catch (err) {
                                end = undefined;
                            }

                            if (typeof end === 'undefined' || !end) {
                                var data = filterData.filter(qmData => { if (qmData['ID'] >= start) return qmData });

                            } else {
                                var data = filterData.filter(qmData => { if (qmData['ID'] >= start && qmData['ID'] < end) return qmData });
                            }

                            var buselectedFromQuery = _GlobalVars["listBU"].filter(
                               qmData => {
                                   if (businnesData[index][$('#buMultipleColumn').val()].includes(qmData['BU'].toString()))
                                       return qmData
                               });

                            if (typeof buselectedFromQuery[0].BU === 'undefined' || !buselectedFromQuery[0].BU) {
                                var error = businnesData[index][$('#buMultipleColumn').val()];
                            }else{
                                var listOfParams = {
                                    data: JSON.stringify(data),
                                    pCountrySelected: pCountrySelected,
                                    pMonthYearSelected: pMonthYearSelected,
                                    pQuaterSelected: pQuaterSelected,
                                    pBUSelected: buselectedFromQuery[0].BU,
                                    SOEID: SOEID,
                                    FileID: IDFile,
                                    Region: Region,
                                    letter: $('#buMultipleColumn').val(),
                                    keyword: "BU"
                                }

                                _callServer({
                                    loadingMsgType: "fullLoading",
                                    loadingMsg: "Loading data...",
                                    url: '/UploadFile/uploadFile',
                                    data: {
                                        'data': JSON.stringify(listOfParams)
                                    },
                                    type: "post",
                                    success: function (json) {
                                        // renderLog();

                                        ongoingPB = ongoingPB + sumPD;
                                        document.getElementById('progressbar').style["width"] = ongoingPB + "%";
                                        insertData(index + 1);
                                    }
                                });
                            }

                            //var listOfParams = {
                            //    data: JSON.stringify(data),
                            //    pCountrySelected: pCountrySelected,
                            //    pMonthYearSelected: pMonthYearSelected,
                            //    pQuaterSelected: pQuaterSelected,
                            //    //pBUSelected: element.BU,
                            //    SOEID: SOEID,
                            //    FileID: IDFile,
                            //    Region: Region,
                            //    letter: $('#buMultipleColumn').val(),
                            //    keyword: "BU",
                            //    columnscount: columnscount

                            //}

                            //_callServer({
                            //    loadingMsgType: "fullLoading",
                            //    loadingMsg: "Loading data...",
                            //    url: '/UploadFile/uploadFileHeaderFix',
                            //    data: {
                            //        'data': JSON.stringify(listOfParams)
                            //    },
                            //    type: "post",
                            //    success: function (json) {
                            //        ongoingPB = ongoingPB + sumPD;
                            //        document.getElementById('progressbar').style["width"] = ongoingPB + "%";

                            //        insertData(index + 1);

                            //    }
                            //});
                        } else {
                            document.getElementById('divProgress').style.display = "none";

                            var listOfParams = {
                                IDFile: IDFile
                            }

                            BindJsonAlias("getLogFile", "listLog", "UploadFile", listOfParams, function () {
                                viewModel.listLog(_GlobalVars["listLog"]);
                            });

                            BindJsonAlias("getLogFileOnly", "listLogByFile", "UploadFile", listOfParams, function () {
                                viewModel.listLogByFile(_GlobalVars["listLogByFile"]);
                            });

                        }
                    }



                }
                

            } else {

                var listOfParams = {
                    data: JSON.stringify(excelSource[Object.keys(excelSource)[0]]),
                    pCountrySelected: pCountrySelected,
                    pMonthYearSelected: pMonthYearSelected,
                    pQuaterSelected: pQuaterSelected,
                    pBUSelected: pBUSelected,
                    SOEID: SOEID,
                    FileID: IDFile,
                    Region: Region,
                    letter: $('#buMultipleColumn').val(),
                    keyword: "BU"


                }

                _callServer({
                    loadingMsgType: "fullLoading",
                    loadingMsg: "Loading data...",
                    url: '/UploadFile/uploadFile',
                    data: {
                        'data': JSON.stringify(listOfParams)
                    },
                    type: "post",
                    success: function (json) {
                        // renderLog();

                        var listOfParams = {
                            IDFile: IDFile
                        }

                        BindJsonAlias("getLogFile", "listLog", "UploadFile", listOfParams, function () {
                            viewModel.listLog(_GlobalVars["listLog"]);
                        });

                        BindJsonAlias("getLogFileOnly", "listLogByFile", "UploadFile", listOfParams, function () {
                            viewModel.listLogByFile(_GlobalVars["listLogByFile"]);
                        });
                    }
                });
            }



        }
    });
    viewModel.ViewUpload(false);

    $('#DivCheckBox').hide();
    _hideMenu();
});


function viewUpload() {
    var count = 0;

    if (totalToViewUpload == 0) { $('#DivCheckBox').hide(); }

    if (typeof CountrySelected === 'undefined' || !CountrySelected) {
        if ($("#MultipleCountry").is(':checked') == true) { count = count + 1 };
    } else { count = count + 1; }
    if (typeof MonthYearSelected === 'undefined' || !MonthYearSelected) {
        if ($("#MultipleMonthYear").is(':checked') == true) { count = count + 1 }
    } else { count = count + 1; }
    if (typeof QuaterSelected === 'undefined' || !QuaterSelected) {
        if ($("#MultipleQuater").is(':checked') == true) { count = count + 1 }
    } else { count = count + 1; }
    if (typeof BUSelected === 'undefined' || !BUSelected) {
        if ($("#MultipleBU").is(':checked') == true) { count = count + 1 }
    } else { count = count + 1; }


    if (count >= totalToViewUpload) {
        viewModel.ViewUpload(true);
    } else {
        viewModel.ViewUpload(false);
    }

    if (typeof FileSelected === 'undefined' || !FileSelected) {
        viewModel.ViewUpload(false);
    }

}