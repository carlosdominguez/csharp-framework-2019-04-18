﻿var _GlobalVars = {
    Region: [],
    cbQuater: [],
    FilterEdidtQuality: [],
    FilterBU: [],
    EditCheck: [],
    SelectedBU: [],
    AccountFilter: [],
    CheckIDFilter: [],
    OperatorFilter: [],
    ValidationFilter: [],
    DetailEditCheck: [],
    DataSelected: [],
    IDeditCheckFilter: []
};



var RegionSelected;
var QuaterSelected;



var viewModel = {
    Region: ko.observable(),
    cbQuater: ko.observable(),
    cbYear: ko.observable(),
    EditCheck: ko.observable(),
    TotalFind: ko.observable(0),
    expandGridRight: ko.observableArray(),
    expandGridLeft: ko.observableArray(),
    BUDIVvisible: ko.observable(false),
    FilterResultVisible:ko.observable(false)
};


viewModel.QuaterSelected = ko.dependentObservable({
    read: viewModel.cbQuater,
    write: function (Quater) {
        QuaterSelected = Quater;

        if ((typeof QuaterSelected === 'undefined' || !QuaterSelected) || (typeof RegionSelected === 'undefined' || !RegionSelected)) {
         
            viewModel.BUDIVvisible(false);
            viewModel.FilterResultVisible(false);
           
        } else {

            var listOfParams = {
                Date: QuaterSelected,
                pID: RegionSelected
            }

            BindJsonAlias("procEC_GetFilterBU", "FilterBU", 'EditCheck', listOfParams, function () {
                renderComboBox('#cbBU', 'Select the BU', "FilterBU", 1, "SelectedBU");
            });
            BindJsonAlias("procEC_GetEditCheckData", "EditCheck", 'EditCheck', listOfParams, function () {
              
            });

            viewModel.BUDIVvisible(true);
        }
    },
    owner: viewModel
});

viewModel.regionSelect = ko.dependentObservable({
    read: viewModel.Region,
    write: function (Region) {
        RegionSelected = Region;

        if ((typeof QuaterSelected === 'undefined' || !QuaterSelected) || (typeof RegionSelected === 'undefined' || !RegionSelected)) {
            viewModel.BUDIVvisible(false);
            viewModel.FilterResultVisible(false);
        } else {
            var listOfParams = {
                Date: QuaterSelected,
                pID: RegionSelected
            }

            BindJsonAlias("procEC_GetFilterBU", "FilterBU", 'EditCheck', listOfParams, function () {
                renderComboBox('#cbBU', 'Select the BU', "FilterBU", 1, "SelectedBU");
            });

            BindJsonAlias("procEC_GetEditCheckData", "EditCheck", 'EditCheck', listOfParams, function () {
               
            });
            viewModel.BUDIVvisible(true);
        }
    },
    owner: viewModel
});

function expandGridRight(key, template, data) {
    this.key = key;
    this.template = ko.observable(template);
    this.data = data;
}

function expandGridLeft(key, template, data) {
    this.key = key;
    this.template = ko.observable(template);
    this.data = data;
}


$(document).ready(function () {
    //$('.usd_input').mask('00000.00');

    viewModel.expandGridRightFilter = function (name) {
        return ko.utils.arrayFirst(this.expandGridRight(), function (expandGridRight) {
            return (expandGridRight.name.replace(/\s+/, "").toLowerCase() === name.replace(/\s+/, "").toLowerCase());
        });
    };

    viewModel.expandGridLeftFilter = function (name) {
        return ko.utils.arrayFirst(this.expandGridLeft(), function (expandGridLeft) {
            return (expandGridLeft.name.replace(/\s+/, "").toLowerCase() === name.replace(/\s+/, "").toLowerCase());
        });
    };


    BindJsonAlias("getRegion", "Region", "RegionFileAdmin", "", function () {
        viewModel.Region(_GlobalVars["Region"]);
        ko.applyBindings(viewModel);
    });

    var listOfParams = {
        IDFile: 0
    }

    BindJsonAlias("getQuater", "cbQuater", "UploadFile", listOfParams, function () {
        viewModel.cbQuater(_GlobalVars["cbQuater"]);
    });
    var FilterEdidtQuality = [];

    FilterEdidtQuality[0] = 'Edit Check' ;
    FilterEdidtQuality[1] = 'Quality Check' ;

    renderComboBoxOnlyElement('#cbEditQuality', 'Choose..', FilterEdidtQuality, 1, "FilterEdidtQuality");

    

    _hideMenu();
});

function onComboBoxCheck()
{
    var AccountFilter;
    var CheckIDFilter;
    var OperatorFilter;
    var ValidationFilter = [];


    var anotherOne = _GlobalVars["SelectedBU"];

    if (_GlobalVars["SelectedBU"].length > 0) {

        viewModel.FilterResultVisible(true);

        var filteredArray = _GlobalVars["EditCheck"].filter(
       qmData => {
           if (anotherOne.includes(qmData['BU'].toString()))
               return qmData
       });

        viewModel.TotalFind(filteredArray.length);

        viewModel.EditCheck(filteredArray);
        _GlobalVars["DataSelected"] = filteredArray;
        nestedTable('#EditCheckTwoSource');


        CheckIDFilter = filteredArray.map(item => item.CheckID).filter((value, index, self) => self.indexOf(value) === index);

        renderComboBoxOnlyElement('#cbCheckID', 'Choose..', CheckIDFilter, 1, "CheckIDFilter");

        OperatorFilter = filteredArray.map(item => item.Operator).filter((value, index, self) => self.indexOf(value) === index);

        renderComboBoxOnlyElement('#cbOperator', 'Choose..', OperatorFilter, 1, "OperatorFilter");

        ValidationFilter[0] = 'Pass';
        ValidationFilter[1] = 'Fail';

        renderComboBoxOnlyElement('#cbValidation', 'Choose..', ValidationFilter, 1, "ValidationFilter");

        //cbAccount = 
        var listOfParams = {
            Date: QuaterSelected,
            pID: RegionSelected
        }

        BindJsonAlias("procEC_GetEditCheckDataDetail", "DetailEditCheck", 'EditCheck', listOfParams, function () {

            AccountFilter = _GlobalVars["DetailEditCheck"].map(item => item.Account).filter((value, index, self) => self.indexOf(value) === index);

            renderComboBoxOnlyElement('#cbAccount', 'Choose..', AccountFilter, 1, "AccountFilter");
        });

    } else
    {
        viewModel.FilterResultVisible(false);
    }

   
}

function filterEditCheck()
{
    var data = _GlobalVars["DataSelected"];

    //AccountFilter: [],
    //CheckIDFilter: [],
    //OperatorFilter: [],
    //ValidationFilter: [],
    //FilterEdidtQuality: [],

    //Account Filter

    if (_GlobalVars['AccountFilter'].length != 0) {
        _GlobalVars["IDeditCheckFilter"] = [];

        var AccountDetail = _GlobalVars["DetailEditCheck"].filter(
         qmData => {
             if (_GlobalVars['AccountFilter'].includes(qmData['Account'].toString()))
                 return qmData
         });

        var checksid = AccountDetail.map(item => item.CheckID).filter((value, index, self) => self.indexOf(value) === index);

     
        var newArr = [];

        for (var i = 0; i < checksid.length; i++) {
            _GlobalVars["IDeditCheckFilter"].push(checksid[i]);
        }


         data = data.filter(
         qmData => {
             if (_GlobalVars["IDeditCheckFilter"].includes(qmData['ID']))
                 return qmData
         });
    }

    //CheckID Filter
    
    if (_GlobalVars['CheckIDFilter'].length != 0){
         data = data.filter(
         qmData => {
             if (_GlobalVars['CheckIDFilter'].includes(qmData['CheckID'].toString()))
                 return qmData
         });
    }
    //Operator Filter
    if (_GlobalVars['OperatorFilter'].length != 0){
         data = data.filter(
        qmData => {
            if (_GlobalVars['OperatorFilter'].includes(qmData['Operator'].toString()))
                return qmData
        });
    }
    //Validation Filter

    

    if (_GlobalVars['ValidationFilter'].length != 0) {

        var ValidationArray = [];

        _GlobalVars['ValidationFilter'].forEach(function (element) {
            if (element == 'Pass')
            {
                ValidationArray.push('1');
            }
            if (element == 'Fail') {
                ValidationArray.push('0');
            }
        });

         data = data.filter(
        qmData => {
            if (ValidationArray.includes(qmData['Validation'].toString()))
                return qmData
        });
    }
    //Type Filter
    if (_GlobalVars['FilterEdidtQuality'].length != 0) {

        var ValidationType = [];

        _GlobalVars['FilterEdidtQuality'].forEach(function (element) {
            if (element == 'Edit Check') {
                ValidationType.push('E');
            }
            if (element == 'Quality Check') {
                ValidationType.push('Q');
            }
        });

        data = data.filter(
        qmData => {
            if (ValidationType.includes(qmData['Type'].toString()))
                return qmData
        });
    }
   

    viewModel.EditCheck(data);
    viewModel.TotalFind(data.length);
    nestedTable('#EditCheckTwoSource');
}



function runEditCheck()
{

    var itemsProcessed = 0;
    _GlobalVars["SelectedBU"].forEach(function (element) {



        var listOfParams = {
            Date: QuaterSelected,
            pID: RegionSelected,
            BU: element
        }

        BindJsonAlias("procEC_RunEditCheck", "EditCheck", 'EditCheck', listOfParams, function () {
            
        });
        itemsProcessed++;
        if (itemsProcessed === _GlobalVars["SelectedBU"].length) {
            onComboBoxCheck();
            nestedTable('#EditCheckTwoSource');
        }


    });



}



function nestedTable(tableName) {
    var $table = $(tableName);

    $table.find('.js-view-parents').on('click', function (e) {
        e.preventDefault();
        var $btn = $(e.target), $row = $btn.closest('tr'), $nextRow = $row.next('tr.expand-child');


        if ($nextRow.length) {
            $nextRow.toggle();
        } else {

            var IDS = $(this).context.id.replace('grid', '');
            var Grid = $(this).context.id.replace(',', '_') ;
            var GridRight = $(this).context.id.replace(',', '_') + 'Right';
            var GridLeft = $(this).context.id.replace(',', '_') + 'Left';

            var DataLeft = _GlobalVars["DetailEditCheck"].filter(qmData => { if (qmData.CheckID == IDS && qmData.IsRight == 0) return qmData });
            var DataRight = _GlobalVars["DetailEditCheck"].filter(qmData => { if (qmData.CheckID == IDS && qmData.IsRight == 1) return qmData });

            viewModel.expandGridRight.push({ name: GridRight, data: DataRight });
            viewModel.expandGridLeft.push({ name: GridLeft, data: DataLeft });

                newRow = '<tr class="expand-child" id="collapse' + Grid + '">' +
                          '<td colspan="6" style="vertical-align: top; border-top: 0; border-right: 0;">' +
                          '<table id="testingLookAndFill' + GridLeft + '"  style="width: 95%; ">' +
                            '<thead>' +
                            '<tr role="row">' +
                            '<th colspan="6" rowspan="1" style="background-color:#8B8B8B;color:white;">Left</th>' +
                            '</tr>' +
                            '<tr role="row">' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 3%;">Sign</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Account</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Description</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Ice Code</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Prior</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Value</th>' +
                            '</tr>' +
                            '</thead>' +
                            '<tbody data-bind="foreach: expandGridLeftFilter(&#39;' + GridLeft + '&#39;).data ">' +
                            '<tr role="row" class="odd">' +
                            '<td data-bind="text: IsSum == 1 ? &#39;+&#39; : &#39;-&#39;;" style="font-size: 41px;"></td>' +
                            '<td data-bind="text: Account" style="background-color:white; "></td>' +
                            '<td data-bind="text: Description" style="overflow: auto;white-space:nowrap;max-width: 40px;"></td>' +
                            '<td data-bind="text: IceCode" style="background-color:white; "></td>' +
                            '<td data-bind="text: Month" style="background-color:white; "></td>' +
                            '<td data-bind="text: Value" style="background-color:white; "></td>' +
                            '</tr>' +
                            '</tbody>' +
                          '</table>' +
                          '</td>' +
                          '<td colspan="6" style="vertical-align: top; border-top: 0; border-right: 0;">' +

                          '<table id="testingLookAndFill' + GridRight + '" style="width: 95%;  ">' +
                            '<thead>' +
                            '<tr role="row">' +
                            '<th colspan="6" rowspan="1" style="background-color:#8B8B8B;color:white;">Rigth</th>' +
                            '</tr>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 3%;">Sign</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Account</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Description</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Ice Code</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Prior</th>' +
                            '<th tabindex="0" rowspan="1" colspan="1" style="width: 5%;">Value</th>' +
                            '</tr>' +
                            '</thead>' +
                            '<tbody data-bind="foreach: expandGridRightFilter(&#39;' + GridRight + '&#39;).data ">' +
                            '<tr role="row" class="odd">' +
                            '<td data-bind="text: IsSum == 1 ? &#39;+&#39; : &#39;-&#39;;" style="font-size: 41px;"></td>' +
                            '<td data-bind="text: Account" style="background-color:white; "></td>' +
                            '<td data-bind="text: Description" style="overflow: auto;white-space:nowrap;max-width: 40px;"></td>' +
                            '<td data-bind="text: IceCode" style="background-color:white; "></td>' +
                            '<td data-bind="text: Month" style="background-color:white; "></td>' +
                            '<td data-bind="text: Value" style="background-color:white; "></td>' +
                            '</tr>' +
                            '</tbody>' +
                            '</table>' +
                          '</td>' +
                          '</tr>';
                $nextRow = $(newRow).insertAfter($row);
            /// $("#testingLookAndFill")[0] ASI FUNCIONA

     
                ko.applyBindings(viewModel, document.getElementById("testingLookAndFill" + GridLeft));
                ko.applyBindings(viewModel, document.getElementById("testingLookAndFill" + GridRight));

                //viewModel[Grid] = _GlobalVars[Grid];
            // viewModel.CompareTwoIdo = "";


        }
    });


}