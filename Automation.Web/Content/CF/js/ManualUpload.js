﻿var _GlobalVars = {
    Region: [],
    FileRegion: [],
    countryList: [],
    accountList: [],
    masterTable: [],
    comboboxBU: [],
    comboboxCountry: [],
    comboboxType: [],
    comboboxCFAccount: [],
    templateDownload: []
};
var MasterTableFilter;
var MasterDownloadFilter;


var RegionSelected;
var CountrySelected;
var QuaterSelected;
var YearSelected;
var ListTypeAccount;

var comboboxBUSelected;
var comboboxCountrySelected;
var comboboxTypeSelected;
var comboboxCFAccountSelected;

var viewModel = {
    Region: ko.observable(),
    FileRegion: ko.observable(),
    viewAddAccount: ko.observable(),
    cbQuater: ko.observable(),
    cbYear: ko.observable(),
    masterTable: ko.observableArray(),
    comboboxBU: ko.observableArray(),
    comboboxCountry: ko.observableArray(),
    comboboxType: ko.observableArray(),
    comboboxCFAccount: ko.observableArray(),
    showMonthDivision: ko.observable(false),
    templateDownload: ko.observableArray()
};

viewModel.comboboxBUSelected = ko.dependentObservable({
    read: viewModel.comboboxBU,
    write: function (item) {
        comboboxBUSelected = item;
        if (typeof comboboxBUSelected === 'undefined') {
            comboboxBUSelected = "0";
        }
    },
    owner: viewModel
});

viewModel.comboboxCountrySelected = ko.dependentObservable({
    read: viewModel.comboboxCountry,
    write: function (item) {
        comboboxCountrySelected = item;
        if (typeof comboboxCountrySelected === 'undefined') {
            comboboxCountrySelected = "0";
        }
    },
    owner: viewModel
});

viewModel.comboboxTypeSelected = ko.dependentObservable({
    read: viewModel.comboboxType,
    write: function (item) {
        comboboxTypeSelected = item;
        if (typeof comboboxTypeSelected === 'undefined') {
            comboboxTypeSelected = "0";
        }
    },
    owner: viewModel
});

viewModel.comboboxCFAccountSelected = ko.dependentObservable({
    read: viewModel.comboboxCFAccount,
    write: function (item) {
        comboboxCFAccountSelected = item;
        if (typeof comboboxCFAccountSelected === 'undefined') {
            comboboxCFAccountSelected = "0";
        }
    },
    owner: viewModel
});

viewModel.QuaterSelected = ko.dependentObservable({
    read: viewModel.FileRegion,
    write: function (Quater) {
        QuaterSelected = Quater;

        var listOfParams = {
            Region: RegionSelected,
            Quater: QuaterSelected
        }

        if ((typeof QuaterSelected === 'undefined' || !QuaterSelected) || (typeof RegionSelected === 'undefined' || !RegionSelected)) {
            viewModel.viewAddAccount(false);
        } else {
            viewModel.viewAddAccount(true);
            BindJsonAlias("getCF_MasterTableDetail", "masterTable", 'ManualUpload', listOfParams, function () {
                viewModel.masterTable(_GlobalVars["masterTable"]);
                MasterTableFilter = _GlobalVars["masterTable"];
            });

            BindJsonAlias("getTemplate", "templateDownload", 'ManualUpload', listOfParams, function () {
                viewModel.templateDownload(_GlobalVars["templateDownload"]);
                MasterDownloadFilter = _GlobalVars["templateDownload"];
            });

            renderComboBox(listOfParams);
        }
    },
    owner: viewModel
});

viewModel.regionSelect = ko.dependentObservable({
    read: viewModel.Region,
    write: function (Region) {
        RegionSelected = Region;

        var listOfParams = {
            Region: RegionSelected,
            Quater: QuaterSelected
        }

        if ((typeof QuaterSelected === 'undefined' || !QuaterSelected) || (typeof RegionSelected === 'undefined' || !RegionSelected)) {
            viewModel.viewAddAccount(false);
        } else {
            viewModel.viewAddAccount(true);
            BindJsonAlias("getCF_MasterTableDetail", "masterTable", 'ManualUpload', listOfParams, function () {
                viewModel.masterTable(_GlobalVars["masterTable"]);
                MasterTableFilter = _GlobalVars["masterTable"];
            });

            BindJsonAlias("getTemplate", "templateDownload", 'ManualUpload', listOfParams, function () {
                viewModel.templateDownload(_GlobalVars["templateDownload"]);
                MasterDownloadFilter = _GlobalVars["templateDownload"];
            });

            renderComboBox(listOfParams);
        }
    },
    owner: viewModel
});

viewModel.gridMasterTable = new KOGridModel({
    data: viewModel.masterTable,
    columns: [
         { header: "Region", data: "Region" },
         { header: "BU", data: "BU" },
         { header: "Country", data: "Country" },
         { header: "CF Account", data: "CFAccount" },
         { header: "Description", data: "Description" },
         { header: "Type Account", data: "Name" },
         { header: "Quater", data: "Quater" },
         { header: "Year", data: "Year" },
         { header: "Current Quater", template: 'divCurrent' }
    ],
    pageSize: 50
});

$(document).ready(function () {
    //$('.usd_input').mask('00000.00');

    BindJsonAlias("getRegion", "Region", "RegionFileAdmin", "", function () {
        viewModel.Region(_GlobalVars["Region"]);
        viewModel.viewAddAccount(false);
        ko.applyBindings(viewModel);
    });

    var listOfParams = {
        IDFile: 0
    }

    BindJsonAlias("getQuater", "cbQuater", "UploadFile", listOfParams, function () {
        viewModel.cbQuater(_GlobalVars["cbQuater"]);
    });

    XLSX.createJsXlsx({
        allColsMode: true,
        idElement: "js-xlsx-plugin",
        fileInput: {
            dropText: "Drop an Excel file here...",
            hideDrop: false,
            inputText: "... or click here to select a file"
        },
        onSuccess: function (jsonData) {

            var excelSource = jsonData;

            var pQuaterSelected = QuaterSelected;

            var listOfParams = {
                data: JSON.stringify(excelSource[Object.keys(jsonData)[0]]),
                pQuaterSelected: pQuaterSelected
            }

            _callServer({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading data...",
                url: '/ManualUpload/uploadManualUpload',
                data: {
                    'data': JSON.stringify(listOfParams)
                },
                type: "post",
                success: function (json) {
                    var listOfParams = {
                        Region: RegionSelected,
                        Quater: QuaterSelected
                    }

                    BindJsonAlias("getCF_MasterTableDetail", "masterTable", 'ManualUpload', listOfParams, function () {
                        viewModel.masterTable(_GlobalVars["masterTable"]);
                        MasterTableFilter = _GlobalVars["masterTable"];
                    });

                    BindJsonAlias("getTemplate", "templateDownload", 'ManualUpload', listOfParams, function () {
                        viewModel.templateDownload(_GlobalVars["templateDownload"]);
                        MasterDownloadFilter = _GlobalVars["templateDownload"];
                    });
                }
            });
        }
    });

    _hideMenu();
});

function setTwoNumberDecimal(event) {
    this.value = parseFloat(this.value).toFixed(2);
}


function renderComboBox(listOfParams) {

    BindJsonAlias("getCF_comboBoxBU", "comboboxBU", 'MasterTableCF', listOfParams, function () {
        viewModel.comboboxBU(_GlobalVars["comboboxBU"]);
    });

    BindJsonAlias("getCF_comboBoxCountry", "comboboxCountry", 'MasterTableCF', listOfParams, function () {
        viewModel.comboboxCountry(_GlobalVars["comboboxCountry"]);
    });

    BindJsonAlias("getCF_comboBoxType", "comboboxType", 'MasterTableCF', listOfParams, function () {
        viewModel.comboboxType(_GlobalVars["comboboxType"]);
    });

    BindJsonAlias("getCF_comboBoxCFAccount", "comboboxCFAccount", 'MasterTableCF', listOfParams, function () {
        viewModel.comboboxCFAccount(_GlobalVars["comboboxCFAccount"]);
    });
}

function filterMasterTable() {

    MasterTableFilter = _GlobalVars["masterTable"];
    MasterDownloadFilter = _GlobalVars["templateDownload"];
    if (comboboxBUSelected != '0') {
        MasterTableFilter = MasterTableFilter.filter(function (el) { return el.BU == comboboxBUSelected; })

        MasterDownloadFilter = MasterDownloadFilter.filter(function (el) { return el.BU == comboboxBUSelected; })
    }
    if (comboboxCountrySelected != '0') {
        MasterTableFilter = MasterTableFilter.filter(function (el) { return el.countryID == comboboxCountrySelected; })

        MasterDownloadFilter = MasterDownloadFilter.filter(function (el) { return el.countryID == comboboxCountrySelected; })
    }
    if (comboboxTypeSelected != '0') {
        MasterTableFilter = MasterTableFilter.filter(function (el) { return el.typeID == comboboxTypeSelected; })

        MasterDownloadFilter = MasterDownloadFilter.filter(function (el) { return el.typeID == comboboxTypeSelected; })
    }
    if (comboboxCFAccountSelected != '0') {
        MasterTableFilter = MasterTableFilter.filter(function (el) { return el.cfAccountID == comboboxCFAccountSelected; })

        MasterDownloadFilter = MasterDownloadFilter.filter(function (el) { return el.cfAccountID == comboboxCFAccountSelected; })
    }


    viewModel.masterTable(MasterTableFilter);


}

function clearMasterTable() {
    viewModel.masterTable(_GlobalVars["masterTable"]);

}

function downloadData() {
    var download = MasterDownloadFilter;

    $.each(download, function (index, value) {

        delete download[index]["cfAccountID"];
        delete download[index]["typeID"];
        delete download[index]["countryID"];
        delete download[index]["ID"];
    });
    

    _downloadExcelJson({
        jsonData: JSON.stringify(download),
        filename: "TemplateManualUpload",
        success: {
            msg: "Please wait, generating report..."
        }
    });

}

