﻿$(document).ready(function () {
    _hideMenu();

    viewSelectedGrid('#OpenPosition');

    $("#jqxExpanderHeadCount").jqxExpander({ width: '100%', height: 'auto', expanded: true, showArrow: true, theme: 'blackberryGreen' });
    $("#jqxExpanderIncosistence").jqxExpander({ width: '100%', height: 'auto', expanded: true, showArrow: true, theme: 'blackberryRed' });
    $("#jqxExpanderFroNoReq").jqxExpander({ width: '100%', height: 'auto', expanded: true, showArrow: true, theme: 'blackberryOrange' });
    $("#jqxExpanderOpen").jqxExpander({ width: '100%', height: 'auto', expanded: true, showArrow: true, theme: 'blackberry' });

    getTotalOpen();

    fntLoadOpen();
});

function viewSelectedGrid(DIV) {
    $('#OpenPosition').hide();
    $('#FroNoReq').hide();
    $('#GDWincosistence').hide();
    $('#Headcount').hide();

    $(DIV).show();

    getTotalOpen();
    getTotalFroNoReq();
    getTotalGDW();
    getTotalHeadCount();
}

function fntLoadOpen() {
    viewSelectedGrid('#OpenPosition');
    GetListOpen();
    $('#UserInfo').hide();
}
function fntLoadFroNoReq() {
    viewSelectedGrid('#FroNoReq');
    GetListFroNoReq();
    $('#UserInfo').hide();
}
function fntLoadGDW() {
    viewSelectedGrid('#GDWincosistence');
    getTotalGDW();
    GetListGDW();
    $('#UserInfo').hide();
}
function fntLoadHeadCount() {
    viewSelectedGrid('#Headcount');
    GetListHeadCount();
    $('#UserInfo').hide();
}



function GetListOpen() {
    

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/HeadCount/renderjqxGridOpen',
        type: "post",
        success: function (json) {

            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                            { name: 'ID', type: 'string' }
                          , { name: 'GOC', type: 'string' }
                          , { name: 'Requisition', type: 'string' }
                          , { name: 'RequisitionTitle', type: 'string' }
                          , { name: 'PersonReplacing', type: 'string' }
                          , { name: 'PersonReplacingSOEID', type: 'string' }
                          , { name: 'StartDate', type: 'string' }
                          , { name: 'GEID', type: 'string' }
                          , { name: 'SOEID', type: 'string' }
                          , { name: 'CandidatesName', type: 'string' }
                          , { name: 'CandidateID', type: 'string' }
                          , { name: 'InternalExternal', type: 'string' }
                          , { name: 'CSC', type: 'string' }
                          , { name: 'FinanceJustification', type: 'string' }
                          , { name: 'EmployeeID', type: 'string' }
                          , { name: 'LastRole', type: 'string' }
                          , { name: 'LastRoleDriver', type: 'string' }
                          , { name: 'NewRole', type: 'string' }
                          , { name: 'NewRoleDriver', type: 'string' }
                          , { name: 'RequisitionID', type: 'string' }
                          , { name: 'Automatic', type: 'string' }
                          , { name: 'MOUNONMOU', type: 'string' }
                          , { name: 'Complete', type: 'string' }
                          , { name: 'RoleID', type: 'string' }
                          , { name: 'LastRoleID', type: 'string' }
                          , { name: 'LastDriver', type: 'string' }
                          , { name: 'NewDriver', type: 'string' }
                          , { name: 'GOCName', type: 'string' }
                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            var imgSelectReq = function (row, datafield, value) {

                var rowData = $('#jqxGridOpen').jqxGrid('getrowdata', row);

                rowData.ID;

                return '<i class="fa fa-search" style=" font-size: 23px; color: #4286f4; margin-left:4px; margin-top:4px;" onClick="reviewEmployee(' + rowData.ID + ')"></i>';
            }


            var imgCreateNonMOU = function (row, datafield, value) {

                var rowData = $('#jqxGridOpen').jqxGrid('getrowdata', row);

                rowData.ID;

                return '<i class="fa fa-plus-square" style=" font-size: 23px; color: #4286f4; margin-left:4px; margin-top:4px;" onClick="createNonMOUToRole(' + rowData.ID + ')"></i>';
            }



            $('#jqxGridOpen').jqxGrid(
           {
               source: dataAdapter,
               theme: 'energyblue',
               width: '99%',
               enabletooltips: true,
               editable: true,
               pagesize: 20,
               pageable: true,
               filterable: true,
               showfilterrow: true,
               columnsresize: true,
               selectionmode: 'singlecell',
               enablebrowserselection: true,
               columns: [
                             { text: 'Center', dataField: 'CSC', filtertype: 'input', editable: false, width: '10%', pinned: false }
                          ,  { text: 'GOC', dataField: 'GOCName', filtertype: 'input', editable: false, width: '10%', pinned: false }
                          , { text: 'Requisition', dataField: 'Requisition', filtertype: 'input', editable: false, width: '10%', pinned: false }
                          , { text: 'StartDate', dataField: 'StartDate', filtertype: 'date', columntype: 'datetimeinput', width: '10%', align: 'right', cellsalign: 'right', cellsformat: 'd' }
                          , { text: 'MOUNONMOU', dataField: 'MOUNONMOU', filtertype: 'input', editable: false, width: '10%', pinned: false }
                          , { text: 'Create Non MOU', width: '3%', editable: false, cellsrenderer: imgCreateNonMOU, pinned: false }
                          , { text: 'GEID', dataField: 'GEID', filtertype: 'input', editable: false, width: '10%', pinned: false }
                          , { text: 'SOEID', dataField: 'SOEID', filtertype: 'input', editable: false, width: '10%', pinned: false }
                          , { text: 'CandidatesName', dataField: 'CandidatesName', filtertype: 'input', editable: false, width: '10%', pinned: false }
                          , {
                              text: 'RoleID', dataField: 'RoleID', width: '10%', columntype: 'combobox',
                              initeditor: function (row, column, editor) {
                                  // assign a new data source to the combobox.
                                  var data = $('#jqxGridOpen').jqxGrid('getrowdata', row);

                                  var listOfParams = {
                                      goc: data["GOC"]
                                  }

                                  _callServer({
                                      loadingMsgType: "fullLoading",
                                      loadingMsg: "Loading data...",
                                      data: {
                                          'data': JSON.stringify(listOfParams)
                                      },
                                      url: '/HeadCount/getRolesByGOC',
                                      type: "post",
                                      success: function (json) {
                                          
                                          var list = jQuery.parseJSON(json);
                                          editor.jqxComboBox({ autoDropDownHeight: true, source: list, promptText: "Please Choose:" });

                                      }
                                  });

                                  
                              },
                              // update the editor's value before saving it.
                              cellvaluechanging: function (row, column, columntype, oldvalue, newvalue) {
                                  // return the old value, if the new value is empty.
                                  if (newvalue == "") return oldvalue;
                              }
                          },
                          , {
                              text: 'NewDriver', dataField: 'NewDriver', width: '10%', columntype: 'combobox',
                              createeditor: function (row, column, editor) {
                                  // assign a new data source to the combobox.

                                  var list2 = [ 'New Work MOU',
                                                'Transfer In MOU',
                                                'Project MOU',
                                                'Investments',
                                                'Divestiture',
                                                'Right Placement In MOU',
                                                'Replacements',
                                                'Approved Resources',
                                                'Employee Type Reclass',
                                                'Internal Movements',
                                                'Internal Restructure',
                                                'Work Absorption MOU'];
                                          editor.jqxComboBox({ autoDropDownHeight: true, source: list2, promptText: "Please Choose:" });


                              },
                              // update the editor's value before saving it.
                              cellvaluechanging: function (row, column, columntype, oldvalue, newvalue) {
                                  // return the old value, if the new value is empty.
                                  if (newvalue == "") return oldvalue;
                              }
                          },
                          , { text: 'PersonReplacing', dataField: 'PersonReplacing', filtertype: 'input', editable: false, width: '10%', pinned: false }
                          , { text: 'PersonReplacingSOEID', dataField: 'PersonReplacingSOEID', filtertype: 'input', editable: false, width: '10%', pinned: false }
                          , { text: 'LastRoleID', dataField: 'LastRoleID', filtertype: 'input', editable: false, width: '10%', pinned: false }
                          , {
                              text: 'LastDriver', dataField: 'LastDriver', width: '10%', columntype: 'combobox',
                              createeditor: function (row, column, editor) {
                                  // assign a new data source to the combobox.

                                  _callServer({
                                      loadingMsgType: "fullLoading",
                                      loadingMsg: "Loading data...",
                                      url: '/HeadCount/getDriverList',
                                      type: "post",
                                      success: function (json) {

                                          var list2 = [
                                               'Transfer out MOU',
                                               'Project MOU',
                                               'Investments',
                                               'Reengineering',
                                               'Right Placement Out',
                                               'Reduction Placement Out',
                                               'Divestiture',
                                               'Replacements',
                                               'Attrition',
                                               'Mobility Out (Leave FRO)',
                                               'Employee Type Reclass',
                                               'Internal Movements',
                                               'Internal Restructure'];
                                          editor.jqxComboBox({ autoDropDownHeight: true, source: list2, promptText: "Please Choose:" });

                                      }
                                  });


                              },
                              // update the editor's value before saving it.
                              cellvaluechanging: function (row, column, columntype, oldvalue, newvalue) {
                                  // return the old value, if the new value is empty.
                                  if (newvalue == "") return oldvalue;
                              }
                          },
                          , { text: 'InternalExternal', dataField: 'InternalExternal', filtertype: 'input', editable: false, width: '10%', pinned: false }
                          
                          , { text: 'FinanceJustification', dataField: 'FinanceJustification', filtertype: 'input', editable: false, width: '10%', pinned: false }
                          , { text: 'Automatic', dataField: 'Automatic', filtertype: 'input', editable: false, width: '10%', pinned: false }
                          
                          
                          
                          
                         



               ]

           });

        }
    });
   
}

function GetListFroNoReq() {
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/HeadCount/GetListFroNoReq',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                          { name: 'ID', type: 'string' },
                          { name: 'SOEID', type: 'string' },
                      { name: 'FirstName', type: 'string' },
                      { name: 'LastName', type: 'string' },
                      { name: 'Goc', type: 'string' },
                      { name: 'HiringDate', type: 'string' },
	                  { name: 'AttritionDate', type: 'string' },
                      { name: 'BLDG_ZONE', type: 'string' },
                      { name: 'REGION', type: 'string' },
                      { name: 'EMPL_CLASS', type: 'string' },
	                  { name: 'Description', type: 'string' },
                      { name: 'FTEEligible', type: 'string' },
                      { name: 'JOBTITLE', type: 'string' },
                      { name: 'DirectManagerID', type: 'string' },
                      { name: 'DirectManager_GEID', type: 'string' },
                      { name: 'SupervisorID', type: 'string' },
                      { name: 'Supervisor_GEID', type: 'string' },
                      { name: 'ManagedSegmentID', type: 'string' },
                      { name: 'Managed_Segment', type: 'string' },
                      { name: 'Grade', type: 'string' }

                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            var imgSelectRole = function (row, datafield, value) {

                var rowData = $('#jqxGridFroNoReq').jqxGrid('getrowdata', row);

                return '<i class="fa fa-search" style=" font-size: 17px; color: #4286f4; margin-top: 8%; margin-left: 10%; cursor: pointer;" onClick="ReviewFroUser(' + row + ')">Edit</i>';
            }


            $('#jqxGridFroNoReq').jqxGrid(
           {
               width: '100%',
               columnsresize: false,
               pagesize: 10,
               pageable: true,
               autoheight: true,
               filterable: true,
               pagermode: 'default',
               theme: 'energyblue',
               source: dataAdapter,
               rowdetails: true,
               showfilterrow: true,
               enablebrowserselection: true,
               columns: [
                      { text: '', width: '6%', editable: false, cellsrenderer: imgSelectRole, pinned: true },
                      { text: 'SOEID', dataField: 'SOEID', filtertype: 'input', width: '10%', editable: false },
                      { text: 'FirstName', dataField: 'FirstName', filtertype: 'input', width: '10%', editable: false },
                      { text: 'LastName', dataField: 'LastName', filtertype: 'input', width: '10%', editable: false },
                      { text: 'Goc', dataField: 'Goc', filtertype: 'checkedlist', width: '10%', editable: false },
                      { text: 'HiringDate', dataField: 'HiringDate', filtertype: 'checkedlist', width: '10%', editable: false },
	                  { text: 'AttritionDate', dataField: 'AttritionDate', filtertype: 'checkedlist', width: '10%', editable: false },
                      { text: 'BLDG_ZONE', dataField: 'BLDG_ZONE', filtertype: 'checkedlist', width: '10%', editable: false },
                      { text: 'REGION', dataField: 'REGION', filtertype: 'checkedlist', width: '10%', editable: false },
                      { text: 'EMPL_CLASS', dataField: 'EMPL_CLASS', filtertype: 'checkedlist', width: '10%', editable: false },
	                  { text: 'Description', dataField: 'Description', filtertype: 'checkedlist', width: '10%', editable: false },
                      { text: 'FTEEligible', dataField: 'FTEEligible', filtertype: 'checkedlist', width: '10%', editable: false },
                      { text: 'JOBTITLE', dataField: 'JOBTITLE', filtertype: 'input', width: '10%', editable: false },
                      { text: 'DirectManagerID', dataField: 'DirectManagerID', filtertype: 'checkedlist', width: '10%', editable: false },
                      { text: 'DirectManager_GEID', dataField: 'DirectManager_GEID', filtertype: 'checkedlist', width: '10%', editable: false },
                      { text: 'SupervisorID', dataField: 'SupervisorID', filtertype: 'checkedlist', width: '10%', editable: false },
                      { text: 'Supervisor_GEID', dataField: 'Supervisor_GEID', filtertype: 'checkedlist', width: '10%', editable: false },
                      { text: 'ManagedSegmentID', dataField: 'ManagedSegmentID', filtertype: 'checkedlist', width: '10%', editable: false },
                      { text: 'Managed_Segment', dataField: 'Managed_Segment', filtertype: 'checkedlist', width: '10%', editable: false },
                      { text: 'Grade', dataField: 'Grade', filtertype: 'checkedlist', width: '10%', editable: false },

               ]

           });

        }
    });
    
}

function ReviewFroUser(row)
{
    var rowData = $('#jqxGridFroNoReq').jqxGrid('getrowdata', row);

    $('#UserInfo').show();


}

function GetListGDW() {

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/HeadCount/getChangesInEmployee',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                            { name: 'ID', type: 'string' },
                          , { name: 'SOEID', type: 'string' },
                          , { name: 'NewValue', type: 'string' },
                          , { name: 'OldValue', type: 'string' },
                          , { name: 'DateChange', type: 'string' },
                          , { name: 'isCheck', type: 'string' },
                          , { name: 'FirstName', type: 'string' },
                          , { name: 'LastName', type: 'string' },
                          , { name: 'HiringDate', type: 'string' },
                          , { name: 'GOC', type: 'string' },
                          , { name: 'REGION', type: 'string' },
                          , { name: 'WORK_COUNTRY', type: 'string' },
                          , { name: 'ClassDescription', type: 'string' },
                          , { name: 'Status', type: 'string' }

                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            $('#jqxGDWInconsistence').jqxGrid(
           {
               width: '100%',
               columnsresize: true,
               pagesize: 10,
               pageable: true,
               autoheight: true,
               filterable: true,
               pagermode: 'default',
               theme: 'energyblue',
               source: dataAdapter,
               rowdetails: true,
               showfilterrow: true,
               selectionmode: 'singlecell',
               enablebrowserselection: true,
               // rowsheight: 35,
               selectionmode: 'checkbox',
               columns: [
                          { text: 'SOEID', dataField: 'SOEID', filtertype: 'checkedlist', width: '10%', editable: false },
                          { text: 'First Name', dataField: 'FirstName', filtertype: 'checkedlist', width: '10%', editable: false },
                          { text: 'Last Name', dataField: 'LastName', filtertype: 'checkedlist', width: '10%', editable: false },
                          { text: 'Status', dataField: 'Status', filtertype: 'checkedlist', width: '10%', editable: false },
                          { text: 'New Value', dataField: 'NewValue', filtertype: 'checkedlist', width: '10%', editable: false },
                          { text: 'Old Value', dataField: 'OldValue', filtertype: 'checkedlist', width: '10%', editable: false },
                          { text: 'Date Change', dataField: 'DateChange', filtertype: 'checkedlist', width: '20%', editable: false },
                          { text: 'is Check', dataField: 'isCheck', filtertype: 'checkedlist', width: '10%', editable: false },
                          { text: 'Hiring Date', dataField: 'HiringDate', filtertype: 'checkedlist', width: '10%', editable: false },
                          { text: 'GOC', dataField: 'GOC', filtertype: 'checkedlist', width: '10%', editable: false },
                          { text: 'REGION', dataField: 'REGION', filtertype: 'checkedlist', width: '10%', editable: false },
                          { text: 'WORK COUNTRY', dataField: 'WORK_COUNTRY', filtertype: 'checkedlist', width: '10%', editable: false },
                          { text: 'Class Description', dataField: 'ClassDescription', filtertype: 'checkedlist', width: '10%', editable: false }
                          
               ]

           });

        }
    });
}

function GetListHeadCount() {

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/HeadCount/GetHeadCount',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                            {name:'GEID',type:'string'},
                            {name:'SOEID',type:'string'},
                            {name:'RITSID',type:'string'},
                            {name:'LastName',type:'string'},
                            {name:'FirstName',type:'string'},
                            {name:'NAME',type:'string'},
                            {name:'EMAIL_ADDR',type:'string'},
                            {name:'LOCATION',type:'string'},
                            {name:'WORK_ADDRESS1',type:'string'},
                            {name:'WORK_ADDRESS3',type:'string'},
                            {name:'WORK_CITY',type:'string'},
                            {name:'WORK_STATE',type:'string'},
                            {name:'WORK_POSTAL',type:'string'},
                            {name:'WORK_COUNTRY',type:'string'},
                            {name:'CNTRY_DESCR',type:'string'},
                            {name:'WORK_BUILDING',type:'string'},
                            {name:'REGION',type:'string'},
                            {name:'RPT_REGION',type:'string'},
                            {name:'GOC',type:'string'},
                            {name:'EMPL_STATUS',type:'string'},
                            {name:'TERMINATION_DT',type:'string'},
                            {name:'EMPL_CLASS',type:'string'},
                            {name:'JOB_FAMILY',type:'string'},
                            {name:'JOBFAM_NAME',type:'string'},
                            {name:'JOB_FUNCTION',type:'string'},
                            {name:'JOBFUNC_NAME',type:'string'},
                            {name:'JOBCODE',type:'string'},
                            {name:'JOBTITLE',type:'string'},
                            {name:'OFFCR_TITLE_CD',type:'string'},
                            {name:'OFFCR_TTL_DESC',type:'string'},
                            {name:'DirectManagerID',type:'string'},
                            {name:'DirectManager_GEID',type:'string'},
                            {name:'SupervisorID',type:'string'},
                            {name:'Supervisor_GEID',type:'string'},
                            {name:'ManagedSegmentID',type:'string'},
                            {name:'Managed_Segment',type:'string'},
                            {name:'Grade',type:'string'},
                            {name:'HireDate',type:'string'},
                            {name:'TransferDate',type:'string'},
                            {name:'ClassDescription',type:'string'}

                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);



            $('#jqxGridHC').jqxGrid(
           {
               width: '100%',
               columnsresize: false,
               pagesize: 10,
               pageable: true,
               autoheight: true,
               filterable: true,
               pagermode: 'default',
               theme: 'energyblue',
               source: dataAdapter,
               rowdetails: true,
               showfilterrow: true,
               enablebrowserselection: true,
               columns: [
                           { text: 'SOEID', dataField: 'SOEID', filtertype: 'input', width: '10%', editable: false },
                           { text: 'First Name', dataField: 'FirstName', filtertype: 'input', width: '10%', editable: false },
                           { text: 'Last Name', dataField: 'LastName', filtertype: 'input', width: '10%', editable: false },
                           { text: 'Region', dataField: 'REGION', filtertype: 'checkedlist', width: '10%', editable: false },
                           { text: 'Location', dataField: 'WORK_CITY', filtertype: 'checkedlist', width: '10%', editable: false },
                           { text: 'GOC', dataField: 'GOC', filtertype: 'checkedlist', width: '10%', editable: false },
                           { text: 'Classification', dataField: 'ClassDescription', filtertype: 'checkedlist', width: '10%', editable: false },
                           { text: 'Job Name', dataField: 'JOBFUNC_NAME', filtertype: 'checkedlist', width: '10%', editable: false },
                           { text: 'Managed Segment', dataField: 'Managed_Segment', filtertype: 'checkedlist', width: '10%', editable: false }
               ]

           });

        }
    });
}

//box total roles
function getTotalOpen() {

    _callServer({
        loadingMsg: "", async: false,
        url: '/HeadCount/getTotalOpen',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            $('#totalOpenPosition').text(response);
        }
    });

}
//box pending review
function getTotalFroNoReq() {
    _callServer({
        loadingMsg: "", async: false,
        url: '/HeadCount/getTotalFroNoReq',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            $('#totalWithoutReq').text(response);
        }
    });

}
//box total in mou
function getTotalGDW() {
    _callServer({
        loadingMsg: "", async: false,
        url: '/HeadCount/getTotalGDW',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            $('#totalGDW').text(response);
        }
    });

}
//boc total in non mou
function getTotalHeadCount() {
    _callServer({
        loadingMsg: "", async: false,
        url: '/HeadCount/getTotalHeadCount',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            $('#totalHeadcount').text(response);
        }
    });

}

function reviewEmployee(EmployeeID)
{



}

function applyPendingForStart() {
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/HeadCount/applyPendingForStart',
        data: {
            'data': JSON.stringify($('#jqxGridOpen').jqxGrid('getdisplayrows'))
        },
        type: "post",
        success: function (json) {
            GetListOpen();
            getTotalOpen();
        }
    });

}


function createNonMOUToRole(ID)
{
    


}