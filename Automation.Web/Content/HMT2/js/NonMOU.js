﻿

$(document).ready(function () {
    _hideMenu();
    Request_tab();

    //  renderFunctionManagedSegment();

    getListDraft();
    //  getListQueue();


    var myVar = setInterval(myTimer, 10000);

    function myTimer() {
        getTotals();
        getAttachments();
    }

    $("#popUpApprovalRequest").jqxWindow({
        width: 1100, resizable: false, isModal: true, autoOpen: false, cancelButton: $("#Cancel"), modalOpacity: 0.01
    });


    $("#popUpLiveStage").jqxWindow({
        width: 1100, resizable: false, isModal: true, autoOpen: false, cancelButton: $("#Cancel"), modalOpacity: 0.01
    });

    getTotals();
});

function CreateHTMLaddRequest() {

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/insertNewRequestDraft',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);
            
            $.each((response), function (index, element) {
                request_SubmitQueQue(element.ID);
            });
        }
    });

}

function renderBudgetGoc() {

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getGocList',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            result = jQuery.parseJSON(response);

            var gocs = [];
            $.each(result, function (i, obj) {
                gocs.push([obj.GOCName]);
            });


            $("#jqxGridBudgetGOC").jqxComboBox({ source: gocs, multiSelect: true, width: 350, height: 25 });
            $("#jqxGridChargeOut").jqxComboBox({ source: gocs, multiSelect: true, width: 350, height: 25 });


        }
    });

}

function closeNotifications() {

    $("#jqxSaveDiscovery").jqxNotification("closeLast");
    $("#WarnjqxSaveDiscovery").jqxNotification("closeLast");

    $("#jqxErrorCharter").jqxNotification("closeLast");
    $("#WarnjqxErrorCharter").jqxNotification("closeLast");

    $("#jqxErrorMilestone").jqxNotification("closeLast");
    $("#WarnjqxErrorMilestone").jqxNotification("closeLast");

    $("#jqxErrorFinancial").jqxNotification("closeLast");
    $("#WarnjqxErrorFinancial").jqxNotification("closeLast");

    $("#jqxErrorApproval").jqxNotification("closeLast");
    $("#WarnjqxErrorApproval").jqxNotification("closeLast");

    $("#errorAddOther").jqxNotification("closeLast");
}

function addMigrationsToRequest() {
    var FTE = 0;
    var BudgetGoc = '';
    var ChargeOut = ''
    var FunctionManagedRow = 0;
    var RequestID = $('#hdfRequestID').val();

    var bGOC = $('#jqxGridBudgetGOC').jqxComboBox('getSelectedItems');
    $.each(bGOC, function (index, element) {
        if (index == 0) {
            BudgetGoc = element.value.split(" ")[0];
        } else {
            BudgetGoc = BudgetGoc + ',' + element.value.split(" ")[0];
        }
    });

    var cGOC = $('#jqxGridChargeOut').jqxComboBox('getSelectedItems');
    $.each(cGOC, function (index, element) {
        if (index == 0) {
            ChargeOut = element.value.split(" ")[0];
        } else {
            ChargeOut = ChargeOut + ',' + element.value.split(" ")[0];
        }
    });

    FTE = $('#txtFTE').val();


    if (($("#jqxComboBoxFunctionManaged").val()) == "") {
        $('#lblMigError').text('Please select one Function/Managed Segment.');
        closeNotifications();
    } else {
        var index = $('#jqxgridFunctionManaged').jqxGrid('getselectedrowindex');
        FunctionManagedRow = ($('#jqxgridFunctionManaged').jqxGrid('getrowdata', index)).RowNumber;

        closeNotifications();

        var listOfParams = {
            pFTE: FTE,
            pBudgetGoc: BudgetGoc,
            pChargeOut: ChargeOut,
            pFunctionManagedRow: FunctionManagedRow,
            pRequestID: RequestID
        }

        _callServer({
            loadingMsg: "", async: false,
            url: '/NonMOU/insertMigrations',
            data: {
                'data': JSON.stringify(listOfParams)
            },
            type: "post",
            success: function (json) {
                var response = jQuery.parseJSON(json);
            }
        });

        renderMyMigrations(RequestID);

        renderBudgetGoc();

        var dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;"></div>';
        $("#jqxComboBoxFunctionManaged").jqxDropDownButton('setContent', dropDownContent);

        $("#txtFTE").val('0');

    }
}

function renderMyMigrations(RequestId) {
    var listOfParams = {

        pRequestID: RequestId
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getMigrations',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            if (response) {
                response;
                //cargar grid

                //

                var data2 = response;

                var source =
               {
                   unboundmode: true,
                   localdata: data2,
                   datafields:
                           [
                           { name: 'ID', type: 'string' },
                           { name: 'RequestID ', type: 'string' },
                           { name: 'FTE', type: 'string' },
                           { name: 'BudgetGoc', type: 'string' },
                           { name: 'ChargeOutGOC', type: 'string' },
                           { name: 'Expr1', type: 'string' },
                           ],
                   datatype: "json"
               };
                var dataAdapter = new $.jqx.dataAdapter(source);

                var imgDelete = function (row, datafield, value) {

                    var rowData = $('#jqxGridMigrations').jqxGrid('getrowdata', row);

                    return '<center><img onClick="deleteMigration(' + rowData.ID + ',' + RequestId + ')" style="margin-left: 5px;margin-top: 5px; cursor:pointer; width:25px; height:25px"   src="../Images/Delete.png"/></center>';
                }


                $('#jqxGridMigrations').jqxGrid(
               {
                   source: dataAdapter,

                   width: '100%',
                   selectionmode: 'none',
                   theme: 'blackberry',
                   autoheight: true,
                   autorowheight: true,
                   pageable: false,
                   filterable: false,
                   columnsresize: true,
                   altrows: true,
                   columns: [
                   { text: 'Delete', filtertype: 'none', width: '5%', columntype: 'image', pinned: false, cellsrenderer: imgDelete },
                   { text: 'FTE', dataField: 'FTE', filtertype: 'input', width: '5%', editable: false },
                   { text: 'Budget GOC', dataField: 'BudgetGoc', filtertype: 'input', width: '30%', editable: false },
                   { text: 'Charge Out/Allocation GOC', dataField: 'ChargeOutGOC', filtertype: 'input', width: '30%', editable: false },
                   { text: 'FRO Function/Managed Segment', dataField: 'Expr1', filtertype: 'input', width: '30%', editable: false },
                   ]

               });

            }

        }
    });

}
function deleteMigration(ID, RequestId) {
    var listOfParams = {
        pRequestID: ID
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/deleteMigrationList',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            renderMyMigrations(RequestId);
        }
    });

}


//Fro functionals
function renderFunctionManagedSegment() {
    var NonMOUID = $('#txtNonMOUID').val();

    var listOfParams = {
        pNonMOU: NonMOUID
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getFunctionManagedSegment',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
           // var response = jQuery.parseJSON(json);

            result = jQuery.parseJSON(json);

          //  if (!$("#jqxgridFunctionManaged").hasClass('jqx-grid')) {

                var source =
                    {
                        localdata: result,
                        datafields:
                        [
                            { name: 'RowNumber', type: 'string' },
                            { name: 'ManagedGeographyName', type: 'string' },
                            { name: 'ManagedSegmentName', type: 'string' },
                            { name: 'ManagerName', type: 'string' },
                        ],
                        datatype: "array",
                        updaterow: function (rowid, rowdata) {
                        }
                    };

                var dataAdapter = new $.jqx.dataAdapter(source);



                $("#jqxgridFunctionManaged").jqxGrid(
                 {
                     width: 600,
                     source: dataAdapter,
                     pageable: true,
                     autoheight: true,
                     columnsresize: true,
                     columns: [
                       { text: 'Managed Geography', columntype: 'textbox', datafield: 'ManagedGeographyName', width: '33%' },
                       { text: 'Managed Segment', columntype: 'textbox', datafield: 'ManagedSegmentName', width: '33%' },
                       { text: 'Manager Name', columntype: 'textbox', datafield: 'ManagerName', width: '33%' },
                     ]
                 });

                $("#jqxComboBoxFunctionManaged").jqxDropDownButton({ width: 400, height: 25 });


                //Salvar nueva seleccion
                $("#jqxgridFunctionManaged").on('rowselect', function (event) {
                    event.stopImmediatePropagation();
                    var args = event.args;
                    var row = $("#jqxgridFunctionManaged").jqxGrid('getrowdata', args.rowindex);
                    var dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;">' + row['ManagedGeographyName'] + ' | ' + row['ManagedSegmentName'] + ' | ' + row['ManagerName'] + '</div>';
                    $("#jqxComboBoxFunctionManaged").jqxDropDownButton('setContent', dropDownContent);

                    autoSaveFunctionMigration(row);


                });


                _callServer({
                    loadingMsg: "Get total MOU...",
                    url: '/HMT2/getFunctionManagedSegmentSelected',
                    data: {
                        'data': JSON.stringify(listOfParams)
                    },
                    type: "post",
                    success: function (json) {

                        response = jQuery.parseJSON(json);
                        if (json == "[]")
                        {
                            var dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;"></div>';
                            $("#jqxComboBoxFunctionManaged").jqxDropDownButton('setContent', dropDownContent);
                        } else {
                            $.each((response), function (index, element) {

                                var dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;">' + element.ManagedGeographyName + ' | ' + element.ManagedSegmentName + ' | ' + element.ManagerName + '</div>';
                                $("#jqxComboBoxFunctionManaged").jqxDropDownButton('setContent', dropDownContent);

                            });
                        }

                    }
                });
            }


        //}

        
    });


}
function autoSaveFunctionMigration(row) {

    var FunctionManagedRow = 0;
    var NonMOUID = $('#txtNonMOUID').val();


    if (($("#jqxComboBoxFunctionManaged").val()) != "") {
        var index = $('#jqxgridFunctionManaged').jqxGrid('getselectedrowindex');
        FunctionManagedRow = row.RowNumber;

        var listOfParams = {
            pFunctionManagedRow: FunctionManagedRow,
            pNonMOU: NonMOUID
        }

        _callServer({
            loadingMsg: "", async: false,
            url: '/NonMOU/insertFuncionManagedRow',
            data: {
                'data': JSON.stringify(listOfParams)
            },
            type: "post",
            success: function (json) {
                var response = jQuery.parseJSON(json);

                if (response) {
                    $.each((response), function (index, element) {
                        $('#txtNonMOUID').val(element.RequestAutoID);
                    });
                }

            }
        });


    }
}


function saveDraftRequest() {
    var listOfParams = {

        pRequestID: $('#hdfRequestID').val(),
        pName: $('#txtName').val(),
        pSummary: $('#txtSummaryBenefits').val()
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/saveDraft',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
           
                getListDraft();
            
        }
    });

}

function getListDraft() {

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getListDraft',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);
            if (response) {

                var data2 = (response);
                if (data2.lenght == 0) {

                    $('#lblCountDraft').text(data2.length)
                }
                else {

                    $('#lblCountDraft').text(data2.length)
                    var source =
                   {
                       unboundmode: true,
                       localdata: data2,
                       datafields:
                               [
                                { name: 'MIGRATIONID', type: 'string' },
                              , { name: 'FTE', type: 'string' },
                              , { name: 'FUNCTIONID', type: 'string' },
                              , { name: 'RequestID', type: 'string' },
                              , { name: 'Status', type: 'string' },
                              , { name: 'NONMOUALIAS', type: 'string' },
                              , { name: 'FUNCTIONMS', type: 'string' },
                              , { name: 'FUNCTIONMSNAME', type: 'string' },
                              , { name: 'FUNCTIONMG', type: 'string' },
                              , { name: 'FUNCTIONMGNAME', type: 'string' },
                              , { name: 'FUNCTIONMANAGER', type: 'string' },
                              , { name: 'STATUSNAME', type: 'string' },
                              , { name: 'Name', type: 'string' },
                              , { name: 'TYPEID', type: 'string' },
                              , { name: 'Scope', type: 'string' },
                              , { name: 'Benefits', type: 'string' },
                              , { name: 'TYPENAME', type: 'string' },
                              , { name: 'FTECOST', type: 'string' },
                              , { name: 'PL', type: 'string' },
                              , { name: 'SITEMANAGERSOEID', type: 'string' },
                              , { name: 'SITEFINANCESOEID', type: 'string' },
                              , { name: 'MSLVL09', type: 'string' },
                              , { name: 'Description', type: 'string' },
                              , { name: 'SITEMANAGENAME', type: 'string' },
                              , { name: 'SITEFINANCENAME', type: 'string' },
                              , { name: 'PLNAME', type: 'string' },
                              , { name: 'PLCOST', type: 'string' },
                              , { name: 'PLMGID', type: 'string' },
                              , { name: 'NameRequest', type: 'string' },
                              , { name: 'CreatedBy', type: 'string' },
                              , { name: 'Date', type: 'string' },
                              , { name: 'StatusRequest', type: 'string' },
                              , { name: 'Initiated', type: 'string' },
                              , { name: 'FTEAvaible', type: 'string' },

                               ],
                       datatype: "json"
                   };
                    var dataAdapter = new $.jqx.dataAdapter(source);

                    var imgEditDiscovery2 = function (row, datafield, value) {

                        var rowData = $('#jqxHistoryGrid').jqxGrid('getrowdata', row);

                        return '<i class="fa fa-search" style=" font-size: 23px; color: #4286f4; margin-left:4px; margin-top:4px;" onclick="openPopUpLiveStage(' + rowData.MIGRATIONID + ')" ></i>'

                    }

                    $('#jqxHistoryGrid').jqxGrid(
                   {
                       source: dataAdapter,

                       width: '99%',
                       selectionmode: 'none',
                       theme: 'blackberry',
                       autoheight: true,
                       autorowheight: true,
                       pageable: true,
                       pagesize: 10,
                       filterable: false,
                       altrows: true,
                       columns: [
                       { text: 'Review Detail', filtertype: 'none', columntype: 'image', pinned: false, cellsrenderer: imgEditDiscovery2, width: 60, pinned: true },
                       { text: 'Non/MOUID', dataField: 'NONMOUALIAS', filtertype: 'input', editable: false, width: 200, pinned: true },
                       { text: 'Status', dataField: 'STATUSNAME', filtertype: 'input', width: 200, editable: false },
                       { text: 'Name', dataField: 'Name', filtertype: 'input', width: 200, editable: false },
                       { text: 'FTE', dataField: 'FTE', filtertype: 'input', width: 60, editable: false },
                       { text: 'Type', dataField: 'TYPENAME', filtertype: 'input', width: 200, editable: false },
                       { text: 'Scope', dataField: 'Scope', filtertype: 'input', width: 200, editable: false },
                       { text: 'Benefits', dataField: 'Benefits', filtertype: 'input', width: 200, editable: false },
                       { text: 'Receiver Location', dataField: 'PLNAME', filtertype: 'input', width: 200, editable: false },
                       { text: 'Receiver Hiring Manager', dataField: 'SITEMANAGENAME', filtertype: 'input', width: 200, editable: false },
                       { text: 'Receiver Site Headcount Lead', dataField: 'SITEFINANCENAME', filtertype: 'input', width: 200, editable: false },
                       { text: 'Receiver MSLVL09', dataField: 'Description', filtertype: 'input', width: 200, editable: false },
                       ]

                   });
                }
            }

        }
    });

}

function request_Edit(ID) {
    var listOfParams = {
        pID: ID
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getDraftByID',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            $.each((response), function (index, element) {
                //Continue = element
                $('#hdfRequestID').val(element.ID);
                $('#txtName').val(element.Name);
                $('#txtSummaryBenefits').val(element.Description);
            })

            renderMyMigrations($('#hdfRequestID').val());

            renderBudgetGoc();

            var dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;"></div>';
            //$("#jqxComboBoxFunctionManaged").jqxDropDownButton('setContent', dropDownContent);

            $("#txtFTE").jqxNumberInput({ inputMode: 'simple', width: '75px', height: '25px', inputMode: 'simple', spinButtons: true });

        }
    });

    $("#jqxTabs").jqxTabs('select', 1);
}
function request_Submit(val) {
    var listOfParams = {
        pRequestID: val
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/saveQueueFromList',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            if (response) {
                if (response == "Error") {

                    closeNotifications();


                } else {
                    closeNotifications();
                    getListDraft();
                    getListQueue();

                    $("#jqxTabs").jqxTabs('select', 0);
                }
            }

        }
    });

  
}
function request_Delete(val) {
    var listOfParams = {
        pRequestID: val
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/deleteDraftFromList',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

                getListDraft();
            
        }
    });

    
}
function saveQueueRequest(val) {
    var listOfParams = {

        pRequestID: $('#hdfRequestID').val(),
        pName: $('#txtName').val(),
        pSummary: $('#txtSummaryBenefits').val()
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/saveQueue',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

         
                if (response == "Error") {

                    closeNotifications();

                    if (val == 1) {

                    } else {

                    }
                } else {
                    getListDraft();
                    getListQueue();

                    $("#jqxTabs").jqxTabs('select', 0);
                }
            

        }
    });

}
function getListQueue() {

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getListQueue',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            if (response) {


                var data2 = (response);
                if (data2.lenght == 0) {

                    $('#lblQueueCount').text(data2.length);
                    $('#lblDiscoveryTotalQueue').text(data2.length);
                }
                else {

                    $('#lblQueueCount').text(data2.length);
                    $('#lblDiscoveryTotalQueue').text(data2.length);
                    var source =
                   {
                       unboundmode: true,
                       localdata: data2,
                       datafields:
                               [
                                { name: 'MIGRATIONID', type: 'string' },
                              , { name: 'FTE', type: 'string' },
                              , { name: 'FUNCTIONID', type: 'string' },
                              , { name: 'RequestID', type: 'string' },
                              , { name: 'Status', type: 'string' },
                              , { name: 'NONMOUALIAS', type: 'string' },
                              , { name: 'FUNCTIONMS', type: 'string' },
                              , { name: 'FUNCTIONMSNAME', type: 'string' },
                              , { name: 'FUNCTIONMG', type: 'string' },
                              , { name: 'FUNCTIONMGNAME', type: 'string' },
                              , { name: 'FUNCTIONMANAGER', type: 'string' },
                              , { name: 'STATUSNAME', type: 'string' },
                              , { name: 'Name', type: 'string' },
                              , { name: 'TYPEID', type: 'string' },
                              , { name: 'Scope', type: 'string' },
                              , { name: 'Benefits', type: 'string' },
                              , { name: 'TYPENAME', type: 'string' },
                              , { name: 'FTECOST', type: 'string' },
                              , { name: 'PL', type: 'string' },
                              , { name: 'SITEMANAGERSOEID', type: 'string' },
                              , { name: 'SITEFINANCESOEID', type: 'string' },
                              , { name: 'MSLVL09', type: 'string' },
                              , { name: 'Description', type: 'string' },
                              , { name: 'SITEMANAGENAME', type: 'string' },
                              , { name: 'SITEFINANCENAME', type: 'string' },
                              , { name: 'PLNAME', type: 'string' },
                              , { name: 'PLCOST', type: 'string' },
                              , { name: 'PLMGID', type: 'string' },
                              , { name: 'NameRequest', type: 'string' },
                              , { name: 'CreatedBy', type: 'string' },
                              , { name: 'Date', type: 'string' },
                              , { name: 'StatusRequest', type: 'string' },
                              , { name: 'Initiated', type: 'string' },
                              , { name: 'FTEAvaible', type: 'string' },
                               ],
                       datatype: "json"
                   };
                    var dataAdapter = new $.jqx.dataAdapter(source);


                    var imgEditDiscovery2 = function (row, datafield, value) {

                        var rowData = $('#jqxGridQueueDiscovery').jqxGrid('getrowdata', row);


                        return '<center><i class="fa fa-search" style=" font-size: 23px; color: #4286f4;" onclick="openPopUpLiveStage(' + rowData.MIGRATIONID + ')" ></i></center>'

                    }

                    var imgEditDiscovery = function (row, datafield, value) {

                        var rowData = $('#jqxGridQueueDiscovery').jqxGrid('getrowdata', row);


                        return '<button class="btnLoadDBTable btn btn-info left5" href="#" onclick="editDiscoveryStage(' + rowData.MIGRATIONID + ')" style="display:inline-block; background-color:#f0ad4e; width:120px;    margin-top: 5px;">Edit/NonMOU</button> <a class="btnDeleteDBTable btn btn-danger" href="#" onclick="request_DeleteQueue(' + rowData.MIGRATIONID + ')" style="display:inline-block; background-color:#d9534f; width:120px;     margin-top: 5px;">Delete</a></br>'

                    }

                    $('#jqxGridQueueDiscovery').jqxGrid(
                   {
                       source: dataAdapter,

                       width: '100%',
                       selectionmode: 'none',
                       theme: 'blackberryOrange',
                       autoheight: true,
                       autorowheight: true,
                       pageable: true,
                       pagesize: 5,
                       filterable: false,
                       //   columnsresize: true,
                       altrows: true,
                       columns: [
                       { text: '', filtertype: 'none', width: 300, columntype: 'image', pinned: false, cellsrenderer: imgEditDiscovery, pinned: true },
                       { text: 'Review Detail', filtertype: 'none', width: 60, columntype: 'image', pinned: false, cellsrenderer: imgEditDiscovery2, pinned: true },
                       { text: 'Non/MOUID', dataField: 'NONMOUALIAS', filtertype: 'input', editable: false, width: 200, pinned: true },
                       { text: 'Status', dataField: 'STATUSNAME', filtertype: 'input', width: 200, editable: false },
                       { text: 'Name', dataField: 'Name', filtertype: 'input', width: 200, editable: false },
                       { text: 'FTE', dataField: 'FTE', filtertype: 'input', width: 60, editable: false },
                       { text: 'Type', dataField: 'TYPENAME', filtertype: 'input', width: 200, editable: false },
                       { text: 'Scope', dataField: 'Scope', filtertype: 'input', width: 200, editable: false },
                       { text: 'Benefits', dataField: 'Benefits', filtertype: 'input', width: 200, editable: false },
                       { text: 'Receiver Location', dataField: 'PLNAME', filtertype: 'input', width: 200, editable: false },
                       { text: 'Receiver Hiring Manager', dataField: 'SITEMANAGENAME', filtertype: 'input', width: 200, editable: false },
                       { text: 'Receiver Site Headcount Lead', dataField: 'SITEFINANCENAME', filtertype: 'input', width: 200, editable: false },
                       { text: 'Receiver MSLVL09', dataField: 'Description', filtertype: 'input', width: 200, editable: false },
                       ]

                   });
                }
            }
        }
    });


}

function request_SubmitQueQue(ID) {
    Management_tab();

    editDiscoveryStage(ID);

}

function request_DeleteQueue(ID) {
    var listOfParams = {
        pRequestID: ID
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/deleteQueueFromList',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {

                getListQueue();
            
        }
    });

}

function addRemoveClass(ID) {
    //$("#RequestTab").removeClass('active');
    //$("#QueueTab").removeClass('active');
    //$("#ManagementTab").removeClass('active');
    //$("#ApprovalTab").removeClass('active');
    //$("#DashboardTab").removeClass('active');
    //$("#ReportTab").removeClass('active');
    //$("#HistoricalListTab").removeClass('active');

    // $(ID).addClass('active');

}
function loadDiv(ID) {
    $("#RequestStage").hide();
    $("#QueueStage").hide();
    $("#DiscoveryStage").hide();
    $("#ApprovalStage").hide();

    $(ID).show();

}

function Request_tab() {
    addRemoveClass("#RequestTab");
    loadDiv("#RequestStage");
    $("#jqxTabs").jqxTabs('select', 0);
    $('#jqxTabs').jqxTabs({ width: '98%', position: 'top', autoHeight: false, scrollable: false, theme: 'blackberry' });
    $('#settings div').css('margin-top', '10px');

}
function Queue_tab() {
    addRemoveClass("#QueueTab");
    loadDiv("#QueueStage");

    renderCompleteStage();
}
function Management_tab() {
    addRemoveClass("#ManagementTab");
    loadDiv("#DiscoveryStage");

    $('#listQueue').show();
    $('#DiscoveryEdit').hide();

    getListQueue();


    $("#jqxNotSendForApproval").jqxNotification({
        width: '100%', position: "top-left", opacity: 0.9, appendContainer: "#containerSendForApproval",
        autoOpen: false, animationOpenDelay: 100, autoClose: true, autoCloseDelay: 20000, template: "success"
    });

}
function Approval_tab() {
    addRemoveClass("#ApprovalTab");
    loadDiv("#ApprovalStage");

    renderApprovalStage();
}
function Dashboard_tab() {
    addRemoveClass("#DashboardTab");
    loadDiv("#MilestoneStage");
}
function Reports_tab() {
    addRemoveClass("#ReportTab");
    loadDiv("#ReportsStage");
}
function Historical_List_tab() {
    addRemoveClass("#HistoricalListTab");
    loadDiv("#HistoryStage");
}

function editDiscoveryStage(ID) {
    addRemoveClass("#ManagementTab");
    loadDiv("#DiscoveryStage");

    $('#listQueue').hide();
    $('#DiscoveryEdit').show();

    var listOfParams = {
        pID: ID
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getDiscoveryData',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            $.each((response), function (index, element) {
                //Continue = element
                renderCBFinancialOther();

                $('#txtNonMOUID').val(element.RequestAutoID);

                //$(_getASPControlID("hdFileUpload")).val(element.RequestAutoID);
                renderDatesOtherfte();
                $('#txtNameDiscovery').val(element.Name);
                $('#lblRegionDiscovery').text(element.ManagedGeographyName);
                renderNonMOUType(element.TypeName);
                renderRequisition();

                //    renderWorkingType();

                $("#txtFTEDiscovery").jqxNumberInput({ inputMode: 'simple', width: '75px', height: '25px', inputMode: 'simple', spinButtons: true });

                $('#txtFTEDiscovery').jqxNumberInput('val', element.FTE);

                //  findSoeidMethod("#jqxFindSOEID","50%");

                //  renderWorkingTeamList();

                $("#dateInitiated").jqxDateTimeInput({ width: '300px', height: '25px', formatString: 'd' });
                $("#dateAvaible").jqxDateTimeInput({ width: '300px', height: '25px', formatString: 'd' });

                //  getMilestoneList();

                renderFunctionManagedSegment();

                renderFinancials(element.ManagedSegmentName);

                renderApproval();

                getAttachments();


                $("#jqxSaveDiscovery").jqxNotification({
                    width: '100%', position: "top-left", opacity: 0.9, appendContainer: "#containerSaveDiscovery",
                    autoOpen: false, animationOpenDelay: 100, autoClose: true, autoCloseDelay: 20000, template: "success"
                });
                $("#WarnjqxSaveDiscovery").jqxNotification({
                    width: '100%', position: "top-left", opacity: 0.9, appendContainer: "#containerWarnSaveDiscovery",
                    autoOpen: false, animationOpenDelay: 100, autoClose: true, autoCloseDelay: 20000, template: "warning"
                });

                $("#jqxErrorCharter").jqxNotification({
                    width: '100%', position: "top-left", opacity: 0.9, appendContainer: "#containerErrorCharter",
                    autoOpen: false, animationOpenDelay: 100, autoClose: true, autoCloseDelay: 20000, template: "error"
                });
                $("#WarnjqxErrorCharter").jqxNotification({
                    width: '100%', position: "top-left", opacity: 0.9, appendContainer: "#containerWarnErrorCharter",
                    autoOpen: false, animationOpenDelay: 100, autoClose: true, autoCloseDelay: 20000, template: "warning"
                });

                $("#jqxErrorMilestone").jqxNotification({
                    width: '100%', position: "top-left", opacity: 0.9, appendContainer: "#containerErrorMilestone",
                    autoOpen: false, animationOpenDelay: 100, autoClose: true, autoCloseDelay: 20000, template: "error"
                });
                $("#WarnjqxErrorMilestone").jqxNotification({
                    width: '100%', position: "top-left", opacity: 0.9, appendContainer: "#containerWarnErrorMilestone",
                    autoOpen: false, animationOpenDelay: 100, autoClose: true, autoCloseDelay: 20000, template: "warning"
                });

                $("#jqxErrorFinancial").jqxNotification({
                    width: '100%', position: "top-left", opacity: 0.9, appendContainer: "#containerErrorFinancial",
                    autoOpen: false, animationOpenDelay: 100, autoClose: true, autoCloseDelay: 20000, template: "error"
                });
                $("#WarnjqxErrorFinancial").jqxNotification({
                    width: '100%', position: "top-left", opacity: 0.9, appendContainer: "#containerWarnErrorFinancial",
                    autoOpen: false, animationOpenDelay: 100, autoClose: true, autoCloseDelay: 20000, template: "warning"
                });

                $("#jqxErrorApproval").jqxNotification({
                    width: '100%', position: "top-left", opacity: 0.9, appendContainer: "#containerErrorApproval",
                    autoOpen: false, animationOpenDelay: 100, autoClose: true, autoCloseDelay: 20000, template: "error"
                });
                $("#WarnjqxErrorApproval").jqxNotification({
                    width: '100%', position: "top-left", opacity: 0.9, appendContainer: "#containerWarnErrorApproval",
                    autoOpen: false, animationOpenDelay: 100, autoClose: true, autoCloseDelay: 20000, template: "warning"
                });

                $("#errorAddOther").jqxNotification({
                    width: '100%', position: "top-left", opacity: 0.9, appendContainer: "#containerErrorAddOther",
                    autoOpen: false, animationOpenDelay: 100, autoClose: true, autoCloseDelay: 20000, template: "error"
                });


                $("#jqxExpanderCharter").jqxExpander({ width: '100%', height: 'auto', expanded: true, showArrow: true, theme: 'blackberryOrange' });
                $("#jqxExpanderMilestone").jqxExpander({ width: '100%', height: 'auto', expanded: true, showArrow: true, theme: 'blackberryOrange' });
                $("#jqxExpanderFinancial").jqxExpander({ width: '100%', height: 'auto', expanded: true, showArrow: true, theme: 'blackberryOrange' });
                $("#jqxExpanderApproval").jqxExpander({ width: '100%', height: 'auto', expanded: false, showArrow: true, theme: 'blackberryOrange' });
                $("#jqxExpanderAttachment").jqxExpander({ width: '100%', height: 'auto', expanded: false, showArrow: true, theme: 'blackberryOrange' });
                $("#jqxExpanderDiscussion").jqxExpander({ width: '100%', height: 'auto', expanded: true, showArrow: true, theme: 'blackberryOrange' });

                $("#titleCharter").text("Step 1 Charter");
                $("#titleMileston").text("Step 2 Milestones");
                $("#titleFinancials").text("Step 3 Financials");
                $("#titleApproval").text("Step 4 Approvals");
                $("#titleAttachments").text("Step 5 Attachments");
                $("#titleDiscussion").text("Step 6 Discussion");

                renderConversation();
                renderDataSaveData();

            });
        }
    });

}


function renderRequisition() {

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getRequisition',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = (response);

            var source =
                {
                    localdata: data2,
                    datatype: "json",
                    datafields: [
                        { name: 'Requisition' }
                    ],
                    async: false
                };

            var dataAdapter = new $.jqx.dataAdapter(source);

            $("#jqxDropDownRequisition").jqxComboBox({ selectedIndex: 0, source: dataAdapter, displayMember: "Requisition", valueMember: "Requisition", width: '65%', height: 25 });


            $("#jqxDropDownRequisition").on('select', function (event) {
                if (event.args) {
                    var item = event.args.item;
                    if (item) {

                    }
                }
            });
        }
    });


}


function renderNonMOUType(NameType) {

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getNonMOUType',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = (response);

            var source =
                {
                    localdata: data2,
                    datatype: "json",
                    datafields: [
                        { name: 'ID' },
                        { name: 'Type' }
                    ],
                    async: false
                };

            var dataAdapter = new $.jqx.dataAdapter(source);

            $("#jqxDropDownMOUType").jqxComboBox({ selectedIndex: 0, source: dataAdapter, displayMember: "Type", valueMember: "Type", width: '99%', height: 25 });

            if (NameType != '') {
                $("#jqxDropDownMOUType").jqxComboBox('selectItem', NameType);
            }

            $("#jqxDropDownMOUType").on('select', function (event) {
                if (event.args) {
                    var item = event.args.item;
                    if (item) {

                    }
                }
            });
        }
    });


}

function renderFinancialManagedSegmentLVL09() {
    var NameType = '';

    var listOfParams = {
        pNonMOU: $('#txtNonMOUID').val()
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getMSLVL09Selected',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            $.each((response), function (index, element) {

                NameType = element.Description;

            });
        }
    });


    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getFinancialMSLVL09',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = (response);

            var source =
                {
                    localdata: data2,
                    datatype: "json",
                    datafields: [

                        { name: 'Description' }
                    ],
                    async: false
                };

            var dataAdapter = new $.jqx.dataAdapter(source);

            $("#cbManagedSegmentLvl9").jqxComboBox({ selectedIndex: 0, source: dataAdapter, displayMember: "Description", valueMember: "Description", width: '99%', height: 25 });

            if (NameType != '') {
                $("#cbManagedSegmentLvl9").jqxComboBox('selectItem', NameType);
            }

            $("#cbManagedSegmentLvl9").on('select', function (event) {
                if (event.args) {
                    var item = event.args.item;
                }
            });
        }
    });

}



function findSoeidMethod(nameTag, widthValue) {
    var timer;
    $(nameTag).jqxInput({
        placeHolder: "Enter a SOEID", height: 25, width: widthValue,
        source: function (query, response) {
            var data2;

            var listOfParams = {
                data: query
            }

            _callServer({
                loadingMsg: "", async: false,
                url: '/NonMOU/getProbablyUser',
                //async: false,
                data: {
                    'data': JSON.stringify(listOfParams)
                },
                type: "post",
                success: function (json) {
                    var response = jQuery.parseJSON(json);

                    //data2 = (response);
                    loadAdapter(response);
                }
            });

            function loadAdapter(data2) {
                var dataAdapter = new $.jqx.dataAdapter
                (
                    {
                        datatype: "jsonp",
                        localdata: data2,
                        datafields:
                        [
                            { name: 'SOEID' }, { name: 'NAME' }
                        ],

                    },
                    {
                        autoBind: true,
                        formatData: function (data) {
                            data.name_startsWith = query;
                            return data;
                        },
                        loadComplete: function (data) {
                            if (data.length > 0) {
                                response($.map(data, function (item) {
                                    return {
                                        label: item.SOEID + ' ' + item.NAME,
                                        value: item.NAME
                                    }
                                }));
                            }
                        }
                    }
                );
            }
            
        }
    });


}



function renderWorkingTeamList() {

    var nonMOU = $('#txtNonMOUID').val();

    var listOfParams = {
        pNonMOU: nonMOU
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getWorkingTeamList',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            if (response) {

                var data2 = (response);

                var source =
               {
                   unboundmode: true,
                   localdata: data2,
                   datafields:
                           [
                           { name: 'ID', type: 'string' },
                           { name: 'NAME', type: 'string' },
                           { name: 'Role', type: 'string' }
                           ],
                   datatype: "json"
               };
                var dataAdapter = new $.jqx.dataAdapter(source);

                var imgDelete = function (row, datafield, value) {

                    var rowData = $('#jqxGridWorkingTeam').jqxGrid('getrowdata', row);


                    // return '<a class="btn" href="#" onclick="deleteWorkingTeam(' + rowData.ID + ')" style="display:inline-block; background-color:#d9534f; width:100px;"><i class="icon-trash" style="padding-top: 8px; font-size: 20px; color: white;"></i> Delete</a></BR></BR> '
                    return '<a class="btn" href="#" onclick="deleteWorkingTeam(' + rowData.ID + ')" style="display:inline-block; background-color:#d9534f; width:100px;"><i class="icon-trash" style="padding-top: 8px; font-size: 20px; color: white;"></i> Delete</a></BR></BR> '
                }

                $('#jqxGridWorkingTeam').jqxGrid(
               {
                   source: dataAdapter,

                   width: '100%',
                   selectionmode: 'none',
                   theme: 'blackberry',
                   autoheight: true,
                   autorowheight: true,
                   pageable: false,

                   filterable: false,
                   columnsresize: true,
                   altrows: true,
                   columns: [
                   { text: '', filtertype: 'none', width: '10%', columntype: 'image', pinned: false, cellsrenderer: imgDelete },
                   { text: 'Role', dataField: 'Role', filtertype: 'input', width: '45%', editable: false },
                   { text: 'NAME', dataField: 'NAME', filtertype: 'input', width: '45%', editable: false },
                   ]

               });

            }
        }
    });


}

function deleteWorkingTeam(ID) {
    var listOfParams = {
        pID: ID
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/deleteWorkingTeam',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);


        }
    });

}

function getMilestoneList() {

    var nonMOU = $('#txtNonMOUID').val();

    var listOfParams = {
        pNonMOU: nonMOU
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getMilestones',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            if (response) {

                var data2 = (response);
                var editedRows = new Array();

                var source =
               {
                   localdata: data2,
                   updaterow: function (rowid, rowdata, commit) {
                       var rowindex = $("#jqxGridMilestones").jqxGrid('getrowboundindexbyid', rowid);
                       editedRows.push({ index: rowindex, data: rowdata });
                       commit(true);
                   },
                   datafields:
                           [
                           { name: 'ID', type: 'string' },
                           { name: 'Enable', type: 'string' },
                           { name: 'Milestone', type: 'string' },
                           { name: 'StartDate', type: 'date' },
                           { name: 'EndDate', type: 'date' },
                           { name: 'Comments', type: 'string' }
                           ],
                   datatype: "json"
               };
                var dataAdapter = new $.jqx.dataAdapter(source);
                var cellclass = function (row, datafield, value, rowdata) {
                    for (var i = 0; i < editedRows.length; i++) {
                        if (editedRows[i].index == row) {
                            return "editedRow";
                        }
                    }
                }

                $('#jqxGridMilestones').jqxGrid(
               {
                   source: dataAdapter,

                   width: '95%',
                   selectionmode: 'multiplecellsadvanced',
                   editable: true,
                   theme: 'blackberry',
                   autoheight: true,
                   autorowheight: true,
                   pageable: false,

                   filterable: false,
                   columnsresize: true,
                   altrows: true,
                   columns: [
                       { text: 'Enable', datafield: 'Enable', threestatecheckbox: true, columntype: 'checkbox', width: '6%' },
                       { text: 'Milestone', dataField: 'Milestone', filtertype: 'input', width: '25%', editable: false },
                       {
                           text: 'StartDate', datafield: 'StartDate', cellclassname: cellclass, columntype: 'datetimeinput', width: 110, align: 'right', cellsalign: 'right', cellsformat: 'd', width: '21%',
                           validation: function (cell, value) {
                               if (value == "")
                                   return true;
                               var year = value.getFullYear();
                               if (year <= 2015) {
                                   return { result: false, message: "Ship Date should be before 1/1/2015" };
                               }
                               return true;
                           }
                       },
                       {
                           text: 'EndDate', datafield: 'EndDate', cellclassname: cellclass, columntype: 'datetimeinput', width: 110, align: 'right', cellsalign: 'right', cellsformat: 'd', width: '21%',
                           validation: function (cell, value) {
                               if (value == "")
                                   return true;
                               var year = value.getFullYear();
                               if (year <= 2015) {
                                   return { result: false, message: "Ship Date should be before 1/1/2015" };
                               }
                               return true;
                           }
                       },
                       { text: 'Comments', dataField: 'Comments', columntype: 'textbox', filtertype: 'input', width: '25%' }
                   ]

               });

            }
        }
    });

}

function renderFinancials(msValue) {
    renderPhysicalLocation();

    findSoeidMethod("#jqxSiteManager", "100%");
    findSoeidMethod("#jqxSiteFinanceManger", "100%");

    // renderBudgetGOC();
    renderChargeOutGOC();
    renderReceivingGOC();

    renderFinancialDetail();
}

function renderPhysicalLocation() {

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getPhysicalLocation',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = (response);

            var source =
                {
                    localdata: data2,
                    datatype: "json",
                    datafields: [
                        { name: 'ID' },
                        { name: 'Name' }
                    ],
                    async: false
                };

            var dataAdapter = new $.jqx.dataAdapter(source);

            $("#cbPhysicalLocation").jqxComboBox({ selectedIndex: 0, source: dataAdapter, displayMember: "Name", valueMember: "Name", width: '99%', height: 25 });

            $("#cbPhysicalLocation").on('select', function (event) {
                if (event.args) {
                    var item = event.args.item;
                    if (item) {

                    }
                }
            });

        }
    });

}

function findGocMethod(nameTag, widthValue) {
    var timer;
    $(nameTag).jqxInput({
        placeHolder: "Enter a GOC", height: 25, width: widthValue,
        source: function (query, response) {
            var data2;

            var listOfParams = {
                data: query
            }

            _callServer({
                loadingMsg: "", async: false,
                url: '/NonMOU/getProbablyGOC',
                data: {
                    'data': JSON.stringify(listOfParams)
                },
                type: "post",
                success: function (json) {
                    var response = jQuery.parseJSON(json);
                    loadAdapter(response);
                    
                }
            });

            function loadAdapter(data2) {


                var dataAdapter = new $.jqx.dataAdapter
                (
                    {
                        datatype: "jsonp",
                        localdata: data2,
                        datafields:
                        [
                            { name: 'GOCID' }, { name: 'GOCName' }
                        ],

                    },
                    {
                        autoBind: true,
                        formatData: function (data) {
                            data.name_startsWith = query;
                            return data;
                        },
                        loadComplete: function (data) {
                            if (data.length > 3) {
                                response($.map(data, function (item) {
                                    return {
                                        label: item.GOCName,
                                        value: item.GOCName
                                    }
                                }));
                            }
                        }
                    }
                );
            }
        }
    });


}

function renderBudgetGOC() {
    // findGocMethod('#jqxBudgetGoc', '75%');
    //$('#MarketBudgetGoc').jqxNumberInput({ inputMode: 'simple', width: '75px', height: '25px', inputMode: 'simple', spinButtons: true, symbol: '%' });
    // renderFinancialGOC('#jqxGridBudgetGOCFinancial', 'Budget');
    //  $('#jqxGridBudgetGOCFinancial')

}

function renderChargeOutGOC() {
    findGocMethod('#jqxChargeOutGoc', '75%');
    $('#MarketChargeOutGOC').jqxNumberInput({ inputMode: 'simple', width: '75px', height: '25px', inputMode: 'simple', spinButtons: true });
    renderFinancialGOC('#jqxGridChargeOutFinancial', 'ChargeOut');


}

function renderReceivingGOC() {
    findGocMethod('#jqxReceivingGoc', '75%');
    $('#MarketReceivingGOC').jqxNumberInput({ inputMode: 'simple', width: '75px', height: '25px', inputMode: 'simple', spinButtons: true });
    renderFinancialGOC('#jqxGridReceivingFinancial', 'Receiving');


}

function renderFinancialGOC(GRID, TYPE) {

    var nonMOU = $('#txtNonMOUID').val();

    var listOfParams = {
        NonMOU: nonMOU,
        GOCTYPE: TYPE
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getFinancialGOC',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            if (response) {


                var data2 = (response);

                var source =
               {
                   unboundmode: true,
                   localdata: data2,
                   datafields:
                           [
                           { name: 'ID', type: 'string' },
                           { name: 'MigrationID', type: 'string' },
                           { name: 'TypeGOC', type: 'string' },
                           { name: 'GOC', type: 'string' },
                           { name: 'Market', type: 'string' },
                           { name: 'Region', type: 'string' },
                           { name: 'MngdGeography', type: 'string' },
                           { name: 'Customer', type: 'string' },
                           { name: 'MngdSegment', type: 'string' },
                           { name: 'LegalVehicule', type: 'string' },
                           { name: 'Expr1', type: 'string' },
                           { name: 'GOCName', type: 'string' },

                           ],
                   datatype: "json"
               };
                var dataAdapter = new $.jqx.dataAdapter(source);

                var imgDelete = function (row, datafield, value) {

                    var rowData = $(GRID).jqxGrid('getrowdata', row);

                    return '<a class="btn" href="#" onclick="deleteFinancialGOC(' + rowData.ID + ')" style="display:inline-block; background-color:#d9534f; width:100px;"><i class="icon-trash" style="padding-top: 8px; font-size: 20px; color: white;"></i> Delete</a></BR></BR> ';
                }

                $(GRID).jqxGrid(
               {
                   source: dataAdapter,

                   width: '100%',
                   selectionmode: 'none',
                   theme: 'blackberry',
                   autoheight: true,
                   autorowheight: true,
                   pageable: false,
                   filterable: false,
                   columnsresize: true,
                   altrows: true,
                   columns: [
                   { text: '', filtertype: 'none', width: '10%', columntype: 'image', pinned: false, cellsrenderer: imgDelete },
                   { text: 'GOC', dataField: 'GOCName', filtertype: 'input', width: '20%', editable: false },
                   { text: 'Percentage', dataField: 'Market', filtertype: 'input', width: '10%', editable: false },
                   { text: 'Region', dataField: 'Region', filtertype: 'input', width: '10%', editable: false },
                   { text: 'Mngd Geography', dataField: 'MngdGeography', filtertype: 'input', width: '10%', editable: false },
                   { text: 'Customer', dataField: 'Customer', filtertype: 'input', width: '10%', editable: false },
                   { text: 'Mngd Segment', dataField: 'MngdSegment', filtertype: 'input', width: '10%', editable: false },
                   { text: 'Legal Vehicule', dataField: 'LegalVehicule', filtertype: 'input', width: '20%', editable: false }
                   ]

               });

            }
        }
    });

 
}

function deleteFinancialGOC(ID) {
    var listOfParams = {
        pID: ID
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/deleteFinancialGocList',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            
                renderFinancialGOC('#jqxGridChargeOutFinancial', 'ChargeOut');
                renderFinancialGOC('#jqxGridReceivingFinancial', 'Receiving');
            
        }
    });

}

function addBudgetFinancial() {
    //var listOfParams = {
    //    GOCTYPE: 'Budget',
    //        NonMOU: $('#txtNonMOUID').val(),
    //        GOC: $('#jqxBudgetGoc').val().split(' ')[0],
    //        Market: $('#MarketBudgetGoc').val()
    //}
    //addFinancialGOC(listOfParams);
}
function addChargeOutFinancial() {

    var listOfParams = {
        GOCTYPE: 'ChargeOut',
        NonMOU: $('#txtNonMOUID').val(),
        GOC: $('#jqxChargeOutGoc').val().split(' ')[0],
        Market: $('#MarketChargeOutGOC').val()
    }
    addFinancialGOC(listOfParams);
}
function addReceivingFinancial() {

    var listOfParams = {
        GOCTYPE: 'Receiving',
        NonMOU: $('#txtNonMOUID').val(),
        GOC: $('#jqxReceivingGoc').val().split(' ')[0],
        Market: $('#MarketReceivingGOC').val()
    }

    addFinancialGOC(listOfParams);

}

function addFinancialGOC(listOfParams) {

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/saveFinancialGOC',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
         
                //renderFinancialGOC('#jqxGridBudgetGOCFinancial', 'Budget');
                renderFinancialGOC('#jqxGridChargeOutFinancial', 'ChargeOut');
                renderFinancialGOC('#jqxGridReceivingFinancial', 'Receiving');
            

        }
    });

}


function renderFinancialDetail() {
    $('#dsTotal').text('0.00');
    $('#lblDSJAN').text('0.00');
    $('#lblDSFEB').text('0.00');
    $('#lblDSMAR').text('0.00');
    $('#lblDSAPR').text('0.00');
    $('#lblDSMAY').text('0.00');
    $('#lblDSJUN').text('0.00');
    $('#lblDSJUL').text('0.00');
    $('#lblDSAUG').text('0.00');
    $('#lblDSSEP').text('0.00');
    $('#lblDSOCT').text('0.00');
    $('#lblDSNOV').text('0.00');
    $('#lblDSDEC').text('0.00');
    $('#lblDSTotal').text('0.00');
    $('#osTotal').text('0.00');
    $('#lblOSJAN').text('0.00');
    $('#lblOSFEB').text('0.00');
    $('#lblOSMAR').text('0.00');
    $('#lblOSAPR').text('0.00');
    $('#lblOSMAY').text('0.00');
    $('#lblOSJUN').text('0.00');
    $('#lblOSJUL').text('0.00');
    $('#lblOSAUG').text('0.00');
    $('#lblOSSEP').text('0.00');
    $('#lblOSOCT').text('0.00');
    $('#lblOSNOV').text('0.00');
    $('#lblOSDEC').text('0.00');
    $('#lblOSTOTAL').text('0.00');
    $('#fteJAN').text('0.00');
    $('#fteFEB').text('0.00');
    $('#fteMAR').text('0.00');
    $('#fteAPR').text('0.00');
    $('#fteMAY').text('0.00');
    $('#fteJUN').text('0.00');
    $('#fteJUL').text('0.00');
    $('#fteAUG').text('0.00');
    $('#fteSEP').text('0.00');
    $('#fteOCT').text('0.00');
    $('#fteNOV').text('0.00');
    $('#fteDEC').text('0.00');
    $('#fteTOTAL').text('0.00');
    //$('#trainCostTotal').text('0.00');
    //$('#tecCostTOTAL').text('0.00');
    $('#FTETotalCostJAN').text('0.00');
    $('#FTETotalCostFEB').text('0.00');
    $('#FTETotalCostMAR').text('0.00');
    $('#FTETotalCostAPR').text('0.00');
    $('#FTETotalCostMAY').text('0.00');
    $('#FTETotalCostJUN').text('0.00');
    $('#FTETotalCostJUL').text('0.00');
    $('#FTETotalCostAUG').text('0.00');
    $('#FTETotalCostSEP').text('0.00');
    $('#FTETotalCostOCT').text('0.00');
    $('#FTETotalCostNOV').text('0.00');
    $('#FTETotalCostDEC').text('0.00');
    $('#FTETotalCostTOTAL').text('0.00');

    $('#2dsTotal').text('0.00');
    $('#2lblDSJAN').text('0.00');
    $('#2lblDSFEB').text('0.00');
    $('#2lblDSMAR').text('0.00');
    $('#2lblDSAPR').text('0.00');
    $('#2lblDSMAY').text('0.00');
    $('#2lblDSJUN').text('0.00');
    $('#2lblDSJUL').text('0.00');
    $('#2lblDSAUG').text('0.00');
    $('#2lblDSSEP').text('0.00');
    $('#2lblDSOCT').text('0.00');
    $('#2lblDSNOV').text('0.00');
    $('#2lblDSDEC').text('0.00');
    $('#2lblDSTotal').text('0.00');
    $('#2osTotal').text('0.00');
    $('#2lblOSJAN').text('0.00');
    $('#2lblOSFEB').text('0.00');
    $('#2lblOSMAR').text('0.00');
    $('#2lblOSAPR').text('0.00');
    $('#2lblOSMAY').text('0.00');
    $('#2lblOSJUN').text('0.00');
    $('#2lblOSJUL').text('0.00');
    $('#2lblOSAUG').text('0.00');
    $('#2lblOSSEP').text('0.00');
    $('#2lblOSOCT').text('0.00');
    $('#2lblOSNOV').text('0.00');
    $('#2lblOSDEC').text('0.00');
    $('#2lblOSTOTAL').text('0.00');
    $('#2fteJAN').text('0.00');
    $('#2fteFEB').text('0.00');
    $('#2fteMAR').text('0.00');
    $('#2fteAPR').text('0.00');
    $('#2fteMAY').text('0.00');
    $('#2fteJUN').text('0.00');
    $('#2fteJUL').text('0.00');
    $('#2fteAUG').text('0.00');
    $('#2fteSEP').text('0.00');
    $('#2fteOCT').text('0.00');
    $('#2fteNOV').text('0.00');
    $('#2fteDEC').text('0.00');
    $('#2fteTOTAL').text('0.00');
    //$('#2trainCostTotal').text('0.00');
    //$('#2tecCostTOTAL').text('0.00');
    $('#2FTETotalCostJAN').text('0.00');
    $('#2FTETotalCostFEB').text('0.00');
    $('#2FTETotalCostMAR').text('0.00');
    $('#2FTETotalCostAPR').text('0.00');
    $('#2FTETotalCostMAY').text('0.00');
    $('#2FTETotalCostJUN').text('0.00');
    $('#2FTETotalCostJUL').text('0.00');
    $('#2FTETotalCostAUG').text('0.00');
    $('#2FTETotalCostSEP').text('0.00');
    $('#2FTETotalCostOCT').text('0.00');
    $('#2FTETotalCostNOV').text('0.00');
    $('#2FTETotalCostDEC').text('0.00');
    $('#2FTETotalCostTOTAL').text('0.00');


    $('#3dsTotal').text('0.00');
    $('#3lblDSJAN').text('0.00');
    $('#3lblDSFEB').text('0.00');
    $('#3lblDSMAR').text('0.00');
    $('#3lblDSAPR').text('0.00');
    $('#3lblDSMAY').text('0.00');
    $('#3lblDSJUN').text('0.00');
    $('#3lblDSJUL').text('0.00');
    $('#3lblDSAUG').text('0.00');
    $('#3lblDSSEP').text('0.00');
    $('#3lblDSOCT').text('0.00');
    $('#3lblDSNOV').text('0.00');
    $('#3lblDSDEC').text('0.00');
    $('#3lblDSTotal').text('0.00');
    $('#3osTotal').text('0.00');
    $('#3lblOSJAN').text('0.00');
    $('#3lblOSFEB').text('0.00');
    $('#3lblOSMAR').text('0.00');
    $('#3lblOSAPR').text('0.00');
    $('#3lblOSMAY').text('0.00');
    $('#3lblOSJUN').text('0.00');
    $('#3lblOSJUL').text('0.00');
    $('#3lblOSAUG').text('0.00');
    $('#3lblOSSEP').text('0.00');
    $('#3lblOSOCT').text('0.00');
    $('#3lblOSNOV').text('0.00');
    $('#3lblOSDEC').text('0.00');
    $('#3lblOSTOTAL').text('0.00');
    $('#3fteJAN').text('0.00');
    $('#3fteFEB').text('0.00');
    $('#3fteMAR').text('0.00');
    $('#3fteAPR').text('0.00');
    $('#3fteMAY').text('0.00');
    $('#3fteJUN').text('0.00');
    $('#3fteJUL').text('0.00');
    $('#3fteAUG').text('0.00');
    $('#3fteSEP').text('0.00');
    $('#3fteOCT').text('0.00');
    $('#3fteNOV').text('0.00');
    $('#3fteDEC').text('0.00');
    $('#3fteTOTAL').text('0.00');
    //$('#3trainCostTotal').text('0.00');
    //$('#3tecCostTOTAL').text('0.00');
    $('#3FTETotalCostJAN').text('0.00');
    $('#3FTETotalCostFEB').text('0.00');
    $('#3FTETotalCostMAR').text('0.00');
    $('#3FTETotalCostAPR').text('0.00');
    $('#3FTETotalCostMAY').text('0.00');
    $('#3FTETotalCostJUN').text('0.00');
    $('#3FTETotalCostJUL').text('0.00');
    $('#3FTETotalCostAUG').text('0.00');
    $('#3FTETotalCostSEP').text('0.00');
    $('#3FTETotalCostOCT').text('0.00');
    $('#3FTETotalCostNOV').text('0.00');
    $('#3FTETotalCostDEC').text('0.00');
    $('#3FTETotalCostTOTAL').text('0.00');

    var myStringArray = [
                '#dsJan',
                '#dsFeb',
                '#dsMar',
                '#dsApr',
                '#dsMay',
                '#dsJun',
                '#dsJul',
                '#dsAug',
                '#dsSep',
                '#dsOct',
                '#dsNov',
                '#dsDec',
                '#2dsJan',
                '#2dsFeb',
                '#2dsMar',
                '#2dsApr',
                '#2dsMay',
                '#2dsJun',
                '#2dsJul',
                '#2dsAug',
                '#2dsSep',
                '#2dsOct',
                '#2dsNov',
                '#2dsDec',
                '#3dsJan',
                '#3dsFeb',
                '#3dsMar',
                '#3dsApr',
                '#3dsMay',
                '#3dsJun',
                '#3dsJul',
                '#3dsAug',
                '#3dsSep',
                '#3dsOct',
                '#3dsNov',
                '#3dsDec',
    ];

    var myStringArray2 = [
                        '#osJAN',
                        '#osFEB',
                        '#osMAR',
                        '#osAPR',
                        '#osMAY',
                        '#osJUN',
                        '#osJUL',
                        '#osAUG',
                        '#osSEP',
                        '#osOCT',
                        '#osNOV',
                        '#osDEC',
                        '#2osJAN',
                        '#2osFEB',
                        '#2osMAR',
                        '#2osAPR',
                        '#2osMAY',
                        '#2osJUN',
                        '#2osJUL',
                        '#2osAUG',
                        '#2osSEP',
                        '#2osOCT',
                        '#2osNOV',
                        '#2osDEC',
                        '#3osJAN',
                        '#3osFEB',
                        '#3osMAR',
                        '#3osAPR',
                        '#3osMAY',
                        '#3osJUN',
                        '#3osJUL',
                        '#3osAUG',
                        '#3osSEP',
                        '#3osOCT',
                        '#3osNOV',
                        '#3osDEC'
    ];

    var myStringArray3 = [
    '#receivingFTE'
    ];


    var arrayLength = myStringArray.length;
    for (var i = 0; i < arrayLength; i++) {
        var inputNameTag = myStringArray[i];

        $(inputNameTag).jqxNumberInput({ inputMode: 'simple', min: 0,digits: 6, width: '70px', height: '25px', inputMode: 'simple', spinButtons: true, spinButtonsStep: 1 });

        $(inputNameTag).on('valueChanged', function (event) {

            var name = '#' + event.currentTarget.id;
            var value = parseFloat(event.args.value);

            if (parseFloat(value) < parseFloat(0.00)) {
                $(name + " > :input").css("background-color", "#ff8080");
            }
            if (parseFloat(value) == parseFloat(0.00)) {
                $(name + " > :input").css("background-color", "#FFFFFF");
            }
            if (parseFloat(value) > parseFloat(0.00)) {
                $(name + " > :input").css("background-color", "#99CCFF");
            }
            calculateDetail();
        });
    }


    var arrayLength2 = myStringArray2.length;
    for (var i = 0; i < arrayLength2; i++) {
        var inputNameTag = myStringArray2[i];

        $(inputNameTag).jqxNumberInput({ disabled: true, inputMode: 'simple', digits: 6, width: '70px', height: '25px', inputMode: 'simple', spinButtons: false, spinButtonsStep: 1 });

        $(inputNameTag).on('valueChanged', function (event) {

            var name = '#' + event.currentTarget.id;
            var value = parseFloat(event.args.value);

            if (parseFloat(value) < parseFloat(0.00)) {
                $(name + " > :input").css("background-color", "#ff8080");
            }
            if (parseFloat(value) == parseFloat(0.00)) {
                $(name + " > :input").css("background-color", "#FFFFFF");
            }
            if (parseFloat(value) > parseFloat(0.00)) {
                $(name + " > :input").css("background-color", "#99CCFF");
            }
            calculateDetail();
        });
    }

    var arrayLength = myStringArray3.length;
    for (var i = 0; i < arrayLength; i++) {
        var inputNameTag = myStringArray3[i];

        $(inputNameTag).jqxNumberInput({ inputMode: 'simple', width: '200px', digits: 6, height: '25px', inputMode: 'simple', spinButtons: true, spinButtonsStep: 1, symbol: '$' });

        $(inputNameTag).on('valueChanged', function (event) {

            var name = '#' + event.currentTarget.id;
            var value = parseFloat(event.args.value);

            if (parseFloat(value) < parseFloat(0.00)) {
                $(name + " > :input").css("background-color", "#ff8080");
            }
            if (parseFloat(value) == parseFloat(0.00)) {
                $(name + " > :input").css("background-color", "#FFFFFF");
            }
            if (parseFloat(value) > parseFloat(0.00)) {
                $(name + " > :input").css("background-color", "#99CCFF");
            }
            calculateDetail();
        });
    }


    getFinancialDetail();
    calculateDetail();
}

function calculateDetail() {
    var totalDirectStaff = $('#dsJan').val() + $('#dsFeb').val() + $('#dsMar').val() + $('#dsApr').val() + $('#dsMay').val() + $('#dsJun').val() + $('#dsJul').val() + $('#dsAug').val() + $('#dsSep').val() + $('#dsOct').val() + $('#dsNov').val() + $('#dsDec').val();
    var totaltotalDirectStaff2 = $('#2dsJan').val() + $('#2dsFeb').val() + $('#2dsMar').val() + $('#2dsApr').val() + $('#2dsMay').val() + $('#2dsJun').val() + $('#2dsJul').val() + $('#2dsAug').val() + $('#2dsSep').val() + $('#2dsOct').val() + $('#2dsNov').val() + $('#2dsDec').val();

    var totaltotalDirectStaff3 = $('#3dsJan').val() + $('#3dsFeb').val() + $('#3dsMar').val() + $('#3dsApr').val() + $('#3dsMay').val() + $('#3dsJun').val() + $('#3dsJul').val() + $('#3dsAug').val() + $('#3dsSep').val() + $('#3dsOct').val() + $('#3dsNov').val() + $('#3dsDec').val();


    $('#dsTotal').text(parseFloat(totalDirectStaff).toFixed(2));
    $('#2dsTotal').text(parseFloat(totaltotalDirectStaff2).toFixed(2));
    $('#3dsTotal').text(parseFloat(totaltotalDirectStaff3).toFixed(2));

    $('#lblDSTotal').text(parseFloat(totalDirectStaff).toFixed(2));
    $('#2lblDSTotal').text(parseFloat(totalDirectStaff + totaltotalDirectStaff2).toFixed(2));
    $('#3lblDSTotal').text(parseFloat(totalDirectStaff + totaltotalDirectStaff2 + totaltotalDirectStaff3).toFixed(2));

    $('#lblDSJAN').text(parseFloat($('#dsJan').val()).toFixed(2));
    $('#lblDSFEB').text(parseFloat($('#dsJan').val() + $('#dsFeb').val()).toFixed(2));
    $('#lblDSMAR').text(parseFloat($('#dsJan').val() + $('#dsFeb').val() + $('#dsMar').val()).toFixed(2));
    $('#lblDSAPR').text(parseFloat($('#dsJan').val() + $('#dsFeb').val() + $('#dsMar').val() + $('#dsApr').val()).toFixed(2));
    $('#lblDSMAY').text(parseFloat($('#dsJan').val() + $('#dsFeb').val() + $('#dsMar').val() + $('#dsApr').val() + $('#dsMay').val()).toFixed(2));
    $('#lblDSJUN').text(parseFloat($('#dsJan').val() + $('#dsFeb').val() + $('#dsMar').val() + $('#dsApr').val() + $('#dsMay').val() + $('#dsJun').val()).toFixed(2));
    $('#lblDSJUL').text(parseFloat($('#dsJan').val() + $('#dsFeb').val() + $('#dsMar').val() + $('#dsApr').val() + $('#dsMay').val() + $('#dsJun').val() + $('#dsJul').val()).toFixed(2));
    $('#lblDSAUG').text(parseFloat($('#dsJan').val() + $('#dsFeb').val() + $('#dsMar').val() + $('#dsApr').val() + $('#dsMay').val() + $('#dsJun').val() + $('#dsJul').val() + $('#dsAug').val()).toFixed(2));
    $('#lblDSSEP').text(parseFloat($('#dsJan').val() + $('#dsFeb').val() + $('#dsMar').val() + $('#dsApr').val() + $('#dsMay').val() + $('#dsJun').val() + $('#dsJul').val() + $('#dsAug').val() + $('#dsSep').val()).toFixed(2));
    $('#lblDSOCT').text(parseFloat($('#dsJan').val() + $('#dsFeb').val() + $('#dsMar').val() + $('#dsApr').val() + $('#dsMay').val() + $('#dsJun').val() + $('#dsJul').val() + $('#dsAug').val() + $('#dsSep').val() + $('#dsOct').val()).toFixed(2));
    $('#lblDSNOV').text(parseFloat($('#dsJan').val() + $('#dsFeb').val() + $('#dsMar').val() + $('#dsApr').val() + $('#dsMay').val() + $('#dsJun').val() + $('#dsJul').val() + $('#dsAug').val() + $('#dsSep').val() + $('#dsOct').val() + $('#dsNov').val()).toFixed(2));
    $('#lblDSDEC').text(parseFloat($('#dsJan').val() + $('#dsFeb').val() + $('#dsMar').val() + $('#dsApr').val() + $('#dsMay').val() + $('#dsJun').val() + $('#dsJul').val() + $('#dsAug').val() + $('#dsSep').val() + $('#dsOct').val() + $('#dsNov').val() + $('#dsDec').val()).toFixed(2));

    $('#2lblDSJAN').text(parseFloat(totalDirectStaff + $('#2dsJan').val()).toFixed(2));
    $('#2lblDSFEB').text(parseFloat(totalDirectStaff + $('#2dsJan').val() + $('#2dsFeb').val()).toFixed(2));
    $('#2lblDSMAR').text(parseFloat(totalDirectStaff + $('#2dsJan').val() + $('#2dsFeb').val() + $('#2dsMar').val()).toFixed(2));
    $('#2lblDSAPR').text(parseFloat(totalDirectStaff + $('#2dsJan').val() + $('#2dsFeb').val() + $('#2dsMar').val() + $('#2dsApr').val()).toFixed(2));
    $('#2lblDSMAY').text(parseFloat(totalDirectStaff + $('#2dsJan').val() + $('#2dsFeb').val() + $('#2dsMar').val() + $('#2dsApr').val() + $('#2dsMay').val()).toFixed(2));
    $('#2lblDSJUN').text(parseFloat(totalDirectStaff + $('#2dsJan').val() + $('#2dsFeb').val() + $('#2dsMar').val() + $('#2dsApr').val() + $('#2dsMay').val() + $('#2dsJun').val()).toFixed(2));
    $('#2lblDSJUL').text(parseFloat(totalDirectStaff + $('#2dsJan').val() + $('#2dsFeb').val() + $('#2dsMar').val() + $('#2dsApr').val() + $('#2dsMay').val() + $('#2dsJun').val() + $('#2dsJul').val()).toFixed(2));
    $('#2lblDSAUG').text(parseFloat(totalDirectStaff + $('#2dsJan').val() + $('#2dsFeb').val() + $('#2dsMar').val() + $('#2dsApr').val() + $('#2dsMay').val() + $('#2dsJun').val() + $('#2dsJul').val() + $('#2dsAug').val()).toFixed(2));
    $('#2lblDSSEP').text(parseFloat(totalDirectStaff + $('#2dsJan').val() + $('#2dsFeb').val() + $('#2dsMar').val() + $('#2dsApr').val() + $('#2dsMay').val() + $('#2dsJun').val() + $('#2dsJul').val() + $('#2dsAug').val() + $('#2dsSep').val()).toFixed(2));
    $('#2lblDSOCT').text(parseFloat(totalDirectStaff + $('#2dsJan').val() + $('#2dsFeb').val() + $('#2dsMar').val() + $('#2dsApr').val() + $('#2dsMay').val() + $('#2dsJun').val() + $('#2dsJul').val() + $('#2dsAug').val() + $('#2dsSep').val() + $('#2dsOct').val()).toFixed(2));
    $('#2lblDSNOV').text(parseFloat(totalDirectStaff + $('#2dsJan').val() + $('#2dsFeb').val() + $('#2dsMar').val() + $('#2dsApr').val() + $('#2dsMay').val() + $('#2dsJun').val() + $('#2dsJul').val() + $('#2dsAug').val() + $('#2dsSep').val() + $('#2dsOct').val() + $('#2dsNov').val()).toFixed(2));
    $('#2lblDSDEC').text(parseFloat(totalDirectStaff + $('#2dsJan').val() + $('#2dsFeb').val() + $('#2dsMar').val() + $('#2dsApr').val() + $('#2dsMay').val() + $('#2dsJun').val() + $('#2dsJul').val() + $('#2dsAug').val() + $('#2dsSep').val() + $('#2dsOct').val() + $('#2dsNov').val() + $('#2dsDec').val()).toFixed(2));

    $('#3lblDSJAN').text(parseFloat(totalDirectStaff + totaltotalDirectStaff2 + $('#3dsJan').val()).toFixed(2));
    $('#3lblDSFEB').text(parseFloat(totalDirectStaff + totaltotalDirectStaff2 + $('#3dsJan').val() + $('#3dsFeb').val()).toFixed(2));
    $('#3lblDSMAR').text(parseFloat(totalDirectStaff + totaltotalDirectStaff2 + $('#3dsJan').val() + $('#3dsFeb').val() + $('#3dsMar').val()).toFixed(2));
    $('#3lblDSAPR').text(parseFloat(totalDirectStaff + totaltotalDirectStaff2 + $('#3dsJan').val() + $('#3dsFeb').val() + $('#3dsMar').val() + $('#3dsApr').val()).toFixed(2));
    $('#3lblDSMAY').text(parseFloat(totalDirectStaff + totaltotalDirectStaff2 + $('#3dsJan').val() + $('#3dsFeb').val() + $('#3dsMar').val() + $('#3dsApr').val() + $('#3dsMay').val()).toFixed(2));
    $('#3lblDSJUN').text(parseFloat(totalDirectStaff + totaltotalDirectStaff2 + $('#3dsJan').val() + $('#3dsFeb').val() + $('#3dsMar').val() + $('#3dsApr').val() + $('#3dsMay').val() + $('#3dsJun').val()).toFixed(2));
    $('#3lblDSJUL').text(parseFloat(totalDirectStaff + totaltotalDirectStaff2 + $('#3dsJan').val() + $('#3dsFeb').val() + $('#3dsMar').val() + $('#3dsApr').val() + $('#3dsMay').val() + $('#3dsJun').val() + $('#3dsJul').val()).toFixed(2));
    $('#3lblDSAUG').text(parseFloat(totalDirectStaff + totaltotalDirectStaff2 + $('#3dsJan').val() + $('#3dsFeb').val() + $('#3dsMar').val() + $('#3dsApr').val() + $('#3dsMay').val() + $('#3dsJun').val() + $('#3dsJul').val() + $('#3dsAug').val()).toFixed(2));
    $('#3lblDSSEP').text(parseFloat(totalDirectStaff + totaltotalDirectStaff2 + $('#3dsJan').val() + $('#3dsFeb').val() + $('#3dsMar').val() + $('#3dsApr').val() + $('#3dsMay').val() + $('#3dsJun').val() + $('#3dsJul').val() + $('#3dsAug').val() + $('#3dsSep').val()).toFixed(2));
    $('#3lblDSOCT').text(parseFloat(totalDirectStaff + totaltotalDirectStaff2 + $('#3dsJan').val() + $('#3dsFeb').val() + $('#3dsMar').val() + $('#3dsApr').val() + $('#3dsMay').val() + $('#3dsJun').val() + $('#3dsJul').val() + $('#3dsAug').val() + $('#3dsSep').val() + $('#3dsOct').val()).toFixed(2));
    $('#3lblDSNOV').text(parseFloat(totalDirectStaff + totaltotalDirectStaff2 + $('#3dsJan').val() + $('#3dsFeb').val() + $('#3dsMar').val() + $('#3dsApr').val() + $('#3dsMay').val() + $('#3dsJun').val() + $('#3dsJul').val() + $('#3dsAug').val() + $('#3dsSep').val() + $('#3dsOct').val() + $('#3dsNov').val()).toFixed(2));
    $('#3lblDSDEC').text(parseFloat(totalDirectStaff + totaltotalDirectStaff2 + $('#3dsJan').val() + $('#3dsFeb').val() + $('#3dsMar').val() + $('#3dsApr').val() + $('#3dsMay').val() + $('#3dsJun').val() + $('#3dsJul').val() + $('#3dsAug').val() + $('#3dsSep').val() + $('#3dsOct').val() + $('#3dsNov').val() + $('#3dsDec').val()).toFixed(2));


    var totalOder = $('#osJAN').val() + $('#osFEB').val() + $('#osMAR').val() + $('#osAPR').val() + $('#osMAY').val() + $('#osJUN').val() + $('#osJUL').val() + $('#osAUG').val() + $('#osSEP').val() + $('#osOCT').val() + $('#osNOV').val() + $('#osDEC').val();
    var totalOder2 = $('#2osJAN').val() + $('#2osFEB').val() + $('#2osMAR').val() + $('#2osAPR').val() + $('#2osMAY').val() + $('#2osJUN').val() + $('#2osJUL').val() + $('#2osAUG').val() + $('#2osSEP').val() + $('#2osOCT').val() + $('#2osNOV').val() + $('#2osDEC').val();
    var totalOder3 = $('#3osJAN').val() + $('#3osFEB').val() + $('#3osMAR').val() + $('#3osAPR').val() + $('#3osMAY').val() + $('#3osJUN').val() + $('#3osJUL').val() + $('#3osAUG').val() + $('#3osSEP').val() + $('#3osOCT').val() + $('#3osNOV').val() + $('#3osDEC').val();


    $('#osTotal').text(parseFloat(totalOder).toFixed(2));
    $('#2osTotal').text(parseFloat(totalOder2).toFixed(2));
    $('#3osTotal').text(parseFloat(totalOder3).toFixed(2));

    $('#lblOSTOTAL').text(parseFloat(totalOder).toFixed(2));
    $('#2lblOSTOTAL').text(parseFloat(totalOder + totalOder2).toFixed(2));
    $('#3lblOSTOTAL').text(parseFloat(totalOder + totalOder2 + totalOder3).toFixed(2));

    $('#lblOSJAN').text(parseFloat($('#osJAN').val()).toFixed(2));
    $('#lblOSFEB').text(parseFloat($('#osJAN').val() + $('#osFEB').val()).toFixed(2));
    $('#lblOSMAR').text(parseFloat($('#osJAN').val() + $('#osFEB').val() + $('#osMAR').val()).toFixed(2));
    $('#lblOSAPR').text(parseFloat($('#osJAN').val() + $('#osFEB').val() + $('#osMAR').val() + $('#osAPR').val()).toFixed(2));
    $('#lblOSMAY').text(parseFloat($('#osJAN').val() + $('#osFEB').val() + $('#osMAR').val() + $('#osAPR').val() + $('#osMAY').val()).toFixed(2));
    $('#lblOSJUN').text(parseFloat($('#osJAN').val() + $('#osFEB').val() + $('#osMAR').val() + $('#osAPR').val() + $('#osMAY').val() + $('#osJUN').val()).toFixed(2));
    $('#lblOSJUL').text(parseFloat($('#osJAN').val() + $('#osFEB').val() + $('#osMAR').val() + $('#osAPR').val() + $('#osMAY').val() + $('#osJUN').val() + $('#osJUL').val()).toFixed(2));
    $('#lblOSAUG').text(parseFloat($('#osJAN').val() + $('#osFEB').val() + $('#osMAR').val() + $('#osAPR').val() + $('#osMAY').val() + $('#osJUN').val() + $('#osJUL').val() + $('#osAUG').val()).toFixed(2));
    $('#lblOSSEP').text(parseFloat($('#osJAN').val() + $('#osFEB').val() + $('#osMAR').val() + $('#osAPR').val() + $('#osMAY').val() + $('#osJUN').val() + $('#osJUL').val() + $('#osAUG').val() + $('#osSEP').val()).toFixed(2));
    $('#lblOSOCT').text(parseFloat($('#osJAN').val() + $('#osFEB').val() + $('#osMAR').val() + $('#osAPR').val() + $('#osMAY').val() + $('#osJUN').val() + $('#osJUL').val() + $('#osAUG').val() + $('#osSEP').val() + $('#osOCT').val()).toFixed(2));
    $('#lblOSNOV').text(parseFloat($('#osJAN').val() + $('#osFEB').val() + $('#osMAR').val() + $('#osAPR').val() + $('#osMAY').val() + $('#osJUN').val() + $('#osJUL').val() + $('#osAUG').val() + $('#osSEP').val() + $('#osOCT').val() + $('#osNOV').val()).toFixed(2));
    $('#lblOSDEC').text(parseFloat($('#osJAN').val() + $('#osFEB').val() + $('#osMAR').val() + $('#osAPR').val() + $('#osMAY').val() + $('#osJUN').val() + $('#osJUL').val() + $('#osAUG').val() + $('#osSEP').val() + $('#osOCT').val() + $('#osNOV').val() + $('#osDEC').val()).toFixed(2));

    $('#2lblOSJAN').text(parseFloat(totalOder + $('#2osJAN').val()).toFixed(2));
    $('#2lblOSFEB').text(parseFloat(totalOder + $('#2osJAN').val() + $('#2osFEB').val()).toFixed(2));
    $('#2lblOSMAR').text(parseFloat(totalOder + $('#2osJAN').val() + $('#2osFEB').val() + $('#2osMAR').val()).toFixed(2));
    $('#2lblOSAPR').text(parseFloat(totalOder + $('#2osJAN').val() + $('#2osFEB').val() + $('#2osMAR').val() + $('#2osAPR').val()).toFixed(2));
    $('#2lblOSMAY').text(parseFloat(totalOder + $('#2osJAN').val() + $('#2osFEB').val() + $('#2osMAR').val() + $('#2osAPR').val() + $('#2osMAY').val()).toFixed(2));
    $('#2lblOSJUN').text(parseFloat(totalOder + $('#2osJAN').val() + $('#2osFEB').val() + $('#2osMAR').val() + $('#2osAPR').val() + $('#2osMAY').val() + $('#2osJUN').val()).toFixed(2));
    $('#2lblOSJUL').text(parseFloat(totalOder + $('#2osJAN').val() + $('#2osFEB').val() + $('#2osMAR').val() + $('#2osAPR').val() + $('#2osMAY').val() + $('#2osJUN').val() + $('#2osJUL').val()).toFixed(2));
    $('#2lblOSAUG').text(parseFloat(totalOder + $('#2osJAN').val() + $('#2osFEB').val() + $('#2osMAR').val() + $('#2osAPR').val() + $('#2osMAY').val() + $('#2osJUN').val() + $('#2osJUL').val() + $('#2osAUG').val()).toFixed(2));
    $('#2lblOSSEP').text(parseFloat(totalOder + $('#2osJAN').val() + $('#2osFEB').val() + $('#2osMAR').val() + $('#2osAPR').val() + $('#2osMAY').val() + $('#2osJUN').val() + $('#2osJUL').val() + $('#2osAUG').val() + $('#2osSEP').val()).toFixed(2));
    $('#2lblOSOCT').text(parseFloat(totalOder + $('#2osJAN').val() + $('#2osFEB').val() + $('#2osMAR').val() + $('#2osAPR').val() + $('#2osMAY').val() + $('#2osJUN').val() + $('#2osJUL').val() + $('#2osAUG').val() + $('#2osSEP').val() + $('#2osOCT').val()).toFixed(2));
    $('#2lblOSNOV').text(parseFloat(totalOder + $('#2osJAN').val() + $('#2osFEB').val() + $('#2osMAR').val() + $('#2osAPR').val() + $('#2osMAY').val() + $('#2osJUN').val() + $('#2osJUL').val() + $('#2osAUG').val() + $('#2osSEP').val() + $('#2osOCT').val() + $('#2osNOV').val()).toFixed(2));
    $('#2lblOSDEC').text(parseFloat(totalOder + $('#2osJAN').val() + $('#2osFEB').val() + $('#2osMAR').val() + $('#2osAPR').val() + $('#2osMAY').val() + $('#2osJUN').val() + $('#2osJUL').val() + $('#2osAUG').val() + $('#2osSEP').val() + $('#2osOCT').val() + $('#2osNOV').val() + $('#2osDEC').val()).toFixed(2));

    $('#3lblOSJAN').text(parseFloat(totalOder + totalOder2 + $('#3osJAN').val()).toFixed(2));
    $('#3lblOSFEB').text(parseFloat(totalOder + totalOder2 + $('#3osJAN').val() + $('#3osFEB').val()).toFixed(2));
    $('#3lblOSMAR').text(parseFloat(totalOder + totalOder2 + $('#3osJAN').val() + $('#3osFEB').val() + $('#3osMAR').val()).toFixed(2));
    $('#3lblOSAPR').text(parseFloat(totalOder + totalOder2 + $('#3osJAN').val() + $('#3osFEB').val() + $('#3osMAR').val() + $('#3osAPR').val()).toFixed(2));
    $('#3lblOSMAY').text(parseFloat(totalOder + totalOder2 + $('#3osJAN').val() + $('#3osFEB').val() + $('#3osMAR').val() + $('#3osAPR').val() + $('#3osMAY').val()).toFixed(2));
    $('#3lblOSJUN').text(parseFloat(totalOder + totalOder2 + $('#3osJAN').val() + $('#3osFEB').val() + $('#3osMAR').val() + $('#3osAPR').val() + $('#3osMAY').val() + $('#3osJUN').val()).toFixed(2));
    $('#3lblOSJUL').text(parseFloat(totalOder + totalOder2 + $('#3osJAN').val() + $('#3osFEB').val() + $('#3osMAR').val() + $('#3osAPR').val() + $('#3osMAY').val() + $('#3osJUN').val() + $('#3osJUL').val()).toFixed(2));
    $('#3lblOSAUG').text(parseFloat(totalOder + totalOder2 + $('#3osJAN').val() + $('#3osFEB').val() + $('#3osMAR').val() + $('#3osAPR').val() + $('#3osMAY').val() + $('#3osJUN').val() + $('#3osJUL').val() + $('#3osAUG').val()).toFixed(2));
    $('#3lblOSSEP').text(parseFloat(totalOder + totalOder2 + $('#3osJAN').val() + $('#3osFEB').val() + $('#3osMAR').val() + $('#3osAPR').val() + $('#3osMAY').val() + $('#3osJUN').val() + $('#3osJUL').val() + $('#3osAUG').val() + $('#3osSEP').val()).toFixed(2));
    $('#3lblOSOCT').text(parseFloat(totalOder + totalOder2 + $('#3osJAN').val() + $('#3osFEB').val() + $('#3osMAR').val() + $('#3osAPR').val() + $('#3osMAY').val() + $('#3osJUN').val() + $('#3osJUL').val() + $('#3osAUG').val() + $('#3osSEP').val() + $('#3osOCT').val()).toFixed(2));
    $('#3lblOSNOV').text(parseFloat(totalOder + totalOder2 + $('#3osJAN').val() + $('#3osFEB').val() + $('#3osMAR').val() + $('#3osAPR').val() + $('#3osMAY').val() + $('#3osJUN').val() + $('#3osJUL').val() + $('#3osAUG').val() + $('#3osSEP').val() + $('#3osOCT').val() + $('#3osNOV').val()).toFixed(2));
    $('#3lblOSDEC').text(parseFloat(totalOder + totalOder2 + $('#3osJAN').val() + $('#3osFEB').val() + $('#3osMAR').val() + $('#3osAPR').val() + $('#3osMAY').val() + $('#3osJUN').val() + $('#3osJUL').val() + $('#3osAUG').val() + $('#3osSEP').val() + $('#3osOCT').val() + $('#3osNOV').val() + $('#3osDEC').val()).toFixed(2));

    $('#fteTOTAL').text(parseFloat(totalDirectStaff + totalOder).toFixed(2));
    $('#2fteTOTAL').text(parseFloat(totalDirectStaff + totaltotalDirectStaff2 + totalOder + totalOder2).toFixed(2));
    $('#3fteTOTAL').text(parseFloat(totalDirectStaff + totaltotalDirectStaff2 + totaltotalDirectStaff3 + totalOder + totalOder2 + totalOder3).toFixed(2));

    $('#fteJAN').text(parseFloat($('#dsJan').val() + $('#osJAN').val()).toFixed(2));
    $('#fteFEB').text(parseFloat($('#dsJan').val() + $('#osJAN').val() +
                                   $('#dsFeb').val() + $('#osFEB').val()).toFixed(2));
    $('#fteMAR').text(parseFloat($('#dsJan').val() + $('#osJAN').val() +
                                   $('#dsFeb').val() + $('#osFEB').val() +
                                   $('#dsMar').val() + $('#osMAR').val()).toFixed(2));
    $('#fteAPR').text(parseFloat($('#dsJan').val() + $('#osJAN').val() +
                                   $('#dsFeb').val() + $('#osFEB').val() +
                                   $('#dsMar').val() + $('#osMAR').val() +
                                   $('#dsApr').val() + $('#osAPR').val()).toFixed(2));
    $('#fteMAY').text(parseFloat($('#dsJan').val() + $('#osJAN').val() +
                                   $('#dsFeb').val() + $('#osFEB').val() +
                                   $('#dsMar').val() + $('#osMAR').val() +
                                   $('#dsApr').val() + $('#osAPR').val() +
                                   $('#dsMay').val() + $('#osMAY').val()).toFixed(2));
    $('#fteJUN').text(parseFloat($('#dsJan').val() + $('#osJAN').val() +
                                   $('#dsFeb').val() + $('#osFEB').val() +
                                   $('#dsMar').val() + $('#osMAR').val() +
                                   $('#dsApr').val() + $('#osAPR').val() +
                                   $('#dsMay').val() + $('#osMAY').val() +
                                   $('#dsJun').val() + $('#osJUN').val()).toFixed(2));
    $('#fteJUL').text(parseFloat($('#dsJan').val() + $('#osJAN').val() +
                                   $('#dsFeb').val() + $('#osFEB').val() +
                                   $('#dsMar').val() + $('#osMAR').val() +
                                   $('#dsApr').val() + $('#osAPR').val() +
                                   $('#dsMay').val() + $('#osMAY').val() +
                                   $('#dsJun').val() + $('#osJUN').val() +
                                   $('#dsJul').val() + $('#osJUL').val()).toFixed(2));
    $('#fteAUG').text(parseFloat($('#dsJan').val() + $('#osJAN').val() +
                                   $('#dsFeb').val() + $('#osFEB').val() +
                                   $('#dsMar').val() + $('#osMAR').val() +
                                   $('#dsApr').val() + $('#osAPR').val() +
                                   $('#dsMay').val() + $('#osMAY').val() +
                                   $('#dsJun').val() + $('#osJUN').val() +
                                   $('#dsJul').val() + $('#osJUL').val() +
                                   $('#dsAug').val() + $('#osAUG').val()).toFixed(2));
    $('#fteSEP').text(parseFloat($('#dsJan').val() + $('#osJAN').val() +
                                   $('#dsFeb').val() + $('#osFEB').val() +
                                   $('#dsMar').val() + $('#osMAR').val() +
                                   $('#dsApr').val() + $('#osAPR').val() +
                                   $('#dsMay').val() + $('#osMAY').val() +
                                   $('#dsJun').val() + $('#osJUN').val() +
                                   $('#dsJul').val() + $('#osJUL').val() +
                                   $('#dsAug').val() + $('#osAUG').val() +
                                   $('#dsSep').val() + $('#osSEP').val()).toFixed(2));
    $('#fteOCT').text(parseFloat($('#dsJan').val() + $('#osJAN').val() +
                                   $('#dsFeb').val() + $('#osFEB').val() +
                                   $('#dsMar').val() + $('#osMAR').val() +
                                   $('#dsApr').val() + $('#osAPR').val() +
                                   $('#dsMay').val() + $('#osMAY').val() +
                                   $('#dsJun').val() + $('#osJUN').val() +
                                   $('#dsJul').val() + $('#osJUL').val() +
                                   $('#dsAug').val() + $('#osAUG').val() +
                                   $('#dsSep').val() + $('#osSEP').val() +
                                   $('#dsOct').val() + $('#osOCT').val()).toFixed(2));
    $('#fteNOV').text(parseFloat($('#dsJan').val() + $('#osJAN').val() +
                                   $('#dsFeb').val() + $('#osFEB').val() +
                                   $('#dsMar').val() + $('#osMAR').val() +
                                   $('#dsApr').val() + $('#osAPR').val() +
                                   $('#dsMay').val() + $('#osMAY').val() +
                                   $('#dsJun').val() + $('#osJUN').val() +
                                   $('#dsJul').val() + $('#osJUL').val() +
                                   $('#dsAug').val() + $('#osAUG').val() +
                                   $('#dsSep').val() + $('#osSEP').val() +
                                   $('#dsOct').val() + $('#osOCT').val() +
                                   $('#dsNov').val() + $('#osNOV').val()).toFixed(2));
    $('#fteDEC').text(parseFloat($('#dsJan').val() + $('#osJAN').val() +
                                   $('#dsFeb').val() + $('#osFEB').val() +
                                   $('#dsMar').val() + $('#osMAR').val() +
                                   $('#dsApr').val() + $('#osAPR').val() +
                                   $('#dsMay').val() + $('#osMAY').val() +
                                   $('#dsJun').val() + $('#osJUN').val() +
                                   $('#dsJul').val() + $('#osJUL').val() +
                                   $('#dsAug').val() + $('#osAUG').val() +
                                   $('#dsSep').val() + $('#osSEP').val() +
                                   $('#dsOct').val() + $('#osOCT').val() +
                                   $('#dsNov').val() + $('#osNOV').val() +
                                   $('#dsDec').val() + $('#osDEC').val()).toFixed(2));

    $('#2fteJAN').text(parseFloat(totalDirectStaff + totalOder + $('#2dsJan').val() + $('#2osJAN').val()).toFixed(2));
    $('#2fteFEB').text(parseFloat(totalDirectStaff + totalOder + $('#2dsJan').val() + $('#2osJAN').val() +
                                   $('#2dsFeb').val() + $('#2osFEB').val()).toFixed(2));
    $('#2fteMAR').text(parseFloat(totalDirectStaff + totalOder + $('#2dsJan').val() + $('#2osJAN').val() +
                                   $('#2dsFeb').val() + $('#2osFEB').val() +
                                   $('#2dsMar').val() + $('#2osMAR').val()).toFixed(2));
    $('#2fteAPR').text(parseFloat(totalDirectStaff + totalOder + $('#2dsJan').val() + $('#2osJAN').val() +
                                   $('#2dsFeb').val() + $('#2osFEB').val() +
                                   $('#2dsMar').val() + $('#2osMAR').val() +
                                   $('#2dsApr').val() + $('#2osAPR').val()).toFixed(2));
    $('#2fteMAY').text(parseFloat(totalDirectStaff + totalOder + $('#2dsJan').val() + $('#2osJAN').val() +
                                   $('#2dsFeb').val() + $('#2osFEB').val() +
                                   $('#2dsMar').val() + $('#2osMAR').val() +
                                   $('#2dsApr').val() + $('#2osAPR').val() +
                                   $('#2dsMay').val() + $('#2osMAY').val()).toFixed(2));
    $('#2fteJUN').text(parseFloat(totalDirectStaff + totalOder + $('#2dsJan').val() + $('#2osJAN').val() +
                                   $('#2dsFeb').val() + $('#2osFEB').val() +
                                   $('#2dsMar').val() + $('#2osMAR').val() +
                                   $('#2dsApr').val() + $('#2osAPR').val() +
                                   $('#2dsMay').val() + $('#2osMAY').val() +
                                   $('#2dsJun').val() + $('#2osJUN').val()).toFixed(2));
    $('#2fteJUL').text(parseFloat(totalDirectStaff + totalOder + $('#2dsJan').val() + $('#2osJAN').val() +
                                   $('#2dsFeb').val() + $('#2osFEB').val() +
                                   $('#2dsMar').val() + $('#2osMAR').val() +
                                   $('#2dsApr').val() + $('#2osAPR').val() +
                                   $('#2dsMay').val() + $('#2osMAY').val() +
                                   $('#2dsJun').val() + $('#2osJUN').val() +
                                   $('#2dsJul').val() + $('#2osJUL').val()).toFixed(2));
    $('#2fteAUG').text(parseFloat(totalDirectStaff + totalOder + $('#2dsJan').val() + $('#2osJAN').val() +
                                   $('#2dsFeb').val() + $('#2osFEB').val() +
                                   $('#2dsMar').val() + $('#2osMAR').val() +
                                   $('#2dsApr').val() + $('#2osAPR').val() +
                                   $('#2dsMay').val() + $('#2osMAY').val() +
                                   $('#2dsJun').val() + $('#2osJUN').val() +
                                   $('#2dsJul').val() + $('#2osJUL').val() +
                                   $('#2dsAug').val() + $('#2osAUG').val()).toFixed(2));
    $('#2fteSEP').text(parseFloat(totalDirectStaff + totalOder + $('#2dsJan').val() + $('#2osJAN').val() +
                                   $('#2dsFeb').val() + $('#2osFEB').val() +
                                   $('#2dsMar').val() + $('#2osMAR').val() +
                                   $('#2dsApr').val() + $('#2osAPR').val() +
                                   $('#2dsMay').val() + $('#2osMAY').val() +
                                   $('#2dsJun').val() + $('#2osJUN').val() +
                                   $('#2dsJul').val() + $('#2osJUL').val() +
                                   $('#2dsAug').val() + $('#2osAUG').val() +
                                   $('#2dsSep').val() + $('#2osSEP').val()).toFixed(2));
    $('#2fteOCT').text(parseFloat(totalDirectStaff + totalOder + $('#2dsJan').val() + $('#2osJAN').val() +
                                   $('#2dsFeb').val() + $('#2osFEB').val() +
                                   $('#2dsMar').val() + $('#2osMAR').val() +
                                   $('#2dsApr').val() + $('#2osAPR').val() +
                                   $('#2dsMay').val() + $('#2osMAY').val() +
                                   $('#2dsJun').val() + $('#2osJUN').val() +
                                   $('#2dsJul').val() + $('#2osJUL').val() +
                                   $('#2dsAug').val() + $('#2osAUG').val() +
                                   $('#2dsSep').val() + $('#2osSEP').val() +
                                   $('#2dsOct').val() + $('#2osOCT').val()).toFixed(2));
    $('#2fteNOV').text(parseFloat(totalDirectStaff + totalOder + $('#2dsJan').val() + $('#2osJAN').val() +
                                   $('#2dsFeb').val() + $('#2osFEB').val() +
                                   $('#2dsMar').val() + $('#2osMAR').val() +
                                   $('#2dsApr').val() + $('#2osAPR').val() +
                                   $('#2dsMay').val() + $('#2osMAY').val() +
                                   $('#2dsJun').val() + $('#2osJUN').val() +
                                   $('#2dsJul').val() + $('#2osJUL').val() +
                                   $('#2dsAug').val() + $('#2osAUG').val() +
                                   $('#2dsSep').val() + $('#2osSEP').val() +
                                   $('#2dsOct').val() + $('#2osOCT').val() +
                                   $('#2dsNov').val() + $('#2osNOV').val()).toFixed(2));
    $('#2fteDEC').text(parseFloat(totalDirectStaff + totalOder + $('#2dsJan').val() + $('#2osJAN').val() +
                                   $('#2dsFeb').val() + $('#2osFEB').val() +
                                   $('#2dsMar').val() + $('#2osMAR').val() +
                                   $('#2dsApr').val() + $('#2osAPR').val() +
                                   $('#2dsMay').val() + $('#2osMAY').val() +
                                   $('#2dsJun').val() + $('#2osJUN').val() +
                                   $('#2dsJul').val() + $('#2osJUL').val() +
                                   $('#2dsAug').val() + $('#2osAUG').val() +
                                   $('#2dsSep').val() + $('#2osSEP').val() +
                                   $('#2dsOct').val() + $('#2osOCT').val() +
                                   $('#2dsNov').val() + $('#2osNOV').val() +
                                   $('#2dsDec').val() + $('#2osDEC').val()).toFixed(2));


    $('#3fteJAN').text(parseFloat(totalDirectStaff + totalOder + totaltotalDirectStaff2 + totalOder2 + $('#3dsJan').val() + $('#3osJAN').val()).toFixed(2));

    $('#3fteFEB').text(parseFloat(totalDirectStaff + totalOder + totaltotalDirectStaff2 + totalOder2 + $('#3dsJan').val() + $('#3osJAN').val() +
                                   $('#3dsFeb').val() + $('#3osFEB').val()).toFixed(2));

    $('#3fteMAR').text(parseFloat(totalDirectStaff + totalOder + totaltotalDirectStaff2 + totalOder2 + $('#3dsJan').val() + $('#3osJAN').val() +
                                   $('#3dsFeb').val() + $('#3osFEB').val() +
                                   $('#3dsMar').val() + $('#3osMAR').val()).toFixed(2));

    $('#3fteAPR').text(parseFloat(totalDirectStaff + totalOder + totaltotalDirectStaff2 + totalOder2 + $('#3dsJan').val() + $('#3osJAN').val() +
                                   $('#3dsFeb').val() + $('#3osFEB').val() +
                                   $('#3dsMar').val() + $('#3osMAR').val() +
                                   $('#3dsApr').val() + $('#3osAPR').val()).toFixed(2));
    $('#3fteMAY').text(parseFloat(totalDirectStaff + totalOder + totaltotalDirectStaff2 + totalOder2 + $('#3dsJan').val() + $('#3osJAN').val() +
                                   $('#3dsFeb').val() + $('#3osFEB').val() +
                                   $('#3dsMar').val() + $('#3osMAR').val() +
                                   $('#3dsApr').val() + $('#3osAPR').val() +
                                   $('#3dsMay').val() + $('#3osMAY').val()).toFixed(2));

    $('#3fteJUN').text(parseFloat(totalDirectStaff + totalOder + totaltotalDirectStaff2 + totalOder2 + $('#3dsJan').val() + $('#3osJAN').val() +
                                   $('#3dsFeb').val() + $('#3osFEB').val() +
                                   $('#3dsMar').val() + $('#3osMAR').val() +
                                   $('#3dsApr').val() + $('#3osAPR').val() +
                                   $('#3dsMay').val() + $('#3osMAY').val() +
                                   $('#3dsJun').val() + $('#3osJUN').val()).toFixed(2));

    $('#3fteJUL').text(parseFloat(totalDirectStaff + totalOder + totaltotalDirectStaff2 + totalOder2 + $('#3dsJan').val() + $('#3osJAN').val() +
                                   $('#3dsFeb').val() + $('#3osFEB').val() +
                                   $('#3dsMar').val() + $('#3osMAR').val() +
                                   $('#3dsApr').val() + $('#3osAPR').val() +
                                   $('#3dsMay').val() + $('#3osMAY').val() +
                                   $('#3dsJun').val() + $('#3osJUN').val() +
                                   $('#3dsJul').val() + $('#3osJUL').val()).toFixed(2));

    $('#3fteAUG').text(parseFloat(totalDirectStaff + totalOder + totaltotalDirectStaff2 + totalOder2 + $('#3dsJan').val() + $('#3osJAN').val() +
                                   $('#3dsFeb').val() + $('#3osFEB').val() +
                                   $('#3dsMar').val() + $('#3osMAR').val() +
                                   $('#3dsApr').val() + $('#3osAPR').val() +
                                   $('#3dsMay').val() + $('#3osMAY').val() +
                                   $('#3dsJun').val() + $('#3osJUN').val() +
                                   $('#3dsJul').val() + $('#3osJUL').val() +
                                   $('#3dsAug').val() + $('#3osAUG').val()).toFixed(2));

    $('#3fteSEP').text(parseFloat(totalDirectStaff + totalOder + totaltotalDirectStaff2 + totalOder2 + $('#3dsJan').val() + $('#3osJAN').val() +
                                   $('#3dsFeb').val() + $('#3osFEB').val() +
                                   $('#3dsMar').val() + $('#3osMAR').val() +
                                   $('#3dsApr').val() + $('#3osAPR').val() +
                                   $('#3dsMay').val() + $('#3osMAY').val() +
                                   $('#3dsJun').val() + $('#3osJUN').val() +
                                   $('#3dsJul').val() + $('#3osJUL').val() +
                                   $('#3dsAug').val() + $('#3osAUG').val() +
                                   $('#3dsSep').val() + $('#3osSEP').val()).toFixed(2));

    $('#3fteOCT').text(parseFloat(totalDirectStaff + totalOder + totaltotalDirectStaff2 + totalOder2 + $('#3dsJan').val() + $('#3osJAN').val() +
                                   $('#3dsFeb').val() + $('#3osFEB').val() +
                                   $('#3dsMar').val() + $('#3osMAR').val() +
                                   $('#3dsApr').val() + $('#3osAPR').val() +
                                   $('#3dsMay').val() + $('#3osMAY').val() +
                                   $('#3dsJun').val() + $('#3osJUN').val() +
                                   $('#3dsJul').val() + $('#3osJUL').val() +
                                   $('#3dsAug').val() + $('#3osAUG').val() +
                                   $('#3dsSep').val() + $('#3osSEP').val() +
                                   $('#3dsOct').val() + $('#3osOCT').val()).toFixed(2));

    $('#3fteNOV').text(parseFloat(totalDirectStaff + totalOder + totaltotalDirectStaff2 + totalOder2 + $('#3dsJan').val() + $('#3osJAN').val() +
                                   $('#3dsFeb').val() + $('#3osFEB').val() +
                                   $('#3dsMar').val() + $('#3osMAR').val() +
                                   $('#3dsApr').val() + $('#3osAPR').val() +
                                   $('#3dsMay').val() + $('#3osMAY').val() +
                                   $('#3dsJun').val() + $('#3osJUN').val() +
                                   $('#3dsJul').val() + $('#3osJUL').val() +
                                   $('#3dsAug').val() + $('#3osAUG').val() +
                                   $('#3dsSep').val() + $('#3osSEP').val() +
                                   $('#3dsOct').val() + $('#3osOCT').val() +
                                   $('#3dsNov').val() + $('#3osNOV').val()).toFixed(2));

    $('#3fteDEC').text(parseFloat(totalDirectStaff + totalOder + totaltotalDirectStaff2 + totalOder2 + $('#3dsJan').val() + $('#3osJAN').val() +
                                   $('#3dsFeb').val() + $('#3osFEB').val() +
                                   $('#3dsMar').val() + $('#3osMAR').val() +
                                   $('#3dsApr').val() + $('#3osAPR').val() +
                                   $('#3dsMay').val() + $('#3osMAY').val() +
                                   $('#3dsJun').val() + $('#3osJUN').val() +
                                   $('#3dsJul').val() + $('#3osJUL').val() +
                                   $('#3dsAug').val() + $('#3osAUG').val() +
                                   $('#3dsSep').val() + $('#3osSEP').val() +
                                   $('#3dsOct').val() + $('#3osOCT').val() +
                                   $('#3dsNov').val() + $('#3osNOV').val() +
                                   $('#3dsDec').val() + $('#3osDEC').val()).toFixed(2));

    Receiving = $("#receivingFTE").val();
    PerMonth = $("#receivingFTE").val() / 12;

    $('#FTETotalCostJAN').text('$ ' + (parseFloat($('#fteJAN').text()) * PerMonth).toFixed(2));
    $('#FTETotalCostFEB').text('$ ' + (parseFloat($('#fteFEB').text()) * PerMonth).toFixed(2));
    $('#FTETotalCostMAR').text('$ ' + (parseFloat($('#fteMAR').text()) * PerMonth).toFixed(2));
    $('#FTETotalCostAPR').text('$ ' + (parseFloat($('#fteAPR').text()) * PerMonth).toFixed(2));
    $('#FTETotalCostMAY').text('$ ' + (parseFloat($('#fteMAY').text()) * PerMonth).toFixed(2));
    $('#FTETotalCostJUN').text('$ ' + (parseFloat($('#fteJUN').text()) * PerMonth).toFixed(2));
    $('#FTETotalCostJUL').text('$ ' + (parseFloat($('#fteJUL').text()) * PerMonth).toFixed(2));
    $('#FTETotalCostAUG').text('$ ' + (parseFloat($('#fteAUG').text()) * PerMonth).toFixed(2));
    $('#FTETotalCostSEP').text('$ ' + (parseFloat($('#fteSEP').text()) * PerMonth).toFixed(2));
    $('#FTETotalCostOCT').text('$ ' + (parseFloat($('#fteOCT').text()) * PerMonth).toFixed(2));
    $('#FTETotalCostNOV').text('$ ' + (parseFloat($('#fteNOV').text()) * PerMonth).toFixed(2));
    $('#FTETotalCostDEC').text('$ ' + (parseFloat($('#fteDEC').text()) * PerMonth).toFixed(2));


    $('#2FTETotalCostJAN').text('$ ' + (parseFloat($('#2fteJAN').text()) * PerMonth).toFixed(2));
    $('#2FTETotalCostFEB').text('$ ' + (parseFloat($('#2fteFEB').text()) * PerMonth).toFixed(2));
    $('#2FTETotalCostMAR').text('$ ' + (parseFloat($('#2fteMAR').text()) * PerMonth).toFixed(2));
    $('#2FTETotalCostAPR').text('$ ' + (parseFloat($('#2fteAPR').text()) * PerMonth).toFixed(2));
    $('#2FTETotalCostMAY').text('$ ' + (parseFloat($('#2fteMAY').text()) * PerMonth).toFixed(2));
    $('#2FTETotalCostJUN').text('$ ' + (parseFloat($('#2fteJUN').text()) * PerMonth).toFixed(2));
    $('#2FTETotalCostJUL').text('$ ' + (parseFloat($('#2fteJUL').text()) * PerMonth).toFixed(2));
    $('#2FTETotalCostAUG').text('$ ' + (parseFloat($('#2fteAUG').text()) * PerMonth).toFixed(2));
    $('#2FTETotalCostSEP').text('$ ' + (parseFloat($('#2fteSEP').text()) * PerMonth).toFixed(2));
    $('#2FTETotalCostOCT').text('$ ' + (parseFloat($('#2fteOCT').text()) * PerMonth).toFixed(2));
    $('#2FTETotalCostNOV').text('$ ' + (parseFloat($('#2fteNOV').text()) * PerMonth).toFixed(2));
    $('#2FTETotalCostDEC').text('$ ' + (parseFloat($('#2fteDEC').text()) * PerMonth).toFixed(2));

    $('#3FTETotalCostJAN').text('$ ' + (parseFloat($('#3fteJAN').text()) * PerMonth).toFixed(2));
    $('#3FTETotalCostFEB').text('$ ' + (parseFloat($('#3fteFEB').text()) * PerMonth).toFixed(2));
    $('#3FTETotalCostMAR').text('$ ' + (parseFloat($('#3fteMAR').text()) * PerMonth).toFixed(2));
    $('#3FTETotalCostAPR').text('$ ' + (parseFloat($('#3fteAPR').text()) * PerMonth).toFixed(2));
    $('#3FTETotalCostMAY').text('$ ' + (parseFloat($('#3fteMAY').text()) * PerMonth).toFixed(2));
    $('#3FTETotalCostJUN').text('$ ' + (parseFloat($('#3fteJUN').text()) * PerMonth).toFixed(2));
    $('#3FTETotalCostJUL').text('$ ' + (parseFloat($('#3fteJUL').text()) * PerMonth).toFixed(2));
    $('#3FTETotalCostAUG').text('$ ' + (parseFloat($('#3fteAUG').text()) * PerMonth).toFixed(2));
    $('#3FTETotalCostSEP').text('$ ' + (parseFloat($('#3fteSEP').text()) * PerMonth).toFixed(2));
    $('#3FTETotalCostOCT').text('$ ' + (parseFloat($('#3fteOCT').text()) * PerMonth).toFixed(2));
    $('#3FTETotalCostNOV').text('$ ' + (parseFloat($('#3fteNOV').text()) * PerMonth).toFixed(2));
    $('#3FTETotalCostDEC').text('$ ' + (parseFloat($('#3fteDEC').text()) * PerMonth).toFixed(2));

    $('#FTETotalCostTOTAL').text('$ ' +
        (
                (parseFloat($('#fteJAN').text()) * PerMonth) +
                (parseFloat($('#fteFEB').text()) * PerMonth) +
                (parseFloat($('#fteMAR').text()) * PerMonth) +
                (parseFloat($('#fteAPR').text()) * PerMonth) +
                (parseFloat($('#fteMAY').text()) * PerMonth) +
                (parseFloat($('#fteJUN').text()) * PerMonth) +
                (parseFloat($('#fteJUL').text()) * PerMonth) +
                (parseFloat($('#fteAUG').text()) * PerMonth) +
                (parseFloat($('#fteSEP').text()) * PerMonth) +
                (parseFloat($('#fteOCT').text()) * PerMonth) +
                (parseFloat($('#fteNOV').text()) * PerMonth) +
                (parseFloat($('#fteDEC').text()) * PerMonth)
        ).toFixed(2));


    $('#2FTETotalCostTOTAL').text('$ ' +
        (
                (parseFloat($('#2fteJAN').text()) * PerMonth) +
                (parseFloat($('#2fteFEB').text()) * PerMonth) +
                (parseFloat($('#2fteMAR').text()) * PerMonth) +
                (parseFloat($('#2fteAPR').text()) * PerMonth) +
                (parseFloat($('#2fteMAY').text()) * PerMonth) +
                (parseFloat($('#2fteJUN').text()) * PerMonth) +
                (parseFloat($('#2fteJUL').text()) * PerMonth) +
                (parseFloat($('#2fteAUG').text()) * PerMonth) +
                (parseFloat($('#2fteSEP').text()) * PerMonth) +
                (parseFloat($('#2fteOCT').text()) * PerMonth) +
                (parseFloat($('#2fteNOV').text()) * PerMonth) +
                (parseFloat($('#2fteDEC').text()) * PerMonth)
        ).toFixed(2));

    $('#3FTETotalCostTOTAL').text('$ ' +
        (
                (parseFloat($('#3fteJAN').text()) * PerMonth) +
                (parseFloat($('#3fteFEB').text()) * PerMonth) +
                (parseFloat($('#3fteMAR').text()) * PerMonth) +
                (parseFloat($('#3fteAPR').text()) * PerMonth) +
                (parseFloat($('#3fteMAY').text()) * PerMonth) +
                (parseFloat($('#3fteJUN').text()) * PerMonth) +
                (parseFloat($('#3fteJUL').text()) * PerMonth) +
                (parseFloat($('#3fteAUG').text()) * PerMonth) +
                (parseFloat($('#3fteSEP').text()) * PerMonth) +
                (parseFloat($('#3fteOCT').text()) * PerMonth) +
                (parseFloat($('#3fteNOV').text()) * PerMonth) +
                (parseFloat($('#3fteDEC').text()) * PerMonth)
        ).toFixed(2));



}

function getFinancialDetail() {

    var nonMOU = $('#txtNonMOUID').val();

    var listOfParams = {
        NonMOU: nonMOU
    }


    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getFinancialDetail',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            $.each((response), function (index, element) {
                var Continue = element

                $('#txtFTEDiscovery').jqxNumberInput('val', element.FTE);

                if (element.Name == 'Direct Staff Inc/Dec' & element.Year == 1) {

                    $('#dsJan').jqxNumberInput('val', element.JAN);
                    $('#dsFeb').jqxNumberInput('val', element.FEB);
                    $('#dsMar').jqxNumberInput('val', element.MAR);
                    $('#dsApr').jqxNumberInput('val', element.APR);
                    $('#dsMay').jqxNumberInput('val', element.MAY);
                    $('#dsJun').jqxNumberInput('val', element.JUN);
                    $('#dsJul').jqxNumberInput('val', element.JUL);
                    $('#dsAug').jqxNumberInput('val', element.AUG);
                    $('#dsSep').jqxNumberInput('val', element.SEP);
                    $('#dsOct').jqxNumberInput('val', element.OCT);
                    $('#dsNov').jqxNumberInput('val', element.NOV);
                    $('#dsDec').jqxNumberInput('val', element.DEC);

                }
                if (element.Name == 'Other FTE Inc/Dec' & element.Year == 1) {

                    $('#osJAN').jqxNumberInput('val', element.JAN);
                    $('#osFEB').jqxNumberInput('val', element.FEB);
                    $('#osMAR').jqxNumberInput('val', element.MAR);
                    $('#osAPR').jqxNumberInput('val', element.APR);
                    $('#osMAY').jqxNumberInput('val', element.MAY);
                    $('#osJUN').jqxNumberInput('val', element.JUN);
                    $('#osJUL').jqxNumberInput('val', element.JUL);
                    $('#osAUG').jqxNumberInput('val', element.AUG);
                    $('#osSEP').jqxNumberInput('val', element.SEP);
                    $('#osOCT').jqxNumberInput('val', element.OCT);
                    $('#osNOV').jqxNumberInput('val', element.NOV);
                    $('#osDEC').jqxNumberInput('val', element.DEC);

                }


                if (element.Name == 'Direct Staff Inc/Dec' & element.Year == 2) {
                    $('#2dsJan').jqxNumberInput('val', element.JAN);
                    $('#2dsFeb').jqxNumberInput('val', element.FEB);
                    $('#2dsMar').jqxNumberInput('val', element.MAR);
                    $('#2dsApr').jqxNumberInput('val', element.APR);
                    $('#2dsMay').jqxNumberInput('val', element.MAY);
                    $('#2dsJun').jqxNumberInput('val', element.JUN);
                    $('#2dsJul').jqxNumberInput('val', element.JUL);
                    $('#2dsAug').jqxNumberInput('val', element.AUG);
                    $('#2dsSep').jqxNumberInput('val', element.SEP);
                    $('#2dsOct').jqxNumberInput('val', element.OCT);
                    $('#2dsNov').jqxNumberInput('val', element.NOV);
                    $('#2dsDec').jqxNumberInput('val', element.DEC);

                }
                if (element.Name == 'Other FTE Inc/Dec' & element.Year == 2) {
                    $('#2osJAN').jqxNumberInput('val', element.JAN);
                    $('#2osFEB').jqxNumberInput('val', element.FEB);
                    $('#2osMAR').jqxNumberInput('val', element.MAR);
                    $('#2osAPR').jqxNumberInput('val', element.APR);
                    $('#2osMAY').jqxNumberInput('val', element.MAY);
                    $('#2osJUN').jqxNumberInput('val', element.JUN);
                    $('#2osJUL').jqxNumberInput('val', element.JUL);
                    $('#2osAUG').jqxNumberInput('val', element.AUG);
                    $('#2osSEP').jqxNumberInput('val', element.SEP);
                    $('#2osOCT').jqxNumberInput('val', element.OCT);
                    $('#2osNOV').jqxNumberInput('val', element.NOV);
                    $('#2osDEC').jqxNumberInput('val', element.DEC);

                }


                if (element.Name == 'Direct Staff Inc/Dec' & element.Year == 3) {
                    $('#3dsJan').jqxNumberInput('val', element.JAN);
                    $('#3dsFeb').jqxNumberInput('val', element.FEB);
                    $('#3dsMar').jqxNumberInput('val', element.MAR);
                    $('#3dsApr').jqxNumberInput('val', element.APR);
                    $('#3dsMay').jqxNumberInput('val', element.MAY);
                    $('#3dsJun').jqxNumberInput('val', element.JUN);
                    $('#3dsJul').jqxNumberInput('val', element.JUL);
                    $('#3dsAug').jqxNumberInput('val', element.AUG);
                    $('#3dsSep').jqxNumberInput('val', element.SEP);
                    $('#3dsOct').jqxNumberInput('val', element.OCT);
                    $('#3dsNov').jqxNumberInput('val', element.NOV);
                    $('#3dsDec').jqxNumberInput('val', element.DEC);

                }
                if (element.Name == 'Other FTE Inc/Dec' & element.Year == 3) {
                    $('#3osJAN').jqxNumberInput('val', element.JAN);
                    $('#3osFEB').jqxNumberInput('val', element.FEB);
                    $('#3osMAR').jqxNumberInput('val', element.MAR);
                    $('#3osAPR').jqxNumberInput('val', element.APR);
                    $('#3osMAY').jqxNumberInput('val', element.MAY);
                    $('#3osJUN').jqxNumberInput('val', element.JUN);
                    $('#3osJUL').jqxNumberInput('val', element.JUL);
                    $('#3osAUG').jqxNumberInput('val', element.AUG);
                    $('#3osSEP').jqxNumberInput('val', element.SEP);
                    $('#3osOCT').jqxNumberInput('val', element.OCT);
                    $('#3osNOV').jqxNumberInput('val', element.NOV);
                    $('#3osDEC').jqxNumberInput('val', element.DEC);

                }


            });
        }
    });

}


function renderApproval() {
    findSoeidMethod('#soeidApprover', '50%');
    $("#inputOrderApproval").jqxNumberInput({ inputMode: 'simple', width: '75px', height: '25px', inputMode: 'simple', spinButtons: true, decimalDigits: 0, min: 0 });

    renderApprovalType('');
    renderApprovalList();
}

function renderApprovalType(NameType) {

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getApprovalType',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = (response);

            var source =
                {
                    localdata: data2,
                    datatype: "json",
                    datafields: [
                        { name: 'ID' },
                        { name: 'Role' }
                    ],
                    async: false
                };

            var dataAdapter = new $.jqx.dataAdapter(source);

            $("#cbRoleApprover").jqxComboBox({ selectedIndex: 0, source: dataAdapter, displayMember: "Role", valueMember: "Role", width: '99%', height: 25 });

            if (NameType != '') {
                $("#cbRoleApprover").jqxComboBox('selectItem', NameType);
            }

            $("#cbRoleApprover").on('select', function (event) {
                if (event.args) {
                    var item = event.args.item;
                    if (item) {

                    }
                }
            });
        }
    });




}

function addApproval() {
    var listOfParams = {
        TYPE: $("#cbRoleApprover").val(),
        NonMOU: $('#txtNonMOUID').val(),
        SOEID: $('#soeidApprover').val().split(' ')[0],
        ORDER: $('#inputOrderApproval').val()
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/saveApprovalOrder',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            
                renderApprovalList();
            
        }
    });

}

function renderApprovalList() {
    var nonMOU = $('#txtNonMOUID').val();

    var listOfParams = {
        NonMOU: nonMOU
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getApprovalListOrder',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = (response);

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                       { name: 'ID', type: 'string' },
                       { name: 'ApproveOrder', type: 'string' },
                       { name: 'Role', type: 'string' },
                       { name: 'NAME', type: 'string' }
                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            var imgDelete = function (row, datafield, value) {

                var rowData = $(jqxGridApproverOrder).jqxGrid('getrowdata', row);
                return '<a class="btn" href="#" onclick="deleteApprovalOrder(' + rowData.ID + ')" style="display:inline-block; background-color:#d9534f; width:70px;">Delete</a></BR></BR> '

            }

            $(jqxGridApproverOrder).jqxGrid(
           {
               source: dataAdapter,

               width: '100%',
               selectionmode: 'none',
               theme: 'blackberry',
               autoheight: true,
               autorowheight: true,
               pageable: false,

               filterable: false,
               columnsresize: true,
               altrows: true,
               columns: [
               { text: '', filtertype: 'none', width: '10%', columntype: 'image', pinned: false, cellsrenderer: imgDelete },
               { text: 'ApproveOrder', dataField: 'ApproveOrder', filtertype: 'input', width: '10%', editable: false },
               { text: 'Role', dataField: 'Role', filtertype: 'input', width: '20%', editable: false },
               { text: 'NAME', dataField: 'NAME', filtertype: 'input', width: '60%', editable: false },
               ]

           });

        }
    });

}

function deleteApprovalOrder(ID) {
    var listOfParams = {
        pID: ID
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/deleteApprovalListOrder',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            
                renderApprovalList();
            

        }
    });



}

function getAttachments() {


    //var nonMOU = $('#txtNonMOUID').val();

    //var listOfParams = {
    //    NonMOU: nonMOU
    //}

    //_callServer({
    //    loadingMsg: "", async: false,
    //    url: '/NonMOU/getAttachmentList',
    //    data: {
    //        'data': JSON.stringify(listOfParams)
    //    },
    //    type: "post",
    //    success: function (json) {
    //        var response = jQuery.parseJSON(json);

    //        var data2 = (response);

    //        var source =
    //       {
    //           unboundmode: true,
    //           localdata: data2,
    //           datafields:
    //                   [
    //                   { name: 'ID', type: 'string' },
    //                   { name: 'Path', type: 'string' }
    //                   ],
    //           datatype: "json"
    //       };
    //        var dataAdapter = new $.jqx.dataAdapter(source);

    //        var imgDelete = function (row, datafield, value) {

    //            var rowData = $(jqxGridAttachments).jqxGrid('getrowdata', row);
    //            return '<a class="btn" href="#" onclick="deleteAttachment(' + rowData.ID + ')" style="display:inline-block; background-color:#d9534f; width:100px;"><i class="icon-trash" style="padding-top: 8px; font-size: 20px; color: white;"></i> Delete</a></BR></BR> '

    //        }

    //        var imgDownload = function (row, datafield, value) {

    //            var rowData = $(jqxGridAttachments).jqxGrid('getrowdata', row);
    //            return '<a class="btn" href="#" onclick="downloadAttachment(' + rowData.ID + ')" style="display:inline-block; background-color:#337ab7; width:100px;"><i class="icon-download" style="padding-top: 8px; font-size: 20px; color: white;"></i> Download</a></BR></BR> '

    //        }

    //        $(jqxGridAttachments).jqxGrid(
    //       {
    //           source: dataAdapter,

    //           width: '100%',
    //           selectionmode: 'none',
    //           theme: 'blackberry',
    //           autoheight: true,
    //           autorowheight: true,
    //           pageable: false,

    //           filterable: false,
    //           columnsresize: true,
    //           altrows: true,
    //           columns: [
    //           { text: '', filtertype: 'none', width: '10%', columntype: 'image', pinned: false, cellsrenderer: imgDelete },
    //           { text: '', filtertype: 'none', width: '10%', columntype: 'image', pinned: false, cellsrenderer: imgDownload },
    //           { text: 'Path', dataField: 'Path', filtertype: 'input', width: '80%', editable: false }
    //           ]

    //       });

    //    }
    //});


}

function deleteAttachment(ID) {

    var listOfParams = {
        pID: ID
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/deleteAttachment',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
        
                getAttachments();
            
        }
    });

}


function downloadAttachment(ID) {


    var listOfParams = {
        pID: ID
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getAttachmentData',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            if (response) {


                $.each((response), function (index, element) {


                    window.open(element.Path);

                });
            }

        }
    });

}

function renderConversation() {

    var listOfParams = {
        pNonMOU: $('#txtNonMOUID').val()
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getCommentConversation',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            $.each((response), function (index, element) {

                $('#mytextarea').val(element.Comment);

            });

        }
    });

}

function saveMOUInDiscovery() {
    var listOfParams = {
        txtNonMOUID: $('#txtNonMOUID').val(),
        txtFTEDiscovery: $('#txtFTEDiscovery').jqxNumberInput('val'),
        txtNameDiscovery: $('#txtNameDiscovery').val(),
        jqxDropDownMOUType: $('#jqxDropDownMOUType').val(),
        ScopeDiscovery: $('#ScopeDiscovery').val(),
        BenefitsDiscovery: $('#BenefitsDiscovery').val(),
        dateInitiated: $('#dateInitiated').val(),
        dateAvaible: $('#dateAvaible').val(),
        // jqxGridMilestones : $('#jqxGridMilestones').jqxGrid('exportdata', 'json'),
        receivingFTE: $('#receivingFTE').jqxNumberInput('val'),
        cbPhysicalLocation: $('#cbPhysicalLocation').val(),
        jqxSiteManager: $('#jqxSiteManager').val().split(' ')[0],
        jqxSiteFinanceManger: $('#jqxSiteFinanceManger').val().split(' ')[0],

        MSLVL09: $("#cbManagedSegmentLvl9").val(),
        dsTotal: $('#dsTotal').text(),
        lblDSJAN: $('#lblDSJAN').text(),
        lblDSFEB: $('#lblDSFEB').text(),
        lblDSMAR: $('#lblDSMAR').text(),
        lblDSAPR: $('#lblDSAPR').text(),
        lblDSMAY: $('#lblDSMAY').text(),
        lblDSJUN: $('#lblDSJUN').text(),
        lblDSJUL: $('#lblDSJUL').text(),
        lblDSAUG: $('#lblDSAUG').text(),
        lblDSSEP: $('#lblDSSEP').text(),
        lblDSOCT: $('#lblDSOCT').text(),
        lblDSNOV: $('#lblDSNOV').text(),
        lblDSDEC: $('#lblDSDEC').text(),
        lblDSTotal: $('#lblDSTotal').text(),
        osTotal: $('#osTotal').text(),
        lblOSJAN: $('#lblOSJAN').text(),
        lblOSFEB: $('#lblOSFEB').text(),
        lblOSMAR: $('#lblOSMAR').text(),
        lblOSAPR: $('#lblOSAPR').text(),
        lblOSMAY: $('#lblOSMAY').text(),
        lblOSJUN: $('#lblOSJUN').text(),
        lblOSJUL: $('#lblOSJUL').text(),
        lblOSAUG: $('#lblOSAUG').text(),
        lblOSSEP: $('#lblOSSEP').text(),
        lblOSOCT: $('#lblOSOCT').text(),
        lblOSNOV: $('#lblOSNOV').text(),
        lblOSDEC: $('#lblOSDEC').text(),
        lblOSTOTAL: $('#lblOSTOTAL').text(),
        fteJAN: $('#fteJAN').text(),
        fteFEB: $('#fteFEB').text(),
        fteMAR: $('#fteMAR').text(),
        fteAPR: $('#fteAPR').text(),
        fteMAY: $('#fteMAY').text(),
        fteJUN: $('#fteJUN').text(),
        fteJUL: $('#fteJUL').text(),
        fteAUG: $('#fteAUG').text(),
        fteSEP: $('#fteSEP').text(),
        fteOCT: $('#fteOCT').text(),
        fteNOV: $('#fteNOV').text(),
        fteDEC: $('#fteDEC').text(),
        fteTOTAL: $('#fteTOTAL').text(),
        // trainCostTotal : $('#trainCostTotal').text(),
        // tecCostTOTAL : $('#tecCostTOTAL').text(),
        FTETotalCostJAN: $('#FTETotalCostJAN').text(),
        FTETotalCostFEB: $('#FTETotalCostFEB').text(),
        FTETotalCostMAR: $('#FTETotalCostMAR').text(),
        FTETotalCostAPR: $('#FTETotalCostAPR').text(),
        FTETotalCostMAY: $('#FTETotalCostMAY').text(),
        FTETotalCostJUN: $('#FTETotalCostJUN').text(),
        FTETotalCostJUL: $('#FTETotalCostJUL').text(),
        FTETotalCostAUG: $('#FTETotalCostAUG').text(),
        FTETotalCostSEP: $('#FTETotalCostSEP').text(),
        FTETotalCostOCT: $('#FTETotalCostOCT').text(),
        FTETotalCostNOV: $('#FTETotalCostNOV').text(),
        FTETotalCostDEC: $('#FTETotalCostDEC').text(),
        FTETotalCostTOTAL: $('#FTETotalCostTOTAL').text(),
        //totalCostJAN : $('#totalCostJAN').text(),
        //totalCostFEB : $('#totalCostFEB').text(),
        //totalCostMAR : $('#totalCostMAR').text(),
        //totalCostAPR : $('#totalCostAPR').text(),
        //totalCostMAY : $('#totalCostMAY').text(),
        //totalCostJUN : $('#totalCostJUN').text(),
        //totalCostJUL : $('#totalCostJUL').text(),
        //totalCostAUG : $('#totalCostAUG').text(),
        //totalCostSEP : $('#totalCostSEP').text(),
        //totalCostOCT : $('#totalCostOCT').text(),
        //totalCostNOV : $('#totalCostNOV').text(),
        //totalCostDEC : $('#totalCostDEC').text(),
        //totalCostTOTAL : $('#totalCostTOTAL').text(),
        dsTotal2: $('#2dsTotal').text(),
        lblDSJAN2: $('#2lblDSJAN').text(),
        lblDSFEB2: $('#2lblDSFEB').text(),
        lblDSMAR2: $('#2lblDSMAR').text(),
        lblDSAPR2: $('#2lblDSAPR').text(),
        lblDSMAY2: $('#2lblDSMAY').text(),
        lblDSJUN2: $('#2lblDSJUN').text(),
        lblDSJUL2: $('#2lblDSJUL').text(),
        lblDSAUG2: $('#2lblDSAUG').text(),
        lblDSSEP2: $('#2lblDSSEP').text(),
        lblDSOCT2: $('#2lblDSOCT').text(),
        lblDSNOV2: $('#2lblDSNOV').text(),
        lblDSDEC2: $('#2lblDSDEC').text(),
        lblDSTotal2: $('#2lblDSTotal').text(),
        osTotal2: $('#2osTotal').text(),
        lblOSJAN2: $('#2lblOSJAN').text(),
        lblOSFEB2: $('#2lblOSFEB').text(),
        lblOSMAR2: $('#2lblOSMAR').text(),
        lblOSAPR2: $('#2lblOSAPR').text(),
        lblOSMAY2: $('#2lblOSMAY').text(),
        lblOSJUN2: $('#2lblOSJUN').text(),
        lblOSJUL2: $('#2lblOSJUL').text(),
        lblOSAUG2: $('#2lblOSAUG').text(),
        lblOSSEP2: $('#2lblOSSEP').text(),
        lblOSOCT2: $('#2lblOSOCT').text(),
        lblOSNOV2: $('#2lblOSNOV').text(),
        lblOSDEC2: $('#2lblOSDEC').text(),
        lblOSTOTAL2: $('#2lblOSTOTAL').text(),
        fteJAN2: $('#2fteJAN').text(),
        fteFEB2: $('#2fteFEB').text(),
        fteMAR2: $('#2fteMAR').text(),
        fteAPR2: $('#2fteAPR').text(),
        fteMAY2: $('#2fteMAY').text(),
        fteJUN2: $('#2fteJUN').text(),
        fteJUL2: $('#2fteJUL').text(),
        fteAUG2: $('#2fteAUG').text(),
        fteSEP2: $('#2fteSEP').text(),
        fteOCT2: $('#2fteOCT').text(),
        fteNOV2: $('#2fteNOV').text(),
        fteDEC2: $('#2fteDEC').text(),
        fteTOTAL2: $('#2fteTOTAL').text(),
        //trainCostTotal2 : $('#2trainCostTotal').text(),
        //tecCostTOTAL2 : $('#2tecCostTOTAL').text(),
        FTETotalCostJAN2: $('#2FTETotalCostJAN').text(),
        FTETotalCostFEB2: $('#2FTETotalCostFEB').text(),
        FTETotalCostMAR2: $('#2FTETotalCostMAR').text(),
        FTETotalCostAPR2: $('#2FTETotalCostAPR').text(),
        FTETotalCostMAY2: $('#2FTETotalCostMAY').text(),
        FTETotalCostJUN2: $('#2FTETotalCostJUN').text(),
        FTETotalCostJUL2: $('#2FTETotalCostJUL').text(),
        FTETotalCostAUG2: $('#2FTETotalCostAUG').text(),
        FTETotalCostSEP2: $('#2FTETotalCostSEP').text(),
        FTETotalCostOCT2: $('#2FTETotalCostOCT').text(),
        FTETotalCostNOV2: $('#2FTETotalCostNOV').text(),
        FTETotalCostDEC2: $('#2FTETotalCostDEC').text(),
        FTETotalCostTOTAL2: $('#2FTETotalCostTOTAL').text(),
        //totalCostJAN2 : $('#2totalCostJAN').text(),
        //totalCostFEB2 : $('#2totalCostFEB').text(),
        //totalCostMAR2 : $('#2totalCostMAR').text(),
        //totalCostAPR2 : $('#2totalCostAPR').text(),
        //totalCostMAY2 : $('#2totalCostMAY').text(),
        //totalCostJUN2 : $('#2totalCostJUN').text(),
        //totalCostJUL2 : $('#2totalCostJUL').text(),
        //totalCostAUG2 : $('#2totalCostAUG').text(),
        //totalCostSEP2 : $('#2totalCostSEP').text(),
        //totalCostOCT2 : $('#2totalCostOCT').text(),
        //totalCostNOV2 : $('#2totalCostNOV').text(),
        //totalCostDEC2 : $('#2totalCostDEC').text(),
        //totalCostTOTAL2 : $('#2totalCostTOTAL').text(),

        dsTotal3: $('#3dsTotal').text(),
        lblDSJAN3: $('#3lblDSJAN').text(),
        lblDSFEB3: $('#3lblDSFEB').text(),
        lblDSMAR3: $('#3lblDSMAR').text(),
        lblDSAPR3: $('#3lblDSAPR').text(),
        lblDSMAY3: $('#3lblDSMAY').text(),
        lblDSJUN3: $('#3lblDSJUN').text(),
        lblDSJUL3: $('#3lblDSJUL').text(),
        lblDSAUG3: $('#3lblDSAUG').text(),
        lblDSSEP3: $('#3lblDSSEP').text(),
        lblDSOCT3: $('#3lblDSOCT').text(),
        lblDSNOV3: $('#3lblDSNOV').text(),
        lblDSDEC3: $('#3lblDSDEC').text(),
        lblDSTotal3: $('#3lblDSTotal').text(),
        osTotal3: $('#3osTotal').text(),
        lblOSJAN3: $('#3lblOSJAN').text(),
        lblOSFEB3: $('#3lblOSFEB').text(),
        lblOSMAR3: $('#3lblOSMAR').text(),
        lblOSAPR3: $('#3lblOSAPR').text(),
        lblOSMAY3: $('#3lblOSMAY').text(),
        lblOSJUN3: $('#3lblOSJUN').text(),
        lblOSJUL3: $('#3lblOSJUL').text(),
        lblOSAUG3: $('#3lblOSAUG').text(),
        lblOSSEP3: $('#3lblOSSEP').text(),
        lblOSOCT3: $('#3lblOSOCT').text(),
        lblOSNOV3: $('#3lblOSNOV').text(),
        lblOSDEC3: $('#3lblOSDEC').text(),
        lblOSTOTAL3: $('#3lblOSTOTAL').text(),
        fteJAN3: $('#3fteJAN').text(),
        fteFEB3: $('#3fteFEB').text(),
        fteMAR3: $('#3fteMAR').text(),
        fteAPR3: $('#3fteAPR').text(),
        fteMAY3: $('#3fteMAY').text(),
        fteJUN3: $('#3fteJUN').text(),
        fteJUL3: $('#3fteJUL').text(),
        fteAUG3: $('#3fteAUG').text(),
        fteSEP3: $('#3fteSEP').text(),
        fteOCT3: $('#3fteOCT').text(),
        fteNOV3: $('#3fteNOV').text(),
        fteDEC3: $('#3fteDEC').text(),
        fteTOTAL3: $('#3fteTOTAL').text(),
        //trainCostTotal3 : $('#3trainCostTotal').text(),
        //tecCostTOTAL3 : $('#3tecCostTOTAL').text(),
        FTETotalCostJAN3: $('#3FTETotalCostJAN').text(),
        FTETotalCostFEB3: $('#3FTETotalCostFEB').text(),
        FTETotalCostMAR3: $('#3FTETotalCostMAR').text(),
        FTETotalCostAPR3: $('#3FTETotalCostAPR').text(),
        FTETotalCostMAY3: $('#3FTETotalCostMAY').text(),
        FTETotalCostJUN3: $('#3FTETotalCostJUN').text(),
        FTETotalCostJUL3: $('#3FTETotalCostJUL').text(),
        FTETotalCostAUG3: $('#3FTETotalCostAUG').text(),
        FTETotalCostSEP3: $('#3FTETotalCostSEP').text(),
        FTETotalCostOCT3: $('#3FTETotalCostOCT').text(),
        FTETotalCostNOV3: $('#3FTETotalCostNOV').text(),
        FTETotalCostDEC3: $('#3FTETotalCostDEC').text(),
        FTETotalCostTOTAL3: $('#3FTETotalCostTOTAL').text(),

        dsJan: $('#dsJan').val(),
        dsFeb: $('#dsFeb').val(),
        dsMar: $('#dsMar').val(),
        dsApr: $('#dsApr').val(),
        dsMay: $('#dsMay').val(),
        dsJun: $('#dsJun').val(),
        dsJul: $('#dsJul').val(),
        dsAug: $('#dsAug').val(),
        dsSep: $('#dsSep').val(),
        dsOct: $('#dsOct').val(),
        dsNov: $('#dsNov').val(),
        dsDec: $('#dsDec').val(),
        osJAN: $('#osJAN').val(),
        osFEB: $('#osFEB').val(),
        osMAR: $('#osMAR').val(),
        osAPR: $('#osAPR').val(),
        osMAY: $('#osMAY').val(),
        osJUN: $('#osJUN').val(),
        osJUL: $('#osJUL').val(),
        osAUG: $('#osAUG').val(),
        osSEP: $('#osSEP').val(),
        osOCT: $('#osOCT').val(),
        osNOV: $('#osNOV').val(),
        osDEC: $('#osDEC').val(),
        dsJan2: $('#2dsJan').val(),
        dsFeb2: $('#2dsFeb').val(),
        dsMar2: $('#2dsMar').val(),
        dsApr2: $('#2dsApr').val(),
        dsMay2: $('#2dsMay').val(),
        dsJun2: $('#2dsJun').val(),
        dsJul2: $('#2dsJul').val(),
        dsAug2: $('#2dsAug').val(),
        dsSep2: $('#2dsSep').val(),
        dsOct2: $('#2dsOct').val(),
        dsNov2: $('#2dsNov').val(),
        dsDec2: $('#2dsDec').val(),
        osJAN2: $('#2osJAN').val(),
        osFEB2: $('#2osFEB').val(),
        osMAR2: $('#2osMAR').val(),
        osAPR2: $('#2osAPR').val(),
        osMAY2: $('#2osMAY').val(),
        osJUN2: $('#2osJUN').val(),
        osJUL2: $('#2osJUL').val(),
        osAUG2: $('#2osAUG').val(),
        osSEP2: $('#2osSEP').val(),
        osOCT2: $('#2osOCT').val(),
        osNOV2: $('#2osNOV').val(),
        osDEC2: $('#2osDEC').val(),
        //trainCostJAN : $('#trainCostJAN').val(),
        //trainCostFEB : $('#trainCostFEB').val(),
        //trainCostMAR : $('#trainCostMAR').val(),
        //trainCostAPR : $('#trainCostAPR').val(),
        //trainCostMAY : $('#trainCostMAY').val(),
        //trainCostJUN : $('#trainCostJUN').val(),
        //trainCostJUL : $('#trainCostJUL').val(),
        //trainCostAUG : $('#trainCostAUG').val(),
        //trainCostSEP : $('#trainCostSEP').val(),
        //trainCostOCT : $('#trainCostOCT').val(),
        //trainCostNOV : $('#trainCostNOV').val(),
        //trainCostDEC : $('#trainCostDEC').val(),
        //tecCostJAN : $('#tecCostJAN').val(),
        //tecCostFEB : $('#tecCostFEB').val(),
        //tecCostMAR : $('#tecCostMAR').val(),
        //tecCostAPR : $('#tecCostAPR').val(),
        //tecCostMAY : $('#tecCostMAY').val(),
        //tecCostJUN : $('#tecCostJUN').val(),
        //tecCostJUL : $('#tecCostJUL').val(),
        //tecCostAUG : $('#tecCostAUG').val(),
        //tecCostSEP : $('#tecCostSEP').val(),
        //tecCostOCT : $('#tecCostOCT').val(),
        //tecCostNOV : $('#tecCostNOV').val(),
        //tecCostDEC : $('#tecCostDEC').val(),
        //trainCostJAN2 : $('#2trainCostJAN').val(),
        //trainCostFEB2 : $('#2trainCostFEB').val(),
        //trainCostMAR2 : $('#2trainCostMAR').val(),
        //trainCostAPR2 : $('#2trainCostAPR').val(),
        //trainCostMAY2 : $('#2trainCostMAY').val(),
        //trainCostJUN2 : $('#2trainCostJUN').val(),
        //trainCostJUL2 : $('#2trainCostJUL').val(),
        //trainCostAUG2 : $('#2trainCostAUG').val(),
        //trainCostSEP2 : $('#2trainCostSEP').val(),
        //trainCostOCT2 : $('#2trainCostOCT').val(),
        //trainCostNOV2 : $('#2trainCostNOV').val(),
        //trainCostDEC2 : $('#2trainCostDEC').val(),
        //tecCostJAN2 : $('#2tecCostJAN').val(),
        //tecCostFEB2 : $('#2tecCostFEB').val(),
        //tecCostMAR2 : $('#2tecCostMAR').val(),
        //tecCostAPR2 : $('#2tecCostAPR').val(),
        //tecCostMAY2 : $('#2tecCostMAY').val(),
        //tecCostJUN2 : $('#2tecCostJUN').val(),
        //tecCostJUL2 : $('#2tecCostJUL').val(),
        //tecCostAUG2 : $('#2tecCostAUG').val(),
        //tecCostSEP2 : $('#2tecCostSEP').val(),
        //tecCostOCT2 : $('#2tecCostOCT').val(),
        //tecCostNOV2 : $('#2tecCostNOV').val(),
        //tecCostDEC2: $('#2tecCostDEC').val(),
        dsJan3: $('#3dsJan').val(),
        dsFeb3: $('#3dsFeb').val(),
        dsMar3: $('#3dsMar').val(),
        dsApr3: $('#3dsApr').val(),
        dsMay3: $('#3dsMay').val(),
        dsJun3: $('#3dsJun').val(),
        dsJul3: $('#3dsJul').val(),
        dsAug3: $('#3dsAug').val(),
        dsSep3: $('#3dsSep').val(),
        dsOct3: $('#3dsOct').val(),
        dsNov3: $('#3dsNov').val(),
        dsDec3: $('#3dsDec').val(),
        osJAN3: $('#3osJAN').val(),
        osFEB3: $('#3osFEB').val(),
        osMAR3: $('#3osMAR').val(),
        osAPR3: $('#3osAPR').val(),
        osMAY3: $('#3osMAY').val(),
        osJUN3: $('#3osJUN').val(),
        osJUL3: $('#3osJUL').val(),
        osAUG3: $('#3osAUG').val(),
        osSEP3: $('#3osSEP').val(),
        osOCT3: $('#3osOCT').val(),
        osNOV3: $('#3osNOV').val(),
        osDEC3: $('#3osDEC').val(),

        comment: $('#mytextarea').val()

    }


    _callServer({
        loadingMsg: "Get total MOU...",
        url: '/HMT2/saveNonMOU',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {

                validateSaveData();
            

        }
    });

   

}


function renderDataSaveData() {


    var listOfParams = {
        pNonMOU: $('#txtNonMOUID').val()
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getDataDiscovery',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            $.each((response), function (index, element) {

                $('#txtFTEDiscovery').jqxNumberInput('val', element.FTE);

                $('#txtNameDiscovery').val(element.Name);
                $('#ScopeDiscovery').val(element.Scope);
                $('#BenefitsDiscovery').val(element.Benefits);
                $("#jqxDropDownMOUType").jqxComboBox('selectItem', element.Type);

                var year = element.Initiated.split("-")[0];
                var month = element.Initiated.split("-")[1] - 1;
                var day = element.Initiated.split("-")[2].replace('T00:00:00', '');

                $('#dateInitiated ').jqxDateTimeInput('setDate', new Date(year, month, day));

                var year2 = element.FTEAvaible.split("-")[0];
                var month2 = element.FTEAvaible.split("-")[1] - 1;
                var day2 = element.FTEAvaible.split("-")[2].replace('T00:00:00', '');

                $('#dateAvaible ').jqxDateTimeInput('setDate', new Date(year2, month2, day2));


                $('#receivingFTE').jqxNumberInput('val', element.FTEStandarCost);
                $("#cbPhysicalLocation").jqxComboBox('selectItem', element.Expr1);
                $("#jqxSiteManager").val(element.SiteManager);
                $("#jqxSiteFinanceManger").val(element.SiteFinanceManager);

                $("#lblRequestBy").text(_getUserInfo().Name);

                renderFinancialManagedSegmentLVL09();

            });
        }
    });

}

function validateSaveData() {
    closeNotifications();
    $("#jqxExpanderCharter").jqxExpander({ expanded: false });
    $("#jqxExpanderMilestone").jqxExpander({ expanded: false });
    $("#jqxExpanderFinancial").jqxExpander({ expanded: false });
    $("#jqxExpanderApproval").jqxExpander({ expanded: false });
    $("#jqxExpanderAttachment").jqxExpander({ expanded: false });
    $("#jqxExpanderDiscussion").jqxExpander({ expanded: false });


    var listOfParams = {
        pNonMOU: $('#txtNonMOUID').val()
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getValidationDiscoveryForm',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            closeNotifications();

            $.each((response), function (index, element) {

                if (element.STEP == 1) {
                    $("#jqxExpanderCharter").jqxExpander({ expanded: true });
                    if (element.WARNING == true) {
                        $('#WarnlblErrorCharter').text(element.ERROR);
                        $("#WarnjqxErrorCharter").jqxNotification("open");

                    }
                    else {
                        $('#lblErrorCharter').text(element.ERROR);
                        $("#jqxErrorCharter").jqxNotification("open");

                    }
                }
                if (element.STEP == 2) {

                    $("#jqxExpanderMilestone").jqxExpander({ expanded: true });
                    if (element.WARNING == true) {
                        $('#WarnlblErrorMilestone').text(element.ERROR);
                        $("#WarnjqxErrorMilestone").jqxNotification("open");

                    }
                    else {
                        $('#lblErrorMilestone').text(element.ERROR);
                        $("#jqxErrorMilestone").jqxNotification("open");

                    }
                }
                if (element.STEP == 3) {
                    $("#jqxExpanderFinancial").jqxExpander({ expanded: true });
                    if (element.WARNING == true) {
                        $('#WarnlblErrorFinancial').text(element.ERROR);
                        $("#WarnjqxErrorFinancial").jqxNotification("open");

                    }
                    else {

                        $('#lblErrorFinancial').text(element.ERROR);
                        $("#jqxErrorFinancial").jqxNotification("open");

                    }
                }
                if (element.STEP == 4) {
                    if (element.WARNING == true) {

                        $('#WarnlblErrorApproval').text(element.ERROR);
                        $("#WarnjqxErrorApproval").jqxNotification("open");

                    }
                    else {

                        $('#lblErrorApproval').text(element.ERROR);
                        $("#jqxErrorApproval").jqxNotification("open");

                    }
                }

                if (element.STEP == 0) {
                    if (element.WARNING == true) {
                        $('#WarnlblSaveDiscovery').text(element.ERROR);
                        $("#WarnjqxSaveDiscovery").jqxNotification("open");

                    }
                    else {
                        $("#jqxExpanderCharter").jqxExpander({ expanded: true });
                        $('#lblSaveDiscovery').text(element.ERROR);
                        $("#jqxSaveDiscovery").jqxNotification("open");

                    }
                }
            });
        }
    });

}


function saveMOUInDiscoveryAndSendForApproval() {


    var listOfParams = {
        txtNonMOUID: $('#txtNonMOUID').val(),
        txtFTEDiscovery: $('#txtFTEDiscovery').jqxNumberInput('val'),
        txtNameDiscovery: $('#txtNameDiscovery').val(),
        jqxDropDownMOUType: $('#jqxDropDownMOUType').val(),
        ScopeDiscovery: $('#ScopeDiscovery').val(),
        BenefitsDiscovery: $('#BenefitsDiscovery').val(),
        dateInitiated: $('#dateInitiated').val(),
        dateAvaible: $('#dateAvaible').val(),
        // jqxGridMilestones : $('#jqxGridMilestones').jqxGrid('exportdata', 'json'),
        receivingFTE: $('#receivingFTE').jqxNumberInput('val'),
        cbPhysicalLocation: $('#cbPhysicalLocation').val(),
        jqxSiteManager: $('#jqxSiteManager').val().split(' ')[0],
        jqxSiteFinanceManger: $('#jqxSiteFinanceManger').val().split(' ')[0],

        MSLVL09: $("#cbManagedSegmentLvl9").val(),
        dsTotal: $('#dsTotal').text(),
        lblDSJAN: $('#lblDSJAN').text(),
        lblDSFEB: $('#lblDSFEB').text(),
        lblDSMAR: $('#lblDSMAR').text(),
        lblDSAPR: $('#lblDSAPR').text(),
        lblDSMAY: $('#lblDSMAY').text(),
        lblDSJUN: $('#lblDSJUN').text(),
        lblDSJUL: $('#lblDSJUL').text(),
        lblDSAUG: $('#lblDSAUG').text(),
        lblDSSEP: $('#lblDSSEP').text(),
        lblDSOCT: $('#lblDSOCT').text(),
        lblDSNOV: $('#lblDSNOV').text(),
        lblDSDEC: $('#lblDSDEC').text(),
        lblDSTotal: $('#lblDSTotal').text(),
        osTotal: $('#osTotal').text(),
        lblOSJAN: $('#lblOSJAN').text(),
        lblOSFEB: $('#lblOSFEB').text(),
        lblOSMAR: $('#lblOSMAR').text(),
        lblOSAPR: $('#lblOSAPR').text(),
        lblOSMAY: $('#lblOSMAY').text(),
        lblOSJUN: $('#lblOSJUN').text(),
        lblOSJUL: $('#lblOSJUL').text(),
        lblOSAUG: $('#lblOSAUG').text(),
        lblOSSEP: $('#lblOSSEP').text(),
        lblOSOCT: $('#lblOSOCT').text(),
        lblOSNOV: $('#lblOSNOV').text(),
        lblOSDEC: $('#lblOSDEC').text(),
        lblOSTOTAL: $('#lblOSTOTAL').text(),
        fteJAN: $('#fteJAN').text(),
        fteFEB: $('#fteFEB').text(),
        fteMAR: $('#fteMAR').text(),
        fteAPR: $('#fteAPR').text(),
        fteMAY: $('#fteMAY').text(),
        fteJUN: $('#fteJUN').text(),
        fteJUL: $('#fteJUL').text(),
        fteAUG: $('#fteAUG').text(),
        fteSEP: $('#fteSEP').text(),
        fteOCT: $('#fteOCT').text(),
        fteNOV: $('#fteNOV').text(),
        fteDEC: $('#fteDEC').text(),
        fteTOTAL: $('#fteTOTAL').text(),
        // trainCostTotal : $('#trainCostTotal').text(),
        // tecCostTOTAL : $('#tecCostTOTAL').text(),
        FTETotalCostJAN: $('#FTETotalCostJAN').text(),
        FTETotalCostFEB: $('#FTETotalCostFEB').text(),
        FTETotalCostMAR: $('#FTETotalCostMAR').text(),
        FTETotalCostAPR: $('#FTETotalCostAPR').text(),
        FTETotalCostMAY: $('#FTETotalCostMAY').text(),
        FTETotalCostJUN: $('#FTETotalCostJUN').text(),
        FTETotalCostJUL: $('#FTETotalCostJUL').text(),
        FTETotalCostAUG: $('#FTETotalCostAUG').text(),
        FTETotalCostSEP: $('#FTETotalCostSEP').text(),
        FTETotalCostOCT: $('#FTETotalCostOCT').text(),
        FTETotalCostNOV: $('#FTETotalCostNOV').text(),
        FTETotalCostDEC: $('#FTETotalCostDEC').text(),
        FTETotalCostTOTAL: $('#FTETotalCostTOTAL').text(),
        //totalCostJAN : $('#totalCostJAN').text(),
        //totalCostFEB : $('#totalCostFEB').text(),
        //totalCostMAR : $('#totalCostMAR').text(),
        //totalCostAPR : $('#totalCostAPR').text(),
        //totalCostMAY : $('#totalCostMAY').text(),
        //totalCostJUN : $('#totalCostJUN').text(),
        //totalCostJUL : $('#totalCostJUL').text(),
        //totalCostAUG : $('#totalCostAUG').text(),
        //totalCostSEP : $('#totalCostSEP').text(),
        //totalCostOCT : $('#totalCostOCT').text(),
        //totalCostNOV : $('#totalCostNOV').text(),
        //totalCostDEC : $('#totalCostDEC').text(),
        //totalCostTOTAL : $('#totalCostTOTAL').text(),
        dsTotal2: $('#2dsTotal').text(),
        lblDSJAN2: $('#2lblDSJAN').text(),
        lblDSFEB2: $('#2lblDSFEB').text(),
        lblDSMAR2: $('#2lblDSMAR').text(),
        lblDSAPR2: $('#2lblDSAPR').text(),
        lblDSMAY2: $('#2lblDSMAY').text(),
        lblDSJUN2: $('#2lblDSJUN').text(),
        lblDSJUL2: $('#2lblDSJUL').text(),
        lblDSAUG2: $('#2lblDSAUG').text(),
        lblDSSEP2: $('#2lblDSSEP').text(),
        lblDSOCT2: $('#2lblDSOCT').text(),
        lblDSNOV2: $('#2lblDSNOV').text(),
        lblDSDEC2: $('#2lblDSDEC').text(),
        lblDSTotal2: $('#2lblDSTotal').text(),
        osTotal2: $('#2osTotal').text(),
        lblOSJAN2: $('#2lblOSJAN').text(),
        lblOSFEB2: $('#2lblOSFEB').text(),
        lblOSMAR2: $('#2lblOSMAR').text(),
        lblOSAPR2: $('#2lblOSAPR').text(),
        lblOSMAY2: $('#2lblOSMAY').text(),
        lblOSJUN2: $('#2lblOSJUN').text(),
        lblOSJUL2: $('#2lblOSJUL').text(),
        lblOSAUG2: $('#2lblOSAUG').text(),
        lblOSSEP2: $('#2lblOSSEP').text(),
        lblOSOCT2: $('#2lblOSOCT').text(),
        lblOSNOV2: $('#2lblOSNOV').text(),
        lblOSDEC2: $('#2lblOSDEC').text(),
        lblOSTOTAL2: $('#2lblOSTOTAL').text(),
        fteJAN2: $('#2fteJAN').text(),
        fteFEB2: $('#2fteFEB').text(),
        fteMAR2: $('#2fteMAR').text(),
        fteAPR2: $('#2fteAPR').text(),
        fteMAY2: $('#2fteMAY').text(),
        fteJUN2: $('#2fteJUN').text(),
        fteJUL2: $('#2fteJUL').text(),
        fteAUG2: $('#2fteAUG').text(),
        fteSEP2: $('#2fteSEP').text(),
        fteOCT2: $('#2fteOCT').text(),
        fteNOV2: $('#2fteNOV').text(),
        fteDEC2: $('#2fteDEC').text(),
        fteTOTAL2: $('#2fteTOTAL').text(),
        //trainCostTotal2 : $('#2trainCostTotal').text(),
        //tecCostTOTAL2 : $('#2tecCostTOTAL').text(),
        FTETotalCostJAN2: $('#2FTETotalCostJAN').text(),
        FTETotalCostFEB2: $('#2FTETotalCostFEB').text(),
        FTETotalCostMAR2: $('#2FTETotalCostMAR').text(),
        FTETotalCostAPR2: $('#2FTETotalCostAPR').text(),
        FTETotalCostMAY2: $('#2FTETotalCostMAY').text(),
        FTETotalCostJUN2: $('#2FTETotalCostJUN').text(),
        FTETotalCostJUL2: $('#2FTETotalCostJUL').text(),
        FTETotalCostAUG2: $('#2FTETotalCostAUG').text(),
        FTETotalCostSEP2: $('#2FTETotalCostSEP').text(),
        FTETotalCostOCT2: $('#2FTETotalCostOCT').text(),
        FTETotalCostNOV2: $('#2FTETotalCostNOV').text(),
        FTETotalCostDEC2: $('#2FTETotalCostDEC').text(),
        FTETotalCostTOTAL2: $('#2FTETotalCostTOTAL').text(),
        //totalCostJAN2 : $('#2totalCostJAN').text(),
        //totalCostFEB2 : $('#2totalCostFEB').text(),
        //totalCostMAR2 : $('#2totalCostMAR').text(),
        //totalCostAPR2 : $('#2totalCostAPR').text(),
        //totalCostMAY2 : $('#2totalCostMAY').text(),
        //totalCostJUN2 : $('#2totalCostJUN').text(),
        //totalCostJUL2 : $('#2totalCostJUL').text(),
        //totalCostAUG2 : $('#2totalCostAUG').text(),
        //totalCostSEP2 : $('#2totalCostSEP').text(),
        //totalCostOCT2 : $('#2totalCostOCT').text(),
        //totalCostNOV2 : $('#2totalCostNOV').text(),
        //totalCostDEC2 : $('#2totalCostDEC').text(),
        //totalCostTOTAL2 : $('#2totalCostTOTAL').text(),

        dsTotal3: $('#3dsTotal').text(),
        lblDSJAN3: $('#3lblDSJAN').text(),
        lblDSFEB3: $('#3lblDSFEB').text(),
        lblDSMAR3: $('#3lblDSMAR').text(),
        lblDSAPR3: $('#3lblDSAPR').text(),
        lblDSMAY3: $('#3lblDSMAY').text(),
        lblDSJUN3: $('#3lblDSJUN').text(),
        lblDSJUL3: $('#3lblDSJUL').text(),
        lblDSAUG3: $('#3lblDSAUG').text(),
        lblDSSEP3: $('#3lblDSSEP').text(),
        lblDSOCT3: $('#3lblDSOCT').text(),
        lblDSNOV3: $('#3lblDSNOV').text(),
        lblDSDEC3: $('#3lblDSDEC').text(),
        lblDSTotal3: $('#3lblDSTotal').text(),
        osTotal3: $('#3osTotal').text(),
        lblOSJAN3: $('#3lblOSJAN').text(),
        lblOSFEB3: $('#3lblOSFEB').text(),
        lblOSMAR3: $('#3lblOSMAR').text(),
        lblOSAPR3: $('#3lblOSAPR').text(),
        lblOSMAY3: $('#3lblOSMAY').text(),
        lblOSJUN3: $('#3lblOSJUN').text(),
        lblOSJUL3: $('#3lblOSJUL').text(),
        lblOSAUG3: $('#3lblOSAUG').text(),
        lblOSSEP3: $('#3lblOSSEP').text(),
        lblOSOCT3: $('#3lblOSOCT').text(),
        lblOSNOV3: $('#3lblOSNOV').text(),
        lblOSDEC3: $('#3lblOSDEC').text(),
        lblOSTOTAL3: $('#3lblOSTOTAL').text(),
        fteJAN3: $('#3fteJAN').text(),
        fteFEB3: $('#3fteFEB').text(),
        fteMAR3: $('#3fteMAR').text(),
        fteAPR3: $('#3fteAPR').text(),
        fteMAY3: $('#3fteMAY').text(),
        fteJUN3: $('#3fteJUN').text(),
        fteJUL3: $('#3fteJUL').text(),
        fteAUG3: $('#3fteAUG').text(),
        fteSEP3: $('#3fteSEP').text(),
        fteOCT3: $('#3fteOCT').text(),
        fteNOV3: $('#3fteNOV').text(),
        fteDEC3: $('#3fteDEC').text(),
        fteTOTAL3: $('#3fteTOTAL').text(),
        //trainCostTotal3 : $('#3trainCostTotal').text(),
        //tecCostTOTAL3 : $('#3tecCostTOTAL').text(),
        FTETotalCostJAN3: $('#3FTETotalCostJAN').text(),
        FTETotalCostFEB3: $('#3FTETotalCostFEB').text(),
        FTETotalCostMAR3: $('#3FTETotalCostMAR').text(),
        FTETotalCostAPR3: $('#3FTETotalCostAPR').text(),
        FTETotalCostMAY3: $('#3FTETotalCostMAY').text(),
        FTETotalCostJUN3: $('#3FTETotalCostJUN').text(),
        FTETotalCostJUL3: $('#3FTETotalCostJUL').text(),
        FTETotalCostAUG3: $('#3FTETotalCostAUG').text(),
        FTETotalCostSEP3: $('#3FTETotalCostSEP').text(),
        FTETotalCostOCT3: $('#3FTETotalCostOCT').text(),
        FTETotalCostNOV3: $('#3FTETotalCostNOV').text(),
        FTETotalCostDEC3: $('#3FTETotalCostDEC').text(),
        FTETotalCostTOTAL3: $('#3FTETotalCostTOTAL').text(),

        dsJan: $('#dsJan').val(),
        dsFeb: $('#dsFeb').val(),
        dsMar: $('#dsMar').val(),
        dsApr: $('#dsApr').val(),
        dsMay: $('#dsMay').val(),
        dsJun: $('#dsJun').val(),
        dsJul: $('#dsJul').val(),
        dsAug: $('#dsAug').val(),
        dsSep: $('#dsSep').val(),
        dsOct: $('#dsOct').val(),
        dsNov: $('#dsNov').val(),
        dsDec: $('#dsDec').val(),
        osJAN: $('#osJAN').val(),
        osFEB: $('#osFEB').val(),
        osMAR: $('#osMAR').val(),
        osAPR: $('#osAPR').val(),
        osMAY: $('#osMAY').val(),
        osJUN: $('#osJUN').val(),
        osJUL: $('#osJUL').val(),
        osAUG: $('#osAUG').val(),
        osSEP: $('#osSEP').val(),
        osOCT: $('#osOCT').val(),
        osNOV: $('#osNOV').val(),
        osDEC: $('#osDEC').val(),
        dsJan2: $('#2dsJan').val(),
        dsFeb2: $('#2dsFeb').val(),
        dsMar2: $('#2dsMar').val(),
        dsApr2: $('#2dsApr').val(),
        dsMay2: $('#2dsMay').val(),
        dsJun2: $('#2dsJun').val(),
        dsJul2: $('#2dsJul').val(),
        dsAug2: $('#2dsAug').val(),
        dsSep2: $('#2dsSep').val(),
        dsOct2: $('#2dsOct').val(),
        dsNov2: $('#2dsNov').val(),
        dsDec2: $('#2dsDec').val(),
        osJAN2: $('#2osJAN').val(),
        osFEB2: $('#2osFEB').val(),
        osMAR2: $('#2osMAR').val(),
        osAPR2: $('#2osAPR').val(),
        osMAY2: $('#2osMAY').val(),
        osJUN2: $('#2osJUN').val(),
        osJUL2: $('#2osJUL').val(),
        osAUG2: $('#2osAUG').val(),
        osSEP2: $('#2osSEP').val(),
        osOCT2: $('#2osOCT').val(),
        osNOV2: $('#2osNOV').val(),
        osDEC2: $('#2osDEC').val(),
        //trainCostJAN : $('#trainCostJAN').val(),
        //trainCostFEB : $('#trainCostFEB').val(),
        //trainCostMAR : $('#trainCostMAR').val(),
        //trainCostAPR : $('#trainCostAPR').val(),
        //trainCostMAY : $('#trainCostMAY').val(),
        //trainCostJUN : $('#trainCostJUN').val(),
        //trainCostJUL : $('#trainCostJUL').val(),
        //trainCostAUG : $('#trainCostAUG').val(),
        //trainCostSEP : $('#trainCostSEP').val(),
        //trainCostOCT : $('#trainCostOCT').val(),
        //trainCostNOV : $('#trainCostNOV').val(),
        //trainCostDEC : $('#trainCostDEC').val(),
        //tecCostJAN : $('#tecCostJAN').val(),
        //tecCostFEB : $('#tecCostFEB').val(),
        //tecCostMAR : $('#tecCostMAR').val(),
        //tecCostAPR : $('#tecCostAPR').val(),
        //tecCostMAY : $('#tecCostMAY').val(),
        //tecCostJUN : $('#tecCostJUN').val(),
        //tecCostJUL : $('#tecCostJUL').val(),
        //tecCostAUG : $('#tecCostAUG').val(),
        //tecCostSEP : $('#tecCostSEP').val(),
        //tecCostOCT : $('#tecCostOCT').val(),
        //tecCostNOV : $('#tecCostNOV').val(),
        //tecCostDEC : $('#tecCostDEC').val(),
        //trainCostJAN2 : $('#2trainCostJAN').val(),
        //trainCostFEB2 : $('#2trainCostFEB').val(),
        //trainCostMAR2 : $('#2trainCostMAR').val(),
        //trainCostAPR2 : $('#2trainCostAPR').val(),
        //trainCostMAY2 : $('#2trainCostMAY').val(),
        //trainCostJUN2 : $('#2trainCostJUN').val(),
        //trainCostJUL2 : $('#2trainCostJUL').val(),
        //trainCostAUG2 : $('#2trainCostAUG').val(),
        //trainCostSEP2 : $('#2trainCostSEP').val(),
        //trainCostOCT2 : $('#2trainCostOCT').val(),
        //trainCostNOV2 : $('#2trainCostNOV').val(),
        //trainCostDEC2 : $('#2trainCostDEC').val(),
        //tecCostJAN2 : $('#2tecCostJAN').val(),
        //tecCostFEB2 : $('#2tecCostFEB').val(),
        //tecCostMAR2 : $('#2tecCostMAR').val(),
        //tecCostAPR2 : $('#2tecCostAPR').val(),
        //tecCostMAY2 : $('#2tecCostMAY').val(),
        //tecCostJUN2 : $('#2tecCostJUN').val(),
        //tecCostJUL2 : $('#2tecCostJUL').val(),
        //tecCostAUG2 : $('#2tecCostAUG').val(),
        //tecCostSEP2 : $('#2tecCostSEP').val(),
        //tecCostOCT2 : $('#2tecCostOCT').val(),
        //tecCostNOV2 : $('#2tecCostNOV').val(),
        //tecCostDEC2: $('#2tecCostDEC').val(),
        dsJan3: $('#3dsJan').val(),
        dsFeb3: $('#3dsFeb').val(),
        dsMar3: $('#3dsMar').val(),
        dsApr3: $('#3dsApr').val(),
        dsMay3: $('#3dsMay').val(),
        dsJun3: $('#3dsJun').val(),
        dsJul3: $('#3dsJul').val(),
        dsAug3: $('#3dsAug').val(),
        dsSep3: $('#3dsSep').val(),
        dsOct3: $('#3dsOct').val(),
        dsNov3: $('#3dsNov').val(),
        dsDec3: $('#3dsDec').val(),
        osJAN3: $('#3osJAN').val(),
        osFEB3: $('#3osFEB').val(),
        osMAR3: $('#3osMAR').val(),
        osAPR3: $('#3osAPR').val(),
        osMAY3: $('#3osMAY').val(),
        osJUN3: $('#3osJUN').val(),
        osJUL3: $('#3osJUL').val(),
        osAUG3: $('#3osAUG').val(),
        osSEP3: $('#3osSEP').val(),
        osOCT3: $('#3osOCT').val(),
        osNOV3: $('#3osNOV').val(),
        osDEC3: $('#3osDEC').val(),

        comment: $('#mytextarea').val()

    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/saveNonMOU',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            
                validateSubbmitForApproval();
            
        }
    });

}


function validateSubbmitForApproval() {
    closeNotifications();
    $("#jqxExpanderCharter").jqxExpander({ expanded: false });
    $("#jqxExpanderMilestone").jqxExpander({ expanded: false });
    $("#jqxExpanderFinancial").jqxExpander({ expanded: false });
    $("#jqxExpanderApproval").jqxExpander({ expanded: false });
    $("#jqxExpanderAttachment").jqxExpander({ expanded: false });
    $("#jqxExpanderDiscussion").jqxExpander({ expanded: false });


    var listOfParams = {
        pNonMOU: $('#txtNonMOUID').val()
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/subbmitForApprovalDiscovery',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            closeNotifications();

            $.each((response), function (index, element) {

                if (element.ERROR == "Continue") {
                    Management_tab();

                    getListQueue();

                    $("#jqxNotSendForApproval").jqxNotification("open");

                }

                if (element.STEP == 1) {
                    $("#jqxExpanderCharter").jqxExpander({ expanded: true });
                    if (element.WARNING == true) {
                        $('#WarnlblErrorCharter').text(element.ERROR);
                        $("#WarnjqxErrorCharter").jqxNotification("open");

                    }
                    else {
                        $('#lblErrorCharter').text(element.ERROR);
                        $("#jqxErrorCharter").jqxNotification("open");

                    }
                }
                if (element.STEP == 2) {

                    $("#jqxExpanderMilestone").jqxExpander({ expanded: true });
                    if (element.WARNING == true) {
                        $('#WarnlblErrorMilestone').text(element.ERROR);
                        $("#WarnjqxErrorMilestone").jqxNotification("open");

                    }
                    else {
                        $('#lblErrorMilestone').text(element.ERROR);
                        $("#jqxErrorMilestone").jqxNotification("open");

                    }
                }
                if (element.STEP == 3) {
                    $("#jqxExpanderFinancial").jqxExpander({ expanded: true });
                    if (element.WARNING == true) {
                        $('#WarnlblErrorFinancial').text(element.ERROR);
                        $("#WarnjqxErrorFinancial").jqxNotification("open");

                    }
                    else {

                        $('#lblErrorFinancial').text(element.ERROR);
                        $("#jqxErrorFinancial").jqxNotification("open");

                    }
                }
                if (element.STEP == 4) {
                    if (element.WARNING == true) {

                        $('#WarnlblErrorApproval').text(element.ERROR);
                        $("#WarnjqxErrorApproval").jqxNotification("open");

                    }
                    else {

                        $('#lblErrorApproval').text(element.ERROR);
                        $("#jqxErrorApproval").jqxNotification("open");

                    }
                }

                if (element.STEP == 0) {
                    if (element.WARNING == true) {
                        $('#WarnlblSaveDiscovery').text(element.ERROR);
                        $("#WarnjqxSaveDiscovery").jqxNotification("open");

                    }
                    else {
                        $("#jqxExpanderCharter").jqxExpander({ expanded: true });
                        $('#lblSaveDiscovery').text(element.ERROR);
                        $("#jqxSaveDiscovery").jqxNotification("open");

                    }
                }
            });
        }
    });

}

function renderApprovalStage() {

    $("#NotificationPendingApproval").jqxNotification({
        width: '100%', position: "top-left", opacity: 0.9, appendContainer: "#cointainerPendingApproval",
        autoOpen: false, animationOpenDelay: 100, autoClose: true, autoCloseDelay: 20000, template: "success"
    });

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getListPendingForApproval',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            if (response) {
                var data2 = (response);
                if (data2.lenght == 0) {

                    $('#lblCountPendingApproval').text(data2.length);

                }
                else {

                    $('#lblCountPendingApproval').text(data2.length);

                    var source =
                   {
                       unboundmode: true,
                       localdata: data2,
                       datafields:
                               [
                                { name: 'ID', type: 'string' },
                                { name: 'RequestAutoID', type: 'string' },
                                { name: 'Name', type: 'string' },
                                { name: 'FTE', type: 'string' },
                                { name: 'ManagedSegmentName', type: 'string' },
                                { name: 'ManagedGeographyName', type: 'string' },
                                { name: 'DateAction', type: 'string' },
                                { name: 'ApprovalID', type: 'string' }
                               ],
                       datatype: "json"
                   };
                    var dataAdapter = new $.jqx.dataAdapter(source);

                    var imgEditDiscovery = function (row, datafield, value) {

                        var rowData = $('#jqxGridApproval').jqxGrid('getrowdata', row);


                        return '<center><i class="fa fa-search" style=" font-size: 23px; color: #4286f4;" onclick="openPopUpDetailApprove(' + rowData.ID + ')" ></i></center>'

                    }

                    $('#jqxGridApproval').jqxGrid(
                   {
                       source: dataAdapter,

                       width: '100%',
                       selectionmode: 'none',
                       theme: 'blackberryRed',
                       selectionmode: 'checkbox',
                       autoheight: true,
                       height: 200,
                       pageable: true,
                       pagesize: 5,
                       filterable: false,
                       columnsresize: true,
                       altrows: true,
                       columns: [
                       { text: 'Review Detail', filtertype: 'none', width: '5%', columntype: 'image', pinned: false, cellsrenderer: imgEditDiscovery },
                       { text: 'Non/MOUID', dataField: 'RequestAutoID', filtertype: 'input', width: '15%', editable: false },
                       { text: 'Date Action', dataField: 'DateAction', filtertype: 'input', width: '15%', editable: false },
                       { text: 'Name', dataField: 'Name', filtertype: 'input', width: '15%', editable: false },
                       { text: 'FTE', dataField: 'FTE', filtertype: 'input', width: '15%', editable: false },
                       { text: 'Managed Segment Name', dataField: 'ManagedSegmentName', filtertype: 'input', width: '15%', editable: false },
                       { text: 'Managed Geography Name', dataField: 'ManagedGeographyName', filtertype: 'input', width: '15%', editable: false },
                       ]

                   });
                }
            }
        }
    });

}

function openPopUpDetailApprove(ID) {
    $("#ViewHTML").empty();

    var listOfParams = {
        pID: ID
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getMigrationDetail',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            closeNotifications();

            $.each((response), function (index, element) {

                $("#ViewHTML").append(element.HTML);
                $("#popUpLabelNM").text(element.Number);
            });
         
            $("#popUpApprovalRequest").jqxWindow('open');
         
        }
    });



}



function approveOne() {

    var listOfParams = {
        pID: $("#popUpLabelNM").text()
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/approveNonMOU',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            

            $("#popUpApprovalRequest").jqxWindow('close');

            Approval_tab();

        }
    });


}

function RejectOne() {

    var listOfParams = {
        pID: $("#popUpLabelNM").text()
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/rejectNonMOU',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            

            $("#popUpApprovalRequest").jqxWindow('close');

            Approval_tab();

        }
    });

}

function ApproveSelected() {

    var array = $('#jqxGridApproval').jqxGrid('getselectedrowindexes');
    var arrayLength = array.length;

    for (var i = 0; i < arrayLength; i++) {

        var listOfParams = {
            pID: $('#jqxGridApproval').jqxGrid('getrowdata', array[i]).RequestAutoID
        }

        _callServer({
            loadingMsg: "", async: false,
            url: '/NonMOU/approveNonMOU',
            data: {
                'data': JSON.stringify(listOfParams)
            },
            type: "post",
            success: function (json) {
                
                $("#popUpApprovalRequest").jqxWindow('close');

                Approval_tab();

            }
        });

    }
}


function RejectSelected() {

    var array = $('#jqxGridApproval').jqxGrid('getselectedrowindexes');
    var arrayLength = array.length;


    for (var i = 0; i < arrayLength; i++) {

        var listOfParams = {
            pID: $('#jqxGridApproval').jqxGrid('getrowdata', array[i]).RequestAutoID
        }

        _callServer({
            loadingMsg: "", async: false,
            url: '/NonMOU/rejectNonMOU',
            data: {
                'data': JSON.stringify(listOfParams)
            },
            type: "post",
            success: function (json) {
                

                $("#popUpApprovalRequest").jqxWindow('close');

                Approval_tab();
            }
        });


    }




}



function renderCompleteStage() {

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getListLiveStage',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            if (response) {
                var data2 = (response);

                var source =
               {
                   unboundmode: true,
                   localdata: data2,
                   datafields:
                           [
                            { name: 'ID', type: 'string' },
                            { name: 'RequestAutoID', type: 'string' },
                            { name: 'Name', type: 'string' },
                            { name: 'FTE', type: 'string' },
                            { name: 'ManagedSegmentName', type: 'string' },
                            { name: 'ManagedGeographyName', type: 'string' },
                           ],
                   datatype: "json"
               };
                var dataAdapter = new $.jqx.dataAdapter(source);

                var imgEditDiscovery = function (row, datafield, value) {

                    var rowData = $('#jqxGridLiveStage').jqxGrid('getrowdata', row);


                    return '<center><i class="fa fa-search" style=" font-size: 23px; color: #4286f4;" onclick="openPopUpLiveStage(' + rowData.ID + ')" ></i></center>'

                }

                $('#jqxGridLiveStage').jqxGrid(
               {
                   source: dataAdapter,

                   width: '100%',
                   selectionmode: 'none',
                   theme: 'blackberryGreen',
                   selectionmode: 'checkbox',
                   autoheight: true,
                   height: 200,
                   pageable: true,
                   pagesize: 10,
                   filterable: false,
                   columnsresize: true,
                   altrows: true,
                   columns: [
                   { text: 'Review Detail', filtertype: 'none', width: '5%', columntype: 'image', pinned: false, cellsrenderer: imgEditDiscovery },
                   { text: 'Non/MOUID', dataField: 'RequestAutoID', filtertype: 'input', width: '15%', editable: false },
                   { text: 'Name', dataField: 'Name', filtertype: 'input', width: '15%', editable: false },
                   { text: 'FTE', dataField: 'FTE', filtertype: 'input', width: '15%', editable: false },
                   { text: 'Managed Segment Name', dataField: 'ManagedSegmentName', filtertype: 'input', width: '20%', editable: false },
                   { text: 'Managed Geography Name', dataField: 'ManagedGeographyName', filtertype: 'input', width: '20%', editable: false },
                   ]

               });

            }
        }
    });

}


function openPopUpLiveStage(ID) {
    $("#innerHTMLLive").empty();

    var listOfParams = {
        pID: ID
    }

    _callServer({
        loadingMsg: "Get information from NonMOU...",
        url: '/NonMOU/getMigrationDetail',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (response) {
            closeNotifications();

            var result = jQuery.parseJSON(response);
            $.each((result), function (index, element) {

                $("#innerHTMLLive").append(element.HTML);
                $("#lblLiveStage").text(element.Number);
            });
          
            $("#popUpLiveStage").jqxWindow('open');
           
        }
    });



}


function getTotals() {

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getTotals',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);


            $.each((response), function (index, element) {


                $("#totalRequest").text(element.Request);
                $("#totalDiscovery").text(element.Discovery);
                $("#totalApproval").text(element.Approval);
                $("#totalLive").text(element.Live);
            });
        }
    });

}




function renderCBFinancialOther() {

    var data2 = '[{"Month": "JAN"},{"Month": "FEB"},{"Month": "MAR"},{"Month": "APR"},{"Month": "MAY"},{"Month": "JUN"},{"Month": "JUL"},{"Month": "AUG"},{"Month": "SEP"},{"Month": "OCT"},{"Month": "NOV"},{"Month": "DEC"}]';

    var source =
        {
            localdata: data2,
            datatype: "json",
            datafields: [

                { name: 'Month' }
            ],
            async: false
        };

    var dataAdapter = new $.jqx.dataAdapter(source);

    $("#cbStartMonth").jqxComboBox({ selectedIndex: 0, source: dataAdapter, displayMember: "Month", valueMember: "Month", width: '99%', height: 25 });


    $("#cbStartMonth").jqxComboBox('selectItem', 'JAN');

    $("#cbStartMonth").on('select', function (event) {
        if (event.args) {
            var item = event.args.item;
        }
    });


    $("#cbEndMonth").jqxComboBox({ selectedIndex: 0, source: dataAdapter, displayMember: "Month", valueMember: "Month", width: '99%', height: 25 });


    $("#cbEndMonth").jqxComboBox('selectItem', 'JAN');

    $("#cbEndMonth").on('select', function (event) {
        if (event.args) {
            var item = event.args.item;
        }
    });

    var data23 = '[{"Year": "1"},{"Year": "2"},{"Year": "3"}]';

    var source2 =
                {
                    localdata: data23,
                    datatype: "json",
                    datafields: [

                        { name: 'Year' }
                    ],
                    async: false
                };

    var dataAdapter2 = new $.jqx.dataAdapter(source2);

    $("#cbStartYear").jqxComboBox({ selectedIndex: 0, source: dataAdapter2, displayMember: "Year", valueMember: "Year", width: '99%', height: 25 });


    $("#cbStartYear").jqxComboBox('selectItem', '1');

    $("#cbStartYear").on('select', function (event) {
        if (event.args) {
            var item = event.args.item;
        }
    });

    $("#cbEndYear").jqxComboBox({ selectedIndex: 0, source: dataAdapter2, displayMember: "Year", valueMember: "Year", width: '99%', height: 25 });


    $("#cbEndYear").jqxComboBox('selectItem', '1');

    $("#cbEndYear").on('select', function (event) {
        if (event.args) {
            var item = event.args.item;
        }
    });
}


function addOtherFTE() {
    var sm;
    var em;

    var StartM = $('#cbStartMonth').val();
    var StartY = $('#cbStartYear').val();
    var EndM = $('#cbEndMonth').val();
    var EndY = $('#cbEndYear').val();


    switch (StartM) {
        case 'JAN':
            sm = 1;
            break;
        case 'FEB':
            sm = 2;
            break;
        case 'MAR':
            sm = 3;
            break;
        case 'APR':
            sm = 4;
            break;
        case 'MAY':
            sm = 5;
            break;
        case 'JUN':
            sm = 6;
            break;
        case 'JUL':
            sm = 7;
            break;
        case 'AUG':
            sm = 8;
            break;
        case 'SEP':
            sm = 9;
            break;
        case 'OCT':
            sm = 10;
            break;
        case 'NOV':
            sm = 11;
            break;
        case 'DEC':
            sm = 12;
    }

    switch (EndM) {
        case 'JAN':
            em = 1;
            break;
        case 'FEB':
            em = 2;
            break;
        case 'MAR':
            em = 3;
            break;
        case 'APR':
            em = 4;
            break;
        case 'MAY':
            em = 5;
            break;
        case 'JUN':
            em = 6;
            break;
        case 'JUL':
            em = 7;
            break;
        case 'AUG':
            em = 8;
            break;
        case 'SEP':
            em = 9;
            break;
        case 'OCT':
            em = 10;
            break;
        case 'NOV':
            em = 11;
            break;
        case 'DEC':
            em = 12;
    }

    var error;

    if (StartY <= EndY) {
        if (StartY == EndY) {
            if (sm < em) {
                error = 0;
            } else {
                error = 1;
                $('#lblError').text("End year needs to be after start year.");
                $("#errorAddOther").jqxNotification("open");

            }
        } else {
            error = 0;
        }
    }
    else {
        error = 1;
        $('#lblError').text("End year needs to be before start year.");
        $("#errorAddOther").jqxNotification("open");
    }

    if (error == 0) {

        var listOfParams = {
            pNonMOU: $('#txtNonMOUID').val(),
            pStartM: StartM,
            pStartY: StartY,
            pEndM: EndM,
            pEndY: EndY
        }

        _callServer({
            loadingMsg: "", async: false,
            url: '/NonMOU/addNewOther',
            data: {
                'data': JSON.stringify(listOfParams)
            },
            type: "post",
            success: function (json) {
                var response = jQuery.parseJSON(json);

                renderDatesOtherfteParameter(response);

                renderFinancialDetail();
                closeNotifications();
            }
        });


    } else {

        //show ERROR
    }

}

function renderDatesOtherfte() {
    var listOfParams = {
        pNonMOU: $('#txtNonMOUID').val()
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/getOthersList',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            renderDatesOtherfteParameter(response);
        }
    });

}

function renderDatesOtherfteParameter(data) {
    var source =
                   {
                       unboundmode: true,
                       localdata: data,
                       datafields:
                               [
                                   { name: 'ID', type: 'string' },
                               { name: 'StartMonth', type: 'string' },
                               { name: 'StartYear', type: 'string' },
                               { name: 'EndMonth', type: 'string' },
                                { name: 'EndYear', type: 'string' }
                               ],
                       datatype: "json"
                   };
    var dataAdapter = new $.jqx.dataAdapter(source);

    var imgDelete = function (row, datafield, value) {

        var rowData = $('#gridOtherFTE').jqxGrid('getrowdata', row);


        // return '<a class="btn" href="#" onclick="deleteWorkingTeam(' + rowData.ID + ')" style="display:inline-block; background-color:#d9534f; width:100px;"><i class="icon-trash" style="padding-top: 8px; font-size: 20px; color: white;"></i> Delete</a></BR></BR> '
        return '<a class="btn" href="#" onclick="deleteOtherFTE(' + rowData.ID + ')" style="display:inline-block; background-color:#d9534f; width:100px;"><i class="icon-trash" style="padding-top: 8px; font-size: 20px; color: white;"></i> Delete</a>'
    }

    $('#gridOtherFTE').jqxGrid(
   {
       source: dataAdapter,

       width: '50%',
       selectionmode: 'none',
       theme: 'blackberry',
       autoheight: true,
       autorowheight: true,
       pageable: false,

       filterable: false,
       columnsresize: true,
       altrows: true,
       columns: [
       { text: '', filtertype: 'none', columntype: 'image', width: '20%', pinned: false, cellsrenderer: imgDelete },
       { text: 'StartMonth', dataField: 'StartMonth', filtertype: 'input', width: '20%', editable: false },
       { text: 'StartYear', dataField: 'StartYear', filtertype: 'input', width: '20%', editable: false },
       { text: 'EndMonth', dataField: 'EndMonth', filtertype: 'input', width: '20%', editable: false },
       { text: 'EndYear', dataField: 'EndYear', filtertype: 'input', width: '20%', editable: false },
       ]

   });

}

function deleteOtherFTE(ID) {
    var listOfParams = {
        pID: ID
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/NonMOU/deleteNewOther',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            renderDatesOtherfteParameter(response);
            renderFinancialDetail();
            closeNotifications();
        }
    });

    
}


function saveMOUFromReq() 
{
    var Req = $('#jqxDropDownRequisition').val();





}