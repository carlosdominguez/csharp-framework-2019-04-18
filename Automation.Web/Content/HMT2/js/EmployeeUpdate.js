﻿$(document).ready(function () {
    loadEmployeeTable();
    _hideMenu();
});

function loadEmployeeTable(){
    $.jqxGridApi.create({
        showTo: "#tblEmployeeUpdate",
        options: {
            //for comments or descriptions
            theme: 'energyblue',
            width: '100%',
            selectionmode: 'multiplecellsadvanced',
            sortable: true,
            height: 500,
            autoheight: false,
            autorowheight: false,
            pageable: false,
            filterable: true,
            editable: false,
            showfilterrow: true,
            columnsresize: true,
            //selectionmode: 'checkbox',
            enablebrowserselection: true,
            groupable: true,
            rowdetails: true,
            autoloadstate: false,
            autosavestate: false,
            async: false
        },
        sp: {
            Name: "[dbo].[SPHMT2_LT_EMPLOYEES]",
            Params: [
            ]
              
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            
            { name: 'ID', text: "Employee ID", type: 'string', width: '8%', hidden: true},
            { name: 'SOEID', text: "SOEID", type: 'string', width: '8%'},
            { name: 'GEID', text: "GEID", type: 'string', width: '9%'},
            { name: 'FirstName', text: "First Name", type: 'string', width: '13%'},
            { name: 'LastName', text: "Last Name", type: 'string', width: '10%'},
            { name: 'EmpGOC', text: "Employee GOC", type: 'string', width: '10%'},
            { name: 'Organization', text: "Organization", type: 'string', width: '9%', cellsrenderer: function (rowIndex) {
                    var dataRecord = $("#tblEmployeeUpdate").jqxGrid('getrowdata', rowIndex);
                    var htmlResult = '';
                    switch (dataRecord.Organization) {
                        case "FRSS":
                            htmlResult += '<center><span class="badge badge-md badge-info" style="margin-top: 5px;">FRSS</span></center>';
                            break;

                        case "Not FRSS":
                            htmlResult += '<center><span class="badge badge-md badge-warning" style="margin-top: 5px;">Not FRSS</span></center>';
                            break;

                    }

                    return htmlResult;
                }
            },
            {
                name: 'IsActive', text: "Status", type: 'string', width: '10%', cellsrenderer: function (rowIndex) {
                    var dataRecord = $("#tblEmployeeUpdate").jqxGrid('getrowdata', rowIndex);
                    var htmlResult = '';
                    switch (dataRecord.IsActive) {
                        case "True":
                            htmlResult += '<center><span class="badge badge-md badge-success" style="margin-top: 5px;">Active</span></center>';
                            break;

                        case "False":
                            htmlResult += '<center><span class="badge badge-md badge-danger" style="margin-top: 5px;">Inactive</span></center>';
                            break;

                    }

                    return htmlResult;
                }
            },
            { name: 'NumberRole', text: "Role Allocation", type: 'string', width: '11%'},
            { name: 'GOCRole', text: "Role GOC", type: 'string', width: '10%'},
            
            { name: 'Difference', text: "Need Update?", type: 'string', width: '10%', pinned: true, cellsrenderer: function (rowIndex) {
                    var dataRecord = $("#tblEmployeeUpdate").jqxGrid('getrowdata', rowIndex);
                    var htmlResult = '';
                    switch (dataRecord.Difference) {
                        case "0":
                            htmlResult += '<center><span class="badge badge-md badge-success" style="margin-top: 5px;">Updated</span></center>';
                            break;

                        case "1":
                            htmlResult += '<center><span class="badge badge-md badge-danger" style="margin-top: 5px;">Need Update</span></center>';
                            break;

                    }

                    return htmlResult;
                }
            }
        ],

        ready: function () {

            
        }
    });
}

$("#btnUpdateEmployee").click(function () {
    _callProcedure({
        loadingMsgType: "Updating Employees",
        loadingMsg: "Loading...",
        name: "[dbo].[SPHMT2_AC_GDW_UPDATE]",
        params: [],
        async: false,
        success: {
            fn: function (responseList) {
                loadEmployeeTable()
            }
        },
    });
});
