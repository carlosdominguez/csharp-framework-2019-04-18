﻿$(document).ready(function () {
    _hideMenu();

    _callServer({
        loadingMsg: "", async: false,
        url: '/MyReport/getTotalRolesList',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);
                data2 = response;

                var sampleData = data2; // loaded from sampledata.js

                var nrecoPivotExt = new NRecoPivotTableExtensions({
                    wrapWith: '<div class="pvtTableRendererHolder"></div>',  // special div is needed by fixed headers when used with pivotUI
                    fixedHeaders: true,
                    drillDownHandler: function (attrFilter) {
                        // handle drill-down somehow
                        alert('Drill-down for: ' + JSON.stringify(attrFilter));
                    }
                });

                var stdRendererNames = ["Table", "Table Barchart", "Heatmap", "Row Heatmap", "Col Heatmap", ];
                var wrappedRenderers = $.extend({}, $.pivotUtilities.renderers);
                $.each(stdRendererNames, function () {
                    var rName = this;
                    wrappedRenderers[rName] =
                        nrecoPivotExt.wrapPivotExportRenderer(
                            nrecoPivotExt.wrapTableRenderer(wrappedRenderers[rName]));
                });

                $('#samplePivotTable3').pivotUI(sampleData, {
                    renderers: wrappedRenderers,
                    rendererOptions: { sort: { direction: "desc", column_key: [2014] } },
                    // vals: ["Total"],
                    // rows: ["Country"],
                    //  cols: ["Year"],
                    aggregatorName: "Sum",
                    onRefresh: function (pivotUIOptions) {
                        // this is correct way to apply fixed headers with pivotUI
                        nrecoPivotExt.initFixedHeaders($('#samplePivotTable3 table.pvtTable'));
                    }
                });

            }
    });

});