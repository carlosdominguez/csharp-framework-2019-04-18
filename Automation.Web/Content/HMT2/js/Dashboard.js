﻿$(document).ready(function () {
    _hideMenu();

    loadDropDowns("SELECT '0' [ID], '  Select Center  ' [Text] UNION SELECT [Center] [ID], [Center] [Text] FROM [HMT2].[dbo].[VWR_AllRoleData2] WHERE [Center] IS NOT NULL GROUP BY [Center]", "0", "drop_Centers");
    loadDropDowns("SELECT '0' [ID], '  Select Product L8 ' [Text] UNION SELECT [Segmenent LV 08] [ID], [Segmenent LV 08] [Text] FROM [HMT2].[dbo].[VWR_AllRoleData2] WHERE [Segmenent LV 08] IS NOT NULL GROUP BY [Segmenent LV 08]", "0", "drop_Products");
    loadDropDowns("SELECT '0' [ID], '  Select Product L9 ' [Text] UNION SELECT [ManagedSegmentName_L09] [ID], [ManagedSegmentName_L09] [Text] FROM [HMT2].[dbo].[VWR_AllRoleData2] WHERE [Segmenent LV 08] = '' GROUP BY [ManagedSegmentName_L09]", "0", "drop_Products9");
    startUpload('0', '0','0');
});

function paintDashboard() {
    
    _callProcedure({
        loadingMsgType: "Loading Upload History",
        loadingMsg: "Loading...",
        name: "[dbo].[SPHMT2_LT_DASHBOARD_RESULTS]",
        params: [],
        success: {
            fn: function (responseList) {
                $("#Dashbody").empty();
                for (var i = 0; i < responseList.length; i++) {
                    
                    switch(i){
                        case 5:
                            specialRow(responseList[i])
                            break
                        default:
                            normalRow(responseList[i]);
                            break;
                    }
                }
            }
        },
    });
}

function normalRow(obj) {
    $('#tblDasboard').find('tbody').append('<tr id="' + obj.rowID + '"><th scope="row"><span class="btn btn-sm btn-outline btn-info box-num">' + obj.rowID + '</span></th></tr>');
    for (var x = 1; x <= 12; x++) {
        $('#' + obj.rowID).append('<td><center>' + obj[x] + '</center></td>');
    }
}

function specialRow(obj) {
    $('#tblDasboard').find('tbody').append('<tr id="' + obj.rowID + '" class="bg-secondary" ><th scope="row"><center><span class="btn btn-sm btn-outline btn-secondary box-num">' + obj.rowID + '</span></center></th></tr>'); //class="bg-warning"
    for (var x = 1; x <= 12; x++) {
        $('#' + obj.rowID).append('<td><center><a class="btn btn-sm btn-outline btn-secondary box-num" >' + obj[x] + '</a></center></td>');
    }
}

function loadDropDowns(selectQuery, selectedId, dropList) {
    //Set default value  
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Log ID Details...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify(selectQuery) },
        type: "post",
        success: function (resultList) {
            $("#" + dropList + "").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $("#" + dropList + "").append($('<option>', { value: objFunction.ID, text: objFunction.Text, selected: (objFunction.ID == selectedId) }));
            }
        }
    });
};

$("#drop_Products").change(function () {
    loadDropDowns("SELECT '0' [ID], '  Select Product L9 ' [Text] UNION SELECT [ManagedSegmentName_L09] [ID], [ManagedSegmentName_L09] [Text] FROM [HMT2].[dbo].[VWR_AllRoleData2] WHERE [Segmenent LV 08] = '" + $("#drop_Products :selected").val() + "' GROUP BY [ManagedSegmentName_L09]", "0", "drop_Products9");
    startUpload($("#drop_Products :selected").val(), $("#drop_Centers :selected").val(), $("#drop_Products9 :selected").val());
});

$("#drop_Centers").change(function () {
    //calculateDashboard($("#drop_Products :selected").val(), $("#drop_Centers :selected").val());
    startUpload($("#drop_Products :selected").val(), $("#drop_Centers :selected").val(), $("#drop_Products9 :selected").val());
});

$("#drop_Products9").change(function () {
    startUpload($("#drop_Products :selected").val(), $("#drop_Centers :selected").val(), $("#drop_Products9 :selected").val());
});

function calculateDashboard(prd, cents, prod9,fnOnSuccess) {
    _callProcedure({
        loadingMsgType: "Loading Dashboard",
        loadingMsg: "Loading...",
        name: "[dbo].[SPHMT2_AC_DASHBOARD_CALCULATION]",
        params: [
                { Name: '@ProductDesc', Value: prd },
                { Name: '@CenterDesc', Value: cents },
                { Name: '@Product9Desc', Value: prod9 }
        ],
        success: {
            fn: function (responseList) {
                paintDashboard();
                if (fnOnSuccess) {
                    fnOnSuccess();
                }
            }
        },
    });
}

function loadDetails(prd,cents,prod9) {

    _callProcedure({
        loadingMsgType: "Loading Dashboard",
        loadingMsg: "Loading...",
        name: "[dbo].[SPHMT2_LT_DASHBOARD_DETAILS]",
        params: [
                { Name: '@CenterID', Value: cents },
                { Name: '@ProductDesc', Value: prd },
                { Name: '@Product9Desc', Value: prod9 }
        ],
        success: {
            fn: function (responseList) {
                for (var i = 0; i < responseList.length; i++){
                    switch(responseList[i].Description){
                        case "DEMAND":
                            $("#DemandDetails").text("Current Demand: " + responseList[i].Count);
                            break;
                        case "ACTUAL":
                            $("#ActualDetails").text("Current Actual: " + responseList[i].Count);
                            break;
                        case "GAP":
                            $("#GAPDetails").text("Current GAP: " + responseList[i].Count);
                            break;
                    }
                }
            }
        },
    });
}

function startUpload(prod, center, prod9) {

    var p8, Cn, p9;

    if (prod == '0')
        p8 = "";
    else
        p8 = prod;

    if (center == '0')
        Cn = "";
    else
        Cn = center;

    if (prod9 == '0')
        p9 = "";
    else
        p9 = prod9;

    calculateDashboard(p8, Cn, p9, function () {
        loadDetails(p8, Cn, p9);
    });
}