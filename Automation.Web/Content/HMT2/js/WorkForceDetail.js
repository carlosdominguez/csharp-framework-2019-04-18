﻿$(document).ready(function () {
    _hideMenu();

    renderComboBox('#cbDate', 'Select a Date', 'getDate', false);


    renderComboBox('#cbCenter', 'Select a Center', 'getCenter', true);
    $('#btnDownloadExcel').hide();

});


function renderComboBox(div, textDefault, procedure, pMultiple) {

    $(div).find('option').remove().end();

    _callServer({
        loadingMsg: "", async: false,
        url: '/Forecast/' + procedure,
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            $.each(response, function (index, element) {
                //$("#cbMyCalendar").append(new Option(element.Value, element.ID));
                $(div).append($('<option>', {
                    value: element.Value,
                    text: element.Value
                }));

            })

            $(div).multiselect({
                click: function (event, ui) {
                    // renderUploadHistory(ui.value);
                }
            });


            $(div).multiselect({
                multiple: pMultiple,
                header: "Select an option",
                noneSelectedText: textDefault,
                selectedList: 1

            }).multiselectfilter();

            $(div).css('width', '100px');
        }
    });
}

function renderGrid() {
    GetListTotalRole();
}



function GetListTotalRole() {

    var DATE = $('#cbDate').multiselect("getChecked");


    var DateParameter;
    $.each(DATE, function (index, value) {
        DateParameter = value.defaultValue;
    });



    var listOfParams = {
        Date: DateParameter
    }




    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/ReengineeringDetail/getTotalReeng',
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [

                           { name: 'Center', type: 'string' },
                           { name: 'JAN', type: 'number' },
                           { name: 'FEB', type: 'number' },
                           { name: 'MAR', type: 'number' },
                           { name: 'APR', type: 'number' },
                           { name: 'MAY', type: 'number' },
                           { name: 'JUN', type: 'number' },
                           { name: 'JUL', type: 'number' },
                           { name: 'AUG', type: 'number' },
                           { name: 'SEP', type: 'number' },
                           { name: 'OCT', type: 'number' },
                           { name: 'NOV', type: 'number' },
                           { name: 'DEC', type: 'number' }
                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            var imgViewSource = function (row, datafield, value) {

                var rowData = $('#jqxGridReenDetail').jqxGrid('getrowdata', row);

                var ID = rowData.IDType;
                var Number = rowData.Number;

                return '<center>' + Number + '   <i class="icon-search" style=" font-size: 23px; color: #4286f4;" onclick="openPopUpLiveStage(' + ID + ')" ></center>';
            }

            var imgDSTemp = function (row, datafield, value) {

                var rowData = $('#jqxGridReenDetail').jqxGrid('getrowdata', row);

                var directStaft = rowData.IsDS;

                if (IsDirectStafRole == "True") {
                    return '<div style="margin-left: 10%;">Direct Staft</div>';
                } else {
                    return '<div  style="margin-left: 10%;">Temp</div>';
                }
            }


            var initrowdetails = function (index, parentElement, gridElement, record) {
                var id = record.uid.toString();

                var gridRole = $($(parentElement).children()[0].children[1]);
                var gridMOU = $($(parentElement).children()[1].children[1]);
                var gridReq = $($(parentElement).children()[2].children[1]);
                var gridDriver = $($(parentElement).children()[3].children[1]);
                var gridEmployee = $($(parentElement).children()[4].children[1]);

                var nestedData;

                nestedData = $('#jqxGridReenDetail').jqxGrid('getrowdata', index);

                var orderssource = {
                    localdata: nestedData,
                    datafields:
                       [
                           { name: 'RoleID', type: 'string' },
                           { name: 'NumberRole', type: 'string' },
                           { name: 'NameRole', type: 'string' },
                           { name: 'GOCRole', type: 'string' },
                           { name: 'MSRole', type: 'string' },
                           { name: 'MGRole', type: 'string' },
                           { name: 'RegionRole', type: 'string' },
                           { name: 'CountryRole', type: 'string' },
                           { name: 'LVIDRole', type: 'string' },
                           { name: 'CreatedByRole', type: 'string' },
                           { name: 'CreatedDateRole', type: 'string' },
                           { name: 'LastUpdateRole', type: 'string' },
                           { name: 'LastUpdateDateRole', type: 'string' },
                           { name: 'IsFteableRole', type: 'string' },
                           { name: 'IsDirectStafRole', type: 'string' },
                           { name: 'RoleIDRoleTypeID', type: 'string' },
                           { name: 'AutoNumericSource', type: 'string' },
                           { name: 'SourceID', type: 'string' },
                           { name: 'IsActiveSource', type: 'string' },
                           { name: 'StartMonth', type: 'string' },
                           { name: 'StartYear', type: 'string' },
                           { name: 'EndMonth', type: 'string' },
                           { name: 'EndYear', type: 'string' },
                           { name: 'DriverID', type: 'string' },
                           { name: 'SourceStatus', type: 'string' },
                           { name: 'SourceName', type: 'string' },
                           { name: 'DriverName', type: 'string' },
                           { name: 'DriverType', type: 'string' },
                           { name: 'RLSDriver', type: 'string' },
                           { name: 'SourceDriver', type: 'string' },
                           { name: 'IsAdd', type: 'string' },
                           { name: 'IsLess', type: 'string' },
                           { name: 'ImpactFRODemand', type: 'string' },
                           { name: 'ImpactType', type: 'string' },
                           { name: 'StatusRole', type: 'string' },
                           { name: 'mgdGeographyDescription', type: 'string' },
                           { name: 'ManagedSegmentDescription', type: 'string' },
                           { name: 'LocationID', type: 'string' },
                           { name: 'LocationName', type: 'string' },
                           { name: 'CenterID', type: 'string' },
                           { name: 'ProfileID', type: 'string' },
                           { name: 'Profile', type: 'string' },
                           { name: 'GOCDescription', type: 'string' },
                           { name: 'EmployeeID', type: 'string' },
                           { name: 'StartDate', type: 'string' },
                           { name: 'EndDate', type: 'string' },
                           { name: 'IsActiveEmployeeRole', type: 'string' },
                           { name: 'ReqID', type: 'string' },
                           { name: 'IsInternalMovement', type: 'string' },
                           { name: 'SOEID', type: 'string' },
                           { name: 'FirstName', type: 'string' },
                           { name: 'LastName', type: 'string' },
                           { name: 'HiringDate', type: 'string' },
                           { name: 'AttritionDate', type: 'string' },
                           { name: 'IsActive', type: 'string' },
                           { name: 'ClassificationID', type: 'string' },
                           { name: 'ClassificationLetter', type: 'string' },
                           { name: 'ClassificationDescription', type: 'string' },
                           { name: 'RequisitionNumber', type: 'string' },
                           { name: 'RequisitionName', type: 'string' },
                           { name: 'RequisitionCreationDate', type: 'string' },
                           { name: 'RequisitionStatusID', type: 'string' },
                           { name: 'Center', type: 'string' },
                           { name: 'PosibleStart', type: 'string' },
                           { name: 'PosibleEnd', type: 'string' }
                       ],
                    datatype: "json"
                };

                var nestedGridAdapter = new $.jqx.dataAdapter(orderssource);
                nestedGridAdapter.dataBind();

                if (gridRole != null) {

                    gridRole.jqxGrid({
                        source: nestedGridAdapter,
                        theme: 'energyblue',
                        width: '100%',
                        autoheight: true,
                        autorowheight: true,
                        pageable: false,
                        filterable: false,
                        showfilterrow: false,
                        columnsresize: true,
                        enablebrowserselection: true,
                        columns: [
                            { text: 'Number Role', dataField: 'NumberRole', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Name Role', dataField: 'NameRole', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Center', dataField: 'Center', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Location Name', dataField: 'LocationName', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Profile', dataField: 'Profile', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Managed Segment', dataField: 'ManagedSegmentDescription', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Managed Geography', dataField: 'mgdGeographyDescription', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Region ', dataField: 'RegionRole', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Country', dataField: 'CountryRole', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'LVID', dataField: 'LVIDRole', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Created By', dataField: 'CreatedByRole', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Created Date', dataField: 'CreatedDateRole', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Last Update', dataField: 'LastUpdateRole', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Last Update Date', dataField: 'LastUpdateDateRole', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Is Fteable', dataField: 'IsFteableRole', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Is Direct Staft', dataField: 'IsDirectStafRole', filtertype: 'checkedlist', width: '9%', editable: false }
                        ]

                    });
                    gridMOU.jqxGrid({
                        source: nestedGridAdapter,
                        theme: 'energyblue',
                        width: '100%',
                        autoheight: true,
                        autorowheight: true,
                        pageable: false,
                        filterable: false,
                        showfilterrow: false,
                        columnsresize: true,
                        enablebrowserselection: true,
                        columns: [
                            { text: 'Auto Numeric', dataField: 'AutoNumericSource', width: '9%', editable: false },
                            { text: 'Posible Start', dataField: 'PosibleStart', width: '9%', editable: false },
                            { text: 'Posible End', dataField: 'PosibleEnd', width: '9%', editable: false }
                        ]

                    });
                    gridDriver.jqxGrid({
                        source: nestedGridAdapter,
                        theme: 'energyblue',
                        width: '100%',
                        autoheight: true,
                        autorowheight: true,
                        pageable: false,
                        filterable: false,
                        showfilterrow: false,
                        columnsresize: true,
                        enablebrowserselection: true,
                        columns: [
                            { text: 'Driver Name', dataField: 'DriverName', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Source Driver', dataField: 'SourceDriver', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Driver Type', dataField: 'DriverType', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'RLS Driver', dataField: 'RLSDriver', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Impact FRO Demand', dataField: 'ImpactFRODemand', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Impact Type', dataField: 'ImpactType', filtertype: 'checkedlist', width: '9%', editable: false }

                        ]

                    });
                    gridReq.jqxGrid({
                        source: nestedGridAdapter,
                        theme: 'energyblue',
                        width: '100%',
                        autoheight: true,
                        autorowheight: true,
                        pageable: false,
                        filterable: false,
                        showfilterrow: false,
                        columnsresize: true,
                        enablebrowserselection: true,
                        columns: [
                            { text: 'Requisition Number', dataField: 'RequisitionNumber', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Requisition Name ', dataField: 'RequisitionName', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Creation Date ', dataField: 'RequisitionCreationDate', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Requisition Status', dataField: 'RequisitionStatusID', filtertype: 'checkedlist', width: '9%', editable: false }
                        ]

                    });
                    gridEmployee.jqxGrid({
                        source: nestedGridAdapter,
                        theme: 'energyblue',
                        width: '100%',
                        autoheight: true,
                        autorowheight: true,
                        pageable: false,
                        filterable: false,
                        showfilterrow: false,
                        columnsresize: true,
                        enablebrowserselection: true,
                        columns: [
                            { text: 'SOEID', dataField: 'SOEID', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'First Name', dataField: 'FirstName', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Last Name', dataField: 'LastName', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Hiring Date', dataField: 'HiringDate', filtertype: 'checkedlist', width: '30%', editable: false },
                            { text: 'Classification', dataField: 'ClassificationLetter', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Description', dataField: 'ClassificationDescription', filtertype: 'checkedlist', width: '9%', editable: false }
                        ]

                    });


                }
            };

            var addfilter = function () {
                var filtergroup = new $.jqx.filter();
                var filter_or_operator = 1;
                var filtervalue = $('#HFRoleID').val();
                var filtercondition = 'contains';
                var filter1 = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);

                filtergroup.addfilter(filter_or_operator, filter1);

                // add the filters.
                $("#jqxGridReenDetail").jqxGrid('addfilter', 'NumberRole', filtergroup);
                // apply the filters.
                $("#jqxGridReenDetail").jqxGrid('applyfilters');
            }

            var toThemeProperty = function (className) {
                return className + " " + className + "-"; //+ theme;
            }

            var groupsrenderer = function (text, group, expanded, data) {

                var rows = new Array();

                rows = data.subItems;

                var sumJan;
                var sumFeb;
                var sumMar;
                var sumApr;
                var sumMay;
                var sumJun;
                var sumJul;
                var sumAug;
                var sumSep;
                var sumOct;
                var sumNov;
                var sumDec;

                sumJan = 0;
                sumFeb = 0;
                sumMar = 0;
                sumApr = 0;
                sumMay = 0;
                sumJun = 0;
                sumJul = 0;
                sumAug = 0;
                sumSep = 0;
                sumOct = 0;
                sumNov = 0;
                sumDec = 0;
                for (var i = 0; i < rows.length; i++) {
                    sumJan = sumJan + rows[i].JAN;
                    sumFeb = sumFeb + rows[i].FEB;
                    sumMar = sumMar + rows[i].MAR;
                    sumApr = sumApr + rows[i].APR;
                    sumMay = sumMay + rows[i].MAY;
                    sumJun = sumJun + rows[i].JUN;
                    sumJul = sumJul + rows[i].JUL;
                    sumAug = sumAug + rows[i].AUG;
                    sumSep = sumSep + rows[i].SEP;
                    sumOct = sumOct + rows[i].OCT;
                    sumNov = sumNov + rows[i].NOV;
                    sumDec = sumDec + rows[i].DEC;

                }



                return '<div class="' + toThemeProperty('jqx-grid-groups-row') + '" style="position: absolute;"><span>' + text
                    + ' Jan ' + sumJan + ' '
                    + ' Feb ' + sumFeb + ' '
                    + ' Mar ' + sumMar + ' '
                    + ' Apr ' + sumApr + ' '
                    + ' May ' + sumMay + ' '
                    + ' Jun ' + sumJun + ' '
                    + ' Jul ' + sumJul + ' '
                    + ' Aug ' + sumAug + ' '
                    + ' Sep ' + sumSep + ' '
                    + ' Oct ' + sumOct + ' '
                    + ' Nov ' + sumNov + ' '
                    + ' Dec ' + sumDec + ' '
                    + '</span>';

            }


            $('#jqxGridReenDetail').jqxGrid(
           {
               source: dataAdapter,
               theme: 'energyblue',
               width: '100%',
               selectionmode: 'multiplecellsadvanced',
               sortable: true,
               autoheight: true,
               autorowheight: false,
               pageable: false,
               filterable: true,
               editable: true,
               showfilterrow: true,
               columnsresize: true,
               //groupable: true,
               //groupsrenderer: groupsrenderer,
               //rowdetails: true,
               autoloadstate: false,
               autosavestate: false,
               //initrowdetails: initrowdetails,
               //rowdetailstemplate: { rowdetails: '<fieldset><legend>Role Info</legend><div id="grid1"></div></fieldset><fieldset><legend>MOU Info</legend><div id="grid2"></div></fieldset><fieldset><legend>Requisition Info</legend><div id="grid3"></div></fieldset><fieldset><legend>Driver Info</legend><div id="grid4"></div></fieldset><fieldset><legend>Employee Info</legend><div id="grid5"></div></fieldset></br></br>', rowdetailsheight: 650, rowdetailshidden: true },
               columns: [

                            , { text: 'Center', dataField: 'Center', width: '8%', editable: false }
                            , { text: 'JAN', dataField: 'JAN', width: '7.6%', editable: false }
                            , { text: 'FEB', dataField: 'FEB', width: '7.6%', editable: false }
                            , { text: 'MAR', dataField: 'MAR', width: '7.6%', editable: false }
                            , { text: 'APR', dataField: 'APR', width: '7.6%', editable: false }
                            , { text: 'MAY', dataField: 'MAY', width: '7.6%', editable: false }
                            , { text: 'JUN', dataField: 'JUN', width: '7.6%', editable: false }
                            , { text: 'JUL', dataField: 'JUL', width: '7.6%', editable: false }
                            , { text: 'AUG', dataField: 'AUG', width: '7.6%', editable: false }
                            , { text: 'SEP', dataField: 'SEP', width: '7.6%', editable: false }
                            , { text: 'OCT', dataField: 'OCT', width: '7.6%', editable: false }
                            , { text: 'NOV', dataField: 'NOV', width: '7.6%', editable: false }
                            , { text: 'DEC', dataField: 'DEC', width: '7.6%', editable: false }
               ]

           });



        }
    });
}

function downloadExcel() {

    //_downloadExcel({
    //    spName: "[spMakerCheckerListIssues]",
    //    spParams: [
    //        { "Name": "@Year", "Value": getSelectedYear() },
    //        { "Name": "@Quarter", "Value": getSelectedQuarter() }
    //    ],
    //    filename: "MakerCheckerIssueList-" + moment().format('MMM-DD-YYYY-HH-MM'),
    //    success: {
    //        msg: "Please wait, generating report..."
    //    }
    //});


    var DATE = $('#cbDate').multiselect("getChecked");


    var DateParameter;
    $.each(DATE, function (index, value) {
        DateParameter = value.defaultValue;
    });



    var listOfParams = {
        Date: DateParameter
    }




    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/ReengineeringDetail/getIdToDownload',
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var sql;

            sql = 'select Center,JAN,FEB,MAR,APR,MAY,JUN,JUL,AUG,SEP,OCT,NOV,DEC from hmt2.dbo.VWReen_DataFirst where DateID = ' + response + ' ';

            _downloadExcel({
                sql: sql,
                filename: "_Re-ENGINEERING_Tracker.xls",
                success: {
                    msg: "Generating report... Please Wait, this operation may take some time to complete."
                }
            });
        }
    });


}