﻿$(document).ready(function () {
    _hideMenu();

    getTotalDriver();
    getTotalCSSCategory();
    getTotalDriverMOU();
    getTotalDriverNonMOU();

    fntLoadDriver();
    $("#jqxExpanderAddDriver").jqxExpander({ width: '100%', height: 'auto', expanded: true, showArrow: true, theme: 'blackberry' });
    $("#jqxExpanderAddDriver").jqxExpander({ expanded: false });

    $("#jqxExpanderDriverMOU").jqxExpander({ width: '100%', height: 'auto', expanded: true, showArrow: true, theme: 'blackberry' });
    $("#jqxExpanderDriverMOU").jqxExpander({ expanded: false });

    $("#jqxExpanderDriverNONMOU").jqxExpander({ width: '100%', height: 'auto', expanded: true, showArrow: true, theme: 'blackberry' });
    $("#jqxExpanderDriverNONMOU").jqxExpander({ expanded: false });

    $("#jqxSaveNewDriver").jqxNotification({
        width: '100%', position: "top-left", opacity: 0.9, appendContainer: "#containerSave",
        autoOpen: false, animationOpenDelay: 100, autoClose: true, autoCloseDelay: 10000, template: "warning"
    });
});


//box total roles
function getTotalDriver() {

    _callServer({
        loadingMsg: "", 
        url: '/Driver/getTotalDriver',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            

            $('#lblDriver').text(response);

           
        }
    });

}
//box pending review
function getTotalCSSCategory() {
    _callServer({
        loadingMsg: "",
        url: '/Driver/getTotalCSSCategory',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            $('#lblCSSCategory').text(response);
        }
    });

}
//box total in mou
function getTotalDriverMOU() {
    _callServer({
        loadingMsg: "", 
        url: '/Driver/getTotalDriverMOU',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            $('#lblDriverMOU').text(response);
        }
    });

}
//boc total in non mou
function getTotalDriverNonMOU() {
    _callServer({
        loadingMsg: "", 
        url: '/Driver/getTotalDriverNonMOU',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            $('#lblDriverNonMOU').text(response);
        }
    });

}


function renderGridDriver() {


    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/Driver/getListDriver',
        type: "post",
        success: function (json) {

            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                        { name: 'ID', type: 'string' },
                      , { name: 'CssDriver', type: 'string' },
                      , { name: 'Name', type: 'string' },
                      , { name: 'RLSDriver', type: 'string' },
                      , { name: 'Source', type: 'string' },
                      , { name: 'IsAdd', type: 'string' },
                      , { name: 'IsLess', type: 'string' },
                      , { name: 'ImpactFRODemand', type: 'string' },
                      , { name: 'ImpactType', type: 'string' },
                      , { name: 'CreatedDate', type: 'string' },
                      , { name: 'CreatedBy', type: 'string' },
                      , { name: 'ModifyBy', type: 'string' },
                      , { name: 'ModifyDate', type: 'string' },
                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            var SUMREST = function (row, datafield, value) {

                var rowData = $('#jqxGridDriver').jqxGrid('getrowdata', row);

                var SUM = rowData.IsAdd;
                var REST = rowData.IsLess;

                if (SUM == true && REST == true) {
                    return '<center><h4>+/(-)</h4></center>';
                } 
                if (SUM == true && REST == false) {
                    return '<center><h4>+</h4></center>';
                }
                if (SUM == false && REST == false) {
                    return '<center><h4></h4></center>';
                }
                if (SUM == false && REST == true) {
                    return '<center><h4>(-)</h4></center>';
                }

                
            }

            var ACTIONS = function (row, datafield, value) {

                var rowData = $('#jqxGridDriver').jqxGrid('getrowdata', row);
                
                return '<a class="btn" href="#" onclick="deleteDriver(' + rowData.ID + ')" style="display:inline-block; background-color:#d9534f; width:100px;"><i class="fa fa-trash-o" style="padding-top: 0px; font-size: 15px; color: white;"></i> Delete</a>';
            }


            $('#jqxGridDriver').jqxGrid(
           {
               source: dataAdapter,
               theme: 'blackberry',
               width: '100%',
              autoheight: true,
               autorowheight: true,
               //height: 600,
               pageable: false,
               filterable: true,
               showfilterrow: true,
               columnsresize: true,
               selectionmode: 'singlecell',
               enablebrowserselection: true,
               columns: [
                    
                    { text: 'CssDriver', dataField: 'CssDriver', filtertype: 'checkedlist', editable: false, width: '10%' },
                    { text: 'Name', dataField: 'Name', filtertype: 'input', editable: false, width: '13%' },
                    { text: 'RLSDriver', dataField: 'RLSDriver', filtertype: 'checkedlist', editable: false, width: '10%' },
                    { text: 'Source', dataField: 'Source', filtertype: 'checkedlist', editable: false, width: '10%' },
                    { text: 'Impact to HC Walk', dataField: 'IsAdd', columntype: 'checkedlist', width: '10%', editable: false, cellsrenderer: SUMREST },
                    { text: 'ImpactFRODemand', dataField: 'ImpactFRODemand', filtertype: 'checkedlist', editable: false, width: '10%' },
                    { text: 'ImpactType', dataField: 'ImpactType', filtertype: 'checkedlist', editable: false, width: '10%' },
                    { text: 'ModifyBy', dataField: 'ModifyBy', filtertype: 'input', editable: false, width: '10%' },
                    { text: 'ModifyDate', dataField: 'ModifyDate', filtertype: 'input', editable: false, width: '10%' },
                    { text: 'Actions', columntype: 'checkedlist', width: '6%', editable: false, cellsrenderer: ACTIONS }
 
               ]

           });


        }
    });


}


function viewSelectedGrid(DIV) {
    $('#Driver').hide();
    $('#CSSCategory').hide();
    $('#DriverMOU').hide();
    $('#DriverNonMOU').hide();

    $(DIV).show();
}

function fntLoadDriver() {
    viewSelectedGrid('#Driver');
    renderGridDriver();

    renderCBAddDriver('/Driver/getCSSDriver', '#cbCSSDriver');
    renderCBAddDriver('/Driver/getRLSDriver', '#cbRLSDriver');
    renderCBAddDriver('/Driver/getSource', '#cbSource');
    renderCBAddDriver('/Driver/getImpactToHCWalk', '#cbImpactToHC');
    renderCBAddDriver('/Driver/getImpacFRODemand', '#cbImpactFro');
    renderCBAddDriver('/Driver/getImpacType', '#cbImpactType');

}

function fntLoadCSSCategory() {
    viewSelectedGrid('#CSSCategory');
   
}
function fntLoadDriverMOU() {
    viewSelectedGrid('#DriverMOU');
    renderGridMOU();
    renderCBAddDriver('/Driver/getDriverCB', '#cbDriverMOU');
    renderCBAddDriver('/Driver/getTypeMOU', '#cbMOUType');
}
function fntLoadDriverNonMOU() {
    viewSelectedGrid('#DriverNonMOU');
    renderGridNONMOU();
    renderCBAddDriver('/Driver/getDriverCBNONMOU', '#cbDriverNONMOU');
    renderCBAddDriver('/Driver/getTypeNONMOU', '#cbNONMOUType');
}


function renderCBAddDriver(URL,NameID) {

    _callServer({
        loadingMsg: "",
        url: URL,
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = (response);

            var source =
                {
                    localdata: data2,
                    datatype: "json",
                    datafields: [
                        { name: 'ID' },
                        { name: 'Name' }
                    ],
                    async: false
                };

            var dataAdapter = new $.jqx.dataAdapter(source);

            $(NameID).jqxComboBox({ selectedIndex: 0, source: dataAdapter, displayMember: "Name", valueMember: "Name", width: '99%', height: 25 });

            //if (NameType != '') {
            //    $(NameID).jqxComboBox('selectItem', NameType);
            //}

            $(NameID).on('select', function (event) {
                if (event.args) {
                    var item = event.args.item;
                    if (item) {

                    }
                }
            });
        }
    });


}

function addNewDriver()
{
    var listOfParams = {
        CSSDRIVER: $("#cbCSSDriver").val(),
        NAME: $("#txtName").val(),
        RLSDRIVER: $("#cbRLSDriver").val(),
        SOURCE: $("#cbSource").val(),
        IMPACTHCWALK: $("#cbImpactToHC").val(),
        IMPACTFRO: $("#cbImpactFro").val(),
        IMPACTTYPE: $("#cbImpactType").val()

    }
    if ($("#txtName").val().length > 3) {

        _callServer({
            loadingMsg: "", async: false,
            url: '/Driver/insertNewDriver',
            data: {
                'data': JSON.stringify(listOfParams)
            },
            type: "post",
            success: function (json) {
                $("#txtName").val() = "";
                fntLoadDriver();
                getTotalDriver();
            }
        });
    } else
    {
        $("#jqxSaveNewDriver").jqxNotification("open");
    }



}

function deleteDriver(ID)
{
    var listOfParams = {
        pID: ID


    }
   
        _callServer({
            loadingMsg: "", async: false,
            url: '/Driver/deleteDriver',
            data: {
                'data': JSON.stringify(listOfParams)
            },
            type: "post",
            success: function (json) {
                fntLoadDriver();
            }
        });


}

function closeNotifications() {

    $("#jqxSaveDiscovery").jqxNotification("closeLast");

}

function renderGridMOU() {


    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/Driver/getListDriverMOU',
        type: "post",
        success: function (json) {

            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                        { name: 'ID', type: 'string' },

                        { name: 'Name', type: 'string' },
                          ,
                        { name: 'CssDriver', type: 'string' },
                          ,
                        { name: 'Driver', type: 'string' },
                          ,
                        { name: 'RLSDriver', type: 'string' },
                          ,
                        { name: 'Source', type: 'string' },
                          ,
                        { name: 'IsAdd', type: 'string' },
                          ,
                        { name: 'IsLess', type: 'string' },
                          ,
                        { name: 'ImpactFRODemand', type: 'string' },
                          ,
                        { name: 'ImpactType', type: 'string' }
                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            var SUMREST = function (row, datafield, value) {

                var rowData = $('#jqxGridDriverMOU').jqxGrid('getrowdata', row);

                var SUM = rowData.IsAdd;
                var REST = rowData.IsLess;

                if (SUM == true && REST == true) {
                    return '<center><h4>+/(-)</h4></center>';
                }
                if (SUM == true && REST == false) {
                    return '<center><h4>+</h4></center>';
                }
                if (SUM == false && REST == false) {
                    return '<center><h4></h4></center>';
                }
                if (SUM == false && REST == true) {
                    return '<center><h4>(-)</h4></center>';
                }


            }

            var ACTIONS = function (row, datafield, value) {

                var rowData = $('#jqxGridDriverMOU').jqxGrid('getrowdata', row);

                return '<a class="btn" href="#" onclick="deleteDriverMOU(' + rowData.ID + ')" style="display:inline-block; background-color:#d9534f; width:100px;"><i class="fa fa-trash-o" style="padding-top: 0px; font-size: 15px; color: white;"></i> Delete</a>';
            }


            $('#jqxGridDriverMOU').jqxGrid(
           {
               source: dataAdapter,
               theme: 'blackberry',
               width: '100%',
               autoheight: true,
               autorowheight: true,
               //height: 600,
               pageable: false,
               filterable: true,
               showfilterrow: true,
               columnsresize: true,
               selectionmode: 'singlecell',
               enablebrowserselection: true,
               columns: [
                   { text: 'MOU Type', dataField: 'Name', filtertype: 'input', editable: false, width: '13%' },
                    { text: 'CssDriver', dataField: 'CssDriver', filtertype: 'checkedlist', editable: false, width: '10%' },
                    { text: 'Driver', dataField: 'Driver', filtertype: 'input', editable: false, width: '13%' },
                    { text: 'RLSDriver', dataField: 'RLSDriver', filtertype: 'checkedlist', editable: false, width: '10%' },
                    { text: 'Source', dataField: 'Source', filtertype: 'checkedlist', editable: false, width: '10%' },
                    { text: 'Impact to HC Walk', dataField: 'IsAdd', columntype: 'checkedlist', width: '10%', editable: false, cellsrenderer: SUMREST },
                    { text: 'ImpactFRODemand', dataField: 'ImpactFRODemand', filtertype: 'checkedlist', editable: false, width: '10%' },
                    { text: 'ImpactType', dataField: 'ImpactType', filtertype: 'checkedlist', editable: false, width: '10%' },
                    { text: 'Actions', columntype: 'checkedlist', width: '6%', editable: false, cellsrenderer: ACTIONS }

               ]

           });


        }
    });


}

function renderGridNONMOU() {


    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/Driver/getListDriverNONMOU',
        type: "post",
        success: function (json) {

            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                        { name: 'ID', type: 'string' },

                        { name: 'Name', type: 'string' },
                          ,
                        { name: 'CssDriver', type: 'string' },
                          ,
                        { name: 'Driver', type: 'string' },
                          ,
                        { name: 'RLSDriver', type: 'string' },
                          ,
                        { name: 'Source', type: 'string' },
                          ,
                        { name: 'IsAdd', type: 'string' },
                          ,
                        { name: 'IsLess', type: 'string' },
                          ,
                        { name: 'ImpactFRODemand', type: 'string' },
                          ,
                        { name: 'ImpactType', type: 'string' }
                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            var SUMREST = function (row, datafield, value) {

                var rowData = $('#jqxGridDriverNonMOU').jqxGrid('getrowdata', row);

                var SUM = rowData.IsAdd;
                var REST = rowData.IsLess;

                if (SUM == true && REST == true) {
                    return '<center><h4>+/(-)</h4></center>';
                }
                if (SUM == true && REST == false) {
                    return '<center><h4>+</h4></center>';
                }
                if (SUM == false && REST == false) {
                    return '<center><h4></h4></center>';
                }
                if (SUM == false && REST == true) {
                    return '<center><h4>(-)</h4></center>';
                }


            }

            var ACTIONS = function (row, datafield, value) {

                var rowData = $('#jqxGridDriverNonMOU').jqxGrid('getrowdata', row);

                return '<a class="btn" href="#" onclick="deleteDriverNONMOU(' + rowData.ID + ')" style="display:inline-block; background-color:#d9534f; width:100px;"><i class="fa fa-trash-o" style="padding-top: 0px; font-size: 15px; color: white;"></i> Delete</a>';
            }


            $('#jqxGridDriverNonMOU').jqxGrid(
           {
               source: dataAdapter,
               theme: 'blackberry',
               width: '100%',
               autoheight: true,
               autorowheight: true,
               //height: 600,
               pageable: false,
               filterable: true,
               showfilterrow: true,
               columnsresize: true,
               selectionmode: 'singlecell',
               enablebrowserselection: true,
               columns: [

                    { text: 'NON MOU Type', dataField: 'Name', filtertype: 'input', editable: false, width: '13%' },
                    { text: 'CssDriver', dataField: 'CssDriver', filtertype: 'checkedlist', editable: false, width: '10%' },
                    { text: 'Driver', dataField: 'Driver', filtertype: 'input', editable: false, width: '13%' },
                    { text: 'RLSDriver', dataField: 'RLSDriver', filtertype: 'checkedlist', editable: false, width: '10%' },
                    { text: 'Source', dataField: 'Source', filtertype: 'checkedlist', editable: false, width: '10%' },
                    { text: 'Impact to HC Walk', dataField: 'IsAdd', columntype: 'checkedlist', width: '10%', editable: false, cellsrenderer: SUMREST },
                    { text: 'ImpactFRODemand', dataField: 'ImpactFRODemand', filtertype: 'checkedlist', editable: false, width: '10%' },
                    { text: 'ImpactType', dataField: 'ImpactType', filtertype: 'checkedlist', editable: false, width: '10%' },
                    { text: 'Actions', columntype: 'checkedlist', width: '6%', editable: false, cellsrenderer: ACTIONS }

               ]

           });


        }
    });


}

function deleteDriverMOU(ID) {
    var listOfParams = {
        pID: ID
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/Driver/deleteDriverMOU',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            fntLoadDriverMOU();
            getTotalDriverMOU();
        }
    });


}

function deleteDriverNONMOU(ID) {
    var listOfParams = {
        pID: ID
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/Driver/deleteDriverNONMOU',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            fntLoadDriverNonMOU();
            getTotalDriverNonMOU();
        }
    });


}


function addNewDriverMOU() {
    var listOfParams = {
        DRIVER: $("#cbDriverMOU").val(),
        TYPE: $("#cbMOUType").val()

    }
   

        _callServer({
            loadingMsg: "", async: false,
            url: '/Driver/insertNewDriverMOU',
            data: {
                'data': JSON.stringify(listOfParams)
            },
            type: "post",
            success: function (json) {
                fntLoadDriverMOU();
                getTotalDriverMOU();
            }
        });
   


}

function addNewDriverNONMOU() {
    var listOfParams = {
        DRIVER: $("#cbDriverNONMOU").val(),
        TYPE: $("#cbNONMOUType").val()
    }
    
        _callServer({
            loadingMsg: "", async: false,
            url: '/Driver/insertNewDriverNONMOU',
            data: {
                'data': JSON.stringify(listOfParams)
            },
            type: "post",
            success: function (json) {
                fntLoadDriverNonMOU();
                getTotalDriverNonMOU();
            }
        });
    



}