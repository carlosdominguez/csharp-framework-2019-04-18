﻿$(document).ready(function () {
    loadUploadPanel();
    $("#jqxdropdownbutton_Productivity_HistoryID").jqxDropDownButton({ template: "summer", width: "300", height: 30 });
    loadHistoryTable();
    _hideMenu();
});

function loadUploadPanel() {
    XLSX.createJsXlsx({
        idElement: "js-xlsx-plugin",
        fileInput: {
            dropText: "Drop an Excel file here...",
            hideDrop: false,
            inputText: "... or click here to select a file"
        },
        onSuccess: function (jsonData) {

            var excelSource = jsonData;

            _callServer({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading data...",
                url: '/Productivity/uploadProductivityFile',
                data: {
                    'data': JSON.stringify(excelSource["Report"])
                },
                type: "post",
                success: function (json) {
                    $('#js-xlsx-plugin').toggle();
                    loadHistoryTable();
                }
            });
        }
    });
}

function loadHistoryTable() {
    _callProcedure({
        loadingMsgType: "Loading Upload History",
        loadingMsg: "Loading...",
        name: "[dbo].[SPHMT2_LT_PRODUCTIVITY_VERSION]",
        params: [],
        success: {
            fn: function (responseList) {
                $.jqxGridApi.create({
                    showTo: "#tbldropProdHistID",
                    options: {
                        //for comments or descriptions
                        height: "300",
                        autoheight: true,
                        autorowheight: true,
                        selectionmode: "singlerow",
                        showfilterrow: false,
                        sortable: true,
                        groupable: false,
                        showfilterrow: true
                    },
                    source: {
                        // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                        dataBinding: "Large Data Set Local",
                        rows: responseList
                    },
                    group: [],
                    columns: [
                        { name: 'ID', type: 'string', text: 'Upload ID', width: '15%' },
                        { name: 'UploadBy', type: 'string', width: '35%' },
                        { name: 'UploadDate', type: 'string', width: '40%' },
                        { name: 'Enable', type: 'bit', width: '10%' }
                    ],

                    ready: function () {
                        _hideLoadingFullPage();

                        $("#tbldropProdHistID").on('rowselect', function (event) {
                            var args = event.args;
                            var row = $("#tbldropProdHistID").jqxGrid('getrowdata', args.rowindex);
                            var dropDownContent = '<div id="selectedUpID" style="position: relative; width:100%; margin-left: 3px; margin-top: 5px;" idUpID="' + row.ID + '" >' + row['ID'] + ' - ' + row['UploadBy'] + ' - ' + row['UploadDate'] + '</div>';
                            $("#jqxdropdownbutton_Productivity_HistoryID").jqxDropDownButton('setContent', dropDownContent);
                            $("#jqxdropdownbutton_Productivity_HistoryID").jqxDropDownButton('close');

                            loadProductivityTable(row.ID);

                        });
                    }
                });

                $.each((responseList), function (index, element) {
                    if (element.Enable == 'True') {
                        var dropDownContent = '<div id="selectedUpID" style="position: relative; width:100%; margin-left: 3px; margin-top: 5px;" idUpID="' + element.ID + '" >' + element['ID'] + ' - ' + element['UploadBy'] + ' - ' + element['UploadDate'] + '</div>';
                        $("#jqxdropdownbutton_Productivity_HistoryID").jqxDropDownButton('setContent', dropDownContent);

                        loadProductivityTable(element.ID);
                    }
                });

            }
        },
    });    
}

function loadProductivityTable(UploadID){
    $.jqxGridApi.create({
        showTo: "#tblProductivity",
        options: {
            //for comments or descriptions
            theme: 'energyblue',
            width: '100%',
            selectionmode: 'multiplecellsadvanced',
            sortable: true,
            height: 500,
            autoheight: false,
            autorowheight: false,
            pageable: false,
            filterable: true,
            editable: true,
            showfilterrow: true,
            columnsresize: true,
            //selectionmode: 'checkbox',
            enablebrowserselection: true,
            groupable: true,
            rowdetails: true,
            autoloadstate: false,
            autosavestate: false,
            async: false
        },
        sp: {
            Name: "[dbo].[SPHMT2_LT_PRODUCTIVITY]",
            Params: [
                    { Name: "@VersionID", Value: UploadID }
            ]
              
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            
            { name: 'ReengID', text: "ReengID", type: 'string', width: '9%', editable: false, pinned: true },
            { name: 'ManagedSegmentDescriptionL8', text: "Managed Segment Description", type: 'string', width: '20%', editable: false, pinned: false },
            { name: 'FTEGOCDescription', text: "FTE GOC Description", type: 'string', width: '20%', editable: false, pinned: false },
            { name: 'GOCNumber', text: "GOC Number", type: 'string', width: '15%', editable: false, pinned: false },
            { name: 'FunctionalSubProcess', text: "Functional Sub-Process", type: 'string', width: '20%', editable: false, pinned: false },
            { name: 'InitiativeTitle', text: "Initiative Title", type: 'string', width: '30%', editable: false, pinned: false },
            { name: 'InitiativeDescription', text: "Initiative Description", type: 'string', width: '30%', editable: false, pinned: false },
            { name: 'Status', text: "Status", type: 'string', width: '9%', editable: false, pinned: true, cellsrenderer: function (rowIndex) {
                var dataRecord = $("#tblProductivity").jqxGrid('getrowdata', rowIndex);
                    var htmlResult = '';
                    switch (dataRecord.Status) {
                        case "Completed":
                            htmlResult += '<center><span class="badge badge-md badge-info" style="margin-top: 5px;">Completed</span></center>';
                            break;

                        case "Cancelled":
                            htmlResult += '<center><span class="badge badge-md badge-secondary" style="margin-top: 5px;">Cancelled</span></center>';
                            break;

                        case "Not Started":
                            htmlResult += '<center><span class="badge badge-md badge-secondary" style="margin-top: 5px;">Not Started</span></center>';
                            break;

                        case "Green":
                            htmlResult += '<center><span class="badge badge-md badge-success" style="margin-top: 5px;">Green</span></center>';
                            break;

                        case "Red":
                            htmlResult += '<center><span class="badge badge-md badge-danger" style="margin-top: 5px;">Red</span></center>';
                            break;

                        case "Yellow":
                            htmlResult += '<center><span class="badge badge-md badge-warning" style="margin-top: 5px;">Yellow</span></center>';
                            break;
                    }

                    return htmlResult;
                }
            },
            { name: 'ConfidenceLevel', text: "Confidence Level", type: 'string', width: '12%', editable: false, pinned: false },
            { name: 'Region', text: "Region", type: 'string', width: '10%', editable: false, pinned: false },
            { name: 'CenterImpact', text: "Center Impact", type: 'string', width: '10%', editable: false, pinned: false },
            { name: 'SavesAccruetoCenter', text: "Saves Accrue to Center", type: 'string', width: '10%', editable: false, pinned: false },
            { name: 'GPLCategory', text: "GPL Category", type: 'string', width: '15%', editable: false, pinned: false },
            { name: 'OldFRSSCategory', text: "Old FRSS Category", type: 'string', width: '20%', editable: false, pinned: false },
            { name: 'FRSSSubCategory', text: "FRSS Sub Category (Key Driver)", type: 'string', width: '20%', editable: false, pinned: false },
            { name: 'FRSSCategory', text: "FRSS Category", type: 'string', width: '15%', editable: false, pinned: false },
            { name: 'DriverMapping', text: "CSS/EO&T Driver Mapping", type: 'string', width: '20%', editable: false, pinned: false },
            { name: 'ProjectStartDate', text: "Project Start Date", type: 'string', width: '15%', editable: false, pinned: false },
            { name: 'ProjectEndDate', text: "Project End Date", type: 'string', width: '15%', editable: false, pinned: false },
            { name: 'FTESaves', text: "FTE Saves", type: 'float', width: '15%', editable: false, pinned: false },
            { name: 'Saves', text: "$ Saves", type: 'float', width: '15%', editable: false, pinned: false },
            { name: 'ConfidenceLevelWeighting', text: "Confidence Level Weighting", type: 'float', width: '15%', editable: false, pinned: false },
            { name: 'WeightedFTESave', text: "Weighted FTE Save", type: 'float', width: '15%', editable: false, pinned: false },
            { name: 'WeightedSave', text: "Weighted $ Save", type: 'float', width: '15%', editable: false, pinned: false },
            { name: 'CarryOver', text: "$ Carry Over", type: 'float', width: '15%', editable: false, pinned: false },
            { name: 'Dependency', text: "Dependency", type: 'string', width: '15%', editable: false, pinned: false },
            { name: 'DependencyComments', text: "Dependency Comments", type: 'string', width: '15%', editable: false, pinned: false },
            { name: 'TypeofRelease', text: "Type of Release", type: 'string', width: '15%', editable: false, pinned: false },
            { name: 'eReq_MOU', text: "eReq # / MOU #", type: 'string', width: '15%', editable: false, pinned: false },
            { name: 'NameofEmployeeNameofMOU', text: "Name of Employee / Name of MOU", type: 'string', width: '15%', editable: false, pinned: false },
            { name: 'ConfirmationDateofRelease', text: "Confirmation Date of Release", type: 'string', width: '15%', editable: false, pinned: false },
            { name: 'Remarks', text: "Remarks", type: 'string', width: '15%', editable: false, pinned: false },
            { name: 'BaselineProjectEndDate', text: "Baseline Project End Date", type: 'string', width: '15%', editable: false, pinned: false },
            { name: 'BaselineFTESaves', text: "Baseline FTE Saves", type: 'float', width: '15%', editable: false, pinned: false },
            { name: 'BaselineSaves', text: "Baseline $ Saves", type: 'float', width: '15%', editable: false, pinned: false },
            { name: 'INCLUDEEXCLUDE', text: "INCLUDE / EXCLUDE", type: 'string', width: '15%', editable: false, pinned: false },
            { name: 'RPL', text: "RPL", type: 'string', width: '15%', editable: false, pinned: false },
            { name: 'WalterNotes', text: "Walter's Notes", type: 'string', width: '15%', editable: false, pinned: false },
            { name: 'SavesEffectiviteDate', text: "Saves Effectivite Date", type: 'string', width: '15%', editable: false, pinned: false },
            { name: 'CSSID', text: "CSS ID", type: 'string', width: '15%', editable: false, pinned: false },
            { name: 'ProcessforGOC', text: "Process for GOC", type: 'string', width: '15%', editable: false, pinned: false },
            { name: 'ProcessforFunctionalSubProcess', text: "Process for Functional Sub-Process", type: 'string', width: '15%', editable: false, pinned: false },
            { name: 'CenterRate', text: "Center Rate", type: 'float', width: '15%', editable: false, pinned: false },
            { name: 'MonthSavesRealizedDate', text: "Month (Saves Realized Date)", type: 'int', width: '15%', editable: false, pinned: false },
            { name: 'MonthReportedProjectEndDate', text: "Month Reported (Project End Date)", type: 'int', width: '15%', editable: false, pinned: false },
            { name: 'Year', text: "Year", type: 'int', width: '15%', editable: false, pinned: false },
            { name: 'EffDateComparison', text: "Eff Date Comparison", type: 'string', width: '15%', editable: false, pinned: false },
            { name: 'RP', text: "RP (Yes / No)", type: 'string', width: '15%', editable: false, pinned: false },
            { name: 'ChargeoutPlanningUnit', text: "Chargeout Planning Unit", type: 'string', width: '15%', editable: false, pinned: false }
        ],

        ready: function () {

            
        }
    });
}

$("#btnUpload").click(function () {
    if (_getUserInfo().Roles.includes("HMT2_SUPER_ADMIN") || _getUserInfo().Roles.includes("HMT2_ADMIN")) {
        $('#js-xlsx-plugin').toggle();
    } else {
        _showNotification("error", "User not allwed to upload files");
    }
    //$('#fine-uploader-manual-trigger').toggle();
});

$("#btnDownload").click(function () {
    _downloadExcel({
        spName: "[dbo].[SPHMT2_LT_PRODUCTIVITY]",
        spParams: [{ Name: "@VersionID", Value: 0 }],
        filename: "Template_Productivty" + "_" + Date.now(),
        success: {
            msg: "Please wait, generating Excel..."
        }
    });
});

$("#btnDownloadView").click(function () {
    _downloadExcel({
        spName: "[dbo].[SPHMT2_LT_PRODUCTIVITY]",
        spParams: [{ Name: "@VersionID", Value: $("#selectedUpID").attr("idUpID") }],
        filename: "Productivty_SelectedView" + "_" + Date.now(),
        success: {
            msg: "Please wait, generating Excel..."
        }
    });
});