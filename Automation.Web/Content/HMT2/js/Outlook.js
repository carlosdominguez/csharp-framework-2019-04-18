﻿$(document).ready(function () {
    _hideMenu();
    loadDropDowns("SELECT 0 [ID], '  Select Center  ' [Text] UNION SELECT [ID] [ID], [Center] [Text] FROM [HMT2].[dbo].[tblFC_CenterOutlook]", "0", "drop_Center");
    loadOutlookPermanet(0);

    XLSX.createJsXlsx({
        idElement: "js-xlsx-plugin",
        fileInput: {
            dropText: "Drop an Excel file here...",
            hideDrop: false,
            inputText: "... or click here to select a file"
        },
        onSuccess: function (jsonData) {

            var excelSource = jsonData;

            _callServer({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading data...",
                url: '/Outlook/uploadOutlookFile',
                data: {
                    'data': JSON.stringify(excelSource["Report"])
                },
                type: "post",
                success: function (json) {
                    $('#js-xlsx-plugin').toggle();
                    loadOutlookPermanet($("#drop_Center :selected").val());
                }
            });
        }
    });
});

function loadDropDowns(selectQuery, selectedId, dropList) {
    //Set default value  
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Log ID Details...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify(selectQuery) },
        type: "post",
        success: function (resultList) {
            $("#" + dropList + "").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $("#" + dropList + "").append($('<option>', { value: objFunction.ID, text: objFunction.Text, selected: (objFunction.ID == selectedId) }));
            }
        }
    });
};

function loadOutlookPermanet(CenterID) {
    var cellsrenderer = function (row, column, value, defaultHtml) {
        if (row == 0 || row == 1 || row == 4 || row == 8 || row == 9) {
            var element = $(defaultHtml);
            element.css('color', '#999');
            return element[0].outerHTML;
        }

        return defaultHtml;
    }

    var MonthColumns = [
        { name: 'Description', text: "ReengID", type: 'string', width: '16%', cellsrenderer: cellsrenderer},
        { name: 'JAN', text: "JAN", datafield: 1, type: 'int', width: '7%', cellsrenderer: cellsrenderer },
        { name: 'FEB', text: "FEB", datafield: 2, type: 'int', width: '7%', cellsrenderer: cellsrenderer },
        { name: 'MAR', text: "MAR", datafield: 3, type: 'int', width: '7%', cellsrenderer: cellsrenderer },
        { name: 'APR', text: "APR", datafield: 4, type: 'int', width: '7%', cellsrenderer: cellsrenderer },
        { name: 'MAY', text: "MAY", datafield: 5, type: 'int', width: '7%', cellsrenderer: cellsrenderer },
        { name: 'JUN', text: "JUN", datafield: 6, type: 'int', width: '7%', cellsrenderer: cellsrenderer },
        { name: 'JUL', text: "JUL", datafield: 7, type: 'int', width: '7%', cellsrenderer: cellsrenderer },
        { name: 'AUG', text: "AUG", datafield: 8, type: 'int', width: '7%', cellsrenderer: cellsrenderer },
        { name: 'SEP', text: "SEP", datafield: 9, type: 'int', width: '7%', cellsrenderer: cellsrenderer },
        { name: 'OCT', text: "OCT", datafield: 10, type: 'int', width: '7%', cellsrenderer: cellsrenderer },
        { name: 'NOV', text: "NOV", datafield: 11, type: 'int', width: '7%', cellsrenderer: cellsrenderer },
        { name: 'DEC', text: "DEC", datafield: 12, type: 'int', width: '7%', cellsrenderer: cellsrenderer },
    ]

    var d = new Date();

    for (var i = 0 ; i <= d.getMonth() + 1; i++) {
        MonthColumns[i].editable = false;
    }

    for (var i = d.getMonth() + 1 ; i <= 12; i++) {
        MonthColumns[i].cellclassname = 'columnTemp';
        MonthColumns[i].editable = true;
    }

    $.jqxGridApi.create({
        showTo: "#tblGridOutlook",
        options: {
            //for comments or descriptions
            theme: 'energyblue',
            width: '100%',
            selectionmode: 'multiplecellsadvanced',
            height: 500,
            autoheight: true,
            autorowheight: true,
            editable: true,
            showfilterrow: false,
            columnsresize: false,
            //selectionmode: 'checkbox',
            enablebrowserselection: true,
            rowdetails: true,
            autoloadstate: true,
            autosavestate: true,
            async: false
        },
        sp: {
            Name: "[dbo].[SPHMT2_LT_OUTLOOK_PERM]",
            Params: [
                    { Name: "@CenterID", Value: CenterID }
            ]
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: MonthColumns,

        ready: function () {
            $("#tblGridOutlook").bind('cellbeginedit', function (event) {
                var column = args.datafield;
                var row = args.rowindex;
                var value = args.value;
                if (row == 0 || row == 1 || row == 4 || row == 8) {
                    $("#tblGridOutlook").jqxGrid('endcelledit', row, column, true);
                }
            });

            var columnindex;

            $('#tblGridOutlook').on('cellclick', function (event) {
                columnindex = args.columnindex;
            });

            $('#tblGridOutlook').on('cellendedit', function (event) {
                var center = $("#drop_Center :selected").val(),
                    descrip = event.args.row.Description,
                    month = event.args.datafield,
                    newValue = event.args.value,
                    da = new Date();
                if (newValue != ''){
                    if (columnindex >= da.getMonth() + 1) {
                        calculateOutlook(center, descrip, month, newValue, 0, function () {
                            loadOutlookPermanet($("#drop_Center :selected").val())
                        })
                    } else {
                        calculateOutlook(center, descrip, month, newValue, 1, function () {
                            loadOutlookPermanet($("#drop_Center :selected").val())
                        })
                    }
                }
            });
        }
    });
}

function calculateOutlook(center, desc, month, newValue, table, fnOnSuccess) {
    _callProcedure({
        loadingMsgType: "Loading Saving Changes",
        loadingMsg: "Loading...",
        name: "[dbo].[SPHMT2_AC_SAVE_OUTLOOK]",
        params: [
            { Name: "@CenterID", Value: center },
            { Name: "@Description", Value: desc },
            { Name: "@Month", Value: month },
            { Name: "@Value", Value: newValue },
            { Name: "@Table", Value: table }
        ],
        success: {
            fn: function (responseList) {
                if (fnOnSuccess)
                    fnOnSuccess();
            }
        },
    });
}

function closeMonth(){
    //GET CURRENT MONTH
    //GET DATA FROM COLUMN 
    //GET CENTER
    var d = new Date(),
        monthNames = ["JAN", "FEB", "MAR", "APR", "MAY", "JUNE", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"],
        currentMonth = monthNames[d.getMonth()],
        row = $('#tblGridOutlook').jqxGrid('getrows'),
        insertValues = [],
        center = $("#drop_Center :selected").val()
        
    for (var i = 0; i < 10; i++) {
        calculateOutlook(center, row[i]["Description"], currentMonth, row[i][currentMonth], 1);
    }
}

$("#drop_Center").change(function () {
    loadOutlookPermanet($("#drop_Center :selected").val());
});

$("#btnUploadMonth").click(function () {
    if (_getUserInfo().Roles.includes("HMT2_SUPER_ADMIN") || _getUserInfo().Roles.includes("HMT2_ADMIN")) {
        $('#js-xlsx-plugin').toggle();
    } else {
        _showNotification("error", "User not allwed to upload files");
    }    
});