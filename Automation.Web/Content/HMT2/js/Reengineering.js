﻿var rowid;

$(document).ready(function () {
    _hideMenu();

        XLSX.createJsXlsx({
            idElement: "js-xlsx-plugin",
            fileInput: {
                dropText: "Drop an Excel file here...",
                hideDrop: false,
                inputText: "... or click here to select a file"
            },
            onSuccess: function (jsonData) {

                var excelSource = jsonData;

                _callServer({
                    loadingMsgType: "fullLoading",
                    loadingMsg: "Loading data...",
                    url: '/Reengineering/uploadReenFile',
                    data: {
                        'data': JSON.stringify(excelSource["Project Details"])
                    },
                    type: "post",
                    success: function (json) {
                        renderGridDataSelectedFile(0);
                    }
                });
            }
        });


    renderVersion();

    renderGridDataSelectedFile(0);

    
});

function renderVersion() {

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "", 
        url: '/Reengineering/getVersionList',
        type: "post",
        success: function (json) {

            result = jQuery.parseJSON(json);


            var source =
                {
                    localdata: result,
                    datafields:
                    [
                        { name: 'ID', type: 'string' },
                        { name: 'UploadBy', type: 'string' },
                        { name: 'UploadDate', type: 'string' },
                        { name: 'Enable', type: 'Boolean' },
                    ],
                    datatype: "array",
                    updaterow: function (rowid, rowdata) {
                    }
                };

            var dataAdapter = new $.jqx.dataAdapter(source);

            var imgEnable = function (row, datafield, value) {

                var rowData = $('#jqxgridVersion').jqxGrid('getrowdata', row);

                if (rowData.Enable == "true") {
                    return 'Selected File'
                }
                else {
                    return 'Unselected'
                }
                

            }

            $("#jqxgridVersion").jqxGrid(
             {
                 width: 600,
                 source: dataAdapter,
                 pageable: true,
                 autoheight: true,
                 columnsresize: true,
                 columns: [
                   { text: 'Upload ID', columntype: 'textbox', datafield: 'ID', width: '3%' },
                   { text: 'Upload By', columntype: 'textbox', datafield: 'UploadBy', width: '31%' },
                   { text: 'Upload Date', columntype: 'textbox', datafield: 'UploadDate', width: '31%' },
                   { text: 'Enable', columntype: 'textbox', cellsrenderer: imgEnable, width: '31%' }
                 ]
             });

            $("#jqxVersion").jqxDropDownButton({ width: 400, height: 25 });


            //Salvar nueva seleccion
            $("#jqxgridVersion").on('rowselect', function (event) {
                event.stopImmediatePropagation();
                var args = event.args;
                var row = $("#jqxgridVersion").jqxGrid('getrowdata', args.rowindex);
                var dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;">' + row['ID'] + ' | ' + row['UploadBy'] + ' | ' + row['UploadDate'] + '</div>';
                $("#jqxVersion").jqxDropDownButton('setContent', dropDownContent);

                rowid = row['ID'];
            });


            $.each((result), function (index, element) {
                if (element.Enable) {
                    var dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;">' + element.ID + ' | ' + element.UploadBy + ' | ' + element.UploadDate + '</div>';
                    $("#jqxVersion").jqxDropDownButton('setContent', dropDownContent);

                    rowid = element.ID;
                }
            });

        }



    });


}

function renderGridDataSelectedFile(ID)
{
    var listOfParams = {
        pFile: ID
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/Reengineering/getListReeng',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {

            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                        { name: 'ID', type: 'string' }
                      , { name: 'UploadID', type: 'string' }
                      , { name: 'ReengID', type: 'string' }
                        , { name: 'InitiativeType', type: 'string' }
                        , { name: 'IncludeExclude', type: 'string' }
                        , { name: 'FinanceFRO', type: 'string' }
                        , { name: 'Title', type: 'string' }
                        , { name: 'InitiativeDescription', type: 'string' }
                        , { name: 'FinanceFROTransformation', type: 'string' }
                        , { name: 'KeyInitiatives', type: 'string' }
                        , { name: 'GPLReengineeringCategory', type: 'string' }
                        , { name: 'Process', type: 'string' }
                        , { name: 'SavesAccruingCenter', type: 'string' }
                        , { name: 'CenterImpact', type: 'string' }
                        , { name: 'Region', type: 'string' }
                        , { name: 'ConfidenceLevel', type: 'string' }
                        , { name: 'FTEReleaseStatus', type: 'string' }
                        , { name: 'ProjectStatus', type: 'string' }
                        , { name: 'ActualStartDate', type: 'string' }
                        , { name: 'ActualEndDate', type: 'string' }
                        , { name: 'PlanFTEOpportunities', type: 'string' }
                        , { name: 'GPL', type: 'string' }
                        , { name: 'GPO', type: 'string' }
                        , { name: 'RPL', type: 'string' }
                        , { name: 'SubProcess', type: 'string' }
                        , { name: 'HCSavesMOUorRP', type: 'string' }
                        , { name: 'IndicateEReqHCSavesMOU', type: 'string' }
                        , { name: 'EmployeeNameMOUTitle', type: 'string' }
                        , { name: 'Month', type: 'string' }
                        , { name: 'QuarterImpact', type: 'string' }
                        , { name: 'Years', type: 'string' }
                        , { name: 'GOC', type: 'string' }
                        , { name: 'NewCSSMaturitySubCategory', type: 'string' }
                        , { name: 'CurrentYearJan', type: 'string' }
                        , { name: 'CurrentYearFeb', type: 'string' }
                        , { name: 'CurrentYearMar', type: 'string' }
                        , { name: 'CurrentYearApr', type: 'string' }
                        , { name: 'CurrentYearMay', type: 'string' }
                        , { name: 'CurrentYearJun', type: 'string' }
                        , { name: 'CurrentYearJul', type: 'string' }
                        , { name: 'CurrentYearAug', type: 'string' }
                        , { name: 'CurrentYearSep', type: 'string' }
                        , { name: 'CurrentYearOct', type: 'string' }
                        , { name: 'CurrentYearNov', type: 'string' }
                        , { name: 'CurrentYearDec', type: 'string' }
                        , { name: 'CurrentYearTotalFTE', type: 'string' }
                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            $('#jqxGridLastFile').jqxGrid(
           {
               source: dataAdapter,
               theme: 'blackberry',
               width: '100%',
              // autoheight: true,
              // autorowheight: true,
               pagesize: 10,
               pageable: true,
               filterable: true,
               showfilterrow: true,
               columnsresize: true,
               selectionmode: 'singlecell',
               enablebrowserselection: true,
               columns: [
                  { text: 'Upload ID', dataField: 'UploadID', filtertype: 'input', editable: false, width: 140 },
                  { text: 'Reeng ID', dataField: 'ReengID', filtertype: 'input', editable: false, width: 120 },
                       { text: 'Initiative Type', dataField: 'InitiativeType', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Include Exclude', dataField: 'IncludeExclude', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Finance FRO', dataField: 'FinanceFRO', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Title', dataField: 'Title', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Initiative Description', dataField: 'InitiativeDescription', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Finance FRO Transformation', dataField: 'FinanceFROTransformation', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Key Initiatives', dataField: 'KeyInitiatives', filtertype: 'input', editable: false, width: 140 },
                       { text: 'GPL Reengineering Category', dataField: 'GPLReengineeringCategory', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Process', dataField: 'Process', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Saves Accruing Center', dataField: 'SavesAccruingCenter', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Center Impact', dataField: 'CenterImpact', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Region', dataField: 'Region', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Confidence Level', dataField: 'ConfidenceLevel', filtertype: 'input', editable: false, width: 140 },
                       { text: 'FTE Release Status', dataField: 'FTEReleaseStatus', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Project Status', dataField: 'ProjectStatus', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Actual Start Date', dataField: 'ActualStartDate', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Actual EndDate', dataField: 'ActualEndDate', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Plan FTE Opportunities', dataField: 'PlanFTEOpportunities', filtertype: 'input', editable: false, width: 140 },
                       { text: 'GPL', dataField: 'GPL', filtertype: 'input', editable: false, width: 140 },
                       { text: 'GPO', dataField: 'GPO', filtertype: 'input', editable: false, width: 140 },
                       { text: 'RPL', dataField: 'RPL', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Sub Process', dataField: 'SubProcess', filtertype: 'input', editable: false, width: 140 },
                       { text: 'HC Saves MOU or RP', dataField: 'HCSavesMOUorRP', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Indicate EReq HCSaves MOU', dataField: 'IndicateEReqHCSavesMOU', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Employee Name MOUTitle', dataField: 'EmployeeNameMOUTitle', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Month', dataField: 'Month', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Quarter Impact', dataField: 'QuarterImpact', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Years', dataField: 'Years', filtertype: 'input', editable: false, width: 140 },
                       { text: 'GOC', dataField: 'GOC', filtertype: 'input', editable: false, width: 140 },
                       { text: 'New CSS Maturity SubCategory', dataField: 'NewCSSMaturitySubCategory', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Current Year Jan', dataField: 'CurrentYearJan', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Current Year Feb', dataField: 'CurrentYearFeb', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Current Year Mar', dataField: 'CurrentYearMar', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Current Year Apr', dataField: 'CurrentYearApr', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Current Year May', dataField: 'CurrentYearMay', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Current Year Jun', dataField: 'CurrentYearJun', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Current Year Jul', dataField: 'CurrentYearJul', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Current Year Aug', dataField: 'CurrentYearAug', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Current Year Sep', dataField: 'CurrentYearSep', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Current Year Oct', dataField: 'CurrentYearOct', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Current Year Nov', dataField: 'CurrentYearNov', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Current Year Dec', dataField: 'CurrentYearDec', filtertype: 'input', editable: false, width: 140 },
                       { text: 'Current Year Total FTE', dataField: 'CurrentYearTotalFTE', filtertype: 'input', editable: false, width: 140 }

               ]

           });


        }
    });


}

function viewSelected()
{
    renderGridDataSelectedFile(rowid);
}


function useForThisMonth()
{
    var listOfParams = {
        pFile: rowid
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Set selection as default file...",
        url: '/Reengineering/setAsDefault',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            renderGridDataSelectedFile(rowid);
            renderVersion();
        }
    });

}