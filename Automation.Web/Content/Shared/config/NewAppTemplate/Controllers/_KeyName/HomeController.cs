﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers._KeyName
{
    [RoutePrefix("_KeyName")]
    public class HomeController : Controller
    {
        //
        // GET: /_KeyName/
        [Route("/")]
        public ActionResult Index()
        {
            return View("~/Views/_KeyName/Home.cshtml");
        }

    }
}
