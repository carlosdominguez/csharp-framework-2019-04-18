﻿/// <reference path="../plugins/util/global.js" />

$(document).ready(function () {
    var urlRequested = _getQueryStringParameter("purl");

    $("#pageNotFoundUrl").attr("href", urlRequested);
    $("#pageNotFoundUrl").html(urlRequested);
});
