﻿/// <reference path="../plugins/util/global.js" />

$(document).ready(function () {

    //Create select of users
    _createSelectSOEID({
        id: "#selSimulateUser",
        selSOEID: ""
    });

    $(".btnSimulateUser").click(function () {
        var selSOEID = $("#selSimulateUser select").val();
        if (selSOEID) {
            
            var url = _getViewVar("SubAppPath") + "/" + _getViewVar("AppKey") + "/?" + $.param({
                psim :  _encodeAsciiString(selSOEID)
            });

            window.location.href = url;
        }
    });
    
});