﻿/// <reference path="../plugins/util/global.js" />

$(document).ready(function () {
    //On Click Check Access Again
    $(".btnCheckAccessAgain").click(function () {
        checkAccessAgain();
    });
});

function checkAccessAgain() {
    _showLoadingFullPage({ msg: "Checking Access..." });

    _refreshSession(function () {
        var urlDenied = _getQueryStringParameter("purl");
        var appKey = _getQueryStringParameter("pappKey");
        if (!urlDenied) {
            urlDenied = _getViewVar("SubAppPath") + "/" + appKey;
        }
        window.location.href = urlDenied;
    });
}