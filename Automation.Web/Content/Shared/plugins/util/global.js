﻿// =============================================================
// Author:		Carlos Dominguez [CD25867]
// Create date: November 05th 2016
// Updated date: October 15th 2018
// Description:	Global functions to use in all applications
// =============================================================

//Global Variables
var _MVCOBJECTS = {};
var _DBOBJECTS = [];
var _DBOBJECTS_ISLOADING = false;
var _DBOBJECTS_PENDING_FNS = [];
var _APPSINFOLIST = [];
var _APPINFO = null;
var _APPDATA = null;
var _AJAX_REQUESTS_COUNT = 0;
var _AJAX_REQUESTS = {};
var _AJAX_PENDING_FNS = [];
var _TIME_ZONES = [];

//JQuery new methods
(function ($) {

    $.fn.extend({
        showLoading: function () {
            $('#loading').remove();
            $(this).after($('<span id="loading"><img alt="" src="data:image/gif;base64,R0lGODlhWgBaAPf/AP7+/tvb2wAAANzc3NXV1dbW1sjIyLOzs8nJydTU1AMDA9fX1yMjI7e3t/39/cvLy9jY2Nra2pSUlGJiYtnZ2c3NzbS0tM7OziIiIiYmJrm5ubGxsZeXl5OTkwICAgUFBdPT05CQkMfHx8rKymtra52dnQYGBmNjY/z8/F5eXiUlJZWVlWhoaLKysszMzL6+vl1dXaOjo/v7+3Jycvr6+iQkJA4ODmBgYNLS0sbGxmFhYd3d3ff39/X19ZKSkvT09I+PjwQEBPLy8u3t7fn5+ba2tjY2Nvj4+NHR0bW1te/v7/b29rCwsF9fX97e3vHx8SEhISgoKC4uLvPz8wEBAcXFxS8vL729vZaWlunp6eTk5I6OjicnJ+Xl5ejo6Obm5uvr69/f3+Hh4WRkZODg4Li4uKKiouzs7Ofn56+vr5ycnOLi4tDQ0MTExJGRkTc3N7+/v+7u7s/Pz66ururq6ry8vPDw8K2trbq6uuPj48PDw2dnZ42NjYqKii0tLVhYWGVlZQ8PD3Z2doyMjCAgIJ6enmZmZg0NDVdXV1xcXBcXFxoaGlpaWhsbGzU1NVlZWaysrKSkpB4eHpqamru7u5iYmJubm8DAwKqqqltbW6urqx8fH2xsbBwcHHNzc3FxcWpqalVVVXBwcBYWFouLi8HBwTExMZ+fn2lpaXV1dRgYGCkpKYmJiQcHB1ZWVlRUVBUVFaWlpcLCwpmZmaCgoKGhoTAwMG9vbywsLG5ubhkZGf7//wsLC4eHh4iIiB0dHTo6OjQ0NHd3d21tbampqSsrK3R0dH5+foSEhBISElNTU6ioqIaGhoCAgDIyMqampgoKCj8/P4KCghMTE1JSUioqKqenpxQUFIODg4GBgXt7e39/f3l5eTMzM3p6enx8fEFBQTg4OBEREf3//1FRUTk5Ofz//3h4eEhISDs7O0dHR319fRAQEFBQUEZGRkxMTAgICIWFhT09PUlJST4+Pjw8PENDQ0VFRU1NTUtLS0pKSkREREJCQgwMDP///////yH/C05FVFNDQVBFMi4wAwEAAAAh/wtYTVAgRGF0YVhNUDw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYxIDY0LjE0MDk0OSwgMjAxMC8xMi8wNy0xMDo1NzowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M1LjEgV2luZG93cyIgeG1wOkNyZWF0ZURhdGU9IjIwMTItMTAtMjdUMTQ6MjA6NTUrMDY6MDAiIHhtcDpNb2RpZnlEYXRlPSIyMDEyLTEwLTI3VDIxOjAwOjA4KzA2OjAwIiB4bXA6TWV0YWRhdGFEYXRlPSIyMDEyLTEwLTI3VDIxOjAwOjA4KzA2OjAwIiBkYzpmb3JtYXQ9ImltYWdlL2dpZiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpGRkYyOEZCMzIwNDYxMUUyOTQyNUFDRjVCRjQwMUYxNiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpGRkYyOEZCNDIwNDYxMUUyOTQyNUFDRjVCRjQwMUYxNiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkZGRjI4RkIxMjA0NjExRTI5NDI1QUNGNUJGNDAxRjE2IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkZGRjI4RkIyMjA0NjExRTI5NDI1QUNGNUJGNDAxRjE2Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Af/+/fz7+vn49/b19PPy8fDv7u3s6+rp6Ofm5eTj4uHg397d3Nva2djX1tXU09LR0M/OzczLysnIx8bFxMPCwcC/vr28u7q5uLe2tbSzsrGwr66trKuqqainpqWko6KhoJ+enZybmpmYl5aVlJOSkZCPjo2Mi4qJiIeGhYSDgoGAf359fHt6eXh3dnV0c3JxcG9ubWxramloZ2ZlZGNiYWBfXl1cW1pZWFdWVVRTUlFQT05NTEtKSUhHRkVEQ0JBQD8+PTw7Ojk4NzY1NDMyMTAvLi0sKyopKCcmJSQjIiEgHx4dHBsaGRgXFhUUExIREA8ODQwLCgkIBwYFBAMCAQAAIfkEBQAA/wAsAAAAAFoAWgAACP8A/QkcSLCgwYMIBQIAMJBIjyk9HCScSLGixYsUGfo7w0bElSuXDAzggbGkyZMWZUDQoKFOnSsv6miQlcWfRpQ4c1oEAKJBnUtwXrwIWgdPHS86kyo9mKdMHaFQX3yMWWqJzaVYcxIpRSmq0KBTKQ3IShalFzxBvYKFSakKirJwL1Io4/Wr1KEwp8TdO7GAhrpD78K5UmfIVb6IB66EqVbwRyU2bybe64RuXbBDf5JcONkiiidfwojpIkRyQjtPhTJefQUOJQMZBcr4wiZHDhxeGC40nRMMDhcuRgivQEFvxgoaMN8VXGaNRTJMaFkqUUJNLQtoDicFIAaBiwvg5YD/H1HhTEU7hJezxlPh7UQAI04VMhPDjJlIMdSYiYDVC3nw4FVwQQUVPFDBExVpUUcZcAzW2mANVNFDRQQUUksMkURiX4aR1FJLHkrxUMF3BAYYoBwuFMCbQWcggEcZRhV1BQQkUWRHLKfQF8OOOtpXCCQ0JLWGCAEKOCCAbMjxAIIrEgTAEBAQiIQTxlVUQQn17cgjjz4GoBMABYxQpIkAVoAAGdod1ORFSRSioZZZxlkCHDrRgMR3AOaZpwte8oUCJBfCuSN+9sVwShJrVkSDHEbqqacL/PHFAya16AgnofTRcoBEOMmAQ6OOXiDeCPwlqpMDc9BiqZaYxlCCBklR/yBmqOFdMAKIiGkwn6AY1mdfCSIklQWRtA5I4A/+cBpXBCWsOqivZpTQRU4M0UAAnqEq6YRufNHQQiG8YloCHqZaJESZjIoK3gMJHNEZGM/kGGevahCD4FJnXIDAgC4QGBwBNXb2hSaW0KJqfbSocQBk5V60RBi/GchuFsp25s8SCNzhKrh3IBEkABUrJcMPSijRQ8N70UCHGGuAEWSaFscs88w012zzzTjnrPPOPPdsEGcoAe1zWQ4QQQQNIQ+dkANPgPGEqTR44UQEEQxAhstKFwQ0GEyQcowwxwyycGQKCRQHBQVQsAAEahcQwIRZK8SQCNpwMswtoogyDCrrIP+RLEFZFLBAABFAwHYEFBAAQZVKM1TFLTOk4skMlE8uyicVECREAgtQULXhnkOwQAEDvDw0Q2EIkorqlLfuiTGinKOFQk4obvjtt1OANlJxz5KLIMC33vonn4BihkA8EEAB7sx3PnjSO3shzOrBC9/6Ld7YsREBa+eu+/cQIB6wzw8MA3wqqVhPuTGe3OLlEAt0z3zzyKIc8xUkqD+8J8B/Ioc/8JPf/G63gPoNDQ/5098MPsE/Qdzif/Ab4PwWMCH7WcwFnJic/hgIPE+IwR88iEAAlifBACxgB9DT2Rm+MQNRzECDwvPEOWbADHf5Yw0CnF/i6BA3f8SABC6EIeX/PmEMYXBiAwP5gdokCIECOME9WfPCMfInxAV6QhSsQNZA7JA4w8WvcxRIgBNs2MMdcGMYxkCf6tBHgm3gKlkMEcIOEpCAAhSgiQtYg+l6eEMf4M2Ft8iFKGZRk4FwBgAoEEIXyEAGMXgBbnwkiKes4QYgrIAYBShbSSxoM8lwMpKgDKUoR0nKUppSZ6b5JFx2o8pTJkYGPBDCFIjQyqUIAQlw0EMBIEkWHtAhDAOgmhPyAIYU8iULfcBHOZwRDGC0gwPaE5pOpuAECgRgANgMAOHyQITOMGQE6IiCKYJhBEc4wgpRcMUOYGYShixhB1XDpjwHMEIyQHEvDEkAOKRg/4RwvOEN/TQnLqgxraTIYA3anOc8C5ebxPwgEaYIhz//+QZzGsEIuMjFPVGiBAgo9KPY5EEtLYKJYlD0ov+8qBGCYQop5OBLWognSOVJAcOMlCK3sEU5UmoEnpYzGMXoRZ12cM2Z0tQ53WpHMAAK0J42tZzd8AMLdHIEoyo0AmEwplLi4A5bOOKkKlWpI6SAiD2ahAZhKKpVIzA7vvQAHaag6FPDGg4r3ECrFHEAQq2KzQgUki83sIJT5xrWYggiKXTwqFW1qcWfEYQGBzBGO9ABgz7gQG4TUUM15BrWizrCCH5oQVKIQNS15qFJN2kDMDxABQ8EIQgCCMQM9JKoL//YwxlgDasj/ECNxubkbEaNwA6sQhFIsIMKNjiEDQIRCBvwQgD2MAxFNFGMbvhTrBe1hS1ksRQUZIFw2dwBNikwACFU5AHiMMFym7tcGyxXAK7YqNZ86Aw/BMOc9wWqI+ZAlh+QgWpoo8AOvkDGhNBgH8h1r4KbewjlUkET7xEIJUJhilXgIgrVcEYKYMPOnDjgB3GggxeUQEuLVKEV/VDwgpO7XA/sQwYd3g0Ic8ABZLCiBC6AMbcSY6peCEDFK27wIaCxCDSt6chky1kuYgvk9io3ELwIxANcWZA9/LjJDF4uL2yAACoTRBgC2DKQsxwIaFyDAl4eSCwE0Ar3HiJqyyw+RCtWQdw0Z+EXCnAznIUsAGGkmSB8uPKbWZzcQ3wAA1/480CO4AoBKHfMCoAGHhRtE4k8QQetoAIveAENXphAAJ3ABKUF4gCNXAERDCgzLEzhjdnhtZSSOQMExGjDHcdlF+Mwxy4CAgAh+QQFAAD/ACwBAAAAWQBaAAAI/wD/CRxIsKDBgwgN0uDR40jChxAjSpw4UQkENgYMjJCzxuE/ABRDihyZkIaTNnpy5BDBss0DMCRjyhRJQVaOESwzGhBRRUSWmUCDGsyiMiNLETtzIBDxoMc/f0KjxpRRIYfOqzkNVBEjtevIMwaWXjWaM4ccB17TShRTBcFYskhFIOChtm7CAW3f7jwqIseTp3YDD3RSRe9eliuFCIQq2G6XKiP08r3p0B/jxg8d/MjSpUuWH5cfCmHqNqtRqzlAQPQHEkUWCEhwBDizGDBQAHFg48CBJDaZJR8hLmhTGqnOnG3QSEQDZ8OdO5AgMSkFM7RMLWyQJNhOgAAIOQT+Qv+cYsCq6b1tCkAE4C8CkzkHDmyIv0F6GKFnkIBIAKK//wQ4EDBFREQVttNxslwAHERiMJHGARYc0AKE8THRwk9AEaCffxwmUAAITqCVmRcXtNFGT1WkNIBHD/VQxBzzxRffhC20MAceMsyEhhwc9uhhAkgMaJ1BMoARQAIEQPCPeBFBMEeNMs4oYY1pkDHTDjjw1+N/ILDRRURDhtTGgxNGSaOEd4ww0wL7befmm9vhcF9glGwQY5QztmBBGqXMVACcgG4HYmBEaAAlnlImscElM0HQZqBuyuGEQCCpVWeEiEo43x1VzBQGG5C+WcGXgVUBX6ZTysfGTHRUEKqWCQD/V6laZEBSJqI12ojhSFDJEIF2oeKwRphdyfDCqbjW1ylQPfQHYAEEILkdEhE0BpISDaShZ640alKGYkHZQYAcgsaJxAALYnZGGZowYaEFLaShySVMBnWEFgX0BiAEtAUXGEgg9SAHHgfcwYQFlFCQY1o92PEEXQPN+m+lRAyBhhd2LEwsZhx37PHHIIcs8sgkl2zyySgntHHKIgOAwssASMzyeEP0sDIRYIjhRBhhiNGvvzM/xdo/SsARQyU+TBJLGwMCxpgSA0AQwdQDUEDBDkEXZNk/SPjAzCBbhM0HK5VQUNAQEFAwQABs7+DEAAtQ8EPWtV3QxyBAhBACEED4/xDCIH0sMNAPEVAQwAD/sD3A2gEssMPCWeexBR956115CG4M4gMdT61h+NRsh354BBBwTvcdvmxxOd97591HEgJFEMDUsos+ewBq0w2GD2GvnjfrfGAxRW60y7748aLTTYAvfPveut6DcEVB7bbbXm3WD7DSvN7cW873IBGcMX311V8ftAF9AKF699yrD0QfEMRhOPnWp8syBH2EsD77rU/exREBSBz9ArADCDhBZihTgg/40Dv+ASFzkxAIvgbItgIMgW7/aEEvGsg+vvWiDgLhAe4GSIE8IDBldJAAKfbXPSCQwhJN+4cQCig72q1tAWKAXNBAsoYOsGILlrMcM/9mQapK9WANC1gA6Kb3BR1iEA3LYAUrfEEKUjCDFXO4IEi25g8HTAENa1hDHuhgPwwOBAURaEAsYoAJPEzKNmYUSWhERKk42vGOeMyjHvfIx5RtUSAOiNnKOMaYQVIEAA4IpEFO2MeCoEAgWZCFwR5QRo9NgQIPuMAAIBaUWQUAFY2gggAEEIRVZGNumKlUHGghChj84REw+MQdwDUTRP7DArAYpS516QglNQYqSODEKxiRiUQYExHkEMYa4BiTK0BDAB8Q5S5FyQUtCAYqAdDBH5qQAhh4MwWJSMErhrGrmNABCgJQgAd2OUoPKEAAyqhjXZYgiFBws5veNKY3qeH/C5mAhA/sDKguczC0ulyBGt5MqEJh0E1X4EAmKACGQAV6DmZ6pQ+uwOdCv6mDV6hBJmDYxEQDOo+PMBIoKPjEHzaq0BSkIBTIkMkO2DFSdkbDkDIRAicYwVCX+jSh3UTEOZxIkS7ooqa7VAehcsFTn/40oU14RDNOChEiSAGpuhwD0NLSjD849anebEIoOjCTYQjAA1SQJjvTKoA5bHUxEtNANmDgChKsQD0WNcgcyPFVl8IgEQl9BaNIApIC8GKUat3lO6XglIMUFAHvUAUsFLEIVUyjBttA5UPoYAivfvWvDHWFKCp5yH9sAamtACFCYvaPA2BAERiAAgNUUAMG/2wiGdSIQ0QoEYpwOvWvKWDEIy5Qy6ekArFpVWs/nmFSRspBBZ1ggHSnK10MTMMQdExIC/7gCt92MxMIhcNbY2KGKKz1HgYYb0FCoYjaUne6UMDAKDZg0ocYQBCIUMYrlKGMR6xDNXmNyRMsoI09kAAZOUALaxOCgE5gwL3vxYB0VfGKP0KEDZCYRCE2UIBH4lQiC16kegcSgmS8l7oYiC0hrPAlRa52NSXjxjROPN0UM4AQDCBuIwfiiVHQuLoShgIUHrDjgSADFrKlsY0JwQVfFlkTqpAwA6Rs4xRjoBPA0OyOwRCMX1QXyFOW8DSwUWSCWAIWXw5zmBfRDS+U2T2kRABEMmSb5BTXQBe/GOybQaLTRYxiEypuBCykwIQR7/EyehiDI2pAiGrwAxnKseWeCxKHxnGFIB8GSkAAACH5BAUAAP8ALAEAAABZAFoAAAj/AP0JHEiwoMGDCAUCGCiDR48jCSNKnEixYsUpYgLgwEEgABgZ/gA4sEiypMmEKLSwQZIABAECCRJQEOJv5MmbOCuqdAliI4ECBZAUmJKzqFGCC8/giAkThMsCPwlEAHm06k0ANFrG3Jrg5c8FOLJYHWsyDo4CXJm+LLAASYSFZONO/MIxbde1QBPwkMs3oZaldr16TUC0r2GCXQCnFey1R024h/mecRlYcIIFNB5HnujgBxg0Weg4psiDwFOoUGHGdBqmoow4WsKQQTMFgG2rT5xACBCBAgQKWhxDPghgDZKfqF8yTYAjTkS4aCqIEFElhwgDBGgOv4qGwoAAAQZ8/x+wYMGTiUcgnE2umnme7QYBhGlj4MEIBA9cPBDhYkjIqwINAYF44PEG3gC/jRaREj1BtRwIbAyQmURZ5GAAAiOMYECGGV54Xk5HGFigiAHsQAEZNiUEwBQRrNQVhAV4QVVEPIwggn0G6IeAATxqeAEKOZ2xwIjgRVCgeAvQNNGKXzhBhhZ07EWRFjngp6GOPF5YQQ5g4FSckUSCWeACXvynInwWLXDjfRuyyeMDD+RAAU4yDCAmkSNS0IWZcTkgxwgPZJklAoTiJ0ICdOKpKG9aGEaDCxoKymOhhhKAEwo73LkocCGhaRQAbLgpKKX7WYpTHkMuGgAFEUAglqdHLf9Qn6QXFjpCGzsAOEQCqoZnZ2F9ZdFGpKMSel8bdgAoAxmajjgAcCPBapQDOFhowHWTGpvDAtJStMSqvX13YAQE7EDEZj1ckEOGLrhQaA6yXKBgTkJEsACrEeQLwQJiQBTZQj/IoYeV++XQBhI/8JkTDV4EcK+dAcSxULdVLUREGBdIt+EFWoAkklUoLDHFD0ekSHHFAqHwQxxK9ADkZjDHLPPMNNds880450yczjDb5sDJPI8FdNAm8UBGAF2kSDRFDvTwxBLwfexPAiwUM80hqgQjTZkKL62QPz+A8EISB5QBxwIJ//cyKf0I4PbbAmBQRNde+xPGBsTMwQQTG8z/MUcZjX6djdtBKGC44R8IAI8FdX/tzw6apHFACxtsQDkTLVjwxUAHKCCAAoUfrsAHno/ihGZEJzVHGphTXnnlBxzQwHk0OCNA6KIfHoQAw9DNcxWY8O3665ZbAEkF/lyCe+6GB7F7I2c0bscGe1tOfOUW7F1EDyXczrzuhVMxAupBrwGJ5S0MTzwTFqSBRiG3L8+8BwKk4TvOEMyxgQVkX/96C3PIA/yc973D0S8J5OMZBe6Quf75z3KaIIMa4ldAw1GBCsgbGsy6oAn2OfCBaQCDHFpBhQqCTgCrWELjlmCBOzywckxIwx3wAJJ8fK6AhRNALxonkAtg4oUbSEMa/zSBKH/g4Bo3/J4AjKCkxv2gDC58YAvuQIlzLQQS8PCeAjxgOLdFYU48FEgcGpAGC7TgAHyjnibw4B+CwMF2cHMbNchwP6I9oQ1pgMTq5qAJSBjgQ/8BWCQyUQ9TROMTdRDIz8I4EAd84QKXuIQekICG4cBFadu5DSOlxshOevKToAylKEdJyk5NrI400+CS0KTKvmjyKHAZggjKhgMV2owHXTDSF/xVFNvAhQzccIQkRtGJDESDA/MyzG2mAIcSdIAPfOhALUTAy6s4AEh1KEYgOgEFQnSzEclAR64iMxIJ+mILHZBAB3wQAlaYoY1XWUgbCKELKGAAA/ZkAAYIwf8OYGwuMmhwAylCEAIJqDOdEhjEJJRQlDgAQxGSkAQGGJBPfEKBHXs4JV9oEIk++OCjBkWoBNzAjDm0kiCTSIY+obDSid6TAYRoxANQWZQCeHQF6gypQX3gBiCwgo43QQE+FLHSlurTpdNAxmE2MAgf6DSnEvioD3qBB5wMwRGLuCdL8elSl44iETTFiQNOsQWcQhWhPA0BHzSBEzJEoRMMqAE+a8CAur4UA6pQhmF4oAYfrICggA2BU9fZATfE4mUm8YItFhHXudb1qPocxR8MI4MSAIGdgRWsOtOKiZP6Qwb3UEVjoUBXu7r0Gqk4jCa2gNnADjadfNBATtaRDLr/3rO0R72nKhjnKbig4ALP4AMzsHCANVQEAb3ILEGdGtUQ9KGIJllIBKAg0YnK1bQ1UIQ70naQRVJgG39ABCMY8YhXTCAGe1GaQZTQgYFmlrk+6MMpzgWgSiSDtI21KxQ6gQERnMkfBtABIk6ggwIXuAnKyEYTE4KEc75XAisAAinGCSAHNGMUitiENwlBiE1cAwMIlMgOJpAIHUzAwDcwsQ5eAQT1HsQFfOjDFoAAWDf0wQ1ssAomgAGLQCSDHeJQRShcgEoHMOMRJ5iAkpe8ZB3AwADPEUgAaEGKXvjCyoOIhXHDehE8xOMc3AACAhQirQjAoAlMTrOJH8EMWGlytFk5wEMdELAGk8FSRRJpgStMnOY0p4ATyfJs0E7xBz73eck3OMEASlmQSRT60EzWQRMKwGiCEMMVN4A0ogGRh0oPpAqhSEGBUzxqJRc4E56gr6eFQIJHjBrFiHbFMzz9NQso49WlVjIjhpEsWtuGCH14xQ1SPAFiT2C8F6A1mf0xBSxk4g8puEETYJAJRJCgCsoeCGQS4IZcTKAJLBCGJpzzymwPRAhrCIOMtG2YXYzDHLsICAAh+QQFAAD/ACwBAAAAWQBaAAAI/wD/CRxIsKDBgwgNovi3hAiAhBAjSpxIcaIQLWEgRAjg5AyNiiBDipxIJwKEAAEGBKBAYcCUfw9HypxJEc2CjQNyogxQIMIPmkCDEvT3z06EjSlVojwKgYzQpzSJBkC6sypKCkOgahVpZ4FVqyopON1KdmIWql937thBpKzbhF/Qpt3Z461dg0fnWuXxzx/Ru2/ByE0bYYeMvn8BQ3zCxNunGdJKOYAZMyEFlEqtHqWAhiKAKV7QdH7SV2utDAJSq46Wo3RCf3kK6Pya9+drgUJ2EADBG0SCAD8Tz/QnSPUHBR6opOa1bCIFCJmtUvAi/KA/NAkIJMhOoMACHAWUuP8e+RBIauUePKhWDu8KzIgLLg/YSH/B2IhKdhfQTqD7fhAEvCTTZAVAo9qB6wkgRV2VHfSEExpF8BwEO2TRlkQL9Kfhhv2BEAFQuSCIIBUkCpDGexAtlEUXoXmxRE3gcdhfAfsVAIJ4Mh3hh4gjKjeGRH7JBIATICywn4w02rjGTF0swiOP/Iy3FQARJGBkkjRymMAADYLkhDhPikiPXTJk6B+WWu7QZUVeOBnmgfkIVB1UVZ6ZZJprUoRCPW8KQIUCAgjzz0JuhZEAlnf2dygOWshEVB99oifCP5O5NQR4iB45Y4CO/gNGDW8q9woAeWpFZKYb4nCfSKT+owEvAgD/SuKs6nHRhWJEBIBDd5sWkAASFPAlZEwWjPKkERQoRlQPFOy63X6/DvDinCMNQIIi6gkQBBfYvERtWQDQEBcBOBwKQRaEfivRml7ocUcaCNgGgLpbVfoPD0IIscRhivXr778AByzwwAQXbLB1B/dLVKkJ20VvwzQdoYUTWTwMsUFELMEDtTFRoI07puDyBjk+nHHxbTzkgYQLD7iAQxdHUCqQAw9NwoUikmAAxSaNwAIMHFKeLFAWLlSRgwhIG+0CHQWFoAghXGSgQg01MKCCJAy4J7RrWhggggFgez0CAjnkAMZAcECBgQpTT011DSr84sySQhMVRxVgIxB23g+I/4DAiyi804jUVLvtNhejZLM1TAW0AfbXexuAgAh67PAPAlBYbXjhhW+SDo4nP7E35JGX/cARxMAi9eZwv80ABhds/UUVY3sdedgj5KAEJqpE7XYNhBNewyZlBH3wF44/fnvebYCBiSJs/x4921QT4h7DA3fh+Nc5LC+5LF5oogjc0k9fdQ0JbA2GHkgb0L33ui+QgeZvT69CBp3sI6zQInSv9+0IIFvs/qEDRQiPfNODBRYW9w8K6CGA3kOAHsQgEAhYQRJu01z0dNEO2yxuBHjzXhvY8BGBFAEDi2AA3KxGtWvQw3IM/IcSRtCGAOqtfXqogLwekgN1qEIXjfhFI/9wBohbxVAgU8BBG8omgjZUoQoFqEulYrKEOeTiFfpwxTf0IBAA2CuGcRgACMATBiVMpkvC+SJijmg8NrrxjXCMoxznSMeDBWkg2PuXxUByx8X1MSh/mQIEEFCBAcRsYEQYQhfQoIQSAkU4dDjFJ2Dwh0c0QRBpOCRggvQDENShBUzYwAHgAIG25BEi/vDiP5CAClckIgUwiGUmQqGNLyjrH3TQwBxacAALHKAFLYDEFUgzHIEU4AaM0IEOmpCCFDRBBzdAhDGyApghWIAJFkiCBXx5gF6mAQ91oYkQjGHJFNyAmU1owg3O+Qo3AAYFl5iDNluwzV5y8w4GAEoDXnH/TnMyc53OvAEMMjEAmblFDGnoJjd9Wc9tpiELM3FAMx6xTH+a8wYBbYIymmMXA9zBngoFaTbngISZPCEXMOinSjGqzia4gg93uYIo69lLkPKSCZOSiRcMYdGVNtOZf5DGKWdCBEocQJvbTCpND5AGOKixInbgRCYq6tNmNuEPrLALCvAwU6UmtZtFaAEXZYKCbzBCpf/EqFURYYm7tIEJ9vQqQy2QBjnQJBLKcOYz0wrLcz7iAW2c18x20IYN3KEMLjAZZSASAXnKtZ4tSMIcViWSLuhgqnu9KCyv+g1NGkSwXbDGFvjAhy2YVgKycGRCltCADTyWmy2ghGpBAoCF/yThFer06Q0SoQMCdBFhAZBAH1ZA3A4YV7h3uBBE1jCHuHo1DWnwQlAAEINH/CGduYVBKG6Qz98W5CFemEQIOkBcLBi3uH2wAERUOYANJDQJv6Rncyn4FD0YAxGh+MMrQpEJZARgIpDYAnEHPGAJGBgIH5LIF2SaBvfeYQOyUOweI/KQIzxAE7SoxQYWIBAU0EsLPjCwBIgrAfKuQMRu0ERF6LAANuAgAmcbyITX+62HYA8BgzjxiE9sYgOfeBJCaOO6GvYCPuiYxD0eMXltWceB4GEL5N3xkX1sXCM2+R962EKJSezjI3cAC2cbKgMLYGQSI3nESi7ElQfyAzX4wFXMPEbzCgYx1isThQ18kPORiQsENXjQzg6wQJ7Pu4LzhsAN9F3zQHiAByBouQM+8EEI+KCGZNFM0YlZgwUKUehKxCAHL3EAoRRdkB6AgQ5KIJSYhRIQACH5BAUAAP8ALAEAAABZAFoAAAj/AP0JHEiwoMGDCA3SIYBkAI+EECNKnEgxIYCBGtr9CiKAVzFhWvw5qEiypEmEI3mQEMCSigeWAn4V8XfxpM2bEgHIgCHAwwcFQBUE4QivDM6jSA1uETB0aFChCgQs6kKzZtKrJbM0EvC0q4eoqURaxUpWYqyeXZ8G+SBARY+ycCd6Ypo2aBAFVKAViMs3oQ60dYO61NO3cEFAdAMDFfDBAE3DhrclViwgUJiqkCX2KMXH2zEscsYmrMNV8V0B4UZOXHIGDJgzSwSKxpnm3iZVinQpgpIIiUiINKKVrhuEioBIOX/kiUABgnMITmLPPsmqUacMKlRkYJCh0aoDEkXY/xjuVUA7GRLpUAjQPEKEAU4gBLCDE8DFWoswaM+ePUOGTVyIINtBF53yEhV3eTCUcabQIZEQAQQwQIQR7uBEABBQ8JZNI0WwChT8hahCDVyogs8RjxlU0wFvwAQTNIZkkSJCMjixHoU4DjAABGGoZlMzuogY4nYMLELJjATZJxARLQwzDzjKMMPGgBApUQCOWE64QwH02YSCPpIIyR8X2o1yDpID1TRdmhF1sYCOWFIIHwRU2UTHG1D4p+eeenaSSWEAhLGejoROiGMEa9y0hi3c8eloJ4gU5oATzBUK56GJ2gRGORg46mgjE6BJFhnuWWoohYjehEIo13maARdcRP+hCiu/8ZVHADuYeuoACzh4kxqKuOrfiAxcICpWSiwgoalyaohTHOC06mkUi+QikANrIgWAGBToSiEFXyCVQzWdiAjrKouAIyNkRNgY4YSERkDAACjaBAAKF8FhyiiEYFBDDRgQMso7O2QmUA9kKMsenAuE8dBNDuB7ERnrAIOBJFBwAc4Kb2ULlwNn7IChfDtkgcKxJSnpjxIPlKEBDg+rDJlqKBwxhRA8qOaxwTz37PPPQAct9NBEl7Vz0WVhi/TSTFtEEhFD0NFl0xQBQMQSRMxm30VaxHDMDLekgswBT9DkI9UE0dDFjgREsAMdNAxYEx6gJDIBIIYAckMTwoD/gPZBQ0CAQwIEFFBAAglQIASbB6QwhiF7sMACKCzsocMefv890BCIG2544YcnULZAOJwwRuSRS45K5E144uDRQPeAAwgFLOB5AQQssAAICxAhUjw6oMICIKlLbjwM1qAs9EVizF777bcjQVUEJ6BOvPGS77EHIIJsSPURiOduO/ShE3CEHolAzoL62GsPyAlkKB+0HSCAPv7tBOQOghB6pGAI9gDM3himBLueDWFwhbuf5woHAQLwz38BBOD2/FZAnp1hcAnQHfkKkCEQKEEPMPhfBI2nPTHID2hPmF0GFfg5w4luDaAoXgQNMYZj+A5tNFhABnFHvsLhgAIn88EN/0aYvRQ0QHP+0AISIHC4DRaADb7KwydOQMQmxCM2aLvIERrIQ/LhYAcnuwgSUHEDQGBvdYnwBho0V5MfLAAJCwBd/gogBwrc8DERkEYiYDCBCeigCWPgwBmQSJAeDKB+CQAB4kCwBt9dRGU0QMAkeoGNPiyDALKp4PJ+4AUn7GANXxDCI9mULZmxUZOETKUqV8nKVrrylVnM5LWGhkptwZIkS9DChNCAnqDJYApnsMMU4paUi4zEDi+oxQp84AYsWAMBdzRMTYggBjY8QAQPeAAbtDCSk9XHH2IohA8kgAUOcAALHQACJuKQmYs8gQ05MEA2HzACBFQhAVi8CdcmIf+BSpjznxyoRAee8YPM9GAEIqgAPbOJgAfY8wL1ugkPYtCBSvgToAHlgBvKYEq4oAAEOXCBSOfJ0BHkIAL69IcctoDRlmJhBV6oVVyyUIURkDSb9RzBCAwggqmZBAB38EFLW+qGHBgmAvG8KT0RYNMR6OEyNunBKco5VIC6wQKAAkFDlZpTnYoAAjdRwiRWUFWA+mAOAFAaXGQgh60qdZ47BUEtBfIDNXSgrP90AxMw49ELiMCmb8WpARYAMWLcFa8cCAEcDLOAHLg1sAYIAE70AAR/YuGiAJ0FBzrghIk4AA0E0KkcAjAFviZECzlYaGAdusabxGGzs8BsZt2wjGj/FqQmQ5BFCw5wAAscYANFiMDJzmYQHlzgsTc1KRJ6eRM2AIGqGK0EFmZRJ4h8wbcNKEISitCAJFggDSIgJkTAUAXkwvUBSjiKfargBh+s4LLlXEEIOCBZp9nBAhvQQAOyu939FiENIzgaGkZQU502tbwxvUoErJFON2yBDx1gwrogAoAcbKAM+81whrWbBKqsqSZKQAICciCCNlTBAKLDykVoQAYElKIULvDCKCEyBAto+MbZbcABBERh2QihCzsIQx7SS5aOkhIiAZhDEnB84yLUwbYkIa7QCNCCJTN5w2Ug8i0HAoIWXFnD3GXnXJm2ACZw98vZLcPipOxKMrTgYsxMXrIFKMHcLfuDCJSwMpMtUAQmYNLOBBGDma+chANQIqKAFkgFmCDnBmB4vwdIwiATnTY59JbPDbBACzZQh+pSOkVguEAdMKwBOEBAOmNOJQ+E8IQfePOER9nFOMyxi4AAACH5BAUAAP8ALAEAAABZAFoAAAj/AP0JHEiwoMGDCAcCEDiEAgEyMhJKnEixokWJC/3lAAXOTxRH5LZkuUiypMmEABz4OyItA4NqfvxUi4JBXil/GU/q3InRHwpukorh8mNFilFTXHDJ4sm0qcEYm6wUnSrFj1Eu9MA43crzDL1VUsMWNerHFgZSOLmqJWmhBi6xUo0atRJF35G1eCv2IQQ3rlwp1bqFyUs4ITcMROH+reqnQuHHBbdh6DtWrlU5kDP7k7BJSt/Fxd5o0UxxIQ8QB2ItS7JDZUqJIrhIsWUF1+yYc00x+KPyIICcS4o0Q8Up3hUaCp0+kMZiwgnnoLY4SZsQxQ0opmrftkoWSoue/iBZ/xFAvny5Ojhz7kwDyBAJUCRIcOI0JpcLikicVbNl2xZuo1BwgkJPyJAXxIEmtBKEAPBY4tQLE6AS33vxzbcHKAtQh1AaqyBmFVFS4PJLO0r0hIUACrRiwgcfmLBiKwJ8cABTX3DCwoQ4xjcMINIQoeFBlyDikgoZqIDdMUP8aNAAyVDh4pNQmiBABnbwRMwNOebICQljIKGkQL8tRMQlvgAygSizZOiTegY1E2OUULYCYwk7yYDNGFlmOUEkXyrEJkG/TQTMm3BCKcAjOymRint54jgGH3069cQmThZqKDC9mTSEMYw2Gh8gfTwGRiOVWrqiAFb4eNIU3HTqKSBYRP/alAxcUNGiqR8IIM+fFgEwyAmeTjgBE7I2hY4Apj4pACg8UYKlp6CAgspgvG41CaGWKiCABjr9NsU3eFao5Q2WhFnYFLYga6oA6kSkk0oUDDMBKCyAwgkqqNCrwzElZpZDIMi28oGKQbgoAAY78JQRCJ/cYMgeJOB7QhO+jJSZSlcQYiCLCwrgBwjFVqQSGMQIsgcqgICyjgZ3VatWoF2IIkmuAkBTAzZJunxRoP78MAAbSIiBHJia5RSHHne08MATpDXt9NNQRy311FRXbfXVj/3mQKZYd+21SSgIoUQPOn9dEAoyyKBzHG1gYsYpsTBRQQ/pmW0QDWCIEcbea8T/MaBBSJyCxSyWqKHGLJXEMJ3dBQmxAwERRB5A5AMwLZBKF2BhSQmcd14I4WIwPlBDEVAQwOmnD0ABBUIMJMYkkxRSiBqc0855JbG0zngPC1AQwQCopw78AMjJsAEHnSfvuQQ5MA6AFhEEAHzwwlMw0hezGK588mpYYs1dZi9BwfDUpy45DRFgsT33lhROR91dKwHBAPSXj/oAEAhRABaFlKD99u2bRMJC9jQljG969EsgAinwgwJUwnD/U57t8mC3IcxPer9TIP124AQIPAECK6id7ZQ3OzUkyWwfHF4GFSg9/AlhCP7z3wg7RztLQMJdXyPC/VrIQtXtADl4kAAE/9dXiBVghnFfgID0eKjBBWjFH2eoxQNn2DkOMGFoX1vIEQIwPiYmkAJhGJBKnFAI9c2uBP2rBBYwUSW7ZeQHTljACicXAAKIQVUZQcMdsMCBScyCA4OrQ+vKZjUerKGLAYAABCjwBSxSR4dwSMMdDiCCLyRHdDhxwBLoIAYxdIEOS/CNzniGSUCV8pSoTKUqV8nKVopOaylBASmhlhJCvsyVFyFCHNDgBSH8LWooWMITfsADrm2lByC4hAaSkIQGtCECv8yMmNBAgQUkoHcB8EI0d7KQM7ygmWXQgDiLYIEcTKFo/nhCABJAgAW4swAJwMEOeOCUM9ShDHgQpz41gP+HJOiBnplZAgiu6c53FmABIAiAqnZCgzY0YJ8QLUMR2GBMvDggDEiAQAEgUNAFFOCjINCCLRMSBgtA9KRlaEAc4IcXJeCgozBdAAQIgINzdgsBSTjpSQ+QoZGeZA04OGhM3TlTHKBhJ0soxUN1us8kPCBrOyhANYfqTgIkIHQ6EUIdlsrUcSLAp2CjgFBh+tGPLoAAAwArQXiw1a7q0wIIeIwDIJCAsRa0rFXF6kkAUIWculUDFiAAAXciBnbGFK8zHc1OCmCBfDIVDw0owxMpooQ8UAACO+gCQClyhgRw9LBmTUAbs0qJBhShDOHcJ2qLkIOKGqSBCHDBA0YwAgP/POALrpEIDaYKWgIgYQCuJYkYisDPk0KWEieUSBxcYIALVEC2znXBCCDgSIQogQ12NWgCEvCDpgAAAswMJ2rFeQANdIEiPaiAASpwARe497nPNUAECNlZgsrUoyAoQHKb8gVZNKAFFmjBAYrgAqaNEgIjqICCF8xg907WNwJRwgAGStP8OoFuW1mIA7IQAQKgtV+vSYgQXOBcBptYuhCYSKAAIAQwfAENYCDbYDPzBdua+MYVQEJ1dxZcqIUBAQ/AcYMvYFNcCiQM6xXygklMN7V+TQsiUPKSL0BPJ3ttCA9wwZKX7F7nsmGbrqQBGxKsYC2XWcsXQAAZjFwQL9g2SrpbrsAILgA+NoMpAAYYgQsQwN4LODfP/bLzQIiAZBFk+bkIMAAb9ivogUzBCUh4wAMukAAt4LDRl6RBD35ABPVY2SK7GIc5dhEQACH5BAUAAP8ALAEAAABZAFoAAAj/AP0JHEiwoMGDCAcCECgkDxk0MhJKnEixokWEADIKTBBC1JgJe7RhinOxpMmTEhcSiZHiD4wJE1JkQnQLCcqbOC1mRGEp1I0JOk7A1HGDkQ4QOZMqJbgQzx8dE4TCnHoCRq4hS7PmFHILxomvYMOOeUTM30KtaC8iYBQ1rNsTKQQRSUu3ojVEN9661QFoTd2/CS09CqoXLEwIgBMXLPFnzJjCX6MGUEzZ35w/bSHfAHWmskUiYdrUuTTiy0KNCQukgCpV6lQdmbCdxSiQh6wt3ratQBDRAeqcAHCc2tJny6A+buZ4MSsRRa9HkaNOhVlURMqFDdwR0tVo0SIM5Kz//8b5YgsQCRI6+HDTgY+EyRN3GErRevoERG4cUJSw6VcGLv9lkMEvKiyzFBuk+JAeeun50AEQHeQh0GwGldLEH018dANUNyByjBAUPdNJDQAKaCIXUGBwRVJDdBBCB+l1AKN6bviwhTURUWgQEs0w8gcjmcw0Ri12UJSHKYSYqKSAXHQCzhM5lTKIgxK4YWWDbkjgAxADTLQQCmzEQkofKxwgoT++SeTGIku2ucoiBt6EwjNAUOnDnTEqiAUfLzBH23UShSJJFFwUaqiSUSwiCk4/lBACg5BGusUdifUgDwZcRKHppiVysYokoeiH0hOTPBrpqUBgkpgS6WC6KadMcv9BSD403PSDJTCeKmkLiaFgDxSZvkporL8gouNFAGBynq6Q8tGGn3VNMKiww/4XhS7f3LRQBX0wi56MbnyBJmDLNEJttSpEsUkpOS1By7LMDlLEsWn1gE8jqwjb6SI3RKStP1+sAOGMVLrRBy1QUiZHMUkGy2kj9YgBnEBOTELKFuzVCAQpyyhRmX5V1KMKsIVmwIAi6lDgDwpJLfREKYW4AUSdtVyQI2VpZpFNOhlsQsgq0fgA5Xj/+sPDFwPskIW/0AJG4RMVlHEFCD94ZvXVWGet9dZcd+3112AnRm/YyGY0Ntl0pZmQDFNMccTZaBt0xBoRdMFyQkLgAAceUrf/EQAPaMLNtdkCRXCLH7Dwokg3wkhsUABJMGFBEUUk0cIBdSwXN0EoLFTJNAKELroAi6g6WwAbTF5EA6yvbkEDWWwuUJpbCEBFEB7krkAQCgjgQSwDnXHAAQ0kkUTrDZShAeZLyL4QHCYIEATuuXuggAdBCGCDHGbpsYEGZSSxOuusl1HEHAnI7o8M/Phe/fvWZ/+IP0O0kEQZ4Y9PfvFJXME02hUwAfwGqAAqiEMLYmBCA4pgPv2Rj4FFSFjcTiG9AcKvd1cggwLFt78OFsECmosbMgRwPQtWjwoCeAYZ7De5Dj7QAkmgg+x6QUITnlAAmBBDGijnwAd+MAkg2lwk/2poQw+gsBRCKALxKOdC1m3gEneL2w5sUEILKqB3jfCYCzbQww4yYQfq80cm3GfF3s3ALE8oA/Ga2IANVOF/cQuAIsg4QAGoYDkL8UIDmLC68ZWhAWmQRdXC6I8riEN6u7viBz4ggF8gYEILGUIbNrCBFrAwCS5oHiEF8oB0jE506FhA01bWBTa0oQ0iAEFnNkkQIkCiCcCwRTRAYQFIEkRUrLRIFMeVy1768pfADKYwh6kYsxGua8ckZtHSIgMhnCEOPcBl1lDQgydMgQdwXMoSyCAHEVRBBA8AwRqkKTaB9CALTqDAAigQAKVlEyd2qEAVDIAABDzABSMQQQI0Wf9MfwhhBxSIQAAGGoAIQCAM/MyJHR7QhnrW854ucIEIkFAryixhBwIdwAAIOlAIOGEuOZEBDkSAgBE4FKIuqAACIkBOuqBAC+wc6EY5WlDT/AsNDXXoQyPK0wdMYZRoeQIFAkpTgiYtAoO8SQGqoNOd8tQFBpCQ4HKiBQhEQKBFlWkEwIATGlzApE1FaURFgJjEhCEAGk2rRovaBZwcQQ4PaKpcESCCAphlqiiRgRPamVWZFlQMeD2IV+M6V51WIX2BPQkKADrTrG6UAlrACQAIQNjC1rMKYQCqVmDaV79GIIQoWUMOSlrYuOYAKxQBgBCyQAYyaAEMIKXIELDa16P/eoxRBjBABcAa1hwkoKUG+YETEkDcBBAAByBAA8vgxoMB0DarFHDCO0uSB3BW1qEjMIALJJgQJUAgAQsggHghsIAE4CAMFZVIHArwXI5GYAHclZMYqpADetYzu20YgQwnsgQCJKAA4SUAgAtQAAKIc6pxiEBM1/reAKB2KWBggwj0QN/6IjW1YfgvgTdcYPH6N74I+YEY1AkBAFNADAlt2cqUIAYnkOELP63IFAiwAA5vWLwEBoETLtKDIXjBC2dYwmm0kliDgAEENr4xjhMAgfSaBADA1VoXBJzkDnuYAEkl5pRrnOQri7cHmvUlGjRc5Q7/F3BF3twQkLCAGtO4WcMcJkAAovxLGVAAB252M5VrDAJxKXMgSkCCgN9MZQATgAJH+DNBtAACAYO3zTUmbpEUPRAZ5AEExh1wAkBAgQdTeiA90EIEcEyBL9SKaJ+WAQ94cITpJiYgACH5BAUAAP8ALAEAAABZAFoAAAj/AP0JHEiwoMGDCAcCEMjjTJYhCxNKnEixokWEACKKSXJKAhAJkfT88AfAwcWTKFMmpAFny6AQPmJuYVWCDEmVOHNSXOhAwyA3PiR0GCpBwhYfYnQqXWpQzqCYMYeukDAVyCQhTLPq7GFpy9CvHbBwwCKUTxuBEbWqrUiBj1CwYcdKcLOFloy1eCvqeQo37ooVQzkMyUs4YRk+bvqKXbFYgpbCkAviGfQW7OLFK9BE3uwPAV+4lzl0uMrZIgovEJCAGDC4Yp4tbrBgWTGW8ewVPkgRS2uw5EIiIOacorVhgUnfSwHkecHkTpo5dzYYsLMTEqnatWVP7TAIgkST/lx4/0uE6A+iR4mkEfDnALzOAkw2WLDQ4sCBFmmKdJG4sEvQ2dnd5ksSvPXmTwswMJLCCRPoMMENjNwAx00qZeTPDvHZd58FB1iQBBNJDFZgQQn4AEQHgBHlQx+RLEHRC4/cMMYYJ9RoYwpNXKDUEwcwoaGGHNqXRhXuJRRGLFv0sUUIgwyyQh0ujkgQHXvAMCMgM2ZJIyOC8KATCHfU1yGHLZB5wAYtZEHhQQuhsMMLadyRxAgiOiDlQHMgMsEYgGCpJY0n/HFJTjJcwoSYGpa5oQV3gLBmb3eiFak/yGRiI6An8EmjDn/4kFMPZWzw46gaDvmoWkfMAMMErLa6pyFY6v8AQzZFnsRDAz6SOioTekAmhCgwOOjqq7GmcMxdKhGBB6K6djiHCJChIAgMNwxL7AkQ+pITAHrk2myHaRRwqlp8MCKsq2PAWuMftOC00ACQfHtAEvMpEdklrlQ7bLqxJoKDTkfU4e2o8zHxwKRMHaHNI9byOwYipNR60kJn9BjkfaIecEcdI202wB6WnjsBg4/MoNlSXhQBSQssb+DyHaV0HNlCBXyCCAxN6KBDE4yEok1SWf3ABiVMpMGEBS8MgEJpNylhhiiypqCDMUz0MG6FAh0xBBpe2LE00wX1EMADF4Th5dVgp6322my37fbbcMct99x01203mxM5wMMSNNz/XRENaDjhhcQETbEDDhWQTQAafVt4d0kCOdHMPM5EYco7rHxBUloAfGFADgiM8MADBhggRxx+o2VSLKuI08gmm0iyCCzdJFFQFwiIIProD4yAAAIuUPf4QmYoIkkNDDCAQQ0qMKBLI0wMpMQIBoxgPe8PuOACAmwckfoFGEiCQfLKK48BBp1ksJ4/EFRhQO+78+7CBVWE4TcAjIyiPBTJn28+BtfIhT964DsEwA97o6uAC+SArLoVoAb8wwD/zPc/SeCiC2CoAgKq1zsE+o50WLEbJq4xPgn2r4QShIIiLvGFKlDPgL+L4e+sZ4DW1E0CsCjhBMlHPhUS4wsiyF3o/2Qow9LZy24cgEX/dshDBkBBFXNAA+hmSMQYGgABLrLbBvS3vyaSDwONEEEPhigCEVyRiCPIARIIBzct4GITXWzi+CQRDNRRIAdnrGLoqqA5v30iGXFsYg2SgQ2B9OABuutdFauQgK/dLQ+O0IUJvagIeYjIH3F4QA5GJ7oZVgEJZ0udAaIwCiiMr3+EuEY32FCQJxDAAFUw4+dEsAAipI4gcnjHKEaxCF3oAhaL+MMATuWAITgBBCCAQBjsgDC60SAJMyDHPKhhjDpI6pYp4U2RFtJMbHrzm+AMpzjHSc5y6iQjEcmIncy5E0jhRQY/eIIQltBNwgyhFNExQAjVwv8DLUQABDhQzQAGV5r2+EMLt5BEEAQgABNkYBtn0MoPIICDBBDgogQA6ABCCZmFXEESDFVAEBbK0FUgAW0X+cECcLCAAmAUo0igQN82kwMbCIAKHviAToOgACoIAANOUIoMKIADAizgpRgtAAjEUM+cCMEPAlCAAnK606ku1B0zzaY/wMAGpHr1ohzNiyUEEASdmvWsOhVAEXTihKJ+9aU4UFNTU6IOAaD1rgtFBaEocNS3YhQHNpnrSWjAALLeFa0CiEZOiACBvvqVAAnYwebyAoZGCKAVhz2rAKTgPZXQ4LEvRcIwCzMFQjQ0szsVQDnYSBEHDAAEoCUADvpYmHT/2BW1H1hoKHTiha4WwLEvLYBFZSYRFEwhC1rQghfi0FmU+uMbhkXtQksAsIv+1qsWxcEOHJmQKZABAgGIgHjFSweTNPAgA5jGaTMrgAwcEWtgSEABXIpU4S6AuAgRgngHMIAABIC/AYBAHriLECwIgKp3tesBzsmeL6hmvtbNaAFsmJAj7IACAPZvf/0LATSwlj3+aAZD0eqBy1YiKwCIQwASgIQEABQEa7AaRb4QAf/a+Mb/jQCFE4IJqDL0x+WgBElQIFgDOeAJX+jCQ3rQTQtvGMc2HgAFbJKQtCzBAscwBAmYcYW7lOTDnHkClKHcXydktcoIaY/j2nYG8Y4Zecr4LScYwPvmG0fAakWmmxLoPOYN19hLeZ7bEzCs4f6O98ZhIHA5aeAEChQ6vG6OwAAiEFF2osUfSlhAjTN8Ywyf2dIOyEKNw7uDHWy4sfu09EBQ8AUKUKDGEaAABJSZalUTpAdf2MF+d0AHRduaIDI4whFokE6mBQQAOw==" title="loading" style="width: 15px;padding-right:5px;" /> Loading....</span>').show('slow'));
        },
        hideLoading: function () {
            $('#loading').remove();
        }
    });

})(jQuery);

//En algunos browser no existe Object.keys
Object.keys = function (obj) {
    var keys = [];

    for (var i in obj) {
        if (obj.hasOwnProperty && obj.hasOwnProperty(i)) {
            keys.push(i);
        }
    }

    return keys;
};

//----------------------------------------------------------------
//Custom Components Functions
//----------------------------------------------------------------
//Variables to manage Select2 of find User
var _flagSelect2SearchDone = true;
var _flagSelect2LastTimeoutDone = true;
var _flagSelect2RequestWithTimeout = true;
var _flagSelect2ValueChangeWhileRequest = false;
function _createSelectSOEID(customOptions) {
    //Default Options.
    var options = {
        id: '',
        selSOEID: '',
        disabled: false,
        onReady: null
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    var $selectContainer;
    if (typeof options.id == "string") {
        $selectContainer = $(options.id);
    } else {
        $selectContainer = options.id;
    }

    var idSelect2 = _createCustomID();
    //var $control = $(
    //    '<div class="form-group"> ' +
    //    '   <select id="select2-' + idSelect2 + '"></select> ' +
    //    '</div> '
    //);
    var $control = $('<select id="select2-' + idSelect2 + '" style="width: 100%" name="select2-' + idSelect2 + '" class="form-control"></select> ');
    $selectContainer.append($control);

    //Fix bug with JQuery Validate on change set success
    $control.change(function () {
        $control.trigger("keyup");
    });

    //_loadDBObjects(function () {
    //var spName = _getDBObjectFullName("FindEmployee");
    var spName = "[Automation].[dbo].[spAFrwkFindEmployee]";

    //Disable Select2
    if (options.disabled) {
        $selectContainer.find("#select2-" + idSelect2).prop('disabled', true);
    }

    var select2 = $selectContainer.find("#select2-" + idSelect2).select2({
        placeholder: "Please enter SOEID or Name:",
        allowClear: true,
        ajax: {
            url: _fixFrameworkURL('/Shared/FindEmployee?spName=' + spName),
            delay: 40,
            processResults: function (dataResult) {
                _flagSelect2SearchDone = true;

                //Call again by value changed
                //console.log("processResults._flagSelect2ValueChangeWhileRequest", _flagSelect2ValueChangeWhileRequest);
                if (_flagSelect2ValueChangeWhileRequest) {
                    _flagSelect2RequestWithTimeout = false;
                    _flagSelect2ValueChangeWhileRequest = false;
                    $(".select2-search__field").trigger("input");
                }

                var data = $.map(dataResult, function (obj) {
                    obj.id = obj.id || obj.SOEID;          // replace pk with your identifier
                    obj.text = obj.text || ('(' + obj.SOEID + ') ' + obj.Name);    // replace name with the property used for the text
                    //obj.text = obj.text || (obj.NAME + ' - ' + obj.EMAIL_ADDR + ' (' + obj.SOEID + ')');    // replace name with the property used for the text

                    return obj;
                });

                return {
                    results: data
                };
            }
        },
        templateSelection: function (data, container) {
            // Add custom attributes to the <option> tag for the selected option
            $(data.element).attr('Name', data.Name);
            $(data.element).attr('Email', data.Email);
            return data.text;
        }
    });

    //Load default value
    if (options.selSOEID) {
        _callServer({
            loadingMsg: "Get employee information...",
            url: '/Shared/FindEmployee',
            data: {
                spName: spName,
                term: options.selSOEID,
                _type: 'query',
                q: options.selSOEID
            },
            type: "GET",
            success: function (resultList) {
                if (resultList.length > 0) {
                    var objEmployee = resultList[0];

                    //Set default value
                    var $option = $('<option>(' + objEmployee.SOEID + ') ' + objEmployee.Name + '</option>').val(objEmployee.SOEID);

                    // append the option and update Select2
                    $selectContainer.find("#select2-" + idSelect2).append($option).trigger('change');
                    //$selectContainer.find("#select2-" + idSelect2).append($option);

                    if (options.onReady) {
                        options.onReady();
                    }
                }
            }
        });
    } else {
        if (options.onReady) {
            options.onReady();
        }
    }
    //console.log(select2);
    //});
}

function _createSelectGOC(customOptions) {
    //Default Options.
    var options = {
        id: '',
        labelClass: "col-md-4",
        fieldClass: "col-md-6",
        actionClass: "col-md-2",
        selelectedGOC: '',
        placeholderMSCode: "Enter MS Code...",
        disabled: false
    };

    //Merge default options
    $.extend(true, options, customOptions);

    //Get GOC container
    var $divContainer = $(options.id);
    var htmlGOCContainer =
        ' <div class="form-group"> ' +
        '     <label class="' + options.labelClass + ' control-label"> ' +
        '         <i class="fa fa-angle-double-right"></i> GOC ' +
        '     </label> ' +
        '     <div class="' + options.fieldClass + '"> ' +
        '         <input class="form-control txtGOC" name="txtGOC" placeholder="Enter GOC or click in edit icon to select..." value="' + options.selelectedGOC + '" /> ' +
        '     </div> ' +
        '     <div class="' + options.actionClass + '"> ' +
        '         <button type="button" title="Select GOC" class="btn btn-default btn-sm btnSelectGOC"> ' +
        '             <i class="fa fa-pencil-square-o"></i> ' +
        '         </button> ' +
        '     </div> ' +
        ' </div> ';
    $divContainer.append(htmlGOCContainer);

    //On click Select GOC
    $divContainer.find(".btnSelectGOC").click(function () {
        openModalFindGOC({
            onSelectGOC: function (selectedRowGOC) {
                var GOCSummary = selectedRowGOC.GOCDesc + " [" + selectedRowGOC.GOC + "]";
                $divContainer.find(".txtGOC").val(GOCSummary);
            }
        });
    });

    function openModalFindGOC(findGOCModalCustomOptions) {
        var idModal = _createCustomID();

        //Default Options.
        var findGOCModalOptions = {
            onSelectGOC: null
        };

        //Hacer un merge con las opciones que nos enviaron y las default.
        $.extend(true, findGOCModalOptions, findGOCModalCustomOptions);

        var htmlContentModal =
            '<div class="row" style="height: 400px;"> ' +
            '   <div class="col-md-6"> ' +

                    //Select User by SOEID
            '       <div class="form-group"> ' +
            '           <label class="col-md-4 control-label"> ' +
            '               <i class="fa fa-angle-double-right"></i> GOCs by MS of User ' +
            '           </label> ' +
            '           <div class="col-md-8"> ' +
            '               <div class="selUserToGetMS" id="selUserToGetMS_' + idModal + '" ></div> ' +
            '               <label class="GOCOfUser" style="padding-left: 14px;"></label>' +
            '           </div> ' +
            '       </div> ' +
            '   </div> ' +

            '   <div class="col-md-5"> ' +
                    //Managed Segment
            '       <div class="form-group"> ' +
            '           <label class="col-md-6 control-label"> ' +
            '               <i class="fa fa-angle-double-right"></i> GOCs by MS Code ' +
            '           </label> ' +
            '           <div class="col-md-6"> ' +
            '               <input class="form-control txtMSCode" name="txtMSCode" placeholder="' + options.placeholderMSCode + '" /> ' +
            '           </div> ' +
            '       </div> ' +
            '    </div> ' +
            '    <div class="col-md-1"> ' +
            '        <button type="button" class="btnFindGOCs btn btn-primary pull-right"><i class="fa fa-search"></i> Find</button> ' +
            '    </div> ' +
            '    <div class="col-md-12" style="padding-top: 15px;"> ' +
            '        <div class="tblSelectGOC" id="tblSelectGOC_' + idModal + '"></div> ' +
            '    </div> ' +
            '</div> ';

        //Show user modal
        _showModal({
            modalId: "modalSelGOC",
            width: "90%",
            addCloseButton: true,
            buttons: [{
                name: "<i class='fa fa-crosshairs'></i> Select",
                class: "btn-success",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    var idTable = "#tblSelectGOC_" + idModal;
                    var selectedRow = $.jqxGridApi.getOneSelectedRow(idTable, true);
                    if (selectedRow) {
                        findGOCModalOptions.onSelectGOC(selectedRow);

                        //Close Modal
                        $modal.find(".close").click();
                    }
                }
            }],
            title: "Find GOC",
            contentHtml: htmlContentModal,
            onReady: function ($modal) {
                //Load select of user to get managed segment
                _createSelectSOEID({
                    id: "#" + $modal.find(".selUserToGetMS").attr("id"),
                    selSOEID: _getSOEID(),
                    onReady: function () {
                        //Load table 
                        loadTable($modal);
                    }
                });

                //On click Find GOCs
                $modal.find(".btnFindGOCs").click(function () {
                    //Load table 
                    loadTable($modal);
                });
            }
        });

        function loadTable($modal) {
            var idTable = "#tblSelectGOC_" + idModal;
            var selectedSOEID = $modal.find(".selUserToGetMS select").val();
            var managedSegmentCode = $modal.find(".txtMSCode").val();
            var sql = "";
            var sqlGOCByManagedSegment =
                " SELECT \n " +
                "     M.[ID]				AS [IDManagedSegment], \n " +
	            "     M.[Description]		AS [ManagedSegmentDesc], \n " +
	            "     MG.[ID]				AS [IDManagedGeography], \n " +
	            "     MG.[Description]	    AS [ManagedGeographyDesc], \n " +
	            "     G.[ID]				AS [GOC], \n " +
	            "     G.[Description]		AS [GOCDesc], \n " +
                "     0		                AS [IsEmployeeGOC] \n " +
                " FROM \n " +
                "     [Automation].[dbo].[fnAFrwkManagedSegmentGetRecursiveChildsList](" + managedSegmentCode + ") AS MChilds \n " +
                "         INNER JOIN [Automation].[dbo].[tblManagedSegment] M WITH(READUNCOMMITTED) ON MChilds.[ID] = M.[ID] \n " +
                "         INNER JOIN [Automation].[dbo].[tblGlobalOrganizationCode] G WITH(READUNCOMMITTED) ON MChilds.[ID] = G.[ManagedSegmentID] \n " +
                "         INNER JOIN [Automation].[dbo].[tblManagedGeography] MG WITH(READUNCOMMITTED) ON G.[ManagedGeographyID] = MG.[ID] \n " +
                " ORDER BY \n " +
                "     M.[Description], MG.[Description], G.[Description] ";

            var sqlGOCsByManagedSegmentOfUser =
                " SELECT \n " +
                "     M.[ID]				AS [IDManagedSegment], \n " +
                "     M.[Description]		AS [ManagedSegmentDesc], \n " +
                "     MG.[ID]				AS [IDManagedGeography], \n " +
                "     MG.[Description]	    AS [ManagedGeographyDesc], \n " +
                "     G.[ID]				AS [GOC], \n " +
                "     G.[Description]		AS [GOCDesc], \n " +
                "     CASE \n " +
                "         WHEN G.[ID] = E.[GOC] THEN 1 \n " +
                "         ELSE 0 \n " +
                "     END					AS [IsEmployeeGOC] \n " +
                " FROM \n " +
                "     [Automation].[dbo].[tblEmployee] E WITH(READUNCOMMITTED) \n " +
                "         CROSS APPLY ( \n " +
                "             SELECT [ID] \n " +
                "             FROM [Automation].[dbo].[fnAFrwkManagedSegmentGetRecursiveChildsList](E.[ManagedSegmentID]) \n " +
                " 	    ) AS MChilds \n " +
                " 	    INNER JOIN [Automation].[dbo].[tblManagedSegment] M WITH(READUNCOMMITTED) ON MChilds.[ID] = M.[ID] \n " +
                " 	    INNER JOIN [Automation].[dbo].[tblGlobalOrganizationCode] G WITH(READUNCOMMITTED) ON G.[ManagedSegmentID] = MChilds.[ID] \n " +
                " 	    INNER JOIN [Automation].[dbo].[tblManagedGeography] MG WITH(READUNCOMMITTED) ON G.[ManagedGeographyID] = MG.[ID] \n " +
                " WHERE \n " +
                "     E.[SOEID] = '" + selectedSOEID + "' \n " +
                " ORDER BY \n " +
                "     MChilds.[ID], MG.[Description], G.[Description]";

            if (selectedSOEID || managedSegmentCode) {
                if (managedSegmentCode) {
                    sql = sqlGOCByManagedSegment;
                } else {
                    sql = sqlGOCsByManagedSegmentOfUser;
                }

                _callServer({
                    loadingMsgType: "topBar",
                    loadingMsg: "Loading GOCs...",
                    url: '/Shared/ExecQuery',
                    data: { 'pjsonSql': _toJSON(sql) },
                    type: "post",
                    success: function (resultList) {
                        $.jqxGridApi.create({
                            showTo: idTable,
                            options: {
                                height: "350",
                                autoheight: false,
                                autorowheight: false,
                                selectionmode: "singlerow",
                                showfilterrow: true,
                                sortable: true,
                                editable: true,
                                groupable: false
                            },
                            source: {
                                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                                dataBinding: "Large Data Set Local",
                                rows: resultList
                            },
                            columns: [
                                //type: string - text - number - int - float - date - time 
                                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                                { name: 'IsEmployeeGOC', type: 'number', hidden: true },
                                { name: 'IDManagedSegment', text: 'MS Code', width: '8%', type: 'string', filtertype: 'checkedlist' },
                                { name: 'ManagedSegmentDesc', text: 'MS Desc', width: '20%', type: 'string', filtertype: 'checkedlist' },
                                { name: 'ManagedGeographyDesc', text: 'Managed Geography', width: '15%', type: 'string', filtertype: 'checkedlist' },
                                { name: 'GOCDesc', text: 'GOC Desc', width: '42%', type: 'string', filtertype: 'input' },
                                { name: 'GOC', text: 'GOC', width: '15%', type: 'string', filtertype: 'input' }
                            ],
                            ready: function () {
                                //Set GOC of user if exists
                                var GOCOfUser = "";
                                var objGOCOfUser = _findOneObjByProperty(resultList, "IsEmployeeGOC", "1");
                                if (objGOCOfUser) {
                                    var GOCOfUser = "<b>GOC: </b>" + objGOCOfUser.GOCDesc + " [" + objGOCOfUser.GOC + "]";
                                }
                                $modal.find(".GOCOfUser").html(GOCOfUser);
                            }
                        });
                    }
                });
            } else {
                _showAlert({
                    showTo: $modal.find(".modal-body"),
                    type: 'error',
                    title: "Message",
                    content: "Please select User or enter Managed Segment Code."
                });
            }


        }
    }
}

function _createSelectEDCFC(customOptions) {

    //Default Options.
    var options = {
        id: "",
        label: "",
        attrs: {
            name: "lblEDCFCsSummary"
        },
        jsonEDCFCs: "[]",
        onSelect: function (strListEDCFCs, newEDCFCs, deletedEDCFCs) { }
    };

    //Merge default options
    $.extend(true, options, customOptions);

    //Load Control
    loadControl();

    //Function load Control
    function loadControl() {
        //Clear current data
        $(options.id).contents().remove();

        //Add field to container
        $(options.id).append(getHtml());

        //Set attr StrListEDCFC with jsonEDCFCs
        var eDCFCs = JSON.parse(options.jsonEDCFCs);
        $(options.id).attr("StrListEDCFC", _generateString(eDCFCs, ["ID"], ";"));

        //Init popovers functions
        _GLOBAL_SETTINGS.tooltipsPopovers();

        //On click Edit EDCFCs
        $(options.id).find(".btnEditEDCFCs").click(function () {
            _openModalSelectEDCFCs({
                jsonEDCFCs: options.jsonEDCFCs,
                onSelect: function (selectedEDCFCs) {
                    var strListEDCFC = _generateString(selectedEDCFCs, ["ID"], ";");

                    //Set new json
                    options.jsonEDCFCs = JSON.stringify(selectedEDCFCs);

                    //Update property
                    $(options.id).attr("StrListEDCFC", strListEDCFC);

                    //Refresh Control
                    loadControl();

                    //Caller
                    options.onSelect(strListEDCFC, selectedEDCFCs);
                }
            });
        });
    }

    //Control HTML
    function getHtml() {
        var eDCFCs = JSON.parse(options.jsonEDCFCs);
        var htmlViewEDCFCs = "";
        if (eDCFCs.length > 0) {
            var htmlEDCFCPopup = "<div style='padding: 10px; font-size: 15px;'>";

            for (var i = 0; i < eDCFCs.length; i++) {
                var tempData = eDCFCs[i];
                htmlEDCFCPopup += " " + tempData.ID + ": " + tempData.Description + "<br>";
            }
            htmlEDCFCPopup += "</div>";

            htmlViewEDCFCs =
                '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + htmlEDCFCPopup + '" data-title="eDCFC Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                '   <img height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png" class="pull-right"> ' +
                '</div>';
        }

        var html =
            ' <div class="form-group"> ' +
            '     <label class="col-md-4 control-label"> ' +
            '         <i class="fa fa-angle-double-right"></i> ' + options.label +
            '     </label> ' +
            '     <div class="col-md-6"> ' +
            '         <div class="form-control" fieldEDCFC="true" validatelement="true" name="' + options.attrs.name + '" > ' +
            '             <div class="pull-left lblEDCFCsSummary">(' + eDCFCs.length + ') Activities mapped</div> ' +
            '             ' + htmlViewEDCFCs +
            '         </div> ' +
            '     </div> ' +
            '     <div class="col-md-2"> ' +
            '         <button type="button" class="btn btn-default btn-sm btnEditEDCFCs "> ' +
            '             <i class="fa fa-pencil"></i> ' +
            '         </button> ' +
            '     </div> ' +
            ' </div> ';

        return html;
    }
}

function _openModalSelectEDCFCs(customOptions) {
    var idModal = _createCustomID();
    var idEDCFCTableSelected = "tblEDCFCSelected_" + idModal;

    //Default Options.
    var options = {
        title: "Select eDCFC Activities",
        jsonEDCFCs: "[]",
        onSelect: null
    };

    //Merge default options
    $.extend(true, options, customOptions);

    //Show Modal to user
    _showModal({
        modalId: "modalSelectEDCFCs",
        width: "90%",
        addCloseButton: true,
        buttons: [{
            name: "<i class='fa fa-crosshairs'></i> Select Activities",
            class: "btn-info",
            closeModalOnClick: false,
            onClick: function ($modal) {
                //Get current eDCFCs
                var eDCFCRows = $('#' + idEDCFCTableSelected).jqxGrid("getRows");

                //Return selected eDCFCs
                options.onSelect(eDCFCRows);

                //Close modal
                $modal.find(".close").click();
            }
        }],
        title: options.title,
        contentHtml: getHtmlEDCFCTableSelected(),
        onReady: function ($modal) {
            //Add new row 
            $modal.find(".btnNew").click(function () {
                newRow();
            });

            //Delete selected row 
            $modal.find(".btnDelete").click(function () {
                deleteRow();
            });

            //Load table 
            loadTable();

            function loadTable() {
                $.jqxGridApi.create({
                    showTo: "#" + idEDCFCTableSelected,
                    options: {
                        //for comments or descriptions
                        height: "250",
                        autoheight: false,
                        autorowheight: false,
                        selectionmode: "singlerow",
                        showfilterrow: true,
                        sortable: true,
                        editable: true,
                        groupable: false
                    },
                    source: {
                        // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                        dataBinding: "Large Data Set Local",
                        rows: JSON.parse(options.jsonEDCFCs)
                    },
                    columns: [
                        //type: string - text - number - int - float - date - time 
                        //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                        //cellsformat: ddd, MMM dd, yyyy h:mm tt
                        { name: 'ID', text: 'Node ID', width: '10%', type: 'number', filtertype: 'number' },
                        { name: 'Description', text: 'eDCFC Activity', width: '90%', type: 'string', filtertype: 'input' }
                    ],
                    ready: function () { }
                });
            }

            function newRow() {
                openModalFindEDCFC({
                    onSelectEDCFC: function (eDCFCSelected) {
                        var $jqxGrid = $("#" + idEDCFCTableSelected);
                        var datarow = {
                            ID: eDCFCSelected.NODE_ID,
                            Description: eDCFCSelected.NODE_NAME
                        };
                        var commit = $jqxGrid.jqxGrid('addrow', null, datarow);
                    }
                });
            }

            function deleteRow() {
                //Delete 
                var objRowSelected = $.jqxGridApi.getOneSelectedRow("#" + idEDCFCTableSelected, true);
                if (objRowSelected) {
                    var htmlListChecklist = "";
                    htmlListChecklist += "<b>Node ID: </b>" + objRowSelected['ID'] + "<br/>";
                    htmlListChecklist += "<b>eDCFC Activity: </b>" + objRowSelected['Description'] + "<br/>";

                    _showModal({
                        width: '40%',
                        modalId: "modalDelRow",
                        addCloseButton: true,
                        buttons: [{
                            name: "<i class='fa fa-trash-o'></i> Delete",
                            class: "btn-danger",
                            onClick: function () {
                                $("#" + idEDCFCTableSelected).jqxGrid('deleterow', objRowSelected.uid);
                            }
                        }],
                        title: "Are you sure you want to delete this row?",
                        contentHtml: htmlListChecklist
                    });
                }
            }

            function saveTable() {
                
            }
        }
    });

    //Create HTML with current eDCFC Activities selected
    function getHtmlEDCFCTableSelected() {
        
        var htmlEDCFCTable =
            '<div class="row" style="height: 400px;"> ' +
            '    <div class="col-md-12"> ' +
            '        <button type="button" class="btnNew btn btn-success top15 left15 bottom15 pull-right"><i class="fa fa-plus-square"></i> New</button> ' +
            '        <button type="button" class="btnDelete btn btn-danger top15 left15 bottom15 pull-right"><i class="fa fa-trash-o"></i> Delete</button> ' +
            '    </div> ' +
            '    <div class="col-md-12"> ' +
            '        <div id="' + idEDCFCTableSelected + '"></div> ' +
            '    </div> ' +
            '</div> ';

        return htmlEDCFCTable;
    }

    //Open eDCFC Modal to find activity
    function openModalFindEDCFC(findEDCFCModalCustomOptions) {
        var idModal = _createCustomID();
        var idTableEDCFCActivitiesFound = "tblSelectEDCFC_" + idModal;

        //Default Options.
        var findEDCFCModalOptions = {
            onSelectEDCFC: null
        };

        //Hacer un merge con las opciones que nos enviaron y las default.
        $.extend(true, findEDCFCModalOptions, findEDCFCModalCustomOptions);

        var htmlContentModal =
            '<div class="row" style="height: 400px;"> ' +

                //Select By Node ID
            '   <div class="col-md-2"> ' +
            '       <div class="form-group"> ' +
            '           <label class="col-md-12 control-label"> ' +
            '               <i class="fa fa-angle-double-right"></i> Node ID ' +
            '           </label> ' +
            '           <div class="col-md-12"> ' +
            '               <input class="form-control txtNodeID" name="txtNodeID" placeholder="e. g. 2344" /> ' +
            '           </div> ' +
            '       </div> ' +
            '    </div> ' +

                //By Description
            '   <div class="col-md-5"> ' +
            '       <div class="form-group"> ' +
            '           <label class="col-md-12 control-label"> ' +
            '               <i class="fa fa-angle-double-right"></i> Description ' +
            '           </label> ' +
            '           <div class="col-md-12"> ' +
            '               <div class="form-control txtEDCFCDescription" placeholder="e. g. CRA Daily Close Calls" contenteditable="true" id="txtEDCFCDescription" name="txtEDCFCDescription"></div>' +
            '           </div> ' +
            '       </div> ' +
            '   </div> ' +

                //Select By Maker SOEID
            '   <div class="col-md-4"> ' +    
            '       <div class="form-group"> ' +
            '           <label class="col-md-12 control-label"> ' +
            '               <i class="fa fa-angle-double-right"></i> eDCFC Maker User ' +
            '           </label> ' +
            '           <div class="col-md-12"> ' +
            '               <div class="selUserToGetEDCFCNodes" id="selUserToGetEDCFCNodes_' + idModal + '" ></div> ' +
            '           </div> ' +
            '       </div> ' +
            '   </div> ' +

            
                //Find Button
            '    <div class="col-md-1"> ' +
            '        <button type="button" class="btnFindEDCFCs btn btn-primary pull-right"><i class="fa fa-search"></i> Find</button> ' +
            '    </div> ' +
            '    <div class="col-md-12" style="padding-top: 15px;"> ' +
            '        <div id="' + idTableEDCFCActivitiesFound + '"></div> ' +
            '    </div> ' +
            '</div> ';

        //Show user modal
        _showModal({
            modalId: "modalSelEDCFC",
            width: "95%",
            addCloseButton: true,
            buttons: [{
                name: "<i class='fa fa-crosshairs'></i> Select",
                class: "btn-success",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    var selectedRow = $.jqxGridApi.getOneSelectedRow("#" + idTableEDCFCActivitiesFound, true);
                    if (selectedRow) {
                        findEDCFCModalOptions.onSelectEDCFC(selectedRow);

                        //Close Modal
                        $modal.find(".close").click();
                    }
                }
            }],
            title: "Find eDCFC Activity",
            contentHtml: htmlContentModal,
            onReady: function ($modal) {
                //Load select of user to get managed segment
                _createSelectSOEID({
                    id: "#" + $modal.find(".selUserToGetEDCFCNodes").attr("id"),
                    selSOEID: _getSOEID(),
                    onReady: function () {
                        //Load table 
                        loadTable($modal);
                    }
                });

                //Load place holder of node description
                _loadDivContentEditablePlaceholders();

                //On click Find EDCFCs
                $modal.find(".btnFindEDCFCs").click(function () {
                    //Load table 
                    loadTable($modal);
                });
            }
        });

        function loadTable($modal) {
            var selectedSOEID = $modal.find(".selUserToGetEDCFCNodes select").val();
            var nodeID = $modal.find(".txtNodeID").val();
            var nodeDesc = _getDivEditableValue($modal.find(".txtEDCFCDescription"));
            var sql = "";

            if (selectedSOEID || nodeID || nodeDesc) {
                var action = "Get Activities by Maker SOEID";
                if (nodeID) {
                    action = "Get Activities by Node ID";
                } else {
                    if (nodeDesc) {
                        action = "Get Activities by Node Name";
                    } 
                }

                _callProcedure({
                    loadingMsgType: "topBar",
                    loadingMsg: "Loading eDCFC Activities...",
                    name: "[dbo].[spPCTManageEDCFC]",
                    params: [
                        { "Name": "@Action", "Value": action },
                        { "Name": "@eDCFCUserSOEID", "Value": selectedSOEID },
                        { "Name": "@NodeDesc", "Value": nodeDesc },
                        { "Name": "@NodeID", "Value": nodeID }
                    ],
                    success: {
                        fn: function (resultList) {
                            $.jqxGridApi.create({
                                showTo: "#" + idTableEDCFCActivitiesFound,
                                options: {
                                    height: "350",
                                    autoheight: false,
                                    autorowheight: false,
                                    selectionmode: "singlerow",
                                    showfilterrow: true,
                                    sortable: true,
                                    editable: true,
                                    groupable: false
                                },
                                source: {
                                    // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                                    dataBinding: "Large Data Set Local",
                                    rows: resultList
                                },
                                columns: [
                                    //type: string - text - number - int - float - date - time 
                                    //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                                    //cellsformat: ddd, MMM dd, yyyy h:mm tt
                                    { name: 'NODE_ID', text: 'Node ID', width: '8%', type: 'number', filtertype: 'number' },
                                    { name: 'GOC', text: 'GOC', width: '9%', type: 'string', filtertype: 'input' },
                                    { name: 'GOC_DESCRIPTION', text: 'GOC Desc', width: '29%', type: 'string', filtertype: 'checkedlist' },
                                    { name: 'NODE_NAME', text: 'eDCFC Activity Name', width: '54%', type: 'string', filtertype: 'input' }
                                ],
                                ready: function () { }
                            });
                        }
                    }
                });
            } else {
                _showAlert({
                    showTo: $modal.find(".modal-body"),
                    type: 'error',
                    title: "Message",
                    content: "Please select Node ID, Description or Maker User."
                });
            }
        }
    }
}

function _showModal(customOptions) {
    //Creation example
    //_showModal({
    //      modalId: "modalFormNewItem",
    //      width: "98%",
    //      buttons: [{
    //          name: "Confirm",
    //          class: "btn-danger",
    //          closeModalOnClick: true,
    //          onClick: function ($modal) {
    //              console.log($modal);
    //          }
    //      }],
    //      addCloseButton: true,
    //      title: "Modal Tittle",
    //      contentHtml: "Body goes here...",
    //      contentAjaxUrl: "",
    //      contentAjaxParams: {},
    //      contentAjaxType: "GET",
    //      loadingMsg: "Getting form information...",
    //      loadingMsgType: "fullLoading" // fullLoading : notification : topBar : alert,
    //      onReady: function(){},
    //      onClose: function(){}
    //});

    //Default Options.
    var options = {
        width: "98%",
        buttons: [],
        addCloseButton: false,
        title: "Modal Tittle",
        contentHtml: "Body goes here...",
        contentAjaxUrl: "",
        contentAjaxParams: {},
        contentAjaxType: "GET",
        loadingMsg: "Loading...",
        loadingMsgType: "fullLoading"
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    function createModal(options) {
        var idModal = options["modalId"] ? options["modalId"] : _createCustomID();
        var idContainerModal = "containerModal-" + idModal;
        var idBtnOpenModal = "btnOpenModal-" + idModal;
        var idTempLinkOpenModal = "btnOpenModal-" + idModal;
        var classCloseModal = "closeModal-" + idModal;
        var idBtnConfirmModal = "btnConfirmModal-" + idModal;
        var htmlModal =
            '<div id="' + idContainerModal + '" > ' +
            '    <a class="btn btn-danger" id="' + idTempLinkOpenModal + '" data-toggle="modal" data-backdrop="static" data-keyboard="false" href="#modal-' + idModal + '" style="display:none;">Modal ' + idModal + '</a> ' +
            '    <div class="modal fade col-xs-12" id="modal-' + idModal + '" role="dialog" aria-hidden="true" > ' +
            '        <div class="modal-dialog animated bounceInDown" style="width: ' + options.width + ';"> ' +
            '            <div class="modal-content"> ' +
            '                <div class="modal-header"> ' +
            '                    <button type="button" class="close ' + classCloseModal + '" data-dismiss="modal" aria-hidden="true">×</button> ' +
            '                    <h4 class="modal-title">:pmodalTitle</h4> ' +
            '                </div> ' +
            '                <div class="modal-body">:pmodalBody <div style="clear:both;"></div></div> ' +
            '                <div class="modal-footer"> ';

        //Add default close modal button
        if (options.addCloseButton) {
            htmlModal += '      <button data-dismiss="modal" class="btn btn-default ' + classCloseModal + '" type="button">Close</button>';
        }

        //Create buttons
        for (var i = 0; i < options.buttons.length; i++) {
            var button = options.buttons[i];

            //Set close modal on click button default
            if (typeof button.closeModalOnClick == "undefined") {
                button.closeModalOnClick = true;
            }

            button.id = idModal + "_btn_" + i;
            htmlModal += '      <button id="' + button.id + '" ' + (button.closeModalOnClick ? 'data-dismiss="modal"' : '') + ' class="btn ' + button['class'] + ' ' + (button.closeModalOnClick ? classCloseModal : '') + '" type="button">' + button.name + '</button>';
        }

        htmlModal +=
            '                </div> ' +
            '            </div> ' +
            '        </div> ' +
            '    </div> ' +
            '</div> ';

        //Crear el mensaje.
        htmlModal = htmlModal.replace(":pmodalTitle", options["title"]);
        htmlModal = htmlModal.replace(":pmodalBody", options["contentHtml"]);

        //Remove if exists modal
        $("#" + idContainerModal).remove();

        //Show confirmation modal.
        var $modal = $(htmlModal);
        $('body').append($modal);

        //On Open Modal
        $("#" + idBtnOpenModal).click(function () {
            if (options["onReady"]) {
                options["onReady"]($modal.find('.modal-content'));

            }

            //Fix zindex with headers and select2
            //$(".utility.navbar").css("z-index", 300);
            //$(".page-topbar").css("z-index", 300);
            //$(".select2-container").css("z-index", 1);

            //console.log('opening modal #' + idContainerModal);
        });

        //On click buttons
        for (var i = 0; i < options.buttons.length; i++) {
            addEventButton(i);
        }

        //Add event to button
        function addEventButton(index) {
            //Create new reference by each index to send in the function
            var tempIndex = index;

            $("#" + options.buttons[tempIndex].id).click(function () {
                var $btn = $(this);
                var closeModal;

                if (options.buttons[tempIndex].onClick) {
                    closeModal = options.buttons[tempIndex].onClick($modal.find('.modal-content'));
                }

                if (typeof closeModal == "undefined") {
                    closeModal = true;
                }

                if (closeModal) {
                    $btn.removeClass("avoidCloseModal");
                } else {
                    $btn.addClass("avoidCloseModal");
                }

                //console.log('confirm modal #' + idContainerModal);
                //$modal.remove();
            });
        };

        //On Close Modal
        $("." + classCloseModal).click(function () {
            var $btn = $(this);

            //On click buttons that need to close modal, ignore when event return true to avoid close the modal.
            if (!$btn.hasClass("avoidCloseModal")) {
                if (options["onClose"]) {
                    options["onClose"]($modal.find('.modal-content'));
                }

                //console.log('closing modal #' + idContainerModal);
                $modal.remove();
                var cantBackdrops = $(".modal-backdrop").length;

                if (cantBackdrops == 1) {
                    $(".modal-backdrop").remove();

                    //Fix boostrap modal scroll error
                    $("body").removeClass("modal-open");

                    //Fix zindex with headers and select2
                    //$(".utility.navbar").css("z-index", 30000);
                    //$(".page-topbar").css("z-index", 30000);
                    //$(".select2-container").css("z-index", 11040);
                } else {
                    //Remove last
                    $($(".modal-backdrop")[cantBackdrops - 1]).remove();
                }
            } else {
                //Prevent data-dismiss="modal"
                return false;
            }
        });

        $("#" + idBtnOpenModal).click();
    }

    //Validate if we need to load an url
    if (options.contentAjaxUrl) {
        _callServer({
            loadingMsgType: options.loadingMsgType,
            loadingMsg: options.loadingMsg,
            url: options.contentAjaxUrl,
            data: options.contentAjaxParams,
            type: options.contentAjaxType,
            success: function (response) {
                options.contentHtml = response;
                createModal(options);
            }
        });
    } else {
        createModal(options);
    }
}

function _classNestableList(customOptions) {
    //_classNestableList({
    //    id: "#content-list-permits",
    //    maxDepth: 1,
    //    buttons: {
    //        addNew: {
    //            //Autogenerate [ID] columns
    //            identity: true
    //        }
    //    },
    //    onSave: function (newPermits) {
    //        saveData("Permits", newPermits);

    //        //Refresh Permits in "Add Permits to Role"
    //        classTabInitConfig.objTabRoles.objRoleXPermits.loadPermits();
    //    },
    //    items: permits,

    //    //type: {string} {input-icon} {radio}
    //    columns: [
    //        { type: 'string', fieldLabel: 'Category', fieldKey: 'Category', fieldPlaceHolder: 'eg. Main', valueTemplate: ':NumOrder - :Category', showInList: true },
    //        { type: 'input-icon', fieldLabel: 'Icon',     fieldKey: 'ClassIcon', fieldPlaceHolder: 'eg. fa-calendar', showInList: false },
    //        { type: 'string', fieldLabel: 'Name', fieldKey: 'Name', fieldPlaceHolder: 'eg. EUC Home', valueTemplate: "<i class='fa :ClassIcon'></i> _cropWord(:Name, 15)", showInList: true },
    //        { type: 'string', fieldLabel: 'Path',     fieldKey: 'Path', fieldPlaceHolder: 'eg. /EUCInventory/', showInList: true }
    //    ]
    //});

    var classNestableList = this;

    //Default Options.
    this.options = {
        maxDepth: 3
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, this.options, customOptions);

    //Buttons Content
    this.$contentButtons = $('<div class="col-xs-12"></div>');

    //Nestable List Content
    this.$contentList = $('<div class="col-xs-12"></div>');
    this.$ukNestable = $('<ul class="uk-nestable list-nestable" data-uk-nestable="{maxDepth:' + this.options.maxDepth + '}"></ul>');
    this.$contentList.append(this.$ukNestable);

    //Nestable Content
    this.$contentNestable = $(this.options.id);
    this.$contentNestable.append(this.$contentButtons);
    this.$contentNestable.append(this.$contentList);
    this.$contentNestable.append($('<div style="clear:both;"></div>'));

    //On Click Add Button
    if (classNestableList.options.buttons.addNew) {
        this.$btnNew = $('<button type="button" class="btnAddNew btn btn-success top15 left15 bottom15 pull-right"><i class="fa fa-asterisk"></i> Add</button>');
        this.$contentButtons.append(this.$btnNew);

        this.$btnNew.click(function () {

            var objItemToEdit = {};

            for (var i = 0; i < classNestableList.options.columns.length; i++) {
                var objColumn = classNestableList.options.columns[i];

                if (objColumn.showInForm) {
                    objItemToEdit[objColumn.fieldKey] = "";
                }
            }

            classNestableList.createFormItem({
                objItem: objItemToEdit,
                onSave: function (objNewObj) {
                    if (classNestableList.options.buttons.addNew.identity) {
                        //Set ID
                        var maximum = 0;
                        $(classNestableList.options.items).each(function () {
                            var value = parseFloat(this.ID);
                            maximum = (value > maximum) ? value : maximum;
                        });
                        objNewObj.ID = maximum + 1;
                    }

                    //Set Parent ID
                    objNewObj.ParentID = "";

                    //Set NumOrder
                    var numOrder = 1;
                    if (classNestableList.options.items.length > 0) {
                        numOrder = parseFloat(classNestableList.options.items[classNestableList.options.items.length - 1].NumOrder) + 1;
                    }
                    objNewObj.NumOrder = numOrder;

                    //Add New Item
                    classNestableList.options.items.push(objNewObj);

                    //Save updates
                    classNestableList.saveItems(classNestableList.options.items, "Insert", objNewObj);

                    //Reload list of Items
                    classNestableList.loadItemsList();
                }
            });
        });
    }

    //On click Load Data from DB
    if (classNestableList.options.buttons.importDB) {
        this.$btnImport = $('<button type="button" class="btnLoadReportDB btn btn-primary top15 left15 bottom15 pull-right" ><i class="fa fa-asterisk"></i> Load from DB</button>');
        this.$contentButtons.append(this.$btnImport);

        this.$btnImport.click(function () {
            var columnsToImport = [];

            for (var i = 0; i < classNestableList.options.columns.length; i++) {
                var objColumn = classNestableList.options.columns[i];

                columnsToImport.push({
                    to: objColumn.fieldKey,
                    value: (objColumn.importValue ? objColumn.importValue : null),
                    default: (objColumn.importDefault ? objColumn.importDefault : null)
                });
            }

            classNestableList.importData({
                dbObjectName: classNestableList.options.entity,
                columns: columnsToImport,
                onSuccess: function (newItems) {
                    //Set new data
                    classNestableList.options.items = newItems;

                    //Order Items by orden number
                    classNestableList.options.items.sort(_compareNumOrder);

                    //Reload list of Items to show new Num Order
                    classNestableList.loadItemsList();

                    //Save updates
                    classNestableList.saveItems(classNestableList.options.items);
                }
            });
        });
    }

    //On click Insert Data to DB
    $(".btnInsertItemDB").click(function () {
        console.log(classNestableList.generateInsert());
    });

    // Public functions (shared across instances)
    this.generateInsert = function () {
        return classNestableList.options.onGenerateInserts();
    };

    this.loadItemsList = function (newItems) {
        if (newItems) {
            classNestableList.options.items = newItems;
        }

        //Remove old data
        classNestableList.$ukNestable.contents().remove();

        //Replace value template with values
        function replaceValueTemplate(strTemplate, objItem) {

            //Replace values from Item
            for (var i = 0; i < classNestableList.options.columns.length; i++) {
                var objColumn = classNestableList.options.columns[i];
                var keyToFind = ":" + objColumn.fieldKey;

                if (_contains(strTemplate, keyToFind)) {
                    strTemplate = _replaceAll(keyToFind, objItem[objColumn.fieldKey], strTemplate);
                }
            }

            //Replace _cropWord function
            if (_contains(strTemplate, "_cropWord(")) {
                var startIndex = strTemplate.indexOf("_cropWord(");
                var endIndex = startIndex + ("_cropWord(".length);

                //find ")"
                var endFnIndex = 0;
                for (var i = endIndex; i < strTemplate.length; i++) {
                    if (strTemplate.charAt(i) == ")") {
                        endFnIndex = i;
                        break;
                    }
                }

                //To replace
                var toReplace = strTemplate.substr(startIndex, endFnIndex + 1);

                //get number
                var number = strTemplate.substr((endFnIndex - 2), 2);

                //get text
                var text = strTemplate.substr(endIndex, ((endFnIndex - 4) - (endIndex)));

                //console.log(toReplace, number, text);

                //Replace Text
                strTemplate = _replaceAll(toReplace, _cropWord(text, number), strTemplate);
            }

            return strTemplate;
        }

        //Get Html Item Recursive
        function getHtmlItem(objItem) {
            var childsItems = [];

            if (objItem.ID) {
                childsItems = _findAllObjByProperty(classNestableList.options.items, "ParentID", objItem.ID);
            }

            var classParent = "";

            if (childsItems.length > 0) {
                classParent = "uk-parent";
            }

            var htmlColumnsItem = "";

            for (var i = 0; i < classNestableList.options.columns.length; i++) {
                var objColumn = classNestableList.options.columns[i];

                if (objColumn.showInList) {
                    if (objColumn.valueTemplate) {
                        //":NumOrder - :Category";
                        //"<i class='fa :ClassIcon'></i> _cropWord(:Name, 15)"
                        var valueTemplate = objColumn.valueTemplate;
                        valueTemplate = replaceValueTemplate(valueTemplate, objItem);

                        htmlColumnsItem +=
                        '<div style="float: left; width: ' + objColumn.width + ';"> ' +
                        '    ' + valueTemplate +
                        '</div> ';
                    } else {
                        if (objColumn.fieldKey == "FlagIsGlobal" && typeof objItem.FlagIsGlobal != 'undefined') {
                            htmlColumnsItem +=
                            '<div style="float: left; width: 50px;"> ' +
                            '    ' + (objItem.FlagIsGlobal == "1" ? '<i class="fa fa-globe"></i>' : '') +
                            '</div> ';
                        } else {
                            htmlColumnsItem +=
                            '<div style="float: left; width: ' + objColumn.width + ';">' + objItem[objColumn.fieldKey] + '</div> ';
                        }
                    }
                }
            }
            //console.log(htmlColumnsItem);
            var $liItem = $(
                '<li class="uk-nestable-list-item ' + classParent + '" item-id="' + objItem.ID + '"> ' +
                '   <div class="uk-nestable-item"> ' +
                '       <div class="uk-nestable-handle"></div> ' +
                '       <div data-nestable-action="toggle"></div> ' +
                '       <div class="list-label"> ' + htmlColumnsItem + ' </div>  ' +
                '       <span style="float: right; padding: 6px;"> ' +
                '           <a href="#" class="color-primary link-underlined margin-right-5 btn-view" item-id="' + objItem.ID + '"> ' +
                '               <i class="glyphicon glyphicon-pencil"></i> Edit ' +
                '           </a> ' +
                '           <a href="#" class="color-danger link-underlined btn-delete" item-id="' + objItem.ID + '"> ' +
                '               <i class="glyphicon glyphicon-remove"></i> Delete ' +
                '           </a> ' +
                '       </span> ' +
                '   </div>  ' +
                '</li> '
            );

            //On Click View Item
            var $btnView = $liItem.find(".btn-view");
            $btnView.click(function (e) {
                e.preventDefault();

                var $btn = $(this);
                var objItem = _findOneObjByProperty(classNestableList.options.items, "ID", $btn.attr("item-id"));

                classNestableList.createFormItem({
                    objItem: objItem,
                    onSave: function (objUpdatedItem) {
                        //Merge objUpdatedItem into objItem recursive
                        $.extend(true, objItem, objUpdatedItem);

                        //Save updates
                        classNestableList.saveItems(classNestableList.options.items, "Edit", objItem);

                        //Reload list of Items
                        classNestableList.loadItemsList();
                    }
                });
            });

            //On Click Delete Item
            var $btnDelete = $liItem.find(".btn-delete");
            $btnDelete.click(function (e) {
                e.preventDefault();

                var $btn = $(this);
                var objItem = _findOneObjByProperty(classNestableList.options.items, "ID", $btn.attr("item-id"));

                var htmlContentToDelete = '<i class="fa ' + objItem.ClassIcon + '"></i> ' + objItem.Name;

                _showModal({
                    width: '35%',
                    modalId: "modalDelItem",
                    addCloseButton: true,
                    buttons: [{
                        name: "Delete",
                        class: "btn-danger",
                        onClick: function () {
                            var indexToDelete = classNestableList.options.items.indexOf(objItem);
                            classNestableList.options.items.splice(indexToDelete, 1);

                            //Reload list without deleted item
                            classNestableList.loadItemsList();

                            //Calculate new order
                            var $liItems = classNestableList.$ukNestable.children();
                            classNestableList.calculateParentAndOrder($liItems);

                            //Reload list of Items to show new Num Order
                            classNestableList.loadItemsList();

                            //Save updates
                            classNestableList.saveItems(classNestableList.options.items, "Delete", objItem);
                        }
                    }],
                    title: "Are you sure you want to delete this row?",
                    contentHtml: htmlContentToDelete
                });
            });

            if (childsItems.length > 0) {
                var $ulChilds = $('<ul class="uk-nestable-list"></ul>');
                for (var i = 0; i < childsItems.length; i++) {
                    $ulChilds.append(getHtmlItem(childsItems[i]));
                }
                $liItem.append($ulChilds);
            }

            return $liItem;
        }

        //Load list of Items
        classNestableList.options.items.sort(_compareNumOrder);
        if (classNestableList.options.items.length > 0) {
            for (var i = 0; i < classNestableList.options.items.length; i++) {
                var objItem = classNestableList.options.items[i];

                //Load only root elements, child are loaded recursive in the function getHtmlItem
                if (!objItem.ParentID) {
                    var $li = getHtmlItem(objItem);
                    classNestableList.$ukNestable.append($li);
                }
            }

            //Add on change event
            if (!_hasEvent(classNestableList.$ukNestable, "change")) {
                classNestableList.$ukNestable.on('change.uk.nestable', function () {
                    var $ulList = $(this);

                    var $liItems = $ulList.children();
                    classNestableList.calculateParentAndOrder($liItems);

                    //Order Items by orden number
                    classNestableList.options.items.sort(_compareNumOrder);

                    //Save updates
                    classNestableList.saveItems(classNestableList.options.items, "Reorder");

                    //Reload list of Items
                    classNestableList.loadItemsList();
                });
            }

            //Init nestable
            UIkit.nestable(classNestableList.$ukNestable, {
                maxDepth: classNestableList.options.maxDepth
            });

            //Load tooltips
            _GLOBAL_SETTINGS.tooltipsPopovers();
        } else {
            _showNotification("error", "Initial Configuration for " + classNestableList.options.entity + " is empty");
        }
    };

    this.calculateParentAndOrder = function ($liItems, objParent) {
        for (var i = 0; i < $liItems.length; i++) {
            var $liItem = $($liItems[i]);
            var objItem = _findOneObjByProperty(classNestableList.options.items, "ID", $liItem.attr("item-id"));

            //Set order number
            if (objParent) {
                objItem.ParentID = objParent.ID;

                //Level 1 eg: 3
                //Level 2 eg: 3.1
                //Level 3 eg: 3.11
                //Level 3 eg: 3.111
                if (_isInt(objParent.NumOrder)) {
                    objItem.NumOrder = objParent.NumOrder + "." + ($liItem.index() + 1);
                } else {
                    objItem.NumOrder = objParent.NumOrder.toString() + ($liItem.index() + 1).toString();
                }
            } else {
                objItem.ParentID = "";
                objItem.NumOrder = ($liItem.index() + 1);
            }

            //Save as string
            objItem.NumOrder = objItem.NumOrder.toString();

            //Validate if is a parent
            if ($liItem.hasClass("uk-parent")) {
                var $liChildItems = $liItem.children(".uk-nestable-list").children();
                classNestableList.calculateParentAndOrder($liChildItems, objItem);
            }
        }
    };

    this.createFormItem = function (createFormOptions) {
        var htmlFields = "";

        for (var i = 0; i < classNestableList.options.columns.length; i++) {
            var objColumn = classNestableList.options.columns[i];

            if (objColumn.showInForm) {
                //Set default Name
                if (!objColumn.fieldLabel) {
                    objColumn.fieldLabel = objColumn.fieldKey;
                }

                switch (objColumn.type) {
                    case "string":
                        htmlFields +=
                            '<div class="form-group"> ' +
                            '    <label class="col-md-4 control-label"> ' +
                            '        <i class="fa fa-asterisk iconasterisk"></i> ' + objColumn.fieldLabel + ' ' +
                            '    </label> ' +
                            '    <div class="col-md-8"> ' +
                            '       <input type="text" class="field-' + objColumn.fieldKey + ' form-control" value="' + createFormOptions.objItem[objColumn.fieldKey] + '" placeholder="' + objColumn.fieldPlaceHolder + '"> ' +
                            '    </div> ' +
                            '</div> ';
                        break;

                    case "textarea":
                        htmlFields +=
                            '<div class="form-group"> ' +
                            '    <label class="col-md-4 control-label"> ' +
                            '        <i class="fa fa-asterisk iconasterisk"></i> ' + objColumn.fieldLabel + ' ' +
                            '    </label> ' +
                            '    <div class="col-md-8"> ' +
                            '       <textarea rows="4" class="field-' + objColumn.fieldKey + ' form-control" placeholder="' + objColumn.fieldPlaceHolder + '">' + createFormOptions.objItem[objColumn.fieldKey] + '</textarea>' +
                            '    </div> ' +
                            '</div> ';
                        break;

                    case "input-icon":
                        htmlFields +=
                            '<div class="form-group"> ' +
                            '    <label class="col-md-4 control-label"> ' +
                            '        <i class="fa fa-asterisk iconasterisk"></i> ' + objColumn.fieldLabel + ' ' +
                            '    </label> ' +
                            '    <div class="col-md-8"> ' +
                            '       <div class="input-group"> ' +
                            '           <span class="input-group-addon"> ' +
                            '               <span class="arrow"></span> ' +
                            '               <i class="fa ' + createFormOptions.objItem[objColumn.fieldKey] + '"></i>     ' +
                            '           </span> ' +
                            '           <input type="text" class="field-' + objColumn.fieldKey + ' form-control" value="' + createFormOptions.objItem[objColumn.fieldKey] + '" placeholder="' + objColumn.fieldPlaceHolder + '"> ' +
                            '       </div> ' +
                            '       <a href="https://fontawesome.com/v4.7.0/icons/" target="_blank">View Icons List</a> ' +
                            '    </div> ' +
                            '</div> ';
                        break;

                    case "radio":
                        htmlFields +=
                            '<div class="form-group"> ' +
                            '    <label class="col-md-4 control-label"> ' +
                            '        <i class="fa fa-asterisk iconasterisk"></i> ' + objColumn.fieldLabel + ' ' +
                            '    </label> ' +
                            '    <div class="col-md-8"> ' +
                            '       <input value="1" type="radio" name="field-' + objColumn.fieldKey + '" class="rbtYes skin-square-green" ' + ((createFormOptions.objItem[objColumn.fieldKey] == "1") ? "checked" : "") + ' > ' +
                            '       <label class="iradio-label form-label hover" style="margin-right: 25px;">Yes </label> ' +
                            '       <input value="0" type="radio" name="field-' + objColumn.fieldKey + '" class="rbtNo skin-square-green" ' + ((createFormOptions.objItem[objColumn.fieldKey] == "0") ? "checked" : "") + ' > ' +
                            '       <label class="iradio-label form-label hover" style="margin-right: 25px;">No </label> ' +
                            '    </div> ' +
                            '</div> ';
                        break;
                }
            }
        }

        var formHtml =
            '<div class="form-horizontal"> ' +
            '    <div class="col-md-10"> ' + htmlFields + ' </div> ' +
            '    <div style="clear: both;"></div> ' +
            '</div> ';

        _showModal({
            width: "75%",
            title: classNestableList.options.entity + " Information",
            contentHtml: formHtml,
            buttons: [{
                name: "Save",
                class: "btn-success",
                closeModalOnClick: true,
                onClick: function ($modal) {
                    var objUpdated = {};

                    for (var i = 0; i < classNestableList.options.columns.length; i++) {
                        var objColumn = classNestableList.options.columns[i];
                        switch (objColumn.type) {
                            case "string":
                                objUpdated[objColumn.fieldKey] = $modal.find(".field-" + objColumn.fieldKey).val();
                                break;

                            case "textarea":
                                objUpdated[objColumn.fieldKey] = $modal.find(".field-" + objColumn.fieldKey).val();
                                break;

                            case "input-icon":
                                objUpdated[objColumn.fieldKey] = $modal.find(".field-" + objColumn.fieldKey).val();
                                break;

                            case "radio":
                                objUpdated[objColumn.fieldKey] = $modal.find('[name="field-' + objColumn.fieldKey + '"]:checked').val();
                                break;
                        }
                    }

                    if (createFormOptions.onSave) {
                        createFormOptions.onSave(objUpdated);
                    }
                }
            }],
            addCloseButton: true,
            onReady: function () {
                _GLOBAL_SETTINGS.iCheck();
            }
        });
    };

    this.saveItems = function (items, action, objItem) {

        if (classNestableList.options.onSave) {
            classNestableList.options.onSave(items, action, objItem);
        }

    };

    this.importData = function (options) {
        //options = {
        //    dbObjectName: "Permit",
        //    columns: [
        //        { to: "ID"},
        //        { to: "NumOrder", from: "NUMBER_ORDER2"},
        //        { to: "ParentID"},
        //        { to: "ClassIcon", default: "fa-dashboard"},
        //        { to: "FlagIsGlobal", value: "0"}
        //    ],
        //    onSuccess: function(permits){}
        //};

        if (_getViewVar("DBIsConfigured") == "1") {
            var htmlContentModal = 'Are you sure you want to import the data from Database?';

            _showModal({
                width: '35%',
                modalId: "modalImportData",
                addCloseButton: true,
                buttons: [{
                    name: "Import",
                    class: "btn-danger",
                    onClick: function () {
                        var tblFullName = _getDBObjectFullName(options.dbObjectName);

                        _callServer({
                            loadingMsg: "Loading data for '" + tblFullName + "'...",
                            url: '/Configuration/AdminDBTable/List',
                            data: { "ptable": tblFullName },
                            success: function (responseList) {
                                if (responseList && responseList.length > 0) {
                                    //Create array to import data
                                    options.importTo = [];

                                    //Add row information by each row
                                    for (var i = 0; i < responseList.length; i++) {
                                        var tempRow = responseList[i];

                                        //Ignore Deleted Data
                                        if (tempRow.IsDeleted == 'False' || typeof tempRow.IsDeleted == 'undefined') {
                                            var newDataToImport = {};

                                            //Create new data
                                            for (var j = 0; j < options.columns.length; j++) {
                                                var objCol = options.columns[j];
                                                var colNameToImport = (objCol.from ? objCol.from : objCol.to);
                                                var valueToImport = (objCol.value ? objCol.value : tempRow[colNameToImport]);
                                                var defaultValue = "";

                                                if (typeof objCol.default != 'undefined') {
                                                    if (objCol.default == ":autonumeric") {
                                                        defaultValue = (i + 1);
                                                    } else {
                                                        defaultValue = objCol.default;
                                                    }
                                                }

                                                newDataToImport[objCol.to] = (typeof valueToImport != 'undefined' ? valueToImport : defaultValue);
                                            }

                                            //Add new data
                                            options.importTo.push(newDataToImport);
                                        }
                                    }

                                    //Reload list of Data to show new information
                                    if (options.onSuccess) {
                                        options.onSuccess(options.importTo);
                                    }
                                } else {
                                    _showNotification("error", "Table '" + tblFullName + "' is empty");
                                }
                            }
                        });
                    }
                }],
                title: "<i class='fa fa-warning'></i> Warning Message",
                contentHtml: htmlContentModal
            });
        } else {
            _showNotification("error", "Framework tables not found for " + _getUserInfo().DefaultAppKey);
        }

    }

    //Load List of Items
    this.loadItemsList();
}

function _loadModuleAccordion() {
    $('.mod-header .toggle-title').click(function () {
        var $el = $(this).parent().parent();

        if ($el.hasClass("collapsed")) {
            $el.removeClass("collapsed");
        } else {
            $el.addClass("collapsed");
        }
    });
}

function _getDivEditableValue($el) {
    var result = "";

    if ($el && $el.length > 0) {
        if ($el.attr("placeholder") != $el.text()) {
            result = $el.text();
        }
    }

    return result;
}

function _setDivEditableValue($el, pvalue) {
    if ($el && $el.length > 0) {
        //Set new value
        $el.html(pvalue);

        //Remove class of placeholder
        $el.removeClass("divPlaceholder");

        //Trigger jqueryvalidation
        $el.trigger("focusout");
    }
}

function _divEditableIsEmpty($el) {

    if ($el.text() == $el.attr("placeholder")) {
        return true;
    }

    if (!$el.text()) {
        return true;
    }

    return false;
}

function _loadDivContentEditablePlaceholders() {

    $("[contenteditable='true']").each(function () {
        var $el = $(this);

        if (!_hasEvent($el, "focusin")) {
            $("[contenteditable='true']").on("focusin", function (e) {
                validateDivValue($(this), "focusin");
            });
        }

        if (!_hasEvent($el, "focusout")) {
            $("[contenteditable='true']").on("focusout", function (e) {
                validateDivValue($(this), "focusout");
            });
        }

        validateDivValue($(this), "init");
    });

    function validateDivValue($el, type) {
        //Removing line breaks
        var divValue = $el.text();

        //if ($el.attr("allowenter")) {
        //    divValue = $el.html();

        //    //Remove copy and paste html styles in contenteditable e. g. <span style="color: rgb(213, 213, 213); font-family: Consolas">*CSS FRSS CR Re-engineering Team &lt;dl.fro.cr.are.team@imcla.lac.nsroot.net&gt;</span>
        //    divValue = _stripHTMLFromText(divValue);
        //} else {
        //    divValue = $el.text();
        //}
        
        //Escape special characters
        divValue = _replaceAll("<", "&lt;", divValue);
        divValue = _replaceAll(">", "&gt;", divValue);

        //Remove spaces
        divValue = $.trim(divValue);

        //Fix IE "<br>"
        divValue = _replaceAll("<br>", "", divValue);

        //Set cleaned value
        $el.html(divValue);

        //Get div Placeholder
        var divPlaceholder = $el.attr("placeholder");

        //Escape special characters
        divPlaceholder = _replaceAll("<", "&lt;", divPlaceholder);
        divPlaceholder = _replaceAll(">", "&gt;", divPlaceholder);

        //Remove placeholder from divValue
        if (divValue) {
            divValue = _replaceAll(divPlaceholder, "", divValue);
        }

        switch (type) {
            case "focusin":
                //Set value without placeholder
                $el.html(divValue);

                //Remove placeholder class
                $el.removeClass("divPlaceholder");
                break;

            case "focusout":
            case "init":
                //When is empty set placeholder
                if (!divValue) {

                    //Set placeholder
                    $el.html(divPlaceholder);

                    //add placeholder class
                    $el.addClass("divPlaceholder");
                }
                break;
        }
    }
}

function _generatePDFFromHtml(element, fileName, onReady) {
    //kendo.drawing.drawDOM($(element)).then(function(group) {
    //    kendo.drawing.pdf.saveAs(group, fileName + ".pdf");
    //});

    if (typeof kendo == "undefined") {
        _showNotification("info", "Generating PDF File...", "GeneratePDF");

        $.getScript(_getViewVar("SubAppPath") + "/Content/Shared/plugins/kendo/kendo.all.min.js", function (data, textStatus, jqxhr) {
            generatePDF();
            console.log("Kendo was loaded.");
        });
    } else {
        generatePDF();
    }

    function generatePDF() {
        // Convert the DOM element to a drawing using kendo.drawing.drawDOM
        kendo.drawing.drawDOM($(element))
            .then(function (group) {
                // Render the result as a PDF file
                return kendo.drawing.exportPDF(group, {
                    paperSize: "auto",
                    //paperSize: "A4",
                    margin: { left: "1cm", top: "1cm", right: "1cm", bottom: "1cm" }
                });
            })
            .done(function (data) {
                // Call onReady
                if (onReady) {
                    onReady();
                }

                //Show success message
                _showNotification("success", "PDF File was generated successfully.", "GeneratePDF");

                // Save the PDF file
                kendo.saveAs({
                    dataURI: data,
                    fileName: fileName + ".pdf"
                });
            });
    }
};

function _loadTimeZone(customOptions) {
    //Default Options.
    var options = {
        onSuccess: null,
        onChange: null
    };

    //Merge default options
    $.extend(true, options, customOptions);

    var sql =
        " SELECT " +
        "     T.[ID], " +
        "     T.[Abbreviation], " +
        "     T.[TimeZone], " +
        "     T.[UTCOffset], " +
        "     T.[UTCOffsetMinutes] " +
        " FROM " +
		"     [Automation].[dbo].[tblTimeZone] T ";

    _callServer({
        loadingMsgType: "topBar",
        loadingMsg: "Loading time zones...",
        url: '/Shared/ExecQuery',
        data: { 'pjsonSql': _toJSON(sql) },
        type: "post",
        success: function (resultList) {
            //Set Time Zones list
            _TIME_ZONES = resultList;

            //Remove current options
            $("#selTimeZone").contents().remove();

            //Add defaul option
            //$("#selTimeZone").append($('<option>', {
            //    value: 0,
            //    text: "-- Select --"
            //}));

            //Find current offset of user
            var browserOffsetMinutes = moment().utcOffset();

            //Add Options
            for (var i = 0; i < resultList.length; i++) {
                var objTemp = resultList[i];
                $("#selTimeZone").append($('<option>', {
                    value: objTemp.ID,
                    text: objTemp.TimeZone,
                    timeZone: objTemp.TimeZone,
                    utcOffset: objTemp.UTCOffset,
                    utcOffsetMinutes: objTemp.UTCOffsetMinutes,
                    selected: (objTemp.UTCOffsetMinutes == browserOffsetMinutes)
                }));
            }

            //On Click Time Zone Summary
            $("#timeZoneSummary").click(function () {
                //Hide Time Zone Summary
                $("#timeZoneSummary").hide();

                //Show Select
                $("#selTimeZone").show();

                //Show options
                var dropdownElement = document.getElementById("selTimeZone");
                var event;
                event = document.createEvent('MouseEvents');
                event.initMouseEvent('mousedown', true, true, window);
                dropdownElement.dispatchEvent(event);
            });

            //On Change Select of Time Zone
            $("#selTimeZone").change(function () {
                calculateTimeZoneSummary();
            });

            //Show Time Zone
            $("#containerTimeZone").show();

            //Load selected Time Zone
            calculateTimeZoneSummary();

            //Update Time Zone every minute
            setInterval(function () {

                //Load selected Time Zone
                calculateTimeZoneSummary(true);

            }, 1000); // 1000 milsec = 1sec
            //}, 60 * 1000); // 60 * 1000 milsec

            //On Success
            if (options.onSuccess) {
                options.onSuccess();
            }
        }
    });

    function calculateTimeZoneSummary(flagInterval) {
        var $selOption = $("#selTimeZone").find("option:selected");
        var timeZone = $selOption.attr("timeZone");
        var utcOffsetMinutes = $selOption.attr("utcOffsetMinutes");
        var selDateTime = moment.utc().add(parseInt(utcOffsetMinutes), 'minutes').format("dddd, MMM DD, YYYY hh:mm A");

        //Show Datetime
        $("#timeZoneSummary").html(timeZone + ' <i class="fa fa-clock-o"></i> ' + selDateTime + '  <i class="fa fa-pencil"></i>');

        if (!flagInterval) {
            //Hide Select
            $("#selTimeZone").hide();

            //Show Time Zone Summary
            $("#timeZoneSummary").show();
        }
    }
}

function _timeZoneConvertLocalToUTC(plocalDateTime, formatedDateStr) {
    if (typeof formatedDateStr == "undefined") {
        formatedDateStr = true;
    }

    var timeZoneInfo = _timeZoneGetSelectedInfo();
    var utcDateTime = moment.utc(plocalDateTime).add(parseInt(timeZoneInfo.UTCOffsetMinutes * -1), 'minutes');

    if (formatedDateStr) {
        return utcDateTime.format("YYYY-MM-DD HH:mm");
    } else {
        return utcDateTime;
    }
}

function _timeZoneConvertUTCToLocal(putcDateTime, formatedDateStr) {
    if (typeof formatedDateStr == "undefined") {
        formatedDateStr = true;
    }

    var timeZoneInfo = _timeZoneGetSelectedInfo();
    var localDateTime = moment(putcDateTime).add(parseInt(timeZoneInfo.UTCOffsetMinutes), 'minutes');

    if (formatedDateStr) {
        return localDateTime.format("YYYY-MM-DD HH:mm");
    } else {
        return localDateTime;
    }
}

function _timeZoneGetSelectedInfo() {
    var objTimeZone = _findOneObjByProperty(_TIME_ZONES, "ID", $("#selTimeZone").val());

    //Convert to int 
    objTimeZone.UTCOffsetMinutes = parseInt(objTimeZone.UTCOffsetMinutes);

    //Set current datetime for selected timezone
    objTimeZone.UTCCurrentDateTime = moment.utc();
    objTimeZone.UTCCurrentDateTimeFormat = objTimeZone.UTCCurrentDateTime.format("YYYY-MM-DD HH:mm");

    //Set current datetime for selected timezone
    objTimeZone.CurrentDateTime = moment(objTimeZone.UTCCurrentDateTimeFormat).add(parseInt(objTimeZone.UTCOffsetMinutes), 'minutes');
    objTimeZone.CurrentDateTimeFormat = objTimeZone.CurrentDateTime.format("YYYY-MM-DD HH:mm");

    return objTimeZone;
}

function _timeZoneGetUTCOffsetMinutes() {
    return _timeZoneGetSelectedInfo().UTCOffsetMinutes;
}

function _timeZoneGetCurrentDateTime() {
    return _timeZoneGetSelectedInfo().CurrentDateTimeFormat;
}

//----------------------------------------------------------------
//Framework Functions
//----------------------------------------------------------------
function _getSuperAdminPermits() {
    return [
        { ID: "-1", NumOrder: "100.01", ParentID: "", Path: "/Shared/SimulateUser", Category: "Super Admin", Name: "Simulate User", ClassIcon: "fa-gamepad" },
        { ID: "-2", NumOrder: "100.02", ParentID: "", Path: "/Configuration/AppManager", Category: "Super Admin", Name: "Apps Manager", ClassIcon: "fa-gears" },
        { ID: "-3", NumOrder: "100.03", ParentID: "", Path: "/Configuration/AppConfig", Category: "Super Admin", Name: "App Config", ClassIcon: "fa-gears" },
        { ID: "-4", NumOrder: "100.04", ParentID: "", Path: "/Configuration/DataBase", Category: "Super Admin", Name: "Server Info & DB", ClassIcon: "fa-wrench" },
        { ID: "-5", NumOrder: "100.05", ParentID: "", Path: "/Configuration/DBGlobalVars", Category: "Super Admin", Name: "DB Global Vars", ClassIcon: "fa-gears" },

        { ID: "-6", NumOrder: "100.06", ParentID: "", Path: "javascript:;", Category: "Super Admin", Name: "Security", ClassIcon: "fa-lock" },
        { ID: "-7", NumOrder: "100.07", ParentID: "-6", Path: "/Configuration/Permits", Category: "Super Admin", Name: "Permits", ClassIcon: "fa-sitemap" },
        { ID: "-8", NumOrder: "100.08", ParentID: "-6", Path: "/Configuration/Reports", Category: "Super Admin", Name: "Reports", ClassIcon: "fa-bar-chart" },
        { ID: "-9", NumOrder: "100.09", ParentID: "-6", Path: "/Configuration/Roles", Category: "Super Admin", Name: "Roles", ClassIcon: "fa-puzzle-piece" },
        { ID: "-10", NumOrder: "100.10", ParentID: "-6", Path: "/Configuration/Users", Category: "Super Admin", Name: "Users", ClassIcon: "fa-users" },

        { ID: "-11", NumOrder: "100.11", ParentID: "", Path: "javascript:;", Category: "Super Admin", Name: "Public Pages", ClassIcon: "fa-lock" },
        
        { ID: "-12", NumOrder: "100.12", ParentID: "-11", Path: "/AutomationFramework", Category: "Super Admin", Name: "Framework Dev Home", ClassIcon: "fa-sitemap" },
        { ID: "-12", NumOrder: "100.13", ParentID: "-11", Path: "/AutomationFramework/Dashboard", Category: "Super Admin", Name: "Framework User Dashboard", ClassIcon: "fa-sitemap" },
        { ID: "-14", NumOrder: "100.14", ParentID: "-11", Path: "/Error/AccessDenied", Category: "Super Admin", Name: "Access Denied", ClassIcon: "fa-sitemap" },
        { ID: "-15", NumOrder: "100.15", ParentID: "-11", Path: "/Error/Offline", Category: "Super Admin", Name: "Offline", ClassIcon: "fa-sitemap" },
        { ID: "-16", NumOrder: "100.16", ParentID: "-11", Path: "/Error/PageNotFound", Category: "Super Admin", Name: "Offline", ClassIcon: "fa-sitemap" },
        
        { ID: "-17", NumOrder: "100.17", ParentID: "", Path: "/Configuration/AdminException", Category: "Super Admin", Name: "Exceptions", ClassIcon: "fa-fire-extinguisher" },
        { ID: "-18", NumOrder: "100.18", ParentID: "", Path: "/Configuration/DLManager", Category: "Super Admin", Name: "DL Manager", ClassIcon: "fa-gears" },
        { ID: "-19", NumOrder: "100.19", ParentID: "", Path: "/Configuration/QueryManager", Category: "Super Admin", Name: "Query Manager", ClassIcon: "fa-fire-extinguisher" },
        { ID: "-20", NumOrder: "100.20", ParentID: "", Path: "/Configuration/EERSConfig", Category: "Super Admin", Name: "EERS Config", ClassIcon: "fa-gears" }
    ];
}

function _renderMenu(pcustomMenuList) {
    var DBIsConfigured = _getViewVar("DBIsConfigured");
    var userInfo = _getViewVar("UserInfo");
    var appKey = _getViewVar("AppKey");
    var permits = JSON.parse(userInfo.Permits);
    var roles = userInfo.Roles;
    var $ulUserMenu = $("#user-menu");
    var superAdminPermits = _getSuperAdminPermits();

    if (_contains(roles, "SUPER_ADMIN") || appKey == "AutomationFramework") {
        $.merge(permits, superAdminPermits);
    } else {
        if (_getViewVar("DBIsConfigured") == "0") {
            $.merge(permits, [
                { ID: "-2", NumOrder: "100.02", ParentID: "", Path: "/Configuration/AppManager?pappKey=" + appKey, Category: "Super Admin", Name: "Apps Manager", ClassIcon: "fa-gears" },
                { ID: "-3", NumOrder: "100.03", ParentID: "", Path: "/Configuration/AppConfig?pappKey=" + appKey, Category: "Super Admin", Name: "App Config", ClassIcon: "fa-gears" },
                { ID: "-4", NumOrder: "100.04", ParentID: "", Path: "/Configuration/DataBase?pappKey=" + appKey, Category: "Super Admin", Name: "Server Info & DB", ClassIcon: "fa-wrench" }
            ]);
        }
    }

    if (pcustomMenuList.length > 0) {
        //Filter only Permits with FlagIsGlobal = "1"
        $.merge(permits, _findAllObjByProperty(pcustomMenuList, "FlagIsGlobal", "1"));
    }

    //Order Menu by number
    permits.sort(_compareNumOrder);

    var lastCategorySection = "";
    var htmlTempOption;

    //Get Html Menu
    function getHtmlPermit(objPermit) {
        var childsPermits = _findAllObjByProperty(permits, "ParentID", objPermit.ID);

        var $liItem = $(
            '<li class=""> ' +
            '    <a href="' + _fixFrameworkURL(objPermit.Path) + '" category="' + objPermit.Category + '"> ' +
            '        <i class="fa ' + objPermit.ClassIcon + '"></i> ' +
            '        <span class="title">' + _cropWord(objPermit.Name, 21) + '</span> ' +
            '    </a> ' +
            '</li> '
        );

        if (childsPermits.length > 0) {
            var $a = $liItem.find("a[category]");
            //Add icon for child elements
            $a.append($('<span class="arrow"></span>'));

            //Remove href for parent Menu
            $a.attr("href", "javascript:;");

            //Add Class to indicate that is parent
            $a.addClass("menu-parent");

            //Add child menu
            var $ulChilds = $('<ul class="sub-menu" style="display: none;"></ul>');
            for (var i = 0; i < childsPermits.length; i++) {
                $ulChilds.append(getHtmlPermit(childsPermits[i]));
            }
            $liItem.append($ulChilds);
        }

        return $liItem;
    }

    //Render User Menu, based in permits
    for (var i = 0; i < permits.length; i++) {
        var objPermit = permits[i];
        htmlTempOption = "";

        //Add category
        if (lastCategorySection != objPermit.Category) {
            $ulUserMenu.append("<li class='menusection'>" + objPermit.Category + "</li>");
            lastCategorySection = objPermit.Category;
        }

        //Add Custom parameters
        if (objPermit.Path == "/CSSUniversity/Assessment") {
            objPermit.Path = "/CSSUniversity/Assessment?" + $.param({
                "pempsid": _encodeAsciiString(_getSOEID()),
                "pvo": "1",
                "pme": "1"
            });
        }

        //Add option only root elements, child are loaded recursive in the function getHtmlPermit
        if (!objPermit.ParentID) {
            $ulUserMenu.append(getHtmlPermit(objPermit));
        }
    }

    //When DB isn't configured render super admin configuration menu
    if (DBIsConfigured == "0") {
        permits = [{
            Path: "/",
            Category: "Main",
            Category: "Configuration",
            Name: "Apps Home",
            ClassIcon: "fa-dashboard"
        }, {
            Path: "/Configuration/AppManager",
            Category: "Configuration",
            Name: "Apps Manager",
            ClassIcon: "fa-gears"
        }];
    }

    //Get Current Page Title
    var $currentPage = $('li a[href="' + _fixFrameworkURL(window.location.pathname + window.location.search) + '"]');
    var classIcon = $currentPage.find("i").attr("class");
    var category = $currentPage.attr("category");
    var pageName = $currentPage.find("span").html();

    //Set Current Page Title
    if ($("#menu-permit-name").attr("isCustomTitle") != "1") {
        $("#menu-permit-name").html(pageName);
    }

    //Create Page Breadcrumb 
    var htmlBreadcrumb = "";
    if (category && pageName) {
        htmlBreadcrumb =
            '<li>' + category + '</li> ' +
            '<li class="active">' +
            '    <a href="' + window.location.pathname + '"><i class="' + classIcon + '"></i>' + pageName + '</a>' +
            '</li>';
    }
    $("#menu-breadcrumb").html(htmlBreadcrumb);

    //Load click sub-menu
    _GLOBAL_SETTINGS.mainMenu();

    //Set Active in left menu, and open childs
    var $currentPage = $('li a[href="' + _fixFrameworkURL(window.location.pathname + window.location.search) + '"]');
    $currentPage.addClass("active");
    $currentPage.closest("li").addClass("open");
    var $subMenu = $currentPage.closest(".sub-menu");
    var parent;
    var rootParent;

    if ($subMenu.length > 0) {
        parent = $subMenu.closest("li");

        $subMenu = $subMenu.parent().closest(".sub-menu");
        if ($subMenu.length > 0) {
            rootParent = $subMenu.closest("li");
        }
    }

    if (rootParent) {
        rootParent.children('a').click();
    }

    if (parent) {
        parent.children('a').click();
    }

    //Add stop Simulate user
    if (_getViewVar("SimulateUser")) {
        var $a = $('<a href="#"><i class="fa fa-id-badge"></i> Stop Simulate User</a>');
        $a.click(function (e) {
            e.preventDefault();

            var url = _getViewVar("SubAppPath") + "/" + _getViewVar("AppKey") + "/?" + $.param({
                pclear: 'true'
            });

            window.location.href = url;
        });
        $(".user-config-items").append($a);
    }
}

function _getCurrentAppInfo(onSuccess) {
    if (!_APPINFO) {
        _callServer({
            url: '/Configuration/AppManager/GetCurrentAppInfo',
            success: function (responseJsonStr) {
                _APPINFO = responseJsonStr;

                if (onSuccess) {
                    onSuccess(_APPINFO);
                }
            }
        });
    } else {
        onSuccess(_APPINFO);
    }
}

function _getCurrentAppData(onSuccess) {
    if (!_APPDATA) {
        _callServer({
            url: '/Configuration/AppManager/GetCurrentAppData',
            success: function (responseJsonStr) {
                _APPDATA = responseJsonStr;

                if (onSuccess) {
                    onSuccess(_APPDATA);
                }
            }
        });
    } else {
        onSuccess(_APPDATA);
    }
}

function _getAppsInfoList(onSuccess) {
    if (_APPSINFOLIST.length == 0) {
        _callServer({
            url: '/Configuration/AppManager/AppInfoList',
            success: function (responseJsonStr) {
                _APPSINFOLIST = responseJsonStr;

                if (onSuccess) {
                    onSuccess(_APPSINFOLIST);
                }
            }
        });
    } else {
        onSuccess(_APPSINFOLIST);
    }
}

function _setPageTitle(ptext) {
    $("#menu-permit-name").attr("isCustomTitle", "1");
    $("#menu-permit-name").html(ptext);
}

function _fixFrameworkURL(purl) {
    //Validate url
    if (purl == null) {
        purl = "";
    }

    //Add Default App Key e.g /HMT2/Reports?pappKey=HMT2
    if (!_contains(purl, _getViewVar("AppKey"))) {
        if (_contains(purl, "?")) {
            purl += "&pappKey=" + _getViewVar("AppKey");
        } else {
            purl += "?pappKey=" + _getViewVar("AppKey");
        }
    }

    //Add Sub Application
    if (_getViewVar("SubAppPath")) {
        if (purl && purl[0] == "/") {
            purl = _getViewVar("SubAppPath") + purl;
        } else {
            purl = _getViewVar("SubAppPath") + "/" + purl;
        }
    }

    return purl;
}

function _loadDBObjects(fnSuccess) {
    if (_DBOBJECTS_ISLOADING) {
        if (fnSuccess) {
            _DBOBJECTS_PENDING_FNS.push(fnSuccess);
        }
        _AJAX_REQUESTS._DBOBJECTS_PENDING_FNS = false;

    } else {
        if (_DBOBJECTS.length == 0 && _DBOBJECTS_ISLOADING == false) {
            _DBOBJECTS_ISLOADING = true;
            if (fnSuccess) {
                _DBOBJECTS_PENDING_FNS.push(fnSuccess);
            }
            _AJAX_REQUESTS._DBOBJECTS_PENDING_FNS = false;

            _callServer({
                loadingMsg: "Loading DB Objects structure...",
                url: '/Configuration/DataBase/GetJsonStructure',
                success: function (responseList) {
                    _DBOBJECTS_ISLOADING = false;
                    _DBOBJECTS = JSON.parse(responseList);

                    //Excecute All Pending functions
                    for (var i = 0; i < _DBOBJECTS_PENDING_FNS.length; i++) {
                        _DBOBJECTS_PENDING_FNS[i](_DBOBJECTS);
                    }

                    //Clear pending functions
                    _DBOBJECTS_PENDING_FNS = [];
                    _AJAX_REQUESTS._DBOBJECTS_PENDING_FNS = true;
                }
            });
        } else {
            if (fnSuccess) {
                fnSuccess(_DBOBJECTS);
            }
        }
    }
}

function _loadDBObjectsEditTable(customOptions) {

    //Default Options.
    var options = {
        tableKey: "",
        idElement: "",
        colKey: "ID",
        hasIdentity: "1"
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    _loadDBObjects(function () {
        var tblName = _getDBObjectFullName(options.tableKey);
        $("#lblTbl" + options.tableKey).html(tblName);

        _callServer({
            loadingMsg: "Loading table '" + tblName + "' information...",
            url: "/Configuration/PartialViewDBTable?" + $.param({
                ptbl: tblName,
                pcolKey: options.colKey,
                pidElementContent: options.idElement,
                phasIdentity: options.hasIdentity
            }),
            success: function (htmlResponse) {
                $("#" + options.idElement).html(htmlResponse);
            }
        });
    });
}

function _getDBObjectFullName(pobjKey) {
    var DBObjectFullName = "";

    if (_DBOBJECTS) {
        var DBObjectFullName = "";

        for (var i = 0; i < _DBOBJECTS.length; i++) {
            var DBObject = _DBOBJECTS[i];
            if (DBObject.key == pobjKey) {
                switch (DBObject.objectType) {
                    case "Table":
                        DBObject["prefix"] = _getViewVar("TablesPrefix");
                        break;
                    case "Procedure":
                        DBObject["prefix"] = _getViewVar("ProcPrefix");
                        break;
                    case "Function":
                        DBObject["prefix"] = _getViewVar("FuncPrefix");
                        break;
                    default:
                        DBObject["prefix"] = "";
                        break;
                }

                DBObjectFullName = DBObject.prefix + DBObject.objectName;
                break;
            }
        }
    }

    return DBObjectFullName;
}

function _loadUsersFromDB(onSuccess) {
    _loadDBObjects(function () {
        var spName = _getDBObjectFullName("AdminUser");

        _callProcedure({
            loadingMsgType: "topBar",
            loadingMsg: "Getting users information...",
            name: "[dbo].[" + spName + "]",
            params: [
                { "Name": "@Action", "Value": "ListUsers" }
            ],
            success: {
                fn: function (responseList) {
                    onSuccess(responseList);
                }
            }
        });
    });
}

function _loadRolesOfUserFromDB(onSuccess, userSOEID) {
    _loadDBObjects(function () {
        var spName = _getDBObjectFullName("AdminUser");

        _callProcedure({
            loadingMsgType: "topBar",
            loadingMsg: "Getting roles of users information...",
            name: "[dbo].[" + spName + "]",
            params: [
                { "Name": "@Action", "Value": "ListRolesOfUser" },
                { "Name": "@UserSOEID", "Value": userSOEID }
            ],
            success: {
                fn: function (responseList) {
                    onSuccess(responseList);
                }
            }
        });
    });
}

function _loadUsersXRolesFromDB(onSuccess) {
    _loadDBObjects(function () {
        var spName = _getDBObjectFullName("AdminUser");

        _callProcedure({
            loadingMsgType: "topBar",
            loadingMsg: "Getting roles of users information...",
            name: "[dbo].[" + spName + "]",
            params: [
                { "Name": "@Action", "Value": "ListUsersXRoles" }
            ],
            success: {
                fn: function (responseList) {
                    onSuccess(responseList);
                }
            }
        });
    });
}

function _loadPermitsFromDB(onSuccess) {
    _loadDBObjects(function () {
        var spName = _getDBObjectFullName("AdminPermit");

        _callProcedure({
            loadingMsgType: "topBar",
            loadingMsg: "Getting permits information...",
            name: "[dbo].[" + spName + "]",
            params: [
                { "Name": "@Action", "Value": "List" }
            ],
            success: {
                fn: function (responseList) {
                    onSuccess(responseList);
                }
            }
        });
    });
}

function _loadReportsFromDB(onSuccess) {
    _loadDBObjects(function () {
        var spName = _getDBObjectFullName("AdminReport");

        _callProcedure({
            loadingMsgType: "topBar",
            loadingMsg: "Getting reports information...",
            name: "[dbo].[" + spName + "]",
            params: [
                { "Name": "@Action", "Value": "List" }
            ],
            success: {
                fn: function (responseList) {
                    onSuccess(responseList);
                }
            }
        });
    });
}

function _loadRolesFromDB(onSuccess) {
    _loadDBObjects(function () {
        var spName = _getDBObjectFullName("AdminRole");

        _callProcedure({
            loadingMsgType: "topBar",
            loadingMsg: "Getting roles information...",
            name: "[dbo].[" + spName + "]",
            params: [
                { "Name": "@Action", "Value": "List" }
            ],
            success: {
                fn: function (responseList) {
                    onSuccess(responseList);
                }
            }
        });
    });
}

function _loadRolesXPermitsFromDB(onSuccess) {
    _loadDBObjects(function () {
        var spName = _getDBObjectFullName("AdminRoleXPermit");

        _callProcedure({
            loadingMsgType: "topBar",
            loadingMsg: "Getting permits of roles information...",
            name: "[dbo].[" + spName + "]",
            params: [
                { "Name": "@Action", "Value": "List" }
            ],
            success: {
                fn: function (responseList) {
                    onSuccess(responseList);
                }
            }
        });
    });
}

function _loadRolesXReportsFromDB(onSuccess) {
    _loadDBObjects(function () {
        var spName = _getDBObjectFullName("AdminRoleXReport");

        _callProcedure({
            loadingMsgType: "topBar",
            loadingMsg: "Getting reports of roles information...",
            name: "[dbo].[" + spName + "]",
            params: [
                { "Name": "@Action", "Value": "List" }
            ],
            success: {
                fn: function (responseList) {
                    onSuccess(responseList);
                }
            }
        });
    });
}

//----------------------------------------------------------------
//Util Functions
//----------------------------------------------------------------
function _getSOEID() {
    var soeid = "";

    if (_getViewVar("UserInfo")) {
        soeid = _getViewVar("UserInfo").SOEID;
    }

    return soeid;
}

function _findAllObjByProperty(array, property, value) {
    var list = $.grep(array, function (n, i) {
        if ((n[property] && value) && (typeof n[property] == "string" && typeof value == "string")) {
            return n[property].toUpperCase() == value.toUpperCase();
        } else {
            return n[property] == value;
        }
    });

    return list;
}

function _findOneObjByProperty(array, property, value) {
    var resultObj = null;
    var list = _findAllObjByProperty(array, property, value);

    if (list.length > 0) {
        resultObj = list[0];
    }

    return resultObj;
}

function _findIndexByProperty(parray, pproperty, pvalue) {
    for (var i = 0, len = parray.length; i < len; i++) {
        if (parray[i][pproperty] === pvalue)
            // Return as soon as the object is found
            return i;
    }
    // The object was not found
    return null;
}

function _generateString(listRows, listCols, rowDelimiter, colDelimiter) {
    var result = "";
    for (var i = 0; i < listRows.length; i++) {
        var objRow = listRows[i];

        if (listCols.length == 1) {
            result += objRow[listCols[0]];
        } else {
            for (var j = 0; j < listCols.length; j++) {
                var colName = listCols[j];

                //When is last col
                if (j == (listCols.length - 1)) {
                    result += objRow[colName];
                } else {
                    result += objRow[colName] + colDelimiter;
                }

            }
        }

        //Avoid last row to add rowDelimiter
        if (i != (listRows.length - 1)) {
            result += rowDelimiter;
        }
    }

    return result;
}

function _stripHTMLFromText(html) {
    //var tmp = document.createElement("DIV");
    //tmp.innerHTML = html;
    //return tmp.textContent || tmp.innerText || "";

    if (html) {
        return html.replace(/&lt;/g, "<");
    }

    return html;
}

function _cleanString(ptext, pisPartOfPath) {
    if (!ptext) {
        ptext = "";
    }

    if (typeof pisPartOfPath == "undefined") {
        pisPartOfPath = false;
    }

    //Remove invalid characters in Paths
    if (pisPartOfPath) {
        ptext = _replaceAll("\\", "", ptext);
        ptext = _replaceAll("/", "", ptext);
        ptext = _replaceAll(":", "", ptext);
        ptext = _replaceAll("*", "", ptext);
        ptext = _replaceAll("?", "", ptext);
        ptext = _replaceAll('"', "", ptext);
        ptext = _replaceAll("'", "", ptext);
        ptext = _replaceAll("<", "", ptext);
        ptext = _replaceAll(">", "", ptext);
        ptext = _replaceAll("|", "", ptext);
        ptext = _replaceAll("–", "-", ptext);
    }
    
    //Remove enters characters
    ptext = _replaceAll("\r", "", ptext);
    ptext = _replaceAll("\n", "", ptext);

    //Remove tabs characters
    ptext = _replaceAll("\t", "", ptext);

    //Trim all data
    ptext = $.trim(ptext);

    return ptext;
}

function _encodeAsciiString(pstr) {
    var chars = pstr.split('');
    var newChars = [];

    for (var i = 0; i < chars.length; i++) {
        newChars.push(pstr.charCodeAt(i));
    }

    //for(var i = 0, i < chars.length
    return newChars.join("-");
}

function _decodeAsciiString(ptext) {
    var newString = "";

    if (ptext) {
        var letters = ptext.split('-');

        for (var i = 0; i < letters.length; i++) {
            newString += "" + String.fromCharCode(letters[i]);
        }
    }

    return newString;
}

function _hasEvent($el, eventName) {
    var objEvents = _getBindEvends($el);

    if (objEvents && objEvents[eventName]) {
        return true;
    } else {
        return false;
    }
}

function _isInt(n) {
    return Math.round(n) == n;
}

function _isFloat(n) {
    return !_isInt(n);
}

function _getBindEvends($el) {
    var events = null;
    if ($el.length > 0) {
        events = $._data($el[0], "events");
    }
    return events;
}

function _validateForm(options) {
    if (!$.validator.methods["valueNotEquals"]) {
        $.validator.addMethod("valueNotEquals", function (value, element, arg) {
            if (//For Select of Employees
                $(element).next().hasClass("select2-container") ||

                //For Select of Process
                $(element).attr("type") == "hidden" ||
                $(element).hasClass('position-hide') ||

                //For normal selects
                $(element).is(':visible') ||

                //Ignore Hidden Selects
                ($(element).attr("aria-required") == "true" && $(element).is(':visible'))
            ) {
                if (!element.hasAttribute("validatelement")) {
                    if (value == null || value == "") {
                        value = "0";
                    }
                }

                if (element.hasAttribute("fieldContact")) {
                    value = $(element).find(".lblContactsSummary").text();
                }

                return arg != value;

            } else {

                return true;
            }
        }, "Please select a valid option.");
        console.log("Adding valueNotEquals to $.validator...");
    }

    if (!$.validator.methods["divEditableRequired"]) {
        $.validator.addMethod("divEditableRequired", function (value, element, arg) {

            //Validate empty value
            if (!value) {
                return false;
            }

            //Validate if have placeholder
            if (value == $(element).attr("placeholder")) {
                return false;
            }

            //Return valid
            return true;

        }, "This field is required.");
        console.log("Adding divEditableRequired to $.validator...");
    }

    if (!$.validator.methods["dlFormat"]) {
        $.validator.addMethod("dlFormat", function (value, element, arg) {

            var isOk = false;

            if (_contains(value, "<") &&
                _contains(value, ">") &&
                _contains(value, ".") &&
                _contains(value, "@")) {
                isOk = true;
            }

            //Validate if we have a valid dl format
            if (!isOk) {
                return false;
            }

            //Return valid
            return true;

        }, "Please use a valid DL format e. g. <b>*CSS FRSS CR Re-engineering Team &lt;dl.fro.cr.are.team@imcla.lac.nsroot.net&gt;</b>");
        console.log("Adding dlFormat to $.validator...");
    }

    var $formToValidate;

    if (typeof options.form == "string") {
        $formToValidate = $(options.form);
    } else {
        $formToValidate = options.form;
    }

    $formToValidate.validate({
        focusInvalid: true,
        ignore: "",
        rules: options.rules,

        invalidHandler: function (event, validator) {
            //display error alert on form submit    
        },

        errorPlacement: function (label, element) { // render error placement for each input type   
            //For Select of Employees
            if (element.next().hasClass("select2-container")) {
                element = element.next();
            }

            //For Select of Process
            if ($(element).attr("type") == "hidden" || $(element).hasClass('position-hide')) {
                element = element.next();
            }

            $('<span class="error"></span>').insertAfter(element).append(label)
            var parent = $(element).parent().parent('.form-group');
            
            if (parent.length == 0) {
                parent = $(element).parent();
            }

            parent.removeClass('has-success').addClass('has-error');
        },

        highlight: function (element) { // hightlight error inputs
            var parent = $(element).parent().parent('.form-group');
            if (parent.length == 0) {
                parent = $(element).parent();
            }

            //Select2 and Select2 of Employees
            if ($(element).next().hasClass("select2-container")) {
                //Select2
                if ($(element).parent().parent().hasClass('form-group')) {
                    parent = $(element).parent().parent();
                } else {
                    //Select2 of Employees
                    parent = $(element).parent().parent().parent('.form-group');
                }
            }

            //For Select of Process
            if ($(element).attr("type") == "hidden" || $(element).hasClass('position-hide')) {
                // Add Code here    
            }

            parent.removeClass('has-success').addClass('has-error');
        },

        unhighlight: function (element) { // revert the change done by hightlight

        },

        success: function (label, element) {
            var parent = $(element).parent().parent('.form-group');
            if (parent.length == 0) {
                parent = $(element).parent();
            }

            //Select2 and Select2 of Employees
            if ($(element).next().hasClass("select2-container")) {
                //Select2
                if ($(element).parent().parent().hasClass('form-group')) {
                    parent = $(element).parent().parent();
                } else {
                    //Select2 of Employees
                    parent = $(element).parent().parent().parent('.form-group');
                }
            }

            //For Select of Process
            if ($(element).attr("type") == "hidden" || $(element).hasClass('position-hide')) {
                // Add Code here    
            }

            parent.removeClass('has-error').addClass('has-success');
        },

        submitHandler: function (form) {

        }
    });

    if (options.validateOnInit) {
        $formToValidate.valid();
    }
}

function _concatArrayInString(data, property, character) {
    var result = "";
    for (var i = 0; i < data.length; i++) {
        result += data[i][property] + character;
    }

    if (data.length > 0) {
        result = result.substring(0, result.length - (character.length));
    }
    return result;
}

function _compareID(a, b) {
    if (parseFloat(a.ID) < parseFloat(b.ID)) {
        return -1;
    }
    if (parseFloat(a.ID) > parseFloat(b.ID)) {
        return 1;
    }
    return 0;
}

function _compareNumOrder(a, b) {
    if (parseFloat(a.NumOrder) < parseFloat(b.NumOrder)) {
        return -1;
    }
    if (parseFloat(a.NumOrder) > parseFloat(b.NumOrder)) {
        return 1;
    }
    return 0;
}

function _replaceAll(find, replace, str) {
    if (typeof str == "string") {
        //return str.replace(new RegExp(find, 'g'), replace);
        return (str ? str.split(find).join(replace) : "");
    } else {
        return str;
    }
}

function _getQueryStringParameter(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function _escapeToHTML(ptext) {
    ptext = _replaceAll("&", "&amp;", ptext);
    ptext = _replaceAll("<", "&lt;", ptext);
    ptext = _replaceAll(">", "&gt;", ptext);
    ptext = _replaceAll('"', "&quot;", ptext);
    ptext = _replaceAll("'", "&#39;", ptext);

    return ptext;
}

function _encodeSkipSideminder(ptext) {
    ptext = _replaceAll("%", "_i_", ptext);
    ptext = _replaceAll("'", "_u_", ptext);
    ptext = _replaceAll("<>", "_u1_", ptext);
    ptext = _replaceAll('"', "_u2_", ptext);
    ptext = _replaceAll("*", "_u3_", ptext);
    ptext = _replaceAll(",", "_u4_", ptext);
    ptext = _replaceAll(".", "_y1_", ptext);
    ptext = _replaceAll("[", "_y2_", ptext);
    ptext = _replaceAll("]", "_y3_", ptext);
    ptext = _replaceAll("=", "_y4_", ptext);
    ptext = _replaceAll("<", "_y5_", ptext);
    ptext = _replaceAll(">", "_y6_", ptext);
    ptext = _replaceAll(" ", "_y7_", ptext);
    ptext = _replaceAll("\\", "_y8_", ptext);
    ptext = _replaceAll("/", "_y9_", ptext);
    ptext = _replaceAll(":", "_z1_", ptext);
    ptext = _replaceAll("\n", "_z2_", ptext);
    ptext = _replaceAll("\t", "_z3_", ptext);

    //ptext = replaceAll("W", "_q_", ptext);
    //ptext = replaceAll("LIKE", "_v_", ptext);
    //ptext = replaceAll(" ", "_w_", ptext);
    //ptext = replaceAll("AND", "_x_", ptext);
    //ptext = replaceAll("(", "_y_", ptext);
    //ptext = replaceAll(")", "_z_", ptext);
    return ptext;
}

function _getSqlProcedure(spName, spParams) {
    var sql = "EXEC " + spName + " \n";

    for (var i = 0; i < spParams.length; i++) {
        console.log(spParams[i].Name, spParams[i].Value);
        console.log(spParams[i].Name, _replaceAll("'", "''", spParams[i].Value));
        //Remove comma in last iteration
        if (i == spParams.length - 1) {
            sql += spParams[i].Name + " = '" + _replaceAll("'", "''", spParams[i].Value) + "' \n";
        } else {
            sql += spParams[i].Name + " = '" + _replaceAll("'", "''", spParams[i].Value) + "', \n";
        }
    }

    return sql;
}

function _htmlEncode(value) {
    //create a in-memory div, set it's inner text(which jQuery automatically encodes)
    //then grab the encoded contents back out.  The div never exists on the page.
    return $('<div/>').text(value).html();
}

function _elementInViewport(el) {
    var top = el.offsetTop;
    var left = el.offsetLeft;
    var width = el.offsetWidth;
    var height = el.offsetHeight;

    while (el.offsetParent) {
        el = el.offsetParent;
        top += el.offsetTop;
        left += el.offsetLeft;
    }

    return (
      top >= window.pageYOffset &&
      left >= window.pageXOffset &&
      (top + height) <= (window.pageYOffset + window.innerHeight) &&
      (left + width) <= (window.pageXOffset + window.innerWidth)
    );
}

function _contains(psource, pfind) {
    if (psource && pfind) {
        return psource.indexOf(pfind) !== -1;
    }
    return false;
}

function _containsIgnoreCase(psource, pfind) {
    if (psource && pfind) {
        return psource.toLowerCase().indexOf(pfind.toLowerCase()) !== -1;
    }
    return false;
}

function _execOnObjectShows(pselector, pfn, psecondsToTry) {
    //Set default seconds to wait to 10 
    var seconds = 3;
    var secsToCheck = 0.5;
    var secsSpendWaintig = 0;
    var isJquerySelector = true;
    if (psecondsToTry) {
        seconds = psecondsToTry;
    }

    //Create function to validate if exists the jquery selector or the window's var
    var existsObject;

    if (pselector && typeof (pselector) == 'string') {
        //Validate if needs to check a var or jquery selector
        if (_contains(pselector, "winvar_")) {
            pselector = pselector.replace("winvar_", "");
            isJquerySelector = false;
        }
        existsObject = function () {
            var exists = false;

            if (isJquerySelector) {
                //if ($(pselector).length > 0 && _elementInViewport($(pselector)[0])) {
                if ($(pselector).length > 0 && $(pselector).is(':visible')) {
                    exists = true;
                }
            } else {
                if (window[pselector]) {
                    exists = true;
                }
            }

            return exists;
        }
    } else {
        //Is a function
        existsObject = pselector;
    }
    //If exists selector run the function
    if (existsObject()) {
        if (pfn) {
            pfn();
        }
    } else {
        //Try to find the element each 2 second
        var checkElementIfShowInterval = setInterval(function () {
            //console.log('_execOnObjectShows checking for: ' + pselector);

            //Check if the element exists
            if (existsObject()) {
                if (pfn) {
                    pfn();
                }
                //_hideLoadingFullPage();
                clearInterval(checkElementIfShowInterval);
            } else {
                //Check for seconds to wait
                if (secsSpendWaintig > seconds) {
                    clearInterval(checkElementIfShowInterval);
                }
            }

            secsSpendWaintig = secsSpendWaintig + secsToCheck;
        }, (secsToCheck * 1000)); //Miliseconds
    }
}

function _cropWord(ptext, plength) {
    var $tempSpan = $("<span>" + ptext + "</span>");
    if (ptext.length <= plength) {
        return $tempSpan.text();
    } else {
        return '<span rel="tooltip" data-color-class="primary" data-toggle="tooltip" data-original-title="<div style=\'width: 200px;margin: 5px;\'>' + ptext + '</div>" data-placement="right">' + $tempSpan.text().slice(0, plength) + '...</span>';
        //return "<span title='" + ptext + "'>" + ptext.slice(0, plength) + "...</span>";
    }
}

function _formatDate(date, format, utc) {
    //_formatDate(new Date(), "dddd h:mmtt d MMM yyyy");    : "Friday 2:56pm 13 Feb 2015"
    //_formatDate(new Date(), "dddd hh:mm TT d MMM yyyy");  : "Friday 02:57 PM 13 Feb 2015"
    //_formatDate(new Date(), "yyyy-MMM-dd hh:mm tt");      : "2015-Sep-04 09:11 am"
    if ((date).toString() != "Invalid Date") {
        var MMMM = ["\x00", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var MMM = ["\x01", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var dddd = ["\x02", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var ddd = ["\x03", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

        function ii(i, len) {
            var s = i + "";
            len = len || 2;
            while (s.length < len) s = "0" + s;
            return s;
        }

        var y = utc ? date.getUTCFullYear() : date.getFullYear();
        format = format.replace(/(^|[^\\])yyyy+/g, "$1" + y);
        format = format.replace(/(^|[^\\])yy/g, "$1" + y.toString().substr(2, 2));
        format = format.replace(/(^|[^\\])y/g, "$1" + y);

        var M = (utc ? date.getUTCMonth() : date.getMonth()) + 1;
        format = format.replace(/(^|[^\\])MMMM+/g, "$1" + MMMM[0]);
        format = format.replace(/(^|[^\\])MMM/g, "$1" + MMM[0]);
        format = format.replace(/(^|[^\\])MM/g, "$1" + ii(M));
        format = format.replace(/(^|[^\\])M/g, "$1" + M);

        var d = utc ? date.getUTCDate() : date.getDate();
        format = format.replace(/(^|[^\\])dddd+/g, "$1" + dddd[0]);
        format = format.replace(/(^|[^\\])ddd/g, "$1" + ddd[0]);
        format = format.replace(/(^|[^\\])dd/g, "$1" + ii(d));
        format = format.replace(/(^|[^\\])d/g, "$1" + d);

        var H = utc ? date.getUTCHours() : date.getHours();
        format = format.replace(/(^|[^\\])HH+/g, "$1" + ii(H));
        format = format.replace(/(^|[^\\])H/g, "$1" + H);

        var h = H > 12 ? H - 12 : H == 0 ? 12 : H;
        format = format.replace(/(^|[^\\])hh+/g, "$1" + ii(h));
        format = format.replace(/(^|[^\\])h/g, "$1" + h);

        var m = utc ? date.getUTCMinutes() : date.getMinutes();
        format = format.replace(/(^|[^\\])mm+/g, "$1" + ii(m));
        format = format.replace(/(^|[^\\])m/g, "$1" + m);

        var s = utc ? date.getUTCSeconds() : date.getSeconds();
        format = format.replace(/(^|[^\\])ss+/g, "$1" + ii(s));
        format = format.replace(/(^|[^\\])s/g, "$1" + s);

        var f = utc ? date.getUTCMilliseconds() : date.getMilliseconds();
        format = format.replace(/(^|[^\\])fff+/g, "$1" + ii(f, 3));
        f = Math.round(f / 10);
        format = format.replace(/(^|[^\\])ff/g, "$1" + ii(f));
        f = Math.round(f / 10);
        format = format.replace(/(^|[^\\])f/g, "$1" + f);

        var T = H < 12 ? "AM" : "PM";
        format = format.replace(/(^|[^\\])TT+/g, "$1" + T);
        format = format.replace(/(^|[^\\])T/g, "$1" + T.charAt(0));

        var t = T.toLowerCase();
        format = format.replace(/(^|[^\\])tt+/g, "$1" + t);
        format = format.replace(/(^|[^\\])t/g, "$1" + t.charAt(0));

        var tz = -date.getTimezoneOffset();
        var K = utc || !tz ? "Z" : tz > 0 ? "+" : "-";
        if (!utc) {
            tz = Math.abs(tz);
            var tzHrs = Math.floor(tz / 60);
            var tzMin = tz % 60;
            K += ii(tzHrs) + ":" + ii(tzMin);
        }
        format = format.replace(/(^|[^\\])K/g, "$1" + K);

        var day = (utc ? date.getUTCDay() : date.getDay()) + 1;
        format = format.replace(new RegExp(dddd[0], "g"), dddd[day]);
        format = format.replace(new RegExp(ddd[0], "g"), ddd[day]);

        format = format.replace(new RegExp(MMMM[0], "g"), MMMM[M]);
        format = format.replace(new RegExp(MMM[0], "g"), MMM[M]);

        format = format.replace(/\\(.)/g, "$1");
    } else {
        format = "";
    }

    return format;
}

function _loadTableByArray(listData, columns, showTo) {
    var $table = $("<table class='table table-bordered'></table>");
    var $lineHeader = $("<tr></tr>");

    //Add columns
    for (var c = 0; c < columns.length; c++) {
        $lineHeader.append($("<td></td>").html(columns[c].Name));
        $table.append($lineHeader);
    }

    //Add rows
    for (var i = 0; i < listData.length; i++) {
        var row = listData[i];
        var $line = $("<tr></tr>");

        for (var j = 0; j < columns.length; j++) {
            $line.append($("<td></td>").html(row[columns[j].Key]));
        }

        $table.append($line);
    }

    $table.appendTo($(showTo));
}

function _console(pobj1, pobj2) {
    if (console) {
        if (pobj1 && pobj2) {
            console.log(pobj1, pobj2);
        } else {
            console.log(pobj1);
        }

    }
}

function _setViewVar(key, value) {
    _MVCOBJECTS[key] = value;
}

function _getViewVar(key) {
    return _MVCOBJECTS[key];
}

function _downloadFile(customOptions) {
    //Default Options.
    var options = {
        loadingMsgType: "fullLoading",
        loadingMsg: "Download File...",
        urlCopyFrom: "",
        fileName: "",
        onSuccess: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    var idAjaxRequest = _createCustomID();
    var idLoading = _createCustomID();

    //reset counter on 10000 calls
    _AJAX_REQUESTS_COUNT++;
    if (_AJAX_REQUESTS_COUNT > 10000) {
        _ajaxResetCounters();
    }

    //Create Request
    var request = new XMLHttpRequest();
    request.open('POST', _fixFrameworkURL("/Shared/DownloadFile"), true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.responseType = 'blob';
    beforeSend();
    request.onload = function (e) {
        _AJAX_REQUESTS[idAjaxRequest] = true;

        if (options.loadingMsg) {
            switch (options.loadingMsgType) {
                case "fullLoading":
                    _hideLoadingFullPage({ idLoading: idLoading });
                    break;
                case "topBar":
                    _hideLoadingTopMessage(idAjaxRequest);
                    break;
            }
        }
        _runAjaxPendingFns();

        if (this.status === 200) {
            var blob = this.response;
            if (window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveBlob(blob, options.fileName);
            }
            else {
                var downloadLink = window.document.createElement('a');
                var contentTypeHeader = request.getResponseHeader("Content-Type");
                downloadLink.href = window.URL.createObjectURL(new Blob([blob], { type: contentTypeHeader }));
                downloadLink.download = options.fileName;
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            }
            options.onSuccess();
        } else {
            _showAlert({
                id: "DownloadFileError",
                type: "error",
                content: "An error occurrect when try to download the files '" + this.statusText + "'.",
                animateScrollTop: true
            });
        }

    };
    request.onerror = function (e) {
        _AJAX_REQUESTS[idAjaxRequest] = true;

        if (options.loadingMsg) {
            switch (options.loadingMsgType) {
                case "fullLoading":
                    _hideLoadingFullPage({ idLoading: idLoading });
                    break;
                case "topBar":
                    _hideLoadingTopMessage(idAjaxRequest);
                    break;
            }
        }

        console.log(e, this);
        _runAjaxPendingFns();
    }
    request.send($.param({
        pfileName: options.fileName,
        purlCopyFrom: options.urlCopyFrom
    }));

    function beforeSend() {
        _AJAX_REQUESTS[idAjaxRequest] = false;

        if (options.loadingMsg) {
            switch (options.loadingMsgType) {
                case "fullLoading":
                    _showLoadingFullPage({ idLoading: idLoading, msg: options.loadingMsg });
                    break;
                case "notification":
                    _showNotification("info", options.loadingMsg);
                    break;
                case "topBar":
                    _showLoadingTopMessage(options.loadingMsg, idLoading);
                    break;
                case "alert":
                    _showAlert({
                        type: "info",
                        content: options.loadingMsg
                    });
                    break;
            }
        }
    }

}

function _downloadExcel(options) {
    if (options.spName || options.spParams) {
        options.sql = _getSqlProcedure(options.spName, options.spParams);
    }

    var url = _getViewVar("SubAppPath") + "/Shared/DownloadExcel/?" + $.param({
        pfilename: options.filename,
        psheetname: options.sheetname,
        ppathToSave: _encodeSkipSideminder(options.pathToSave),
        psql: _encodeSkipSideminder(options.sql),
        pappKey: _getViewVar("AppKey")
    });

    if (options.multiplesFiles) {
        window.open(url);
    } else {
        window.location.href = url;
    }

    if (options.success.msg) {
        _showNotification("success", options.success.msg);
    }
}

function _downloadExcelJson(options) {
    if (options.spName || options.spParams) {
        options.sql = _getSqlProcedure(options.spName, options.spParams);
    }

    var url = _getViewVar("SubAppPath") + "/Shared/DownloadExcelJson/?" + $.param({
        pfilename: options.filename,
        psheetname: options.sheetname,
        ppathToSave: _encodeSkipSideminder(options.pathToSave),
        pappKey: _getViewVar("AppKey")
    });

    var request = new XMLHttpRequest();
    request.open('POST', url, true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.responseType = 'blob';

    request.onload = function (e) {
        if (this.status === 200) {
            var blob = this.response;
            if (window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveBlob(blob, options.filename);
            }
            else {
                var downloadLink = window.document.createElement('a');
                var contentTypeHeader = request.getResponseHeader("Content-Type");
                downloadLink.href = window.URL.createObjectURL(new Blob([blob], { type: contentTypeHeader }));
                downloadLink.download = options.filename;
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
            }
        }
    };
    request.send($.param({
        jsonData: options.jsonData
    }));


    //$.ajax({
    //    url: url,
    //    type: "POST",
    //    data: {
    //        jsonData: options.jsonData
    //    },
    //    success: function (request) {
    //        var blob = request;
    //        if(window.navigator.msSaveOrOpenBlob) {
    //            window.navigator.msSaveBlob(blob, fileName);
    //        }else{
    //            var downloadLink = window.document.createElement('a');
    //          //  var contentTypeHeader = XMLHttpRequest.getResponseHeader("Content-Type");
    //            //downloadLink.href = window.URL.createObjectURL(new Blob([blob], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" }));

    //            downloadLink.href = window.URL.createObjectURL(new Blob([blob], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" }));
    //            downloadLink.download = options.filename;
    //            document.body.appendChild(downloadLink);
    //            downloadLink.click();
    //            document.body.removeChild(downloadLink);
    //        }


    //        if (options.success.msg) {
    //            _showNotification("success", options.success.msg);
    //        }
    //    },
    //    error: function (msg) {
    //        alert(msg);
    //        //swal({ title: "Something went wrong!", text: "Error occured!", type: "error" });
    //    }
    //});
}

function _loadMonths(customOptions) {
    var reportingPeriodMoment;
    var cantBackMonth = 4;
    var cantForward = 2;
    var resultList = [];

    //Default Options.
    var options = {
        showTo: "",
        reportingPeriod: "",
        selYear: 0,
        selMonth: 0,
        cantBackMonths: 4,
        cantForwardMonths: 4
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    //Set cantBackMonths
    cantBackMonth = options.cantBackMonths;
    cantForward = options.cantForwardMonths;

    //If not exist set current month
    if (!options.reportingPeriod) {
        options.reportingPeriod = moment().format('MMMM YYYY');
    }

    //If exists selYear and selMonth
    if (options.selYear && options.selMonth) {
        options.reportingPeriod = moment((options.selMonth + ' ' + options.selYear), 'MM YYYY').format('MMMM YYYY');
    }

    reportingPeriodMoment = moment(options.reportingPeriod, 'MMMM YYYY');

    var currentMonthId = parseInt(reportingPeriodMoment.format('M'));
    var currentYear = parseInt(reportingPeriodMoment.format('YYYY'));

    //Add Current Month
    var tempMonthId = currentMonthId;
    var tempYear = currentYear;
    var tempPeriod = currentMonthId + " " + currentYear;
    resultList.push({
        id: tempYear + "" + moment(tempPeriod, 'M YYYY').format('MM'),
        year: tempYear + "",
        monthId: tempMonthId + "",
        monthName: moment(tempPeriod, 'M YYYY').format('MMMM'),
        period: moment(tempPeriod, 'M YYYY').format('MMMM YYYY'),
        rows: "0"
    });

    //Get Back Months
    for (var i = 0; i < cantBackMonth; i++) {
        if (tempMonthId == 1) {
            tempMonthId = 12;
            tempYear = tempYear - 1;
        } else {
            tempMonthId--;
        }

        tempPeriod = tempMonthId + " " + tempYear;
        resultList.push({
            id: tempYear + "" + moment(tempPeriod, 'M YYYY').format('MM'),
            year: tempYear + "",
            monthId: tempMonthId + "",
            monthName: moment(tempPeriod, 'M YYYY').format('MMMM'),
            period: moment(tempPeriod, 'M YYYY').format('MMMM YYYY'),
            rows: "0"
        });
    }

    //Get Forward Months
    tempMonthId = currentMonthId;
    tempYear = currentYear;
    tempPeriod = "";
    for (var i = 0; i < cantForward; i++) {
        if (tempMonthId == 12) {
            tempMonthId = 1;
            tempYear = tempYear + 1;
        } else {
            tempMonthId++;
        }

        tempPeriod = tempMonthId + " " + tempYear;
        resultList.push({
            id: tempYear + "" + moment(tempPeriod, 'M YYYY').format('MM'),
            year: tempYear + "",
            monthId: tempMonthId + "",
            monthName: moment(tempPeriod, 'M YYYY').format('MMMM'),
            period: moment(tempPeriod, 'M YYYY').format('MMMM YYYY'),
            rows: "0"
        });
    }

    //Sort array by id
    resultList.sort(function (a, b) {
        if (parseFloat(a.id) < parseFloat(b.id)) {
            return 1;
        }
        if (parseFloat(a.id) > parseFloat(b.id)) {
            return -1;
        }
        return 0;
    });

    //Add options to select
    if (options.showTo) {
        $(options.showTo).contents().remove();
        for (var i = 0; i < resultList.length; i++) {
            var objTemp = resultList[i];
            //console.log(objTemp.period, options.reportingPeriod);
            $(options.showTo).append($('<option>', {
                year: objTemp.year,
                month: objTemp.monthId,
                value: objTemp.period,
                text: objTemp.period,
                selected: (objTemp.period == options.reportingPeriod)
            }));
        }
    }

    return resultList;
}

function _copyToClipboard(text) {
    //Update values
    var $txtCopyToClipboard = $('#txtCopyToClipboard');
    $txtCopyToClipboard.text(text);
    $txtCopyToClipboard.val(text);

    var copyTextarea = document.querySelector('#txtCopyToClipboard');
    copyTextarea.select();

    try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copying text command was ' + msg);
    } catch (err) {
        console.log('Oops, unable to copy');
    }
}

function _clearServerMemory() {
    _callServer({
        loadingMsg: "Clean Server Memory...",
        url: '/Shared/CleanMemory',
        success: function (responseList) { }
    });
}

function _killCurrentProcess(onSuccess) {
    _callServer({
        loadingMsg: "Kill current process...",
        url: '/Shared/KillCurrentProcess',
        success: function (responseList) {
            if (onSuccess) {
                onSuccess();
            }
        }
    });
}

function _isChrome() {
    var isChrome = false;
    if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
        isChrome = true;
    }

    return isChrome;
}

function _isBrowserSupportJS() {
    var support = false;
    if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
        support = true;
    }
    if ($('html').is('.IE8') || $('html').is('.IE9') || $('html').is('.upIE9')) {
        support = true;
    }
    return support;
}

function _toJSON(pobject) {
    return JSON.stringify(pobject, null, "\t");
}

function _refreshJqxGridColumnsSize() {
    //Refresh Grid Columns
    //_execOnObjectShows(
    //    function () {
    //        if ($(".page-sidebar").width() == 60) {
    //            return true;
    //        } else {
    //            return false;
    //        }
    //    }, function () {
    //        console.log("$.jqxGridApi.refreshJqxGridColumnSize()...");
    //        $.jqxGridApi.refreshJqxGridColumnSize();
    //    }
    //);
}

function _hideMenu(customOptions) {

    //Default Options.
    var options = {
        onSuccess: null
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    _execOnObjectShows(
        function () {
            if (!$._data($(".sidebar_toggle")[0], "events")) {
                return false;
            } else {
                return true;
            }
        },
        function () {
            if ($(".page-sidebar").hasClass("expandit") || (!$(".page-sidebar").hasClass("expandit") && !$(".page-sidebar").hasClass("collapseit"))) {
                $(".sidebar_toggle").click();
                $(".sidebar_toggle").attr("wasHideMenu", "1");

                //Trigger On Success
                execOnSuccess();
            }
        }
    );

    function execOnSuccess() {

        setTimeout(function () {

            if (options.onSuccess) {
                options.onSuccess();
            }

        }, 500);
    }

    //_refreshJqxGridColumnsSize();
}

function _createCustomID(addMilliseconds) {
    var myDate = new Date();
    var id = "";

    if (typeof addMilliseconds == "undefined") {
        addMilliseconds = true;
    }
    id += myDate.getFullYear();
    id += (myDate.getMonth() + 1) < 10 ? '0' + (myDate.getMonth() + 1) : (myDate.getMonth() + 1);
    id += myDate.getDate() < 10 ? '0' + myDate.getDate() : myDate.getDate();
    id += myDate.getHours() < 10 ? '0' + myDate.getHours() : myDate.getHours();
    id += myDate.getMinutes() < 10 ? '0' + myDate.getMinutes() : myDate.getMinutes();
    id += myDate.getSeconds() < 10 ? '0' + myDate.getSeconds() : myDate.getSeconds();
    if (addMilliseconds) {
        id += myDate.getMilliseconds() < 10 ? '0' + myDate.getMilliseconds() : myDate.getMilliseconds();
    }

    return id;
}

function _mergeObject(pobject1, pobject2) {
    var defaultKeys = Object.keys(pobject1);

    $(defaultKeys).each(function (index, value) {
        if (typeof pobject1[value] == 'object') {
            if (typeof pobject2[value] == "undefined") {
                pobject2[value] = pobject1[value];
            } else {
                _mergeObject(pobject1[value], pobject2[value]);
            }
        } else {
            //Si no tiene algun valor lo asignamos y salimos.
            if (typeof pobject2[value] == "undefined") {
                //Si no tiene algun valor lo asignamos y salimos.
                pobject2[value] = pobject1[value];
            }
        }
    });
}

function _toggleFullScreen() {
    $(".content-body").toggleClass("toggle-fullscreen");
    if ($(".content-body").hasClass("toggle-fullscreen")) {
        $("body").css("overflow", "hidden");
    } else {
        $("body").css("overflow", "");
    }
}

//----------------------------------------------------------------
//Security Functions
//----------------------------------------------------------------
function _getUserInfo() {
    return _getViewVar("UserInfo");
}

function _userHaveRole(prole) {
    var roles = _getUserInfo().Roles.split(',');
    for (var i = 0; i < roles.length; i++) {
        if (prole == roles[i]) {
            return true;
        }
    }
    return false;
}

function _userHasAccessToCurrentUrl(appPermits, userPermits) {
    //Show Loading to user
    _showLoadingFullPage({
        idLoading: "UserAccess",
        msg: "Validating user access...",
    });

    var objUser = _getUserInfo();
    var hasAccess = true;

    //Validate only Urls in AppPermits
    var currentUrlNeedToValidate = false;
    var objCurrentAppPermit = null;

    function isCurrentUrl(permitUrl) {
        var result = false;

        if (permitUrl) {
            var currentUrl = window.location.pathname;
            permitUrl = (_getViewVar("SubAppPath") + permitUrl);

            //Remove last for permitUrl char (/) "/SupportDocProdCtrl/"
            if (permitUrl.slice(-1) == "/") {
                permitUrl = permitUrl.slice(0, -1);
            }

            //Remove last for currentUrl char (/) "/SupportDocProdCtrl/"
            if (currentUrl.slice(-1) == "/") {
                currentUrl = currentUrl.slice(0, -1);
            }

            //"/SupportDocProdCtrl/ProcessList?pviewType=Checker" in "/SupportDocProdCtrl/ProcessList"
            if (_contains(permitUrl, currentUrl)) {

                //Validate Query String
                var permitUrlQueryStrings = permitUrl.replace(currentUrl, "");
                if (permitUrlQueryStrings) {
                    if (_contains(window.location.search, permitUrlQueryStrings)) {
                        result = true;
                    }
                } else {
                    result = true;
                }
            }
        }
        
        return result;
    }

    for (var i = 0; i < appPermits.length; i++) {
        var objPermit = appPermits[i];

        if (isCurrentUrl(objPermit.Path)) {    
            currentUrlNeedToValidate = true;
            objCurrentAppPermit = objPermit;
            break;
        }
    }

    //Validate if current user has access to the current URL
    var userHasAccessToUrl = false;
    var urlAccessDenied = _getViewVar("SubAppPath") + "/Error/AccessDenied/?" + $.param({
        pappKey: _getViewVar("AppKey"),
        purl: window.location.href
    });

    if (currentUrlNeedToValidate && objCurrentAppPermit.FlagIsGlobal != "1") {
        for (var u = 0; u < userPermits.length; u++) {
            var objUserPermit = userPermits[u];

            if (isCurrentUrl(objUserPermit.Path)) {
                userHasAccessToUrl = true;
                break;
            }
        }
    } else {
        //Validate if current URL is Used for Super Admin, (Allow Access)
        var supperAdminPermits = _getSuperAdminPermits();
        for (var i = 0; i < supperAdminPermits.length; i++) {
            var objSuperAdminPermit = supperAdminPermits[i];

            if (isCurrentUrl(objSuperAdminPermit.Path)) {
                userHasAccessToUrl = true;
                break;
            }
        }
    }

    //Validate if URL is Global
    if (userHasAccessToUrl == false && objCurrentAppPermit && objCurrentAppPermit.FlagIsGlobal == "1") {
        userHasAccessToUrl = true;
    }

    //Add hidden public URLS
    var publicURLS = [
        'CF/AddKey'
    ];
    if (userHasAccessToUrl == false) {
        var currentPathname = window.location.pathname;
        for (var i = 0; i < publicURLS.length; i++) {
            if (_contains(currentPathname, publicURLS[i])) {
                userHasAccessToUrl = true;
                break;
            }
        }
    }

    //Validate if we need to redirect
    if (userHasAccessToUrl == false) {
        hasAccess = false;

        //Show custom error
        if (objCurrentAppPermit && objCurrentAppPermit.AccessCommentHtml) {

            _addSessionMessage(objCurrentAppPermit.AccessCommentHtml, function () {
                //Redirect to Access Denied Page
                window.location.href = urlAccessDenied;
            });

        } else {

            //Redirect to Access Denied Page
            window.location.href = urlAccessDenied;

        }
    } else {
        //Hide Loading
        _hideLoadingFullPage({
            idLoading: "UserAccess"
        });
    }

    return hasAccess;
}

function _logout() {
    /*Production and UAT*/
    $.removeCookie('SMSESSION', { path: '/', domain: '.citigroup.net' });
    $.removeCookie('SMIDENTITY', { path: '/', domain: '.citigroup.net' });

    /*eucsrvappdev002 and eucsrvappuat003*/
    $.removeCookie('SMSESSION', { path: '/', domain: '.nsroot.net' });

    location.reload();
}

function _refreshSession(onSuccess) {
    _callServer({
        loadingMsgType: "topBar",
        loadingMsg: "Refresh session...",
        url: '/Shared/RefreshSession',
        success: function (responseList) {
            if (onSuccess) {
                onSuccess();
            }
        }
    });
}

//----------------------------------------------------------------
//Ajax Functions
//----------------------------------------------------------------
function _callProcedure(options) {
    //Set default options
    options.ignoreErrorMsg = typeof options.ignoreErrorMsg != "undefined" ? options.ignoreErrorMsg : false;
    options.async = typeof options.async != "undefined" ? options.async : true;
    options.response = typeof options.response != "undefined" ? (options.response ? "1" : "0") : "1";
    options.loadingMsgType = typeof options.loadingMsgType != "undefined" ? options.loadingMsgType : "topBar";

    //Check parameters data
    if (validateParameters()) {
        _callServer({
            url: "/Shared/CallProcedure",
            data: {
                pdataProcedure: _toJSON({
                    sp: {
                        Name: options.name,
                        Params: options.params
                    }
                }),
                presponse: options.response
            },
            ignoreErrorMsg: options.ignoreErrorMsg,
            error: options.error,
            success: options.success,
            async: options.async,
            loadingMsgType: options.loadingMsgType,
            loadingMsg: options.loadingMsg,
            isCallProcedure: true,
            type: "POST"
        });
    } else {
        var msgHtml = "Ajax error, please review and fix missing properties 'Name' and 'Value' in params: <pre>" + _toJSON({
            sp: {
                Name: options.name,
                Params: options.params
            }
        }) + "</pre";

        _showAlert({
            type: 'error',
            title: "Message",
            content: msgHtml,
            animateScrollTop: true
        });
    }

    function validateParameters() {
        var isOk = true;

        for (var i = 0; i < options.params.length; i++) {
            var objParam = options.params[i];

            if (typeof objParam.Name == 'undefined' || typeof objParam.Value == 'undefined') {
                isOk = false;
                break;
            }
        }

        return isOk;
    }
}

function _callServer(params) {
    params.ignoreErrorMsg = typeof params.ignoreErrorMsg != "undefined" ? params.ignoreErrorMsg : false;
    params.loadingMsgType = typeof params.loadingMsgType != "undefined" ? params.loadingMsgType : "topBar";
    params.loadingMsg = typeof params.loadingMsg != "undefined" ? params.loadingMsg : "Loading...";
    params.isCallProcedure = typeof params.isCallProcedure != "undefined" ? params.isCallProcedure : false;
    var idAjaxRequest = _createCustomID();

    //reset counter on 10000 calls
    _AJAX_REQUESTS_COUNT++;
    if (_AJAX_REQUESTS_COUNT > 10000) {
        _ajaxResetCounters();
    }

    if (!params["type"]) {
        params["type"] = "GET"
    }

    if (typeof params["async"] == "undefined") {
        params["async"] = true
    }

    var idLoading = _createCustomID();

    //Add Default App
    if (_contains(params["url"], "?")) {
        params["url"] += "&pappKey=" + _getViewVar("AppKey");
    } else {
        params["url"] += "?pappKey=" + _getViewVar("AppKey");
    }

    $.ajax({
        async: params["async"],
        url: _getViewVar("SubAppPath") + params["url"],
        data: params["data"],
        beforeSend: function () {
            _AJAX_REQUESTS[idAjaxRequest] = false;

            if (params.loadingMsg) {
                switch (params.loadingMsgType) {
                    case "fullLoading":
                        _showLoadingFullPage({ idLoading: idLoading, msg: params.loadingMsg });
                        break;
                    case "notification":
                        _showNotification("info", params.loadingMsg);
                        break;
                    case "topBar":
                        _showLoadingTopMessage(params.loadingMsg, idAjaxRequest);
                        break;
                    case "alert":
                        _showAlert({
                            type: "info",
                            content: params.loadingMsg
                        });
                        break;
                }
            }

            if (params["beforeSend"]) {
                params["beforeSend"]();
            }
        },
        success: function (response) {
            _AJAX_REQUESTS[idAjaxRequest] = true;

            if (params.loadingMsg) {
                switch (params.loadingMsgType) {
                    case "fullLoading":
                        _hideLoadingFullPage({ idLoading: idLoading });
                        break;
                    case "topBar":
                        _hideLoadingTopMessage(idAjaxRequest);
                        break;
                }
            }

            //When Single Sing lost the session
            if (response && typeof response == "string" &&
                (response.indexOf("This page is used to hold your data while you are being authorized for your request") != -1 ||
                 response.indexOf("/siteminderagent/forms") != -1)) {

                _showModal({
                    width: '40%',
                    modalId: "modalSessionEnd",
                    addCloseButton: false,
                    buttons: [{
                        name: "Login",
                        class: "btn-success",
                        onClick: function () {
                            //Refresh page on click "Login"
                            window.location.reload();
                        }
                    }],
                    title: "Session Expired",
                    contentHtml: "Your session is end, please login again.",
                    onReady: function ($modal) {

                        //Refresh page if click in the (x) icon
                        $modal.find(".close").click(function () {
                            window.location.reload();
                        });

                        //Refresh page if click in backdrop of modal
                        $(".modal").click(function (e) {
                            var $target = $(e.target);

                            //On click in backdrop, ignore click in .modal-body or .modal-title
                            if ($target.hasClass("modal")) {
                                window.location.reload();
                            }
                        });
                    }
                });

            } else {

                if (params["success"]) {
                    if (typeof params["success"] == "function") {
                        params["success"](response, params["success"].params);
                    } else {
                        if (params["success"].msg) {
                            _showNotification("success", params["success"].msg);
                            //_showAlert({
                            //    "showTo": params["success"].showTo,
                            //    "type": "success",
                            //    "content": params["success"].msg
                            //});
                        }

                        if (params["success"].fn) {
                            params["success"].fn(response, params["success"].params);
                        }
                    }
                }

                _runAjaxPendingFns();
            }
        },
        error: function (xhr, textStatus, thrownError) {
            _AJAX_REQUESTS[idAjaxRequest] = true;

            if (params.loadingMsg) {
                switch (params.loadingMsgType) {
                    case "fullLoading":
                        _hideLoadingFullPage({ idLoading: idLoading });
                        break;
                    case "topBar":
                        _hideLoadingTopMessage(idAjaxRequest);
                        break;
                }
            }

            //Ignore error message to user
            if (params.ignoreErrorMsg == false) {
                _catchAjaxError(xhr, textStatus, thrownError);
            }

            _runAjaxPendingFns();

            if (typeof params["error"] == "function") {
                params["error"](xhr, textStatus, thrownError);
            }
        },
        type: params["type"]
    });
}

function _ajaxResetCounters() {
    _AJAX_REQUESTS = {};
    _AJAX_REQUESTS_COUNT = 0;
}

function _ajaxIsComplete() {
    var keys = Object.keys(_AJAX_REQUESTS);
    var result = true;

    for (var i = 0; i < keys.length; i++) {
        if (_AJAX_REQUESTS[keys[i]] == false) {
            result = false;
            break;
        }
    }
    return result;
}

function _runAjaxPendingFns() {
    if (_ajaxIsComplete()) {
        //Execute all pending function
        for (var i = 0; i < _AJAX_PENDING_FNS.length; i++) {
            _AJAX_PENDING_FNS[i]();
        }
        //Clear array of functions
        _AJAX_PENDING_FNS = [];
    }
}

function _execOnAjaxComplete(fn) {
    //Add to pending functions
    if (fn) {
        _AJAX_PENDING_FNS.push(fn);

        //Check if all ajax complete
        _runAjaxPendingFns();
    }
}

function _catchAjaxError(xhr, textStatus, thrownError) {
    var cookieErrorMsg = $.cookie("exceptionMsg");

    _showDetailAlert({
        title: "Message",
        shortMsg: ((cookieErrorMsg) ? cookieErrorMsg : "An error ocurred."),
        longMsg: xhr['responseText'],
        type: "error",
        viewLabel: "View Details"
    });

    _GLOBAL_SETTINGS.tooltipsPopovers();
}

//----------------------------------------------------------------
//Loading Messages
//----------------------------------------------------------------
function _showLoadingFullPageAsync() {
    $("html").css("overflow", "hidden");
    $("#fullLoadingPageAsync").show();
}

function _showLoadingTopMessage(pmsg, pid) {
    if (!pid) {
        pid = "GlobalTopMsg";
    }

    //If exists the id
    if ($("#TopMsg-" + pid).length > 0) {
        $("#TopMsg-" + pid).remove();
    }

    //Add opacity to others top message
    if ($(".pace-activity-label").length > 0) {
        $(".pace-activity-label").css("opacity", 0);
    }

    var $label = $("<span class='pace-activity-label' id='TopMsg-" + pid + "'>" + ((pmsg) ? pmsg : "Loading...") + "</span>");
    $(".pace-activity").before($label);

    $(".pace-activity").parent().removeClass("pace-inactive");
    $(".pace-activity").parent().addClass("pace-active");
    $(".pace-activity").parent().addClass("top-bar-loading");

    //console.log('showFullLoading');
}

function _showLoadingFullPage(options) {
    if (!options) { options = {}; }

    var idLoading = options["idLoading"];
    var msg = options["msg"];

    ////http://tobiasahlin.com/spinkit/
    //var htmlSkCircle =
    //    '<div class="sk-circle"> ' +
    //    '  <div class="sk-circle1 sk-child"></div> ' +
    //    '  <div class="sk-circle2 sk-child"></div> ' +
    //    '  <div class="sk-circle3 sk-child"></div> ' +
    //    '  <div class="sk-circle4 sk-child"></div> ' +
    //    '  <div class="sk-circle5 sk-child"></div> ' +
    //    '  <div class="sk-circle6 sk-child"></div> ' +
    //    '  <div class="sk-circle7 sk-child"></div> ' +
    //    '  <div class="sk-circle8 sk-child"></div> ' +
    //    '  <div class="sk-circle9 sk-child"></div> ' +
    //    '  <div class="sk-circle10 sk-child"></div> ' +
    //    '  <div class="sk-circle11 sk-child"></div> ' +
    //    '  <div class="sk-circle12 sk-child"></div> ' +
    //    '</div> ';

    var htmlCubes =
        '<div class="spinner"> ' +
        '  <div class="cube1"></div> ' +
        '  <div class="cube2"></div> ' +
        '</div>';

    $('body').append(
            $('<div id="fullLoading" ' + (idLoading ? 'idLoading="' + idLoading + '"' : '') + ' style="display: block;" role="status" aria-hidden="false">' +
                '<div class="ProgressHolder">' +
                    '<div class="divLoading">' +
                        '<div class="divLoadingImg">' + htmlCubes +
                            //'<img alt="Loading..." src="data:image/gif;base64,R0lGODlhWgBaAPf/AP7+/tvb2wAAANzc3NXV1dbW1sjIyLOzs8nJydTU1AMDA9fX1yMjI7e3t/39/cvLy9jY2Nra2pSUlGJiYtnZ2c3NzbS0tM7OziIiIiYmJrm5ubGxsZeXl5OTkwICAgUFBdPT05CQkMfHx8rKymtra52dnQYGBmNjY/z8/F5eXiUlJZWVlWhoaLKysszMzL6+vl1dXaOjo/v7+3Jycvr6+iQkJA4ODmBgYNLS0sbGxmFhYd3d3ff39/X19ZKSkvT09I+PjwQEBPLy8u3t7fn5+ba2tjY2Nvj4+NHR0bW1te/v7/b29rCwsF9fX97e3vHx8SEhISgoKC4uLvPz8wEBAcXFxS8vL729vZaWlunp6eTk5I6OjicnJ+Xl5ejo6Obm5uvr69/f3+Hh4WRkZODg4Li4uKKiouzs7Ofn56+vr5ycnOLi4tDQ0MTExJGRkTc3N7+/v+7u7s/Pz66ururq6ry8vPDw8K2trbq6uuPj48PDw2dnZ42NjYqKii0tLVhYWGVlZQ8PD3Z2doyMjCAgIJ6enmZmZg0NDVdXV1xcXBcXFxoaGlpaWhsbGzU1NVlZWaysrKSkpB4eHpqamru7u5iYmJubm8DAwKqqqltbW6urqx8fH2xsbBwcHHNzc3FxcWpqalVVVXBwcBYWFouLi8HBwTExMZ+fn2lpaXV1dRgYGCkpKYmJiQcHB1ZWVlRUVBUVFaWlpcLCwpmZmaCgoKGhoTAwMG9vbywsLG5ubhkZGf7//wsLC4eHh4iIiB0dHTo6OjQ0NHd3d21tbampqSsrK3R0dH5+foSEhBISElNTU6ioqIaGhoCAgDIyMqampgoKCj8/P4KCghMTE1JSUioqKqenpxQUFIODg4GBgXt7e39/f3l5eTMzM3p6enx8fEFBQTg4OBEREf3//1FRUTk5Ofz//3h4eEhISDs7O0dHR319fRAQEFBQUEZGRkxMTAgICIWFhT09PUlJST4+Pjw8PENDQ0VFRU1NTUtLS0pKSkREREJCQgwMDP///////yH/C05FVFNDQVBFMi4wAwEAAAAh/wtYTVAgRGF0YVhNUDw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYxIDY0LjE0MDk0OSwgMjAxMC8xMi8wNy0xMDo1NzowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M1LjEgV2luZG93cyIgeG1wOkNyZWF0ZURhdGU9IjIwMTItMTAtMjdUMTQ6MjA6NTUrMDY6MDAiIHhtcDpNb2RpZnlEYXRlPSIyMDEyLTEwLTI3VDIxOjAwOjA4KzA2OjAwIiB4bXA6TWV0YWRhdGFEYXRlPSIyMDEyLTEwLTI3VDIxOjAwOjA4KzA2OjAwIiBkYzpmb3JtYXQ9ImltYWdlL2dpZiIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpGRkYyOEZCMzIwNDYxMUUyOTQyNUFDRjVCRjQwMUYxNiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpGRkYyOEZCNDIwNDYxMUUyOTQyNUFDRjVCRjQwMUYxNiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkZGRjI4RkIxMjA0NjExRTI5NDI1QUNGNUJGNDAxRjE2IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkZGRjI4RkIyMjA0NjExRTI5NDI1QUNGNUJGNDAxRjE2Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Af/+/fz7+vn49/b19PPy8fDv7u3s6+rp6Ofm5eTj4uHg397d3Nva2djX1tXU09LR0M/OzczLysnIx8bFxMPCwcC/vr28u7q5uLe2tbSzsrGwr66trKuqqainpqWko6KhoJ+enZybmpmYl5aVlJOSkZCPjo2Mi4qJiIeGhYSDgoGAf359fHt6eXh3dnV0c3JxcG9ubWxramloZ2ZlZGNiYWBfXl1cW1pZWFdWVVRTUlFQT05NTEtKSUhHRkVEQ0JBQD8+PTw7Ojk4NzY1NDMyMTAvLi0sKyopKCcmJSQjIiEgHx4dHBsaGRgXFhUUExIREA8ODQwLCgkIBwYFBAMCAQAAIfkEBQAA/wAsAAAAAFoAWgAACP8A/QkcSLCgwYMIBQIAMJBIjyk9HCScSLGixYsUGfo7w0bElSuXDAzggbGkyZMWZUDQoKFOnSsv6miQlcWfRpQ4c1oEAKJBnUtwXrwIWgdPHS86kyo9mKdMHaFQX3yMWWqJzaVYcxIpRSmq0KBTKQ3IShalFzxBvYKFSakKirJwL1Io4/Wr1KEwp8TdO7GAhrpD78K5UmfIVb6IB66EqVbwRyU2bybe64RuXbBDf5JcONkiiidfwojpIkRyQjtPhTJefQUOJQMZBcr4wiZHDhxeGC40nRMMDhcuRgivQEFvxgoaMN8VXGaNRTJMaFkqUUJNLQtoDicFIAaBiwvg5YD/H1HhTEU7hJezxlPh7UQAI04VMhPDjJlIMdSYiYDVC3nw4FVwQQUVPFDBExVpUUcZcAzW2mANVNFDRQQUUksMkURiX4aR1FJLHkrxUMF3BAYYoBwuFMCbQWcggEcZRhV1BQQkUWRHLKfQF8OOOtpXCCQ0JLWGCAEKOCCAbMjxAIIrEgTAEBAQiIQTxlVUQQn17cgjjz4GoBMABYxQpIkAVoAAGdod1ORFSRSioZZZxlkCHDrRgMR3AOaZpwte8oUCJBfCuSN+9sVwShJrVkSDHEbqqacL/PHFAya16AgnofTRcoBEOMmAQ6OOXiDeCPwlqpMDc9BiqZaYxlCCBklR/yBmqOFdMAKIiGkwn6AY1mdfCSIklQWRtA5I4A/+cBpXBCWsOqivZpTQRU4M0UAAnqEq6YRufNHQQiG8YloCHqZaJESZjIoK3gMJHNEZGM/kGGevahCD4FJnXIDAgC4QGBwBNXb2hSaW0KJqfbSocQBk5V60RBi/GchuFsp25s8SCNzhKrh3IBEkABUrJcMPSijRQ8N70UCHGGuAEWSaFscs88w012zzzTjnrPPOPPdsEGcoAe1zWQ4QQQQNIQ+dkANPgPGEqTR44UQEEQxAhstKFwQ0GEyQcowwxwyycGQKCRQHBQVQsAAEahcQwIRZK8SQCNpwMswtoogyDCrrIP+RLEFZFLBAABFAwHYEFBAAQZVKM1TFLTOk4skMlE8uyicVECREAgtQULXhnkOwQAEDvDw0Q2EIkorqlLfuiTGinKOFQk4obvjtt1OANlJxz5KLIMC33vonn4BihkA8EEAB7sx3PnjSO3shzOrBC9/6Ld7YsREBa+eu+/cQIB6wzw8MA3wqqVhPuTGe3OLlEAt0z3zzyKIc8xUkqD+8J8B/Ioc/8JPf/G63gPoNDQ/5098MPsE/Qdzif/Ab4PwWMCH7WcwFnJic/hgIPE+IwR88iEAAlifBACxgB9DT2Rm+MQNRzECDwvPEOWbADHf5Yw0CnF/i6BA3f8SABC6EIeX/PmEMYXBiAwP5gdokCIECOME9WfPCMfInxAV6QhSsQNZA7JA4w8WvcxRIgBNs2MMdcGMYxkCf6tBHgm3gKlkMEcIOEpCAAhSgiQtYg+l6eEMf4M2Ft8iFKGZRk4FwBgAoEEIXyEAGMXgBbnwkiKes4QYgrIAYBShbSSxoM8lwMpKgDKUoR0nKUppSZ6b5JFx2o8pTJkYGPBDCFIjQyqUIAQlw0EMBIEkWHtAhDAOgmhPyAIYU8iULfcBHOZwRDGC0gwPaE5pOpuAECgRgANgMAOHyQITOMGQE6IiCKYJhBEc4wgpRcMUOYGYShixhB1XDpjwHMEIyQHEvDEkAOKRg/4RwvOEN/TQnLqgxraTIYA3anOc8C5ebxPwgEaYIhz//+QZzGsEIuMjFPVGiBAgo9KPY5EEtLYKJYlD0ov+8qBGCYQop5OBLWognSOVJAcOMlCK3sEU5UmoEnpYzGMXoRZ12cM2Z0tQ53WpHMAAK0J42tZzd8AMLdHIEoyo0AmEwplLi4A5bOOKkKlWpI6SAiD2ahAZhKKpVIzA7vvQAHaag6FPDGg4r3ECrFHEAQq2KzQgUki83sIJT5xrWYggiKXTwqFW1qcWfEYQGBzBGO9ABgz7gQG4TUUM15BrWizrCCH5oQVKIQNS15qFJN2kDMDxABQ8EIQgCCMQM9JKoL//YwxlgDasj/ECNxubkbEaNwA6sQhFIsIMKNjiEDQIRCBvwQgD2MAxFNFGMbvhTrBe1hS1ksRQUZIFw2dwBNikwACFU5AHiMMFym7tcGyxXAK7YqNZ86Aw/BMOc9wWqI+ZAlh+QgWpoo8AOvkDGhNBgH8h1r4KbewjlUkET7xEIJUJhilXgIgrVcEYKYMPOnDjgB3GggxeUQEuLVKEV/VDwgpO7XA/sQwYd3g0Ic8ABZLCiBC6AMbcSY6peCEDFK27wIaCxCDSt6chky1kuYgvk9io3ELwIxANcWZA9/LjJDF4uL2yAACoTRBgC2DKQsxwIaFyDAl4eSCwE0Ar3HiJqyyw+RCtWQdw0Z+EXCnAznIUsAGGkmSB8uPKbWZzcQ3wAA1/480CO4AoBKHfMCoAGHhRtE4k8QQetoAIveAENXphAAJ3ABKUF4gCNXAERDCgzLEzhjdnhtZSSOQMExGjDHcdlF+Mwxy4CAgAh+QQFAAD/ACwBAAAAWQBaAAAI/wD/CRxIsKDBgwgN0uDR40jChxAjSpw4UQkENgYMjJCzxuE/ABRDihyZkIaTNnpy5BDBss0DMCRjyhRJQVaOESwzGhBRRUSWmUCDGsyiMiNLETtzIBDxoMc/f0KjxpRRIYfOqzkNVBEjtevIMwaWXjWaM4ccB17TShRTBcFYskhFIOChtm7CAW3f7jwqIseTp3YDD3RSRe9eliuFCIQq2G6XKiP08r3p0B/jxg8d/MjSpUuWH5cfCmHqNqtRqzlAQPQHEkUWCEhwBDizGDBQAHFg48CBJDaZJR8hLmhTGqnOnG3QSEQDZ8OdO5AgMSkFM7RMLWyQJNhOgAAIOQT+Qv+cYsCq6b1tCkAE4C8CkzkHDmyIv0F6GKFnkIBIAKK//wQ4EDBFREQVttNxslwAHERiMJHGARYc0AKE8THRwk9AEaCffxwmUAAITqCVmRcXtNFGT1WkNIBHD/VQxBzzxRffhC20MAceMsyEhhwc9uhhAkgMaJ1BMoARQAIEQPCPeBFBMEeNMs4oYY1pkDHTDjjw1+N/ILDRRURDhtTGgxNGSaOEd4ww0wL7befmm9vhcF9glGwQY5QztmBBGqXMVACcgG4HYmBEaAAlnlImscElM0HQZqBuyuGEQCCpVWeEiEo43x1VzBQGG5C+WcGXgVUBX6ZTysfGTHRUEKqWCQD/V6laZEBSJqI12ojhSFDJEIF2oeKwRphdyfDCqbjW1ylQPfQHYAEEILkdEhE0BpISDaShZ640alKGYkHZQYAcgsaJxAALYnZGGZowYaEFLaShySVMBnWEFgX0BiAEtAUXGEgg9SAHHgfcwYQFlFCQY1o92PEEXQPN+m+lRAyBhhd2LEwsZhx37PHHIIcs8sgkl2zyySgntHHKIgOAwssASMzyeEP0sDIRYIjhRBhhiNGvvzM/xdo/SsARQyU+TBJLGwMCxpgSA0AQwdQDUEDBDkEXZNk/SPjAzCBbhM0HK5VQUNAQEFAwQABs7+DEAAtQ8EPWtV3QxyBAhBACEED4/xDCIH0sMNAPEVAQwAD/sD3A2gEssMPCWeexBR956115CG4M4gMdT61h+NRsh354BBBwTvcdvmxxOd97591HEgJFEMDUsos+ewBq0w2GD2GvnjfrfGAxRW60y7748aLTTYAvfPveut6DcEVB7bbbXm3WD7DSvN7cW873IBGcMX311V8ftAF9AKF699yrD0QfEMRhOPnWp8syBH2EsD77rU/exREBSBz9ArADCDhBZihTgg/40Dv+ASFzkxAIvgbItgIMgW7/aEEvGsg+vvWiDgLhAe4GSIE8IDBldJAAKfbXPSCQwhJN+4cQCig72q1tAWKAXNBAsoYOsGILlrMcM/9mQapK9WANC1gA6Kb3BR1iEA3LYAUrfEEKUjCDFXO4IEi25g8HTAENa1hDHuhgPwwOBAURaEAsYoAJPEzKNmYUSWhERKk42vGOeMyjHvfIx5RtUSAOiNnKOMaYQVIEAA4IpEFO2MeCoEAgWZCFwR5QRo9NgQIPuMAAIBaUWQUAFY2gggAEEIRVZGNumKlUHGghChj84REw+MQdwDUTRP7DArAYpS516QglNQYqSODEKxiRiUQYExHkEMYa4BiTK0BDAB8Q5S5FyQUtCAYqAdDBH5qQAhh4MwWJSMErhrGrmNABCgJQgAd2OUoPKEAAyqhjXZYgiFBws5veNKY3qeH/C5mAhA/sDKguczC0ulyBGt5MqEJh0E1X4EAmKACGQAV6DmZ6pQ+uwOdCv6mDV6hBJmDYxEQDOo+PMBIoKPjEHzaq0BSkIBTIkMkO2DFSdkbDkDIRAicYwVCX+jSh3UTEOZxIkS7ooqa7VAehcsFTn/40oU14RDNOChEiSAGpuhwD0NLSjD849anebEIoOjCTYQjAA1SQJjvTKoA5bHUxEtNANmDgChKsQD0WNcgcyPFVl8IgEQl9BaNIApIC8GKUat3lO6XglIMUFAHvUAUsFLEIVUyjBttA5UPoYAivfvWvDHWFKCp5yH9sAamtACFCYvaPA2BAERiAAgNUUAMG/2wiGdSIQ0QoEYpwOvWvKWDEIy5Qy6ekArFpVWs/nmFSRspBBZ1ggHSnK10MTMMQdExIC/7gCt92MxMIhcNbY2KGKKz1HgYYb0FCoYjaUne6UMDAKDZg0ocYQBCIUMYrlKGMR6xDNXmNyRMsoI09kAAZOUALaxOCgE5gwL3vxYB0VfGKP0KEDZCYRCE2UIBH4lQiC16kegcSgmS8l7oYiC0hrPAlRa52NSXjxjROPN0UM4AQDCBuIwfiiVHQuLoShgIUHrDjgSADFrKlsY0JwQVfFlkTqpAwA6Rs4xRjoBPA0OyOwRCMX1QXyFOW8DSwUWSCWAIWXw5zmBfRDS+U2T2kRABEMmSb5BTXQBe/GOybQaLTRYxiEypuBCykwIQR7/EyehiDI2pAiGrwAxnKseWeCxKHxnGFIB8GSkAAACH5BAUAAP8ALAEAAABZAFoAAAj/AP0JHEiwoMGDCAUCGCiDR48jCSNKnEixYsUpYgLgwEEgABgZ/gA4sEiypMmEKLSwQZIABAECCRJQEOJv5MmbOCuqdAliI4ECBZAUmJKzqFGCC8/giAkThMsCPwlEAHm06k0ANFrG3Jrg5c8FOLJYHWsyDo4CXJm+LLAASYSFZONO/MIxbde1QBPwkMs3oZaldr16TUC0r2GCXQCnFey1R024h/mecRlYcIIFNB5HnujgBxg0Weg4psiDwFOoUGHGdBqmoow4WsKQQTMFgG2rT5xACBCBAgQKWhxDPghgDZKfqF8yTYAjTkS4aCqIEFElhwgDBGgOv4qGwoAAAQZ8/x+wYMGTiUcgnE2umnme7QYBhGlj4MEIBA9cPBDhYkjIqwINAYF44PEG3gC/jRaREj1BtRwIbAyQmURZ5GAAAiOMYECGGV54Xk5HGFigiAHsQAEZNiUEwBQRrNQVhAV4QVVEPIwggn0G6IeAATxqeAEKOZ2xwIjgRVCgeAvQNNGKXzhBhhZ07EWRFjngp6GOPF5YQQ5g4FSckUSCWeACXvynInwWLXDjfRuyyeMDD+RAAU4yDCAmkSNS0IWZcTkgxwgPZJklAoTiJ0ICdOKpKG9aGEaDCxoKymOhhhKAEwo73LkocCGhaRQAbLgpKKX7WYpTHkMuGgAFEUAglqdHLf9Qn6QXFjpCGzsAOEQCqoZnZ2F9ZdFGpKMSel8bdgAoAxmajjgAcCPBapQDOFhowHWTGpvDAtJStMSqvX13YAQE7EDEZj1ckEOGLrhQaA6yXKBgTkJEsACrEeQLwQJiQBTZQj/IoYeV++XQBhI/8JkTDV4EcK+dAcSxULdVLUREGBdIt+EFWoAkklUoLDHFD0ekSHHFAqHwQxxK9ADkZjDHLPPMNNds880450yczjDb5sDJPI8FdNAm8UBGAF2kSDRFDvTwxBLwfexPAiwUM80hqgQjTZkKL62QPz+A8EISB5QBxwIJ//cyKf0I4PbbAmBQRNde+xPGBsTMwQQTG8z/MUcZjX6djdtBKGC44R8IAI8FdX/tzw6apHFACxtsQDkTLVjwxUAHKCCAAoUfrsAHno/ihGZEJzVHGphTXnnlBxzQwHk0OCNA6KIfHoQAw9DNcxWY8O3665ZbAEkF/lyCe+6GB7F7I2c0bscGe1tOfOUW7F1EDyXczrzuhVMxAupBrwGJ5S0MTzwTFqSBRiG3L8+8BwKk4TvOEMyxgQVkX/96C3PIA/yc973D0S8J5OMZBe6Quf75z3KaIIMa4ldAw1GBCsgbGsy6oAn2OfCBaQCDHFpBhQqCTgCrWELjlmCBOzywckxIwx3wAJJ8fK6AhRNALxonkAtg4oUbSEMa/zSBKH/g4Bo3/J4AjKCkxv2gDC58YAvuQIlzLQQS8PCeAjxgOLdFYU48FEgcGpAGC7TgAHyjnibw4B+CwMF2cHMbNchwP6I9oQ1pgMTq5qAJSBjgQ/8BWCQyUQ9TROMTdRDIz8I4EAd84QKXuIQekICG4cBFadu5DSOlxshOevKToAylKEdJyk5NrI400+CS0KTKvmjyKHAZggjKhgMV2owHXTDSF/xVFNvAhQzccIQkRtGJDESDA/MyzG2mAIcSdIAPfOhALUTAy6s4AEh1KEYgOgEFQnSzEclAR64iMxIJ+mILHZBAB3wQAlaYoY1XWUgbCKELKGAAA/ZkAAYIwf8OYGwuMmhwAylCEAIJqDOdEhjEJJRQlDgAQxGSkAQGGJBPfEKBHXs4JV9oEIk++OCjBkWoBNzAjDm0kiCTSIY+obDSid6TAYRoxANQWZQCeHQF6gypQX3gBiCwgo43QQE+FLHSlurTpdNAxmE2MAgf6DSnEvioD3qBB5wMwRGLuCdL8elSl44iETTFiQNOsQWcQhWhPA0BHzSBEzJEoRMMqAE+a8CAur4UA6pQhmF4oAYfrICggA2BU9fZATfE4mUm8YItFhHXudb1qPocxR8MI4MSAIGdgRWsOtOKiZP6Qwb3UEVjoUBXu7r0Gqk4jCa2gNnADjadfNBATtaRDLr/3rO0R72nKhjnKbig4ALP4AMzsHCANVQEAb3ILEGdGtUQ9KGIJllIBKAg0YnK1bQ1UIQ70naQRVJgG39ABCMY8YhXTCAGe1GaQZTQgYFmlrk+6MMpzgWgSiSDtI21KxQ6gQERnMkfBtABIk6ggwIXuAnKyEYTE4KEc75XAisAAinGCSAHNGMUitiENwlBiE1cAwMIlMgOJpAIHUzAwDcwsQ5eAQT1HsQFfOjDFoAAWDf0wQ1ssAomgAGLQCSDHeJQRShcgEoHMOMRJ5iAkpe8ZB3AwADPEUgAaEGKXvjCyoOIhXHDehE8xOMc3AACAhQirQjAoAlMTrOJH8EMWGlytFk5wEMdELAGk8FSRRJpgStMnOY0p4ATyfJs0E7xBz73eck3OMEASlmQSRT60EzWQRMKwGiCEMMVN4A0ogGRh0oPpAqhSEGBUzxqJRc4E56gr6eFQIJHjBrFiHbFMzz9NQso49WlVjIjhpEsWtuGCH14xQ1SPAFiT2C8F6A1mf0xBSxk4g8puEETYJAJRJCgCsoeCGQS4IZcTKAJLBCGJpzzymwPRAhrCIOMtG2YXYzDHLsICAAh+QQFAAD/ACwBAAAAWQBaAAAI/wD/CRxIsKDBgwgNovi3hAiAhBAjSpxIcaIQLWEgRAjg5AyNiiBDipxIJwKEAAEGBKBAYcCUfw9HypxJEc2CjQNyogxQIMIPmkCDEvT3z06EjSlVojwKgYzQpzSJBkC6sypKCkOgahVpZ4FVqyopON1KdmIWql937thBpKzbhF/Qpt3Z461dg0fnWuXxzx/Ru2/ByE0bYYeMvn8BQ3zCxNunGdJKOYAZMyEFlEqtHqWAhiKAKV7QdH7SV2utDAJSq46Wo3RCf3kK6Pya9+drgUJ2EADBG0SCAD8Tz/QnSPUHBR6opOa1bCIFCJmtUvAi/KA/NAkIJMhOoMACHAWUuP8e+RBIauUePKhWDu8KzIgLLg/YSH/B2IhKdhfQTqD7fhAEvCTTZAVAo9qB6wkgRV2VHfSEExpF8BwEO2TRlkQL9Kfhhv2BEAFQuSCIIBUkCpDGexAtlEUXoXmxRE3gcdhfAfsVAIJ4Mh3hh4gjKjeGRH7JBIATICywn4w02rjGTF0swiOP/Iy3FQARJGBkkjRymMAADYLkhDhPikiPXTJk6B+WWu7QZUVeOBnmgfkIVB1UVZ6ZZJprUoRCPW8KQIUCAgjzz0JuhZEAlnf2dygOWshEVB99oifCP5O5NQR4iB45Y4CO/gNGDW8q9woAeWpFZKYb4nCfSKT+owEvAgD/SuKs6nHRhWJEBIBDd5sWkAASFPAlZEwWjPKkERQoRlQPFOy63X6/DvDinCMNQIIi6gkQBBfYvERtWQDQEBcBOBwKQRaEfivRml7ocUcaCNgGgLpbVfoPD0IIscRhivXr778AByzwwAQXbLB1B/dLVKkJ20VvwzQdoYUTWTwMsUFELMEDtTFRoI07puDyBjk+nHHxbTzkgYQLD7iAQxdHUCqQAw9NwoUikmAAxSaNwAIMHFKeLFAWLlSRgwhIG+0CHQWFoAghXGSgQg01MKCCJAy4J7RrWhggggFgez0CAjnkAMZAcECBgQpTT011DSr84sySQhMVRxVgIxB23g+I/4DAiyi804jUVLvtNhejZLM1TAW0AfbXexuAgAh67PAPAlBYbXjhhW+SDo4nP7E35JGX/cARxMAi9eZwv80ABhds/UUVY3sdedgj5KAEJqpE7XYNhBNewyZlBH3wF44/fnvebYCBiSJs/x4921QT4h7DA3fh+Nc5LC+5LF5oogjc0k9fdQ0JbA2GHkgb0L33ui+QgeZvT69CBp3sI6zQInSv9+0IIFvs/qEDRQiPfNODBRYW9w8K6CGA3kOAHsQgEAhYQRJu01z0dNEO2yxuBHjzXhvY8BGBFAEDi2AA3KxGtWvQw3IM/IcSRtCGAOqtfXqogLwekgN1qEIXjfhFI/9wBohbxVAgU8BBG8omgjZUoQoFqEulYrKEOeTiFfpwxTf0IBAA2CuGcRgACMATBiVMpkvC+SJijmg8NrrxjXCMoxznSMeDBWkg2PuXxUByx8X1MSh/mQIEEFCBAcRsYEQYQhfQoIQSAkU4dDjFJ2Dwh0c0QRBpOCRggvQDENShBUzYwAHgAIG25BEi/vDiP5CAClckIgUwiGUmQqGNLyjrH3TQwBxacAALHKAFLYDEFUgzHIEU4AaM0IEOmpCCFDRBBzdAhDGyApghWIAJFkiCBXx5gF6mAQ91oYkQjGHJFNyAmU1owg3O+Qo3AAYFl5iDNluwzV5y8w4GAEoDXnH/TnMyc53OvAEMMjEAmblFDGnoJjd9Wc9tpiELM3FAMx6xTH+a8wYBbYIymmMXA9zBngoFaTbngISZPCEXMOinSjGqzia4gg93uYIo69lLkPKSCZOSiRcMYdGVNtOZf5DGKWdCBEocQJvbTCpND5AGOKixInbgRCYq6tNmNuEPrLALCvAwU6UmtZtFaAEXZYKCbzBCpf/EqFURYYm7tIEJ9vQqQy2QBjnQJBLKcOYz0wrLcz7iAW2c18x20IYN3KEMLjAZZSASAXnKtZ4tSMIcViWSLuhgqnu9KCyv+g1NGkSwXbDGFvjAhy2YVgKycGRCltCADTyWmy2ghGpBAoCF/yThFer06Q0SoQMCdBFhAZBAH1ZA3A4YV7h3uBBE1jCHuHo1DWnwQlAAEINH/CGduYVBKG6Qz98W5CFemEQIOkBcLBi3uH2wAERUOYANJDQJv6Rncyn4FD0YAxGh+MMrQpEJZARgIpDYAnEHPGAJGBgIH5LIF2SaBvfeYQOyUOweI/KQIzxAE7SoxQYWIBAU0EsLPjCwBIgrAfKuQMRu0ERF6LAANuAgAmcbyITX+62HYA8BgzjxiE9sYgOfeBJCaOO6GvYCPuiYxD0eMXltWceB4GEL5N3xkX1sXCM2+R962EKJSezjI3cAC2cbKgMLYGQSI3nESi7ElQfyAzX4wFXMPEbzCgYx1isThQ18kPORiQsENXjQzg6wQJ7Pu4LzhsAN9F3zQHiAByBouQM+8EEI+KCGZNFM0YlZgwUKUehKxCAHL3EAoRRdkB6AgQ5KIJSYhRIQACH5BAUAAP8ALAEAAABZAFoAAAj/AP0JHEiwoMGDCA3SIYBkAI+EECNKnEgxIYCBGtr9CiKAVzFhWvw5qEiypEmEI3mQEMCSigeWAn4V8XfxpM2bEgHIgCHAwwcFQBUE4QivDM6jSA1uETB0aFChCgQs6kKzZtKrJbM0EvC0q4eoqURaxUpWYqyeXZ8G+SBARY+ycCd6Ypo2aBAFVKAViMs3oQ60dYO61NO3cEFAdAMDFfDBAE3DhrclViwgUJiqkCX2KMXH2zEscsYmrMNV8V0B4UZOXHIGDJgzSwSKxpnm3iZVinQpgpIIiUiINKKVrhuEioBIOX/kiUABgnMITmLPPsmqUacMKlRkYJCh0aoDEkXY/xjuVUA7GRLpUAjQPEKEAU4gBLCDE8DFWoswaM+ePUOGTVyIINtBF53yEhV3eTCUcabQIZEQAQQwQIQR7uBEABBQ8JZNI0WwChT8hahCDVyogs8RjxlU0wFvwAQTNIZkkSJCMjixHoU4DjAABGGoZlMzuogY4nYMLELJjATZJxARLQwzDzjKMMPGgBApUQCOWE64QwH02YSCPpIIyR8X2o1yDpID1TRdmhF1sYCOWFIIHwRU2UTHG1D4p+eeenaSSWEAhLGejoROiGMEa9y0hi3c8eloJ4gU5oATzBUK56GJ2gRGORg46mgjE6BJFhnuWWoohYjehEIo13maARdcRP+hCiu/8ZVHADuYeuoACzh4kxqKuOrfiAxcICpWSiwgoalyaohTHOC06mkUi+QikANrIgWAGBToSiEFXyCVQzWdiAjrKouAIyNkRNgY4YSERkDAACjaBAAKF8FhyiiEYFBDDRgQMso7O2QmUA9kKMsenAuE8dBNDuB7ERnrAIOBJFBwAc4Kb2ULlwNn7IChfDtkgcKxJSnpjxIPlKEBDg+rDJlqKBwxhRA8qOaxwTz37PPPQAct9NBEl7Vz0WVhi/TSTFtEEhFD0NFl0xQBQMQSRMxm30VaxHDMDLekgswBT9DkI9UE0dDFjgREsAMdNAxYEx6gJDIBIIYAckMTwoD/gPZBQ0CAQwIEFFBAAglQIASbB6QwhiF7sMACKCzsocMefv890BCIG2544YcnULZAOJwwRuSRS45K5E144uDRQPeAAwgFLOB5AQQssAAICxAhUjw6oMICIKlLbjwM1qAs9EVizF777bcjQVUEJ6BOvPGS77EHIIJsSPURiOduO/ShE3CEHolAzoL62GsPyAlkKB+0HSCAPv7tBOQOghB6pGAI9gDM3himBLueDWFwhbuf5woHAQLwz38BBOD2/FZAnp1hcAnQHfkKkCEQKEEPMPhfBI2nPTHID2hPmF0GFfg5w4luDaAoXgQNMYZj+A5tNFhABnFHvsLhgAIn88EN/0aYvRQ0QHP+0AISIHC4DRaADb7KwydOQMQmxCM2aLvIERrIQ/LhYAcnuwgSUHEDQGBvdYnwBho0V5MfLAAJCwBd/gogBwrc8DERkEYiYDCBCeigCWPgwBmQSJAeDKB+CQAB4kCwBt9dRGU0QMAkeoGNPiyDALKp4PJ+4AUn7GANXxDCI9mULZmxUZOETKUqV8nKVrrylVnM5LWGhkptwZIkS9DChNCAnqDJYApnsMMU4paUi4zEDi+oxQp84AYsWAMBdzRMTYggBjY8QAQPeAAbtDCSk9XHH2IohA8kgAUOcAALHQACJuKQmYs8gQ05MEA2HzACBFQhAVi8CdcmIf+BSpjznxyoRAee8YPM9GAEIqgAPbOJgAfY8wL1ugkPYtCBSvgToAHlgBvKYEq4oAAEOXCBSOfJ0BHkIAL69IcctoDRlmJhBV6oVVyyUIURkDSb9RzBCAwggqmZBAB38EFLW+qGHBgmAvG8KT0RYNMR6OEyNunBKco5VIC6wQKAAkFDlZpTnYoAAjdRwiRWUFWA+mAOAFAaXGQgh60qdZ47BUEtBfIDNXSgrP90AxMw49ELiMCmb8WpARYAMWLcFa8cCAEcDLOAHLg1sAYIAE70AAR/YuGiAJ0FBzrghIk4AA0E0KkcAjAFviZECzlYaGAdusabxGGzs8BsZt2wjGj/FqQmQ5BFCw5wAAscYANFiMDJzmYQHlzgsTc1KRJ6eRM2AIGqGK0EFmZRJ4h8wbcNKEISitCAJFggDSIgJkTAUAXkwvUBSjiKfargBh+s4LLlXEEIOCBZp9nBAhvQQAOyu939FiENIzgaGkZQU502tbwxvUoErJFON2yBDx1gwrogAoAcbKAM+81whrWbBKqsqSZKQAICciCCNlTBAKLDykVoQAYElKIULvDCKCEyBAto+MbZbcABBERh2QihCzsIQx7SS5aOkhIiAZhDEnB84yLUwbYkIa7QCNCCJTN5w2Ug8i0HAoIWXFnD3GXnXJm2ACZw98vZLcPipOxKMrTgYsxMXrIFKMHcLfuDCJSwMpMtUAQmYNLOBBGDma+chANQIqKAFkgFmCDnBmB4vwdIwiATnTY59JbPDbBACzZQh+pSOkVguEAdMKwBOEBAOmNOJQ+E8IQfePOER9nFOMyxi4AAACH5BAUAAP8ALAEAAABZAFoAAAj/AP0JHEiwoMGDCAcCEDiEAgEyMhJKnEixokWJC/3lAAXOTxRH5LZkuUiypMmEABz4OyItA4NqfvxUi4JBXil/GU/q3InRHwpukorh8mNFilFTXHDJ4sm0qcEYm6wUnSrFj1Eu9MA43crzDL1VUsMWNerHFgZSOLmqJWmhBi6xUo0atRJF35G1eCv2IQQ3rlwp1bqFyUs4ITcMROH+reqnQuHHBbdh6DtWrlU5kDP7k7BJSt/Fxd5o0UxxIQ8QB2ItS7JDZUqJIrhIsWUF1+yYc00x+KPyIICcS4o0Q8Up3hUaCp0+kMZiwgnnoLY4SZsQxQ0opmrftkoWSoue/iBZ/xFAvny5Ojhz7kwDyBAJUCRIcOI0JpcLikicVbNl2xZuo1BwgkJPyJAXxIEmtBKEAPBY4tQLE6AS33vxzbcHKAtQh1AaqyBmFVFS4PJLO0r0hIUACrRiwgcfmLBiKwJ8cABTX3DCwoQ4xjcMINIQoeFBlyDikgoZqIDdMUP8aNAAyVDh4pNQmiBABnbwRMwNOebICQljIKGkQL8tRMQlvgAygSizZOiTegY1E2OUULYCYwk7yYDNGFlmOUEkXyrEJkG/TQTMm3BCKcAjOymRint54jgGH3069cQmThZqKDC9mTSEMYw2Gh8gfTwGRiOVWrqiAFb4eNIU3HTqKSBYRP/alAxcUNGiqR8IIM+fFgEwyAmeTjgBE7I2hY4Apj4pACg8UYKlp6CAgspgvG41CaGWKiCABjr9NsU3eFao5Q2WhFnYFLYga6oA6kSkk0oUDDMBKCyAwgkqqNCrwzElZpZDIMi28oGKQbgoAAY78JQRCJ/cYMgeJOB7QhO+jJSZSlcQYiCLCwrgBwjFVqQSGMQIsgcqgICyjgZ3VatWoF2IIkmuAkBTAzZJunxRoP78MAAbSIiBHJia5RSHHne08MATpDXt9NNQRy311FRXbfXVj/3mQKZYd+21SSgIoUQPOn9dEAoyyKBzHG1gYsYpsTBRQQ/pmW0QDWCIEcbea8T/MaBBSJyCxSyWqKHGLJXEMJ3dBQmxAwERRB5A5AMwLZBKF2BhSQmcd14I4WIwPlBDEVAQwOmnD0ABBUIMJMYkkxRSiBqc0855JbG0zngPC1AQwQCopw78AMjJsAEHnSfvuQQ5MA6AFhEEAHzwwlMw0hezGK588mpYYs1dZi9BwfDUpy45DRFgsT33lhROR91dKwHBAPSXj/oAEAhRABaFlKD99u2bRMJC9jQljG969EsgAinwgwJUwnD/U57t8mC3IcxPer9TIP124AQIPAECK6id7ZQ3OzUkyWwfHF4GFSg9/AlhCP7z3wg7RztLQMJdXyPC/VrIQtXtADl4kAAE/9dXiBVghnFfgID0eKjBBWjFH2eoxQNn2DkOMGFoX1vIEQIwPiYmkAJhGJBKnFAI9c2uBP2rBBYwUSW7ZeQHTljACicXAAKIQVUZQcMdsMCBScyCA4OrQ+vKZjUerKGLAYAABCjwBSxSR4dwSMMdDiCCLyRHdDhxwBLoIAYxdIEOS/CNzniGSUCV8pSoTKUqV8nKVopOaylBASmhlhJCvsyVFyFCHNDgBSH8LWooWMITfsADrm2lByC4hAaSkIQGtCECv8yMmNBAgQUkoHcB8EI0d7KQM7ygmWXQgDiLYIEcTKFo/nhCABJAgAW4swAJwMEOeOCUM9ShDHgQpz41gP+HJOiBnplZAgiu6c53FmABIAiAqnZCgzY0YJ8QLUMR2GBMvDggDEiAQAEgUNAFFOCjINCCLRMSBgtA9KRlaEAc4IcXJeCgozBdAAQIgINzdgsBSTjpSQ+QoZGeZA04OGhM3TlTHKBhJ0soxUN1us8kPCBrOyhANYfqTgIkIHQ6EUIdlsrUcSLAp2CjgFBh+tGPLoAAAwArQXiw1a7q0wIIeIwDIJCAsRa0rFXF6kkAUIWculUDFiAAAXciBnbGFK8zHc1OCmCBfDIVDw0owxMpooQ8UAACO+gCQClyhgRw9LBmTUAbs0qJBhShDOHcJ2qLkIOKGqSBCHDBA0YwAgP/POALrpEIDaYKWgIgYQCuJYkYisDPk0KWEieUSBxcYIALVEC2znXBCCDgSIQogQ12NWgCEvCDpgAAAswMJ2rFeQANdIEiPaiAASpwARe497nPNUAECNlZgsrUoyAoQHKb8gVZNKAFFmjBAYrgAqaNEgIjqICCF8xg907WNwJRwgAGStP8OoFuW1mIA7IQAQKgtV+vSYgQXOBcBptYuhCYSKAAIAQwfAENYCDbYDPzBdua+MYVQEJ1dxZcqIUBAQ/AcYMvYFNcCiQM6xXygklMN7V+TQsiUPKSL0BPJ3ttCA9wwZKX7F7nsmGbrqQBGxKsYC2XWcsXQAAZjFwQL9g2SrpbrsAILgA+NoMpAAYYgQsQwN4LODfP/bLzQIiAZBFk+bkIMAAb9ivogUzBCUh4wAMukAAt4LDRl6RBD35ABPVY2SK7GIc5dhEQACH5BAUAAP8ALAEAAABZAFoAAAj/AP0JHEiwoMGDCAcCECgkDxk0MhJKnEixokWEADIKTBBC1JgJe7RhinOxpMmTEhcSiZHiD4wJE1JkQnQLCcqbOC1mRGEp1I0JOk7A1HGDkQ4QOZMqJbgQzx8dE4TCnHoCRq4hS7PmFHILxomvYMOOeUTM30KtaC8iYBQ1rNsTKQQRSUu3ojVEN9661QFoTd2/CS09CqoXLEwIgBMXLPFnzJjCX6MGUEzZ35w/bSHfAHWmskUiYdrUuTTiy0KNCQukgCpV6lQdmbCdxSiQh6wt3ratQBDRAeqcAHCc2tJny6A+buZ4MSsRRa9HkaNOhVlURMqFDdwR0tVo0SIM5Kz//8b5YgsQCRI6+HDTgY+EyRN3GErRevoERG4cUJSw6VcGLv9lkMEvKiyzFBuk+JAeeun50AEQHeQh0GwGldLEH018dANUNyByjBAUPdNJDQAKaCIXUGBwRVJDdBBCB+l1AKN6bviwhTURUWgQEs0w8gcjmcw0Ri12UJSHKYSYqKSAXHQCzhM5lTKIgxK4YWWDbkjgAxADTLQQCmzEQkofKxwgoT++SeTGIku2ucoiBt6EwjNAUOnDnTEqiAUfLzBH23UShSJJFFwUaqiSUSwiCk4/lBACg5BGusUdifUgDwZcRKHppiVysYokoeiH0hOTPBrpqUBgkpgS6WC6KadMcv9BSD403PSDJTCeKmkLiaFgDxSZvkporL8gouNFAGBynq6Q8tGGn3VNMKiww/4XhS7f3LRQBX0wi56MbnyBJmDLNEJttSpEsUkpOS1By7LMDlLEsWn1gE8jqwjb6SI3RKStP1+sAOGMVLrRBy1QUiZHMUkGy2kj9YgBnEBOTELKFuzVCAQpyyhRmX5V1KMKsIVmwIAi6lDgDwpJLfREKYW4AUSdtVyQI2VpZpFNOhlsQsgq0fgA5Xj/+sPDFwPskIW/0AJG4RMVlHEFCD94ZvXVWGet9dZcd+3112AnRm/YyGY0Ntl0pZmQDFNMccTZaBt0xBoRdMFyQkLgAAceUrf/EQAPaMLNtdkCRXCLH7Dwokg3wkhsUABJMGFBEUUk0cIBdSwXN0EoLFTJNAKELroAi6g6WwAbTF5EA6yvbkEDWWwuUJpbCEBFEB7krkAQCgjgQSwDnXHAAQ0kkUTrDZShAeZLyL4QHCYIEATuuXuggAdBCGCDHGbpsYEGZSSxOuusl1HEHAnI7o8M/Phe/fvWZ/+IP0O0kEQZ4Y9PfvFJXME02hUwAfwGqAAqiEMLYmBCA4pgPv2Rj4FFSFjcTiG9AcKvd1cggwLFt78OFsECmosbMgRwPQtWjwoCeAYZ7De5Dj7QAkmgg+x6QUITnlAAmBBDGijnwAd+MAkg2lwk/2poQw+gsBRCKALxKOdC1m3gEneL2w5sUEILKqB3jfCYCzbQww4yYQfq80cm3GfF3s3ALE8oA/Ga2IANVOF/cQuAIsg4QAGoYDkL8UIDmLC68ZWhAWmQRdXC6I8riEN6u7viBz4ggF8gYEILGUIbNrCBFrAwCS5oHiEF8oB0jE506FhA01bWBTa0oQ0iAEFnNkkQIkCiCcCwRTRAYQFIEkRUrLRIFMeVy1768pfADKYwh6kYsxGua8ckZtHSIgMhnCEOPcBl1lDQgydMgQdwXMoSyCAHEVRBBA8AwRqkKTaB9CALTqDAAigQAKVlEyd2qEAVDIAABDzABSMQQQI0Wf9MfwhhBxSIQAAGGoAIQCAM/MyJHR7QhnrW854ucIEIkFAryixhBwIdwAAIOlAIOGEuOZEBDkSAgBE4FKIuqAACIkBOuqBAC+wc6EY5WlDT/AsNDXXoQyPK0wdMYZRoeQIFAkpTgiYtAoO8SQGqoNOd8tQFBpCQ4HKiBQhEQKBFlWkEwIATGlzApE1FaURFgJjEhCEAGk2rRovaBZwcQQ4PaKpcESCCAphlqiiRgRPamVWZFlQMeD2IV+M6V51WIX2BPQkKADrTrG6UAlrACQAIQNjC1rMKYQCqVmDaV79GIIQoWUMOSlrYuOYAKxQBgBCyQAYyaAEMIKXIELDa16P/eoxRBjBABcAa1hwkoKUG+YETEkDcBBAAByBAA8vgxoMB0DarFHDCO0uSB3BW1qEjMIALJJgQJUAgAQsggHghsIAE4CAMFZVIHArwXI5GYAHclZMYqpADetYzu20YgQwnsgQCJKAA4SUAgAtQAAKIc6pxiEBM1/reAKB2KWBggwj0QN/6IjW1YfgvgTdcYPH6N74I+YEY1AkBAFNADAlt2cqUIAYnkOELP63IFAiwAA5vWLwEBoETLtKDIXjBC2dYwmm0kliDgAEENr4xjhMAgfSaBADA1VoXBJzkDnuYAEkl5pRrnOQri7cHmvUlGjRc5Q7/F3BF3twQkLCAGtO4WcMcJkAAovxLGVAAB252M5VrDAJxKXMgSkCCgN9MZQATgAJH+DNBtAACAYO3zTUmbpEUPRAZ5AEExh1wAkBAgQdTeiA90EIEcEyBL9SKaJ+WAQ94cITpJiYgACH5BAUAAP8ALAEAAABZAFoAAAj/AP0JHEiwoMGDCAcCEMjjTJYhCxNKnEixokWEACKKSXJKAhAJkfT88AfAwcWTKFMmpAFny6AQPmJuYVWCDEmVOHNSXOhAwyA3PiR0GCpBwhYfYnQqXWpQzqCYMYeukDAVyCQhTLPq7GFpy9CvHbBwwCKUTxuBEbWqrUiBj1CwYcdKcLOFloy1eCvqeQo37ooVQzkMyUs4YRk+bvqKXbFYgpbCkAviGfQW7OLFK9BE3uwPAV+4lzl0uMrZIgovEJCAGDC4Yp4tbrBgWTGW8ewVPkgRS2uw5EIiIOacorVhgUnfSwHkecHkTpo5dzYYsLMTEqnatWVP7TAIgkST/lx4/0uE6A+iR4mkEfDnALzOAkw2WLDQ4sCBFmmKdJG4sEvQ2dnd5ksSvPXmTwswMJLCCRPoMMENjNwAx00qZeTPDvHZd58FB1iQBBNJDFZgQQn4AEQHgBHlQx+RLEHRC4/cMMYYJ9RoYwpNXKDUEwcwoaGGHNqXRhXuJRRGLFv0sUUIgwyyQh0ujkgQHXvAMCMgM2ZJIyOC8KATCHfU1yGHLZB5wAYtZEHhQQuhsMMLadyRxAgiOiDlQHMgMsEYgGCpJY0n/HFJTjJcwoSYGpa5oQV3gLBmb3eiFak/yGRiI6An8EmjDn/4kFMPZWzw46gaDvmoWkfMAMMErLa6pyFY6v8AQzZFnsRDAz6SOioTekAmhCgwOOjqq7GmcMxdKhGBB6K6djiHCJChIAgMNwxL7AkQ+pITAHrk2myHaRRwqlp8MCKsq2PAWuMftOC00ACQfHtAEvMpEdklrlQ7bLqxJoKDTkfU4e2o8zHxwKRMHaHNI9byOwYipNR60kJn9BjkfaIecEcdI202wB6WnjsBg4/MoNlSXhQBSQssb+DyHaV0HNlCBXyCCAxN6KBDE4yEok1SWf3ABiVMpMGEBS8MgEJpNylhhiiypqCDMUz0MG6FAh0xBBpe2LE00wX1EMADF4Th5dVgp6322my37fbbcMct99x01203mxM5wMMSNNz/XRENaDjhhcQETbEDDhWQTQAafVt4d0kCOdHMPM5EYco7rHxBUloAfGFADgiM8MADBhggRxx+o2VSLKuI08gmm0iyCCzdJFFQFwiIIProD4yAAAIuUPf4QmYoIkkNDDCAQQ0qMKBLI0wMpMQIBoxgPe8PuOACAmwckfoFGEiCQfLKK48BBp1ksJ4/EFRhQO+78+7CBVWE4TcAjIyiPBTJn28+BtfIhT964DsEwA97o6uAC+SArLoVoAb8wwD/zPc/SeCiC2CoAgKq1zsE+o50WLEbJq4xPgn2r4QShIIiLvGFKlDPgL+L4e+sZ4DW1E0CsCjhBMlHPhUS4wsiyF3o/2Qow9LZy24cgEX/dshDBkBBFXNAA+hmSMQYGgABLrLbBvS3vyaSDwONEEEPhigCEVyRiCPIARIIBzct4GITXWzi+CQRDNRRIAdnrGLoqqA5v30iGXFsYg2SgQ2B9OABuutdFauQgK/dLQ+O0IUJvagIeYjIH3F4QA5GJ7oZVgEJZ0udAaIwCiiMr3+EuEY32FCQJxDAAFUw4+dEsAAipI4gcnjHKEaxCF3oAhaL+MMATuWAITgBBCCAQBjsgDC60SAJMyDHPKhhjDpI6pYp4U2RFtJMbHrzm+AMpzjHSc5y6iQjEcmIncy5E0jhRQY/eIIQltBNwgyhFNExQAjVwv8DLUQABDhQzQAGV5r2+EMLt5BEEAQgABNkYBtn0MoPIICDBBDgogQA6ABCCZmFXEESDFVAEBbK0FUgAW0X+cECcLCAAmAUo0igQN82kwMbCIAKHviAToOgACoIAANOUIoMKIADAizgpRgtAAjEUM+cCMEPAlCAAnK606ku1B0zzaY/wMAGpHr1ohzNiyUEEASdmvWsOhVAEXTihKJ+9aU4UFNTU6IOAaD1rgtFBaEocNS3YhQHNpnrSWjAALLeFa0CiEZOiACBvvqVAAnYwebyAoZGCKAVhz2rAKTgPZXQ4LEvRcIwCzMFQjQ0szsVQDnYSBEHDAAEoCUADvpYmHT/2BW1H1hoKHTiha4WwLEvLYBFZSYRFEwhC1rQghfi0FmU+uMbhkXtQksAsIv+1qsWxcEOHJmQKZABAgGIgHjFSweTNPAgA5jGaTMrgAwcEWtgSEABXIpU4S6AuAgRgngHMIAABIC/AYBAHriLECwIgKp3tesBzsmeL6hmvtbNaAFsmJAj7IACAPZvf/0LATSwlj3+aAZD0eqBy1YiKwCIQwASgIQEABQEa7AaRb4QAf/a+Mb/jQCFE4IJqDL0x+WgBElQIFgDOeAJX+jCQ3rQTQtvGMc2HgAFbJKQtCzBAscwBAmYcYW7lOTDnHkClKHcXydktcoIaY/j2nYG8Y4Zecr4LScYwPvmG0fAakWmmxLoPOYN19hLeZ7bEzCs4f6O98ZhIHA5aeAEChQ6vG6OwAAiEFF2osUfSlhAjTN8Ywyf2dIOyEKNw7uDHWy4sfu09EBQ8AUKUKDGEaAABJSZalUTpAdf2MF+d0AHRduaIDI4whFokE6mBQQAOw==" />' +
                        '</div>' +
                        '<div class="divLoadingText">' +
                            '<h4>' +
                             (msg ? msg : 'Loading...') +
                            '</h4>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
              '</div>'));
}

function _hideLoadingFullPageAsync() {
    $("html").css("overflow", "");
    $("#fullLoadingPageAsync").hide();
}

function _hideLoadingTopMessage(pid) {
    if (!pid) {
        pid = "GlobalTopMsg";
    }
    $("#TopMsg-" + pid).remove();

    //Remove loading when no messages
    if ($(".pace-activity-label").length == 0) {
        $(".pace-activity").parent().removeClass("pace-active");
        $(".pace-activity").parent().removeClass("top-bar-loading");
        $(".pace-activity").parent().addClass("pace-inactive");
    } else {
        //Remove opacity to last one message
        $($(".pace-activity-label")[($(".pace-activity-label").length - 1)]).css("opacity", "1");
    }
}

function _hideLoadingFullPage(options) {
    if (!options) { options = {}; }

    var idLoading = options["idLoading"];

    if (idLoading) {
        $('#fullLoading[idLoading="' + idLoading + '"]').remove();
    } else {
        $('#fullLoading').remove();
    }
}

//----------------------------------------------------------------
//Notifications / Alert / User messages
//----------------------------------------------------------------
function _showNotification(type, msg, id, autohide) {
    //Creation example
    //_showNotification('success', 'hola');
    var secondsToHide = 15;

    if (typeof autohide != "undefined" && autohide === false) {
        secondsToHide = 0;
    }

    Messenger({
        extraClasses: 'messenger-fixed messenger-on-right messenger-on-top',
        theme: 'flat'
    }).post({
        type: type,
        showCloseButton: true,
        message: msg,
        hideAfter: secondsToHide,
        id: (id ? id : _createCustomID())
    });
}

function _showAlert(defaultOptions) {
    //Creation example
    //_showAlert({
    //    showTo (prependTo): "#mainContentArea",
    //    showTo (prependTo): $("#mainContentArea"),
    //    showToElTop (before): $("#mainContentArea"),
    //    type: 'error',
    //    title: "Message"
    //    content: "Please select at least one row",
    //    onReady: function($alert){}
    //    animateScrollTop: false
    //});

    //Default Options.
    var option = {
        id: "alert-" + _createCustomID(),
        showTo: "#mainMessages",
        animateScrollTop: false,
        title: "Message",
        type: "info"
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, option, defaultOptions);

    var strAlert =
        '<div id="' + option["id"] + '" style="clear:both;"> \
            <div class="alert :ptypeAlert alert-dismissible fade in"> \
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> \
                <strong>:ptitle</strong> \
                <p>:pcontent</p> \
            </div> \
        </div>';
    var className;

    switch (option['type'].toLowerCase()) {

        case "warning":
            className = "alert-warning";
            break;

        case "success":
            className = "alert-success";
            break;

        case "info":
            className = "alert-info";
            break;

        case "error":
            className = "alert-danger";
            break;

        case "primary":
            className = "alert-primary";
            break;

        default:
            className = "alert-default";
            break;

    }

    //Crear el mensaje.
    strAlert = strAlert.replace(":ptitle", option["title"] ? option["title"] : 'Alerta');
    strAlert = strAlert.replace(":pcontent", option["content"] ? option["content"] : '[Empty message]');
    strAlert = strAlert.replace(":ptypeAlert", className);

    if (option["showToElTop"]) {
        var $showToElTop = option["showToElTop"];

        //Remover si existe algun mensaje en el elemento.
        $showToElTop.find("#" + option["id"]).remove();

        //Agregar el mensaje.

        var htmlAlert = $(strAlert).hide();

        $showToElTop.before(htmlAlert);

        htmlAlert.fadeIn(100, function () {
            if (option["onReady"]) {
                option["onReady"](htmlAlert);
            }
        });
    } else {
        if (option["showTo"]) {

            var $elShowto;

            if (typeof option["showTo"] == "object") {
                $elShowto = option["showTo"];
            } else {
                $elShowto = $(option["showTo"]);
            }

            //Remover si existe algun mensaje en el elemento.
            $elShowto.find("#" + option["id"]).remove();

            //Agregar el mensaje
            var htmlAlert = $(strAlert).hide().prependTo($elShowto);

            htmlAlert.fadeIn(100, function () {
                if (option["onReady"]) {
                    option["onReady"](htmlAlert);
                }
            });
        }
    }

    if (option['animateScrollTop'] === true) {
        //Subimos el Scroll de la pagina para mostrar el mensaje.
        $('html, body').animate({
            scrollTop: '0px'
        }, 800);
    }
}

function _showDetailAlert(options) {
    //Creation example
    //_showDetailAlert({
    //    shortMsg: "Application message.",
    //    longMsg: "Long content.",
    //    title: "Information message",
    //    type: "error",
    //    viewLabel: "View Details",
    //    animateScrollTop: false
    //});

    //Default options
    var defaulOptions = {
        id: "alert-" + _createCustomID(),
        shortMsg: "Please click in view details to see the message.",
        title: "Message",
        type: "info",
        viewLabel: "View Details",
        animateScrollTop: true
    };

    //merge defaulOptions into options
    options = $.extend(true, {}, defaulOptions, options);

    //Show view link only if we have a long message
    if (options["longMsg"] && (options["longMsg"]).length < 200 && options["shortMsg"] == "Please click in view details to see the message.") {
        //Set message
        options["shortMsg"] = options["longMsg"];
    } else {
        //Popup content
        //var $contentAlert = $('<div rel="popover" class="pull-right" style="margin: 6px; " data-animate=" animated fadeIn " data-container="body" data-color-class="' + ((options["type"].toLowerCase() == "error") ? "danger" : options["type"].toLowerCase()) + '" data-toggle="popover" data-placement="bottom"  data-title="Exception" data-trigger="click" data-html="true" data-original-title="" title="" ></div>');
        var $contentAlert = $('<div class="pull-right"></div>');

        //Add link View Details
        $contentAlert.append('<a href="#" modalContent="" onclick="_showModal({ modalId: \'modalDetailMessage\', title: \'Message Details\', contentHtml: $(this).attr(\'modalContent\') });">' + options["viewLabel"] + '</a><br />');

        //Add long content
        var htmlModal = "<p>" + options["shortMsg"] + "</p>" + options["longMsg"] + "";
        $contentAlert.find('a').attr("modalContent", htmlModal);
        //$contentAlert.attr('data-content', options["longMsg"]);
    }
    var $contentDiv = $('<div>' + options["shortMsg"] + ' <br /></div>');
    $contentDiv.append($contentAlert);
    $contentDiv.append($('<div style="clear:both;"></div>'));

    _showAlert({
        id: options["id"],
        showTo: options["showTo"],
        type: options["type"].toLowerCase(),
        title: options["title"],
        content: $contentDiv.html(),
        animateScrollTop: options["animateScrollTop"]
    });
}

function _addSessionMessage(pmessage, onSuccess) {
    _callServer({
        loadingMsg: "Adding session message...",
        url: '/Shared/AddSessionMessage',
        data: {
            pmessage: pmessage
        },
        success: function (responseList) {
            if (onSuccess) {
                onSuccess(responseList);
            }
        }
    });
}

//----------------------------------------------------------------
//Function to manage User and Active Directory DLs and DL Members
//----------------------------------------------------------------
function _createInputDL(customOptions) {
    var idInputDL = _createCustomID();

    //Default Options.
    var options = {
        id: '',
        formGroupRender: true,
        labelRender: true,
        labelText: "DL",
        labelClass: "col-md-2 control-label",
        fieldClass: "col-md-9",
        actionClass: "col-md-1",
        dlSummary: '',
        placeholder: "e. g. *CSS FRSS CR Re-engineering Team &lt;dl.fro.cr.are.team@imcla.lac.nsroot.net&gt;",
        validations: {
            divEditableRequired: false,
            dlFormat: true
        },
        disabled: false
    };

    //Merge default options
    $.extend(true, options, customOptions);

    //Get GOC container
    var $divContainer = $(options.id);
    var htmlInputDL =
        ' <form class="formDL" id="formDL_' + idInputDL + '"> ';

    if (options.formGroupRender) {
        htmlInputDL +=
        '    <div class="form-group"> ';
    }

    if (options.labelRender) {
        htmlInputDL +=
        '        <label class="' + options.labelClass + '"> ' +
        '            <i class="fa fa-angle-double-right"></i> ' + options.labelText + ' ' +
        '        </label> ';
    }
        
    htmlInputDL +=
        '        <div class="' + options.fieldClass + '"> ' +
        '            <div class="form-control inputDL" name="inputDL_' + idInputDL + '" placeholder="' + options.placeholder + '" contenteditable="true" autocomplete="off" >' + options.dlSummary + '</div> ' +
        '        </div> ' +
        '       <div class="' + options.actionClass + '"> ' +
        '           <button type="button" title="View Contact of DL" class="btn btn-default btn-sm btnViewContacts"> ' +
        '               <i class="fa fa-search-plus"></i> ' +
        '           </button> ' +
        '       </div> ';

    if (options.formGroupRender) {
        htmlInputDL +=
        '    </div> ';
    }

    htmlInputDL +=
        ' </form> ';

    $divContainer.append(htmlInputDL);

    //On click View Contacts of DL
    $divContainer.find(".btnViewContacts").click(function () {
        openModalViewContacts();
    });

    //Load placeholders
    _loadDivContentEditablePlaceholders();

    //Load Validations
    var rules = {};
    rules['inputDL_' + idInputDL] = {};

    if (options.validations.divEditableRequired) {
        rules['inputDL_' + idInputDL].divEditableRequired = true;
    }

    if (options.validations.dlFormat) {
        rules['inputDL_' + idInputDL].dlFormat = true;
    }

    _validateForm({
        form: $divContainer.find(".formDL"),
        rules: rules
    });

    function openModalViewContacts() {
        var $dlField = $divContainer.find(".inputDL");
        var dlSummary = $dlField.text();

        //Validate if field has value
        if (!_divEditableIsEmpty($dlField)) {
            //Get Email DL
            var emailDL = _getEmailFromDLSummary(dlSummary);

            //Validate if format is valid
            if ($dlField.attr("aria-invalid") && $dlField.attr("aria-invalid") != "true") {

                //Show Modal to the user
                _openModalViewContactsOfDL({
                    emailDL: emailDL
                });
            } else {
                _showNotification("error", "DL '" + emailDL + "' Format is incorrect.", "DLError");
            }

        } else {
            _showNotification("error", "DL is empty.", "DLError");
        }
    }
}

function _createSelectContact(customOptions) {

    //Default Options.
    var options = {
        id: "",
        label: "",
        attrs: {
            name: "lblContactsSummary"
        },
        spName: "",
        spParams: [],
        jsonContacts: "[]",
        onSelect: function (strListContacts, newContacts, deletedContacts) { }
    };

    //Merge default options
    $.extend(true, options, customOptions);

    //Load Control
    loadControl();

    //Function load Control
    function loadControl() {
        //Clear current data
        $(options.id).contents().remove();

        //Add field to container
        $(options.id).append(getHtml());

        //Set attr StrListContact with jsonContacts
        var contacts = JSON.parse(options.jsonContacts);
        $(options.id).attr("StrListContact", _generateString(contacts, ["Type", "ID"], "~", "|"));

        //Init popovers functions
        _GLOBAL_SETTINGS.tooltipsPopovers();

        //On click Edit Contacts
        $(options.id).find(".btnEditContacts").click(function () {
            _openModalSelectContacts({
                title: "Select " + options.label + " Contacts",
                spName: options.spName,
                spParams: options.spParams,
                jsonTempContacts: options.jsonContacts,
                loadTempContacts: $(options.id).attr("LoadTempContacts"),
                onSelect: function (strListContact, selectedContacts) {
                    //Create Json Contacts
                    var contacts = [];
                    for (var i = 0; i < selectedContacts.length; i++) {
                        contacts.push({
                            ID: selectedContacts[i].ID,
                            Level: selectedContacts[i].Level,
                            Type: selectedContacts[i].Type,
                            Email: selectedContacts[i].Email,
                            Description: selectedContacts[i].Description
                        });
                    }

                    //Set new json
                    options.jsonContacts = JSON.stringify(contacts);

                    //Update property
                    $(options.id).attr("StrListContact", strListContact);

                    //Update property to load jsonTempContacts
                    $(options.id).attr("LoadTempContacts", "1");

                    //Refresh Control
                    loadControl();

                    //Caller
                    options.onSelect(strListContact);
                }
            });
        });
    }

    //Control HTML
    function getHtml() {
        var contacts = JSON.parse(options.jsonContacts);
        //var contacts = JSON.parse('[{"Type":"User","Description":"Carlos Dominguez [CD25867]"},{"Type":"User","Description":"Campos Soto, Alejandro [AC94030]"},{"Type":"DL","Description":"*CSS FRSS CR Re-engineering Team"}]');
        var htmlViewContacts = "";
        var userCount = 0;
        var dlCount = 0;
        if (contacts.length > 0) {
            var htmlContactPopup = "<div style='padding: 10px; font-size: 15px;'>";

            for (var i = 0; i < contacts.length; i++) {
                var tempData = contacts[i];

                if (tempData.Type == "User") {
                    userCount++;
                }

                if (tempData.Type == "DL") {
                    dlCount++;
                }

                htmlContactPopup += " " + tempData.Type + ": " + tempData.Description + "<br>";
            }
            htmlContactPopup += "</div>";

            htmlViewContacts =
                '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + htmlContactPopup + '" data-title="Contacts Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                '   <img height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png" class="pull-right"> ' +
                '</div>';
        }

        var html =
            ' <div class="form-group"> ' +
            '     <label class="col-md-4 control-label"> ' +
            '         <i class="fa fa-angle-double-right"></i> ' + options.label +
            '     </label> ' +
            '     <div class="col-md-6"> ' +
            '         <div class="form-control" fieldContact="true" validatelement="true" name="' + options.attrs.name + '" > ' +
            '             <div class="pull-left lblContactsSummary">(' + userCount + ') Users and (' + dlCount + ') DLs</div> ' +
            '             ' + htmlViewContacts +
            '         </div> ' +
            '     </div> ' +
            '     <div class="col-md-2"> ' +
            '         <button type="button" class="btn btn-default btn-sm btnEditContacts "> ' +
            '             <i class="fa fa-pencil"></i> ' +
            '         </button> ' +
            '     </div> ' +
            ' </div> ';

        return html;
    }
}

function _openModalSelectContacts(customOptions) {
    var idModal = _createCustomID();

    //Default Options.
    var options = {
        title: "Select Contacts",
        jsonTempContacts: "[]",
        loadTempContacts: "0",
        spName: "",
        spParams: [],
        onSelect: null
    };

    //Merge default options
    $.extend(true, options, customOptions);

    //Show Modal to user
    _showModal({
        modalId: "modalSelectContacts",
        width: "90%",
        addCloseButton: true,
        buttons: [{
            name: "<i class='fa fa-crosshairs'></i> Select Contacts",
            class: "btn-info",
            closeModalOnClick: false,
            onClick: function ($modal) {
                //Get current contacts
                var contactRows = $('#tblAdminUsers_' + idModal).jqxGrid("getRows");

                //Only save level 1 rows
                var rootContactRows = _findAllObjByProperty(contactRows, "Level", "1");

                //Return selected contacts
                options.onSelect(_generateString(rootContactRows, ["Type", "ID"], "~", "|"), rootContactRows);

                //Close modal
                $modal.find(".close").click();
            }
        }],
        title: options.title,
        contentHtml: getHtml(),
        onReady: function ($modal) {
            onReady($modal);
        }
    });

    //Create HTML
    function getHtml() {
        var html =
            '<div class="row"> ' +
            '    <div class="col-md-12"> ' +

            '       <div class="row"> ' +
            '           <div class="col-md-9"> ' +
            '               <div class="form-group"> ' +
            '                   <label class="col-md-4 control-label"> ' +

            '                       <ul class="list-unstyled"> ' +
            '                           <li class="col-md-6"> ' +
            '                               <input type="radio" value="User" name="chkContactType_' + idModal + '" class="skin-flat-green" checked> ' +
            '                               <label class="icheck-label form-label">User</label> ' +
            '                           </li> ' +
            '                           <li class="col-md-6"> ' +
            '                               <input type="radio" value="DL" name="chkContactType_' + idModal + '" class="skin-flat-green" > ' +
            '                               <label class="icheck-label form-label">DL Group</label> ' +
            '                           </li> ' +
            '                           <div style="clear:both;"></div> ' +
            '                       </ul> ' +

            '                   </label> ' +
            '                   <div class="fieldContactUser"> ' +
            '                       <div class="col-md-7"> ' +
                                        //Select User by SOEID
            '                           <div class="selAdminUsers" id="selAdminUsers_' + idModal + '"></div> ' +
            '                           <a class="btnAddMultiplesContacts" href="#">Add multiples by (;)</a> ' +
            '                       </div> ' +
            '                       <div class="col-md-1"> ' +
            '                           <button type="button" class="btnAddUser btn btn-success bottom15 pull-left"><i class="fa fa-plus-square"></i> Add</button> ' +
            '                       </div> ' +
            '                   </div> ' +
            '                   <div class="fieldContactDL"> ' +
                                    //DL of Users
            '                       <div class="txtAdminUsersDL" id="txtAdminUsersDL_' + idModal + '"></div> ' +
            '                   </div> ' +
            '               </div> ' +
            '           </div>' +

            '           <div class="col-md-3"> ' +
            '               <button type="button" class="btnDeleteContact btn btn-danger bottom15 pull-right"><i class="fa fa-trash-o"></i> Delete row</button> ' +
            '               <button type="button" class="btnAddDL btn btn-success bottom15 pull-left"><i class="fa fa-plus-square"></i> Add</button> ' +
            '           </div>' +
            '       </div>' +

            '    </div> ' +

            '    <div class="col-md-12 containerTblAdminUsers"> ' +
            '       <div class="clearfix tblAdminUsers" id="tblAdminUsers_' + idModal + '"></div> ' +
            '    </div> ' +
            '</div> ';

        return html;
    }

    //On ready HTML in modal
    function onReady($modal) {
        var idTableAdminUsers = '#tblAdminUsers_' + idModal;
        var idSelAdminUsers = "#selAdminUsers_" + idModal;
        var idTxtAdminUsersDL = "#txtAdminUsersDL_" + idModal;

        //Add new row 
        $modal.find(".btnAddUser,.btnAddDL").click(function () {
            newRow();
        });
        //Delete selected row 
        $modal.find(".btnDeleteContact").click(function () {
            deleteRow();
        });

        //On Click Add Multiples by (;)
        $modal.find(".btnAddMultiplesContacts").click(function () {
            openModalMultiplesContacts({
                onSelect: function (multipleContactsFound) {
                    //Return selected contacts
                    options.onSelect(_generateString(multipleContactsFound, ["Type", "ID"], "~", "|"), multipleContactsFound);

                    //Close modal
                    $modal.find(".close").click();
                }
            });
        });

        //Create Select of Admin Users
        _createSelectSOEID({
            id: idSelAdminUsers,
            selSOEID: ""
        });

        //Create Input of DL
        _createInputDL({
            id: idTxtAdminUsersDL,
            formGroupRender: false,
            labelRender: false,
            fieldClass: "col-md-7"
        });

        //Creacte Radio Buttons and Checkbox iCheck library
        _GLOBAL_SETTINGS.iCheck();

        //On Check contact type
        $modal.find("input[name='chkContactType_" + idModal + "']").on('ifChecked', function () {
            var contactType = $(this).val();
            if (contactType == "User") {
                $modal.find(".fieldContactUser").show();
                $modal.find(".fieldContactDL").hide();
                $modal.find(".btnAddDL").hide();
                $modal.find(".containerTblAdminUsers").removeClass("top15");
            }

            if (contactType == "DL") {
                $modal.find(".fieldContactUser").hide();
                $modal.find(".fieldContactDL").show();
                $modal.find(".btnAddDL").show();
                $modal.find(".containerTblAdminUsers").addClass("top15");
            }
        });

        //Show user field default
        $modal.find(".fieldContactUser").show();
        $modal.find(".fieldContactDL").hide();
        $modal.find(".btnAddDL").hide();

        //Load table 
        loadTableUsers();

        function loadTableUsers() {
            var gridDefinition = {
                showTo: idTableAdminUsers,
                options: {
                    //for comments or descriptions
                    height: "370",
                    autoheight: false,
                    autorowheight: false,
                    selectionmode: "singlerow",
                    showfilterrow: true,
                    sortable: true,
                    editable: true,
                    groupable: false
                },
                
                columns: [
                    //type: string - text - number - int - float - date - time 
                    //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                    //cellsformat: ddd, MMM dd, yyyy h:mm tt
                    { name: 'ID', type: 'string', hidden: true },
                    { name: 'Level', type: 'number', hidden: true },
                    { name: 'Type', text: 'Type', width: '10%', type: 'string', filtertype: 'checkedlist' },
                    {
                        name: 'Description', text: 'Description', width: '55%', type: 'html', filtertype: 'input',
                        cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                            var paddingXLevel = 35;
                            var padding = paddingXLevel * (rowData.Level - 1);
                            var badgeColorToUse = "badge-info";
                            var badgeColors = {
                                1: "badge-info",
                                2: "badge-purple",
                                3: "badge-accent",
                                4: "badge-primary"
                            };

                            if (padding == 0) {
                                padding = 2;
                            }

                            if (badgeColors[rowData.Level]) {
                                badgeColorToUse = badgeColors[rowData.Level];
                            }

                            var tagHtml = '<span class="badge ' + badgeColorToUse + '">L' + rowData.Level + '</span>';
                            var resultHTML =
                                ' <span style="margin-left: ' + padding + 'px;line-height: 28px;" > ' +
                                    tagHtml + ' ' + value +
                                '</span>';

                            return resultHTML;
                        }
                    },
                    { name: 'Email', text: 'Email', width: '35%', type: 'string', filtertype: 'input' }
                ],
                ready: function () { }
            };

            if (options.loadTempContacts == "1") {
                gridDefinition.source = {
                    // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                    dataBinding: "Large Data Set Local",
                    rows: JSON.parse( options.jsonTempContacts )
                };
            } else {
                gridDefinition.sp = {
                    Name: options.spName,
                    Params: options.spParams,
                    OnDataLoaded: function (responseList) {
                        //$modal.find(".lblAdminUsers").html("Admin Users (" + responseList.length + ")");
                    }
                };

                gridDefinition.source = {
                    // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
                    dataBinding: "Large Data Set"
                };
            }

            $.jqxGridApi.create(gridDefinition);
        }

        function newRow() {
            var $jqxGrid = $(idTableAdminUsers);
            var contactType = $("[name='chkContactType_" + idModal + "']:checked").val()

            if (contactType == "User") {
                //User SOEID
                var inputUserSOEID = $modal.find(".selAdminUsers select").find(":selected").val();
                var inputUserName = $modal.find(".selAdminUsers select").find(":selected").attr("Name");

                //Validate if the user fill all field
                if (inputUserSOEID) {

                    //Validate if soeid not exist in current list
                    var gridRows = $jqxGrid.jqxGrid("getRows");
                    var existsRow = _findAllObjByProperty(gridRows, "ID", inputUserSOEID);
                    if (existsRow.length == 0) {
                        var datarow = {
                            ID: inputUserSOEID,
                            Level: 1,
                            Type: 'User',
                            Description: inputUserName + " [" + inputUserSOEID + "]",
                            Email: inputUserSOEID + "@citi.com"
                        };
                        var commit = $jqxGrid.jqxGrid('addrow', null, datarow);

                        //Refresh Users Counter
                        $modal.find(".lblAdminUsers").html("Admin Users (" + $jqxGrid.jqxGrid("getRows").length + ")");
                    } else {
                        _showAlert({
                            showTo: $jqxGrid.parent(),
                            type: 'error',
                            title: "Message",
                            content: "The user already exists."
                        });
                    }
                }
            }

            if(contactType == "DL") {
                //DL for Admin
                var $inputAdminUsersDL = $modal.find(".txtAdminUsersDL .inputDL");
                if (!_divEditableIsEmpty($inputAdminUsersDL)) {
                    var inputDL = $inputAdminUsersDL.text();
                    var emailDL = _getEmailFromDLSummary(inputDL);

                    //Insert DL in database if not exists
                    _getAutomationIDDL({
                        emailDL: emailDL,
                        onSuccess: function (idDL) {

                            //Validate if soeid not exist in current list
                            var gridRows = $jqxGrid.jqxGrid("getRows");
                            var existsRow = _findAllObjByProperty(gridRows, "ID", idDL);
                            if (existsRow.length == 0) {

                                var datarow = {
                                    ID: idDL,
                                    Level: 1,
                                    Type: 'DL',
                                    Description: $.trim(inputDL.split("<")[0]),
                                    Email: emailDL
                                };
                                var commit = $jqxGrid.jqxGrid('addrow', null, datarow);

                                //Refresh RPLs Counter
                                $modal.find(".lblAdminUsers").html("Admin Users (" + $jqxGrid.jqxGrid("getRows").length + ")");

                            } else {
                                _showAlert({
                                    showTo: $jqxGrid.parent(),
                                    type: 'error',
                                    title: "Message",
                                    content: "The DL already exists."
                                });
                            }

                        }
                    });
                } else {
                    _showAlert({
                        showTo: $jqxGrid.parent(),
                        type: 'error',
                        title: "Message",
                        content: "Please select an user or enter DL."
                    });
                }
            }
        }

        function deleteRow() {
            var $jqxGrid = $(idTableAdminUsers);

            //Delete 
            var objRowSelected = $.jqxGridApi.getOneSelectedRow(idTableAdminUsers, true);
            if (objRowSelected) {

                var htmlContentModal = "";

                htmlContentModal += "<b>Type: </b>" + objRowSelected['Type'] + "<br/>";
                htmlContentModal += "<b>Description: </b>" + objRowSelected['Description'] + "<br/>";

                _showModal({
                    width: '40%',
                    modalId: "modalDelRow",
                    addCloseButton: true,
                    buttons: [{
                        name: "Delete",
                        class: "btn-danger",
                        onClick: function () {
                            //Delete row from grid
                            $jqxGrid.jqxGrid('deleterow', objRowSelected.uid);

                            //Refresh Users Counter
                            //$modal.find(".lblAdminUsers").html("Admin Users (" + $jqxGrid.jqxGrid("getRows").length + ")");
                        }
                    }],
                    title: "Are you sure you want to delete this row?",
                    contentHtml: htmlContentModal
                });
            }
        }
    }

    //Open Modal multiples by (;)
    function openModalMultiplesContacts(customMultipleContactsOptions) {
        var idModalMultipleContacts = _createCustomID();
        var idTableMultipleContacts = 'tblMultipleContacts_' + idModalMultipleContacts;
        var idTxtMultipleContacts = 'txtMultipleContacts_' + idModalMultipleContacts;

        //Default Options.
        var multipleContactsOptions = {
            onSelect: null
        };

        //Merge default options
        $.extend(true, multipleContactsOptions, customMultipleContactsOptions);

        //Show Modal to user
        _showModal({
            modalId: "modalMultipleContacts",
            width: "90%",
            addCloseButton: true,
            buttons: [{
                name: "<i class='fa fa-crosshairs'></i> Select Valid Contacts",
                class: "btn-info",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    //Get current contacts
                    var contactRows = $('#' + idTableMultipleContacts).jqxGrid("getRows");
                    var validContacts = [];

                    //Remove contacts with empty ID
                    for (var i = 0; i < contactRows.length; i++) {
                        if (contactRows[i].ID) {
                            validContacts.push(contactRows[i]);
                        }
                    }

                    //Return selected contacts
                    multipleContactsOptions.onSelect(validContacts);

                    //Close modal
                    $modal.find(".close").click();
                }
            }],
            title: "Past multiple contacts from text",
            contentHtml: getHtmlMultipleContacts(),
            onReady: function ($modal) {
                onReadyMultipleContacts($modal);
            }
        });

        //Create HTML
        function getHtmlMultipleContacts() {
            var html =
                '<div class="row" style="min-height: 430px;"> ' +
                '    <div class="col-md-12"> ' +

                '       <div class="row"> ' +
                '           <div class="col-md-12"> ' +
                '               <div class="form-group"> ' +
                '                   <label class="col-md-12 control-label">Text with contacts:</label> ' +
                '                   <div class="col-md-12"> ' +
                '                       <textarea rows="4" placeholder="e. g. Brucino, Frank [FIN]; Vega, Omar1 [CCC-OT]; *CCA US Buffalo Controllers; OR cd25867; ov20282; kk53989; cm08588" class="form-control txtMultipleContacts" id="' + idTxtMultipleContacts + '"></textarea> ' +
                '                   </div> ' +
                '               </div> ' +
                '           </div>' +

                '           <div class="col-md-12"> ' +
                '               <button type="button" style="margin-top: 25px;" class="btnProcessText btn btn-success bottom10 right15 pull-right"><i class="fa fa-plus-square"></i> Process text</button> ' +
                '               <button type="button" style="margin-top: 25px;" class="btnDeleteRowM btn btn-danger bottom10 right15 pull-right"><i class="fa fa-trash-o"></i> Delete row</button> ' +
                '           </div>' +
                '       </div>' +

                '    </div> ' +

                '    <div class="col-md-12 containerTableMultipleContacts"> ' +
                '       <div class="clearfix tblMultipleContacts" id="' + idTableMultipleContacts + '"></div> ' +
                '    </div> ' +
                '</div> ';

            return html;
        }

        //On ready HTML in modal
        function onReadyMultipleContacts($modal) {
            //Add autosize to textareas
            $modal.find("textarea").autosize();

            //Fix big height on init
            $modal.find("textarea").css("height", "100px");

            //On click process Text
            $modal.find(".btnProcessText").click(function () {
                proccessText();
            });

            //On click delete row of multiple contacts table
            $modal.find(".btnDeleteRowM").click(function () {
                deleteRow();
            });

            //Load table 
            //loadTableUsers();

            function proccessText() {
                var inputText = $("#" + idTxtMultipleContacts).val();
                if (_contains(inputText, ";")) {
                    var splitData = inputText.split(";");
                    var contactsFound = [];

                    //Show Message to user
                    _showLoadingFullPage({
                        id: "ProcessingText",
                        msg: "Looking contacts in text..."
                    });

                    for (var i = 0; i < splitData.length; i++) {
                        var idContact = "";
                        var contactEmail = "";
                        var contactDescription = "";
                        var contactType = "";

                        //Process only if exist data
                        if ($.trim(splitData[i])) {
                            var contactProcessedPart = splitData[i];
                            var splitRow = contactProcessedPart;

                            //Remove spaces
                            splitRow = $.trim(splitRow);

                            //Remove '
                            splitRow = _replaceAll("'", "", splitRow);

                            //*Latam Brazil-Product Control Group <latambrazil-productcontrolgroup@imcla.lac.nsroot.net>
                            //Saltaren, Diana K [FIN] <ds29036@imcnam.ssmb.com>
                            var splitDataDL = splitRow.split("<");

                            if (splitDataDL.length == 2) {
                                //Validate Error: Lee, Robin ,*FRO CR Gaps CIW Maintenance <dl.fro.cr.gaps.ciw.maintenance@imcla.lac.nsroot.net>
                                var part1 = $.trim(splitDataDL[0]);
                                var part2 = splitDataDL[1].slice(0, -1);

                                if (_contains(part1, "*") && part1.substr(0, 1) == "*") {
                                    //Case 1: *FRO CR Gaps CIW Maintenance <dl.fro.cr.gaps.ciw.maintenance@imcla.lac.nsroot.net>
                                    contactDescription = part1;
                                    contactEmail = part2;
                                } else {
                                    if (!_contains(part1, "*")) {
                                        //Case 2: Cabanas Silva, Mariana [GCB] <mc50000@imcla.lac.nsroot.net>
                                        contactDescription = part1;
                                        contactEmail = part2;
                                    }
                                }

                            } else {
                                //Validate Error:  Cabanas Silva, Mariana [GCB] <mc50000@imcla.lac.nsroot.net>Concilco, Oscar [ICG-MKTS] <oc40078@imcla.lac.nsroot.net>
                                var countChar1 = splitRow.split("<").length - 1;
                                var countChar2 = splitRow.split("[").length - 1;

                                //Case 3: *FRO CR Gaps CIW Maintenance
                                if (countChar1 == 0 && countChar2 == 0 && splitRow.substr(0, 1) == "*") {
                                    contactDescription = $.trim(splitRow);
                                }

                                //Case 4: Cabanas Silva, Mariana [GCB]
                                if (countChar2 == 1) {
                                    contactDescription = $.trim(splitRow);
                                }

                                //Case 5: mc50000
                                if (countChar1 == 0 && countChar2 == 0 && $.trim(splitRow).length == 7) {
                                    contactDescription = $.trim(splitRow);
                                }
                            }

                            //Validate Type (User or DL)
                            if (splitRow.substr(0, 1) == "*") {
                                contactType = "DL";

                                if (contactEmail || contactDescription) {
                                    //Find ID DL
                                    _getAutomationIDDL({
                                        callbackParams: {
                                            contactEmail: contactEmail,
                                            contactDescription: contactDescription,
                                            contactType: contactType,
                                            contactProcessedPart: contactProcessedPart
                                        },
                                        emailDL: contactEmail,
                                        descriptionDL: contactDescription,
                                        onSuccess: function (idAutomationDL, callbackParams) {
                                            if (idAutomationDL == "-1") {
                                                idAutomationDL = "";
                                            }

                                            addContactFound({
                                                ID: idAutomationDL,
                                                Level: 1,
                                                Email: callbackParams.contactEmail,
                                                Description: callbackParams.contactDescription,
                                                Type: callbackParams.contactType,
                                                ProcessedPart: callbackParams.contactProcessedPart
                                            });
                                        }
                                    });
                                } else {
                                    addContactFound({
                                        ID: idContact,
                                        Level: 1,
                                        Email: contactEmail,
                                        Description: contactDescription,
                                        Type: contactType,
                                        ProcessedPart: contactProcessedPart
                                    });
                                }

                            } else {
                                contactType = "User";

                                //Get ID by Email
                                if (contactEmail) {
                                    idContact = contactEmail.split("@")[0];
                                }

                                //If idContact still null
                                if (!idContact && contactDescription) {
                                    var employeeSOEID = "";
                                    var employeeName = "";

                                    //Validate if contact is a SOEID
                                    if (contactDescription.length == 7) {
                                        //Look information by SOEID
                                        employeeSOEID = contactDescription;
                                    }

                                    //Validate if contact description have the format Saltaren, Diana K [FIN]
                                    if (_contains(contactDescription, "[")) {
                                        var splitDataName = contactDescription.split("[");
                                        employeeName = $.trim(splitDataName[0]);
                                    }

                                    findEmployee({
                                        callbackParams: {
                                            idContact: idContact,
                                            contactEmail: contactEmail,
                                            contactDescription: contactDescription,
                                            contactType: contactType,
                                            contactProcessedPart: contactProcessedPart
                                        },
                                        soeid: employeeSOEID,
                                        name: employeeName,
                                        onSuccess: function (resultList, callbackParams) {
                                            if (resultList.length == 1) {
                                                var objEmployee = resultList[0];

                                                addContactFound({
                                                    ID: objEmployee.SOEID,
                                                    Level: 1,
                                                    Email: objEmployee.Email,
                                                    Description: objEmployee.Name,
                                                    Type: callbackParams.contactType,
                                                    ProcessedPart: callbackParams.contactProcessedPart
                                                });
                                            } else {
                                                addContactFound({
                                                    ID: callbackParams.idContact,
                                                    Level: 1,
                                                    Email: callbackParams.contactEmail,
                                                    Description: callbackParams.contactDescription,
                                                    Type: callbackParams.contactType,
                                                    ProcessedPart: callbackParams.contactProcessedPart
                                                });
                                            }
                                        }
                                    });
                                } else {
                                    addContactFound({
                                        ID: idContact,
                                        Level: 1,
                                        Email: contactEmail,
                                        Description: contactDescription,
                                        Type: contactType,
                                        ProcessedPart: contactProcessedPart
                                    });
                                }
                            }
                        }
                    }

                    function addContactFound(pcontact) {
                        contactsFound.push(pcontact);
                    }

                    function findEmployee(customFindEmployeesOptions) {

                        //Default Options.
                        var findEmployeesOptions = {
                            //This is used on loop to avoid lost the current values of variables
                            callbackParams: {},
                            soeid: "",
                            name: "",
                            onSuccess: null
                        };

                        //Hacer un merge con las opciones que nos enviaron y las default.
                        $.extend(true, findEmployeesOptions, customFindEmployeesOptions);

                        var sqlFindEmployee =
                            "SELECT \n" +
                            "    E.[SOEID]      AS [SOEID], \n" +
                            "    E.[NAME]       AS [Name], \n" +
                            "    E.[EMAIL_ADDR] AS [Email] \n" +
                            "FROM \n" +
                            "    [Automation].[dbo].[tblEmployee] E WITH(READUNCOMMITTED) \n" +
                            "WHERE \n";

                        if (findEmployeesOptions.soeid) {
                            sqlFindEmployee +=
                            "    E.[SOEID] = '" + findEmployeesOptions.soeid + "'";
                        }
                            
                        if (findEmployeesOptions.name) {
                            sqlFindEmployee +=
                            "    E.[NAME] = '" + findEmployeesOptions.name + "'";
                        }

                        //Get Business Days Result
                        _callServer({
                            loadingMsgType: "topBar",
                            loadingMsg: "Looking Employee '" + (findEmployeesOptions.soeid ? findEmployeesOptions.soeid : findEmployeesOptions.name) + "'...",
                            url: '/Ajax/ExecQuery',
                            data: { 'pjsonSql': _toJSON(sqlFindEmployee) },
                            type: "POST",
                            success: function (resultList) {
                                findEmployeesOptions.onSuccess(resultList, findEmployeesOptions.callbackParams);
                            }
                        });
                    }

                    //When all ajax are completed (_loadTimeZone();, _updateCurrentUserDLs();)
                    _execOnAjaxComplete(function () {
                        loadMultipleContactsFound(contactsFound);
                    });

                } else {
                    _showAlert({
                        id: "AlertErrorMultiple",
                        showTo: $("#" + idTableMultipleContacts).parent(),
                        type: "error",
                        content: "The text that you entered don't have contacts separated by (;)."
                    });
                }
            }

            function loadMultipleContactsFound(contactsFound) {
                var gridDefinition = {
                    showTo: "#" + idTableMultipleContacts,
                    options: {
                        //for comments or descriptions
                        height: "370",
                        autoheight: false,
                        autorowheight: false,
                        selectionmode: "singlerow",
                        showfilterrow: true,
                        sortable: true,
                        editable: true,
                        groupable: false
                    },
                    source : {
                        // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                        dataBinding: "Large Data Set Local",
                        rows: contactsFound
                    },
                    columns: [
                        //type: string - text - number - int - float - date - time 
                        //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                        //cellsformat: ddd, MMM dd, yyyy h:mm tt
                        
                        { name: 'Type', type: 'string', hidden: true },
                        { name: 'Level', type: 'string', hidden: true },
                        {
                            name: 'IsValidContact', text: 'Valid', width: '5%', pinned: true, type: 'bool', filtertype: 'boolean', editable: false, filterable: false, cellsalign: 'center', align: 'center',
                            cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                                var isOk = false;

                                if (rowData.ID) {
                                    isOk = true;
                                }
                                var htmlResult = (isOk ? '<i style="color:green;width: 55px;text-align: center;font-size: 26px;padding: 5px;" class="fa fa-check-square-o"></i>' : '<i style="color:red;width: 55px;text-align: center;font-size: 26px;padding: 5px;" class="fa fa-times-circle"></i>');
                                return htmlResult;
                            }
                        },
                        {
                            name: 'ProcessedPart', text: 'Text Part', width: '35%', type: 'string', filtertype: 'checkedlist',
                            cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                                value = _replaceAll("<", "&lt;", value);
                                value = _replaceAll(">", "&gt;", value);
                                return '<div class="jqx-grid-cell-left-align" style="margin-top: 8.5px;">' + value + ';</div>';
                            }
                        },
                        { name: 'ID', text: 'ID', type: 'string', width: '10%', type: 'string', filtertype: 'checkedlist' },
                        { name: 'Description', text: 'Description', width: '25%', type: 'string', filtertype: 'checkedlist' },
                        { name: 'Email', text: 'Email', width: '25%', type: 'string', filtertype: 'input' }
                    ],
                    ready: function () {
                        //Show Message to user
                        _hideLoadingFullPage({
                            id: "ProcessingText"
                        });
                    }
                };

                $.jqxGridApi.create(gridDefinition);
            }

            function deleteRow() {
                var $jqxGrid = $("#" + idTableMultipleContacts);

                //Delete 
                var objRowSelected = $.jqxGridApi.getOneSelectedRow("#" + idTableMultipleContacts, true);
                if (objRowSelected) {

                    var htmlContentModal = "";

                    htmlContentModal += "<b>Type: </b>" + objRowSelected['Type'] + "<br/>";
                    htmlContentModal += "<b>Description: </b>" + objRowSelected['Description'] + "<br/>";
                    htmlContentModal += "<b>Email: </b>" + objRowSelected['Email'] + "<br/>";

                    _showModal({
                        width: '40%',
                        modalId: "modalDelRow",
                        addCloseButton: true,
                        buttons: [{
                            name: "Delete",
                            class: "btn-danger",
                            onClick: function () {
                                //Delete row from grid
                                $jqxGrid.jqxGrid('deleterow', objRowSelected.uid);

                                //Refresh Users Counter
                                //$modal.find(".lblAdminUsers").html("Admin Users (" + $jqxGrid.jqxGrid("getRows").length + ")");
                            }
                        }],
                        title: "Are you sure you want to delete this row?",
                        contentHtml: htmlContentModal
                    });
                }
            }
        }
    }
}

function _openModalViewContactsOfDL(customOptions) {

    //Default Options.
    var options = {
        title: "Contact Information",
        idDL: "0",
        emailDL: ""
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    if (options.emailDL || options.idDL != "0") {
        var idTable = "tblContactOfDL";
        var htmlContentModal =
            '<div class="row" style="min-height: 400px;"> ' +
            '    <div class="col-md-12"> ' +
            '        <div id="' + idTable + '"></div> ' +
            '    </div> ' +
            '</div> ';

        _showModal({
            modalId: "modalContactOfDL",
            width: "85%",
            addCloseButton: true,
            title: options.title,
            contentHtml: htmlContentModal,
            onReady: function ($modal) {
                if (options.idDL == "0") {
                    //Allways add or refresh members dlEmail
                    _addAutomationIDDL({
                        emailDL: options.emailDL,
                        onSuccess: function (idDLCreatedOrUpdated) {
                            //Load table 
                            loadTable(idDLCreatedOrUpdated);
                        }
                    });

                    //Get ID of Current DL in Database if not exists insert it
                    //_getAutomationIDDL({
                    //    emailDL: options.emailDL,
                    //    onSuccess: function (idDL) {
                    //        //Load table 
                    //        loadTable(idDL);
                    //    }
                    //});
                } else {
                    //Load table 
                    loadTable(options.idDL);
                }
            }
        });

        function loadTable(idAutomationDL) {

            _callProcedure({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading DL Contacts...",
                name: "[Automation].[dbo].[spAFrwkAdminDL]",
                params: [
                    { Name: "@Action", Value: "List Contacts Tree" },
                    { Name: "@IDDL", Value: idAutomationDL }
                ],
                success: function (resultList) {
                    $.jqxGridApi.create({
                        showTo: "#" + idTable,
                        options: {
                            height: "400",
                            autoheight: false,
                            autorowheight: false,
                            selectionmode: "singlerow",
                            showfilterrow: true,
                            sortable: true,
                            editable: true,
                            groupable: false
                        },
                        source: {
                            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                            dataBinding: "Large Data Set Local",
                            rows: resultList
                        },
                        columns: [
                            //type: string - text - number - int - float - date - time 
                            //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                            //cellsformat: ddd, MMM dd, yyyy h:mm tt
                            { name: 'Level', type: 'number', hidden: true },
                            { name: 'Type', text: 'Type', width: '15%', type: 'string', filtertype: 'checkedlist' },
                            {
                                name: 'Description', text: 'Description', width: '45%', type: 'html', filtertype: 'input',
                                cellsrenderer: function (boundIndex, datafield, value, cellHtml, colProperties, rowData) {
                                    var paddingXLevel = 35;
                                    var padding = paddingXLevel * (rowData.Level - 1);
                                    var badgeColorToUse = "badge-info";
                                    var badgeColors = {
                                        1: "badge-info",
                                        2: "badge-purple",
                                        3: "badge-accent",
                                        4: "badge-primary"
                                    };

                                    if (padding == 0) {
                                        padding = 2;
                                    }

                                    if (badgeColors[rowData.Level]) {
                                        badgeColorToUse = badgeColors[rowData.Level];
                                    }

                                    var tagHtml = '<span class="badge ' + badgeColorToUse + '">L' + rowData.Level + '</span>';
                                    var resultHTML =
                                        ' <span style="margin-left: ' + padding + 'px;line-height: 28px;" > ' +
                                            tagHtml + ' ' + value +
                                        '</span>';

                                    return resultHTML;
                                }
                            },
                            { name: 'Email', text: 'Email', width: '40%', type: 'string', filtertype: 'input' }
                        ],
                        ready: function () { }
                    });
                }
            });
        }
    } else {
        _showNotification("error", "global.js:_openModalViewContactsOfDL DL is empty.", "DLError");
    }
}

function _updateCurrentUserDLs(customOptions) {
    var callServerToUpdate = false;

    //Default Options.
    var options = {
        forceRefresh: false,
        onSuccess: null
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    //If not exist variable update data
    var strNextDateForUpdateUserDLs = localStorage.getItem("NextDateForUpdatedUserDLs");
    if (!strNextDateForUpdateUserDLs) {
        callServerToUpdate = true;
    } else {
        //Check last updated date with current date
        var nextDateForUpdate = new Date(strNextDateForUpdateUserDLs);
        var currentDate = new Date();
        if (nextDateForUpdate < currentDate) {
            callServerToUpdate = true;
        }
    }

    if (callServerToUpdate || options.forceRefresh == true) {
        _callServer({
            loadingMsgType: "topBar",
            loadingMsg: "Updating current user DLs...",
            url: '/Configuration/DLManager/UpdateCurrentUserDL',
            data: {},
            type: "GET",
            success: function (dlsUpdated) {
                //Show console
                console.log(dlsUpdated + " DLs of User updated.");

                //Set cache variable to avoid multiples update in one day
                var tomorrow = new Date();
                tomorrow.setDate(tomorrow.getDate() + 1);
                strNextDateForUpdateUserDLs = _formatDate(tomorrow, "yyyy-MM-dd") + " 00:00";
                //strNextDateForUpdateUserDLs = _formatDate(tomorrow, "yyyy-MM-dd HH:mm");
                localStorage.setItem("NextDateForUpdatedUserDLs", strNextDateForUpdateUserDLs);

                if (options.onSuccess) {
                    options.onSuccess(dlsUpdated);
                }
            }
        });
    } else {
        //Show console
        console.log("DLs of User are going to be updated on '" + strNextDateForUpdateUserDLs + "'.");

        if (options.onSuccess) {
            options.onSuccess("-1");
        }
    }
}

function _addAutomationIDDL(customOptions) {

    //Default Options.
    var options = {
        emailDL: "",
        descriptionDL: "",
        onSuccess: null
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Looking for DL Members in '" + (options.emailDL ? options.emailDL : options.descriptionDL) + "'...",
        url: '/Configuration/DLManager/AddAutomationDL',
        data: {
            pemailDL: options.emailDL,
            pdescriptionDL: options.descriptionDL
        },
        type: "GET",
        success: function (idDL) {
            if (options.onSuccess) {
                options.onSuccess(idDL);
            }
        }
    });
}

function _getAutomationIDDL(customOptions) {

    //Default Options.
    var options = {
        //This is used on loop to avoid lost the current values of variables
        callbackParams: {},
        emailDL: "",
        descriptionDL:"",
        onSuccess: null
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    _callServer({
        loadingMsg: "Processing DL '" + (options.emailDL ? options.emailDL : options.descriptionDL) + "'...",
        url: '/Configuration/DLManager/GetAutomationIDDL',
        data: {
            pemailDL: options.emailDL,
            pdescriptionDL: options.descriptionDL
        },
        type: "GET",
        success: function (idDL) {

            if (idDL == "0") {
                _addAutomationIDDL({
                    emailDL: options.emailDL,
                    descriptionDL: options.descriptionDL,
                    onSuccess: function (idDLCreated) {
                        if (options.onSuccess) {
                            options.onSuccess(idDLCreated, options.callbackParams);
                        }
                    }
                });
            } else {
                if (options.onSuccess) {
                    options.onSuccess(idDL, options.callbackParams);
                }
            }
        }
    });
}

function _getEmailFromDLSummary(dlSummary) {
    var emailResult = "";

    //*CSS FRSS CR Re-engineering Team <dl.fro.cr.are.team@imcla.lac.nsroot.net>
    if (dlSummary) {
        var splitData = dlSummary.split("<");

        if (splitData.length == 1) {
            emailResult = dlSummary;
        }

        if (splitData.length == 2) {
            //Remove last character "dl.fro.cr.are.team@imcla.lac.nsroot.net>"
            emailResult = splitData[1].substring(0, splitData[1].length - 1)
        }
    }

    return emailResult;
}

//----------------------------------------------------------------
//Other methods
//----------------------------------------------------------------
function findSoeidMethod(nameTag, widthValue) {
    var timer;
    $(nameTag).jqxInput({
        placeHolder: "Enter a SOEID", height: 25, width: widthValue,
        source: function (query, response) {
            var data2;

            var listOfParams = {
                data: query
            }

            _callServer({
                loadingMsg: "", async: false,
                url: '/NonMOU/getProbablyUser',
                //async: false,
                data: {
                    'data': JSON.stringify(listOfParams)
                },
                type: "post",
                success: function (json) {
                    var response = jQuery.parseJSON(json);

                    //data2 = (response);
                    loadAdapter(response);
                }
            });

            function loadAdapter(data2) {
                var dataAdapter = new $.jqx.dataAdapter
                (
                    {
                        datatype: "jsonp",
                        localdata: data2,
                        datafields:
                        [
                            { name: 'SOEID' }, { name: 'NAME' }
                        ],

                    },
                    {
                        autoBind: true,
                        formatData: function (data) {
                            data.name_startsWith = query;
                            return data;
                        },
                        loadComplete: function (data) {
                            if (data.length > 0) {
                                response($.map(data, function (item) {
                                    return {
                                        label: item.SOEID + ' ' + item.NAME,
                                        value: item.NAME
                                    }
                                }));
                            }
                        }
                    }
                );
            }

        }
    });


}

function findGocMethod(nameTag, widthValue) {
    var timer;
    $(nameTag).jqxInput({
        placeHolder: "Enter a GOC", height: 25, width: widthValue,
        source: function (query, response) {
            var data2;

            var listOfParams = {
                data: query
            }

            _callServer({
                loadingMsg: "", async: false,
                url: '/NonMOU/getProbablyGOC',
                data: {
                    'data': JSON.stringify(listOfParams)
                },
                type: "post",
                success: function (json) {
                    var response = jQuery.parseJSON(json);
                    loadAdapter(response);

                }
            });

            function loadAdapter(data2) {


                var dataAdapter = new $.jqx.dataAdapter
                (
                    {
                        datatype: "jsonp",
                        localdata: data2,
                        datafields:
                        [
                            { name: 'GOCID' }, { name: 'GOCName' }
                        ],

                    },
                    {
                        autoBind: true,
                        formatData: function (data) {
                            data.name_startsWith = query;
                            return data;
                        },
                        loadComplete: function (data) {
                            if (data.length > 3) {
                                response($.map(data, function (item) {
                                    return {
                                        label: item.GOCName,
                                        value: item.GOCName
                                    }
                                }));
                            }
                        }
                    }
                );
            }
        }
    });


}

//----------------------------------------------------------------
//On Document Ready
//----------------------------------------------------------------
$(document).ready(function () {
    var objUser = _getUserInfo();

    //Create avatar with initials
    $('.avatarme').avatarMe({
        avatarClass: 'avatar-me',
        max: 3 // maximum letters displayed in the avartar
    });

    //Show session messages
    var messages = _getViewVar("SessionMessages");
    if (messages) {
        for (var i = 0; i < messages.length; i++) {
            _showDetailAlert({
                title: "Message",
                longMsg: messages[i].Msg,
                type: messages[i].Type,
                viewLabel: "View Details"
            });
        }
    }

    //Add tooltip to Roles
    var htmlRoles = "";
    var rolesSplit = objUser.Roles.split(",");
    var rolesDescSplit = objUser.RolesDesc.split(",");
    //if (objUser.IsDeleted == "False" || typeof objUser.IsDeleted == 'undefined') {
        //if (objUser.IsDeleted == "False" ) {
        if (objUser.Roles != "TBD" && objUser.Roles != "Role Not Found") {
            //Remove Last Char (,)
            //strRoles = strRoles.substring(0, strRoles.length - 1);
            //strRoles = _replaceAll(",", "\n", strRoles);

            for (var i = 0; i < rolesSplit.length; i++) {
                if (rolesDescSplit[i]) {
                    htmlRoles += rolesDescSplit[i] + " <br />";
                } else {
                    if (rolesSplit[i]) {
                        htmlRoles += rolesSplit[i] + " <br />";
                    }
                }
            }

        } else {
            htmlRoles = "No roles found!";
        }
        //console.log(htmlRoles);
        $(".profile-title").html(_cropWord(htmlRoles, 25));

        //Render Menu
        _getCurrentAppData(function (appData) {

            //Render Menu only if user has access to the current Url
            if (_userHasAccessToCurrentUrl(appData.Permits, JSON.parse(objUser.Permits))) {
                //Send custom menu
                _renderMenu(appData.Permits);
            }

            //Add Tooltips to menu
            _GLOBAL_SETTINGS.tooltipsPopovers();

            //Update Page Title when is "Loading Page..."
            if ($("#menu-permit-name").html() == "Loading Page...") {
                _setPageTitle("");
            }
        });
    //} else {
    //    $(".profile-title").html("Access Denied!");
    //}
});