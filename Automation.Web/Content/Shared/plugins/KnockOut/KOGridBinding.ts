﻿/// <reference path="scripts/typings/knockout/knockout.d.ts" />

// ================== BEGIN IE8 support ========
Array.prototype.map = Array.prototype.map || function <T, U>(callbackfn: (value: T, index: number, array: T[]) => U, thisArg?: any): U[] {
	var res: U[] = [];
	var target = thisArg || this;
	for (var i = 0; i < this.length; i++) {
		var src = this[i];
		var dst = callbackfn.call(target, src, i, this);
		res.push(dst);
	}
	return res;
}
// ===================END IE8 Support ==========


interface IKOGridColumn {
	header?: string;
	headerTemplate?: string;
	data?: any; // propertyName:string | function(rowData)
	template?: string; // template taking the row as parameter
	dataTemplate?: string; // template taking the cell data as value property, i.e. { value: data }
	visible?: any; //boolean | KnockoutObservable<boolean>
	sortable?: any; //boolean | KnockoutObservable<boolean>
	cellStyle?: (row) => any;  // add style to the TD
	headerStyle?: any;  // add style to the TH
}
interface IKOGridConfig {
	data?: any; // Array<any> || KnockoutObservableArray<any>;
	pageSize?: number;
	columns?: Array<IKOGridColumn>;
}
enum KOGridSortOrder {
	None,
	Asc,
	Desc,
}
enum KOGridNavigation {
	First,
	Previous,
	Next,
	Last
}
class KOGridColumn {
	header: string = null;
	// REMARK: to prevent event bubbling: data-bind="event: { click: function() {} }, clickBubble: false"
	headerTemplate: string = null;
	data: (row: any) => any = null; // function to get data for row
	template: string = null; // template taking the row as parameter
	dataTemplate: string = null; // template taking the cell data as value property, i.e. { value: data }
	cellStyle: (row) => any = null; // add style to the TD
	headerStyle: any;  // add style to the TH
	visible = ko.observable(true);
	sortable = ko.observable(true);
	sort = ko.observable(KOGridSortOrder.None);
	constructor(config?: IKOGridColumn) {

		if (config !== undefined && config !== null) {
			this.header = config.header ? config.header : null;
			this.headerTemplate = config.headerTemplate ? config.headerTemplate : null;
			if (config.data) {
				if (typeof config.data === 'function')
					this.data = config.data;
				else
					this.data = x => x[config.data];
			}
			this.template = config.template ? config.template : null;
			this.dataTemplate = config.dataTemplate ? config.dataTemplate : null;
			this.headerStyle = config.headerStyle ? config.headerStyle : null;
			this.cellStyle = config.cellStyle ? config.cellStyle : null;
			if (config.visible !== undefined) {
				if (ko.isObservable(config.visible))
					this.visible = config.visible;
				else
					this.visible(config.visible === true);
			}
			if (config.sortable !== undefined) {
				if (ko.isObservable(config.sortable))
					this.sortable = config.sortable;
				else
					this.sortable(config.sortable === true);
			}
		}
	}
}
class KOGridModel {
	// data
	columns: Array<KOGridColumn> = []; // REMARK: set that before itemsSource, or call update()
	itemsSource = ko.observableArray();

	// UI
	inputClass = ko.observable<string>('form-control inline small');

	// manipulation
	paginations = ko.observableArray([5, 10, 25, 50, 100]);
	pageSize = ko.observable(10);
	currentPage = ko.observable(1);

	filter = ko.observable<string>();
	sortColumn = ko.observable<number>();

	// templates for rendering
	templateTable = "KOGridModelDefaultViewTemplate";
	templateHeader = "KOGridModelDefaultViewHeaderTemplate";

	// computed
	private reevaluateComputed = ko.observable(0);
	currentItems: KnockoutComputed<Array<any>>;
	currentPageItems: KnockoutComputed<Array<any>>;
	maxPage: KnockoutComputed<number>;
	numItems: KnockoutComputed<number>;

	constructor(config?: IKOGridConfig) {
		this.setupComputed();
		if (config) {
			this.initialize(config);
		}
	}
	private setupComputed() {
		var delayExtender = <any>{ rateLimit: 5 }; // enable bulk operation to go unimpeded
		this.currentItems = ko.computed(() => {
			this.reevaluateComputed(); // create dependency on that

			var rows = this.itemsSource();
			if (rows === null || rows === undefined) return [];
			if (this.columns.length === 0) return rows;

			// filter
			var ff = this.filter();
			if (ff) {
				ff = ('' + ff).toLowerCase();
				var rows2 = [];
				for (var i = 0; i < rows.length; i++) {
					if (this.filterRow(rows[i], ff)) {
						rows2.push(rows[i]);
					}
				}
				rows = rows2;
			}

			// sort
			var sc = this.sortColumn();
			if (typeof sc === 'number' && sc >= 0 && sc < this.columns.length) {
				rows = this.sortArray(rows, sc);
				if (this.columns[sc].sort() === KOGridSortOrder.Desc)
					rows = rows.reverse();
			}

			return rows;
		}, this).extend(delayExtender);
		this.numItems = ko.computed(() => {
			var a = this.currentItems();
			if (!a) return 0;
			return a.length;
		}).extend(delayExtender);
		this.currentPageItems = ko.computed(() => {
			var startIndex = this.pageSize() * (this.currentPage() - 1);
			var rows = this.currentItems();
			var ps = this.pageSize();
			if (!rows || ps <= 0) return [];
			return rows.slice(startIndex, startIndex + ps);
		}, this).extend(delayExtender);
		this.maxPage = ko.computed(() => {
			var rows = this.currentItems();
			var ps = this.pageSize();
			if (!rows || ps <= 0) return 0;
			var max = Math.ceil(rows.length / ps);
			return max;
		}, this).extend(delayExtender);
		this.maxPage.subscribe(x => {
			if (x < 1) x = 1;
			if (this.currentPage() > x) {
				this.currentPage(x);
			}
		});
		this.currentPage.subscribe(x => {
			if (typeof x === 'string') x = parseInt(<any>x);
			if (x === undefined || x === null || isNaN(x)) {
				this.currentPage(1);
				return;
			}
			var max = this.maxPage();
			if (max < 1) max = 1;
			if (x < 1) {
				this.currentPage(1);
			}
			else if (x > max) {
				this.currentPage(max);
			}
		});
	}
	private initialize(config: IKOGridConfig) {
		if (config.data) {
			if (!config.columns)
				this.scaffold(config.data);
		}
		if (config.columns)
			this.columns = config.columns.map((value, index, array) => new KOGridColumn(value));
		if (config.pageSize !== undefined)
			this.pageSize(config.pageSize);

		// set itemsSource last, (i.e. set columns first) so that this.currentItems() recompute
		if (config.data) {
			if (ko.isObservable(config.data)) {
				this.itemsSource = config.data;
				this.update();
			} else {
				this.itemsSource(config.data);
			}
		}
	}

	private getRowGetter(iRow: number): (row: any) => any {
		var getter = this.columns[iRow].data;
		if (!getter)
			return undefined;
		return (x) => ko.unwrap(getter(x));
	}

	// used for filtering
	private filterRow(row: any, pattern: string): boolean {
		for (var j = 0; j < this.columns.length; j++) {
			var getter = this.getRowGetter(j);
			if (!getter)
				continue;
			var dat = getter(row);
			if (!dat)
				continue;
			var sdat = ('' + dat).toLowerCase();
			if (sdat.indexOf(pattern) > -1)
				return true;
		}
		return false;
	}

	// used for sorting
	private sortArray(rows: any[], column: number): any[] {
		var getter = this.getRowGetter(column);
		if (!getter)
			return rows;
		return rows.sort((r1, r2) => {
			var a = getter(r1);
			var b = getter(r2);
			var ta = typeof a;
			var tb = typeof b;
			if (ta !== tb)
				return 0;
			if (ta === 'number')
				return (a - b);
			if (ta === 'string' || (a instanceof Date && b instanceof Date))
				return a > b ? 1 : a === b ? 0 : -1;
			if (ta === 'boolean')
				return a === b ? 0 : a ? 1 : -1;
			return 0;
		});
	}

	// force new evaluation of computed variables
	update() {
		this.reevaluateComputed(this.reevaluateComputed() + 1);
	}

	// as the name say
	sort(column: number) {
		if (!this.columns[column].sortable())
			return;
		var getter = this.getRowGetter(column);
		if (!getter)
			return;

		var prev = this.sortColumn();
		if (prev === column) {
			if (this.columns[prev].sort() === KOGridSortOrder.Asc)
				this.columns[prev].sort(KOGridSortOrder.Desc);
			else
				this.columns[prev].sort(KOGridSortOrder.Asc);
		}
		else {
			if (prev !== undefined) {
				this.columns[prev].sort(KOGridSortOrder.None);
			}
			this.sortColumn(column);
			this.columns[column].sort(KOGridSortOrder.Asc);
		}
	}

	// change currentPage
	moveTo(dst: KOGridNavigation) {
		var cur = this.currentPage();
		if (typeof cur === 'string') cur = parseInt(<any>cur);
		if (typeof cur !== 'number' || isNaN(cur)) cur = 1;
		switch (dst) {
			case KOGridNavigation.First:
				cur = 1;
				break;
			case KOGridNavigation.Previous:
				cur--;
				break;
			case KOGridNavigation.Next:
				cur++;
				break;
			case KOGridNavigation.Last:
				cur = this.maxPage();
				break;
		}
		if (cur > this.maxPage()) cur = this.maxPage();
		if (cur < 1) cur = 1;
		this.currentPage(cur);
	}

	// set some default columns properties from the data
	scaffold(data) {
		data = ko.unwrap(data);
		var res: KOGridColumn[] = [];
		if ((typeof data.length !== 'number') || data.length === 0) {
			return res;
		}

		for (var p in ko.unwrap(data[0])) {
			var gc = {
				header: p,
				data: p
			};
			res.push(new KOGridColumn(gc));
		}
		this.columns = res;
	}
}

// default template, for the model
(function () {
	function addTemplate(name, markup) {
		document.write("<script type='text/html' id='" + name + "'>" + markup + "<" + "/script>");
	};
	addTemplate("KOGridModelDefaultViewTemplate", "\
		<thead><tr><td data-bind=\"attr: {colspan: columns.length}\">\
			<!-- ko template: templateHeader -->\
			<!-- /ko -->\
		</td></tr></thead>\
		<thead>\
			<tr data-bind=\"foreach: columns\">\
			<th data-bind=\"attr: { style: headerStyle }, css: { hidden: !visible(), 'sort-none': sortable() && data && sort() === KOGridSortOrder.None, 'sort-asc': sort() === KOGridSortOrder.Asc, 'sort-desc': sort() === KOGridSortOrder.Desc }, click: function() { $parent.sort($index()); }\">\
				<!-- ko if: header -->\
				<span data-bind=\"text: header\"></span>\
				<!-- /ko -->\
				<!-- ko if: !header && headerTemplate -->\
					<!-- ko template: headerTemplate -->\
					<!-- /ko -->\
				<!-- /ko -->\
			</th>\
			</tr>\
		</thead>\
		<tbody data-bind=\"foreach: currentPageItems\">\
			<tr data-bind=\"foreach: $parent.columns\">\
			<td data-bind=\"attr: { style: cellStyle ? ko.unwrap(cellStyle($parent)) : null }, css: { hidden: !visible(), sorted: $parentContext.$parent.sortColumn() === $index() } \">\
				<!-- ko if: !dataTemplate && !template && data -->\
				<span data-bind=\"text: ko.unwrap(data($parent)) \"></span>\
				<!-- /ko -->\
				<!-- ko if: !dataTemplate && template -->\
				<span data-bind=\"template: { name: template, data: $parent }\"></span>\
				<!-- /ko -->\
				<!-- ko if: dataTemplate && data -->\
				<span data-bind=\"template: { name: dataTemplate, data: { value: data($parent) } }\"></span>\
				<!-- /ko -->\
			</td>\
			</tr>\
		</tbody><tfoot><tr><td data-bind=\"attr: {colspan: columns.length}\">\
			<!-- ko template: templateHeader -->\
			<!-- /ko -->\
		</td></tr></tfoot>");
	addTemplate("KOGridModelDefaultViewHeaderTemplate", "\
			<div style=\"position:relative;\">\
				<div style=\"position:absolute; right:0;\">\
					<b>Search:</b> <input type=\"text\" style=\"width:9em;\" autocomplete='off' data-bind=\"attr: { 'class': inputClass }, value:filter, valueUpdate: 'afterkeydown'\"/>\
				</div>\
				<b>Display:</b> <select style=\"width:5em;\" data-bind=\"attr: { 'class': inputClass }, options:paginations, value: pageSize\"></select>\
				out of <span data-bind=\"text:numItems\"></span> entries, \
				<b>Page:</b>\
				<div class=\"btn-group\">\
					<button type=\"button\" class=\"small btn btn-default\" data-bind=\"click: function() { moveTo(KOGridNavigation.First); }\">1</button>\
					<button type=\"button\" class=\"small btn btn-default\" data-bind=\"click: function() { moveTo(KOGridNavigation.Previous); }\">&lsaquo;</button>\
					<button type=\"button\" style=\"background:#428bca;color:white;\" class=\"small btn disabled\" data-bind=\"text: currentPage\"></button>\
					<button type=\"button\" class=\"small btn btn-default\" data-bind=\"click: function() { moveTo(KOGridNavigation.Next); }\">&rsaquo;</button>\
					<button type=\"button\" class=\"small btn btn-default\" data-bind=\"click: function() { moveTo(KOGridNavigation.Last); }, text: maxPage\"></button>\
				</div>\
			</div>\
		");
	// alternative header with editable currentPage text input
	addTemplate("KOGridModelDefaultViewHeaderTemplate2", "\
			<div style=\"position:relative;\">\
				<div style=\"position:absolute; right:0;\">\
					<b>Search:</b> <input type=\"text\" style=\"width:9em;\" autocomplete='off' data-bind=\"attr: { 'class': inputClass }, value:filter, valueUpdate: 'afterkeydown'\"/>\
				</div>\
				<b>Display:</b> <select style=\"width:5em;\" data-bind=\"attr: { 'class': inputClass }, options:paginations, value: pageSize\"></select>\
				out of <span data-bind=\"text:numItems\"></span> entries, \
				<b>Page:</b>\
				<div class=\"btn-group\">\
					<button type=\"button\" class=\"small btn btn-default\" data-bind=\"click: function() { moveTo(KOGridNavigation.First); }\">1</button>\
					<button type=\"button\" class=\"small btn btn-default\" data-bind=\"click: function() { moveTo(KOGridNavigation.Previous); }\">&lsaquo;</button>\
					<input type=\"text\" style=\"width:2em;float:left;background:#428bca;color:white;\" data-bind=\"attr: { 'class': inputClass }, value: currentPage\"></button>\
					<button type=\"button\" class=\"small btn btn-default\" data-bind=\"click: function() { moveTo(KOGridNavigation.Next); }\">&rsaquo;</button>\
					<button type=\"button\" class=\"small btn btn-default\" data-bind=\"click: function() { moveTo(KOGridNavigation.Last); }, text: maxPage\"></button>\
				</div>\
			</div>\
		");
})();

// the binding now!
interface KnockoutBindingHandlers {
	kogrid: KnockoutBindingHandler;
}
// this binding's model MUST be a KOGridModel
ko.bindingHandlers.kogrid = {
	init: function () {
		return { 'controlsDescendantBindings': true };
	},
	update: function (element: HTMLElement, viewModelAccessor, allBindings, viewModel, bindingContext) {
		var model = viewModelAccessor();

		if (model && !(model instanceof KOGridModel))
			throw new Error("Binding's value MUST be a KOGridModel");
		if (element.nodeName.toUpperCase() !== "TABLE")
			throw new Error("Bound Element MUST be a TABLE tag");

		while (element.firstChild)
			ko.removeNode(<Element>element.firstChild);

		var ctx = bindingContext.extend(model);
		ctx.$parent = ctx.$data;
		ctx.$parentContext = bindingContext;
		ctx.$data = model;

		ko.renderTemplate(model.templateTable, ctx, null, element, "replaceChildren");
	}
};
