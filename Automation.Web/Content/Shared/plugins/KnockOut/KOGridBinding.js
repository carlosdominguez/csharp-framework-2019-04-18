﻿/// <reference path="scripts/typings/knockout/knockout.d.ts" />
// ================== BEGIN IE8 support ========
Array.prototype.map = Array.prototype.map || function (callbackfn, thisArg) {
    var res = [];
    var target = thisArg || this;
    for (var i = 0; i < this.length; i++) {
        var src = this[i];
        var dst = callbackfn.call(target, src, i, this);
        res.push(dst);
    }
    return res;
};


var KOGridSortOrder;
(function (KOGridSortOrder) {
    KOGridSortOrder[KOGridSortOrder["None"] = 0] = "None";
    KOGridSortOrder[KOGridSortOrder["Asc"] = 1] = "Asc";
    KOGridSortOrder[KOGridSortOrder["Desc"] = 2] = "Desc";
})(KOGridSortOrder || (KOGridSortOrder = {}));
var KOGridNavigation;
(function (KOGridNavigation) {
    KOGridNavigation[KOGridNavigation["First"] = 0] = "First";
    KOGridNavigation[KOGridNavigation["Previous"] = 1] = "Previous";
    KOGridNavigation[KOGridNavigation["Next"] = 2] = "Next";
    KOGridNavigation[KOGridNavigation["Last"] = 3] = "Last";
})(KOGridNavigation || (KOGridNavigation = {}));
var KOGridColumn = (function () {
    function KOGridColumn(config) {
        this.header = null;
        // REMARK: to prevent event bubbling: data-bind="event: { click: function() {} }, clickBubble: false"
        this.headerTemplate = null;
        this.data = null;
        this.template = null;
        this.dataTemplate = null;
        this.cellStyle = null;
        this.visible = ko.observable(true);
        this.sortable = ko.observable(true);
        this.sort = ko.observable(0 /* None */);
        if (config !== undefined && config !== null) {
            this.header = config.header ? config.header : null;
            this.headerTemplate = config.headerTemplate ? config.headerTemplate : null;
            if (config.data) {
                if (typeof config.data === 'function')
                    this.data = config.data;
                else
                    this.data = function (x) {
                        return x[config.data];
                    };
            }
            this.template = config.template ? config.template : null;
            this.dataTemplate = config.dataTemplate ? config.dataTemplate : null;
            this.headerStyle = config.headerStyle ? config.headerStyle : null;
            this.cellStyle = config.cellStyle ? config.cellStyle : null;
            if (config.visible !== undefined) {
                if (ko.isObservable(config.visible))
                    this.visible = config.visible;
                else
                    this.visible(config.visible === true);
            }
            if (config.sortable !== undefined) {
                if (ko.isObservable(config.sortable))
                    this.sortable = config.sortable;
                else
                    this.sortable(config.sortable === true);
            }
        }
    }
    return KOGridColumn;
})();
var KOGridModel = (function () {
    function KOGridModel(config) {
        // data
        this.columns = [];
        this.itemsSource = ko.observableArray();
        // UI
        this.inputClass = ko.observable('form-control inline small');
        // manipulation
        this.paginations = ko.observableArray([5, 10, 25, 50, 100]);
        this.pageSize = ko.observable(10);
        this.currentPage = ko.observable(1);
        this.filter = ko.observable();
        this.sortColumn = ko.observable();
        // templates for rendering
        this.templateTable = "KOGridModelDefaultViewTemplate";
        this.templateHeader = "KOGridModelDefaultViewHeaderTemplate";
        // computed
        this.reevaluateComputed = ko.observable(0);
        this.setupComputed();
        if (config) {
            this.initialize(config);
        }
    }
    KOGridModel.prototype.setupComputed = function () {
        var _this = this;
        var delayExtender = { rateLimit: 5 };
        this.currentItems = ko.computed(function () {
            _this.reevaluateComputed(); // create dependency on that

            var rows = _this.itemsSource();
            if (rows === null || rows === undefined)
                return [];
            if (_this.columns.length === 0)
                return rows;

            // filter
            var ff = _this.filter();
            if (ff) {
                ff = ('' + ff).toLowerCase();
                var rows2 = [];
                for (var i = 0; i < rows.length; i++) {
                    if (_this.filterRow(rows[i], ff)) {
                        rows2.push(rows[i]);
                    }
                }
                rows = rows2;
            }

            // sort
            var sc = _this.sortColumn();
            if (typeof sc === 'number' && sc >= 0 && sc < _this.columns.length) {
                rows = _this.sortArray(rows, sc);
                if (_this.columns[sc].sort() === 2 /* Desc */)
                    rows = rows.reverse();
            }

            return rows;
        }, this).extend(delayExtender);
        this.numItems = ko.computed(function () {
            var a = _this.currentItems();
            if (!a)
                return 0;
            return a.length;
        }).extend(delayExtender);
        this.currentPageItems = ko.computed(function () {
            var startIndex = _this.pageSize() * (_this.currentPage() - 1);
            var rows = _this.currentItems();
            var ps = _this.pageSize();
            if (!rows || ps <= 0)
                return [];
            return rows.slice(startIndex, startIndex + ps);
        }, this).extend(delayExtender);
        this.maxPage = ko.computed(function () {
            var rows = _this.currentItems();
            var ps = _this.pageSize();
            if (!rows || ps <= 0)
                return 0;
            var max = Math.ceil(rows.length / ps);
            return max;
        }, this).extend(delayExtender);
        this.maxPage.subscribe(function (x) {
            if (x < 1)
                x = 1;
            if (_this.currentPage() > x) {
                _this.currentPage(x);
            }
        });
        this.currentPage.subscribe(function (x) {
            if (typeof x === 'string')
                x = parseInt(x);
            if (x === undefined || x === null || isNaN(x)) {
                _this.currentPage(1);
                return;
            }
            var max = _this.maxPage();
            if (max < 1)
                max = 1;
            if (x < 1) {
                _this.currentPage(1);
            } else if (x > max) {
                _this.currentPage(max);
            }
        });
    };
    KOGridModel.prototype.initialize = function (config) {
        if (config.data) {
            if (!config.columns)
                this.scaffold(config.data);
        }
        if (config.columns)
            this.columns = config.columns.map(function (value, index, array) {
                return new KOGridColumn(value);
            });
        if (config.pageSize !== undefined)
            this.pageSize(config.pageSize);

        // set itemsSource last, (i.e. set columns first) so that this.currentItems() recompute
        if (config.data) {
            if (ko.isObservable(config.data)) {
                this.itemsSource = config.data;
                this.update();
            } else {
                this.itemsSource(config.data);
            }
        }
    };

    KOGridModel.prototype.getRowGetter = function (iRow) {
        var getter = this.columns[iRow].data;
        if (!getter)
            return undefined;
        return function (x) {
            return ko.unwrap(getter(x));
        };
    };

    // used for filtering
    KOGridModel.prototype.filterRow = function (row, pattern) {
        for (var j = 0; j < this.columns.length; j++) {
            var getter = this.getRowGetter(j);
            if (!getter)
                continue;
            var dat = getter(row);
            if (!dat)
                continue;
            var sdat = ('' + dat).toLowerCase();
            if (sdat.indexOf(pattern) > -1)
                return true;
        }
        return false;
    };

    // used for sorting
    KOGridModel.prototype.sortArray = function (rows, column) {
        var getter = this.getRowGetter(column);
        if (!getter)
            return rows;
        return rows.sort(function (r1, r2) {
            var a = getter(r1);
            var b = getter(r2);
            var ta = typeof a;
            var tb = typeof b;
            if (ta !== tb)
                return 0;
            if (ta === 'number')
                return (a - b);
            if (ta === 'string' || (a instanceof Date && b instanceof Date))
                return a > b ? 1 : a === b ? 0 : -1;
            if (ta === 'boolean')
                return a === b ? 0 : a ? 1 : -1;
            return 0;
        });
    };

    // force new evaluation of computed variables
    KOGridModel.prototype.update = function () {
        this.reevaluateComputed(this.reevaluateComputed() + 1);
    };

    // as the name say
    KOGridModel.prototype.sort = function (column) {
        if (!this.columns[column].sortable())
            return;
        var getter = this.getRowGetter(column);
        if (!getter)
            return;

        var prev = this.sortColumn();
        if (prev === column) {
            if (this.columns[prev].sort() === 1 /* Asc */)
                this.columns[prev].sort(2 /* Desc */);
            else
                this.columns[prev].sort(1 /* Asc */);
        } else {
            if (prev !== undefined) {
                this.columns[prev].sort(0 /* None */);
            }
            this.sortColumn(column);
            this.columns[column].sort(1 /* Asc */);
        }
    };

    // change currentPage
    KOGridModel.prototype.moveTo = function (dst) {
        var cur = this.currentPage();
        if (typeof cur === 'string')
            cur = parseInt(cur);
        if (typeof cur !== 'number' || isNaN(cur))
            cur = 1;
        switch (dst) {
            case 0 /* First */:
                cur = 1;
                break;
            case 1 /* Previous */:
                cur--;
                break;
            case 2 /* Next */:
                cur++;
                break;
            case 3 /* Last */:
                cur = this.maxPage();
                break;
        }
        if (cur > this.maxPage())
            cur = this.maxPage();
        if (cur < 1)
            cur = 1;
        this.currentPage(cur);
    };

    // set some default columns properties from the data
    KOGridModel.prototype.scaffold = function (data) {
        data = ko.unwrap(data);
        var res = [];
        if ((typeof data.length !== 'number') || data.length === 0) {
            return res;
        }

        for (var p in ko.unwrap(data[0])) {
            var gc = {
                header: p,
                data: p
            };
            res.push(new KOGridColumn(gc));
        }
        this.columns = res;
    };
    return KOGridModel;
})();

// default template, for the model
(function () {
    function addTemplate(name, markup) {
        document.write("<script type='text/html' id='" + name + "'>" + markup + "<" + "/script>");
    }
    ;
    addTemplate("KOGridModelDefaultViewTemplate", "\
		<thead><tr><td data-bind=\"attr: {colspan: columns.length}\">\
			<!-- ko template: templateHeader -->\
			<!-- /ko -->\
		</td></tr></thead>\
		<thead>\
			<tr data-bind=\"foreach: columns\">\
			<th data-bind=\"attr: { style: headerStyle }, css: { hidden: !visible(), 'sort-none': sortable() && data && sort() === KOGridSortOrder.None, 'sort-asc': sort() === KOGridSortOrder.Asc, 'sort-desc': sort() === KOGridSortOrder.Desc }, click: function() { $parent.sort($index()); }\">\
				<!-- ko if: header -->\
				<span data-bind=\"text: header\"></span>\
				<!-- /ko -->\
				<!-- ko if: !header && headerTemplate -->\
					<!-- ko template: headerTemplate -->\
					<!-- /ko -->\
				<!-- /ko -->\
			</th>\
			</tr>\
		</thead>\
		<tbody data-bind=\"foreach: currentPageItems\">\
			<tr data-bind=\"foreach: $parent.columns\">\
			<td data-bind=\"attr: { style: cellStyle ? ko.unwrap(cellStyle($parent)) : null }, css: { hidden: !visible(), sorted: $parentContext.$parent.sortColumn() === $index() } \">\
				<!-- ko if: !dataTemplate && !template && data -->\
				<span data-bind=\"text: ko.unwrap(data($parent)) \"></span>\
				<!-- /ko -->\
				<!-- ko if: !dataTemplate && template -->\
				<span data-bind=\"template: { name: template, data: $parent }\"></span>\
				<!-- /ko -->\
				<!-- ko if: dataTemplate && data -->\
				<span data-bind=\"template: { name: dataTemplate, data: { value: data($parent) } }\"></span>\
				<!-- /ko -->\
			</td>\
			</tr>\
		</tbody><tfoot><tr><td data-bind=\"attr: {colspan: columns.length}\">\
			<!-- ko template: templateHeader -->\
			<!-- /ko -->\
		</td></tr></tfoot>");
    addTemplate("KOGridModelDefaultViewHeaderTemplate", "\
			<div style=\"position:relative;\">\
				<div style=\"position:absolute; right:0;\">\
					<b>Search:</b> <input type=\"text\" style=\"width:9em;\" autocomplete='off' data-bind=\"attr: { 'class': inputClass }, value:filter, valueUpdate: 'afterkeydown'\"/>\
				</div>\
				<b>Display:</b> <select style=\"width:5em;\" data-bind=\"attr: { 'class': inputClass }, options:paginations, value: pageSize\"></select>\
				out of <span data-bind=\"text:numItems\"></span> entries, \
				<b>Page:</b>\
				<div class=\"btn-group\">\
					<button type=\"button\" class=\"small btn btn-default\" data-bind=\"click: function() { moveTo(KOGridNavigation.First); }\">1</button>\
					<button type=\"button\" class=\"small btn btn-default\" data-bind=\"click: function() { moveTo(KOGridNavigation.Previous); }\">&lsaquo;</button>\
					<button type=\"button\" style=\"background:#428bca;color:white;\" class=\"small btn disabled\" data-bind=\"text: currentPage\"></button>\
					<button type=\"button\" class=\"small btn btn-default\" data-bind=\"click: function() { moveTo(KOGridNavigation.Next); }\">&rsaquo;</button>\
					<button type=\"button\" class=\"small btn btn-default\" data-bind=\"click: function() { moveTo(KOGridNavigation.Last); }, text: maxPage\"></button>\
				</div>\
			</div>\
		");

    // alternative header with editable currentPage text input
    addTemplate("KOGridModelDefaultViewHeaderTemplate2", "\
			<div style=\"position:relative;\">\
				<div style=\"position:absolute; right:0;\">\
					<b>Search:</b> <input type=\"text\" style=\"width:9em;\" autocomplete='off' data-bind=\"attr: { 'class': inputClass }, value:filter, valueUpdate: 'afterkeydown'\"/>\
				</div>\
				<b>Display:</b> <select style=\"width:5em;\" data-bind=\"attr: { 'class': inputClass }, options:paginations, value: pageSize\"></select>\
				out of <span data-bind=\"text:numItems\"></span> entries, \
				<b>Page:</b>\
				<div class=\"btn-group\">\
					<button type=\"button\" class=\"small btn btn-default\" data-bind=\"click: function() { moveTo(KOGridNavigation.First); }\">1</button>\
					<button type=\"button\" class=\"small btn btn-default\" data-bind=\"click: function() { moveTo(KOGridNavigation.Previous); }\">&lsaquo;</button>\
					<input type=\"text\" style=\"width:2em;float:left;background:#428bca;color:white;\" data-bind=\"attr: { 'class': inputClass }, value: currentPage\"></button>\
					<button type=\"button\" class=\"small btn btn-default\" data-bind=\"click: function() { moveTo(KOGridNavigation.Next); }\">&rsaquo;</button>\
					<button type=\"button\" class=\"small btn btn-default\" data-bind=\"click: function() { moveTo(KOGridNavigation.Last); }, text: maxPage\"></button>\
				</div>\
			</div>\
		");
})();


// this binding's model MUST be a KOGridModel
ko.bindingHandlers.kogrid = {
    init: function () {
        return { 'controlsDescendantBindings': true };
    },
    update: function (element, viewModelAccessor, allBindings, viewModel, bindingContext) {
        var model = viewModelAccessor();

        if (model && !(model instanceof KOGridModel))
            throw new Error("Binding's value MUST be a KOGridModel");
        if (element.nodeName.toUpperCase() !== "TABLE")
            throw new Error("Bound Element MUST be a TABLE tag");

        while (element.firstChild)
            ko.removeNode(element.firstChild);

        var ctx = bindingContext.extend(model);
        ctx.$parent = ctx.$data;
        ctx.$parentContext = bindingContext;
        ctx.$data = model;

        ko.renderTemplate(model.templateTable, ctx, null, element, "replaceChildren");
    }
};
//# sourceMappingURL=KOGridBinding.js.map
