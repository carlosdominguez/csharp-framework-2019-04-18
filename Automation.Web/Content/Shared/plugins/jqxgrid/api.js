﻿/// <reference path="../util/global.js" />

"use strict";
/*
* Magic Table Plugin
* Copyright (c) 2014 Carlos Dominguez
* Released under the MIT license
*/
var _jqxGrids = {};
$.jqxGridApi = {};
$.jqxGridApi.editors = {};
$.jqxGridApi.serverFilters = {};
$.jqxGridApi.fnOnAnyChanged = {};
$.jqxGridApi.rowsChanged = [];
$.jqxGridApi.rowsDeleted = [];
$.jqxGridApi.rowsChangedFindById = function (idGrid) {
    var result = null;

    //Look for the data
    for (var i = 0; i < $.jqxGridApi.rowsChanged.length; i++) {
        var temData = $.jqxGridApi.rowsChanged[i];
        if (temData.id == idGrid) {
            result = temData;
            break;
        }
    }

    //If not exist create automatically
    if (!result) {
        result = {
            id: idGrid,
            rows: []
        }
        $.jqxGridApi.rowsChanged.push(result);
    }
    return result;
};
$.jqxGridApi.rowsDeletedFindById = function (idGrid) {
    var result = null;

    //Look for the data
    for (var i = 0; i < $.jqxGridApi.rowsDeleted.length; i++) {
        var temData = $.jqxGridApi.rowsDeleted[i];
        if (temData.id == idGrid) {
            result = temData;
            break;
        }
    }

    //If not exist create automatically
    if (!result) {
        result = {
            id: idGrid,
            rows: []
        }
        $.jqxGridApi.rowsDeleted.push(result);
    }
    return result;
};
$.jqxGridApi.rowsChangedAdd = function (idGrid, objRow) {
    var dataChanged = $.jqxGridApi.rowsChangedFindById(idGrid);
    var isAdded = false;

    //if the row exists remove it
    for (var i = 0; i < dataChanged.rows.length; i++) {
        if (dataChanged.rows[i].uid == objRow.uid) {
            dataChanged.rows[i] = objRow;
            isAdded = true;
        }
    }

    //if not exists
    if (isAdded == false) {
        dataChanged.rows.push(objRow);
    }

    if (dataChanged.event) {
        dataChanged.event();
    }
};
$.jqxGridApi.rowsChangedDelete = function (idGrid, objRowId) {
    var dataChanged = $.jqxGridApi.rowsChangedFindById(idGrid);

    var dataDeleted = $.jqxGridApi.rowsDeletedFindById(idGrid);
    var dataRowToDelete = $(idGrid).jqxGrid('getrowdata', objRowId);
    dataDeleted.rows.push(dataRowToDelete);

    //if the row exists remove it
    for (var i = 0; i < dataChanged.rows.length; i++) {
        if (dataChanged.rows[i].uid == objRowId) {
            dataChanged.rows.splice(i, 1);
            break;
        }
    }

    if (dataChanged.event) {
        dataChanged.event();
    }
}
$.jqxGridApi.localStorageSelect = {};
$.jqxGridApi.localStorage = [];
$.jqxGridApi.localStorageFindById = function (idGrid) {
    var result = null;

    //Look for the data
    for (var i = 0; i < $.jqxGridApi.localStorage.length; i++) {
        var temData = $.jqxGridApi.localStorage[i];
        if (temData.id == idGrid) {
            result = temData;
            break;
        }
    }

    //If not exist create automatically
    if (!result) {
        result = {
            id: idGrid,
            rows: null,
            fnReload: null
        }
        $.jqxGridApi.localStorage.push(result);
    }
    return result;
};
$.jqxGridApi.getSelectedRows = function (idGrid) {
    //NOTE: Is not working for Server Paging Grids
    var $jqxGrid = $(idGrid);
    var rowIndexes = $jqxGrid.jqxGrid('selectedrowindexes');
    var list = [];

    for (var i = 0; i < rowIndexes.length; i++) {
        var dataRecord = $jqxGrid.jqxGrid('getrowdata', rowIndexes[i]);
        if (dataRecord) {
            list.push(dataRecord);
        }
    }

    return list;
};
$.jqxGridApi.allChangesSaved = function (idGrid, showMsg) {
    var $jqxGrid = $(idGrid);
    var result = true;
    var changes = $.jqxGridApi.rowsChangedFindById(idGrid).rows;
    if (changes.length > 0) {
        result = false;

        if (showMsg) {
            _showAlert({
                showTo: $jqxGrid.parent(),
                type: 'error',
                content: "Please save your changes.",
                animateScrollTop: false
            });
        }
    }
    return result;
}
$.jqxGridApi.getOneSelectedRow = function (idGrid, showMsg) {
    var $jqxGrid = $(idGrid);
    var rowIndex = $jqxGrid.jqxGrid('selectedrowindex');
    var dataRecord = $jqxGrid.jqxGrid('getrowdata', rowIndex);

    if (!dataRecord) {
        if (showMsg) {
            _showAlert({
                showTo: $jqxGrid.parent(),
                type: 'error',
                content: "Please select at least one row",
                animateScrollTop: false
            });
        }
    }

    return dataRecord;
}
$.jqxGridApi.onAnyChanges = function (idGrid, fn) {

    if (!$.jqxGridApi.fnOnAnyChanged[idGrid]) {
        $.jqxGridApi.fnOnAnyChanged[idGrid] = [];
    }

    $.jqxGridApi.fnOnAnyChanged[idGrid].push(fn);
};
$.jqxGridApi.clearGridElement = function (pidElement) {
    var $prevEl = $(pidElement).prev();
    var htmlNewDiv = "<div id='" + pidElement.replace("#", "") + "'></div>";

    if ($prevEl.length == 1) {
        $(pidElement).remove();
        $(htmlNewDiv).insertAfter($prevEl);
    } else {
        var $nextEl = $(pidElement).next();
        if ($nextEl.length == 1) {
            $(pidElement).remove();
            $(htmlNewDiv).insertBefore($nextEl);
        } else {
            var $parentEl = $(pidElement).parent();
            $(pidElement).remove();
            $parentEl.append(htmlNewDiv);
        }
    }
};
$.jqxGridApi.refreshJqxGridColumnSize = function () {
    if ($.jqxGridApi.localStorage.length > 0) {
        for (var i = 0; i < $.jqxGridApi.localStorage.length; i++) {
            var tempJqxGridInfo = $.jqxGridApi.localStorage[i];
            $(tempJqxGridInfo.id).jqxGrid('refresh');
            console.log("JqxGrid " + tempJqxGridInfo.id + " columns width refresh");
        }
    }
};
$.jqxGridApi.getSource = function (idGrid) {
    var $jqxGrid = $(idGrid);
    return $jqxGrid.jqxGrid('source').records;
};
$.jqxGridApi.getAllRows = function (idGrid) {
    var $jqxGrid = $(idGrid);
    var rows = [];

    for (var i = 0; i < $jqxGrid.jqxGrid('getdatainformation').rowscount; i++) {
        rows.push($jqxGrid.jqxGrid('getrenderedrowdata', i));
    }

    return rows;
};
$.jqxGridApi.setFilter = function(idGrid, data) {
    $.jqxGridApi.serverFilters[idGrid] = data;
}
$.jqxGridApi.getFilter = function (idGrid, data) {
    return $.jqxGridApi.serverFilters[idGrid];
}

//Default pconfigurations.
$.jqxGridApi.colProperties = [
    "cellsformat",
    "filteritems",
    "columntype",
    "columngroup",
    "filtertype",
    "pinned",
    "cellsrenderer",
    "cellclassname",
    "cellsalign",
    "align",
    "editable",
    "filterable",
    "sortable",
    "filter",
    "hidden",
    "value",
    "values",
    "validation",
    "aggregates",
    "aggregatesrenderer"
];
$.jqxGridApi.defaultOptions = {
    //Fix error duplicate /MC/MC SubAppPath because _callServer will add this
    //url: _getViewVar("SubAppPath") + '/Shared/JqxGrid',
    url: '/Shared/JqxGrid',
    widgets: {
        jqxListBox: {
            width: '100%',
            height: '500'
        }
    },
    options: {
        width: '100%',
        height: '500',
        theme: 'light'
    },
    source: {
        pagesize: 20
    }
};

// Create the grid in the interface
$.jqxGridApi.generateJqxGrid = function (pcurrentGrid) {
    // Prepare the data and configuration
    var source = null;
    var jqxGridOptions = {};
    var pconfiguration = pcurrentGrid["configuration"];
    var $dataGrid = $.jqxGridApi.localStorageFindById(pcurrentGrid.configuration.showTo);

    var fnRenderGridRows = function (params) {
        var data = {};
        for (var i = params.startindex; i < params.endindex; i++) {
            data[i] = $dataGrid.rows[i];
        }
        return data;
    }

    switch (pconfiguration.source.dataBinding) {
        //Data Binding Large Data Set Mode
        case "Large Data Set":
        case "Large Data Set Local":
            source = {
                localdata: $dataGrid.rows,
                //Data field's type. Possible values: 'string', 'date', 'number', 'float', 'int', 'bool'.
                datafields: pcurrentGrid.datafields,
                datatype: "array",
                hierarchy: pcurrentGrid.configuration.source.hierarchy,
                id: pcurrentGrid.configuration.source.id
            };

            if (pconfiguration.type && pconfiguration.type == "jqxTreeGrid") {
                jqxGridOptions = {
                    filterable: true,
                    sortable: true
                };
            } else {
                jqxGridOptions = {
                    showfilterrow: true,
                    filterable: true,
                    sortable: true
                };
            }

            break;

            //Data Binding Virtual Paging Mode
        case "Virtual Paging":
            source = {
                datatype: "array",
                localdata: {},
                pagesize: pconfiguration.source.pagesize,
                totalrecords: $dataGrid.rows.length,
                hierarchy: pcurrentGrid.configuration.source.hierarchy,
                id: pcurrentGrid.configuration.source.id
            };

            jqxGridOptions = {
                virtualmode: true,
                pageable: true,
                pagermode: 'simple',
                rendergridrows: fnRenderGridRows
            };
            break;

            //Data Binding Virtual Scrolling Mode
        case "Virtual Scrolling":
            source = {
                datatype: "array",
                localdata: {},
                totalrecords: $dataGrid.rows.length,
                hierarchy: pcurrentGrid.configuration.source.hierarchy,
                id: pcurrentGrid.configuration.source.id
            };

            jqxGridOptions = {
                virtualmode: true,
                rendergridrows: fnRenderGridRows
            };
            break;


            //Data Binding Server Paging Mode
        case "Server Paging":
            source =
            {
                datatype: "json",
                datafields: pcurrentGrid.datafields,
                url: _fixFrameworkURL(pconfiguration.url),
                type: "POST",
                data: {
                    paction: "list",
                    pjqxGridJson: _toJSON(pconfiguration)
                },
                filter: function () {
                    // update the grid and send a request to the server.
                    $(pconfiguration.showTo).jqxGrid('updatebounddata');

                },
                sort: function () {
                    // update the grid and send a request to the server.
                    $(pconfiguration.showTo).jqxGrid('updatebounddata');
                },
                pagesize: pconfiguration.source.pagesize,
                root: 'Rows',
                beforeprocessing: function (data) {
                    source.totalrecords = data.TotalRows;
                    $.jqxGridApi.setFilter(pconfiguration.showTo, {
                        FilterID: data.FilterID,
                        Where: data.Where,
                        Sort: data.Sort
                    });

                    //source.totalrecords = 39548;
                    //console.log(data);
                },
                hierarchy: pcurrentGrid.configuration.source.hierarchy,
                id: pcurrentGrid.configuration.source.id
            };

            jqxGridOptions = {
                //showfilterrow: true,
                filterable: true,
                sortable: true,
                //groupable: true,
                pageable: true,
                pagermode: 'simple',
                virtualmode: true
            };
            break;

            //Data Binding Server Scrolling Mode
        case "Server Scrolling":
            source =
            {
                datatype: "json",
                datafields: pcurrentGrid.datafields,
                url: pconfiguration.url,
                type: "POST",
                data: {
                    paction: "list",
                    pjqxGridJson: _toJSON(pconfiguration)
                },
                filter: function () {
                    // update the grid and send a request to the server.
                    $(pconfiguration.showTo).jqxGrid('updatebounddata');
                },
                sort: function () {
                    // update the grid and send a request to the server.
                    $(pconfiguration.showTo).jqxGrid('updatebounddata');
                },
                root: 'Rows',
                //totalrecords: 50000,
                beforeprocessing: function (data) {
                    source.totalrecords = data.TotalRows;
                    //source.totalrecords = 39548;
                },
                hierarchy: pcurrentGrid.configuration.source.hierarchy,
                id: pcurrentGrid.configuration.source.id
            };

            jqxGridOptions = {
                //showfilterrow: true,
                filterable: true,
                sortable: true,
                //groupable: true,
                virtualmode: true,
                pagesize: pconfiguration.source.pagesize
            };
            break;

            //Data Binding Normal Mode
        default:
            source = {
                datatype: "json",
                url: pconfiguration.url,
                type: "POST",
                data: {
                    paction: "list",
                    pjqxGridJson: _toJSON(pconfiguration)
                },
                //Data field's type. Possible values: 'string', 'date', 'number', 'float', 'int', 'bool'.
                datafields: pcurrentGrid.datafields,
                hierarchy: pcurrentGrid.configuration.source.hierarchy,
                id: pcurrentGrid.configuration.source.id
            };

            if (pconfiguration.type && pconfiguration.type == "jqxTreeGrid") {
                jqxGridOptions = {
                    filterable: true,
                    sortable: true
                };
            } else {
                jqxGridOptions = {
                    showfilterrow: true,
                    filterable: true,
                    sortable: true
                };
            }

            break;
    }

    //On changes rows
    if (pconfiguration.onUpdateRow) {
        source.updaterow = pconfiguration.onUpdateRow;
    } else {
        source.updaterow = function (rowid, rowdata, commit) {
            $.jqxGridApi.rowsChangedAdd(pconfiguration.showTo, rowdata);
            // synchronize with the server - send update command
            // call commit with parameter true if the synchronization with the server was successful 
            // and with parameter false if the synchronization has failed.
            commit(true);
        }
    }

    //On add rows
    if (pconfiguration.onAddRow) {
        source.addrow = pconfiguration.onAddRow;
    } else {
        source.addrow = function (rowid, rowdata, position, commit) {
            $.jqxGridApi.rowsChangedAdd(pconfiguration.showTo, rowdata);
            // synchronize with the server - send insert command
            // call commit with parameter true if the synchronization with the server was successful. 
            // and with parameter false if the synchronization has failed.
            // you can pass additional argument to the commit callback which represents the new ID if it is generated from a Database. Example: commit(true, idInDB) where "idInDB" is the row's ID in the Database.
            commit(true);
        }
    }

    //On Delete rows
    if (pconfiguration.onDeleteRow) {
        source.deleterow = pconfiguration.onDeleteRow;
    } else {
        source.deleterow = function (rowid, commit) {
            $.jqxGridApi.rowsChangedDelete(pconfiguration.showTo, rowid);
            // synchronize with the server - send delete command
            // call commit with parameter true if the synchronization with the server was successful 
            // and with parameter false if the synchronization has failed.
            commit(true);
        }
    }

    //Create dataAdapter
    $dataGrid.source = source;
    var dataAdapter = new $.jqx.dataAdapter(source, {
        loadError: function (xhr, status, error) {
            _catchAjaxError(xhr, status, error);
        }
    });
    $dataGrid.dataAdapter = dataAdapter;

    //Only for Server Paging or Server Scrolling
    if (pconfiguration.source.dataBinding == "Server Paging" || pconfiguration.source.dataBinding == "Server Scrolling") {
        jqxGridOptions.rendergridrows = function () {
            return dataAdapter.records;
        }
    }

    // Default configuration for all types of jqxGrid
    if (typeof pconfiguration.columngroups != "undefined") {
        jqxGridOptions.columngroups = pconfiguration.columngroups;
    }
    
    if (typeof pconfiguration.options.showstatusbar != "undefined") {
        jqxGridOptions.showstatusbar = pconfiguration.options.showstatusbar;
    }

    if (typeof pconfiguration.options.statusbarheight != "undefined") {
        jqxGridOptions.statusbarheight = pconfiguration.options.statusbarheight;
    }

    if (typeof pconfiguration.options.showaggregates != "undefined") {
        jqxGridOptions.showaggregates = pconfiguration.options.showaggregates;
    }

    jqxGridOptions.autoheight = (typeof pconfiguration.options.autoheight != "undefined") ? pconfiguration.options.autoheight : true;
    jqxGridOptions.autorowheight = (typeof pconfiguration.options.autorowheight != "undefined") ? pconfiguration.options.autorowheight : true;
    jqxGridOptions.scrollfeedback = (typeof pconfiguration.options.scrollfeedback != "undefined") ? pconfiguration.options.scrollfeedback : null;
    jqxGridOptions.scrollmode = (typeof pconfiguration.options.scrollmode != "undefined") ? pconfiguration.options.scrollmode : null;
    jqxGridOptions.width = pconfiguration.options.width;
    jqxGridOptions.height = pconfiguration.options.height;
    jqxGridOptions.source = dataAdapter;
    jqxGridOptions.columns = pcurrentGrid.columns;
    jqxGridOptions.theme = pconfiguration.options.theme;
    jqxGridOptions.columnsresize = (typeof pconfiguration.options.columnsresize != "undefined") ? pconfiguration.options.columnsresize : true;
    jqxGridOptions.columnsreorder = true;
    jqxGridOptions.ready = pconfiguration.ready;
    jqxGridOptions.rowsheight = 33;

    if (jqxGridOptions.autorowheight) {
        jqxGridOptions.autoheight = true;
    }
    //jqxGridOptions.autoshowfiltericon = true;

    //Filter row
    if (pconfiguration.options.showfilterrow) {
        jqxGridOptions.filterable = true;
        jqxGridOptions.showfilterrow = true;
    } else {
        jqxGridOptions.filterable = false;
        jqxGridOptions.showfilterrow = false;
    }

    // Sortable
    jqxGridOptions.sortable = (pconfiguration.options.sortable) ? true : false;

    // Groupable
    if (pconfiguration.options.groupable) {
        jqxGridOptions.groupable = true;
        jqxGridOptions.groups = pconfiguration.groups ? pconfiguration.groups : [];
    }

    //Selection Mode
    //Examples: 'singlerow', 'multiplerows', 'checkbox', 'multiplerowsadvanced' or 'multiplerowsextended'.
    if (pconfiguration.options.selectionmode) {
        jqxGridOptions.selectionmode = pconfiguration.options.selectionmode;
    }

    //Browser Selection
    jqxGridOptions.enablebrowserselection = (typeof pconfiguration.options.enablebrowserselection != "undefined") ? pconfiguration.options.enablebrowserselection : false;

    //Editable
    if (pconfiguration.options.editable) {
        jqxGridOptions.editable = true;
        //jqxGridOptions.selectionmode = 'singlerow';
        jqxGridOptions.editmode = 'dblclick';
    }

    //Refresh data
    //pconfiguration.options.refresh
    //if (pconfiguration.options.refresh) {

    //}

    //Clear div of ShowTo, some times filters aren't working when another grid is created in same div
    if (pconfiguration.options.cleardiv) {
        $.jqxGridApi.clearGridElement(pconfiguration.showTo);
    }

    //Remove old grids
    var $currentJqxGrid = $(pconfiguration.showTo);
    var $newJqxGridDiv = $("<div id='jqxGridTemp'></div>");
    $currentJqxGrid.before($newJqxGridDiv);
    $newJqxGridDiv.attr("id", $(pconfiguration.showTo).attr("id"));
    $newJqxGridDiv.attr("class", $(pconfiguration.showTo).attr("class"));
    $currentJqxGrid.remove();

    //Create jqxGrid
    $(pconfiguration.showTo).jqxGrid(jqxGridOptions);
    _console(pconfiguration.showTo, jqxGridOptions);

    var idGrid = pcurrentGrid.configuration.showTo;

    // handle select and unselect for Paging Grids.
    //$(idGrid).on('rowselect', function (event) {
    //    var args = event.args;
    //    var rowBoundIndex = args.rowindex;
    //    var rowData = args.row;
    //    console.log('rowselect', rowData);
    //});

    //$(idGrid).on('rowunselect', function (event) {
    //    var args = event.args;
    //    var rowBoundIndex = args.rowindex;
    //    var rowData = args.row;
    //    console.log('rowunselect', rowData);
    //});

    //Create Hide and Show col Widget
    if (pconfiguration.widgets.jqxListBox.showTo) {
        var listSource = pcurrentGrid.listSourceColWidget;
        var idJqxListBox = pconfiguration.widgets.jqxListBox.showTo;

        $(idJqxListBox).jqxListBox({
            source: listSource,
            width: pconfiguration.widgets.jqxListBox.width,
            height: pconfiguration.widgets.jqxListBox.height,
            checkboxes: true,
            theme: pconfiguration.options.theme
        });
        $(idJqxListBox).on('checkChange', function (event) {
            $(idGrid).jqxGrid('beginupdate');
            if (event.args.checked) {
                $(idGrid).jqxGrid('showcolumn', event.args.value);
            }
            else {
                $(idGrid).jqxGrid('hidecolumn', event.args.value);
            }
            $(idGrid).jqxGrid('endupdate');
        });

        //Uncheck button configuration
        if (pconfiguration.widgets.jqxListBox.btnUncheckID) {
            var idBtnUncheck = pconfiguration.widgets.jqxListBox.btnUncheckID;
            $(idBtnUncheck).click(pconfiguration.widgets.jqxListBox.btnUncheckClick);
        }
    }

    //Create Context Menu
    if (pconfiguration.widgets.jqxMenu && pconfiguration.widgets.jqxMenu.showTo) {
        pcurrentGrid.contextMenu = $(pconfiguration.widgets.jqxMenu.showTo).jqxMenu({
            width: '100%',
            //height: 60,
            autoOpenPopup: false,
            mode: 'popup',
            theme: pconfiguration.options.theme
        });

        $(idGrid).on('contextmenu', function () {
            return false;
        });

        // handle context menu clicks.
        $(idGrid).on('rowclick', function (event) {
            if (event.args.rightclick) {
                $(idGrid).jqxGrid('selectrow', event.args.rowindex);
                var scrollTop = $(window).scrollTop();
                var scrollLeft = $(window).scrollLeft();
                pcurrentGrid.contextMenu.jqxMenu('open', parseInt(event.args.originalEvent.clientX) + 5 + scrollLeft, parseInt(event.args.originalEvent.clientY) + 5 + scrollTop);
                return false;
            }
        });
        $(pconfiguration.widgets.jqxMenu.showTo).on('itemclick', pconfiguration.widgets.jqxMenu.itemclick);
    }
};

// Validate the configuration or wait for json in virtual mode, then execute generateJqxGrid
$.jqxGridApi.create = function (pconfiguration) {

    var allOk = true;
    var message = "";

    //Hacer un merge con las opciones que nos enviaron y las default.
    _mergeObject($.jqxGridApi.defaultOptions, pconfiguration);

    //Validate pconfiguration
    if (!pconfiguration.sp || (!pconfiguration.sp.Name && !pconfiguration.sp.SQL)) {
        if (!pconfiguration.source || !pconfiguration.source.rows) {
            allOk = false;
            message += "$.jqxGridApi have a missing parameter: <pre>pconfiguration.sp.Name or pconfiguration.sp.SQL or pconfiguration.source.rows</pre>";
        }
    }

    if (!pconfiguration.showTo) {
        allOk = false;
        message += "$.jqxGridApi have a missing parameter: <pre>pconfiguration.showTo</pre>";
    }

    //Create pcurrentGrid.datafields and pcurrentGrid.columns
    var tempCol = null;
    var tempComputedColumns = [];
    var listColumns = [];

    //Init default variables
    var _currentGrid = {};
    _currentGrid.datafields = [];
    _currentGrid.columns = [];
    _currentGrid.listSourceColWidget = [];
    _currentGrid.configuration = pconfiguration;
    _jqxGrids[pconfiguration.showTo] = _currentGrid;

    for (var i = 0; i < pconfiguration.columns.length && allOk; i++) {
        //If ignore property doesn't exist add default value
        if (typeof pconfiguration.columns[i].ignore == "undefined") {
            pconfiguration.columns[i].ignore = false;
        }

        //Validate
        if (!pconfiguration.columns[i].name) {
            allOk = false;
            message += "$.jqxGridApi have a missing parameter: <pre>pconfiguration.columns[" + i + "].name</pre>";
        } else {
            if (!pconfiguration.columns[i].type) {
                allOk = false;
                message += "$.jqxGridApi have a missing parameter: <pre>pconfiguration.columns[" + i + "].type</pre>";
            } else {

                //Add data field
                _currentGrid.datafields.push({
                    name: pconfiguration.columns[i].name,
                    type: pconfiguration.columns[i].type
                });

                //Add Column
                tempCol = {};

                //Add Column Properties
                for (var p = 0; p < $.jqxGridApi.colProperties.length; p++) {
                    if (typeof pconfiguration.columns[i][$.jqxGridApi.colProperties[p]] != "undefined") {
                        tempCol[$.jqxGridApi.colProperties[p]] = pconfiguration.columns[i][$.jqxGridApi.colProperties[p]];

                        //Change the property "filter" with string, because it's can be a variable or function and we get problems
                        //with Json decode in the server
                        if ($.jqxGridApi.colProperties[p] == "filter") {
                            pconfiguration.columns[i][$.jqxGridApi.colProperties[p]] = "";
                        }
                    }
                }

                //Add pcurrentGrid.columns Defaults Properties
                tempCol.text = pconfiguration.columns[i].text ? pconfiguration.columns[i].text : pconfiguration.columns[i].name;
                tempCol.width = pconfiguration.columns[i].width ? pconfiguration.columns[i].width : '150';

                if (pconfiguration.columns[i].ignore) {
                    tempCol.ignore = true;
                }

                tempCol.datafield = pconfiguration.columns[i].name;
                _currentGrid.columns.push(tempCol);

                if (pconfiguration.columns[i].computedcolumn === true) {
                    //pconfiguration.columns.splice(i, 1);
                    tempComputedColumns.push({
                        index: i,
                        value: pconfiguration.columns[i]
                    });
                    delete pconfiguration.columns[i];
                }

                //Remove array from columns type list
                if (pconfiguration.columns[i] && pconfiguration.columns[i].list) {
                    listColumns.push({
                        index: i,
                        list: pconfiguration.columns[i].list,
                        text: (typeof pconfiguration.columns[i].text != "undefined" ? pconfiguration.columns[i].text : pconfiguration.columns[i].list.datafield)
                    });
                    delete pconfiguration.columns[i].list;
                }

                //Add List Source to hide and show pcurrentGrid.columns
                _currentGrid.listSourceColWidget.push({
                    label: tempCol.text,
                    value: tempCol.datafield,
                    checked: true
                });
            }
        }
    }

    //Validate Parameters
    var spParams;

    if (pconfiguration.sp && pconfiguration.sp.Params && pconfiguration.sp.Params.length > 0) {
        spParams = pconfiguration.sp.Params;

        for (var i = 0; i < spParams.length; i++) {
            var objParam = spParams[i];

            if (typeof objParam.Name == 'undefined' || typeof objParam.Value == 'undefined') {
                allOk = false;
                message = "Ajax error, please review and fix missing properties 'Name' and 'Value' in Params: <pre>" + _toJSON(pconfiguration.sp);
                break;
            }
        }
    }

    if (allOk) {
        //Validate Virtual Data
        if (pconfiguration.source.dataBinding && (pconfiguration.source.dataBinding == "Large Data Set" || pconfiguration.source.dataBinding == "Virtual Paging" || pconfiguration.source.dataBinding == "Virtual Scrolling")) {
            var $dataGrid = $.jqxGridApi.localStorageFindById(_currentGrid.configuration.showTo);

            var successFirstCall = function (response) {
                //On Data Loaded
                if (pconfiguration.sp.OnDataLoaded) {
                    pconfiguration.sp.OnDataLoaded(response);
                }

                $dataGrid.rows = response;

                //Add Computed columns
                for (var i = 0; i < tempComputedColumns.length; i++) {
                    _currentGrid.configuration.columns[tempComputedColumns[i].index] = tempComputedColumns[i].value;
                }

                //Add List attribute
                var listAdapters = {};
                for (var i = 0; i < listColumns.length; i++) {
                    var tempList = listColumns[i].list;
                    //_currentGrid.configuration.columns[listColumns[i].index].list = tempList;

                    if (tempList && tempList.array) {

                        //Create adapter
                        listAdapters[tempList.datafield] = new $.jqx.dataAdapter({
                            datatype: "array",
                            datafields: [
                                { name: tempList.displayfield, type: 'string' },
                                { name: tempList.datafield, type: 'string' }
                            ],
                            localdata: tempList.array
                        }, {
                            autoBind: true
                        });

                        //Update Datafield
                        _currentGrid.datafields.push({
                            name: tempList.displayfield,
                            value: tempList.datafield,
                            values: { source: listAdapters[tempList.datafield].records, value: tempList.datafield, name: tempList.displayfield }
                        });

                        //Update colum definition
                        _currentGrid.columns[listColumns[i].index].text = (typeof listColumns[i].text != "undefined" ? listColumns[i].text : tempList.datafield);
                        _currentGrid.columns[listColumns[i].index].datafield = tempList.datafield;
                        _currentGrid.columns[listColumns[i].index].displayfield = tempList.displayfield;
                        _currentGrid.columns[listColumns[i].index].createeditor = tempList.createeditor;

                        //Add information of the editor
                        $.jqxGridApi.editors[tempList.datafield] = {
                            source: listAdapters[tempList.datafield],
                            displayMember: tempList.displayfield,
                            valueMember: tempList.datafield
                        };

                        // update the editor's value before saving it.
                        _currentGrid.columns[listColumns[i].index].cellvaluechanging = function (row, column, columntype, oldvalue, newvalue) {
                            // return the old value, if the new value is empty.
                            if (newvalue == "") return oldvalue;
                        }

                    } else {
                        _showAlert({
                            type: 'error',
                            content: "$.jqxGridApi have a missing parameter: <pre>pconfiguration.columns[" + i + "].list.array</pre>"
                        });
                    }

                }

                //Remove ignore columns
                for (var i = 0; i < _currentGrid.columns.length; i++) {
                    if (_currentGrid.columns[i].ignore) {
                        delete _currentGrid.columns[i];
                        //delete _currentGrid.datafields[i];
                        delete _currentGrid.configuration.columns[i];
                    }
                }

                $.jqxGridApi.generateJqxGrid(_currentGrid);
                _hideLoadingTopMessage();
            };
            var successReload = function (response) {
                $dataGrid.rows = response;
                $dataGrid.source.localdata = $dataGrid.rows;

                if ($(_currentGrid.configuration.showTo).length == 1) {
                    $(_currentGrid.configuration.showTo).jqxGrid('updatebounddata');
                }

                _hideLoadingTopMessage();
            };
            var ajaxOptions = {
                url: pconfiguration.url,
                data: {
                    paction: "list",
                    pjqxGridJson: _toJSON(pconfiguration)
                },
                beforeSend: function () {
                    _showLoadingTopMessage("Loading data from server...");
                },
                error: function (xhr, textStatus, thrownError) {
                    _showAlert({
                        "xhr": xhr,
                        "error": thrownError
                    });
                },
                type: "POST"
            };

            //Load JSon and wait for it
            ajaxOptions.success = successFirstCall;
            //console.log(ajaxOptions);

            if(typeof _currentGrid.configuration.loadingMsgType != "undefined"){
                ajaxOptions.loadingMsgType = _currentGrid.configuration.loadingMsgType;
            }
            
            if (typeof _currentGrid.configuration.loadingMsg != "undefined") {
                ajaxOptions.loadingMsg = _currentGrid.configuration.loadingMsg;
            }

            _callServer(ajaxOptions);
            //$.ajax(ajaxOptions);

            //If we need reload the data
            $dataGrid.fnReload = function (spConfig) {
                ajaxOptions.success = successReload;
                //ajaxOptions.async = false;

                //Add new parameters
                if (spConfig) {
                    pconfiguration.sp = spConfig;
                }

                ajaxOptions.data.pjqxGridJson = _toJSON(pconfiguration);

                //Refresh until all the request are completed
                var t = setInterval(function () {
                    if (_ajaxIsComplete()) {

                        _callServer(ajaxOptions);
                        //$.ajax(ajaxOptions);
                        clearInterval(t);

                    }
                }, 500);

            };

        } else if (pconfiguration.source.dataBinding == "Large Data Set Local") {
            var $dataGrid = $.jqxGridApi.localStorageFindById(_currentGrid.configuration.showTo);
            $dataGrid.rows = pconfiguration.source.rows;

            //Add Computed columns
            for (var i = 0; i < tempComputedColumns.length; i++) {
                _currentGrid.configuration.columns[tempComputedColumns[i].index] = tempComputedColumns[i].value;
            }

            //Add List attribute
            var listAdapters = {};
            for (var i = 0; i < listColumns.length; i++) {
                var tempList = listColumns[i].list;
                //_currentGrid.configuration.columns[listColumns[i].index].list = tempList;

                if (tempList && tempList.array) {

                    //Create adapter
                    listAdapters[tempList.datafield] = new $.jqx.dataAdapter({
                        datatype: "array",
                        datafields: [
                            { name: tempList.displayfield, type: 'string' },
                            { name: tempList.datafield, type: 'string' }
                        ],
                        localdata: tempList.array
                    }, {
                        autoBind: true
                    });

                    //Update Datafield
                    _currentGrid.datafields.push({
                        name: tempList.displayfield,
                        value: tempList.datafield,
                        values: { source: listAdapters[tempList.datafield].records, value: tempList.datafield, name: tempList.displayfield }
                    });

                    //Update colum definition
                    _currentGrid.columns[listColumns[i].index].text = (typeof listColumns[i].text != "undefined" ? listColumns[i].text : tempList.datafield);
                    _currentGrid.columns[listColumns[i].index].datafield = tempList.datafield;
                    _currentGrid.columns[listColumns[i].index].displayfield = tempList.displayfield;
                    _currentGrid.columns[listColumns[i].index].createeditor = tempList.createeditor;

                    //Add information of the editor
                    $.jqxGridApi.editors[tempList.datafield] = {
                        source: listAdapters[tempList.datafield],
                        displayMember: tempList.displayfield,
                        valueMember: tempList.datafield
                    };

                    // update the editor's value before saving it.
                    _currentGrid.columns[listColumns[i].index].cellvaluechanging = function (row, column, columntype, oldvalue, newvalue) {
                        // return the old value, if the new value is empty.
                        if (newvalue == "") return oldvalue;
                    }

                } else {
                    _showAlert({
                        type: 'error',
                        content: "$.jqxGridApi have a missing parameter: <pre>pconfiguration.columns[" + i + "].list.array</pre>"
                    });
                }

            }

            //Remove ignore columns
            for (var i = 0; i < _currentGrid.columns.length; i++) {
                if (_currentGrid.columns[i].ignore) {
                    delete _currentGrid.columns[i];
                    //delete _currentGrid.datafields[i];
                    delete _currentGrid.configuration.columns[i];
                }
            }

            $.jqxGridApi.generateJqxGrid(_currentGrid);

            //If we need reload the data
            $dataGrid.fnReload = function (newRows) {
                $dataGrid.rows = newRows;
                $dataGrid.source.localdata = $dataGrid.rows;

                if ($(_currentGrid.configuration.showTo).length == 1) {
                    $(_currentGrid.configuration.showTo).jqxGrid('updatebounddata');
                }
            };

        } else {
            $.jqxGridApi.generateJqxGrid(_currentGrid);
        }
    } else {
        _showAlert({
            type: 'error',
            content: message
        });
    }
};

//Create editors for combobox and dropdownlists
$.jqxGridApi.createEditor = function (type, columnID, row, value, editor) {
    if (type == "dropdownlist") {

        // assign a new data source to the dropdownlist.
        editor.jqxDropDownList({
            source: $.jqxGridApi.editors[columnID].source,
            displayMember: $.jqxGridApi.editors[columnID].displayMember,
            valueMember: $.jqxGridApi.editors[columnID].valueMember,
            //autoDropDownHeight: true,
            promptText: "Please Choose:"
        });
    }

    if (type == "combobox") {
        // assign a new data source to the combobox.
        editor.jqxComboBox({
            source: $.jqxGridApi.editors[columnID].source,
            displayMember: $.jqxGridApi.editors[columnID].displayMember,
            valueMember: $.jqxGridApi.editors[columnID].valueMember,
            //autoDropDownHeight: true,
            promptText: "Please Choose:"
        });
    }
};

// Create Grid to find a employee
$.jqxGridApi.idPopupFindEmployee = 1;
$.jqxGridApi.createGridFindEmployee = function (pconfiguration) {
    var idBodyPopup;
    var idPopup;
    var idBtnPopupClose;
    var showTo = pconfiguration.showTo;

    if (showTo == "popup") {
        idPopup = "FindEmployee" + $.jqxGridApi.idPopupFindEmployee;
        $.jqxGridApi.idPopupFindEmployee++;

        _createPopup(idPopup, "Please select the employee", "", null);

        idBodyPopup = idPopup + "BodyPopup";
        idBtnPopupClose = idPopup + "btnClosePopup";

        pconfiguration.showTo = "#" + idBodyPopup;
    }

    var $jqxGrid = $(pconfiguration.showTo);

    $.jqxGridApi.create({
        showTo: pconfiguration.showTo,
        options: {
            //for comments or descriptions
            height: "400",
            autoheight: false,
            autorowheight: false,
            selectionmode: (pconfiguration.selectionmode ? pconfiguration.selectionmode : 'checkbox'),
            showfilterrow: true
        },
        sp: {
            Name: "[HRTTDB].[dbo].[FROU_spAdminEmployeeList]",
            Params: [
                { Name: "@ManagerSOEID", Value: (pconfiguration.managerSOEID ? pconfiguration.managerSOEID : _getSessionSOEID()) }
            ]
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        columns: [
            { name: 'SOEID', text: 'SOEID', type: 'string', filtertype: 'input', width: '9%' },
            { name: 'NAME', text: 'Name', type: 'string', filtertype: 'input', width: '29%' },
            { name: 'FIRST_NAME', text: 'First name', type: 'string', filtertype: 'input', width: '15%' },
            { name: 'LAST_NAME', text: 'Last name', type: 'string', filtertype: 'input', width: '15%' },
            { name: 'EMAIL_ADDR', text: 'Email address', type: 'string', filtertype: 'input', width: '29%' }
        ],
        ready: function () {
            //Automatic select employees selected before
            if (pconfiguration.selectedEmployees) {
                var rows = $jqxGrid.jqxGrid('getrows');
                var objRow;

                for (var i = 0; i < rows.length; i++) {
                    objRow = rows[i];
                    if (existInSelectedSoeids(objRow.SOEID)) {
                        $jqxGrid.jqxGrid('selectrow', objRow.uid);
                    }
                }
            }

            function existInSelectedSoeids(psoeid) {
                var result = false;
                for (var i = 0; i < pconfiguration.selectedEmployees.length; i++) {
                    if (psoeid.toLowerCase() == pconfiguration.selectedEmployees[i].toLowerCase()) {
                        result = true;
                        break;
                    }
                }
                return result;
            }
        }
    });

    //With substring remove "." and "#" for example id=".titleBig-select-employees"
    var idBtnSelectEmployees = pconfiguration.showTo.substring(1, pconfiguration.showTo.length) + "-select-employees";
    var idBtnSearchEmployees = pconfiguration.showTo.substring(1, pconfiguration.showTo.length) + "-search-employees";

    //Add select employees button
    $jqxGrid.before('<input class="AutoWH" type="button" id="' + idBtnSelectEmployees + '" name="' + idBtnSelectEmployees + '" value="Select" title="Select">');

    $("#" + idBtnSelectEmployees).click(function () {
        var rowIndexes = $jqxGrid.jqxGrid('selectedrowindexes');
        var listSOEIDs = [];

        for (var i = 0; i < rowIndexes.length; i++) {
            dataRecord = $jqxGrid.jqxGrid('getrowdata', rowIndexes[i]);
            listSOEIDs.push(dataRecord);
        }

        if (pconfiguration.onSelected) {
            pconfiguration.onSelected(listSOEIDs);
        }
        if (showTo == "popup") {
            $('#' + idBtnPopupClose).click();
        }
    });

    //Add Search button
    $jqxGrid.before('<input class="AutoWH" type="button" id="' + idBtnSearchEmployees + '" name="' + idBtnSearchEmployees + '" value="Load data base in filter" title="Load data base in filter"><pre id="' + idBtnSearchEmployees + '-filter"></pre>');

    $("#" + idBtnSearchEmployees).click(function () {
        //Get filters values
        var filters = $jqxGrid.jqxGrid("getfilterinformation");
        var params = [];
        var filterInfo;
        var filterMsg = "";
        var tempFilterValue;

        for (var i = 0; i < filters.length; i++) {
            filterInfo = filters[i];

            params.push({
                Name: "@" + filterInfo.datafield,
                Value: filterInfo.filter.getfilters()[0].value
            });
            tempFilterValue = filterInfo.filter.getfilters()[0].value;

            //if is string set "contains" if not set "="
            //if (isNaN(tempFilterValue)) {
            filterMsg += filterInfo.filtercolumntext + " contains '" + tempFilterValue + "' | ";
            //} else {
            //    filterMsg += filterInfo.filtercolumntext + " equals " + tempFilterValue + " | ";
            //}
        }

        //Add filter message
        if (filterMsg) {
            $("#" + idBtnSearchEmployees + "-filter").html("<b>Main Filter:</b>" + filterMsg);

            var spConfig = {
                Name: "[dbo].[FROU_spAdminEmployeeList]",
                Params: params
            }

            $.jqxGridApi.localStorageFindById(pconfiguration.showTo).fnReload(spConfig);
            $jqxGrid.jqxGrid('updatebounddata');

        } else {
            $("#" + idBtnSearchEmployees + "-filter").html("Enter a filter in some input field to load the data.");
        }

    });
};

// Create Grid to select a Managed Segment
$.jqxGridApi.idPopupFindManagedSegment = 1;
$.jqxGridApi.createGridFindManagedSegment = function (pconfiguration) {
    var idBodyPopup;
    var idPopup;
    var idBtnPopupClose;
    var showTo = pconfiguration.showTo;

    if (showTo == "popup") {
        idPopup = "FindManagedSegment" + $.jqxGridApi.idPopupFindManagedSegment;
        $.jqxGridApi.idPopupFindManagedSegment++;

        _createPopup(idPopup, "Please select the managed segment", "", null);

        idBodyPopup = idPopup + "BodyPopup";
        idBtnPopupClose = idPopup + "btnClosePopup";

        pconfiguration.showTo = "#" + idBodyPopup;
    }

    var $jqxGrid = $(pconfiguration.showTo);

    $.jqxGridApi.create({
        showTo: pconfiguration.showTo,
        options: {
            //for comments or descriptions
            height: "400",
            autoheight: false,
            autorowheight: true,
            selectionmode: (pconfiguration.selectionmode ? pconfiguration.selectionmode : 'checkbox'),
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[SP_QM_ManagedSegmentList]",
            Params: []
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        columns: [
            { name: 'ManagedSegment', text: 'Managed Segment', type: 'string', filtertype: 'input', width: '20%' },
            { name: 'ManagedSegmentDescription', text: 'Managed Segment Description', type: 'string', filtertype: 'input', width: '77%' }
        ],
        ready: function () {
            //Automatic select managed segments selected before
            if (pconfiguration.selectedManagedSegments) {
                var rows = $jqxGrid.jqxGrid('getrows');
                var objRow;

                for (var i = 0; i < rows.length; i++) {
                    objRow = rows[i];
                    if (existInSelectedManagedSegments(objRow.ManagedSegment)) {
                        $jqxGrid.jqxGrid('selectrow', objRow.uid);
                    }
                }
            }

            function existInSelectedManagedSegments(pmanagedSegment) {
                var result = false;
                for (var i = 0; i < pconfiguration.selectedManagedSegments.length; i++) {
                    if (pmanagedSegment == pconfiguration.selectedManagedSegments[i]) {
                        result = true;
                        break;
                    }
                }
                return result;
            }
        }
    });

    //With substring remove "." and "#" for example id=".titleBig-select-managed-segment"
    var idBtnSelectManagedSegment = pconfiguration.showTo.substring(1, pconfiguration.showTo.length) + "-select-managed-segment";
    var idBtnSearchManagedSegment = pconfiguration.showTo.substring(1, pconfiguration.showTo.length) + "-search-managed-segment";

    //Add select managed segment button
    $jqxGrid.before('<input class="AutoWH" type="button" id="' + idBtnSelectManagedSegment + '" name="' + idBtnSelectManagedSegment + '" value="Select" title="Select">');

    $("#" + idBtnSelectManagedSegment).click(function () {
        var rowIndexes = $jqxGrid.jqxGrid('selectedrowindexes');
        var listManagedSegments = [];

        for (var i = 0; i < rowIndexes.length; i++) {
            dataRecord = $jqxGrid.jqxGrid('getrowdata', rowIndexes[i]);
            listManagedSegments.push(dataRecord);
        }

        if (pconfiguration.onSelected) {
            pconfiguration.onSelected(listManagedSegments);
        }
        if (showTo == "popup") {
            $('#' + idBtnPopupClose).click();
        }
    });

    //Add Search button
    $jqxGrid.before('<input class="AutoWH" type="button" id="' + idBtnSearchManagedSegment + '" name="' + idBtnSearchManagedSegment + '" value="Load data base in filter" title="Load data base in filter"><pre id="' + idBtnSearchManagedSegment + '-filter"></pre>');

    $("#" + idBtnSearchManagedSegment).click(function () {
        //Get filters values
        var filters = $jqxGrid.jqxGrid("getfilterinformation");
        var params = [];
        var filterInfo;
        var tempFilterValue;
        var filterMsg = "";

        for (var i = 0; i < filters.length; i++) {
            filterInfo = filters[i];

            params.push({
                Name: "@" + filterInfo.datafield,
                Value: filterInfo.filter.getfilters()[0].value
            });
            tempFilterValue = filterInfo.filter.getfilters()[0].value;

            if (isNaN(tempFilterValue)) {
                filterMsg += filterInfo.filtercolumntext + " contains '" + tempFilterValue + "' | ";
            } else {
                filterMsg += filterInfo.filtercolumntext + " equals " + tempFilterValue + " | ";
            }

        }

        //Add filter message
        if (filterMsg) {
            $("#" + idBtnSearchManagedSegment + "-filter").html("<b>Main Filter:</b>" + filterMsg);

            var spConfig = {
                Name: "[dbo].[SP_QM_ManagedSegmentList]",
                Params: params
            }

            $.jqxGridApi.localStorageFindById(pconfiguration.showTo).fnReload(spConfig);
            $jqxGrid.jqxGrid('updatebounddata');
        } else {
            $("#" + idBtnSearchManagedSegment + "-filter").html("Enter a filter in some input field to load the data.");
        }

    });
};

// Create Grid to find a GLMS Training
$.jqxGridApi.idPopupFindGLMSTraining = 1;
$.jqxGridApi.createGridFindGLMSTraining = function (pconfiguration) {
    var idBodyPopup;
    var idPopup;
    var idBtnPopupClose;
    var showTo = pconfiguration.showTo;

    if (showTo == "popup") {
        idPopup = "FindGLMSTraining" + $.jqxGridApi.idPopupFindGLMSTraining;
        $.jqxGridApi.idPopupFindGLMSTraining++;

        _createPopup(idPopup, "Please select the GLMS Training", "", null);

        idBodyPopup = idPopup + "BodyPopup";
        idBtnPopupClose = idPopup + "btnClosePopup";

        pconfiguration.showTo = "#" + idBodyPopup;
    }

    var $jqxGrid = $(pconfiguration.showTo);

    $.jqxGridApi.create({
        showTo: pconfiguration.showTo,
        options: {
            //for comments or descriptions
            height: "400",
            autoheight: false,
            autorowheight: false,
            selectionmode: (pconfiguration.selectionmode ? pconfiguration.selectionmode : 'checkbox'),
            showfilterrow: true,
            editable: true
        },
        sp: {
            Name: "[dbo].[FROU_spAdminGLMSTrainingList]",
            Params: []
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        columns: [
            { name: 'IDActivity', text: 'IDActivity', width: '9%', type: 'number', filtertype: 'number', editable: true },
            {
                name: 'Training', text: 'Training', width: '450px', type: 'html', filtertype: 'input', cellsrenderer: function (rowIndex, datafield, value) {
                    var result = '';
                    var dataRecord = $(pconfiguration.showTo).jqxGrid('getrowdata', rowIndex);

                    result = '<a target="_blank" style="line-height: 25px; padding-left: 5px;" href="https://training.citigroup.net/SumTotal/app/management/LMS_ActDetails.aspx?UserMode=0&amp;ActivityId=' + dataRecord.IDActivity + '">' + dataRecord.Training + '</a>';

                    return result;
                }
            },

            { name: 'Code', text: 'Code', type: 'string', filtertype: 'input', editable: true },
            { name: 'ActivityType', text: 'ActivityType', type: 'string', filtertype: 'input', editable: false, filterable: false },
            { name: 'ContentType', text: 'ContentType', type: 'string', filtertype: 'input', editable: false, filterable: false },
            { name: 'CreditHours', width: '7%', text: 'CreditHours', type: 'number', filtertype: 'number', editable: false, filterable: false },
            { name: 'Region', text: 'Region', width: '260px', type: 'string', filtertype: 'checkedlist', editable: false, filterable: false }
        ],
        ready: function () {
            //Automatic select GLMS Training selected before
            if (pconfiguration.selectedGLMSTrainings) {
                var rows = $jqxGrid.jqxGrid('getrows');
                var objRow;

                for (var i = 0; i < rows.length; i++) {
                    objRow = rows[i];
                    if (existInSelectedGLMSTrainings(objRow.IDActivity)) {
                        $jqxGrid.jqxGrid('selectrow', objRow.uid);
                    }
                }
            }

            function existInSelectedGLMSTrainings(pidActivity) {
                var result = false;
                for (var i = 0; i < pconfiguration.selectedGLMSTrainings.length; i++) {
                    if (pidActivity == pconfiguration.selectedGLMSTrainings[i]) {
                        result = true;
                        break;
                    }
                }
                return result;
            }
        }
    });

    //With substring remove "." and "#" for example id=".titleBig-select-employees"
    var idBtnSelectGLMSTrainings = pconfiguration.showTo.substring(1, pconfiguration.showTo.length) + "-select-GLMSTrainings";
    var idBtnSearchGLMSTrainings = pconfiguration.showTo.substring(1, pconfiguration.showTo.length) + "-search-GLMSTrainings";

    //Add select GLMSTrainings button
    $jqxGrid.before('<input class="AutoWH" type="button" id="' + idBtnSelectGLMSTrainings + '" name="' + idBtnSelectGLMSTrainings + '" value="Select" title="Select">');

    $("#" + idBtnSelectGLMSTrainings).click(function () {
        var rowIndexes = $jqxGrid.jqxGrid('selectedrowindexes');
        var listGLMSTrainings = [];

        for (var i = 0; i < rowIndexes.length; i++) {
            dataRecord = $jqxGrid.jqxGrid('getrowdata', rowIndexes[i]);
            listGLMSTrainings.push(dataRecord);
        }

        if (pconfiguration.onSelected) {
            pconfiguration.onSelected(listGLMSTrainings);
        }
        if (showTo == "popup") {
            $('#' + idBtnPopupClose).click();
        }
    });

    //Add Search button
    $jqxGrid.before('<input class="AutoWH" type="button" id="' + idBtnSearchGLMSTrainings + '" name="' + idBtnSearchGLMSTrainings + '" value="Load data base in filter" title="Load data base in filter"><pre id="' + idBtnSearchGLMSTrainings + '-filter"></pre>');

    $("#" + idBtnSearchGLMSTrainings).click(function () {
        //Get filters values
        var filters = $jqxGrid.jqxGrid("getfilterinformation");
        var params = [];
        var filterInfo;
        var filterMsg = "";
        var tempFilterValue;

        for (var i = 0; i < filters.length; i++) {
            filterInfo = filters[i];

            params.push({
                Name: "@" + filterInfo.datafield,
                Value: filterInfo.filter.getfilters()[0].value
            });
            tempFilterValue = filterInfo.filter.getfilters()[0].value;

            //if is string set "contains" if not set "="
            //if (isNaN(tempFilterValue)) {
            filterMsg += filterInfo.filtercolumntext + " contains '" + tempFilterValue + "' | ";
            //} else {
            //    filterMsg += filterInfo.filtercolumntext + " equals " + tempFilterValue + " | ";
            //}
        }

        //Add filter message
        if (filterMsg) {
            $("#" + idBtnSearchGLMSTrainings + "-filter").html("<b>Main Filter:</b>" + filterMsg);

            var spConfig = {
                Name: "[dbo].[FROU_spAdminGLMSTrainingList]",
                Params: params
            }

            $.jqxGridApi.localStorageFindById(pconfiguration.showTo).fnReload(spConfig);
            $jqxGrid.jqxGrid('updatebounddata');

        } else {
            $("#" + idBtnSearchGLMSTrainings + "-filter").html("Enter a filter in some input field to load the data.");
        }

    });
};