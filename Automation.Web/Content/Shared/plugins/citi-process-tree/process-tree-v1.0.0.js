var _processCollapseCache = {};

function _createPopupSelectProcess(customOptions) {
    //Default Options.
    var options = {
        height: "full",
        imgInfoPath: _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png'
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    var htmlContentModal = "<div id='popupContainerProcessTree'></div>";
    var objProcessTreePlugin = null;

    _showModal({
        width: '70%',
        modalId: "SelectProcessTree",
        addCloseButton: true,
        buttons: [{
            name: "Select Process",
            class: "btn-success",
            closeModalOnClick: false,
            onClick: function ($modal) {
                //Trigger _showModal.onReady.objProcessTreePlugin.onSelect
                objProcessTreePlugin.fnOnSelect();
            }
        }],
        title: "Please select Process:",
        onReady: function ($modal) {
            //Create Process Tree Table
            objProcessTreePlugin = _createProcessTree({
                height: options.height,
                idContainer: "popupContainerProcessTree",
                hideSelectBtn: true,
                onSelect: function (objProcessTree) {

                    if (options.onSelect) {
                        options.onSelect(objProcessTree);
                    }

                    //Close Modal
                    $modal.find(".close").click();
                }
            });
        },
        contentHtml: htmlContentModal
    });
}

function _createProcessTree(customOptions) {
    //Default Options.
    var options = {
        height: "full",
        idContainer: "containerProcessTree",
        idTable: "tblProcessTree-" + _createCustomID(),
        hideSelectBtn: false,
        imgInfoPath: _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png'
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    //Add header with rows count and filter
    var $header = $(
        ' <div class="form-horizontal" style="width:100%; float: left;margin-top: 15px;"> ' +
        '     <div class="row"> ' +
        '         <div class="col-md-11"> ' +
        '             <div class="row"> ' +
        '                 <div class="col-md-6"> ' +
        '                     <!末 L1 Process 末> ' +
        '                     <div class="form-group"> ' +
        '                         <label for="selProcessL2" class="col-md-4 control-label" style="text-align: left;"><span class="badge badge-info">L2</span> Product:</label> ' +
        '                         <div class="col-md-8"> ' +
        '                             <select class="form-control selProcessL2"></select> ' +
        '                         </div> ' +
        '                     </div> ' +
        '                 </div> ' +
        '                 <div class="col-md-6"> ' +
        '                     <!末 L2 Process 末> ' +
        '                     <div class="form-group"> ' +
        '                         <label for="selProcessL3" class="col-md-4 control-label" style="text-align: left;"><span class="badge badge-purple">L3</span> Menu of Services:</label> ' +
        '                         <div class="col-md-8"> ' +
        '                             <select class="form-control selProcessL3"></select> ' +
        '                         </div> ' +
        '                     </div> ' +
        '                 </div> ' +
        '             </div> ' +
        '             <div class="row"> ' +
        '                 <div class="col-md-6"> ' +
        '                     <!末 L3 Process 末> ' +
        '                     <div class="form-group"> ' +
        '                         <label for="selProcessL4" class="col-md-4 control-label" style="text-align: left;"><span class="badge badge-accent">L4</span> Sub-Process:</label> ' +
        '                         <div class="col-md-8"> ' +
        '                             <select class="form-control selProcessL4"></select> ' +
        '                         </div> ' +
        '                     </div> ' +
        '                 </div> ' +
        '                 <div class="col-md-6"> ' +
        '                     <!末 L4 Process 末> ' +
        '                     <div class="form-group"> ' +
        '                         <label for="selProcessL5" class="col-md-4 control-label" style="text-align: left;"><span class="badge badge-primary">L5</span> Activity:</label> ' +
        '                         <div class="col-md-8"> ' +
        '                             <select class="form-control selProcessL5"></select> ' +
        '                         </div> ' +
        '                     </div> ' +
        '                 </div> ' +
        '             </div> ' +
        '         </div> ' +
        '     </div> ' +
        ' </div> '
    );

    //Add Process Tree Table
    var $tblProcessTree = $('<div id="' + options.idTable + '"></div>');

    //Add footer with select button
    var $footer = $(
        '<div class="form-horizontal"> ' +
        '    <div class="row"> ' +
        '        <div class="col-md-3"></div> ' +
        '        <div class="col-md-7"></div> ' +
        '        <div class="col-md-2"> ' +
        '            <button type="button" class="btn btn-primary facebook btnSelectProcessTree" style="margin-top: 6px;"><i class="fa fa-check-square-o"></i> Select Process </button> ' +
        '        </div> ' +
        '    </div> ' +
        '</div>');

    //Add elements to container
    $("#" + options.idContainer).append($header);
    $("#" + options.idContainer).append($tblProcessTree);

    if (options.hideSelectBtn == false) {
        $("#" + options.idContainer).append($footer);
    }

    //On change Process L1 Filter
    _loadFilterProcess("selProcessL2", 2, null, $header);
    $header.find(".selProcessL2").change(function () {

        //Clear Process L4
        $header.find(".selProcessL4").val("0");
        $header.find(".selProcessL4").contents().remove();

        //Clear Process L5
        $header.find(".selProcessL5").val("0");
        $header.find(".selProcessL5").contents().remove();

        //Load Process L3
        _loadFilterProcess("selProcessL3", 3, true, $header);

        //Load Table
        loadTableProcessTree();
    });

    $header.find(".selProcessL3").change(function () {

        //Clear Process L5
        $header.find(".selProcessL5").val("0");
        $header.find(".selProcessL5").contents().remove();

        //Load Process L4
        _loadFilterProcess("selProcessL4", 4, true, $header);

        //Load Table
        loadTableProcessTree();
    });

    $header.find(".selProcessL4").change(function () {

        //Load Process L5
        _loadFilterProcess("selProcessL5", 5, true, $header);

        //Load Table
        loadTableProcessTree();
    });

    $header.find(".selProcessL5").change(function () {
        //Load Table
        loadTableProcessTree();
    });

    //On click select MCA
    options.fnOnSelect = function () {
        var selectedRow = $.jqxGridApi.getOneSelectedRow("#" + options.idTable, true);
        if (selectedRow) {
            if (options.onSelect) {
                options.onSelect(selectedRow);
            }
        }
    }

    //On click Select Process
    $footer.find(".btnSelectProcessTree").click(options.fnOnSelect);

    //Load Process Tree Data and Table
    function loadTableProcessTree() {

        _getProcessData({
            $filterContainer: $header,
            onReady: function (resultList) {
                _createProcessTable({
                    idTable: "#" + options.idTable,
                    processList: resultList
                });
            }
        });

    }
    loadTableProcessTree();

    return options;
}

function _refreshFilterProcessByChange($filterContainer, plevel) {
    var selectedValue = $filterContainer.find(".selProcessL" + plevel).val();
    _loadFilterProcess("selProcessL" + plevel, plevel, null, $filterContainer, selectedValue);

    var parentLevel = (plevel - 1);
    if (parentLevel > 1) {
        var selectedValueParent = $filterContainer.find(".selProcessL" + parentLevel).val();
        _loadFilterProcess("selProcessL" + parentLevel, parentLevel, null, $filterContainer, selectedValueParent);
    }
}

function _loadFilterProcess(pidSelect, plevel, pclearSelect, $filterContainer, pselectedValue) {
    var sqlProcess =
        " SELECT \n" +
	    "     P.[ID], \n" +
	    "     P.[IDParent], \n" +
	    "     P.[Level], \n" +
	    "     P.[Category], \n" +
	    "     P.[Key], \n" +
	    "     P.[Name], \n" +
	    "     P.[IsDeleted], \n" +
	    "     (SELECT COUNT(1) FROM [Automation].[dbo].[tblProcess] T1 WHERE T1.[IDParent] = P.[ID] AND T1.[IsDeleted] = 0)	AS [CantChilds] \n" +
        " FROM \n" +
	    "     [Automation].[dbo].[tblProcess] P \n" +
        " WHERE \n" +
	    "     P.[IsDeleted] = 0 AND \n" +
	    "     P.[Level] = " + plevel + " :pFilters \n "
    " ORDER BY \n" +
    "     P.[Name] \n";

    if (pclearSelect) {
        $filterContainer.find("." + pidSelect).val("0");
        $filterContainer.find("." + pidSelect).contents().remove();
    }

    var sqlFilters = _getSQLFiltersProcess($filterContainer, plevel);

    //Add Filters
    sqlProcess = _replaceAll(":pFilters", sqlFilters, sqlProcess);

    _callServer({
        loadingMsg: "Loading L" + plevel + " Process Filter...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': _toJSON(sqlProcess) },
        type: "POST",
        success: function (resultList) {
            $filterContainer.find("." + pidSelect).contents().remove();

            //Add All option
            $filterContainer.find("." + pidSelect).append($('<option>', {
                value: 0,
                text: "All"
            }));

            //Render Process Options
            for (var i = 0; i < resultList.length; i++) {
                var objProcess = resultList[i];

                $filterContainer.find("." + pidSelect).append($('<option>', {
                    selected: (pselectedValue == objProcess.ID),
                    value: objProcess.ID,
                    text: objProcess.Key + " " + objProcess.Name + " (" + objProcess.CantChilds + ")"
                }));
            }

            //Create Select2
            //$("#" + pidSelect).select2();
        }
    });
}

function _getSQLFiltersProcess($filterContainer, plevel) {
    var sqlFilters = "";

    //Process L5
    if (sqlFilters == "" && plevel != 5 && $filterContainer.find(".selProcessL5").val() && $filterContainer.find(".selProcessL5").val() != "0") {
        sqlFilters += " AND (P.[ID] = " + $filterContainer.find(".selProcessL2").val() + " OR \n";
        sqlFilters += "      P.[ID] = " + $filterContainer.find(".selProcessL3").val() + " OR \n";
        sqlFilters += "      P.[ID] = " + $filterContainer.find(".selProcessL4").val() + " OR \n";
        sqlFilters += "      P.[ID] IN (SELECT F.[ID] FROM [Automation].[dbo].[fnAFrwkProcessTreeGetChildsList](" + $filterContainer.find(".selProcessL5").val() + ") F) ) \n";
    }

    //Process L4
    if (sqlFilters == "" && plevel != 4 && $filterContainer.find(".selProcessL4").val() && $filterContainer.find(".selProcessL4").val() != "0") {
        sqlFilters += " AND (P.[ID] = " + $filterContainer.find(".selProcessL2").val() + " OR \n";
        sqlFilters += "      P.[ID] = " + $filterContainer.find(".selProcessL3").val() + " OR \n";
        sqlFilters += "      P.[ID] IN (SELECT F.[ID] FROM [Automation].[dbo].[fnAFrwkProcessTreeGetChildsList](" + $filterContainer.find(".selProcessL4").val() + ") F) ) \n";
    }

    //Process L3
    if (sqlFilters == "" && plevel != 3 && $filterContainer.find(".selProcessL3").val() && $filterContainer.find(".selProcessL3").val() != "0") {
        sqlFilters += " AND (P.[ID] = " + $filterContainer.find(".selProcessL2").val() + " OR \n";
        sqlFilters += "      P.[ID] IN (SELECT F.[ID] FROM [Automation].[dbo].[fnAFrwkProcessTreeGetChildsList](" + $filterContainer.find(".selProcessL3").val() + ") F) ) \n";
    }

    //Process L2
    if (sqlFilters == "" && plevel != 2 && $filterContainer.find(".selProcessL2").val() && $filterContainer.find(".selProcessL2").val() != "0") {
        sqlFilters += " AND P.[ID] IN (SELECT F.[ID] FROM [Automation].[dbo].[fnAFrwkProcessTreeGetChildsList](" + $filterContainer.find(".selProcessL2").val() + ") F) ";
    }

    return sqlFilters;
}

function _getProcessEntityToLeadDataByIDType(customOptions) {
    //Default Options.
    var options = {
        type: "entities",
        onReady: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    var sqlEntityToLead =
        " SELECT \n" +
	    "     E.[ID]    AS [IDEntityToLead] \n" +
        "    ,E.[Name]  AS [EntityToLeadName] \n" +
        " FROM \n" +
		"     [Automation].[dbo].[tblProcessEntityToLead] E \n" +
        " WHERE \n" +
		"     E.[IsDeleted] = 0 AND E.[IDType] = " + options.idType + " \n" +
        " ORDER BY \n" +
		"     E.[Name] \n";

    //Load data
    _callServer({
        loadingMsg: "Loading data of " + options.type + "...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': _toJSON(sqlEntityToLead) },
        type: "POST",
        success: function (resultList) {

            if (options.onReady) {
                options.onReady(resultList);
            }
        }
    });
}

function _getProcessEntityToLeadTypeData(customOptions) {
    //Default Options.
    var options = {
        onReady: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    var sqlEntityToLeadType =
        " SELECT \n" +
	    "     E.[ID]    AS [IDEntityToLeadType] \n" +
        "    ,E.[Name]  AS [EntityToLeadTypeName] \n" +
        " FROM \n" +
		"     [Automation].[dbo].[tblProcessEntityToLeadType] E \n" +
        " ORDER BY \n" +
		"     E.[Name] \n";

    //Load process information
    _callServer({
        loadingMsg: "Loading entity to lead types...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': _toJSON(sqlEntityToLeadType) },
        type: "POST",
        success: function (resultList) {

            if (options.onReady) {
                options.onReady(resultList);
            }
        }
    });
}

function _getProcessCriticalCategoryData(customOptions) {
    //Default Options.
    var options = {
        onReady: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    var sqlProcessCriticalCategory =
        " SELECT \n" +
	    "     C.[ID]    AS [IDCriticalCategory] \n" +
        "    ,C.[Name]  AS [CriticalCategoryName] \n" +
        " FROM \n" +
		"     [Automation].[dbo].[tblProcessCriticalCategory] C \n" +
        " ORDER BY \n" +
		"     C.[Name] \n";

    //Load process information
    _callServer({
        loadingMsg: "Loading Critical Categories...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': _toJSON(sqlProcessCriticalCategory) },
        type: "POST",
        success: function (resultList) {

            if (options.onReady) {
                options.onReady(resultList);
            }
        }
    });
}

function _getProcessData(customOptions) {
    //Default Options.
    var options = {
        $filterContainer: $(".filter-process"),
        showAll: false,
        onReady: function () { }
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    if (_userHaveRole("CPR_PROCESSTREE")) {
        var sqlProcess =
        " SELECT \n" +
	    "     P.[ID] \n" +
	    "    ,P.[IDParent] \n" +
        "    ,P.[IDCriticalCategory] \n" +
	    "    ,P.[GPLSOEID] \n" +
	    "    ,E.[NAME] AS [GPLName] \n" +
	    "    ,P.[Level] \n" +
	    "    ,P.[Category] \n" +
	    "    ,P.[Key] \n" +
	    "    ,P.[Name] \n" +
	    "    ,P.[FullName] \n" +
        "    ,RI.* \n" +
	    "    ,LI.* \n" +
	    "    ,SI.* \n" +
        "    ,UR.* \n" +
	    "    ,P.[CreatedBy] \n" +
	    "    ,P.[CreatedDate] \n" +
	    "    ,P.[ModifiedBy] \n" +
	    "    ,P.[ModifiedDate] \n" +
	    "    ,P.[IsDeleted] \n" +
	    "    ,(SELECT COUNT(1) FROM [Automation].[dbo].[tblProcess] T WHERE T.[IDParent] = P.[ID] AND T.[IsDeleted] = 0)	AS [CantChilds] \n" +
        " FROM \n" +
		"     [Automation].[dbo].[tblProcess] P \n" +
        "           LEFT JOIN [Automation].[dbo].[tblEmployee] E WITH(READUNCOMMITTED) ON P.[GPLSOEID] = E.[SOEID] \n" +
        "           OUTER APPLY( \n" +
		"           	SELECT * \n" +
		"           	FROM [Automation].[dbo].[fnAFrwkGetLineInfoRPL](P.[ID]) \n" +
		"           ) AS RI \n" +
        "           OUTER APPLY( \n" +
        "               SELECT * \n" +
        "               FROM [Automation].[dbo].[fnAFrwkGetLineInfoLead](P.[ID]) \n" +
        "           ) AS LI \n" +
        "           OUTER APPLY( \n" +
        "               SELECT * \n" +
        "               FROM [Automation].[dbo].[fnAFrwkGetLineInfoResource](P.[ID]) \n" +
        "           ) AS SI \n" +
        //---------------- NEW CODE USER ---------------------
        "           OUTER APPLY( \n" +
        "               SELECT * \n" +
        "               FROM [Automation].[dbo].[fnAFrwkGetLineInfoUser](P.[ID]) \n" +
        "           ) AS UR \n" +
        //----------------------------------------------------
        " WHERE \n" +
		"     (P.[IsDeleted] = 0 :pFilters) OR (P.[ID] = 1) \n " +
        " ORDER BY \n" +
		"     P.[FullName] \n";
    } else {
        var sqlProcess =
        " ;WITH PROCESS_ID_CTE ([ID] ,[IDParent] ) AS ( \n " +
        " SELECT \n " +
        " P.[ID] \n " +
        " ,P.[IDParent] \n " +     
        "  FROM \n" +
        " [Automation].[dbo].[tblProcess] P \n " +
        " LEFT JOIN [Automation].[dbo].[tblEmployee] E WITH(READUNCOMMITTED) ON P.[GPLSOEID] = E.[SOEID] \n " +
        " OUTER APPLY( \n " +
        " SELECT * \n " +
        " FROM [Automation].[dbo].[fnAFrwkGetLineInfoRPL](P.[ID]) \n " +
        " ) AS RI \n " +
        " OUTER APPLY( \n " +
        "               SELECT * \n " +
        "               FROM [Automation].[dbo].[fnAFrwkGetLineInfoLead](P.[ID]) \n " +
        "           ) AS LI \n " +
        " OUTER APPLY( \n " +
        "    SELECT * \n " +
        "    FROM [Automation].[dbo].[fnAFrwkGetLineInfoResource](P.[ID]) \n " +
        " ) AS SI \n " +
        " OUTER APPLY( \n " +
        "               SELECT * \n " +
        "               FROM [Automation].[dbo].[fnAFrwkGetLineInfoUser](P.[ID])  \n " +
        " ) AS UR  \n " +
        " WHERE  \n " +
        " ((P.[IsDeleted] = 0 ) OR (P.[ID] = 1) ) \n " +
        " AND P.[ID] IN (  \n " +
        " SELECT DISTINCT [IDProcess] \n " +
        " FROM [Automation].[dbo].[tblProcessXUser]  \n " +
        " WHERE [UserSOEID] = '" + _getSOEID() + "' AND [IsDeleted] = 0)  \n " +
        " ) \n " + 
        " SELECT \n" +
	    "     P.[ID] \n" +
	    "    ,P.[IDParent] \n" +
        "    ,P.[IDCriticalCategory] \n" +
	    "    ,P.[GPLSOEID] \n" +
	    "    ,E.[NAME] AS [GPLName] \n" +
	    "    ,P.[Level] \n" +
	    "    ,P.[Category] \n" +
	    "    ,P.[Key] \n" +
	    "    ,P.[Name] \n" +
	    "    ,P.[FullName] \n" +
        "    ,RI.* \n" +
	    "    ,LI.* \n" +
	    "    ,SI.* \n" +
        "    ,UR.* \n" +
	    "    ,P.[CreatedBy] \n" +
	    "    ,P.[CreatedDate] \n" +
	    "    ,P.[ModifiedBy] \n" +
	    "    ,P.[ModifiedDate] \n" +
	    "    ,P.[IsDeleted] \n" +
	    "    ,(SELECT COUNT(1) FROM [Automation].[dbo].[tblProcess] T WHERE T.[IDParent] = P.[ID] AND T.[IsDeleted] = 0)	AS [CantChilds] \n" +
        " FROM \n" +
		"     [Automation].[dbo].[tblProcess] P \n" +
        "           LEFT JOIN [Automation].[dbo].[tblEmployee] E WITH(READUNCOMMITTED) ON P.[GPLSOEID] = E.[SOEID] \n" +
        "           OUTER APPLY( \n" +
		"           	SELECT * \n" +
		"           	FROM [Automation].[dbo].[fnAFrwkGetLineInfoRPL](P.[ID]) \n" +
		"           ) AS RI \n" +
        "           OUTER APPLY( \n" +
        "               SELECT * \n" +
        "               FROM [Automation].[dbo].[fnAFrwkGetLineInfoLead](P.[ID]) \n" +
        "           ) AS LI \n" +
        "           OUTER APPLY( \n" +
        "               SELECT * \n" +
        "               FROM [Automation].[dbo].[fnAFrwkGetLineInfoResource](P.[ID]) \n" +
        "           ) AS SI \n" +
        //---------------- NEW CODE USER ---------------------
        "           OUTER APPLY( \n" +
        "               SELECT * \n" +
        "               FROM [Automation].[dbo].[fnAFrwkGetLineInfoUser](P.[ID]) \n" +
        "           ) AS UR \n" +
        //----------------------------------------------------
        " WHERE \n" +
		"     ((P.[IsDeleted] = 0 :pFilters) OR (P.[ID] = 1)) \n " +
        "     AND P.[ID] IN (   SELECT DISTINCT PROCESS_ID \n " +
        "                       FROM PROCESS_ID_CTE \n " +
        "                       UNPIVOT( \n " +
        "                       PROCESS_ID FOR COL IN ([ID],[IDParent]) \n " +
        "                       ) as IDs) \n " +
        " ORDER BY \n" +
		"     P.[FullName] \n";
    }
    

    var sqlFilters = "";

    if (!options.showAll) {
        sqlFilters = _getSQLFiltersProcess(options.$filterContainer);
    }

    //Add Filters
    sqlProcess = _replaceAll(":pFilters", sqlFilters, sqlProcess);

    //Load process information
    _callServer({
        loadingMsg: "Loading Process Tree...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': _toJSON(sqlProcess) },
        type: "POST",
        success: function (resultList) {

            if (options.onReady) {
                options.onReady(resultList);
            }
        }
    });
}

function _getProcessTitleSummary(processList, objProcess) {
    var result = "";
    var parents = _getProcessParentsRecursive(processList, objProcess.IDParent);
    for (var i = parents.length - 1; i >= 0; i--) {
        var objParent = parents[i];

        result += (objParent.Key ? (objParent.Key + ": ") : "") + "L" + objParent.Level + " " + objParent.Name + " \n";
    }

    result += (objProcess.Key ? (objProcess.Key + ": ") : "") + "L" + objProcess.Level + " " + objProcess.Name + " (" + objProcess.CantChilds + ")";
    return result;
}

function _createProcessTable(customOptions) {
    //Default Options.
    var options = {
        idTable: "",
        simple: true,
        useCollapseCache: false,
        processList: [],
        criticalCategoryList: [],
        onReady: function () {}
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    var jqxOptions = {
        //height: (($(window).height() < 700) ? "480" : "700"),
        autorowheight: false,
        //showfilterrow: true,
        sortable: true,
        editable: true,
        selectionmode: "singlerow"
    }

    if (options.height != "full") {
        jqxOptions.height = options.height;
        jqxOptions.autoheight = false;
    } else {
        jqxOptions.autoheight = true;
    }

    var columns = [
        { name: 'IsOpen', type: 'bool', hidden: true },
        { name: 'CantChilds', type: 'number', hidden: true },
        { name: 'ID', type: 'string', hidden: true },
        { name: 'IDParent', type: 'string', hidden: true },
        { name: 'Level', type: 'number', hidden: true },
        { name: 'Category', type: 'string', hidden: true },
        { name: 'GPLSOEID', type: 'string', hidden: true },
    ];

    columns.push({ name: 'Key', text: 'ID', type: 'string', width: (options.simple ? '10%' : '60px'), cellsalign: 'center', align: 'center', filtertype: 'input' });
    columns.push({
        name: 'Name', text: 'Name', width: (options.simple ? '90%' : '740px'), type: 'html', filtertype: 'input', cellsrenderer: function (rowIndex, datafield, value) {
            var resultHTML = '';
            var dataRecord = $(options.idTable).jqxGrid('getrowdata', rowIndex);
            var paddingXLevel = 35;
            var padding = paddingXLevel * (dataRecord.Level - 1);
            var htmlOpen = " (-)";
            var classIsOpenedOrClosed = "isOpened";

            //Set default open
            if (typeof dataRecord.IsOpen == "undefined") {
                dataRecord.IsOpen = true;
            }

            if (dataRecord.IsOpen == false || (options.useCollapseCache && _processCollapseCache[dataRecord.ID] == false)) {
                htmlOpen = " (+)";
                classIsOpenedOrClosed = "isClosed";
            }

            if (padding == 0) {
                padding = 2;
            }
            var htmlDeleted = "";
            if (_userHaveRole("CPR_PROCESSTREE")) {
                htmlDeleted = "<code class='pull-right' style='margin-top: 5px;margin-right: 3px;'>Parent was deleted, please reassign</code>";
            }
            

            var resultList = $.grep(options.processList, function (v) {
                return v.ID == dataRecord.IDParent;
            });

            if (resultList.length > 0 || dataRecord.IDParent === "") {
                htmlDeleted = "";
            }

            var levelText = 'L' + dataRecord.Level;
            var levelHtml;

            switch (dataRecord.Level) {
                case 2:
                    levelHtml = '<span class="badge badge-info">' + levelText + '</span>';
                    break;
                case 3:
                    levelHtml = '<span class="badge badge-purple">' + levelText + '</span>';
                    break;
                case 4:
                    levelHtml = '<span class="badge badge-accent">' + levelText + '</span>';
                    break;
                case 5:
                    levelHtml = '<span class="badge badge-primary">' + levelText + '</span>';
                    break;
                case 6:
                    levelHtml = '<span class="badge badge-warning">' + levelText + '</span>';
                    break;
                default:
                    levelHtml = levelText;
                    break;
            }

            var processSummaryTitle = _getProcessTitleSummary(options.processList, dataRecord);
            if (dataRecord.CantChilds > 0) {
                resultHTML =
                        ' <button type="button" rowUid="' + dataRecord.uid + '" idProcess="' + dataRecord.ID + '" idParent="' + dataRecord.IDParent + '" class="btn btn-xs btnOpenChilds btn-success ' + classIsOpenedOrClosed + '" aria-label="Left Align" style="margin-left: ' + padding + 'px; margin-top: 0px;">' +
                        '    ' + htmlOpen +
                        ' </button>' +
                        ' <span style="line-height: 28px; padding-left: 2px;" title="' + processSummaryTitle + '" > ' + levelHtml + ' ' + value + ' (' + dataRecord.CantChilds + ')</span>' + htmlDeleted;
            } else {
                resultHTML =
                        ' <button type="button" rowUid="' + dataRecord.uid + '" idProcess="' + dataRecord.ID + '" idParent="' + dataRecord.IDParent + '" class="btn btn-xs btnOpenChilds ' + classIsOpenedOrClosed + ' disabled" aria-label="Left Align" style="margin-left: ' + padding + 'px; margin-top: 0px;">' +
                        '    ' + htmlOpen +
                        ' </button>' +
                        ' <span style="line-height: 28px; padding-left: 2px;" title="' + processSummaryTitle + '" > ' + levelHtml + ' ' + value + ' (' + dataRecord.CantChilds + ')</span>' + htmlDeleted;
            }

            return resultHTML;
        }
    });

    if (options.simple != true) {
        columns.push({
            name: 'IDCriticalCategory', text: 'Critical Category', columntype: 'dropdownlist', type: 'string', filtertype: 'checkedlist', width: '235px',
            cellclassname: function (rowIndex, columnfield, value) {
                //var dataRecord = $(options.idTable).jqxGrid('getrowdata', rowIndex);
                //switch (dataRecord.IDCriticalCategory) {
                //    case "1":
                //        break;
                //    case "2":
                //        break;
                //    case "3":
                //        break;
                //    default:
                //        break;
                //}
                var className = "";

                switch (value) {
                    case "Citi Critical Process":
                        className = "processCitiCritical";
                        break;

                    case "FRSS KPA (Key Practice Activity)":
                        className = "processKPA";
                        break;

                    case "Non Critical Process":
                        className = "processNonCritical";
                        break;
                }

                return className;
            },
            list: {
                array: options.criticalCategoryList,
                datafield: 'IDCriticalCategory',
                displayfield: 'CriticalCategoryName',
                //text: 'Critical Category',
                //autoComplete: true,
                createeditor: function (row, value, editor) {
                    $.jqxGridApi.createEditor('dropdownlist', 'IDCriticalCategory', row, value, editor);
                }
            }
        });

        columns.push({
            name: 'CriticalCategoryOk', text: ' ', width: '40px', type: 'bool', filtertype: 'boolean', editable: false, filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                var dataRecord = $(options.idTable).jqxGrid('getrowdata', rowIndex);
                var isOk = false;
                var resultHmtl = "";

                if (dataRecord.CantChilds > 0 && (dataRecord.IDCriticalCategory > 1)) {
                    resultHmtl = '<i title="Only the last level of process can have Critical Category" style="cursor:pointer; color:red;width: 40px;text-align: center;font-size: 26px;padding: 5px;" class="fa fa-times-circle"></i>';
                }

                if (dataRecord.CantChilds == 0 && (dataRecord.IDCriticalCategory == 1)) {
                    resultHmtl = '<i title="A Critical Category is required for last level of process" style="cursor:pointer; color:red;width: 40px;text-align: center;font-size: 26px;padding: 5px;" class="fa fa-times-circle"></i>';
                }

                //var errorHtml = '<i style="color:red;width: 40px;text-align: center;font-size: 26px;padding: 5px;" class="fa fa-times-circle"></i>';
                //var successHtml = '<i style="color:green;width: 40px;text-align: center;font-size: 26px;padding: 5px;" class="fa fa-check-square-o"></i>';

                return resultHmtl;
            }
        });
        //---------------- NEW USER CODE ----------------------------
        columns.push({ name: 'UserJson', type: 'string', hidden: true });
        columns.push({
            name: 'UserCount', text: 'Users', width: '120px', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                var dataRecord = $(options.idTable).jqxGrid('getrowdata', rowIndex);
                var htmlResult = '<span style="line-height:31px;margin-left:12px;">' + (dataRecord.UserCount > 0 ? dataRecord.UserCount : "") + '</span>';

                //Add Users
                if (dataRecord.UserJson) {
                    var rows = JSON.parse(dataRecord.UserJson);
                    if (rows.length > 0) {
                        var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                        for (var i = 0; i < rows.length; i++) {
                            var tempData = rows[i];
                            infoHtml += tempData.UserName + " (" + tempData.UserSOEID + ")<br>";
                        }

                        infoHtml += "</div>";

                        htmlResult +=
                            '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="User Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                            '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/info.png"/> ' +
                            '</div>';
                    }
                }

                return htmlResult;
            }
        });
        //-----------------------------------------------------------
        columns.push({ name: 'GPLName', text: 'GPL', type: 'string', filtertype: 'checkedlist', width: '180px' });
        columns.push({ name: 'RPLJson', type: 'string', hidden: true });
        columns.push({
            name: 'RPLCount', text: 'RPLs', width: '120px', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                var dataRecord = $(options.idTable).jqxGrid('getrowdata', rowIndex);
                var htmlResult = '<span style="line-height:31px;margin-left:12px;">' + (dataRecord.RPLCount > 0 ? dataRecord.RPLCount : "") + '</span>';

                //Add RPLs
                if (dataRecord.RPLJson) {
                    var rows = JSON.parse(dataRecord.RPLJson);
                    if (rows.length > 0) {
                        var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                        for (var i = 0; i < rows.length; i++) {
                            var tempData = rows[i];
                            infoHtml += tempData.EntityToLeadName + ": " + tempData.RPLName + " (" + tempData.RPLSOEID + ")<br>";
                        }

                        infoHtml += "</div>";

                        htmlResult +=
                            '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="RPL Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                            '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/info.png"/> ' +
                            '</div>';
                    }
                }

                return htmlResult;
            }
        });
        columns.push({ name: 'LeadJson', type: 'string', hidden: true });
        columns.push({
            name: 'LeadCount', text: 'Leads', width: '120px', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                var dataRecord = $(options.idTable).jqxGrid('getrowdata', rowIndex);
                var htmlResult = '<span style="line-height:31px;margin-left:12px;">' + (dataRecord.LeadCount > 0 ? dataRecord.LeadCount : "") + '</span>';

                //Add Leads
                if (dataRecord.LeadJson) {
                    var rows = JSON.parse(dataRecord.LeadJson);
                    if (rows.length > 0) {
                        var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                        for (var i = 0; i < rows.length; i++) {
                            var tempData = rows[i];
                            infoHtml += tempData.EntityToLeadName + ": " + tempData.LeadName + " (" + tempData.LeadSOEID + ")<br>";
                        }

                        infoHtml += "</div>";

                        htmlResult +=
                            '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="Lead Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                            '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/info.png"/> ' +
                            '</div>';
                    }
                }

                return htmlResult;
            }
        });
        columns.push({ name: 'ResourceJson', type: 'string', hidden: true });
        columns.push({
            name: 'ResourceCount', text: 'Resources', width: '120px', type: 'string', filtertype: 'input', editable: false, filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                var dataRecord = $(options.idTable).jqxGrid('getrowdata', rowIndex);
                var htmlResult = '<span style="line-height:31px;margin-left:12px;">' + (dataRecord.ResourceCount > 0 ? dataRecord.ResourceCount : "") + '</span>';

                //Add Resources
                if (dataRecord.ResourceJson) {
                    var rows = JSON.parse(dataRecord.ResourceJson);
                    if (rows.length > 0) {
                        var infoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                        for (var i = 0; i < rows.length; i++) {
                            var tempData = rows[i];
                            infoHtml += "<a href='" + tempData.Link + "' target='_blank'>" + tempData.Link + "</a>" + (tempData.Description ? (": " + tempData.Description) : "") + "<br>";
                        }

                        infoHtml += "</div>";

                        htmlResult +=
                            '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + infoHtml + '" data-title="Resource Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                            '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/info.png"/> ' +
                            '</div>';
                    }
                }

                return htmlResult;
            }
        });
    }

    //Create table
    $.jqxGridApi.create({
        showTo: options.idTable,
        options: {
            //for comments or descriptions
            height: (($(window).height() < 700) ? "480" : "700"),
            autoheight: false,
            autorowheight: false,
            showfilterrow: true,
            sortable: true,
            editable: true,
            selectionmode: "singlerow"
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set Local",
            rows: options.processList
        },
        //type: string - text - number - int - float - date - time 
        //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
        //cellsformat: ddd, MMM dd, yyyy h:mm tt
        columns: columns,
        ready: function () {
            if (options.simple != true) {
                //Auto Save Changes
                $(options.idTable).on('cellvaluechanged', function (event) {
                    // event arguments.
                    var args = event.args;
                    // column data field.
                    var datafield = event.args.datafield;
                    // row's bound index.
                    var rowBoundIndex = args.rowindex;
                    // new cell value.
                    var value = args.newvalue;
                    // old cell value.
                    var oldvalue = args.oldvalue;

                    console.log(datafield);

                    if (datafield == "Key" || datafield == "Name" || datafield == "IDCriticalCategory" || datafield == "GPLName") {
                        var selectedRowData = $(options.idTable).jqxGrid('getrowdata', rowBoundIndex);
                        var saveInDB = true;

                        //Check if GPL Name is SOEID
                        if (datafield == "GPLName") {
                            if (selectedRowData.GPLName.length == 7) {
                                selectedRowData.GPLSOEID = selectedRowData.GPLName;
                            } else {
                                saveInDB = false;
                            }
                        }

                        //Check undefined or null in selectedRowData.IDCriticalCategory
                        if (!selectedRowData.IDCriticalCategory) {
                            selectedRowData.IDCriticalCategory = 0;
                        }

                        //Save data in DB
                        if (saveInDB) {
                            _callProcedure({
                                response: true,
                                name: "[Automation].[dbo].[spAFrwkProcessAdmin]",
                                params: [
                                    { "Name": "@Action", "Value": 'Update' },
                                    { "Name": "@IDProcess", "Value": selectedRowData.ID },
                                    { "Name": "@IDParent", "Value": selectedRowData.IDParent },
                                    { "Name": "@IDCriticalCategory", "Value": selectedRowData.IDCriticalCategory },
                                    { "Name": "@GPLSOEID", "Value": selectedRowData.GPLSOEID },
                                    { "Name": "@StrListRPLs", "Value": 'Skip' },
                                    { "Name": "@StrListLeads", "Value": 'Skip' },
                                    { "Name": "@StrListResources", "Value": 'Skip' },
                                    { "Name": "@Level", "Value": selectedRowData.Level },
                                    { "Name": "@Category", "Value": ('L' + selectedRowData.Level) },
                                    { "Name": "@Key", "Value": selectedRowData.Key },
                                    { "Name": "@Name", "Value": $.trim(selectedRowData.Name) },
                                    { "Name": "@SessionSOEID", "Value": _getSOEID() }
                                ],
                                success: {
                                    fn: function (responseID) {
                                        if (responseID.length > 0) {
                                            //show success alert
                                            _showNotification("success", "The information (L" + selectedRowData.Level + " " + selectedRowData.Name + ") was saved successfully.", "AlertProcess");

                                            //Refresh filters process
                                            if (options.onSavedProcess) {
                                                options.onSavedProcess(selectedRowData);
                                            }
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
            }

            $(options.idTable).on("rowclick", function (event) {
                //Show tooltips
                _GLOBAL_SETTINGS.tooltipsPopovers();
            });

            //Add event
            $(options.idTable).on("click", ".btnOpenChilds", function () {
                var $btn = $(this);
                onClickCollapse($btn.attr("idProcess"), $btn.hasClass("isOpened"), $btn.attr("rowuid"));
            });
            
            if (options.useCollapseCache) {
                restoreCollapseCache();
            }

            if (options.onReady) {
                options.onReady();
            }
        }
    });

    function restoreCollapseCache() {
        //Reload Clicked cache
        var listProcessKeys = Object.keys(_processCollapseCache);
        var refreshGrid = false;

        //Set default value
        $(options.processList).each(function () {
            var objProcess = this;
            if (typeof objProcess.IsVisible == "undefined") {
                objProcess.IsVisible = true;
            }
        });
        for (var i = 0; i < listProcessKeys.length; i++) {
            var idProcessCache = listProcessKeys[i];

            if (_processCollapseCache[idProcessCache] == false) {
                refreshGrid = true;
                var childsToHideOrShow = _getProcessChildsRecursive(options.processList, idProcessCache);

                $(options.processList).each(function () {
                    var objProcess = this;

                    var resultChildFound = $.grep(childsToHideOrShow, function (c) {
                        return c.ID == objProcess.ID;
                    });

                    if (resultChildFound.length > 0) {
                        objProcess.IsVisible = false;
                        objProcess.IsOpen = false;
                    }
                });
            }
        }

        if (refreshGrid) {
            var processToShow = $.grep(options.processList, function (p) {
                return p.IsVisible;
            });

            //Refresh table with new array
            $.jqxGridApi.localStorageFindById(options.idTable).fnReload(processToShow);
            $(options.idTable).jqxGrid('updatebounddata');

            //console.log($.jqxGridApi.localStorageFindById(options.idTable));
        }
    }

    function onClickCollapse(idProcess, isOpen, rowuid) {
        $(options.idTable).attr("ClickedRowBtnChild", rowuid);
        var childsToHideOrShow = _getProcessChildsRecursive(options.processList, idProcess);

        var clickedProcess;
        var resultClickedProcess = $.grep(options.processList, function (p) {
            return p.ID == idProcess;
        });

        if (resultClickedProcess.length > 0) {
            clickedProcess = resultClickedProcess[0];
        }

        clickedProcess.IsOpen = !isOpen;

        _processCollapseCache[idProcess] = !isOpen;

        $(options.processList).each(function () {
            var objProcess = this;

            //Set default value
            if (typeof objProcess.IsVisible == "undefined") {
                objProcess.IsVisible = true;
            }

            var resultChildFound = $.grep(childsToHideOrShow, function (c) {
                return c.ID == objProcess.ID;
            });

            if (resultChildFound.length > 0) {
                objProcess.IsVisible = !isOpen;
                objProcess.IsOpen = !isOpen;
            }
        });

        var processToShow = $.grep(options.processList, function (p) {
            return p.IsVisible;
        });

        //Refresh table with new array
        $.jqxGridApi.localStorageFindById(options.idTable).fnReload(processToShow);
        $(options.idTable).jqxGrid('updatebounddata');
    }

    
}

function _getProcessParentsRecursive(processList, idParent) {
    var result = [];
    var parents = $.grep(processList, function (v) {
        return v.ID == idParent;
    });

    //Add childs
    $.merge(result, parents);

    //Get Childs of childs
    for (var i = 0; i < parents.length; i++) {
        $.merge(result, _getProcessParentsRecursive(processList, parents[i].IDParent));
    }

    //Return result
    return result;
}

function _getProcessChildsRecursive(processList, idProcess) {
    var result = [];
    var childs = $.grep(processList, function (v) {
        return v.IDParent == idProcess;
    });

    //Add childs
    $.merge(result, childs);

    //Get Childs of childs
    for (var i = 0; i < childs.length; i++) {
        $.merge(result, _getProcessChildsRecursive(processList, childs[i].ID));
    }

    //Return result
    return result;
}
