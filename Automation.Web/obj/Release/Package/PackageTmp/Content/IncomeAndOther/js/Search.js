﻿
//#region SQL QUERIES
var entityDDL = "SELECT * FROM (SELECT  ' All' as ID, ' All' as VAL UNION SELECT DISTINCT [entityName] as ID,[entityName] as VAL FROM [dbo].[EntityMaster] ) as a order by a.ID";
var functionDDL = "SELECT * FROM (SELECT  ' All' as ID, ' All' as VAL  UNION SELECT DISTINCT [function] as ID,[function] as VAL FROM [dbo].[EntityMaster] ) as a order by a.ID";
var frequencyDDL = "SELECT * FROM (SELECT  ' All' as ID, ' All' as VAL  UNION SELECT DISTINCT [frequency]  as ID, [frequency]  as VAL FROM [dbo].[EntityMaster] ) as a order by a.ID";

var preparerDDL = "SELECT * FROM (SELECT  ' All' as ID, ' All' as VAL  UNION SELECT DISTINCT [preparer] as ID,[preparer] as VAL FROM [dbo].[FilingCalendar]WHERE preparer IS NOT NULL) as a order by a.ID";
var reviewerDDL = "SELECT * FROM (SELECT  ' All' as ID, ' All' as VAL  UNION  SELECT DISTINCT [reviewer] as ID, [reviewer] as VAL FROM [IncomeAndOtherQD].[dbo].[FilingCalendar]WHERE [reviewer] IS NOT NULL  ) as a order by a.ID";
var duemonthDDL = "SELECT  ' All' as ID, 0 as VAL UNION SELECT DISTINCT Datename(month,(filingDueDate)) as ID, month(filingDueDate) as VAL FROM [dbo].[FilingCalendar] order by VAL";
var dueyearDDL = "SELECT  ' All' as ID, ' All' as VAL UNION SELECT DISTINCT CONVERT(varchar(10),year(filingDueDate)) as ID , CONVERT(varchar(10),year(filingDueDate)) as VAL FROM [dbo].[FilingCalendar]";
var statusDDL = "SELECT  ' All' as ID, ' All' as VAL  UNION SELECT DISTINCT CASE WHEN [status] = 0 THEN (CASE WHEN statusPrepared = 0 THEN 'Incomplete' ELSE 'Prepared' END) ELSE (CASE WHEN statusPrepared = 1 " +
                "THEN 'Complete' ELSE 'Complete' END) END AS [ID],CASE WHEN [status] = 0 THEN (CASE WHEN statusPrepared = 0 THEN 'Incomplete' ELSE 'Prepared' END) ELSE (CASE WHEN statusPrepared = 1 THEN 'Complete' ELSE 'Complete' END) END AS [VAL] FROM [dbo].[FilingCalendar]";
var calendarTypeDDL = "SELECT  ' All' as ID, ' All' as VAL  UNION SELECT  'Default' as ID, 'Default' as VAL  UNION SELECT  'TrustReturnPackage' as ID, 'TrustReturnPackage' as VAL UNION SELECT  'AppointmentsPackage' as ID, 'AppointmentsPackage' as VAL  UNION SELECT  'TaxPackage' as ID, 'TaxPackage' as VAL";


//#endregion

//#region LOAD DROPLISTS
function loadDropDownlists(selectString, droplistId) {
    _showLoadingFullPage({ msg: "Loading Data..." });
    _callServer({
        loadingMsg: "Loading Data ...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify(selectString) },
        type: "post",
        success: function (resultList) {
            $("#" + droplistId + "").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $("#" + droplistId + "").append($('<option>', { value: objFunction.VAL, text: objFunction.ID, selected: (objFunction.ID == " All") }));
            }
            $('#ddlDueYear').val(new Date().getFullYear());
            _hideLoadingFullPage();
        }
    });
};
//#endregion

//#region DOCUMENT READY
$(document).ready(function () {
    _showLoadingFullPage({ msg: "Loading Data..." });
    
    //LOAD DROPLISTS
    loadDropDownlists(entityDDL, "ddlEntity");
    loadDropDownlists(functionDDL, "ddlFunction");
    loadDropDownlists(frequencyDDL, "ddlFrequency");
    loadDropDownlists(reviewerDDL, "ddlReviewer");
    loadDropDownlists(preparerDDL, "ddlPreparer");
    loadDropDownlists(duemonthDDL, "ddlDueMonth");
    loadDropDownlists(dueyearDDL, "ddlDueYear");
    loadDropDownlists(statusDDL, "ddlStatus");
    loadDropDownlists(calendarTypeDDL, "ddlCalendarType");


    _hideLoadingFullPage();
});
//#endregion

//#region LOAD TABLE
$("#btnSearch").click(function () {
    loadTableSearch();
    //$("#btnLock").prop('disabled', false);
    
});

function loadTableSearch() {
    $.jqxGridApi.create({
        showTo: "#tblFilingSearch",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_SC_CalendarListingSearch]",
            Params: [
                { Name: "@entity", Value: $("#ddlEntity :selected").val() },
                { Name: "@logid", Value: $("#txtLogID").val() },
                { Name: "@function", Value: $("#ddlFunction :selected").val() },
                { Name: "@frequency", Value: $("#ddlFrequency :selected").val() },
                { Name: "@reviewer", Value: $("#ddlReviewer :selected").val() },
                { Name: "@preparer", Value: $("#ddlPreparer :selected").val() },
                { Name: "@duemonth", Value: $("#ddlDueMonth :selected").val() },
                { Name: "@dueyear", Value: $("#ddlDueYear :selected").val() },
                { Name: "@status", Value: $("#ddlStatus :selected").val() },
                { Name: "@calendartype", Value: $("#ddlCalendarType :selected").val() }
            ]
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'logID', type: 'string' },
            { name: 'entityID', type: 'string', hidden: true },
            { name: 'entityName', type: 'string' },
            { name: 'function', type: 'string'},
            { name: 'frequency', type: 'string'},
            { name: 'reviewer', type: 'string'},
            { name: 'preparer', type: 'string'},
            { name: 'filingDueDate', text: 'Filing Due Date', type: 'date', width: '20%', cellsformat: "M/d/yyyy" },
            { name: 'filingID', type: 'string', hidden: true },
            { name: 'status', type: 'string', hidden: true },
            { name: 'statusPrepared', type: 'string', hidden: true },
            { name: 'statusDesc', type: 'string'},
            { name: 'statuspreparedDate', text: 'Prepared Date', type: 'date', cellsformat: "M/d/yyyy" },
            { name: 'commentFlag', type: 'string'},
            { name: 'counts', type: 'string'},
            { name: 'statusDate', text: 'Status Date', type: 'date', cellsformat: "M/d/yyyy" },
            { name: 'statusSOEID', type: 'string'},
            { name: 'statusPreparedDate', text: 'Status Prepared Date', type: 'date', cellsformat: "M/d/yyyy"},
            { name: 'statusPreparedSOEID', type: 'string'},
            { name: 'comments', type: 'string', hidden: true },
            { name: 'ttiCode', type: 'string', hidden: true },
            { name: 'EIN', type: 'string', hidden: true },
            { name: 'state', type: 'string', hidden: true },
            { name: 'city', type: 'string', hidden: true },
            { name: 'branch', type: 'string', hidden: true },
            { name: 'dueDay', type: 'string', hidden: true },
            { name: 'dueMonth', type: 'string' }
        ],

        ready: function () {
            _hideLoadingFullPage();
            $('#tblFilingSearch').on('rowdoubleclick', function (event) {
                // event.args.rowindex is a bound index.
                if (typeof event.args.group == "undefined") {
                    var row = event.args.rowindex;
                    var datarow = $("#tblFilingSearch").jqxGrid('getrowdata', row);
                    showCompleteModal(datarow);
                }
               

               
                
            });
        }
    });
}

function showCompleteModal(row) {
    //var row = $.jqxGridApi.getOneSelectedRow("#tblCalendarComplete", true);
    _showModal({
        title: "<h3>Filing Calendar - Log ID:" + row.logID + " - Entity: " + row.entityName + "</h3>",
        width: "98%",
        contentAjaxUrl: "/IncomeAndOther/ModalTemplate",
        contentAjaxParams: {
            logID: row.logID,
            entityID: row.entityID,
            entityName: row.entityName,
            function1: row.function,
            frequency: row.frequency,
            reviewer: row.reviewer,
            preparer: row.preparer,
            filingDueDate: _formatDate(new Date(row.filingDueDate), 'MM/dd/yyyy'),
            filingID: row.filingID,
            status: row.status,
            statusPrepared: row.statusPrepared,
            statusDesc: row.statusDesc,
            preparedDdate: _formatDate(new Date(row.statusPreparedDate), 'MMM-dd-yyyy'),
            reviewedDate: _formatDate(new Date(row.statusDate), 'MMM-dd-yyyy'),
            commentFlag: row.commentFlag,
            counts: row.counts,
            statusDate: row.statusDate,
            statusSOEID: row.statusSOEID,
            statusPreparedDate: row.statusPreparedDate,
            statusPreparedSOEID: row.statusPreparedSOEID,
            comments: row.comments,
            ttiCode: row.ttiCode,
            EIN: row.EIN,
            state: row.state,
            city: row.city,
            branch: row.branch,
            dueDay: row.dueDay,
            dueMonth: row.dueMonth
        },
        buttons: [{
            name: "Save Changes",
            class: "btn-info",
            onClick: function ($modal) {
                _callServer({
                    url: '/IncomeAndOther/CompletedEdit',
                    data: { 'pjson': JSON.stringify(row), 'comments': $("#txtNewComments").val() },
                    type: "post",
                    success: function (savingStatus) {
                        _showNotification("success", "Data was saved successfully.");
                        loadTableSearch();
                    }
                });
                //alert($modal.find("#txtlogID").val());
            }
        }],
        onReady: function ($modal) {
            //loadDropDownGlobalProcess(row.function);
            $modal.find("#txtfilingDueDate").prop('disabled', true);
            $modal.find("#txtaccounts").prop('disabled', true);
            $modal.find("#spinner").prop('disabled', true);



            $modal.find("#spinner").spinner({
                min: 0,
                max: 500,
                step: 1,
                start: 1000,
                numberFormat: "C"
            });
            // $modal.find("#spinner").val("20");
        }
    });
}

//#endregion






