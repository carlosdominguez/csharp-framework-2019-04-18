﻿var dueyearDDL = "SELECT  '--Select--' as ID, '9999' as VAL UNION SELECT DISTINCT CONVERT(varchar(10),year(filingDueDate)) as ID , CONVERT(varchar(10),year(filingDueDate)) as VAL FROM [dbo].[FilingCalendar] order by VAL DESC";


$(document).ready(function () {

    $('#fine-uploader-manual-trigger').fineUploader({
        template: 'qq-template-manual-trigger',
        request: {
            endpoint: '/IncomeAndOther/UploadYE'
        },
        thumbnails: {
            placeholders: {
                waitingPath: '/Content/Shared/plugins/fine-uploader/placeholders/waiting-generic.png',
                notAvailablePath: '/Content/Shared/plugins/fine-uploader/placeholders/not_available-generic.png'
            }
        },
        callbacks: {
            onAllComplete: function (succeeded, failed) {
                if (failed.length == 0) {
                    _showAlert({
                        type: "success",
                        title: "Message",
                        content: "The file was uploaded successfully"
                    });
                }
            },
            onError: function (id, name, errorReason, xhrOrXdr) {
                if (name) {
                    _showDetailAlert({
                        title: "Message",
                        shortMsg: "An error ocurred with the file '" + name + "'.",
                        longMsg: errorReason,
                        type: "Error",
                        viewLabel: "View Details"
                    });
                }
            },
            //onSubmit: function (id, name) {
            //    $('#fine-uploader-manual-trigger').fineUploader('setParams', { 'typeUpload': $('[name^="iCheck-TypeUpload"]:checked').val() });
            //},
            //onManualRetry: function (id, name) {
            //    $('#fine-uploader-manual-trigger').fineUploader('setParams', { 'typeUpload': $('[name^="iCheck-TypeUpload"]:checked').val() });
            //}
        },
        autoUpload: false
    });

    //On click upload files
    $('#trigger-upload').click(function () {
        //if ($('[name^="iCheck-TypeUpload"]:checked').length > 0) {
            $('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
        //} else {
        //    _showAlert({
        //        content: "Please select the type of the file <b>'GLMS Transcript'</b> or <b>'Udemy Training Catalog'</b>."
        //    });
        //}
    });

    loadDropDownlists(dueyearDDL, "ddlDueYearActivate");
});


function loadDropDownlists(selectString, droplistId) {
    _showLoadingFullPage({ msg: "Loading Data..." });
    _callServer({
        loadingMsg: "Loading Data ...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify(selectString) },
        type: "post",
        success: function (resultList) {
            $("#" + droplistId + "").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $("#" + droplistId + "").append($('<option>', { value: objFunction.VAL, text: objFunction.ID, selected: (objFunction.ID == " All") }));
            }
            $('#ddlDueYear').val(new Date().getFullYear());
            _hideLoadingFullPage();
        }
    });
};