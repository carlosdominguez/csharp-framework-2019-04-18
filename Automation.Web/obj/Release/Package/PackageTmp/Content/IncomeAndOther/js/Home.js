var _skillItemTypes = [
    {
        idType: 2,
        shortName: "glms",
        description: "GLMS Training",
        isTraining: true
    },
    {
        idType: 4,
        shortName: "udemy",
        description: "Udemy Training",
        isTraining: true
    },
    {
        shortName: "otherdev",
        description: "Other Development Actions"
    }
];
var _currentStates = [
    { id: 0, name: "-- Select --" },
    { id: -1, name: "Starter" },
    { id: 1, name: "Basic" },
    { id: 2, name: "Intermediate" },
    { id: 3, name: "Advanced" }
];

$(document).ready(function () {

    //Load Global Process
    loadDropDownGlobalProcess({
        fnSuccess: function (globalProcessList, idSelectedGlobalProcess) {

            //Load Process Role
            loadDropDownProcessRole({
                idSelGlobalProccess: idSelectedGlobalProcess,
                fnSuccess: function () {

                    //Load Job Level
                    loadDropDownJobLevel({
                        fnSuccess: function () {

                            //Load page scripts
                            loadPage();
                        }
                    });
                }
            });
        }
    });

    function loadPage() {
        //Load filter trainings information
        loadFilterTrainingSummary();

        //Init Datepicker in Discussion Date
        $("#txtDiscussionDate").datepicker();

        //On Click Save buttons
        $('#btnSaveTop,#btnSaveButtom').click(function () {
            saveAssessment($(this), "TopAndButtomSaveBtn");
        });

        //On Change Global Process
        $("#ddlGlobalProcess").change(function () {
            var idSelGlobalProcess = $("#ddlGlobalProcess").val();

            //Load Process Role
            loadDropDownProcessRole({
                loadingMsgType: "topBar",
                idSelGlobalProccess: idSelGlobalProcess,
                fnSuccess: function () {}
            });
        });

        //On Click Download Report
        $('.btnDownloadReport').click(function () {
            $el = $(this);
            validateExcelIsDownloaded();
            _showLoadingTopMessage();

            var soeid = $("#lblEmployeeSOEID").text();
            var idSelGlobalProcess = $("#ddlGlobalProcess").val();
            var idSelProcessRole = $("#ddlProcessRole").val();
            var idSelComptency = $("#ddlCompetency").val();
            var idSelJobLevel = $("#ddlJobLevel").val();

            var url = "FROU_AssessmentOfEmployeeAJAX.aspx";
            url = $.addToQueryString(url, "paction", "downloadReport");
            url = $.addToQueryString(url, "psoeidEmployee", soeid);
            url = $.addToQueryString(url, "pidGlobalProcess", idSelGlobalProcess);
            url = $.addToQueryString(url, "pidProcessRole", idSelProcessRole);
            url = $.addToQueryString(url, "pidCompetency", idSelComptency);
            url = $.addToQueryString(url, "pidJobLevel", idSelJobLevel);
            url = $.addToQueryString(url, "ptypeFile", $el.attr("typeFile"));
            url = $.addToQueryString(url, "preportName", $el.attr("reportName"));

            window.location.href = url;
        });

        //Load assessment
        $('#btnAssessment').click(function () {
            _execOnObjectShows("winvar_objTrainingSummary", function () {
                //Validate if the manager change the Process Role or Job Level
                if ((($("#ddlProcessRole").val() != _getViewVar("ViewModel").HFCurrentIDProcessRole) ||
                    ($("#ddlJobLevel").val() != _getViewVar("ViewModel").HFCurrentIDJobLevel)) && !isViewOtherAssessment()) {
                    _showAlert({
                        title: "Message",
                        type: "error",
                        showTo: "#employee-information",
                        content: "Due to your recent changes, please click on the Save Assessment button before Loading the Assessment.",
                        animateScrollTop: true
                    });
                } else if (window.objTrainingSummary.REQUIRED == 0 && isViewOnlyAssessment()) {
                    _showAlert({
                        title: "Message",
                        type: "error",
                        showTo: "#employee-information",
                        content: "Your assessment is empty, please contact your manager (" + $("#lblManagerName").text() + ") to review and set the Current State for each skill in your assessment.",
                        animateScrollTop: true
                    });
                } else {
                    loadAssessment();
                }
            }, 15);
        });

        //Load the tab with the Employee Information
        $("#tab-employee-information").click(function () {
            $("#contentOtherAssessment").hide();
            $("#contentAssessmentInformation").show();
            $(".assessment-area").show();

            //Hide if the assessment is empty
            if ($("#assessment-competencies").html() == "") {
                $(".assessment-area").hide();
            }
        });
        $("#tab-employee-information").click();

        //Load other assessments
        $("#tab-other-assessments").click(function () {
            loadOtherAssessments();
            $("#contentOtherAssessment").show();
            $("#contentAssessmentInformation").hide();
            $(".assessment-area").hide();
        });

        //Check if is View Only
        viewOnlyFirstLoad();

        //Check if is view other assessment
        viewOtherAssessment();
    }

    //Load History
    //loadAssessmentHistory($("#lblEmployeeSOEID").text(), "#content-history-assessment");
});

function loadDropDownGlobalProcess(params) {
    //Set default value
    params.loadingMsgType = typeof params.loadingMsgType != "undefined" ? params.loadingMsgType : "fullLoading";

    _callServer({
        loadingMsgType: params.loadingMsgType,
        loadingMsg: "Loading Global Process...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT '0' AS [ID], '-- Select --' AS [Name] UNION SELECT ID, Name FROM FROU_tblGlobalProcess WHERE Status = 1 ORDER BY Name ASC") },
        type: "post",
        success: function (resultList) {
            $("#ddlGlobalProcess").contents().remove();

            //Render Global Process Options
            for (var i = 0; i < resultList.length; i++) {
                var objGlobalProcess = resultList[i];
                var idSelectedGlobalProcess = _getViewVar("ViewModel").HFCurrentIDGlobalProcess;

                if (_getViewVar("ViewModel").HFViewOtherAssessment == "1") {
                    idSelectedGlobalProcess = _getViewVar("ViewModel").HFOtherIDGlobalProcess;
                }
                
                $("#ddlGlobalProcess").append($('<option>', { value: objGlobalProcess.ID, text: objGlobalProcess.Name, selected: (objGlobalProcess.ID == idSelectedGlobalProcess) }));
            }

            if (params.fnSuccess) {
                params.fnSuccess(resultList, idSelectedGlobalProcess);
            }
        }
    });
};

function loadDropDownProcessRole(params) {
    //Set default value
    params.loadingMsgType = typeof params.loadingMsgType != "undefined" ? params.loadingMsgType : "fullLoading";

    //$("#ddlProcessRole").showLoading();
    _callServer({
        loadingMsgType: params.loadingMsgType,
        loadingMsg: "Loading Process Roles...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT '0' AS [ID], '-- Select --' AS [Name] UNION SELECT PR.ID, PR.Name FROM dbo.FROU_tblProcessRole PR WHERE PR.IDGlobalProcess = " + params.idSelGlobalProccess + " ORDER BY 2 ASC") },
        type: "post",
        success: function (resultList) {
            $("#ddlProcessRole").contents().remove();
            //$("#ddlProcessRole").hideLoading();

            //Render Process Role Options
            for (var i = 0; i < resultList.length; i++) {
                var objProcessRole = resultList[i];
                var idSelectedProcessRole = _getViewVar("ViewModel").HFCurrentIDProcessRole;

                if (_getViewVar("ViewModel").HFViewOtherAssessment == "1") {
                    idSelectedProcessRole = _getViewVar("ViewModel").HFOtherIDProcessRole;
                }

                $("#ddlProcessRole").append($('<option>', { value: objProcessRole.ID, text: objProcessRole.Name, selected: (objProcessRole.ID == idSelectedProcessRole) }));
            }

            if (params.fnSuccess) {
                params.fnSuccess(resultList);
            }
        }
    });
};

function loadDropDownJobLevel(params) {
    //Set default value
    params.loadingMsgType = typeof params.loadingMsgType != "undefined" ? params.loadingMsgType : "fullLoading";

    //$("#ddlProcessRole").showLoading();
    _callServer({
        loadingMsgType: params.loadingMsgType,
        loadingMsg: "Loading Job Levels...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT '0' AS [ID], '-- Select --' AS [Name] UNION SELECT ID, Name FROM FROU_tblJobLevel WHERE ID <> 4 ORDER BY ID ASC") },
        type: "post",
        success: function (resultList) {
            $("#ddlJobLevel").contents().remove();

            //Render Job Level Options
            for (var i = 0; i < resultList.length; i++) {
                var objJobLevel = resultList[i];
                var idSelectedJobLevel = _getViewVar("ViewModel").HFCurrentIDJobLevel;

                if (_getViewVar("ViewModel").HFViewOtherAssessment == "1") {
                    idSelectedJobLevel = _getViewVar("ViewModel").HFOtherIDJobLevel;
                }

                $("#ddlJobLevel").append($('<option>', { value: objJobLevel.ID, text: objJobLevel.Name, selected: (objJobLevel.ID == idSelectedJobLevel) }));
            }

            if (params.fnSuccess) {
                params.fnSuccess(resultList);
            }
        }
    });
};

function loadAssessment() {
    var $btnAssessment = $(this);
    var soeidEmployee = _getViewVar("ViewModel").lblEmployeeSOEID;
    var idSelGlobalProcess = $("#ddlGlobalProcess").val();
    var idSelProcessRole = $("#ddlProcessRole").val();
    var idSelComptency = $("#ddlCompetency").val();
    var idSelJobLevel = $("#ddlJobLevel").val();
    var filterTrainings = $("#filterTrainingSelected").attr("value");

    $btnAssessment.attr("disabled", "disabled");

    //Load competency information
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Getting competencies information...",
        name: "[dbo].[FROU_spAssessmentGetInfoCompetency]",
        params: [
            { "Name": "@SOEIDEmployee", "Value": soeidEmployee },
            { "Name": "@IDProcessRole", "Value": idSelProcessRole },
            { "Name": "@IDJobLevel", "Value": idSelJobLevel },
            { "Name": "@IDCompetency", "Value": idSelComptency },
            { "Name": "@FilterTraining", "Value": filterTrainings }
        ],
        success: {
            fn: function (responseList) {
                //Set title
                $("#title-assessment").html("Assessment filter by: " + $("#filterTrainingSelected").text().replace(")", " trainings)"));
                $btnAssessment.removeAttr("disabled");

                $('.assessment-area').hide();

                //Show the assessment only when the Tab Employee Information is Active
                if ($("#tab-employee-information").parent().hasClass('active')) {
                    $('.assessment-area').show('slow');
                }

                //Clear old content
                $("#assessment-competencies").contents().remove();

                for (var i = 0; i < responseList.length; i++) {
                    var objCompetency = responseList[i];
                    var htmlCompetencyContainer =
                        '<h4>' + objCompetency.Name + '</h4>' +
                        '<div class="panel-group collapsed" id="accordion-competency-' + objCompetency.ID + '" role="tablist" aria-multiselectable="true">' +
                        '</div>';

                    $("#assessment-competencies").append(htmlCompetencyContainer);

                    _callProcedure({
                        loadingMsgType: "fullLoading",
                        loadingMsg: "Getting skills information of " + objCompetency.Name + "...",
                        name: "[dbo].[FROU_spAssessmentGetInfoSkill]",
                        params: [
                            { "Name": "@SOEID", "Value": soeidEmployee },
                            { "Name": "@IDProcessRole", "Value": idSelProcessRole },
                            { "Name": "@IDCompetency", "Value": objCompetency.ID },
                            { "Name": "@IDJobLevel", "Value": idSelJobLevel },
                            { "Name": "@FilterTraining", "Value": filterTrainings }
                        ],
                        success: {
                            params: {
                                isLastCompetency: (i == responseList.length - 1)
                            },
                            fn: function (skillList, params) {
                                console.log("Skills of competency", skillList);
                                var htmlAccordionPanels = "";

                                for (var j = 0; j < skillList.length; j++) {
                                    var objSkill = skillList[j];
                                    
                                    //AdvancedDefinition:"- Drives and owns engagement with key stakeholders.↵<br /><br />- Owns issues with stakeholders, evaluating/proposing solutions thoroughly (assumptions and risks) and  anticipates issues↵<br /><br />- Drives a culture of ownership, championing the resolution of issues even beyond own area of responsibility↵<br /><br />- Optimizes key financial metrics to improve performance over time<br /><br />"
                                    //BasicDefinition:"- Owns ones individual performance and related outcome, considering alternatives before proposing solutions↵<br /><br />- Considers customer impact↵<br /><br />- Regularly improves personal productivity↵<br /><br />- Identifies and escalates issues in a timely manner and resolve where relevant
                                    //CompletedTrainings:"0 of 0"
                                    //CompletionPercentage:"0.00"
                                    //DevelopmentNeeds:"None"
                                    //DueDatePast:"0"
                                    //IDCompetency:"0"
                                    //IDJobLevel:"2"
                                    //IDPFCurrentState:"2"
                                    //IDPFIdealState:"2"
                                    //IDProcessRole:"26"
                                    //IDSkill:"17"
                                    //IntermediateDefinition:"- Independent decision making within own process↵<br /><br />- Escalates and resolves problems in a timely, appropriate manner↵<br /><br />- Takes responsibility for process decisions made↵<br /><br />- Shows team how their performance contributes to the organization’s development<br /><br />"
                                    //JobLevel:"Senior Analyst"
                                    //PFCurrentState:"Intermediate"
                                    //PFIdealState:"Intermediate"
                                    //PendingTrainings:"0"
                                    //Skill:"Accountability / Ownership"

                                    htmlAccordionPanels +=
                                        '<div class="panel panel-default" idSkill="' + objSkill.IDSkill + '">' +
		                                    '<div class="panel-heading" role="tab" id="headingSkill-' + objSkill.IDSkill + '">' +
			                                    '<h5 class="panel-title">' +
                                                    '<a data-toggle="collapse" ' +
                                                        ' BasicDefinition="' + objSkill.BasicDefinition + '" ' +
                                                        ' IntermediateDefinition="' + objSkill.IntermediateDefinition + '" ' +
                                                        ' AdvancedDefinition="' + objSkill.AdvancedDefinition + '" ' +
                                                        ' CompletedTrainings="' + objSkill.CompletedTrainings + '" ' +
                                                        ' DevelopmentNeeds="' + objSkill.DevelopmentNeeds + '" ' +
                                                        ' IDPFIdealState="' + objSkill.IDPFIdealState + '" ' +
                                                        ' PFIdealState="' + objSkill.PFIdealState + '" ' +
                                                        ' IDSkill="' + objSkill.IDSkill + '" ' +
                                                        ' Skill="' + objSkill.Skill + '" ' +
                                                        ' IDProcessRole="' + objSkill.IDProcessRole + '" ' +
                                                        ' IDJobLevel="' + objSkill.IDJobLevel + '" ' +
                                                        ' IDPFCurrentState="' + objSkill.IDPFCurrentState + '" ' +
                                                        ' PFCurrentState="' + objSkill.PFCurrentState + '" ' +
                                                        ' IsLoaded="0" ' +
                                                        ' data-parent="#accordion-competency-' + objCompetency.ID + '" ' +
                                                        ' href="#collapseSkill-' + objSkill.IDSkill + '" ' +
                                                        ' aria-expanded="false" ' +
                                                        ' aria-controls="collapseSkill-' + objSkill.IDSkill + '" ' +
                                                        ' class="collapsed skill-accordion">' +
					                                        '<i class="fa fa-book"></i> ' + objSkill.Skill;

                                                        //Show warning that is mission select the current state
                                                        if (objSkill.IDPFCurrentState == "0") {
                                                            htmlAccordionPanels += '<span class="badge badge-warning pull-right" style="color:white;">Current State is missing </span>';
                                                        } else {
                                                            //Show due dates in red badge
                                                            htmlAccordionPanels += ((objSkill.DueDatePast != "0") ? '<span class="pull-right badge badge-danger" style="color:white; right: 45px; margin-top: -15px; position: absolute;">' + objSkill.DueDatePast + ' </span>' : '');

                                                            //Show Count of Trainings by Skill
                                                            htmlAccordionPanels += '<span class="badge badge-success pull-right">  ' + objSkill.TrainingCount + ' Training(s)</span>';
                                                        }
                                                            
                                    htmlAccordionPanels +=
				                                    '</a>' +
			                                    '</h5>' +
		                                    '</div>' +
		                                    '<div id="collapseSkill-' + objSkill.IDSkill + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSkill-' + objSkill.IDSkill + '" aria-expanded="false" style="height: 0px;">' +
			                                    '<div class="panel-body" id="contentSkill-' + objSkill.IDSkill + '"></div>' +
		                                    '</div>' +
	                                    '</div>';
                                }

                                var $accordionPanels = $(htmlAccordionPanels);

                                //Remove old accordions Panels
                                $("#accordion-competency-" + objSkill.IDCompetency).contents().remove();

                                //Add Panels of competency accordion
                                $("#accordion-competency-" + objSkill.IDCompetency).append($accordionPanels);

                                //On click accordion
                                $("#accordion-competency-" + objSkill.IDCompetency).find(".skill-accordion").click(function () {
                                    var $skillEle = $(this);

                                    if ($skillEle.attr("IsLoaded") == "0") {
                                        loadSkillInformation({
                                            BasicDefinition: $skillEle.attr("BasicDefinition"),
                                            IntermediateDefinition: $skillEle.attr("IntermediateDefinition"),
                                            AdvancedDefinition: $skillEle.attr("AdvancedDefinition"),
                                            CompletedTrainings: $skillEle.attr("CompletedTrainings"),
                                            DevelopmentNeeds: $skillEle.attr("DevelopmentNeeds"),
                                            IDPFIdealState: $skillEle.attr("IDPFIdealState"),
                                            PFIdealState: $skillEle.attr("PFIdealState"),
                                            IDSkill: $skillEle.attr("IDSkill"),
                                            Skill: $skillEle.attr("Skill"),
                                            IDProcessRole: $skillEle.attr("IDProcessRole"),
                                            IDJobLevel: $skillEle.attr("IDJobLevel"),
                                            IDPFCurrentState: $skillEle.attr("IDPFCurrentState"),
                                            PFCurrentState: $skillEle.attr("PFCurrentState")
                                        });
                                    }

                                    //Download scroll
                                    $('html, body').animate({
                                        scrollTop: $("a[IDSkill='" + $skillEle.attr("IDSkill") + "']").offset().top - 85
                                    }, 2000);

                                    $skillEle.attr("IsLoaded", "1");
                                });

                                //Hide left menu
                                if (params.isLastCompetency) {
                                    if ($(".page-sidebar").hasClass("expandit") || (!$(".page-sidebar").hasClass("expandit") && !$(".page-sidebar").hasClass("collapseit"))) {
                                        $(".sidebar_toggle").click();
                                    }
                                    //Download scroll
                                    $('html, body').animate({
                                        scrollTop: $("#assessment-competencies").offset().top - 120
                                    }, 2000);
                                }
                            }
                        }
                    });
                }
            }
        }
    });
}

function loadSkillInformation(objSkill) {
    //Create tabs
    var htmlTabsSkill =
        '<!-- Nav tabs --> ' +
        '<ul class="nav nav-tabs"> ' +
        '    <li class="active"> ' +
        '        <a href="#tab-info-skill-' + objSkill.IDSkill + '-content" id="tab-info-skill-' + objSkill.IDSkill + '" data-toggle="tab" aria-expanded="true"> ' +
        '            <i class="fa fa-info"></i> Skill information ' +
        '        </a> ' +
        '    </li> ' +
        '</ul> ';
    var $tabsSkill = $(htmlTabsSkill);

    var htmlTabsItemTypes = "";
    for (var i = 0; i < _skillItemTypes.length; i++) {
        var objItemType = _skillItemTypes[i];
        htmlTabsItemTypes +=
            '<li style="display:none;" > ' +
            '    <a href="#tab-' + objItemType.shortName + '-skill-' + objSkill.IDSkill + '-content" id="tab-' + objItemType.shortName + '-skill-' + objSkill.IDSkill + '" data-toggle="tab" aria-expanded="true"> ' +
            '        <i class="fa fa-graduation-cap"></i> ' + objItemType.description + ' <span class="badge badge-success"></span>' +
            '    </a> ' +
            '</li> ';
    }
    var $tabsItemTypes = $(htmlTabsItemTypes);
    $tabsSkill.append($tabsItemTypes);
    
    //Create contents
    var htmlContentsSkill =
        '<!-- Tab panes --> ' +
        '<div class="tab-content"> ' +
        '    <div class="tab-pane fade active in" id="tab-info-skill-' + objSkill.IDSkill + '-content"> ' +
        '       <div class="col-md-8 well"> ' +
        '           <h4><i class="fa fa-line-chart"></i> Basic</h4> ' +
        '           <div class="info text-muted"> ' +
        '               ' + objSkill.BasicDefinition + ' ' +
        '           </div> ' +
        '           <h4><i class="fa fa-line-chart"></i> Intermediate</h4> ' +
        '           <div class="info text-muted"> ' +
        '               ' + objSkill.IntermediateDefinition + ' ' +
        '           </div> ' +
        '           <h4><i class="fa fa-line-chart"></i> Advanced</h4> ' +
        '           <div class="info text-muted"> ' +
        '               ' + objSkill.AdvancedDefinition + ' ' +
        '           </div> ' +
        '       </div> ' +
        '       <div class="col-md-4 well transparent"> ' +
        '           <form id="form-skill-' + objSkill.IDSkill + '" class="form-horizontal"> ' +
        '               <div class="row"> ' +
        '                   <div class="col-xs-12"> ' +

        '                       <!-- Current State of Skill --> ' +
        '                       <div class="form-group"> ' +
        '                           <label for="ddlCurrentState" class="col-sm-4 control-label"> ' +
        '                               Current State ' +
        '                           </label> ' +
        '                           <div class="col-sm-8"> ' +
        '                               <select name="ddlCurrentState" idSkill="' + objSkill.IDSkill + '" class="ddlCurrentState form-control"> ';
                                        for (var i = 0; i < _currentStates.length; i++) {
                                            htmlContentsSkill += '<option value="' + _currentStates[i].id + '" ' + ((_currentStates[i].id == objSkill.IDPFCurrentState) ? 'selected="true"' : '') + '>' + _currentStates[i].name + '</option> ';
                                        }
    htmlContentsSkill += '              </select> ' +
        '                           </div> ' +
        '                       </div> ' +

        '                       <!-- Ideal State --> ' +
        '                       <div class="form-group"> ' +
        '                           <label for="lblIdealState" class="col-sm-4 control-label"> ' +
        '                               Ideal State ' +
        '                           </label> ' +
        '                           <div class="col-sm-8" style="padding-top: 4px;"> ' +
        '                               <span class="lblIdealState" style="font-size: Larger;">' + objSkill.PFIdealState + '</span> ' +
        '                           </div> ' +
        '                       </div> ' +

        '                       <!-- Development Needs --> ' +
        '                       <div class="form-group"> ' +
        '                           <label for="lblDevelopmentNeeds" class="col-sm-4 control-label"> ' +
        '                               Development Needs ' +
        '                           </label> ' +
        '                           <div class="col-sm-8" style="padding-top: 4px;"> ' +
        '                               <span class="lblDevelopmentNeeds" style="font-size: Larger;">' + objSkill.DevelopmentNeeds + '</span> ' +
        '                           </div> ' +
        '                       </div> ' +

        '                       <!-- Completed Trainings --> ' +
        '                       <div class="form-group"> ' +
        '                           <label for="lblCompleted" class="col-sm-4 control-label"> ' +
        '                               Completed Trainings ' +
        '                           </label> ' +
        '                           <div class="col-sm-8" style="padding-top: 4px;"> ' +
        '                               <span class="lblCompleted" style="font-size: Larger;">' + objSkill.CompletedTrainings + '</span> ' +
        '                           </div> ' +
        '                       </div> ' +
        '                   </div> ' +
        '               </div> ' +
        '           </form> ' +
        '       </div> ' +
        '       <div class="row" style="clear: both;"></div> ' +
        '    </div> ' +
        '</div> ';
    var $contentsSkill = $(htmlContentsSkill);

    var htmlContentsItemTypes = "";
    for (var i = 0; i < _skillItemTypes.length; i++) {
        var objItemType = _skillItemTypes[i];
        htmlContentsItemTypes +=
            '<div class="tab-pane fade in" id="tab-' + objItemType.shortName + '-skill-' + objSkill.IDSkill + '-content"> ' +
            '</div> ';
    }
    var $contentsItemTypes = $(htmlContentsItemTypes);
    $contentsSkill.append($contentsItemTypes);

    $("#contentSkill-" + objSkill.IDSkill).append($tabsSkill);
    $("#contentSkill-" + objSkill.IDSkill).append($contentsSkill);

    //Trainings still not loaded
    $("#contentSkill-" + objSkill.IDSkill).attr("IsLoaded", "0");

    //Add jquery Validate to Current State
    _validateForm({
        form: "#form-skill-" + objSkill.IDSkill,
        rules: {
            ddlCurrentState: {
                valueNotEquals: "0",
                required: true
            }
        },
        validateOnInit: true
    });

    //Load training only if the manager selected the current state
    if (objSkill.IDPFCurrentState != "0") {
        //Load GLMS Trainings
        loadGLMSTrainings(objSkill);

        //Load Udemy Trainings
        loadUdemyTrainings(objSkill);

        //Load Other Development Actions
        loadOtherDevelopmentActions(objSkill);

        //Trainings loaded
        $("#contentSkill-" + objSkill.IDSkill).attr("IsLoaded", "1");
    } 

    //On Change Current State
    $("#contentSkill-" + objSkill.IDSkill).find(".ddlCurrentState").change(function () {
        calculteRequiredTrainingsBasedCurrentState(objSkill, this);
    });
}

function loadGLMSTrainings(objSkill) {
    var shortNameGLMS = "glms";

    //'contentSkill-' + objSkill.IDSkill, 
    //Load GLMS Training information
    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Getting GLMS Trainings for '" + objSkill.Skill + "'...",
        name: "[dbo].[FROU_spAssessmentGetInfoOfSkillItem]",
        params: [
            { "Name": "@SOEID", "Value": _getViewVar("ViewModel").lblEmployeeSOEID },
            { "Name": "@IDProcessRole", "Value": objSkill.IDProcessRole },
            { "Name": "@IDJobLevel", "Value": objSkill.IDJobLevel },
            { "Name": "@IDSkill", "Value": objSkill.IDSkill },
            { "Name": "@IDPLCurrentState", "Value": objSkill.IDPFCurrentState },
            { "Name": "@IDSkillItemType", "Value": 2 },
            { "Name": "@FilterTrainings", "Value": $("#filterTrainingSelected").attr("value") }
        ],
        success: {
            params: {
                Skill: objSkill.Skill,
                IDSkill: objSkill.IDSkill,
                IDPFIdealState: objSkill.IDPFIdealState,
                PFIdealState: objSkill.PFIdealState,
                IDPFCurrentState: objSkill.IDPFCurrentState,
                PFCurrentState: objSkill.PFCurrentState
            },
            fn: function (GLMSTrainingsList, params) {
                
                var idGLMSContent = 'tab-' + shortNameGLMS + '-skill-' + params.IDSkill + '-content';
                var idGLMSTab = 'tab-' + shortNameGLMS + '-skill-' + params.IDSkill;

                console.log("GLMS Trainings", GLMSTrainingsList);
                //AdminFlagNotRequired:"0"
                //Audience:""
                //Code:"sklsft_mo_aacc_a07_dt_enus"
                //CompletedDate:""
                //FlagNotRequired:"-1"
                //IDActivity:"911564"
                //IDProficiencyLevel:"1"
                //IDSkillItem:"1905"
                //IDStatus:"1"
                //ProficiencyLevel:"Basic"
                //SkillItem:"Access 2010 Macros and VBA "
                //SkillItemType:"GLMS Training"
                //Status:"Pending"
                //TargetDate:""

                var $tableTrainings = $('<table class="table table-hover"></table>');
                //htmlTable += '<thead>';
                //htmlTable += '  <tr>';
                //htmlTable += '      <td>Level</td>';
                //htmlTable += '      <td>Training</td>';
                //htmlTable += '      <td>Completed Date</td>';
                //htmlTable += '      <td>Status</td>';
                //htmlTable += '      <td>Due Date</td>';
                //htmlTable += '      <td>Not Required</td>';
                //htmlTable += '  </tr>';
                //htmlTable += '</thead>';

                for (var i = 0; i < GLMSTrainingsList.length; i++) {
                    var objGLMSTraining = GLMSTrainingsList[i];
                    var $trRow = $("<tr></tr>");

                    //Check if the value is "-1" that means the SkillItem still not saved in the database, so get default value
                    //for "FlagNotRequired" from Training Plan admin section
                    var infoTrainingStatus = updateTrainingStatus({
                        FlagNotRequired: objGLMSTraining.FlagNotRequired,
                        AdminFlagNotRequired: objGLMSTraining.AdminFlagNotRequired,
                        IDProficiencyLevel: objGLMSTraining.IDProficiencyLevel,
                        Status: objGLMSTraining.Status,
                        IDPFCurrentState: params.IDPFCurrentState,
                        PFCurrentState: params.PFCurrentState,
                        IDPFIdealState: params.IDPFIdealState,
                        PFIdealState: params.PFIdealState,
                        TargetDate: objGLMSTraining.TargetDate
                    });
                    //Save original FlagNotRequired
                    $trRow.attr("FlagNotRequired", objGLMSTraining.FlagNotRequired);

                    objGLMSTraining.FlagNotRequired = infoTrainingStatus.flagNotRequiredCalculated;

                    //This attributtes are using in $("#" + idGLMSContent).find(".chkFlagNotRequired").on("ifChanged", function (event) {});
                    $trRow.attr("SkillItem", objGLMSTraining.SkillItem);
                    $trRow.attr("IDSkillItem", objGLMSTraining.IDSkillItem);
                    $trRow.attr("AdminFlagNotRequired", objGLMSTraining.AdminFlagNotRequired);
                    $trRow.attr("IDProficiencyLevel", objGLMSTraining.IDProficiencyLevel);
                    $trRow.attr("Status", objGLMSTraining.Status);
                    $trRow.attr("IDPFIdealState", params.IDPFIdealState);
                    $trRow.attr("PFIdealState", params.PFIdealState);
                    $trRow.attr("IDSkill", params.IDSkill);
                    $trRow.attr("htmlBadgeStatus", infoTrainingStatus.htmlBadgeStatus);

                    if (objGLMSTraining.FlagNotRequired == "1") {
                        $trRow.addClass("notRequired");
                    }

                    //Column Proficiency Level
                    var $tdProficiencyLevel = $("<td></td>");
                    $tdProficiencyLevel.html(objGLMSTraining.ProficiencyLevel);
                    $trRow.append($tdProficiencyLevel);

                    //Column Skill Item (Link to GLML)
                    var $tdSkillItem = $("<td></td>");
                    var $aLinkGLMS = $("<a></a>", {
                        target: "_blank",
                        href: 'https://training.citigroup.net/SumTotal/app/management/LMS_ActDetails.aspx?UserMode=0&ActivityId=' + objGLMSTraining.IDActivity
                    });
                    $aLinkGLMS.html(objGLMSTraining.SkillItem);
                    $tdSkillItem.append($aLinkGLMS);
                    $trRow.append($tdSkillItem);

                    //Column Completed Date
                    var $tdCompletedDate = $("<td></td>", { title: "Completed Date" });
                    $tdCompletedDate.html(objGLMSTraining.CompletedDate);
                    $trRow.append($tdCompletedDate);

                    //Column Status (Badge)
                    var $tdStatus = $("<td></td>", { class: "tdStatus" });
                    $tdStatus.html(infoTrainingStatus.htmlBadgeStatus);
                    $trRow.append($tdStatus);

                    //Column Due Date
                    var $tdDueDate = $("<td></td>", {
                        style: "width:135px;",
                        title: "Define a Due Date for this training"
                    });
                    var $inputDueDate = $("<input>", {
                        name: "txtTargetDate",
                        type: "text",
                        value: objGLMSTraining.TargetDate,
                        class: "form-control hasDatepicker txtTargetDate"
                    });
                    $tdDueDate.append($inputDueDate);
                    $trRow.append($tdDueDate);

                    //Column Not Required
                    var $tdNotRequired = $("<td></td>", {
                        style: "width:20px;",
                        title: "Check this training as Not Required"
                    });
                    var $inputChkNotRequired = $("<input>", {
                        type: "checkbox",
                        class: "skin-square-red chkFlagNotRequired"
                    });
                    if (objGLMSTraining.FlagNotRequired == "1") {
                        $inputChkNotRequired.attr("checked", "checked");
                    }
                    $tdNotRequired.append($inputChkNotRequired);
                    $trRow.append($tdNotRequired);

                    //Add Training Row to the table
                    $tableTrainings.append($trRow);
                }

                //Add table of trainings
                $("#" + idGLMSContent).append($tableTrainings);

                //Init Not Required Checkbox
                _GLOBAL_SETTINGS.iCheck();

                //Init Datepicker for Due Date
                $("#" + idGLMSContent).find(".txtTargetDate").datepicker();
                
                //On Change Due Date
                $("#" + idGLMSContent).find(".txtTargetDate").on("change", function (event) {
                    var $inputTargetDate = $(this);
                    var $trTraining = $inputTargetDate.parent().parent();

                    updateTrainingStatus({
                        IDSkillItem: $trTraining.attr("IDSkillItem")
                    }, 'ChkNotRequired');
                });

                //On click not required
                $("#" + idGLMSContent).find(".chkFlagNotRequired").on("ifChanged", function (event) {
                    var $chkNotRequired = $(this);
                    var $trTraining = $chkNotRequired.parent().parent().parent();

                    updateTrainingStatus({
                        IDSkillItem: $trTraining.attr("IDSkillItem")
                    }, 'ChkNotRequired');
                });

                //Add count of trainings
                updateCountersByStatus(params.IDSkill, shortNameGLMS);

                //Show tab GLMS Trainings
                $("#" + idGLMSTab).parent().show();
            }
        }
    });
};

function loadUdemyTrainings(objSkill) {
    var shortNameGLMS = "udemy";
    var sqlUdemy = 
        "SELECT U.* " +
        "FROM FROU_tblUdemyXSkill UXS " +
        "       INNER JOIN [dbo].[FROU_tblUdemyCourses] U ON UXS.IDUdemyTraining = U.ID " +
        "WHERE IDSkill = " + objSkill.IDSkill

    //Load Udemy Training information
    _callServer({
        loadingMsgType: "topBar",
        loadingMsg: "Getting Udemy Trainings for '" + objSkill.Skill + "'...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify(sqlUdemy) },
        type: "post",
        success: {
            params: {
                Skill: objSkill.Skill,
                IDSkill: objSkill.IDSkill,
                IDPFIdealState: objSkill.IDPFIdealState,
                PFIdealState: objSkill.PFIdealState,
                IDPFCurrentState: objSkill.IDPFCurrentState,
                PFCurrentState: objSkill.PFCurrentState
            },
            fn: function (GLMSTrainingsList, params) {

                var idGLMSContent = 'tab-' + shortNameGLMS + '-skill-' + params.IDSkill + '-content';
                var idGLMSTab = 'tab-' + shortNameGLMS + '-skill-' + params.IDSkill;

                console.log("Udemy Trainings", GLMSTrainingsList);
                //AdminFlagNotRequired:"0"
                //Audience:""
                //Code:"sklsft_mo_aacc_a07_dt_enus"
                //CompletedDate:""
                //FlagNotRequired:"-1"
                //IDActivity:"911564"
                //IDProficiencyLevel:"1"
                //IDSkillItem:"1905"
                //IDStatus:"1"
                //ProficiencyLevel:"Basic"
                //SkillItem:"Access 2010 Macros and VBA "
                //SkillItemType:"GLMS Training"
                //Status:"Pending"
                //TargetDate:""

                var $tableTrainings = $('<table class="table table-hover"></table>');
                //htmlTable += '<thead>';
                //htmlTable += '  <tr>';
                //htmlTable += '      <td>Level</td>';
                //htmlTable += '      <td>Training</td>';
                //htmlTable += '      <td>Completed Date</td>';
                //htmlTable += '      <td>Status</td>';
                //htmlTable += '      <td>Due Date</td>';
                //htmlTable += '      <td>Not Required</td>';
                //htmlTable += '  </tr>';
                //htmlTable += '</thead>';

                for (var i = 0; i < GLMSTrainingsList.length; i++) {
                    var objGLMSTraining = GLMSTrainingsList[i];
                    var $trRow = $("<tr></tr>");

                    objGLMSTraining.ProficiencyLevel = (objGLMSTraining.ProficiencyLevel) ? objGLMSTraining.ProficiencyLevel : "No Level";

                    //Check if the value is "-1" that means the SkillItem still not saved in the database, so get default value
                    //for "FlagNotRequired" from Training Plan admin section
                    var infoTrainingStatus = updateTrainingStatus({
                        FlagNotRequired: objGLMSTraining.FlagNotRequired,
                        AdminFlagNotRequired: objGLMSTraining.AdminFlagNotRequired,
                        IDProficiencyLevel: objGLMSTraining.IDProficiencyLevel,
                        Status: objGLMSTraining.Status,
                        IDPFCurrentState: params.IDPFCurrentState,
                        PFCurrentState: params.PFCurrentState,
                        IDPFIdealState: params.IDPFIdealState,
                        PFIdealState: params.PFIdealState,
                        TargetDate: objGLMSTraining.TargetDate
                    });
                    //Save original FlagNotRequired
                    $trRow.attr("FlagNotRequired", objGLMSTraining.FlagNotRequired);

                    objGLMSTraining.FlagNotRequired = infoTrainingStatus.flagNotRequiredCalculated;

                    //This attributtes are using in $("#" + idGLMSContent).find(".chkFlagNotRequired").on("ifChanged", function (event) {});
                    $trRow.attr("SkillItem", objGLMSTraining.CourseTitle);
                    $trRow.attr("IDSkillItem", objGLMSTraining.CourseCode);
                    $trRow.attr("AdminFlagNotRequired", objGLMSTraining.AdminFlagNotRequired);
                    $trRow.attr("IDProficiencyLevel", objGLMSTraining.IDProficiencyLevel);
                    $trRow.attr("Status", objGLMSTraining.Status);
                    $trRow.attr("IDPFIdealState", params.IDPFIdealState);
                    $trRow.attr("PFIdealState", params.PFIdealState);
                    $trRow.attr("IDSkill", params.IDSkill);
                    $trRow.attr("htmlBadgeStatus", infoTrainingStatus.htmlBadgeStatus);

                    if (objGLMSTraining.FlagNotRequired == "1") {
                        $trRow.addClass("notRequired");
                    }

                    //Column Proficiency Level
                    var $tdProficiencyLevel = $("<td></td>");
                    $tdProficiencyLevel.html(objGLMSTraining.ProficiencyLevel);
                    $trRow.append($tdProficiencyLevel);

                    //Column Skill Item (Link to GLML)
                    var $tdSkillItem = $("<td></td>");
                    var $aLinkGLMS = $("<a></a>", {
                        target: "_blank",
                        href: objGLMSTraining.URL
                    });
                    $aLinkGLMS.html(objGLMSTraining.CourseTitle);
                    $tdSkillItem.append($aLinkGLMS);
                    $trRow.append($tdSkillItem);

                    //Column Completed Date
                    var $tdCompletedDate = $("<td></td>", { title: "Completed Date" });
                    $tdCompletedDate.html(objGLMSTraining.CompletedDate);
                    $trRow.append($tdCompletedDate);

                    //Column Status (Badge)
                    var $tdStatus = $("<td></td>", { class: "tdStatus" });
                    $tdStatus.html(infoTrainingStatus.htmlBadgeStatus);
                    $trRow.append($tdStatus);

                    //Column Due Date
                    var $tdDueDate = $("<td></td>", {
                        style: "width:135px;",
                        title: "Define a Due Date for this training"
                    });
                    var $inputDueDate = $("<input>", {
                        name: "txtTargetDate",
                        type: "text",
                        value: objGLMSTraining.TargetDate,
                        class: "form-control hasDatepicker txtTargetDate"
                    });
                    $tdDueDate.append($inputDueDate);
                    $trRow.append($tdDueDate);

                    //Column Not Required
                    var $tdNotRequired = $("<td></td>", {
                        style: "width:20px;",
                        title: "Check this training as Not Required"
                    });
                    var $inputChkNotRequired = $("<input>", {
                        type: "checkbox",
                        class: "skin-square-red chkFlagNotRequired"
                    });
                    if (objGLMSTraining.FlagNotRequired == "1") {
                        $inputChkNotRequired.attr("checked", "checked");
                    }
                    $tdNotRequired.append($inputChkNotRequired);
                    $trRow.append($tdNotRequired);

                    //Add Training Row to the table
                    $tableTrainings.append($trRow);
                }

                //Add table of trainings
                $("#" + idGLMSContent).append($tableTrainings);

                //Init Not Required Checkbox
                _GLOBAL_SETTINGS.iCheck();

                //Init Datepicker for Due Date
                $("#" + idGLMSContent).find(".txtTargetDate").datepicker();

                //On Change Due Date
                $("#" + idGLMSContent).find(".txtTargetDate").on("change", function (event) {
                    var $inputTargetDate = $(this);
                    var $trTraining = $inputTargetDate.parent().parent();

                    updateTrainingStatus({
                        IDSkillItem: $trTraining.attr("IDSkillItem")
                    }, 'ChkNotRequired');
                });

                //On click not required
                $("#" + idGLMSContent).find(".chkFlagNotRequired").on("ifChanged", function (event) {
                    var $chkNotRequired = $(this);
                    var $trTraining = $chkNotRequired.parent().parent().parent();

                    updateTrainingStatus({
                        IDSkillItem: $trTraining.attr("IDSkillItem")
                    }, 'ChkNotRequired');
                });

                //Add count of trainings
                updateCountersByStatus(params.IDSkill, shortNameGLMS);

                //Show tab GLMS Trainings
                $("#" + idGLMSTab).parent().show();
            }
        }
    });
};

function loadOtherDevelopmentActions(objSkill) {
    //Show tab Other Development Actions
    $("#tab-otherdev-skill-" + objSkill.IDSkill).parent().show();
};

function updateTrainingStatus(pobjTraining, triggeredBy) {
    //triggeredBy = 'ChkNotRequired' | 'CurrentState'
    //pobjTraining.FlagNotRequired = "";
    //pobjTraining.AdminFlagNotRequired = "";
    //pobjTraining.IDProficiencyLevel = "";
    //pobjTraining.Status = "";
    //pobjTraining.IDPFCurrentState = "";
    //pobjTraining.PFCurrentState = "";
    //pobjTraining.IDPFIdealState = "";
    //pobjTraining.PFIdealState = "";
    //pobjTraining.TargetDate = "";
    //pobjTraining.IDSkill = "";

    var htmlBadgeStatus = "";
    var status = "";
    var $chkNotRequired = null;
    var htmlOldBadgeStatus = "";
    var $trSkillItem = $("tr[IDSkillItem='" + pobjTraining.IDSkillItem + "']");

    //If row of skillItem exists get values from it
    if ($trSkillItem.length > 0) {
        $chkNotRequired = $trSkillItem.find(".chkFlagNotRequired");
        var $contentSkill = $("#contentSkill-" + $trSkillItem.attr("IDSkill"));
        var $ddlCurrentState = $contentSkill.find(".ddlCurrentState");
        var idPFCurrentState = $ddlCurrentState.find("option:selected").val();
        var PFCurrentState = $ddlCurrentState.find("option:selected").text();
        htmlOldBadgeStatus = $trSkillItem.attr("htmlBadgeStatus");

        pobjTraining.FlagNotRequired = $chkNotRequired.is(":checked") ? "1" : "0";
        pobjTraining.AdminFlagNotRequired = $trSkillItem.attr("AdminFlagNotRequired");
        pobjTraining.IDProficiencyLevel = $trSkillItem.attr("IDProficiencyLevel");
        pobjTraining.Status = $trSkillItem.attr("Status");
        pobjTraining.IDPFIdealState = $trSkillItem.attr("IDPFIdealState");
        pobjTraining.PFIdealState = $trSkillItem.attr("PFIdealState");
        pobjTraining.IDPFCurrentState = idPFCurrentState;
        pobjTraining.PFCurrentState = PFCurrentState;
        pobjTraining.TargetDate = $trSkillItem.find(".txtTargetDate").val();
        pobjTraining.IDSkill = $trSkillItem.attr("IDSkill");

        if ($chkNotRequired.is(":checked")) {
            $trSkillItem.addClass("notRequired");
        } else {
            $trSkillItem.removeClass("notRequired");
        }
    }

    var flagNotRequired = pobjTraining.FlagNotRequired;
    var adminFlagNotRequired = pobjTraining.AdminFlagNotRequired;

    if ((pobjTraining.FlagNotRequired == "-1" || $trSkillItem.attr("FlagNotRequired") == "-1") && triggeredBy != 'ChkNotRequired') {
        if (parseInt(pobjTraining.IDProficiencyLevel) > parseInt(pobjTraining.IDPFCurrentState) && parseInt(pobjTraining.IDProficiencyLevel) <= parseInt(pobjTraining.IDPFIdealState)) {
            if (pobjTraining.AdminFlagNotRequired == "-1") {
                if (pobjTraining.Status == "Completed") {
                    htmlBadgeStatus = '<span class="badge badge-success">Completed</span>';
                    status = 'Completed';
                } else {
                    htmlBadgeStatus = '<span class="badge badge-warning">Recomended employee is ' + pobjTraining.PFCurrentState + '</span>';
                    status = "Recomended employee is " + pobjTraining.PFCurrentState;
                }
                //Check as Required
                if ($chkNotRequired) {
                    if ($chkNotRequired.is(":checked")) {
                        $chkNotRequired.iCheck('uncheck');
                    }
                }
                pobjTraining.FlagNotRequired = "0";
            } else {
                if (pobjTraining.AdminFlagNotRequired == "1") {
                    htmlBadgeStatus = '<span class="badge badge-secondary">Not Required (By CSS Admins)</span>';
                    status = "Not Required (By CSS Admins)";
                    //Check as Not Required
                    if ($chkNotRequired) {
                        if (!$chkNotRequired.is(":checked")) {
                            $chkNotRequired.iCheck('check');
                        }
                    }
                } else {
                    if (pobjTraining.Status == "Completed") {
                        htmlBadgeStatus = '<span class="badge badge-success">Completed</span>';
                        status = 'Completed';
                    } else {
                        htmlBadgeStatus = '<span class="badge badge-info">Recomended (By CSS Admins)</span>';
                        status = "Recomended (By CSS Admins)";
                    }
                    //Check as Required
                    if ($chkNotRequired) {
                        if ($chkNotRequired.is(":checked")) {
                            $chkNotRequired.iCheck('uncheck');
                        }
                    }
                }
                pobjTraining.FlagNotRequired = pobjTraining.AdminFlagNotRequired;
            }
            
        } else {
            if (parseInt(pobjTraining.IDProficiencyLevel) <= parseInt(pobjTraining.IDPFCurrentState)) {
                htmlBadgeStatus = '<span class="badge badge-secondary">Not Required (Already ' + pobjTraining.PFCurrentState + ')</span>';
                status = 'Not Required (Already ' + pobjTraining.PFCurrentState + ')';
            } else {
                if (parseInt(pobjTraining.IDProficiencyLevel) > parseInt(pobjTraining.IDPFIdealState)) {
                    htmlBadgeStatus = '<span class="badge badge-secondary">Not Required (Ideal State ' + pobjTraining.PFIdealState + ')</span>';
                    status = 'Not Required (Ideal State ' + pobjTraining.PFIdealState + ')';
                } else {
                    htmlBadgeStatus = '<span class="badge badge-secondary">Not Required (By Current State)</span>';
                    status = 'Not Required (By Current State)';
                }
            }
            pobjTraining.FlagNotRequired = "1";

            //Check as Not Required
            if ($chkNotRequired) {
                if (!$chkNotRequired.is(":checked")) {
                    $chkNotRequired.iCheck('check');
                }
            }
        }
    } else {

        if (pobjTraining.FlagNotRequired == "1") {
            htmlBadgeStatus = '<span class="badge badge-secondary">Not Required (By Manager)</span>';
            status = 'Not Required (By Manager)';
        } else {
            if (pobjTraining.Status == "Completed") {
                htmlBadgeStatus = '<span class="badge badge-success">Completed</span>';
                status = 'Completed';
            } else {
                //Validate if the training is due date
                var nowDt = _formatDate(new Date(), "MMM dd, yyyy");
                if ((new Date(nowDt).getTime()) > (new Date(pobjTraining.TargetDate).getTime())) {
                    htmlBadgeStatus = '<span class="badge badge-danger">Overdue</span>';
                    status = 'Overdue';
                } else {
                    if (parseInt(pobjTraining.IDProficiencyLevel) <= parseInt(pobjTraining.IDPFCurrentState)) {
                        htmlBadgeStatus = '<span class="badge badge-warning">Optional already ' + pobjTraining.PFCurrentState + '</span>';
                        status = 'Optional already ' + pobjTraining.PFCurrentState;
                    } else {
                        if (parseInt(pobjTraining.IDProficiencyLevel) > parseInt(pobjTraining.IDPFIdealState)) {
                            htmlBadgeStatus = '<span class="badge badge-warning">Optional Ideal State ' + pobjTraining.PFIdealState + '</span>';
                            status = 'Optional Ideal State is ' + pobjTraining.PFIdealState;
                        } else {
                            htmlBadgeStatus = '<span class="badge badge-warning">Pending</span>';
                            status = 'Pending';
                        }
                    }
                }
            }
        }
    }

    //if (pobjTraining.Status == "Completed") {
    //    htmlBadgeStatus = '<span class="badge badge-success">Completed</span>';
    //    status = 'Completed';

    //} else {


    //}

    //if (parseInt(pobjTraining.IDProficiencyLevel) > parseInt(pobjTraining.IDPFCurrentState) && parseInt(pobjTraining.IDProficiencyLevel) <= parseInt(pobjTraining.IDPFIdealState)) {
        
    //} else {

    //    if (parseInt(pobjTraining.IDProficiencyLevel) > parseInt(pobjTraining.IDPFCurrentState) && parseInt(pobjTraining.IDProficiencyLevel) <= parseInt(pobjTraining.IDPFIdealState)) {

    //    } else {
    //        if (parseInt(pobjTraining.IDProficiencyLevel) <= parseInt(pobjTraining.IDPFCurrentState)) {
    //            htmlBadgeStatus = '<span class="badge badge-info">Optional already ' + pobjTraining.PFCurrentState + '</span>';
    //            status = 'Optional already ' + pobjTraining.PFCurrentState;
    //        } else {
    //            if (parseInt(pobjTraining.IDProficiencyLevel) > parseInt(pobjTraining.IDPFIdealState)) {
    //                htmlBadgeStatus = '<span class="badge badge-info">Optional Ideal State ' + pobjTraining.PFIdealState + '</span>';
    //                status = 'Optional Ideal State ' + pobjTraining.PFIdealState;
    //            } else {
    //                htmlBadgeStatus = '<span class="badge badge-info">Optional</span>';
    //                status = 'Optional';
    //            }
    //        }
    //        //htmlBadgeStatus = '<span class="badge badge-secondary">Not Required</span>';
    //        //status = 'Not Required';
    //        pobjTraining.FlagNotRequired = "1";
    //    }
    //}


    //if (pobjTraining.FlagNotRequired == "-1") {

    //    if (parseInt(pobjTraining.IDProficiencyLevel) > parseInt(pobjTraining.IDPFCurrentState) && parseInt(pobjTraining.IDProficiencyLevel) <= parseInt(pobjTraining.IDPFIdealState)) {
    //        if (pobjTraining.Status == "Completed") {
    //            htmlBadgeStatus = '<span class="badge badge-success">Completed</span>';
    //            status = 'Completed';
    //        } else {
    //            if (pobjTraining.AdminFlagNotRequired == "-1") {
    //                htmlBadgeStatus = '<span class="badge badge-warning">Recomended by Current State</span>';
    //                status = "Recomended by Current State";
    //                pobjTraining.FlagNotRequired = "1";
    //            }
    //            else {
    //                if (pobjTraining.AdminFlagNotRequired == "1") {
    //                    htmlBadgeStatus = '<span class="badge badge-secondary">Not Recomended by CSS Admins</span>';
    //                    status = "Not Recomended by CSS Admins";
    //                } else {
    //                    htmlBadgeStatus = '<span class="badge badge-info">Recomended by CSS Admins</span>';
    //                    status = "Recomended by CSS Admins";
    //                }
    //                pobjTraining.FlagNotRequired = pobjTraining.AdminFlagNotRequired;
    //            }
    //        }
    //    } else {
    //        if (parseInt(pobjTraining.IDProficiencyLevel) <= parseInt(pobjTraining.IDPFCurrentState)) {
    //            htmlBadgeStatus = '<span class="badge badge-info">Optional already ' + pobjTraining.PFCurrentState + '</span>';
    //            status = 'Optional already ' + pobjTraining.PFCurrentState;
    //        } else {
    //            if (parseInt(pobjTraining.IDProficiencyLevel) > parseInt(pobjTraining.IDPFIdealState)) {
    //                htmlBadgeStatus = '<span class="badge badge-info">Optional Ideal State ' + pobjTraining.PFIdealState + '</span>';
    //                status = 'Optional Ideal State ' + pobjTraining.PFIdealState;
    //            } else {
    //                htmlBadgeStatus = '<span class="badge badge-info">Optional</span>';
    //                status = 'Optional';
    //            }
    //        }
    //        //htmlBadgeStatus = '<span class="badge badge-secondary">Not Required</span>';
    //        //status = 'Not Required';
    //        pobjTraining.FlagNotRequired = "1";
    //    }
    //} else {
        
    //}

    //Update Status of the training
    if ($trSkillItem.length > 0) {
        $trSkillItem.find(".tdStatus").html(htmlBadgeStatus);

        //if ($trSkillItem.attr("htmlOldBadgeStatus") && status != 'Overdue' && (status.indexOf('Not Required') == -1 )) {
        //    $trSkillItem.find(".tdStatus").html($trSkillItem.attr("htmlOldBadgeStatus"));
        //    $trSkillItem.attr("htmlOldBadgeStatus", "");
        //} else {
        //    $trSkillItem.find(".tdStatus").html(htmlBadgeStatus);
        //    $trSkillItem.attr("htmlOldBadgeStatus", htmlOldBadgeStatus);
        //}

        //Update Trainings counters
        updateCountersByStatus(pobjTraining.IDSkill);
    }

    return {
        flagNotRequired: flagNotRequired,
        adminFlagNotRequired: adminFlagNotRequired,
        flagNotRequiredCalculated: pobjTraining.FlagNotRequired,
        htmlBadgeStatus: htmlBadgeStatus,
        status: status
    };
};

function updateCountersByStatus(pidSkill, typeTraining) {

    //Not Saved (default): Recomended by CSS Admins | Optional | Optional Ideal State {} | Optional already {} | 
    updateCounter('badge-info');

    //Saved: Pending | Optional Ideal State {} | Optional already {} | 
    //Not Saved (default): Recomended by Current State
    updateCounter('badge-warning');

    //Saved: Overdue |
    updateCounter('badge-danger');

    //Saved: Completed |
    updateCounter('badge-success');

    //Saved: Not Required |
    //Not Saved (default): Not Recomended by CSS Admins |
    updateCounter('badge-secondary');
    
    function updateCounter(ptypeCounter) {
        //ids contents: 
        //  - tab-info-skill-2-content, 
        //  - tab-glms-skill-2-content, 
        //  - tab-udemy-skill-2-content, 
        //  - tab-otherdev-skill-2-content
        var $contentTrainings = $("#tab-" + typeTraining + "-skill-" + pidSkill + "-content");

        //ids tabs: 
        //  - tab-info-skill-2, 
        //  - tab-glms-skill-2, 
        //  - tab-udemy-skill-2, 
        //  - tab-otherdev-skill-2
        var $tabTrainings = $("#tab-" + typeTraining + "-skill-" + pidSkill);

        //Get count of status
        var count = $contentTrainings.find('.' + ptypeCounter).length;

        //Update tab counter
        if (count != 0) {
            if ($tabTrainings.find('.' + ptypeCounter).length > 0) {
                $tabTrainings.find('.' + ptypeCounter).html(count);
            } else {
                $tabTrainings.append('<span class="badge ' + ptypeCounter + '">' + count + '</span>');
            }
        } else {
            if ($tabTrainings.find('.' + ptypeCounter).length > 0) {
                $tabTrainings.find('.' + ptypeCounter).remove();
            }
        }
    }
}

function loadAssessmentHistory(psoeidEmployee, pidSelectorShowTo) {
    var htmlHistoryLog = "";
    if (psoeidEmployee) {
        //Save change to get the ID
        _callProcedure({
            loadingMsg: "Loading assessment history...",
            name: "[dbo].[FROU_spAssessmentHistoryChangesList]",
            params: [
                { "Name": "@SOEID", "Value": psoeidEmployee }
            ],
            success: {
                fn: function (responseListChanges) {
                    if (responseListChanges.length > 0) {
                        var objTempChangeComment;

                        //Load comments
                        for (var i = 0; i < responseListChanges.length; i++) {
                            objTempChangeComment = responseListChanges[i];

                            //htmlHistoryLog += "<b>" + _formatDate(new Date(objTempChangeComment.CREATED_DATE), "yyyy-MMM-dd hh:mm tt") + ": </b> Change by " + objTempChangeComment.NAME + " [" + objTempChangeComment.SOEID + "]:"
                            htmlHistoryLog += "<b>" + _formatDate(new Date(objTempChangeComment.CREATED_DATE), "yyyy-MMM-dd") + ": </b> Modified by " + objTempChangeComment.CREATED_NAME + " [" + objTempChangeComment.CREATED_SOEID + "]:"
                            htmlHistoryLog += "<table style='width: 100%;' class='table table-bordered table-hover table-striped table-condensed wrap'>";
                            htmlHistoryLog += "     <tbody>";
                            htmlHistoryLog += "         <tr class='GridviewScrollHeader'>";
                            htmlHistoryLog += "             <th scope='col' style='width: 50%;'>Field Name</th>";
                            htmlHistoryLog += "             <th scope='col' style='width: 20%;'>Old Value</th>";
                            htmlHistoryLog += "             <th scope='col' style='width: 10%;'></th>";
                            htmlHistoryLog += "             <th scope='col' style='width: 20%;'>New Value</th>";
                            htmlHistoryLog += "         </tr>";

                            //Load field changes by each comment
                            _callProcedure({
                                loadingMsg: "Loading history fields old values and new values...",
                                name: "[dbo].[FROU_spAssessmentHistoryFieldList]",
                                params: [
                                    { "Name": "@CHANGE_ID", "Value": objTempChangeComment.CHANGE_ID }
                                ],
                                success: {
                                    fn: function (responseListFieldChanges) {
                                        if (responseListFieldChanges.length > 0) {
                                            var objTempFieldChage;

                                            //Load field changes by each comment
                                            for (var j = 0; j < responseListFieldChanges.length; j++) {
                                                objTempFieldChage = responseListFieldChanges[j];

                                                htmlHistoryLog += "<tr class='GridviewScrollItem'>";
                                                htmlHistoryLog += "     <td>" + objTempFieldChage.FIELD_NAME + "</td>";
                                                htmlHistoryLog += "     <td style='color: red;'>" + objTempFieldChage.OLD_VALUE + "</td>";
                                                htmlHistoryLog += "     <td style='text-align: center;'> => </td>";
                                                htmlHistoryLog += "     <td style='color: green';>" + objTempFieldChage.NEW_VALUE + "</td>";
                                                htmlHistoryLog += "</tr>";
                                            }
                                        }
                                    }
                                }
                            });

                            htmlHistoryLog += "     </tbody>";
                            htmlHistoryLog += "</table>";
                            //htmlHistoryLog += "<pre style='margin-top: -21px;font-size: 15px;'><b>Comment:</b> " + objTempChangeComment.COMMENT + "</pre><br>";
                        }
                    }
                }
            }
        });

        if (!htmlHistoryLog) {
            htmlHistoryLog = "<pre>No changes were detected.</pre>";
        }

        $(pidSelectorShowTo).html(htmlHistoryLog);
    }
}

function loadFilterTrainingSummary() {
    var soeidEmployee = $("#lblEmployeeSOEID").text();
    var idSelProcessRole = $("#ddlProcessRole").val();
    var idSelJobLevel = $("#ddlJobLevel").val();
    var idSelComptency = $("#ddlCompetency").val();

    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Filters Training information...",
        name: "[dbo].[FROU_spGetFilterTrainingSummary]",
        params: [
            { Name: "@SOEID", Value: soeidEmployee },
            { Name: "@IDProcessRole", Value: idSelProcessRole },
            { Name: "@IDJobLevel", Value: idSelJobLevel },
            { Name: "@IDCompetency", Value: idSelComptency },
            { Name: "@IDSkill", Value: "0" }
        ],
        success: {
            fn: function (responseList) {
                if (responseList.length > 0) {
                    console.log("Filter Training information", responseList);
                    var objSummary = responseList[0];
                    window.objTrainingSummary = objSummary;

                    //Set selected values
                    //NOTE: Also need to update FROU_AssessmentOfEmployee.aspx:252 <button type="button" class="btn btn-default" id="filterTrainingSelected" value="WITH_DUE_DATES">
                    if (isViewOnlyAssessment() == true) {

                        //Set filter if trainings with due dates <> 0
                        if (objSummary.WITH_DUE_DATES != 0) {
                            $("#filterTrainingSelected").html("With due dates (" + objSummary.WITH_DUE_DATES + ")");
                            $("#filterTrainingSelected").attr("value", 'WITH_DUE_DATES');

                        } else if (objSummary.WITHOUT_DUE_DATES != 0) {
                            $("#filterTrainingSelected").html("Without due dates (" + objSummary.WITHOUT_DUE_DATES + ")");
                            $("#filterTrainingSelected").attr("value", 'WITHOUT_DUE_DATES');

                        } else {
                            $("#filterTrainingSelected").html("Required (" + objSummary.REQUIRED + ")");
                            $("#filterTrainingSelected").attr("value", 'REQUIRED');

                        }
                    } else {
                        $("#filterTrainingSelected").html("View all trainings (" + objSummary.ALL + ")");
                        $("#filterTrainingSelected").attr("value", 'ALL');
                    }

                    //Set option summary
                    $("#filterOptionAll").html("View all trainings (" + objSummary.ALL + ")");
                    $("#filterOptionRequired").html("Required (" + objSummary.REQUIRED + ")");
                    $("#filterOptionCompleted").html("Completed (" + objSummary.COMPLETED + ")");
                    $("#filterOptionPending").html("Pending (" + objSummary.PENDING + ")");
                    $("#filterOptionRequiredOptionals").html("Optionals (" + objSummary.REQUIRED_ONLY_OPTIONALS + ")");
                    $("#filterOptionWithoutDueDates").html("Without due dates (" + objSummary.WITHOUT_DUE_DATES + ")");
                    $("#filterOptionWithDueDates").html("With due dates (" + objSummary.WITH_DUE_DATES + ")");
                    $("#filterOptionOverdue").html("Overdue (" + objSummary.OVERDUE + ")");

                    //Set Pending Trainings field
                    $("#lblPendingTrainings").html(objSummary.PENDING);
                    $("#lblPendingTrainings").css('cursor', 'pointer');
                    if (typeof $._data($("#lblPendingTrainings")[0], 'events') == 'undefined') {
                        $("#lblPendingTrainings").click(function (e) {
                            e.preventDefault();
                            $("#filterTrainingSelected").html($("#filterOptionPending").html());
                            $("#filterTrainingSelected").attr("value", 'PENDING');
                            $("#btnAssessment").click();
                        });
                    }

                    //Set Overdue training field
                    $("#lblDueDatePast").html("(" + objSummary.OVERDUE + " Overdue)");
                    $("#lblDueDatePast").css('cursor', 'pointer');
                    if (typeof $._data($("#lblDueDatePast")[0], 'events') == 'undefined') {
                        $("#lblDueDatePast").click(function (e) {
                            e.preventDefault();
                            $("#filterTrainingSelected").html($("#filterOptionOverdue").html());
                            $("#filterTrainingSelected").attr("value", 'OVERDUE');
                            $("#btnAssessment").click();
                        });
                    }

                    //Set Completed Trainings field
                    $("#lblCompletedTrainings").html(objSummary.COMPLETED + ' of ' + objSummary.REQUIRED);

                    //Set Completion %
                    var completedPercentage = 0;
                    if (objSummary.REQUIRED && objSummary.REQUIRED != "0") {
                        completedPercentage = ((objSummary.COMPLETED ? objSummary.COMPLETED : 0) / objSummary.REQUIRED) * 100;
                    }
                    $("#lblCompletionPercentage").html((objSummary.COMPLETED + ' of ' + objSummary.REQUIRED) + ' / ' + completedPercentage.toFixed(2) + ' %');

                    //Completion % priority
                    var completedPercentagePriority = 0;
                    if (objSummary.REQUIRED_WITHOUT_OPTIONALS && objSummary.REQUIRED_WITHOUT_OPTIONALS != "0") {
                        completedPercentagePriority = ((objSummary.COMPLETED_WITHOUT_OPTIONALS ? objSummary.COMPLETED_WITHOUT_OPTIONALS : 0) / objSummary.REQUIRED_WITHOUT_OPTIONALS) * 100;
                    }
                    $("#lblCompletionPercentagePriority").html((objSummary.COMPLETED_WITHOUT_OPTIONALS + ' of ' + objSummary.REQUIRED_WITHOUT_OPTIONALS) + ' / ' + completedPercentagePriority.toFixed(2) + ' %');

                    //Completion % optionals
                    var completedPercentageOptionals = 0;
                    if (objSummary.REQUIRED_ONLY_OPTIONALS && objSummary.REQUIRED_ONLY_OPTIONALS != "0") {
                        completedPercentageOptionals = ((objSummary.COMPLETED_ONLY_OPTIONALS ? objSummary.COMPLETED_ONLY_OPTIONALS : 0) / objSummary.REQUIRED_ONLY_OPTIONALS) * 100;
                    }
                    $("#lblCompletionPercentageOptionals").html((objSummary.COMPLETED_ONLY_OPTIONALS + ' of ' + objSummary.REQUIRED_ONLY_OPTIONALS) + ' / ' + completedPercentageOptionals.toFixed(2) + ' %');
                }
            }
        }
    });

    //Add event to the options of the filter trainings
    if (typeof $._data($("#filterOptionAll:parent")[0], 'events') == 'undefined') {
        $("#filterOptionAll").parent().click(function (e) {
            e.preventDefault();
            $("#filterTrainingSelected").html($("#filterOptionAll").html());
            $("#filterTrainingSelected").attr("value", 'ALL');
        });

        $("#filterOptionRequired").parent().click(function (e) {
            e.preventDefault();
            $("#filterTrainingSelected").html($("#filterOptionRequired").html());
            $("#filterTrainingSelected").attr("value", 'REQUIRED');
        });

        $("#filterOptionCompleted").parent().click(function (e) {
            e.preventDefault();
            $("#filterTrainingSelected").html($("#filterOptionCompleted").html());
            $("#filterTrainingSelected").attr("value", 'COMPLETED');
        });

        $("#filterOptionPending").parent().click(function (e) {
            e.preventDefault();
            $("#filterTrainingSelected").html($("#filterOptionPending").html());
            $("#filterTrainingSelected").attr("value", 'PENDING');
        });

        $("#filterOptionRequiredOptionals").parent().click(function (e) {
            e.preventDefault();
            $("#filterTrainingSelected").html($("#filterOptionRequiredOptionals").html());
            $("#filterTrainingSelected").attr("value", 'REQUIRED:ONLY_OPTIONALS');
        });

        $("#filterOptionWithoutDueDates").parent().click(function (e) {
            e.preventDefault();
            $("#filterTrainingSelected").html($("#filterOptionWithoutDueDates").html());
            $("#filterTrainingSelected").attr("value", 'WITHOUT_DUE_DATES');
        });

        $("#filterOptionWithDueDates").parent().click(function (e) {
            e.preventDefault();
            $("#filterTrainingSelected").html($("#filterOptionWithDueDates").html());
            $("#filterTrainingSelected").attr("value", 'WITH_DUE_DATES');
        });

        $("#filterOptionOverdue").parent().click(function (e) {
            e.preventDefault();
            $("#filterTrainingSelected").html($("#filterOptionOverdue").html());
            $("#filterTrainingSelected").attr("value", 'OVERDUE');
        });
    }
}

function loadOtherAssessments() {
    $.jqxGridApi.create({
        showTo: "#tblOtherAssessments",
        options: {
            //for comments or descriptions
            height: "200",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            editable: true
        },
        sp: {
            Name: "[dbo].[FROU_spAssessmentsList]",
            Params: [
                { Name: "@EMPLOYEE_SOEID", Value: $("#lblEmployeeSOEID").text() }
            ]
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        columns: [
            //type: string - text - number - int - float - date - time 
            //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
            //cellsformat: ddd, MMM dd, yyyy h:mm tt
            { name: 'IDGlobalProcess', type: 'number', hidden: true },
            { name: 'IDProcessRole', type: 'number', hidden: true },
            { name: 'IDJobLevel', type: 'number', hidden: true },
            { name: 'SOEID', type: 'string', width: '10%' },
            { name: 'GlobalProcess', type: 'string', width: '25%' },
            { name: 'ProcessRole', type: 'string', width: '25%' },
            { name: 'JobLevel', type: 'string', width: '20%' },
            { name: 'LastUpdate', text: 'Last Discussion Date', type: 'date', width: '15%', cellsformat: "ddd, MMM dd, yyyy" },
            //{ name: 'CompletionPercentage', text: 'Completion %', type: 'number', width: '10%', cellsformat: "p" },
            {
                name: 'ViewAssessment', computedcolumn: true, text: ' ', type: 'html', editable: false, filterable: false, sortable: false, width: '5%', cellsrenderer: function (rowIndex, dataField, value) {
                    var dataRecord = $("#tblOtherAssessments").jqxGrid('getrowdata', rowIndex);
                    return '<div style="text-align: center;line-height: 28px;"><a href="FROU_AssessmentOfEmployee.aspx?' +
            	        'pempsid=' + _getViewVar("ViewModel").HFEmpsID +
                        '&pvo=' + (_getViewVar("ViewModel").HFViewOnly ? _getViewVar("ViewModel").HFViewOnly : '0') +
                        '&pme=' + (_getViewVar("ViewModel").HFViewOnlyMenu ? _getViewVar("ViewModel").HFViewOnlyMenu : '0') +
                        '&pvoa=1' +
                        '&pidgp=' + dataRecord.IDGlobalProcess +
                        '&pidgpd=' + encodeURIComponent(dataRecord.GlobalProcess) +
                        '&pidpr=' + dataRecord.IDProcessRole +
                        '&pidprd=' + encodeURIComponent(dataRecord.ProcessRole) +
                        '&pidjl=' + dataRecord.IDJobLevel +
                        '&pidjld=' + encodeURIComponent(dataRecord.JobLevel) + '">View</a></div>';
                }

            }
        ],
        ready: function () {
        }
    });
}

function isValidAssessment() {
    var result = true;
    var idSelGlobalProcess = $("#ddlGlobalProcess").val();
    var idSelProcessRole = $("#ddlProcessRole").val();
    var idSelJobLevel = $("#ddlJobLevel").val();
    var discussionDate = $("#txtDiscussionDate").val();

    if (!idSelGlobalProcess || idSelGlobalProcess == "0") {
        result = false;
        _showAlert({
            type: 'error',
            content: "Please select 'Global Process' this field cannot be empty."
        });
        $("#ddlGlobalProcess").focus();
    }
    if ((!idSelProcessRole || idSelProcessRole == "0") && result) {
        result = false;
        _showAlert({
            type: 'error',
            content: "Please select 'Process Role' this field cannot be empty."
        });
        $("#ddlProcessRole").focus();
    }
    if ((!idSelJobLevel || idSelJobLevel == "0") && result) {
        result = false;
        _showAlert({
            type: 'error',
            content: "Please select 'Job Level' this field cannot be empty."
        });
        $("#ddlJobLevel").focus();
    }
    if (!discussionDate && result) {
        result = false;
        _showAlert({
            type: 'error',
            content: "Please select 'Discussion date with Direct Report' this field cannot be empty."
        });
    }
    return result;
}

function saveAssessment($el, typeSaveBtn) {
    //Validate if the manager change the Process Role or Job Level
    //if (($("#ddlJobLevel").val() != _getViewVar("ViewModel").HFCurrentIDJobLevel) && !isViewOtherAssessment()) {

    //    showJSConfirmModal("<b>Would you like to continue?</b><br><br>This action (Job Level “" + $(_getASPControlID("HFCurrentJobLevel")).val() + "” to “" + $("#ddlJobLevel").find('option:selected').text() + "”) will require re-selection of priority and required trainings", function () {
    //        runSaveAssessment();
    //    });

    //} else if (($("#ddlProcessRole").val() != _getViewVar("ViewModel").HFCurrentIDProcessRole) && !isViewOtherAssessment()) {

    //    showJSConfirmModal("<b>Would you like to continue?</b><br><br>This action (Process Role “" + $(_getASPControlID("HFCurrentProcessRole")).val() + "” to “" + $("#ddlProcessRole").find('option:selected').text() + "”) will require re-selection of priority and required trainings", function () {
    //        runSaveAssessment();
    //    });

    //} else {
    //    runSaveAssessment();
    //}
    function runSaveAssessment() {
        var loadinMsgType = "fullLoading"; //fullLoading, notification, topBar, alert

        if (isValidAssessment()) {

            var dataSkillAssessment = [];
            var dataSkillItemAssessment = [];
            var dataAdditionalDevelopmentAssessment = [];
            var idSelGlobalProcess = $("#ddlGlobalProcess").val();
            var idSelProcessRole = $("#ddlProcessRole").val();
            var idSelJobLevel = $("#ddlJobLevel").val();
            var discussionDate = $("#txtDiscussionDate").val();
            var soeid = $("#lblEmployeeSOEID").text();

            // Skill Information
            $('div.panel[idSkill]').each(function () {
                var tempData = {};

                $trSkillRow = $(this);
                $selCurrentState = $trSkillRow.find('.ddlCurrentState');

                if ($selCurrentState.length > 0) {
                    tempData["skill"] = $trSkillRow.find('a[Skill]').attr("Skill");
                    tempData["idSkill"] = $trSkillRow.attr("idSkill");
                    tempData["idPLCurrentState"] = $selCurrentState.val();

                    dataSkillAssessment.push(tempData);
                }
            });

            // Skilll Item Information
            $('tr[idSkillItem]').each(function () {
                var tempData = {};

                $trSkillItemRow = $(this);
                $chkFlagNotRequired = $trSkillItemRow.find('.chkFlagNotRequired');
                $targetDateTraining = $trSkillItemRow.find('.txtTargetDate');

                tempData["idSkill"] = $trSkillItemRow.attr("idSkill");
                tempData["idSkillItem"] = $trSkillItemRow.attr("idSkillItem");
                tempData["skillItem"] = $trSkillItemRow.attr("skillItem");
                tempData["idProficiencyLevel"] = $trSkillItemRow.attr("idProficiencyLevel");
                tempData["idStatus"] = 0;
                tempData["flagNotRequired"] = ($chkFlagNotRequired.is(":checked")) ? '1' : '0';
                tempData["targetDateTraining"] = $targetDateTraining.val();
                dataSkillItemAssessment.push(tempData);
            });

            //Additional Development Information
            $('tr[idAdditionalDevelopment]').each(function () {
                var tempData = {};

                $trAdditionalDevelopmentRow = $(this);
                $selProficiencyLevel = $trAdditionalDevelopmentRow.find('.selProficiencyLevel');
                $selArea = $trAdditionalDevelopmentRow.find('.selArea');
                $txtComment = $trAdditionalDevelopmentRow.find('.txtComment');
                $txtTargetDate = $trAdditionalDevelopmentRow.find('.txtTargetDate');
                $selStatus = $trAdditionalDevelopmentRow.find('.selStatus');

                tempData["idSkill"] = $trAdditionalDevelopmentRow.attr("additionalDevelopment-idSkill");
                tempData["idAdditionalDevelopment"] = $trAdditionalDevelopmentRow.attr("idAdditionalDevelopment");
                tempData["idProficiencyLevel"] = $selProficiencyLevel.val();
                tempData["idArea"] = $selArea.val();
                tempData["comment"] = $txtComment.val();
                tempData["targetDate"] = $txtTargetDate.val();
                tempData["idStatus"] = $selStatus.val();
                tempData["idTempRow"] = $trAdditionalDevelopmentRow.attr("id");

                dataAdditionalDevelopmentAssessment.push(tempData);
            });

            //Create change id
            _callProcedure({
                loadingMsgType: loadinMsgType, //fullLoading, notification, topBar, alert
                loadingMsg: "Creating change history...",
                name: "[dbo].[FROU_spAssessmentHistoryChangeCreate]",
                params: [
                    { Name: "@EMPLOYEE_SOEID", Value: soeid },
                    { Name: "@ID_PROCESS_ROLE", Value: idSelProcessRole },
                    { Name: "@ID_JOB_LEVEL", Value: idSelJobLevel },
                    { Name: "@CREATED_SOEID", Value: _getSOEID() },
                    { Name: "@COMMENT", Value: "" }
                ],
                success: {
                    fn: function (responseList) {
                        if (responseList.length > 0) {
                            var idChange = responseList[0].IDNewChange

                            //Save Employee Information
                            _callProcedure({
                                response: false,
                                loadingMsgType: loadinMsgType, //fullLoading, notification, topBar, alert
                                loadingMsg: "Saving " + _getViewVar("ViewModel").lblEmployeeName + " Information...",
                                name: "[dbo].[FROU_spAssessmentMapEmployee]",
                                params: [
                                    { Name: "@SOEID", Value: soeid },
                                    { Name: "@IDChange", Value: idChange },
                                    { Name: "@IDJobLevel", Value: idSelJobLevel },
                                    { Name: "@IDGlobalProcess", Value: idSelGlobalProcess },
                                    { Name: "@IDProcessRole", Value: idSelProcessRole },
                                    { Name: "@DiscussionDate", Value: discussionDate }
                                ],
                                success: {
                                    fn: function (responseList) {
                                        _showNotification('success', "The assessment was saved successfully.");

                                        //Update current IDProcessRole and IDJobLevel BD Values
                                        _getViewVar("ViewModel").HFCurrentIDProcessRole = idSelProcessRole;
                                        _getViewVar("ViewModel").HFCurrentIDJobLevel = idSelJobLevel;

                                        //Save Skill Information
                                        if (dataSkillAssessment.length > 0) {
                                            for (var i = 0; i < dataSkillAssessment.length; i++) {
                                                var dataSkill = dataSkillAssessment[i];

                                                _callProcedure({
                                                    response: false,
                                                    loadingMsgType: loadinMsgType, //fullLoading, notification, topBar, alert
                                                    loadingMsg: "Saving Current States for Skills...",
                                                    name: "[dbo].[FROU_spAssessmentSkill]",
                                                    params: [
                                                        { Name: "@SOEID", Value: soeid },
                                                        { Name: "@IDChange", Value: idChange },
                                                        { Name: "@IDProcessRole", Value: idSelProcessRole },
                                                        { Name: "@IDJobLevel", Value: idSelJobLevel },

                                                        { Name: "@IDSkill", Value: dataSkill.idSkill },
                                                        { Name: "@IDProficiencyLevel", Value: dataSkill.idPLCurrentState }
                                                    ],
                                                    success: {
                                                        params: {
                                                            isLastSkill: (i == dataSkillAssessment.length - 1)
                                                        },
                                                        fn: function (responseList, params) {
                                                            if (params.isLastSkill) {
                                                                _showNotification("success", "Current State of Skills were saved successfully.");
                                                            }
                                                        }
                                                    }
                                                });
                                            }
                                        }

                                        //Save SkillItem Information
                                        if (dataSkillItemAssessment.length > 0) {
                                            for (var i = 0; i < dataSkillItemAssessment.length; i++) {
                                                var dataSkillItem = dataSkillItemAssessment[i];
                                                var psParams = [
                                                        { Name: "@SessionSOEID", Value: _getSOEID() },
                                                        { Name: "@SOEID", Value: soeid },
                                                        { Name: "@IDChange", Value: idChange },
                                                        { Name: "@IDProcessRole", Value: idSelProcessRole },
                                                        { Name: "@IDJobLevel", Value: idSelJobLevel },
                                                        { Name: "@IDSkill", Value: dataSkillItem.idSkill },
                                                        { Name: "@IDSkillItem", Value: dataSkillItem.idSkillItem },
                                                        { Name: "@IDStatus", Value: dataSkillItem.idStatus },
                                                        { Name: "@FlagNotRequired", Value: dataSkillItem.flagNotRequired }
                                                ];

                                                if (dataSkillItem.targetDateTraining) {
                                                    psParams.push({ Name: "@TargetDateTraining", Value: dataSkillItem.targetDateTraining });
                                                }

                                                _callProcedure({
                                                    response: false,
                                                    loadingMsgType: loadinMsgType, //fullLoading, notification, topBar, alert
                                                    loadingMsg: "Saving 'Due Dates' and 'Tranings Required' for Skills...",
                                                    name: "[dbo].[FROU_spAssessmentSkillItems]",
                                                    params: psParams,
                                                    success: {
                                                        params: {
                                                            isLastSkillItem: (i == dataSkillItemAssessment.length - 1)
                                                        },
                                                        fn: function (responseList, params) {
                                                            if (params.isLastSkillItem) {
                                                                _showNotification("success", "'Due Dates' and 'Required Trainings' of Skills were saved successfully.");
                                                            }
                                                        }
                                                    }
                                                });
                                            }
                                        }

                                        //Saving Additional Developments Needs
                                        if (dataAdditionalDevelopmentAssessment.length > 0) {
                                            _callProcedure({
                                                loadingMsgType: loadinMsgType, //fullLoading, notification, topBar, alert
                                                loadingMsg: "Saving Skill Information...",
                                                name: "[dbo].[FROU_spAssessmentMapEmployee]",
                                                params: [
                                                    { Name: "@SOEID", Value: soeid },
                                                    { Name: "@IDChange", Value: idChange },
                                                    { Name: "@IDJobLevel", Value: idSelJobLevel },
                                                    { Name: "@IDGlobalProcess", Value: idSelGlobalProcess },
                                                    { Name: "@IDProcessRole", Value: idSelProcessRole },
                                                    { Name: "@DiscussionDate", Value: discussionDate }
                                                ],
                                                success: {
                                                    fn: function (responseList) {

                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            });

                            //$.ajax({
                            //    url: 'FROU_AssessmentOfEmployeeAJAX.aspx',
                            //    data: {
                            //        paction: 'saveAssessment',
                            //        psoeid: soeid,
                            //        pidChange: idChange,
                            //        pidJobLevel: idSelJobLevel,
                            //        pidGlobalProcess: idSelGlobalProcess,
                            //        pidProcessRole: idSelProcessRole,
                            //        pdiscussionDate: discussionDate,
                            //        pdataSkill: dataSkillAssessment,
                            //        pdataSkillItem: dataSkillItemAssessment,
                            //        pdataAdditionalDevelopment: dataAdditionalDevelopmentAssessment
                            //    },
                            //    beforeSend: function () {
                            //        _showLoadingTopMessage();
                            //    },
                            //    success: function (response) {
                            //        //Only is JSON With Additional Development Records Mapping
                            //        try {
                            //            var recordsMapping = $.parseJSON(response);

                            //            $.each(recordsMapping, function (key, val) {
                            //                $("#" + key).attr("idAdditionalDevelopment", val);
                            //                var $imgDeleteButton = $("#" + key).find("img");
                            //                var strOnclick = $imgDeleteButton.attr("onclick");
                            //                $imgDeleteButton.attr("onclick", strOnclick.replace(", 0", ", " + val));
                            //            });
                            //        }
                            //        catch (err) { }

                            //        if (typeSaveBtn == "OtherDevActionsSaveBtn") {
                            //            _showAlert({
                            //                type: 'success',
                            //                content: "The information was saved successfully.",
                            //                showToElTop: $el           //img
                            //                            .parent()   //td
                            //                            .parent()   //tr
                            //                            .parent()   //tbody
                            //                            .parent(),  //table
                            //                animateScrollTop: false
                            //            });
                            //        } else {
                            //            _showAlert({
                            //                type: 'success',
                            //                content: "The assessment was saved successfully."
                            //                //content: "The assessment was saved successfully. <br/> <br/> <b>Note:</b> Please refresh the page (F5) to update the percentages, pending, overdue and completed tranings."
                            //            });
                            //        }

                            //        //Set the new process role and job level
                            //        _getViewVar("ViewModel").HFCurrentIDProcessRole = idSelProcessRole;
                            //        _getViewVar("ViewModel").HFCurrentIDJobLevel = idSelJobLevel;

                            //        //Set all trainings
                            //        loadFilterTrainingSummary();

                            //        _hideLoadingTopMessage();
                            //    },
                            //    error: function (xhr, textStatus, thrownError) {
                            //        _hideLoadingTopMessage();
                            //        if (xhr['responseText']) {
                            //            _showAlert({
                            //                type: 'error',
                            //                content: xhr['responseText']
                            //            });
                            //        } else {
                            //            _showAlert({
                            //                type: 'error',
                            //                content: thrownError
                            //            });
                            //        }
                            //    },
                            //    type: "POST"
                            //});
                        }
                    }
                }
            });
        }
    }

    runSaveAssessment();
}

function updateSkillSummary($trSkill) {
    //console.log("chkFlagNotRequired", $trSkill.next().find("tr[skilliitem-idskill]"));
    var $trSkillItems = $trSkill.next().find("tr[skilliitem-idskill]");
    var $otherDevelopmentActions = $trSkill.next().find("tr[idadditionaldevelopment]");
    var totalTraining = 0;
    var completedTraining = 0;
    var pendingTraining = 0;
    var overDueTraining = 0;
    var percentage = 0;

    //Loop for trainings
    $trSkillItems.each(function () {
        $tr = $(this);

        //Not Required
        if ($tr.find(".chkFlagNotRequired").is(':checked') == false && ($tr.find(".status").text() == "Pending" || $tr.find(".status").text() == "Pending - Optional")) {
            pendingTraining++;
        }

        //Status
        if ($tr.find(".status").text() == "Completed") {
            completedTraining++;
        }
    });

    //Loop for Other Development Actions
    $otherDevelopmentActions.each(function () {
        $tr = $(this);

        //Pending
        if ($tr.find(".selStatus").val() == "2") { //2: Pending
            pendingTraining++;
        }

        //Status
        if ($tr.find(".selStatus").val() == "3") { //3: Completed
            completedTraining++;
        }
    });

    totalTraining = pendingTraining + completedTraining;
    percentage = (totalTraining > 0) ? (100 / totalTraining) * completedTraining : 0;

    //Round 2 decimals
    percentage = (Math.round(percentage * 100) / 100);

    //console.log("totalTraining", totalTraining);
    //console.log("completedTraining", completedTraining);
    //console.log("pendingTraining", pendingTraining);

    $trSkill.find(".pendingTraining").html(pendingTraining);
    $trSkill.find(".completedTraining").html(completedTraining + " of " + totalTraining);
    $trSkill.find(".percentageTraining").html(percentage + " %");
}

function loadSkillItems(pparams) {
    var $competencyTitle = $('#competency-' + pparams["idCompetencySkill"] + '-title');
    var idSkill = pparams["idSkill"];
    var $el = pparams["el"];
    var filterTrainings = $("#filterTrainingSelected").attr("value");

    $.ajax({
        url: 'FROU_AssessmentOfEmployeeAJAX.aspx',
        data: {
            paction: 'getHTMLTrainingsAndOthers',
            psoeidEmployee: pparams["soeidEmployee"],
            pidProcessRole: pparams["idSelProcessRole"],
            pidJobLevel: pparams["idSelJobLevel"],
            pidCompetency: pparams["idSelCompetency"],
            pidPLCurrentState: pparams["idSelPLCurrentState"],
            pidSkill: idSkill,
            pfilterTrainings: filterTrainings
        },
        beforeSend: function () {
            _showLoadingTopMessage();
        },
        success: function (response) {
            $el.removeAttr("disabled");
            var $trSkill = $('tr[idSkill="' + idSkill + '"]');
            $trSkill.after(response);

            var $trSkillItems = $("#skillMapping-" + idSkill + "-skill-items");
            $trSkillItems.hide();
            $trSkillItems.show('slow');

            $trSkillItems.find('.datepicker').datepicker();

            $trSkill.find('.selCurrentState').removeAttr('disabled');

            _hideLoadingTopMessage();
            $el.attr("src", "App_Themes/Base/images/toggle-collapse-green.png");

            $(".toggle-btn-skill-" + idSkill + "-type").click(function (e) {
                var $el = $(this);
                var idTableSkillItemType = $el.attr("targetToggleElementID");
                var $tableSkillItemType = $('#' + idTableSkillItemType);
                var btnToggleText = $el.text();

                $tableSkillItemType.toggle();

                if ($tableSkillItemType.is(":visible")) {
                    $el.text(btnToggleText.replace('+', '-'));
                } else {
                    $el.text(btnToggleText.replace('-', '+'));
                }

                e.preventDefault();
            });

            //Send notifications of trainins (Remember and Retake)
            $trSkill.next().find('.btnSendNotification').click(function (e) {
                e.preventDefault();

                var $el = $(this);
                var soeidEmployee = $("#lblEmployeeSOEID").text();
                var idSelProcessRole = $("#ddlProcessRole").val();
                var idSelJobLevel = $("#ddlJobLevel").val();
                var idSkill = $el.parent().parent().attr("skillIItem-idSkill");
                var idSkillItem = $el.attr('idSkillItem');
                var typeNotification = $el.attr('typeNotification');

                $.ajax({
                    url: 'FROU_AssessmentOfEmployeeAJAX.aspx',
                    data: {
                        paction: 'sendNotificationOfTraining',
                        psoeidEmployee: soeidEmployee,
                        pidProcessRole: idSelProcessRole,
                        pidJobLevel: idSelJobLevel,
                        pidSkill: idSkill,
                        pidSkillItem: idSkillItem,
                        ptypeNotification: typeNotification
                    },
                    beforeSend: function () {
                        _showLoadingTopMessage();
                    },
                    success: function (response) {
                        generateAlert('success', 'The notification was sent successfully.');
                        _hideLoadingTopMessage();
                        $el.attr('disabled', 'disabled');

                        $el.after($('<span>' + $el.text() + '</span>'));
                        $el.remove();
                    },
                    error: function (xhr, textStatus, thrownError) {
                        _hideLoadingTopMessage();
                        if (xhr['responseText']) {
                            _showAlert({
                                type: 'error',
                                content: xhr['responseText']
                            });
                        } else {
                            _showAlert({
                                type: 'error',
                                content: thrownError
                            });
                        }
                    },
                    type: "POST"
                });
            });

            //Targetdate change
            $trSkill.next().find('.txtTrainingTargetDate').change(function (e) {
                updateSkillSummary($trSkill);
            });

            //Add Check all "Not Required"
            $trSkill.next().find(".chkAllFlagNotRequired").click(function () {
                var $el = $(this);
                var isCheckedAll = $el.is(':checked');

                //Check all element
                $trSkill.next().find('.chkFlagNotRequired').each(function () {
                    var $op = $(this);
                    var $trSkillItem = $op.parent().parent();
                    var $colStatus = $trSkillItem.find(".colStatus");
                    var $colDueDate = $trSkillItem.find(".txtTrainingTargetDate");

                    if ($op.is(':checked') != isCheckedAll && $colStatus.find(".status") != "Completed" && !$colDueDate.val()) {
                        $op.click();
                    }
                });
            });

            //When click on check of "Required"
            $trSkill.next().find('.chkFlagNotRequired').change(function (e) {
                var $el = $(this);
                var $trSkillItem = $('tr[idskillitem="' + $el.attr('idSkillItem') + '"]');
                //var indexPendingCol = 4;
                //var indexCompleteDateCol = indexPendingCol - 1;

                var $row = $el.parent().parent();
                var idSkill = $row.attr("skilliitem-idskill");
                var idPFCurrentState = $('[skillMapping-' + idSkill + ']').find('.selCurrentState').val();
                var textIdealState = $('[skillMapping-' + idSkill + ']').find('.selCurrentState').parent().next().text();
                var idPFLevelTraining = $row.attr('skilliitem-idproficiencylevel');
                var $chkFlagNotRequired = $row.find('.chkFlagNotRequired');

                var $colCompletedDate = $trSkillItem.find(".colCompletedDate");
                var $colStatus = $trSkillItem.find(".colStatus");
                var $colDueDate = $trSkillItem.find(".txtTrainingTargetDate");

                //If we have old values saved load them
                if ($trSkillItem.attr("oldValueStatusClass")) {
                    $trSkillItem.attr("style", $trSkillItem.attr("oldValue"));

                    $trSkillItem.removeClass($trSkillItem.attr("statusClass"));
                    $trSkillItem.addClass($trSkillItem.attr("oldValueStatusClass"));
                    $trSkillItem.attr("statusClass", $trSkillItem.attr("oldValueStatusClass"));

                    $colCompletedDate.html($colCompletedDate.attr("oldValue"));
                    $colStatus.html($colStatus.attr("oldValue"));
                    $colDueDate.val($colDueDate.val() ? $colDueDate.val() : ($colDueDate.attr("oldValue") ? $colDueDate.attr("oldValue") : $colDueDate.val()));

                    //Remove attr oldValue, because was loaded
                    $trSkillItem.removeAttr("oldValue");
                    $colCompletedDate.removeAttr("oldValue");
                    $colStatus.removeAttr("oldValue");
                    $colDueDate.removeAttr("oldValue");
                    $trSkillItem.removeAttr("oldValueStatusClass");

                    //Disable or Enable Due Date Field
                    if ($el.is(':checked') == true) {
                        //Disable Target Date" 
                        $colDueDate.attr("disabled", "disabled");

                        //If the field have value save it
                        $colDueDate.attr("oldValue", $colDueDate.val());
                        $colDueDate.val("");
                    } else {
                        //Enable Target Date" 
                        $colDueDate.removeAttr("disabled");
                        $colDueDate.removeAttr("readonly");
                    }
                } else {
                    //Save old HTML
                    $trSkillItem.attr("oldValue", $trSkillItem.attr("style") ? $trSkillItem.attr("style") : "");
                    $trSkillItem.attr("oldValueStatusClass", $trSkillItem.attr("statusClass"));
                    $colCompletedDate.attr("oldValue", $colCompletedDate.html());
                    $colStatus.attr("oldValue", $colStatus.html());
                    $colDueDate.attr("oldValue", $colDueDate.val() ? $colDueDate.val() : ($colDueDate.attr("oldValue") ? $colDueDate.attr("oldValue") : $colDueDate.val()));

                    //----------------------------------------------------------------------------
                    //When user click "Not Required"
                    //----------------------------------------------------------------------------
                    if ($el.is(':checked') == true) {
                        //Set style italic and color Gray
                        //$trSkillItem.css({
                        //    "font-style": "italic",
                        //    "color": "rgb(185, 185, 185)"
                        //});

                        if ($colStatus.find(".status").text() != "Completed") {
                            //Set new HTML 
                            $colCompletedDate.html("N/A");

                            //Remove due Date
                            $colDueDate.val("");
                        }

                        $colStatus.html("<span class='status'>Not required</span>");

                        //Set new Class Status Not Required
                        $trSkillItem.removeClass($trSkillItem.attr("statusClass"));
                        $trSkillItem.addClass("SkillItemRowNotRequired");
                        $trSkillItem.attr("statusClass", "SkillItemRowNotRequired");


                        //Disable Target Date" 
                        $colDueDate.attr("disabled", "disabled");

                    } else {

                        //Remove style italic and color Gray
                        //$trSkillItem.css({
                        //    "font-style": "",
                        //    "text-decoration": "",
                        //    "color": ""
                        //});

                        //Set new Class Status Not Required
                        $trSkillItem.removeClass($trSkillItem.attr("statusClass"));

                        //Set new HTML
                        if (idPFLevelTraining > getValueFromIdealState(textIdealState)) {
                            if ($colStatus.find(".status").text() != "Completed") {
                                $colCompletedDate.html("Pending");
                                $colStatus.html("<span class='status'>Pending - Optional</span>");
                            }

                            //Add new Class Status Not Required
                            $trSkillItem.addClass("SkillItemRowOptional");
                            $trSkillItem.attr("statusClass", "SkillItemRowOptional");
                        } else {
                            if ($colStatus.find(".status").text() != "Completed") {
                                $colCompletedDate.text("Pending");
                                $colStatus.html("<span class='status'>Pending</span>");
                            }

                            //Add new Class Status Not Required
                            $trSkillItem.addClass("SkillItemRowPending");
                            $trSkillItem.attr("statusClass", "SkillItemRowPending");

                            //Set old Value
                            $colDueDate.val($colDueDate.attr("oldValue") ? $colDueDate.attr("oldValue") : "");
                        }


                        //Enable Target Date" 
                        $colDueDate.removeAttr("disabled");
                        $colDueDate.removeAttr("readonly");

                    }
                }

                //Update summary of the Skill
                updateSkillSummary($trSkill);
            });

            //Automatically check "Not Required" column base in "Current State"
            calculteRequiredTrainingsBasedCurrentState(idSkill);

            //Check if is View Only
            viewOnlyAfterExpandSkills($trSkill);

        },
        error: function (xhr, textStatus, thrownError) {
            $el.removeAttr("disabled");
            _hideLoadingTopMessage();
            if (xhr['responseText']) {
                _showAlert({
                    type: 'error',
                    content: xhr['responseText']
                });
            } else {
                _showAlert({
                    type: 'error',
                    content: thrownError
                });
            }
        },
        type: "POST"
    });
}

function calculteRequiredTrainingsBasedCurrentState(objSkill, ddlCurrentState) {
    console.log("Calculating Required Trainings for IDSkill " + objSkill.IDSkill);

    var $aSkillInfo = $("a[IDSkill='" + objSkill.IDSkill + "']");

    if ($("#contentSkill-" + objSkill.IDSkill).attr("IsLoaded") == "1") {
        $("tr[IDSkillItem]tr[IDSkill='" + objSkill.IDSkill + "']").each(function () {
            var $trTraining = $(this);
            updateTrainingStatus({
                IDSkillItem: $trTraining.attr("IDSkillItem")
            }, 'CurrentState');
        });
    } else {
        //Load GLMS Trainings
        loadGLMSTrainings(objSkill);

        //Load Udemy Trainings
        loadUdemyTrainings(objSkill);

        //Load Other Development Actions
        loadOtherDevelopmentActions(objSkill);

        //Trainings loaded
        $("#contentSkill-" + objSkill.IDSkill).attr("IsLoaded", "1");
    }
    
    calculateDevelopmentNeed(
        $aSkillInfo.attr("IDPFIdealState"),
        $aSkillInfo.attr("PFIdealState"),
        ddlCurrentState,
        $("#contentSkill-" + objSkill.IDSkill).find(".lblDevelopmentNeeds")
    );

    //var idPFCurrentState = $('[skillMapping-' + idSkill + ']').find('.selCurrentState').val();
    //var textIdealState = $('[skillMapping-' + idSkill + ']').find('.selCurrentState').parent().next().text();
    ////var idPFIdealState = $('[skillMapping-' + idSkill + ']').find('.selIdealState').val();

    ////Auto check Required trainings
    //$('[skilliitem-idskill=' + idSkill + ']').each(function () {
    //    var $row = $(this);
    //    var idPFLevelTraining = $row.attr('skilliitem-idproficiencylevel');
    //    var $chkFlagNotRequired = $row.find('.chkFlagNotRequired');

    //    var $colStatus = $row.find(".colStatus");
    //    var $colCompletedDate = $row.find(".colCompletedDate");
    //    var $colDueDate = $row.find(".txtTrainingTargetDate");

    //    //Validate if Completed Training is Not Required
    //    if ($colStatus.find(".status").text() == 'Completed' && $chkFlagNotRequired.is(':checked') == true) {
    //        //Save old HTML
    //        $row.attr("oldValue", $row.attr("style") ? $row.attr("style") : "");
    //        $row.attr("oldValueStatusClass", $row.attr("statusClass"));

    //        $colCompletedDate.attr("oldValue", $colCompletedDate.html());
    //        $colStatus.attr("oldValue", $colStatus.html());
    //        $colDueDate.attr("oldValue", $colDueDate.val());

    //        //Set new HTML 
    //        $colStatus.html("<span class='status'>Not required</span>");

    //        //Set new Class Status Not Required
    //        $row.removeClass($row.attr("statusClass"));
    //        $row.addClass("SkillItemRowNotRequired");
    //        $row.attr("statusClass", "SkillItemRowNotRequired");

    //        //Disable Target Date" 
    //        $colDueDate.attr("disabled", "disabled");
    //    }

    //    if ($colStatus.find(".status").text() != 'Completed' && $chkFlagNotRequired.attr('dbsaved') != 'true' && !$colDueDate.val()) {
    //        //Doy Req: Check all with Not Required If "Not Required" is not Check
    //        //if ($chkFlagNotRequired.is(':checked') == false) {
    //        //    $chkFlagNotRequired.click();
    //        //}

    //        //If Current Status == "-- Select --"
    //        if (idPFCurrentState == "0") {

    //            //Set the training as Not Required
    //            if ($chkFlagNotRequired.is(':checked') == false) {
    //                $chkFlagNotRequired.click();
    //            }

    //        } else {
    //            //------------------------------------------------------------------------------
    //            //FOR NEW ASSESSMENTS
    //            //------------------------------------------------------------------------------
    //            //If I'm Intermediate, I don't need to take basic and Intermediate trainings
    //            if (idPFLevelTraining <= idPFCurrentState) {
    //                //If "Not Required" is Check  [ ] = [x]
    //                if ($chkFlagNotRequired.is(':checked') == false) {
    //                    $chkFlagNotRequired.click();
    //                }

    //            } else {
    //                //If I'm Started validate Pending Training between Ideal State and Proficiency level of the training
    //                //Calculate optional traingins
    //                if (idPFLevelTraining > getValueFromIdealState(textIdealState)) {
    //                    $colStatus.html("<span class='status'>Pending - Optional</span>");
    //                    $colCompletedDate.text("Pending");
    //                    //If "Not Required" is Not Checked [ ] = [X]
    //                    if ($chkFlagNotRequired.is(':checked') == false) {
    //                        $chkFlagNotRequired.click();
    //                    }
    //                } else {
    //                    $colStatus.html("<span class='status'>Pending</span>");
    //                    $colCompletedDate.text("Pending");
    //                    //If "Not Required" is Checked [x] = [ ]
    //                    if ($chkFlagNotRequired.is(':checked') == true) {
    //                        $chkFlagNotRequired.click();
    //                    }
    //                }
    //            }
    //        }
    //    }
    //});
}

function calculateDevelopmentNeed(pidPFIdealState, ppfIdealState, pel, showTo) {
    var $el = $(pel);
    var developmentNeed = "";
    var idPFCurrentState = $el.find(':selected').val();
    var pfCurrentState = $el.find(':selected').text();

    if (pidPFIdealState > 0) {
        if (idPFCurrentState > 0 || idPFCurrentState == -1) {
            if (idPFCurrentState < pidPFIdealState) {
                developmentNeed = pfCurrentState + " to " + ppfIdealState;
            }
            if (idPFCurrentState == pidPFIdealState) {
                developmentNeed = "None";
            }
            if (idPFCurrentState > pidPFIdealState) {
                developmentNeed = "+";
            }
        } else {
            developmentNeed = "Pending Current State";
        }
    } else {
        developmentNeed = "Pending Ideal State";
    }

    $(showTo).val(developmentNeed);
    $(showTo).text(developmentNeed);

    //$expandBtn = $('#' + $el.attr('idExpandBtn'));
    //$expandBtn.attr('forceRefresh', true);
    //$expandBtn.click();
}

var contNewAdditionalDevelopmentRow = 1;
function addNewAdditionalDevelopment(pidBodyTable) {
    var $bodyTable = $("#" + pidBodyTable);
    var strHTMLNewRow = $bodyTable.attr("strHTMLNewRow");

    //Add new ID to the new row
    var idNewRow = "additional-development-new-" + contNewAdditionalDevelopmentRow;
    strHTMLNewRow = strHTMLNewRow.replace(":pidNewRow", idNewRow);                  //<tr id=":pidNewRow"
    strHTMLNewRow = strHTMLNewRow.replace("':pidNewRow'", "'" + idNewRow + "'");    //<img onclick="deleteAdditionalDevelopment(':pidNewRow'
    contNewAdditionalDevelopmentRow++;

    $bodyTable.append(strHTMLNewRow);
    var $HTMLNewRow = $bodyTable.find("#" + idNewRow);
    $HTMLNewRow.hide().show("slow");
    $HTMLNewRow.find('.datepicker').datepicker();
}

function deleteAdditionalDevelopment(pidElToDelete, pidAdditionalDevelopmentToDelete, el) {
    if (showConfirmModal("Are you sure to delete this row from Additional Developments?", el)) {
        if (pidAdditionalDevelopmentToDelete != 0) {
            $.ajax({
                url: 'FROU_AssessmentOfEmployeeAJAX.aspx',
                data: {
                    paction: 'deleteAdditionalDevelopment',
                    pidAdditionalDevelopment: pidAdditionalDevelopmentToDelete
                },
                beforeSend: function () {
                    _showLoadingTopMessage();
                },
                success: function (response) {
                    generateAlert('success', 'The record was deleted successfully.');
                    _hideLoadingTopMessage();

                    $("#" + pidElToDelete).remove();
                },
                error: function (xhr, textStatus, thrownError) {
                    _hideLoadingTopMessage();
                    if (xhr['responseText']) {
                        _showAlert({
                            type: 'error',
                            content: xhr['responseText']
                        });
                    } else {
                        _showAlert({
                            type: 'error',
                            content: thrownError
                        });
                    }
                },
                type: "POST"
            });
        } else {
            $("#" + pidElToDelete).remove();
        }
    }
}

function getValueFromIdealState(pidealState) {
    var result = "0";
    switch (pidealState) {
        case "Starter":
            result = "-1";
            break;
        case "Basic":
            result = "1";
            break;
        case "Intermediate":
            result = "2";
            break;
        case "Advanced":
            result = "3";
            break;
    }
    return result;
}

//Other assessment functions
function isViewOtherAssessment() {
    return _getViewVar("ViewModel").HFViewOtherAssessment == "1";
}

function viewOtherAssessment() {
    if (isViewOtherAssessment()) {
        $("#headerTitle").html("Historical assessment: " + _getViewVar("ViewModel").HFOtherGlobalProcess + " - " + _getViewVar("ViewModel").HFOtherProcessRole + " - " + _getViewVar("ViewModel").HFOtherJobLevel);
        //$("#btnReturnAssessment").show();
    } else {
        $("#headerTitle").html("Assessment of Employee Skills");
        $("#btnReturnAssessment").hide();
    }
}

//View Only Functions

function isViewOnlyAssessment() {
    return _getViewVar("ViewModel").HFViewOnly == "1";
}

function isViewOnlyFieldsSelectedByManager() {
    var result = true;

    if ($("#ddlGlobalProcess").val() == "0" ||
        $("#ddlProcessRole").val() == "0" ||
        $("#ddlJobLevel").val() == "0") {
        _showAlert({
            type: 'error',
            content: "Global Process, Process Role and Job Level must be selected by your Manager to review your assessment."
        });

        $(".customBtn").remove();
        $("#btnAssessment").remove();

        result = false;
    }

    return result;
}

function viewOnlyFirstLoad() {
    if (isViewOnlyAssessment()) {
        //Remove save button
        $("#btnSaveTop").remove();

        //Disable Global Process
        $("#ddlGlobalProcess").attr("disabled", "disabled");

        //Disable Process Role
        $("#ddlProcessRole").attr("disabled", "disabled");

        //Disable Job Level
        $("#ddlJobLevel").attr("disabled", "disabled");

        //Remove Menu
        if (_getViewVar("ViewModel").HFViewOnlyMenu != "1") {
            $(".menu-nav").html("");

            //Add link to request access
            $(".menu-nav").html("<li><a href='FROU_Default.aspx' title='FRO Finance University Home Page'>Portal Home</a></li><li><a href='doc/FAQ_CSS_University.pdf' title='FAQ' target='_blank'>FAQ</a></li>");
        }

        //Modify title
        $(".titleBig").html("CSS University – FRO Curriculum - My Assessment");

        //Check all fields selected
        if (isViewOnlyFieldsSelectedByManager()) {

            //Load the assessment
            $("#btnAssessment").click();

        }
    }
}

function viewOnlyAfterLoadAssessment() {
    if (isViewOnlyAssessment()) {
        //Block the current State
        $(".selCurrentState").attr("disabled", "disabled");

        //Remove the save button
        $("#btnSaveButtom").remove();
    }
}

function viewOnlyAfterExpandSkills($trSkill) {
    if (isViewOnlyAssessment()) {
        $trSkill.find(".selCurrentState").attr("disabled", "disabled");

        var $table = $trSkill.next().find("table").first();

        var reminderIndexColumn = $table.find('th:contains("Send Notification")').index();
        $table.find("tr").find("th:eq(" + reminderIndexColumn + "),td:eq(" + reminderIndexColumn + ")").remove();

        var flagNotRequiredIndexColumn = $table.find('th:contains("Not Required")').index();
        $table.find("tr").find("th:eq(" + flagNotRequiredIndexColumn + "),td:eq(" + flagNotRequiredIndexColumn + ")").remove();

        $trSkill.next().find("[name='btnAddAdditionalDevelopment']").remove();
    }
}