﻿//#region DOCUMENT READY
$(document).ready(function () {
    loadTableReturs();
    
});
//#endregion

//#region VARIABLES
var accessAdmin = "INCOME_AND_OTHER_ADMIN";
var accessSuperAdmin = "INCOME_AND_OTHER_SUPER_ADMIN";
var accessPreparer = "INCOME_AND_OTHER_DEFAULT_USER";
var accessReviewer = "INCOME_AND_OTHER_MANAGER";
var userAccess = _getViewVar("UserInfo").Roles.replace(",", "");
var dateInput, dateinputsent, dateinputcopy, dateInputSubmittedCorpTaxSP, checkInputSentLEM, checkInputReceiveLEM, checkInputSubmittedCorpTax;
var calendarType = 'TrustReturnPackage';



function returnButtons(row) {
var listButtons = [
    {
    name: "Return to Preparer",
    class: "btn-warning",
    onClick: function ($modal) {


        var soeid = _getSOEID();
        var preparerSoeid = row.statusPreparedSOEID;

        if (soeid.toUpperCase() != preparerSoeid.toUpperCase()) {
            if ($("#txtNewComments").val() != "") {
                _callServer({
                    url: '/IncomeAndOther/ReviewerReturnToPreparer',
                    data: {
                        'pjson': JSON.stringify(row), 'comments': $("#txtNewComments").val()
                    },
                    type: "post",
                    success: function (savingStatus) {
                        _showNotification("success", "Data was saved successfully.");
                        reloadTablesBySelectedCalendar();
                    }
                });
            } else {
                _showNotification("error", "Please add a comment.");
            }
        } else {
            _showNotification("error", "You prepare this filing.");
        }
    }
}, 
    {
        name: "Delete",
        class: "btn-danger",
        onClick: function ($modal) {
            if (userAccess == accessReviewer) {
                if ($("#txtNewComments").val().length > 20) {
                    _callServer({
                        url: '/IncomeAndOther/ReviewerDeleteRP',
                        data: { 'pjson': JSON.stringify(row), 'comments': $("#txtNewComments").val() },
                        type: "post",
                        success: function (savingStatus) {
                            _showNotification("success", "Data was saved successfully.");
                            reloadTablesBySelectedCalendar();
                        }
                    });
                } else {
                    _showNotification("error", "Please enter a comment");
                }
            } else {
                _showNotification("error", "Only reviewers can delete filings");
            }

            //alert($modal.find("#txtlogID").val());
        }
    },
    {
    name: "Approve",
    class: "btn-success",
    onClick: function ($modal) {
        validateDatesTrust();
        if (userAccess == accessReviewer || userAccess == accessSuperAdmin) {
            if (row.statusPreparedSOEID.toUpperCase() != _getSOEID().toUpperCase()) {

                _callServer({
                    url: '/IncomeAndOther/ReviewerSaveRP',
                    data: {
                        'pjson': JSON.stringify(row), 'comments': $("#txtNewComments").val(), 'accounts': $("#spinner").val(), 'calendarType': calendarType, 'duedate': $("#txtfilingDueDate").val(),
                        'dateInput': dateInput, 'dateIputSent': dateinputsent, 'dateInputCopy': dateinputcopy,
                        'dateInputSubmittedCorpTaxSP': dateInputSubmittedCorpTaxSP,
                        'checkInputSentLEM': checkInputSentLEM, 'checkInputReceiveLEM': checkInputReceiveLEM, 'checkInputSubmittedCorpTax': checkInputSubmittedCorpTax
                    },
                    type: "post",
                    success: function (savingStatus) {
                        _showNotification("success", "Data was saved successfully.");
                        reloadTablesBySelectedCalendar();
                    }
                });
            } else {
                _showNotification("error", "You cannot review something you prepared");
            }

        } else {
            _showNotification("error", "You don't have access to review filings");
        }
    }
},
    {
            name: "Save Changes",
            class: "btn-info",
            onClick: function ($modal) {
                validateDatesTrust();
                _callServer({
                    url: '/IncomeAndOther/ReviewerEditRP',
                    data: {
                        'pjson': JSON.stringify(row), 'comments': $("#txtNewComments").val(), 'accounts': $("#spinner").val(), 'calendarType': calendarType, 'duedate': $("#txtfilingDueDate").val(),
                        'dateInput': dateInput, 'dateIputSent': dateinputsent, 'dateInputCopy': dateinputcopy,
                        'dateInputSubmittedCorpTaxSP': dateInputSubmittedCorpTaxSP,
                        'checkInputSentLEM': checkInputSentLEM, 'checkInputReceiveLEM': checkInputReceiveLEM, 'checkInputSubmittedCorpTax': checkInputSubmittedCorpTax
                    },
                    type: "post",
                    success: function (savingStatus) {
                        _showNotification("success", "Data was saved successfully.");
                        reloadTablesBySelectedCalendar();
                    }
                });
            }
        }
];

removeButtonsPreparer(listButtons);

return listButtons;
}
//#endregion

function removeButtonsPreparer(listButtons) {


    if (userAccess == "INCOME_AND_OTHER_DEFAULT_USER") {
        var searchTerm = "Return to Preparer",
        index = -1;
        for (var i = 0, len = listButtons.length; i < len; i++) {
            if (listButtons[i].name === searchTerm) {
                index = i;
                break;
            }
        }

        listButtons.splice(index, 1);

        var searchTerm1 = "Delete",
        index1 = -1;
        for (var i = 0, len = listButtons.length; i < len; i++) {
            if (listButtons[i].name === searchTerm1) {
                index1 = i;
                break;
            }
        }
        listButtons.splice(index1, 1);


        var searchTerm2 = "Approve",
        index2 = -1;
        for (var i = 0, len = listButtons.length; i < len; i++) {
            if (listButtons[i].name === searchTerm2) {
                index2 = i;
                break;
            }
        }
        listButtons.splice(index2, 1);
    }

}


//#region LOAD ALL TABLES
function loadTableReturs() {
    _showLoadingFullPage({ msg: "Loading Calendar..." });
    reloadTableIncomplete("TrustReturnPackage");
    reloadTablePrepare("TrustReturnPackage");
    reloadTableCompleted("TrustReturnPackage");
};

function reloadTablesBySelectedCalendar() {
    var selectedValue = $("#ddlCalendarType option:selected").val();
    reloadTableIncomplete(selectedValue);
    reloadTablePrepare(selectedValue);
    reloadTableCompleted(selectedValue);
}

$("#ddlCalendarType").change(function () {

    var selectedCalendar = $("#ddlCalendarType option:selected").text();
    $("#headerCalendarType").text(selectedCalendar);
    reloadTablesBySelectedCalendar();



    _showAlert({
        showTo: $('#ddlCalendarType').parent(),
        type: 'info',
        title: "Calendar Changed",
        content: "The calendar was changed succesfully.",
        onReady: function ($alert) { }
    });

});
//#endregion

//#region RELOAD TABLES FUNCTION
function reloadTableIncomplete(calendarType) {
    $.jqxGridApi.create({
        showTo: "#tblCalendarIncompleteRP",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_CalendarListingByMonthYear_RP]",
            Params: [
                { Name: "@year", Value: new Date().getFullYear() },
                { Name: "@month", Value: "1,2,3,4,5,6,7,8,9,10,11,12" },
                { Name: "@statusPrepared", Value: 0 },
                { Name: "@status", Value: 0 },
                { Name: "@calendarType", Value: calendarType }

            ]
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [

            { name: 'logID', type: 'string', width: '5%' },
            { name: 'entityID', type: 'string', hidden: true },
            { name: 'entityName', type: 'string', width: '25%' },
            { name: 'function', type: 'string', width: '15%' },
            { name: 'frequency', type: 'string', width: '10%' },
            { name: 'filingDueDate', text: 'Filing Due Date', type: 'date', width: '15%', cellsformat: "M/d/yyyy" },
            { name: 'date_DueCorpTax', text: 'Tax Corp Due Date', type: 'date', width: '15%', cellsformat: "M/d/yyyy" },
            { name: 'reviewer', type: 'string', width: '10%' },
            { name: 'preparer', type: 'string', width: '10%' },
            { name: 'dateTR_ExtensionSentIRS', type: 'string', hidden: true },
            { name: 'dateTR_SentTrustee', type: 'string', hidden: true },
            { name: 'dateTR_CopyReceiveTrustee', type: 'string', hidden: true },
            { name: 'dateA_SubmittedCorpTaxSP', type: 'string', hidden: true },
            { name: 'dateTP_SentLEM', type: 'string', hidden: true },
            { name: 'dateTP_ReceiveLEM', type: 'string', hidden: true },
            { name: 'dateTP_SubmittedCorpTax', type: 'string', hidden: true },
            { name: 'date_ReturnToPreparer', type: 'string', hidden: true },
            { name: 'filingID', type: 'string', hidden: true },
            { name: 'status', type: 'string', hidden: true },
            { name: 'statusPrepared', type: 'string', hidden: true },
            { name: 'statusDesc', type: 'string', hidden: true },
            { name: 'preparedDdate', text: 'Prepared Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'reviewedDate', text: 'Reviewed Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'commentFlag', type: 'string', hidden: true },
            { name: 'counts', type: 'string', hidden: true },
            { name: 'statusDate', text: 'Status Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'statusSOEID', type: 'string', hidden: true },
            { name: 'statusPreparedDate', text: 'Status Prepared Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'statusPreparedSOEID', type: 'string', hidden: true },
            { name: 'comments', type: 'string', hidden: true },
            { name: 'ttiCode', type: 'string', hidden: true },
            { name: 'EIN', type: 'string', hidden: true },
            { name: 'state', type: 'string', hidden: true },
            { name: 'city', type: 'string', hidden: true },
            { name: 'branch', type: 'string', hidden: true },
            { name: 'dueDay', type: 'string', hidden: true },
            { name: 'dueMonth', type: 'string', hidden: true }
        ],

        ready: function () {
            _hideLoadingFullPage();
            $('#tblCalendarIncompleteRP').on('rowdoubleclick', function (event) {
                // event.args.rowindex is a bound index.
                if (typeof event.args.group == "undefined") {
                    var row = event.args.rowindex;
                    var datarow = $("#tblCalendarIncompleteRP").jqxGrid('getrowdata', row);
                    showIncompleModal(datarow);
                }
            });
        }
    });
}

function reloadTablePrepare(calendarType) {
    $.jqxGridApi.create({
        showTo: "#tblCalendarPreparedRP",
        options: {
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_CalendarListingByMonthYear_RP]",
            Params: [
                { Name: "@year", Value: new Date().getFullYear() },
                { Name: "@month", Value: "1,2,3,4,5,6,7,8,9,10,11,12" },
                { Name: "@statusPrepared", Value: 1 },
                { Name: "@status", Value: 0 },
                { Name: "@calendarType", Value: calendarType }
            ]
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'logID', type: 'string', width: '10%' },
            { name: 'entityID', type: 'string', hidden: true },
            { name: 'entityName', type: 'string', width: '25%' },
            { name: 'function', type: 'string', width: '15%' },
            { name: 'frequency', type: 'string', width: '10%' },
            { name: 'filingDueDate', text: 'Filing Due Date', type: 'date', width: '20%', cellsformat: "M/d/yyyy" },
            { name: 'date_DueCorpTax', text: 'Tax Corp Due Date', type: 'date', width: '15%', cellsformat: "M/d/yyyy" },
            { name: 'reviewer', type: 'string', width: '10%' },
            { name: 'preparer', type: 'string', width: '10%' },
            { name: 'filingID', type: 'string', hidden: true },
            { name: 'dateTR_ExtensionSentIRS', type: 'string', hidden: true },
            { name: 'dateTR_SentTrustee', type: 'string', hidden: true },
            { name: 'dateTR_CopyReceiveTrustee', type: 'string', hidden: true },
            { name: 'dateA_SubmittedCorpTaxSP', type: 'string', hidden: true },
            { name: 'dateTP_SentLEM', type: 'string', hidden: true },
            { name: 'dateTP_ReceiveLEM', type: 'string', hidden: true },
            { name: 'dateTP_SubmittedCorpTax', type: 'string', hidden: true },
            { name: 'date_ReturnToPreparer', type: 'string', hidden: true },
            { name: 'status', type: 'string', hidden: true },
            { name: 'statusPrepared', type: 'string', hidden: true },
            { name: 'statusDesc', type: 'string', hidden: true },
            { name: 'preparedDdate', text: 'Prepared Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'reviewedDate', text: 'Reviewed Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'commentFlag', type: 'string', hidden: true },
            { name: 'counts', type: 'string', hidden: true },
            { name: 'statusDate', text: 'Status Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'statusSOEID', type: 'string', hidden: true },
            { name: 'statusPreparedDate', text: 'Status Prepared Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'statusPreparedSOEID', type: 'string', hidden: true },
            { name: 'comments', type: 'string', hidden: true },
            { name: 'ttiCode', type: 'string', hidden: true },
            { name: 'EIN', type: 'string', hidden: true },
            { name: 'state', type: 'string', hidden: true },
            { name: 'city', type: 'string', hidden: true },
            { name: 'branch', type: 'string', hidden: true },
            { name: 'dueDay', type: 'string', hidden: true },
            { name: 'dueMonth', type: 'string', hidden: true }
        ],
        ready: function () {
            _hideLoadingFullPage();
            $('#tblCalendarPreparedRP').on('rowdoubleclick', function (event) {
                // event.args.rowindex is a bound index.
                if (typeof event.args.group == "undefined") {
                    var row = event.args.rowindex;
                    var datarow = $("#tblCalendarPreparedRP").jqxGrid('getrowdata', row);
                    showPrepareModal(datarow);
                }
            });
        }
    });
}

function reloadTableCompleted(calendarType) {
    $.jqxGridApi.create({
        showTo: "#tblCalendarCompleteRP",
        options: {
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_CalendarListingByMonthYear_RP]",
            Params: [
                { Name: "@year", Value: new Date().getFullYear() },
                { Name: "@month", Value: "1,2,3,4,5,6,7,8,9,10,11,12" },
                { Name: "@statusPrepared", Value: 1 },
                { Name: "@status", Value: 1 },
                { Name: "@calendarType", Value: calendarType }
            ]
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'logID', type: 'string', width: '10%' },
            { name: 'entityID', type: 'string', hidden: true },
            { name: 'entityName', type: 'string', width: '25%' },
            { name: 'function', type: 'string', width: '15%' },
            { name: 'frequency', type: 'string', width: '10%' },
            { name: 'filingDueDate', text: 'Filing Due Date', type: 'date', width: '20%', cellsformat: "M/d/yyyy" },
            { name: 'date_DueCorpTax', text: 'Tax Corp Due Date', type: 'date', width: '15%', cellsformat: "M/d/yyyy" },
            { name: 'reviewer', type: 'string', width: '10%' },
            { name: 'preparer', type: 'string', width: '10%' },
            { name: 'filingID', type: 'string', hidden: true },
            { name: 'dateTR_ExtensionSentIRS', type: 'string', hidden: true },
            { name: 'dateTR_SentTrustee', type: 'string', hidden: true },
            { name: 'dateTR_CopyReceiveTrustee', type: 'string', hidden: true },
            { name: 'dateA_SubmittedCorpTaxSP', type: 'string', hidden: true },
            { name: 'dateTP_SentLEM', type: 'string', hidden: true },
            { name: 'dateTP_ReceiveLEM', type: 'string', hidden: true },
            { name: 'dateTP_SubmittedCorpTax', type: 'string', hidden: true },
            { name: 'date_ReturnToPreparer', type: 'string', hidden: true },
            { name: 'status', type: 'string', hidden: true },
            { name: 'statusPrepared', type: 'string', hidden: true },
            { name: 'statusDesc', type: 'string', hidden: true },
            { name: 'preparedDdate', text: 'Prepared Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'reviewedDate', text: 'Reviewed Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'commentFlag', type: 'string', hidden: true },
            { name: 'counts', type: 'string', hidden: true },
            { name: 'statusDate', text: 'Status Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'statusSOEID', type: 'string', hidden: true },
            { name: 'statusPreparedDate', text: 'Status Prepared Date', type: 'date', cellsformat: "M/d/yyyy", hidden: true },
            { name: 'statusPreparedSOEID', type: 'string', hidden: true },
            { name: 'comments', type: 'string', hidden: true },
            { name: 'ttiCode', type: 'string', hidden: true },
            { name: 'EIN', type: 'string', hidden: true },
            { name: 'state', type: 'string', hidden: true },
            { name: 'city', type: 'string', hidden: true },
            { name: 'branch', type: 'string', hidden: true },
            { name: 'dueDay', type: 'string', hidden: true },
            { name: 'dueMonth', type: 'string', hidden: true }
        ],
        ready: function () {
            _hideLoadingFullPage()
            $('#tblCalendarCompleteRP').on('rowdoubleclick', function (event) {
                if (typeof event.args.group == "undefined") {
                    // event.args.rowindex is a bound index.
                    var row = event.args.rowindex;
                    var datarow = $("#tblCalendarCompleteRP").jqxGrid('getrowdata', row);
                    showCompleteModal(datarow);
                }
            });
        }
    });
}
//#endregion

//#region SHOW MODALS
function showIncompleModal(row) {
    _showModal({
        title: "<h3>Return Package Filing Calendar - Log ID:" + row.logID + " - Entity: " + row.entityName + "</h3>",
        width: "98%",
        contentAjaxUrl: "/IncomeAndOther/ModalTemplateRP",
        contentAjaxParams: {
            logID: row.logID,
            entityID: row.entityID,
            entityName: row.entityName,
            function1: row.function,
            frequency: row.frequency,
            reviewer: row.reviewer,
            preparer: row.preparer,
            filingDueDate: _formatDate(new Date(row.filingDueDate), 'MM/dd/yyyy'),
            date_DueCorpTax: _formatDate(new Date(row.date_DueCorpTax), 'MM/dd/yyyy'),
            filingID: row.filingID,
            status: row.status,
            statusPrepared: row.statusPrepared,
            statusDesc: row.statusDesc,
            preparedDdate: _formatDate(new Date(row.preparedDdate), 'MM/dd/yyyy'),
            reviewedDate: _formatDate(new Date(row.reviewedDate), 'MM/dd/yyyy'),
            commentFlag: row.commentFlag,
            counts: row.counts,
            statusDate: _formatDate(new Date(row.statusDate), 'MM/dd/yyyy'),
            statusSOEID: row.statusSOEID,
            statusPreparedDate: _formatDate(new Date(row.statusPreparedDate), 'MM/dd/yyyy'),
            statusPreparedSOEID: row.statusPreparedSOEID,
            comments: row.comments,
            ttiCode: row.ttiCode,
            EIN: row.EIN,
            state: row.state,
            city: row.city,
            branch: row.branch,
            dueDay: row.dueDay,
            dueMonth: row.dueMonth
        },
        buttons: [{
            name: "Delete",
            class: "btn-danger",
            onClick: function ($modal) {
                if (userAccess == accessReviewer) {
                    if ($("#txtNewComments").val().length > 20) {
                        _callServer({
                            url: '/IncomeAndOther/PreparerDeleteRP',
                            data: { 'pjson': JSON.stringify(row), 'comments': $("#txtNewComments").val() },
                            type: "post",
                            success: function (savingStatus) {
                                _showNotification("success", "Data was saved successfully.");
                                reloadTablesBySelectedCalendar();
                            }
                        });
                    } else {
                        _showNotification("error", "Please enter a comment");
                    }
                } else {
                    _showNotification("error", "Only reviewers can delete filings");
                }

            }
        },
        {
            name: "Prepare",
            class: "btn-success",
            onClick: function ($modal) {
                validateDatesTrust();
                if (userAccess == accessPreparer || userAccess == accessReviewer || userAccess == accessSuperAdmin) {
                    _callServer({
                        url: '/IncomeAndOther/PreparerSaveRP',
                        data: {
                            'pjson': JSON.stringify(row), 'comments': $("#txtNewComments").val(), 'calendarType': calendarType,
                            'dateInput': dateInput, 'dateIputSent': dateinputsent, 'dateInputCopy': dateinputcopy,
                            'dateInputSubmittedCorpTaxSP': dateInputSubmittedCorpTaxSP,
                            'checkInputSentLEM': checkInputSentLEM, 'checkInputReceiveLEM': checkInputReceiveLEM, 'checkInputSubmittedCorpTax': checkInputSubmittedCorpTax

                        },

                        type: "post",
                        success: function (savingStatus) {
                            _showNotification("success", "Data was saved successfully.");
                            reloadTablesBySelectedCalendar();

                        }
                    });
                } else {
                    _showNotification("error", "You don't have access to prepare.");
                }
            }
        },

        {
            name: "Save Changes",
            class: "btn-info",
            onClick: function ($modal) {
                validateDatesTrust();
                _callServer({
                    url: '/IncomeAndOther/PreparerEditRP',
                    data: {
                        'pjson': JSON.stringify(row), 'comments': $("#txtNewComments").val(), 'accounts': $("#spinner").val(), 'calendarType': calendarType,
                        'dateInput': dateInput, 'dateIputSent': dateinputsent, 'dateInputCopy': dateinputcopy,
                        'dateInputSubmittedCorpTaxSP': dateInputSubmittedCorpTaxSP,
                        'checkInputSentLEM': checkInputSentLEM, 'checkInputReceiveLEM': checkInputReceiveLEM, 'checkInputSubmittedCorpTax': checkInputSubmittedCorpTax
                    },
                    type: "post",
                    success: function (savingStatus) {
                        _showNotification("success", "Data was saved successfully.");
                        reloadTablesBySelectedCalendar();
                    }
                });
            }
        }],
        onReady: function ($modal) {
            loadDropDownGlobalProcess(row.function);
            $modal.find("#txtfilingDueDate").prop('disabled', true);
            $modal.find("#txttaxCorpDueDate").prop('disabled', true);

            $modal.find("#spinner").spinner({
                min: 0,
                max: 500,
                step: 1,
                start: 1000,
                numberFormat: "C"
            });

            //#region INITIALIZE DATETIME INPUT

            var selectedCalendar = $("#ddlCalendarType option:selected").val();

            //--ALL
            switch (selectedCalendar) {
                case "TrustReturnPackage":
                    //--TRUST RETURNS
                    $modal.find("#divAppointment").hide();
                    $modal.find("#divTaxPackage").hide();

                    $modal.find("#dateInputExtensionSentIRS").datepicker();
                    $modal.find("#dateInputSentTrustee").datepicker();
                    $modal.find("#dateInputCopyReceiveTrustee").datepicker();
                    $modal.find("#dateInputReturnToPreparer").datepicker();

                    $modal.find("#dateInputExtensionSentIRS").val(validateNull(_formatDate(new Date(row.dateTR_ExtensionSentIRS), 'MM/dd/yyyy')));
                    $modal.find("#dateInputSentTrustee").val(validateNull(_formatDate(new Date(row.dateTR_SentTrustee), 'MM/dd/yyyy')));
                    $modal.find("#dateInputCopyReceiveTrustee").val(validateNull(_formatDate(new Date(row.dateTR_CopyReceiveTrustee), 'MM/dd/yyyy')));
                    $modal.find("#dateInputReturnToPreparer").val(validateNull(_formatDate(new Date(row.date_ReturnToPreparer), 'MM/dd/yyyy')));

                    break;
                case "AppointmentsPackage":
                    //--APPOINTMENTS
                    $modal.find("#divTrustReturn").hide();
                    $modal.find("#divTaxPackage").hide();

                    $modal.find("#dateInputSubmittedCorpTaxSP").datepicker();
                    $modal.find("#dateInputReturnToPreparer").datepicker();

                    $modal.find("#dateInputSubmittedCorpTaxSP").val(_formatDate(new Date(row.dateA_SubmittedCorpTaxSP), 'MM/dd/yyyy'));
                    $modal.find("#dateInputReturnToPreparer").val(validateNull(_formatDate(new Date(row.date_ReturnToPreparer), 'MM/dd/yyyy')));

                    break;
                case "TaxPackage":
                    //--TAX PACKAGES
                    $modal.find("#divTrustReturn").hide();
                    $modal.find("#divAppointment").hide();

                    $modal.find("#dateInputSentLEM").datepicker();
                    $modal.find("#dateInputReceiveLEM").datepicker();
                    $modal.find("#dateInputSubmittedCorpTax").datepicker();
                    $modal.find("#dateInputReturnToPreparer").datepicker();


                    $modal.find("#dateInputSentLEM").val(validateNull(_formatDate(new Date(row.dateTP_SentLEM), 'MM/dd/yyyy')));
                    $modal.find("#dateInputReceiveLEM").val(validateNull(_formatDate(new Date(row.dateTP_ReceiveLEM), 'MM/dd/yyyy')));
                    $modal.find("#dateInputSubmittedCorpTax").val(validateNull(_formatDate(new Date(row.dateTP_SubmittedCorpTax), 'MM/dd/yyyy')));
                    $modal.find("#dateInputReturnToPreparer").val(validateNull(_formatDate(new Date(row.date_ReturnToPreparer), 'MM/dd/yyyy')));

                    break;
            }
            //#endregion

            _GLOBAL_SETTINGS.iCheck();
        }
    });


}
function showPrepareModal(row) {
    _showModal({
        title: "<h3>Return Package Filing Calendar - Log ID:" + row.logID + " - Entity: " + row.entityName + "</h3>",
        width: "98%",
        contentAjaxUrl: "/IncomeAndOther/ModalTemplateRP",
        contentAjaxParams: {
            logID: row.logID,
            entityID: row.entityID,
            entityName: row.entityName,
            function1: row.function,
            frequency: row.frequency,
            reviewer: row.reviewer,
            preparer: row.preparer,
            filingDueDate: _formatDate(new Date(row.filingDueDate), 'MM/dd/yyyy'),
            date_DueCorpTax: _formatDate(new Date(row.date_DueCorpTax), 'MM/dd/yyyy'),
            filingID: row.filingID,
            status: row.status,
            statusPrepared: row.statusPrepared,
            statusDesc: row.statusDesc,
            preparedDdate: _formatDate(new Date(row.preparedDdate), 'MM/dd/yyyy'),
            reviewedDate: _formatDate(new Date(row.reviewedDate), 'MM/dd/yyyy'),
            commentFlag: row.commentFlag,
            counts: row.counts,
            statusDate: _formatDate(new Date(row.statusDate), 'MM/dd/yyyy'),
            statusSOEID: row.statusSOEID,
            statusPreparedDate: _formatDate(new Date(row.statusPreparedDate), 'MM/dd/yyyy'),
            statusPreparedSOEID: row.statusPreparedSOEID,
            comments: row.comments,
            ttiCode: row.ttiCode,
            EIN: row.EIN,
            state: row.state,
            city: row.city,
            branch: row.branch,
            dueDay: row.dueDay,
            dueMonth: row.dueMonth
        },
        buttons: returnButtons(row),
        onReady: function ($modal) {
            loadDropDownGlobalProcess(row.function);

            $modal.find("#spinner").spinner({
                min: 0,
                max: 500,
                step: 1,
                start: 1000,
                numberFormat: "C"
            });

            //#region INITIALIZE DATETIME INPUT

            var selectedCalendar = $("#ddlCalendarType option:selected").val();

            //--ALL
            switch (selectedCalendar) {
                case "TrustReturnPackage":
                    //--TRUST RETURNS
                    $modal.find("#divAppointment").hide();
                    $modal.find("#divTaxPackage").hide();
                    $modal.find("#dateInputExtensionSentIRS").datepicker();
                    $modal.find("#dateInputSentTrustee").datepicker();
                    $modal.find("#dateInputCopyReceiveTrustee").datepicker();
                    $modal.find("#dateInputReturnToPreparer").datepicker();

                    $modal.find("#dateInputExtensionSentIRS").val(validateNull(_formatDate(new Date(row.dateTR_ExtensionSentIRS), 'MM/dd/yyyy')));
                    $modal.find("#dateInputSentTrustee").val(validateNull(_formatDate(new Date(row.dateTR_SentTrustee), 'MM/dd/yyyy')));
                    $modal.find("#dateInputCopyReceiveTrustee").val(validateNull(_formatDate(new Date(row.dateTR_CopyReceiveTrustee), 'MM/dd/yyyy')));
                    $modal.find("#dateInputReturnToPreparer").val(validateNull(_formatDate(new Date(row.date_ReturnToPreparer), 'MM/dd/yyyy')));

                    break;
                case "AppointmentsPackage":
                    //--APPOINTMENTS
                    $modal.find("#divTrustReturn").hide();
                    $modal.find("#divTaxPackage").hide();
                    $modal.find("#dateInputSubmittedCorpTaxSP").datepicker();
                    $modal.find("#dateInputReturnToPreparer").datepicker();

                    $modal.find("#dateInputSubmittedCorpTaxSP").val(validateNull(_formatDate(new Date(row.dateA_SubmittedCorpTaxSP), 'MM/dd/yyyy')));
                    $modal.find("#dateInputReturnToPreparer").val(validateNull(_formatDate(new Date(row.date_ReturnToPreparer), 'MM/dd/yyyy')));

                    break;
                case "TaxPackage":
                    //--TAX PACKAGES
                    $modal.find("#divTrustReturn").hide();
                    $modal.find("#divAppointment").hide();
                    $modal.find("#dateInputSentLEM").datepicker();
                    $modal.find("#dateInputReceiveLEM").datepicker();
                    $modal.find("#dateInputSubmittedCorpTax").datepicker();
                    $modal.find("#dateInputReturnToPreparer").datepicker();


                    $modal.find("#dateInputSentLEM").val(validateNull(_formatDate(new Date(row.dateTP_SentLEM), 'MM/dd/yyyy')));
                    $modal.find("#dateInputReceiveLEM").val(validateNull(_formatDate(new Date(row.dateTP_ReceiveLEM), 'MM/dd/yyyy')));
                    $modal.find("#dateInputSubmittedCorpTax").val(validateNull(_formatDate(new Date(row.dateTP_SubmittedCorpTax), 'MM/dd/yyyy')));
                    $modal.find("#dateInputReturnToPreparer").val(validateNull(_formatDate(new Date(row.date_ReturnToPreparer), 'MM/dd/yyyy')));

                    break;
            }
            //#endregion

            _GLOBAL_SETTINGS.iCheck();
        }
    });
}
function showCompleteModal(row) {
    _showModal({
        title: "<h3>Return Package Filing Calendar - Log ID:" + row.logID + " - Entity: " + row.entityName + "</h3>",
        width: "98%",
        contentAjaxUrl: "/IncomeAndOther/ModalTemplateRP",
        contentAjaxParams: {
            logID: row.logID,
            entityID: row.entityID,
            entityName: row.entityName,
            function1: row.function,
            frequency: row.frequency,
            reviewer: row.reviewer,
            preparer: row.preparer,
            filingDueDate: _formatDate(new Date(row.filingDueDate), 'MM/dd/yyyy'),
            date_DueCorpTax: _formatDate(new Date(row.date_DueCorpTax), 'MM/dd/yyyy'),
            filingID: row.filingID,
            status: row.status,
            statusPrepared: row.statusPrepared,
            statusDesc: row.statusDesc,
            preparedDdate: _formatDate(new Date(row.preparedDdate), 'MM/dd/yyyy'),
            reviewedDate: _formatDate(new Date(row.reviewedDate), 'MM/dd/yyyy'),
            commentFlag: row.commentFlag,
            counts: row.counts,
            statusDate: _formatDate(new Date(row.statusDate), 'MM/dd/yyyy'),
            statusSOEID: row.statusSOEID,
            statusPreparedDate: _formatDate(new Date(row.statusPreparedDate), 'MM/dd/yyyy'),
            statusPreparedSOEID: row.statusPreparedSOEID,
            comments: row.comments,
            ttiCode: row.ttiCode,
            EIN: row.EIN,
            state: row.state,
            city: row.city,
            branch: row.branch,
            dueDay: row.dueDay,
            dueMonth: row.dueMonth
        },
        buttons: [{
            name: "Save Changes",
            class: "btn-info",
            onClick: function ($modal) {
                _callServer({
                    url: '/IncomeAndOther/CompletedEditRP',
                    data: { 'pjson': JSON.stringify(row), 'comments': $("#txtNewComments").val() },
                    type: "post",
                    success: function (savingStatus) {
                        _showNotification("success", "Data was saved successfully.");
                        reloadTablesBySelectedCalendar();
                    }
                });
            }
        }],
        onReady: function ($modal) {
            loadDropDownGlobalProcess(row.function);
            $modal.find("#txtfilingDueDate").prop('disabled', true);
            $modal.find("#txttaxCorpDueDate").prop('disabled', true);
            $modal.find("#txtaccounts").prop('disabled', true);

            $modal.find("#spinner").spinner({
                min: 0,
                max: 500,
                step: 1,
                start: 1000,
                numberFormat: "C"
            });

            //#region INITIALIZE DATETIME INPUT

            var selectedCalendar = $("#ddlCalendarType option:selected").val();

            //--ALL
            switch (selectedCalendar) {
                case "TrustReturnPackage":
                    //--TRUST RETURNS
                    $modal.find("#divAppointment").hide();
                    $modal.find("#divTaxPackage").hide();
                    $modal.find("#dateInputExtensionSentIRS").datepicker();
                    $modal.find("#dateInputSentTrustee").datepicker();
                    $modal.find("#dateInputCopyReceiveTrustee").datepicker();
                    $modal.find("#dateInputReturnToPreparer").datepicker();

                    $modal.find("#dateInputExtensionSentIRS").val(validateNull(_formatDate(new Date(row.dateTR_ExtensionSentIRS), 'MM/dd/yyyy')));
                    $modal.find("#dateInputSentTrustee").val(validateNull(_formatDate(new Date(row.dateTR_SentTrustee), 'MM/dd/yyyy')));
                    $modal.find("#dateInputCopyReceiveTrustee").val(validateNull(_formatDate(new Date(row.dateTR_CopyReceiveTrustee), 'MM/dd/yyyy')));
                    $modal.find("#dateInputReturnToPreparer").val(validateNull(_formatDate(new Date(row.date_ReturnToPreparer), 'MM/dd/yyyy')));

                    break;
                case "AppointmentsPackage":
                    //--APPOINTMENTS
                    $modal.find("#divTrustReturn").hide();
                    $modal.find("#divTaxPackage").hide();
                    $modal.find("#dateInputSubmittedCorpTaxSP").datepicker();
                    $modal.find("#dateInputReturnToPreparer").datepicker();

                    $modal.find("#dateInputSubmittedCorpTaxSP").val(_formatDate(new Date(row.dateA_SubmittedCorpTaxSP), 'MM/dd/yyyy'));
                    $modal.find("#dateInputReturnToPreparer").val(validateNull(_formatDate(new Date(row.date_ReturnToPreparer), 'MM/dd/yyyy')));

                    break;
                case "TaxPackage":
                    //--TAX PACKAGES
                    $modal.find("#divTrustReturn").hide();
                    $modal.find("#divAppointment").hide();
                    $modal.find("#dateInputSentLEM").datepicker();
                    $modal.find("#dateInputReceiveLEM").datepicker();
                    $modal.find("#dateInputSubmittedCorpTax").datepicker();
                    $modal.find("#dateInputReturnToPreparer").datepicker();


                    $modal.find("#dateInputSentLEM").val(validateNull(_formatDate(new Date(row.dateTP_SentLEM), 'MM/dd/yyyy')));
                    $modal.find("#dateInputReceiveLEM").val(validateNull(_formatDate(new Date(row.dateTP_ReceiveLEM), 'MM/dd/yyyy')));
                    $modal.find("#dateInputSubmittedCorpTax").val(validateNull(_formatDate(new Date(row.dateTP_SubmittedCorpTax), 'MM/dd/yyyy')));
                    $modal.find("#dateInputReturnToPreparer").val(validateNull(_formatDate(new Date(row.date_ReturnToPreparer), 'MM/dd/yyyy')));

                    break;
            }
            //#endregion

            _GLOBAL_SETTINGS.iCheck();
        }
    });
}


//#endregion

//#region Show loading message
function loadTableComplete() {
    _showLoadingFullPage({ msg: "Loading Calendar..." });
};
//#endregion Show loading message

//#region Populate function type droplist
function loadDropDownGlobalProcess(selectedFunction) {
    _callServer({
        loadingMsg: "Loading Global Process...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT '0' AS [funcID], '-- Select --' AS [function] UNION SELECT REPLACE(CAST([funcID] AS VARCHAR(36)),'-','') as [funcID],[function] FROM [IncomeAndOtherQD].[dbo].[FunctionType] where active = 1 ORDER BY [function] ASC") },
        type: "post",
        success: function (resultList) {
            $("#ddlFunction").contents().remove();

            //Render Global Process Options
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $("#ddlFunction").append($('<option>', { value: objFunction.funcID, text: objFunction.function, selected: (objFunction.function.toLowerCase() == selectedFunction.toLowerCase()) }));
            }
        }
    });
};
//#endregion Populate function type droplist

//#region Validate Dates
function validateDatesTrust() {
    //GET AND VALIDATE DATES

    var finalDate;
    var today = new Date();
    var dd = today.getDate();

    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }
        
    today = mm + '/' + dd + '/' + yyyy;
       
    finalDate = today;


    calendarType = $("#ddlCalendarType option:selected").val();

    if (calendarType == "TrustReturnPackage") {
        dateInput = _formatDate(new Date($("#dateInputExtensionSentIRS").val()), 'MM/dd/yyyy');
        dateinputsent = _formatDate(new Date($("#dateInputSentTrustee").val()), 'MM/dd/yyyy');
        dateinputcopy = _formatDate(new Date($("#dateInputCopyReceiveTrustee").val()), 'MM/dd/yyyy');
        dateInputSubmittedCorpTaxSP = "0";
        checkInputSentLEM = "0";
        checkInputReceiveLEM = "0";
        checkInputSubmittedCorpTax = "0";

        if ($("#checkInputExtensionSentIRS").is(":checked") == true && dateInput != "NaN/NaN/NaN") {
            dateInput = $.jqx.dataFormat.formatdate(dateInput, 'd');
        } else if ($("#checkInputExtensionSentIRS").is(":checked") == true && dateInput == "NaN/NaN/NaN") {
            dateInput = finalDate;
        }
        else { dateInput = "0"; }

        if ($("#checkInputSentTrustee").is(":checked") == true && dateinputsent != "NaN/NaN/NaN") {
            dateinputsent = $.jqx.dataFormat.formatdate(dateinputsent, 'd');
        } else if ($("#checkInputSentTrustee").is(":checked") == true && dateinputsent == "NaN/NaN/NaN") {
            dateinputsent = finalDate;
        }
        else { dateinputsent = "0"; }

        if ($("#checkInputCopyReceiveTrustee").is(":checked") == true && dateinputcopy != "NaN/NaN/NaN") {
            dateinputcopy = $.jqx.dataFormat.formatdate(dateinputcopy, 'd');
        } else if ($("#checkInputCopyReceiveTrustee").is(":checked") == true && dateinputcopy == "NaN/NaN/NaN") {
            dateinputcopy = finalDate;
        } else { dateinputcopy = "0"; }

    }
    if (calendarType == "AppointmentsPackage") {
        dateInput = "0";
        dateinputsent = "0";
        dateinputcopy = "0";
        dateInputSubmittedCorpTaxSP = _formatDate(new Date($("#dateInputSubmittedCorpTaxSP").val()), 'MM/dd/yyyy');
        checkInputSentLEM = "0";
        checkInputReceiveLEM = "0";
        checkInputSubmittedCorpTax = "0";

        if ($("#checkInputSubmittedCorpTaxSP").is(":checked") == true && dateInputSubmittedCorpTaxSP != "NaN/NaN/NaN") {
            dateInputSubmittedCorpTaxSP = $.jqx.dataFormat.formatdate(dateInputSubmittedCorpTaxSP, 'd');
        } else if ($("#checkInputSubmittedCorpTaxSP").is(":checked") == true && dateInputSubmittedCorpTaxSP == "NaN/NaN/NaN") {
            dateInputSubmittedCorpTaxSP = finalDate;
        } else { dateInputSubmittedCorpTaxSP = "0"; }

    }
    if (calendarType == "TaxPackage") {
        dateInput = "0";;
        dateinputsent = "0";
        dateinputcopy = "0";
        dateInputSubmittedCorpTaxSP = "0";
        checkInputSentLEM = _formatDate(new Date($("#dateInputSentLEM").val()), 'MM/dd/yyyy');
        checkInputReceiveLEM = _formatDate(new Date($("#dateInputReceiveLEM").val()), 'MM/dd/yyyy');
        checkInputSubmittedCorpTax = _formatDate(new Date($("#dateInputSubmittedCorpTax").val()), 'MM/dd/yyyy');

        if ($("#checkInputSentLEM").is(":checked") == true && checkInputSentLEM != "NaN/NaN/NaN") {
            checkInputSentLEM = $.jqx.dataFormat.formatdate(checkInputSentLEM, 'd');
        } else if ($("#checkInputSentLEM").is(":checked") == true && checkInputSentLEM == "NaN/NaN/NaN") {
            checkInputSentLEM = finalDate;
        } else { checkInputSentLEM = "0"; }

        if ($("#checkInputReceiveLEM").is(":checked") == true && checkInputReceiveLEM != "NaN/NaN/NaN") {
            checkInputReceiveLEM = $.jqx.dataFormat.formatdate(checkInputReceiveLEM, 'd');
        } else if ($("#checkInputReceiveLEM").is(":checked") == true && checkInputReceiveLEM == "NaN/NaN/NaN") {
            checkInputReceiveLEM = finalDate;
        } else { checkInputReceiveLEM = "0"; }

        if ($("#checkInputSubmittedCorpTax").is(":checked") == true && checkInputSubmittedCorpTax != "NaN/NaN/NaN") {
            checkInputSubmittedCorpTax = $.jqx.dataFormat.formatdate(checkInputSubmittedCorpTax, 'd');
        } else if ($("#checkInputSubmittedCorpTax").is(":checked") == true && checkInputSubmittedCorpTax == "NaN/NaN/NaN") {
            checkInputSubmittedCorpTax = finalDate;
        } else { checkInputSubmittedCorpTax = "0"; }

    }
}

function validateNull(data) {
    if (data == "") { return ""; } else { return data; }
}
//#endregion


