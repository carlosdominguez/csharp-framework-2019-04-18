﻿$(document).ready(function () {

    $('#fine-uploader-manual-trigger').fineUploader({
        template: 'qq-template-manual-trigger',
        request: {
            endpoint:  _getViewVar("SubAppPath") + '/IQueryMetrics/UploadReport1'
        },
        thumbnails: {
            placeholders: {
                waitingPath: _getViewVar("SubAppPath") + '/Content/Shared/plugins/fine-uploader/placeholders/waiting-generic.png',
                notAvailablePath: _getViewVar("SubAppPath") + '/Content/Shared/plugins/fine-uploader/placeholders/not_available-generic.png'
            }
        },
        callbacks: {
            onAllComplete: function (succeeded, failed) {
                if (failed.length == 0) {
                    _showAlert({
                        type: "success",
                        title: "Message",
                        content: "The file was uploaded successfully"
                    });
                }
            },
            onError: function (id, name, errorReason, xhrOrXdr) {
                if (name) {
                    _showDetailAlert({
                        title: "Message",
                        shortMsg: "An error ocurred with the file '" + name + "'.",
                        longMsg: errorReason,
                        type: "Error",
                        viewLabel: "View Details"
                    });
                }
            },
            //onSubmit: function (id, name) {
            //    $('#fine-uploader-manual-trigger').fineUploader('setParams', { 'typeUpload': $('[name^="iCheck-TypeUpload"]:checked').val() });
            //},
            //onManualRetry: function (id, name) {
            //    $('#fine-uploader-manual-trigger').fineUploader('setParams', { 'typeUpload': $('[name^="iCheck-TypeUpload"]:checked').val() });
            //}
        },
        autoUpload: false
    });

    //On click upload files
    $('#trigger-upload').click(function () {
        //if ($('[name^="iCheck-TypeUpload"]:checked').length > 0) {
        $('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
        //} else {
        //    _showAlert({
        //        content: "Please select the type of the file <b>'GLMS Transcript'</b> or <b>'Udemy Training Catalog'</b>."
        //    });
        //}
    });
});