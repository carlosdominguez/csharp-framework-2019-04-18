/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="SupportDocGlobal.js" />

$(document).ready(function () {
    var currentDate = moment();
    var lastMonth = moment(currentDate).add(-1, 'M');

    $("#txtStartDate").val(lastMonth.format('MMM DD, YYYY'));
    $("#txtStartDate").datepicker();
    $("#txtStartDate").datepicker().on('changeDate', function (ev) {
        //Reload table
        loadTableAuditLog();
    });

    $("#txtEndDate").val(currentDate.format('MMM DD, YYYY'));
    $("#txtEndDate").datepicker();
    $("#txtEndDate").datepicker().on('changeDate', function (ev) {
        //Reload table
        loadTableAuditLog();
    });

    //On Click Download Audit Report
    $(".btnExportToExcel").click(function () {
        downloadToExcelAudit();
    });

    //Load table
    loadTableAuditLog();
});

function getStartDate() {
    var startDate = $("#txtStartDate").val();
    var momentStartDate = moment(startDate, 'MMM DD, YYYY');

    return momentStartDate.format("YYYY-MM-DD");
}

function getEndDate() {
    var endDate = $("#txtEndDate").val();
    var momentEndDate = moment(endDate, 'MMM DD, YYYY');

    return momentEndDate.format("YYYY-MM-DD");
}

function loadTableAuditLog() {
    $.jqxGridApi.create({
        showTo: "#tblAuditLog",
        options: {
            //for comments or descriptions
            height: "400",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: true,
            sortable: true,
            editable: true
        },
        sp: {
            Name: "[dbo].[spSDocProdCtrlListPFDetailAuditLog]",
            Params: [
                { Name: "@StartDate", Value: getStartDate() },
                { Name: "@EndDate", Value: getEndDate() }
            ]
        },
        source: {
            dataBinding: "Large Data Set"
        },
        columns: [
            { name: 'Type', text: 'Type', width: '125px', type: 'string', filtertype: 'checkedlist' },
            { name: 'CountryCode', text: 'Country', width: '75px', type: 'string', filtertype: 'checkedlist' },
            { name: 'Process', text: 'Process', width: '90px', type: 'string', filtertype: 'checkedlist' },
            { name: 'FileName', text: 'File Name', width: '410px', type: 'string', filtertype: 'input' },
            { name: 'Comment', text: 'Comment', width: '170px', type: 'string', filtertype: 'input' },
            { name: 'CreatedBy', text: 'SOEID', width: '90px', type: 'string', filtertype: 'checkedlist' },
            { name: 'CreatedByName', text: 'Name', width: '240px', type: 'string', filtertype: 'checkedlist' },
            { name: 'CreatedDate', text: 'Date', width: '170px', type: 'string', filtertype: 'input' }
        ]
    });
}

function downloadToExcelAudit() {
    _downloadExcel({
        spName: "[dbo].[spSDocProdCtrlListPFDetailAuditLog]",
        spParams: [
            { Name: "@StartDate", Value: getStartDate() },
            { Name: "@EndDate", Value: getEndDate() }
        ],
        filename: "AuditLogReport-" + moment().format('MMM-DD-YYYY-HH-MM'),
        success: {
            msg: "Please wait, generating report..."
        }
    });
}