//@ sourceURL=_TemplateProcessFile.js
/// <reference path="../../Shared/plugins/util/global.js" />

$(document).ready(function () {
    // Load New Process File
    if ($("#HFAction").val() == "Insert") {
        loadNewProcessFile();
    }

    // Load Edit Process File
    if ($("#HFAction").val() == "Edit") {
        loadObjProcessFileByID($("#HFIDProcessFile").val(), function (objProcessFile) {
            loadEditProcessFile(objProcessFile);
        });
    } 

    // Validate Form
    loadValidations();

    // On click Save
    $(".btnSave").click(function () {
        saveProcessFile();
    });

    // On Click Manage Process
    $(".btnManageProcess").click(function (e) {
        e.preventDefault();
        showModalManageProcess();
    });

    // On Click Manage Country
    $(".btnManageCountry").click(function (e) {
        e.preventDefault();
        showModalManageCountry();
    });

    // Validate copy contacts 
    var rows = _getCheckedRows('#tblProcessFiles');
    if (rows.length > 1) {
        $(".lblProcessFileToCopy").show();
        $(".lblProcessFileToCopy").html("(" + rows.length + ") Process Files will be copy contacts");
    }
});

function showModalManageProcess() {
    var idTable = "tblManageProcess";
    var htmlContentModal =
        '<div class="row"> ' +
        '    <div class="col-md-12"> ' +
        '        <button type="button" class="btnAddNew btn btn-success top15 left15 bottom15 pull-right"><i class="fa fa-asterisk"></i> Add</button> ' +
        '        <button type="button" class="btnDelete btn btn-danger top15 left15 bottom15 pull-right"><i class="fa fa-remove"></i> Delete</button> ' +
        '    </div> ' +
        '    <div class="col-md-12"> ' +
        '        <div id="' + idTable + '"></div> ' +
        '    </div> ' +
        '</div> ';

    _showModal({
        modalId: "modalManageProcess",
        width: '70%',
        addCloseButton: true,
        buttons: [{
            name: "<i class='fa fa-save'></i> Save",
            class: "btn-success",
            closeModalOnClick: false,
            onClick: function ($modal) {
                saveTable();
            }
        }],
        title: "Manage Process",
        contentHtml: htmlContentModal,
        onReady: function ($modal) {
            //Add new row to "#" + idTable
            $modal.find(".btnAddNew").click(function () {
                addNewRow();
            });

            //Delete selected row to "#" + idTable
            $modal.find(".btnDelete").click(function () {
                deleteRow();
            });

            //Load table 
            loadTable();
        }
    });

    function loadTable() {
        $.jqxGridApi.create({
            showTo: "#" + idTable,
            options: {
                //for comments or descriptions
                height: "250",
                autoheight: false,
                autorowheight: false,
                selectionmode: "singlerow",
                showfilterrow: true,
                sortable: true,
                editable: true,
                groupable: false
            },
            sp: {
                Name: "[dbo].[spSDocProdCtrlAdminProcess]",
                Params: [
                    { Name: "@Action", Value: "List" }
                ]
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
                dataBinding: "Large Data Set"
            },
            columns: [
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                { name: 'ID', type: 'number', hidden: true },
                { name: 'Name', text: 'Process', width: '97%', type: 'string', filtertype: 'input' }
            ],
            ready: function () {
                //Reload Select Of Process
                loadSelectOfProcess();
            }
        });
    }

    function addNewRow() {
        var $jqxGrid = $("#" + idTable);
        var datarow = {
            ID: 0,
            Name: ''
        };
        var commit = $jqxGrid.jqxGrid('addrow', null, datarow);

        _showNotification("success", "An empty row was added in the table.");
    }

    function deleteRow() {
        //Delete 
        var objRowSelected = $.jqxGridApi.getOneSelectedRow("#" + idTable, true);
        if (objRowSelected) {
            var htmlContentModal = "";
            htmlContentModal += "<b>Process: </b>" + objRowSelected['Name'] + "<br/>";

            _showModal({
                width: '40%',
                modalId: "modalDelRow",
                addCloseButton: true,
                buttons: [{
                    name: "Delete",
                    class: "btn-danger",
                    onClick: function () {
                        if (objRowSelected.ID != 0) {
                            _callProcedure({
                                loadingMsgType: "fullLoading",
                                loadingMsg: "Saving process...",
                                name: "[dbo].[spSDocProdCtrlAdminProcess]",
                                params: [
                                    { "Name": "@Action", "Value": "Delete" },
                                    { "Name": "@IDProcess", "Value": objRowSelected.ID },
                                    { "Name": "@Name", "Value": objRowSelected.Name },
                                    { "Name": "@SessionSOEID", "Value": _getSOEID() }
                                ],
                                //Show message only for the last 
                                success: {
                                    showTo: $("#" + idTable).parent(),
                                    msg: "Row was deleted successfully.",
                                    fn: function () {
                                        var reloadData = setInterval(function () {
                                            //console.log("Reloading data from #tblReasons");
                                            //$.jqxGridApi.localStorageFindById("#" + idTable).fnReload();
                                            //$("#" + idTable).jqxGrid('updateBoundData');

                                            loadTable();

                                            //Remove changes
                                            $.jqxGridApi.rowsChangedFindById("#" + idTable).rows = [];

                                            clearInterval(reloadData);
                                        }, 500);
                                    }
                                }
                            });
                        } else {
                            $("#" + idTable).jqxGrid('deleterow', objRowSelected.uid);
                        }
                    }
                }],
                title: "Are you sure you want to delete this row?",
                contentHtml: htmlContentModal
            });
        }
    }

    function saveTable() {
        var rowsChanged = $.jqxGridApi.rowsChangedFindById("#" + idTable).rows;

        //Validate changes
        if (rowsChanged.length > 0) {
            var fnSuccess = {
                showTo: $("#" + idTable).parent(),
                msg: "The data was saved successfully.",
                fn: function () {
                    var reloadData = setInterval(function () {
                        //console.log("Reloading data from #tblReasons");
                        //$.jqxGridApi.localStorageFindById("#" + idTable).fnReload();
                        //$("#" + idTable).jqxGrid('updateBoundData');

                        loadTable();

                        //Remove changes
                        $.jqxGridApi.rowsChangedFindById("#" + idTable).rows = [];

                        clearInterval(reloadData);
                    }, 1500);
                }
            };

            for (var i = 0; i < rowsChanged.length; i++) {
                var objRow = rowsChanged[i];
                var action = "Edit";

                if (objRow.ID == 0) {
                    var action = "Insert";
                }

                _callProcedure({
                    loadingMsgType: "fullLoading",
                    loadingMsg: "Saving process...",
                    name: "[dbo].[spSDocProdCtrlAdminProcess]",
                    params: [
                        { "Name": "@Action", "Value": action },
                        { "Name": "@IDProcess", "Value": objRow.ID },
                        { "Name": "@Name", "Value": objRow.Name },
                        { "Name": "@SessionSOEID", "Value": _getSOEID() }
                    ],
                    //Show message only for the last 
                    success: (i == rowsChanged.length - 1) ? fnSuccess : null
                });
            }
        } else {
            _showAlert({
                showTo: $("#" + idTable).parent(),
                type: 'error',
                title: "Message",
                content: "No changes detected."
            });
        }
    }
}

function showModalManageCountry() {
    var idTable = "tblManageCountry";
    var htmlContentModal =
        '<div class="row"> ' +
        '    <div class="col-md-12"> ' +
        '        <button type="button" class="btnAddNew btn btn-success top15 left15 bottom15 pull-right"><i class="fa fa-asterisk"></i> Add</button> ' +
        '        <button type="button" class="btnCheckAccess btn btn-info top15 left15 bottom15 pull-right"><i class="fa fa-asterisk"></i> Check Access</button> ' +
        '        <button type="button" class="btnDelete btn btn-danger top15 left15 bottom15 pull-right"><i class="fa fa-remove"></i> Delete</button> ' +
        '    </div> ' +
        '    <div class="col-md-12"> ' +
        '        <div id="' + idTable + '"></div> ' +
        '    </div> ' +
        '</div> ';

    _showModal({
        modalId: "modalManageCountry",
        width: '70%',
        addCloseButton: true,
        buttons: [{
            name: "<i class='fa fa-save'></i> Save",
            class: "btn-success",
            closeModalOnClick: false,
            onClick: function ($modal) {
                saveTable();
            }
        }],
        title: "Manage Country",
        contentHtml: htmlContentModal,
        onReady: function ($modal) {
            //Add new row 
            $modal.find(".btnAddNew").click(function () {
                addNewRow();
            });

            //Delete selected row 
            $modal.find(".btnDelete").click(function () {
                deleteRow();
            });

            //Check access
            $modal.find(".btnCheckAccess").click(function () {
                checkAccess();
            });

            //Load table 
            loadTable();
        }
    });

    function loadTable() {
        $.jqxGridApi.create({
            showTo: "#" + idTable,
            options: {
                //for comments or descriptions
                height: "250",
                autoheight: false,
                autorowheight: false,
                selectionmode: "singlerow",
                showfilterrow: true,
                sortable: true,
                editable: true,
                groupable: false
            },
            sp: {
                Name: "[dbo].[spSDocProdCtrlAdminCountry]",
                Params: [
                    { Name: "@Action", Value: "List" }
                ]
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
                dataBinding: "Large Data Set"
            },
            columns: [
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                { name: 'ID', type: 'number', hidden: true },
                { name: 'Code', text: 'Code', width: '13%', type: 'string', filtertype: 'input' },
                { name: 'Country', text: 'Country Name', width: '19%', type: 'string', filtertype: 'input' },
                { name: 'ShareFolderPath', text: 'Share Folder Path', width: '66%', type: 'string', filtertype: 'input' }
            ],
            ready: function () {
                //Reload Select Of Country
                loadSelectOfCountries();
            }
        });
    }

    function addNewRow() {
        var $jqxGrid = $("#" + idTable);
        var datarow = {
            ID: 0,
            Code: '',
            Country: '',
            ShareFolderPath: ''
        };
        var commit = $jqxGrid.jqxGrid('addrow', null, datarow);

        _showNotification("success", "An empty row was added in the table.");
    }

    function deleteRow() {
        //Delete 
        var objRowSelected = $.jqxGridApi.getOneSelectedRow("#" + idTable, true);
        if (objRowSelected) {
            var htmlContentModal = "";
            htmlContentModal += "<b>Code: </b>" + objRowSelected['Code'] + "<br/>";
            htmlContentModal += "<b>Country: </b>" + objRowSelected['Country'] + "<br/>";
            htmlContentModal += "<b>Share Folder Path: </b>" + objRowSelected['ShareFolderPath'] + "<br/>";

            _showModal({
                width: '40%',
                modalId: "modalDelRow",
                addCloseButton: true,
                buttons: [{
                    name: "Delete",
                    class: "btn-danger",
                    onClick: function () {
                        if (objRowSelected.ID != 0) {
                            _callProcedure({
                                loadingMsgType: "fullLoading",
                                loadingMsg: "Saving country...",
                                name: "[dbo].[spSDocProdCtrlAdminCountry]",
                                params: [
                                    { "Name": "@Action", "Value": "Delete" },
                                    { "Name": "@IDCountry", "Value": objRowSelected.ID },
                                    { "Name": "@SessionSOEID", "Value": _getSOEID() }
                                ],
                                //Show message only for the last 
                                success: {
                                    showTo: $("#" + idTable).parent(),
                                    msg: "Row was deleted successfully.",
                                    fn: function () {
                                        var reloadData = setInterval(function () {
                                            //console.log("Reloading data from #tblReasons");
                                            //$.jqxGridApi.localStorageFindById("#" + idTable).fnReload();
                                            //$("#" + idTable).jqxGrid('updateBoundData');

                                            loadTable();

                                            //Remove changes
                                            $.jqxGridApi.rowsChangedFindById("#" + idTable).rows = [];

                                            clearInterval(reloadData);
                                        }, 500);
                                    }
                                }
                            });
                        } else {
                            $("#" + idTable).jqxGrid('deleterow', objRowSelected.uid);
                        }
                    }
                }],
                title: "Are you sure you want to delete this row?",
                contentHtml: htmlContentModal
            });
        }
    }

    function checkAccess() {
        var objRowSelected = $.jqxGridApi.getOneSelectedRow("#" + idTable, true);
        if (objRowSelected) {
            _callServer({
                loadingMsgType: "topBar",
                loadingMsg: "Checking access...",
                url: '/SupportDocProdCtrl/CheckAccess',
                data: {
                    typeAuthentication: _getTypeAuthentication(),
                    user: _encodeSkipSideminder(_getWindowsUser()),
                    pass: _getWindowsPassword(),
                    pathToTest: _encodeSkipSideminder(objRowSelected["ShareFolderPath"])
                },
                success: function (msg) {
                    _showNotification("info", msg, "CheckAccess", false);
                }
            });
        }
    }

    function saveTable() {
        var rowsChanged = $.jqxGridApi.rowsChangedFindById("#" + idTable).rows;

        //Validate changes
        if (rowsChanged.length > 0) {
            var fnSuccess = {
                showTo: $("#" + idTable).parent(),
                msg: "The data was saved successfully.",
                fn: function () {
                    var reloadData = setInterval(function () {
                        //console.log("Reloading data from #tblReasons");
                        //$.jqxGridApi.localStorageFindById("#" + idTable).fnReload();
                        //$("#" + idTable).jqxGrid('updateBoundData');

                        loadTable();

                        //Remove changes
                        $.jqxGridApi.rowsChangedFindById("#" + idTable).rows = [];

                        clearInterval(reloadData);
                    }, 1500);
                }
            };

            for (var i = 0; i < rowsChanged.length; i++) {
                var objRow = rowsChanged[i];
                var action = "Edit";

                if (objRow.ID == 0) {
                    var action = "Insert";
                }

                _callProcedure({
                    loadingMsgType: "fullLoading",
                    loadingMsg: "Saving process...",
                    name: "[dbo].[spSDocProdCtrlAdminCountry]",
                    params: [
                        { "Name": "@Action", "Value": action },
                        { "Name": "@IDCountry", "Value": objRow.ID },
                        { "Name": "@Code", "Value": objRow.Code },
                        { "Name": "@Country", "Value": objRow.Country },
                        { "Name": "@ShareFolderPath", "Value": objRow.ShareFolderPath },
                        { "Name": "@SessionSOEID", "Value": _getSOEID() }
                    ],
                    //Show message only for the last 
                    success: (i == rowsChanged.length - 1) ? fnSuccess : null
                });
            }
        } else {
            _showAlert({
                showTo: $("#" + idTable).parent(),
                type: 'error',
                title: "Message",
                content: "No changes detected."
            });
        }
    }
}

function loadSelectOfProcess(selectedIDProcess) {
    var sql = "                             \
        SELECT                              \
        '0'				AS [ID],            \
	    '-- Select --'	AS [Name]           \
        UNION                               \
        SELECT                              \
            [ID],                           \
            [Name]                          \
        FROM                                \
            [dbo].[tblSDocProdCtrlProcess]  \
        WHERE                               \
            [IsDeleted] = 0                 \
        ORDER BY                            \
            (2) ASC                         ";

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Process...",
        url: '/Shared/ExecQuery',
        data: { 'pjsonSql': _toJSON(sql) },
        type: "post",
        success: function (resultList) {
            $("#selProcess").contents().remove();

            //Add Options
            for (var i = 0; i < resultList.length; i++) {
                var objTemp = resultList[i];
                $("#selProcess").append($('<option>', {
                    value: objTemp.ID,
                    text: objTemp.Name,
                    selected: (selectedIDProcess == objTemp.ID)
                }));
            }

            $("#selProcess").select2();
        }
    });
};

function loadSelectOfCountries(selectedCountryCode) {
    var sql = "                                         \
        SELECT                                          \
            '0'	                        AS [Code],      \
            '-- Select --'	            AS [Country]    \
        UNION                                           \
        SELECT                                          \
            [Code]                      AS [Code],      \
            ISNULL([Country], [Code])   AS [Country]    \
        FROM                                            \
            [dbo].[tblSDocProdCtrlCountry]              \
        WHERE                                           \
            [IsDeleted] = 0                             \
        ORDER BY                                        \
            (2) ASC                                     ";

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Countries...",
        url: '/Shared/ExecQuery',
        data: { 'pjsonSql': _toJSON(sql) },
        type: "post",
        success: function (resultList) {
            $("#selCountry").contents().remove();

            //Add Options
            for (var i = 0; i < resultList.length; i++) {
                var objTemp = resultList[i];
                $("#selCountry").append($('<option>', {
                    value: objTemp.Code,
                    text: objTemp.Country,
                    selected: (selectedCountryCode == objTemp.Code)
                }));
            }

            $("#selCountry").select2();
        }
    });
};

function loadSelectOfFrequency(selectedIDFrequency) {
    var sql =
        " SELECT " +
        "     '0'	            AS [ID]," +
        "     '-- Select --'	AS [Frequency]" +
        " UNION " +
        " SELECT " +
        "     [ID]              AS [ID], " +
        "     [Frequency]       AS [Frequency] " +
        " FROM " +
        "     [dbo].[tblSDocProdCtrlFrequency] " +
        " ORDER BY " +
        "     (2) ASC ";

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Frequencies...",
        url: '/Shared/ExecQuery',
        data: { 'pjsonSql': _toJSON(sql) },
        type: "post",
        success: function (resultList) {
            $("#selFrequency").contents().remove();

            //Add Options
            for (var i = 0; i < resultList.length; i++) {
                var objTemp = resultList[i];
                $("#selFrequency").append($('<option>', {
                    value: objTemp.ID,
                    text: objTemp.Frequency,
                    selected: (selectedIDFrequency == objTemp.ID)
                }));
            }

            $("#selFrequency").select2();
        }
    });
};

function loadTableOfPermits(IDProcessFile, PermitType) {
    var idTable = "#tbl" + PermitType + "s";

    //Add new row 
    $(".btnNew" + PermitType).click(function () {
        newRow();
    });

    //Delete selected row 
    $(".btnDelete" + PermitType).click(function () {
        deleteRow();
    });

    function loadTablePermits() {
        $.jqxGridApi.create({
            showTo: idTable,
            options: {
                //for comments or descriptions
                height: "350",
                autoheight: false,
                autorowheight: false,
                selectionmode: "singlerow",
                showfilterrow: true,
                sortable: true,
                editable: true,
                groupable: false
            },
            sp: {
                Name: "[dbo].[spSDocProdCtrlAdminPFPermits]",
                Params: [
                    { Name: "@Action", Value: "List" },
                    { Name: "@Type", Value: PermitType },
                    { Name: "@IDProcessFile", Value: IDProcessFile }
                ],
                OnDataLoaded: function (responseList) {
                    $("#lbl" + PermitType + "s").html(PermitType + "s (" + responseList.length + ")");
                }
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
                dataBinding: "Large Data Set"
            },
            columns: [
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                { name: 'IDPFPermit', type: 'number', hidden: true },
                { name: 'SOEID', text: 'SOEID', width: '30%', type: 'string', filtertype: 'input' },
                { name: 'Name', text: 'Name', width: '70%', type: 'string', filtertype: 'input' }
            ],
            ready: function () {}
        });
    }

    function newRow() {
        var $jqxGrid = $(idTable);
        var $optionSelected = $("#sel" + PermitType + " select").find(':selected');

        //Validate if employee was selected
        if ($optionSelected.length > 0) {

            //Validate if employee not exist in currect selection
            var checkersRows = $("#tblCheckers").jqxGrid("getRows");
            var existsChecker = _findAllObjByProperty(checkersRows, "SOEID", $optionSelected.val());
            if (existsChecker.length == 0) {

                //Validate if employe not exist in Makers
                var makersRows = $("#tblMakers").jqxGrid("getRows");
                var existsMaker = _findAllObjByProperty(makersRows, "SOEID", $optionSelected.val());
                if (existsMaker.length == 0) {

                    var datarow = {
                        IDPFPermit: 0,
                        SOEID: $optionSelected.val(),
                        Name: $optionSelected.attr("Name")
                    };
                    var commit = $jqxGrid.jqxGrid('addrow', null, datarow);

                    //Refresh Permits Counter
                    $("#lbl" + PermitType + "s").html(PermitType + "s (" + $jqxGrid.jqxGrid("getRows").length + ")");
                } else {
                    _showAlert({
                        showTo: $(idTable).parent(),
                        type: 'error',
                        title: "Message",
                        content: "'" + $optionSelected.text() + "' already exist as Maker."
                    });
                }
            } else {
                _showAlert({
                    showTo: $(idTable).parent(),
                    type: 'error',
                    title: "Message",
                    content: "'" + $optionSelected.text() + "' already exist as Checker."
                });
            }
        } else {
            _showAlert({
                showTo: $(idTable).parent(),
                type: 'error',
                title: "Message",
                content: "Please select the employee SOEID."
            });
        }
    }

    function deleteRow() {
        //Delete 
        var objRowSelected = $.jqxGridApi.getOneSelectedRow(idTable, true);
        if (objRowSelected) {
            var htmlContentModal = "";
            htmlContentModal += "<b>User: </b>(" + objRowSelected['SOEID'] + ") " + objRowSelected['Name'] + "<br/>";

            _showModal({
                width: '40%',
                modalId: "modalDelRow",
                addCloseButton: true,
                buttons: [{
                    name: "Delete",
                    class: "btn-danger",
                    onClick: function () {
                        if (objRowSelected.IDPFPermit != 0) {
                            _callProcedure({
                                loadingMsgType: "fullLoading",
                                loadingMsg: "Deleting row...",
                                name: "[dbo].[spSDocProdCtrlAdminPFPermits]",
                                params: [
                                    { "Name": "@Action", "Value": "Delete" },
                                    { "Name": "@IDPFPermit", "Value": objRowSelected.IDPFPermit },
                                    { "Name": "@SessionSOEID", "Value": _getSOEID() }
                                ],
                                //Show message only for the last 
                                success: {
                                    showTo: $(idTable).parent(),
                                    msg: "Row was deleted successfully.",
                                    fn: function () {
                                        var reloadData = setInterval(function () {
                                            //Reload table
                                            loadTablePermits();

                                            //Remove changes
                                            $.jqxGridApi.rowsChangedFindById(idTable).rows = [];

                                            clearInterval(reloadData);
                                        }, 500);
                                    }
                                }
                            });
                        } else {
                            $(idTable).jqxGrid('deleterow', objRowSelected.uid);
                        }
                    }
                }],
                title: "Are you sure you want to delete this row?",
                contentHtml: htmlContentModal
            });
        }
    }
    
    //Load table of permits
    loadTablePermits();
}

function loadTableOfEmails(IDProcessFile) {
    var idTable = "#tblEmails";

    //Add new row 
    $(".btnNewEmail").click(function () {
        newRow();
    });

    //Delete selected row 
    $(".btnDeleteEmail").click(function () {
        deleteRow();
    });

    function loadTableEmails() {
        $.jqxGridApi.create({
            showTo: idTable,
            options: {
                //for comments or descriptions
                height: "350",
                autoheight: false,
                autorowheight: false,
                selectionmode: "singlerow",
                showfilterrow: true,
                sortable: true,
                editable: true,
                groupable: false
            },
            sp: {
                Name: "[dbo].[spSDocProdCtrlAdminPFEmails]",
                Params: [
                    { Name: "@Action", Value: "List" },
                    { Name: "@IDProcessFile", Value: IDProcessFile }
                ],
                OnDataLoaded: function (responseList) {
                    $("#lblEmails").html("Stakeholder (" + responseList.length + ")");
                }
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
                dataBinding: "Large Data Set"
            },
            columns: [
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                { name: 'IDPFEmail', type: 'number', hidden: true },
                { name: 'Email', text: 'Stakeholder', width: '90%', type: 'string', filtertype: 'input' },
                {
                    name: 'EmailFormatOk', text: 'Valid', width: '10%', type: 'bool', filtertype: 'boolean', editable: false, filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                        var dataRecord = $(idTable).jqxGrid('getrowdata', rowIndex);
                        var isOk = false;

                        if (_contains(dataRecord.Email, "<") &&
                            _contains(dataRecord.Email, ">") &&
                            _contains(dataRecord.Email, "@")) {
                            isOk = true;
                        }
                        var htmlResult = (isOk ? '<i style="color:green;width: 80px;text-align: center;font-size: 26px;padding: 5px;" class="fa fa-check-square-o"></i>' : '<i style="color:red;width: 80px;text-align: center;font-size: 26px;padding: 5px;" class="fa fa-times-circle"></i>');
                        return htmlResult;
                    }
                }
            ],
            ready: function () {

            }
        });
    }

    function newRow() {
        var $jqxGrid = $(idTable);
        var inputEmail = $("#txtEmail").val();

        //Validate if exists the email
        if (inputEmail) {

            //Validate if email not exist in current list
            var emailsRows = $(idTable).jqxGrid("getRows");
            var existsEmail = _findAllObjByProperty(emailsRows, "Email", inputEmail);
            if (existsEmail.length == 0) {

                var datarow = {
                    IDPFEmail: 0,
                    Email: inputEmail
                };
                var commit = $jqxGrid.jqxGrid('addrow', null, datarow);

                //Refresh Emails Counter
                $("#lblEmails").html("Stakeholder (" + $jqxGrid.jqxGrid("getRows").length + ")");
            } else {
                _showAlert({
                    showTo: $(idTable).parent(),
                    type: 'error',
                    title: "Message",
                    content: "'" + inputEmail + "' already exist in Stakeholder."
                });
            }
        } else {
            _showAlert({
                showTo: $(idTable).parent(),
                type: 'error',
                title: "Message",
                content: "Input Email is empty."
            });
        }
    }

    function deleteRow() {
        //Delete 
        var objRowSelected = $.jqxGridApi.getOneSelectedRow(idTable, true);
        if (objRowSelected) {
            var htmlContentModal = "";
            htmlContentModal += "<b>Email: </b>" + objRowSelected['Email'] + "<br/>";

            _showModal({
                width: '40%',
                modalId: "modalDelRow",
                addCloseButton: true,
                buttons: [{
                    name: "Delete",
                    class: "btn-danger",
                    onClick: function () {
                        if (objRowSelected.IDPFEmail != 0) {
                            _callProcedure({
                                loadingMsgType: "fullLoading",
                                loadingMsg: "Deleting row...",
                                name: "[dbo].[spSDocProdCtrlAdminPFEmails]",
                                params: [
                                    { "Name": "@Action", "Value": "Delete" },
                                    { "Name": "@IDPFEmail", "Value": objRowSelected.IDPFEmail },
                                    { "Name": "@SessionSOEID", "Value": _getSOEID() }
                                ],
                                //Show message only for the last 
                                success: {
                                    showTo: $(idTable).parent(),
                                    msg: "Row was deleted successfully.",
                                    fn: function () {
                                        var reloadData = setInterval(function () {
                                            //Reload table
                                            loadTableEmails();

                                            //Remove changes
                                            $.jqxGridApi.rowsChangedFindById(idTable).rows = [];

                                            clearInterval(reloadData);
                                        }, 500);
                                    }
                                }
                            });
                        } else {
                            $(idTable).jqxGrid('deleterow', objRowSelected.uid);
                        }
                    }
                }],
                title: "Are you sure you want to delete this row?",
                contentHtml: htmlContentModal
            });
        }
    }

    //Load table 
    loadTableEmails();
}

function loadValidations() {
    var rules = {};

    // Process
    rules["selProcess"] = {
        required: true,
        valueNotEquals: 0
    };

    // Country
    rules["selCountry"] = {
        required: true,
        valueNotEquals: 0
    };

    // Frequency
    rules["selFrequency"] = {
        required: true,
        valueNotEquals: 0
    };
    
    // File Name
    rules["txtFileName"] = {
        required: true
    };

    _validateForm({
        form: '#formProcessFile',
        rules: rules
    });
}

function loadObjProcessFileByID(idProcessFile, fnOnSuccess) {
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Process File...",
        name: "[dbo].[spSDocProdCtrlAdminProcessFile]",
        params: [
            { "Name": "@IDProcessFile", "Value": idProcessFile }
        ],
        success: {
            fn: function (responseList) {
                if (responseList.length > 0) {
                    fnOnSuccess(responseList[0]);
                }
            }
        }
    });
}

function loadNewProcessFile() {
    // Process
    loadSelectOfProcess();

    // Country
    loadSelectOfCountries();

    // Frequency
    loadSelectOfFrequency();

    // Checkers
    loadTableOfPermits(0, "Checker");

    // Select of Checker
    _createSelectSOEID({
        id: "#selChecker",
        selSOEID: ""
    });

    // Makers
    loadTableOfPermits(0, "Maker");

    // Select of Maker
    _createSelectSOEID({
        id: "#selMaker",
        selSOEID: ""
    });

    // Emails
    loadTableOfEmails(0);
}

function loadEditProcessFile(objEditProcessFile) {
    // Process
    loadSelectOfProcess(objEditProcessFile.IDProcess);

    // Country
    loadSelectOfCountries(objEditProcessFile.CountryCode);

    // Frequency
    loadSelectOfFrequency(objEditProcessFile.IDFrequency);

    // File Ext
    $("#selFileExt").val(objEditProcessFile.FileExt);

    // File Name
    $("#txtFileName").val(objEditProcessFile.FileName);

    // Checkers
    loadTableOfPermits(objEditProcessFile.IDProcessFile, "Checker");

    // Select of Checker
    _createSelectSOEID({
        id: "#selChecker",
        selSOEID: ""
    });

    // Makers
    loadTableOfPermits(objEditProcessFile.IDProcessFile, "Maker");

    // Select of Maker
    _createSelectSOEID({
        id: "#selMaker",
        selSOEID: ""
    });

    // Emails
    loadTableOfEmails(objEditProcessFile.IDProcessFile);
}

function getObjProcessFile() {
    var objProcessFile = {
        IDProcessFile: { FieldName: 'IDProcessFile', FieldValue: $("#HFIDProcessFile").val() },
        IDProcess: { FieldName: 'Process', FieldValue: $("#selProcess").val() },
        CountryCode: { FieldName: 'Country', FieldValue: $("#selCountry").val() },
        IDFrequency: { FieldName: 'Frequency', FieldValue: $("#selFrequency").val() },
        FileName: { FieldName: 'FileName', FieldValue: $("#txtFileName").val() },
        FileExt: { FieldName: 'FileExt', FieldValue: $("#selFileExt").val() }
    };

    return objProcessFile;
}

function generateString(data, property, character) {
    var result = "";
    for (var i = 0; i < data.length; i++) {
        result += data[i][property] + character;
    }

    if (data.length > 0) {
        result = result.substring(0, result.length - (character.length));
    }
    return result;
}

function saveProcessFile() {
    if ($("#formProcessFile").valid()) {
        var objProcessFile = getObjProcessFile();
        var action = "Insert";

        if ($("#HFAction").val() == "Edit") {
            action = "Edit";
        }

        //Get Checkers SOEIDs
        var checkerRows = $("#tblCheckers").jqxGrid("getRows");
        var listCheckerSOEIDs = generateString(checkerRows, "SOEID", ";");

        //Get Makers SOEIDs
        var makerRows = $("#tblMakers").jqxGrid("getRows");
        var listMakersSOEIDs = generateString(makerRows, "SOEID", ";");

        //Get Emails
        var emailRows = $("#tblEmails").jqxGrid("getRows");
        var listEmails = generateString(emailRows, "Email", ";");

        //Get Checked Rows to Copy Contacts
        var rowsToCopyContacts = _getCheckedRows('#tblProcessFiles');
        var counterRowsCopiedProcessed = 0;

        //Fix Bug: En Process Manager, si se da click solo a 1 linea con el Checkbox no permite agregar Checkers o Makers.
        if (rowsToCopyContacts.length > 1) {
            //Remove current process file
            var objCurrentPF = _findOneObjByProperty(rowsToCopyContacts, "IDProcessFile", objProcessFile.IDProcessFile.FieldValue);
            var indexToRemove = rowsToCopyContacts.indexOf(objCurrentPF);
            if (indexToRemove > -1) {
                rowsToCopyContacts.splice(indexToRemove, 1);
            }

            for (var i = 0; i < rowsToCopyContacts.length; i++) {
                var objTempRow = rowsToCopyContacts[i];

                //Save change to get the ID
                _callProcedure({
                    loadingMsgType: "topBar",
                    loadingMsg: "Copy Contacts to Process File...",
                    name: "[dbo].[spSDocProdCtrlAdminProcessFile]",
                    params: [
                        { "Name": "@Action", "Value": "Edit" },
                        { "Name": "@IDProcessFile", "Value": objTempRow["IDProcessFile"] },
                        { "Name": "@IDProcess", "Value": objTempRow["IDProcess"] },
                        { "Name": "@IDFrequency", "Value": objTempRow["IDFrequency"] },
                        { "Name": "@CountryCode", "Value": objTempRow["CountryCode"] },
                        { "Name": "@FileName", "Value": objTempRow["FileName"] },
                        { "Name": "@FileExt", "Value": objTempRow["FileExt"] },
                        { "Name": "@ListCheckerSOEDs", "Value": listCheckerSOEIDs },
                        { "Name": "@ListMakerSOEIDs", "Value": listMakersSOEIDs },
                        { "Name": "@ListEmails", "Value": listEmails },
                        { "Name": "@SessionSOEID", "Value": _getSOEID() }
                    ],
                    success: {
                        fn: function (response) {
                            counterRowsCopiedProcessed++;
                            saveCurrentRowChanges();
                        }
                    }
                });
            }
        } else {
            //Fix Bug: En Process Manager, si se da click solo a 1 linea con el Checkbox no permite agregar Checkers o Makers.
            counterRowsCopiedProcessed = rowsToCopyContacts.length;
            saveCurrentRowChanges();
        }

        function saveCurrentRowChanges() {
            if (counterRowsCopiedProcessed == rowsToCopyContacts.length) {
                //Save change to get the ID
                _callProcedure({
                    loadingMsgType: "topBar",
                    loadingMsg: "Saving Process File...",
                    name: "[dbo].[spSDocProdCtrlAdminProcessFile]",
                    params: [
                        { "Name": "@Action", "Value": action },
                        { "Name": "@IDProcessFile", "Value": objProcessFile.IDProcessFile.FieldValue },
                        { "Name": "@IDProcess", "Value": objProcessFile.IDProcess.FieldValue },
                        { "Name": "@IDFrequency", "Value": objProcessFile.IDFrequency.FieldValue },
                        { "Name": "@CountryCode", "Value": objProcessFile.CountryCode.FieldValue },
                        { "Name": "@FileName", "Value": objProcessFile.FileName.FieldValue },
                        { "Name": "@FileExt", "Value": objProcessFile.FileExt.FieldValue },
                        { "Name": "@ListCheckerSOEDs", "Value": listCheckerSOEIDs },
                        { "Name": "@ListMakerSOEIDs", "Value": listMakersSOEIDs },
                        { "Name": "@ListEmails", "Value": listEmails },
                        { "Name": "@SessionSOEID", "Value": _getSOEID() }
                    ],
                    success: {
                        fn: function (response) {
                            //Show Message
                            _showNotification("success", "Process File " + objProcessFile.FileName.FieldValue + "." + objProcessFile.FileExt.FieldValue + " was saved successfully.", "ProcessFileAlert");
                            
                            //Reload table of process list
                            _loadProcessFilesTable("#tblProcessFiles");
                        }
                    }
                });
            }
        }
        
    }
}