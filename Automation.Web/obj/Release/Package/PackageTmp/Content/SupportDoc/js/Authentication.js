/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="SupportDocGlobal.js" />

$(document).ready(function () {
    
    //On Change Authentication Type
    $("input[name=TypeAuthentication]").on('ifChanged', function () {
        var authType = $("input[name=TypeAuthentication]:checked").val();
        _setTypeAuthentication(authType);
    });

    //On change windows user
    $("#txtUserID").change(function () {
        var user = $("#txtUserID").val();
        _setWindowsUser(user);
    });

    //On change password user
    $("#txtPassword").change(function () {
        var pass = $("#txtPassword").val();
        _setWindowsPassword(pass);
    });

    //On click Check access
    $(".btnCheckAccess").click(function () {
        checkAccess();
    });

    loadCurrentData();
});

function loadCurrentData() {
    //Set authentication type
    var typeAuth = _getTypeAuthentication();
    $("#rbt" + typeAuth).iCheck('check');

    //Set windows user
    var winUser = _getWindowsUser();
    $("#txtUserID").val(winUser);

    //Set windows password
    var winPassword = _getWindowsPassword(true);
    $("#txtPassword").val(winPassword);
}

function checkAccess() {
    var pathToTest = $("#txtPathToTest").val();
    _callServer({
        loadingMsgType: "topBar",
        loadingMsg: "Checking access...",
        url: '/SupportDoc/CheckAccess',
        data: {
            typeAuthentication: _getTypeAuthentication(),
            user: _encodeSkipSideminder( _getWindowsUser() ),
            pass: _getWindowsPassword(),
            pathToTest: _encodeSkipSideminder(pathToTest)
        },
        success: function (msg) {
            _showNotification("info", msg);
        }
    });
}