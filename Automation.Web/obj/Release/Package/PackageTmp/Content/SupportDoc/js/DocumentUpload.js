/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="SupportDocGlobal.js" />

$(document).ready(function () {

    //Hide Menu Only when is a Responsible View
    _hideMenu();

    // Load Periods
    _loadMonths({
        showTo: "#selPeriod",
        selYear: $("#HFYear").val(),
        selMonth: $("#HFMonth").val()
    });

    //On click upload document
    $(".btnUploadDoc").click(function () {
        uploadDoc();
    });

    //On click export to excel
    $(".btnExportToExcel").click(function () {
        exportToExcel();
    });

    //On click reload table
    $(".btnRefresh").click(function () {
        checkFileExistByResponsible(function () {
            //Load pending documents by responsible
            loadDocumentByResponsible();
        });
    });

    //On Change Period
    $("#selPeriod").change(function () {
        $(".btnRefresh").click();
    });

    $(".btnRefresh").click();
});

function exportToExcel() {
    var spName = "[dbo].[spSDocPeriodListPagingExcel]";
    var spParams = [
        { "Name": "@StrWhere", "Value": "WHERE ( [Year] = " + $("#selPeriod").find("option:selected").attr("year") + " AND [Month] = " + $("#selPeriod").find("option:selected").attr("month") + " AND ([NotificationStatus] ='" + $("#HFNotificationStatus").val() + "') AND [ResponsilbleSOEID] LIKE '%" + $("#HFSOEIDResponsible").val() + "%')" },
        { "Name": "@EndIndexRow", "Value": 100000 }
    ];
    var filename = "SupportDocToolPendingDocs - " + $("#selPeriod").find("option:selected").val();

    _downloadExcel({
        sql: _getSqlProcedure(spName, spParams),
        filename: filename,
        success: {
            msg: "Please Wait. Generating report..."
        }
    });
}

function uploadDoc() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#tblDocs', true);
    if (rowData) {
        var htmlContentModal = '';
        htmlContentModal += "<b>Full Key: </b> " + rowData["FullKey"] + " <br/>";
        htmlContentModal += "<b>File Name: </b> " + rowData["FileName"] + " <br/>";
        htmlContentModal += "<b>File Extension: </b> " + rowData["Extension"] + " <br/>";
        htmlContentModal += "<b>Location to upload: </b> " + rowData["FileFolderPath"] + " <br/><br/>";
        htmlContentModal += "<div id='fine-uploader-manual-trigger'></div>";

        _showModal({
            width: '50%',
            modalId: "modalUpload",
            addCloseButton: true,
            title: "Upload Document",
            contentHtml: htmlContentModal,
            onReady: function ($modal) {
                initFileUploader(rowData["Extension"], rowData["FileName"], rowData["FileFolderPath"]);
            }
        });
    }
    
}

function initFileUploader(pext, pfileName, plocationToSave) {
    $('#fine-uploader-manual-trigger').fineUploader({
        template: 'qq-template-manual-trigger',
        request: {
            endpoint: _getViewVar("SubAppPath") + '/SupportDoc/Upload'
        },
        thumbnails: {
            placeholders: {
                waitingPath: _getViewVar("SubAppPath") + '/Content/Shared/plugins/fine-uploader/placeholders/waiting-generic.png',
                notAvailablePath: _getViewVar("SubAppPath") + '/Content/Shared/plugins/fine-uploader/placeholders/not_available-generic.png'
            }
        },
        callbacks: {
            onAllComplete: function (succeeded, failed) {
                console.log(succeeded, failed);
                if (failed.length == 0) {
                    _showAlert({
                        showTo: ".modal-body",
                        type: "success",
                        title: "Message",
                        content: "The file was uploaded successfully.",
                        animateScrollTop: false
                    });

                    //Refresh Table
                    loadDocumentByResponsible();

                    //Create Log
                    _insertAuditLog({
                        Action: "Uploaded Doc",
                        Description: (plocationToSave + pfileName + '.' + pext)
                    });
                }
            },
            onError: function (id, name, errorReason, xhrOrXdr) {
                if (name && !_contains(errorReason, "has an invalid extension")) {
                    _showDetailAlert({
                        showTo: ".modal-body",
                        title: "Message",
                        shortMsg: "An error ocurred with the file '" + name + "'.",
                        longMsg: "<pre>" + errorReason + "</pre>",
                        type: "Error",
                        viewLabel: "View Details"
                    });
                }
            },
            onSubmit: function (id, name) {
                //var file = this.getFile(id);
                //validated = validate(file);

                //if (validated) {
                //    return true;
                //} else {
                //    return false;
                //}

                //Add parameter of locationToSave
                $('#fine-uploader-manual-trigger').fineUploader('setParams', {
                    'year': $("#selPeriod").find("option:selected").attr("year"),
                    'month': $("#selPeriod").find("option:selected").attr("month"),
                    'locationToSave': plocationToSave
                });
            },
            onManualRetry: function (id, name) {
                //Add parameter of locationToSave
                $('#fine-uploader-manual-trigger').fineUploader('setParams', {
                    'year': $("#selPeriod").find("option:selected").attr("year"),
                    'month': $("#selPeriod").find("option:selected").attr("month"),
                    'locationToSave': plocationToSave
                });
            }
        },
        validation: {
            allowedExtensions: [pext],
            itemLimit: 1
            //sizeLimit: 51200 // 50 kB = 50 * 1024 bytes
        },
        autoUpload: false
    });

    //Validation before send files
    $('#trigger-upload').click(function () {

        //Validate if the file has the correct name
        if ($(".qq-upload-file-selector.qq-upload-file.qq-editable").attr("title") != pfileName + '.' + pext) {
            _showAlert({
                showTo: ".modal-body",
                content: "Please change the name of your document to '<b>" + pfileName + "</b>'."
            });
        } else {
            $('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
        }

        //if ($('[name^="iCheck-TypeUpload"]:checked').length > 0) {
        //    $('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
        //} else {
        //    _showAlert({
        //        content: "Please select the type of the file <b>'GLMS Transcript'</b> or <b>'Udemy Training Catalog'</b>."
        //    });
        //}
    });
}

function loadDocumentByResponsible() {
    var whereSQL = "";
    if ($("#HFNotificationStatus").val()) {
        whereSQL = "WHERE ( [Year] = " + $("#selPeriod").find("option:selected").attr("year") + " AND [Month] = " + $("#selPeriod").find("option:selected").attr("month") + " AND ([NotificationStatus] ='" + $("#HFNotificationStatus").val() + "') AND [ResponsilbleSOEID] LIKE '%" + $("#HFSOEIDResponsible").val() + "%')";
    } else {
        whereSQL = "WHERE ( [Year] = " + $("#selPeriod").find("option:selected").attr("year") + " AND [Month] = " + $("#selPeriod").find("option:selected").attr("month") + " AND [ResponsilbleSOEID] LIKE '%" + $("#HFSOEIDResponsible").val() + "%')";
    }

    $.jqxGridApi.create({
        showTo: "#tblDocs",
        options: {
            //for comments or descriptions
            height: "450",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: true,
            sortable: true,
            editable: true
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set"
        },
        sp: {

            Name: "[dbo].[spSDocPeriodListPaging]",
            Params: [
                //{ Name: "@StrWhere", Value: "WHERE ( [Year] = " + $("#selPeriod").find("option:selected").attr("year") + " AND [Month] = " + $("#selPeriod").find("option:selected").attr("month") + " AND ([FileStatus] = 'Not Received' OR [StatusByPO] = 'Rejected') AND [ResponsilbleSOEID] LIKE '%" + $("#HFSOEIDResponsible").val() + "%')" },
                { Name: "@StrWhere", Value: whereSQL },
                { Name: "@EndIndexRow", Value: 100000 }
            ],
        },
        columns: [
            //type: string - text - number - int - float - date - time 
            //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
            //cellsformat: ddd, MMM dd, yyyy h:mm tt
            { name: 'IDDoc', type: 'number', hidden: true },
            { name: 'IDDocPeriod', type: 'number', hidden: true },
            { name: 'JsonHistory', type: 'string', hidden: true },
            { name: 'ProofOwnerSOEID', type: 'string', hidden: true },
            {
                name: 'FileStatus', text: 'File Status', width: '145px', type: 'string', pinned: true, filtertype: 'checkedlist', editable: false, filterable: true, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                    var dataRecord = $("#tblDocs").jqxGrid('getrowdata', rowIndex);
                    var htmlResult = '';

                    //Add Status
                    switch (dataRecord.FileStatus) {
                        case "Received":
                            htmlResult += '<span class="badge badge-md badge-info" style="margin-top: 5px; margin-left: 5px;"> Received </span>';
                            break;

                        case "Not Received":
                            htmlResult += '<span class="badge badge-md badge-warning" style="margin-top: 5px; margin-left: 5px;"> Not Received </span>';
                            break;

                        default:
                            htmlResult += '<span class="badge badge-md badge-warning" style="margin-top: 5px; margin-left: 5px;">' + dataRecord.FileStatus + '</span>';
                            break;
                    }

                    //Add History
                    if (dataRecord.JsonHistory) {
                        var historyRows = JSON.parse(dataRecord.JsonHistory);
                        if (historyRows.length > 0) {
                            var historyInfoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                            for (var i = 0; i < historyRows.length; i++) {
                                var tempData = historyRows[i];

                                historyInfoHtml += "<b>User:</b> (" + tempData.CreatedBySOEID + ") " + tempData.CreatedByName + "<br>";
                                historyInfoHtml += "<b>File Status:</b> " + tempData.FileStatus + "<br>";
                                historyInfoHtml += "<b>Status By PO:</b> " + tempData.StatusByPO + "<br>";

                                if (tempData.Comment) {
                                    historyInfoHtml += "<b>Date:</b> " + tempData.CreatedDate + "<br>";
                                    historyInfoHtml += "<b>Comment:</b> " + tempData.Comment + " <br><br>";
                                } else {
                                    historyInfoHtml += "<b>Date:</b> " + tempData.CreatedDate + "<br><br>";
                                }
                            }

                            historyInfoHtml += "</div>";

                            htmlResult +=
                                '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="right" data-content="' + historyInfoHtml + '" data-title="History Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png"/> ' +
                                '</div>';
                        }
                    }

                    return htmlResult;
                }
            },
            {
                name: 'StatusByPO', text: 'Status By PO', width: '100px', type: 'string', pinned: true, filtertype: 'checkedlist', editable: false, filterable: true, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                    var dataRecord = $("#tblDocs").jqxGrid('getrowdata', rowIndex);
                    var htmlResult = '';

                    switch (dataRecord.StatusByPO) {
                        case "Approved":
                            htmlResult = '<span class="badge badge-md badge-success" style="margin-top: 5px; margin-left: 5px;"> Approved </span>';
                            break;

                        case "Rejected":
                            htmlResult = '<span class="badge badge-md badge-danger" style="margin-top: 5px; margin-left: 5px;"> Rejected </span>';
                            break;

                        default:
                            htmlResult = '<span class="badge badge-md badge-warning" style="margin-top: 5px; margin-left: 5px;">' + dataRecord.StatusByPO + '</span>';
                            break;
                    }

                    return htmlResult;
                }
            },
            { name: 'FullKey', text: 'Full Key', width: '350px', type: 'string', filtertype: 'input' },
            { name: 'FileName', text: 'File Name', width: '350px', type: 'string', filtertype: 'input' },
            { name: 'Extension', text: 'Ext', width: '50px', type: 'string', filterable: 'input' },
            { name: 'FileFolderPath', text: 'Location to upload', width: '1000px', type: 'string', filtertype: 'input' },
            { name: 'BU', text: 'BU', type: 'string', width: '70px', filterable: 'input' },
            { name: "Balance", text: "Balance", width: '150px', type: 'float', filtertype: 'number', cellsformat: 'd' },
            { name: 'Year', text: 'Year', width: '70px', type: 'string', filtertype: 'input' },
            { name: 'Month', text: 'Month', width: '70px', type: 'string', filtertype: 'input' }
        ],
        ready: function () {
            //On select row event
            //$("#tblMakerCheckerIssues").on('rowselect', function (event) {
            //    $(".btnViewIssue").attr("href", _getViewVar("SubAppPath") + "/MEIL/MakerChecker/AdminIssue?pissueID=" + event.args.row.IssueID);
            //});

            var $jqxGrid = $("#tblDocs");

            //Add Popup to see comments
            _GLOBAL_SETTINGS.tooltipsPopovers();
            $jqxGrid.on("rowclick", function (event) {
                _GLOBAL_SETTINGS.tooltipsPopovers();
            });
        }
    });
}

function checkFileExistByResponsible(fnOnSuccess) {

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Checking documents in Share Folder...",
        url: '/SupportDoc/CheckDocumentsByResponsible',
        data: {
            year: $("#selPeriod").find("option:selected").attr("year"),
            month: $("#selPeriod").find("option:selected").attr("month"),
            responsibleSOEID: $("#HFSOEIDResponsible").val(),
            notificationStatus: $("#HFNotificationStatus").val(),
            typeAuthentication: _getTypeAuthentication(),
            user: _encodeSkipSideminder(_getWindowsUser()),
            pass: _getWindowsPassword()
        },
        success: function (resultMsg) {
            if (fnOnSuccess) {
                _showNotification("info", resultMsg);
                fnOnSuccess();
            }
        }
    });

    //Faster option
    //_callProcedure({
    //    loadingMsgType: "fullLoading",
    //    loadingMsg: "Checking documents in Share Folder...",
    //    name: "[dbo].[spSDocCheckFileExists]",
    //    params: [
    //        { "Name": "@SessionSOEID", "Value": _getSOEID() },
    //        { "Name": "@Year", "Value": $("#selPeriod").find("option:selected").attr("year") },
    //        { "Name": "@Month", "Value": $("#selPeriod").find("option:selected").attr("month") }
    //    ],
    //    success: {
    //        fn: function (responseList) {
    //            if (fnOnSuccess) {
    //                fnOnSuccess();
    //            }
    //        }
    //    }
    //});
}