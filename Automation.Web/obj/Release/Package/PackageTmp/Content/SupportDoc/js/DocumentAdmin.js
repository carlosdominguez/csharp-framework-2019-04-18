/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="SupportDocGlobal.js" />

var regionList = {};

$(document).ready(function () {
    var strFiltersTable = "Year;Month";

    // Load Periods
    _loadMonths({
        showTo: "#selPeriod"
    });
    _loadPeriodRecords();

    //On Change Period
    $("#selPeriod").change(function () {
        //Load table
        _loadDocumentTableList(_getCurrentFilters(strFiltersTable));
    });

    //Load table
    _loadDocumentTableList(_getCurrentFilters(strFiltersTable));

    //Create to import excel
    XLSX.createJsXlsx({
        idElement: "js-xlsx-plugin",
        fileInput: {
            dropText: "Drop an Excel file here...",
            hideDrop: false,
            inputText: "... or click here to select a file"
        },
        onSuccess: function (excelSource) {

            if (isValidExcel(excelSource)) {
                _callServer({
                    loadingMsgType: "fullLoading",
                    loadingMsg: "Uploading data...",
                    url: '/SupportDoc/UploadDocData',
                    data: {
                        'pjsonData': JSON.stringify(excelSource["Data Base"]),
                        'pyear': $("#selPeriod").find("option:selected").attr("year"),
                        'pmonth': $("#selPeriod").find("option:selected").attr("month")
                    },
                    type: "post",
                    success: function (json) {
                        //Clear input file
                        $(".jsxlsx-input-file").val("");

                        //Check if file exist
                        _checkFileExistsByYearMonth(function () {
                            //Load table
                            _loadDocumentTableList(_getCurrentFilters(strFiltersTable));

                            //Loading Periods Records
                            _loadPeriodRecords();
                        });
                    }
                });
            } else {
                //Clear input file
                $(".jsxlsx-input-file").val("");
            }
        }
    });

    //Load regions information
    loadRegionsInfo();
});

function isValidExcel(excelSource) {
    var isValid = true;
    var allowedColumns = [
        { "Country": false, "Region": true },
        { "FullKey": true },
        { "Balance": true },
        { "Status": true },
        { "FileName": false, "Support documentation File Name": true },
        { "Extension": true, "Extention": false },
        { "ReconciliationType": false, "Reconciliation Type": true },
        { "Contraloria": true },
        { "BU": false, "Unidad de Negocio": true },
        { "ProofOwnerDesc": false, "Proof Owner": true},
        { "ProofOwnerSOEID": false, "Proof Owner SOEID": true },
        { "ProofOwnerBackupDesc": false, "Proof Owner Back Up": false, "Proof Owner Backup": true },
        { "ProofOwnerBackupSOEID": false, "Proof Owner BackUp SOEID": false, "Proof Owner Backup SOEID": true },
        { "ResponsilbleDesc": false, "Responsible": true },
        { "ResponsilbleSOEID": false, "Responsible SOEID": true },
        { "AccountOwnerDesc": false, "Account Owner": true },
        { "AccountOwnerSOEID": false, "Account Owner SOEID": true },
        { "EscalationDB8": true, Ignore: 1 },
        { "EscalationDB10": true, Ignore: 1 },
        { "EscalationDB12": true, Ignore: 1 }
    ];

    //Validate if Sheet with name "Data Base" exists
    if (!excelSource["Data Base"]) {
        _showAlert({
            type: 'error',
            content: "Please review the Excel file that exists the Sheet Name '<b>Data Base</b>' with the data to upload.",
            animateScrollTop: true
        });

        isValid = false;
    }

    //Validate if "Region" Column  exists
    if (isValid && !excelSource["Data Base"][0]["Region"]) {
        _showAlert({
            type: 'error',
            content: "Please review the Excel file that exists '<b>Region</b>' in the First Column.",
            animateScrollTop: true
        });
        isValid = false;
    }

    //Validate "Region" valid data
    if (isValid) {
        for (var i = 0; i < excelSource["Data Base"].length; i++) {
            if (!regionList[excelSource["Data Base"][i]["Region"]]) {
                var strRegions = "";
                var regionsKeys = Object.keys(regionList);
                for (var i = 0; i < regionsKeys.length; i++) {
                    strRegions += " - " + regionList[regionsKeys[i]].Name + " <br>";
                }
                _showAlert({
                    type: 'error',
                    content: "Please review the Excel file in the colum '<b>Region</b>' only the following options are allowed: <br> " + strRegions,
                    animateScrollTop: true
                });
                isValid = false;
                break;
            }
        }
    }

    //Validate columns
    if (isValid) {
        var excelColumnsKeys = Object.keys(excelSource["Data Base"][0]);
        var missingCols = [];
        var extraCols = [];

        //Validate if the colums allowed are defined in the excel data
        for (var i = 0; i < allowedColumns.length; i++) {
            var allowedColumExist = false;
            var objAllowCol = allowedColumns[i];
            var keysAllowColNames = Object.keys(objAllowCol);
            var defaultKeyAllowColName = "";

            //When is EscalationDB8, EscalationDB10, EscalationDB12 ignore because sometimes
            //comes empty in the array of excel but exist in the excel file
            if (objAllowCol.Ignore && objAllowCol.Ignore == 1) {
                allowedColumExist = true;
            } else {
                for (var j = 0; j < keysAllowColNames.length && allowedColumExist == false; j++) {
                    var keyAllowColName = keysAllowColNames[j];

                    //Set default Key
                    if (objAllowCol[keyAllowColName]) {
                        defaultKeyAllowColName = keyAllowColName;
                    }

                    //Look if allowed column exist in excel columns
                    for (var k = 0; k < excelColumnsKeys.length; k++) {
                        var excelCol = excelColumnsKeys[k];

                        if (excelCol.toLocaleLowerCase() == keyAllowColName.toLocaleLowerCase()) {
                            allowedColumExist = true;
                            break;
                        }
                    }
                }
            }

            if (allowedColumExist == false) {
                missingCols.push(defaultKeyAllowColName);
            }
        }

        //Validate if the colums of the excel are allowed colums
        for (var i = 0; i < excelColumnsKeys.length; i++) {
            var excelCol = excelColumnsKeys[i];
            var excelColAllowed = false;

            for (var i = 0; i < allowedColumns.length && excelColAllowed == false; i++) {
                var objAllowCol = allowedColumns[i];
                var keysAllowColNames = Object.keys(objAllowCol);

                for (var j = 0; j < keysAllowColNames.length; j++) {
                    var keyAllowColName = keysAllowColNames[j];

                    if (excelCol.toLocaleLowerCase() == keyAllowColName.toLocaleLowerCase()) {
                        excelColAllowed = true;
                        break;
                    }
                }
            }

            if (excelColAllowed == false) {
                extraCols.push(excelCol);
            }
        }

        if (extraCols.length > 0 || missingCols.length > 0) {
            var msg = "Excel file must have the following columns: <br>";

            for (var i = 0; i < allowedColumns.length; i++) {
                var objAllowCol = allowedColumns[i];
                var keysAllowColNames = Object.keys(objAllowCol);

                for (var j = 0; j < keysAllowColNames.length; j++) {
                    var keyAllowColName = keysAllowColNames[j];

                    //Set default Key
                    if (objAllowCol[keyAllowColName] === true) {
                        msg += " - " + keyAllowColName + " <br>";
                    }
                }
            }

            msg += "<br>";
            if (extraCols.length > 0) {
                msg += "Please remove the following extra columns in the Excel file (review hidden colums): <br>";

                for (var i = 0; i < extraCols.length; i++) {
                    msg += " - " + extraCols[i] + "<br>";
                }
            }

            if (missingCols.length > 0) {
                msg += "Please add the following columns in the Excel file: <br>";

                for (var i = 0; i < missingCols.length; i++) {
                    msg += " - " + missingCols[i] + "<br>";
                }
            }

            _showAlert({
                type: 'error',
                content: msg,
                animateScrollTop: true
            });
            isValid = false;
        }
    }
    
    return isValid;
}

function loadRegionsInfo() {
    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Getting regions...",
        name: "[dbo].[spSDocAdminRegion]",
        params: [
            { "Name": "@Action", "Value": "List" }
        ],
        success: {
            fn: function (responseList) {
                if(responseList.length > 0){
                    for (var i = 0; i < responseList.length; i++) {
                        regionList[responseList[i]['Region']] = {
                            ID: responseList[i]['IDRegion'],
                            Name: responseList[i]['Region']
                        };
                    }
                }else{
                    _showAlert({
                        type: 'error',
                        content: "Please go to 'Regions & Emails' in the menu and add one Region at least.",
                        animateScrollTop: true
                    });
                }
            }
        }
    });
}