/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="SupportDocGlobal.js" />
var idJqxGridRegion = "#tblRegion";
var idJqxGridEmailTemplate = "#tblEmailTemplate";

$(document).ready(function () {
    _hideMenu();

    //=============================================
    // REGIONS
    //=============================================
    //Add New row to Region
    $(".btnAddNewRegion").click(function () {
        addEditRegion();
    });

    //Edit row to Region
    $(".btnEditRegion").click(function () {
        var objRowSelected = $.jqxGridApi.getOneSelectedRow(idJqxGridRegion, true);
        if (objRowSelected) {
            addEditRegion(objRowSelected);
        }
    });

    //Delete selected row to Region
    $(".btnDeleteRegion").click(function () {
        deleteRowRegion();
    });

    loadTableRegion();

    //=============================================
    // EMAIL TEMPLATES
    //=============================================
    //Add New row to EmailTemplate
    $(".btnAddEditEmailTemplate").click(function () {
        addEditEmailTemplate();
    });

    //On Click Clear
    $(".btnClearEmailTemplate").click(function () {
        $("#txtIDEmailTemplate").val(0);
        $("#txtTemplateName").val("");
        $("#txtEmailDL").val("");
        $("#txtEmailSubject").val("");
    });

    //Edit row to EmailTemplate
    $(".btnViewEmailTemplate").click(function () {
        var objRowSelected = $.jqxGridApi.getOneSelectedRow(idJqxGridEmailTemplate, true);
        if (objRowSelected) {
            loadInfoSelectedEmailTemplate(objRowSelected);
        }
    });

    //Delete selected row to EmailTemplate
    $(".btnDeleteEmailTemplate").click(function () {
        deleteRowEmailTemplate();
    });

    loadTableEmailTemplate();

});

//=============================================
// REGIONS
//=============================================
function loadTableRegion() {
    $.jqxGridApi.create({
        showTo: idJqxGridRegion,
        options: {
            //for comments or descriptions
            height: "400",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: true,
            sortable: true,
            editable: true
        },
        sp: {
            Name: "[dbo].[spSDocAdminRegion]",
            Params: [
                { Name: "@Action", Value: "List" }
            ]
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        columns: [
            //type: string - text - number - int - float - date - time 
            //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
            //cellsformat: ddd, MMM dd, yyyy h:mm tt
            { name: 'IDRegion', type: 'number', hidden: true },
            { name: 'Region', text: 'Region', width: '20%', type: 'string', filtertype: 'checkedlist' },
            { name: 'TemplateName', text: 'EmailTemplate', width: '20%', type: 'string', filtertype: 'checkedlist' },
            { name: 'ShareFolderPath', text: 'Share Folder Path', width: '60%', type: 'string', filtertype: 'checkedlist' }
        ],
        ready: function () { }
    });
}

function addEditRegion(objRegion) {
    if (!objRegion) {
        objRegion = {
            IDRegion: 0,
            Region: "",
            IDEmailTemplate: 0,
            ShareFolderPath: "\\\\sjovnasfro0002.wlb.lac.nsroot.net\\froqt0018\\BSS_MEX_O&T\\Support Documentation\\"
        };
    }

    var formHtml =
        '<div class="form-horizontal"> ' +
        '    <div class="row"> ' +
        '        <div class="col-md-6"> ' +
        '            <div class="form-group"> ' +
        '                <label class="col-md-4 control-label"> ' +
        '                    <i class="fa fa-asterisk iconasterisk"></i> Region Name</label> ' +
        '                <div class="col-md-8"> ' +
        '                    <input type="text" id="txtRegion" value="' + objRegion.Region + '" class="form-control"> ' +
        '                </div> ' +
        '            </div> ' +
        '        </div> ' +
        '        <div class="col-md-6"> ' +
        '            <div class="form-group"> ' +
        '                <label class="col-md-4 control-label"> ' +
        '                    <i class="fa fa-asterisk iconasterisk"></i> Email Template ' +
        '                </label> ' +
        '                <div class="col-md-8"> ' +
        '                    <select id="selIDEmailTemplate" class="form-control"> ' +
        '                        <option value="English">English</option> ' +
        '                        <option value="Spanish">Spanish</option> ' +
        '                        <option value="Portuguese">Portuguese</option> ' +
        '                    </select> ' +
        '                </div> ' +
        '            </div> ' +
        '        </div> ' +
        '        <div class="col-md-12"> ' +
        '            <div class="form-group"> ' +
        '                <label class="col-md-2 control-label"> ' +
        '                    <i class="fa fa-asterisk iconasterisk"></i> Share Folder Path </label> ' +
        '                <div class="col-md-10"> ' +
        '                    <input type="text" id="txtShareFolder" value="' + objRegion.ShareFolderPath + '" class="form-control"> ' +
        '                    <button type="button" class="btnCheckAccess btn btn-success top15 left15 bottom15 pull-right"><i class="fa fa-unlock"></i> Check access</button> ' +
        '                </div> ' +
        '            </div> ' +
        '        </div> ' +
        '    </div> ' +
        '</div>'; 

    _showModal({
        width: "75%",
        title: "Region Information",
        contentHtml: formHtml,
        buttons: [{
            name: "Save",
            class: "btn-success",
            closeModalOnClick: true,
            onClick: function ($modal) {
                saveTableRegion({
                    IDRegion: objRegion.IDRegion,
                    Region: $modal.find("#txtRegion").val(),
                    IDEmailTemplate: ($modal.find("#selIDEmailTemplate").val() ? $modal.find("#selIDEmailTemplate").val() : 0),
                    ShareFolderPath: $modal.find("#txtShareFolder").val()
                })
            }
        }],
        addCloseButton: true,
        onReady: function () {
            //Load Email Templates
            loadSelectEmailTemplate(objRegion.IDEmailTemplate);

            //On click Check access
            $(".btnCheckAccess").click(function () {
                checkAccess();
            });
        }
    });
}

function loadSelectEmailTemplate(selectedId) {
    var idElement = "selIDEmailTemplate";
    var SQLQuery = "                          \
        SELECT                                \
            ET.[ID],                          \
            ET.[TemplateName] AS [Text]       \
        FROM                                  \
            [dbo].[tblSDocEmailTemplate] ET   \
        WHERE                                 \
            ET.[IsDeleted] = 0 ";

    _callServer({
        loadingMsgType: "topBar",
        loadingMsg: "Loading select Email Template...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify(SQLQuery) },
        type: "post",
        success: function (resultList) {
            $("#" + idElement).contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objItem = resultList[i];
                $("#" + idElement + "").append(
                    $('<option>', {
                        value: objItem.ID,
                        text: objItem.Text,
                        selected: (objItem.ID == selectedId)
                    })
                );
            }
        }
    });
};

function deleteRowRegion() {
    //Delete 
    var objRowSelected = $.jqxGridApi.getOneSelectedRow(idJqxGridRegion, true);
    if (objRowSelected) {
        var htmlContentModal = "";
        htmlContentModal += "<b>Region: </b>" + objRowSelected['Region'] + "<br/>";
        htmlContentModal += "<b>Email Template: </b>" + objRowSelected['TemplateName'] + "<br/>";
        htmlContentModal += "<b>Share Folder Path: </b>" + objRowSelected['ShareFolderPath'] + "<br/>";

        _showModal({
            width: '40%',
            modalId: "modalDelRow",
            addCloseButton: true,
            buttons: [{
                name: "Delete",
                class: "btn-danger",
                onClick: function () {
                    _callProcedure({
                        loadingMsgType: "topBar",
                        loadingMsg: "Deleting region...",
                        name: "[dbo].[spSDocAdminRegion]",
                        params: [
                            { "Name": "@Action", "Value": "Delete" },
                            { "Name": "@IDRegion", "Value": objRowSelected.IDRegion },
                            { "Name": "@SessionSOEID", "Value": _getSOEID() }
                        ],
                        //Show message only for the last 
                        success: {
                            showTo: $(idJqxGridRegion).parent(),
                            msg: "Row was deleted successfully.",
                            fn: function () {
                                loadTableRegion();
                            }
                        }
                    });
                }
            }],
            title: "Are you sure you want to delete this row?",
            contentHtml: htmlContentModal
        });
    }
}

function saveTableRegion(objRegion) {
    var action = "Edit";

    if (objRegion.IDRegion == 0) {
        var action = "Insert";
    }

    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Saving region...",
        name: "[dbo].[spSDocAdminRegion]",
        params: [
            { "Name": "@Action", "Value": action },
            { "Name": "@IDRegion", "Value": objRegion.IDRegion },
            { "Name": "@Region", "Value": objRegion.Region },
            { "Name": "@IDEmailTemplate", "Value": objRegion.IDEmailTemplate },
            { "Name": "@ShareFolderPath", "Value": objRegion.ShareFolderPath },
            { "Name": "@SessionSOEID", "Value": _getSOEID() }
        ],
        //Show message only for the last 
        success: {
            showTo: $(idJqxGridRegion).parent(),
            msg: "Region was saved successfully.",
            fn: function () {
                loadTableRegion();
            }
        }
    });
}

//=============================================
// EMAIL TEMPLATES
//=============================================
function loadTableEmailTemplate() {
    $.jqxGridApi.create({
        showTo: idJqxGridEmailTemplate,
        options: {
            //for comments or descriptions
            height: "500",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: true,
            sortable: true,
            editable: true
        },
        sp: {
            Name: "[dbo].[spSDocAdminEmailTemplate]",
            Params: [
                { Name: "@Action", Value: "List" }
            ]
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        columns: [
            //type: string - text - number - int - float - date - time 
            //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
            //cellsformat: ddd, MMM dd, yyyy h:mm tt
            { name: 'IDEmailTemplate', type: 'number', hidden: true },
            { name: 'EmailDL', type: 'string', hidden: true },
            { name: 'EmailSubject', type: 'string', hidden: true },
            { name: 'TPTitle', type: 'string', hidden: true },
            { name: 'TPParagraphInformation', type: 'string', hidden: true },
            { name: 'TPCitiSharedServices', type: 'string', hidden: true },
            { name: 'TPLabelResponsible', type: 'string', hidden: true },
            { name: 'TPLabelPO', type: 'string', hidden: true },
            { name: 'TPLabelReason', type: 'string', hidden: true },
            { name: 'TPReason', type: 'string', hidden: true },
            { name: 'TPLabelPendingDocs', type: 'string', hidden: true },
            { name: 'TPParagraphPleaseClick', type: 'string', hidden: true },
            { name: 'TPLink', type: 'string', hidden: true },
            { name: 'TemplateName', text: 'Template Name', width: '100%', type: 'string', filtertype: 'checkedlist' }
        ],
        ready: function () { }
    });
}

function addEditEmailTemplate() {
    saveTableEmailTemplate({
        IDEmailTemplate: $("#txtIDEmailTemplate").val(),
        TemplateName: $("#txtTemplateName").val(),
        EmailDL: $("#txtEmailDL").val(),
        EmailSubject: $("#txtEmailSubject").val(),
        TPTitle: $("#TPTitle").text(),
        TPParagraphInformation: $("#TPParagraphInformation").text(),
        TPCitiSharedServices: $("#TPCitiSharedServices").text(),
        TPLabelResponsible: $("#TPLabelResponsible").text(),
        TPLabelPO: $("#TPLabelPO").text(),
        TPLabelReason: $("#TPLabelReason").text(),
        TPReason: $("#TPReason").text(),
        TPLabelPendingDocs: $("#TPLabelPendingDocs").text(),
        TPParagraphPleaseClick: $("#TPParagraphPleaseClick").text(),
        TPLink: $("#TPLink").text()
    })
}

function loadInfoSelectedEmailTemplate(objEmailTemplate) {
    $("#txtIDEmailTemplate").val(objEmailTemplate.IDEmailTemplate);
    $("#txtTemplateName").val(objEmailTemplate.TemplateName);
    $("#txtEmailDL").val(objEmailTemplate.EmailDL);
    $("#txtEmailSubject").val(objEmailTemplate.EmailSubject);
    $("#TPTitle").text(objEmailTemplate.TPTitle);
    $("#TPParagraphInformation").text(objEmailTemplate.TPParagraphInformation);
    $("#TPCitiSharedServices").text(objEmailTemplate.TPCitiSharedServices);
    $("#TPLabelResponsible").text(objEmailTemplate.TPLabelResponsible);
    $("#TPLabelPO").text(objEmailTemplate.TPLabelPO);
    $("#TPLabelReason").text(objEmailTemplate.TPLabelReason);
    $("#TPReason").text(objEmailTemplate.TPReason);
    $("#TPLabelPendingDocs").text(objEmailTemplate.TPLabelPendingDocs);
    $("#TPParagraphPleaseClick").text(objEmailTemplate.TPParagraphPleaseClick);
    $("#TPLink").text(objEmailTemplate.TPLink);
}

function deleteRowEmailTemplate() {
    //Delete 
    var objRowSelected = $.jqxGridApi.getOneSelectedRow(idJqxGridEmailTemplate, true);
    if (objRowSelected) {
        var htmlContentModal = "";
        htmlContentModal += "<b>Template Name: </b>" + objRowSelected['TemplateName'] + "<br/>";

        _showModal({
            width: '40%',
            modalId: "modalDelRow",
            addCloseButton: true,
            buttons: [{
                name: "Delete",
                class: "btn-danger",
                onClick: function () {
                    _callProcedure({
                        loadingMsgType: "topBar",
                        loadingMsg: "Deleting email template...",
                        name: "[dbo].[spSDocAdminEmailTemplate]",
                        params: [
                            { "Name": "@Action", "Value": "Delete" },
                            { "Name": "@IDEmailTemplate", "Value": objRowSelected.IDEmailTemplate },
                            { "Name": "@SessionSOEID", "Value": _getSOEID() }
                        ],
                        //Show message only for the last 
                        success: {
                            showTo: $(idJqxGridEmailTemplate).parent(),
                            msg: "Row was deleted successfully.",
                            fn: function () {
                                loadTableEmailTemplate();
                            }
                        }
                    });
                }
            }],
            title: "Are you sure you want to delete this row?",
            contentHtml: htmlContentModal
        });
    }
}

function saveTableEmailTemplate(objEmailTemplate) {
    var action = "Edit";

    if (objEmailTemplate.IDEmailTemplate == 0) {
        var action = "Insert";
    }

    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Saving Email Template...",
        name: "[dbo].[spSDocAdminEmailTemplate]",
        params: [
            { "Name": "@Action", "Value": action },
            { "Name": "@IDEmailTemplate", "Value": objEmailTemplate.IDEmailTemplate },
            { "Name": "@TemplateName", "Value": objEmailTemplate.TemplateName },
            { "Name": "@EmailDL", "Value": objEmailTemplate.EmailDL },
            { "Name": "@EmailSubject", "Value": objEmailTemplate.EmailSubject },
            { "Name": "@TPTitle", "Value": objEmailTemplate.TPTitle },
            { "Name": "@TPParagraphInformation", "Value": objEmailTemplate.TPParagraphInformation },
            { "Name": "@TPCitiSharedServices", "Value": objEmailTemplate.TPCitiSharedServices },
            { "Name": "@TPLabelResponsible", "Value": objEmailTemplate.TPLabelResponsible },
            { "Name": "@TPLabelPO", "Value": objEmailTemplate.TPLabelPO },
            { "Name": "@TPLabelReason", "Value": objEmailTemplate.TPLabelReason },
            { "Name": "@TPReason", "Value": objEmailTemplate.TPReason },
            { "Name": "@TPLabelPendingDocs", "Value": objEmailTemplate.TPLabelPendingDocs },
            { "Name": "@TPParagraphPleaseClick", "Value": objEmailTemplate.TPParagraphPleaseClick },
            { "Name": "@TPLink", "Value": objEmailTemplate.TPLink },
            { "Name": "@SessionSOEID", "Value": _getSOEID() }
        ],
        //Show message only for the last 
        success: {
            showTo: $(idJqxGridEmailTemplate).parent(),
            msg: "Email Template was saved successfully.",
            fn: function () {
                loadTableEmailTemplate();
            }
        }
    });
}

//=============================================
// OTHER
//=============================================
function checkAccess() {
    var pathToTest = $("#txtShareFolder").val();
    _callServer({
        loadingMsgType: "topBar",
        loadingMsg: "Checking access...",
        url: '/SupportDoc/CheckAccess',
        data: {
            typeAuthentication: _getTypeAuthentication(),
            user: _encodeSkipSideminder(_getWindowsUser()),
            pass: _getWindowsPassword(),
            pathToTest: _encodeSkipSideminder(pathToTest)
        },
        success: function (msg) {
            _showNotification("info", msg);
        }
    });
}
