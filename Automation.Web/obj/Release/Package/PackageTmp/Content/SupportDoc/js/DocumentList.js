/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="SupportDocGlobal.js" />
var strFiltersTable = "Year;Month;ProofOwnerSOEID;FileStatus;StatusByPO";

$(document).ready(function () {

    //Hide left Menu
    _hideMenu();

    // Load POs
    if ($("#HFViewType").val() == "PO") {
        _createSelectSOEID({ id: "#selPO", selSOEID: _getSOEID(), disabled: true });
    } else {
        _createSelectSOEID({ id: "#selPO", selSOEID: "" });
        strFiltersTable = "Year;Month;FileStatus;StatusByPO";
    }

    //On Change PO
    $("#selPO select").change(function () {
        //Load table
        _loadDocumentTableList(_getCurrentFilters(strFiltersTable));
    });

    // Load Periods
    _loadMonths({
        showTo: "#selPeriod"
    });
    _loadPeriodRecords();

    //On Change Period
    $("#selPeriod").change(function () {
        //Load table
        _loadDocumentTableList(_getCurrentFilters(strFiltersTable));

        //Check File status
        //_checkFileExistsByYearMonth(function () {  
        //  _loadDocumentTableList(_getCurrentFilters(strFiltersTable));
        //});
    });

    //On Change File Status
    $("#selFileStatus").change(function () {
        //Load table
        _loadDocumentTableList(_getCurrentFilters(strFiltersTable));
    });

    //On Change Status By PO
    $("#selStatusByPO").change(function () {
        //Load table
        _loadDocumentTableList(_getCurrentFilters(strFiltersTable));
    });

    //On click reload table
    $(".btnRefresh").click(function () {
        //Check File status
        _checkFileExistsByYearMonth(function () {
            //Load table
            _loadDocumentTableList(_getCurrentFilters(strFiltersTable));
        });
    });

    //On click approve document
    $(".btnApprove").click(function () {
        approveDoc();
    });

    //On click reject document
    $(".btnReject").click(function () {
        rejectDoc();
    });

    //On click download document
    $(".btnDownloadDoc").click(function (e) {
        downloadDoc(e);
    });

    //On Export Approved documents
    $(".btnExportApprovedDocs").click(function (e) {
        exportApproved();
    });

    //Check File status
    _checkFileExistsByYearMonth(function () {
        //Load table
        _loadDocumentTableList(_getCurrentFilters(strFiltersTable));
    });
});

function rejectDoc() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#tblDocs', true);
    if (rowData) {

        //Fix: Hide PO Select
        $(".select2-container").css("z-index", "0");

        var htmlContentModal = "<b>Name: </b> <br/>" + rowData.FileName + "<br/>";
        htmlContentModal += "<b>Path: </b> <br/>" + rowData.FileFullPath + "<br/>";
        htmlContentModal += "<b>Comment: </b> <br/><textarea id='txtCommentReject' style='width:100%;height: 130px;'></textarea><br/>";

        _showModal({
            width: '35%',
            modalId: "modalDelApp",
            addCloseButton: true,
            buttons: [{
                name: "Reject",
                class: "btn-danger",
                onClick: function () {

                    var commentReject = $("#txtCommentReject").val();

                    _callProcedure({
                        loadingMsgType: "topBar",
                        loadingMsg: "Rejecting document...",
                        name: "[dbo].[spSDocAdminStatus]",
                        params: [
                            { "Name": "@ServerURL", "Value": _getViewVar("ServerURL") },
                            { "Name": "@SessionSOEID", "Value": _getSOEID() },
                            { "Name": "@IDDocPeriod", "Value": rowData.IDDocPeriod },
                            { "Name": "@StatusByPO", "Value": 'Rejected' },
                            { "Name": "@Comment", "Value": commentReject }
                        ],
                        success: {
                            fn: function (responseList) {
                                //Create Log
                                _insertAuditLog({
                                    Action: "Rejected Doc",
                                    Description: rowData.FileFullPath
                                });

                                //Update all rows in the grid
                                //var allRows = $.jqxGridApi.getAllRows('#tblDocs');
                                var allRows = $('#tblDocs').jqxGrid('getRows');
                                for (var i = 0; i < allRows.length; i++) {
                                    var objTempRow = allRows[i];

                                    if (objTempRow && objTempRow.FileName == rowData.FileName && objTempRow.Extension == rowData.Extension) {
                                        objTempRow.StatusByPO = 'Rejected';

                                        objHistory = {
                                            IDDocPeriod: rowData.IDDocPeriod,
                                            FileStatus: rowData.FileStatus,
                                            StatusByPO: 'Rejected',
                                            Comment: commentReject,
                                            CreatedByName: _getUserInfo().Name,
                                            CreatedBySOEID: _getUserInfo().SOEID,
                                            CreatedDate: moment().format('MMM DD YYYY h:mmA')
                                        };

                                        var history = JSON.parse(objTempRow.JsonHistory);
                                        history.push(objHistory);

                                        objTempRow.JsonHistory = JSON.stringify(history);

                                        //Set new value
                                        $('#tblDocs').jqxGrid('updaterow', objTempRow.boundindex, objTempRow);
                                    }
                                }

                                //Create Log of Rejected Notification
                                _insertAuditLog({
                                    Action: "Rejected Notification",
                                    Description: rowData.ResponsilbleDesc + " (" + rowData.ResponsilbleSOEID + ")"
                                });

                                _showNotification("success", "Document '" + rowData.FileName + "." + rowData.Extension + "' was rejected successfully and notified to " + rowData.ResponsilbleDesc + " (" + rowData.ResponsilbleSOEID + ").")
                            }
                        }
                    });
                }
            }],
            title: "Are you sure you want to approve this row?",
            contentHtml: htmlContentModal
        });
    }
}

function approveDoc() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#tblDocs', true);
    if (rowData) {

        //Fix: Hide PO Select
        $(".select2-container").css("z-index", "0");

        var htmlContentModal = "<b>Name: </b>" + rowData.FileName + "<br/>";
        htmlContentModal += "<b>Path: </b>" + rowData.FileFullPath + "<br/>";

        _showModal({
            width: '35%',
            modalId: "modalDelApp",
            addCloseButton: true,
            buttons: [{
                name: "Approved",
                class: "btn-success",
                onClick: function () {
                    _callProcedure({
                        loadingMsgType: "topBar",
                        loadingMsg: "Approving document...",
                        name: "[dbo].[spSDocAdminStatus]",
                        params: [
                            { "Name": "@ServerURL", "Value": _getViewVar("ServerURL") },
                            { "Name": "@SessionSOEID", "Value": _getSOEID() },
                            { "Name": "@IDDocPeriod", "Value": rowData.IDDocPeriod },
                            { "Name": "@StatusByPO", "Value": 'Approved' },
                            { "Name": "@Comment", "Value": '' }
                        ],
                        success: {
                            fn: function (responseList) {
                                //Create Log
                                _insertAuditLog({
                                    Action: "Approved Doc",
                                    Description: rowData.FileFullPath
                                });

                                //Update all rows in the grid
                                //var allRows = $.jqxGridApi.getAllRows('#tblDocs');
                                var allRows = $('#tblDocs').jqxGrid('getRows');
                                for (var i = 0; i < allRows.length; i++) {
                                    var objTempRow = allRows[i];

                                    if (objTempRow && objTempRow.FileName == rowData.FileName && objTempRow.Extension == rowData.Extension) {
                                        objTempRow.StatusByPO = 'Approved';

                                        objHistory = {
                                            IDDocPeriod: rowData.IDDocPeriod,
                                            FileStatus: rowData.FileStatus,
                                            StatusByPO: 'Approved',
                                            Comment: '',
                                            CreatedByName: _getUserInfo().Name,
                                            CreatedBySOEID: _getUserInfo().SOEID,
                                            CreatedDate: moment().format('MMM DD YYYY h:mmA')
                                        };

                                        var history = JSON.parse(objTempRow.JsonHistory);
                                        history.push(objHistory);

                                        objTempRow.JsonHistory = JSON.stringify(history);

                                        //Set new value
                                        $('#tblDocs').jqxGrid('updaterow', objTempRow.boundindex, objTempRow);
                                    }
                                }
                                
                                _showNotification("success", "Document '" + rowData.FileName + "." + rowData.Extension + "' was approved successfully.")
                            }
                        }
                    });
                }
            }],
            title: "Are you sure you want to approve this row?",
            contentHtml: htmlContentModal
        });
    }
}

function downloadDoc(e) {
    e.preventDefault();

    var rowData = $.jqxGridApi.getOneSelectedRow('#tblDocs', true);
    if (rowData) {
        if (rowData.FileStatus == "Received") {

            _showNotification("success", "Downloading document '" + rowData.FileName + "' please wait...");

            setTimeout(function () {
                window.location.href = _getViewVar("SubAppPath") + "/SupportDoc/DownloadDoc/?" + $.param({
                    'docShareFolderFilePath': _encodeSkipSideminder(rowData.FileFullPath),
                    'fileName': _encodeSkipSideminder(rowData.FileName + '.' + rowData.Extension),
                    'ext': rowData.Extension,
                    'typeAuthentication': _getTypeAuthentication(),
                    'user': _encodeSkipSideminder(_getWindowsUser()),
                    'pass': _getWindowsPassword()
                });

                //Create Log
                _insertAuditLog({
                    Action: "Download File",
                    Description: rowData.FileFullPath
                });
            }, 1000);

            //_callServer({
            //    loadingMsgType: "topBar",
            //    loadingMsg: "Downloading document '" + rowData.FileName + "' please wait...",
            //    url: '/SupportDoc/DownloadDoc',
            //    data: {
            //        'docShareFolderFilePath': rowData.FileFullPath,
            //        'fileName': rowData.FileName + '.' + rowData.Extension
            //    },
            //    type: "post",
            //    success: function (urlToDownload) {
            //        if (urlToDownload) {
            //            window.open(_replaceAll("~", "", urlToDownload), '_blank');
            //        } else {
            //            _showNotification("error", "An error occurred while downloading the file.");
            //        }
            //    }
            //});
        } else {
            _showNotification("error", "File not exists in '" + rowData.FileFullPath + "'");
        }
    }
}

function exportApproved() {
    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Getting Approved Documents...",
        name: "[dbo].[spSDocPeriodListPaging]",
        params: [
            { "Name": "@StrWhere", "Value": "WHERE ( [Year] = " + $("#selPeriod").find("option:selected").attr("year") + " AND [Month] = " + $("#selPeriod").find("option:selected").attr("month") + " AND [FileStatus] = 'Received' AND [StatusByPO] = 'Approved')" },
            { "Name": "@EndIndexRow", "Value": 100000 }
        ],
        success: {
            fn: function (responseList) {
                
                if (responseList.length > 0) {
                    var distinctDocs = {};
                    var distinctRegionPaths = {};

                    //Fix: Hide PO Select, cannot move to function _showModal because it can hide select2 in modals
                    $(".select2-container").css("z-index", "0");

                    var htmlContentModal = "<b>Approved Documents:</b><pre>";

                    //Add Path of documents to export
                    for (var i = 0; i < responseList.length; i++) {
                        var objRowTemp = responseList[i];

                        //Only if not exists document to avoid duplicates
                        if (!distinctDocs[objRowTemp.FileFullPath]) {
                            htmlContentModal += objRowTemp.FileFullPath + " \n";
                            distinctDocs[objRowTemp.FileFullPath] = true;
                        }
                        
                        //Only if not exists path of region to export approved
                        if (!distinctRegionPaths[objRowTemp.RegionFolderPath]) {
                            distinctRegionPaths[objRowTemp.RegionFolderPath] = true;
                        }
                    }
                    htmlContentModal += "</pre>";

                    //Add Path of region to export
                    htmlContentModal += "<b>Export To:</b><pre>";
                    var regionPaths = Object.keys(distinctRegionPaths);
                    for (var i = 0; i < regionPaths.length; i++) {
                        htmlContentModal += regionPaths[i] + "Approved\\";
                    }
                    htmlContentModal += "</pre>";

                    _showModal({
                        width: '50%',
                        modalId: "modalDelApp",
                        addCloseButton: true,
                        buttons: [{
                            name: "Export",
                            class: "btn-success",
                            onClick: function () {

                                _callServer({
                                    loadingMsgType: "fullLoading",
                                    loadingMsg: "Exporting documents...",
                                    url: '/SupportDoc/ExportDocuments',
                                    data: {
                                        year: $("#selPeriod").find("option:selected").attr("year"),
                                        month: $("#selPeriod").find("option:selected").attr("month"),
                                        typeAuthentication: _getTypeAuthentication(),
                                        user: _encodeSkipSideminder(_getWindowsUser()),
                                        pass: _getWindowsPassword()
                                    },
                                    success: function (responseData) {
                                        if (responseData.success == "1") {
                                            //Create Log
                                            _insertAuditLog({
                                                Action: "Exported Approved Docs",
                                                Description: responseData.msg
                                            });

                                            htmlContentModal = "<b>Documents Copied:</b><pre>" + responseData.msg + "</pre>";

                                            _showModal({
                                                width: '50%',
                                                modalId: "modalConfirmApp",
                                                addCloseButton: true,
                                                buttons: [{
                                                    name: "Ok",
                                                    class: "btn-success",
                                                    onClick: function () {}
                                                }],
                                                title: "Documents were exported successfully",
                                                contentHtml: htmlContentModal
                                            });
                                        } else {
                                            _showNotification("error", responseData.msg);
                                        }
                                    }
                                });
                            }
                        }],
                        title: "Are you sure you want to export the following approved documents?",
                        contentHtml: htmlContentModal
                    });
                } else {
                    _showNotification("error", "No approved documents found");
                }
            }
        }
    });
}