/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="SupportDocGlobal.js" />

$(document).ready(function () {
    //Load table
    loadTableAuditLog();
});

function loadTableAuditLog() {
    $.jqxGridApi.create({
        showTo: "#tblAuditLog",
        options: {
            //for comments or descriptions
            height: "400",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: true,
            sortable: true,
            editable: true
        },
        sp: {
            Name: "[dbo].[spSDocAuditAdmin]",
            Params: [
                { Name: "@TypeAction", Value: "List" }
            ]
        },
        source: {
            dataBinding: "Large Data Set"
        },
        columns: [ 
            { name: 'Action', text: 'Action', width: '15%', type: 'string', filtertype: 'input' },
            { name: 'Description', text: 'Description', width: '35%', type: 'string', filtertype: 'input' },
            { name: 'CreatedBy', text: 'SOEID', width: '10%', type: 'string', filtertype: 'input' },
            { name: 'CreatedByName', text: 'Name', width: '20%', type: 'string', filtertype: 'input' },
            { name: 'CreatedDate', text: 'Date', width: '20%', type: 'string', filtertype: 'input' }
        ]
    });
}