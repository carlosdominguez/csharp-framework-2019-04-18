/// <reference path="../../Shared/plugins/util/global.js" />

function _loadPeriodRecords() {
    var sql = "                                                             \
        SELECT                                                              \
	        [Year], [Month] AS [MonthNumber],                               \
	        LEFT(DATENAME(MONTH,DATEADD(mm,[Month],-1)),3) AS [MonthName],  \
	        COUNT(1) [Rows]                                                 \
        FROM                                                                \
            [dbo].[tblSDocPeriod]                                           \
        WHERE                                                               \
            [IsDeleted] = 0                                                 \
        GROUP BY                                                            \
            [Year], [Month]                                                 \
        ORDER BY                                                            \
            [Year], [Month]";

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Periods Records...",
        url: '/Shared/ExecQuery',
        data: { 'pjsonSql': _toJSON(sql) },
        type: "post",
        success: function (resultList) {
            if (resultList) {
                for (var i = 0; i < resultList.length; i++) {
                    var objTemp = resultList[i];
                    var $tempOption = $("#selPeriod").find("option[year=" + objTemp.Year + "][month=" + objTemp.MonthNumber + "]");

                    if ($tempOption) {
                        var stringIndex = $tempOption.text().indexOf(" (");
                        if (stringIndex > 0) {
                            $tempOption.text($tempOption.text().substr(0, stringIndex));
                        }
                        $tempOption.text($tempOption.text() + ' (' + objTemp.Rows + ' records)');
                    }

                    //$("#selPeriod").append($('<option>', {
                    //    year: objTemp.Year,
                    //    month: objTemp.MonthNumber,
                    //    value: objTemp.MonthName + ' ' + objTemp.Year,
                    //    text: objTemp.MonthName + ' ' + objTemp.Year + ' (' + objTemp.Rows + ' records)'
                    //}));
                }
            }
        }
    });
}

function _loadDocumentTableList(filters) {
    $.jqxGridApi.create({
        showTo: "#tblDocs",
        options: {
            //for comments or descriptions
            height: "500",
            autoheight: false,
            autorowheight: false,
            showfilterrow: true,
            sortable: true,
            editable: true,
            //'singlerow', 'multiplerows', 'checkbox', 'multiplerowsadvanced' or 'multiplerowsextended'.
            selectionmode: "singlerow"
        },
        sp: {
            Name: "[dbo].[spSDocPeriodListPaging]",
            Params: [],
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Server Paging",
            pagesize: 200
        },
        groups: [],
        columns: [
            //type: string - text - number - int - float - date - time 
            //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
            //cellsformat: ddd, MMM dd, yyyy h:mm tt
            { name: 'IDDoc', type: 'number', hidden: true },
            { name: 'IDDocPeriod', type: 'number', hidden: true },
            { name: 'JsonHistory', type: 'string', hidden: true },
            { name: 'ProofOwnerSOEID', text: 'Proof Owner SOEID', type: 'string', hidden: true },
            { name: 'ResponsilbleDesc', text: 'Responsilble Desc', type: 'string', hidden: true },
            { name: 'ResponsilbleSOEID', text: 'Responsilble SOEID', type: 'string', hidden: true },
            {
                name: 'FileStatus', text: 'File Status', width: '145px', type: 'string', pinned: true, filtertype: 'input', editable: false, filterable: true, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                    var dataRecord = $("#tblDocs").jqxGrid('getrowdata', rowIndex);
                    var htmlResult = '';
                    
                    //Add Status
                    switch (dataRecord.FileStatus) {
                        case "Received":
                            htmlResult += '<span class="badge badge-md badge-info" style="margin-top: 5px; margin-left: 5px;"> Received </span>';
                            break;

                        case "Not Received":
                            htmlResult += '<span class="badge badge-md badge-warning" style="margin-top: 5px; margin-left: 5px;"> Not Received </span>';
                            break;

                        default:
                            htmlResult += '<span class="badge badge-md badge-warning" style="margin-top: 5px; margin-left: 5px;">' + dataRecord.FileStatus + '</span>';
                            break;
                    }

                    //Add History
                    if (dataRecord.JsonHistory) {
                        var historyRows = JSON.parse(dataRecord.JsonHistory);
                        if (historyRows.length > 0) {
                            var historyInfoHtml = "<div style='padding: 10px; font-size: 15px;'>";

                            for (var i = 0; i < historyRows.length; i++) {
                                var tempData = historyRows[i];

                                historyInfoHtml += "<b>User:</b> (" + tempData.CreatedBySOEID + ") " + tempData.CreatedByName + "<br>";
                                historyInfoHtml += "<b>File Status:</b> " + tempData.FileStatus + "<br>";
                                historyInfoHtml += "<b>Status By PO:</b> " + tempData.StatusByPO + "<br>";

                                if (tempData.Comment) {
                                    historyInfoHtml += "<b>Date:</b> " + tempData.CreatedDate + "<br>";
                                    historyInfoHtml += "<b>Comment:</b> " + tempData.Comment + " <br><br>";
                                } else {
                                    historyInfoHtml += "<b>Date:</b> " + tempData.CreatedDate + "<br><br>";
                                }
                            }

                            historyInfoHtml += "</div>";

                            htmlResult +=
                                '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="right" data-content="' + historyInfoHtml + '" data-title="History Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png"/> ' +
                                '</div>';
                        }
                    }

                    return htmlResult;
                }
            },
            {
                name: 'StatusByPO', text: 'Status By PO', width: '100px', type: 'string', pinned: true, filtertype: 'input', editable: false, filterable: true, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                    var dataRecord = $("#tblDocs").jqxGrid('getrowdata', rowIndex);
                    var htmlResult = '';

                    switch (dataRecord.StatusByPO) {
                        case "Approved":
                            htmlResult = '<span class="badge badge-md badge-success" style="margin-top: 5px; margin-left: 5px;"> Approved </span>';
                            break;

                        case "Rejected":
                            htmlResult = '<span class="badge badge-md badge-danger" style="margin-top: 5px; margin-left: 5px;"> Rejected </span>';
                            break;

                        default:
                            htmlResult = '<span class="badge badge-md badge-warning" style="margin-top: 5px; margin-left: 5px;">' + dataRecord.StatusByPO + '</span>';
                            break;
                    }

                    return htmlResult;
                }
            },
            
            { name: 'FileName', text: 'File Name', width: '400px', type: 'string', filtertype: 'input' },
            { name: 'Extension', text: 'Ext', width: '50px', type: 'string', filterable: 'input' },
            { name: 'BU', text: 'BU', type: 'string', width: '70px', filterable: 'input' },
            
            { name: 'FullKey', text: 'Full Key', width: '350px', type: 'string', filtertype: 'input' },
            { name: "Balance", text: "Balance", width: '150px', type: 'float', filtertype: 'number', cellsformat: 'd' },
            { name: 'FileFullPath', text: 'Folder Link', width: '1200px', type: 'string', filtertype: 'input' },
            { name: 'Year', text: 'Year', width: '70px', type: 'string', filtertype: 'input' },
            { name: 'Month', text: 'Month', width: '70px', type: 'string', filtertype: 'input' }
        ],
        ready: function () {
            //On select row event
            //$("#tblMakerCheckerIssues").on('rowselect', function (event) {
            //    $(".btnViewIssue").attr("href", _getViewVar("SubAppPath") + "/MEIL/MakerChecker/AdminIssue?pissueID=" + event.args.row.IssueID);
            //});
            
            var $jqxGrid = $("#tblDocs");

            //Add Popup to see comments
            _GLOBAL_SETTINGS.tooltipsPopovers();
            $jqxGrid.on("rowclick", function (event) {
                _GLOBAL_SETTINGS.tooltipsPopovers();
            });

            //Add Filters to table
            if (filters && filters.length > 0) {
                for (var i = 0; i < filters.length; i++) {
                    var tempFilter = filters[i];

                    if (tempFilter["FilterColumn"] && tempFilter["FilterValue"]) {
                        var filterGroup = new $.jqx.filter();
                        var filter_or_operator = 1;
                        var filterCondition = 'equal';
                        var filter = filterGroup.createfilter('stringfilter', tempFilter["FilterValue"], filterCondition);
                        filterGroup.addfilter(filter_or_operator, filter);

                        $jqxGrid.jqxGrid('addfilter', tempFilter["FilterColumn"], filterGroup);
                    }
                }

                $jqxGrid.jqxGrid('applyfilters');
            }
        }
    });
}

function _getCurrentFilters(strFilters) {
    //strFilters: Year;Month;ProofOwnerSOEID;FileStatus;StatusByPO
    var filters = [];
    var year = $("#selPeriod").find("option:selected").attr("year");
    var month = $("#selPeriod").find("option:selected").attr("month");
    var poSOEID = _getSOEID();
    var fileStatus = $("#selFileStatus").val();
    var statusByPO = $("#selStatusByPO").val();

    if (year && _contains(strFilters, "Year")) {
        filters.push({ FilterColumn: "Year", FilterValue: $("#selPeriod").find("option:selected").attr("year") });
    }

    if (month && _contains(strFilters, "Month")) {
        filters.push({ FilterColumn: "Month", FilterValue: $("#selPeriod").find("option:selected").attr("month") });
    }

    if (poSOEID && _contains(strFilters, "ProofOwnerSOEID")) {
        filters.push({ FilterColumn: "ProofOwnerSOEID", FilterValue: poSOEID });
    }

    if (fileStatus && _contains(strFilters, "FileStatus")) {
        if (fileStatus != "All") {
            filters.push({ FilterColumn: "FileStatus", FilterValue: fileStatus });
        }
    }

    if (statusByPO && _contains(strFilters, "StatusByPO")) {
        if (statusByPO != "All") {
            filters.push({ FilterColumn: "StatusByPO", FilterValue: statusByPO });
        }
    }

    return filters;
}

function _checkFileExistsByYearMonth(fnOnSuccess) {
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Checking documents in Share Folder...",
        url: '/SupportDoc/CheckDocumentsByYearMonth',
        data: {
            year: $("#selPeriod").find("option:selected").attr("year"),
            month: $("#selPeriod").find("option:selected").attr("month"),
            typeAuthentication: _getTypeAuthentication(),
            user: _encodeSkipSideminder(_getWindowsUser()),
            pass: _getWindowsPassword()
        },
        success: function (resultMsg) {
            if (fnOnSuccess) {
                _showNotification("info", resultMsg);
                fnOnSuccess();
            }
        }
    });

    //Faster option
    //_callProcedure({
    //    loadingMsgType: "fullLoading",
    //    loadingMsg: "Checking documents in Share Folder...",
    //    name: "[dbo].[spSDocCheckFileExists]",
    //    params: [
    //        { "Name": "@SessionSOEID", "Value": _getSOEID() },
    //        { "Name": "@Year", "Value": $("#selPeriod").find("option:selected").attr("year") },
    //        { "Name": "@Month", "Value": $("#selPeriod").find("option:selected").attr("month") }
    //    ],
    //    success: {
    //        fn: function (responseList) {
    //            if (fnOnSuccess) {
    //                fnOnSuccess();
    //            }
    //        }
    //    }
    //});
}

function _insertAuditLog(options) {
    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Creating audit log...",
        name: "[dbo].[spSDocAuditAdmin]",
        params: [
            { "Name": "@TypeAction", "Value": "Insert" },
            { "Name": "@AuditAction", "Value": options.Action },
            { "Name": "@Description", "Value": options.Description },
            { "Name": "@CreatedBy", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                if (options.onSuccess) {
                    options.onSuccess();
                }
            }
        }
    });
}

//------------------------------------------------------
//Authentication methods
//------------------------------------------------------
function _setWindowsUser(user) {
    localStorage.setItem("WindowsUser", user);
}

function _getWindowsUser() {
    var userID = "";
    if (localStorage.getItem("WindowsUser")) {
        userID = localStorage.getItem("WindowsUser");
    }

    return userID;
}

function _setWindowsPassword(pass) {
    localStorage.setItem("WindowsPassword", _encodeAsciiString(pass));
}

function _getWindowsPassword(decodePass) {
    var pass = "";
    if (localStorage.getItem("WindowsPassword")) {
        pass = localStorage.getItem("WindowsPassword");

        if (decodePass) {
            pass = _decodeAsciiString(pass);
        }
    }

    return pass;
}

function _setTypeAuthentication(auth) {
    localStorage.setItem("TypeAuthentication", auth);
}

function _getTypeAuthentication() {
    var auth = "ToolAuthentication";
    if (localStorage.getItem("TypeAuthentication")) {
        auth = localStorage.getItem("TypeAuthentication");
    }

    return auth;
}
