{
	"Permits": [
		{
			"ID": 1,
			"ParentID": "",
			"Category": "Main",
			"ClassIcon": "fa-file-zip-o",
			"Name": "PO Documents",
			"Path": "/SupportDoc/DocumentList?pviewType=PO",
			"NumOrder": "1.00",
			"FlagIsGlobal": "0"
		},
		{
			"ID": 2,
			"ParentID": "",
			"Category": "Main",
			"ClassIcon": "fa-file-word-o",
			"Name": "All Documents",
			"Path": "/SupportDoc/DocumentList?pviewType=ALL",
			"NumOrder": "2.00",
			"FlagIsGlobal": "0"
		},
		{
			"ID": 3,
			"ParentID": "",
			"Category": "Main",
			"ClassIcon": "fa-envelope",
			"Name": "Send Email / Escalations",
			"Path": "/SupportDoc/DocumentEscalation",
			"NumOrder": "3.00",
			"FlagIsGlobal": "0"
		},
		{
			"ID": 4,
			"ParentID": "",
			"Category": "Main",
			"ClassIcon": "fa-list-ul",
			"Name": "My Pending Documents",
			"Path": "/SupportDoc/DocumentUpload",
			"NumOrder": "4.00",
			"FlagIsGlobal": "0"
		},
		{
			"ID": 5,
			"ParentID": "",
			"Category": "Main",
			"ClassIcon": "fa-line-chart",
			"Name": "Reports",
			"Path": "/Reports",
			"NumOrder": "5.00",
			"FlagIsGlobal": "0"
		},
		{
			"ID": 6,
			"ParentID": "",
			"Category": "Configuration",
			"ClassIcon": "fa-database",
			"Name": "Import Data",
			"Path": "/SupportDoc/DocumentAdmin",
			"NumOrder": "6.00",
			"FlagIsGlobal": "0"
		},
		{
			"ID": 7,
			"ParentID": "",
			"Category": "Configuration",
			"ClassIcon": "fa-globe",
			"Name": "Regions & Emails",
			"Path": "/SupportDoc/RegionAdmin",
			"NumOrder": "7.00",
			"FlagIsGlobal": "0"
		},
		{
			"ID": 8,
			"ParentID": "",
			"Category": "Configuration",
			"ClassIcon": "fa-lock",
			"Name": "Audit Log",
			"Path": "/SupportDoc/AuditLog",
			"NumOrder": "8.00",
			"FlagIsGlobal": "0"
		},
		{
			"ID": 9,
			"ParentID": "",
			"Category": "Configuration",
			"ClassIcon": "fa-key",
			"Name": "Authentication",
			"Path": "/SupportDoc/Authentication",
			"NumOrder": "9.00",
			"FlagIsGlobal": "0"
		}
	],
	"Reports": [
		{
			"ID": 1,
			"Category": "General",
			"Name": "Status By Manager Report",
			"Path": "/SupportDocumentation/StatusBy_Manger_Report",
			"NumOrder": "1.00",
			"FlagIsGlobal": "0"
		},
		{
			"ID": 2,
			"Category": "General",
			"Name": "Status By PO Report",
			"Path": "/SupportDocumentation/StatusByPO_Report",
			"NumOrder": "2.00",
			"FlagIsGlobal": "0"
		},
		{
			"ID": 3,
			"Category": "General",
			"Name": "Status By Responsible Report",
			"Path": "/SupportDocumentation/StatusByResponsible_Report",
			"NumOrder": "3.00",
			"FlagIsGlobal": "0"
		},
		{
			"ID": 4,
			"Category": "General",
			"Name": "Raw Data Report",
			"Path": "/SupportDocumentation/RawData_Report",
			"NumOrder": "4.00",
			"FlagIsGlobal": "0"
		},
		{
			"ID": 5,
			"Category": "General",
			"Name": "Escalations  Report",
			"Path": "/SupportDocumentation/Escalation_Report",
			"NumOrder": "5.00",
			"FlagIsGlobal": "0"
		}
	],
	"Roles": [
		{
			"ID": "3",
			"Name": "SUPPORTDOC_MANAGER",
			"EERSMarketplaceRoleID": "",
			"EERSFunctionCode": "Support Doc - Manager",
			"EERSFunctionDescription": "",
			"EERSIgnore": "True"
		},
		{
			"ID": "4",
			"Name": "SUPPORTDOC_DEFAULT_USER",
			"EERSMarketplaceRoleID": "168184_4",
			"EERSFunctionCode": "Support Doc - User",
			"EERSFunctionDescription": "[RO] - Users with ready only access",
			"EERSIgnore": "False"
		},
		{
			"ID": "5",
			"Name": "SUPPORTDOC_PO",
			"EERSMarketplaceRoleID": "168184_5",
			"EERSFunctionCode": "Support Doc - PO",
			"EERSFunctionDescription": "[RW] - User with access to read and write data",
			"EERSIgnore": "False"
		},
		{
			"ID": "6",
			"Name": "SUPPORTDOC_ADMIN",
			"EERSMarketplaceRoleID": "168184_6",
			"EERSFunctionCode": "Support Doc - Administrator",
			"EERSFunctionDescription": "[RW] - Users with full access to the tool",
			"EERSIgnore": "False"
		},
		{
			"ID": "7",
			"Name": "SUPPORTDOC_SUPER_ADMIN",
			"EERSMarketplaceRoleID": "",
			"EERSFunctionCode": "Support Doc - Super Admin",
			"EERSFunctionDescription": "",
			"EERSIgnore": "True"
		}
	],
	"RolesXPermits": [
		{
			"IDRole": "5",
			"IDPermit": 1,
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:41:55 PM"
		},
		{
			"IDRole": "7",
			"IDPermit": 1,
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:42:20 PM"
		},
		{
			"IDRole": "6",
			"IDPermit": 2,
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:42:15 PM"
		},
		{
			"IDRole": "7",
			"IDPermit": 2,
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:42:22 PM"
		},
		{
			"IDRole": "5",
			"IDPermit": 3,
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:41:57 PM"
		},
		{
			"IDRole": "6",
			"IDPermit": 3,
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:42:14 PM"
		},
		{
			"IDRole": "7",
			"IDPermit": 3,
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:42:21 PM"
		},
		{
			"IDRole": "4",
			"IDPermit": 4,
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:41:35 PM"
		},
		{
			"IDRole": "6",
			"IDPermit": 4,
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:42:15 PM"
		},
		{
			"IDRole": "7",
			"IDPermit": 4,
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:42:21 PM"
		},
		{
			"IDRole": "5",
			"IDPermit": 5,
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:41:56 PM"
		},
		{
			"IDRole": "6",
			"IDPermit": 5,
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:42:14 PM"
		},
		{
			"IDRole": "7",
			"IDPermit": 5,
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:42:21 PM"
		},
		{
			"IDRole": "6",
			"IDPermit": 6,
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:42:14 PM"
		},
		{
			"IDRole": "7",
			"IDPermit": 6,
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:42:20 PM"
		},
		{
			"IDRole": "6",
			"IDPermit": 7,
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:42:14 PM"
		},
		{
			"IDRole": "7",
			"IDPermit": 7,
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:42:21 PM"
		},
		{
			"IDRole": "5",
			"IDPermit": 8,
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:41:56 PM"
		},
		{
			"IDRole": "6",
			"IDPermit": 8,
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:42:14 PM"
		},
		{
			"IDRole": "7",
			"IDPermit": 8,
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:42:20 PM"
		},
		{
			"IDRole": "7",
			"IDPermit": 9,
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:42:21 PM"
		}
	],
	"RolesXReports": [],
	"Users": [
		{
			"SOEID": "AH77574",
			"GEID": "1010877574",
			"Email": "adrian.alberto.hermoso@citi.com",
			"Name": "Adrian Alberto Hermoso",
			"NumberOfSessions": "9",
			"CreatedBy": "CD25867",
			"CreatedDate": "9/4/2017 3:56:31 PM",
			"ModifiedBy": "LAC\\AH77574",
			"ModifiedDate": "10/19/2017 1:34:27 PM",
			"LastSessionDate": "10/19/2017 1:34:27 PM",
			"Roles": "SUPPORTDOC_DEFAULT_USER,SUPPORTDOC_SUPER_ADMIN,",
			"IsDeleted": "False"
		},
		{
			"SOEID": "CD25867",
			"GEID": "1010425867",
			"Email": "carlos.dominguez@citi.com",
			"Name": "Carlos Dominguez",
			"NumberOfSessions": "168",
			"CreatedBy": "CD25867",
			"CreatedDate": "5/17/2017 11:10:39 AM",
			"ModifiedBy": "LAC\\cd25867",
			"ModifiedDate": "11/13/2017 1:55:00 PM",
			"LastSessionDate": "11/13/2017 1:55:00 PM",
			"Roles": "SUPPORTDOC_DEFAULT_USER,SUPPORTDOC_SUPER_ADMIN,",
			"IsDeleted": "False"
		},
		{
			"SOEID": "JM77963",
			"GEID": "1010677963",
			"Email": "jessica.moracalderon@citi.com",
			"Name": "Jessica Mora",
			"NumberOfSessions": "10",
			"CreatedBy": "CD25867",
			"CreatedDate": "9/8/2017 10:51:27 AM",
			"ModifiedBy": "CD25867",
			"ModifiedDate": "10/2/2017 3:39:41 PM",
			"LastSessionDate": "9/12/2017 4:21:06 PM",
			"Roles": "SUPPORTDOC_ADMIN,",
			"IsDeleted": "False"
		},
		{
			"SOEID": "JR59554",
			"GEID": "1010559554",
			"Email": "junior.richmond@citi.com",
			"Name": "Junior Alberto Richmond",
			"NumberOfSessions": "0",
			"CreatedBy": "CD25867",
			"CreatedDate": "9/11/2017 12:42:51 PM",
			"ModifiedBy": "CD25867",
			"ModifiedDate": "10/2/2017 3:40:51 PM",
			"LastSessionDate": "1/1/1900 12:00:00 AM",
			"Roles": "SUPPORTDOC_ADMIN,",
			"IsDeleted": "False"
		},
		{
			"SOEID": "KN31533",
			"GEID": "1010431532",
			"Email": "katherine.noguera@citi.com",
			"Name": "Katherine Noguera",
			"NumberOfSessions": "7",
			"CreatedBy": "CD25867",
			"CreatedDate": "8/29/2017 4:19:57 PM",
			"ModifiedBy": "CD25867",
			"ModifiedDate": "10/2/2017 3:39:54 PM",
			"LastSessionDate": "10/2/2017 9:13:56 AM",
			"Roles": "SUPPORTDOC_SUPER_ADMIN,",
			"IsDeleted": "False"
		},
		{
			"SOEID": "LC61547",
			"GEID": "1010061547",
			"Email": "luis.cajiao@citi.com",
			"Name": "Luis Mariano Cajiao Campos",
			"NumberOfSessions": "0",
			"CreatedBy": "CD25867",
			"CreatedDate": "9/11/2017 12:43:07 PM",
			"ModifiedBy": "CD25867",
			"ModifiedDate": "10/2/2017 3:40:41 PM",
			"LastSessionDate": "1/1/1900 12:00:00 AM",
			"Roles": "SUPPORTDOC_ADMIN,",
			"IsDeleted": "False"
		},
		{
			"SOEID": "MM43399",
			"GEID": "1010443397",
			"Email": "maria.madrigal@citi.com",
			"Name": "Maria Jose Madrigal",
			"NumberOfSessions": "0",
			"CreatedBy": "CD25867",
			"CreatedDate": "9/11/2017 12:42:37 PM",
			"ModifiedBy": "CD25867",
			"ModifiedDate": "10/2/2017 3:40:26 PM",
			"LastSessionDate": "1/1/1900 12:00:00 AM",
			"Roles": "SUPPORTDOC_ADMIN,",
			"IsDeleted": "False"
		},
		{
			"SOEID": "RE65657",
			"GEID": "1001265657",
			"Email": "rodney.alberto.espinoza@citi.com",
			"Name": "Rodney Alberto Espinoza",
			"NumberOfSessions": "12",
			"CreatedBy": "LAC\\cd25867",
			"CreatedDate": "10/11/2017 1:48:23 PM",
			"ModifiedBy": "LAC\\re65657",
			"ModifiedDate": "11/13/2017 11:14:46 AM",
			"LastSessionDate": "11/13/2017 11:14:46 AM",
			"Roles": "SUPPORTDOC_DEFAULT_USER,",
			"IsDeleted": "False"
		},
		{
			"SOEID": "SS23997",
			"GEID": "1010523997",
			"Email": "susan.steller@citi.com",
			"Name": "Susan Maria Steller",
			"NumberOfSessions": "1",
			"CreatedBy": "CD25867",
			"CreatedDate": "9/11/2017 12:43:22 PM",
			"ModifiedBy": "CD25867",
			"ModifiedDate": "10/2/2017 3:40:07 PM",
			"LastSessionDate": "9/29/2017 4:51:44 PM",
			"Roles": "SUPPORTDOC_ADMIN,",
			"IsDeleted": "False"
		}
	],
	"UsersXRoles": [
		{
			"SOEID": "CD25867",
			"IDRole": "4",
			"CreatedBy": "LAC\\frosvcedcfcpr01",
			"CreatedDate": "10/2/2017 3:39:31 PM"
		},
		{
			"SOEID": "JM77963",
			"IDRole": "6",
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:39:41 PM"
		},
		{
			"SOEID": "JR59554",
			"IDRole": "6",
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:40:51 PM"
		},
		{
			"SOEID": "LC61547",
			"IDRole": "6",
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:40:41 PM"
		},
		{
			"SOEID": "MM43399",
			"IDRole": "6",
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:40:26 PM"
		},
		{
			"SOEID": "SS23997",
			"IDRole": "6",
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:40:07 PM"
		},
		{
			"SOEID": "AH77574",
			"IDRole": "7",
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:40:58 PM"
		},
		{
			"SOEID": "CD25867",
			"IDRole": "7",
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:39:31 PM"
		},
		{
			"SOEID": "KN31533",
			"IDRole": "7",
			"CreatedBy": "CD25867",
			"CreatedDate": "10/2/2017 3:39:54 PM"
		}
	]
}