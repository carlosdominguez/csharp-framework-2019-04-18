﻿var _APPSINFO;
var _CURRENTAPP = {
    EERSConfig : {
        Configuration: {
            DEV: {
                ServerName: "serverTest",
                ListRoleProcedure: "lrpTest",
                ListUserProcedure: "lupTest",
                IsaPluginPath:"IPP"
            },
            UAT: {
                ServerName: "serverTest",
                ListRoleProcedure: "lrpTest",
                ListUserProcedure: "lupTest",
                IsaPluginPath: "IPP"
            },
            PROD: {
                ServerName: "serverTest",
                ListRoleProcedure: "lrpTest",
                ListUserProcedure: "lupTest",
                IsaPluginPath: "IPP"
            }
        },
        RolesApprovers:[]
    }
};

$(document).ready(function () {
    
    //Load web config
    loadPage();

    //On Save Web Config
    $(".btnSave").click(function () {
        saveEERSConfig();
    });

    //Add new row to the table
    $(".btnAddNewRow").click(function () {
        addNewRow();
    });

    //Delete row in the table
    $(".btnDelete").click(function () {
        deleteRow();
    });




});

function loadPage() {
    _callServer({
        url: '/Shared/GetAppsInfo',
        success: function (responseJsonStr) {
            _APPSINFO = JSON.parse(responseJsonStr);

            var tempCurrentApp = _findOneObjByProperty(_APPSINFO, "KeyName", _getUserInfo().DefaultAppKey);
            $.extend(true, _CURRENTAPP, tempCurrentApp);



            loadData();
        }
    });
    
};

function loadData() {
    //DEV VALUES
    $("#txtServerNameDev").val(_CURRENTAPP.EERSConfig.Configuration.DEV.ServerName);
    $("#txtListRoleProcedureDev").val(_CURRENTAPP.EERSConfig.Configuration.DEV.ListRoleProcedure);
    $("#txtListUserProcedureDev").val(_CURRENTAPP.EERSConfig.Configuration.DEV.ListUserProcedure);
    $("#txtIsaPluginPathDev").val(_CURRENTAPP.EERSConfig.Configuration.DEV.IsaPluginPath);

    //UAT VALUES
    $("#txtServerNameUat").val(_CURRENTAPP.EERSConfig.Configuration.UAT.ServerName);
    $("#txtListRoleProcedureUat").val(_CURRENTAPP.EERSConfig.Configuration.UAT.ListRoleProcedure);
    $("#txtListUserProcedureUat").val(_CURRENTAPP.EERSConfig.Configuration.UAT.ListUserProcedure);
    $("#txtIsaPluginPathUat").val(_CURRENTAPP.EERSConfig.Configuration.UAT.IsaPluginPath);

    //PROD VALUES
    $("#txtServerNameProd").val(_CURRENTAPP.EERSConfig.Configuration.PROD.ServerName);
    $("#txtListRoleProcedureProd").val(_CURRENTAPP.EERSConfig.Configuration.PROD.ListRoleProcedure);
    $("#txtListUserProcedureProd").val(_CURRENTAPP.EERSConfig.Configuration.PROD.ListUserProcedure);
    $("#txtIsaPluginPathProd").val(_CURRENTAPP.EERSConfig.Configuration.PROD.IsaPluginPath);

    loadTableApprovers();
}

function setData() {
    //DEV VALUES
    _CURRENTAPP.EERSConfig.Configuration.DEV.ServerName = $("#txtServerNameDev").val();
    _CURRENTAPP.EERSConfig.Configuration.DEV.ListRoleProcedure = $("#txtListRoleProcedureDev").val();
    _CURRENTAPP.EERSConfig.Configuration.DEV.ListUserProcedure = $("#txtListUserProcedureDev").val();
    _CURRENTAPP.EERSConfig.Configuration.DEV.IsaPluginPath = $("#txtIsaPluginPathDev").val();

    //UAT VALUES
    _CURRENTAPP.EERSConfig.Configuration.UAT.ServerName = $("#txtServerNameUat").val();
    _CURRENTAPP.EERSConfig.Configuration.UAT.ListRoleProcedure = $("#txtListRoleProcedureUat").val();
    _CURRENTAPP.EERSConfig.Configuration.UAT.ListUserProcedure = $("#txtListUserProcedureUat").val();
    _CURRENTAPP.EERSConfig.Configuration.UAT.IsaPluginPath = $("#txtIsaPluginPathUat").val();

    //PROD VALUES
    _CURRENTAPP.EERSConfig.Configuration.PROD.ServerName = $("#txtServerNameProd").val();
    _CURRENTAPP.EERSConfig.Configuration.PROD.ListRoleProcedure = $("#txtListRoleProcedureProd").val();
    _CURRENTAPP.EERSConfig.Configuration.PROD.ListUserProcedure = $("#txtListUserProcedureProd").val();
    _CURRENTAPP.EERSConfig.Configuration.PROD.IsaPluginPath = $("#txtIsaPluginPathProd").val();

}

function loadTableApprovers() {
    $.jqxGridApi.create({
        showTo: "#tblCmpRolesApprovers",
        options: {
            //for comments or descriptions
            height: "500",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: true,
            sortable: true,
            editable: true,
            groupable: true
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set Local",
            rows: _CURRENTAPP.EERSConfig.RolesApprovers
        },
        groups: [],
        columns: [
            //type: string - text - number - int - float - date - time 
            //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
            //cellsformat: ddd, MMM dd, yyyy h:mm tt
            { name: 'Role', text: 'Role', width: '20%', type: 'string', editable: true },
            { name: 'RoleID', text: 'RoleID', width: '20%', type: 'string', editable: true },
            { name: 'ApproverModel', text: 'ApproverModel', width: '20%', type: 'string',editable: true },
            { name: 'ApproverGroup', text: 'ApproverGroup', width: '20%', type: 'string', editable: true },
            { name: 'Members', text: 'Members', width: '20%', type: 'string', editable: true }
        ],
        ready: function () {

        }
    });


}

function addNewRow() {
    var $jqxGrid = $("#tblCmpRolesApprovers");
    var datarow = {};

    datarow["Role"] = "";
    datarow["RoleID"] = "";
    datarow["ApproverModel"] = "";
    datarow["ApproverGroup"] = "";
    datarow["Members"] = "";

    var commit = $jqxGrid.jqxGrid('addrow', null, datarow);
}

function deleteRow() {
    //Delete 
    var objRowSelected = $.jqxGridApi.getOneSelectedRow("#tblCmpRolesApprovers", true);
    if (objRowSelected) {
        var htmlContentModal = "<b>Role:</b>" + objRowSelected["Role"] + "<br/>";

        _showModal({
            width: '35%',
            modalId: "modalDelApp",
            addCloseButton: true,
            buttons: [{
                name: "Delete",
                class: "btn-danger",
                onClick: function () {
                    $("#tblCmpRolesApprovers").jqxGrid('deleterow', objRowSelected.uid);
                }
            }],
            title: "Are you sure you want to delete this row?",
            contentHtml: htmlContentModal
        });
    }
};

function saveEERSConfig() {
    var jqxRows = $.jqxGridApi.getAllRows("#tblCmpRolesApprovers");
    var rowsToSave = [];
    var appKeyDefault = "";

    for (var i = 0; i < jqxRows.length; i++) {
        rowsToSave.push({
            Role: jqxRows[i].Role,
            RoleID: jqxRows[i].RoleID,
            ApproverModel: jqxRows[i].ApproverModel,
            ApproverGroup: jqxRows[i].ApproverGroup,
            Members: jqxRows[i].Members
        });
    }
    _CURRENTAPP.EERSConfig.RolesApprovers = rowsToSave;

    setData();

    //$.merge(_APPSINFO, [_CURRENTAPP]);
    var tempCurrentApp = _findOneObjByProperty(_APPSINFO, "KeyName", _getUserInfo().DefaultAppKey);
    tempCurrentApp.EERSConfig = _CURRENTAPP.EERSConfig;
    //Save app info data
    _callServer({
        loadingMsg: "Saving changes...",
        url: '/Shared/SaveAppsInfo',
        data: { 'pjson': _toJSON(_APPSINFO) },
        type: "post",
        success: function (savingStatus) {
            _showNotification("success", "Data was saved successfully.");
        }
    });
    
    console.log(_APPSINFO);
}
