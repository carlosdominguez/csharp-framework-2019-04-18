﻿/// <reference path="../Shared/plugins/util/global.js" />

var permits;
var reports;
var roles;
var rolesXPermits;
var rolesXReports = [];

$(document).ready(function () {

    //Load Reports
    _loadReportsFromDB(function (reportsList) {
        reports = reportsList;
    });

    //Load Permits
    _loadPermitsFromDB(function (permitsList) {
        permits = permitsList;
    });

    //Load Roles
    _loadRolesFromDB(function (rolesList) {
        roles = rolesList;
    });

    //Load Permits of Roles
    _loadRolesXPermitsFromDB(function (rolesXPermitsList) {
        rolesXPermits = rolesXPermitsList;
    });

    //Load Reports of Roles
    _loadRolesXReportsFromDB(function (rolesXReportsList) {
        rolesXReports = rolesXReportsList;
    });

    _execOnAjaxComplete(function () {
        var objTabRoles = new ClassTabRoles();
    });
});

//Class for Load Tab Roles
function ClassTabRoles() {
    var classTabRoles = this;

    this.loadDropDownOfRoles = function (idSelect, mappingData, idRoleSelected) {
        var $select = $(idSelect);

        $(idSelect).find("option").remove();
        $select.append($('<option>', {
            value: "0",
            text: "-- Select a Role --"
        }));

        //Load options in select
        for (var i = 0; i < roles.length; i++) {
            var objRole = roles[i];
            var optionName = objRole.Name;
            var selected = false;

            if (mappingData) {
                var itemsOfRole = _findAllObjByProperty(mappingData, "IDRole", objRole.ID);
                optionName += " (" + itemsOfRole.length + " Items granted)";
            }

            var objOption = {
                value: objRole.ID,
                text: optionName
            };

            if (idRoleSelected && objRole.ID == idRoleSelected) {
                selected = true;
            }

            if (selected) {
                objOption.selected = true;
            }

            $select.append($('<option>', objOption));
        }

        $select.select2({
            placeholder: 'Choose a role:',
            allowClear: true
        }).on('select2-open', function () {
            // Adding Custom Scrollbar
            $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
        });
    };

    //Tab for add, delete and edit Role
    //--------------------------------------------
    function ClassListRoles() {
        var classListRoles = this;

        //On click in btn add new role
        $(".btnAddNewRole").click(function () {
            classListRoles.addNewRole();
        });

        //On click in btn edit role
        $(".btnEditRole").click(function () {
            classListRoles.editRole();
        });

        //On click in btn delete role
        $(".btnDeleteRole").click(function () {
            classListRoles.deleteRole();
        });

        this.createFormRole = function (options) {
            var objRole = options.objRole;

            if (objRole.ID || objRole.ID == "0") {
                objRole.IDOld = objRole.ID;
            }

            var formHtml =
                '<div class="form-horizontal"> ' +
                '    <div class="col-md-10"> ' +
                '        <div class="form-group"> ' +
                '            <label class="col-md-4 control-label"> ' +
                '                <i class="fa fa-asterisk iconasterisk"></i> ID ' +
                '            </label> ' +
                '            <div class="col-md-8"> ' +
                '               <input type="text" class="txtID form-control" value="' + objRole.ID + '" placeholder="eg. 4"> ' +
                '            </div> ' +
                '        </div> ' +
                '        <div class="form-group"> ' +
                '            <label class="col-md-4 control-label"> ' +
                '                <i class="fa fa-asterisk iconasterisk"></i> Name ' +
                '            </label> ' +
                '            <div class="col-md-8"> ' +
                '               <input type="text" class="txtName form-control" value="' + objRole.Name + '" placeholder="eg. ISSUE_LOG_MANAGER"> ' +
                '            </div> ' +
                '        </div> ' +
                '        <div class="form-group"> ' +
                '            <label class="col-md-4 control-label"> ' +
                '                <i class="fa fa-asterisk iconasterisk"></i> Marketplace Code ' +
                '            </label> ' +
                '            <div class="col-md-8"> ' +
                '               <input type="text" class="txtEERSMarketplaceRoleID form-control" value="' + objRole.EERSMarketplaceRoleID + '" placeholder="eg. 165986_2"> ' +
                '            </div> ' +
                '        </div> ' +
                '        <div class="form-group"> ' +
                '            <label class="col-md-4 control-label"> ' +
                '                <i class="fa fa-asterisk iconasterisk"></i> Marketplace Name ' +
                '            </label> ' +
                '            <div class="col-md-8"> ' +
                '               <input type="text" class="txtEERSFunctionCode form-control" value="' + objRole.EERSFunctionCode + '" placeholder="eg. Issue Log - Manager"> ' +
                '            </div> ' +
                '        </div> ' +
                '        <div class="form-group"> ' +
                '            <label class="col-md-4 control-label"> ' +
                '                <i class="fa fa-asterisk iconasterisk"></i> EERS Function Descrition ' +
                '            </label> ' +
                '            <div class="col-md-8"> ' +
                '               <input type="text" class="txtEERSFunctionDescription form-control" value="' + objRole.EERSFunctionDescription + '" placeholder="eg. [RO] - Users with ready only access"> ' +
                '            </div> ' +
                '        </div> ' +
                '        <div class="form-group"> ' +
                '            <label class="col-md-4 control-label"> ' +
                '                <i class="fa fa-asterisk iconasterisk"></i> Ignore Role in EERS? ' +
                '            </label> ' +
                '            <div class="col-md-8"> ' +
                '               <input value="1" type="radio" name="chkFlagEERSIgnore" class="rbtFlagGlobalYes skin-square-green" ' + ((objRole.EERSIgnore == "True") ? "checked" : "") + ' > ' +
                '               <label class="iradio-label form-label hover" style="margin-right: 25px;">Yes </label> ' +
                '               <input value="0" type="radio" name="chkFlagEERSIgnore" class="rbtFlagGlobalNo skin-square-green" ' + ((objRole.EERSIgnore == "False") ? "checked" : "") + ' > ' +
                '               <label class="iradio-label form-label hover" style="margin-right: 25px;">No </label> ' +
                '            </div> ' +
                '        </div> ' +
                '    </div> ' +
                '    <div style="clear: both;"></div> ' +
                '</div> ';

            _showModal({
                width: "75%",
                title: "Role Information",
                contentHtml: formHtml,
                buttons: [{
                    name: "Save",
                    class: "btn-success",
                    closeModalOnClick: true,
                    onClick: function ($modal) {
                        objRole.ID = $modal.find(".txtID").val();
                        objRole.Name = $modal.find(".txtName").val();
                        objRole.EERSMarketplaceRoleID = $modal.find(".txtEERSMarketplaceRoleID").val();
                        objRole.EERSFunctionCode = $modal.find(".txtEERSFunctionCode").val();
                        objRole.EERSFunctionDescription = $modal.find(".txtEERSFunctionDescription").val();
                        objRole.EERSIgnore = ($modal.find('[name="chkFlagEERSIgnore"]:checked').val() == '1' ? 'True' : 'False');

                        if (options.onSave) {
                            options.onSave(objRole);
                        }
                    }
                }],
                addCloseButton: true,
                onReady: function () {
                    _GLOBAL_SETTINGS.iCheck();
                }
            });
        };

        this.loadTableRoles = function () {
            _showLoadingTopMessage("Loading Table of Roles...", "TableRoles");
            $.jqxGridApi.create({
                showTo: "#tblRole",
                options: {
                    //for comments or descriptions
                    height: "500",
                    autoheight: true,
                    autorowheight: false,
                    showfilterrow: true,
                    sortable: true,
                    editable: true,
                    selectionmode: "singlerow"
                },
                source: {
                    // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                    dataBinding: "Large Data Set Local",
                    rows: roles
                },
                groups: [],
                columns: [
                    //type: string - text - number - int - float - date - time 
                    //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                    //cellsformat: ddd, MMM dd, yyyy h:mm tt
                    { name: 'ID', text: 'ID', width: '40px', type: 'number', filtertype: 'number' },
                    { name: 'Name', text: 'Name', width: '300px', type: 'number', filtertype: 'number' },
                    { name: 'EERSMarketplaceRoleID', text: 'Marketplace Code (eg. 165986_20)', width: '250px', type: 'string', filtertype: 'checkedlist' },
                    { name: 'EERSFunctionCode', text: 'Marketplace Name (eg. MEIL Basic)', width: '250px', type: 'string', filtertype: 'checkedlist' },
                    { name: 'EERSFunctionDescription', text: 'EERS Function Description', width: '450px', type: 'string', filtertype: 'checkedlist' },
                    { name: 'EERSIgnore', text: 'EERS Ignore', width: '90px', type: 'string', filtertype: 'checkedlist' }
                ],
                ready: function () {}
            });

            setTimeout(function () {
                _hideLoadingTopMessage("TableRoles");
            }, 600);
        };

        this.addNewRole = function () {
            var objNewRole = {
                ID: '0',
                Name: '',
                EERSMarketplaceRoleID: '',
                EERSFunctionCode: '',
                EERSFunctionDescription: '',
                EERSIgnore: 'False'
            };

            classListRoles.createFormRole({
                objRole: objNewRole,
                onSave: function (objNewRole) {
                    var spName = _getDBObjectFullName("AdminRole");

                    _callProcedure({
                        loadingMsgType: "fullLoading",
                        loadingMsg: "Saving new role '" + objNewRole.Name + "'...",
                        name: "[dbo].[" + spName + "]",
                        params: [
                            { "Name": "@Action", "Value": "Insert" },
                            { "Name": "@IDRole", "Value": objNewRole.ID },
                            { "Name": "@Name", "Value": objNewRole.Name },
                            { "Name": "@EERSMarketplaceRoleID", "Value": objNewRole.EERSMarketplaceRoleID },
                            { "Name": "@EERSFunctionCode", "Value": objNewRole.EERSFunctionCode },
                            { "Name": "@EERSFunctionDescription", "Value": objNewRole.EERSFunctionDescription },
                            { "Name": "@EERSIgnore", "Value": (objNewRole.EERSIgnore == "True" ? '1' : '0') },
                            { "Name": "@SessionSOEID", "Value": _getSOEID() }
                        ],
                        //Show message only for the last 
                        success: function () {
                            _showNotification("success", "New Role '" + objNewRole.Name + "' was created successfully.");

                            //Load Roles
                            _loadRolesFromDB(function (rolesList) {
                                roles = rolesList;

                                //Reload table of roles
                                classListRoles.loadTableRoles();

                                //Reload select in "Add Permits to Role"
                                classTabRoles.loadDropDownOfRoles("#selRoleOfPermits", rolesXPermits);

                                //Reload select in "Add Reports to Role"
                                classTabRoles.loadDropDownOfRoles("#selRoleOfReports", rolesXReports);
                            });
                        }
                    });
                }
            });
        };

        this.editRole = function () {
            var objRowSelected = $.jqxGridApi.getOneSelectedRow("#tblRole", true);
            if (objRowSelected) {
                classListRoles.createFormRole({
                    objRole: objRowSelected,
                    onSave: function (objUpdatedRole) {
                        var spName = _getDBObjectFullName("AdminRole");

                        _callProcedure({
                            loadingMsgType: "fullLoading",
                            loadingMsg: "Updating role '" + objUpdatedRole.Name + "'...",
                            name: "[dbo].[" + spName + "]",
                            params: [
                                { "Name": "@Action", "Value": "Edit" },
                                { "Name": "@IDRole", "Value": objUpdatedRole.ID },
                                { "Name": "@IDRoleOld", "Value": objUpdatedRole.IDOld },
                                { "Name": "@Name", "Value": objUpdatedRole.Name },
                                { "Name": "@EERSMarketplaceRoleID", "Value": objUpdatedRole.EERSMarketplaceRoleID },
                                { "Name": "@EERSFunctionCode", "Value": objUpdatedRole.EERSFunctionCode },
                                { "Name": "@EERSFunctionDescription", "Value": objUpdatedRole.EERSFunctionDescription },
                                { "Name": "@EERSIgnore", "Value": (objUpdatedRole.EERSIgnore == "True" ? '1' : '0') },
                                { "Name": "@SessionSOEID", "Value": _getSOEID() }
                            ],
                            //Show message only for the last 
                            success: function () {
                                _showNotification("success", "Role '" + objUpdatedRole.Name + "' was modified successfully.");

                                //Load Roles
                                _loadRolesFromDB(function (rolesList) {
                                    roles = rolesList;

                                    //Reload table of roles
                                    classListRoles.loadTableRoles();

                                    //Reload select in "Add Permits to Role"
                                    classTabRoles.loadDropDownOfRoles("#selRoleOfPermits", rolesXPermits);

                                    //Reload select in "Add Reports to Role"
                                    classTabRoles.loadDropDownOfRoles("#selRoleOfReports", rolesXReports);

                                    //Refresh Session
                                    _refreshSession();
                                });
                            }
                        });
                    }
                });
            }
        };

        this.deleteRole = function () {
            //Delete 
            var objRowSelected = $.jqxGridApi.getOneSelectedRow("#tblRole", true);
            if (objRowSelected) {
                var spName = _getDBObjectFullName("AdminRole");
                var htmlContentModal = "";
                htmlContentModal += "<b>Role: </b>" + objRowSelected['Name'] + "<br/>";

                _showModal({
                    width: '40%',
                    modalId: "modalDelRow",
                    addCloseButton: true,
                    buttons: [{
                        name: "Delete",
                        class: "btn-danger",
                        onClick: function () {
                            if (objRowSelected.ID != 0) {
                                _callProcedure({
                                    loadingMsgType: "fullLoading",
                                    loadingMsg: "Deleting role '" + objRowSelected.Name + "'...",
                                    name: "[dbo].[" + spName + "]",
                                    params: [
                                        { "Name": "@Action", "Value": "Delete" },
                                        { "Name": "@IDRole", "Value": objRowSelected.ID },
                                        { "Name": "@SessionSOEID", "Value": _getSOEID() }
                                    ],
                                    //Show message only for the last 
                                    success: function(){
                                        _showNotification("success", "Role '" + objRowSelected.Name + "' was deleted successfully.");

                                        //Load Roles
                                        _loadRolesFromDB(function (rolesList) {
                                            roles = rolesList;

                                            //Reload select in "Add Permits to Role"
                                            classTabRoles.loadDropDownOfRoles("#selRoleOfPermits", rolesXPermits);

                                            //Reload select in "Add Reports to Role"
                                            classTabRoles.loadDropDownOfRoles("#selRoleOfReports", rolesXReports);

                                            //Reload table of roles
                                            classListRoles.loadTableRoles();
                                        });
                                    }
                                });
                            } else {
                                $("#tblRole").jqxGrid('deleterow', objRowSelected.uid);
                            }
                        }
                    }],
                    title: "Are you sure you want to delete this row?",
                    contentHtml: htmlContentModal
                });
            }
        };

        //Load Table of Roles
        this.loadTableRoles();
    }
    this.objListRoles = new ClassListRoles();

    //Tab for add permits to Role
    //--------------------------------------------
    function ClassRoleXPermits() {
        var classRoleXPermits = this;

        //On click check all Permits
        $(".checkAllRoleXPermits").on("ifChanged", function () {
            var $checkAll = $(this);
            var grantToAll = $checkAll.is(":checked");

            $("[idPermit]").each(function (indexEach, item) {
                var $el = $(item);
                var idPermit = $el.attr("idPermit");
                var isGranted = $el.is(":checked");

                if (isGranted != grantToAll) {
                    if (grantToAll) {
                        $el.iCheck('check');
                    } else {
                        $el.iCheck('uncheck');
                    }
                }
            });
        });

        //On click save permits of role
        $(".btnSaveRoleXPermits").click(function () {
            classRoleXPermits.saveRoleXPermits();
        });

        //On change select of roles
        $("#selRoleOfPermits").change(function () {
            classRoleXPermits.loadPermitsOfRole($("#selRoleOfPermits").val());
        });

        this.loadPermits = function () {
            var categoryPermits = Enumerable.From(permits)
                    .GroupBy(
                        "$.Category",
                        "{ ID: $.ID, Category: $.Category, Name: $.Name, Path: $.Path, ClassIcon: $.ClassIcon}",
                        "{ Category: $, Permits: $$.ToArray() }"
                    )
                    .ToArray();

            //Clean old data
            $("#permitCategories").contents().remove();

            for (var i = 0; i < categoryPermits.length; i++) {
                var objCategoryPermit = categoryPermits[i];

                var $title = $('<p class="text-success"></p>');
                $title.html('<i class="fa fa-cube"></i>  ' + objCategoryPermit.Category);

                var idTable = 'tblPermitsBody' + objCategoryPermit.Category;
                var $tableOfPermits = $(
                    '<table class="table table-hover"> ' +
                    '    <tbody id="' + idTable + '"> ' +
                    '    </tbody> ' +
                    '</table> '
                );

                $("#permitCategories").append($title);
                $("#permitCategories").append($tableOfPermits);

                //Add rows to table
                for (var j = 0; j < objCategoryPermit.Permits.length; j++) {
                    var objPermit = objCategoryPermit.Permits[j];

                    var $rowPermit = $('<tr></tr>');

                    var $colName = $('<td style="width: 90%;"></td>');
                    $colName.html('<i class="fa ' + objPermit.ClassIcon + '"></i> ' + objPermit.Name);
                    $rowPermit.append($colName);

                    var $colIsGranted = $('<td style="width: 10%;"></td>');
                    $colIsGranted.html('<input type="checkbox" idPermit="' + objPermit.ID + '" class="skin-square-green chkIsGranted">');
                    $rowPermit.append($colIsGranted);

                    $tableOfPermits.append($rowPermit);
                }
            }

            _GLOBAL_SETTINGS.iCheck();
        };

        this.loadPermitsOfRole = function (idRole) {
            var idRole = $("#selRoleOfPermits").val();
            var permitsOfRole = _findAllObjByProperty(rolesXPermits, "IDRole", idRole);

            $("[idPermit]").each(function (indexEach, item) {
                var $el = $(item);
                var idPermit = $el.attr("idPermit");
                var isGranted = $el.is(":checked");

                var hasPermitAccess = _findOneObjByProperty(permitsOfRole, "IDPermit", idPermit);

                //Off ifChanged Event
                var fnIfChanged;
                var events = _getBindEvends($el);
                if (events.ifChanged) {
                    fnIfChanged = events.ifChanged["0"].handler;
                    $el.off('ifChanged');
                }

                // Check or Uncheck Input
                if (hasPermitAccess) {
                    if (isGranted == false) {
                        $el.iCheck('check');
                    }
                } else {
                    if (isGranted) {
                        $el.iCheck('uncheck');
                    }
                }

                //Bind ifChanged Event
                if (fnIfChanged) {
                    $el.on('ifChanged', fnIfChanged);
                }
            });
        };

        this.saveRoleXPermits = function () {
            var spAdminPermit = _getDBObjectFullName("AdminRoleXPermit");
            var idRole = $("#selRoleOfPermits").val();

            if (idRole != 0) {
                var totalPermits = $("[idPermit]").length;
                $("[idPermit]").each(function (indexEach, item) {
                    var $el = $(item);
                    var idPermit = $el.attr("idPermit");
                    var isGranted = $el.is(":checked");
                    var action = "Insert";

                    if (isGranted == false) {
                        action = "Delete";
                    }

                    //Save changes only
                    var permitsOfRole = _findAllObjByProperty(rolesXPermits, "IDRole", idRole);
                    var hasPermitAccess = _findOneObjByProperty(permitsOfRole, "IDPermit", idPermit);

                    //Insert only: If not exist and Insert OR exists and delete
                    if ((action == "Insert" && !hasPermitAccess) || (action == "Delete" && hasPermitAccess)) {
                        _callProcedure({
                            loadingMsgType: "fullLoading",
                            loadingMsg: "Saving permits of role...",
                            name: "[dbo].[" + spAdminPermit + "]",
                            params: [
                                { "Name": "@Action", "Value": action },
                                { "Name": "@IDRole", "Value": idRole },
                                { "Name": "@IDPermit", "Value": idPermit },
                                { "Name": "@SessionSOEID", "Value": _getSOEID() }
                            ],
                            success: {}
                        });
                    }
                });

                _execOnAjaxComplete(function () {
                    _loadRolesXPermitsFromDB(function (newRolesXPermits) {
                        //Set new list of permits of role
                        rolesXPermits = newRolesXPermits;

                        //Reload roles to show new items mapped
                        classTabRoles.loadDropDownOfRoles("#selRoleOfPermits", newRolesXPermits, $("#selRoleOfPermits").val());

                        //Show message
                        _showNotification("success", "Permits of role were saved successfully.", "RoleSaved");

                        //Refresh Session Permits
                        _refreshSession();
                    });
                });
            } else {
                _showNotification("error", "Please select at least one Role.");
            }
        }

        //Load roles in "Mapping Permits to Roles"
        classTabRoles.loadDropDownOfRoles("#selRoleOfPermits", rolesXPermits);

        //Load Permits Availables
        this.loadPermits();
    }
    this.objRoleXPermits = new ClassRoleXPermits();
    
    //Tab for add reports to Role
    //--------------------------------------------
    function ClassRoleXReports() {
        var classRoleXReports = this;

        //On click check all Reports
        $(".checkAllRoleXReports").on("ifChanged", function () {
            var $checkAll = $(this);
            var grantToAll = $checkAll.is(":checked");

            $("[idReport]").each(function (indexEach, item) {
                var $el = $(item);
                var idReport = $el.attr("idReport");
                var isGranted = $el.is(":checked");

                if (isGranted != grantToAll) {
                    if (grantToAll) {
                        $el.iCheck('check');
                    } else {
                        $el.iCheck('uncheck');
                    }
                }
            });
        });

        //On click save reports of role
        $(".btnSaveRoleXReports").click(function () {
            classRoleXReports.saveRoleXReports();
        });

        //On change select of roles
        $("#selRoleOfReports").change(function () {
            classRoleXReports.loadReportsOfRole($("#selRoleOfReports").val());
        });

        this.loadReports = function () {
            var categoryReports = Enumerable.From(reports)
                    .GroupBy(
                        "$.Category",
                        "{ ID: $.ID, Category: $.Category, Name: $.Name, Path: $.Path, ClassIcon: $.ClassIcon}",
                        "{ Category: $, Reports: $$.ToArray() }"
                    )
                    .ToArray();

            //Clean old data
            $("#reportCategories").contents().remove();

            for (var i = 0; i < categoryReports.length; i++) {
                var objCategoryReport = categoryReports[i];

                var $title = $('<p class="text-success"></p>');
                $title.html('<i class="fa fa-line-chart"></i>  ' + objCategoryReport.Category);

                var idTable = 'tblReportsBody' + objCategoryReport.Category;
                var $tableOfReports = $(
                    '<table class="table table-hover"> ' +
                    '    <tbody id="' + idTable + '"> ' +
                    '    </tbody> ' +
                    '</table> '
                );

                $("#reportCategories").append($title);
                $("#reportCategories").append($tableOfReports);

                //Add rows to table
                for (var j = 0; j < objCategoryReport.Reports.length; j++) {
                    var objReport = objCategoryReport.Reports[j];

                    var $rowReport = $('<tr></tr>');

                    var $colName = $('<td style="width: 90%;"></td>');
                    $colName.html('<i class="fa fa-bar-chart"></i> ' + objReport.Name);
                    $rowReport.append($colName);

                    var $colIsGranted = $('<td style="width: 10%;"></td>');
                    $colIsGranted.html('<input type="checkbox" idReport="' + objReport.ID + '" class="skin-square-green chkIsGranted">');
                    $rowReport.append($colIsGranted);

                    $tableOfReports.append($rowReport);
                }
            }

            _GLOBAL_SETTINGS.iCheck();
        };

        this.loadReportsOfRole = function (idRole) {
            var idRole = $("#selRoleOfReports").val();
            var reportsOfRole = _findAllObjByProperty(rolesXReports, "IDRole", idRole);

            $("[idReport]").each(function (indexEach, item) {
                var $el = $(item);
                var idReport = $el.attr("idReport");
                var isGranted = $el.is(":checked");

                var hasReportAccess = _findOneObjByProperty(reportsOfRole, "IDReport", idReport);

                //Off ifChanged Event
                var fnIfChanged;
                var events = _getBindEvends($el);
                if (events.ifChanged) {
                    fnIfChanged = events.ifChanged["0"].handler;
                    $el.off('ifChanged');
                }

                // Check or Uncheck Input
                if (hasReportAccess) {
                    if (isGranted == false) {
                        $el.iCheck('check');
                    }
                } else {
                    if (isGranted) {
                        $el.iCheck('uncheck');
                    }
                }

                //Bind ifChanged Event
                if (fnIfChanged) {
                    $el.on('ifChanged', fnIfChanged);
                }
            });
        };

        this.saveRoleXReports = function () {
            var spAdminReport = _getDBObjectFullName("AdminRoleXReport");
            var idRole = $("#selRoleOfReports").val();

            if (idRole != 0) {
                var totalReports = $("[idReport]").length;
                $("[idReport]").each(function (indexEach, item) {
                    var $el = $(item);
                    var idReport = $el.attr("idReport");
                    var isGranted = $el.is(":checked");
                    var action = "Insert";

                    if (isGranted == false) {
                        action = "Delete";
                    }

                    //Save changes only
                    var reportsOfRole = _findAllObjByProperty(rolesXReports, "IDRole", idRole);
                    var hasReportAccess = _findOneObjByProperty(reportsOfRole, "IDReport", idReport);

                    //Insert only: If not exist and Insert OR exists and delete
                    if ((action == "Insert" && !hasReportAccess) || (action == "Delete" && hasReportAccess)) {
                        _callProcedure({
                            loadingMsgType: "fullLoading",
                            loadingMsg: "Saving reports of role...",
                            name: "[dbo].[" + spAdminReport + "]",
                            params: [
                                { "Name": "@Action", "Value": action },
                                { "Name": "@IDRole", "Value": idRole },
                                { "Name": "@IDReport", "Value": idReport },
                                { "Name": "@SessionSOEID", "Value": _getSOEID() }
                            ],
                            success: {}
                        });
                    }
                });

                _execOnAjaxComplete(function () {
                    _loadRolesXReportsFromDB(function (newRolesXReports) {
                        //Set new list of reports of role
                        rolesXReports = newRolesXReports;

                        //Reload roles to show new items mapped
                        classTabRoles.loadDropDownOfRoles("#selRoleOfReports", newRolesXReports, $("#selRoleOfReports").val());

                        //Show message
                        _showNotification("success", "Reports of role were saved successfully.", "RoleSaved");

                        //Refresh Session Reports
                        _refreshSession();
                    });
                });
            } else {
                _showNotification("error", "Please select at least one Role.");
            }
        }

        //Load roles in "Mapping Reports to Roles"
        classTabRoles.loadDropDownOfRoles("#selRoleOfReports", rolesXReports);

        //Load Reports Availables
        this.loadReports();
    }
    this.objRoleXReports = new ClassRoleXReports();

}