﻿/// <reference path="../Shared/plugins/util/global.js" />

var _SQLExistsObjects = "";
var _DBObjectList;
var objTablInitialConfig;

$(document).ready(function () {

    //Load Global DB Structure table
    $("#btnReviewGlobalDBStructure").click(function () {
        var typeStructure = $('[name^="typeBDConfig"]:checked').val();
        reviewDatabase(typeStructure, "Global");

        //Show Save and Edit Buttons
        $("#btnEditBDJSon").show();
        $("#btnSaveDBStructure").show();

        //Hide Delete and Create Run Query
        $("#btnDeleteObjects").hide();
        $("#btnCreateMissingObjects").hide();
        $(".btnRunDelete").hide();
        $(".btnRunCreate").hide();
    });

    //Load Local DB Structure table
    $("#btnReviewLocalDBStructure").click(function () {
        var typeStructure = $('[name^="typeBDConfig"]:checked').val();
        reviewDatabase(typeStructure, "Local");

        //Hide Save and Edit Buttons
        $("#btnEditBDJSon").hide();
        $("#btnSaveDBStructure").hide();

        //Show Delete and Create Run Query
        $("#btnDeleteObjects").show();
        $("#btnCreateMissingObjects").show();
        $(".btnRunDelete").show();
        $(".btnRunCreate").show();
    });

    //Execute delete queries in DB
    $("#btnDeleteObjects").click(function () {

        //Start deleting missing object with tab index 0
        deleteDBObjectsRecursive(0);

    });

    //Execute create queries in DB
    $("#btnCreateMissingObjects").click(function () {

        //Start creating missing object with tab index 0
        createDBObjectsRecursive(0);

    });

    //Save information to json /Content/Sources/database-structure.txt
    $("#btnSaveDBStructure").click(function () {
        saveBDStructure();
    });

    //Set Tables Prefix
    $("#txtTablesPrefix").text(_getViewVar("TablesPrefix"));

    //Set Procudures Prefix
    $("#txtProceduresPrefix").text(_getViewVar("ProcPrefix"));

    //Set Functions Prefix
    $("#txtFunctionsPrefix").text(_getViewVar("FuncPrefix"));

    //Load DB Structure
    _loadDBObjects();

    //Load Tab Initial Configuration
    _getCurrentAppData(function (appData) {
        objTablInitialConfig = new ClassTabInitialConfig();
    });
});

function ClassTabInitialConfig() {
    var classTabInitConfig = this;
    var permits = _APPDATA.Permits;
    var reports = _APPDATA.Reports;

    var roles = _APPDATA.Roles;
    var rolesXPermits = _APPDATA.RolesXPermits;
    var rolesXReports = _APPDATA.RolesXReports;

    var users = _APPDATA.Users;
    var usersXRoles = _APPDATA.UsersXRoles;

    this.updateAppDataIDs = function () {
        //--------------------------------------
        //Disable update ID Role
        //--------------------------------------

        permits = _APPDATA.Permits;
        reports = _APPDATA.Reports;
        //roles = _APPDATA.Roles;
        rolesXPermits = _APPDATA.RolesXPermits;
        rolesXReports = _APPDATA.RolesXReports;
        users = _APPDATA.Users;
        usersXRoles = _APPDATA.UsersXRoles;

        var copyPermits = JSON.parse(_toJSON(permits));
        var copyReports = JSON.parse(_toJSON(reports));
        //var copyRoles = JSON.parse(_toJSON(roles));

        //New IDs for Permits
        for (var i = 0; i < copyPermits.length; i++) {
            var objCopyPermit = copyPermits[i];
            objCopyPermit.OldID = objCopyPermit.ID;
            objCopyPermit.ID = (i + 1);

            //Set new Identity ID
            permits[i].ID = objCopyPermit.ID;
        }
        //New IDs for ParentIDs
        for (var i = 0; i < permits.length; i++) {
            var objParentPermit = _findOneObjByProperty(copyPermits, "OldID", permits[i].ParentID);
            if (objParentPermit) {
                permits[i].ParentID = objParentPermit.ID;
            }
        }
        saveData("Permits", permits);

        //New IDs for Reports
        for (var i = 0; i < copyReports.length; i++) {
            var objCopyReport = copyReports[i];
            objCopyReport.OldID = objCopyReport.ID;
            objCopyReport.ID = (i + 1);

            //Set new Identity ID
            reports[i].ID = objCopyReport.ID;
        }
        saveData("Reports", reports);

        ////New IDs for Roles
        //for (var i = 0; i < copyRoles.length; i++) {
        //    var objCopyRole = copyRoles[i];
        //    objCopyRole.OldID = objCopyRole.ID;
        //    objCopyRole.ID = (i + 1);

        //    //Set new Identity ID
        //    roles[i].ID = objCopyRole.ID;
        //}
        //saveData("Roles", roles);

        //New IDs for Role x Permits
        var newRolesXPermits = [];
        for (var i = 0; i < rolesXPermits.length; i++) {
            var objRoleXPermit = rolesXPermits[i];

            //var objRole = _findOneObjByProperty(copyRoles, "OldID", objRoleXPermit.IDRole);
            var objPermit = _findOneObjByProperty(copyPermits, "OldID", objRoleXPermit.IDPermit);

            //if (objRole && objPermit) {
            if (objPermit) {
                //objRoleXPermit.IDRole = objRole.ID;
                objRoleXPermit.IDPermit = objPermit.ID;
                newRolesXPermits.push(objRoleXPermit);
            }
        }
        saveData("RolesXPermits", newRolesXPermits);

        //New IDs for Role x Reports
        var newRolesXReports = [];
        for (var i = 0; i < rolesXReports.length; i++) {
            var objRoleXReport = rolesXReports[i];

            //var objRole = _findOneObjByProperty(copyRoles, "OldID", objRoleXReport.IDRole);
            var objReport = _findOneObjByProperty(copyReports, "OldID", objRoleXReport.IDReport);

            //if (objRole && objReport) {
            if (objReport) {
                //objRoleXReport.IDRole = objRole.ID;
                objRoleXReport.IDReport = objReport.ID;
                newRolesXReports.push(objRoleXReport);
            }
        }
        saveData("RolesXReports", newRolesXReports);

        //New IDs for User x Roles
        //var newUsersXRoles = [];
        //for (var i = 0; i < usersXRoles.length; i++) {
        //    var objUserXRole = usersXRoles[i];
        //    var objRole = _findOneObjByProperty(copyRoles, "OldID", objUserXRole.IDRole);

        //    if (objRole) {
        //        objUserXRole.IDRole = objRole.ID;
        //        newUsersXRoles.push(objUserXRole);
        //    }
        //}
        //saveData("UsersXRoles", newUsersXRoles);
    };
    
    function saveData(keyToSave, newData) {
        _APPDATA[keyToSave] = newData;

        //Save app info data
        _callServer({
            loadingMsg: "Saving changes...",
            url: '/Configuration/AppManager/SaveAppData',
            data: {
                pjsonAppData: _toJSON(_APPDATA, null, "\t"),
                pkeyAppName: _getUserInfo().DefaultAppKey
            },
            type: "post",
            success: function (savingStatus) {
                _showNotification("success", keyToSave + " data for '" + _getUserInfo().DefaultAppKey + "' was saved successfully.", "InitialDataSaved");
            }
        });
    }
    function deleteData(KeyToDelete, onConfirm) {
        var htmlContentModal = 'Are you sure you want to delete you local data for "' + KeyToDelete + '"?';

        _showModal({
            width: '35%',
            modalId: "modalImportData",
            addCloseButton: true,
            buttons: [{
                name: "Delete",
                class: "btn-danger",
                onClick: function () {
                    onConfirm();
                }
            }],
            title: "<i class='fa fa-warning'></i> Warning Message",
            contentHtml: htmlContentModal
        });
    }
    function importData(options) {
        //options = {
        //    dbObjectName: "Permit",
        //    columns: [
        //        { to: "ID"},
        //        { to: "NumOrder", from: "NUMBER_ORDER2"},
        //        { to: "ParentID"},
        //        { to: "ClassIcon", default: "fa-dashboard"},
        //        { to: "FlagIsGlobal", value: "0"}
        //    ],
        //    onSuccess: function(permits){}
        //};

        if (_getViewVar("DBIsConfigured") == "1") {
            var htmlContentModal = 'Are you sure you want to import the data from Database?';

            _showModal({
                width: '35%',
                modalId: "modalImportData",
                addCloseButton: true,
                buttons: [{
                    name: "Import",
                    class: "btn-danger",
                    onClick: function () {
                        var tblFullName = _getDBObjectFullName(options.dbObjectName);

                        _callServer({
                            loadingMsg: "Loading data for '" + tblFullName + "'...",
                            url: '/Configuration/AdminDBTable/List',
                            data: { "ptable": tblFullName },
                            success: function (responseList) {
                                if (responseList && responseList.length > 0) {
                                    //Create array to import data
                                    options.importTo = [];

                                    //Add row information by each row
                                    for (var i = 0; i < responseList.length; i++) {
                                        var tempRow = responseList[i];

                                        //Ignore Deleted Data
                                        if (tempRow.IsDeleted == 'False' || typeof tempRow.IsDeleted == 'undefined') {
                                            var newDataToImport = {};

                                            //Create new data
                                            for (var j = 0; j < options.columns.length; j++) {
                                                var objCol = options.columns[j];
                                                var colNameToImport = (objCol.from ? objCol.from : objCol.to);
                                                var valueToImport = (objCol.value ? objCol.value : tempRow[colNameToImport]);
                                                var defaultValue = "";

                                                if (typeof objCol.default != 'undefined') {
                                                    if (objCol.default == ":autonumeric") {
                                                        defaultValue = (i + 1);
                                                    } else {
                                                        defaultValue = objCol.default;
                                                    }
                                                }

                                                newDataToImport[objCol.to] = (typeof valueToImport != 'undefined' ? valueToImport : defaultValue);
                                            }

                                            //Add new data
                                            options.importTo.push(newDataToImport);
                                        }
                                    }
                                    //Reload list of Data to show new information
                                    if (options.onSuccess) {
                                        options.onSuccess(options.importTo);
                                    }
                                } else {
                                    _showNotification("error", "Table '" + tblFullName + "' is empty");
                                }
                            }
                        });
                    }
                }],
                title: "<i class='fa fa-warning'></i> Warning Message",
                contentHtml: htmlContentModal
            });
        } else {
            _showNotification("error", "Framework tables not found for " + _getUserInfo().DefaultAppKey);
        }

    }

    //Class for Load Tab Permits
    this.objTabPermits = new _classNestableList({
        id: "#content-list-permits",
        entity: "Permit",
        maxDepth: 3,
        buttons: {
            addNew: {
                //Autogenerate [ID] columns on save new item
                identity: true
            },
            importDB: {}
        },
        onSave: function (newPermits) {
            saveData("Permits", newPermits);

            //Refresh Permits in "Add Permits to Role"
            classTabInitConfig.objTabRoles.objRoleXPermits.loadPermits();
        },
        onGenerateInserts: function () {
            var sqlInserts = "";

            if (permits && permits.length > 0) {
                var tblPermit = _getDBObjectFullName("Permit");

                sqlInserts =
                "SET IDENTITY_INSERT [dbo].[" + tblPermit + "] ON \n" +
                "GO \n" +
                "INSERT [dbo].[" + tblPermit + "] ([ID], [ParentID], [NumOrder], [ClassIcon], [Category], [Name], [Path], [AccessCommentHtml], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [IsDeleted]) \n" +
                "VALUES \n";

                //Add Permits
                var cantPermits = permits.length;
                $(permits).each(function (i) {
                    //Last iteration
                    if (cantPermits == (i + 1)) {
                        sqlInserts += "  ( " + this.ID + ", " + (this.ParentID ? this.ParentID : 'NULL') + ",  " + this.NumOrder + ", '" + this.ClassIcon + "', '" + this.Category + "', '" + this.Name + "', '" + this.Path + "', " + (this.AccessCommentHtml ? ("'" + this.AccessCommentHtml + "'"): 'NULL') + ", '" + _getSOEID() + "', GETDATE(), NULL, NULL, 0) \n";
                    } else {
                        sqlInserts += "  ( " + this.ID + ", " + (this.ParentID ? this.ParentID : 'NULL') + ",  " + this.NumOrder + ", '" + this.ClassIcon + "', '" + this.Category + "', '" + this.Name + "', '" + this.Path + "', " + (this.AccessCommentHtml ? ("'" + this.AccessCommentHtml + "'"): 'NULL') + ", '" + _getSOEID() + "', GETDATE(), NULL, NULL, 0), \n";
                    }
                });
                sqlInserts +=
                "GO \n" +
                "SET IDENTITY_INSERT [dbo].[" + tblPermit + "] OFF \n" +
                "GO";
            }

            return sqlInserts;
        },
        items: _APPDATA.Permits,

        //type: {string} {input-icon} {radio}
        columns: [
            { type: 'string', fieldKey: 'ID', showInList: false, showInForm: false },
            { type: 'string', fieldKey: 'ParentID', showInList: false, showInForm: false },
            { type: 'string', fieldKey: 'Category', width: '160px', fieldPlaceHolder: 'eg. Main', valueTemplate: ':NumOrder - :Category', showInList: true, showInForm: true },
            { type: 'input-icon', fieldKey: 'ClassIcon', fieldPlaceHolder: 'eg. fa-calendar', showInList: false, showInForm: true },
            { type: 'string', fieldKey: 'Name', width: '200px', fieldPlaceHolder: 'eg. EUC Home', valueTemplate: "<i class='fa :ClassIcon'></i> _cropWord(:Name, 15)", showInList: true, showInForm: true },
            { type: 'string', fieldKey: 'Path', width: '310px', fieldPlaceHolder: 'eg. /EUCInventory/', valueTemplate: "_cropWord(:Path, 39)", showInList: true, showInForm: true },
            { type: 'textarea', fieldKey: 'AccessCommentHtml', showInList: false, fieldPlaceHolder: 'eg. Only Admins and Managers have access to Reports.', showInForm: true },
            { type: 'string', fieldKey: 'NumOrder', showInList: false, showInForm: false},

            //FlagIsGlobal not exists in DB so importValue is need it
            { type: 'radio', fieldKey: 'FlagIsGlobal', showInList: true, showInForm: true, importValue: "0" }
        ]
    });

    //Class for Load Tab Reports
    this.objTabReports = new _classNestableList({
        id: "#content-list-reports",
        entity: "Report",
        maxDepth: 1,
        buttons: {
            addNew: {
                //Autogenerate [ID] columns on add new item
                identity: true
            },
            importDB: {}
        },
        onSave: function (newReports) {
            saveData("Reports", newReports);

            //Refresh Reports in "Add Reports to Role"
            classTabInitConfig.objTabRoles.objRoleXReports.loadReports();
        },
        onGenerateInserts: function () {
            var sqlInserts = "";

            if (reports && reports.length > 0) {
                var tblReport = _getDBObjectFullName("Report");

                sqlInserts =
                "SET IDENTITY_INSERT [dbo].[" + tblReport + "] ON \n" +
                "GO \n" +
                "INSERT [dbo].[" + tblReport + "] ([ID], [NumOrder], [Category], [Name], [Path], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [IsDeleted]) \n" +
                "VALUES \n";

                //Add Reports
                var cantReports = reports.length;
                $(reports).each(function (i) {
                    //Last iteration
                    if (cantReports == (i + 1)) {
                        sqlInserts += "  ( " + this.ID + ", " + this.NumOrder + ", '" + this.Category + "', '" + this.Name + "', '" + this.Path + "', '" + _getSOEID() + "', GETDATE(), NULL, NULL, 0) \n";
                    } else {
                        sqlInserts += "  ( " + this.ID + ", " + this.NumOrder + ", '" + this.Category + "', '" + this.Name + "', '" + this.Path + "', '" + _getSOEID() + "', GETDATE(), NULL, NULL, 0), \n";
                    }
                });
                sqlInserts +=
                "GO \n" +
                "SET IDENTITY_INSERT [dbo].[" + tblReport + "] OFF \n" +
                "GO";
            }

            return sqlInserts;
        },
        items: _APPDATA.Reports,

        //type: {string} {input-icon} {radio}
        columns: [
            { type: 'string', fieldKey: 'ID', showInList: false, showInForm: false },
            { type: 'string', fieldKey: 'Category', width: '150px', fieldPlaceHolder: 'eg. EUC Metrics Reports', valueTemplate: '_cropWord(:NumOrder - :Category, 15)', showInList: true, showInForm: true },
            { type: 'string', fieldKey: 'Name', width: '220px', fieldPlaceHolder: 'eg. EUC General Information', valueTemplate: "<i class='fa fa-bar-chart'></i> _cropWord(:Name, 25)", showInList: true, showInForm: true },
            { type: 'string', fieldKey: 'Path', width: '310px', fieldPlaceHolder: 'eg. /EUCHRSS/EUC General Information', valueTemplate: "_cropWord(:Path, 39)", showInList: true, showInForm: true },

            //NumOrder is new column so importDefault (:autonumeric) is going to add a identity number
            { type: 'string', fieldKey: 'NumOrder', showInList: false, showInForm: false, importDefault: ":autonumeric" },

            //FlagIsGlobal not exists in DB so importValue is need it
            { type: 'radio', fieldKey: 'FlagIsGlobal', showInList: true, showInForm: true, importValue: "0" }

        ]
    });

    //Class for Load Tab Roles
    function ClassTabRoles() {
        var classTabRoles = this;

        this.loadDropDownOfRoles = function (idSelect, mappingData, idRoleSelected) {
            var $select = $(idSelect);

            $(idSelect).find("option").remove();
            $select.append($('<option>', {
                value: "0",
                text: "-- Select a Role --"
            }));

            //Load options in select
            for (var i = 0; i < roles.length; i++) {
                var objRole = roles[i];
                var optionName = objRole.Name;
                var selected = false;

                if (mappingData) {
                    var itemsOfRole = _findAllObjByProperty(mappingData, "IDRole", objRole.ID);
                    optionName += " (" + itemsOfRole.length + " Items granted)";
                }

                var objOption = {
                    value: objRole.ID,
                    text: optionName
                };

                if (idRoleSelected && objRole.ID == idRoleSelected) {
                    selected = true;
                }

                if (selected) {
                    objOption.selected = true;
                }

                $select.append($('<option>', objOption));
            }

            $select.select2({
                placeholder: 'Choose a role:',
                allowClear: true
            }).on('select2-open', function () {
                // Adding Custom Scrollbar
                $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
            });
        };

        //Tab for add, delete and edit Role
        //--------------------------------------------
        function ClassListRoles() {
            var classListRoles = this;

            //Add new row to the table
            $(".btnAddNewRole").click(function () {
                classListRoles.addNewRole();
            });

            //On click Load Data from DB
            $(".btnLoadRoleDB").click(function () {
                importData({
                    dbObjectName: "Role",
                    columns: [
                        { to: "ID" },
                        { to: "Name" },
                        { to: "EERSMarketplaceRoleID" },
                        { to: "EERSFunctionCode" },
                        { to: "EERSFunctionDescription" },
                        { to: "EERSIgnore", default: "False" }
                    ],
                    onSuccess: function (newRoles) {
                        //Set new data
                        roles = newRoles;

                        //Reload list of Reports to show new Num Order
                        classListRoles.loadTableRoles();

                        //Save updates
                        classListRoles.saveRoles(roles);
                    }
                });
            });

            //On click Insert Data to DB
            $(".btnInsertRoleDB").click(function () {
                console.log(classListRoles.generateInsert());
            });

            // Public functions (shared across instances)
            this.generateInsert = function () {
                var sqlInserts = "";

                if (roles && roles.length > 0) {
                    var tblRole = _getDBObjectFullName("Role");

                    sqlInserts =
                    "INSERT [dbo].[" + tblRole + "] ([ID], [Name], [EERSMarketplaceRoleID], [EERSFunctionCode], [EERSFunctionDescription], [EERSIgnore], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [IsDeleted]) \n" +
                    "VALUES \n";

                    //Add Roles
                    var cantRoles = roles.length;
                    $(roles).each(function (i) {
                        //Last iteration
                        if (cantRoles == (i + 1)) {
                            sqlInserts += "  ( " + this.ID + ", '" + this.Name + "', '" + this.EERSMarketplaceRoleID + "', '" + this.EERSFunctionCode + "', '" + this.EERSFunctionDescription + "', " + (this.EERSIgnore == "False" ? '0' : '1') + ", '" + _getSOEID() + "', GETDATE(), NULL, NULL, 0) \n";
                        } else {
                            sqlInserts += "  ( " + this.ID + ", '" + this.Name + "', '" + this.EERSMarketplaceRoleID + "', '" + this.EERSFunctionCode + "', '" + this.EERSFunctionDescription + "', " + (this.EERSIgnore == "False" ? '0' : '1') + ", '" + _getSOEID() + "', GETDATE(), NULL, NULL, 0), \n";
                        }
                    });
                    sqlInserts +=
                    "GO";
                }

                return sqlInserts;
            };

            this.loadTableRoles = function () {
                $.jqxGridApi.create({
                    showTo: "#tblRole",
                    options: {
                        //for comments or descriptions
                        height: "500",
                        autoheight: true,
                        autorowheight: false,
                        showfilterrow: true,
                        sortable: true,
                        editable: true,
                        selectionmode: "singlerow"
                    },
                    source: {
                        // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                        dataBinding: "Large Data Set Local",
                        rows: roles
                    },
                    groups: [],
                    columns: [
                        //type: string - text - number - int - float - date - time 
                        //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                        //cellsformat: ddd, MMM dd, yyyy h:mm tt
                        {
                            name: 'ID', text: 'ID', width: '4%', type: 'number', filtertype: 'number', validation: function (cell, value) {
                                //Validate Unique value for ID Role
                                var rows = $.jqxGridApi.getAllRows('#tblRole');

                                for (var i = 0; i < rows.length; i++) {
                                    var objTempRow = rows[i];

                                    if (objTempRow.ID == value) {
                                        return { result: false, message: "ID should be UNIQUE" };
                                    }
                                }
                                return true;
                            }
                        },
                        { name: 'Name', text: 'Name', width: '23%', type: 'string', filtertype: 'input' },
                        { name: 'EERSMarketplaceRoleID', text: 'Marketplace Code (eg. 165986_20)', width: '17%', type: 'string', filtertype: 'checkedlist' },
                        { name: 'EERSFunctionCode', text: 'Marketplace Name (eg. MEIL Basic)', width: '25%', type: 'string', filtertype: 'checkedlist' },
                        { name: 'EERSFunctionDescription', text: 'EERS Function Description', width: '25%', type: 'string', filtertype: 'checkedlist' },
                        { name: 'EERSIgnore', text: 'EERS Ignore', width: '6%', type: 'string', filtertype: 'checkedlist' }
                    ],
                    ready: function () {
                        //Save all roles on value changed
                        $("#tblRole").unbind('cellvaluechanged').on('cellvaluechanged', function (event) {
                            var rows = $.jqxGridApi.getAllRows('#tblRole');
                            var newRoles = [];

                            for (var i = 0; i < rows.length; i++) {
                                var objRow = rows[i];

                                newRoles.push({
                                    ID: objRow.ID,
                                    Name: objRow.Name,
                                    EERSMarketplaceRoleID: objRow.EERSMarketplaceRoleID,
                                    EERSFunctionCode: objRow.EERSFunctionCode,
                                    EERSFunctionDescription: objRow.EERSFunctionDescription,
                                    EERSIgnore: objRow.EERSIgnore
                                });
                            }

                            //sort by ID
                            newRoles.sort(_compareID);

                            //Save temp data
                            classListRoles.saveRoles(newRoles);

                            //If ID was changed refresh RolesXPermits, RolesXReports and UserXRoles
                            if (event.args.datafield == "ID") {
                                var flagToSave = false;

                                //Update Permits of Role
                                for (var i = 0; i < rolesXPermits.length; i++) {
                                    var objRoleXPermit = rolesXPermits[i];
                                    if (objRoleXPermit.IDRole == event.args.oldvalue) {
                                        objRoleXPermit.IDRole = event.args.newvalue;
                                        flagToSave = true;
                                    }
                                }
                                if (flagToSave) {
                                    saveData("RolesXPermits", rolesXPermits);
                                    flagToSave = false;
                                }

                                //Update Reports of Role
                                for (var i = 0; i < rolesXReports.length; i++) {
                                    var objRoleXReport = rolesXReports[i];
                                    if (objRoleXReport.IDRole == event.args.oldvalue) {
                                        objRoleXReport.IDRole = event.args.newvalue;
                                        flagToSave = true;
                                    }
                                }
                                if (flagToSave) {
                                    saveData("RolesXReports", rolesXReports);
                                    flagToSave = false;
                                }

                                //Update Roles of User
                                for (var i = 0; i < usersXRoles.length; i++) {
                                    var objUserXRole = usersXRoles[i];
                                    if (objUserXRole.IDRole == event.args.oldvalue) {
                                        objUserXRole.IDRole = event.args.newvalue;
                                        flagToSave = true;
                                    }
                                }
                                if (flagToSave) {
                                    saveData("UsersXRoles", usersXRoles);
                                }
                            }
                        });
                    }
                });
            };

            this.addNewRole = function () {
                var $jqxGrid = $("#tblRole");
                var datarow = {};

                datarow["ID"] = "0";
                datarow["Name"] = "";
                datarow["EERSFunctionCode"] = "";
                datarow["EERSFunctionDescription"] = "";
                datarow["EERSIgnore"] = "";

                var commit = $jqxGrid.jqxGrid('addrow', null, datarow);
            };

            this.saveRoles = function (roles) {
                saveData("Roles", roles);

                //Reload roles in "Add Permits to Role"
                classTabRoles.loadDropDownOfRoles("#selRoleOfPermits", rolesXPermits);

                //Reload roles in "Add Reports to Role"
                classTabRoles.loadDropDownOfRoles("#selRoleOfReports", rolesXReports);
            };

            //Load Table of Roles
            this.loadTableRoles();
        }
        this.objListRoles = new ClassListRoles();

        //Tab for add permits to Role
        //--------------------------------------------
        function ClassRoleXPermits() {
            var classRoleXPermits = this;

            //On Click Clean Local Data
            $(".btnCleanRoleXPermitsDB").click(function () {
                deleteData("Permits of Role", function () {
                    rolesXPermits = [];
                    classRoleXPermits.saveRoleXPermits(rolesXPermits);
                });
            });

            //On click Load Data from DB
            $(".btnLoadRoleXPermitsDB").click(function () {
                importData({
                    dbObjectName: "RoleXPermit",
                    columns: [
                        { to: "IDRole" },
                        { to: "IDPermit" },
                        { to: "CreatedBy" },
                        { to: "CreatedDate" }
                    ],
                    onSuccess: function (newRoleXPermits) {
                        var filterRoles = [];
                        var filterRoleXPermits = [];

                        //Filter only existing Roles
                        for (var i = 0; i < roles.length; i++) {
                            var objRole = roles[i];
                            $.merge(filterRoles, _findAllObjByProperty(newRoleXPermits, "IDRole", objRole.ID));
                        }

                        //Filter only existing Permits
                        for (var i = 0; i < permits.length; i++) {
                            var objPermit = permits[i];
                            $.merge(filterRoleXPermits, _findAllObjByProperty(filterRoles, "IDPermit", objPermit.ID));
                        }

                        if (filterRoleXPermits.length > 0) {
                            //Set new data
                            rolesXPermits = filterRoleXPermits;

                            //Save updates
                            classRoleXPermits.saveRoleXPermits(rolesXPermits);
                        } else {
                            _showNotification("error", "No data available to import, only found (" + newRoleXPermits.length + ") rows deleted in DB.");
                        }
                    }
                });
            });

            //On click check all Permits
            $(".checkAllRoleXPermits").on("ifChanged", function () {
                var $checkAll = $(this);
                var grantToAll = $checkAll.is(":checked");

                $("[idPermit]").each(function (indexEach, item) {
                    var $el = $(item);
                    var idPermit = $el.attr("idPermit");
                    var isGranted = $el.is(":checked");

                    if (isGranted != grantToAll) {
                        if (grantToAll) {
                            $el.iCheck('check');
                        } else {
                            $el.iCheck('uncheck');
                        }
                    }
                });
            });

            //On change select of roles
            $("#selRoleOfPermits").change(function () {
                classRoleXPermits.loadPermitsOfRole($("#selRoleOfPermits").val());
            });

            // Public functions (shared across instances)
            this.generateInsert = function () {
                var sqlInserts = "";

                if (rolesXPermits && rolesXPermits.length > 0) {
                    var tblRoleXPemits = _getDBObjectFullName("RoleXPermit");
                    //"IDRole": "1",
		            //"IDPermit": "1",
		            //"CreatedBy": "TT69564",
		            //"CreatedDate": "3/1/2017 5:32:03 PM"
                    sqlInserts =
                    "SET IDENTITY_INSERT [dbo].[" + tblRoleXPemits + "] ON \n" +
                    "GO \n" +
                    "INSERT [dbo].[" + tblRoleXPemits + "] ([ID], [IDRole], [IDPermit], [CreatedBy], [CreatedDate]) \n" +
                    "VALUES \n";

                    //Add Permits of Role
                    var cantRoles = rolesXPermits.length;
                    $(rolesXPermits).each(function (i) {
                        //Last iteration
                        if (cantRoles == (i + 1)) {
                            sqlInserts += "  ( " + (i + 1) + ", " + this.IDRole + ", " + this.IDPermit + ", '" + this.CreatedBy + "', '" + this.CreatedDate + "') \n";
                        } else {
                            sqlInserts += "  ( " + (i + 1) + ", " + this.IDRole + ", " + this.IDPermit + ", '" + this.CreatedBy + "', '" + this.CreatedDate + "'), \n";
                        }
                    });
                    sqlInserts +=
                    "GO \n" +
                    "SET IDENTITY_INSERT [dbo].[" + tblRoleXPemits + "] OFF \n" +
                    "GO";
                }

                return sqlInserts;
            };

            this.loadPermits = function () {
                var categoryPermits = Enumerable.From(permits)
                        .GroupBy(
                            "$.Category",
                            "{ ID: $.ID, Category: $.Category, Name: $.Name, Path: $.Path, ClassIcon: $.ClassIcon}",
                            "{ Category: $, Permits: $$.ToArray() }"
                        )
                        .ToArray();

                //Clean old data
                $("#permitCategories").contents().remove();

                for (var i = 0; i < categoryPermits.length; i++) {
                    var objCategoryPermit = categoryPermits[i];

                    var $title = $('<p class="text-success"></p>');
                    $title.html('<i class="fa fa-cube"></i>  ' + objCategoryPermit.Category);

                    var idTable = 'tblPermitsBody' + objCategoryPermit.Category;
                    var $tableOfPermits = $(
                        '<table class="table table-hover"> ' +
                        '    <tbody id="' + idTable + '"> ' +
                        '    </tbody> ' +
                        '</table> '
                    );

                    $("#permitCategories").append($title);
                    $("#permitCategories").append($tableOfPermits);

                    //Add rows to table
                    for (var j = 0; j < objCategoryPermit.Permits.length; j++) {
                        var objPermit = objCategoryPermit.Permits[j];

                        var $rowPermit = $('<tr></tr>');

                        var $colName = $('<td style="width: 90%;"></td>');
                        $colName.html('<i class="fa ' + objPermit.ClassIcon + '"></i> ' + objPermit.Name);
                        $rowPermit.append($colName);

                        var $colIsGranted = $('<td style="width: 10%;"></td>');
                        $colIsGranted.html('<input type="checkbox" idPermit="' + objPermit.ID + '" class="skin-square-green chkIsGranted">');
                        $rowPermit.append($colIsGranted);

                        $tableOfPermits.append($rowPermit);
                    }
                }

                _GLOBAL_SETTINGS.iCheck();

                //Add event on change iCheck
                $("#permitCategories").find("[idPermit]").on("ifChanged", function (event) {
                    var $el = $(this);
                    var idRole = $("#selRoleOfPermits").val();
                    var idPermit = $el.attr("idPermit");
                    var isGranted = $el.is(":checked");

                    if (idRole != "0") {
                        var permitsOfRole = _findAllObjByProperty(rolesXPermits, "IDRole", idRole);
                        var hasPermitAccess = _findOneObjByProperty(permitsOfRole, "IDPermit", idPermit);

                        if (hasPermitAccess) {
                            //Remove Permit
                            if (!isGranted) {
                                var indexToDelete = rolesXPermits.indexOf(hasPermitAccess);
                                rolesXPermits.splice(indexToDelete, 1);

                                //Save updates
                                classRoleXPermits.saveRoleXPermits(rolesXPermits);
                            }
                        } else {
                            //Add new record
                            if (isGranted) {
                                rolesXPermits.push({
                                    IDRole: idRole,
                                    IDPermit: idPermit,
                                    CreatedBy: _getSOEID(),
                                    CreatedDate: moment().format('M/D/YYYY h:mm:ss A')
                                });

                                //Save updates
                                classRoleXPermits.saveRoleXPermits(rolesXPermits);
                            }
                        }
                    } else {
                        _showNotification("error", "Please select a Role", "ErrorSelectRole");
                    }
                });
            };

            this.loadPermitsOfRole = function (idRole) {
                var idRole = $("#selRoleOfPermits").val();
                var permitsOfRole = _findAllObjByProperty(rolesXPermits, "IDRole", idRole);

                $("[idPermit]").each(function (indexEach, item) {
                    var $el = $(item);
                    var idPermit = $el.attr("idPermit");
                    var isGranted = $el.is(":checked");

                    var hasPermitAccess = _findOneObjByProperty(permitsOfRole, "IDPermit", idPermit);

                    //Off ifChanged Event
                    var fnIfChanged;
                    var events = _getBindEvends($el);
                    if (events.ifChanged) {
                        fnIfChanged = events.ifChanged["0"].handler;
                        $el.off('ifChanged');
                    }

                    // Check or Uncheck Input
                    if (hasPermitAccess) {
                        if (isGranted == false) {
                            $el.iCheck('check');
                        }
                    } else {
                        if (isGranted) {
                            $el.iCheck('uncheck');
                        }
                    }

                    //Bind ifChanged Event
                    if (fnIfChanged) {
                        $el.on('ifChanged', fnIfChanged);
                    }
                });
            };

            this.saveRoleXPermits = function (newRolesXPermits) {
                //Save Data in Initial Configuration
                saveData("RolesXPermits", newRolesXPermits);

                //Reload roles to show new items mapped
                classTabRoles.loadDropDownOfRoles("#selRoleOfPermits", newRolesXPermits, $("#selRoleOfPermits").val());

                //Reload list of Permits to show new item granted
                classRoleXPermits.loadPermitsOfRole($("#selRoleOfPermits").val());
            };

            //Load roles in "Mapping Permits to Roles"
            classTabRoles.loadDropDownOfRoles("#selRoleOfPermits", rolesXPermits);

            //Load Permits Availables
            this.loadPermits();
        }
        this.objRoleXPermits = new ClassRoleXPermits();

        //Tab for add reports to Role
        //--------------------------------------------
        function ClassRoleXReports() {
            var classRoleXReports = this;

            //On Click Clean Local Data
            $(".btnCleanRoleXReportsDB").click(function () {
                deleteData("Reports of Role", function () {
                    rolesXReports = [];
                    classRoleXReports.saveRoleXReports(rolesXReports);
                });
            });

            //On click Load Data from DB
            $(".btnLoadRoleXReportsDB").click(function () {
                importData({
                    dbObjectName: "RoleXReport",
                    columns: [
                        { to: "IDRole" },
                        { to: "IDReport" },
                        { to: "CreatedBy" },
                        { to: "CreatedDate" }
                    ],
                    onSuccess: function (newRoleXReports) {
                        var filterRoles = [];
                        var filterRoleXReports = [];

                        //Filter only existing Roles
                        for (var i = 0; i < roles.length; i++) {
                            var objRole = roles[i];
                            $.merge(filterRoles, _findAllObjByProperty(newRoleXReports, "IDRole", objRole.ID));
                        }

                        //Filter only existing Reports
                        for (var i = 0; i < reports.length; i++) {
                            var objReport = reports[i];
                            $.merge(filterRoleXReports, _findAllObjByProperty(filterRoles, "IDReport", objReport.ID));
                        }

                        if (filterRoleXReports.length > 0) {
                            //Set new data
                            rolesXReports = filterRoleXReports;

                            //Save updates
                            classRoleXReports.saveRoleXReports(rolesXReports);
                        } else {
                            _showNotification("error", "No data available to import, only found (" + newRoleXReports.length + ") rows deleted in DB.");
                        }
                    }
                });
            });

            //On click check all Reports
            $(".checkAllRoleXReports").on("ifChanged", function () {
                var $checkAll = $(this);
                var grantToAll = $checkAll.is(":checked");

                $("[idReport]").each(function (indexEach, item) {
                    var $el = $(item);
                    var idReport = $el.attr("idReport");
                    var isGranted = $el.is(":checked");

                    if (isGranted != grantToAll) {
                        if (grantToAll) {
                            $el.iCheck('check');
                        } else {
                            $el.iCheck('uncheck');
                        }
                    }
                });
            });

            //On change select of roles
            $("#selRoleOfReports").change(function () {
                classRoleXReports.loadReportsOfRole($("#selRoleOfReports").val());
            });

            // Public functions (shared across instances)
            this.generateInsert = function () {
                var sqlInserts = "";

                if (rolesXReports && rolesXReports.length > 0) {
                    var tblRoleXPemits = _getDBObjectFullName("RoleXReport");
                    //"IDRole": "1",
                    //"IDReport": "1",
                    //"CreatedBy": "TT69564",
                    //"CreatedDate": "3/1/2017 5:32:03 PM"
                    sqlInserts =
                    "SET IDENTITY_INSERT [dbo].[" + tblRoleXPemits + "] ON \n" +
                    "GO \n" +
                    "INSERT [dbo].[" + tblRoleXPemits + "] ([ID], [IDRole], [IDReport], [CreatedBy], [CreatedDate]) \n" +
                    "VALUES \n";

                    //Add Reports of Role
                    var cantRoles = rolesXReports.length;
                    $(rolesXReports).each(function (i) {
                        //Last iteration
                        if (cantRoles == (i + 1)) {
                            sqlInserts += "  ( " + (i + 1) + ", " + this.IDRole + ", " + this.IDReport + ", '" + this.CreatedBy + "', '" + this.CreatedDate + "') \n";
                        } else {
                            sqlInserts += "  ( " + (i + 1) + ", " + this.IDRole + ", " + this.IDReport + ", '" + this.CreatedBy + "', '" + this.CreatedDate + "'), \n";
                        }
                    });
                    sqlInserts +=
                    "GO \n" +
                    "SET IDENTITY_INSERT [dbo].[" + tblRoleXPemits + "] OFF \n" +
                    "GO";
                }

                return sqlInserts;
            };

            this.loadReports = function () {
                var categoryReports = Enumerable.From(reports)
                        .GroupBy(
                            "$.Category",
                            "{ ID: $.ID, Category: $.Category, Name: $.Name, Path: $.Path, ClassIcon: $.ClassIcon}",
                            "{ Category: $, Reports: $$.ToArray() }"
                        )
                        .ToArray();

                //Clean old data
                $("#reportCategories").contents().remove();

                for (var i = 0; i < categoryReports.length; i++) {
                    var objCategoryReport = categoryReports[i];

                    var $title = $('<p class="text-success"></p>');
                    $title.html('<i class="fa fa fa-line-chart"></i>  ' + objCategoryReport.Category);

                    var idTable = 'tblReportsBody' + objCategoryReport.Category;
                    var $tableOfReports = $(
                        '<table class="table table-hover"> ' +
                        '    <tbody id="' + idTable + '"> ' +
                        '    </tbody> ' +
                        '</table> '
                    );

                    $("#reportCategories").append($title);
                    $("#reportCategories").append($tableOfReports);

                    //Add rows to table
                    for (var j = 0; j < objCategoryReport.Reports.length; j++) {
                        var objReport = objCategoryReport.Reports[j];

                        var $rowReport = $('<tr></tr>');

                        var $colName = $('<td style="width: 90%;"></td>');
                        $colName.html('<i class="fa fa-bar-chart"></i> ' + objReport.Name);
                        $rowReport.append($colName);

                        var $colIsGranted = $('<td style="width: 10%;"></td>');
                        $colIsGranted.html('<input type="checkbox" idReport="' + objReport.ID + '" class="skin-square-green chkIsGranted">');
                        $rowReport.append($colIsGranted);

                        $tableOfReports.append($rowReport);
                    }
                }

                _GLOBAL_SETTINGS.iCheck();

                //Add event on change iCheck
                $("#reportCategories").find("[idReport]").on("ifChanged", function (event) {
                    var $el = $(this);
                    var idRole = $("#selRoleOfReports").val();
                    var idReport = $el.attr("idReport");
                    var isGranted = $el.is(":checked");

                    if (idRole != "0") {
                        var reportsOfRole = _findAllObjByProperty(rolesXReports, "IDRole", idRole);
                        var hasReportAccess = _findOneObjByProperty(reportsOfRole, "IDReport", idReport);

                        if (hasReportAccess) {
                            //Remove Report
                            if (!isGranted) {
                                var indexToDelete = rolesXReports.indexOf(hasReportAccess);
                                rolesXReports.splice(indexToDelete, 1);

                                //Save updates
                                classRoleXReports.saveRoleXReports(rolesXReports);
                            }
                        } else {
                            //Add new record
                            if (isGranted) {
                                rolesXReports.push({
                                    IDRole: idRole,
                                    IDReport: idReport,
                                    CreatedBy: _getSOEID(),
                                    CreatedDate: moment().format('M/D/YYYY h:mm:ss A')
                                });

                                //Save updates
                                classRoleXReports.saveRoleXReports(rolesXReports);
                            }
                        }
                    } else {
                        _showNotification("error", "Please select a Role", "ErrorSelectRole");
                    }
                });
            };

            this.loadReportsOfRole = function (idRole) {
                var idRole = $("#selRoleOfReports").val();
                var reportsOfRole = _findAllObjByProperty(rolesXReports, "IDRole", idRole);

                $("[idReport]").each(function (indexEach, item) {
                    var $el = $(item);
                    var idReport = $el.attr("idReport");
                    var isGranted = $el.is(":checked");

                    var hasReportAccess = _findOneObjByProperty(reportsOfRole, "IDReport", idReport);

                    //Off ifChanged Event
                    var fnIfChanged;
                    var events = _getBindEvends($el);
                    if (events.ifChanged) {
                        fnIfChanged = events.ifChanged["0"].handler;
                        $el.off('ifChanged');
                    }

                    // Check or Uncheck Input
                    if (hasReportAccess) {
                        if (isGranted == false) {
                            $el.iCheck('check');
                        }
                    } else {
                        if (isGranted) {
                            $el.iCheck('uncheck');
                        }
                    }

                    //Bind ifChanged Event
                    if (fnIfChanged) {
                        $el.on('ifChanged', fnIfChanged);
                    }
                });
            };

            this.saveRoleXReports = function (newRolesXReports) {
                //Save Data in Initial Configuration
                saveData("RolesXReports", newRolesXReports);

                //Reload roles to show new items mapped
                classTabRoles.loadDropDownOfRoles("#selRoleOfReports", newRolesXReports, $("#selRoleOfReports").val());

                //Reload list of Reports to show new item granted
                classRoleXReports.loadReportsOfRole($("#selRoleOfReports").val());
            };

            //Load roles in "Mapping Reports to Roles"
            classTabRoles.loadDropDownOfRoles("#selRoleOfReports", rolesXReports);

            //Load Reports Availables
            this.loadReports();
        }
        this.objRoleXReports = new ClassRoleXReports();
    }
    this.objTabRoles = new ClassTabRoles();

    //Class for Load Tab Users
    function ClassTabUsers() {
        var classTabUsers = this;

        this.loadDropDownOfUsers = function (idSelect, mappingData, idUserSelected) {
            var $select = $(idSelect);

            $(idSelect).find("option").remove();
            $select.append($('<option>', {
                value: "0",
                text: "-- Select a User --"
            }));

            //Load options in select
            for (var i = 0; i < users.length; i++) {
                var objUser = users[i];
                var optionName = objUser.Name + ' [' + objUser.SOEID + ']';
                var selected = false;

                if (mappingData) {
                    var itemsOfUser = _findAllObjByProperty(mappingData, "SOEID", objUser.SOEID);
                    optionName += " (" + itemsOfUser.length + " Items granted)";
                }

                var objOption = {
                    value: objUser.SOEID,
                    text: optionName
                };

                if (idUserSelected && objUser.SOEID == idUserSelected) {
                    selected = true;
                }

                if (selected) {
                    objOption.selected = true;
                }

                $select.append($('<option>', objOption));
            }

            $select.select2({
                placeholder: 'Choose a user:',
                allowClear: true
            }).on('select2-open', function () {
                // Adding Custom Scrollbar
                $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
            });
        };

        //Add new row to the table
        $(".btnAddNewUser").click(function () {
            addNewUser();
        });

        //On click Load Data from DB
        $(".btnLoadUserDB").click(function () {
            importData({
                dbObjectName: "User",
                columns: [
                    { to: "SOEID" },
                    { to: "GEID" },
                    { to: "Email" },
                    { to: "Name" },
                    { to: "NumberOfSessions" },
                    { to: "CreatedBy" },
                    { to: "CreatedDate" },
                    { to: "ModifiedBy" },
                    { to: "ModifiedDate" },
                    { to: "LastSessionDate" },
                    { to: "Roles" },
                    { to: "IsDeleted", default: "False" }
                ],
                onSuccess: function (newUsers) {
                    //Set new data
                    users = newUsers;

                    //Reload User Table
                    classTabUsers.loadTableUsers();

                    //Save updates
                    classTabUsers.saveUsers(users);
                }
            });
        });

        //On click Insert Data to DB
        $(".btnInsertUserDB").click(function () {
            console.log(classListUsers.generateInsert());
        });

        //Map Roles to User
        //--------------------------------------------
        function ClassUserXRoles() {
            var classUserXRoles = this;

            //On Click Clean Local Data
            $(".btnCleanUserXRolesDB").click(function () {
                deleteData("Roles of User", function () {
                    usersXRoles = [];
                    classUserXRoles.saveUserXRoles(usersXRoles);
                });
            });

            //On click Load Data from DB
            $(".btnLoadUserXRolesDB").click(function () {
                importData({
                    dbObjectName: "UserXRole",
                    columns: [
                        { to: "SOEID" },
                        { to: "IDRole" },
                        { to: "CreatedBy" },
                        { to: "CreatedDate" }
                    ],
                    onSuccess: function (newUserXRoles) {
                        var filterUsers = [];
                        var filterUserXRoles = [];

                        //Filter only existing Users
                        for (var i = 0; i < users.length; i++) {
                            var objUser = users[i];
                            $.merge(filterUsers, _findAllObjByProperty(newUserXRoles, "SOEID", objUser.SOEID));
                        }

                        //Filter only existing Roles
                        for (var i = 0; i < roles.length; i++) {
                            var objRole = roles[i];
                            $.merge(filterUserXRoles, _findAllObjByProperty(filterUsers, "IDRole", objRole.ID));
                        }

                        if (filterUserXRoles.length > 0) {
                            //Update string of Roles in users
                            classUserXRoles.updateStrRolesInUsers(filterUserXRoles);

                            //Set new data
                            usersXRoles = filterUserXRoles;

                            //Save updates
                            classUserXRoles.saveUserXRoles(usersXRoles);
                        } else {
                            _showNotification("error", "No data available to import, only found (" + newUserXRoles.length + ") rows deleted in DB.");
                        }
                    }
                });
            });

            //On click check all Roles
            $(".checkAllUserXRoles").on("ifChanged", function () {
                var $checkAll = $(this);
                var grantToAll = $checkAll.is(":checked");

                $("[idRole]").each(function (indexEach, item) {
                    var $el = $(item);
                    var idRole = $el.attr("idRole");
                    var isGranted = $el.is(":checked");

                    if (isGranted != grantToAll) {
                        if (grantToAll) {
                            $el.iCheck('check');
                        } else {
                            $el.iCheck('uncheck');
                        }
                    }
                });
            });

            //On change select of users
            $("#selUserOfRoles").change(function () {
                classUserXRoles.loadRolesOfUser($("#selUserOfRoles").val());
            });

            // Public functions (shared across instances)
            this.generateInsert = function () {
                var sqlInserts = "";

                if (usersXRoles && usersXRoles.length > 0) {
                    var tblUserXRole = _getDBObjectFullName("UserXRole");

                    sqlInserts =
                    "SET IDENTITY_INSERT [dbo].[" + tblUserXRole + "] ON \n" +
                    "GO \n" +
                    "INSERT [dbo].[" + tblUserXRole + "] ([ID], [SOEID], [IDRole], [CreatedBy], [CreatedDate]) \n" +
                    "VALUES \n";

                    //Add Permits of Role
                    var cantRoles = usersXRoles.length;
                    $(usersXRoles).each(function (i) {
                        //Last iteration
                        if (cantRoles == (i + 1)) {
                            sqlInserts += "  ( " + (i + 1) + ", '" + this.SOEID + "', " + this.IDRole + ", '" + this.CreatedBy + "', '" + this.CreatedDate + "') \n";
                        } else {
                            sqlInserts += "  ( " + (i + 1) + ", '" + this.SOEID + "', " + this.IDRole + ", '" + this.CreatedBy + "', '" + this.CreatedDate + "'), \n";
                        }
                    });
                    sqlInserts +=
                    "GO \n" +
                    "SET IDENTITY_INSERT [dbo].[" + tblUserXRole + "] OFF \n" +
                    "GO";
                }

                return sqlInserts;
            };

            this.loadRoles = function () {
                var categoryRoles = Enumerable.From(roles)
                        .GroupBy(
                            "$.Category",
                            "{ ID: $.ID, Category: $.Category, Name: $.Name, Path: $.Path, ClassIcon: $.ClassIcon}",
                            "{ Category: $, Roles: $$.ToArray() }"
                        )
                        .ToArray();

                //Clean old data
                $("#roleCategories").contents().remove();

                for (var i = 0; i < categoryRoles.length; i++) {
                    var objCategoryRole = categoryRoles[i];

                    var $title = $('<p class="text-success"></p>');
                    $title.html('<i class="fa fa fa-cube"></i>  ' + (objCategoryRole.Category ? objCategoryRole.Category : 'Items' ));

                    var idTable = 'tblRolesBody' + objCategoryRole.Category;
                    var $tableOfRoles = $(
                        '<table class="table table-hover"> ' +
                        '    <tbody id="' + idTable + '"> ' +
                        '    </tbody> ' +
                        '</table> '
                    );

                    $("#roleCategories").append($title);
                    $("#roleCategories").append($tableOfRoles);

                    //Add rows to table
                    for (var j = 0; j < objCategoryRole.Roles.length; j++) {
                        var objRole = objCategoryRole.Roles[j];

                        var $rowRole = $('<tr></tr>');

                        var $colName = $('<td style="width: 90%;"></td>');
                        $colName.html('<i class="fa fa-bar-chart"></i> ' + objRole.Name);
                        $rowRole.append($colName);

                        var $colIsGranted = $('<td style="width: 10%;"></td>');
                        $colIsGranted.html('<input type="checkbox" idRole="' + objRole.ID + '" class="skin-square-green chkIsGranted">');
                        $rowRole.append($colIsGranted);

                        $tableOfRoles.append($rowRole);
                    }
                }

                _GLOBAL_SETTINGS.iCheck();

                //Add event on change iCheck
                $("#roleCategories").find("[idRole]").on("ifChanged", function (event) {
                    var $el = $(this);
                    var idUser = $("#selUserOfRoles").val();
                    var idRole = $el.attr("idRole");
                    var isGranted = $el.is(":checked");

                    if (idUser != "0") {
                        var rolesOfUser = _findAllObjByProperty(usersXRoles, "SOEID", idUser);
                        var hasRoleAccess = _findOneObjByProperty(rolesOfUser, "IDRole", idRole);

                        if (hasRoleAccess) {
                            //Remove Role
                            if (!isGranted) {
                                var indexToDelete = usersXRoles.indexOf(hasRoleAccess);
                                usersXRoles.splice(indexToDelete, 1);

                                //Save updates
                                classUserXRoles.saveUserXRoles(usersXRoles);

                                //Update string of Roles in users
                                classUserXRoles.updateStrRolesInUsers(usersXRoles);
                            }
                        } else {
                            //Add new record
                            if (isGranted) {
                                usersXRoles.push({
                                    SOEID: idUser,
                                    IDRole: idRole,
                                    CreatedBy: _getSOEID(),
                                    CreatedDate: moment().format('M/D/YYYY h:mm:ss A')
                                });

                                //Save updates
                                classUserXRoles.saveUserXRoles(usersXRoles);

                                //Update string of Roles in users
                                classUserXRoles.updateStrRolesInUsers(usersXRoles);
                            }
                        }
                    } else {
                        _showNotification("error", "Please select a User", "ErrorSelectUser");
                    }
                });
            };

            this.loadRolesOfUser = function (idUser) {
                var idUser = $("#selUserOfRoles").val();
                var rolesOfUser = _findAllObjByProperty(usersXRoles, "SOEID", idUser);

                $("[idRole]").each(function (indexEach, item) {
                    var $el = $(item);
                    var idRole = $el.attr("idRole");
                    var isGranted = $el.is(":checked");

                    var hasRoleAccess = _findOneObjByProperty(rolesOfUser, "IDRole", idRole);

                    //Off ifChanged Event
                    var fnIfChanged;
                    var events = _getBindEvends($el);
                    if (events.ifChanged) {
                        fnIfChanged = events.ifChanged["0"].handler;
                        $el.off('ifChanged');
                    }

                    // Check or Uncheck Input
                    if (hasRoleAccess) {
                        if (isGranted == false) {
                            $el.iCheck('check');
                        }
                    } else {
                        if (isGranted) {
                            $el.iCheck('uncheck');
                        }
                    }

                    //Bind ifChanged Event
                    if (fnIfChanged) {
                        $el.on('ifChanged', fnIfChanged);
                    }
                });
            };

            this.updateStrRolesInUsers = function (tempUsersXRoles) {
                //Update Roles String of Users
                for (var i = 0; i < users.length; i++) {
                    var objUser = users[i];
                    var rolesOfUser = _findAllObjByProperty(tempUsersXRoles, "SOEID", objUser.SOEID);

                    //Sort by ID
                    rolesOfUser.sort(_compareID);

                    var strRoles = "";

                    for (var j = 0; j < rolesOfUser.length; j++) {
                        var objRole = _findOneObjByProperty(roles, "ID", rolesOfUser[j].IDRole);
                        strRoles += objRole.Name + ","
                    }

                    users[i].Roles = strRoles;
                }

                //Save Users
                classTabUsers.saveUsers(users);
            }

            this.saveUserXRoles = function (newUsersXRoles) {
                //Save Data in Initial Configuration
                saveData("UsersXRoles", newUsersXRoles);

                //Reload users to show new items mapped
                classTabUsers.loadDropDownOfUsers("#selUserOfRoles", newUsersXRoles, $("#selUserOfRoles").val());

                //Reload list of Roles to show new item granted
                classUserXRoles.loadRolesOfUser($("#selUserOfRoles").val());
            };

            //Load users in "Mapping Roles to Users"
            classTabUsers.loadDropDownOfUsers("#selUserOfRoles", usersXRoles);

            //Load Roles Availables
            this.loadRoles();
        }
        this.objUserXRoles = new ClassUserXRoles();

        // Public functions (shared across instances)
        this.generateInsert = function () {
            var sqlInserts = "";

            if (users && users.length > 0) {
                var tblUser = _getDBObjectFullName("User");

                sqlInserts =
                "INSERT [dbo].[" + tblUser + "] ([SOEID], [GEID], [Email], [Name], [NumberOfSessions], [LastSessionDate], [Roles], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [IsDeleted]) \n" +
                "VALUES \n";

                //Add Users
                var cantUsers = users.length;
                $(users).each(function (i) {
                    //Last iteration
                    if (cantUsers == (i + 1)) {
                        sqlInserts += "  ( '" + this.SOEID + "', '" + this.GEID + "', '" + this.Email + "', '" + this.Name + "', " + this.NumberOfSessions + ", '" + this.LastSessionDate + "', '" + this.Roles + "', '" + this.CreatedBy + "', '" + this.CreatedDate + "', '" + this.ModifiedBy + "', '" + this.ModifiedDate + "', " + (this.IsDeleted == "False" ? '0' : '1') + ") \n";
                    } else {
                        sqlInserts += "  ( '" + this.SOEID + "', '" + this.GEID + "', '" + this.Email + "', '" + this.Name + "', " + this.NumberOfSessions + ", '" + this.LastSessionDate + "', '" + this.Roles + "', '" + this.CreatedBy + "', '" + this.CreatedDate + "', '" + this.ModifiedBy + "', '" + this.ModifiedDate + "', " + (this.IsDeleted == "False" ? '0' : '1') + "), \n";
                    }
                });
                sqlInserts +=
                "GO";
            }

            return sqlInserts;
        };

        this.loadTableUsers = function () {
            $.jqxGridApi.create({
                showTo: "#tblUser",
                options: {
                    //for comments or descriptions
                    height: "500",
                    autoheight: true,
                    autorowheight: false,
                    showfilterrow: true,
                    sortable: true,
                    editable: true,
                    selectionmode: "singlerow"
                },
                source: {
                    // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                    dataBinding: "Large Data Set Local",
                    rows: users
                },
                groups: [],
                columns: [
                    //type: string - text - number - int - float - date - time 
                    //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                    //cellsformat: ddd, MMM dd, yyyy h:mm tt
                    { name: 'SOEID', text: 'SOEID', width: '100', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center' },
                    { name: 'GEID', text: 'GEID', width: '100', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center' },
                    { name: 'Email', text: 'Email', width: '250', type: 'string', filtertype: 'input' },
                    { name: 'Name', text: 'Name', width: '200', type: 'string', filtertype: 'input' },
                    { name: 'NumberOfSessions', text: 'NumberOfSessions', width: '150', type: 'number', filtertype: 'input', cellsalign: 'center', align: 'center' },
                    { name: 'CreatedBy', text: 'CreatedBy', width: '100', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center' },
                    { name: 'CreatedDate', text: 'CreatedDate', width: '170', type: 'string', filtertype: 'input' },
                    { name: 'ModifiedBy', text: 'ModifiedBy', width: '100', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center' },
                    { name: 'ModifiedDate', text: 'ModifiedDate', width: '170', type: 'string', filtertype: 'input' },
                    { name: 'LastSessionDate', text: 'LastSessionDate', width: '170', type: 'string', filtertype: 'input' },
                    { name: 'Roles', text: 'Roles', width: '300', type: 'string', filtertype: 'input' },
                    { name: 'IsDeleted', text: 'IsDeleted', width: '75', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center' }
                ],
                ready: function () { }
            });
        };

        this.addNewUser = function () {
            var $jqxGrid = $("#tblUser");
            var datarow = {};

            datarow["Name"] = "";
            datarow["EERSFunctionCode"] = "";
            datarow["EERSFunctionDescription"] = "";
            datarow["EERSIgnore"] = "";

            var commit = $jqxGrid.jqxGrid('addrow', null, datarow);
        };

        this.saveUsers = function (users) {
            saveData("Users", users);
        };
    }
    this.objTabUsers = new ClassTabUsers();
}

function reviewDatabase(option, globalOrLocal) {
    console.log(option);

    _loadDBObjects(function (responseListJson) {
        setDBObjectList(responseListJson, globalOrLocal);

        //Update SQL with custom variables
        var dbObjectWithCustomVars = [];
        var responseList = getDBObjectList();

        if (globalOrLocal == "Local") {
            //Update Ids of local data
            objTablInitialConfig.updateAppDataIDs();

            for (var i = 0; i < responseList.length; i++) {
                var tempData = responseList[i];
                var source = {
                    prefix: tempData.prefix,
                    objectName: tempData.objectName,
                    objectType: tempData.objectType
                };

                tempData.createQuery = replaceQueryWithCustomVars(source, tempData.createQuery);
                tempData.deleteQuery = replaceQueryWithCustomVars(source, tempData.deleteQuery);
                tempData.existsQuery = replaceQueryWithCustomVars(source, tempData.existsQuery);

                dbObjectWithCustomVars.push(tempData);
            }

            setDBObjectList(dbObjectWithCustomVars);
        }

        //Show tab DB Structure
        $("#tab-label-db-structure").show();
        $("#tab-pane-db-structure").show();

        //Generete html with tabs and forms by each DB Object
        loadTabsFormsByItem();

        //Review if exists DB Objects
        checkIfExistsObjects(useDefaultPrefix = true);

        //Review if DB Objects changed
        //checkIfChangeObjects(useDefaultPrefix = true);

        //Go to the tab with DB Structure
        $("#tab-label-db-structure a").click();

        //Click first tab to load codemirror
        $("#tab-labels").find('li.active a').click();
    });

    //_callServer({
    //    url: '/Configuration/DataBase/GetJsonStructure',
    //    data: { pType: option },
    //    success: function () { }
    //});
}

function updateSQLExistObject(useDefaultPrefixInWebConfig) {
    var databaseName = $.trim($("#lblDBName").text());
    var DBObjectList = getDBObjectList();
    _SQLExistsObjects = "SELECT \n";

    //Update exists Query with prefix from Web Config
    if (useDefaultPrefixInWebConfig) {
        var tablesPrefix = $("#txtTablesPrefix").val();
        var proceduresPrefix = $("#txtProceduresPrefix").val();
        var functionsPrefix = $("#txtFunctionsPrefix").val();
        
        //Set default value from Web Config to prefix
        for (var i = 0; i < DBObjectList.length; i++) {
            var objBDObject = DBObjectList[i];
            var tempExists = objBDObject.existsQuery;

            //Only object not saved
            if (objBDObject.isSaved == "0") {
                switch (objBDObject.objectType) {
                    case "Table":
                        //Update Prefix 
                        objBDObject.prefix = tablesPrefix;
                        break;

                    case "Procedure":
                        //Update Prefix 
                        objBDObject.prefix = proceduresPrefix;
                        break;

                    case "Function":
                        //Update Prefix 
                        objBDObject.prefix = functionsPrefix;
                        break;

                    case "Profile":
                        break;
                }
            }

            //Replace SQL Query variables: :P_DATABASE, :P_OBJECT_NAME, :P_(ObjectKey), :P_APPLICATION_NAME, :P_SHORT_APP_NAME
            tempExists = replaceQueryWithCustomVars(objBDObject, tempExists);

            //Concat to the Select of exists objects
            _SQLExistsObjects += "(" + tempExists + ")  AS [" + objBDObject.key + "], \n"
        }
    } else {
        //Update exists Query with custom prefix in the form
        var tabPanels = $("#tab-content-bd .tab-pane");

        for (var i = 0; i < tabPanels.length; i++) {
            var $panel = $(tabPanels[i]);
            var tempExists = $panel.find('.txtExistsQuery').val();

            //Replace SQL Query variables: :P_DATABASE, :P_OBJECT_NAME, :P_(ObjectKey), :P_APPLICATION_NAME, :P_SHORT_APP_NAME
            tempExists = replaceQueryWithCustomVars($panel, tempExists);

            //Concat to the Select of exists objects
            _SQLExistsObjects += "(" + tempExists + ")  AS [" + $panel.find('.txtKey').val() + "], \n"
        }
    }
    //Remove last chart
    _SQLExistsObjects = _SQLExistsObjects.slice(0, -3);
}

function replaceQueryWithCustomVars(psource, ptemplateQuery) {
    var tablesPrefix = $.trim($("#txtTablesPrefix").text());
    var procPrefix = $.trim($("#txtProceduresPrefix").text());
    var funcPrefix = $.trim($("#txtFunctionsPrefix").text());
    var appName = _getViewVar("AppKey");
    var databaseName = $.trim($("#lblDBName").text());
    var appCSID = $.trim($("#lblAppCSID").text());
    var DBObjectList = getDBObjectList();
    var objectFullName = "";
    var objectPrefix = "";
    var objectType = "";
    var userInfo = _getUserInfo();

    //Set full Object Name
    if (typeof psource['prefix'] != "undefined") {
        //Is [objBDObject] var
        var objBDObject = psource;

        switch (objBDObject.objectType) {
            case "Table":
                objectPrefix = tablesPrefix;
                break;
            case "Function":
                objectPrefix = funcPrefix;
                break;
            case "Procedure":
                objectPrefix = procPrefix;
                break;
        }

        objectFullName = objectPrefix + objBDObject.objectName;
    } else {
        //Is [$panel] var
        var $panel = psource;
        objectType = $panel.find('[name^="iCheck"]:checked').attr('objecttype');

        switch (objectType) {
            case "Table":
                objectPrefix = tablesPrefix;
                break;
            case "Function":
                objectPrefix = funcPrefix;
                break;
            case "Procedure":
                objectPrefix = procPrefix;
                break;
        }

        objectFullName = objectPrefix + $panel.find('.txtName').val();
    }
    
    //Replace :P_DATABASE
    ptemplateQuery = _replaceAll(":P_DATABASE", databaseName, ptemplateQuery);

    //Replace :P_OBJECT_NAME
    ptemplateQuery = _replaceAll(":P_OBJECT_NAME", objectFullName, ptemplateQuery);

    //Replace :P_TABLES_PREFIX
    ptemplateQuery = _replaceAll(":P_TABLES_PREFIX", tablesPrefix, ptemplateQuery);

    //Replace :P_PROC_PREFIX
    ptemplateQuery = _replaceAll(":P_PROC_PREFIX", procPrefix, ptemplateQuery);

    //Replace :P_FUNC_PREFIX
    ptemplateQuery = _replaceAll(":P_FUNC_PREFIX", funcPrefix, ptemplateQuery);

    //Replace :P_(ObjectKey)
    for (var j = 0; j < DBObjectList.length; j++) {
        if (ptemplateQuery.indexOf(":P_" + DBObjectList[j].key) !== -1) {
            ptemplateQuery = _replaceAll(":P_" + DBObjectList[j].key, DBObjectList[j].prefix + DBObjectList[j].objectName, ptemplateQuery);
        }
    }

    //Replace :P_CSID
    ptemplateQuery = _replaceAll(":P_CSID", appCSID, ptemplateQuery);

    //Replace :P_APPLICATION_NAME
    ptemplateQuery = _replaceAll(":P_APPLICATION_NAME", appName, ptemplateQuery);

    //Replace :P_SHORT_APP_NAME
    var shortAppName = appName.toUpperCase();
    shortAppName = _replaceAll(" ", "_", shortAppName);
    ptemplateQuery = _replaceAll(":P_SHORT_APP_NAME", shortAppName, ptemplateQuery);

    //Replace :P_DATA_INSERT_PERMITS
    ptemplateQuery = _replaceAll(":P_DATA_INSERT_PERMITS", objTablInitialConfig.objTabPermits.generateInsert(), ptemplateQuery);

    //Replace :P_DATA_INSERT_REPORTS
    ptemplateQuery = _replaceAll(":P_DATA_INSERT_REPORTS", objTablInitialConfig.objTabReports.generateInsert(), ptemplateQuery);

    //Replace :P_DATA_INSERT_ROLES
    ptemplateQuery = _replaceAll(":P_DATA_INSERT_ROLES", objTablInitialConfig.objTabRoles.objListRoles.generateInsert(), ptemplateQuery);

    //Replace :P_DATA_INSERT_ROLEXPERMITS
    ptemplateQuery = _replaceAll(":P_DATA_INSERT_ROLEXPERMITS", objTablInitialConfig.objTabRoles.objRoleXPermits.generateInsert(), ptemplateQuery);

    //Replace :P_DATA_INSERT_ROLEXREPORTS
    ptemplateQuery = _replaceAll(":P_DATA_INSERT_ROLEXREPORTS", objTablInitialConfig.objTabRoles.objRoleXReports.generateInsert(), ptemplateQuery);

    //Replace :P_DATA_INSERT_USERS
    ptemplateQuery = _replaceAll(":P_DATA_INSERT_USERS", objTablInitialConfig.objTabUsers.generateInsert(), ptemplateQuery);

    //Replace :P_DATA_INSERT_USERXROLES
    ptemplateQuery = _replaceAll(":P_DATA_INSERT_USERXROLES", objTablInitialConfig.objTabUsers.objUserXRoles.generateInsert(), ptemplateQuery);

    //Replace :P_USER_NAME
    ptemplateQuery = _replaceAll(":P_USER_NAME", userInfo.Name, ptemplateQuery);

    //Replace :P_USER_EMAIL
    ptemplateQuery = _replaceAll(":P_USER_EMAIL", userInfo.Email, ptemplateQuery);

    //Replace :P_USER_SOEID
    ptemplateQuery = _replaceAll(":P_USER_SOEID", userInfo.SOEID, ptemplateQuery);

    //Replace :P_CURRENT_DATE
    ptemplateQuery = _replaceAll(":P_CURRENT_DATE", moment().format('MMMM Do YYYY, hh:mm:ss A'), ptemplateQuery);

    return ptemplateQuery;
}

function checkIfExistsObjects(useDefaultPrefix, fnSuccess) {
    //Update global var (_SQLExistsObjects) 
    updateSQLExistObject(useDefaultPrefix);

    //Get information if exists object
    _callServer({
        url: '/Shared/ExecQuery',
        data: { 'pjsonSql': JSON.stringify(_SQLExistsObjects) },
        type: "post",
        success: function (resultList) {
            if (resultList.length > 0) {
                var DBObjectsInfo = resultList[0];

                for (var objDBKey in DBObjectsInfo) {
                    if (DBObjectsInfo[objDBKey] == "0") {
                        $("[href='#tab-" + objDBKey + "'] i").css("color", "red");
                        $("#tab-" + objDBKey).attr("existsObject", 0);
                    } else {
                        $("[href='#tab-" + objDBKey + "'] i").css("color", "green");
                        $("#tab-" + objDBKey).attr("existsObject", 1);
                    }
                }

                if (fnSuccess) {
                    fnSuccess(resultList);
                }
            }
        }
    });
}

function loadTabsFormsByItem() {
    var htmlLi = "";
    var htmlContent = "";
    var DBObjectList = getDBObjectList();

    for (var i = 0; i < DBObjectList.length; i++) {
        var iconTab = "";
        switch (DBObjectList[i].objectType) {
            case "Table":
                iconTab = "fa-table";
                break;

            case "Procedure":
                iconTab = "fa-gavel";
                break;

            case "Function":
                iconTab = "fa-wrench";
                break;

            case "Profile":
                iconTab = "fa-envelope-o";
                break;
        }

        htmlLi +=
            '<li ' + ((i == 0) ? "class='active'" : "") + '> ' +
            '    <a href="#tab-' + DBObjectList[i].objectName + '" class="tab-open-content" data-toggle="tab"> ' +
            '        <i class="fa ' + iconTab + '"></i>' + _cropWord(DBObjectList[i].objectName, 10) + ' ' +
            '    </a> ' +
            '</li> ';

        htmlContent += getHtmlFormDBStructure(DBObjectList[i], (i == 0));
    }

    var $tabLabels = $(htmlLi);
    var $tabContent = $(htmlContent);

    //Clean old tabs
    $("#tab-labels").contents().remove();

    //Clean old contents
    $("#tab-content-bd").contents().remove();

    //Add tabs
    $("#tab-labels").append($tabLabels);
    $("#tab-content-bd").append($tabContent);

    //On click tab
    $("a[data-toggle='tab']").click(function () {
        if ($($(this).attr("href")).find(".btnRunCreate").length == 1) {
            var $btnCreate = $($(this).attr("href")).find(".btnRunCreate");
            var boundEvents = $._data($btnCreate[0], "events");

            if (!(boundEvents)) {
                //Init icheck
                _GLOBAL_SETTINGS.iCheck();

                //Init tooltips
                _GLOBAL_SETTINGS.tooltipsPopovers();

                //On Run Create
                $($(this).attr("href")).find(".btnRunCreate").click(function () {
                    createDBObject($(this).parent().parent(), function () {
                        checkDBIsConfigured();
                    });
                });

                //On Run Delete
                $($(this).attr("href")).find(".btnRunDelete").click(function () {
                    deleteDBObject($(this).parent().parent(), function () {
                        checkDBIsConfigured();
                    });
                });

                _execOnObjectShows("#tab-pane-db-structure.tab-pane.active .txtCreateQuery", function () {

                    //Init Codemirrow
                    _GLOBAL_SETTINGS.initCodeMirror();

                    //Init Perfect Scrollbar
                    _GLOBAL_SETTINGS.initPerfectScrollbar();
                });
            }
        }
    });
}

function getHtmlFormDBStructure(objDBStructure, isActive) {
    var htmlForm = 
    '<div class="tab-pane plugin-perfect-scrollbar fade ps-container ' + ((isActive) ? "ps-active-y active in" : "") + '" id="tab-' + objDBStructure.objectName + '">' +
    '  <!--Is Saved?-->' +
    '  <input type="hidden" class="form-control txtIsSaved" value="' + objDBStructure.isSaved + '" name="txtIsSaved" aria-required="true" aria-invalid="false"> ' +
    '' +
    '  <!--Create #--> ' +
    '  <input type="hidden" class="form-control txtOrderNumber" value="' + objDBStructure.numOrder + '" name="txtOrderNumber" aria-required="true" aria-invalid="false"> ' +
    '' +
    '  <!--Delete #--> ' +
    '  <input type="hidden" class="form-control txtDelNumOrder" value="' + objDBStructure.delNumOrder + '" name="txtDelNumOrder" aria-required="true" aria-invalid="false"> ' +
    '' +
    '  <!--Lock Delete--> ' +
    '  <input type="hidden" class="form-control txtLockDelete" value="' + objDBStructure.lockDelete + '" name="txtLockDelete" aria-required="true" aria-invalid="false"> ' +
    '' +
    '  <!--Key-->' +
    '  <input type="hidden" class="form-control txtKey" value="' + objDBStructure.key + '" name="txtKey" aria-required="true" aria-invalid="false"> ' +
    '' +
    '  <!--Delete and create buttons--> ' +
    '  <div class="col-xs-12"> ' +
    '      <button type="button" class="pull-right btnRunCreate btn btn-info bottom15 right15" ><i class="fa fa-asterisk"></i> Run Create</button>' +
    '      <button type="button" class="pull-right btnRunDelete btn btn-danger bottom15 right15" ><i class="fa fa-asterisk"></i> Run Delete</button>' +
    '  </div> ' +
    '' +
    '  <!--Prefix for tables--> ' +
    '  <div class="form-group has-success col-xs-6"> ' +
    '      <label class="form-label">Prefix</label> ' +
    '      <span class="desc"></span> ' +
    '      <div class="controls"> ' +
    '          <input type="text" class="form-control txtPrefix" value="' + objDBStructure.prefix + '" name="txtPrefix" aria-required="true" aria-invalid="false"> ' +
    '          <span class="error"> ' +
    '              <label class="txtPrefix-error error" for="txtPrefix"></label> ' +
    '          </span> ' +
    '      </div> ' +
    '  </div> ' +
    '' +
    '  <!--Object Name-->' +
    '  <div class="form-group has-success col-xs-6"> ' +
    '      <label class="form-label">Name</label> ' +
    '      <span class="desc"></span> ' +
    '      <div class="controls"> ' +
    '          <input type="text" class="form-control txtName" value="' + objDBStructure.objectName + '" name="txtName" aria-required="true" aria-invalid="false"> ' +
    '          <span class="error"> ' +
    '              <label class="txtName-error error" for="txtName"></label> ' +
    '          </span> ' +
    '      </div> ' +
    '  </div> ' +
    '' +
    '  <!--Create Query--> ' +
    '  <div class="form-group has-success col-xs-12"> ' +
    '      <label class="form-label">Create Query</label> ' +
    '      <span class="desc">Sql to excute to create the object in the DB.</span> ' +
    '      <div class="controls"> ' +
    '          <textarea class="txtCreateQuery plugin-codemirror" >' + objDBStructure.createQuery + '</textarea> ' +
    '          <span class="error"> ' +
    '              <label class="txtCreateQuery-error error" for="txtCreateQuery"></label> ' +
    '          </span> ' +
    '      </div> ' +
    '  </div> ' +
    '' +
    '  <!--Delete Query--> ' +
    '  <div class="form-group has-success col-xs-12"> ' +
    '      <label class="form-label">Delete Query</label> ' +
    '      <span class="desc">Sql to excute to delete object in the DB.</span> ' +
    '      <div class="controls"> ' +
    '          <textarea class="txtDeleteQuery plugin-codemirror">' + objDBStructure.deleteQuery + '</textarea> ' +
    '          <span class="error"> ' +
    '              <label class="txtDeleteQuery-error error" for="txtDeleteQuery"></label> ' +
    '          </span> ' +
    '      </div> ' +
    '  </div> ' +
    '' +
    '  <!--Exists Query--> ' +
    '  <div class="form-group has-success col-xs-12"> ' +
    '      <label class="form-label">Exists Query</label> ' +
    '      <span class="desc">Sql to excute to validate if exists the object in the DB.</span> ' +
    '      <div class="controls"> ' +
    '          <textarea class="txtExistsQuery plugin-codemirror">' + objDBStructure.existsQuery + '</textarea> ' +
    '          <span class="error"> ' +
    '              <label class="txtExistsQuery-error error" for="txtExistsQuery"></label> ' +
    '          </span> ' +
    '      </div> ' +
    '  </div> ' +
    '' +
    '  <!--Tables for Roles, Permits and ERRS--> ' +
    '  <div class="form-group col-xs-12"> ' +
    '      <label class="form-label" for="field-7">Type</label> ' +
    '      <span class="desc"></span> ' +
    '      <div class="controls"> ' +
    '          <input id="rbtBDTypeTable" objecttype="Table" type="radio" name="iCheck-' + objDBStructure.numOrder + '-' + objDBStructure.objectKey + '" class="skin-square-green" ' + ((objDBStructure.objectType == "Table") ? "checked" : "") + '/> ' +
    '          <label class="iradio-label form-label hover" for="rbtBDTypeTable" style="margin-right: 25px;">Table </label> ' +
    '' +
    '          <input id="rbtBDTypeProc" objecttype="Procedure" type="radio" name="iCheck-' + objDBStructure.numOrder + '-' + objDBStructure.objectKey + '" class="skin-square-green" ' + ((objDBStructure.objectType == "Procedure") ? "checked" : "") + '> ' +
    '          <label class="iradio-label form-label hover" for="rbtBDTypeProc" style="margin-right: 25px;">Procedure </label> ' +
    ' ' +
    '          <input id="rbtBDTypeFunc" objecttype="Function" type="radio" name="iCheck-' + objDBStructure.numOrder + '-' + objDBStructure.objectKey + '" class="skin-square-green" ' + ((objDBStructure.objectType == "Function") ? "checked" : "") + '> ' +
    '          <label class="iradio-label form-label hover" for="rbtBDTypeFunc" style="margin-right: 25px;">Function </label> ' +
    ' ' +
    '          <input id="rbtBDTypeProfile" objecttype="Profile" type="radio" name="iCheck-' + objDBStructure.numOrder + '-' + objDBStructure.objectKey + '" class="skin-square-green" ' + ((objDBStructure.objectType == "Profile") ? "checked" : "") + '> ' +
    '          <label class="iradio-label form-label hover" for="rbtBDTypeProfile" style="margin-right: 25px;">Profile </label> ' +
    '      </div> ' +
    '  </div> ' +
    '</div> ';

    return htmlForm;
}                                                                                                                                                          

function createDBObject($panel, fnSuccess) {
    var appName = $.trim($("#lblAppName").text());
    var databaseName = $.trim($("#lblDBName").text());
    var DBObjectFullName = $panel.find('.txtPrefix').val() + $panel.find('.txtName').val();

    //Create the object only if not exist
    if ($panel.attr("existsObject") == "0") {
        var tempCreateSQL = $panel.find('.txtCreateQuery').val();

        //Replace SQL Query variables: :P_DATABASE, :P_OBJECT_NAME, :P_(ObjectKey), :P_APPLICATION_NAME, :P_SHORT_APP_NAME
        tempCreateSQL = replaceQueryWithCustomVars($panel, tempCreateSQL);

        if (tempCreateSQL) {
            //Execute query 
            _callServer({
                url: '/Shared/ExecQuery',
                data: { 'pjsonSql': JSON.stringify(tempCreateSQL) },
                type: "post",
                loadingMsg: "Creating DB Object " + $panel.find('.txtPrefix').val() + $panel.find('.txtName').val() + "...",
                success: function (resultList) {
                    //Check new objects
                    checkIfExistsObjects(useDefaultPrefix = false, function () {
                        if (fnSuccess) {
                            fnSuccess(resultList);
                        }
                    });

                    _showNotification("success", "DB Object '" + DBObjectFullName + "' was created successfully.");
                }
            });
        } else {
            _showNotification("info", "Create Query is empty for '" + DBObjectFullName + "'.");
        }
    } else {
        _showNotification("info", "DB Object '" + DBObjectFullName + "' already exists.");
    }
}

function createDBObjectsRecursive(indexPanel) {
    //Recursive create function
    var tabPanels = $("#tab-content-bd .tab-pane")

    //Validate if the index exists
    if (indexPanel < tabPanels.length) {
        var $panel = $(tabPanels[indexPanel]);

        //Create the object only if not exist
        if ($panel.attr("existsObject") == "0") {
            var tempCreateSQL = $panel.find('.txtCreateQuery').val();
            if (tempCreateSQL) {

                createDBObject($panel, function (resultList) {
                    //Continue with the next object
                    createDBObjectsRecursive(indexPanel + 1);
                });

            } else {
                //If the query is empty continue with the next object
                createDBObjectsRecursive(indexPanel + 1);
            }
        } else {
            //If exists continue with the next object
            createDBObjectsRecursive(indexPanel + 1);
        }
    } else {
        _showNotification("success", "Create queries were executed successfully.");

        checkDBIsConfigured();
    }
}

function deleteDBObject($panel, fnSuccess) {
    var appName = $.trim($("#lblAppName").text());
    var databaseName = $.trim($("#lblDBName").text());
    var DBObjectFullName = $panel.find('.txtPrefix').val() + $panel.find('.txtName').val();

    //Delete the object only if exists
    if ($panel.attr("existsObject") == "1") {
        var tempDeleteSQL = $panel.find('.txtDeleteQuery').val();

        //Replace SQL Query variables: :P_DATABASE, :P_OBJECT_NAME, :P_(ObjectKey), :P_APPLICATION_NAME, :P_SHORT_APP_NAME
        tempDeleteSQL = replaceQueryWithCustomVars($panel, tempDeleteSQL);

        if (tempDeleteSQL) {
            //Execute delete query 
            _callServer({
                url: '/Shared/ExecQuery',
                data: { 'pjsonSql': JSON.stringify(tempDeleteSQL) },
                type: "post",
                loadingMsg: "Deleting DB Object " + DBObjectFullName + "...",
                success: function (resultList) {
                    //Check deleted objects
                    checkIfExistsObjects(useDefaultPrefix = false, function () {
                        if (fnSuccess) {
                            fnSuccess(resultList);
                        }
                    });

                    _showNotification("success", "DB Object '" + DBObjectFullName + "' was deleted successfully.");
                }
            });
        } else {
            _showNotification("info", "DB Object '" + DBObjectFullName + "' delete query is empty.");
        }
    } else {
        _showNotification("info", "DB Object '" + DBObjectFullName + "' cannot be deleted because not exists.");
    }
}

function deleteDBObjectsRecursive(indexPanel) {
    //Recursive delete function
    var tabPanels = $("#tab-content-bd .tab-pane");

    //Order tab panels by delete orden number
    tabPanels.sort(compareByDelNumOrder);

    //Validate if the index exists
    if (indexPanel < tabPanels.length) {
        var $panel = $(tabPanels[indexPanel]);

        //delete the object only if exists
        if ($panel.attr("existsObject") == "1") {
            var tempDeleteSQL = $panel.find('.txtDeleteQuery').val();
            if (tempDeleteSQL) {

                //Execute delete query 
                deleteDBObject($panel, function (resultList) {
                    //Continue with the next object
                    deleteDBObjectsRecursive(indexPanel + 1);
                });

            } else {
                //If the query is empty continue with the next object
                deleteDBObjectsRecursive(indexPanel + 1);
            }
        } else {
            //If exists continue with the next object
            deleteDBObjectsRecursive(indexPanel + 1);
        }
    } else {
        _showNotification("success", "Delete queries were executed successfully.");

        checkDBIsConfigured();
    }
}

function saveBDStructure() {
    var tabPanels = $("#tab-content-bd .tab-pane");
    var rowsToSave = [];

    for (var i = 0; i < tabPanels.length; i++) {
        var $panel = $(tabPanels[i]);

        //Set saved value
        //$panel.find('.txtIsSaved').val(1),
        rowsToSave.push({
            //isSaved: $panel.find('.txtIsSaved').val(),
            isSaved: "1",
            numOrder: $panel.find('.txtOrderNumber').val(),
            delNumOrder: $panel.find('.txtDelNumOrder').val(),
            lockDelete: $panel.find('.txtLockDelete').val(),
            key: $panel.find('.txtKey').val(),
            //prefix: $panel.find('.txtPrefix').val(),
            prefix: ":P_PROC_PREFIX",
            objectName: $panel.find('.txtName').val(),
            objectType: $panel.find('[name^="iCheck"]:checked').attr('objecttype'),
            createQuery: $panel.find('.txtCreateQuery').val(),
            deleteQuery: $panel.find('.txtDeleteQuery').val(),
            existsQuery: $panel.find('.txtExistsQuery').val()
        });
    }

    _callServer({
        url: '/Configuration/DataBase/SaveJsonStructure',
        data: { 'pjson': _toJSON(rowsToSave), pType: $('[name^="typeBDConfig"]:checked').val() },
        type: "post",
        success: function (savingStatus) {
            //Refresh _DBOBJECTS
            _DBOBJECTS = [];
            _loadDBObjects(function () {
                _showNotification("success", "DB Global Structure was saved successfully.");
                checkDBIsConfigured();
            });
        }
    });
}

function checkDBIsConfigured() {
    var DBIsConfigured = "0";

    //Validate if DBIsConfigured
    if ($("#tab-Exceptions").attr("existsObject") == "1" &&
        $("#tab-InsertException").attr("existsObject") == "1" &&
        $("#tab-GetPermitsByUser").attr("existsObject") == "1" &&
        $("#tab-UserInfo").attr("existsObject") == "1") {

        DBIsConfigured = "1";
    }

    //if all ok add role SUPER_ADMIN
    if (DBIsConfigured == "1") {
        //Validate if current user have SUPER_ADMIN Role
        var userInfo = _getViewVar("UserInfo");
        if (userInfo.Roles.indexOf("SUPER_ADMIN") === -1) {
            _getCurrentAppInfo(function (appInfo) {
                var keyNameUpper = _replaceAll(" ", "_", appInfo.KeyName.toUpperCase());
                var shortNameUpper = _replaceAll(" ", "_", appInfo.ShortName.toUpperCase());
                var superAdminRole = _findOneObjByProperty(_APPDATA.Roles, "Name", keyNameUpper + "_SUPER_ADMIN");

                if (!superAdminRole) {
                    superAdminRole = _findOneObjByProperty(_APPDATA.Roles, "Name", shortNameUpper + "_SUPER_ADMIN");
                }

                if (superAdminRole) {
                    _callProcedure({
                        response: false,
                        loadingMsg: "Add SUPER_ADMIN Role to the current user '" + _getSOEID() + "'.",
                        name: "[dbo].[" + getDBObjectFullName("AdminRolesByUser") + "]",
                        params: [
                            { Name: "@Action", Value: "Edit" },
                            { Name: "@UserSOEID", Value: _getSOEID() },
                            { Name: "@IDRole", Value: superAdminRole.ID }, //ID 4 = SUPER_ADMIN
                            { Name: "@SessionSOEID", Value: _getSOEID() }
                        ],
                        success: {
                            fn: function (responseResult) {
                                var userInfo = _getViewVar("UserInfo");
                                userInfo.Roles = "SUPER_ADMIN";
                                _setViewVar("UserInfo", userInfo);

                                _showNotification("success", "Role SUPER_ADMIN was granted successfully to '" + _getSOEID() + "'.");

                                //Save DBIsConfigured value
                                saveDBIsConfigured(DBIsConfigured);
                            }
                        }
                    });
                } else {
                    _showNotification("error", "Cannot find Super Admin Role");
                }
            });
        }
    } else {
        //If DB not configured set TEMP_ROLE 
        var userInfo = _getViewVar("UserInfo");
        userInfo.Roles = "TEMP_ROLE";
        _setViewVar("UserInfo", userInfo);

        //Save DBIsConfigured value
        saveDBIsConfigured(DBIsConfigured);
    }

}

function saveDBIsConfigured(DBIsConfigured) {
    //Save DBIsConfigured value
    _callServer({
        url: '/Configuration/DataBase/UpdateDBIsConfigured',
        data: { 'pDBIsConfigured': DBIsConfigured },
        type: "post",
        success: function (savingStatus) {

            _showNotification("success", "Updated 'DBIsConfigured' global setting value to  '" + DBIsConfigured + "'.");

        }
    });
}

function getDBObjectFullName(pobjKey) {
    var DBObjectList = getDBObjectList();
    var DBObjectFullName = "";

    for (var i = 0; i < DBObjectList.length; i++) {
        var DBObject = DBObjectList[i];
        if (DBObject.key == pobjKey) {
            DBObjectFullName = DBObject.prefix + DBObject.objectName;
            break;
        }
    }

    return DBObjectFullName;
}

function compareByDelNumOrder(a, b) {
    if ($(a).find('.txtDelNumOrder').val() < $(b).find('.txtDelNumOrder').val()) {
        return -1;
    }
    if ($(a).find('.txtDelNumOrder').val() > $(b).find('.txtDelNumOrder').val()) {
        return 1;
    }
    return 0;
}

function setDBObjectList(plist, globalOrLocal) {
    _DBObjectList = JSON.parse(_toJSON(plist));

    if (globalOrLocal == "Local") {
        var tablesPrefix = $.trim($("#txtTablesPrefix").text());
        var procPrefix = $.trim($("#txtProceduresPrefix").text());
        var funcPrefix = $.trim($("#txtFunctionsPrefix").text());

        //Update Prefix for Tables, Procedures and Functions
        for (var i = 0; i < _DBObjectList.length; i++) {
            switch (_DBObjectList[i].objectType) {
                case "Table":
                    _DBObjectList[i].prefix = tablesPrefix;
                    break;
                case "Function":
                    _DBObjectList[i].prefix = funcPrefix;
                    break;
                case "Procedure":
                    _DBObjectList[i].prefix = procPrefix;
                    break;
                default:
                    _DBObjectList[i].prefix = "";
                    break;
            }
        }
    }
}

function getDBObjectList() {
    //var tabPanels = $("#tab-content-bd .tab-pane");
    //var rows = [];

    //if ($("#tab-content-bd .tab-pane").length > 0) {
    //    for (var i = 0; i < tabPanels.length; i++) {
    //        var $panel = $(tabPanels[i]);
    //        rows.push({
    //            isSaved: $panel.find('.txtIsSaved').val(),
    //            numOrder: $panel.find('.txtOrderNumber').val(),
    //            delNumOrder: $panel.find('.txtDelNumOrder').val(),
    //            lockDelete: $panel.find('.txtLockDelete').val(),
    //            key: $panel.find('.txtKey').val(),
    //            prefix: $panel.find('.txtPrefix').val(),
    //            objectName: $panel.find('.txtName').val(),
    //            objectType: $panel.find('[name^="iCheck"]:checked').attr('objecttype'),
    //            createQuery: $panel.find('.txtCreateQuery').val(),
    //            deleteQuery: $panel.find('.txtDeleteQuery').val(),
    //            existsQuery: $panel.find('.txtExistsQuery').val()
    //        });
    //    }
    //} else {
    //    rows = _DBObjectList;
    //}

    //return rows;
    return _DBObjectList;
}