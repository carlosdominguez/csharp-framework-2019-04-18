﻿var idMainJqxGrid = "#tblReports";

$(document).ready(function () {
    _hideMenu();

    //Add new row to idMainJqxGrid
    $(".btnAddNew").click(function () {
        addNewRow();
    });

    //Delete selected row to idMainJqxGrid
    $(".btnDelete").click(function () {
        deleteRow();
    });

    //Save table idMainJqxGrid
    $(".btnSave").click(function () {
        saveTable();
    });

    //Reload Table idMainJqxGrid
    $(".btnReload").click(function () {
        loadTableReports();
    });

    //To get "AdminReport" spName
    _loadDBObjects(function () {
        //Load table on document ready
        loadTableReports();
    });

});

function loadTableReports() {
    var spName = _getDBObjectFullName("AdminReport");
    $.jqxGridApi.create({
        showTo: idMainJqxGrid,
        options: {
            //for comments or descriptions
            height: "450",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: true,
            sortable: true,
            editable: true,
            groupable: true
        },
        sp: {
            Name: "[dbo].[" + spName + "]",
            Params: [
                { Name: "@Action", Value: "List" }
            ]
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        groups: [],
        columns: [
            //type: string - text - number - int - float - date - time 
            //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
            //cellsformat: ddd, MMM dd, yyyy h:mm tt
            { name: 'ID', type: 'number', hidden: true },
            { name: 'Category', text: 'Category', width: '10%', type: 'string', filtertype: 'checkedlist' },
            { name: 'Name', text: 'Name', width: '40%', type: 'string', filtertype: 'input' },
            { name: 'Path', text: 'Path', width: '50%', type: 'string', filtertype: 'input' }
        ],
        ready: function () { }
    });
}

function addNewRow() {
    var $jqxGrid = $(idMainJqxGrid);
    var datarow = {
        ID: 0,
        Category: '',
        Name: '',
        Path: ''
    };
    var commit = $jqxGrid.jqxGrid('addrow', null, datarow);
}

function deleteRow() {
    //Delete 
    var objRowSelected = $.jqxGridApi.getOneSelectedRow(idMainJqxGrid, true);
    if (objRowSelected) {
        var spName = _getDBObjectFullName("AdminReport");
        var htmlContentModal = "";
        htmlContentModal += "<b>Category: </b>" + objRowSelected['Category'] + "<br/>";
        htmlContentModal += "<b>Name: </b>" + objRowSelected['Name'] + "<br/>";
        htmlContentModal += "<b>Path: </b>" + objRowSelected['Path'] + "<br/>";

        _showModal({
            width: '40%',
            modalId: "modalDelRow",
            addCloseButton: true,
            buttons: [{
                name: "Delete",
                class: "btn-danger",
                onClick: function () {
                    if (objRowSelected.ID != 0) {
                        _callProcedure({
                            loadingMsgType: "fullLoading",
                            loadingMsg: "Saving reports...",
                            name: "[dbo].[" + spName + "]",
                            params: [
                                { "Name": "@Action", "Value": "Delete" },
                                { "Name": "@IDReport", "Value": objRowSelected.ID },
                                { "Name": "@Category", "Value": objRowSelected.Category },
                                { "Name": "@Name", "Value": objRowSelected.Name },
                                { "Name": "@Path", "Value": objRowSelected.Path },
                                { "Name": "@SessionSOEID", "Value": _getSOEID() }
                            ],
                            //Show message only for the last 
                            success: {
                                showTo: $(idMainJqxGrid).parent(),
                                msg: "Row was deleted successfully.",
                                fn: function () {
                                    var reloadData = setInterval(function () {
                                        //console.log("Reloading data from #tblReports");
                                        //$.jqxGridApi.localStorageFindById(idMainJqxGrid).fnReload();
                                        //$(idMainJqxGrid).jqxGrid('updateBoundData');

                                        loadTableReports();

                                        //Remove changes
                                        $.jqxGridApi.rowsChangedFindById(idMainJqxGrid).rows = [];

                                        clearInterval(reloadData);
                                    }, 500);
                                }
                            }
                        });
                    } else {
                        $(idMainJqxGrid).jqxGrid('deleterow', objRowSelected.uid);
                    }
                }
            }],
            title: "Are you sure you want to delete this row?",
            contentHtml: htmlContentModal
        });
    }
}

function saveTable() {
    var spName = _getDBObjectFullName("AdminReport");
    var rowsChanged = $.jqxGridApi.rowsChangedFindById(idMainJqxGrid).rows;
    
    //Validate changes
    if (rowsChanged.length > 0) {
        var fnSuccess = {
            showTo: $(idMainJqxGrid).parent(),
            msg: "The data was saved successfully.",
            fn: function () {
                var reloadData = setInterval(function () {
                    //console.log("Reloading data from #tblReports");
                    //$.jqxGridApi.localStorageFindById(idMainJqxGrid).fnReload();
                    //$(idMainJqxGrid).jqxGrid('updateBoundData');
                    
                    loadTableReports();

                    //Remove changes
                    $.jqxGridApi.rowsChangedFindById(idMainJqxGrid).rows = [];

                    clearInterval(reloadData);
                }, 1500);
            }
        };

        for (var i = 0; i < rowsChanged.length; i++) {
            var objRow = rowsChanged[i];
            var action = "Edit";

            if (objRow.ID == 0) {
                var action = "Insert";
            }

            _callProcedure({
                loadingMsgType: "fullLoading",
                loadingMsg: "Saving reports...",
                name: "[dbo].[" + spName + "]",
                params: [
                    { "Name": "@Action", "Value": action },
                    { "Name": "@IDReport", "Value": objRow.ID },
                    { "Name": "@Category", "Value": objRow.Category },
                    { "Name": "@Name", "Value": objRow.Name },
                    { "Name": "@Path", "Value": objRow.Path },
                    { "Name": "@SessionSOEID", "Value": _getSOEID() }
                ],
                //Show message only for the last 
                success: (i == rowsChanged.length - 1) ? fnSuccess : null
            });
        }
    } else {
        _showAlert({
            showTo: $(idMainJqxGrid).parent(),
            type: 'error',
            title: "Message",
            content: "No changes detected."
        });
    }
}