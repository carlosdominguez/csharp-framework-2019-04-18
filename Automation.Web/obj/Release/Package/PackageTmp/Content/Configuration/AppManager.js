﻿"use strict";

$(document).ready(function () {
    
    //On Save Web Config
    $(".btnSave").click(function () {
        saveTableApps();
    });

    //Add new row to the table
    $(".btnAddNewRow").click(function () {
        addNewRow();
    });

    //Delete row in the table
    $(".btnDelete").click(function () {
        deleteRow();
    });

    //On click in reload button
    $(".btnReLoad").click(function () {
        loadTableApps();
    });

    //On click Deploy to IIS
    $("#btnDeployToIIS").click(function (e) {

    });

    //Load table
    loadTableApps();
});

function loadTableApps() {
    //Set empty to refresh array of app info
    _APPSINFOLIST = [];
    
    //Get new information
    _getAppsInfoList(function (appsInfoList) {
        loadTable(appsInfoList);
    });
    
    function loadTable(appsInfoList) {
        $.jqxGridApi.create({
            showTo: "#tblApps",
            options: {
                //for comments or descriptions
                //height: "450",
                autoheight: true,
                autorowheight: false,
                showfilterrow: true,
                sortable: true,
                editable: true,
                //scrollmode: 'deferred',
                //scrollfeedback: function (row) {
                //    console.log(row);
                //    if (row == null) return "";
                //    var table = "<table>";
                //    var columns = ['FullName', 'KeyName'];
                //    for (var i = 0; i < columns.length; i++) {
                //        var field = columns[i];
                //        var cellvalue = row[field];
                //        table += "<tr><td>" + cellvalue + "</td></tr>";
                //    }
                //    table += "</table>";

                //    //When buttons are rendered exec function
                //    _execOnObjectShows(".swichButton", function () {
                //        //Init swicht button
                //        $(".swichButton").each(function () {
                //            var $btn = $(this);
                //            $btn.jqxSwitchButton({ height: 23, width: 81, checked: true, onLabel: 'Yes', offLabel: 'No' });
                //        });
                //    });

                //    return table;
                //}
                selectionmode: "singlerow"
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                dataBinding: "Large Data Set Local",
                rows: appsInfoList
            },
            groups: [],
            columns: [
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                {
                    name: 'IsDefault', text: 'Is Default?', width: '150px', type: 'bool', filtertype: 'boolean', editable: false, filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                        var dataRecord = $("#tblApps").jqxGrid('getrowdata', rowIndex);
                        var htmlResult = "<div isDefault='" + dataRecord.IsDefault + "' rowIndex='" + rowIndex + "' celluniqueid='" + dataRecord.uniqueid + "_" + this.visibleindex + "' style='margin: auto; margin-top: 4px;' class='swichButton'></div>";
                        console.log('Returning html cell (' + htmlResult + ')');
                        return htmlResult;
                    }
                },
                {
                    name: 'StructureOkInProject', text: 'Structure', width: '80px', type: 'bool', filtertype: 'boolean', editable: false, filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                        var dataRecord = $("#tblApps").jqxGrid('getrowdata', rowIndex);
                        var htmlResult = (dataRecord.StructureOkInProject ? '<i style="color:green;width: 80px;text-align: center;font-size: 26px;padding: 5px;" class="fa fa-check-square-o"></i>' : '<i style="color:red;width: 80px;text-align: center;font-size: 26px;padding: 5px;" class="fa fa-minus-square"></i>');
                        return htmlResult;
                    }
                },
                { name: 'CSID', text: 'CSID', width: '100px', type: 'number', filtertype: 'number', cellsalign: 'center', align: 'center' },
                { name: 'KeyName', text: 'Key Name (Folder name)', width: '200px', type: 'string', filtertype: 'checkedlist' },
                { name: 'ShortName', text: 'Short Name (Tool title)', width: '200px', type: 'string', filtertype: 'checkedlist' },
                { name: 'AppOwnerSOEID', text: 'Owner SOEID', width: '100px', type: 'string', filtertype: 'number', cellsalign: 'center', align: 'center' },
                { name: 'AppOwnerName', text: 'Owner Name', width: '250px', type: 'string', filtertype: 'number', cellsalign: 'center', align: 'center' },
                { name: 'AutomationID', text: 'Automation ID', width: '150px', type: 'string', filtertype: 'checkedlist', cellsalign: 'center', align: 'center' },
                { name: 'AutomationName', text: 'Automation Name', width: '200px', type: 'string', filtertype: 'checkedlist' },
                { name: 'FullName', text: 'Full Name', width: '300px', type: 'string', filtertype: 'checkedlist' }
            ],
            ready: function () {
                var $jqxGrid = $("#tblApps");

                $(".swichButton").each(function () {
                    var $btn = $(this);

                    console.log('$(".swichButton").each(function () {');
                    initializeButton($btn);
                });

                $("#tblApps").on('DOMSubtreeModified', function (e) {
                    var $element = $(e.target);

                    if ($element.children().hasClass('swichButton')) {

                        var $btn = $element.children();

                        console.log('$("#tblApps").on(DOMSubtreeModified, function (e) {', $btn.parent().html());
                        initializeButton($btn);
                    }
                });

                function initializeButton($btn) {
                    //Validate if not have mapped events
                    if (!jQuery._data($btn[0], "events")) {
                        //if (!$btn.hasClass("jqx-switchbutton")) {

                        $btn.children().remove();

                        $btn.jqxSwitchButton({
                            height: 23,
                            width: 81,
                            checked: ($btn.attr("isDefault") == "true" ? 1 : 0),
                            onLabel: 'Yes',
                            offLabel: 'No'
                        });

                        
                        $btn.on('checked', function (event) {
                            console.log('button checked', event);

                            //Get new value
                            var rowIndex = $(this).attr("rowIndex");
                            var dataRecord = $jqxGrid.jqxGrid('getrowdata', rowIndex);
                            dataRecord.IsDefault = false;

                            //Set new value
                            $jqxGrid.jqxGrid('updaterow', rowIndex, dataRecord);

                            //Update cell cache on checked
                            //if ($jqxGrid.jqxGrid('_cellscache')[$btn.attr("celluniqueid")]) {
                            //    $jqxGrid.jqxGrid('_cellscache')[$btn.attr("celluniqueid")].element = $btn.parent().html();
                            //}
                        });

                        $btn.on('unchecked', function (event) {
                            console.log('button unchecked', event);

                            //Get new value
                            var rowIndex = $(this).attr("rowIndex");
                            var dataRecord = $jqxGrid.jqxGrid('getrowdata', rowIndex);
                            dataRecord.IsDefault = true;

                            //Set false others rows
                            var rows = $.jqxGridApi.getAllRows("#tblApps");
                            for (var i = 0; i < rows.length; i++) {
                                if (rows[i].uid != rowIndex) {
                                    rows[i].IsDefault = false;
                                    //$jqxGrid.jqxGrid('updaterow', rows[i].uid, rows[i]);
                                } 
                            }

                            //Set row as selected
                            //$jqxGrid.jqxGrid('selectrow', rowIndex);

                            //Set new value
                            $jqxGrid.jqxGrid('updaterow', rowIndex, dataRecord);

                            //if ($jqxGrid.jqxGrid('_cellscache')[$btn.attr("celluniqueid")]) {
                            //    $jqxGrid.jqxGrid('_cellscache')[$btn.attr("celluniqueid")].element = $btn.parent().html();
                            //}
                        });

                        //if ($jqxGrid.jqxGrid('_cellscache')[$btn.attr("celluniqueid")]) {
                        //    $jqxGrid.jqxGrid('_cellscache')[$btn.attr("celluniqueid")].element = $btn.parent().html();
                        //}
                    }else{
                        console.log($btn, 'already have mapped events', jQuery._data($btn[0], "events"));
                    }
                }

                //Update Action Deploy href="/Configuration/Deploy/?appKeyName=SupportDoc"
                $jqxGrid.on('rowclick', function (event) {
                    if (typeof event.args.group == "undefined") {
                        //_showLoadingFullPage();
                        var row = event.args.rowindex;
                        var datarow = $jqxGrid.jqxGrid('getrowdata', row);
                        
                        console.log(datarow);

                        var href = _getViewVar("SubAppPath")
                    }
                });
            }
        });
    }
}

function saveTableApps() {
    var jqxRowsChanged = $.jqxGridApi.rowsChangedFindById("#tblApps").rows;
    //var appKeyDefault = "";

    for (var i = 0; i < jqxRowsChanged.length; i++) {
        saveApp(jqxRowsChanged);

        //if (appInfoToSave.IsDefault) {
        //    appKeyDefault = _replaceAll(" ", "", appInfoToSave.KeyName);
        //}
    }

    function saveApp(jqxRowsChanged) {
        var appInfoToSave = {
            CSID: jqxRowsChanged[i].CSID,
            KeyName: _replaceAll(" ", "", jqxRowsChanged[i].KeyName),
            ShortName: jqxRowsChanged[i].ShortName,
            FullName: jqxRowsChanged[i].FullName,
            AutomationID: jqxRowsChanged[i].AutomationID,
            AutomationName: jqxRowsChanged[i].AutomationName,
            AppOwnerName: jqxRowsChanged[i].AppOwnerName,
            AppOwnerSOEID: jqxRowsChanged[i].AppOwnerSOEID,
            StructureOkInProject: jqxRowsChanged[i].StructureOkInProject,
            IsDefault: jqxRowsChanged[i].IsDefault
        };

        var continueSaving = true;
        if ($(".enableValidation").is(":checked")) {
            continueSaving = validateAppInfo(appInfoToSave);
        }

        if (continueSaving) {
            //Save app info data
            _callServer({
                loadingMsg: "Saving changes for '" + appInfoToSave.KeyName + "'...",
                url: '/Configuration/AppManager/SaveAppInfo',
                data: { 'pjson': _toJSON(appInfoToSave, null, "\t") },
                type: "post",
                success: function (savingStatus) {
                    if (appInfoToSave.IsDefault) {
                        _showNotification("success", "Current Default App '" + appInfoToSave.KeyName + "' please refresh the page (F5) to load the new configuration.");
                    }
                    _showNotification("success", "Data was saved successfully for '" + appInfoToSave.KeyName + "'.");

                    if (savingStatus.msg && savingStatus.msg != "Saved!") {
                        _showAlert({ content: savingStatus.msg });
                    }
                    //Reload Table
                    loadTableApps();
                }
            });
        }
    }

    function validateAppInfo(objApp) {
        var result = true;
        var msg = "";

        console.log(objApp);

        if (!objApp.CSID) {
            msg += '- CSID is missing.<br>';
            result = false;
        }

        if (!objApp.KeyName) {
            msg += '- Key Name (Folder name) is missing.<br>';
            result = false;
        }

        if (!objApp.ShortName) {
            msg += '- Short Name (Tool title) is missing.<br>';
            result = false;
        }

        if (!objApp.AppOwnerSOEID) {
            msg += '- Owner SOEID is missing.<br>';
            result = false;
        }

        if (!objApp.AppOwnerName) {
            msg += '- Owner Name is missing.<br>';
            result = false;
        }

        if (!objApp.AutomationID) {
            msg += '- Automation ID is missing.<br>';
            result = false;
        }

        if (!objApp.AutomationName) {
            msg += '- Automation Name is missing.<br>';
            result = false;
        }

        if (!objApp.FullName) {
            msg += '- Full Name is missing.<br>';
            result = false;
        }

        if (!result) {
            _showAlert({
                type: 'error',
                content: 'Please review the following errors ' + (objApp.ShortName ? objApp.ShortName : '') + ': <br>' + msg
            });
        }

        return result;
    }

    //if (appKeyDefault) {
    //    //Save default app key
    //    _callServer({
    //        loadingMsg: "Setting default app...",
    //        url: '/Configuration/AppManager/SetDefaultApp',
    //        data: { 'pdefaultApp': appKeyDefault },
    //        type: "post",
    //        success: function (savingStatus) {
    //            _showNotification("success", "Data was saved successfully, please refresh the page (F5) to load the default tool.");

    //            if (savingStatus.msg && savingStatus.msg != "Saved!") {
    //                _showAlert({ content: savingStatus.msg });
    //            }
    //        }
    //    });
    //} 

}

function addNewRow() {
    var $jqxGrid = $("#tblApps");
    var objUser = _getUserInfo();
    var datarow = {};

    datarow["KeyName"] = "";
    datarow["ShortName"] = "";
    datarow["AutomationName"] = "";
    datarow["AppOwnerSOEID"] = objUser.SOEID;
    datarow["AppOwnerName"] = objUser.Name;

    var commit = $jqxGrid.jqxGrid('addrow', null, datarow);
}

function deleteRow() {
    //Delete 
    var objRowSelected = $.jqxGridApi.getOneSelectedRow("#tblApps", true);
    if (objRowSelected) {
        var htmlContentModal = "<b>App Name:</b>" + objRowSelected["ShortName"] + "<br/>";

        _showModal({
            width: '35%',
            modalId: "modalDelApp",
            addCloseButton: true,
            buttons: [{
                name: "Delete",
                class: "btn-danger",
                onClick: function () {
                    $("#tblApps").jqxGrid('deleterow', objRowSelected.uid);
                }
            }],
            title: "Are you sure you want to delete this row?",
            contentHtml: htmlContentModal
        });
    }
}

function deployToIIS() {

}