﻿$(document).ready(function () {

    //Init Codemirror
    _GLOBAL_SETTINGS.initCodeMirror();

    //On click in Execute Query
    $("#btnExecuteQuery").click(function () {
        loadTableByQuery();
    });

    //Download Excel
    $("#btnDownloadExcel").click(function () {
        downloadQueryToExcel();
    });
    
});

function loadTableByQuery() {
    var sql = $("#txtExecuteQuery").val();

    //if (sql.toUpperCase().indexOf("TOP 800") !== -1) {
        //Get information if exists object
        _callServer({
            url: '/Shared/ExecQuery',
            data: { 'pjsonSql': JSON.stringify(sql) },
            type: "post",
            success: function (resultList) {
                loadTable(resultList);

                //Get count information
                var sqlArray = $("#txtExecuteQuery").val().split("FROM");
                var sqlCount = "SELECT COUNT(1) AS TotalRows FROM " + sqlArray[1];

                //if (sqlArray[1]) {
                //    $("#lblRowsFound").html("Calculation total rows...");
                //    //Get information if exists object
                //    _callServer({
                //        url: '/Shared/ExecQuery',
                //        data: { 'pjsonSql': JSON.stringify(sqlCount) },
                //        type: "post",
                //        success: function (resultList) {
                //            console.log(resultList);
                //            if (resultList.length > 0) {
                //                $("#lblRowsFound").html(resultList[0].TotalRows + " rows founds.");
                //            } else {
                //                $("#lblRowsFound").html("0 rows founds.");
                //            }
                //        }
                //    });
                //}
            }
        });
    //} else {
    //    _showAlert({
    //        "type": "error",
    //        "content": "Please add 'TOP 800' to your select.",
    //        "animateScrollTop": true
    //    });
    //}

    function loadTable(list) {
        if (list.length > 0) {
            //Generate columns
            var objTemp = list[0];
            var jqxGidColumns = [];
            var objTempColumns = Object.keys(objTemp);

            _DBTableColumns = objTempColumns;
            for (var i = 0; i < objTempColumns.length; i++) {
                jqxGidColumns.push({
                    name: objTempColumns[i],
                    type: "string",
                    filtertype: "input",
                    editable: true
                });
            }

            $.jqxGridApi.create({
                showTo: "#tblQueryManager",
                options: {
                    //for comments or descriptions
                    height: "450",
                    autoheight: false,
                    autorowheight: false,
                    selectionmode: "singlerow",
                    showfilterrow: true,
                    sortable: true,
                    editable: true,
                    groupable: false
                },
                source: {
                    // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                    dataBinding: "Large Data Set Local",
                    rows: list
                },
                groups: [],
                columns: jqxGidColumns,
                ready: function () {
                    _GLOBAL_SETTINGS.tooltipsPopovers();
                }
            });
        } else {
            _showAlert({
                type: 'info',
                title: "Message",
                content: "No data found for table " + _getViewVar("TableName")
            });
        }
    };
};

function downloadQueryToExcel() {
    var sql = $("#txtExecuteQuery").val();

    //Remove 'Top 800'
    sql = _replaceAll('TOP 800', '', sql);

    _downloadExcel({
        sql: sql,
        filename: _createCustomID() + "_QueryManagerReport.xls",
        success: {
            msg: "Generating report... Please Wait, this operation may take some time to complete."
        }
    });
};