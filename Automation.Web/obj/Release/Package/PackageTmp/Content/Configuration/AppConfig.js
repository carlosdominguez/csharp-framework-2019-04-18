﻿$(document).ready(function () {
    
    //Load web config
    loadTableAppConfig();

    //On Save App Config
    $("#btnSaveAppConfig").click(function () {
        saveAppConfig();
    });
});

function loadTableAppConfig() {
    _callServer({
        url: '/Configuration/AppConfig/List',
        success: function (responseList) {
            loadTable(responseList);
        }
    });
    
    function loadTable(webConfigList) {
        $.jqxGridApi.create({
            showTo: "#tblAppConfig",
            options: {
                //for comments or descriptions
                //height: "1000",
                autoheight: true,
                autorowheight: false,
                selectionmode: "singlerow",
                //showfilterrow: true,
                sortable: true,
                editable: true
                //groupable: true
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                dataBinding: "Large Data Set Local",
                rows: webConfigList
            },
            columns: [
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                { name: 'section', text: 'Section', hidden: true, width: '15%', type: 'string', filtertype: 'checkedlist', editable: true },
                { name: 'name', text: 'Name', width: '25%', type: 'string', filtertype: 'checkedlist', editable: true },
                { name: 'value', text: 'Value', width: '75%', type: 'string', filtertype: 'checkedlist', editable: true }

            ],
            ready: function () {
                
            }
        });
    }
    
    //loadTable(_get("listAppConfig"));
};

function saveAppConfig() {
    var jqxRows = $.jqxGridApi.rowsChangedFindById("#tblAppConfig").rows;
    var rowsToSave = [];

    for (var i = 0; i < jqxRows.length; i++) {
        rowsToSave.push({
            section: jqxRows[i].section,
            name: jqxRows[i].name,
            value: jqxRows[i].value
        });
    }

    _callServer({
        url: '/Configuration/AppConfig/Save',
        data: { 'pjson': _toJSON(rowsToSave) },
        type: "post",
        success: function (savingStatus) {
            _showNotification("success", "Data was saved successfully.");
        }
    });
};