﻿$(document).ready(function () {

    loadTableExceptions();
});

function loadTableExceptions() {
    _callServer({
        url: '/Configuration/AdminException/List',
        success: function (responseList) { 
            //_loadTableByArray(responseList, [
            //    { "Name": "ID", "Key": "ID" },
            //    { "Name": "SOE_ID_SESSION", "Key": "SOE_ID_SESSION" },
            //    { "Name": "CREATED_DATE", "Key": "CREATED_DATE" },
            //    { "Name": "METHOD", "Key": "METHOD" },
            //    { "Name": "EXCEPTION", "Key": "EXCEPTION" }
            //], "#tblExceptions");
            loadTable(responseList);
        }
    });

    function loadTable(list) {
        $.jqxGridApi.create({
            showTo: "#tblExceptions",
            options: {
                //for comments or descriptions
                height: "450",
                autoheight: false,
                autorowheight: false,
                selectionmode: "singlerow",
                showfilterrow: true,
                sortable: true,
                editable: true,
                groupable: false
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                dataBinding: "Large Data Set Local",
                rows: list
            },
            groups: [],
            columns: [
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                { name: 'ID', text: 'ID', width: '5%', type: 'float', filtertype: 'checkedlist', editable: true },
                { name: 'SOE_ID_SESSION', text: 'SOE_ID_SESSION', width: '15%', type: 'string', filtertype: 'checkedlist', editable: true },
                { name: 'CREATED_DATE', text: 'CREATED_DATE', width: '20%', type: 'string', filtertype: 'input', editable: true },
                { name: 'METHOD', text: 'METHOD', width: '30%', type: 'string', filtertype: 'input', editable: true },
                //{ name: 'EXCEPTION', text: 'EXCEPTION', width: '30%', type: 'string', filtertype: 'input', editable: true },
                {
                    name: 'EXCEPTION', computedcolumn: true, width: '30%', text: 'EXCEPTION', type: 'html', editable: false, filterable: false, sortable: false, cellsrenderer: function (rowIndex, dataField, value) {
                        var dataRecord = $("#tblExceptions").jqxGrid('getrowdata', rowIndex);
                        var $contentDiv = $('<div></div>');
                        var $exceptionTempDiv = $('<div jqxGridExceptionRowId="' + rowIndex + '"></div>').attr('ex', '<pre>' +dataRecord.EXCEPTION +'</pre>');
                        $exceptionTempDiv.attr("onclick", "_showModal({title: \'Details\', contentHtml: $(\'[jqxGridExceptionRowId=" + rowIndex + "]\').attr(\'ex\') })");
                        $exceptionTempDiv.html(_cropWord(dataRecord.EXCEPTION, 20));
                        $contentDiv.append($exceptionTempDiv);
                        return $contentDiv.html();
                    }

                },

            ],
            ready: function () {
                _GLOBAL_SETTINGS.tooltipsPopovers();
            }
        });

    }
};