﻿/// <reference path="../Shared/plugins/util/global.js" />

$(document).ready(function () {
    loadPartialViewDBTable();

});

function loadPartialViewDBTable() {
    _loadDBObjectsEditTable({
        tableKey: "GlobalVar",
        idElement: "content-tblGlobalVars",
        colKey: "VAR_NAME",
        hasIdentity: "0"
    });
};
