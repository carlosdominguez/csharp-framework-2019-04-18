﻿$(document).ready(function () {
    loadPartialViewDBTable();

    _execOnObjectShows(
        function () {
            if (!$._data($(".sidebar_toggle")[0], "events")) {
                return false;
            } else {
                return true;
            }
        },
        function () {
            if ($(".page-sidebar").hasClass("expandit") || (!$(".page-sidebar").hasClass("expandit") && !$(".page-sidebar").hasClass("collapseit"))) {
                $(".sidebar_toggle").click();
            } 
        }
    );
});

function loadPartialViewDBTable() {
    _loadDBObjectsEditTable("User", "content-tblUser");
    _loadDBObjectsEditTable("UserXRole", "content-tblUserXRole");
};