﻿//@ sourceURL=_DBTable.js

function _DBTableLoad(options) {

    var _DBTableColumns;
    var _TableName = options.TableName;
    var _IDElementContent = options.IDElementContent;
    var _HasIdentity = options.HasIdentity;

    $(document).ready(function () {
        //Init icheck
        _GLOBAL_SETTINGS.iCheck();

        //Save information to json /Content/Sources/database-structure.txt
        $("#" + _IDElementContent + " .btnSaveDBTable").click(function () {
            saveBDTable();
        });

        //Delete selected row
        $("#" + _IDElementContent + " .btnDeleteDBTable").click(function () {
            deleteRowDBTable();
        });

        //Add new row to the table
        $("#" + _IDElementContent + " .btnAddNewRowDBTable").click(function () {
            addNewRowBDTable();
        });

        //Reload Table
        $("#" + _IDElementContent + " .btnLoadDBTable").click(function () {
            loadTable();
        });

        //Set Yes or No for Identity
        if (_HasIdentity == "1") {
            $("[name='iCheck-is-identity-" + _TableName + "'][value='Yes']").iCheck('check');
        } else {
            $("[name='iCheck-is-identity-" + _TableName + "'][value='No']").iCheck('check');
        }

        loadTable();
    });

    function updatePendingChanges() {
        var jqxRows = $.jqxGridApi.rowsChangedFindById("#" + _IDElementContent + " .tblDBTable").rows;
        var jqxDeletedRows = $.jqxGridApi.rowsDeletedFindById("#" + _IDElementContent + " .tblDBTable").rows;
        var cantChanges = (jqxRows.length + jqxDeletedRows.length);
        $("#" + _IDElementContent + " .btnSaveDBTable").html('<i class="fa fa-save"></i> Save (' + cantChanges + ' changes)');
    }

    function loadTable() {
        _TableName = $("#" + _IDElementContent + " .txtTableName").val();
        _callServer({
            loadingMsg: "Loading data for '" + _TableName + "'...",
            url: '/Configuration/AdminDBTable/List',
            data: { "ptable": _TableName },
            success: function (responseList) {
                //Generate columns
                var objTemp = responseList[0];
                var jqxGidColumns = [];
                var objTempColumns = Object.keys(objTemp);

                _DBTableColumns = objTempColumns;
                for (var i = 0; i < objTempColumns.length; i++) {
                    jqxGidColumns.push({
                        name: objTempColumns[i],
                        type: "string",
                        filtertype: "input",
                        editable: true
                    });
                }

                //Load columns information from DB
                if (jqxGidColumns.length == 0) {
                    var sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'" + _TableName + "'";

                    _callServer({
                        loadingMsg: "Loading columns information for " + _TableName + "...",
                        url: '/Shared/ExecQuery',
                        data: { 'pjsonSql': _toJSON(sql) },
                        type: "post",
                        success: function (resultList) {
                            _DBTableColumns = [];

                            for (var i = 0; i < resultList.length; i++) {
                                jqxGidColumns.push({
                                    name: resultList[i]["COLUMN_NAME"],
                                    type: "string",
                                    filtertype: "input",
                                    editable: true
                                });
                                _DBTableColumns.push(resultList[i]["COLUMN_NAME"]);
                            }

                            loadTable(responseList, jqxGidColumns);
                        }
                    });
                } else {
                    loadTable(responseList, jqxGidColumns);
                }
            }
        });

        function loadTable(list, jqxGidColumns) {

            $.jqxGridApi.create({
                showTo: "#" + _IDElementContent + " .tblDBTable",
                options: {
                    //for comments or descriptions
                    height: "450",
                    autoheight: false,
                    autorowheight: false,
                    selectionmode: "singlerow",
                    showfilterrow: true,
                    sortable: true,
                    editable: true,
                    groupable: false
                },
                source: {
                    // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                    dataBinding: "Large Data Set Local",
                    rows: list
                },
                groups: [],
                columns: jqxGidColumns,
                ready: function () {
                    _GLOBAL_SETTINGS.tooltipsPopovers();

                    $("#" + _IDElementContent + " .tblDBTable").jqxGrid('autoresizecolumns');


                }
            });
        };
    }

    function addNewRowBDTable() {
        var $jqxGrid = $("#" + _IDElementContent + " .tblDBTable");
        var datarow = {};

        for (var i = 0; i < _DBTableColumns.length; i++) {
            datarow[_DBTableColumns[i]] = "";
        }
        datarow["IsNew"] = "1";
        var commit = $jqxGrid.jqxGrid('addrow', null, datarow);
    }

    function deleteRowDBTable() {
        //Delete 
        var objRowSelected = $.jqxGridApi.getOneSelectedRow("#" + _IDElementContent + " .tblDBTable", true);
        if (objRowSelected) {
            var htmlContentModal = "";

            for (var i = 0; i < _DBTableColumns.length; i++) {
                htmlContentModal += "<b>" + _DBTableColumns[i] + ":</b>" + objRowSelected[_DBTableColumns[i]] + "<br/>";
            }

            _showModal({
                width: '40%',
                modalId: "modalDelBDTable",
                addCloseButton: true,
                buttons: [{
                    name: "Delete",
                    class: "btn-danger",
                    onClick: function () {
                        $("#" + _IDElementContent + " .tblDBTable").jqxGrid('deleterow', objRowSelected.uid);
                    }
                }],
                title: "Are you sure you want to delete this row?",
                contentHtml: htmlContentModal
            });
        }
    }

    function saveBDTable() {
        var jqxRows = $.jqxGridApi.rowsChangedFindById("#" + _IDElementContent + " .tblDBTable").rows;
        var jqxDeletedRows = $.jqxGridApi.rowsDeletedFindById("#" + _IDElementContent + " .tblDBTable").rows;
        var tableName = $("#" + _IDElementContent + " .txtTableName").val();
        var columnKey = $("#" + _IDElementContent + " .txtColumnKey").val();
        var isIdentity = $("#" + _IDElementContent + " .iCheck-is-identity-content").find('[name^="iCheck"]:checked').val();
        var sqlToDelete = "";
        var sqlToInsert = "";
        var sqlToUpdate = "";

        //Create deleted queries
        for (var i = 0; i < jqxDeletedRows.length; i++) {
            if (jqxDeletedRows[i]) {
                sqlToDelete += "DELETE FROM [" + tableName + "] WHERE [" + columnKey + "] = '" + jqxDeletedRows[i][columnKey] + "' GO \n";
            }
        }

        //Create insert and update rows query
        for (var i = 0; i < jqxRows.length; i++) {
            //Update
            if (jqxRows[i][columnKey] && jqxRows[i]["IsNew"] != "1") {
                sqlToUpdate += "UPDATE " + "[" + tableName + "] SET ";
                for (var j = 0; j < _DBTableColumns.length; j++) {
                    //Remove identity column
                    if (isIdentity == "Yes") {
                        if (_DBTableColumns[j] != columnKey) {
                            sqlToUpdate += "[" + _DBTableColumns[j] + "] = " + ((jqxRows[i][_DBTableColumns[j]]) ? "'" + jqxRows[i][_DBTableColumns[j]] + "'" : "NULL") + ", ";
                        }
                    } else {
                        sqlToUpdate += "[" + _DBTableColumns[j] + "] = " + ((jqxRows[i][_DBTableColumns[j]]) ? "'" + jqxRows[i][_DBTableColumns[j]] + "'" : "NULL") + ", ";
                    }
                }
                //Remove last 2 chars ", "
                sqlToUpdate = sqlToUpdate.slice(0, -2);
                sqlToUpdate += " WHERE [" + columnKey + "] = '" + jqxRows[i][columnKey] + "' GO \n";
            } else {
                //Insert
                sqlToInsert += "INSERT INTO " + "[" + tableName + "] (";
                for (var j = 0; j < _DBTableColumns.length; j++) {

                    //Remove identity column
                    if (isIdentity == "Yes") {
                        if (_DBTableColumns[j] != columnKey) {
                            sqlToInsert += "[" + _DBTableColumns[j] + "], ";
                        }
                    } else {
                        sqlToInsert += "[" + _DBTableColumns[j] + "], ";
                    }
                }
                //Remove last 2 chars ", "
                sqlToInsert = sqlToInsert.slice(0, -2);
                sqlToInsert += ") VALUES (";

                for (var j = 0; j < _DBTableColumns.length; j++) {
                    //Remove identity column
                    if (isIdentity == "Yes") {
                        if (_DBTableColumns[j] != columnKey) {
                            sqlToInsert += "" + ((jqxRows[i][_DBTableColumns[j]]) ? "'" + jqxRows[i][_DBTableColumns[j]] + "'" : "NULL") + ", ";
                        }
                    } else {
                        sqlToInsert += "" + ((jqxRows[i][_DBTableColumns[j]]) ? "'" + jqxRows[i][_DBTableColumns[j]] + "'" : "NULL") + ", ";
                    }
                }
                //Remove last 2 chars ", "
                sqlToInsert = sqlToInsert.slice(0, -2);
                sqlToInsert += ") GO \n";
            }
        }

        var sql = ((sqlToDelete) ? sqlToDelete : "") + ((sqlToUpdate) ? sqlToUpdate : "") + ((sqlToInsert) ? sqlToInsert : "");

        if (sql) {
            //Execute delete query 
            _callServer({
                loadingMsg: "Saving changes in " + tableName + "...",
                url: '/Shared/ExecQuery?pforceSessionRefresh=1',
                data: { 'pjsonSql': _toJSON(sql) },
                type: "post",
                success: function (resultList) {
                    _showNotification("success", "Data was successfully saved.");

                    //Remove changes
                    $.jqxGridApi.rowsChanged = [];
                    $.jqxGridApi.rowsDeleted = [];
                }
            });
        } else {
            _showNotification("info", "There aren't changes detected.");
        }
    }
};