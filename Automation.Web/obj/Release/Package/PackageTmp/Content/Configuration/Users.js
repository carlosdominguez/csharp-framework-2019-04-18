﻿/// <reference path="../Shared/plugins/util/global.js" />

var roles;
var users;
var usersXRoles;

$(document).ready(function () {

    //Load Roles
    _loadRolesFromDB(function (rolesList) {
        roles = rolesList;
    });

    //Load Users
    _loadUsersFromDB(function (usersList) {
        users = usersList;
    });

    _execOnAjaxComplete(function () {
        var objListUsers = new ClassListUsers();
    });
});

//Class for Load Tab Users
//Tab for add, delete and edit User
//--------------------------------------------
function ClassListUsers() {
    var classListUsers = this;

    //On click in btn add new user
    $(".btnAddNewUser").click(function () {
        classListUsers.addNewUser();
    });

    //On click in btn edit user
    $(".btnEditUser").click(function () {
        classListUsers.editUser();
    });

    //On click in btn delete user
    $(".btnDeleteUser").click(function () {
        classListUsers.deleteUser();
    });

    this.createFormUser = function (options) {
        var objUser = options.objUser;
        //console.log(objUser);
        var formHtml =
        '<div class="row"> ' +
        '    <div class="col-md-12"> ' +
        //'        <h3>Select User</h3> ' +
        '        <p>Please select the user to grand access:</p> ' +
        '        <div class="selUserOfRoles"></div> ' +
        '    </div> ' +
        '    <div class="col-md-12"> ' +
        '        <div class="pull-left"> ' +
        //'            <h3>Set Roles</h3> ' +
        '            <p>Please check the roles that you need to map to the selected user:</p> ' +
        '        </div> ' +
        '        <div style="text-align: center; padding-right: 39px; padding-bottom: 8px;" class="pull-right"> ' +
        '            Check all <br /> ' +
        '            <input type="checkbox" class="skin-square-green checkAllUserXRoles"> ' +
        '        </div> ' +
        '        <div style="clear:both;"></div> ' +
        '        <div class="well transparent" id="roleCategories"></div> ' +
        '    </div> ' +
        //'    <div class="col-md-12"> ' +
        //'        <h3>Is Active?</h3> ' +
        //'        <p>User was deleted or not deleted:</p> ' +
        //'        <div class="form-group"> ' +
        //'           <input value="1" type="radio" name="chkFlagIsDeleted" class="rbtFlagIsDeletedYes skin-square-green" ' + ((objUser.IsDeleted) ? "checked" : "") + ' > ' +
        //'           <label class="iradio-label form-label hover" style="margin-right: 25px;">Deleted </label> ' +
        //'           <input value="0" type="radio" name="chkFlagIsDeleted" class="rbtFlagIsDeletedNo skin-square-green" ' + ((objUser.IsDeleted == false) ? "checked" : "") + ' > ' +
        //'           <label class="iradio-label form-label hover" style="margin-right: 25px;">Not Deleted </label> ' +
        //'        </div> ' +
        //'    </div> ' +
        '</div> ';

        var objUserXRoles;

        _showModal({
            width: "75%",
            title: "User Information",
            contentHtml: formHtml,
            buttons: [{
                name: "Save",
                class: "btn-success",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    objUser.SOEID = $modal.find(".selUserOfRoles select").val();

                    if (objUser.SOEID) {
                        //objUser.IsDeleted = ($modal.find('[name="chkFlagIsDeleted"]:checked').val() == '1' ? 'True' : 'False');

                        //Saves roles of user
                        objUserXRoles.saveUserXRoles();

                        //On Complete ajaxs reload list
                        _execOnAjaxComplete(function () {

                            //Save IsDeleted and Create User
                            if (options.onSave) {
                                options.onSave(objUser);
                            }

                            //Close Modal
                            $modal.find("[data-dismiss='modal']").click();
                        });
                    } else {
                        _showNotification("error", "Please select an employee", "ErrorMissingEmp");
                    }
                }
            }],
            addCloseButton: true,
            onReady: function ($modal) {
                //Init Radio buttons
                _GLOBAL_SETTINGS.iCheck();

                //Create select of employees
                var selectSOEIDOptions = {
                    id: ".selUserOfRoles",
                    selSOEID: objUser.SOEID
                };

                //Disable if exist soeid
                if (objUser.SOEID) {
                    selectSOEIDOptions.disabled = true;
                }
                _createSelectSOEID(selectSOEIDOptions);

                //Load roles of user
                if (objUser.SOEID) {
                    _loadRolesOfUserFromDB(function (rolesOfUser) {
                        usersXRoles = rolesOfUser;

                        //Load roles list
                        objUserXRoles = new ClassUserXRoles($modal);
                    }, objUser.SOEID);
                } else {
                    usersXRoles = [];

                    //Load roles list
                    objUserXRoles = new ClassUserXRoles($modal);
                }
            }
        });
    };

    this.loadTableUsers = function () {
        _showLoadingTopMessage("Loading Table of Users...", "TableUsers");
        $.jqxGridApi.create({
            showTo: "#tblUser",
            options: {
                //for comments or descriptions
                height: "500",
                autoheight: false,
                autorowheight: false,
                showfilterrow: true,
                sortable: true,
                editable: true,
                selectionmode: "singlerow"
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                dataBinding: "Large Data Set Local",
                rows: users
            },
            groups: [],
            columns: [
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                //{
                //    name: 'IsDeleted', text: 'Status', width: '80px', type: 'bool', filtertype: 'boolean', editable: false, filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                //        var dataRecord = $("#tblUser").jqxGrid('getrowdata', rowIndex);
                //        var htmlResult = (dataRecord.IsDeleted == false ? '<i style="color:green;width: 80px;text-align: center;font-size: 26px;padding: 5px;" class="fa fa-check-square-o"></i>' : '<i style="color:red;width: 80px;text-align: center;font-size: 26px;padding: 5px;" class="fa fa-minus-square"></i>');
                //        return htmlResult;
                //    }
                //},
                { name: 'SOEID', text: 'SOEID', width: '100', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center' },
                { name: 'GEID', text: 'GEID', width: '100', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center' },
                { name: 'Email', text: 'Email', width: '250', type: 'string', filtertype: 'input' },
                { name: 'Name', text: 'Name', width: '200', type: 'string', filtertype: 'input' },
                { name: 'NumberOfSessions', text: 'NumberOfSessions', width: '150', type: 'number', filtertype: 'input', cellsalign: 'center', align: 'center' },
                { name: 'CreatedBy', text: 'CreatedBy', width: '100', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center' },
                { name: 'CreatedDate', text: 'CreatedDate', width: '185', type: 'date', cellsformat: 'MM/dd/yyyy h:mm tt', filtertype: 'input', cellsalign: 'center', align: 'center' },
                { name: 'ModifiedBy', text: 'ModifiedBy', width: '100', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center' },
                { name: 'ModifiedDate', text: 'ModifiedDate', width: '185', type: 'date', cellsformat: 'MM/dd/yyyy h:mm tt', filtertype: 'input', cellsalign: 'center', align: 'center' },
                { name: 'LastSessionDate', text: 'LastSessionDate', width: '185', type: 'date', cellsformat: 'MM/dd/yyyy h:mm tt', filtertype: 'input', cellsalign: 'center', align: 'center' },
                { name: 'Roles', text: 'Roles', width: '300', type: 'string', filtertype: 'input' }
            ],
            ready: function () { }
        });

        setTimeout(function () {
            _hideLoadingTopMessage("TableUsers");
        }, 600);
    };

    this.deleteUser = function () {
        var objRowSelected = $.jqxGridApi.getOneSelectedRow("#tblUser", true);
        if (objRowSelected) {
            var htmlContentModal = "";
            htmlContentModal += "<b>User: </b>" + objRowSelected['Name'] + "<br/>";

            _showModal({
                width: '40%',
                modalId: "modalDelRow",
                addCloseButton: true,
                buttons: [{
                    name: "Delete",
                    class: "btn-danger",
                    onClick: function () {
                        if (objRowSelected.SOEID) {
                            var spName = _getDBObjectFullName("AdminUser");

                            _callProcedure({
                                response: false,
                                loadingMsgType: "fullLoading",
                                loadingMsg: "Deleting user '" + objRowSelected.Name + "'...",
                                name: "[dbo].[" + spName + "]",
                                params: [
                                    { "Name": "@Action", "Value": "DeleteUser" },
                                    { "Name": "@UserSOEID", "Value": objRowSelected.SOEID },
                                    { "Name": "@IsDeleted", "Value": '1' },
                                    { "Name": "@SessionSOEID", "Value": _getSOEID() }
                                ],
                                //Show message only for the last 
                                success: function () {
                                    _showNotification("success", "User '" + objRowSelected.Name + "' was deleted successfully.");

                                    //Load Users
                                    _loadUsersFromDB(function (usersList) {
                                        users = usersList;

                                        //Refresh Session
                                        _refreshSession();

                                        //Reload table of users
                                        classListUsers.loadTableUsers();
                                    });
                                }
                            });
                        }
                    }
                }],
                title: "Are you sure you want to delete this row?",
                contentHtml: htmlContentModal
            });
        }
    };

    this.addNewUser = function () {
        var objNewUser = {
            SOEID: '',
            IsDeleted: false
        };

        classListUsers.createFormUser({
            objUser: objNewUser,
            onSave: function (objNewUser) {
                var spName = _getDBObjectFullName("AdminUser");

                _callProcedure({
                    response: false,
                    loadingMsgType: "fullLoading",
                    loadingMsg: "Saving new user '" + objNewUser.SOEID + "'...",
                    name: "[dbo].[" + spName + "]",
                    params: [
                        { "Name": "@Action", "Value": "AddUser" },
                        { "Name": "@UserSOEID", "Value": objNewUser.SOEID },
                        { "Name": "@SessionSOEID", "Value": _getSOEID() }
                    ],
                    //Show message only for the last 
                    success: function () {
                        _showNotification("success", "New User '" + objNewUser.SOEID + "' was created successfully.");

                        //Load Users
                        _loadUsersFromDB(function (usersList) {
                            users = usersList;

                            //Refresh Session
                            _refreshSession();

                            //Reload table of users
                            classListUsers.loadTableUsers();
                        });
                    }
                });
            }
        });
    };

    this.editUser = function () {
        var objRowSelected = $.jqxGridApi.getOneSelectedRow("#tblUser", true);
        if (objRowSelected) {
            classListUsers.createFormUser({
                objUser: objRowSelected,
                onSave: function (objUpdatedUser) {
                    //var spName = _getDBObjectFullName("AdminUser");

                    /*callProcedure({
                        response: false,
                        loadingMsgType: "fullLoading",
                        loadingMsg: "Updating user '" + objUpdatedUser.Name + "'...",
                        name: "[dbo].[" + spName + "]",
                        params: [
                            { "Name": "@Action", "Value": "EditUser" },
                            { "Name": "@UserSOEID", "Value": objUpdatedUser.SOEID },
                            { "Name": "@IsDeleted", "Value": (objUpdatedUser.IsDeleted == 'True' ? '1' : '0') },
                            { "Name": "@SessionSOEID", "Value": _getSOEID() }
                        ],
                        //Show message only for the last 
                        success: function () {
                            _showNotification("success", "User '" + objUpdatedUser.Name + "' was modified successfully.");
                        }
                    });*/

                    //Load Users
                    _loadUsersFromDB(function (usersList) {
                        users = usersList;

                        //Refresh Session
                        _refreshSession();

                        //Reload table of users
                        classListUsers.loadTableUsers();

                        //Show message
                        _showNotification("success", "Roles of user were saved successfully.", "UserSaved");
                    });
                }
            });
        }
    };

    //Load Table of Users
    this.loadTableUsers();
}

//Tab for add roles to User
//--------------------------------------------
function ClassUserXRoles($modal) {
    var classUserXRoles = this;

    //On click check all Roles
    $(".checkAllUserXRoles").on("ifChanged", function () {
        var $checkAll = $(this);
        var grantToAll = $checkAll.is(":checked");

        $("[idRole]").each(function (indexEach, item) {
            var $el = $(item);
            var idRole = $el.attr("idRole");
            var isGranted = $el.is(":checked");

            if (isGranted != grantToAll) {
                if (grantToAll) {
                    $el.iCheck('check');
                } else {
                    $el.iCheck('uncheck');
                }
            }
        });
    });

    //On click save roles of user
    $(".btnSaveUserXRoles").click(function () {
        classUserXRoles.saveUserXRoles();
    });

    //On change select of users
    $(".selUserOfRoles").change(function () {
        //Load roles if exists SOEID
        var selSOEID = $modal.find(".selUserOfRoles select").val();
        if (selSOEID) {
            classUserXRoles.loadRolesOfUser(selSOEID);
        }
    });

    this.loadRoles = function () {
        var categoryRoles = Enumerable.From(roles)
                .GroupBy(
                    "$.Category",
                    "{ ID: $.ID, Category: $.Category, Name: $.Name, Path: $.Path, ClassIcon: $.ClassIcon}",
                    "{ Category: $, Roles: $$.ToArray() }"
                )
                .ToArray();

        //Clean old data
        $("#roleCategories").contents().remove();

        for (var i = 0; i < categoryRoles.length; i++) {
            var objCategoryRole = categoryRoles[i];

            var $title = $('<p class="text-success"></p>');
            $title.html('<i class="fa fa-cubes"></i> Role List ');

            var idTable = 'tblRolesBody' + objCategoryRole.Category;
            var $tableOfRoles = $(
                '<table class="table table-hover"> ' +
                '    <tbody id="' + idTable + '"> ' +
                '    </tbody> ' +
                '</table> '
            );

            //$("#roleCategories").append($title);
            $("#roleCategories").before($tableOfRoles);
            $("#roleCategories").hide();

            //Add rows to table
            for (var j = 0; j < objCategoryRole.Roles.length; j++) {
                var objRole = objCategoryRole.Roles[j];

                var $rowRole = $('<tr></tr>');

                var $colName = $('<td style="width: 90%;"></td>');
                $colName.html('<i class="fa fa-cube"></i> ' + objRole.Name);
                $rowRole.append($colName);

                var $colIsGranted = $('<td style="width: 10%;"></td>');
                $colIsGranted.html('<input type="checkbox" idRole="' + objRole.ID + '" class="skin-square-green chkIsGranted">');
                $rowRole.append($colIsGranted);

                $tableOfRoles.append($rowRole);
            }
        }

        _GLOBAL_SETTINGS.iCheck();
    };

    this.loadRolesOfUser = function (idUser) {
        var idUser = $modal.find(".selUserOfRoles select").val();
        var rolesOfUser = _findAllObjByProperty(usersXRoles, "SOEID", idUser);

        $("[idRole]").each(function (indexEach, item) {
            var $el = $(item);
            var idRole = $el.attr("idRole");
            var isGranted = $el.is(":checked");

            var hasRoleAccess = _findOneObjByProperty(rolesOfUser, "IDRole", idRole);

            //Off ifChanged Event
            var fnIfChanged;
            var events = _getBindEvends($el);
            if (events.ifChanged) {
                fnIfChanged = events.ifChanged["0"].handler;
                $el.off('ifChanged');
            }

            // Check or Uncheck Input
            if (hasRoleAccess) {
                if (isGranted == false) {
                    $el.iCheck('check');
                }
            } else {
                if (isGranted) {
                    $el.iCheck('uncheck');
                }
            }

            //Bind ifChanged Event
            if (fnIfChanged) {
                $el.on('ifChanged', fnIfChanged);
            }
        });
    };

    this.saveUserXRoles = function () {
        var spAdminRole = _getDBObjectFullName("AdminUser");
        var idUser = $modal.find(".selUserOfRoles select").val();

        if (idUser != 0) {
            var totalRoles = $("[idRole]").length;
            $("[idRole]").each(function (indexEach, item) {
                var $el = $(item);
                var idRole = $el.attr("idRole");
                var isGranted = $el.is(":checked");
                var action = "AddRole";

                if (isGranted == false) {
                    action = "DeleteRole";
                }

                //Save changes only
                var rolesOfUser = _findAllObjByProperty(usersXRoles, "SOEID", idUser);
                var hasRoleAccess = _findOneObjByProperty(rolesOfUser, "IDRole", idRole);

                //Insert only: If not exist and Insert OR exists and delete
                if ((action == "AddRole" && !hasRoleAccess) || (action == "DeleteRole" && hasRoleAccess)) {
                    _callProcedure({
                        response: false,
                        loadingMsgType: "fullLoading",
                        loadingMsg: "Saving roles of user...",
                        name: "[dbo].[" + spAdminRole + "]",
                        params: [
                            { "Name": "@Action", "Value": action },
                            { "Name": "@UserSOEID", "Value": idUser },
                            { "Name": "@IDRole", "Value": idRole },
                            { "Name": "@SessionSOEID", "Value": _getSOEID() }
                        ],
                        success: {}
                    });
                }
            });
        } else {
            _showNotification("error", "Please select at least one User.");
        }
    }

    //Load Roles Availables
    this.loadRoles();

    //Load selected roles of current user
    $modal.find(".selUserOfRoles").trigger("change");
}