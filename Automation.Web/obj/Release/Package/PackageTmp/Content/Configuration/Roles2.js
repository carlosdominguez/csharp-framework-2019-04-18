﻿$(document).ready(function () {
    loadPartialViewDBTable();

    //To get "AdminRole" spName
    _loadDBObjects(function () {
        //Load roles in "Mapping Reports to Roles"
        loadDropDownOfRoles("#selRoleOfReports");

        //On change select of roles
        $("#selRoleOfReports").change(function () {
            loadReportsOfRole($("#selRoleOfReports").val());
        });

        //Load Reports Availables
        loadReports();

        //On Save Reports of Role
        $("#btnSaveRoleXReports").click(function () {
            saveReportsOfRole();
        });
    });
});

function loadPartialViewDBTable() {
    _loadDBObjectsEditTable("Role", "content-tblRole");
    _loadDBObjectsEditTable("RoleXPermit", "content-tblRoleXPermit");
};

function loadDropDownOfRoles(idSelect) {
    var spName = _getDBObjectFullName("AdminRole");
    
    _callProcedure({
        response: true,
        loadingMsgType: "topBar",
        loadingMsg: "Loading roles...",
        name: "[dbo].[" + spName + "]",
        params: [
            { "Name": "@Action", "Value": "List" }
        ],
        //Show message only for the last 
        success: {
            fn: function (resultList) {
                var $select = $(idSelect);
                $select.append($('<option>', {
                    value: "0",
                    text: "-- Select a Role --"
                }));

                //Load options in select
                for (var i = 0; i < resultList.length; i++) {
                    var objRole = resultList[i];
                    $select.append($('<option>', {
                        value: objRole.ID,
                        text: objRole.Name
                    }));
                }

                $select.select2({
                    placeholder: 'Choose a role:',
                    allowClear: true
                }).on('select2-open', function () {
                    // Adding Custom Scrollbar
                    $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
                });
                //$select.select2();
            }
        }
    });
}

function loadReports() {
    var spAdminReport = _getDBObjectFullName("AdminReport");

    _callProcedure({
        response: true,
        loadingMsgType: "topBar",
        loadingMsg: "Loading reports by role...",
        name: "[dbo].[" + spAdminReport + "]",
        params: [
            { "Name": "@Action", "Value": "List" }
        ],
        success: {
            fn: function (resultList) {
                var categoryReports = Enumerable.From(resultList)
                    .GroupBy(
    	                "$.Category",
                        "{ ID: $.ID, Category: $.Category, Name: $.Name, Path: $.Path}",
                        "{ Category: $, Reports: $$.ToArray() }"
                    )
                    .ToArray();

                for (var i = 0; i < categoryReports.length; i++) {
                    var objCategoryReport = categoryReports[i];

                    var $title = $('<p class="text-success"></p>');
                    $title.html('<i class="fa fa-line-chart"></i> ' + objCategoryReport.Category);

                    var idTable = 'tblReportsBody' + objCategoryReport.Category;
                    var $tableOfReports = $(
                        '<table class="table table-hover"> ' +
                        //'    <thead> ' +
                        //'        <tr> ' +
                        //'            <th style="width: 90%;">Report Name</th> ' +
                        //'            <th style="width: 10%;">Grand</th> ' +
                        //'        </tr> ' +
                        //'    </thead> ' +
                        '    <tbody id="' + idTable + '"> ' +
                        '    </tbody> ' +
                        '</table> '
                    );

                    $("#reportCategories").append($title);
                    $("#reportCategories").append($tableOfReports);

                    //Add rows to table
                    for (var j = 0; j < objCategoryReport.Reports.length; j++) {
                        var objReport = objCategoryReport.Reports[j];

                        var $rowReport = $('<tr></tr>');

                        var $colName = $('<td style="width: 90%;"></td>');
                        $colName.html('<i class="fa fa-bar-chart"></i> ' + objReport.Name);
                        $rowReport.append($colName);

                        var $colIsGranted = $('<td style="width: 10%;"></td>');
                        $colIsGranted.html('<input type="checkbox" idReport="'+objReport.ID+'" class="skin-square-green chkIsGranted">');
                        $rowReport.append($colIsGranted);

                        $tableOfReports.append($rowReport);
                    }
                }

                _GLOBAL_SETTINGS.iCheck();
            }
        }
    });
}

function loadReportsOfRole(idRole) {
    var spAdminReport = _getDBObjectFullName("AdminRoleXReport");
    var idRole = $("#selRoleOfReports").val();

    _callProcedure({
        response: true,
        loadingMsgType: "topBar",
        loadingMsg: "Loading reports of selected role...",
        name: "[dbo].[" + spAdminReport + "]",
        params: [
            { "Name": "@Action", "Value": "List" },
            { "Name": "@IDRole", "Value": idRole }
        ],
        success: {
            fn: function (reportsOfRole) {

                $("[idReport]").each(function (indexEach, item) {
                    var $el = $(item);
                    var idReport = $el.attr("idReport");
                    var isGranted = $el.is(":checked");

                    var hasReportAccess = _findOneObjByProperty(reportsOfRole, "IDReport", idReport);
                    if (hasReportAccess) {
                        if (isGranted == false) {
                            $el.iCheck('check');
                        }
                    } else {
                        if (isGranted) {
                            $el.iCheck('uncheck');
                        }
                    }
                });

                
            }
        }
    });
}

function saveReportsOfRole() {
    var spAdminReport = _getDBObjectFullName("AdminRoleXReport");
    var idRole = $("#selRoleOfReports").val();

    if (idRole != 0) {
        var totalReports = $("[idReport]").length;
        $("[idReport]").each(function (indexEach, item) {
            var $el = $(item);
            var idReport = $el.attr("idReport");
            var isGranted = $el.is(":checked");
            var action = "Insert";

            if (isGranted == false) {
                action = "Delete";
            }

            _callProcedure({
                response: true,
                loadingMsgType: "topBar",
                loadingMsg: "Saving reports of role...",
                name: "[dbo].[" + spAdminReport + "]",
                params: [
                    { "Name": "@Action", "Value": action },
                    { "Name": "@IDRole", "Value": idRole },
                    { "Name": "@IDReport", "Value": idReport },
                    { "Name": "@SessionSOEID", "Value": _getSOEID() }
                ],
                success: {
                    fn: function (resultList) {
                        if (indexEach === totalReports - 1) {
                            _showAlert({
                                type: 'success',
                                title: "Message",
                                content: "The data was saved successfully.",
                                animateScrollTop: true
                            }); 
                        }
                        
                    }
                }
            });
        });
    } else {
        _showAlert({
            type: 'error',
            title: "Message",
            content: "Please select at least one Role.",
            animateScrollTop: true
        });
    }
}