﻿/// <reference path="../Shared/plugins/util/global.js" />

$(document).ready(function () {
    _loadDBObjects(function () {
        loadPermits();
    });
});

function loadPermits() {
    var objListPermits;
    
    //Create nestable List
    _loadPermitsFromDB(function(permitsList){
        objListPermits = new _classNestableList({
            id: "#content-list-permits",
            entity: "Permit",
            maxDepth: 3,
            buttons: {
                addNew: {}
            },
            onSave: function (newPermits, action, objPermit) {
                console.log(newPermits, action, objPermit);

                if (action == "Insert") {
                    addPermit(objPermit);
                }

                if (action == "Delete") {
                    deletePermit(objPermit);
                }

                if (action == "Edit") {
                    editPermit(objPermit);
                }

                if (action == "Reorder") {
                    //Save all permits with new NumOrder
                    for (var i = 0; i < newPermits.length; i++) {
                        editPermit(newPermits[i], false);
                    }

                    //Refresh Permits list when all is saved
                    _execOnAjaxComplete(function () {
                        //Update Menus in Framework
                        _refreshSession();

                        //Update List of permits
                        _loadPermitsFromDB(function (permitsListDB) {
                            _showNotification("success", "Permits order was modified successfully", "ReorderPermit");

                            objListPermits.loadItemsList(permitsListDB);
                        });
                    });
                }
            },
            items: permitsList,

            //type: {string} {input-icon} {radio}
            columns: [
                { type: 'string', fieldKey: 'ID', showInList: false, showInForm: false },
                { type: 'string', fieldKey: 'ParentID', showInList: false, showInForm: false },
                { type: 'string', fieldKey: 'Category', width: '160px', fieldPlaceHolder: 'eg. Main', valueTemplate: ':NumOrder - :Category', showInList: true, showInForm: true },
                { type: 'input-icon', fieldKey: 'ClassIcon', fieldPlaceHolder: 'eg. fa-calendar', showInList: false, showInForm: true },
                { type: 'string', fieldKey: 'Name', width: '200px', fieldPlaceHolder: 'eg. EUC Home', valueTemplate: "<i class='fa :ClassIcon'></i> _cropWord(:Name, 15)", showInList: true, showInForm: true },
                { type: 'string', fieldKey: 'Path', width: '310px', fieldPlaceHolder: 'eg. /EUCInventory/', valueTemplate: "_cropWord(:Path, 39)", showInList: true, showInForm: true },
                { type: 'textarea', fieldKey: 'AccessCommentHtml', showInList: false, fieldPlaceHolder: 'eg. Only Admins and Managers have access to Reports.', showInForm: true },
                { type: 'string', fieldKey: 'NumOrder', showInList: false, showInForm: false }
            ]
        });
    });

    function addPermit(objPermit) {
        var spName = _getDBObjectFullName("AdminPermit");

        _callProcedure({
            loadingMsgType: "topBar",
            loadingMsg: "Saving new permit '" + objPermit.Name + "'...",
            name: "[dbo].[" + spName + "]",
            params: [
                { "Name": "@Action", "Value": "Insert" },
                { "Name": "@ParentID", "Value": objPermit.ParentID },
                { "Name": "@NumOrder", "Value": objPermit.NumOrder },
                { "Name": "@ClassIcon", "Value": objPermit.ClassIcon },
                { "Name": "@Category", "Value": objPermit.Category },
                { "Name": "@Name", "Value": objPermit.Name },
                { "Name": "@Path", "Value": objPermit.Path },
                { "Name": "@AccessCommentHtml", "Value": objPermit.AccessCommentHtml },
                { "Name": "@SessionSOEID", "Value": _getSOEID() }
            ],
            success: function () {
                _showNotification("success", "Permit was saved successfully", "AddPermit");

                //Update Menus in Framework
                _refreshSession();

                //Refresh Permits List from DB
                _loadPermitsFromDB(function (permitsListDB) {
                    objListPermits.loadItemsList(permitsListDB);
                });
            }
        });
    }

    function deletePermit(objPermit) {
        var spName = _getDBObjectFullName("AdminPermit");

        _callProcedure({
            loadingMsgType: "topBar",
            loadingMsg: "Deleting permit '" + objPermit.Name + "'...",
            name: "[dbo].[" + spName + "]",
            params: [
                { "Name": "@Action", "Value": "Delete" },
                { "Name": "@IDRow", "Value": objPermit.ID },
                { "Name": "@SessionSOEID", "Value": _getSOEID() }
            ],
            success: function () {
                _showNotification("success", "Permit was deleted successfully", "DeletePermit");

                //Update Menus in Framework
                _refreshSession();

                //Refresh Permits List from DB
                _loadPermitsFromDB(function (permitsListDB) {
                    objListPermits.loadItemsList(permitsListDB);
                });
            }
        });
    }

    function editPermit(objPermit, flagRefresh) {
        var spName = _getDBObjectFullName("AdminPermit");

        _callProcedure({
            loadingMsgType: "topBar",
            loadingMsg: "Saving changes for permit '" + objPermit.Name + "'...",
            name: "[dbo].[" + spName + "]",
            params: [
                { "Name": "@Action", "Value": "Edit" },
                { "Name": "@IDRow", "Value": objPermit.ID },
                { "Name": "@ParentID", "Value": objPermit.ParentID },
                { "Name": "@NumOrder", "Value": objPermit.NumOrder },
                { "Name": "@ClassIcon", "Value": objPermit.ClassIcon },
                { "Name": "@Category", "Value": objPermit.Category },
                { "Name": "@Name", "Value": objPermit.Name },
                { "Name": "@Path", "Value": objPermit.Path },
                { "Name": "@AccessCommentHtml", "Value": objPermit.AccessCommentHtml },
                { "Name": "@SessionSOEID", "Value": _getSOEID() }
            ],
            success: function () {
                if (typeof flagRefresh == 'undefined' || flagRefresh) {
                    _showNotification("success", "Permit was modified successfully", "EditPermit");

                    //Update Menus in Framework
                    _refreshSession();

                    //Refresh Permits List from DB
                    _loadPermitsFromDB(function (permitsListDB) {
                        objListPermits.loadItemsList(permitsListDB);
                    });
                }
            }
        });
    }
}