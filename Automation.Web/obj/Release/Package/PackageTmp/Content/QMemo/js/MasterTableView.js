/// <reference path="../../Shared/plugins/util/global.js" />
/// <reference path="QMemoGlobal.js" />

//Set full Screen
_toggleFullScreen();

//Not check for new full suite files
_IGNORE_FULL_SUITE_FILE = true;

$(document).ready(function () {
    //Set tab title
    $('title').text($("#HFQMemoAccountFilter").val() + ' ' + $("#HFBusinessFilter").val());

    //Set summary title
    var htmlSummary = "";

    var reportName;

    //QMemo Accounts By Bussiness Details
    if ($("#HFTypeReportFilter").val() == "QMemoXBusinessDetailPaging") {
        reportName = "<b>QMemo Mapping Table Details: </b>";
    }

    //Full Suite Details
    if ($("#HFTypeReportFilter").val() == "FullSuiteMapDetailPaging") {
        reportName = "<b>Full Suite Details: </b>";
    }

    htmlSummary += "" + $("#HFYearFilter").val() + "Q";
    htmlSummary += $("#HFQuarterFilter").val() + " ";
    htmlSummary += $("#HFQMemoAccountFilter").val() + " ";
    htmlSummary += $("#HFBusinessFilter").val() + " ";
    htmlSummary += $("#HFBalance").val();

    $('#viewSummary').html(reportName + htmlSummary);

    //Export to excel create manually
    $("#btnExportMasterTable").click(function () {
        downloadMasterTableSummary();
    });

    //Load Business
    _loadBusiness(function () {
        //Load table by type
        loadTableByType();
    });
});

function getSelectedYear() {
    var selVal = $("#HFYearFilter").val();
    return selVal;
}

function getSelectedQuarter() {
    var selVal = $("#HFQuarterFilter").val();
    return selVal;
}

function getFilterGroup(ptype) {
    var filterValue = "";
    var filterGroup = null;

    if (ptype == "Year") {
        filterValue = getSelectedYear();
        filterValue = filterValue ? filterValue : 0;
    }

    if (ptype == "Quarter") {
        filterValue = getSelectedQuarter();
        filterValue = filterValue ? filterValue : 0;
    }

    if (ptype == "BusinessID") {
        filterValue = $("#HFBusinessIDFilter").val();
        filterValue = filterValue ? filterValue : 0;
    }

    if (ptype == "QMemoID") {
        filterValue = $("#HFQMemoAccountIDFilter").val();
        filterValue = filterValue ? filterValue : 0;
    }

    if (filterValue != null) {
        filterGroup = new $.jqx.filter();
        var filter_or_operator = 1;
        var filterCondition = 'equal';
        var filter = filterGroup.createfilter('stringfilter', filterValue, filterCondition);
        filterGroup.addfilter(filter_or_operator, filter);
    }

    return filterGroup;
};

function downloadMasterTableSummary() {
    var spName;
    var spParams;
    var filename;
    var filterData = $.jqxGridApi.getFilter("#tbl-master-table");
    var pqmemoID = $("#HFQMemoAccountIDFilter").val();
    var pqmemo = $("#HFQMemoAccountFilter").val();
    var pbusinessID = $("#HFBusinessIDFilter").val();

    var pbusiness = $("#HFBusinessFilter").val();
    pbusiness = _replaceAll(" ", "", pbusiness);
    pbusiness = _replaceAll("-", "", pbusiness);

    var onlyMapped = $("#HFOnlyMapped").val();
    if (typeof onlyMapped == "undefined") {
        onlyMapped = "1";
    }

    //QMemo Accounts By Bussiness Details
    if ($("#HFTypeReportFilter").val() == "QMemoXBusinessDetailPaging") {
        spName = "[SP_FSQuarter_MTQMemoXBusinessDetailPagingExcel]";
        spParams = [
            { "Name": "@Year", "Value": getSelectedYear() },
            { "Name": "@Quarter", "Value": getSelectedQuarter() },
            { "Name": "@StrWhere", "Value": filterData.Where },
            { "Name": "@StrOrderBy", "Value": filterData.Sort },
            { "Name": "@StartIndexRow", "Value": 0 },
            { "Name": "@EndIndexRow", "Value": 9999999 },
            { "Name": "@BusinessIDs", Value: _getStrBusinessIDByUser() },
            { "Name": "@OnlyMapped", "Value": onlyMapped }
        ];

        filename = "MappedBalanceDetails_" + getSelectedYear() + "Q" + getSelectedQuarter() + "_" + pqmemo + "_" + pbusiness + "_" + _createCustomID(false) + "";
    }

    //QMemo Accounts By Bussiness Details
    if ($("#HFTypeReportFilter").val() == "FullSuiteMapDetailPaging") {
        spName = "[SP_FSQuarter_MTFullSuiteMapDetailPagingExcel]";
        spParams = [
            { "Name": "@Year", "Value": getSelectedYear() },
            { "Name": "@Quarter", "Value": getSelectedQuarter() },
            { "Name": "@StrWhere", "Value": filterData.Where },
            { "Name": "@StrOrderBy", "Value": filterData.Sort },
            { "Name": "@StartIndexRow", "Value": 0 },
            { "Name": "@EndIndexRow", "Value": 9999999 },
            { "Name": "@BusinessIDs", Value: _getStrBusinessIDByUser() },
            { "Name": "@OnlyMapped", "Value": onlyMapped }
        ];

        filename = "FullSuiteDetails_" + getSelectedYear() + "Q" + getSelectedQuarter() + "_" + pqmemo + "_" + pbusiness + "_" + _createCustomID(false) + "";
    }

    _downloadExcel({
        spName: spName,
        spParams: spParams,
        filename: filename,
        success: {
            msg: "Please Wait, this operation may take some time to complete. Generating report..."
        }
    });
};

//-----------------------------------------------------------------------
// VIEW REPORT BY TYPE
//-----------------------------------------------------------------------
function loadTableByType() {
    var gridConfig;

    //QMemo Accounts By Bussiness Details
    if ($("#HFTypeReportFilter").val() == "QMemoXBusinessDetailPaging") {
        gridConfig = getConfigViewQMemoXBusinessDetailPaging();
    }

    //Full Suite Details
    if ($("#HFTypeReportFilter").val() == "FullSuiteMapDetailPaging") {
        gridConfig = getConfigViewFullSuiteMapDetailPaging();
    }

    var gridHeight = "900";
    var browserHeight = document.documentElement.clientHeight;

    //RCA TV
    if (browserHeight == 638) {
        gridHeight = "535";
    }

    //HP 840
    if (browserHeight == 678) {
        gridHeight = "570";
    }

    $.jqxGridApi.create({
        showTo: "#tbl-master-table",
        options: {
            //for comments or descriptions
            height: gridHeight,
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: true,
            sortable: true,
            editable: true,
            //Clear div of ShowTo, some times filters aren't working when another grid is created in same div
            cleardiv: true
        },
        sp: {
            Name: gridConfig.spName,
            Params: gridConfig.spParams
        },
        source: gridConfig.source,
        //type: string - text - number - int - float - date - time 
        //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
        //cellsformat: ddd, MMM dd, yyyy h:mm tt, c2
        columns: gridConfig.columns,
        ready: gridConfig.fnReady
    });
}

//QMemo Accounts Mapped Download with Filters
function getConfigViewQMemoXBusinessDetailPaging() {
    var columns = [];
    var spName = "";
    var source;
    var fnReady;

    //Set columns
    columns.push({ name: "YEAR", type: "int", hidden: true });
    columns.push({ name: "QUARTER", type: "int", hidden: true });

    columns.push({ name: "BUSINESS_ID", type: "int", hidden: true });
    columns.push({ name: "BUSINESS_NAME", text: "Business", type: 'string', filtertype: "input" });

    columns.push({ name: "QMEMO_ACCOUNT_ID", type: "int", hidden: true });
    columns.push({ name: "QMEMO_ACCOUNT", text: "QMemo Account", type: 'string', filtertype: "input" });
    columns.push({ name: "QMEMO_ACCOUNT_DESC", text: "QMemo Account Desc", type: 'string', width: '180px', filtertype: "input" });

    columns.push({ name: "GL_ACCT_ID", text: "GL Account", type: 'string', filtertype: "input" });
    columns.push({ name: "GL_ACCT_DESC", text: "GL Account Desc", type: 'string', width: '180px', filtertype: "input" });
    columns.push({ name: "FDL_ACCT_ID", text: "FDL Account", type: 'string', filtertype: "input" });
    columns.push({ name: "FDL_ACCT_DESC", text: "FDL Account Desc", type: 'string', width: '180px', filtertype: "input" });
    columns.push({ name: "GOC", text: "GOC", type: 'string', filtertype: "input" });
    columns.push({ name: "ENTRPS_PROD_ID", text: "Enterprice Product", type: 'string', filtertype: "input" });
    columns.push({ name: "ENTRPS_PROD_DESC", text: "Enterprice Product Desc", type: 'string', filtertype: "input" });
    columns.push({ name: "CUST_ID", text: "Customer ID", type: 'string', filtertype: "input" });
    columns.push({ name: "SUB_GL_FEED_ID", text: "SubGL Feed ID", type: 'string', filtertype: "input" });
    columns.push({ name: "AFFL_FRS_BSUNIT_ID", text: "Affiliate BU", type: 'string', filtertype: "input" });
    columns.push({ name: "ICE_CODE", text: "ICE Code", type: 'string', filtertype: "input" });
    columns.push({ name: "BALANCE", text: "Balance", type: 'float', filtertype: 'number', cellsformat: 'd' });

    //Set default filter by Year and Quarter
    var indexYear = _findIndexByProperty(columns, "name", "YEAR");
    var indexQuarter = _findIndexByProperty(columns, "name", "QUARTER");
    columns[indexYear].filter = getFilterGroup("Year");
    columns[indexQuarter].filter = getFilterGroup("Quarter");

    //Add QMemo ID Filter
    if ($("#HFQMemoAccountIDFilter").val()) {
        var indexQMemoID = _findIndexByProperty(columns, "name", "QMEMO_ACCOUNT_ID");
        columns[indexQMemoID].filter = getFilterGroup("QMemoID");
    }

    //Add Business ID Filter
    if ($("#HFBusinessIDFilter").val()) {
        var indexBusinessID = _findIndexByProperty(columns, "name", "BUSINESS_ID");
        columns[indexBusinessID].filter = getFilterGroup("BusinessID");
    }

    //Set Procedure
    spName = "[dbo].[SP_FSQuarter_MTQMemoXBusinessDetailPaging]";

    //Set Source
    source = {
        // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
        dataBinding: "Server Paging",
        pagesize: 200
    };

    //Set on ready fuction
    fnReady = function () { };

    //Params
    var spParams = [
        { Name: "@Year", Value: getSelectedYear() },
        { Name: "@Quarter", Value: getSelectedQuarter() },
        { Name: "@BusinessIDs", Value: _getStrBusinessIDByUser() },
        { Name: "@OnlyMapped", Value: $("#HFOnlyMapped").val() }
    ];

    return {
        columns: columns,
        spName: spName,
        spParams: spParams,
        source: source,
        fnReady: fnReady
    };
};

//Full Suite Map Detail Paging
function getConfigViewFullSuiteMapDetailPaging() {
    var columns = [];
    var spName = "";
    var source;
    var fnReady;

    //Set columns
    //columns.push({ name: "MT.ID", text: "MasterTableID", type: "int", filtertype: "number" });



    columns.push({ name: "QMEMO_ACCOUNT_ID", type: "int", hidden: true });
    columns.push({ name: "BUSINESS_ID", type: "int", hidden: true });

    columns.push({ name: "YEAR", text: "Year", type: "int", filterable: false, sortable: false, width: '60px', cellsalign: 'center', align: 'center' });
    columns.push({ name: "QUARTER", text: "Quarter", type: "int", filterable: false, sortable: false, width: '60px', cellsalign: 'center', align: 'center' });
    columns.push({ name: "ACTG_PRD_NO", text: "Account Period Number", type: 'string', filterable: false, sortable: false, width: '50px', cellsalign: 'center', align: 'center' });

    columns.push({ name: "FRS_BSUNIT_ID", text: "BU", type: "int", width: '50px', filtertype: "number", filterable: false, sortable: false, cellsalign: 'center', align: 'center' });
    //columns.push({ name: "ISNULL(BS.ID,0)", text: "BusinessID", type: "int", filtertype: "number" });
    columns.push({ name: "QMEMO_ACCOUNT", text: "QMemo Account", type: 'string', filterable: false, sortable: false, width: '120px', filtertype: "input", cellsalign: 'center', align: 'center' });
    columns.push({ name: "BUSINESS_NAME", text: "Business", type: 'string', filterable: false, sortable: false, width: '100px', filtertype: "input", cellsalign: 'center', align: 'center' });

    columns.push({ name: "MANAGED_SEGMENT", text: "Managed Segment", type: 'int', width: '100px', filtertype: "number", cellsalign: 'center', align: 'center' });
    columns.push({ name: "MANAGED_SEGMENT_DESC", text: "Managed Segment Desc", type: 'string', width: '200px', filtertype: "input" });

    columns.push({ name: "LV_ID", text: "Legal Vehicle", type: 'string', width: '100px', filtertype: "input", cellsalign: 'center', align: 'center' });

    columns.push({ name: "GOC", text: "GOC", type: 'string', width: '100px', filtertype: "input", cellsalign: 'center', align: 'center' });
    columns.push({ name: "GL_ACCT_ID", text: "GL Account", type: 'string', filtertype: "input", cellsalign: 'center', align: 'center' });
    columns.push({ name: "GL_ACCT_DESC", text: "GL Account Desc", type: 'string', width: '280px', filtertype: "input" });

    //columns.push({ name: "MT.FDLAccountID", text: "BD FDL Account ID", type: 'int', filtertype: "number" });
    columns.push({ name: "FDL_ACCT_ID", text: "FDL Account", type: 'string', filtertype: "input" });
    columns.push({ name: "FDL_ACCT_DESC", text: "FDL Account Desc", type: 'string', filtertype: "input" });

    columns.push({ name: "ENTRPS_PROD_ID", text: "Enterprice Product", type: 'string', filtertype: "input" });
    columns.push({ name: "ENTRPS_PROD_DESC", text: "Enterprice Product Desc", type: 'string', filtertype: "input" });

    columns.push({ name: "SRC_SYS_ID", text: "Source System", type: 'string', filtertype: "input" });
    columns.push({ name: "SUB_GL_FEED_ID", text: "Journal Sub GL Feed", type: 'string', filtertype: "input" });
    columns.push({ name: "CPRT_GOC", text: "Counter Party GOC", type: 'string', filtertype: "input" });
    columns.push({ name: "AFFL_LV_ID", text: "Affiliate Legal Vehicle", type: 'string', filtertype: "input" });
    columns.push({ name: "AFFL_FRS_BSUNIT_ID", text: "Affiliate BU", type: 'string', filtertype: "input" });
    columns.push({ name: "ICE_CODE", text: "ICE Code", type: 'string', filtertype: "input" });
    columns.push({ name: "CUST_ID", text: "Customer ID", type: 'string', filtertype: "input" });
    //columns.push({ name: "T.Code", text: "Transaction Currency Code", type: 'string', filtertype: "input" });
    columns.push({ name: "GAAP_TYP_CD", text: "GAAP Type Code", type: 'string', filtertype: "input" });

    columns.push({ name: "BSYTD_ISQTD_BALANCE", text: "Balance", type: 'float', filtertype: 'number', cellsformat: 'd' });
    columns.push({ name: "FULL_KEY", text: "Key", type: 'string', filtertype: "input" });

    //Set default filter by Year and Quarter
    //var indexYear = _findIndexByProperty(columns, "name", "YEAR");
    //var indexQuarter = _findIndexByProperty(columns, "name", "QUARTER");
    //columns[indexYear].filter = getFilterGroup("Year");
    //columns[indexQuarter].filter = getFilterGroup("Quarter");

    //Add QMemo ID Filter
    if ($("#HFQMemoAccountIDFilter").val()) {
        var indexQMemoID = _findIndexByProperty(columns, "name", "QMEMO_ACCOUNT_ID");
        columns[indexQMemoID].filter = getFilterGroup("QMemoID");
    }

    //Add Business ID Filter
    if ($("#HFBusinessIDFilter").val()) {
        var indexBusinessID = _findIndexByProperty(columns, "name", "BUSINESS_ID");
        columns[indexBusinessID].filter = getFilterGroup("BusinessID");
    }

    //Set Procedure
    spName = "[dbo].[SP_FSQuarter_MTFullSuiteMapDetailPaging]";

    //Set Source
    source = {
        // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
        dataBinding: "Server Paging",
        pagesize: 200
    };

    //Set on ready fuction
    fnReady = function () { };

    //Params
    var spParams = [
        { Name: "@Year", Value: getSelectedYear() },
        { Name: "@Quarter", Value: getSelectedQuarter() },
        { Name: "@BusinessIDs", Value: _getStrBusinessIDByUser() },
        { Name: "@OnlyMapped", Value: $("#HFOnlyMapped").val() }
    ];

    return {
        columns: columns,
        spName: spName,
        spParams: spParams,
        source: source,
        fnReady: fnReady
    };
};