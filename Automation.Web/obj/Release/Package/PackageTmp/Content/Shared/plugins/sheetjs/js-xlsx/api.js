var X = XLSX;
var subAppPath = (typeof _getViewVar != "undefined" ? _getViewVar("SubAppPath") : "");

var XW = {
    /* worker message */
    msg: 'xlsx',
    /* worker scripts */
    rABS: subAppPath + '/Content/Shared/plugins/sheetjs/js-xlsx/xlsxworker2.js',
    norABS: subAppPath + '/Content/Shared/plugins/sheetjs/js-xlsx/xlsxworker1.js',
    noxfer: subAppPath + '/Content/Shared/plugins/sheetjs/js-xlsx/xlsxworker.js'
};

XLSX.createJsXlsx = function (customOptions) {
    var options = {
        idElement: "",
        //onSuccess: function (jsonData) { },
        //onError: function (data) { },
        format: "json", //csv / json / form,
        useWebWorker: true,
        useTransferrables: true,
        readAsBinaryStr: true,
        loadingMsg: "Processing the file...",
        fileInput: {
            dropText: "Drop an XLSX / XLSM / XLSB / ODS / XLS / XML file here...",
            hideDrop: false,
            inputText: "... or click here to select a file"
        }
    };

    //merge defaulOptions into options
    $.extend(true, options, customOptions);

    var rABS = typeof FileReader !== "undefined" && typeof FileReader.prototype !== "undefined" && typeof FileReader.prototype.readAsBinaryString !== "undefined";
    if (!rABS) {
        options.readAsBinaryStr = false;
        //document.getElementsByName("userabs")[0].disabled = true;
        //document.getElementsByName("userabs")[0].checked = false;
    }

    var use_worker = typeof Worker !== 'undefined';
    if (!use_worker) {
        options.useWebWorker = false;
        //document.getElementsByName("useworker")[0].disabled = true;
        //document.getElementsByName("useworker")[0].checked = false;
    }

    var transferable = use_worker;
    if (!transferable) {
        options.readAsBinaryStr = false;
        //document.getElementsByName("xferable")[0].disabled = true;
        //document.getElementsByName("xferable")[0].checked = false;
    }

    var $divDrop = $('<div class="jsxlsx-drop" ' + (options.fileInput.hideDrop ? 'style="display:none;"' : '') + '>' + options.fileInput.dropText + '</div>');
    var $inputFile = $('<input type="file" name="xlfile" class="jsxlsx-input-file" />');
    var $inputFileRow = $('<p></p>');
    $inputFileRow.append($inputFile);
    $inputFileRow.append(options.fileInput.inputText);

    $("#" + options.idElement).append($divDrop);
    $("#" + options.idElement).append($inputFileRow);

    options.dropDiv = $divDrop[0];


    loadDropDiv($divDrop[0], options);
    loadFileInput($inputFile[0], options);
}

var wtf_mode = false;

function fixdata(data) {
    var o = "", l = 0, w = 10240;
    for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
    o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
    return o;
}

function ab2str(data) {
    var o = "", l = 0, w = 10240;
    for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint16Array(data.slice(l * w, l * w + w)));
    o += String.fromCharCode.apply(null, new Uint16Array(data.slice(l * w)));
    return o;
}

function s2ab(s) {
    var b = new ArrayBuffer(s.length * 2), v = new Uint16Array(b);
    for (var i = 0; i != s.length; ++i) v[i] = s.charCodeAt(i);
    return [v, b];
}

function xw_noxfer(data, cb) {
    var worker = new Worker(XW.noxfer);
    worker.onmessage = function (e) {
        switch (e.data.t) {
            case 'ready': break;
            case 'e': console.error(e.data.d); break;
            case XW.msg: cb(JSON.parse(e.data.d)); break;
        }
    };
    var arr = rABS ? data : btoa(fixdata(data));
    worker.postMessage({ d: arr, b: rABS });
}

function xw_xfer(data, cb, options) {
    var worker = new Worker(rABS ? XW.rABS : XW.norABS);
    worker.onmessage = function (e) {
        switch (e.data.t) {
            case 'ready': break;
            case 'e': console.error(e.data.d); break;
            default: xx = ab2str(e.data).replace(/\n/g, "\\n").replace(/\r/g, "\\r"); console.log("done"); cb(JSON.parse(xx), options); break;
        }
    };
    if (rABS) {
        var val = s2ab(data);
        worker.postMessage(val[1], [val[1]]);
    } else {
        worker.postMessage(data, [data]);
    }
}

function xw(data, cb, options) {
    transferable = options.useTransferrables;
    if (transferable) xw_xfer(data, cb, options);
    else xw_noxfer(data, cb);
}

function get_radio_value(radioName) {
    var radios = document.getElementsByName(radioName);
    for (var i = 0; i < radios.length; i++) {
        if (radios[i].checked || radios.length === 1) {
            return radios[i].value;
        }
    }
}

function to_json(workbook) {
    var result = {};
    workbook.SheetNames.forEach(function (sheetName) {
        var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
        if (roa.length > 0) {
            result[sheetName] = roa;
        }
    });
    return result;
}

function to_csv(workbook) {
    var result = [];
    workbook.SheetNames.forEach(function (sheetName) {
        var csv = X.utils.sheet_to_csv(workbook.Sheets[sheetName]);
        if (csv.length > 0) {
            result.push("SHEET: " + sheetName);
            result.push("");
            result.push(csv);
        }
    });
    return result.join("\n");
}

function to_formulae(workbook) {
    var result = [];
    workbook.SheetNames.forEach(function (sheetName) {
        var formulae = X.utils.get_formulae(workbook.Sheets[sheetName]);
        if (formulae.length > 0) {
            result.push("SHEET: " + sheetName);
            result.push("");
            result.push(formulae.join("\n"));
        }
    });
    return result.join("\n");
}

var tarea = document.getElementById('b64data');
function b64it() {
    if (typeof console !== 'undefined') console.log("onload", new Date());
    var wb = X.read(tarea.value, { type: 'base64', WTF: wtf_mode });
    process_wb(wb);
}

function process_wb(wb, options) {
    var output = "";
    switch (options.format) {
        case "json":
            output = to_json(wb);
            break;
        case "form":
            output = to_formulae(wb);
            break;
        default:
            output = to_csv(wb);
    }

    if (typeof _showNotification != "undefined") {
        _showNotification("success", "File was process successfuly.");
    }

    if (options.onSuccess) {
        options.onSuccess(output);
    }
    //if (out.innerText === undefined) out.textContent = output;
    //else out.innerText = output;
    if (typeof console !== 'undefined') console.log("output", new Date());
}

function loadDropDiv(drop, options) {
    //var drop = document.getElementById('drop');
    function handleDrop(e) {

        if (typeof _showNotification != "undefined") {
            _showNotification("info", options.loadingMsg);
        }

        e.stopPropagation();
        e.preventDefault();
        rABS = options.readAsBinaryStr;
        use_worker = options.useWebWorker;
        var files = e.dataTransfer.files;
        var f = files[0];
        {
            var reader = new FileReader();
            var name = f.name;
            reader.onload = function (e) {
                if (typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
                var data = e.target.result;
                if (use_worker) {
                    xw(data, process_wb, options);
                } else {
                    var wb;
                    if (rABS) {
                        wb = X.read(data, { type: 'binary' });
                    } else {
                        var arr = fixdata(data);
                        wb = X.read(btoa(arr), { type: 'base64' });
                    }
                    process_wb(wb, options);
                }
            };
            if (rABS) reader.readAsBinaryString(f);
            else reader.readAsArrayBuffer(f);
        }
    }

    function handleDragover(e) {
        e.stopPropagation();
        e.preventDefault();
        e.dataTransfer.dropEffect = 'copy';
    }

    if (drop.addEventListener) {
        drop.addEventListener('dragenter', handleDragover, false);
        drop.addEventListener('dragover', handleDragover, false);
        drop.addEventListener('drop', handleDrop, false);
    }
}

function loadFileInput(xlf, options) {
    //var xlf = document.getElementById('xlf');
    function handleFile(e) {

        if (typeof _showNotification != "undefined") {
            _showNotification("info", options.loadingMsg);
        }

        rABS = options.readAsBinaryStr;
        use_worker = options.useWebWorker;
        var files = e.target.files;
        var f = files[0];
        {
            var reader = new FileReader();
            var name = f.name;
            reader.onload = function (e) {
                if (typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
                var data = e.target.result;
                if (use_worker) {
                    xw(data, process_wb, options);
                } else {
                    var wb;
                    if (rABS) {
                        wb = X.read(data, { type: 'binary' });
                    } else {
                        var arr = fixdata(data);
                        wb = X.read(btoa(arr), { type: 'base64' });
                    }
                    process_wb(wb, options);
                }
            };
            if (rABS) reader.readAsBinaryString(f);
            else reader.readAsArrayBuffer(f);
        }
    }

    if (xlf.addEventListener) xlf.addEventListener('change', handleFile, false);
}
