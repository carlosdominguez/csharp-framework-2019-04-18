function _createMCASummaryLink(customOptions) {
    //Default Options.
    var options = {
        idContainer: "lblMCASummary",
        idLinkSummary: "linkMCA-" + _createCustomID(),
        imgMCAInfoPath: _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png',
        idMCAList: [431]
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    //Add MCA Link summary
    var $mcaLinkSummary = $(
        '<div id="' + options.idLinkSummary + '"> ' +
        '   MCA Control (' + options.idMCAList.length + ' <div class="infoTooltip" style="float: left;"></div> ) <a href="#" class="btnEditMCA"> Edit <i class="fa fa-edit"></i></a> ' +
        '</div>');

    //Add elements to container
    $("#" + options.idContainer).append($mcaLinkSummary);

    //On click Edit MCA
    $mcaLinkSummary.find(".btnEditMCA").click(function () {
        var htmlContentModal = "<div id='popupContainerMCA'></div>";
        var objMCAPlugin = null;

        _showModal({
            width: '70%',
            modalId: "SelectMCA",
            addCloseButton: true,
            buttons: [{
                name: "Select MCA",
                class: "btn-success",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    objMCAPlugin.fnOnSelect();
                }
            }],
            title: "Please select MCA:",
            onReady: function ($modal) {
                //Create MCA Table
                objMCAPlugin = _createMCATable({
                    idContainer: "popupContainerMCA",
                    groupBy: ['Function', 'Center'],
                    hideSelectBtn: true,
                    onSelect: function (objMCA) {
                        options.idMCAList = [objMCA.ID];
                        refreshTooltip();

                        //Close Modal
                        $modal.find(".close").click();
                    }
                });
            },
            contentHtml: htmlContentModal
        });
    });

    //Load MCA Information
    function refreshTooltip() {
        var strWhere = "";
        if (options.idMCAList.length > 0) {
            strWhere += "WHERE ";
            strWhere += "( [ID] IN (" + options.idMCAList.join(",") + ") ) ";
        }
        _callProcedure({
            loadingMsgType: "topBar",
            loadingMsg: "Getting MCA information...",
            name: "[Automation].[dbo].[spAFrwkMCAListPaging]",
            params: [
                { Name: "@StrWhere", Value: strWhere },
                { Name: "@StartIndexRow", Value: '0' },
                { Name: "@EndIndexRow", Value: '5000' }
            ],
            success: {
                fn: function (responseList) {
                    if (responseList.length > 0) {
                        var objMCAResult = responseList[0];
                        var htmlResult = "";
                        var infoHtml = "";

                        infoHtml += "<div style='padding: 10px; font-size: 15px;height: 500px;overflow-y:scroll;width:750px;'>";

                        infoHtml += "<table class='table table-striped'> ";
                        infoHtml += "    <tr> ";
                        infoHtml += "        <td style='width: 115px;'><b>ID</b></td> ";
                        infoHtml += "        <td>" + objMCAResult.ID + "</td>";
                        infoHtml += "    </tr> ";
                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>Function</b></td> ";
                        infoHtml += "        <td>" + objMCAResult.Function + "</td>";
                        infoHtml += "    </tr> ";
                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>Center</b></td> ";
                        infoHtml += "        <td>" + objMCAResult.Center + "</td>";
                        infoHtml += "    </tr> ";
                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>Process L1</b></td> ";
                        infoHtml += "        <td>" + objMCAResult.CriticalProcessLevel1 + "</td>";
                        infoHtml += "    </tr> ";
                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>Process L2</b></td> ";
                        infoHtml += "        <td>" + objMCAResult.CriticalProcessLevel2 + "</td>";
                        infoHtml += "    </tr> ";
                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>Process L3</b></td> ";
                        infoHtml += "        <td>" + objMCAResult.CriticalProcessLevel3 + "</td>";
                        infoHtml += "    </tr> ";
                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>Process L4</b></td> ";
                        infoHtml += "        <td>" + objMCAResult.CriticalProcessLevel4 + "</td>";
                        infoHtml += "    </tr> ";

                        if (objMCAResult.CriticalProcessLevel5) {
                            infoHtml += "<tr> ";
                            infoHtml += "    <td><b>Process L5</b></td> ";
                            infoHtml += "    <td>" + objMCAResult.CriticalProcessLevel5 + "</td>";
                            infoHtml += "</tr> ";
                        }

                        if (objMCAResult.CriticalProcessLevel6) {
                            infoHtml += "<tr> ";
                            infoHtml += "    <td><b>Process L6</b></td> ";
                            infoHtml += "    <td>" + objMCAResult.CriticalProcessLevel6 + "</td>";
                            infoHtml += "</tr> ";
                        }

                        if (objMCAResult.Platform) {
                            infoHtml += "<tr> ";
                            infoHtml += "    <td><b>Platform</b></td> ";
                            infoHtml += "    <td>" + objMCAResult.Platform + "</td>";
                            infoHtml += "</tr> ";
                        }

                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>Control L1</b></td> ";
                        infoHtml += "        <td>" + objMCAResult.ControlLevel1 + "</td>";
                        infoHtml += "    </tr> ";
                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>Control L2</b></td> ";
                        infoHtml += "        <td>" + objMCAResult.ControlLevel2 + "</td>";
                        infoHtml += "    </tr> ";
                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>Control L3</b></td> ";
                        infoHtml += "        <td>" + objMCAResult.ControlLevel3 + "</td>";
                        infoHtml += "    </tr> ";

                        if (objMCAResult.ControlLevel4) {
                            infoHtml += "<tr> ";
                            infoHtml += "    <td><b>Control L4</b></td> ";
                            infoHtml += "    <td>" + objMCAResult.ControlLevel4 + "</td>";
                            infoHtml += "</tr> ";
                        }

                        if (objMCAResult.ControlLevel5) {
                            infoHtml += "<tr> ";
                            infoHtml += "    <td><b>Control L5</b></td> ";
                            infoHtml += "    <td>" + objMCAResult.ControlLevel5 + "</td>";
                            infoHtml += "</tr> ";
                        }

                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>GPL</b></td> ";
                        infoHtml += "        <td>" + objMCAResult.GPL + "</td>";
                        infoHtml += "    </tr> ";
                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>RPL</b></td> ";
                        infoHtml += "        <td>" + objMCAResult.RPL + "</td>";
                        infoHtml += "    </tr> ";
                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>IsCritical</b></td> ";
                        infoHtml += "        <td>" + objMCAResult.IsCritical + "</td>";
                        infoHtml += "    </tr> ";
                        infoHtml += "</table> ";

                        infoHtml += "</div>";

                        htmlResult +=
                            '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="right" data-content="' + infoHtml + '" data-title="MCA Control Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: left; margin-left: 2px;"> ' +
                            '       <img style="margin-right: 5px; margin-top: 0px;" height="20" width="20" src="' + options.imgMCAInfoPath + '"/> ' +
                            '</div> ';

                        //Add tooltip info
                        $mcaLinkSummary.find(".infoTooltip").contents().remove();
                        $mcaLinkSummary.find(".infoTooltip").append(htmlResult);
                        _GLOBAL_SETTINGS.tooltipsPopovers();
                    }
                }
            }
        });
    }
    refreshTooltip();
}

function _createMCATable(customOptions) {
    //Default Options.
    var options = {
        idContainer: "containerMCA",
        idTable: "tblMCA-" + _createCustomID(),
        groupBy: [],
        hideSelectBtn: false,
        imgMCAInfoPath: _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png'
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    //Add header with rows count and filter
    var $header = $(
        '<div class="form-horizontal"> ' +
        '    <div class="row"> ' +
        '        <div class="col-md-3"> ' +
        '            <h4 class="tblMCARowsCount">Loading MCA Controls...</h4> ' +
        '        </div> ' +
        '        <div class="col-md-7"> ' +
        '            <input type="text" placeholder="MCA Finder: e.g. eRecon Process, QMemo" class="form-control col-md-10 txtFilterMCA" name="txtFilterMCA"> ' +
        '        </div> ' +
        '        <div class="col-md-2"> ' +
        '            <button type="button" class="btn btn-primary facebook btnFindMCA"><i class="fa fa-search"></i></button> ' +
        '        </div> ' +
        '    </div> ' +
        '</div>');

    //Add MCA Table
    var $tblMCA = $('<div id="' + options.idTable + '"></div>');

    //Add footer with select button
    var $footer = $(
        '<div class="form-horizontal"> ' +
        '    <div class="row"> ' +
        '        <div class="col-md-3"></div> ' +
        '        <div class="col-md-7"></div> ' +
        '        <div class="col-md-2"> ' +
        '            <button type="button" class="btn btn-primary facebook btnSelectMCA" style="margin-top: 6px;"><i class="fa fa-check-square-o"></i> Select MCA </button> ' +
        '        </div> ' +
        '    </div> ' +
        '</div>');

    //Add elements to container
    $("#" + options.idContainer).append($header);
    $("#" + options.idContainer).append($tblMCA);

    if (options.hideSelectBtn == false) {
        $("#" + options.idContainer).append($footer);
    }

    //On click find MCA
    $header.find(".btnFindMCA").click(function () {
        loadTableMCA();
    });

    //On click select MCA
    options.fnOnSelect = function () {
        var selectedRow = $.jqxGridApi.getOneSelectedRow("#" + options.idTable, true);
        if (selectedRow) {
            if (options.onSelect) {
                options.onSelect(selectedRow);
            }
        }
    }
    $footer.find(".btnSelectMCA").click(options.fnOnSelect);

    //Load Table MCA
    function loadTableMCA() {
        var strWhere = "";
        var filterMCA = $header.find(".txtFilterMCA").val();

        if (filterMCA) {
            strWhere += "WHERE ";
            strWhere += "( [GPL] like '%" + filterMCA + "%' ) OR ";
            strWhere += "( [RPL] like '%" + filterMCA + "%' ) OR ";
            strWhere += "( [Function] like '%" + filterMCA + "%' ) OR ";
            strWhere += "( [Center] like '%" + filterMCA + "%' ) OR ";
            strWhere += "( [CriticalProcessLevel1] like '%" + filterMCA + "%' ) OR ";
            strWhere += "( [CriticalProcessLevel2] like '%" + filterMCA + "%' ) OR ";
            strWhere += "( [CriticalProcessLevel3] like '%" + filterMCA + "%' ) OR ";
            strWhere += "( [CriticalProcessLevel4] like '%" + filterMCA + "%' ) OR ";
            strWhere += "( [CriticalProcessLevel5] like '%" + filterMCA + "%' ) OR ";
            strWhere += "( [CriticalProcessLevel6] like '%" + filterMCA + "%' ) OR ";
            strWhere += "( [Platform] like '%" + filterMCA + "%' ) OR ";
            strWhere += "( [ControlLevel1] like '%" + filterMCA + "%' ) OR ";
            strWhere += "( [ControlLevel2] like '%" + filterMCA + "%' ) OR ";
            strWhere += "( [ControlLevel3] like '%" + filterMCA + "%' ) OR ";
            strWhere += "( [ControlLevel4] like '%" + filterMCA + "%' ) OR ";
            strWhere += "( [ControlLevel5] like '%" + filterMCA + "%' ) ";
        }

        $.jqxGridApi.create({
            showTo: "#" + options.idTable,
            options: {
                //for comments or descriptions
                height: "500",
                autoheight: false,
                autorowheight: false,
                showfilterrow: true,
                sortable: true,
                editable: true,
                selectionmode: "singlerow",
                groupable: true
            },
            sp: {
                Name: "[Automation].[dbo].[spAFrwkMCAListPaging]",
                Params: [
                    { Name: "@StrWhere", Value: strWhere },
                    { Name: "@StartIndexRow", Value: '0' },
                    { Name: "@EndIndexRow", Value: '5000' }
                ],
                OnDataLoaded: function (responseList) {
                    $($header).find(".tblMCARowsCount").html("MCA Controls (" + responseList.length + ")");
                }
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                dataBinding: "Large Data Set"
            },
            groups: options.groupBy,
            columns: [
                //type: string - text - number - int - float - date - time 
                //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
                //cellsformat: ddd, MMM dd, yyyy h:mm tt
                {
                    name: 'ID', text: 'MCA ID', type: 'number', width: '120px', filtertype: 'number', cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                        var dataRecord = $("#" + options.idTable).jqxGrid('getrowdata', rowIndex);
                        var htmlResult = "";
                        var infoHtml = "";

                        infoHtml += "<div style='padding: 10px; font-size: 15px;height: 500px;overflow-y:scroll;width:750px;'>";

                        infoHtml += "<table class='table table-striped'> ";
                        infoHtml += "    <tr> ";
                        infoHtml += "        <td style='width: 115px;'><b>ID</b></td> ";
                        infoHtml += "        <td>" + dataRecord.ID + "</td>";
                        infoHtml += "    </tr> ";
                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>Function</b></td> ";
                        infoHtml += "        <td>" + dataRecord.Function + "</td>";
                        infoHtml += "    </tr> ";
                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>Center</b></td> ";
                        infoHtml += "        <td>" + dataRecord.Center + "</td>";
                        infoHtml += "    </tr> ";
                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>Process L1</b></td> ";
                        infoHtml += "        <td>" + dataRecord.CriticalProcessLevel1 + "</td>";
                        infoHtml += "    </tr> ";
                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>Process L2</b></td> ";
                        infoHtml += "        <td>" + dataRecord.CriticalProcessLevel2 + "</td>";
                        infoHtml += "    </tr> ";
                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>Process L3</b></td> ";
                        infoHtml += "        <td>" + dataRecord.CriticalProcessLevel3 + "</td>";
                        infoHtml += "    </tr> ";
                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>Process L4</b></td> ";
                        infoHtml += "        <td>" + dataRecord.CriticalProcessLevel4 + "</td>";
                        infoHtml += "    </tr> ";

                        if (dataRecord.CriticalProcessLevel5) {
                            infoHtml += "<tr> ";
                            infoHtml += "    <td><b>Process L5</b></td> ";
                            infoHtml += "    <td>" + dataRecord.CriticalProcessLevel5 + "</td>";
                            infoHtml += "</tr> ";
                        }

                        if (dataRecord.CriticalProcessLevel6) {
                            infoHtml += "<tr> ";
                            infoHtml += "    <td><b>Process L6</b></td> ";
                            infoHtml += "    <td>" + dataRecord.CriticalProcessLevel6 + "</td>";
                            infoHtml += "</tr> ";
                        }

                        if (dataRecord.Platform) {
                            infoHtml += "<tr> ";
                            infoHtml += "    <td><b>Platform</b></td> ";
                            infoHtml += "    <td>" + dataRecord.Platform + "</td>";
                            infoHtml += "</tr> ";
                        }

                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>Control L1</b></td> ";
                        infoHtml += "        <td>" + dataRecord.ControlLevel1 + "</td>";
                        infoHtml += "    </tr> ";
                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>Control L2</b></td> ";
                        infoHtml += "        <td>" + dataRecord.ControlLevel2 + "</td>";
                        infoHtml += "    </tr> ";
                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>Control L3</b></td> ";
                        infoHtml += "        <td>" + dataRecord.ControlLevel3 + "</td>";
                        infoHtml += "    </tr> ";

                        if (dataRecord.ControlLevel4) {
                            infoHtml += "<tr> ";
                            infoHtml += "    <td><b>Control L4</b></td> ";
                            infoHtml += "    <td>" + dataRecord.ControlLevel4 + "</td>";
                            infoHtml += "</tr> ";
                        }

                        if (dataRecord.ControlLevel5) {
                            infoHtml += "<tr> ";
                            infoHtml += "    <td><b>Control L5</b></td> ";
                            infoHtml += "    <td>" + dataRecord.ControlLevel5 + "</td>";
                            infoHtml += "</tr> ";
                        }

                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>GPL</b></td> ";
                        infoHtml += "        <td>" + dataRecord.GPL + "</td>";
                        infoHtml += "    </tr> ";
                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>RPL</b></td> ";
                        infoHtml += "        <td>" + dataRecord.RPL + "</td>";
                        infoHtml += "    </tr> ";
                        infoHtml += "    <tr> ";
                        infoHtml += "        <td><b>IsCritical</b></td> ";
                        infoHtml += "        <td>" + dataRecord.IsCritical + "</td>";
                        infoHtml += "    </tr> ";
                        infoHtml += "</table> ";

                        infoHtml += "</div>";

                        htmlResult +=
                            '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="right" data-content="' + infoHtml + '" data-title="MCA Control Information" data-trigger="click" data-html="true" data-original-title="" title="" style="float: left; margin-left: 2px;"> ' +
                            '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + options.imgMCAInfoPath + '"/> ' +
                            '</div> ';

                        htmlResult +=
                            '<span style="line-height:31px;margin-left:5px;">View Info</span>';

                        return htmlResult;
                    }
                },
                {
                    name: 'eDCFCCount', text: 'eDCFC Count', width: '130px', type: 'number', editable: false, filtertype: 'number', cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                        var dataRecord = $("#" + options.idTable).jqxGrid('getrowdata', rowIndex);
                        var marginLeft = 52;
                        var htmlResult = "";
                        var infoHtml = "<div class='contentEDCFCActivities'>Loading...</div>";

                        if (dataRecord.eDCFCCount > 0) {
                            marginLeft = 36;
                            htmlResult +=
                                '<div rel="popover" data-edcfc-mca-id="' + dataRecord.ID + '" data-color-class="success" data-container="body" data-toggle="popover" data-placement="right" data-content="' + infoHtml + '" data-title="eDCFC Activities" data-trigger="click" data-html="true" data-original-title="" title="" style="float: left; margin-left: 2px;"> ' +
                                '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + options.imgMCAInfoPath + '"/> ' +
                                '</div> ';
                        }

                        htmlResult +=
                            '<span style="line-height:31px;margin-left:5px;">' + dataRecord.eDCFCCount + '</span>';

                        return "<div style='margin-left: " + marginLeft + "px;'>" + htmlResult + "</div>";
                    }
                },
                { name: 'Function', text: 'Function', width: '130px', type: 'string', filtertype: 'checkedlist' },
                { name: 'Center', text: 'Center', width: '130px', type: 'string', filtertype: 'checkedlist' },
                { name: 'CriticalProcessLevel1', text: 'CriticalProcessLevel1', width: '200px', type: 'string', filtertype: 'checkedlist' },
                { name: 'CriticalProcessLevel2', text: 'CriticalProcessLevel2', width: '200px', type: 'string', filtertype: 'checkedlist' },
                { name: 'CriticalProcessLevel3', text: 'CriticalProcessLevel3', width: '350px', type: 'string', filtertype: 'checkedlist' },
                { name: 'CriticalProcessLevel4', text: 'CriticalProcessLevel4', width: '350px', type: 'string', filtertype: 'checkedlist' },
                { name: 'CriticalProcessLevel5', text: 'CriticalProcessLevel5', width: '350px', type: 'string', filtertype: 'checkedlist' },
                { name: 'CriticalProcessLevel6', text: 'CriticalProcessLevel6', width: '350px', type: 'string', filtertype: 'checkedlist' },
                { name: 'Platform', text: 'Platform', width: '130px', type: 'string', filtertype: 'checkedlist' },
                { name: 'ControlLevel1', text: 'ControlLevel1', width: '130px', type: 'string', filtertype: 'checkedlist' },
                { name: 'ControlLevel2', text: 'ControlLevel2', width: '130px', type: 'string', filtertype: 'checkedlist' },
                { name: 'ControlLevel3', text: 'ControlLevel3', width: '130px', type: 'string', filtertype: 'checkedlist' },
                { name: 'ControlLevel4', text: 'ControlLevel4', width: '130px', type: 'string', filtertype: 'checkedlist' },
                { name: 'ControlLevel5', text: 'ControlLevel5', width: '130px', type: 'string', filtertype: 'checkedlist' },
                { name: 'GPL', text: 'GPL', width: '130px', type: 'string', filtertype: 'checkedlist' },
                { name: 'RPL', text: 'RPL', width: '130px', type: 'string', filtertype: 'checkedlist' },
                { name: 'IsCritical', text: 'IsCritical', width: '130px', type: 'string', filtertype: 'checkedlist' }
            ],
            ready: function () {
                //On select row event
                $("#" + options.idTable).on('rowselect', function (event) {
                    //$(".btnViewIssue").attr("href", _getViewVar("SubAppPath") + "/MEIL/MakerChecker/AdminIssue?pissueID=" + event.args.row.IssueID);
                });

                $("#" + options.idTable).on("rowclick", function (event) {
                    //Load MCA Information popover
                    _GLOBAL_SETTINGS.tooltipsPopovers();

                    //Add eDCFC event to load by ajax information
                    $('*[data-edcfc-mca-id]').each(function (index, divElement) {
                        if (!_hasEvent($(divElement), "click") || (_getBindEvends($(divElement)).click && _getBindEvends($(divElement)).click.length == 1)) {
                            $(divElement).click(function () {
                                var e = $(this);
                                e.off('click');

                                var sqlGetEDCFCActivities =
                                    " SELECT " +
                                    "     N.* " +
                                    " FROM  " +
                                    "     [TITAN].[dbo].[Node] T1N  " +
                                    "     INNER JOIN [TITAN].[dbo].[NodeData] T2N ON T2N.[NodeID] = T1N.[ID] " +
                                    "     INNER JOIN [TITAN].[dbo].[qryNodeSummary] N ON T1N.[ID] = N.[NODE_ID] " +
                                    " WHERE " +
                                    "     T1N.[IsActive] = 1 AND " +
                                    "     T2N.[MetadataID] = 27 AND " +
                                    "     T2N.[Value] = '" + e.attr("data-edcfc-mca-id") + "' ";

                                _callServer({
                                    //loadingMsgType: fullLoading - notification - topBar - alert
                                    loadingMsgType: "topBar",
                                    loadingMsg: "Loading eDCFC Activities...",
                                    url: '/Ajax/ExecQuery',
                                    data: { 'pjsonSql': _toJSON(sqlGetEDCFCActivities) },
                                    type: "post",
                                    success: function (resultList) {
                                        var infoHtml = "";
                                        infoHtml += "<div style='padding: 10px; font-size: 15px;max-height: 500px;overflow-x:scroll;width:750px;'>";
                                        infoHtml += "<table class='table table-striped' style='width: 1850px;'> ";
                                        infoHtml += "    <tr> ";
                                        infoHtml += "        <td style='width: 300px;'><b>Activity</b></td> ";
                                        infoHtml += "        <td style='width: 250px;'><b>Owner</b></td> ";
                                        infoHtml += "        <td style='width: 250px;'><b>GPL</b></td> ";
                                        infoHtml += "        <td style='width: 250px;'><b>RPL</b></td> ";
                                        infoHtml += "        <td style='width: 250px;'><b>GOC</b></td> ";
                                        infoHtml += "        <td style='width: 250px;'><b>Managed Segment</b></td> ";
                                        infoHtml += "    </tr> ";

                                        for (var i = 0; i < resultList.length; i++) {
                                            var objEDCFC = resultList[i];
                                            infoHtml += "    <tr> ";
                                            infoHtml += "        <td>" + objEDCFC.NODE_NAME + "</td>";
                                            infoHtml += "        <td>" + objEDCFC.OWNER_NAME + "</td>";
                                            infoHtml += "        <td>" + objEDCFC.GPL_NAME + " [" + objEDCFC.GPL_SOEID + "]</td>";
                                            infoHtml += "        <td>" + objEDCFC.RPL_NAME + " [" + objEDCFC.RPL_SOEID + "]</td>";
                                            infoHtml += "        <td>" + objEDCFC.MANAGED_SEGMENT_DESCRIPTION + " [" + objEDCFC.MANAGED_SEGMENT + "]</td>";
                                            infoHtml += "        <td>" + objEDCFC.GOC_DESCRIPTION + " [" + objEDCFC.GOC + "]</td>";
                                            infoHtml += "    </tr> ";
                                        }

                                        infoHtml += "</table>";
                                        infoHtml += "</div>";

                                        $("#" + e.attr("aria-describedby")).find(".contentEDCFCActivities").html(infoHtml);
                                    }
                                });
                            });
                        }
                    });
                });
            }
        });
    }
    loadTableMCA();

    return options;
}