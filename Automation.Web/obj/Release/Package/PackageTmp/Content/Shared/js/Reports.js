﻿/// <reference path="../plugins/util/global.js" />

$(document).ready(function () {

    if (_getViewVar("DBIsConfigured") == "1") {
        //To get "AdminRole" spName
        _loadDBObjects(function () {
            var spAdminReport = _getDBObjectFullName("AdminReport");

            _callProcedure({
                response: true,
                loadingMsgType: "topBar",
                loadingMsg: "Loading reports by role...",
                name: "[dbo].[" + spAdminReport + "]",
                params: [
                    { "Name": "@Action", "Value": "List" }
                ],
                success: {
                    fn: function (resultList) {
                        //Sort by number
                        resultList.sort(_compareNumOrder);

                        //Load Reports Availables
                        loadReportsOfUser(resultList);
                    }
                }
            });
        });
    } else {
        //Get from temp data
        _getCurrentAppData(function (appData) {
            loadReportsOfUser(appData.Reports);
        });
    }

    _hideMenu();
});

function loadReportsOfUser(reportsList) {
    var categoryReports = Enumerable.From(reportsList)
        .GroupBy(
    	    "$.Category",
            "{ ID: $.ID, Category: $.Category, Name: $.Name, Path: $.Path}",
            "{ Category: $, Reports: $$.ToArray() }"
        )
        .ToArray();

    for (var i = 0; i < categoryReports.length; i++) {
        var objCategoryReport = categoryReports[i];

        var $title = $('<p class="text-success"></p>');
        $title.html('<i class="fa fa-line-chart"></i> ' + objCategoryReport.Category);

        var idTable = 'tblReportsBody' + objCategoryReport.Category;
        var $tableOfReports = $(
            '<table class="table table-hover"> ' +
            //'    <thead> ' +
            //'        <tr> ' +
            //'            <th style="width: 90%;">Report Name</th> ' +
            //'            <th style="width: 10%;">Grand</th> ' +
            //'        </tr> ' +
            //'    </thead> ' +
            '    <tbody id="' + idTable + '"> ' +
            '    </tbody> ' +
            '</table> '
        );

        $("#reportCategories").append($title);
        $("#reportCategories").append($tableOfReports);

        //Add rows to table
        for (var j = 0; j < objCategoryReport.Reports.length; j++) {
            var objReport = objCategoryReport.Reports[j];

            var $rowReport = $('<tr></tr>');

            var $colName = $('<td style="width: 90%;"></td>');
            $colName.html('<i class="fa fa-bar-chart"></i> ' + objReport.Name);
            $rowReport.append($colName);

            var $colIsGranted = $('<td style="width: 10%;"></td>');
            var $buttonView = $('<button type="button" class="btn btn-primary facebook btnOpenReport"><i class="fa fa-search"></i></button>');
            $buttonView.attr("idReport", objReport.ID);
            $buttonView.attr("reportName", objReport.Name);
            $buttonView.attr("reportPath", objReport.Path);
            $colIsGranted.append($buttonView);
            $rowReport.append($colIsGranted);

            $tableOfReports.append($rowReport);
        }
    }

    $(".btnOpenReport").click(function () {
        openTabReport($(this));
    });
}

function openTabReport($btn) {
    var idReport = $btn.attr("idReport");
    var reportName = $btn.attr("reportName");
    var reportPath = $btn.attr("reportPath");
    var tabTitle = _cropWord(reportName, 10);
    var idiframe = 'iframeReport' + idReport;
    var urlReport =
        _getViewVar("SubAppPath") + '/Views/Shared/CustomReportViewer.aspx' +
            '?reportPath=' + encodeURIComponent(reportPath) +
            '&reportName=' + encodeURIComponent(reportName) +
            '&iframeID=' + encodeURIComponent(idiframe) +
            '&pappKey=' + _getViewVar("AppKey");

    var $tap = $(
        '<li id="tab-report-' + idReport + '"> ' +
        '    <a href="#tab-content-report-' + idReport + '" data-toggle="tab"> ' +
        '        <i class="fa fa-bar-chart"></i> ' + tabTitle +
        '        <i style="cursor:pointer;" target="_blank" class="glyphicon glyphicon-share-alt" href="' + urlReport + '" onclick="window.open(\'' + urlReport + '\', \'_blank\');"></i>' +
        '        <i class="glyphicon glyphicon-fullscreen btnFullScreen" style="cursor:pointer;"></i> ' +
        '        <i class="fa fa-times-circle btnClose" style="cursor:pointer;"></i> ' +
        '    </a> ' +
        '</li> '
    );

    //Full Screen button
    $tap.find(".btnFullScreen").click(function () {
        $(".content-body").toggleClass("toggle-fullscreen");
        if ($(".content-body").hasClass("toggle-fullscreen")) {
            $("body").css("overflow", "hidden");
        } else {
            $("body").css("overflow", "");
        }

        resizeAllIframes();
    });

    //Remove tab
    $tap.find(".btnClose").click(function () {
        $("#tab-report-" + idReport).remove();
        $("#tab-content-report-" + idReport).remove();

        $('[href="#tab-report-list"]').click();
    });
     
    var $tabContent = $(
        '<div class="tab-pane fade in" id="tab-content-report-' + idReport + '"> ' +
        '    <div class="row"> ' +
        '        <div class="col-md-12"> ' +
        '           <h4><i class="fa fa-bar-chart"></i> ' + reportName + '</h4> ' +
        '           <iframe id="' + idiframe + '" width="100%" height="100%" class="iframe-class" frameborder="0" src="' + urlReport + '"></iframe> ' +
        '        </div>' +
        '    </div> ' +
        '</div> '
    );

    _showLoadingFullPage({ msg: "Loading report..."});

    $("#tab-reports").append($tap);
    $("#tab-reports-content").append($tabContent);

    //Click new tap
    $tap.find("a").click();

    //Subimos el Scroll de la pagina para mostrar el mensaje.
    $('html, body').animate({
        scrollTop: '0px'
    }, 800);
}

function resizeAllIframes() {
    //var iFrame = document.getElementById('iFrame1');
    //resizeIFrameToFitContent(iFrame);

    // or, to resize all iframes:
    var iframes = document.querySelectorAll("iframe");
    for (var i = 0; i < iframes.length; i++) {
        resizeIFrameToFitContent(iframes[i]);
    }
}

function resizeIFrameToFitContent(iFrame) {
    console.log("Resizing iframe...");
    var newWidth = iFrame.contentWindow.document.body.scrollWidth;
    var newHeight = $(iFrame.contentWindow.document.body).find("#ReportViewer_fixedTable").height();

    //var newHeight = ( height ? height : iFrame.contentWindow.document.body.scrollHeight);
    console.log($(iFrame.contentWindow.document.body).find("#ReportViewer_fixedTable").height());

    if (newHeight < 200) {
        
        newHeight = newHeight + 350;
    } else {
        //First display of the report, add 210 extra pixels by issue in select
        newHeight = newHeight + 210;
    }

    $(iFrame).css("width", "100%");

    if (newHeight != $(iFrame).height()) {
        $(iFrame).css("height", newHeight);
        //alert("Setting new Height... " + newHeight);
    }
}

window.addEventListener('DOMContentReady', function (e) {
    resizeAllIframes();
});

// fire iframe resize when window is resized
//$(window).resize(function () {
//    //console.debug("window resized - firing resizeHeight on iframe");
//    setIFrameSize();
//});

//function setIFrameSize() {
//    var ogWidth = 700;
//    var ogHeight = 600;
//    var ogRatio = ogWidth / ogHeight;

//    var windowWidth = $(window).width();
//    if (windowWidth < 480) {
//        var parentDivWidth = $(".iframe-class").parent().width();
//        var newHeight = (parentDivWidth / ogRatio);
//        $(".iframe-class").addClass("iframe-class-resize");
//        $(".iframe-class-resize").css("width", parentDivWidth);
//        $(".iframe-class-resize").css("height", newHeight);
//    } else {
//        $(".iframe-class").removeClass("iframe-class-resize").css({ width: '', height: '' });
//    }
//}

//function resizeIframeHeigth(idFrame) {
//    $("#" + idFrame)[0].style.height = ($("#" + idFrame)[0].contentWindow.document.body.scrollHeight + 50) + 'px';
//};