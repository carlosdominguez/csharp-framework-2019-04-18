﻿function renderComboBox(div, textDefault, procedure, pMultiple, pRole) {

    $(div).find('option').remove().end();

    var listOfParams = {
        RoleID: pRole
    }


    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        async: false,
        url: '/EC/' + procedure,
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var countY = 0

            $.each(response, function (index, element) {
                //$("#cbMyCalendar").append(new Option(element.Value, element.ID));
                if (countY == 0) {

                    $(div).append($('<option>', {
                        value: element.Value,
                        text: element.Value,
                        defaultSelected: true,
                        selected: true
                    }));
                } else {

                    $(div).append($('<option>', {
                        value: element.Value,
                        text: element.Value
                    }));
                }

                countY = countY + 1;

            })

            $(div).multiselect({
                click: function (event, ui) {
                    // renderUploadHistory(ui.value);
                }
            });


            $(div).multiselect({
                multiple: pMultiple,
                header: textDefault,
                noneSelectedText: textDefault,
                selectedList: 1

            }).multiselectfilter();

            $(div).css('width', '100px');
        }
    });
}