var _GlobalVars = {
    Period: [],
    Bussiness: []
};

var viewModel = {
    Period: ko.observable(),
    GridBusiness: ko.observable()
};


viewModel.periodSelect = ko.dependentObservable({
    read: viewModel.Period,
    write: function (Period) {

        var listOfParams = {
            pPeriod: Period.value,
            pSOEID: _getSOEID()
        }
        BindJson("getBusiness", "Period", "");      
        ko.applyBindings(viewModel.GridBusiness);
    },
    owner: viewModel
});





$(document).ready(function () {
    BindJson("getPeriod", "Period", "", function () {
        viewModel.Period = _GlobalVars["Period"];

        ko.applyBindings(viewModel.Period);
    });

});

function BindJson(procedure, varName, listOfParams, onSuccess) {

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/EC/' + procedure,
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {

            _GlobalVars[varName] = jQuery.parseJSON(json);

            if (onSuccess) {
                onSuccess();
            }
        }
    });
}
