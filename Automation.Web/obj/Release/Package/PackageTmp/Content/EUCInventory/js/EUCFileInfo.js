
$(document).ready(function () {
    var editID = '';
    var previousTabID = '';

    settingValidation();
    $('#pills').bootstrapWizard({
        'tabClass': 'nav nav-pills',
        'debug': false,
        onShow: function (tab, navigation, index) {
            
            console.log('onShow - ' + tab + ' index: ' + index);
            switch (index) {
                case 1:
                    LoadTabGeneralInformation();
                    //$('#EUCID').attr('tabindex', 1);
                    //localStorage.setItem('tabindex', 1);
                    break;
                case 2:
                    LoadTabCriticality();
                    //$('#EUCID').attr('tabindex', 2);
                    //localStorage.setItem('tabindex', 2);
                    break;
                case 3:
                    LoadTabDataGathering();
                    //$('#EUCID').attr('tabindex', 3);
                    //localStorage.setItem('tabindex', 3);
                    break;
                case 4:
                    LoadTabRemediation();
                    //$('#EUCID').attr('tabindex', 4);
                    //localStorage.setItem('tabindex', 4);
                    break;
                default:
                    break;
            }
        },
        onNext: function (tab, navigation, index) {
          console.log('onNext' + ' - index: ' + index + ' - tab: ' + tab);
            switch (index) {
                case 1:
                    var isValidGeneralInfoTab = validateRequiredGeneralInfo();
                    if (isValidGeneralInfoTab) {
                        savetblEUC();
                        LoadTabCriticality();
                    }
                    return isValidGeneralInfoTab;
                    break;
                case 2:
                    //$('a[href="#euc-form-criticality-tab2"]').css('background', 'green');
                    //$('html, body').animate({
                    //    scrollTop: '0px'
                    //}, 800);
                    //_showNotification("success", "EUC Criticality - Business Important - was saved successfully.");
                    var isValidCriticalityInfoTab = validateRequiredCriticality();
                    if (isValidCriticalityInfoTab) {
                        savetblEUC_Criticality();
                        LoadTabDataGathering();
                    }
                    return isValidCriticalityInfoTab;
                    break;
                case 3:
                    $('a[href="#euc-form-remediation-tab4"]').css('background', 'green');
                    $('html, body').animate({
                        scrollTop: '0px'
                    }, 800);
                    _showNotification("success", "EUC Remediation was saved successfully.");
                    //validateRequiredDataGathering();
                    break;
                case 4:
                    //validateRequiredRemediation();
                    break;
                default:
                    break;
          }
            //if ($.isFunction($.fn.validate)) {
            //    //var $valid = $("#commentForm").valid();
            //    var $valid = $("#fGeneralInformation").valid();
            //    if (!$valid) {
            //       // $validator.focusInvalid();
            //        return false;
            //    } else {
            //        $('#pills').find('.form-wizard').children('li').eq(index - 1).addClass('complete');
            //        $('#pills').find('.form-wizard').children('li').eq(index - 1).find('.step').html('<i class="fa fa-check"></i>');
            //    }
            //}
            
        },
        onPrevious: function (tab, navigation, index) {
            console.log('onPrevious' + ' - index: ' + index + ' - tab: ' + tab);
        },
        onLast: function (tab, navigation, index) {
            console.log('onLast' + ' - index: ' + index + ' - tab: ' + tab);
        },
        onTabClick: function (tab, navigation, index) {
            console.log('onTabClick' + ' - index: ' + index + ' - tab: ' + tab);
            $('#EUCID').attr('isLoaded', '0');
            //console.log('onTabClick');
            //console.log('onNext' + ' - index: ' + index + ' - tab: ' + tab);
            switch (index) {
                case 0:
                    
                    var isValidGeneralInfoTab = validateRequiredGeneralInfo();
                    if (isValidGeneralInfoTab) {
                        //save                        
                    } 
                    return isValidGeneralInfoTab;
                    break;
                case 1:
                    LoadTabGeneralInformation();
                    validateRequiredCriticality();
                    break;
                case 2:
                    validateRequiredDataGathering();
                    break;
                case 3:
                    validateRequiredRemediation();
                    break;
                default:
                    break;
            }
            //alert('on tab click disabled');
        },
        onTabShow: function (tab, navigation, index) {
            console.log('onTabShow' + ' index: ' + index);
            localStorage.setItem('tabindex', index);
            //switch (index) {
            //    case 0:
            //        LoadTabGeneralInformation();
            //        break;
            //    case 1:
            //        LoadTabCriticality();
            //        break;
            //    case 2:
            //        LoadTabDataGathering();
            //        break;
            //    case 3:
            //        LoadTabRemediation();
            //        break;
            //    default:
            //        break;
            //}

            var $total = navigation.find('li').length;
            var $current = index + 1;
            var $percent = ($current / $total) * 100;
            $('#pills .progress-bar').css({
                width: $percent + '%'
            });
        }
    });

    $('#pills .finish').click(function () {
        alert('Finished!, Starting over!');
        $('#pills').find("a[href*='tab1']").trigger('click');
    });

    //DataGathering events
    //$('#btnAddProductRelated').click(function () {
    //    var getselectedrowindexes = $('#divProductRelated').jqxGrid('getselectedrowindexes');
    //    if (getselectedrowindexes.length > 0){
    //        // returns the selected row's data.
    //        for (var i = 0; i < getselectedrowindexes.length; i++) {
    //            var selectedRowData = $('#divProductRelated').jqxGrid('getrowdata', getselectedrowindexes[i]);
    //            console.log(selectedRowData);

    //        }

    //    }
    //});
    //

    //$(".sidebar_toggle").click();

    _GLOBAL_SETTINGS.tooltipsPopovers();

    _hideMenu();

    var index = getTabIndex();
    if (index) {
        switch (index) {
            case '2':
                //LoadTabGeneralInformation();
                LoadTabCriticality();

                break;
            case '3':
                // LoadTabGeneralInformation();
                LoadTabCriticality();
                LoadTabDataGathering();

                break;
            case '4':
                //LoadTabGeneralInformation();
                LoadTabCriticality();
                LoadTabDataGathering();
                LoadTabRemediation();

                break;
            default:
                break;
        }
    }

    var eucid = getEUCID();
    if (eucid != 0) {
        _showLoadingFullPage({ msg: "Getting EUC information..." });
    }
        
    //On Change Functions
});

$(document).ajaxStop(function () {

    var eucid = getEUCID();
    if (eucid) {
        var index = getTabIndex();
        console.log('index: ' + index + ',EUCID: ' + eucid + ',isLoaded: ' + $('#EUCID').attr('isLoaded') + 'OUT IF');
        if (index && ($('#EUCID').attr('isLoaded') == '0')) {
            console.log('index: ' + index + ',EUCID: ' + eucid + ',isLoaded: ' + $('#EUCID').attr('isLoaded') + 'IN IF');
            switch (index) {
                case '-1':
                    loadEUCGeneralInfo(eucid);
                    break;
                case '1':
                    loadEUCGeneralInfo(eucid);
                    break;

                case '2':
                    loadEUCGeneralInfo(eucid);
                    LoadTabCriticality();

                    break;
                case '3':
                    LoadTabGeneralInformation();
                    LoadTabCriticality();
                    LoadTabDataGathering();

                    break;
                case '4':
                    LoadTabGeneralInformation();
                    LoadTabCriticality();
                    LoadTabDataGathering();
                    LoadTabRemediation();

                    break;
            }

            $('#EUCID').attr('isLoaded', '1');
        }
    }
    _hideLoadingFullPage();
    console.log('AJAX DONE!');
});

function getEUCID() {
    var eucid = 0;
    if ($('#EUCID').text()) {
        localStorage.setItem('EUCID', $('#EUCID').text());
    }

    if (localStorage.getItem('EUCID')) {
        eucid = localStorage.getItem('EUCID');
    }

    return eucid;
};

function getTabIndex() {
    var tabindex = -1;
    if ($('#EUCID').attr('tabindex')) {
        localStorage.setItem('tabindex', $('#EUCID').attr('tabindex'));
    }

    if (localStorage.getItem('tabindex')) {
        tabindex = localStorage.getItem('tabindex');
    }

    return tabindex;
};

function loadEUCGeneralInfo(ID) {
    //------------------------------------------------------------
    //_callProcedure({
    //    loadingMsgType: "fullLoading",
    //    loadingMsg: "Getting competencies information...",
    //    name: "[dbo].[FROU_spAssessmentGetInfoCompetency]",
    //    params: [
    //        { "Name": "@SOEIDEmployee", "Value": soeidEmployee },
    //        { "Name": "@IDProcessRole", "Value": idSelProcessRole },
    //        { "Name": "@IDJobLevel", "Value": idSelJobLevel },
    //        { "Name": "@IDCompetency", "Value": idSelComptency },
    //        { "Name": "@FilterTraining", "Value": filterTrainings }
    //    ],
    //    success: {
    //        fn: function (responseList) { }
    //    }
    //});
    //------------------------------------------------------------
    console.log(ID);
    _callProcedure({
        loadingMsgType: "fullLoading",
            loadingMsg: "Getting EUC information...",
            name: "[dbo].[spMaintenance_GetEUCGeneralInfo]",
            params: [
                { "Name": "@p_ID", "Value": ID }
            ],
            success: {
                fn: function (responseList) {
                    if (responseList.length > 0) {
                        var objEUC = responseList[0];
                        $('#ddlStatus').val(objEUC['environmentStatus']);
                        $('#ddlCreationReason').val(objEUC['creationReason']);
                        $('#ddlNumberUser').val(objEUC['numbersOfUsers']);
                        $('#ddlFileType').val(objEUC['fileType']);
                        $('#ddlSourceUse').val(objEUC['sourceOfUse']);
                        $('#ddlBusinessTax').val(objEUC['businessTaxonomy']);
                        $('textarea[name=formfield_txtEUCDescription]').val(objEUC['description']);
                        $('#ddlMainPurpose').val(objEUC['mainPurpose']);
                        $('#txtEUCName').val(objEUC['Name']);
                        $('#ddlManager').val(objEUC['manager']);
                        loadDropDownGOC(objEUC['goc'], 1, objEUC['manager']);//(selectedStatusID, OrgID, ManagerSOEID)
                        $('#ddlGOC').val(objEUC['goc']);
                        loadDropDownDelegate(objEUC['delegate'], 1, objEUC['manager'], objEUC['goc']);
                        $('#ddlRegion').val(objEUC['region']);
                        loadDropDownCountry(objEUC['contry'], 1, objEUC['region']);
                        loadDropDownCenter(objEUC['center'], 1, objEUC['contry']);
                        loadtextBoxBISOName(1, objEUC['center']);
                        $("#ddlCountry").removeAttr('disabled');
                        $("#ddlCenter").removeAttr('disabled');
                        $("input[type=radio][name=EUCDefinition][value=Yes]").iCheck('check');
                        $('#ddlFrecuencyOfUse').val(objEUC['frecuencyOfUse']);
                        $("input[type=radio][name=GoldCopymoved][value=No]").iCheck('check');
                        $("input[type=radio][name=EUCUserManual][value=No]").iCheck('check');
                        $("#FilePath").val(objEUC['productionPath']);
                        //------------------ Load Documentation Controls --------------------------
                        _callProcedure({
                            loadingMsgType: "fullLoading",
                            loadingMsg: "Getting EUC information...",
                            name: "[dbo].[spMaintenance_GetEUCDocumentationInfo]",
                            params: [
                                { "Name": "@p_ID", "Value": ID }
                            ],
                            success: {
                                fn: function (responseList) {
                                    if (responseList.length > 0) {
                                        var objEUC = responseList[0];
                                        $('#dateLastReview').val(_formatDate(new Date(objEUC['lastReviewDate']), 'MM/dd/yyyy'));
                                            //.val(objEUC['lastReviewDate']);
                                        $('#ddlDocumentationReviewer').val(objEUC['reviewedBy']);
                                        if (objEUC['documentationPathBlueworks'] == 'NOT IN BLUEWORKS') {
                                            $('#chkNotInBlueWorks').iCheck('check');
                                        } else {
                                            $('#txtBlueworks').val(objEUC['documentationPathBlueworks']);
                                        }
                                    }
                                    
                                }
                            }
                        });
                        //-------------------------------------------------------------------------
                        //------------------ Load Elimination Strategy --------------------------
                        _callProcedure({
                            loadingMsgType: "fullLoading",
                            loadingMsg: "Getting EUC information...",
                            name: "[dbo].[spMaintenance_GetEUCEliminationStrategy]",
                            params: [
                                { "Name": "@p_ID", "Value": ID }
                            ],
                            success: {
                                fn: function (responseList) {
                                    if (responseList.length > 0) {
                                        var objEUC = responseList[0];
                                       
                                        if (objEUC['dataQualityIssue'] == 'True') {
                                            
                                            $("input[type=radio][name=chkDataQualityIssue][value=Yes]").iCheck('check');
                                            //IMRID
                                            if (objEUC['IMRID'] == 'NO IMRID') {                                                
                                                $('#chkNoIMR').iCheck('check');
                                            } else {                                                
                                                $('#txtDataQualityIssueID').val(objEUC['IMRID']);
                                            }

                                        } else {                                            
                                            $("input[type=radio][name=chkDataQualityIssue][value=No]").iCheck('check');
                                        }

                                        if (objEUC['missingFunctionality'] == 'True') {
                                            $("input[type=radio][name=chkCoreSystem][value=Yes]").iCheck('check');
                                            if (objEUC['workOrderID'] == 'NO WORK ORDER ID') {
                                                $('#chkNoWorkOrder').iCheck('check');
                                            } else {
                                                $('#txtWorkOrderID').val(objEUC['workOrderID']);
                                            }
                                        } else {
                                            $("input[type=radio][name=chkCoreSystem][value=No]").iCheck('check');
                                        }

                                        if (objEUC['missingFunctionality'] == 'False' && objEUC['dataQualityIssue'] == 'False') {
                                            $('#ddlEUCExists').val(objEUC['whyThisEUCExist']);
                                        }

                                        $('#txtWhyEUCExist').val(objEUC['explainWhyThisEUCExist']);
                                        console.log(objEUC['isTherePlanToEliminateThisEUC'], false);
                                        if (objEUC['isTherePlanToEliminateThisEUC']=='True') {
                                            $("input[type=radio][name=chkEliminatePlan][value=Yes]").iCheck('check');
                                            $('#dateEliminationPlan').val(_formatDate(new Date(objEUC['planCreatedDate']), 'MM/dd/yyyy'));
                                            $('#dateRetirementPlan').val(_formatDate(new Date(objEUC['retirementScheduledDate']), 'MM/dd/yyyy'));
                                        } else {
                                            $("input[type=radio][name=chkEliminatePlan][value=No]").iCheck('check');
                                        }

                                        if (objEUC['thisEUCCanNotBeEliminated'] == 'True') {
                                            $('#chkEUCCantEliminated').prop('checked', true);
                                            $('#txtReasonEUCCantEliminated').val(objEUC['reasonWhyEUCNotBeEliminated']);
                                        } else {
                                            $('#datePlanWillBeCreated').val(_formatDate(new Date(objEUC['planWillBeCreatedDate']), 'MM/dd/yyyy'));
                                        }
                                        
                                        
                                        //$('#dateLastReview').val(_formatDate(new Date(objEUC['lastReviewDate']), 'MM/dd/yyyy'));
                                        ////.val(objEUC['lastReviewDate']);
                                        //$('#ddlDocumentationReviewer').val(objEUC['reviewedBy']);
                                        //if (objEUC['documentationPathBlueworks'] == 'NOT IN BLUEWORKS') {
                                        //    $('#chkNotInBlueWorks').iCheck('check');
                                        //} else {
                                        //    $('#txtBlueworks').val(objEUC['documentationPathBlueworks']);
                                        //}
                                    }

                                }
                            }
                        });
                        //-------------------------------------------------------------------------
                    } else {
                        _showNotification("error", "No data found for EUC ID " + ID);
                    }
                }


            }
    });
};

function AutoFill_GeneralInfo() {
    $('#ddlStatus').val(2);
    $('#ddlCreationReason').val(3);
    $('#ddlNumberUser').val(1);
    $('#ddlFileType').val(1);
    $('#ddlSourceUse').val(1);
    $('#ddlBusinessTax').val(19);
    $('textarea[name=formfield_txtEUCDescription]').val('EUC test Description');
    $('#ddlMainPurpose').val(9);
    $('#txtEUCName').val('EUC test Name');
    $('#ddlManager').val('GE44563');
    loadDropDownGOC(3168002070, 1, 'GE44563');
    $('#ddlGOC').val('3168002070');
    loadDropDownDelegate('LB24042', 1, 'GE44563', '3168002070');
    $('#ddlRegion').val(3);
    loadDropDownCountry(14, 1, 3);
    loadDropDownCenter(2, 1, 14);
    loadtextBoxBISOName(1, 2);
    $("#ddlCountry").removeAttr('disabled');
    $("#ddlCenter").removeAttr('disabled');
    $("input[type=radio][name=EUCDefinition][value=Yes]").iCheck('check');
    $('#ddlFrecuencyOfUse').val(5);
    $("input[type=radio][name=GoldCopymoved][value=No]").iCheck('check');
    $("input[type=radio][name=EUCUserManual][value=No]").iCheck('check');
    $('#chkNotInBlueWorks').iCheck('check');
    $("input[type=radio][name=chkDataQualityIssue][value=No]").iCheck('check');
    $("input[type=radio][name=chkCoreSystem][value=No]").iCheck('check');
    $('#ddlEUCExists').val('8');
    $('#txtWhyEUCExist').val('test test test test test test test test test test ');
    $("input[type=radio][name=chkEliminatePlan][value=No]").iCheck('check');
    $('#chkEUCCantEliminated').prop('checked', true);
    $('#divReasonEUCCantEliminated').show();
    $('#divPlanWillBeCreated').hide();
    $('#txtReasonEUCCantEliminated').val('test test test test test test test test test test ');
    $("#dateLastReview").val(_formatDate(new Date($.now()), 'MM/dd/yyyy'));
    $("#FilePath").val('testFile.xlsx');
};

function AutoFill_CriticalityInfo() {
    $("input[type=radio][name=EUCEUCMCA][value=Yes]").iCheck('check');
    $('#ddlMCA').val('4');
    $("input[type=radio][name=EUCControlProcess][value=Yes]").iCheck('check');
    $("input[type=radio][name=EUCCOBRequired][value=No]").iCheck('check');
    $("input[type=radio][name=EUCEndResult][value=No]").iCheck('check');
    $('#ddlUsedFor').val('1');
    $("input[type=radio][name=EUCSOXRelated][value=No]").iCheck('check');
    $('#ddlEUCMaterialityLevel').val('2');
    $('#ddlEUCDataClasification').val('4');
};

//Validation Function
function settingValidation() {
    $.validator.addMethod("valueNotEquals", function (value, element, arg) {
        if ($(element).attr("aria-required") == "true" && $(element).is(':visible')) {
            return arg != value;
        } else {
            return true;
        }
    }, "Please select a valid option.");

    var errorPlacement = function (label, element) { // render error placement for each input type   
        // console.log(label);
        var parent = $(element).parent();
        
        if (parent.attr("class").indexOf("iradio") !== -1) {
            var parentContainer = parent.parent();
            var errorSpam = $('<span class="error" style="display:block;"></span>');

            errorSpam.append(label);

            parentContainer.find(".iradio-label").last().after(errorSpam);

            
            //.insertAfter(element).append(label)
            //parent = $(element).parent().parent('.form-group');
        } else {
            $('<span class="error" ></span>').insertAfter(element).append(label)
            parent = $(element).parent().parent('.form-group');
        }
        
        parent.removeClass('has-success').addClass('has-error');
    };

    $('#fCriticalityAssessment').validate({
        focusInvalid: false,
        ignore: "",
        rules: {
            EUCEUCMCA: { required: true, valueNotEquals: 0 },
            formfield_ddlMCA: {
                required: {
                    depends: function () {
                        return $("input[type=radio][name=EUCEUCMCA]:checked").val() == 'Yes';
                    }
                }, valueNotEquals: 0
            },
            EUCControlProcess: { required: true, valueNotEquals: 0 },
            EUCCOBRequired: { required: true, valueNotEquals: 0 },
            EUCEndResult: { required: true, valueNotEquals: 0 },
            formfield_ddlUsedFor: { required: true, valueNotEquals: 0 },
            EUCSOXRelated: { required: true, valueNotEquals: 0 },
            formfield_ddlEUCMaterialityLevel: { required: true, valueNotEquals: 0 },
            formfield_ddlEUCDataClasification: { required: true, valueNotEquals: 0 }
        },
        invalidHandler: function (event, validator) {
            //display error alert on form submit    
        },

        errorPlacement: errorPlacement,

        highlight: function (element) { // hightlight error inputs
            var parent = $(element).parent().parent('.form-group');
            parent.removeClass('has-success').addClass('has-error');
        },

        unhighlight: function (element) { // revert the change done by hightlight

        },

        success: function (label, element) {
            var parent = $(element).parent().parent('.form-group');
            parent.removeClass('has-error').addClass('has-success');
        },

        submitHandler: function (form) {

        }
    });

    $('#fGeneralInformation').validate({
        focusInvalid: false,
        ignore: "",
        rules: {
            formfield_ddlStatus: { required: true, valueNotEquals: 0 },
            formfield_ddlCreationReason: { required: true, valueNotEquals: 0 },
            formfield_txtOtherReason: {
                required:
                    {
                        depends: function () {
                            return $("#ddlCreationReason").find('option:selected').text() == 'Other';
                        }
                    }
            },
            formfield_ddlRegion: { required: true, valueNotEquals: 0 },
            formfield_ddlCountry: { required: true, valueNotEquals: 0 },
            formfield_ddlCenter: { required: true, valueNotEquals: 0 },
            formfield_ddlManager: { required: true, valueNotEquals: 0 },
            formfield_ddlGOC: { required: true, valueNotEquals: 0 },
            formfield_ddlDelegate: { required: true, valueNotEquals: 0 },
            formfield_txtEUCName: { required: true, valueNotEquals: 0 },
            formfield_txtEUCDescription: { required: true, valueNotEquals: 0 },
            formfield_ddlMainPurpose: { required: true, valueNotEquals: 0 },
            formfield_ddlSourceUse: { required: true, valueNotEquals: 0 },
            formfield_ddlBusinessTax: { required: true, valueNotEquals: 0 },
            formfield_ddlFileType: { required: true, valueNotEquals: 0 },
            formfield_ddlNumberUser: { required: true, valueNotEquals: 0 },
            //Depends on EUC Definition
            formfield_ddlFrecuencyOfUse: {
                required: {
                    depends: function () {
                        return $("input[type=radio][name=EUCDefinition]:checked").val() == 'Yes';
                    }
                }, valueNotEquals: 0
            },
            GoldCopymoved: {
                required: {
                    depends: function () {
                        return $("input[type=radio][name=EUCDefinition]:checked").val() == 'Yes';
                    }
                }
            },
            EUCUserManual: {
                required: {
                    depends: function () {
                        return $("input[type=radio][name=EUCDefinition]:checked").val() == 'Yes';
                    }
                }
            },
            formfield_FileTest: {
                required: {
                    depends: function () {
                        return $("input[type=radio][name=EUCDefinition]:checked").val() == 'Yes';
                    }
                }
            },
            formfield_dateLastReview: {
                required: {
                    depends: function () {
                        return $("input[type=radio][name=EUCDefinition]:checked").val() == 'Yes';
                    }
                }
            },
            formfield_ddlDocumentationReviewer: {
                required: {
                    depends: function () {
                        return $("input[type=radio][name=EUCDefinition]:checked").val() == 'Yes';
                    }
                }
            },
            formfield_txtBlueworks: {
                required: {
                    depends: function () {
                        return $("input[type=radio][name=EUCDefinition]:checked").val() == 'Yes';
                    }
                }
            },
            chkDataQualityIssue: {
                required: {
                    depends: function () {
                        return $("input[type=radio][name=EUCDefinition]:checked").val() == 'Yes';
                    }
                }
            },
            chkCoreSystem: {
                required: {
                    depends: function () {
                        return $("input[type=radio][name=EUCDefinition]:checked").val() == 'Yes';
                    }
                }
            },
            formfield_txtWhyEUCExist: {
                required: {
                    depends: function () {
                        return $("input[type=radio][name=EUCDefinition]:checked").val() == 'Yes';
                    }
                }
            },
            chkEliminatePlan: {
                required: {
                    depends: function () {
                        return $("input[type=radio][name=EUCDefinition]:checked").val() == 'Yes';
                    }
                }
            },
            formfield_txtDataQualityIssueID: {
                required: {
                    depends: function () {
                        return $("input[type=radio][name=chkDataQualityIssue]:checked").val() == 'Yes';
                    }
                }
            },
            formfield_txtWorkOrderID: {
                required: {
                    depends: function () {
                        return $("input[type=radio][name=chkCoreSystem]:checked").val() == 'Yes';
                    }
                }
            },
            formfield_ddlEUCExists: {
                required: {
                    depends: function () {
                        if (($("input[type=radio][name=chkDataQualityIssue]:checked").val() == 'No') && ($("input[type=radio][name=chkCoreSystem]:checked").val() == 'No')) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }, valueNotEquals: 0
            }

        },

        invalidHandler: function (event, validator) {
            //display error alert on form submit    
        },

        errorPlacement: errorPlacement
        ,

        highlight: function (element) { // hightlight error inputs
            var parent = $(element).parent().parent('.form-group');
            parent.removeClass('has-success').addClass('has-error');
        },

        unhighlight: function (element) { // revert the change done by hightlight

        },

        success: function (label, element) {
            var parent = $(element).parent().parent('.form-group');
            parent.removeClass('has-error').addClass('has-success');
        },

        submitHandler: function (form) {

        }
    });
};
///

//**********************************************************************
//************************* Load Tab Functions ************************* 
function LoadTabGeneralInformation() {
    console.log('Load Maintenance data');
    //----------------
    $("#dateLastReview").datepicker();
    $("#dateEliminationPlan").datepicker();
    $("#datePlanWillBeCreated").datepicker();
    $("#dateRetirementPlan").datepicker();
    //$("#dateLastReview").val(_formatDate(new Date(row.dateTR_ExtensionSentIRS), 'MM/dd/yyyy'));
    //Maintenance FUNCTIONS
    //$("#dateLastReview").jqxDateTimeInput({ template: "summer", width: "100%", height: 30 });
    //$("#dateEliminationPlan").jqxDateTimeInput({ template: "summer", width: "20%", height: 30 });
    //$("#dateRetirementPlan").jqxDateTimeInput({ template: "summer", width: "20%", height: 30 });
    //$("#datePlanWillBeCreated").jqxDateTimeInput({ template: "summer", width: "20%", height: 30 });
    //var date = $("#dateInput").jqxDateTimeInput('getDate');
    //var formattedDate = $.jqx.dataFormat.formatdate(date, 'd');
    $("#ddlCreationReason").change(function () {
        //$("#ddlCreationReason").find('option:selected').text();
        if ($(this).find('option:selected').text() == "Other") {
            $("#divtxtOtherReason").show();
            $("#divtxtOtherReason").val("");
        } else {
            $("#divtxtOtherReason").hide();
        }
    });

    $("#chkNotInBlueWorks").on('ifChanged',function () {
        if ($("input[name=NotInBlueworks]").is(":checked")) {
                    $("#txtBlueworks").val('');
                    $("#txtBlueworks").attr('disabled', 'disabled');
                   // console.log('NotIn');
                } else {
                    $("#txtBlueworks").val('');
                    $("#txtBlueworks").removeAttr('disabled');
                }
    });

    //$("input[name=NotInBlueworks]").change(function () {
    //    console.log('In');
    //    if ($("input[name=NotInBlueworks]").is(":checked")) {
    //        $("#txtBlueworks").val('');
    //        $("#txtBlueworks").attr('disabled', 'disabled');
    //        console.log('NotIn');
    //    } else {
    //        $("#txtBlueworks").val('');
    //        $("#txtBlueworks").removeAttr('disabled');
    //    }
    //});

    $("input[type=radio][name=chkDataQualityIssue]").on('ifChecked', function (event) {
        if ($(this).val() == 'Yes') {
            $('#divDataQualityIssue').show();
        } else {
            $('#divDataQualityIssue').hide();
        }
        checkNO();
    });

    $("input[type=radio][name=chkCoreSystem]").on('ifChecked', function (event) {
        if ($(this).val() == 'Yes') {
            $('#divCoreSystem').show();
        } else {
            $('#divCoreSystem').hide();
        }
        checkNO();
    });

    //EUCDefinition
    $("input[type=radio][name=EUCDefinition]").on('ifChecked', function (event) {
        if ($(this).val() == 'Yes') {
            EUCDefinition_ShowOrHide(true);
        } else {
            EUCDefinition_ShowOrHide(false);
        }
    });

    //chkEliminatePlan
    $("input[type=radio][name=chkEliminatePlan]").on('ifChecked', function (event) {
        if ($(this).val() == 'Yes') {
            $('.divEliminationRetirementPlan').show();
            $('#divEUCCantEliminated').hide();
            $('#divPlanWillBeCreated').hide();
            $('#divReasonEUCCantEliminated').hide();
        } else {
            $('.divEliminationRetirementPlan').hide();
            $('#divPlanWillBeCreated').show();
            $('#divEUCCantEliminated').show();
            $("input[name=EUCCantEliminated]").attr("checked", false);
        }
    });
    //divEUCCantEliminated
    $("input[name=EUCCantEliminated]").change(function () {

        if ($("input[name=EUCCantEliminated]").is(":checked")) {
            $('#divReasonEUCCantEliminated').show();
            $('#divPlanWillBeCreated').hide();
            $('.divEliminationRetirementPlan').hide();

        } else {
            $('#divReasonEUCCantEliminated').hide();
            $('#divPlanWillBeCreated').show();
            $('.divEliminationRetirementPlan').hide();

        }


    });

    //NoIMR
    $("input[name=NoIMR]").on('ifChanged', function () {
        if ($("input[name=NoIMR]").is(":checked")) {
            $("#txtDataQualityIssueID").val('No IMR ID');
            $("#txtDataQualityIssueID").attr('disabled', 'disabled');
        } else {
            $("#txtDataQualityIssueID").val('');
            $("#txtDataQualityIssueID").removeAttr('disabled');
        }
    });

    //NoWorkOrder
    $("input[name=NoWorkOrder]").on('ifChanged', function () {
        if ($("input[name=NoWorkOrder]").is(":checked")) {
            $("#txtWorkOrderID").val('No Work Order ID');
            $("#txtWorkOrderID").attr('disabled', 'disabled');
        } else {
            $("#txtWorkOrderID").val('');
            $("#txtWorkOrderID").removeAttr('disabled');
        }
    });

    //Load DDL: ddlStatus
    loadDropDownStatus(0, 1);//_getUserInfo().OrganizationID);
    //Load DDL: ddlCreationReason
    loadDropDownCreationReason(0, 1);
    //Load DDL: ddlRegion
    loadDropDownRegion(0, 1);
    //Load DDL: ddlCountry
    loadDropDownCountry(0, 1, 0);
    //Load DDL: ddlCenter
    loadDropDownCenter(0, 1, 0);
    //Load DDL: ddlManager
    loadDropDownManager(0, 1);
    //Load DDL: ddlMainPurpose
    loadDropDownMainPurpose(0, 1);
    //Load DDL: ddlSourceUse
    loadDropDownSourceOfUse(0, 1)
    //Load DDL: ddlBusinessTax
    loadDropDownBusinessTax(0, 1)
    //Load DDL: ddlFileType
    loadDropDownFileTypes(0, 1)
    //Load DDL: ddlFrecuencyOfUse
    loadDropDownFrecuencyOfUse(0, 1);
    //Load DDL: ddlNumberUser
    loadDropDownNumberOfUsers(0, 1);
    //Load DDL: ddlEUCExists
    loadDropDownEUCExists(0, 1);
    //OnChange DDL: ddlRegion
    $("#ddlRegion").change(function () {
        var idRegionSelected = $("#ddlRegion").val();
        //Load DDL: ddlCountry
        loadDropDownCountry(0, 1, idRegionSelected);
        //Enable ddlCountry
        $("#ddlCountry").removeAttr('disabled');
    });
    //OnChange DDL: ddlCountry
    $("#ddlCountry").change(function () {
        var idCountrySelected = $("#ddlCountry").val();
        //Load DDL: ddlCenter
        loadDropDownCenter(0, 1, idCountrySelected);
        //Enable ddlCenter
        $("#ddlCenter").removeAttr('disabled');

    });
    //OnChange DDL: ddlCenter
    $("#ddlCenter").change(function () {
        var idCenter = $("#ddlCenter").val();
        //Load textbox: txtBISO
        loadtextBoxBISOName(1, idCenter);
    });
    //OnChange DDL: ddlManager
    $("#ddlManager").change(function () {
        var SOEIDManager = $("#ddlManager").val();
        //Load DDL: ddlGOC
        loadDropDownGOC(0, 1, SOEIDManager);
       // console.log('manager change');

    });
    //OnChange DDL: ddlDelegate
    $("#ddlDelegate").change(function () {
        var SOEIDDelegate = $("#ddlDelegate").val();
        hideDocumentationReviewer(SOEIDDelegate);
    });
    //OnChange DDL: ddlGOC
    $("#ddlGOC").change(function () {
        var SOEIDManager = $("#ddlManager").val();
        var GOCID = $("#ddlGOC").val();
        //Load DDL: ddlDelegate
        loadDropDownDelegate(0, 1, SOEIDManager, GOCID);
    });

    //OnChange File
    $("#FileTest").change(function () {
        var fullname = $("#FileTest")[0].value;
        var last = fullname.lastIndexOf("\\");
        $("#FilePath").val(fullname.substring(last + 1, fullname.length));
        //console.log();
    });

};

function LoadTabCriticality() {
   console.log('Load Criticality data');
    //EUC MCA - EUCEUCMCA
    $('input[type=radio][name=EUCEUCMCA]').on('ifChecked', function (event) {
        if ($(this).val() == 'Yes') {
            $('#divEUCMCA').show();
        } else {
            $('#divEUCMCA').hide();
        }
    });

    //SOX Related YES/NO
    $('input[type=radio][name=EUCSOXRelated]').on('ifChecked', function (event) {
        if ($(this).val() == 'Yes') {
            var dataRecord = $('#divEUCtblCriticalityQuestions').jqxGrid('getrowdata', 0);
            dataRecord.Answer = true;
            //Set new value
            $('#divEUCtblCriticalityQuestions').jqxGrid('updaterow', 0, dataRecord);
        } else {
            var dataRecord = $('#divEUCtblCriticalityQuestions').jqxGrid('getrowdata', 0);
            dataRecord.Answer = false;
            //Set new value
            $('#divEUCtblCriticalityQuestions').jqxGrid('updaterow', 0, dataRecord);
        }
    });

    //Load DDL: ddlMCA
    loadDropDownMCA(0, 1);
    //Load Grid: divEUCtblCriticalityQuestions
    loadCriticalityQuestions(1);
    //Load DDL: ddlUsedFor
    loadDropDownUsedFor(0, 1);
    //Load DDL: ddlEUCMaterialityLevel
    loadDropDownMaterialityLevel(0, 1);
    //Load DDL: ddlEUCDataClasification
    loadDropDownDataClasification(0, 1);
};

function LoadTabDataGathering() {
   console.log('Load Data Gathering data');
    //Load grid Product Related
    loadProductRelated(1);
};

function LoadTabRemediation() {
    console.log('Load Remediation data');
    $("#dateEUCRetirement").jqxDateTimeInput({ template: "summer", width: "100%", height: 30 });
    //Load Grid: divEUCCharacterizes
    loadEUCCharacterizes(1);
    //Load
    loadDropDownddlEUCInPlaceOfThis(0, 1);
    //Load
    loadDropDownEUCAging(0, 1);
    //Load
    loadDropDownEUCDataEntryMethod(0, 1);
    //OnChange DDL: chkEUCInPlaceOfThis
    $("#ddlEUCInPlaceOfThis").change(function () {
        if ($("#ddlEUCInPlaceOfThis option:selected").text() == "Other") {
            $("#EUCInPlaceOfThis").show();
        } else {
            $("#EUCInPlaceOfThis").hide();
        }
    });
    //chkEUCOnlyThisFunction
    $('input[type=radio][name=chkEUCOnlyThisFunction]').on('ifChecked', function (event) {
        if ($(this).val() == 'Yes') {
            $('#divEUCAuditFindingsPastYear').show();
            $('#divEUCBIorMBI').show();
        } else {
            $('#divEUCAuditFindingsPastYear').hide();
            $('#divEUCBIorMBI').hide();
        }
    });
    //
    $('input[type=radio][name=chkEUCRiskAssociated]').on('ifChecked', function (event) {
        if ($(this).val() == 'Yes') {
            $('#divEUCRiskAssociatedExplain').show();
        } else {
            $('#divEUCRiskAssociatedExplain').hide();
        }
    });

};
//************************* Load Tab Functions ************************* 
//**********************************************************************
//************************** Load Tab Methods ************************** 
//LoadTabMethodsGeneralInformation
function checkNO() {
    if ($("input[type=radio][name=chkCoreSystem]:checked").val() == 'No' && $("input[type=radio][name=chkDataQualityIssue]:checked").val() == 'No') {
        $('#divEUCExists').show();
    } else {
        $('#divEUCExists').hide();
    }
}


//Load DDL: ddlStatus
function loadDropDownStatus(selectedStatusID, OrgID) {
    //Set default value  
    _callServer({
        loadingMsg: "Loading EUC Status...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT '0' AS [ID], '-- Select --' AS [Name] UNION SELECT [ID],[Name] FROM [dbo].[fnEUCStatus] (" + OrgID + ")") },
        type: "post",
        success: function (resultList) {
            $("#ddlStatus").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objStatus = resultList[i];
                $("#ddlStatus").append($('<option>', {
                    value: objStatus.ID, text: objStatus.Name, selected:
                        (objStatus.ID == selectedStatusID)
                }));
            }
        }
    });
};

//Load DDL: ddlCreationReason
function loadDropDownCreationReason(selectedStatusID, OrgID) {
    //Set default value  
    _callServer({
        loadingMsg: "Loading EUC Creation Reason...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify("SELECT '0' AS 'ID', '-- Select --' AS 'CreationReasonName' UNION SELECT [ID] AS 'ID' ,[CreationReasonName] AS 'CreationReasonName' "
                                        + " FROM [EUCInventory].[dbo].[tblMaintenance_CreationReason] WHERE [OrgID] = " + OrgID)
        },
        type: "post",
        success: function (resultList) {
            $("#ddlCreationReason").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objCreationReason = resultList[i];
                $("#ddlCreationReason").append($('<option>', {
                    value: objCreationReason.ID, text: objCreationReason.CreationReasonName, selected:
                        (objCreationReason.ID == selectedStatusID)
                }));
            }
        }
    });
};

//Load DDL: ddlRegion
function loadDropDownRegion(selectedStatusID, OrgID) {
    _callServer({
        loadingMsg: "Loading EUC Region...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify("SELECT 0 AS 'ID','-- Select --' AS 'RegionName' " +
                                       " UNION SELECT [ID] AS 'ID'  ,[dsc_region] AS 'RegionName' " +
                                       " FROM [EUCInventory].[dbo].[tblMaintenance_Region] WHERE [OrgID] =" + OrgID)
        },
        type: "post",
        success: function (resultList) {
            $("#ddlRegion").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objRegion = resultList[i];
                $("#ddlRegion").append($('<option>', {
                    value: objRegion.ID, text: objRegion.RegionName, selected:
                        (objRegion.ID == selectedStatusID)
                }));
            }


        }
    });
};

//Load DDL: ddlCountry
function loadDropDownCountry(selectedStatusID, OrgID, RegionID) {
    var vQuery;
    if (RegionID == 0) {
        vQuery = "SELECT 0 AS 'ID','-- Select --' AS 'CountryName' UNION SELECT [ID] AS 'ID',[dsc_Country] AS 'CountryName' " +
                                       " FROM [EUCInventory].[dbo].[tblMaintenance_Country] WHERE [OrgID] =" + OrgID
    } else {
        vQuery = "SELECT 0 AS 'ID','-- Select --' AS 'CountryName' UNION SELECT [ID] AS 'ID',[dsc_Country] AS 'CountryName' " +
                                       " FROM [EUCInventory].[dbo].[tblMaintenance_Country] WHERE [OrgID] =" + OrgID + " AND [IDRegion] =" + RegionID
    }
    _callServer({
        loadingMsg: "Loading EUC Country...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify(vQuery)
        },
        type: "post",
        success: function (resultList) {
            $("#ddlCountry").contents().remove();

            for (var i = 0; i < resultList.length; i++) {
                var objCountry = resultList[i];
                $("#ddlCountry").append($('<option>', {
                    value: objCountry.ID, text: objCountry.CountryName, selected:
                        (objCountry.ID == selectedStatusID)
                }));
            }

        }
    });

};

//Load DDL: ddlCenter
function loadDropDownCenter(selectedStatusID, OrgID, CountryID) {
    var vQuery;
    if (CountryID == 0) {
        vQuery = "SELECT 0 AS 'ID', '-- Select --' AS 'CenterName' UNION SELECT [ID] AS 'ID' ,[CenterName] AS 'CenterName' " +
                 " FROM [EUCInventory].[dbo].[tblMaintenance_Center] WHERE [OrgID] =" + OrgID
    } else {
        vQuery = "SELECT 0 AS 'ID', '-- Select --' AS 'CenterName' UNION SELECT [ID] AS 'ID' ,[CenterName] AS 'CenterName' " +
                 " FROM [EUCInventory].[dbo].[tblMaintenance_Center] WHERE [OrgID] =" + OrgID + " AND [CountryID] =" + CountryID
    }
    _callServer({
        loadingMsg: "Loading EUC Center...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify(vQuery)
        },
        type: "post",
        success: function (resultList) {
            $("#ddlCenter").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objCenter = resultList[i];
                $("#ddlCenter").append($('<option>', {
                    value: objCenter.ID, text: objCenter.CenterName, selected:
                        (objCenter.ID == selectedStatusID)
                }));
            }
        }
    });
};

//Load DDL: ddlManager
function loadDropDownManager(selectedStatusID, OrgID) {
    var vQuery;
    vQuery = "SELECT '0' AS 'ManagerSOEID' , '-- Select --' AS 'ManagerName' UNION SELECT [ManagerSOEID],[ManagerName] FROM [dbo].[fnMaintenanceGetManagers] (" + OrgID + ") ORDER BY [ManagerName]";

    _callServer({
        loadingMsg: "Loading EUC Manager...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify(vQuery)
        },
        type: "post",
        success: function (resultList) {
            $("#ddlManager").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objManager = resultList[i];
                $("#ddlManager").append($('<option>', {
                    value: objManager.ManagerSOEID, text: objManager.ManagerName, selected:
                        (objManager.ManagerSOEID == selectedStatusID)
                }));
            }
        }
    });
};

//Load DDL: ddlGOC
function loadDropDownGOC(selectedStatusID, OrgID, ManagerSOEID) {
    var vQuery;
    vQuery = "SELECT '-- Select --' AS 'GOCName','0' AS 'GOCID' UNION SELECT [GOCName],[GOCID] FROM [dbo].[fnMaintenanceGetGOCs] ('" + ManagerSOEID + "'," + OrgID + ")";

    _callServer({
        loadingMsg: "Loading EUC GOCs...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify(vQuery)
        },
        type: "post",
        success: function (resultList) {
            $("#ddlGOC").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objGOC = resultList[i];
                $("#ddlGOC").append($('<option>', {
                    value: objGOC.GOCID, text: objGOC.GOCName, selected:
                        (objGOC.GOCID == selectedStatusID)
                }));
            }
            $("#ddlGOC").removeAttr('disabled');
        }
    });
};

//Load DDL: ddlDelegate & ddlDocumentationReviewer
function loadDropDownDelegate(selectedStatusID, OrgID, ManagerSOEID, GOCID) {
    var vQuery;
    vQuery = "SELECT '-- Select --' AS 'EmployeeName' , '0' AS 'EmployeeSOEID' UNION SELECT [EmployeeName],[EmployeeSOEID] FROM [dbo].[fnMaintenanceGetEmployees] ('" + ManagerSOEID + "','" + GOCID + "'," + OrgID + ")";
    _callServer({
        loadingMsg: "Loading EUC Delegates...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify(vQuery)
        },
        type: "post",
        success: function (resultList) {
            $("#ddlDelegate").contents().remove();
            $("#").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objDelegate = resultList[i];
                $("#ddlDelegate").append($('<option>', {
                    value: objDelegate.EmployeeSOEID, text: objDelegate.EmployeeName, selected:
                        (objDelegate.EmployeeSOEID == selectedStatusID)
                }));
                $("#ddlDocumentationReviewer").append($('<option>', {
                    value: objDelegate.EmployeeSOEID, text: objDelegate.EmployeeName, selected:
                        (objDelegate.EmployeeSOEID == selectedStatusID)
                }));
            }
            $("#ddlDelegate").removeAttr('disabled');
            $("#ddlDocumentationReviewer").removeAttr('disabled');

        }
    });
};

//Load DDL: ddlMainPurpose
function loadDropDownMainPurpose(selectedStatusID, OrgID) {
    var vQuery;
    vQuery = "SELECT '0' AS 'MainPurposeID' , '-- Select --' AS 'MainPurposeName' UNION SELECT [ID] AS 'MainPurposeID' ,[MainPurposeName] AS 'MainPurposeName' FROM [EUCInventory].[dbo].[tblMaintenance_MainPurpose] WHERE [OrgID] = " + OrgID;

    _callServer({
        loadingMsg: "Loading EUC Main Purpose...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify(vQuery)
        },
        type: "post",
        success: function (resultList) {
            $("#ddlMainPurpose").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objMainPurpose = resultList[i];
                $("#ddlMainPurpose").append($('<option>', {
                    value: objMainPurpose.MainPurposeID, text: objMainPurpose.MainPurposeName, selected:
                        (objMainPurpose.MainPurposeID == selectedStatusID)
                }));
            }
        }
    });
};

//Load DDL: ddlSourceUse
function loadDropDownSourceOfUse(selectedStatusID, OrgID) {
    var vQuery;
    vQuery = "SELECT 0 AS 'SourceOfUseID', '-- Select --' AS 'SourceOfUseName' UNION SELECT [ID] AS 'SourceOfUseID',[EUC_Source] AS 'SourceOfUseName' FROM [EUCInventory].[dbo].[tblMaintenance_SourceOfUse] WHERE [OrgID] =" + OrgID;

    _callServer({
        loadingMsg: "Loading EUC Source Of Use...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify(vQuery)
        },
        type: "post",
        success: function (resultList) {
            $("#ddlSourceUse").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objSourceOfUse = resultList[i];
                $("#ddlSourceUse").append($('<option>', {
                    value: objSourceOfUse.SourceOfUseID, text: objSourceOfUse.SourceOfUseName, selected:
                        (objSourceOfUse.SourceOfUseID == selectedStatusID)
                }));
            }
        }
    });
};

//Load DDL: ddlBusinessTax
function loadDropDownBusinessTax(selectedStatusID, OrgID) {
    var vQuery;
    vQuery = "SELECT 0 AS 'BusinessTaxonomyID','-- Select --' AS 'BusinessTaxonomyName' UNION SELECT [ID] AS 'BusinessTaxonomyID',[BusinessTaxonomy] AS 'BusinessTaxonomyName' FROM [EUCInventory].[dbo].[tblMaintenance_BusinessTaxonomy] WHERE [OrgID] =" + OrgID;

    _callServer({
        loadingMsg: "Loading EUC Source Of Use...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify(vQuery)
        },
        type: "post",
        success: function (resultList) {
            $("#ddlBusinessTax").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objBusinessTax = resultList[i];
                $("#ddlBusinessTax").append($('<option>', {
                    value: objBusinessTax.BusinessTaxonomyID, text: objBusinessTax.BusinessTaxonomyName, selected:
                        (objBusinessTax.BusinessTaxonomyID == selectedStatusID)
                }));
            }
        }
    });
};

//Load DDL: ddlFileType
function loadDropDownFileTypes(selectedStatusID, OrgID) {
    var vQuery;
    vQuery = "SELECT 0 AS 'FileTypeID','-- Select --' AS 'FileTypeName' UNION SELECT [ID] AS 'FileTypeID',[FileType] AS 'FileTypeName' FROM [EUCInventory].[dbo].[tblMaintenance_FileTypes] WHERE [OrgID] =" + OrgID;

    _callServer({
        loadingMsg: "Loading EUC File Types...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify(vQuery)
        },
        type: "post",
        success: function (resultList) {
            $("#ddlFileType").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objFileType = resultList[i];
                $("#ddlFileType").append($('<option>', {
                    value: objFileType.FileTypeID, text: objFileType.FileTypeName, selected:
                        (objFileType.FileTypeID == selectedStatusID)
                }));
            }
        }
    });
};

//Load txt: txtBISO
function loadtextBoxBISOName(OrgID, CenterID) {
    _callServer({
        loadingMsg: "Loading BISO Name...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify("SELECT [ID] AS 'ID' ,[name] AS 'BISOName' FROM [EUCInventory].[dbo].[tblMaintenance_BISO] " +
                                        " WHERE [orgID] =" + OrgID + " AND [centerID] =" + CenterID)
        },
        type: "post",
        success: function (resultList) {
            if (resultList.length > 0) {
                var objBISO = resultList[0];
                $("#txtBISO").val(objBISO.BISOName);
                $("#txtBISO").attr('idBISO', objBISO.ID);
            }
        }
    });
};
////Load DDL: ddlFileType
function loadDropDownFrecuencyOfUse(selectedStatusID, OrgID) {
    var vQuery;
    vQuery = "SELECT 0 AS 'FrecuencyID','-- Select --' AS 'FrecuencyName' UNION SELECT [ID],[Frecuency] FROM [EUCInventory].[dbo].[tblMaintenance_FrecuencyOfUse] WHERE [OrgID] =" + OrgID;

    _callServer({
        loadingMsg: "Loading EUC File Frecuency Of Use...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify(vQuery)
        },
        type: "post",
        success: function (resultList) {
            $("#ddlFrecuencyOfUse").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objFrecuency = resultList[i];
                $("#ddlFrecuencyOfUse").append($('<option>', {
                    value: objFrecuency.FrecuencyID, text: objFrecuency.FrecuencyName, selected:
                        (objFrecuency.FrecuencyID == selectedStatusID)
                }));
            }
        }
    });
};
////Load DDL: ddlNumberUser
function loadDropDownNumberOfUsers(selectedStatusID, OrgID) {
    var vQuery;
    vQuery = "SELECT 0 AS 'NumberOfUserID','-- Select --' AS 'NumberOfUserName' UNION SELECT [ID] AS 'NumberOfUserID',[NumberOfUser] AS 'NumberOfUserName' FROM [EUCInventory].[dbo].[tblMaintenance_NumberOfUsers] WHERE [OrgID] =" + OrgID;

    _callServer({
        loadingMsg: "Loading EUC File Frecuency Of Use...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify(vQuery)
        },
        type: "post",
        success: function (resultList) {
            $("#ddlNumberUser").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objNumberOfUser = resultList[i];
                $("#ddlNumberUser").append($('<option>', {
                    value: objNumberOfUser.NumberOfUserID, text: objNumberOfUser.NumberOfUserName, selected:
                        (objNumberOfUser.NumberOfUserID == selectedStatusID)
                }));
            }
        }
    });
};
//Show/Hide According EUC Definition
function EUCDefinition_ShowOrHide(status) {
    if (status) {
        $('#divEUCEliminationStrategy').show();
        $('#h4EUCEliminationStrategy').show();
        $('#divFrecuencyOfUse').show();
        $('#divGoldCopyMoved').show();
        $('#divProductionPath').show();
        $('#divUserManual').show();
        $('#divEUCDocumentation').show();
        $('#h4EUCDocumentation').show();

    }
    else {
        $('#divEUCEliminationStrategy').hide();
        $('#h4EUCEliminationStrategy').hide();
        $('#divFrecuencyOfUse').hide();
        $('#divGoldCopyMoved').hide();
        $('#divProductionPath').hide();
        $('#divUserManual').hide();
        $('#divEUCDocumentation').hide();
        $('#h4EUCDocumentation').hide();
    }
};
////Load DDL: ddlEUCExists
function loadDropDownEUCExists(selectedStatusID, OrgID) {
    var vQuery;
    vQuery = "SELECT 0 AS 'EUCExistsID','-- Select --' AS 'EUCExistsName' UNION SELECT [ID] AS 'EUCExistsID',[EUCExistsName] AS 'EUCExistsName' FROM [EUCInventory].[dbo].[tblMaintenance_EUCExists] WHERE [OrgID] =" + OrgID;

    _callServer({
        loadingMsg: "Loading EUC Exists...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify(vQuery)
        },
        type: "post",
        success: function (resultList) {
            $("#ddlEUCExists").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objEUCExists = resultList[i];
                $("#ddlEUCExists").append($('<option>', {
                    value: objEUCExists.EUCExistsID, text: objEUCExists.EUCExistsName, selected:
                        (objEUCExists.EUCExistsID == selectedStatusID)
                }));
            }
        }
    });
};
///
function hideDocumentationReviewer(SOEIDDelegate) {
    $("#ddlDocumentationReviewer>option").show();    
    $("#ddlDocumentationReviewer option[value=" + SOEIDDelegate + "]").hide();
    if (SOEIDDelegate == $("#ddlDocumentationReviewer").val()) {
        $("#ddlDocumentationReviewer").val(0);
    }
    
};

//----------------------------------------------------------------

//LoadTabMethodsCriticality
//Load Grid: divEUCtblCriticalityQuestions
function loadCriticalityQuestions(OrgID) {
    console.log('loadCriticalityQuestions');
    $.jqxGridApi.create({
        showTo: "#divEUCtblCriticalityQuestions",
        options: {
            //for comments or descriptions
            height: "450",
            autoheight: true,
            //autorowheight:true,
            autorowheight: true,
            showfilterrow: false,
            sortable: true,
            editable: true,
            selectionmode: "singlerow"
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set"
        },
        sp: {

            Name: "[dbo].[spCriticality_GetQuestions]",
            Params: [
                { Name: "@pOrgID", Value: OrgID }
            ],
        },
        groups: [],
        columns: [
            { name: 'QuestionNo', text: 'No.', width: '5%', type: 'number', cellsalign: 'center', align: 'center' },
            { name: 'Question', text: 'Question', width: '85%', type: 'text', align: 'center' },
            {
                name: 'Answer', text: 'YES NO', width: '10%', type: 'bool', filtertype: 'boolean', editable: false,
                filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                    var dataRecord = $("#divEUCtblCriticalityQuestions").jqxGrid('getrowdata', rowIndex);
                    var htmlResult = "<div Answer='" + dataRecord.Answer + "' rowIndex='" + rowIndex + "' celluniqueid='"
                        + dataRecord.uniqueid + "_" + this.visibleindex
                        + "' style='margin: auto; margin-top: 1.5px;' class='swichButton'></div>";
                    //console.log('Returning html cell (' + htmlResult + ')');
                    return htmlResult;
                }
            }
        ],
        ready: function () {
            var $jqxGrid = $("#divEUCtblCriticalityQuestions");
            $(".swichButton").each(function () {
                var $btn = $(this);
               // console.log('$(".swichButton").each(function () {');
                initializeButton($btn);
            });

            $("#divEUCtblCriticalityQuestions").on('DOMSubtreeModified', function (e) {
                var $element = $(e.target);
                if ($element.children().hasClass('swichButton')) {
                    var $btn = $element.children();
                    //console.log('$("#divEUCtblCriticalityQuestions").on(DOMSubtreeModified, function (e) {', $btn.parent().html());
                    initializeButton($btn);
                }
            });
            function initializeButton($btn) {
                //Validate if not have mapped events
                if (!jQuery._data($btn[0], "events")) {
                    $btn.children().remove();
                    $btn.jqxSwitchButton({
                        height: 23,
                        width: 81,
                        checked: ($btn.attr("Answer") == "true" ? 1 : 0),
                        onLabel: 'Yes',
                        offLabel: 'No'
                    });
                    $btn.on('checked', function (event) {
                        //Active YES Radio button SOX Related
                        
                        $("input[type=radio][name=EUCSOXRelated][value=No]").iCheck('check');
                        //Get new value
                        var rowIndex = $(this).attr("rowIndex");
                        var dataRecord = $jqxGrid.jqxGrid('getrowdata', rowIndex);
                        dataRecord.Answer = false;
                        //Set new value
                        $jqxGrid.jqxGrid('updaterow', rowIndex, dataRecord);
                        
                    });
                    $btn.on('unchecked', function (event) {
                        //Active NO Radio button SOX Related
                        $("input[type=radio][name=EUCSOXRelated][value=Yes]").iCheck('check');
                        console.log('button unchecked', event);

                        //Get new value
                        var rowIndex = $(this).attr("rowIndex");
                        var dataRecord = $jqxGrid.jqxGrid('getrowdata', rowIndex);
                        dataRecord.Answer = true;
                        //Set false others rows
                        var rows = $.jqxGridApi.getAllRows("#divEUCtblCriticalityQuestions");
                        //Set new value
                        $jqxGrid.jqxGrid('updaterow', rowIndex, dataRecord);
                        
                        
                        
                    });
                } else {
                    //console.log($btn, 'already have mapped events', jQuery._data($btn[0], "events"));
                }
            }
            $jqxGrid.on('cellvaluechanged', function (event) {
                //Allow only one default
                if (event.args.datafield == "Answer" && event.args.newvalue == true) {
                    var rows = $.jqxGridApi.getAllRows("#divEUCtblCriticalityQuestions");
                }
            });

            _hideMenu();
            //$('#divEUCtblCriticalityQuestions').jqxGrid('autoresizecolumns');
        }

    });
}
//Load DDL: ddlMCA
function loadDropDownMCA(selectedStatusID, OrgID) {
    var vQuery;
    vQuery = "SELECT 0 AS 'BusinessProcessID','-- Select --' AS 'BusinessProcessName' UNION SELECT [ID] AS 'BusinessProcessID',[BusinessProcessName] AS 'BusinessProcessName' FROM [EUCInventory].[dbo].[tblCriticality_BusinessProcessName] WHERE [OrgID] =" + OrgID;

    _callServer({
        loadingMsg: "Loading EUC MCA...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify(vQuery)
        },
        type: "post",
        success: function (resultList) {
            $("#ddlMCA").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objMCA = resultList[i];
                $("#ddlMCA").append($('<option>', {
                    value: objMCA.BusinessProcessID, text: objMCA.BusinessProcessName, selected:
                        (objMCA.BusinessProcessID == selectedStatusID)
                }));
            }
        }
    });
};
//---------------------------------------------------------------
//Load DDL: ddlUsedFor
function loadDropDownUsedFor(selectedStatusID, OrgID) {
    var vQuery;
    vQuery = "SELECT 0 AS 'UsedForID','-- Select --' AS 'UsedForName' " +
                " UNION SELECT [ID] AS 'UsedForID' ,[usedFor] AS 'UsedForName' " +
                " FROM [EUCInventory].[dbo].[tblCriticality_UsedFor] WHERE [OrgID] = " + OrgID;
    _callServer({
        loadingMsg: "Loading EUC Used For...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify(vQuery)
        },
        type: "post",
        success: function (resultList) {
            $("#ddlUsedFor").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objUsedFor = resultList[i];
                $("#ddlUsedFor").append($('<option>', {
                    value: objUsedFor.UsedForID, text: objUsedFor.UsedForName, selected:
                        (objUsedFor.UsedForID == selectedStatusID)
                }));
            }
        }
    });
};
//---------------------------------------------------------------
//Load DDL: ddlEUCMaterialityLevel
function loadDropDownMaterialityLevel(selectedStatusID, OrgID) {
    var vQuery;
    vQuery = "SELECT 0 AS 'MaterialityLevelID', '-- Select --' AS 'MaterialityLevelName' " +
            " UNION SELECT [ID] AS 'MaterialityLevelID',[materialityLevel] AS 'MaterialityLevelName' " +
            " FROM [EUCInventory].[dbo].[tblCriticality_MaterialityLevel] WHERE [OrgID] = " + OrgID;
    _callServer({
        loadingMsg: "Loading EUC Materiality Level...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify(vQuery)
        },
        type: "post",
        success: function (resultList) {
            $("#ddlEUCMaterialityLevel").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objMeteriaityLevel = resultList[i];
                $("#ddlEUCMaterialityLevel").append($('<option>', {
                    value: objMeteriaityLevel.MaterialityLevelID, text: objMeteriaityLevel.MaterialityLevelName, selected:
                        (objMeteriaityLevel.MaterialityLevelID == selectedStatusID)
                }));
            }
        }
    });
};
//---------------------------------------------------------------
//Load DDL: ddlEUCDataClasification
function loadDropDownDataClasification(selectedStatusID, OrgID) {
    var vQuery;
    vQuery = "SELECT 0 AS 'DataClasificationID', '-- Select --' AS 'DataClasificationNAME' " + 
            " UNION SELECT [ID],[dataClasification] FROM [EUCInventory].[dbo].[tblCriticality_DataClasification] " +
            " WHERE [OrgID] = " + OrgID;
    _callServer({
        loadingMsg: "Loading EUC Data Clasification...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify(vQuery)
        },
        type: "post",
        success: function (resultList) {
            $("#ddlEUCDataClasification").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objDataClasification = resultList[i];
                $("#ddlEUCDataClasification").append($('<option>', {
                    value: objDataClasification.DataClasificationID, text: objDataClasification.DataClasificationNAME, selected:
                        (objDataClasification.DataClasificationID == selectedStatusID)
                }));
            }
        }
    });
};
//---------------------------------------------------------------

//LoadTabMethodsDataGathering
//Load Grid: divProductRelated
function loadProductRelated(OrgID) {
    $.jqxGridApi.create({
        showTo: "#divProductRelated",
        options: {
            //for comments or descriptions
            height: "450",
            autoheight: false,
            autorowheight: false,
            showfilterrow: true,
            sortable: true,
            editable: true,
            selectionmode: "multiplerows"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        sp: {
            Name: "[dbo].[spCriticality_GetProductRelated]",
            Params: [
                { Name: "@pOrgID", Value: OrgID }
            ],
        },
        groups: [],
        columns: [
            { name: 'ID', type: 'number', hidden: 'true' },
            { name: 'ProductRelatedName', text: 'Product Name', width: '100%', type: 'text', align: 'center' }
        ]
    });
}

//DataGathering FUNCTIONS
function saveProductRelated(OrgID, EUCID, ProductID) {
    _callServer({
        url: '/EUCInventory/saveProductRelated',
        data: { 'OrgID': OrgID, 'EUCID': EUCID, 'ProductID': ProductID },
        type: "post",
        success: function (savingStatus) {
            _showNotification("success", "Data was saved successfully." + savingStatus);
        }
    });
    //------------------------------------------------------------
    //_callProcedure({
    //    loadingMsgType: "fullLoading",
    //    loadingMsg: "Getting competencies information...",
    //    name: "[dbo].[FROU_spAssessmentGetInfoCompetency]",
    //    params: [
    //        { "Name": "@SOEIDEmployee", "Value": soeidEmployee },
    //        { "Name": "@IDProcessRole", "Value": idSelProcessRole },
    //        { "Name": "@IDJobLevel", "Value": idSelJobLevel },
    //        { "Name": "@IDCompetency", "Value": idSelComptency },
    //        { "Name": "@FilterTraining", "Value": filterTrainings }
    //    ],
    //    success: {
    //        fn: function (responseList) { }
    //    }
    //});
    //------------------------------------------------------------
};
//---------------------------------------------------------------

//LoadTabMethodsRemediation
//lOAD Grid: divEUCCharacterizes
function loadEUCCharacterizes(OrgID) {
    $.jqxGridApi.create({
        showTo: "#divEUCCharacterizes",
        options: {
            //for comments or descriptions
            height: "450",
            autoheight: false,
            autorowheight: false,
            showfilterrow: false,
            sortable: true,
            editable: true,
            selectionmode: "singlerow"
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set"
        },
        sp: {

            Name: "[dbo].[spRemediation_GetEUCCharacterizes]",
            Params: [
                { Name: "@pOrgID", Value: OrgID }
            ],
        },
        groups: [],
        columns: [

            { name: 'EUCCharacterize', text: 'Characterize', width: '90%', type: 'text', align: 'center' },
             {
                 name: 'Answer', text: 'Apply', width: '10%', type: 'bool', filtertype: 'boolean', editable: false,
                 filterable: false, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                     var dataRecord = $("#divEUCCharacterizes").jqxGrid('getrowdata', rowIndex);
                     var htmlResult = "<div Answer='" + dataRecord.Answer + "' rowIndex='" + rowIndex + "' celluniqueid='"
                         + dataRecord.uniqueid + "_" + this.visibleindex
                         + "' style='margin: auto; margin-top: 1.5px;' class='swichButton'></div>";
                    // console.log('Returning html cell (' + htmlResult + ')');
                     return htmlResult;
                 }
             }

        ],
        ready: function () {
            var $jqxGrid = $("#divEUCCharacterizes");
            $(".swichButton").each(function () {
                var $btn = $(this);
                //console.log('$(".swichButton").each(function () {');
                initializeButton($btn);
            });

            $("#divEUCCharacterizes").on('DOMSubtreeModified', function (e) {
                var $element = $(e.target);
                if ($element.children().hasClass('swichButton')) {
                    var $btn = $element.children();
                    //console.log('$("#divEUCCharacterizes").on(DOMSubtreeModified, function (e) {', $btn.parent().html());
                    initializeButton($btn);
                }
            });

            function initializeButton($btn) {
                //Validate if not have mapped events
                if (!jQuery._data($btn[0], "events")) {
                    $btn.children().remove();
                    $btn.jqxSwitchButton({
                        height: 23,
                        width: 81,
                        checked: ($btn.attr("Answer") == "true" ? 1 : 0),
                        onLabel: 'Yes',
                        offLabel: 'No'
                    });

                    $btn.on('checked', function (event) {
                       // console.log('button checked', event);

                        //Get new value
                        var rowIndex = $(this).attr("rowIndex");
                        var dataRecord = $jqxGrid.jqxGrid('getrowdata', rowIndex);
                        dataRecord.Answer = false;

                        //Set new value
                        $jqxGrid.jqxGrid('updaterow', rowIndex, dataRecord);

                        //Update cell cache on checked
                        //if ($jqxGrid.jqxGrid('_cellscache')[$btn.attr("celluniqueid")]) {
                        //    $jqxGrid.jqxGrid('_cellscache')[$btn.attr("celluniqueid")].element = $btn.parent().html();
                        //}
                    });
                    $btn.on('unchecked', function (event) {
                       // console.log('button unchecked', event);

                        //Get new value
                        var rowIndex = $(this).attr("rowIndex");
                        var dataRecord = $jqxGrid.jqxGrid('getrowdata', rowIndex);
                        dataRecord.Answer = true;

                        //Set false others rows
                        var rows = $.jqxGridApi.getAllRows("#divEUCCharacterizes");
                        //Comment 01-11-2017
                        //for (var i = 0; i < rows.length; i++) {
                        //    if (rows[i].uid != rowIndex) {
                        //        rows[i].Answer = false;
                        //        //$jqxGrid.jqxGrid('updaterow', rows[i].uid, rows[i]);
                        //    }
                        //}

                        //Set row as selected
                        //$jqxGrid.jqxGrid('selectrow', rowIndex);

                        //Set new value
                        $jqxGrid.jqxGrid('updaterow', rowIndex, dataRecord);

                        //if ($jqxGrid.jqxGrid('_cellscache')[$btn.attr("celluniqueid")]) {
                        //    $jqxGrid.jqxGrid('_cellscache')[$btn.attr("celluniqueid")].element = $btn.parent().html();
                        //}
                    });
                } else {
                    //console.log($btn, 'already have mapped events', jQuery._data($btn[0], "events"));
                }
            }

            $jqxGrid.on('cellvaluechanged', function (event) {
                //Allow only one default
                if (event.args.datafield == "Answer" && event.args.newvalue == true) {
                    var rows = $.jqxGridApi.getAllRows("#divEUCCharacterizes");
                }
            });

            _hideMenu();
        }

    });
}

//Load DDL: ddlEUCInPlaceOfThis
function loadDropDownddlEUCInPlaceOfThis(selectedStatusID, OrgID) {
    var vQuery;
    vQuery = "SELECT 0 AS 'ReasonID','-- Select --' AS 'ReasonName' UNION SELECT [ID],[reason] FROM [EUCInventory].[dbo].[tblRemediation_ExistingAppNotUseReason] WHERE [OrgID] =" + OrgID;

    _callServer({
        loadingMsg: "Loading EUC In Reason...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify(vQuery)
        },
        type: "post",
        success: function (resultList) {
            $("#ddlEUCInPlaceOfThis").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objReasonOf = resultList[i];
                $("#ddlEUCInPlaceOfThis").append($('<option>', {
                    value: objReasonOf.ReasonID, text: objReasonOf.ReasonName, selected:
                        (objReasonOf.ReasonID == selectedStatusID)
                }));
            }
        }
    });
};

//Load DDL: ddlEUCAging
function loadDropDownEUCAging(selectedStatusID, OrgID) {
    var vQuery;
    vQuery = "SELECT 0 AS 'agingID','-- Select --' AS 'agingName' UNION SELECT [ID] AS 'agingID',[Aging] AS 'agingName' FROM [EUCInventory].[dbo].[tblRemediation_EUCAging] WHERE [OrgID] =" + OrgID;

    _callServer({
        loadingMsg: "Loading EUC Aging...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify(vQuery)
        },
        type: "post",
        success: function (resultList) {
            $("#ddlEUCAging").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objAging = resultList[i];
                $("#ddlEUCAging").append($('<option>', {
                    value: objAging.agingID, text: objAging.agingName, selected:
                        (objAging.agingID == selectedStatusID)
                }));
            }
        }
    });
};

//Load DDL: ddlEUCDataEntryMethod
function loadDropDownEUCDataEntryMethod(selectedStatusID, OrgID) {
    var vQuery;
    vQuery = "SELECT 0 AS 'entryMethodID','-- Select --' AS 'entryMethod' UNION SELECT [ID] AS 'entryMethodID',[entryMethod] AS 'entryMethod'" +
                " FROM [EUCInventory].[dbo].[tblRemediation_DataEntryMethod] WHERE [OrgID] =" + OrgID;
    _callServer({
        loadingMsg: "Loading EUC Data Entry Method...",
        url: '/Ajax/ExecQuery',
        data: {
            'pjsonSql': JSON.stringify(vQuery)
        },
        type: "post",
        success: function (resultList) {
            $("#ddlEUCDataEntryMethod").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objDataEntry = resultList[i];
                $("#ddlEUCDataEntryMethod").append($('<option>', {
                    value: objDataEntry.entryMethodID, text: objDataEntry.entryMethod, selected:
                        (objDataEntry.entryMethodID == selectedStatusID)
                }));
            }
        }
    });
};
//---------------------------------------------------------------
//************************** Load Tab Methods ************************** 
//Required Fields Validator Function
function validateRequiredGeneralInfo() {
    var $valid = $("#fGeneralInformation").valid();
    if ($valid) {
        LoadTabCriticality();
        return true;
    } else {
        return false;
    }
    //console.log('Valid Status: ' + $valid);
    //if ($.isFunction($.fn.validate)) {
    //    //var $valid = $("#commentForm").valid();
    //    var $valid = $("#fGeneralInformation").valid();
    //    if (!$valid) {
    //        $validator.focusInvalid();
    //        return false;
    //    } else {
    //        $('#pills').find('.form-wizard').children('li').eq(index - 1).addClass('complete');
    //        $('#pills').find('.form-wizard').children('li').eq(index - 1).find('.step').html('<i class="fa fa-check"></i>');
    //    }
    //}
};

function validateRequiredCriticality() {
    var $valid = $("#fCriticalityAssessment").valid();
    if ($valid) {
        LoadTabRemediation();
        return true;
    } else {
        return false;
    }
};

//---------------------------------------------------------------//
//************************ SAVE Functions ************************
function savetblEUC() {
    //--------------- General Info -------------------------------
    var v_environmentStarus = $('#ddlStatus').val();
    var v_creationReason = $('#ddlCreationReason').val();
    var v_otherCreationReason = $('#txtOtherReason').val();
    var v_statusDetails = $('#txtEUCStatusDetails').val();
    var v_Region = $('#ddlRegion').val();
    var v_Country = $('#ddlCountry').val();
    var v_Center = $('#ddlCenter').val();
    var v_Manager = $('#ddlManager').val();
    var v_Delegate = $('#ddlDelegate').val();
    var v_GOC = $('#ddlGOC').val();
    var v_OtherStakeHolder = $('#txtOtherStakeHolder').val();
    var v_TechContactSOEID = $('#txtTechContactSOEID').val();
    var v_Name = $('#txtEUCName').val();
    var v_MainPurpose = $('#ddlMainPurpose').val();
    var v_Description = $('#txtEUCDescription').val();
    var v_BusinessTaxonomy = $('#ddlBusinessTax').val();
    var v_SourceOfUse = $('#ddlSourceUse').val();
    var v_FileType = $('#ddlFileType').val();
    var v_NumberOfUsers = $('#ddlNumberUser').val();
    var v_FrecuencyOfUse = $('#ddlFrecuencyOfUse').val();
    var v_GoldCopyMoved = (($('input[name=GoldCopymoved]:checked', '#fGeneralInformation').val() == 'Yes') ? true : false);//$('#').val();
    //var v_ProductionPath = $('#FileTest').val();
    var v_ProductionPath = $("#FilePath").val();
    var v_UserManual = (($('input[name=EUCUserManual]:checked', '#fGeneralInformation').val() == 'Yes') ? true : false);//$('#').val();
    var v_OrgID = 1;//$('#').val();
    //------------------- Documentation Info ----------------------------------
    //-----------------------------------------------------------------------//
    var v_ReviewDate = _formatDate(new Date($("#dateLastReview").val()), 'MM/dd/yyyy'); //$('#dateLastReview').val();
    var v_ReviewedBy = $('#ddlDocumentationReviewer').val();
    var v_DocumentationPathBlueworks;
    if ($('#chkNotInBlueWorks').prop('checked')) {
        v_DocumentationPathBlueworks = 'NOT IN BLUEWORKS';
    } else {
        v_DocumentationPathBlueworks = $('#txtBlueworks').val();
    }
    //--------------- Elimination Strategy Info -------------------------------
    var v_dataQualityIssue = (($('input[name=chkDataQualityIssue]:checked', '#fGeneralInformation').val() == 'Yes') ? true : false);
    var v_IMRID;
    if (v_dataQualityIssue) {
        v_IMRID = $('#txtDataQualityIssueID').val();
    } else {
        v_IMRID = 'NO IMRID';
    }
    
    var v_missingFunctionality = (($('input[name=chkCoreSystem]:checked', '#fGeneralInformation').val() == 'Yes') ? true : false);
    var v_workOrderID;
    if (v_missingFunctionality) {
        v_workOrderID = $('#txtWorkOrderID').val();
    } else {
        v_workOrderID = 'NO WORK ORDER ID';
    }
    var v_whyThisEUCExist;
    if (v_dataQualityIssue==false && v_missingFunctionality==false) {
        v_whyThisEUCExist = $('#ddlEUCExists').val();
    }
    var v_explainWhyThisEUCExist = $('#txtWhyEUCExist').val();
    var v_isTherePlanToEliminateThisEUC = (($('input[name=chkEliminatePlan]:checked', '#fGeneralInformation').val() == 'Yes') ? true : false);
    var v_thisEUCCanNotBeEliminated = $('#chkEUCCantEliminated').prop('checked');
    var v_reasonWhyEUCNotBeEliminated = $('#txtReasonEUCCantEliminated').val();
    var v_planCreatedDate;
    var v_retirementScheduledDate;
    if (v_isTherePlanToEliminateThisEUC) {
        v_planCreatedDate = $('#inputdateEliminationPlan').val();
        v_retirementScheduledDate = $('#inputdateRetirementPlan').val();
    } else {
        v_planCreatedDate = '1/1/1900';
        v_retirementScheduledDate = '1/1/1900';
    }
    var v_planWillBeCreatedDate;
    if (v_thisEUCCanNotBeEliminated) {
        v_planWillBeCreatedDate = '1/1/1900';
    } else {
        v_planWillBeCreatedDate = $('#inputdatePlanWillBeCreated').val();
    }
    
    
    if (getEUCID()=='0') {
        //-----------------------------------------------------------------------//
        _callServer({
            url: '/EUCInventory/EUCFileInfo/tblEUC_Save',
            data: {
                'p_environmentStatus': v_environmentStarus, 'p_creationReason': v_creationReason, 'p_otherCreationReason': v_otherCreationReason,
                'p_statusDetails': v_statusDetails, 'p_region': v_Region, 'p_country': v_Country, 'p_center': v_Center,
                'p_manager': v_Manager, 'p_delegate': v_Delegate,
                'p_goc': v_GOC, 'p_otherStakeholder': v_OtherStakeHolder, 'p_techContactSOEID': v_TechContactSOEID, 'p_Name': v_Name,
                'p_mainPurpose': v_MainPurpose, 'p_description': v_Description, 'p_businessTaxonomy': v_BusinessTaxonomy,
                'p_sourceOfUse': v_SourceOfUse, 'p_fileType': v_FileType, 'p_numbersOfUsers': v_NumberOfUsers,
                'p_frecuencyOfUse': v_FrecuencyOfUse, 'p_goldCopyMoved': v_GoldCopyMoved, 'p_productionPath': v_ProductionPath,
                'p_userManual': v_UserManual, 'p_OrgID': v_OrgID, 'p_globalEUCID': 0,
                //------------------- Documentation Params ----------------------------------
                'p_ReviewDate': v_ReviewDate, 'p_ReviewedBy': v_ReviewedBy, 'p_DocumentationPathBlueworks': v_DocumentationPathBlueworks,
                //--------------- Elimination Strategy Params -------------------------------
                'p_dataQualityIssue': v_dataQualityIssue, 'p_IMRID': v_IMRID, 'p_missingFunctionality': v_missingFunctionality,
                'p_workOrderID': v_workOrderID, 'p_whyThisEUCExist': v_whyThisEUCExist, 'p_explainWhyThisEUCExist': v_explainWhyThisEUCExist,
                'p_isTherePlanToEliminateThisEUC': v_isTherePlanToEliminateThisEUC, 'p_thisEUCCanNotBeEliminated': v_thisEUCCanNotBeEliminated,
                'p_reasonWhyEUCNotBeEliminated': v_reasonWhyEUCNotBeEliminated, 'p_planCreatedDate': v_planCreatedDate,
                'p_retirementScheduledDate': v_retirementScheduledDate, 'p_planWillBeCreatedDate': v_planWillBeCreatedDate
            },
            type: "post",
            success: function (savingStatus) {
                if (savingStatus == 0) {
                    _showNotification("error", "EUC was not saved!");
                } else {
                    $('#EUCID').text(savingStatus);
                    $('a[href="#euc-form-general-info-tab1"]').css('background', 'green');
                    $('html, body').animate({
                        scrollTop: '0px'
                    }, 800);
                    _showNotification("success", "EUC was saved successfully.");
                }

            }
        });
        //-----------------------------------------------------------------------//
    } else {
        //-----------------------------------------------------------------------//
        //_callServer({
        //    url: '/EUCInventory/EUCFileInfo/tblEUC_Save',
        //    data: {
        //        'p_environmentStatus': v_environmentStarus, 'p_creationReason': v_creationReason, 'p_otherCreationReason': v_otherCreationReason,
        //        'p_statusDetails': v_statusDetails, 'p_region': v_Region, 'p_contry': v_Country, 'p_center': v_Center,
        //        'p_manager': v_Manager, 'p_delegate': v_Delegate,
        //        'p_goc': v_GOC, 'p_otherStakeholder': v_OtherStakeHolder, 'p_techContactSOEID': v_TechContactSOEID, 'p_Name': v_Name,
        //        'p_mainPurpose': v_MainPurpose, 'p_description': v_Description, 'p_businessTaxonomy': v_BusinessTaxonomy,
        //        'p_sourceOfUse': v_SourceOfUse, 'p_fileType': v_FileType, 'p_numbersOfUsers': v_NumberOfUsers,
        //        'p_frecuencyOfUse': v_FrecuencyOfUse, 'p_goldCopyMoved': v_GoldCopyMoved, 'p_productionPath': v_ProductionPath,
        //        'p_userManual': v_UserManual, 'p_OrgID': v_OrgID, 'p_globalEUCID': 0,
        //        //------------------- Documentation Params ----------------------------------
        //        'p_ReviewDate': v_ReviewDate, 'p_ReviewedBy': v_ReviewedBy, 'p_DocumentationPathBlueworks': v_DocumentationPathBlueworks,
        //        //--------------- Elimination Strategy Params -------------------------------
        //        'p_dataQualityIssue': v_dataQualityIssue, 'p_IMRID': v_IMRID, 'p_missingFunctionality': v_missingFunctionality,
        //        'p_workOrderID': v_workOrderID, 'p_whyThisEUCExist': v_whyThisEUCExist, 'p_explainWhyThisEUCExist': v_explainWhyThisEUCExist,
        //        'p_isTherePlanToEliminateThisEUC': v_isTherePlanToEliminateThisEUC, 'p_thisEUCCanNotBeEliminated': v_thisEUCCanNotBeEliminated,
        //        'p_reasonWhyEUCNotBeEliminated': v_reasonWhyEUCNotBeEliminated, 'p_planCreatedDate': v_planCreatedDate,
        //        'p_retirementScheduledDate': v_retirementScheduledDate, 'p_planWillBeCreatedDate': v_planWillBeCreatedDate
        //    },
        //    type: "post",
        //    success: function (savingStatus) {
        //        if (savingStatus == 0) {
        //            _showNotification("error", "EUC was not saved!");
        //        } else {
        //            $('#EUCID').text(savingStatus);
        //            $('a[href="#euc-form-general-info-tab1"]').css('background', 'green');
        //            $('html, body').animate({
        //                scrollTop: '0px'
        //            }, 800);
        //            _showNotification("success", "EUC was saved successfully.");
        //        }

        //    }
        //});
        //-----------------------------------------------------------------------//
    }
    
    
};
//---------------------------------------------------------------//
//************************ SAVE Criticality ***********************
function savetblEUC_Criticality() {
    //---------------------------------------------------------------//

    //---------------------------------------------------------------//
    var answers = $.jqxGridApi.getAllRows('#divEUCtblCriticalityQuestions');
    var v_answersString = '';
    //---------------------------------------------------------------//
    //------------------------ First Group -------------------------//
    var v_isMCA = (($('input[name=EUCEUCMCA]:checked', '#fCriticalityAssessment').val() == 'Yes') ? true : false);
    var v_MCA = $('#ddlMCA').val(); 
    var v_isControlForAProcess = (($('input[name=EUCControlProcess]:checked', '#fCriticalityAssessment').val() == 'Yes') ? true : false);
    var v_isCOBRequired = (($('input[name=EUCCOBRequired]:checked', '#fCriticalityAssessment').val() == 'Yes') ? true : false);
    var v_isProcessEndNeeded = (($('input[name=EUCEndResult]:checked', '#fCriticalityAssessment').val() == 'Yes') ? true : false);
    //---------------------------------------------------------------//
    //----------------------- Seconde Group -------------------------//
    var v_usedFor = $('#ddlUsedFor').val();
    var v_SOXRelated = (($('input[name=EUCSOXRelated]:checked', '#fCriticalityAssessment').val() == 'Yes') ? true : false);
    var v_materialityLevel = $('#ddlEUCMaterialityLevel').val();
    var v_materialityLevelText = $('#ddlEUCMaterialityLevel').find(":selected").text();
    var v_dataClasification = $('#ddlEUCDataClasification').val();
    //---------------------------------------------------------------//
    var v_criticality = 0;
    //------------------ Setting criticality ------------------------//
    if (v_isMCA || v_isControlForAProcess || v_isCOBRequired || v_isProcessEndNeeded) {
        v_criticality = 'Business Important';
    } else {
        v_criticality = 'Non-Critical';
    }
    
    if (v_SOXRelated) {
        v_criticality = 'Critical';
    }
    if (v_materialityLevelText == '$20 - $100 Million' || v_materialityLevelText == 'Above $100 Million') {
        v_criticality = 'Critical';
    }
    //---------------------------------------------------------------//
    var v_OrgId = 1;

    $.each(answers, function (index) {
        v_answersString = v_answersString + '[' + this['QuestionNo'] + '][' + (this['Answer'] ? 1 : 0) + '],';
    });
    
    v_answersString = v_answersString.slice(0, -1);
    console.log(v_answersString);
    var vEUCID = getEUCID();

    _callServer({
        url: '/EUCInventory/EUCFileInfo/tblEUC_Criticality',
        data: {
            'p_OrgID': v_OrgId,'p_isMCAProcessRelated':v_isMCA,'p_MCAProcess':v_MCA,
            'p_isControlForAProcess':v_isControlForAProcess,'p_COBRequired':v_isCOBRequired,
            'p_isProcessEndNeeded':v_isProcessEndNeeded,'p_usedFor':v_usedFor,'p_materialityLevel':v_materialityLevel,
            'p_SOXRelated':v_SOXRelated,'p_dataClasification':v_dataClasification,'p_criticality':v_criticality,
            //------------------- Documentation Params ----------------------------------
            'p_EUCID': vEUCID, 'p_Answers': v_answersString
        },
        type: "post",
        success: function (savingStatus) {
            if (savingStatus == 0) {
                _showNotification("error", "EUC was not saved!");
            } else {                
                $('a[href="#euc-form-criticality-tab2"]').css('background', 'green');
                $('html, body').animate({
                    scrollTop: '0px'
                }, 800);
                _showNotification("success", "EUC was saved successfully.");
                _showNotification("success", "EUC criticality assigned: " +  savingStatus + ".");
            }

        }
    });
    
    //for (var i = 0; i < length; i++) {

    //}

    //answers.forEach(function (value) {
    //    console.log(value);
    //});
};











