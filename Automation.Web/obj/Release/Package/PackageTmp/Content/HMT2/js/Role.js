﻿var RowListSelected;
var state = null;;
$(document).ready(function () {
    _hideMenu();

    GetListTotalRole();

  
    $('#roleInfoDiv').jqxWindow({
        width: 1100, height: 600, resizable: false, isModal: true, autoOpen: false, cancelButton: $("#Cancel"), modalOpacity: 0.7
    });

    $('[data-toggle="tooltip"]').tooltip();

    $("#popUpMOUNMList").jqxWindow({
        width: 1100, height: 600, resizable: false, isModal: true, autoOpen: false, cancelButton: $("#Cancel"), modalOpacity: 0.7
    });

    $("#popUpRequisition").jqxWindow({
        width: 1100, height: 600, resizable: false, isModal: true, autoOpen: false, cancelButton: $("#Cancel"), modalOpacity: 0.7
    });

    $("#columnsSelect").jqxWindow({
    width: 300, height: 300, resizable: false, isModal: true, autoOpen: false, cancelButton: $("#Cancel"), modalOpacity: 0.7});

    $('#AddRoleByGoc').hide();
});

function openPopUpColumns() {
    $('#columnsSelect').jqxWindow('open');
}
function GetListTotalRole() {
    $('#lblChoose').text("Roles detail");
    $('#divPenRev').hide();

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/Role/getTotalRolesList',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                            { name: 'RoleID', type: 'string' },
                          , { name: 'NumberRole', type: 'string' },
                          , { name: 'NameRole', type: 'string' },
                          , { name: 'GOCRole', type: 'string' },
                          , { name: 'MSRole', type: 'string' },
                          , { name: 'MGRole', type: 'string' },
                          , { name: 'RegionRole', type: 'string' },
                          , { name: 'CountryRole', type: 'string' },
                          , { name: 'LVIDRole', type: 'string' },
                          , { name: 'CreatedByRole', type: 'string' },
                          , { name: 'CreatedDateRole', type: 'string' },
                          , { name: 'LastUpdateRole', type: 'string' },
                          , { name: 'LastUpdateDateRole', type: 'string' },
                          , { name: 'IsFteableRole', type: 'string' },
                          , { name: 'IsDirectStafRole', type: 'string' },
                          , { name: 'RoleIDRoleTypeID', type: 'string' },
                          , { name: 'AutoNumericSource', type: 'string' },
                          , { name: 'SourceID', type: 'string' },
                          , { name: 'IsActiveSource', type: 'string' },
                          , { name: 'StartMonth', type: 'string' },
                          , { name: 'StartYear', type: 'string' },
                          , { name: 'EndMonth', type: 'string' },
                          , { name: 'EndYear', type: 'string' },
                          , { name: 'DriverID', type: 'string' },
                          , { name: 'SourceStatus', type: 'string' },
                          , { name: 'SourceName', type: 'string' },
                          , { name: 'DriverName', type: 'string' },
                          , { name: 'DriverType', type: 'string' },
                          , { name: 'RLSDriver', type: 'string' },
                          , { name: 'SourceDriver', type: 'string' },
                          , { name: 'IsAdd', type: 'string' },
                          , { name: 'IsLess', type: 'string' },
                          , { name: 'ImpactFRODemand', type: 'string' },
                          , { name: 'ImpactType', type: 'string' },
                          , { name: 'StatusRole', type: 'string' },
                          , { name: 'mgdGeographyDescription', type: 'string' },
                          , { name: 'ManagedSegmentDescription', type: 'string' },
                          , { name: 'LocationID', type: 'string' },
                          , { name: 'LocationName', type: 'string' },
                          , { name: 'CenterID', type: 'string' },
                          , { name: 'ProfileID', type: 'string' },
                          , { name: 'Profile', type: 'string' },
                          , { name: 'GOCDescription', type: 'string' },
                          , { name: 'EmployeeID', type: 'string' },
                          , { name: 'StartDate', type: 'string' },
                          , { name: 'EndDate', type: 'string' },
                          , { name: 'IsActiveEmployeeRole', type: 'string' },
                          , { name: 'ReqID', type: 'string' },
                          , { name: 'IsInternalMovement', type: 'string' },
                          , { name: 'SOEID', type: 'string' },
                          , { name: 'FirstName', type: 'string' },
                          , { name: 'LastName', type: 'string' },
                          , { name: 'HiringDate', type: 'string' },
                          , { name: 'AttritionDate', type: 'string' },
                          , { name: 'IsActive', type: 'string' },
                          , { name: 'ClassificationID', type: 'string' },
                          , { name: 'ClassificationLetter', type: 'string' },
                          , { name: 'ClassificationDescription', type: 'string' },
                          , { name: 'RequisitionNumber', type: 'string' },
                          , { name: 'RequisitionName', type: 'string' },
                          , { name: 'RequisitionCreationDate', type: 'string' },
                          , { name: 'RequisitionStatusID', type: 'string' },
                          , { name: 'Center', type: 'string' },
                          , { name: 'PosibleStart', type: 'string' },
                          , { name: 'PosibleEnd', type: 'string' },
                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            var imgViewSource = function (row, datafield, value) {

                var rowData = $('#jqxGridRoles').jqxGrid('getrowdata', row);

                var ID = rowData.IDType;
                var Number = rowData.Number;

                return '<center>' + Number + '   <i class="icon-search" style=" font-size: 23px; color: #4286f4;" onclick="openPopUpLiveStage(' + ID + ')" ></center>';
            }

            var imgDSTemp = function (row, datafield, value) {

                var rowData = $('#jqxGridRoles').jqxGrid('getrowdata', row);

                var directStaft = rowData.IsDirectStaf;

                if (IsDirectStafRole == "Yes") {
                    return '<div style="margin-left: 10%;">Direct Staft</div>';
                } else {
                    return '<div  style="margin-left: 10%;">Temp</div>';
                }
            }

            var imgSelectRole = function (row, datafield, value) {

                var rowData = $('#jqxGridRoles').jqxGrid('getrowdata', row);

                var ret = '';
                //return '<button type="button" class="btn btn-secondary btn-icon bottom15 right15"   onClick="reviewRole(' + rowData.RoleID + ',' + row + ')"><i class="fa fa-edit "></i></button>';
                ret = ret + '<button type="button" class="btn btn-primary"  title="Edit Row" onClick="reviewRole(' + rowData.RoleID + ',' + row + ')"><i class="fa fa-edit"></i></button>';
                //ret = ret + '<button type="button" class="btn btn-primary"  title="Edit Requisition" onClick="reviewRole(' + rowData.RoleID + ',' + row + ')"><i class="fa fa-edit"></i></button>'
                //ret = ret + '<button type="button" class="btn btn-primary"  title="Edit MOU/NONMOU" onClick="reviewRole(' + rowData.RoleID + ',' + row + ')"><i class="fa fa-edit"></i></button>'
                //ret = ret + '<button type="button" class="btn btn-primary"  title="Edit MOU/NONMOU" onClick="reviewRole(' + rowData.RoleID + ',' + row + ')"><i class="fa fa-edit"></i></button>'
                return ret;
               
            }

            var initrowdetails = function (index, parentElement, gridElement, record) {
                var id = record.uid.toString();

                var gridRole = $($(parentElement).children()[0].children[1]);
                var gridMOU = $($(parentElement).children()[1].children[1]);
                var gridReq = $($(parentElement).children()[2].children[1]);
                var gridDriver = $($(parentElement).children()[3].children[1]);
                var gridEmployee = $($(parentElement).children()[4].children[1]);

                var nestedData;

                nestedData = $('#jqxGridRoles').jqxGrid('getrowdata', index);

                        var orderssource = {
                            localdata: nestedData,
                            datafields:
                               [
                                   { name: 'RoleID', type: 'string' },
                                   { name: 'NumberRole', type: 'string' },
                                   { name: 'NameRole', type: 'string' },
                                   { name: 'GOCRole', type: 'string' },
                                   { name: 'MSRole', type: 'string' },
                                   { name: 'MGRole', type: 'string' },
                                   { name: 'RegionRole', type: 'string' },
                                   { name: 'CountryRole', type: 'string' },
                                   { name: 'LVIDRole', type: 'string' },
                                   { name: 'CreatedByRole', type: 'string' },
                                   { name: 'CreatedDateRole', type: 'string' },
                                   { name: 'LastUpdateRole', type: 'string' },
                                   { name: 'LastUpdateDateRole', type: 'string' },
                                   { name: 'IsFteableRole', type: 'string' },
                                   { name: 'IsDirectStafRole', type: 'string' },
                                   { name: 'RoleIDRoleTypeID', type: 'string' },
                                   { name: 'AutoNumericSource', type: 'string' },
                                   { name: 'SourceID', type: 'string' },
                                   { name: 'IsActiveSource', type: 'string' },
                                   { name: 'StartMonth', type: 'string' },
                                   { name: 'StartYear', type: 'string' },
                                   { name: 'EndMonth', type: 'string' },
                                   { name: 'EndYear', type: 'string' },
                                   { name: 'DriverID', type: 'string' },
                                   { name: 'SourceStatus', type: 'string' },
                                   { name: 'SourceName', type: 'string' },
                                   { name: 'DriverName', type: 'string' },
                                   { name: 'DriverType', type: 'string' },
                                   { name: 'RLSDriver', type: 'string' },
                                   { name: 'SourceDriver', type: 'string' },
                                   { name: 'IsAdd', type: 'string' },
                                   { name: 'IsLess', type: 'string' },
                                   { name: 'ImpactFRODemand', type: 'string' },
                                   { name: 'ImpactType', type: 'string' },
                                   { name: 'StatusRole', type: 'string' },
                                   { name: 'mgdGeographyDescription', type: 'string' },
                                   { name: 'ManagedSegmentDescription', type: 'string' },
                                   { name: 'LocationID', type: 'string' },
                                   { name: 'LocationName', type: 'string' },
                                   { name: 'CenterID', type: 'string' },
                                   { name: 'ProfileID', type: 'string' },
                                   { name: 'Profile', type: 'string' },
                                   { name: 'GOCDescription', type: 'string' },
                                   { name: 'EmployeeID', type: 'string' },
                                   { name: 'StartDate', type: 'string' },
                                   { name: 'EndDate', type: 'string' },
                                   { name: 'IsActiveEmployeeRole', type: 'string' },
                                   { name: 'ReqID', type: 'string' },
                                   { name: 'IsInternalMovement', type: 'string' },
                                   { name: 'SOEID', type: 'string' },
                                   { name: 'FirstName', type: 'string' },
                                   { name: 'LastName', type: 'string' },
                                   { name: 'HiringDate', type: 'string' },
                                   { name: 'AttritionDate', type: 'string' },
                                   { name: 'IsActive', type: 'string' },
                                   { name: 'ClassificationID', type: 'string' },
                                   { name: 'ClassificationLetter', type: 'string' },
                                   { name: 'ClassificationDescription', type: 'string' },
                                   { name: 'RequisitionNumber', type: 'string' },
                                   { name: 'RequisitionName', type: 'string' },
                                   { name: 'RequisitionCreationDate', type: 'string' },
                                   { name: 'RequisitionStatusID', type: 'string' },
                                   { name: 'Center', type: 'string' },
                                   { name: 'PosibleStart', type: 'string' }, 
                                   { name: 'PosibleEnd', type: 'string' }
                               ],
                            datatype: "json"
                        };

                        var nestedGridAdapter = new $.jqx.dataAdapter(orderssource);
                        nestedGridAdapter.dataBind();

                        if (gridRole != null) {

                            gridRole.jqxGrid({
                                source: nestedGridAdapter,
                                theme: 'energyblue',
                                width: '100%',
                                autoheight: true,
                                autorowheight: true,
                                pageable: false,
                                filterable: false,
                                showfilterrow: false,
                                columnsresize: true,
                                enablebrowserselection: true,
                                columns: [
                                    { text: 'Number Role', dataField: 'NumberRole', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Name Role', dataField: 'NameRole', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Center', dataField: 'Center', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Location Name', dataField: 'LocationName', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Charge Out', dataField: 'Profile', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Managed Segment', dataField: 'ManagedSegmentDescription', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Managed Geography', dataField: 'mgdGeographyDescription', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Region ', dataField: 'RegionRole', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Country', dataField: 'CountryRole', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'LVID', dataField: 'LVIDRole', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Created By', dataField: 'CreatedByRole', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Created Date', dataField: 'CreatedDateRole', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Last Update', dataField: 'LastUpdateRole', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Last Update Date', dataField: 'LastUpdateDateRole', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Is Fteable', dataField: 'IsFteableRole', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Is Direct Staft', dataField: 'IsDirectStafRole', filtertype: 'checkedlist', width: '9%', editable: false }
                                ]

                            });
                            gridMOU.jqxGrid({
                                source: nestedGridAdapter,
                                theme: 'energyblue',
                                width: '100%',
                                autoheight: true,
                                autorowheight: true,
                                pageable: false,
                                filterable: false,
                                showfilterrow: false,
                                columnsresize: true,
                                enablebrowserselection: true,
                                columns: [
                                    { text: 'Auto Numeric', dataField: 'AutoNumericSource',width: '9%', editable: false },
                                    { text: 'Posible Start', dataField: 'PosibleStart',  width: '9%', editable: false },
                                    { text: 'Posible End', dataField: 'PosibleEnd',  width: '9%', editable: false }
                                ]

                            });
                            gridDriver.jqxGrid({
                                source: nestedGridAdapter,
                                theme: 'energyblue',
                                width: '100%',
                                autoheight: true,
                                autorowheight: true,
                                pageable: false,
                                filterable: false,
                                showfilterrow: false,
                                columnsresize: true,
                                enablebrowserselection: true,
                                columns: [
                                    { text: 'Driver Name', dataField: 'DriverName', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Source Driver', dataField: 'SourceDriver', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Driver Type', dataField: 'DriverType', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'RLS Driver', dataField: 'RLSDriver', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Impact FRO Demand', dataField: 'ImpactFRODemand', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Impact Type', dataField: 'ImpactType', filtertype: 'checkedlist', width: '9%', editable: false }

                                ]

                            });
                            gridReq.jqxGrid({
                                source: nestedGridAdapter,
                                theme: 'energyblue',
                                width: '100%',
                                autoheight: true,
                                autorowheight: true,
                                pageable: false,
                                filterable: false,
                                showfilterrow: false,
                                columnsresize: true,
                                enablebrowserselection: true,
                                columns: [
                                    { text: 'Requisition Number', dataField: 'RequisitionNumber', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Requisition Name ', dataField: 'RequisitionName', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Creation Date ', dataField: 'RequisitionCreationDate', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Requisition Status', dataField: 'RequisitionStatusID', filtertype: 'checkedlist', width: '9%', editable: false }
                                ]

                            });
                            gridEmployee.jqxGrid({
                                source: nestedGridAdapter,
                                theme: 'energyblue',
                                width: '100%',
                                autoheight: true,
                                autorowheight: true,
                                pageable: false,
                                filterable: false,
                                showfilterrow: false,
                                columnsresize: true,
                                enablebrowserselection: true,
                                columns: [
                                    { text: 'SOEID', dataField: 'SOEID', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'First Name', dataField: 'FirstName', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Last Name', dataField: 'LastName', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Hiring Date', dataField: 'HiringDate', filtertype: 'checkedlist', width: '30%', editable: false },
                                    { text: 'Classification', dataField: 'ClassificationLetter', filtertype: 'checkedlist', width: '9%', editable: false },
                                    { text: 'Description', dataField: 'ClassificationDescription', filtertype: 'checkedlist', width: '9%', editable: false }
                                ]

                            });


                        }
            };

            var addfilter = function () {
                var filtergroup = new $.jqx.filter();
                var filter_or_operator = 1;
                var filtervalue = $('#HFRoleID').val();
                var filtercondition = 'contains';
                var filter1 = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
               
                filtergroup.addfilter(filter_or_operator, filter1);

                // add the filters.
                $("#jqxGridRoles").jqxGrid('addfilter', 'NumberRole', filtergroup);
                // apply the filters.
                $("#jqxGridRoles").jqxGrid('applyfilters');
            }


            $('#jqxGridRoles').jqxGrid(
           {
               source: dataAdapter,
               theme: 'energyblue',
               width: '100%',
               selectionmode: 'multiplecellsadvanced',
               sortable: true,
               height:700,
               autoheight: false,
               autorowheight: false,
               pageable: false,
               filterable: true,
               editable: true,
               showfilterrow: true,
               columnsresize: true,
               selectionmode: 'checkbox',
               enablebrowserselection: true,
               groupable: true,
               rowdetails: true,
               autoloadstate: false,
               autosavestate: false,
               initrowdetails: initrowdetails,
               ready: function () {
                   addfilter();
               },
               rowdetailstemplate: { rowdetails: '<fieldset><legend>Role Info</legend><div id="grid1"></div></fieldset><fieldset><legend>MOU Info</legend><div id="grid2"></div></fieldset><fieldset><legend>Requisition Info</legend><div id="grid3"></div></fieldset><fieldset><legend>Driver Info</legend><div id="grid4"></div></fieldset><fieldset><legend>Employee Info</legend><div id="grid5"></div></fieldset></br></br>', rowdetailsheight: 650, rowdetailshidden: true },
               groups: ['Center', 'GOCDescription'],
               columns: [
                 
                       { text: 'Role', dataField: 'NumberRole', width: '8%', filtertype: 'input', editable: false },
                       { text: 'Role Status', dataField: 'StatusRole', width: '6%', filtertype: 'input', editable: true },
                       { text: 'Center', dataField: 'Center', width: '6%', filtertype: 'input', editable: false },
                       { text: 'MOU/NonMOU #', dataField: 'AutoNumericSource', width: '10%', filtertype: 'input', editable: false },
                       { text: 'GOC', dataField: 'GOCDescription', width: '10%', filtertype: 'input', editable: false },
                       {
                           text: 'Driver', dataField: 'DriverName', columntype: 'combobox', width: '10%', filtertype: 'input', editable: true,
                           createeditor: function (row, column, editor) {
                               // assign a new data source to the combobox.
                               var list = ['New Work MOU',
                                            'Transfer In MOU',
                                            'Transfer out MOU',
                                            'Project MOU',
                                            'Investments',
                                            'Reengineering',
                                            'Right Placement Out',
                                            'Reduction Placement Out',
                                            'Divestiture',
                                            'Right Placement In MOU',
                                            'Replacements',
                                            'Attrition',
                                            'Approved Resources',
                                            'Mobility Out (Leave FRO)',
                                            'Employee Type Reclass',
                                            'Internal Movements',
                                            'Internal Restructure',
                                            'Work Absorption MOU',
                                            'Plan'];


                               
                               editor.jqxComboBox({ autoDropDownHeight: true, source: list, promptText: "Please Choose:" });
                           }
                       },
                       { text: 'Is DirectStaf', dataField: 'IsDirectStafRole', width: '3%', filtertype: 'input', editable: false },
                       { text: 'Requisition', dataField: 'RequisitionNumber', width: '6%', filtertype: 'input', editable: false },
                       { text: 'SOEID', dataField: 'SOEID', width: '6%', filtertype: 'input', editable: false },
                       { text: 'Posible Start', dataField: 'PosibleStart',  width: '10%', editable: false },
                       { text: 'Posible End', dataField: 'PosibleEnd',  width: '10%', editable: false },
                       { text: 'Hiring Date', dataField: 'HiringDate', width: '6%', editable: false },
                       { text: 'Charge Out', dataField: 'Profile', filtertype: 'checkedlist', width: '4%', editable: false }
                       
               ]

           });

            var listSource = [
                               { text: 'Role', value: 'NumberRole', checked: true },
                               { text: 'Center', value: 'Center', checked: true },
                               { text: 'MOU/NonMOU #', value: 'AutoNumericSource', checked: true },
                               { text: 'GOC', value: 'GOCDescription', checked: true },
                               { text: 'Driver', value: 'DriverName', checked: true },
                               { text: 'Is DirectStaf', value: 'IsDirectStafRole', checked: true },
                               { text: 'Requisition', value: 'RequisitionNumber', checked: true },
                               { text: 'SOEID', value: 'SOEID', checked: true },
                               { text: 'Employee Classification', value: 'ClassificationDescription', checked: true },
                               { text: 'Role Status', value: 'StatusRole', checked: true },
                            ];








            $("#jqxlistbox").jqxListBox({ source: listSource, width: 240, height: 240, checkboxes: true });
            $("#jqxlistbox").on('checkChange', function (event) {
                $("#jqxGridRoles").jqxGrid('beginupdate');
                if (event.args.checked) {
                    $("#jqxGridRoles").jqxGrid('showcolumn', event.args.value);
                }
                else {
                    $("#jqxGridRoles").jqxGrid('hidecolumn', event.args.value);
                }
                $("#jqxGridRoles").jqxGrid('endupdate');
            });


        }
    });
}


function renderGridReq() {
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/Requisition/getReqList',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);


            var source =
           {
               unboundmode: true,
               localdata: response,
               datafields:
                       [

                      { text: 'Number', type: 'string' },
                      { text: 'Name', type: 'string' },
                      { text: 'DateCreated', type: 'string' },
                      { text: 'NumberPositions', type: 'string' },
                      { text: 'Clevel', type: 'string' },
                      { text: 'StatusID', type: 'string' },
                      { text: 'DaysOpen', type: 'string' },
                      { text: 'GOC', type: 'string' },
                      { text: 'Expr1', type: 'string' },
                      { text: 'BusinessUnit', type: 'string' },
                      { text: 'Business', type: 'string' },
                      { text: 'BusinessDetail', type: 'string' },
                      { text: 'ManagedGeography', type: 'string' },
                      { text: 'PhysicalRegion', type: 'string' },
                      { text: 'ManagedFunction', type: 'string' },
                      { text: 'OrgLevel1', type: 'string' },
                      { text: 'OrgLevel2', type: 'string' },
                      { text: 'OrgLevel3', type: 'string' },
                      { text: 'OrgLevel4', type: 'string' },
                      { text: 'OrgLevel5', type: 'string' },
                      { text: 'OrgLevel6', type: 'string' },
                      { text: 'OrgLevel8', type: 'string' },
                      { text: 'OrgLevel7', type: 'string' },
                      { text: 'OrgLevel9', type: 'string' },
                      { text: 'MGLevel1', type: 'string' },
                      { text: 'MGLevel2', type: 'string' },
                      { text: 'MGLevel3', type: 'string' },
                      { text: 'MGLevel4', type: 'string' },
                      { text: 'MGLevel5', type: 'string' },
                      { text: 'MGLevel6', type: 'string' },
                      { text: 'CSWWorkflowName', type: 'string' },
                      { text: 'SourceSystem', type: 'string' },
                      { text: 'Expr2', type: 'string' },
                      { text: 'GOCDescription', type: 'string' },
                      { text: 'Recruiter', type: 'string' },
                      { text: 'RecruiterGEID', type: 'string' },
                      { text: 'RelationshipIndicator', type: 'string' },
                      { text: 'LefttoHire', type: 'string' },
                      { text: 'ReqStatusDate', type: 'string' },
                      { text: 'FirstApprovalDate', type: 'string' },
                      { text: 'ReqCreationDate', type: 'string' },
                      { text: 'InternalPostingStatus', type: 'string' },
                      { text: 'InternalPostingDate', type: 'string' },
                      { text: 'InternalUnpostingDate', type: 'string' },
                      { text: 'PersonReplacing', type: 'string' },
                      { text: 'PersonReplacingSOEID', type: 'string' },
                      { text: 'EmployeeReferalBonus', type: 'string' },
                      { text: 'EligibleforCampsHire', type: 'string' },
                      { text: 'HiringManagerGEID', type: 'string' },
                      { text: 'HiringManager', type: 'string' },
                      { text: 'FinancialJustification', type: 'string' },
                      { text: 'RequisitionJustification', type: 'string' },
                      { text: 'IsitaProductionJob', type: 'string' },
                      { text: 'FullTimePartTime', type: 'string' },
                      { text: 'OvertimeStatus', type: 'string' },
                      { text: 'ReqSalaryGrade', type: 'string' },
                      { text: 'MappedOfficerTitle', type: 'string' },
                      { text: 'OfficerTitle', type: 'string' },
                      { text: 'OfficerTitleCode', type: 'string' },
                      { text: 'StandardGrade', type: 'string' },
                      { text: 'Grade', type: 'string' },
                      { text: 'Country', type: 'string' },
                      { text: 'WorldRegion', type: 'string' },
                      { text: 'CSSCenter', type: 'string' },
                      { text: 'LocationCode', type: 'string' },
                      { text: 'OLM', type: 'string' },
                      { text: 'CSC', type: 'string' },
                      { text: 'CompanyCode', type: 'string' }
                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            //var imgViewSource = function (row, datafield, value) {

            //    var rowData = $('#jqxGridReqs').jqxGrid('getrowdata', row);

            //    var ID = rowData.IDType;
            //    var Number = rowData.Number;

            //    return '<center>' + Number + '   <i class="icon-search" style=" font-size: 23px; color: #4286f4;" onclick="openPopUpLiveStage(' + ID + ')" ></center>';
            //}



            $('#jqxGridReqs').jqxGrid(
           {
               source: dataAdapter,
               theme: 'energyblue',
               width: '100%',
               // autoheight: true,
               // autorowheight: true,
               pagesize: 10,
               pageable: true,
               filterable: true,
               showfilterrow: true,
               columnsresize: true,
               selectionmode: 'singlecell',
               enablebrowserselection: true,
               columns: [
                      { text: 'Number', dataField: 'Number', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Name', dataField: 'Name', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Date Created', dataField: 'DateCreated', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Number Positions', dataField: 'NumberPositions', filtertype: 'input', width: '150px', editable: false },
                      { text: 'C level', dataField: 'Clevel', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Days Open', dataField: 'DaysOpen', filtertype: 'input', width: '150px', editable: false },
                      { text: 'GOC', dataField: 'GOC', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Status', dataField: 'Expr1', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Business Unit', dataField: 'BusinessUnit', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Business', dataField: 'Business', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Business Detail', dataField: 'BusinessDetail', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Managed Geography', dataField: 'ManagedGeography', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Physical Region', dataField: 'PhysicalRegion', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Managed Function', dataField: 'ManagedFunction', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Org Level1', dataField: 'OrgLevel1', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Org Level2', dataField: 'OrgLevel2', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Org Level3', dataField: 'OrgLevel3', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Org Level4', dataField: 'OrgLevel4', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Org Level5', dataField: 'OrgLevel5', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Org Level6', dataField: 'OrgLevel6', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Org Level8', dataField: 'OrgLevel8', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Org Level7', dataField: 'OrgLevel7', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Org Level9', dataField: 'OrgLevel9', filtertype: 'input', width: '150px', editable: false },
                      { text: 'MG Level1', dataField: 'MGLevel1', filtertype: 'input', width: '150px', editable: false },
                      { text: 'MG Level2', dataField: 'MGLevel2', filtertype: 'input', width: '150px', editable: false },
                      { text: 'MG Level3', dataField: 'MGLevel3', filtertype: 'input', width: '150px', editable: false },
                      { text: 'MG Level4', dataField: 'MGLevel4', filtertype: 'input', width: '150px', editable: false },
                      { text: 'MG Level5', dataField: 'MGLevel5', filtertype: 'input', width: '150px', editable: false },
                      { text: 'MG Level6', dataField: 'MGLevel6', filtertype: 'input', width: '150px', editable: false },
                      { text: 'CS Workflow Name', dataField: 'CSWWorkflowName', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Source System', dataField: 'SourceSystem', filtertype: 'input', width: '150px', editable: false },
                      { text: 'GOC Description', dataField: 'GOCDescription', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Recruiter', dataField: 'Recruiter', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Recruiter GEID', dataField: 'RecruiterGEID', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Relationship Indicator', dataField: 'RelationshipIndicator', filtertype: 'input', width: '150px', editable: false },
                      { text: 'LefttoHire', dataField: 'LefttoHire', filtertype: 'input', width: '150px', editable: false },
                      { text: 'ReqStatus Date', dataField: 'ReqStatusDate', filtertype: 'input', width: '150px', editable: false },
                      { text: 'First Approval Date', dataField: 'FirstApprovalDate', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Req Creation Date', dataField: 'ReqCreationDate', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Internal Posting Status', dataField: 'InternalPostingStatus', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Internal Posting Date', dataField: 'InternalPostingDate', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Internal Unposting Date', dataField: 'InternalUnpostingDate', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Person Replacing', dataField: 'PersonReplacing', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Person Replacing SOEID', dataField: 'PersonReplacingSOEID', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Employee Referal Bonus', dataField: 'EmployeeReferalBonus', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Eligible for Camps Hire', dataField: 'EligibleforCampsHire', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Hiring Manager GEID', dataField: 'HiringManagerGEID', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Hiring Manager', dataField: 'HiringManager', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Financial Justification', dataField: 'FinancialJustification', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Requisition Justification', dataField: 'RequisitionJustification', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Isita Production Job', dataField: 'IsitaProductionJob', filtertype: 'input', width: '150px', editable: false },
                      { text: 'FullTime PartTime', dataField: 'FullTimePartTime', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Overtime Status', dataField: 'OvertimeStatus', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Req Salary Grade', dataField: 'ReqSalaryGrade', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Mapped Officer Title', dataField: 'MappedOfficerTitle', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Officer Title', dataField: 'OfficerTitle', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Officer TitleCode', dataField: 'OfficerTitleCode', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Standard Grade', dataField: 'StandardGrade', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Grade', dataField: 'Grade', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Country', dataField: 'Country', filtertype: 'input', width: '150px', editable: false },
                      { text: 'World Region', dataField: 'WorldRegion', filtertype: 'input', width: '150px', editable: false },
                      { text: 'CSS Center', dataField: 'CSSCenter', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Location Code', dataField: 'LocationCode', filtertype: 'input', width: '150px', editable: false },
                      { text: 'OLM', dataField: 'OLM', filtertype: 'input', width: '150px', editable: false },
                      { text: 'CSC', dataField: 'CSC', filtertype: 'input', width: '150px', editable: false },
                      { text: 'Company Code', dataField: 'CompanyCode', filtertype: 'input', width: '150px', editable: false }
               ]

           });

        }
    });
}

function reviewRole(ID, row) {

    $('#roleInfoDiv').jqxWindow('open');

    var rowData = $('#jqxGridRoles').jqxGrid('getrowdata', row);

    $('#lblReq').text('Role ' + rowData.RoleAuto);

    renderChargeOutGOC();
    //renderDirectStaff();

    $('#jqxGoc').val(rowData.GOCName)

    $('#txtName').val(rowData.Name)

    $('#txtMSLVL09').val(rowData.ManagedSegment);
    $('#txtGeography').val(rowData.ManagedGeography);
    $('#txtRegion').val(rowData.MgdRegion);

    $('#txtLVID').val(rowData.LVID);
    $('#txtChargeOut').val(rowData.ChargeOutProfile);
    $('#txtStatus').val(rowData.Status);

    if (rowData.IsDirectStaf) {
        renderDirectStaff('Direct Staff', '#cbDirectStaft');
    } else {
        renderDirectStaff('Temp', '#cbDirectStaft');
    }
    if (rowData.IsFteable) {
        renderIsFTeable('Is FTeble', "#cbFteable");
    } else {
        renderIsFTeable('Not FTeable', "#cbFteable");
    }
    renderPhysicalLocation(rowData.PhysicalLocation);


    if (rowData.Number.substring(0, 2) == "NM") {
        renderTypeValuesNONMOU(rowData.Number);
    }
    else {
        renderTypeValuesMOU(rowData.Number);
    }

    renderTaleoData(rowData.RoleAuto);
    renderTypeInfo(rowData.Number, rowData.RoleAuto);
    renderMOUNMhistory(rowData.RoleAuto);
    renderGridEmployee(rowData.RoleAuto);
}

function renderGridEmployee(Role) {

    var listOfParams = {
        pRole: Role
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/Role/getEmployeeHistory',
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
                  {
                      unboundmode: true,
                      localdata: data2,
                      datafields:
                              [
                                    { name: 'ID', type: 'string' },
                                    { name: 'SOEID', type: 'string' },
                                    { name: 'FirstName', type: 'string' },
                                    { name: 'LastName', type: 'string' },
                                    { name: 'IsActive', type: 'string' },
                                    { name: 'HiringDate', type: 'string' },
                                    { name: 'AttritionDate', type: 'string' },
                                    { name: 'ClassificationID', type: 'string' }

                              ],
                      datatype: "json"
                  };

            var dataAdapter = new $.jqx.dataAdapter(source);

            $('#gridEmployee').jqxGrid(
           {
               source: dataAdapter,
               theme: 'energyblue',
               autoheight: true,
               width: '100%',
               pageable: false,
               columnsresize: true,
               selectionmode: 'singlecell',
               enablebrowserselection: true,
               columns: [
                                  { text: 'SOEID', dataField: 'SOEID', filtertype: 'input', editable: false, width: '10%' }
                                , { text: 'First Name', dataField: 'FirstName', filtertype: 'input', editable: false, width: '10%' }
                                , { text: 'Last Name', dataField: 'LastName', filtertype: 'input', editable: false, width: '10%' }
                                , { text: 'Hiring Date', dataField: 'HiringDate', filtertype: 'input', editable: false, width: '10%' }
                                , { text: 'Active', dataField: 'IsActive', filtertype: 'input', editable: false, width: '10%' }
                                , { text: 'Attrition Date', dataField: 'AttritionDate', filtertype: 'input', editable: false, width: '10%' }

               ]

           });

        }
    });

}

function renderChargeOutGOC() {
    findGocMethod('#jqxGoc', '100%');
    $('#jqxGoc').on('select',
      function () {
          loadGocData();
      });
}

function findGocMethod(nameTag, widthValue) {
    var timer;
    $(nameTag).jqxInput({
        placeHolder: "Enter a GOC", height: 25, width: widthValue,
        source: function (query, response) {
            var data2;

            var listOfParams = {
                data: query
            }

            _callServer({
                loadingMsg: "", async: false,
                url: '/NonMOU/getProbablyGOC',
                data: {
                    'data': JSON.stringify(listOfParams)
                },
                type: "post",
                success: function (json) {
                    var response = jQuery.parseJSON(json);
                    loadAdapter(response);

                }
            });

            function loadAdapter(data2) {


                var dataAdapter = new $.jqx.dataAdapter
                (
                    {
                        datatype: "jsonp",
                        localdata: data2,
                        datafields:
                        [
                            { name: 'GOCID' }, { name: 'GOCName' }
                        ],

                    },
                    {
                        autoBind: true,
                        formatData: function (data) {
                            data.name_startsWith = query;
                            return data;
                        },
                        loadComplete: function (data) {
                            if (data.length > 3) {
                                response($.map(data, function (item) {
                                    return {
                                        label: item.GOCName,
                                        value: item.GOCName
                                    }
                                }));
                            }
                        }
                    }
                );
            }
        }
    });


}

function renderStatus() {

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/Role/getStatus',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = (response);

            var source =
                {
                    localdata: data2,
                    datatype: "json",
                    datafields: [

                        { name: 'Description' }
                    ],
                    async: false
                };

            var dataAdapter = new $.jqx.dataAdapter(source);

            $("#cbManagedSegmentLvl9").jqxComboBox({ selectedIndex: 0, source: dataAdapter, displayMember: "Description", valueMember: "Description", width: '99%', height: 25 });

            //if (NameType != '') {
            //    $("#cbManagedSegmentLvl9").jqxComboBox('selectItem', NameType);
            //}

            $("#cbManagedSegmentLvl9").on('select', function (event) {
                if (event.args) {
                    var item = event.args.item;
                }
            });
        }
    });

}

function renderDirectStaff(variable, container) {
    data2 = jQuery.parseJSON('[{"Description": "Direct Staff"},{"Description": "Temp"}]');

    var source =
        {
            localdata: data2,
            datatype: "json",
            datafields: [

                { name: 'Description' }
            ],
            async: false
        };

    var dataAdapter = new $.jqx.dataAdapter(source);

    $(container).jqxComboBox({ selectedIndex: 0, source: dataAdapter, displayMember: "Description", valueMember: "Description", width: '99%', height: 25 });

    if (variable != '') {
        $(container).jqxComboBox('selectItem', variable);
    }

    $(container).on('select', function (event) {
        if (event.args) {
            var item = event.args.item;
        }
    });


}

function renderIsFTeable(variable, container) {
    data2 = jQuery.parseJSON('[{"Description": "Is FTeble"},{"Description": "Not FTeable"}]');

    var source =
        {
            localdata: data2,
            datatype: "json",
            datafields: [

                { name: 'Description' }
            ],
            async: false
        };

    var dataAdapter = new $.jqx.dataAdapter(source);

    $(container).jqxComboBox({ selectedIndex: 0, source: dataAdapter, displayMember: "Description", valueMember: "Description", width: '99%', height: 25 });

    if (variable != '') {
        $(container).jqxComboBox('selectItem', variable);
    }

    $(container).on('select', function (event) {
        if (event.args) {
            var item = event.args.item;
        }
    });


}

function loadGocData() {
    var listOfParams = {
        pGOC: $('#jqxGoc').val()
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/Role/getGOCData',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {

            var response = jQuery.parseJSON(json);

            $.each((response), function (index, element) {

                $('#txtMSLVL09').val(element.ManagedSegmentID + ' ' + element.ManagedSegmentName);
                $('#txtGeography').val(element.ManagedGeographyID + ' ' + element.ManagedGeographyName);
                $('#txtRegion').val(element.Region);

                $('#txtLVID').val(element.LegalVehiculeID);
                $('#txtChargeOut').val(element.Market);
            });





        }
    });

}

function saveNewGOC() {
    var listOfParams = {
        pName: $('#txtName').val(),
        pGOC: $('#jqxGoc').val(),
        pPL: $("#cbPhysicalLocation").val(),
        pDS: $("#cbDirectStaft").val(),
        pFTE: $("#cbFteable").val(),
        pRole: $('#lblReq').text().split(' ')[1]
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        url: '/Role/saveChanges',
        type: "post",
        success: function (json) {
            GetListTotalRole();
        }
    });

}

function renderPhysicalLocation(value) {

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/NonMOU/getPhysicalLocation',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = (response);

            var source =
                {
                    localdata: data2,
                    datatype: "json",
                    datafields: [
                        { name: 'ID' },
                        { name: 'Name' }
                    ],
                    async: false
                };

            var dataAdapter = new $.jqx.dataAdapter(source);

            $("#cbPhysicalLocation").jqxComboBox({ selectedIndex: 0, source: dataAdapter, displayMember: "Name", valueMember: "Name", width: '99%', height: 25 });

            if (value != '') {
                $("#cbPhysicalLocation").jqxComboBox('selectItem', value);
            }

            $("#cbPhysicalLocation").on('select', function (event) {
                if (event.args) {
                    var item = event.args.item;
                    if (item) {

                    }
                }
            });

        }
    });

}

function renderTypeValuesMOU(mou) {
    var listOfParams = {
        pMOU: mou
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/Role/getMOUDashboardFilter',
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                           { name: 'ID', type: 'string' },
                           { name: 'MOU', type: 'string' },
                           { name: 'Name', type: 'string' },
                           { name: 'ManagedGeographyID', type: 'string' },
                           { name: 'ManagedGeographyName', type: 'string' },
                           { name: 'ManagedSegmentID', type: 'string' },
                           { name: 'ManagedSegmentName', type: 'string' },
                           { name: 'ApprovalSOEID', type: 'string' },
                           { name: 'ApprovalName', type: 'string' },
                           { name: 'ApprovedDate', type: 'string' },
                           { name: 'FTE', type: 'string' },
                           { name: 'TotalRole', type: 'string' },
                           { name: 'CssDriver', type: 'string' },
                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            $('#gridInfo').jqxGrid(
           {
               width: '100%',
               columnsresize: false,
               autoheight: true,
               pagermode: 'default',
               theme: 'energyblue',
               source: dataAdapter,
               columns: [
               { text: 'MOU', dataField: 'MOU', filtertype: 'checkedlist', width: '10%', editable: false },
               { text: 'Driver', dataField: 'CssDriver', filtertype: 'checkedlist', width: '8%', editable: false },
               { text: 'Name', dataField: 'Name', filtertype: 'checkedlist', width: '16%', editable: false },
               { text: 'Managed Geography', dataField: 'ManagedGeographyName', filtertype: 'input', width: '10%', editable: false },
               { text: 'Managed Segment', dataField: 'ManagedSegmentName', filtertype: 'input', width: '10%', editable: false },
               { text: 'Approval SOEID', dataField: 'ApprovalSOEID', filtertype: 'input', width: '10%', editable: false },
               { text: 'Approval Name', dataField: 'ApprovalName', filtertype: 'input', width: '10%', editable: false },
               { text: 'Approved Date', dataField: 'ApprovedDate', filtertype: 'input', width: '10%', editable: false },
               { text: 'FTE', dataField: 'FTE', filtertype: 'input', width: '10%', editable: false },
               { text: 'Total Roles', dataField: 'TotalRole', filtertype: 'input', width: '10%', editable: false }
               ]

           });

        }
    });


}
function renderTypeValuesNONMOU(nonmou) {
    var listOfParams = {
        pNonMOU: nonmou
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        url: '/Role/getNonMOUDashboardFilter',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                           { name: 'NonMOU', type: 'string' },
                           { name: 'FTE', type: 'string' },
                           { name: 'FroFunctionsMS', type: 'string' },
                           { name: 'RequestID', type: 'string' },
                           { name: 'Status', type: 'string' },
                           { name: 'NonMOUAlias', type: 'string' },
                           { name: 'Name', type: 'string' },
                           { name: 'Type', type: 'string' },
                           { name: 'Scope', type: 'string' },
                           { name: 'Benefits', type: 'string' },
                           { name: 'FTEStandarCost', type: 'string' },
                           { name: 'PhysicalLocationID', type: 'string' },
                           { name: 'SiteManager', type: 'string' },
                           { name: 'SiteFinanceManager', type: 'string' },
                           { name: 'ManagedGeographyID', type: 'string' },
                           { name: 'ManagedGeographyName', type: 'string' },
                           { name: 'ManagedSegmentID', type: 'string' },
                           { name: 'ManagedSegmentName', type: 'string' },
                           { name: 'ChargeOutGOC', type: 'string' },
                           { name: 'ReceivingGOC', type: 'string' },
                           { name: 'TotalDirectStafft', type: 'string' },
                           { name: 'TotalTemporal', type: 'string' },
                           { name: 'CssDriver', type: 'string' },

                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);


            $('#gridInfo').jqxGrid(
           {
               width: '100%',
               columnsresize: false,
               autoheight: true,
               pagermode: 'default',
               theme: 'energyblue',
               source: dataAdapter,
               columns: [
                           { text: 'NonMOUAlias', dataField: 'NonMOUAlias', filtertype: 'checkedlist', width: '10%', editable: false },
                           { text: 'Name', dataField: 'Name', filtertype: 'checkedlist', width: '10%', editable: false },
                           { text: 'Type', dataField: 'Type', filtertype: 'checkedlist', width: '10%', editable: false },
                           { text: 'Driver', dataField: 'CssDriver', filtertype: 'checkedlist', width: '8%', editable: false },
                           { text: 'ManagedGeographyName', dataField: 'ManagedGeographyName', filtertype: 'checkedlist', width: '10%', editable: false },
                           { text: 'ManagedSegmentName', dataField: 'ManagedSegmentName', filtertype: 'checkedlist', width: '10%', editable: false },
                           { text: 'ReceivingGOC', dataField: 'ReceivingGOC', filtertype: 'checkedlist', width: '10%', editable: false },
                           { text: 'FTEStandarCost', dataField: 'FTEStandarCost', filtertype: 'checkedlist', width: '10%', editable: false },
                           { text: 'TotalDirectStafft', dataField: 'TotalDirectStafft', filtertype: 'checkedlist', width: '10%', editable: false },
                           { text: 'TotalTemporal', dataField: 'TotalTemporal', filtertype: 'checkedlist', width: '10%', editable: false },
               ]

           });

        }
    });

}

function renderTypeInfo(number, role) {
    var listOfParams = {
        pNumber: number,
        pRole: role
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        url: '/Role/getTypeInfo',
        type: "post",
        success: function (json) {

            var response = jQuery.parseJSON(json);


            $.each((response), function (index, element) {

                var DATEposibleStart = new Date(element.PosibleStartDateYear, element.PosibleStartDateMonth, 1, 1, 1, 1, 1);

                if (element.PosibleEndDateYear < 10) {
                    var DATEposibleEnd = new Date();
                }
                else {
                    var DATEposibleEnd = new Date(element.PosibleEndDateYear, element.PosibleEndDateMonth, 1, 1, 1, 1, 1);
                }
                $("#posibleStart").jqxDateTimeInput({ width: '300px', height: '25px', formatString: 'Y' });
                $("#posibleEnd").jqxDateTimeInput({ width: '300px', height: '25px', formatString: 'Y' });

                $('#posibleStart ').jqxDateTimeInput('setDate', DATEposibleStart);

                if (element.IsEndDate == true) {
                    $('#IsEndDate').attr('checked', true);
                    $("#posibleEnd").jqxDateTimeInput({ disabled: false });
                    $('#posibleEnd ').jqxDateTimeInput('setDate', DATEposibleEnd);
                } else {
                    $('#IsEndDate').attr('checked', false);
                    $("#posibleEnd").jqxDateTimeInput({ disabled: true });
                    $('#posibleEnd ').jqxDateTimeInput('setDate', DATEposibleEnd);
                }
            });


        }
    });

}
function viewDisableCB() {
    if ($('#IsEndDate').is(":checked") == true) {
        $("#posibleEnd").jqxDateTimeInput({ disabled: false });
    }
    else {
        $("#posibleEnd").jqxDateTimeInput({ disabled: true });
    }
}


function saveChangeTypeDates() {
    var listOfParams = {
        StartMonth: $('#posibleStart').jqxDateTimeInput('getDate').getMonth() + 1,
        StartYear: $('#posibleStart').jqxDateTimeInput('getDate').getFullYear(),
        EndMonth: $('#posibleEnd').jqxDateTimeInput('getDate').getMonth() + 1,
        EndYear: $('#posibleEnd').jqxDateTimeInput('getDate').getFullYear(),
        HaveEndDate: $('#IsEndDate').is(":checked"),
        pRole: $('#lblReq').text().split(' ')[1]
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Saving data",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        url: '/Role/saveChangesDates',
        type: "post",
        success: function (json) {

        }
    });


}

function renderTaleoData(Role) {

    var listOfParams = {
        pRole: Role
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/Role/getRequisitionActive',
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
                  {
                      unboundmode: true,
                      localdata: data2,
                      datafields:
                              [
                                    { name: 'ID', type: 'string' }
                                  , { name: 'Number', type: 'string' }
                                  , { name: 'Name', type: 'string' }
                                  , { name: 'DateCreated', type: 'string' }
                                  , { name: 'NumberPositions', type: 'string' }
                                  , { name: 'Clevel', type: 'string' }
                                  , { name: 'StatusID', type: 'string' }
                                  , { name: 'DaysOpen', type: 'string' }
                                  , { name: 'GOC', type: 'string' }
                                  , { name: 'Expr1', type: 'string' }
                                  , { name: 'BusinessUnit', type: 'string' }
                                  , { name: 'Business', type: 'string' }
                                  , { name: 'BusinessDetail', type: 'string' }
                                  , { name: 'ManagedGeography', type: 'string' }
                                  , { name: 'PhysicalRegion', type: 'string' }
                                  , { name: 'ManagedFunction', type: 'string' }
                                  , { name: 'OrgLevel1', type: 'string' }
                                  , { name: 'OrgLevel2', type: 'string' }
                                  , { name: 'OrgLevel3', type: 'string' }
                                  , { name: 'OrgLevel4', type: 'string' }
                                  , { name: 'OrgLevel5', type: 'string' }
                                  , { name: 'OrgLevel6', type: 'string' }
                                  , { name: 'OrgLevel7', type: 'string' }
                                  , { name: 'OrgLevel8', type: 'string' }
                                  , { name: 'OrgLevel9', type: 'string' }
                                  , { name: 'OrgLevel10', type: 'string' }
                                  , { name: 'OrgLevel11', type: 'string' }
                                  , { name: 'OrgLevel12', type: 'string' }
                                  , { name: 'MGLevel1', type: 'string' }
                                  , { name: 'MGLevel2', type: 'string' }
                                  , { name: 'MGLevel3', type: 'string' }
                                  , { name: 'MGLevel4', type: 'string' }
                                  , { name: 'MGLevel5', type: 'string' }
                                  , { name: 'MGLevel6', type: 'string' }
                                  , { name: 'MGLevel7', type: 'string' }
                                  , { name: 'CSWWorkflowName', type: 'string' }
                                  , { name: 'SourceSystem', type: 'string' }
                                  , { name: 'GOCDescription', type: 'string' }
                                  , { name: 'Recruiter', type: 'string' }
                                  , { name: 'RecruiterGEID', type: 'string' }
                                  , { name: 'RelationshipIndicator', type: 'string' }
                                  , { name: 'Requisition_', type: 'string' }
                                  , { name: 'AlternateReq_', type: 'string' }
                                  , { name: 'JobCode', type: 'string' }
                                  , { name: 'JobFunction', type: 'string' }
                                  , { name: 'RequisitionTitle', type: 'string' }
                                  , { name: 'C_ofPositions', type: 'string' }
                                  , { name: 'LefttoHire', type: 'string' }
                                  , { name: 'CurrentReqStatus', type: 'string' }
                                  , { name: 'ReqStatusDate', type: 'string' }
                                  , { name: 'FirstApprovalDate', type: 'string' }
                                  , { name: 'ReqCreationDate', type: 'string' }
                                  , { name: 'InternalPostingStatus', type: 'string' }
                                  , { name: 'InternalPostingDate', type: 'string' }
                                  , { name: 'InternalUnpostingDate', type: 'string' }
                                  , { name: 'PersonReplacing', type: 'string' }
                                  , { name: 'PersonReplacingSOEID', type: 'string' }
                                  , { name: 'EmployeeReferalBonus', type: 'string' }
                                  , { name: 'EligibleforCampsHire', type: 'string' }
                                  , { name: 'HiringManager', type: 'string' }
                                  , { name: 'HiringManagerGEID', type: 'string' }
                                  , { name: 'HRRepresentative', type: 'string' }
                                  , { name: 'FinancialJustification', type: 'string' }
                                  , { name: 'RequisitionJustification', type: 'string' }
                                  , { name: 'IsitaProductionJob', type: 'string' }
                                  , { name: 'FullTimePartTime', type: 'string' }
                                  , { name: 'OvertimeStatus', type: 'string' }
                                  , { name: 'ReqSalaryGrade', type: 'string' }
                                  , { name: 'MappedOfficerTitle', type: 'string' }
                                  , { name: 'OfficerTitle', type: 'string' }
                                  , { name: 'OfficerTitleCode', type: 'string' }
                                  , { name: 'StandardGrade', type: 'string' }
                                  , { name: 'Grade', type: 'string' }
                                  , { name: 'WorldRegion', type: 'string' }
                                  , { name: 'Country', type: 'string' }
                                  , { name: 'StateProvince', type: 'string' }
                                  , { name: 'City', type: 'string' }
                                  , { name: 'CSSCenter', type: 'string' }
                                  , { name: 'LocationCode', type: 'string' }
                                  , { name: 'CSC', type: 'string' }
                                  , { name: 'OLM', type: 'string' }
                                  , { name: 'CompanyCode', type: 'string' }
                                  , { name: 'AgeGrouping', type: 'string' }
                                  , { name: 'AccountExpenseCode', type: 'string' }
                                  , { name: 'Applications', type: 'string' }
                                  , { name: 'Internal', type: 'string' }
                                  , { name: 'External', type: 'string' }
                                  , { name: 'New', type: 'string' }
                                  , { name: 'Testing1', type: 'string' }
                                  , { name: 'Reviewed', type: 'string' }
                                  , { name: 'PhoneScreen', type: 'string' }
                                  , { name: 'Testing2', type: 'string' }
                                  , { name: 'Interview', type: 'string' }
                                  , { name: 'OfferCreation', type: 'string' }
                                  , { name: 'OfferExtended', type: 'string' }
                                  , { name: 'OfferAccepted', type: 'string' }
                                  , { name: 'Hired', type: 'string' }
                                  , { name: 'OfferDeclined', type: 'string' }
                                  , { name: 'ReqEmployeeStatus', type: 'string' }
                                  , { name: 'ReqCreatorUser', type: 'string' }
                                  , { name: 'APACLegalVehicle', type: 'string' }
                                  , { name: 'SumofOpenDaysLefttoHire', type: 'string' }
                              ],
                      datatype: "json"
                  };

            var dataAdapter = new $.jqx.dataAdapter(source);

            $('#gridTaleo').jqxGrid(
           {
               source: dataAdapter,
               theme: 'energyblue',
               autoheight: true,
               width: '100%',
               pageable: false,
               columnsresize: true,
               selectionmode: 'singlecell',
               enablebrowserselection: true,
               columns: [
                             { text: 'Number', dataField: 'Number', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'Name', dataField: 'Name', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'DateCreated', dataField: 'DateCreated', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'NumberPositions', dataField: 'NumberPositions', filtertype: 'checkedlist', editable: false, width: 150 }
                           , { text: 'Clevel', dataField: 'Clevel', filtertype: 'checkedlist', editable: false, width: 150 }
                           , { text: 'DaysOpen', dataField: 'DaysOpen', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'GOC', dataField: 'GOC', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'Status', dataField: 'Expr1', filtertype: 'checkedlist', editable: false, width: 150 }
                           , { text: 'BusinessUnit', dataField: 'BusinessUnit', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'Business', dataField: 'Business', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'BusinessDetail', dataField: 'BusinessDetail', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'ManagedGeography', dataField: 'ManagedGeography', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'PhysicalRegion', dataField: 'PhysicalRegion', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'ManagedFunction', dataField: 'ManagedFunction', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OrgLevel1', dataField: 'OrgLevel1', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OrgLevel2', dataField: 'OrgLevel2', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OrgLevel3', dataField: 'OrgLevel3', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OrgLevel4', dataField: 'OrgLevel4', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OrgLevel5', dataField: 'OrgLevel5', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OrgLevel6', dataField: 'OrgLevel6', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OrgLevel7', dataField: 'OrgLevel7', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OrgLevel8', dataField: 'OrgLevel8', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OrgLevel9', dataField: 'OrgLevel9', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'MGLevel1', dataField: 'MGLevel1', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'MGLevel2', dataField: 'MGLevel2', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'MGLevel3', dataField: 'MGLevel3', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'MGLevel4', dataField: 'MGLevel4', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'MGLevel5', dataField: 'MGLevel5', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'SourceSystem', dataField: 'SourceSystem', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'GOCDescription', dataField: 'GOCDescription', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'Recruiter', dataField: 'Recruiter', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'RecruiterGEID', dataField: 'RecruiterGEID', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'JobCode', dataField: 'JobCode', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'JobFunction', dataField: 'JobFunction', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'RequisitionTitle', dataField: 'RequisitionTitle', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'C_ofPositions', dataField: 'C_ofPositions', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'LefttoHire', dataField: 'LefttoHire', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'CurrentReqStatus', dataField: 'CurrentReqStatus', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'ReqStatusDate', dataField: 'ReqStatusDate', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'FirstApprovalDate', dataField: 'FirstApprovalDate', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'ReqCreationDate', dataField: 'ReqCreationDate', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'InternalPostingStatus', dataField: 'InternalPostingStatus', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'InternalPostingDate', dataField: 'InternalPostingDate', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'InternalUnpostingDate', dataField: 'InternalUnpostingDate', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'PersonReplacing', dataField: 'PersonReplacing', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'PersonReplacingSOEID', dataField: 'PersonReplacingSOEID', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'EmployeeReferalBonus', dataField: 'EmployeeReferalBonus', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'EligibleforCampsHire', dataField: 'EligibleforCampsHire', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'HiringManager', dataField: 'HiringManager', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'HiringManagerGEID', dataField: 'HiringManagerGEID', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'HRRepresentative', dataField: 'HRRepresentative', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'FinancialJustification', dataField: 'FinancialJustification', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'FullTimePartTime', dataField: 'FullTimePartTime', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OvertimeStatus', dataField: 'OvertimeStatus', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'ReqSalaryGrade', dataField: 'ReqSalaryGrade', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'MappedOfficerTitle', dataField: 'MappedOfficerTitle', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OfficerTitle', dataField: 'OfficerTitle', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OfficerTitleCode', dataField: 'OfficerTitleCode', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'StandardGrade', dataField: 'StandardGrade', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'Grade', dataField: 'Grade', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'WorldRegion', dataField: 'WorldRegion', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'Country', dataField: 'Country', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'StateProvince', dataField: 'StateProvince', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'City', dataField: 'City', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'CSSCenter', dataField: 'CSSCenter', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'LocationCode', dataField: 'LocationCode', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'CSC', dataField: 'CSC', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OLM', dataField: 'OLM', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'CompanyCode', dataField: 'CompanyCode', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'ReqEmployeeStatus', dataField: 'ReqEmployeeStatus', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'ReqCreatorUser', dataField: 'ReqCreatorUser', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'SumofOpenDaysLefttoHire', dataField: 'SumofOpenDaysLefttoHire', filtertype: 'input', editable: false, width: 150 }
               ]

           });

        }
    });

}

function renderMOUNMhistory(Role) {

    var listOfParams = {
        pRole: Role
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/Role/getMOUNMlistFromRole',
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
                  {
                      unboundmode: true,
                      localdata: data2,
                      datafields:
                              [

                                { name: 'ID', type: 'string' }
                                , { name: 'Alias', type: 'string' }
                                , { name: 'TYPE', type: 'string' }
                                , { name: 'NAME', type: 'string' }
                                , { name: 'MANAGEDGEOGRAPHYID', type: 'string' }
                                , { name: 'MANAGEDGEOGRAPHYNAME', type: 'string' }
                                , { name: 'MANAGEDSEGMENTID', type: 'string' }
                                , { name: 'MANAGEDSEGMENTNAME', type: 'string' }
                                , { name: 'CSSDRIVER', type: 'string' }
                                , { name: 'CreationDate', type: 'string' }

                              ],
                      datatype: "json"
                  };

            var dataAdapter = new $.jqx.dataAdapter(source);

            $('#gridHistoryMOU').jqxGrid(
           {
               source: dataAdapter,
               theme: 'energyblue',
               autoheight: true,
               width: '100%',
               pageable: false,
               columnsresize: true,
               selectionmode: 'singlecell',
               enablebrowserselection: true,
               columns: [
                                  { text: 'Date Modify Role', dataField: 'CreationDate', filtertype: 'input', editable: false, width: '10%' }
                                , { text: 'Alias', dataField: 'Alias', filtertype: 'input', editable: false, width: '10%' }
                                , { text: 'TYPE', dataField: 'TYPE', filtertype: 'input', editable: false, width: '10%' }
                                , { text: 'Name', dataField: 'NAME', filtertype: 'input', editable: false, width: '10%' }
                                , { text: 'Managed Geography', dataField: 'MANAGEDGEOGRAPHYID', filtertype: 'input', editable: false, width: '10%' }
                                , { text: 'Managed Geograph Name', dataField: 'MANAGEDGEOGRAPHYNAME', filtertype: 'input', editable: false, width: '10%' }
                                , { text: 'Managed Segment', dataField: 'MANAGEDSEGMENTID', filtertype: 'input', editable: false, width: '10%' }
                                , { text: 'Managed Segment Name', dataField: 'MANAGEDSEGMENTNAME', filtertype: 'input', editable: false, width: '10%' }
                                , { text: 'CSS Driver', dataField: 'CSSDRIVER', filtertype: 'input', editable: false, width: '10%' }

               ]

           });

        }
    });

}



function renderGocListRole() {


    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/Role/getGOCRoleList',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
                  {
                      unboundmode: true,
                      localdata: data2,
                      datafields:
                              [
                               { name: 'ManagedGeographyID', type: 'string' }
                              , { name: 'ManagedGeographyName', type: 'string' }
                              , { name: 'GOCID', type: 'string' }
                              , { name: 'GOCName', type: 'string' }
                              , { name: 'ManagedSegmentID', type: 'string' }
                              , { name: 'ManagedSegmentName', type: 'string' }
                              , { name: 'LegalVehiculeID', type: 'string' }
                              , { name: 'LegalVehiculeName', type: 'string' }
                              , { name: 'ManagedGeographyName_L02', type: 'string' }
                              , { name: 'Region', type: 'string' }
                              , { name: 'ManagedSegmentName_L06', type: 'string' }
                              , { name: 'CustomerManagedSegment', type: 'string' }
                              , { name: 'ManagedSegmentName_L09', type: 'string' }
                              , { name: 'L07', type: 'string' }
                              , { name: 'Market', type: 'string' }
                              , { name: 'TotalRoles', type: 'number' }
                              ],
                      datatype: "json"
                  };

            var dataAdapter = new $.jqx.dataAdapter(source);

            var initrowdetails = function (index, parentElement, gridElement, record) {
                var id = record.uid.toString();

                var grid = $($(parentElement).children()[0]);

                var nestedData;

                var listOfParams = {
                    pGOC: record.GOCID,
                }

                _callServer({
                    loadingMsg: "", async: false,
                    url: '/Role/getTotalRolesListGOC',
                    data: {
                        'data': JSON.stringify(listOfParams)
                    },
                    type: "post",
                    success: function (json) {
                        nestedData = jQuery.parseJSON(json);

                        var orderssource = {
                            localdata: nestedData,
                            datafields:
                               [
                               { name: 'ID', type: 'string' },
                              , { name: 'RoleID', type: 'string' },
                              , { name: 'RoleTypeID', type: 'string' },
                              , { name: 'FromType', type: 'string' },
                              , { name: 'Number', type: 'string' },
                              , { name: 'IDType', type: 'string' },
                              , { name: 'CreationDate', type: 'string' },
                              , { name: 'CreatedBy', type: 'string' },
                              , { name: 'IsActive', type: 'string' },
                              , { name: 'ModifyBy', type: 'string' },
                              , { name: 'ModifyDate', type: 'string' },
                              , { name: 'Name', type: 'string' },
                              , { name: 'Description', type: 'string' },
                              , { name: 'GOCID', type: 'string' },
                              , { name: 'MSL09', type: 'string' },
                              , { name: 'Process', type: 'string' },
                              , { name: 'SubProcess', type: 'string' },
                              , { name: 'MgdGeaography', type: 'string' },
                              , { name: 'MgdRegion', type: 'string' },
                              , { name: 'MgdCountry', type: 'string' },
                              , { name: 'LVID', type: 'string' },
                              , { name: 'PhysicalLocationID', type: 'string' },
                              , { name: 'ChargeOutProfileID', type: 'string' },
                              , { name: 'RoleIsActive', type: 'string' },
                              , { name: 'RoleCreaetedBy', type: 'string' },
                              , { name: 'CreatedDate', type: 'string' },
                              , { name: 'LastUpdateBy', type: 'string' },
                              , { name: 'LastUpdateDate', type: 'date' },
                              , { name: 'StatusID', type: 'string' },
                              , { name: 'IsFteable', type: 'string' },
                              , { name: 'IsDirectStaf', type: 'string' },
                              , { name: 'FTEPercentage', type: 'string' },
                              , { name: 'Status', type: 'string' },
                              , { name: 'ManagedSegment', type: 'string' },
                              , { name: 'ManagedGeography', type: 'string' },
                              , { name: 'PhysicalLocation', type: 'string' },
                              , { name: 'ChargeOutProfile', type: 'string' }
                              , { name: 'RoleAuto', type: 'string' }
                              , { name: 'GOCName', type: 'string' }
                               ],
                            datatype: "json"
                        };

                        var nestedGridAdapter = new $.jqx.dataAdapter(orderssource);
                        nestedGridAdapter.dataBind();

                        if (grid != null) {

                            grid.jqxGrid({
                                source: nestedGridAdapter,
                                theme: 'energyblue',
                                width: '95%',
                                autoheight: true,
                                autorowheight: true,
                                pagesize: 5,
                                pageable: true,
                                filterable: true,
                                showfilterrow: true,
                                columnsresize: true,
                                enablebrowserselection: true,
                                columns: [
                                { text: 'Center', dataField: 'PhysicalLocation', filtertype: 'checkedlist', width: '9%', editable: false },
                                { text: 'Status', dataField: 'Status', filtertype: 'checkedlist', width: '9%', editable: false },
                                { text: 'Source', dataField: 'FromType', filtertype: 'checkedlist', width: '9%', editable: false },
                           //     { text: 'MOU/Non MOU ID#', dataField: 'Number', columntype: 'textbox', filtercondition: 'starts_with', width: '9%', editable: false, cellsrenderer: imgViewSource },
                                { text: 'Role ID', dataField: 'RoleAuto', columntype: 'textbox', filtercondition: 'starts_with', width: '9%', editable: false },
                                //{ text: 'FTE Type', columntype: 'textbox', filtercondition: 'starts_with', width: '9%', editable: false, cellsrenderer: imgDSTemp },
                                { text: 'GOC', dataField: 'GOCName', filtertype: 'input', width: '9%', editable: false },
                                { text: 'Managed Segment L09', dataField: 'MSL09', filtertype: 'input', width: '9%', editable: false },
                                { text: 'Managed Geography', dataField: 'MgdGeaography', filtertype: 'input', width: '9%', editable: false },
                                { text: 'Last Update By', dataField: 'LastUpdateBy', filtertype: 'input', width: '8%', editable: false },
                                { text: 'Last Update Date', dataField: 'LastUpdateDate', cellsformat: 'd', filtertype: 'imput', width: '8%', editable: false }

                                ]

                            });

                        }
                    }
                });
            };

            var imgAddRole = function (row, datafield, value) {

                var rowData = $('#jqxGridRolesByGOC').jqxGrid('getrowdata', row);

                //return '<a id="A1" class="btn btn-small btn-info" data-toggle="tooltip" title="Add new Role" onclick="AddNewRole(' + row + ')"><i class="fa fa-plus"></i></a>';
                return '<button type="button" class="btn btn-secondary btn-icon bottom15 right15"  onClick="AddNewRole(' + row + ')"><i class="fa fa-plus"></i> &nbsp; <span>Add</span></button>';
                //return '<i class="fa fa-plus-circle" style=" font-size: 17px; color: #6ab53c; margin-left:4px; margin-top:4px; cursor: pointer;" onClick="AddNewRole(' + row + ')"> Add</i>';
            }


            $('#jqxGridRolesByGOC').jqxGrid(
           {
               width: '95%',
               rowsheight: 40,
               columnsresize: false,
               pagesize: 10,
               pageable: true,
               autoheight: true,
               filterable: true,
               pagermode: 'default',
               theme: 'energyblue',
               source: dataAdapter,
               rowdetails: true,
               showfilterrow: true,
               enablebrowserselection: true,
               initrowdetails: initrowdetails,
               rowdetailstemplate: { rowdetails: "<div id='grid' style='margin: 0px;top:5px;'></div>", rowdetailsheight: 400, rowdetailshidden: true },
               columns: [
                                { text: '', filtertype: 'none', width: '5%', columntype: 'image', pinned: true, cellsrenderer: imgAddRole, width: 90 },
                              , { text: 'Total Roles', dataField: 'TotalRoles', filtertype: 'number', editable: false, width: 70 }
                              , { text: 'Managed GeographyI D', dataField: 'ManagedGeographyID', filtertype: 'input', editable: false, width: 70 }
                              , { text: 'Managed Geography Name', dataField: 'ManagedGeographyName', filtertype: 'input', editable: false, width: 100 }
                              , { text: 'GOC ID', dataField: 'GOCID', filtertype: 'input', editable: false, width: 100 }
                              , { text: 'GOC Name', dataField: 'GOCName', filtertype: 'input', editable: false, width: 250 }
                              , { text: 'Managed Segment ID', dataField: 'ManagedSegmentID', filtertype: 'input', editable: false, width: 70 }
                              , { text: 'Managed Segment Name', dataField: 'ManagedSegmentName', filtertype: 'input', editable: false, width: 250 }
                              , { text: 'Legal Vehicule ID', dataField: 'LegalVehiculeID', filtertype: 'input', editable: false, width: 70 }
                              , { text: 'Legal Vehicule Name', dataField: 'LegalVehiculeName', filtertype: 'input', editable: false, width: 250 }
                              , { text: 'Managed Geography Name_L02', dataField: 'ManagedGeographyName_L02', filtertype: 'input', editable: false, width: 150 }
                              , { text: 'Region', dataField: 'Region', filtertype: 'input', editable: false, width: 150 }
                              , { text: 'Managed Segment Name_L06', dataField: 'ManagedSegmentName_L06', filtertype: 'input', editable: false, width: 150 }
                              , { text: 'Customer Managed Segment', dataField: 'CustomerManagedSegment', filtertype: 'input', editable: false, width: 150 }
                              , { text: 'Managed SegmentName_ L09', dataField: 'ManagedSegmentName_L09', filtertype: 'input', editable: false, width: 150 }
                              , { text: 'L07', dataField: 'L07', filtertype: 'input', editable: false, width: 150 }
                              , { text: 'Market', dataField: 'Market', filtertype: 'input', editable: false, width: 150 }
                              ,
               ]

           });

        }
    });

}

function fntLoadRolesInGOC() {
    viewSelectedGrid('#viewNonMOU');
    renderGocListRole();
    $('#AddRoleByGoc').hide();
    $("#jqxExpanderRoleByGOC").jqxExpander({ expanded: true });
}

function LinkNewMOUNM() {
    $('html,body').scrollTop(0);
    $("#popUpMOUNMList").jqxWindow('open');

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/Role/getMOUNMList',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                       { name: 'ID', type: 'string' },
                     , { name: 'Alias', type: 'string' },
                     , { name: 'TYPE', type: 'string' },
                     , { name: 'NAME', type: 'string' },
                     , { name: 'MANAGEDGEOGRAPHYID', type: 'string' },
                     , { name: 'MANAGEDGEOGRAPHYNAME', type: 'string' },
                     , { name: 'MANAGEDSEGMENTID', type: 'string' },
                     , { name: 'MANAGEDSEGMENTNAME', type: 'string' },
                     , { name: 'CSSDRIVER', type: 'string' }
                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);


            var imgAddRole = function (row, datafield, value) {
                return '<center><input type="button" value="Select" class="btn btn-success" onclick="addSelectedMOUtoRole(' + row + ')"/></center>';
            }


            $('#jqxGridListMOUNM').jqxGrid(
           {
               source: dataAdapter,
               theme: 'energyblue',
               width: '95%',
               selectionmode: 'multiplecellsadvanced',
               autoheight: true,
               pagesize: 10,
               pageable: true,
               filterable: true,
               showfilterrow: true,
               columnsresize: true,
               //         selectionmode: 'singlecell',
               enablebrowserselection: true,
               columns: [
                       { text: '', width: '10%', editable: false, cellsrenderer: imgAddRole }
                     , { text: 'Alias', dataField: 'Alias', filtertype: 'checkedlist', width: '10%', editable: false },
                     , { text: 'TYPE', dataField: 'TYPE', filtertype: 'checkedlist', width: '10%', editable: false },
                     , { text: 'NAME', dataField: 'NAME', filtertype: 'checkedlist', width: '10%', editable: false },
                     , { text: 'MANAGED GEOGRAPHY ID', dataField: 'MANAGEDGEOGRAPHYID', filtertype: 'checkedlist', width: '10%', editable: false },
                     , { text: 'MANAGED GEOGRAPHY NAME', dataField: 'MANAGEDGEOGRAPHYNAME', filtertype: 'checkedlist', width: '10%', editable: false },
                     , { text: 'MANAGED SEGMENT ID', dataField: 'MANAGEDSEGMENTID', filtertype: 'checkedlist', width: '10%', editable: false },
                     , { text: 'MANAGEDS EGMENT NAME', dataField: 'MANAGEDSEGMENTNAME', filtertype: 'checkedlist', width: '10%', editable: false },
                     , { text: 'CSS DRIVER', dataField: 'CSSDRIVER', filtertype: 'checkedlist', width: '10%', editable: false }

               ]

           });

        }
    });


}

function addSelectedMOUtoRole(row) {
    //Add New MOU NON MOU TO ONE EXISTING ROLE

    var rowData = $('#jqxGridListMOUNM').jqxGrid('getrowdata', row);

    rowData.Alias;

    var listOfParams = {
        pMOUNMnumber: rowData.Alias,
        pRole: $('#lblReq').text().split(' ')[1]
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        url: '/Role/LinkageNewMOU',
        type: "post",
        success: function (json) {

            GetListTotalRole();

            if (rowData.Alias.substring(0, 2) == "NM") {
                renderTypeValuesNONMOU(rowData.Alias);
            }
            else {
                renderTypeValuesMOU(rowData.Alias);
            }


            renderMOUNMhistory($('#lblReq').text().split(' ')[1]);


        }
    });


}

function AddNewRole(row) {
    $("#jqxExpanderRoleByGOC").jqxExpander({ expanded: false });

    $('#AddRoleByGoc').show();
    var rowData = $('#jqxGridRolesByGOC').jqxGrid('getrowdata', row);


    findGocMethod('#jqxGocAR', '100%');

    $('#jqxGocAR').on('select',
    function () {
        loadGocDataAddNewRole();
    });


    $('#jqxGocAR').val(rowData.GOCName);

    loadGocDataAddNewRole();

    renderPhysicalLocationAddNewRole('');

    renderDirectStaff('Direct Staff', '#cbDirectStaftAR');

    renderIsFTeable('Is FTeble', "#cbFteableAR");

    $('#txtStatusAR').val('Vacant');

    $("#TotalRoles").jqxNumberInput({ width: '50px', height: '25px', spinButtons: true, decimalDigits: 0, max: 50, min: 1, digits: 2 });
    $('#TotalRoles').val(1);
}

function loadGocDataAddNewRole() {
    var listOfParams = {
        pGOC: $('#jqxGocAR').val()
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/Role/getGOCData',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {

            var response = jQuery.parseJSON(json);

            $.each((response), function (index, element) {

                $('#txtMSLVL09AR').val(element.ManagedSegmentID + ' ' + element.ManagedSegmentName);
                $('#txtGeographyAR').val(element.ManagedGeographyID + ' ' + element.ManagedGeographyName);
                $('#txtRegionAR').val(element.Region);

                $('#txtLVIDAR').val(element.LegalVehiculeID);
                $('#txtChargeOutAR').val(element.Market);
            });





        }
    });

}

function renderPhysicalLocationAddNewRole(value) {

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/NonMOU/getPhysicalLocation',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = (response);

            var source =
                {
                    localdata: data2,
                    datatype: "json",
                    datafields: [
                        { name: 'ID' },
                        { name: 'Name' }
                    ],
                    async: false
                };

            var dataAdapter = new $.jqx.dataAdapter(source);

            $("#cbPhysicalLocationAR").jqxComboBox({ selectedIndex: 0, source: dataAdapter, displayMember: "Name", valueMember: "Name", width: '99%', height: 25 });

            if (value != '') {
                $("#cbPhysicalLocationAR").jqxComboBox('selectItem', value);
            }

            $("#cbPhysicalLocationAR").on('select', function (event) {
                if (event.args) {
                    var item = event.args.item;
                    if (item) {

                    }
                }
            });

        }
    });

}

function AddNewRoleInput() {

    var listOfParams = {
        pName: $('#txtNameAR').val(),
        pGOC: $('#jqxGocAR').val(),
        pPL: $("#cbPhysicalLocationAR").val(),
        pDS: $("#cbDirectStaftAR").val(),
        pFTE: $("#cbFteableAR").val(),
        pTotal: $('#TotalRoles').val()
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        url: '/Role/saveNewRoles',
        type: "post",
        success: function (json) {
            fntLoadRolesInGOC();
        }
    });


}

function closeViewRoleAR() {
    $("#jqxExpanderRoleByGOC").jqxExpander({ expanded: true });
    $('#AddRoleByGoc').hide();



}

function LinkageRoleWithPopUp() {

    $("#popUpRequisition").jqxWindow('open');


}

function saveState()
{
    state = $("#jqxGridRoles").jqxGrid('savestate');

}
function loadState()
{
    if (state) {
        $("#jqxGridRoles").jqxGrid('loadstate', state);
    }
    else {
        $("#jqxGridRoles").jqxGrid('loadstate');
    }
}


function downloadExcel()
{
    

    var sql = 'select * from VWR_AllRoleData';

            _downloadExcel({
                sql: sql,
                filename: _createCustomID() + "_RoleTracker.xls",
                success: {
                    msg: "Generating report... Please Wait, this operation may take some time to complete."
                }
            });


}


function downloadExcelReportExample() {


    var sql = 'exec Titan.dbo.procR_ReportGuillermoData';

    _downloadExcel({
        sql: sql,
        filename: _createCustomID() + "_RoleTracker.xls",
        success: {
            msg: "Generating report... Please Wait, this operation may take some time to complete."
        }
    });



}

function downloadExcelReportLastMonths() {


    var sql = 'exec Titan.dbo.procR_ReportLastMonth';

    _downloadExcel({
        sql: sql,
        filename: _createCustomID() + "_RoleTracker.xls",
        success: {
            msg: "Generating report... Please Wait, this operation may take some time to complete."
        }
    });



}