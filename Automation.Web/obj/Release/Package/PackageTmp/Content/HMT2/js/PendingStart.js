﻿var rowid;

$(document).ready(function () {
    _hideMenu();

    XLSX.createJsXlsx({
        idElement: "js-xlsx-plugin",
        fileInput: {
            dropText: "Drop an Excel file here...",
            hideDrop: false,
            inputText: "... or click here to select a file"
        },
        onSuccess: function (jsonData) {

            var excelSource = jsonData;

            _callServer({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading data...",
                url: '/PendingStart/uploadReenFile',
                data: {
                    'data': JSON.stringify(excelSource["Pending Start"])
                },
                type: "post",
                success: function (json) {
                    renderGridDataSelectedFile(0);
                }
            });
        }
    });


    renderVersion();

    renderGridDataSelectedFile(0);


});

function renderVersion() {

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "",
        url: '/PendingStart/getVersionList',
        type: "post",
        success: function (json) {

            result = jQuery.parseJSON(json);


            var source =
                {
                    localdata: result,
                    datafields:
                    [
                        { name: 'ID', type: 'string' },
                        { name: 'UploadBy', type: 'string' },
                        { name: 'UploadDate', type: 'string' },
                        { name: 'Enable', type: 'Boolean' },
                    ],
                    datatype: "array",
                    updaterow: function (rowid, rowdata) {
                    }
                };

            var dataAdapter = new $.jqx.dataAdapter(source);

            var imgEnable = function (row, datafield, value) {

                var rowData = $('#jqxgridVersion').jqxGrid('getrowdata', row);

                if (rowData.Enable == "true") {
                    return 'Selected File'
                }
                else {
                    return 'Unselected'
                }


            }

            $("#jqxgridVersion").jqxGrid(
             {
                 width: 600,
                 source: dataAdapter,
                 pageable: true,
                 autoheight: true,
                 columnsresize: true,
                 columns: [
                   { text: 'Upload ID', columntype: 'textbox', datafield: 'ID', width: '3%' },
                   { text: 'Upload By', columntype: 'textbox', datafield: 'UploadBy', width: '31%' },
                   { text: 'Upload Date', columntype: 'textbox', datafield: 'UploadDate', width: '31%' },
                   { text: 'Enable', columntype: 'textbox', cellsrenderer: imgEnable, width: '31%' }
                 ]
             });

            $("#jqxVersion").jqxDropDownButton({ width: 400, height: 25 });


            //Salvar nueva seleccion
            $("#jqxgridVersion").on('rowselect', function (event) {
                event.stopImmediatePropagation();
                var args = event.args;
                var row = $("#jqxgridVersion").jqxGrid('getrowdata', args.rowindex);
                var dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;">' + row['ID'] + ' | ' + row['UploadBy'] + ' | ' + row['UploadDate'] + '</div>';
                $("#jqxVersion").jqxDropDownButton('setContent', dropDownContent);

                rowid = row['ID'];
            });


            $.each((result), function (index, element) {
                if (element.Enable) {
                    var dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;">' + element.ID + ' | ' + element.UploadBy + ' | ' + element.UploadDate + '</div>';
                    $("#jqxVersion").jqxDropDownButton('setContent', dropDownContent);

                    rowid = element.ID;
                }
            });

        }



    });


}

function renderGridDataSelectedFile(ID) {
    var listOfParams = {
        pFile: ID
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/PendingStart/getListReeng',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {

            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                       { name: 'ID', type: 'string' },
                       { name: 'UploadID', type: 'string' },
                        { name: 'Business', type: 'string' },
                        { name: 'SubBusiness', type: 'string' },
                       { name: 'ManagedGeograph', type: 'string' },
                       { name: 'PhysicalRegion', type: 'string' },
                       { name: 'ManagedFunction', type: 'string' },
                       { name: 'OrgLevel1', type: 'string' },
                       { name: 'OrgLevel2', type: 'string' },
                       { name: 'OrgLevel3', type: 'string' },
                       { name: 'OrgLevel4', type: 'string' },
                       { name: 'OrgLevel5', type: 'string' },
                       { name: 'OrgLevel6', type: 'string' },
                       { name: 'OrgLevel7', type: 'string' },
                       { name: 'OrgLevel8', type: 'string' },
                       { name: 'OrgLevel9', type: 'string' },
                       { name: 'OrgLevel10', type: 'string' },
                       { name: 'OrgLevel11', type: 'string' },
                       { name: 'OrgLevel12', type: 'string' },
                       { name: 'MGLevel1', type: 'string' },
                       { name: 'MGLevel2', type: 'string' },
                       { name: 'MGLevel3', type: 'string' },
                       { name: 'MGLevel4', type: 'string' },
                       { name: 'CSWWorkflowName', type: 'string' },
                       { name: 'SourceSystem', type: 'string' },
                       { name: 'GOC', type: 'string' },
                       { name: 'GOCDescription', type: 'string' },
                       { name: 'Recruiter', type: 'string' },
                       { name: 'RecruiterGEID', type: 'string' },
                       { name: 'Requisition', type: 'string' },
                       { name: 'AlternateReq', type: 'string' },
                       { name: 'ReqSalaryGrade', type: 'string' },
                       { name: 'JobCode', type: 'string' },
                       { name: 'JobFunction', type: 'string' },
                       { name: 'RequisitionTitle', type: 'string' },
                       { name: 'FirstApprovalDate', type: 'string' },
                       { name: 'InternalPostingStatus', type: 'string' },
                       { name: 'ExternalPostingStatus', type: 'string' },
                       { name: 'PersonReplacing', type: 'string' },
                       { name: 'PersonReplacingSOEID', type: 'string' },
                       { name: 'HiringManager', type: 'string' },
                       { name: 'HiringManagerGEID', type: 'string' },
                       { name: 'HRRepresentative', type: 'string' },
                       { name: 'FinanceJustification', type: 'string' },
                       { name: 'RequisitionJustification', type: 'string' },
                       { name: 'IsitaProductionJob', type: 'string' },
                       { name: 'FulltimeParttime', type: 'string' },
                       { name: 'EligibleforCampusHire', type: 'string' },
                       { name: 'OvertimeStatus', type: 'string' },
                       { name: 'MappedOfficerTitle', type: 'string' },
                       { name: 'OfficerTitle', type: 'string' },
                       { name: 'OfficerTitleCode', type: 'string' },
                       { name: 'MappedStandardGrade', type: 'string' },
                       { name: 'Grade', type: 'string' },
                       { name: 'WorldRegion', type: 'string' },
                       { name: 'Country', type: 'string' },
                       { name: 'StateProvince', type: 'string' },
                       { name: 'City', type: 'string' },
                       { name: 'CSSCenter', type: 'string' },
                       { name: 'LocationCode', type: 'string' },
                       { name: 'TimetoStart', type: 'string' },
                       { name: 'TimetoOfferAccept', type: 'string' },
                       { name: 'TimetoOfferExtend', type: 'string' },
                       { name: 'CompanyCode', type: 'string' },
                       { name: 'AccountExpenseCode', type: 'string' },
                       { name: 'JobOfferIsTentative', type: 'string' },
                       { name: 'StartMonth', type: 'string' },
                       { name: 'StartDate', type: 'string' },
                       { name: 'StartPendingStarts', type: 'string' },
                       { name: 'OfferAcceptDate', type: 'string' },
                       { name: 'JobOfferExtendedDate', type: 'string' },
                       { name: 'CandidateID', type: 'string' },
                       { name: 'CandidatesName', type: 'string' },
                       { name: 'GEID', type: 'string' },
                       { name: 'SOEID', type: 'string' },
                       { name: 'ApplicationCurrentCSWStatus', type: 'string' },
                       { name: 'CurrentSalaryGradeInternalCandidatesonly', type: 'string' },
                       { name: 'InternalExternal', type: 'string' },
                       { name: 'GeneralSource', type: 'string' },
                       { name: 'MajorSourceApplication', type: 'string' },
                       { name: 'MinorSourceApplication', type: 'string' },
                       { name: 'MajorSourceOffer', type: 'string' },
                       { name: 'MinorSourceOffer', type: 'string' },
                       { name: 'PriorEmployer', type: 'string' },
                       { name: 'RecruitingAgencyCost', type: 'string' },
                       { name: 'RecentCollegeGraduate', type: 'string' },
                       { name: 'OtherFees', type: 'string' },
                       { name: 'CSC', type: 'string' },
                       { name: 'APACLegalVehicle', type: 'string' }
                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            $('#jqxGridLastFile').jqxGrid(
           {
               source: dataAdapter,
               theme: 'blackberry',
               width: '100%',
               // autoheight: true,
               // autorowheight: true,
               pagesize: 10,
               pageable: true,
               filterable: true,
               showfilterrow: true,
               columnsresize: true,
               selectionmode: 'singlecell',
               enablebrowserselection: true,
               columns: [
                  { text: 'UploadID', dataField: 'UploadID', editable: false, width: '10%' }
                , { text: 'Business', dataField: 'Business', editable: false, width: '10%' }
                , { text: 'SubBusiness', dataField: 'SubBusiness', editable: false, width: '10%' }
                , { text: 'ManagedGeograph', dataField: 'ManagedGeograph', editable: false, width: '10%' }
                , { text: 'PhysicalRegion', dataField: 'PhysicalRegion', editable: false, width: '10%' }
                , { text: 'ManagedFunction', dataField: 'ManagedFunction', editable: false, width: '10%' }
                , { text: 'OrgLevel1', dataField: 'OrgLevel1', editable: false, width: '10%' }
                , { text: 'OrgLevel2', dataField: 'OrgLevel2', editable: false, width: '10%' }
                , { text: 'OrgLevel3', dataField: 'OrgLevel3', editable: false, width: '10%' }
                , { text: 'OrgLevel4', dataField: 'OrgLevel4', editable: false, width: '10%' }
                , { text: 'OrgLevel5', dataField: 'OrgLevel5', editable: false, width: '10%' }
                , { text: 'OrgLevel6', dataField: 'OrgLevel6', editable: false, width: '10%' }
                , { text: 'OrgLevel7', dataField: 'OrgLevel7', editable: false, width: '10%' }
                , { text: 'OrgLevel8', dataField: 'OrgLevel8', editable: false, width: '10%' }
                , { text: 'OrgLevel9', dataField: 'OrgLevel9', editable: false, width: '10%' }
                , { text: 'OrgLevel10', dataField: 'OrgLevel10', editable: false, width: '10%' }
                , { text: 'OrgLevel11', dataField: 'OrgLevel11', editable: false, width: '10%' }
                , { text: 'OrgLevel12', dataField: 'OrgLevel12', editable: false, width: '10%' }
                , { text: 'MGLevel1', dataField: 'MGLevel1', editable: false, width: '10%' }
                , { text: 'MGLevel2', dataField: 'MGLevel2', editable: false, width: '10%' }
                , { text: 'MGLevel3', dataField: 'MGLevel3', editable: false, width: '10%' }
                , { text: 'MGLevel4', dataField: 'MGLevel4', editable: false, width: '10%' }
                , { text: 'CSWWorkflowName', dataField: 'CSWWorkflowName', editable: false, width: '10%' }
                , { text: 'SourceSystem', dataField: 'SourceSystem', editable: false, width: '10%' }
                , { text: 'GOC', dataField: 'GOC', editable: false, width: '10%' }
                , { text: 'GOCDescription', dataField: 'GOCDescription', editable: false, width: '10%' }
                , { text: 'Recruiter', dataField: 'Recruiter', editable: false, width: '10%' }
                , { text: 'RecruiterGEID', dataField: 'RecruiterGEID', editable: false, width: '10%' }
                , { text: 'Requisition', dataField: 'Requisition', editable: false, width: '10%' }
                , { text: 'AlternateReq', dataField: 'AlternateReq', editable: false, width: '10%' }
                , { text: 'ReqSalaryGrade', dataField: 'ReqSalaryGrade', editable: false, width: '10%' }
                , { text: 'JobCode', dataField: 'JobCode', editable: false, width: '10%' }
                , { text: 'JobFunction', dataField: 'JobFunction', editable: false, width: '10%' }
                , { text: 'RequisitionTitle', dataField: 'RequisitionTitle', editable: false, width: '10%' }
                , { text: 'FirstApprovalDate', dataField: 'FirstApprovalDate', editable: false, width: '10%' }
                , { text: 'InternalPostingStatus', dataField: 'InternalPostingStatus', editable: false, width: '10%' }
                , { text: 'ExternalPostingStatus', dataField: 'ExternalPostingStatus', editable: false, width: '10%' }
                , { text: 'PersonReplacing', dataField: 'PersonReplacing', editable: false, width: '10%' }
                , { text: 'PersonReplacingSOEID', dataField: 'PersonReplacingSOEID', editable: false, width: '10%' }
                , { text: 'HiringManager', dataField: 'HiringManager', editable: false, width: '10%' }
                , { text: 'HiringManagerGEID', dataField: 'HiringManagerGEID', editable: false, width: '10%' }
                , { text: 'HRRepresentative', dataField: 'HRRepresentative', editable: false, width: '10%' }
                , { text: 'FinanceJustification', dataField: 'FinanceJustification', editable: false, width: '10%' }
                , { text: 'RequisitionJustification', dataField: 'RequisitionJustification', editable: false, width: '10%' }
                , { text: 'IsitaProductionJob', dataField: 'IsitaProductionJob', editable: false, width: '10%' }
                , { text: 'FulltimeParttime', dataField: 'FulltimeParttime', editable: false, width: '10%' }
                , { text: 'EligibleforCampusHire', dataField: 'EligibleforCampusHire', editable: false, width: '10%' }
                , { text: 'OvertimeStatus', dataField: 'OvertimeStatus', editable: false, width: '10%' }
                , { text: 'MappedOfficerTitle', dataField: 'MappedOfficerTitle', editable: false, width: '10%' }
                , { text: 'OfficerTitle', dataField: 'OfficerTitle', editable: false, width: '10%' }
                , { text: 'OfficerTitleCode', dataField: 'OfficerTitleCode', editable: false, width: '10%' }
                , { text: 'MappedStandardGrade', dataField: 'MappedStandardGrade', editable: false, width: '10%' }
                , { text: 'Grade', dataField: 'Grade', editable: false, width: '10%' }
                , { text: 'WorldRegion', dataField: 'WorldRegion', editable: false, width: '10%' }
                , { text: 'Country', dataField: 'Country', editable: false, width: '10%' }
                , { text: 'StateProvince', dataField: 'StateProvince', editable: false, width: '10%' }
                , { text: 'City', dataField: 'City', editable: false, width: '10%' }
                , { text: 'CSSCenter', dataField: 'CSSCenter', editable: false, width: '10%' }
                , { text: 'LocationCode', dataField: 'LocationCode', editable: false, width: '10%' }
                , { text: 'TimetoStart', dataField: 'TimetoStart', editable: false, width: '10%' }
                , { text: 'TimetoOfferAccept', dataField: 'TimetoOfferAccept', editable: false, width: '10%' }
                , { text: 'TimetoOfferExtend', dataField: 'TimetoOfferExtend', editable: false, width: '10%' }
                , { text: 'CompanyCode', dataField: 'CompanyCode', editable: false, width: '10%' }
                , { text: 'AccountExpenseCode', dataField: 'AccountExpenseCode', editable: false, width: '10%' }
                , { text: 'JobOfferIsTentative', dataField: 'JobOfferIsTentative', editable: false, width: '10%' }
                , { text: 'StartMonth', dataField: 'StartMonth', editable: false, width: '10%' }
                , { text: 'StartDate', dataField: 'StartDate', editable: false, width: '10%' }
                , { text: 'StartPendingStarts', dataField: 'StartPendingStarts', editable: false, width: '10%' }
                , { text: 'OfferAcceptDate', dataField: 'OfferAcceptDate', editable: false, width: '10%' }
                , { text: 'JobOfferExtendedDate', dataField: 'JobOfferExtendedDate', editable: false, width: '10%' }
                , { text: 'CandidateID', dataField: 'CandidateID', editable: false, width: '10%' }
                , { text: 'CandidatesName', dataField: 'CandidatesName', editable: false, width: '10%' }
                , { text: 'GEID', dataField: 'GEID', editable: false, width: '10%' }
                , { text: 'SOEID', dataField: 'SOEID', editable: false, width: '10%' }
                , { text: 'ApplicationCurrentCSWStatus', dataField: 'ApplicationCurrentCSWStatus', editable: false, width: '10%' }
                , { text: 'CurrentSalaryGradeInternalCandidatesonly', dataField: 'CurrentSalaryGradeInternalCandidatesonly', editable: false, width: '10%' }
                , { text: 'InternalExternal', dataField: 'InternalExternal', editable: false, width: '10%' }
                , { text: 'GeneralSource', dataField: 'GeneralSource', editable: false, width: '10%' }
                , { text: 'MajorSourceApplication', dataField: 'MajorSourceApplication', editable: false, width: '10%' }
                , { text: 'MinorSourceApplication', dataField: 'MinorSourceApplication', editable: false, width: '10%' }
                , { text: 'MajorSourceOffer', dataField: 'MajorSourceOffer', editable: false, width: '10%' }
                , { text: 'MinorSourceOffer', dataField: 'MinorSourceOffer', editable: false, width: '10%' }
                , { text: 'PriorEmployer', dataField: 'PriorEmployer', editable: false, width: '10%' }
                , { text: 'RecruitingAgencyCost', dataField: 'RecruitingAgencyCost', editable: false, width: '10%' }
                , { text: 'RecentCollegeGraduate', dataField: 'RecentCollegeGraduate', editable: false, width: '10%' }
                , { text: 'OtherFees', dataField: 'OtherFees', editable: false, width: '10%' }
                , { text: 'CSC', dataField: 'CSC', editable: false, width: '10%' }
                , { text: 'APACLegalVehicle', dataField: 'APACLegalVehicle', editable: false, width: '10%' }




               ]
           });


        }
    });


}

function viewSelected() {
    renderGridDataSelectedFile(rowid);
}


function useForThisMonth() {
    var listOfParams = {
        pFile: rowid
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Set selection as default file...",
        url: '/PendingStart/setAsDefault',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            renderGridDataSelectedFile(rowid);
            renderVersion();
        }
    });

}