﻿var rowid;

$(document).ready(function () {
    _hideMenu();

    XLSX.createJsXlsx({
        idElement: "js-xlsx-plugin",
        fileInput: {
            dropText: "Drop an Excel file here...",
            hideDrop: false,
            inputText: "... or click here to select a file"
        },
        onSuccess: function (jsonData) {

            var excelSource = jsonData;

            _callServer({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading data...",
                url: '/Attrition/uploadReenFile',
                data: {
                    'data': JSON.stringify(excelSource["Details"])
                },
                type: "post",
                success: function (json) {
                    renderGridDataSelectedFile(0);
                }
            });
        }
    });


    renderVersion();

    renderGridDataSelectedFile(0);


});

function renderVersion() {

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "",
        url: '/Attrition/getVersionList',
        type: "post",
        success: function (json) {

            result = jQuery.parseJSON(json);


            var source =
                {
                    localdata: result,
                    datafields:
                    [
                        { name: 'ID', type: 'string' },
                        { name: 'UploadBy', type: 'string' },
                        { name: 'UploadDate', type: 'string' },
                        { name: 'Enable', type: 'Boolean' },
                    ],
                    datatype: "array",
                    updaterow: function (rowid, rowdata) {
                    }
                };

            var dataAdapter = new $.jqx.dataAdapter(source);

            var imgEnable = function (row, datafield, value) {

                var rowData = $('#jqxgridVersion').jqxGrid('getrowdata', row);

                if (rowData.Enable == "true") {
                    return 'Selected File'
                }
                else {
                    return 'Unselected'
                }


            }

            $("#jqxgridVersion").jqxGrid(
             {
                 width: 600,
                 source: dataAdapter,
                 pageable: true,
                 autoheight: true,
                 columnsresize: true,
                 columns: [
                   { text: 'Upload ID', columntype: 'textbox', datafield: 'ID', width: '3%' },
                   { text: 'Upload By', columntype: 'textbox', datafield: 'UploadBy', width: '31%' },
                   { text: 'Upload Date', columntype: 'textbox', datafield: 'UploadDate', width: '31%' },
                   { text: 'Enable', columntype: 'textbox', cellsrenderer: imgEnable, width: '31%' }
                 ]
             });

            $("#jqxVersion").jqxDropDownButton({ width: 400, height: 25 });


            //Salvar nueva seleccion
            $("#jqxgridVersion").on('rowselect', function (event) {
                event.stopImmediatePropagation();
                var args = event.args;
                var row = $("#jqxgridVersion").jqxGrid('getrowdata', args.rowindex);
                var dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;">' + row['ID'] + ' | ' + row['UploadBy'] + ' | ' + row['UploadDate'] + '</div>';
                $("#jqxVersion").jqxDropDownButton('setContent', dropDownContent);

                rowid = row['ID'];
            });


            $.each((result), function (index, element) {
                if (element.Enable) {
                    var dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;">' + element.ID + ' | ' + element.UploadBy + ' | ' + element.UploadDate + '</div>';
                    $("#jqxVersion").jqxDropDownButton('setContent', dropDownContent);

                    rowid = element.ID;
                }
            });

        }



    });


}

function renderGridDataSelectedFile(ID) {
    var listOfParams = {
        pFile: ID
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/Attrition/getListReeng',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {

            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                        { name: 'ID', type: 'string' },
                      , { name: 'UploadID', type: 'string' },
                        , { name: 'ReportingPeriod', type: 'string' },
                        , { name: 'GEID', type: 'string' },
                        , { name: 'SOEID', type: 'string' },
                        , { name: 'FullName', type: 'string' },
                        , { name: 'OfficerTitleDesc', type: 'string' },
                        , { name: 'Grade', type: 'string' },
                        , { name: 'TerminationActionDate', type: 'string' },
                        , { name: 'TerminationDate', type: 'string' },
                        , { name: 'TerminationAction', type: 'string' },
                        , { name: 'TerminationActionReason', type: 'string' },
                        , { name: 'TermCategoryforTalkingPointsDeck', type: 'string' },
                        , { name: 'TerminationActionReasonDescription', type: 'string' },
                        , { name: 'TermType', type: 'string' },
                        , { name: 'TerminationVoluntary', type: 'string' },
                        , { name: 'InvoluntaryTermination', type: 'string' },
                        , { name: 'FutureEmployerDescription', type: 'string' },
                        , { name: 'DirectManager1Name', type: 'string' },
                        , { name: 'FLSAStatus', type: 'string' },
                        , { name: 'FLSADescription', type: 'string' },
                        , { name: 'ServiceDate', type: 'string' },
                        , { name: 'NumberofyearsofService', type: 'string' },
                        , { name: 'TenureCategory', type: 'string' },
                        , { name: 'WorkCity', type: 'string' },
                        , { name: 'WorkCountryCode', type: 'string' },
                        , { name: 'ReportingCenter1', type: 'string' },
                        , { name: 'ReportingCenter2', type: 'string' },
                        , { name: 'MsNodeL09', type: 'string' },
                        , { name: 'MsDescrL09', type: 'string' },
                        , { name: 'GlobalProcess', type: 'string' },
                        , { name: 'MsNodeL10', type: 'string' },
                        , { name: 'MsDescrL10', type: 'string' },
                        , { name: 'MsNodeL11', type: 'string' },
                        , { name: 'MsDescrL11', type: 'string' },
                        , { name: 'MsNodeL12', type: 'string' },
                        , { name: 'MsDescrL12', type: 'string' },
                        , { name: 'MsNodeL13', type: 'string' },
                        , { name: 'MsDescrL13', type: 'string' },
                        , { name: 'ReviewRating', type: 'string' },
                        , { name: 'ReviewDate', type: 'string' },
                        , { name: 'Previous1YRReviewRating', type: 'string' },
                        , { name: 'Previous1YRReviewDate', type: 'string' },
                        , { name: 'Previous2YRReviewRating', type: 'string' },
                        , { name: 'Previous2YRReviewDate', type: 'string' }
                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            $('#jqxGridLastFile').jqxGrid(
           {
               source: dataAdapter,
               theme: 'blackberry',
               width: '100%',
               // autoheight: true,
               // autorowheight: true,
               pagesize: 10,
               pageable: true,
               filterable: true,
               showfilterrow: true,
               columnsresize: true,
               selectionmode: 'singlecell',
               enablebrowserselection: true,
               columns: [
                  { text: 'UploadID', dataField: 'UploadID', filtertype: 'input', editable: false, width: 40 },
                  , { text: 'ReportingPeriod', dataField: 'ReportingPeriod', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'GEID', dataField: 'GEID', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'SOEID', dataField: 'SOEID', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'Fulltext', dataField: 'Fulltext', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'OfficerTitleDesc', dataField: 'OfficerTitleDesc', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'Grade', dataField: 'Grade', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'TerminationActionDate', dataField: 'TerminationActionDate', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'TerminationDate', dataField: 'TerminationDate', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'TerminationAction', dataField: 'TerminationAction', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'TerminationActionReason', dataField: 'TerminationActionReason', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'TermCategoryforTalkingPointsDeck', dataField: 'TermCategoryforTalkingPointsDeck', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'TerminationActionReasonDescription', dataField: 'TerminationActionReasonDescription', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'TermType', dataField: 'TermType', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'TerminationVoluntary', dataField: 'TerminationVoluntary', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'InvoluntaryTermination', dataField: 'InvoluntaryTermination', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'FutureEmployerDescription', dataField: 'FutureEmployerDescription', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'DirectManager1text', dataField: 'DirectManager1text', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'FLSAStatus', dataField: 'FLSAStatus', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'FLSADescription', dataField: 'FLSADescription', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'ServiceDate', dataField: 'ServiceDate', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'NumberofyearsofService', dataField: 'NumberofyearsofService', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'TenureCategory', dataField: 'TenureCategory', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'WorkCity', dataField: 'WorkCity', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'WorkCountryCode', dataField: 'WorkCountryCode', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'ReportingCenter1', dataField: 'ReportingCenter1', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'ReportingCenter2', dataField: 'ReportingCenter2', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'MsNodeL09', dataField: 'MsNodeL09', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'MsDescrL09', dataField: 'MsDescrL09', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'GlobalProcess', dataField: 'GlobalProcess', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'MsNodeL10', dataField: 'MsNodeL10', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'MsDescrL10', dataField: 'MsDescrL10', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'MsNodeL11', dataField: 'MsNodeL11', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'MsDescrL11', dataField: 'MsDescrL11', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'MsNodeL12', dataField: 'MsNodeL12', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'MsDescrL12', dataField: 'MsDescrL12', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'MsNodeL13', dataField: 'MsNodeL13', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'MsDescrL13', dataField: 'MsDescrL13', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'ReviewRating', dataField: 'ReviewRating', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'ReviewDate', dataField: 'ReviewDate', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'Previous1YRReviewRating', dataField: 'Previous1YRReviewRating', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'Previous1YRReviewDate', dataField: 'Previous1YRReviewDate', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'Previous2YRReviewRating', dataField: 'Previous2YRReviewRating', filtertype: 'input', editable: false, width: 120 },
                        , { text: 'Previous2YRReviewDate', dataField: 'Previous2YRReviewDate', filtertype: 'input', editable: false, width: 120 }

               ]

           });


        }
    });


}

function viewSelected() {
    renderGridDataSelectedFile(rowid);
}


function useForThisMonth() {
    var listOfParams = {
        pFile: rowid
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Set selection as default file...",
        url: '/Attrition/setAsDefault',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            renderGridDataSelectedFile(rowid);
            renderVersion();
        }
    });

}