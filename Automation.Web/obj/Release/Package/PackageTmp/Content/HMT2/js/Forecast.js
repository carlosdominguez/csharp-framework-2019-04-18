﻿/// <reference path="../../Shared/plugins/util/global.js" />
var JAN = '';
var FEB = '';
var MAR = '';
var APR = '';
var MAY = '';
var JUN = '';
var JUL = '';
var AUG = '';
var SEP = '';
var OCT = '';
var NOV = '';
var DEC = '';

var JANEditable = false;
var FEBEditable = false;
var MAREditable = false;
var APREditable = false;
var MAYEditable = false;
var JUNEditable = false;
var JULEditable = false;
var AUGEditable = false;
var SEPEditable = false;
var OCTEditable = false;
var NOVEditable = false;
var DECEditable = false;

var JANclass = 'classNormal';
var FEBclass = 'classNormal';
var MARclass = 'classNormal';
var APRclass = 'classNormal';
var MAYclass = 'classNormal';
var JUNclass = 'classNormal';
var JULclass = 'classNormal';
var AUGclass = 'classNormal';
var SEPclass = 'classNormal';
var OCTclass = 'classNormal';
var NOVclass = 'classNormal';
var DECclass = 'classNormal';

var NewFirst = false;

$(document).ready(function () {
    _hideMenu();

    renderComboBox2('#cbDate', 'Select a Date', 'getDate', false);

    //renderComboBox('#cbGOC', 'Select a GOC', 'getGocList', true);

    renderComboBox('#cbMS', 'Select a MS', 'getManagedSegment', true);

    renderComboBox('#cbCenter', 'Select a Center', 'getCenter', true);

    renderComboBox('#cbDriver', 'Select a Driver', 'getCSSDriver', true);

    renderComboBox('#cbGOC', 'Select a Goc', 'getGOCValues', true);

    renderComboBoxDirectStaff();

    renderComboBoxTypeView();


    $('#forecastReportButtons').hide();
});


function renderComboBox2(div, textDefault, procedure, pMultiple) {

    $(div).find('option').remove().end();

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        async: false,
        url: '/Forecast/' + procedure,
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);
            
            var countY = 0

            $.each(response, function (index, element) {
                //$("#cbMyCalendar").append(new Option(element.Value, element.ID));
                if (countY == 0) {

                    $(div).append($('<option>', {
                        value: element.Value,
                        text: element.Value,
                        defaultSelected: true,
                        selected: true
                    }));
                } else {

                    $(div).append($('<option>', {
                        value: element.Value,
                        text: element.Value
                    }));
                }

                countY = countY + 1;

            })

            $(div).multiselect({
                click: function (event, ui) {
                    // renderUploadHistory(ui.value);
                }
            });


            $(div).multiselect({
                multiple: pMultiple,
                header: "Select an option",
                noneSelectedText: textDefault,
                selectedList: 1

            }).multiselectfilter();

            $(div).css('width', '100px');
        }
    });
}

function renderComboBox(div, textDefault, procedure, pMultiple) {

    $(div).find('option').remove().end();

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        async: false,
        url: '/Forecast/' + procedure,
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            $.each(response, function (index, element) {
                //$("#cbMyCalendar").append(new Option(element.Value, element.ID));
                $(div).append($('<option>', {
                    value: element.Value,
                    text: element.Value
                }));

            })

            $(div).multiselect({
                click: function (event, ui) {
                    // renderUploadHistory(ui.value);
                }
            });


            $(div).multiselect({
                multiple: pMultiple,
                header: "Select an option",
                noneSelectedText: textDefault,
                selectedList: 1

            }).multiselectfilter();

            $(div).css('width', '100px');
        }
    });
}

function renderComboBoxDirectStaff() {

    $('#cbDirectStaff').find('option').remove().end();

    $('#cbDirectStaff').append($('<option>', {
        value: 1,
        text: 'Direct Staff'
    }));

    $('#cbDirectStaff').append($('<option>', {
        value: 2,
        text: 'Temporal'
    }));

    $('#cbDirectStaff').multiselect({
        click: function (event, ui) { }
    });

    $('#cbDirectStaff').multiselect({
        multiple: true,
        header: "Select an option",
        noneSelectedText: 'Select',
        selectedList: 1

    }).multiselectfilter();

    $('#cbDirectStaff').css('width', '100px');

    

}

function renderComboBoxTypeView() {

    $('#cbTypeView').find('option').remove().end();

    $('#cbTypeView').append($('<option>', {
        value: 0,
        text: 'MS View',
        defaultSelected: true,
        selected: true
        
    }));

    $('#cbTypeView').append($('<option>', {
        value: 1,
        text: 'Center View'
    }));

    //$('#cbTypeView').append($('<option>', {
    //    value: 2,
    //    text: 'Driver View'
    //}));

    $('#cbTypeView').multiselect({
        click: function (event, ui) { }
    });

    $('#cbTypeView').multiselect({
        multiple: false,
        header: "Select an option",
        noneSelectedText: 'Select',
        selectedList: 1

    }).multiselectfilter();

    $('#cbTypeView').css('width', '100px');

    var i = 0;

    $("#cbTypeView").multiselect("widget").find(":checkbox").each(function () {
        if (i == 0) {
            this.click();
        }
        i = i++;
    });

}

function GenerateReport() {
    $('#forecastReport').show();
    $('#forecastReportButtons').show();

    var DATE = $('#cbDate').multiselect("getChecked");
    var MS = $('#cbMS').multiselect("getChecked");
    var CENTER = $('#cbCenter').multiselect("getChecked");

    var DirectStaff = $('#cbDirectStaff').multiselect("getChecked");
    var CSSDriver = $('#cbDriver').multiselect("getChecked");
    var Goc = $('#cbGOC').multiselect("getChecked");


    var errors = 0;
    var errorText = '';

    var DateParameter = '0';
    var MSParameter = '0';
    var CenterParameter = '0';
    var DirectStaffParameter = '0';
    var CSSDriverParameter = '0';
    var GocParameter = '0';


    $.each(DATE, function (index, value) {
        DateParameter = value.defaultValue;
    });

    var res = DateParameter.split(" ");

    if (res[0] == 'January')
    {
         JAN = 'JAN A';
         FEB = 'FEB FC';
         MAR = 'MAR FC';
         APR = 'APR FC';
         MAY = 'MAY FC';
         JUN = 'JUN FC';
         JUL = 'JUL FC';
         AUG = 'AUG FC';
         SEP = 'SEP FC';
         OCT = 'OCT FC';
         NOV = 'NOV FC';
         DEC = 'DEC FC';
         JANEditable = false;
         FEBEditable = true;
         MAREditable = true;
         APREditable = true;
         MAYEditable = true;
         JUNEditable = true;
         JULEditable = true;
         AUGEditable = true;
         SEPEditable = true;
         OCTEditable = true;
         NOVEditable = true;
         DECEditable = true;

         JANclass = 'classSelectedMonth';
         FEBclass = 'classFC';
         MARclass = 'classFC';
         APRclass = 'classFC';
         MAYclass = 'classFC';
         JUNclass = 'classFC';
         JULclass = 'classFC';
         AUGclass = 'classFC';
         SEPclass = 'classFC';
         OCTclass = 'classFC';
         NOVclass = 'classFC';
         DECclass = 'classFC';
    }
    if (res[0] == 'February') {
         JAN = 'JAN';
         FEB = 'FEB A';
         MAR = 'MAR FC';
         APR = 'APR FC';
         MAY = 'MAY FC';
         JUN = 'JUN FC';
         JUL = 'JUL FC';
         AUG = 'AUG FC';
         SEP = 'SEP FC';
         OCT = 'OCT FC';
         NOV = 'NOV FC';
         DEC = 'DEC FC';
         JANEditable = false;
         FEBEditable = false;
         MAREditable = true;
         APREditable = true;
         MAYEditable = true;
         JUNEditable = true;
         JULEditable = true;
         AUGEditable = true;
         SEPEditable = true;
         OCTEditable = true;
         NOVEditable = true;
         DECEditable = true;

         JANclass = 'classNormal';
         FEBclass = 'classSelectedMonth';
         MARclass = 'classFC';
         APRclass = 'classFC';
         MAYclass = 'classFC';
         JUNclass = 'classFC';
         JULclass = 'classFC';
         AUGclass = 'classFC';
         SEPclass = 'classFC';
         OCTclass = 'classFC';
         NOVclass = 'classFC';
         DECclass = 'classFC';
    }
    if (res[0] == 'March') {
         JAN = 'JAN';
         FEB = 'FEB';
         MAR = 'MAR A';
         APR = 'APR FC';
         MAY = 'MAY FC';
         JUN = 'JUN FC';
         JUL = 'JUL FC';
         AUG = 'AUG FC';
         SEP = 'SEP FC';
         OCT = 'OCT FC';
         NOV = 'NOV FC';
         DEC = 'DEC FC';
         JANEditable = false;
         FEBEditable = false;
         MAREditable = false;
         APREditable = true;
         MAYEditable = true;
         JUNEditable = true;
         JULEditable = true;
         AUGEditable = true;
         SEPEditable = true;
         OCTEditable = true;
         NOVEditable = true;
         DECEditable = true;

         JANclass = 'classNormal';
         FEBclass = 'classNormal';
         MARclass = 'classSelectedMonth';
         APRclass = 'classFC';
         MAYclass = 'classFC';
         JUNclass = 'classFC';
         JULclass = 'classFC';
         AUGclass = 'classFC';
         SEPclass = 'classFC';
         OCTclass = 'classFC';
         NOVclass = 'classFC';
         DECclass = 'classFC';
    }
    if (res[0] == 'April') {
         JAN = 'JAN';
         FEB = 'FEB';
         MAR = 'MAR';
         APR = 'APR A';
         MAY = 'MAY FC';
         JUN = 'JUN FC';
         JUL = 'JUL FC';
         AUG = 'AUG FC';
         SEP = 'SEP FC';
         OCT = 'OCT FC';
         NOV = 'NOV FC';
         DEC = 'DEC FC';
         JANEditable = false;
         FEBEditable = false;
         MAREditable = false;
         APREditable = false;
         MAYEditable = true;
         JUNEditable = true;
         JULEditable = true;
         AUGEditable = true;
         SEPEditable = true;
         OCTEditable = true;
         NOVEditable = true;
         DECEditable = true;

        JANclass = 'classNormal';
        FEBclass = 'classNormal';
        MARclass = 'classNormal';
        APRclass = 'classSelectedMonth';
        MAYclass = 'classFC';
        JUNclass = 'classFC';
        JULclass = 'classFC';
        AUGclass = 'classFC';
        SEPclass = 'classFC';
        OCTclass = 'classFC';
        NOVclass = 'classFC';
        DECclass = 'classFC';


    }
    if (res[0] == 'May') {
         JAN = 'JAN';
         FEB = 'FEB';
         MAR = 'MAR';
         APR = 'APR';
         MAY = 'MAY A';
         JUN = 'JUN FC';
         JUL = 'JUL FC';
         AUG = 'AUG FC';
         SEP = 'SEP FC';
         OCT = 'OCT FC';
         NOV = 'NOV FC';
         DEC = 'DEC FC';
         JANEditable = false;
         FEBEditable = false;
         MAREditable = false;
         APREditable = false;
         MAYEditable = false;
         JUNEditable = true;
         JULEditable = true;
         AUGEditable = true;
         SEPEditable = true;
         OCTEditable = true;
         NOVEditable = true;
         DECEditable = true;

         JANclass = 'classNormal';
         FEBclass = 'classNormal';
         MARclass = 'classNormal';
         APRclass = 'classNormal';
         MAYclass = 'classSelectedMonth';
         JUNclass = 'classFC';
         JULclass = 'classFC';
         AUGclass = 'classFC';
         SEPclass = 'classFC';
         OCTclass = 'classFC';
         NOVclass = 'classFC';
         DECclass = 'classFC';
    }
    if (res[0] == 'June') {
         JAN = 'JAN';
         FEB = 'FEB';
         MAR = 'MAR';
         APR = 'APR';
         MAY = 'MAY';
         JUN = 'JUN A';
         JUL = 'JUL FC';
         AUG = 'AUG FC';
         SEP = 'SEP FC';
         OCT = 'OCT FC';
         NOV = 'NOV FC';
         DEC = 'DEC FC';
         JANEditable = false;
         FEBEditable = false;
         MAREditable = false;
         APREditable = false;
         MAYEditable = false;
         JUNEditable = false;
         JULEditable = true;
         AUGEditable = true;
         SEPEditable = true;
         OCTEditable = true;
         NOVEditable = true;
         DECEditable = true;

         JANclass = 'classNormal';
         FEBclass = 'classNormal';
         MARclass = 'classNormal';
         APRclass = 'classNormal';
         MAYclass = 'classNormal';
         JUNclass = 'classSelectedMonth';
         JULclass = 'classFC';
         AUGclass = 'classFC';
         SEPclass = 'classFC';
         OCTclass = 'classFC';
         NOVclass = 'classFC';
         DECclass = 'classFC';
    }
    if (res[0] == 'July') {
         JAN = 'JAN';
         FEB = 'FEB';
         MAR = 'MAR';
         APR = 'APR';
         MAY = 'MAY';
         JUN = 'JUN';
         JUL = 'JUL A';
         AUG = 'AUG FC';
         SEP = 'SEP FC';
         OCT = 'OCT FC';
         NOV = 'NOV FC';
         DEC = 'DEC FC';
         JANEditable = false;
         FEBEditable = false;
         MAREditable = false;
         APREditable = false;
         MAYEditable = false;
         JUNEditable = false;
         JULEditable = false;
         AUGEditable = true;
         SEPEditable = true;
         OCTEditable = true;
         NOVEditable = true;
         DECEditable = true;

         JANclass = 'classNormal';
         FEBclass = 'classNormal';
         MARclass = 'classNormal';
         APRclass = 'classNormal';
         MAYclass = 'classNormal';
         JUNclass = 'classNormal';
         JULclass = 'classSelectedMonth';
         AUGclass = 'classFC';
         SEPclass = 'classFC';
         OCTclass = 'classFC';
         NOVclass = 'classFC';
         DECclass = 'classFC';
    }
    if (res[0] == 'August') {
         JAN = 'JAN';
         FEB = 'FEB';
         MAR = 'MAR';
         APR = 'APR';
         MAY = 'MAY';
         JUN = 'JUN';
         JUL = 'JUL';
         AUG = 'AUG A';
         SEP = 'SEP FC';
         OCT = 'OCT FC';
         NOV = 'NOV FC';
         DEC = 'DEC FC';
         JANEditable = false;
         FEBEditable = false;
         MAREditable = false;
         APREditable = false;
         MAYEditable = false;
         JUNEditable = false;
         JULEditable = false;
         AUGEditable = false;
         SEPEditable = true;
         OCTEditable = true;
         NOVEditable = true;
         DECEditable = true;

         JANclass = 'classNormal';
         FEBclass = 'classNormal';
         MARclass = 'classNormal';
         APRclass = 'classNormal';
         MAYclass = 'classNormal';
         JUNclass = 'classNormal';
         JULclass = 'classNormal';
         AUGclass = 'classSelectedMonth';
         SEPclass = 'classFC';
         OCTclass = 'classFC';
         NOVclass = 'classFC';
         DECclass = 'classFC';
    }
    if (res[0] == 'September') {
         JAN = 'JAN';
         FEB = 'FEB';
         MAR = 'MAR';
         APR = 'APR';
         MAY = 'MAY';
         JUN = 'JUN';
         JUL = 'JUL';
         AUG = 'AUG';
         SEP = 'SEP A';
         OCT = 'OCT FC';
         NOV = 'NOV FC';
         DEC = 'DEC FC';
         JANEditable = false;
         FEBEditable = false;
         MAREditable = false;
         APREditable = false;
         MAYEditable = false;
         JUNEditable = false;
         JULEditable = false;
         AUGEditable = false;
         SEPEditable = false;
         OCTEditable = true;
         NOVEditable = true;
         DECEditable = true;

         JANclass = 'classNormal';
         FEBclass = 'classNormal';
         MARclass = 'classNormal';
         APRclass = 'classNormal';
         MAYclass = 'classNormal';
         JUNclass = 'classNormal';
         JULclass = 'classNormal';
         AUGclass = 'classNormal';
         SEPclass = 'classSelectedMonth';
         OCTclass = 'classFC';
         NOVclass = 'classFC';
         DECclass = 'classFC';

    }
    if (res[0] == 'October') {
         JAN = 'JAN';
         FEB = 'FEB';
         MAR = 'MAR';
         APR = 'APR';
         MAY = 'MAY';
         JUN = 'JUN';
         JUL = 'JUL';
         AUG = 'AUG';
         SEP = 'SEP';
         OCT = 'OCT A';
         NOV = 'NOV FC';
         DEC = 'DEC FC';
         JANEditable = false;
         FEBEditable = false;
         MAREditable = false;
         APREditable = false;
         MAYEditable = false;
         JUNEditable = false;
         JULEditable = false;
         AUGEditable = false;
         SEPEditable = false;
         OCTEditable = false;
         NOVEditable = true;
         DECEditable = true;

         JANclass = 'classNormal';
         FEBclass = 'classNormal';
         MARclass = 'classNormal';
         APRclass = 'classNormal';
         MAYclass = 'classNormal';
         JUNclass = 'classNormal';
         JULclass = 'classNormal';
         AUGclass = 'classNormal';
         SEPclass = 'classNormal';
         OCTclass = 'classSelectedMonth';
         NOVclass = 'classFC';
         DECclass = 'classFC';
    }
    if (res[0] == 'November') {
         JAN = 'JAN';
         FEB = 'FEB';
         MAR = 'MAR';
         APR = 'APR';
         MAY = 'MAY';
         JUN = 'JUN';
         JUL = 'JUL';
         AUG = 'AUG';
         SEP = 'SEP';
         OCT = 'OCT';
         NOV = 'NOV A';
         DEC = 'DEC FC';
         JANEditable = false;
         FEBEditable = false;
         MAREditable = false;
         APREditable = false;
         MAYEditable = false;
         JUNEditable = false;
         JULEditable = false;
         AUGEditable = false;
         SEPEditable = false;
         OCTEditable = false;
         NOVEditable = false;
         DECEditable = true;

         JANclass = 'classNormal';
         FEBclass = 'classNormal';
         MARclass = 'classNormal';
         APRclass = 'classNormal';
         MAYclass = 'classNormal';
         JUNclass = 'classNormal';
         JULclass = 'classNormal';
         AUGclass = 'classNormal';
         SEPclass = 'classNormal';
         OCTclass = 'classNormal';
         NOVclass = 'classSelectedMonth';
         DECclass = 'classFC';
    }
    if (res[0] == 'December') {
         JAN = 'JAN';
         FEB = 'FEB';
         MAR = 'MAR';
         APR = 'APR';
         MAY = 'MAY';
         JUN = 'JUN';
         JUL = 'JUL';
         AUG = 'AUG';
         SEP = 'SEP';
         OCT = 'OCT';
         NOV = 'NOV';
         DEC = 'DEC A';
         JANEditable = false;
         FEBEditable = false;
         MAREditable = false;
         APREditable = false;
         MAYEditable = false;
         JUNEditable = false;
         JULEditable = false;
         AUGEditable = false;
         SEPEditable = false;
         OCTEditable = false;
         NOVEditable = false;
         DECEditable = false;

         JANclass = 'classNormal';
         FEBclass = 'classNormal';
         MARclass = 'classNormal';
         APRclass = 'classNormal';
         MAYclass = 'classNormal';
         JUNclass = 'classNormal';
         JULclass = 'classNormal';
         AUGclass = 'classNormal';
         SEPclass = 'classNormal';
         OCTclass = 'classNormal';
         NOVclass = 'classNormal';
         DECclass = 'classSelectedMonth';
    }

    $.each(MS, function (index, value) {
        if (index == 0) {
            MSParameter = value.defaultValue;
        }
        else {
            MSParameter = MSParameter + '~' + value.defaultValue;
        }
    });

    $.each(CENTER, function (index, value) {
        if (index == 0) {
            CenterParameter = value.defaultValue;
        }
        else {
            CenterParameter = CenterParameter + '~' + value.defaultValue;
        }
    });

    $.each(DirectStaff, function (index, value) {
        if (index == 0) {
            DirectStaffParameter = value.defaultValue;
        }
        else {
            DirectStaffParameter = DirectStaffParameter + '~' + value.defaultValue;
        }
    });

    $.each(CSSDriver, function (index, value) {
        if (index == 0) {
            CSSDriverParameter = value.defaultValue;
        }
        else {
            CSSDriverParameter = CSSDriverParameter + '~' + value.defaultValue;
        }
    });

    $.each(Goc, function (index, value) {
        if (index == 0) {
            GocParameter = value.defaultValue;
        }
        else {
            GocParameter = GocParameter + '~' + value.defaultValue;
        }
    });

  


    if (DATE.length = 0) {
    }

    var listOfParams = {
        Date: DateParameter,
        MS: MSParameter,
        Center: CenterParameter,
        DS: DirectStaffParameter,
        CSSDriver: CSSDriverParameter,
        GOC: GocParameter
    }

    $("#addHtml").empty();

    var TypeView = $('#cbTypeView').multiselect("getChecked");
    var TypeViewData;
    $.each(TypeView, function (index, value) {
        TypeViewData = value.defaultValue;
    });

    if (TypeViewData == "0")
    {
        getHTMLGrid(listOfParams);
        $('#forecastReportButtons').show();
    }
    if (TypeViewData == "1")
    {
        getHTMLGridCenter(listOfParams);
        $('#forecastReportButtons').show();
    }
    if (TypeViewData == "2")
    {
        getHTMLGrid(listOfParams);
        $('#forecastReportButtons').show();
    }
    
    


}

function getHTMLGrid(listOfParams) {

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/ForeCast/getHTMLGrid',
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {
            var response = jQuery.parseJSON(json);
            
            var html = '';
           
            for (var i = 0; i < response.length; i++) {

                html = html + ' <div id="' + response[i].GridName +'"></div> ';

            }

            $("#addHtml").append(html);

       
            NewFirst = true;
            for (var i = 0; i < response.length; i++) {

                if (i === 0)
                {
                    getGridForecast(response[i].ID, true, '#' + response[i].GridName);
                }
                else
                {
                    getGridForecast(response[i].ID, false, '#' + response[i].GridName);
                }

            }
           
  
        }

    });

}


function getHTMLGridCenter(listOfParams) {

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/ForeCast/getHTMLGridCenter',
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var html = '';

            for (var i = 0; i < response.length; i++) {

                html = html + ' <div id="' + response[i].GridName + '"></div> ';

            }

            $("#addHtml").append(html);


            NewFirst = true;
            for (var i = 0; i < response.length; i++) {

                if (i === 0) {
                    getGridForecastCenter(response[i].ID, true, '#' + response[i].GridName);
                }
                else {
                    getGridForecastCenter(response[i].ID, false, '#' + response[i].GridName);
                }

            }


        }

    });

}


function getGridForecast(pMS, first, grid, parameters) {

    var CENTER = $('#cbCenter').multiselect("getChecked");
    var DATE = $('#cbDate').multiselect("getChecked");
    var CenterParameter = '0'
    var DateParameter = '0'

    var DirectStaff = $('#cbDirectStaff').multiselect("getChecked");
    var CSSDriver = $('#cbDriver').multiselect("getChecked");
    var Goc = $('#cbGOC').multiselect("getChecked");

    var DirectStaffParameter = '0';
    var CSSDriverParameter = '0';
    var GocParameter = '0';


    $.each(DirectStaff, function (index, value) {
        if (index == 0) {
            DirectStaffParameter = value.defaultValue;
        }
        else {
            DirectStaffParameter = DirectStaffParameter + '~' + value.defaultValue;
        }
    });
    $.each(CSSDriver, function (index, value) {
        if (index == 0) {
            CSSDriverParameter = value.defaultValue;
        }
        else {
            CSSDriverParameter = CSSDriverParameter + '~' + value.defaultValue;
        }
    });
    $.each(Goc, function (index, value) {
        if (index == 0) {
            GocParameter = value.defaultValue;
        }
        else {
            GocParameter = GocParameter + '~' + value.defaultValue;
        }
    });
    $.each(CENTER, function (index, value) {
        if (index == 0) {
            CenterParameter = value.defaultValue;
        }
        else {
            CenterParameter = CenterParameter + '~' + value.defaultValue;
        }
    });
    $.each(DATE, function (index, value) {
        DateParameter = value.defaultValue;
    });

    var listOfParams = {
        Date: DateParameter,
        Center: CenterParameter,
        MS: pMS,
        DS: DirectStaffParameter,
        CSSDriver: CSSDriverParameter,
        GOC: GocParameter

    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/ForeCast/getForecastMSDriver',

        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
                  {
                      unboundmode: true,
                      localdata: data2,
                      datafields:
                              [
                                { name: 'ID', type: 'string' }
		                      , { name: 'ManagedSegment', type: 'string' }
		                      , { name: 'CSSDriver', type: 'string' }
                              , { name: 'JAN', type: 'number' }
                              , { name: 'FEB', type: 'number' }
                              , { name: 'MAR', type: 'number' }
                              , { name: 'APR', type: 'number' }
                              , { name: 'MAY', type: 'number' }
                              , { name: 'JUN', type: 'number' }
                              , { name: 'JUL', type: 'number' }
                              , { name: 'AUG', type: 'number' }
                              , { name: 'SEP', type: 'number' }
                              , { name: 'OCT', type: 'number' }
                              , { name: 'NOV', type: 'number' }
                              , { name: 'DEC', type: 'number' }
                              ],
                      datatype: "json"
                  };

            var dataAdapter = new $.jqx.dataAdapter(source);

            var initrowdetails = function (index, parentElement, gridElement, record) {
                var id = record.uid.toString();

                var grid2 = $($($($($($($(parentElement).children()[0]).children()[0]).children()[0]).children()[0]).children()[0]).children()[1]);

                var total = $($($($($($($($($($($(parentElement).children()[0]).children()[0]).children()[0]).children()[1]).children()[0]).children()[1]).children()[0]).children()[1]).children()[1]).children()[0]);
                var start = $($($($($($($($($($($(parentElement).children()[0]).children()[0]).children()[0]).children()[1]).children()[0]).children()[1]).children()[0]).children()[1]).children()[3]).children()[0]);
                var isEndDate = $($($($($($($($($($($(parentElement).children()[0]).children()[0]).children()[0]).children()[1]).children()[0]).children()[1]).children()[0]).children()[1]).children()[5]).children()[0]);
                var end = $($($($($($($($($($($(parentElement).children()[0]).children()[0]).children()[0]).children()[1]).children()[0]).children()[1]).children()[0]).children()[1]).children()[7]).children()[0]);
                var driverCB = $($($($($($($($($($($(parentElement).children()[0]).children()[0]).children()[0]).children()[1]).children()[0]).children()[1]).children()[0]).children()[1]).children()[9]).children()[0]);

                var goc = $($($($($($($($($($($(parentElement).children()[0]).children()[0]).children()[0]).children()[1]).children()[0]).children()[2]).children()[0]).children()[1]).children()[1]).children()[0]);
                var type = $($($($($($($($($($($(parentElement).children()[0]).children()[0]).children()[0]).children()[1]).children()[0]).children()[2]).children()[0]).children()[1]).children()[3]).children()[0]);
                var center = $($($($($($($($($($($(parentElement).children()[0]).children()[0]).children()[0]).children()[1]).children()[0]).children()[2]).children()[0]).children()[1]).children()[5]).children()[0]);
                var button = $($($($($($($($($($($(parentElement).children()[0]).children()[0]).children()[0]).children()[1]).children()[0]).children()[2]).children()[0]).children()[1]).children()[7]).children()[0]);
                

                var gridFC = $($($($($($($($($($($($(parentElement).children()[0]).children()[0]).children()[0]).children()[1]).children()[0]).children()[3]).children()[0])).children()[0]).children()[1]).children()[0]);

                var availableFormats = ['Direct Staff', 'Temporal'];

                type.jqxDropDownList({ source: availableFormats, selectedIndex: 0, width: '90%', height: '25px' });

                var centerSource = ['TAMPA', 'NYC', 'DELAWARE', 'EMEA', 'COSTA RICA', 'MANILA', 'MUMBAI'];

                center.jqxDropDownList({ source: centerSource, selectedIndex: 0, width: '90%', height: '25px' });

                start.jqxDateTimeInput({ width: '90%', height: '25px', formatString: 'Y' });
                end.jqxDateTimeInput({ width: '90%', height: '25px', formatString: 'Y' });
                total.jqxNumberInput({ width: '100px', height: '25px', inputMode: 'simple', spinButtons: true, min: 0, max: 10, decimalDigits: 0 });

                var CENTER = $('#cbCenter').multiselect("getChecked");
                var DATE = $('#cbDate').multiselect("getChecked");
                var CenterParameter = '0'
                var DateParameter = '0'

                var DirectStaff = $('#cbDirectStaff').multiselect("getChecked");
                var Goc = $('#cbGOC').multiselect("getChecked");

                var DirectStaffParameter = '0';
                var GocParameter = '0';

                var MSParameter = record.ManagedSegment;
                var CSSDriverParameter = record.CSSDriver;


                $.each(DirectStaff, function (index, value) {
                    if (index == 0) {
                        DirectStaffParameter = value.defaultValue;
                    }
                    else {
                        DirectStaffParameter = DirectStaffParameter + '~' + value.defaultValue;
                    }
                });

                $.each(Goc, function (index, value) {
                    if (index == 0) {
                        GocParameter = value.defaultValue;
                    }
                    else {
                        GocParameter = GocParameter + '~' + value.defaultValue;
                    }
                });

                $.each(CENTER, function (index, value) {
                    if (index == 0) {
                        CenterParameter = value.defaultValue;
                    }
                    else {
                        CenterParameter = CenterParameter + '~' + value.defaultValue;
                    }
                });
                $.each(DATE, function (index, value) {
                    DateParameter = value.defaultValue;
                });

                var nestedData;

                var listOfParams = {
                    MS: record.ManagedSegment,
                    Driver: record.CSSDriver,
                    Date: DateParameter,
                    Center: CenterParameter,
                    GOC: GocParameter,
                    DS: DirectStaffParameter
                }

                _callServer({
                    loadingMsgType: "fullLoading",loadingMsg: "Loading data...", async: false,
                    url: '/ForeCast/getPosibleDrivers',
                    data: {
                        'data': JSON.stringify(listOfParams)
                    },
                    type: "post",
                    success: function (json) {
                        nestedData = jQuery.parseJSON(json);

                        var source2 =
                            {
                                localdata: nestedData,
                                datafields:
                                   [
                                    { name: 'Value', type: 'string' }
                                   ],
                                datatype: "json"
                            };
                        var dataAdapter2 = new $.jqx.dataAdapter(source2);

                        driverCB.jqxDropDownList({
                            selectedIndex: 2, source: dataAdapter2, displayMember: "Value", valueMember: "Value", width: '100%', height: 25
                        });

                        driverCB.on('select', function (event) {
                            if (event.args) {
                                var item = event.args.item;
                                if (item) {
                                    var valueelement = $("<div></div>");
                                    valueelement.text("Value: " + item.value);
                                    var labelelement = $("<div></div>");
                                    labelelement.text("Label: " + item.label);
                                    $("#selectionlog").children().remove();
                                    $("#selectionlog").append(labelelement);
                                    $("#selectionlog").append(valueelement);
                                }
                            }
                        });
                    }
                });


                _callServer({
                    loadingMsgType: "fullLoading",loadingMsg: "Loading data...", async: false,
                    url: '/ForeCast/getRowDetailManagedSegmentView',
                    data: {
                        'data': JSON.stringify(listOfParams)
                    },
                    type: "post",
                    success: function (json) {
                        nestedData = jQuery.parseJSON(json);

                            var source2 =
                                {
                                    localdata: nestedData,
                                    datafields:
                                       [
                                        { name: 'GOC', type: 'string' }
                                       ],
                                    datatype: "json"
                                };
                                var dataAdapter2 = new $.jqx.dataAdapter(source2);

                                goc.jqxDropDownList({
                                    selectedIndex: 2, source: dataAdapter2, displayMember: "GOC", valueMember: "GOC", width: '100%', height: 25
                                });

                                goc.on('select', function (event) {
                                    if (event.args) {
                                        var item = event.args.item;
                                        if (item) {
                                            var valueelement = $("<div></div>");
                                            valueelement.text("Value: " + item.value);
                                            var labelelement = $("<div></div>");
                                            labelelement.text("Label: " + item.label);
                                            $("#selectionlog").children().remove();
                                            $("#selectionlog").append(labelelement);
                                            $("#selectionlog").append(valueelement);
                                        }
                                    }
                                });


                        var orderssource = {
                            localdata: nestedData,
                            datafields:
                               [
                                { name: 'ID', type: 'string' }
                              , { name: 'GOC', type: 'string' }
                              , { name: 'JAN', type: 'number' }
                              , { name: 'FEB', type: 'number' }
                              , { name: 'MAR', type: 'number' }
                              , { name: 'APR', type: 'number' }
                              , { name: 'MAY', type: 'number' }
                              , { name: 'JUN', type: 'number' }
                              , { name: 'JUL', type: 'number' }
                              , { name: 'AUG', type: 'number' }
                              , { name: 'SEP', type: 'number' }
                              , { name: 'OCT', type: 'number' }
                              , { name: 'NOV', type: 'number' }
                              , { name: 'DEC', type: 'number' }
                              , { name: 'IsDirectStaff', type: 'string' }
                               ],
                            datatype: "json"
                        };

                        var nestedGridAdapter = new $.jqx.dataAdapter(orderssource);


                        var initrowdetailsGrid2 = function (index, parentElement, gridElement, record) {

                            //var id = record.uid.toString();

                            var gridDriverDetail = $($(parentElement).children()[0]);

                            var nestedDataGridDriverDetail;

                            var listOfParamsGrid2 = {
                                MS: MSParameter,
                                Driver: CSSDriverParameter,
                                Date: DateParameter,
                                Center: CenterParameter,
                                GOC: record.GOC,
                                DS: record.IsDirectStaff,
                            }

                            _callServer({
                                loadingMsgType: "fullLoading",loadingMsg: "Loading data...", async: false,
                                url: '/ForeCast/getRowDetailGOCbyDrivers',
                                data: {
                                    'data': JSON.stringify(listOfParamsGrid2)
                                },
                                type: "post",
                                success: function (json) {
                                    nestedDataGridDriverDetail = jQuery.parseJSON(json);

                                    var orderssourceDriverDetail = {
                                        localdata: nestedDataGridDriverDetail,
                                        datafields:
                                           [
                                                { name: 'ID', type: 'string' }
                                              , { name: 'Driver', type: 'string' }
                                              , { name: 'JAN', type: 'number' }
                                              , { name: 'FEB', type: 'number' }
                                              , { name: 'MAR', type: 'number' }
                                              , { name: 'APR', type: 'number' }
                                              , { name: 'MAY', type: 'number' }
                                              , { name: 'JUN', type: 'number' }
                                              , { name: 'JUL', type: 'number' }
                                              , { name: 'AUG', type: 'number' }
                                              , { name: 'SEP', type: 'number' }
                                              , { name: 'OCT', type: 'number' }
                                              , { name: 'NOV', type: 'number' }
                                              , { name: 'DEC', type: 'number' }
                                           ],
                                        datatype: "json"
                                    };

                                    var nestedGridAdapterGridDriverDetail = new $.jqx.dataAdapter(orderssourceDriverDetail);
                                   // nestedGridAdapterGridDriverDetail.dataBind();

                                    if (gridDriverDetail != null) {
                                            gridDriverDetail.jqxGrid({
                                                height: 150,
                                                theme: 'blackberry',
                                                width: '80%',
                                                enabletooltips: true,
                                                source: nestedGridAdapterGridDriverDetail,
                                                selectionmode: 'multiplecellsadvanced',
                                                editable: true,
                                                showaggregates: true,
                                                columns: [

                                                                   { text: 'Driver', dataField: 'Driver', filtertype: 'input', editable: false, width: '22%' }
                                                                 , {
                                                                     text: JAN, dataField: 'JAN', filtertype: 'input', cellclassname: JANclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                     createeditor: function (row, cellvalue, editor) {
                                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'JAN' });
                                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) 
                                                                     { var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates, 
                                                                     function (key, value) { var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0) 
                                                                     { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>'; }); renderstring += "</div>"; return renderstring; } }
                                                                 , {
                                                                     text: FEB, dataField: 'FEB', filtertype: 'input', cellclassname: FEBclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                     createeditor: function (row, cellvalue, editor) {
                                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'FEB' });
                                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                         function (key, value) {
                                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                         }); renderstring += "</div>"; return renderstring;
                                                                     }
                                                                 }
                                                                 , {
                                                                     text: MAR, dataField: 'MAR', filtertype: 'input', cellclassname: MARclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                     createeditor: function (row, cellvalue, editor) {
                                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'MAR' });
                                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                         function (key, value) {
                                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                         }); renderstring += "</div>"; return renderstring;
                                                                     }
                                                                 }
                                                                 , {
                                                                     text: APR, dataField: 'APR', filtertype: 'input', cellclassname: APRclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                     createeditor: function (row, cellvalue, editor) {
                                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'APR' });
                                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                         function (key, value) {
                                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                         }); renderstring += "</div>"; return renderstring;
                                                                     }
                                                                 }
                                                                 , {
                                                                     text: MAY, dataField: 'MAY', filtertype: 'input', cellclassname: MAYclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                     createeditor: function (row, cellvalue, editor) {
                                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'MAY' });
                                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                         function (key, value) {
                                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                         }); renderstring += "</div>"; return renderstring;
                                                                     }
                                                                 }
                                                                 , {
                                                                     text: JUN, dataField: 'JUN', filtertype: 'input', cellclassname: JUNclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                     createeditor: function (row, cellvalue, editor) {
                                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'JUN' });
                                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                         function (key, value) {
                                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                         }); renderstring += "</div>"; return renderstring;
                                                                     }
                                                                 }
                                                                 , {
                                                                     text: JUL, dataField: 'JUL', filtertype: 'input', cellclassname: JULclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                     createeditor: function (row, cellvalue, editor) {
                                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'JUL' });
                                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                         function (key, value) {
                                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                         }); renderstring += "</div>"; return renderstring;
                                                                     }
                                                                 }
                                                                 , {
                                                                     text: AUG, dataField: 'AUG', filtertype: 'input', cellclassname: AUGclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                     createeditor: function (row, cellvalue, editor) {
                                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'AUG' });
                                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                         function (key, value) {
                                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                         }); renderstring += "</div>"; return renderstring;
                                                                     }
                                                                 }
                                                                 , {
                                                                     text: SEP, dataField: 'SEP', filtertype: 'input', cellclassname: SEPclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                     createeditor: function (row, cellvalue, editor) {
                                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'SEP' });
                                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                         function (key, value) {
                                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                         }); renderstring += "</div>"; return renderstring;
                                                                     }
                                                                 }
                                                                 , {
                                                                     text: OCT, dataField: 'OCT', filtertype: 'input', cellclassname: OCTclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                     createeditor: function (row, cellvalue, editor) {
                                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'OCT' });
                                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                         function (key, value) {
                                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                         }); renderstring += "</div>"; return renderstring;
                                                                     }
                                                                 }
                                                                 , {
                                                                     text: NOV, dataField: 'NOV', filtertype: 'input', cellclassname: NOVclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                     createeditor: function (row, cellvalue, editor) {
                                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'NOV' });
                                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                         function (key, value) {
                                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                         }); renderstring += "</div>"; return renderstring;
                                                                     }
                                                                 }
                                                                 , {
                                                                     text: DEC, dataField: 'DEC', filtertype: 'input', cellclassname: DECclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                     createeditor: function (row, cellvalue, editor) {
                                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'DEC' });
                                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                         function (key, value) {
                                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                         }); renderstring += "</div>"; return renderstring;
                                                                     }
                                                                 }


                                                ]

                                            });

                                    }
                                }
                            });


                        };


                        //nestedGridAdapter.dataBind();

                        if (grid2 != null) {

                            grid2.jqxGrid({
                                
                                height: 350,
                                theme: 'blackberry',
                                width: '100%',
                                enabletooltips: true,
                                source: nestedGridAdapter,
                                selectionmode: 'multiplecellsadvanced',
                                editable: true,
                                showaggregates: true,
                                rowdetails: true,
                                initrowdetails: initrowdetailsGrid2,
                                rowdetailstemplate: { rowdetails: "<div id='gridDriverDetail' style='margin: 0px;top:5px;'></div>", rowdetailsheight: 200, rowdetailshidden: true },
                                columns: [

                                                   { text: 'GOC', dataField: 'GOC', filtertype: 'input', editable: false, width: '22%' }
                                                 , { text: 'Type', dataField: 'IsDirectStaff', filtertype: 'input', editable: false, width: '8%' }
                                                 , {
                                                     text: JAN, dataField: 'JAN', filtertype: 'input', cellclassname: JANclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'JAN' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) 
                                                        { var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates, 
                                                        function (key, value) { var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0) 
                                                        { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>'; }); renderstring += "</div>"; return renderstring; } }
                                                 , {
                                                     text: FEB, dataField: 'FEB', filtertype: 'input', cellclassname: FEBclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'FEB' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: MAR, dataField: 'MAR', filtertype: 'input', cellclassname: MARclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'MAR' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: APR, dataField: 'APR', filtertype: 'input', cellclassname: APRclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'APR' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: MAY, dataField: 'MAY', filtertype: 'input', cellclassname: MAYclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'MAY' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: JUN, dataField: 'JUN', filtertype: 'input', cellclassname: JUNclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'JUN' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: JUL, dataField: 'JUL', filtertype: 'input', cellclassname: JULclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'JUL' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: AUG, dataField: 'AUG', filtertype: 'input', cellclassname: AUGclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'AUG' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: SEP, dataField: 'SEP', filtertype: 'input', cellclassname: SEPclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'SEP' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: OCT, dataField: 'OCT', filtertype: 'input', cellclassname: OCTclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'OCT' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: NOV, dataField: 'NOV', filtertype: 'input', cellclassname: NOVclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'NOV' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: DEC, dataField: 'DEC', filtertype: 'input', cellclassname: DECclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'DEC' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }


                                ]

                            });

                        }
                    }
                });

                _callServer({
                    loadingMsgType: "fullLoading",loadingMsg: "Loading data...", async: false,
                    url: '/ForeCast/getFCDetailGrid',
                    data: {
                        'data': JSON.stringify(listOfParams)
                    },
                    type: "post",
                    success: function (json) {
                        nestedData = jQuery.parseJSON(json);

                        var orderssource = {
                            localdata: nestedData,
                            datafields:
                               [
                                { name: 'ID', type: 'string' }
                              , { name: 'PeriodID', type: 'string' }
                              , { name: 'CSSDriverID', type: 'string' }
	                          , { name: 'GOC', type: 'string' }
                              , { name: 'StartMonth', type: 'string' }
                              , { name: 'StartYear', type: 'string' }
                              , { name: 'EndMonth', type: 'string' }
                              , { name: 'EndYear', type: 'string' }
                              , { name: 'CreatedBy', type: 'string' }
                              , { name: 'CreatedDate', type: 'string' }
                              , { name: 'Enable', type: 'string' }
                              , { name: 'HaveEndDate', type: 'string' }
                              , { name: 'Total', type: 'string' }
	                          , { name: 'IsDS', type: 'string' }
	                          , { name: 'CSSDriver', type: 'string' }
                              , { name: 'Center', type: 'string' }
                               ],
                            datatype: "json"
                        };

                        var nestedGridAdapter = new $.jqx.dataAdapter(orderssource);
                        //nestedGridAdapter.dataBind();

                        if (gridFC != null) {

                            gridFC.jqxGrid({

                                height: 240,
                                theme: 'blackberry',
                                width: '100%',
                                enabletooltips: true,
                                source: nestedGridAdapter,
                                editable: true,
                                showaggregates: true,
                                showtoolbar: true,
                                rendertoolbar: function (toolbar) {
                                    var me = this;
                                    var container = $("<div style='margin: 5px;'></div>");
                                    toolbar.append(container);
                                    container.append('<input style="margin-left: 5px;" id="deleterowbutton" type="button" value="Delete Selected Row" />');
                                    $("#deleterowbutton").jqxButton();
                                    // update row.
                                    // delete row.
                                    $("#deleterowbutton").on('click', function () {
                                        var selectedrowindex = gridFC.jqxGrid('getselectedrowindex');
                                        var rowscount = gridFC.jqxGrid('getdatainformation').rowscount;

                                        var listOfParamsDeleteForeCast = {
                                            ID: gridFC.jqxGrid('getrowdata', selectedrowindex).ID
                                        }

                                        _callServer({
                                            loadingMsgType: "fullLoading",loadingMsg: "Loading data...", async: false,
                                            url: '/ForeCast/deleteForeCastDetail',
                                            data: {
                                                'data': JSON.stringify(listOfParamsDeleteForeCast)
                                            },
                                            type: "post",
                                            success: function (json) {
                                            }
                                        });

                                        if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                                            var id = gridFC.jqxGrid('getrowid', selectedrowindex);
                                            var commit = gridFC.jqxGrid('deleterow', id);
                                        }
                                    });
                                },
                                columns: [
                                                    { text: 'CSSDriver', dataField: 'CSSDriver', filtertype: 'input', editable: false, width: '100' }
                                                  , { text: 'Center', dataField: 'Center', filtertype: 'input', editable: false, width: '100' }
                                                  , { text: 'Total', dataField: 'Total', filtertype: 'input', editable: false, width: '100' }
	                                              , { text: 'GOC', dataField: 'GOC', filtertype: 'input', editable: false, width: '100' }
                                                  , { text: 'IsDS', dataField: 'IsDS', filtertype: 'input', editable: false, width: '100' }
                                                  , { text: 'StartMonth', dataField: 'StartMonth', filtertype: 'input', editable: false, width: '100' }
                                                  , { text: 'StartYear', dataField: 'StartYear', filtertype: 'input', editable: false, width: '100' }
                                                  , { text: 'HaveEndDate', dataField: 'HaveEndDate', filtertype: 'input', editable: false, width: '100' }
                                                  , { text: 'EndMonth', dataField: 'EndMonth', filtertype: 'input', editable: false, width: '100' }
                                                  , { text: 'EndYear', dataField: 'EndYear', filtertype: 'input', editable: false, width: '100' }
                                                  , { text: 'CreatedBy', dataField: 'CreatedBy', filtertype: 'input', editable: false, width: '100' }
                                                  , { text: 'CreatedDate', dataField: 'CreatedDate', filtertype: 'input', editable: false, width: '100' }
                                         ]

                            });

                        }
                    }
                });


                button.click(function () {

                    var listOfParamsAddFC = {
                        DATE : listOfParams['Date'],
                        CSSDriver : listOfParams['Driver'],
                        GOC : goc.val(),
                        IsDS : type.val(),
                        Center : center.val(),
                        Total : total.val(),
                        Start :start.val(),
                        End : end.val(),
                        HaveEnd: isEndDate.is(":checked"),
                        driver: driverCB.val()
                    }

                    _callServer({
                        loadingMsgType: "fullLoading",loadingMsg: "Loading data...", async: false,
                        url: '/ForeCast/addForeCastValues',
                        data: {
                            'data': JSON.stringify(listOfParamsAddFC)
                        },
                        type: "post",
                        success: function (json) {

                            _callServer({
                                loadingMsgType: "fullLoading",loadingMsg: "Loading data...", async: false,
                                url: '/ForeCast/getRowDetailManagedSegmentView',
                                data: {
                                    'data': JSON.stringify(listOfParams)
                                },
                                type: "post",
                                success: function (json) {
                                    nestedData = jQuery.parseJSON(json);

                                    var orderssource = {
                                        localdata: nestedData,
                                        datafields:
                                           [
                                            { name: 'ID', type: 'string' }
                                          , { name: 'GOC', type: 'string' }
                                          , { name: 'JAN', type: 'number' }
                                          , { name: 'FEB', type: 'number' }
                                          , { name: 'MAR', type: 'number' }
                                          , { name: 'APR', type: 'number' }
                                          , { name: 'MAY', type: 'number' }
                                          , { name: 'JUN', type: 'number' }
                                          , { name: 'JUL', type: 'number' }
                                          , { name: 'AUG', type: 'number' }
                                          , { name: 'SEP', type: 'number' }
                                          , { name: 'OCT', type: 'number' }
                                          , { name: 'NOV', type: 'number' }
                                          , { name: 'DEC', type: 'number' }
                                          , { name: 'IsDirectStaff', type: 'string' }
                                           ],
                                        datatype: "json"
                                    };

                                    var nestedGridAdapter = new $.jqx.dataAdapter(orderssource);
                                    //nestedGridAdapter.dataBind();

                                    if (grid2 != null) {

                                        grid2.jqxGrid({

                                            height: 350,
                                            theme: 'blackberry',
                                            width: '100%',
                                            enabletooltips: true,
                                            source: nestedGridAdapter,
                                            selectionmode: 'multiplecellsadvanced',
                                            editable: true,
                                            showaggregates: true,
                                            columns: [
                                                        { text: 'GOC', dataField: 'GOC', filtertype: 'input', editable: false, width: '22%' }
                                                 , { text: 'IsDirectStaff', dataField: 'IsDirectStaff', filtertype: 'input', editable: false, width: '8%' }
                                                 , {
                                                     text: JAN, dataField: 'JAN', filtertype: 'input', cellclassname: JANclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'JAN' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: FEB, dataField: 'FEB', filtertype: 'input', cellclassname: FEBclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'FEB' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: MAR, dataField: 'MAR', filtertype: 'input', cellclassname: MARclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'MAR' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: APR, dataField: 'APR', filtertype: 'input', cellclassname: APRclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'APR' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: MAY, dataField: 'MAY', filtertype: 'input', cellclassname: MAYclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'MAY' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: JUN, dataField: 'JUN', filtertype: 'input', cellclassname: JUNclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'JUN' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: JUL, dataField: 'JUL', filtertype: 'input', cellclassname: JULclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'JUL' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: AUG, dataField: 'AUG', filtertype: 'input', cellclassname: AUGclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'AUG' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: SEP, dataField: 'SEP', filtertype: 'input', cellclassname: SEPclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'SEP' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: OCT, dataField: 'OCT', filtertype: 'input', cellclassname: OCTclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'OCT' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: NOV, dataField: 'NOV', filtertype: 'input', cellclassname: NOVclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'NOV' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: DEC, dataField: 'DEC', filtertype: 'input', cellclassname: DECclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'DEC' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }

                                            ]

                                        });

                                    }
                                }
                            });

                            _callServer({
                                loadingMsgType: "fullLoading",loadingMsg: "Loading data...", async: false,
                                url: '/ForeCast/getFCDetailGrid',
                                data: {
                                    'data': JSON.stringify(listOfParams)
                                },
                                type: "post",
                                success: function (json) {
                                    nestedData = jQuery.parseJSON(json);

                                    var orderssource = {
                                        localdata: nestedData,
                                        datafields:
                                           [
                                            { name: 'ID', type: 'string' }
                                          , { name: 'PeriodID', type: 'string' }
                                          , { name: 'CSSDriverID', type: 'string' }
                                          , { name: 'GOC', type: 'string' }
                                          , { name: 'StartMonth', type: 'string' }
                                          , { name: 'StartYear', type: 'string' }
                                          , { name: 'EndMonth', type: 'string' }
                                          , { name: 'EndYear', type: 'string' }
                                          , { name: 'CreatedBy', type: 'string' }
                                          , { name: 'CreatedDate', type: 'string' }
                                          , { name: 'Enable', type: 'string' }
                                          , { name: 'HaveEndDate', type: 'string' }
                                          , { name: 'Total', type: 'string' }
                                          , { name: 'IsDS', type: 'string' }
                                          , { name: 'CSSDriver', type: 'string' }
                                          , { name: 'Center', type: 'string' }
                                           ],
                                        datatype: "json"
                                    };

                                    var nestedGridAdapter = new $.jqx.dataAdapter(orderssource);
                                    //nestedGridAdapter.dataBind();

                                    if (gridFC != null) {

                                        gridFC.jqxGrid({

                                            height: 240,
                                            theme: 'blackberry',
                                            width: '100%',
                                            enabletooltips: true,
                                            source: nestedGridAdapter,
                                            editable: true,
                                            showaggregates: true,
                                            showtoolbar: true,
                                            rendertoolbar: function (toolbar) {
                                                var me = this;
                                                var container = $("<div style='margin: 5px;'></div>");
                                                toolbar.append(container);
                                                container.append('<input style="margin-left: 5px;" id="deleterowbutton" type="button" value="Delete Selected Row" />');
                                                $("#deleterowbutton").jqxButton();
                                                // update row.
                                                // delete row.
                                                $("#deleterowbutton").on('click', function () {
                                                    var selectedrowindex = gridFC.jqxGrid('getselectedrowindex');
                                                    var rowscount = gridFC.jqxGrid('getdatainformation').rowscount;


                                                    var listOfParamsDeleteForeCast = {
                                                        ID: gridFC.jqxGrid('getrowdata', selectedrowindex).ID
                                                    }

                                                    _callServer({
                                                        loadingMsgType: "fullLoading",loadingMsg: "Loading data...", async: false,
                                                        url: '/ForeCast/deleteForeCastDetail',
                                                        data: {
                                                            'data': JSON.stringify(listOfParamsDeleteForeCast)
                                                        },
                                                        type: "post",
                                                        success: function (json) {
                                                        }
                                                    });

                                                    if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                                                        var id = gridFC.jqxGrid('getrowid', selectedrowindex);
                                                        var commit = gridFC.jqxGrid('deleterow', id);
                                                    }
                                                });
                                            },
                                            columns: [
                                                                { text: 'CSSDriver', dataField: 'CSSDriver', filtertype: 'input', editable: false, width: '100' }
                                                              , { text: 'Center', dataField: 'Center', filtertype: 'input', editable: false, width: '100' }
                                                              , { text: 'Total', dataField: 'Total', filtertype: 'input', editable: false, width: '100' }
                                                              , { text: 'GOC', dataField: 'GOC', filtertype: 'input', editable: false, width: '100' }
                                                              , { text: 'IsDS', dataField: 'IsDS', filtertype: 'input', editable: false, width: '100' }
                                                              , { text: 'StartMonth', dataField: 'StartMonth', filtertype: 'input', editable: false, width: '100' }
                                                              , { text: 'StartYear', dataField: 'StartYear', filtertype: 'input', editable: false, width: '100' }
                                                              , { text: 'HaveEndDate', dataField: 'HaveEndDate', filtertype: 'input', editable: false, width: '100' }
                                                              , { text: 'EndMonth', dataField: 'EndMonth', filtertype: 'input', editable: false, width: '100' }
                                                              , { text: 'EndYear', dataField: 'EndYear', filtertype: 'input', editable: false, width: '100' }
                                                              , { text: 'CreatedBy', dataField: 'CreatedBy', filtertype: 'input', editable: false, width: '100' }
                                                              , { text: 'CreatedDate', dataField: 'CreatedDate', filtertype: 'input', editable: false, width: '100' }
                                            ]

                                        });

                                    }
                                }
                            });


                        }
                    });


                });

            };


            var rowDetailTemp = "<table  style='width:98%'> \
                                    <tr> \
                                    <td  style='width:50%;vertical-align:top;'> \
                                        <fieldset><legend>GOC Detail</legend> \
                                            <div id='grid1' ></div> \
                                        </fieldset> \
                                    </td> \
                                    <td  style='width:50%;vertical-align:top;'> \
                                        <fieldset> \
                                        <legend>FC Detail</legend> \
                                            <table style='width:100%'> \
                                                <tr  style='width:100%'> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:18%'>Total</td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:18%'>Start</td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:18%'>Have End</td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:18%'>End</td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:18%'>Driver</td> \
                                                </tr> \
                                                <tr  style='width:100%'> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:18%'><div id='total'></div></td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:18%'><div id='start'></div></td>   \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:18%'><center><input type='checkbox'/></center></td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:18%'><div id='end'></div></td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:18%'><div id='driverCB'></div></td> \
                                                </tr>         \
                                                </table><table>        \
                                                <tr  style='width:100%'> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:23%'><br/>Goc</td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:23%'><br/>Type</td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:23%'><br/>Center</td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:23%'></td> \
                                                </tr> \
                                                    <tr  style='width:100%'> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:23%'><div id='goc'></div></td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:23%'><div id='type'></div>   </td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:23%'><div id='center'></div>   </td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:23%'><input type='button' class='btn btn-primary btn-icon bottom15 right15' value='Add' /></td> \
                                                </tr>         \
                                                </table>      \
                                                <table   style='width:100%'> \
                                                    <tr   style='width:100%'> \
                                                        <td  style='width:2%'> \
                                                        </td> \
                                                        <td  style='width:98%'> \
                                                            <div id='gridFCdetail'></div> \
                                                        </td> \
                                                    </tr> \
                                                </table> \
                                       </fieldset>   \
                                    </td>         \
                                    </tr>         \
                                    </table> \
                                    <br/> <center>*When you finish adding the forecast please refresh</center> \
                                    <center><input type='button' class='btn btn-primary btn-icon bottom15 right15' onclick='ReprocessData()'  value='Refresh principal grid.' /></center><br/> ";


            if (response.length > 0) {

                $(grid).jqxGrid(
               {
                   width: '98%',
                   columnsresize: false,
                   autoheight: true,
                   filterable: false,
                   pagermode: 'default',
                   source: dataAdapter,
                   showheader: true,
                   enablebrowserselection: true,
                   source: dataAdapter,
                   theme: 'blackberry',
                   showstatusbar: false,
                   statusbarheight: 30,
                   editable: true,
                   enabletooltips: true,
                   selectionmode: 'multiplecellsadvanced',
                   showaggregates: false,
                   rowdetails: true,
                   initrowdetails: initrowdetails,
                   rowdetailstemplate: { rowdetails: rowDetailTemp, rowdetailsheight: 510, rowdetailshidden: true },
                   columns: [
                                , { text: 'ManagedSegment', dataField: 'ManagedSegment', filtertype: 'input', editable: false, width: '20%' }
                                , { text: 'CSSDriver', dataField: 'CSSDriver', filtertype: 'input', editable: false, width: '20%' }
                                , { text: JAN, dataField: 'JAN', cellclassname: JANclass, filtertype: 'input', editable: false, width: '5%' }
                                , { text: FEB, dataField: 'FEB', cellclassname: FEBclass, filtertype: 'input', editable: false, width: '5%' }
                                , { text: MAR, dataField: 'MAR', cellclassname: MARclass, filtertype: 'input', editable: false, width: '5%' }
                                , { text: APR, dataField: 'APR', cellclassname: APRclass, filtertype: 'input', editable: false, width: '5%' }
                                , { text: MAY, dataField: 'MAY', cellclassname: MAYclass, filtertype: 'input', editable: false, width: '5%' }
                                , { text: JUN, dataField: 'JUN', cellclassname: JUNclass, filtertype: 'input', editable: false, width: '5%' }
                                , { text: JUL, dataField: 'JUL', cellclassname: JULclass, filtertype: 'input', editable: false, width: '5%' }
                                , { text: AUG, dataField: 'AUG', cellclassname: AUGclass, filtertype: 'input', editable: false, width: '5%' }
                                , { text: SEP, dataField: 'SEP', cellclassname: SEPclass, filtertype: 'input', editable: false, width: '5%' }
                                , { text: OCT, dataField: 'OCT', cellclassname: OCTclass, filtertype: 'input', editable: false, width: '5%' }
                                , { text: NOV, dataField: 'NOV', cellclassname: NOVclass, filtertype: 'input', editable: false, width: '5%' }
                                , { text: DEC, dataField: 'DEC', cellclassname: DECclass, filtertype: 'input', editable: false, width: '5%' }


                   ]
               });

                NewFirst = false;

            }

        }
    });

}

function deleteForeCastValues(row, parameters) {

    var Parame = parameters;


}

function downloadForeCast() {
    _generatePDFFromHtml('#forecastReport', 'ForeCast');

    GenerateReport();
}



function ReprocessData()
{
    _callServer({
        loadingMsgType: "fullLoading",loadingMsg: "Loading data...", async: false,
        url: '/Forecast/ReprocessData' ,
        type: "post",
        success: function (json) {
            GenerateReport();
        }
    });

}



function getGridForecastCenter(pMS, first, grid, parameters) {

    var CENTER = $('#cbCenter').multiselect("getChecked");
    var DATE = $('#cbDate').multiselect("getChecked");
    var CenterParameter = '0'
    var DateParameter = '0'

    var DirectStaff = $('#cbDirectStaff').multiselect("getChecked");
    var CSSDriver = $('#cbDriver').multiselect("getChecked");
    var Goc = $('#cbGOC').multiselect("getChecked");


    var MSCB = $('#cbMS').multiselect("getChecked");

    var DirectStaffParameter = '0';
    var CSSDriverParameter = '0';
    var GocParameter = '0';


    $.each(DirectStaff, function (index, value) {
        if (index == 0) {
            DirectStaffParameter = value.defaultValue;
        }
        else {
            DirectStaffParameter = DirectStaffParameter + '~' + value.defaultValue;
        }
    });
    $.each(CSSDriver, function (index, value) {
        if (index == 0) {
            CSSDriverParameter = value.defaultValue;
        }
        else {
            CSSDriverParameter = CSSDriverParameter + '~' + value.defaultValue;
        }
    });
    $.each(Goc, function (index, value) {
        if (index == 0) {
            GocParameter = value.defaultValue;
        }
        else {
            GocParameter = GocParameter + '~' + value.defaultValue;
        }
    });
    $.each(CENTER, function (index, value) {
        if (index == 0) {
            CenterParameter = value.defaultValue;
        }
        else {
            CenterParameter = CenterParameter + '~' + value.defaultValue;
        }
    });
    $.each(DATE, function (index, value) {
        DateParameter = value.defaultValue;
    });



    var listOfParams = {
        Date: DateParameter,
        Center: CenterParameter,
        MS: pMS,
        DS: DirectStaffParameter,
        CSSDriver: CSSDriverParameter,
        GOC: GocParameter

    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/ForeCast/getForecastCenterDriver',

        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
                  {
                      unboundmode: true,
                      localdata: data2,
                      datafields:
                              [
                                { name: 'ID', type: 'string' }
		                      , { name: 'ManagedSegment', type: 'string' }
		                      , { name: 'CSSDriver', type: 'string' }
                              , { name: 'JAN', type: 'number' }
                              , { name: 'FEB', type: 'number' }
                              , { name: 'MAR', type: 'number' }
                              , { name: 'APR', type: 'number' }
                              , { name: 'MAY', type: 'number' }
                              , { name: 'JUN', type: 'number' }
                              , { name: 'JUL', type: 'number' }
                              , { name: 'AUG', type: 'number' }
                              , { name: 'SEP', type: 'number' }
                              , { name: 'OCT', type: 'number' }
                              , { name: 'NOV', type: 'number' }
                              , { name: 'DEC', type: 'number' }
                              ],
                      datatype: "json"
                  };

            var dataAdapter = new $.jqx.dataAdapter(source);

            var initrowdetails = function (index, parentElement, gridElement, record) {
                var id = record.uid.toString();

                var grid2 = $($($($($($($(parentElement).children()[0]).children()[0]).children()[0]).children()[0]).children()[0]).children()[1]);

                var total = $($($($($($($($($($($(parentElement).children()[0]).children()[0]).children()[0]).children()[1]).children()[0]).children()[1]).children()[0]).children()[1]).children()[1]).children()[0]);
                var start = $($($($($($($($($($($(parentElement).children()[0]).children()[0]).children()[0]).children()[1]).children()[0]).children()[1]).children()[0]).children()[1]).children()[3]).children()[0]);
                var isEndDate = $($($($($($($($($($($(parentElement).children()[0]).children()[0]).children()[0]).children()[1]).children()[0]).children()[1]).children()[0]).children()[1]).children()[5]).children()[0]);
                var end = $($($($($($($($($($($(parentElement).children()[0]).children()[0]).children()[0]).children()[1]).children()[0]).children()[1]).children()[0]).children()[1]).children()[7]).children()[0]);
                var driverCB = $($($($($($($($($($($(parentElement).children()[0]).children()[0]).children()[0]).children()[1]).children()[0]).children()[1]).children()[0]).children()[1]).children()[9]).children()[0]);

                var goc = $($($($($($($($($($($(parentElement).children()[0]).children()[0]).children()[0]).children()[1]).children()[0]).children()[2]).children()[0]).children()[1]).children()[1]).children()[0]);
                var type = $($($($($($($($($($($(parentElement).children()[0]).children()[0]).children()[0]).children()[1]).children()[0]).children()[2]).children()[0]).children()[1]).children()[3]).children()[0]);
                var center = $($($($($($($($($($($(parentElement).children()[0]).children()[0]).children()[0]).children()[1]).children()[0]).children()[2]).children()[0]).children()[1]).children()[5]).children()[0]);
                var button = $($($($($($($($($($($(parentElement).children()[0]).children()[0]).children()[0]).children()[1]).children()[0]).children()[2]).children()[0]).children()[1]).children()[7]).children()[0]);


                var gridFC = $($($($($($($($($($($($(parentElement).children()[0]).children()[0]).children()[0]).children()[1]).children()[0]).children()[3]).children()[0])).children()[0]).children()[1]).children()[0]);

                var availableFormats = ['Direct Staff', 'Temporal'];

                type.jqxDropDownList({ source: availableFormats, selectedIndex: 0, width: '90%', height: '25px' });

                var centerSource = ['TAMPA', 'NYC', 'DELAWARE', 'EMEA', 'COSTA RICA', 'MANILA', 'MUMBAI'];

                center.jqxDropDownList({ source: centerSource, selectedIndex: 0, width: '90%', height: '25px' });

                start.jqxDateTimeInput({ width: '90%', height: '25px', formatString: 'Y' });
                end.jqxDateTimeInput({ width: '90%', height: '25px', formatString: 'Y' });
                total.jqxNumberInput({ width: '100px', height: '25px', inputMode: 'simple', spinButtons: true, min: 0, max: 10, decimalDigits: 0 });

                var CENTER = $('#cbCenter').multiselect("getChecked");
                var DATE = $('#cbDate').multiselect("getChecked");
                var CenterParameter = '0'
                var DateParameter = '0'

                var DirectStaff = $('#cbDirectStaff').multiselect("getChecked");
                var Goc = $('#cbGOC').multiselect("getChecked");

                var DirectStaffParameter = '0';
                var GocParameter = '0';

                var MSParameter = record.ManagedSegment;
                var CSSDriverParameter = record.CSSDriver;


                $.each(DirectStaff, function (index, value) {
                    if (index == 0) {
                        DirectStaffParameter = value.defaultValue;
                    }
                    else {
                        DirectStaffParameter = DirectStaffParameter + '~' + value.defaultValue;
                    }
                });

                $.each(Goc, function (index, value) {
                    if (index == 0) {
                        GocParameter = value.defaultValue;
                    }
                    else {
                        GocParameter = GocParameter + '~' + value.defaultValue;
                    }
                });

                $.each(CENTER, function (index, value) {
                    if (index == 0) {
                        CenterParameter = value.defaultValue;
                    }
                    else {
                        CenterParameter = CenterParameter + '~' + value.defaultValue;
                    }
                });
                $.each(DATE, function (index, value) {
                    DateParameter = value.defaultValue;
                });

                var nestedData;

                var listOfParams = {
                    MS: record.ManagedSegment,
                    Driver: record.CSSDriver,
                    Date: DateParameter,
                    Center: CenterParameter,
                    GOC: GocParameter,
                    DS: DirectStaffParameter
                }

                _callServer({
                    loadingMsgType: "fullLoading", loadingMsg: "Loading data...", async: false,
                    url: '/ForeCast/getPosibleDrivers',
                    data: {
                        'data': JSON.stringify(listOfParams)
                    },
                    type: "post",
                    success: function (json) {
                        nestedData = jQuery.parseJSON(json);

                        var source2 =
                            {
                                localdata: nestedData,
                                datafields:
                                   [
                                    { name: 'Value', type: 'string' }
                                   ],
                                datatype: "json"
                            };
                        var dataAdapter2 = new $.jqx.dataAdapter(source2);

                        driverCB.jqxDropDownList({
                            selectedIndex: 2, source: dataAdapter2, displayMember: "Value", valueMember: "Value", width: '100%', height: 25
                        });

                        driverCB.on('select', function (event) {
                            if (event.args) {
                                var item = event.args.item;
                                if (item) {
                                    var valueelement = $("<div></div>");
                                    valueelement.text("Value: " + item.value);
                                    var labelelement = $("<div></div>");
                                    labelelement.text("Label: " + item.label);
                                    $("#selectionlog").children().remove();
                                    $("#selectionlog").append(labelelement);
                                    $("#selectionlog").append(valueelement);
                                }
                            }
                        });
                    }
                });


                _callServer({
                    loadingMsgType: "fullLoading", loadingMsg: "Loading data...", async: false,
                    url: '/ForeCast/getRowDetailManagedSegmentView',
                    data: {
                        'data': JSON.stringify(listOfParams)
                    },
                    type: "post",
                    success: function (json) {
                        nestedData = jQuery.parseJSON(json);

                        var source2 =
                            {
                                localdata: nestedData,
                                datafields:
                                   [
                                    { name: 'GOC', type: 'string' }
                                   ],
                                datatype: "json"
                            };
                        var dataAdapter2 = new $.jqx.dataAdapter(source2);

                        goc.jqxDropDownList({
                            selectedIndex: 2, source: dataAdapter2, displayMember: "GOC", valueMember: "GOC", width: '100%', height: 25
                        });

                        goc.on('select', function (event) {
                            if (event.args) {
                                var item = event.args.item;
                                if (item) {
                                    var valueelement = $("<div></div>");
                                    valueelement.text("Value: " + item.value);
                                    var labelelement = $("<div></div>");
                                    labelelement.text("Label: " + item.label);
                                    $("#selectionlog").children().remove();
                                    $("#selectionlog").append(labelelement);
                                    $("#selectionlog").append(valueelement);
                                }
                            }
                        });


                        var orderssource = {
                            localdata: nestedData,
                            datafields:
                               [
                                { name: 'ID', type: 'string' }
                              , { name: 'GOC', type: 'string' }
                              , { name: 'JAN', type: 'number' }
                              , { name: 'FEB', type: 'number' }
                              , { name: 'MAR', type: 'number' }
                              , { name: 'APR', type: 'number' }
                              , { name: 'MAY', type: 'number' }
                              , { name: 'JUN', type: 'number' }
                              , { name: 'JUL', type: 'number' }
                              , { name: 'AUG', type: 'number' }
                              , { name: 'SEP', type: 'number' }
                              , { name: 'OCT', type: 'number' }
                              , { name: 'NOV', type: 'number' }
                              , { name: 'DEC', type: 'number' }
                              , { name: 'IsDirectStaff', type: 'string' }
                               ],
                            datatype: "json"
                        };

                        var nestedGridAdapter = new $.jqx.dataAdapter(orderssource);


                        var initrowdetailsGrid2 = function (index, parentElement, gridElement, record) {

                            //var id = record.uid.toString();

                            var gridDriverDetail = $($(parentElement).children()[0]);

                            var nestedDataGridDriverDetail;

                            var listOfParamsGrid2 = {
                                MS: MSParameter,
                                Driver: CSSDriverParameter,
                                Date: DateParameter,
                                Center: CenterParameter,
                                GOC: record.GOC,
                                DS: record.IsDirectStaff,
                            }

                            _callServer({
                                loadingMsgType: "fullLoading", loadingMsg: "Loading data...", async: false,
                                url: '/ForeCast/getRowDetailGOCbyDrivers',
                                data: {
                                    'data': JSON.stringify(listOfParamsGrid2)
                                },
                                type: "post",
                                success: function (json) {
                                    nestedDataGridDriverDetail = jQuery.parseJSON(json);

                                    var orderssourceDriverDetail = {
                                        localdata: nestedDataGridDriverDetail,
                                        datafields:
                                           [
                                                { name: 'ID', type: 'string' }
                                              , { name: 'Driver', type: 'string' }
                                              , { name: 'JAN', type: 'number' }
                                              , { name: 'FEB', type: 'number' }
                                              , { name: 'MAR', type: 'number' }
                                              , { name: 'APR', type: 'number' }
                                              , { name: 'MAY', type: 'number' }
                                              , { name: 'JUN', type: 'number' }
                                              , { name: 'JUL', type: 'number' }
                                              , { name: 'AUG', type: 'number' }
                                              , { name: 'SEP', type: 'number' }
                                              , { name: 'OCT', type: 'number' }
                                              , { name: 'NOV', type: 'number' }
                                              , { name: 'DEC', type: 'number' }
                                           ],
                                        datatype: "json"
                                    };

                                    var nestedGridAdapterGridDriverDetail = new $.jqx.dataAdapter(orderssourceDriverDetail);
                                    // nestedGridAdapterGridDriverDetail.dataBind();

                                    if (gridDriverDetail != null) {
                                        gridDriverDetail.jqxGrid({
                                            height: 150,
                                            theme: 'blackberry',
                                            width: '80%',
                                            enabletooltips: true,
                                            source: nestedGridAdapterGridDriverDetail,
                                            selectionmode: 'multiplecellsadvanced',
                                            editable: true,
                                            showaggregates: true,
                                            columns: [

                                                               { text: 'Driver', dataField: 'Driver', filtertype: 'input', editable: false, width: '22%' }
                                                             , {
                                                                 text: JAN, dataField: 'JAN', filtertype: 'input', cellclassname: JANclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                 createeditor: function (row, cellvalue, editor) {
                                                                     editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'JAN' });
                                                                 }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                     var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                     function (key, value) {
                                                                         var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                         { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                     }); renderstring += "</div>"; return renderstring;
                                                                 }
                                                             }
                                                             , {
                                                                 text: FEB, dataField: 'FEB', filtertype: 'input', cellclassname: FEBclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                 createeditor: function (row, cellvalue, editor) {
                                                                     editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'FEB' });
                                                                 }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                     var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                     function (key, value) {
                                                                         var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                         { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                     }); renderstring += "</div>"; return renderstring;
                                                                 }
                                                             }
                                                             , {
                                                                 text: MAR, dataField: 'MAR', filtertype: 'input', cellclassname: MARclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                 createeditor: function (row, cellvalue, editor) {
                                                                     editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'MAR' });
                                                                 }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                     var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                     function (key, value) {
                                                                         var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                         { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                     }); renderstring += "</div>"; return renderstring;
                                                                 }
                                                             }
                                                             , {
                                                                 text: APR, dataField: 'APR', filtertype: 'input', cellclassname: APRclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                 createeditor: function (row, cellvalue, editor) {
                                                                     editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'APR' });
                                                                 }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                     var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                     function (key, value) {
                                                                         var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                         { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                     }); renderstring += "</div>"; return renderstring;
                                                                 }
                                                             }
                                                             , {
                                                                 text: MAY, dataField: 'MAY', filtertype: 'input', cellclassname: MAYclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                 createeditor: function (row, cellvalue, editor) {
                                                                     editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'MAY' });
                                                                 }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                     var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                     function (key, value) {
                                                                         var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                         { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                     }); renderstring += "</div>"; return renderstring;
                                                                 }
                                                             }
                                                             , {
                                                                 text: JUN, dataField: 'JUN', filtertype: 'input', cellclassname: JUNclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                 createeditor: function (row, cellvalue, editor) {
                                                                     editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'JUN' });
                                                                 }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                     var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                     function (key, value) {
                                                                         var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                         { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                     }); renderstring += "</div>"; return renderstring;
                                                                 }
                                                             }
                                                             , {
                                                                 text: JUL, dataField: 'JUL', filtertype: 'input', cellclassname: JULclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                 createeditor: function (row, cellvalue, editor) {
                                                                     editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'JUL' });
                                                                 }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                     var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                     function (key, value) {
                                                                         var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                         { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                     }); renderstring += "</div>"; return renderstring;
                                                                 }
                                                             }
                                                             , {
                                                                 text: AUG, dataField: 'AUG', filtertype: 'input', cellclassname: AUGclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                 createeditor: function (row, cellvalue, editor) {
                                                                     editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'AUG' });
                                                                 }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                     var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                     function (key, value) {
                                                                         var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                         { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                     }); renderstring += "</div>"; return renderstring;
                                                                 }
                                                             }
                                                             , {
                                                                 text: SEP, dataField: 'SEP', filtertype: 'input', cellclassname: SEPclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                 createeditor: function (row, cellvalue, editor) {
                                                                     editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'SEP' });
                                                                 }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                     var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                     function (key, value) {
                                                                         var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                         { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                     }); renderstring += "</div>"; return renderstring;
                                                                 }
                                                             }
                                                             , {
                                                                 text: OCT, dataField: 'OCT', filtertype: 'input', cellclassname: OCTclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                 createeditor: function (row, cellvalue, editor) {
                                                                     editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'OCT' });
                                                                 }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                     var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                     function (key, value) {
                                                                         var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                         { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                     }); renderstring += "</div>"; return renderstring;
                                                                 }
                                                             }
                                                             , {
                                                                 text: NOV, dataField: 'NOV', filtertype: 'input', cellclassname: NOVclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                 createeditor: function (row, cellvalue, editor) {
                                                                     editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'NOV' });
                                                                 }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                     var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                     function (key, value) {
                                                                         var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                         { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                     }); renderstring += "</div>"; return renderstring;
                                                                 }
                                                             }
                                                             , {
                                                                 text: DEC, dataField: 'DEC', filtertype: 'input', cellclassname: DECclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                                 createeditor: function (row, cellvalue, editor) {
                                                                     editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'DEC' });
                                                                 }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                                     var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                                     function (key, value) {
                                                                         var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                                         { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                                     }); renderstring += "</div>"; return renderstring;
                                                                 }
                                                             }


                                            ]

                                        });

                                    }
                                }
                            });


                        };


                        //nestedGridAdapter.dataBind();

                        if (grid2 != null) {

                            grid2.jqxGrid({

                                height: 350,
                                theme: 'blackberry',
                                width: '100%',
                                enabletooltips: true,
                                source: nestedGridAdapter,
                                selectionmode: 'multiplecellsadvanced',
                                editable: true,
                                showaggregates: true,
                                rowdetails: true,
                                initrowdetails: initrowdetailsGrid2,
                                rowdetailstemplate: { rowdetails: "<div id='gridDriverDetail' style='margin: 0px;top:5px;'></div>", rowdetailsheight: 200, rowdetailshidden: true },
                                columns: [

                                                   { text: 'GOC', dataField: 'GOC', filtertype: 'input', editable: false, width: '22%' }
                                                 , { text: 'Type', dataField: 'IsDirectStaff', filtertype: 'input', editable: false, width: '8%' }
                                                 , {
                                                     text: JAN, dataField: 'JAN', filtertype: 'input', cellclassname: JANclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'JAN' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: FEB, dataField: 'FEB', filtertype: 'input', cellclassname: FEBclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'FEB' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: MAR, dataField: 'MAR', filtertype: 'input', cellclassname: MARclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'MAR' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: APR, dataField: 'APR', filtertype: 'input', cellclassname: APRclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'APR' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: MAY, dataField: 'MAY', filtertype: 'input', cellclassname: MAYclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'MAY' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: JUN, dataField: 'JUN', filtertype: 'input', cellclassname: JUNclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'JUN' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: JUL, dataField: 'JUL', filtertype: 'input', cellclassname: JULclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'JUL' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: AUG, dataField: 'AUG', filtertype: 'input', cellclassname: AUGclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'AUG' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: SEP, dataField: 'SEP', filtertype: 'input', cellclassname: SEPclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'SEP' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: OCT, dataField: 'OCT', filtertype: 'input', cellclassname: OCTclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'OCT' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: NOV, dataField: 'NOV', filtertype: 'input', cellclassname: NOVclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'NOV' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: DEC, dataField: 'DEC', filtertype: 'input', cellclassname: DECclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'DEC' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }


                                ]

                            });

                        }
                    }
                });

                _callServer({
                    loadingMsgType: "fullLoading", loadingMsg: "Loading data...", async: false,
                    url: '/ForeCast/getFCDetailGrid',
                    data: {
                        'data': JSON.stringify(listOfParams)
                    },
                    type: "post",
                    success: function (json) {
                        nestedData = jQuery.parseJSON(json);

                        var orderssource = {
                            localdata: nestedData,
                            datafields:
                               [
                                { name: 'ID', type: 'string' }
                              , { name: 'PeriodID', type: 'string' }
                              , { name: 'CSSDriverID', type: 'string' }
	                          , { name: 'GOC', type: 'string' }
                              , { name: 'StartMonth', type: 'string' }
                              , { name: 'StartYear', type: 'string' }
                              , { name: 'EndMonth', type: 'string' }
                              , { name: 'EndYear', type: 'string' }
                              , { name: 'CreatedBy', type: 'string' }
                              , { name: 'CreatedDate', type: 'string' }
                              , { name: 'Enable', type: 'string' }
                              , { name: 'HaveEndDate', type: 'string' }
                              , { name: 'Total', type: 'string' }
	                          , { name: 'IsDS', type: 'string' }
	                          , { name: 'CSSDriver', type: 'string' }
                              , { name: 'Center', type: 'string' }
                               ],
                            datatype: "json"
                        };

                        var nestedGridAdapter = new $.jqx.dataAdapter(orderssource);
                        //nestedGridAdapter.dataBind();

                        if (gridFC != null) {

                            gridFC.jqxGrid({

                                height: 240,
                                theme: 'blackberry',
                                width: '100%',
                                enabletooltips: true,
                                source: nestedGridAdapter,
                                editable: true,
                                showaggregates: true,
                                showtoolbar: true,
                                rendertoolbar: function (toolbar) {
                                    var me = this;
                                    var container = $("<div style='margin: 5px;'></div>");
                                    toolbar.append(container);
                                    container.append('<input style="margin-left: 5px;" id="deleterowbutton" type="button" value="Delete Selected Row" />');
                                    $("#deleterowbutton").jqxButton();
                                    // update row.
                                    // delete row.
                                    $("#deleterowbutton").on('click', function () {
                                        var selectedrowindex = gridFC.jqxGrid('getselectedrowindex');
                                        var rowscount = gridFC.jqxGrid('getdatainformation').rowscount;

                                        var listOfParamsDeleteForeCast = {
                                            ID: gridFC.jqxGrid('getrowdata', selectedrowindex).ID
                                        }

                                        _callServer({
                                            loadingMsgType: "fullLoading", loadingMsg: "Loading data...", async: false,
                                            url: '/ForeCast/deleteForeCastDetail',
                                            data: {
                                                'data': JSON.stringify(listOfParamsDeleteForeCast)
                                            },
                                            type: "post",
                                            success: function (json) {
                                            }
                                        });

                                        if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                                            var id = gridFC.jqxGrid('getrowid', selectedrowindex);
                                            var commit = gridFC.jqxGrid('deleterow', id);
                                        }
                                    });
                                },
                                columns: [
                                                    { text: 'CSSDriver', dataField: 'CSSDriver', filtertype: 'input', editable: false, width: '100' }
                                                  , { text: 'Center', dataField: 'Center', filtertype: 'input', editable: false, width: '100' }
                                                  , { text: 'Total', dataField: 'Total', filtertype: 'input', editable: false, width: '100' }
	                                              , { text: 'GOC', dataField: 'GOC', filtertype: 'input', editable: false, width: '100' }
                                                  , { text: 'IsDS', dataField: 'IsDS', filtertype: 'input', editable: false, width: '100' }
                                                  , { text: 'StartMonth', dataField: 'StartMonth', filtertype: 'input', editable: false, width: '100' }
                                                  , { text: 'StartYear', dataField: 'StartYear', filtertype: 'input', editable: false, width: '100' }
                                                  , { text: 'HaveEndDate', dataField: 'HaveEndDate', filtertype: 'input', editable: false, width: '100' }
                                                  , { text: 'EndMonth', dataField: 'EndMonth', filtertype: 'input', editable: false, width: '100' }
                                                  , { text: 'EndYear', dataField: 'EndYear', filtertype: 'input', editable: false, width: '100' }
                                                  , { text: 'CreatedBy', dataField: 'CreatedBy', filtertype: 'input', editable: false, width: '100' }
                                                  , { text: 'CreatedDate', dataField: 'CreatedDate', filtertype: 'input', editable: false, width: '100' }
                                ]

                            });

                        }
                    }
                });


                button.click(function () {

                    var listOfParamsAddFC = {
                        DATE: listOfParams['Date'],
                        CSSDriver: listOfParams['Driver'],
                        GOC: goc.val(),
                        IsDS: type.val(),
                        Center: center.val(),
                        Total: total.val(),
                        Start: start.val(),
                        End: end.val(),
                        HaveEnd: isEndDate.is(":checked"),
                        driver: driverCB.val()
                    }

                    _callServer({
                        loadingMsgType: "fullLoading", loadingMsg: "Loading data...", async: false,
                        url: '/ForeCast/addForeCastValues',
                        data: {
                            'data': JSON.stringify(listOfParamsAddFC)
                        },
                        type: "post",
                        success: function (json) {

                            _callServer({
                                loadingMsgType: "fullLoading", loadingMsg: "Loading data...", async: false,
                                url: '/ForeCast/getRowDetailManagedSegmentView',
                                data: {
                                    'data': JSON.stringify(listOfParams)
                                },
                                type: "post",
                                success: function (json) {
                                    nestedData = jQuery.parseJSON(json);

                                    var orderssource = {
                                        localdata: nestedData,
                                        datafields:
                                           [
                                            { name: 'ID', type: 'string' }
                                          , { name: 'GOC', type: 'string' }
                                          , { name: 'JAN', type: 'number' }
                                          , { name: 'FEB', type: 'number' }
                                          , { name: 'MAR', type: 'number' }
                                          , { name: 'APR', type: 'number' }
                                          , { name: 'MAY', type: 'number' }
                                          , { name: 'JUN', type: 'number' }
                                          , { name: 'JUL', type: 'number' }
                                          , { name: 'AUG', type: 'number' }
                                          , { name: 'SEP', type: 'number' }
                                          , { name: 'OCT', type: 'number' }
                                          , { name: 'NOV', type: 'number' }
                                          , { name: 'DEC', type: 'number' }
                                          , { name: 'IsDirectStaff', type: 'string' }
                                           ],
                                        datatype: "json"
                                    };

                                    var nestedGridAdapter = new $.jqx.dataAdapter(orderssource);
                                    //nestedGridAdapter.dataBind();

                                    if (grid2 != null) {

                                        grid2.jqxGrid({

                                            height: 350,
                                            theme: 'blackberry',
                                            width: '100%',
                                            enabletooltips: true,
                                            source: nestedGridAdapter,
                                            selectionmode: 'multiplecellsadvanced',
                                            editable: true,
                                            showaggregates: true,
                                            columns: [
                                                        { text: 'GOC', dataField: 'GOC', filtertype: 'input', editable: false, width: '22%' }
                                                 , { text: 'IsDirectStaff', dataField: 'IsDirectStaff', filtertype: 'input', editable: false, width: '8%' }
                                                 , {
                                                     text: JAN, dataField: 'JAN', filtertype: 'input', cellclassname: JANclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'JAN' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: FEB, dataField: 'FEB', filtertype: 'input', cellclassname: FEBclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'FEB' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: MAR, dataField: 'MAR', filtertype: 'input', cellclassname: MARclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'MAR' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: APR, dataField: 'APR', filtertype: 'input', cellclassname: APRclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'APR' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: MAY, dataField: 'MAY', filtertype: 'input', cellclassname: MAYclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'MAY' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: JUN, dataField: 'JUN', filtertype: 'input', cellclassname: JUNclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'JUN' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: JUL, dataField: 'JUL', filtertype: 'input', cellclassname: JULclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'JUL' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: AUG, dataField: 'AUG', filtertype: 'input', cellclassname: AUGclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'AUG' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: SEP, dataField: 'SEP', filtertype: 'input', cellclassname: SEPclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'SEP' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: OCT, dataField: 'OCT', filtertype: 'input', cellclassname: OCTclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'OCT' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: NOV, dataField: 'NOV', filtertype: 'input', cellclassname: NOVclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'NOV' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }
                                                 , {
                                                     text: DEC, dataField: 'DEC', filtertype: 'input', cellclassname: DECclass, columntype: 'numberinput', editable: false, width: '5.8%%',
                                                     createeditor: function (row, cellvalue, editor) {
                                                         editor.jqxNumberInput({ decimalDigits: 0, digits: 3, min: 'DEC' });
                                                     }, aggregates: ['sum'], aggregatesrenderer: function (aggregates, column, element, summaryData) {
                                                         var renderstring = "<div class='jqx-widget-content  style='float: left; width: 100%; height: 100%; '>"; $.each(aggregates,
                                                         function (key, value) {
                                                             var name = key == 'sum' ? 'Sum' : 'Avg'; var color = 'blue'; if (key == 'sum' && summaryData['sum'] < 0)
                                                             { color = 'red'; } renderstring += '<div style="color: ' + color + '; position: relative; margin: 6px; text-align: left; overflow: hidden;">' + value + '</div>';
                                                         }); renderstring += "</div>"; return renderstring;
                                                     }
                                                 }

                                            ]

                                        });

                                    }
                                }
                            });

                            _callServer({
                                loadingMsgType: "fullLoading", loadingMsg: "Loading data...", async: false,
                                url: '/ForeCast/getFCDetailGrid',
                                data: {
                                    'data': JSON.stringify(listOfParams)
                                },
                                type: "post",
                                success: function (json) {
                                    nestedData = jQuery.parseJSON(json);

                                    var orderssource = {
                                        localdata: nestedData,
                                        datafields:
                                           [
                                            { name: 'ID', type: 'string' }
                                          , { name: 'PeriodID', type: 'string' }
                                          , { name: 'CSSDriverID', type: 'string' }
                                          , { name: 'GOC', type: 'string' }
                                          , { name: 'StartMonth', type: 'string' }
                                          , { name: 'StartYear', type: 'string' }
                                          , { name: 'EndMonth', type: 'string' }
                                          , { name: 'EndYear', type: 'string' }
                                          , { name: 'CreatedBy', type: 'string' }
                                          , { name: 'CreatedDate', type: 'string' }
                                          , { name: 'Enable', type: 'string' }
                                          , { name: 'HaveEndDate', type: 'string' }
                                          , { name: 'Total', type: 'string' }
                                          , { name: 'IsDS', type: 'string' }
                                          , { name: 'CSSDriver', type: 'string' }
                                          , { name: 'Center', type: 'string' }
                                           ],
                                        datatype: "json"
                                    };

                                    var nestedGridAdapter = new $.jqx.dataAdapter(orderssource);
                                    //nestedGridAdapter.dataBind();

                                    if (gridFC != null) {

                                        gridFC.jqxGrid({

                                            height: 240,
                                            theme: 'blackberry',
                                            width: '100%',
                                            enabletooltips: true,
                                            source: nestedGridAdapter,
                                            editable: true,
                                            showaggregates: true,
                                            showtoolbar: true,
                                            rendertoolbar: function (toolbar) {
                                                var me = this;
                                                var container = $("<div style='margin: 5px;'></div>");
                                                toolbar.append(container);
                                                container.append('<input style="margin-left: 5px;" id="deleterowbutton" type="button" value="Delete Selected Row" />');
                                                $("#deleterowbutton").jqxButton();
                                                // update row.
                                                // delete row.
                                                $("#deleterowbutton").on('click', function () {
                                                    var selectedrowindex = gridFC.jqxGrid('getselectedrowindex');
                                                    var rowscount = gridFC.jqxGrid('getdatainformation').rowscount;


                                                    var listOfParamsDeleteForeCast = {
                                                        ID: gridFC.jqxGrid('getrowdata', selectedrowindex).ID
                                                    }

                                                    _callServer({
                                                        loadingMsgType: "fullLoading", loadingMsg: "Loading data...", async: false,
                                                        url: '/ForeCast/deleteForeCastDetail',
                                                        data: {
                                                            'data': JSON.stringify(listOfParamsDeleteForeCast)
                                                        },
                                                        type: "post",
                                                        success: function (json) {
                                                        }
                                                    });

                                                    if (selectedrowindex >= 0 && selectedrowindex < rowscount) {
                                                        var id = gridFC.jqxGrid('getrowid', selectedrowindex);
                                                        var commit = gridFC.jqxGrid('deleterow', id);
                                                    }
                                                });
                                            },
                                            columns: [
                                                                { text: 'CSSDriver', dataField: 'CSSDriver', filtertype: 'input', editable: false, width: '100' }
                                                              , { text: 'Center', dataField: 'Center', filtertype: 'input', editable: false, width: '100' }
                                                              , { text: 'Total', dataField: 'Total', filtertype: 'input', editable: false, width: '100' }
                                                              , { text: 'GOC', dataField: 'GOC', filtertype: 'input', editable: false, width: '100' }
                                                              , { text: 'IsDS', dataField: 'IsDS', filtertype: 'input', editable: false, width: '100' }
                                                              , { text: 'StartMonth', dataField: 'StartMonth', filtertype: 'input', editable: false, width: '100' }
                                                              , { text: 'StartYear', dataField: 'StartYear', filtertype: 'input', editable: false, width: '100' }
                                                              , { text: 'HaveEndDate', dataField: 'HaveEndDate', filtertype: 'input', editable: false, width: '100' }
                                                              , { text: 'EndMonth', dataField: 'EndMonth', filtertype: 'input', editable: false, width: '100' }
                                                              , { text: 'EndYear', dataField: 'EndYear', filtertype: 'input', editable: false, width: '100' }
                                                              , { text: 'CreatedBy', dataField: 'CreatedBy', filtertype: 'input', editable: false, width: '100' }
                                                              , { text: 'CreatedDate', dataField: 'CreatedDate', filtertype: 'input', editable: false, width: '100' }
                                            ]

                                        });

                                    }
                                }
                            });


                        }
                    });


                });

            };


            var rowDetailTemp = "<table  style='width:98%'> \
                                    <tr> \
                                    <td  style='width:50%;vertical-align:top;'> \
                                        <fieldset><legend>GOC Detail</legend> \
                                            <div id='grid1' ></div> \
                                        </fieldset> \
                                    </td> \
                                    <td  style='width:50%;vertical-align:top;'> \
                                        <fieldset> \
                                        <legend>FC Detail</legend> \
                                            <table style='width:100%'> \
                                                <tr  style='width:100%'> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:18%'>Total</td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:18%'>Start</td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:18%'>Have End</td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:18%'>End</td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:18%'>Driver</td> \
                                                </tr> \
                                                <tr  style='width:100%'> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:18%'><div id='total'></div></td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:18%'><div id='start'></div></td>   \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:18%'><center><input type='checkbox'/></center></td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:18%'><div id='end'></div></td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:18%'><div id='driverCB'></div></td> \
                                                </tr>         \
                                                </table><table>        \
                                                <tr  style='width:100%'> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:23%'><br/>Goc</td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:23%'><br/>Type</td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:23%'><br/>Center</td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:23%'></td> \
                                                </tr> \
                                                    <tr  style='width:100%'> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:23%'><div id='goc'></div></td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:23%'><div id='type'></div>   </td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:23%'><div id='center'></div>   </td> \
                                                    <td style='width:2%'></td> \
                                                    <td style='width:23%'><input type='button' class='btn btn-primary btn-icon bottom15 right15' value='Add' /></td> \
                                                </tr>         \
                                                </table>      \
                                                <table   style='width:100%'> \
                                                    <tr   style='width:100%'> \
                                                        <td  style='width:2%'> \
                                                        </td> \
                                                        <td  style='width:98%'> \
                                                            <div id='gridFCdetail'></div> \
                                                        </td> \
                                                    </tr> \
                                                </table> \
                                       </fieldset>   \
                                    </td>         \
                                    </tr>         \
                                    </table> \
                                    <br/> <center>*When you finish adding the forecast please refresh</center> \
                                    <center><input type='button' class='btn btn-primary btn-icon bottom15 right15' onclick='ReprocessData()'  value='Refresh principal grid.' /></center><br/> ";


            if (response.length > 0) {

                $(grid).jqxGrid(
               {
                   width: '98%',
                   columnsresize: false,
                   autoheight: true,
                   filterable: false,
                   pagermode: 'default',
                   source: dataAdapter,
                   showheader: true,
                   enablebrowserselection: true,
                   source: dataAdapter,
                   theme: 'blackberry',
                   showstatusbar: false,
                   statusbarheight: 30,
                   editable: true,
                   enabletooltips: true,
                   selectionmode: 'multiplecellsadvanced',
                   showaggregates: false,
                   rowdetails: true,
                   initrowdetails: initrowdetails,
                   rowdetailstemplate: { rowdetails: rowDetailTemp, rowdetailsheight: 510, rowdetailshidden: true },
                   columns: [
                                , { text: 'Center', dataField: 'ManagedSegment', filtertype: 'input', editable: false, width: '20%' }
                                , { text: 'CSSDriver', dataField: 'CSSDriver', filtertype: 'input', editable: false, width: '20%' }
                                , { text: JAN, dataField: 'JAN', cellclassname: JANclass, filtertype: 'input', editable: false, width: '5%' }
                                , { text: FEB, dataField: 'FEB', cellclassname: FEBclass, filtertype: 'input', editable: false, width: '5%' }
                                , { text: MAR, dataField: 'MAR', cellclassname: MARclass, filtertype: 'input', editable: false, width: '5%' }
                                , { text: APR, dataField: 'APR', cellclassname: APRclass, filtertype: 'input', editable: false, width: '5%' }
                                , { text: MAY, dataField: 'MAY', cellclassname: MAYclass, filtertype: 'input', editable: false, width: '5%' }
                                , { text: JUN, dataField: 'JUN', cellclassname: JUNclass, filtertype: 'input', editable: false, width: '5%' }
                                , { text: JUL, dataField: 'JUL', cellclassname: JULclass, filtertype: 'input', editable: false, width: '5%' }
                                , { text: AUG, dataField: 'AUG', cellclassname: AUGclass, filtertype: 'input', editable: false, width: '5%' }
                                , { text: SEP, dataField: 'SEP', cellclassname: SEPclass, filtertype: 'input', editable: false, width: '5%' }
                                , { text: OCT, dataField: 'OCT', cellclassname: OCTclass, filtertype: 'input', editable: false, width: '5%' }
                                , { text: NOV, dataField: 'NOV', cellclassname: NOVclass, filtertype: 'input', editable: false, width: '5%' }
                                , { text: DEC, dataField: 'DEC', cellclassname: DECclass, filtertype: 'input', editable: false, width: '5%' }


                   ]
               });


                NewFirst = false;
            } 

        }
    });

}