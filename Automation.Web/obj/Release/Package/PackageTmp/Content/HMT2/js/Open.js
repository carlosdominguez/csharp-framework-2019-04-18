﻿var rowid;

$(document).ready(function () {
    _hideMenu();

    XLSX.createJsXlsx({
        idElement: "js-xlsx-plugin",
        fileInput: {
            dropText: "Drop an Excel file here...",
            hideDrop: false,
            inputText: "... or click here to select a file"
        },
        onSuccess: function (jsonData) {

            var excelSource = jsonData;

            _callServer({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading data...",
                url: '/Open/uploadReenFile',
                data: {
                    'data': JSON.stringify(excelSource["Open"])
                },
                type: "post",
                success: function (json) {
                    renderGridDataSelectedFile(0);
                }
            });
        }
    });


    renderVersion();

    renderGridDataSelectedFile(0);


});

function renderVersion() {

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "",
        url: '/Open/getVersionList',
        type: "post",
        success: function (json) {

            result = jQuery.parseJSON(json);


            var source =
                {
                    localdata: result,
                    datafields:
                    [
                        { name: 'ID', type: 'string' },
                        { name: 'UploadBy', type: 'string' },
                        { name: 'UploadDate', type: 'string' },
                        { name: 'Enable', type: 'Boolean' },
                    ],
                    datatype: "array",
                    updaterow: function (rowid, rowdata) {
                    }
                };

            var dataAdapter = new $.jqx.dataAdapter(source);

            var imgEnable = function (row, datafield, value) {

                var rowData = $('#jqxgridVersion').jqxGrid('getrowdata', row);

                if (rowData.Enable == "true") {
                    return 'Selected File'
                }
                else {
                    return 'Unselected'
                }


            }

            $("#jqxgridVersion").jqxGrid(
             {
                 width: 600,
                 source: dataAdapter,
                 pageable: true,
                 autoheight: true,
                 columnsresize: true,
                 columns: [
                   { text: 'Upload ID', columntype: 'textbox', datafield: 'ID', width: '3%' },
                   { text: 'Upload By', columntype: 'textbox', datafield: 'UploadBy', width: '31%' },
                   { text: 'Upload Date', columntype: 'textbox', datafield: 'UploadDate', width: '31%' },
                   { text: 'Enable', columntype: 'textbox', cellsrenderer: imgEnable, width: '31%' }
                 ]
             });

            $("#jqxVersion").jqxDropDownButton({ width: 400, height: 25 });


            //Salvar nueva seleccion
            $("#jqxgridVersion").on('rowselect', function (event) {
                event.stopImmediatePropagation();
                var args = event.args;
                var row = $("#jqxgridVersion").jqxGrid('getrowdata', args.rowindex);
                var dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;">' + row['ID'] + ' | ' + row['UploadBy'] + ' | ' + row['UploadDate'] + '</div>';
                $("#jqxVersion").jqxDropDownButton('setContent', dropDownContent);

                rowid = row['ID'];
            });


            $.each((result), function (index, element) {
                if (element.Enable) {
                    var dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;">' + element.ID + ' | ' + element.UploadBy + ' | ' + element.UploadDate + '</div>';
                    $("#jqxVersion").jqxDropDownButton('setContent', dropDownContent);

                    rowid = element.ID;
                }
            });

        }



    });


}

function renderGridDataSelectedFile(ID) {
    var listOfParams = {
        pFile: ID
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/Open/getListReeng',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {

            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                       { name: 'ID', type: 'string' },
                       { name: 'UploadID', type: 'string' },
                       { name: 'BusinessUnit', type: 'string' },
                       { name: 'Business', type: 'string' },
                       { name: 'BusinessDetail', type: 'string' },
                       { name: 'ManagedGeographY', type: 'string' },
                       { name: 'PhysicalRegion', type: 'string' },
                       { name: 'ManagedFunction', type: 'string' },
                       { name: 'OrgLevel1', type: 'string' },
                       { name: 'OrgLevel2', type: 'string' },
                       { name: 'OrgLevel3', type: 'string' },
                       { name: 'OrgLevel4', type: 'string' },
                       { name: 'OrgLevel5', type: 'string' },
                       { name: 'OrgLevel6', type: 'string' },
                       { name: 'OrgLevel7', type: 'string' },
                       { name: 'OrgLevel8', type: 'string' },
                       { name: 'OrgLevel9', type: 'string' },
                       { name: 'OrgLevel10', type: 'string' },
                       { name: 'OrgLevel11', type: 'string' },
                       { name: 'OrgLevel12', type: 'string' },
                       { name: 'MGLevel1', type: 'string' },
                       { name: 'MGLevel2', type: 'string' },
                       { name: 'MGLevel3', type: 'string' },
                       { name: 'MGLevel4', type: 'string' },
                       { name: 'MGLevel5', type: 'string' },
                       { name: 'MGLevel6', type: 'string' },
                       { name: 'MGLevel7', type: 'string' },
                       { name: 'CSWWorkflowName', type: 'string' },
                       { name: 'SourceSystem', type: 'string' },
                       { name: 'GOC', type: 'string' },
                       { name: 'GOCDescription', type: 'string' },
                       { name: 'Recruiter', type: 'string' },
                       { name: 'RecruiterGEID', type: 'string' },
                       { name: 'RelationshipIndicator', type: 'string' },
                       { name: 'Requisition', type: 'string' },
                       { name: 'AlternateReq', type: 'string' },
                       { name: 'JobCode', type: 'string' },
                       { name: 'JobFunction', type: 'string' },
                       { name: 'RequisitionTitle', type: 'string' },
                       { name: 'CountPositions', type: 'string' },
                       { name: 'LeftToHire', type: 'string' },
                       { name: 'CurrentReqStatus', type: 'string' },
                       { name: 'ReqStatusDate', type: 'string' },
                       { name: 'FirstApprovalDate', type: 'string' },
                       { name: 'ReqCreationDate', type: 'string' },
                       { name: 'InternalPostingStatus', type: 'string' },
                       { name: 'InternalPostingDate', type: 'string' },
                       { name: 'InternalUnpostingDate', type: 'string' },
                       { name: 'PersonReplacing', type: 'string' },
                       { name: 'PersonReplacingSOEID', type: 'string' },
                       { name: 'EmployeeReferalBonus', type: 'string' },
                       { name: 'EligibleforCampsHire', type: 'string' },
                       { name: 'HiringManager', type: 'string' },
                       { name: 'HiringManagerGEID', type: 'string' },
                       { name: 'HRRepresentative', type: 'string' },
                       { name: 'FinancialJustification', type: 'string' },
                       { name: 'RequisitionJustification', type: 'string' },
                       { name: 'IsitaProductionJob', type: 'string' },
                       { name: 'FulltimePartTime', type: 'string' },
                       { name: 'OvertimeStatus', type: 'string' },
                       { name: 'ReqSalaryGrade', type: 'string' },
                       { name: 'MappedOfficerTitle', type: 'string' },
                       { name: 'OfficerTitle', type: 'string' },
                       { name: 'OfficerTitleCode', type: 'string' },
                       { name: 'StandardGrade', type: 'string' },
                       { name: 'Grade', type: 'string' },
                       { name: 'WorldRegion', type: 'string' },
                       { name: 'Country', type: 'string' },
                       { name: 'StateProvince', type: 'string' },
                       { name: 'City', type: 'string' },
                       { name: 'CSSCenter', type: 'string' },
                       { name: 'LocationCode', type: 'string' },
                       { name: 'CSC', type: 'string' },
                       { name: 'OLM', type: 'string' },
                       { name: 'CompanyCode', type: 'string' },
                       { name: 'DaysOpen', type: 'string' },
                       { name: 'AgeGrouping', type: 'string' },
                       { name: 'AccountExpenseCode', type: 'string' },
                       { name: 'Applications', type: 'string' },
                       { name: 'Internal', type: 'string' },
                       { name: 'External', type: 'string' },
                       { name: 'New', type: 'string' },
                       { name: 'Testing1', type: 'string' },
                       { name: 'Reviewed', type: 'string' },
                       { name: 'PhoneScreen', type: 'string' },
                       { name: 'Testing2', type: 'string' },
                       { name: 'Interview', type: 'string' },
                       { name: 'OfferCreation', type: 'string' },
                       { name: 'OfferExtended', type: 'string' },
                       { name: 'OfferAccepted', type: 'string' },
                       { name: 'Hired', type: 'string' },
                       { name: 'OfferDeclined', type: 'string' },
                       { name: 'ReqEmployeeStatus', type: 'string' },
                       { name: 'ReqCreatorUser', type: 'string' },
                       { name: 'APACLegalVehicle', type: 'string' },
                       { name: 'SumOfOpenDaysLeftToHire', type: 'string' }

                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            $('#jqxGridLastFile').jqxGrid(
           {
               source: dataAdapter,
               theme: 'blackberry',
               width: '100%',
               pagesize: 10,
               pageable: true,
               columnsresize: true,
              // selectionmode: 'singlecell',
               enablebrowserselection: true,
               columns: [
                 { text: 'UploadID', dataField: 'UploadID', editable: false, width: '10%' }
               , { text: 'BusinessUnit', dataField: 'BusinessUnit', editable: false, width: '10%' }
               , { text: 'Business', dataField: 'Business', editable: false, width: '10%' }
               , { text: 'BusinessDetail', dataField: 'BusinessDetail', editable: false, width: '10%' }
               , { text: 'ManagedGeographY', dataField: 'ManagedGeographY', editable: false, width: '10%' }
               , { text: 'PhysicalRegion', dataField: 'PhysicalRegion', editable: false, width: '10%' }
               , { text: 'ManagedFunction', dataField: 'ManagedFunction', editable: false, width: '10%' }
               , { text: 'CSWWorkflowName', dataField: 'CSWWorkflowName', editable: false, width: '10%' }
               , { text: 'SourceSystem', dataField: 'SourceSystem', editable: false, width: '10%' }
               , { text: 'GOC', dataField: 'GOC', editable: false, width: '10%' }
               , { text: 'GOCDescription', dataField: 'GOCDescription', editable: false, width: '10%' }
               , { text: 'Recruiter', dataField: 'Recruiter', editable: false, width: '10%' }
               , { text: 'RecruiterGEID', dataField: 'RecruiterGEID', editable: false, width: '10%' }
               , { text: 'RelationshipIndicator', dataField: 'RelationshipIndicator', editable: false, width: '10%' }
               , { text: 'Requisition', dataField: 'Requisition', editable: false, width: '10%' }
               , { text: 'AlternateReq', dataField: 'AlternateReq', editable: false, width: '10%' }
               , { text: 'JobCode', dataField: 'JobCode', editable: false, width: '10%' }
               , { text: 'JobFunction', dataField: 'JobFunction', editable: false, width: '10%' }
               , { text: 'RequisitionTitle', dataField: 'RequisitionTitle', editable: false, width: '10%' }
               , { text: 'CountPositions', dataField: 'CountPositions', editable: false, width: '10%' }
               , { text: 'LeftToHire', dataField: 'LeftToHire', editable: false, width: '10%' }
               , { text: 'CurrentReqStatus', dataField: 'CurrentReqStatus', editable: false, width: '10%' }
               , { text: 'ReqStatusDate', dataField: 'ReqStatusDate', editable: false, width: '10%' }
               , { text: 'FirstApprovalDate', dataField: 'FirstApprovalDate', editable: false, width: '10%' }
               , { text: 'ReqCreationDate', dataField: 'ReqCreationDate', editable: false, width: '10%' }
               , { text: 'InternalPostingStatus', dataField: 'InternalPostingStatus', editable: false, width: '10%' }
               , { text: 'InternalPostingDate', dataField: 'InternalPostingDate', editable: false, width: '10%' }
               , { text: 'InternalUnpostingDate', dataField: 'InternalUnpostingDate', editable: false, width: '10%' }
               , { text: 'PersonReplacing', dataField: 'PersonReplacing', editable: false, width: '10%' }
               , { text: 'PersonReplacingSOEID', dataField: 'PersonReplacingSOEID', editable: false, width: '10%' }
               , { text: 'EmployeeReferalBonus', dataField: 'EmployeeReferalBonus', editable: false, width: '10%' }
               , { text: 'EligibleforCampsHire', dataField: 'EligibleforCampsHire', editable: false, width: '10%' }
               , { text: 'HiringManager', dataField: 'HiringManager', editable: false, width: '10%' }
               , { text: 'HiringManagerGEID', dataField: 'HiringManagerGEID', editable: false, width: '10%' }
               , { text: 'HRRepresentative', dataField: 'HRRepresentative', editable: false, width: '10%' }
               , { text: 'FinancialJustification', dataField: 'FinancialJustification', editable: false, width: '10%' }
               , { text: 'RequisitionJustification', dataField: 'RequisitionJustification', editable: false, width: '10%' }
               , { text: 'IsitaProductionJob', dataField: 'IsitaProductionJob', editable: false, width: '10%' }
               , { text: 'FulltimePartTime', dataField: 'FulltimePartTime', editable: false, width: '10%' }
               , { text: 'OvertimeStatus', dataField: 'OvertimeStatus', editable: false, width: '10%' }
               , { text: 'ReqSalaryGrade', dataField: 'ReqSalaryGrade', editable: false, width: '10%' }
               , { text: 'MappedOfficerTitle', dataField: 'MappedOfficerTitle', editable: false, width: '10%' }
               , { text: 'OfficerTitle', dataField: 'OfficerTitle', editable: false, width: '10%' }
               , { text: 'OfficerTitleCode', dataField: 'OfficerTitleCode', editable: false, width: '10%' }
               , { text: 'StandardGrade', dataField: 'StandardGrade', editable: false, width: '10%' }
               , { text: 'Grade', dataField: 'Grade', editable: false, width: '10%' }
               , { text: 'WorldRegion', dataField: 'WorldRegion', editable: false, width: '10%' }
               , { text: 'Country', dataField: 'Country', editable: false, width: '10%' }
               , { text: 'StateProvince', dataField: 'StateProvince', editable: false, width: '10%' }
               , { text: 'City', dataField: 'City', editable: false, width: '10%' }
               , { text: 'CSSCenter', dataField: 'CSSCenter', editable: false, width: '10%' }
               , { text: 'LocationCode', dataField: 'LocationCode', editable: false, width: '10%' }
               , { text: 'CSC', dataField: 'CSC', editable: false, width: '10%' }
               , { text: 'OLM', dataField: 'OLM', editable: false, width: '10%' }
               , { text: 'CompanyCode', dataField: 'CompanyCode', editable: false, width: '10%' }
               , { text: 'DaysOpen', dataField: 'DaysOpen', editable: false, width: '10%' }
               , { text: 'AgeGrouping', dataField: 'AgeGrouping', editable: false, width: '10%' }
               , { text: 'AccountExpenseCode', dataField: 'AccountExpenseCode', editable: false, width: '10%' }
               , { text: 'Applications', dataField: 'Applications', editable: false, width: '10%' }
               , { text: 'Internal', dataField: 'Internal', editable: false, width: '10%' }
               , { text: 'External', dataField: 'External', editable: false, width: '10%' }
               , { text: 'New', dataField: 'New', editable: false, width: '10%' }
               , { text: 'Testing1', dataField: 'Testing1', editable: false, width: '10%' }
               , { text: 'Reviewed', dataField: 'Reviewed', editable: false, width: '10%' }
               , { text: 'PhoneScreen', dataField: 'PhoneScreen', editable: false, width: '10%' }
               , { text: 'Testing2', dataField: 'Testing2', editable: false, width: '10%' }
               , { text: 'Interview', dataField: 'Interview', editable: false, width: '10%' }
               , { text: 'OfferCreation', dataField: 'OfferCreation', editable: false, width: '10%' }
               , { text: 'OfferExtended', dataField: 'OfferExtended', editable: false, width: '10%' }
               , { text: 'OfferAccepted', dataField: 'OfferAccepted', editable: false, width: '10%' }
               , { text: 'Hired', dataField: 'Hired', editable: false, width: '10%' }
               , { text: 'OfferDeclined', dataField: 'OfferDeclined', editable: false, width: '10%' }
               , { text: 'ReqEmployeeStatus', dataField: 'ReqEmployeeStatus', editable: false, width: '10%' }
               , { text: 'ReqCreatorUser', dataField: 'ReqCreatorUser', editable: false, width: '10%' }
               , { text: 'APACLegalVehicle', dataField: 'APACLegalVehicle', editable: false, width: '10%' }
               , { text: 'SumOfOpenDaysLeftToHire', dataField: 'SumOfOpenDaysLeftToHire', editable: false, width: '10%' }




               ]

           });


        }
    });


}

function viewSelected() {
    renderGridDataSelectedFile(rowid);
}


function useForThisMonth() {
    var listOfParams = {
        pFile: rowid
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Set selection as default file...",
        url: '/Open/setAsDefault',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            renderGridDataSelectedFile(rowid);
            renderVersion();
        }
    });

}