﻿var rowid;

$(document).ready(function () {
    _hideMenu();

    XLSX.createJsXlsx({
        idElement: "js-xlsx-plugin",
        fileInput: {
            dropText: "Drop an Excel file here...",
            hideDrop: false,
            inputText: "... or click here to select a file"
        },
        onSuccess: function (jsonData) {

            var excelSource = jsonData;

            _callServer({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading data...",
                url: '/Plan/uploadReenFile',
                data: {
                    'data': JSON.stringify(excelSource["PlanTemplate"])
                },
                type: "post",
                success: function (json) {
                    renderGridDataSelectedFile(0);
                }
            });
        }
    });


    renderVersion();

    renderGridDataSelectedFile(0);


});

function renderVersion() {

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "",
        url: '/Plan/getVersionList',
        type: "post",
        success: function (json) {

            result = jQuery.parseJSON(json);


            var source =
                {
                    localdata: result,
                    datafields:
                    [
                        { name: 'ID', type: 'string' },
                        { name: 'UploadBy', type: 'string' },
                        { name: 'UploadDate', type: 'string' },
                        { name: 'Enable', type: 'Boolean' },
                    ],
                    datatype: "array",
                    updaterow: function (rowid, rowdata) {
                    }
                };

            var dataAdapter = new $.jqx.dataAdapter(source);

            var imgEnable = function (row, datafield, value) {

                var rowData = $('#jqxgridVersion').jqxGrid('getrowdata', row);

                if (rowData.Enable == "true") {
                    return 'Selected File'
                }
                else {
                    return 'Unselected'
                }


            }

            $("#jqxgridVersion").jqxGrid(
             {
                 width: 600,
                 source: dataAdapter,
                 pageable: true,
                 autoheight: true,
                 columnsresize: true,
                 columns: [
                   { text: 'Upload ID', columntype: 'textbox', datafield: 'ID', width: '3%' },
                   { text: 'Upload By', columntype: 'textbox', datafield: 'UploadBy', width: '31%' },
                   { text: 'Upload Date', columntype: 'textbox', datafield: 'UploadDate', width: '31%' },
                   { text: 'Enable', columntype: 'textbox', cellsrenderer: imgEnable, width: '31%' }
                 ]
             });

            $("#jqxVersion").jqxDropDownButton({ width: 400, height: 25 });


            //Salvar nueva seleccion
            $("#jqxgridVersion").on('rowselect', function (event) {
                event.stopImmediatePropagation();
                var args = event.args;
                var row = $("#jqxgridVersion").jqxGrid('getrowdata', args.rowindex);
                var dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;">' + row['ID'] + ' | ' + row['UploadBy'] + ' | ' + row['UploadDate'] + '</div>';
                $("#jqxVersion").jqxDropDownButton('setContent', dropDownContent);

                rowid = row['ID'];
            });


            $.each((result), function (index, element) {
                if (element.Enable) {
                    var dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;">' + element.ID + ' | ' + element.UploadBy + ' | ' + element.UploadDate + '</div>';
                    $("#jqxVersion").jqxDropDownButton('setContent', dropDownContent);

                    rowid = element.ID;
                }
            });

        }



    });


}

function renderGridDataSelectedFile(ID) {
    var listOfParams = {
        pFile: ID
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/Plan/getListReeng',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {

            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                         { name: 'ID', type: 'string' },
                         { name: 'UploadID', type: 'string' },
                         { name: 'ManagedSegment', type: 'string' },
                         { name: 'GOC', type: 'string' },
                         { name: 'Center', type: 'string' },
                         { name: 'FTEType', type: 'string' }, 
                         { name: 'DecPastPlan', type: 'string' },
                         { name: 'JanPlan', type: 'string' },
                         { name: 'FebPlan', type: 'string' },
                         { name: 'MarPlan', type: 'string' },
                         { name: 'AprPlan', type: 'string' },
                         { name: 'MayPlan', type: 'string' },
                         { name: 'JunPlan', type: 'string' },
                         { name: 'JulPlan', type: 'string' },
                         { name: 'AugPlan', type: 'string' },
                         { name: 'SepPlan', type: 'string' },
                         { name: 'OctPlan', type: 'string' },
                         { name: 'NovPlan', type: 'string' },
                         { name: 'DecPlan', type: 'string' },
                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            $('#jqxGridLastFile').jqxGrid(
           {
               source: dataAdapter,
               theme: 'blackberry',
               width: '100%',
               // autoheight: true,
               // autorowheight: true,
               pagesize: 10,
               pageable: true,
               filterable: true,
               showfilterrow: true,
               columnsresize: true,
               selectionmode: 'singlecell',
               enablebrowserselection: true,
               columns: [
                           { text: 'UploadID', dataField: 'UploadID', filtertype: 'input', editable: false, width: 60 }
                         , { text: 'ManagedSegment', dataField: 'ManagedSegment', filtertype: 'input', editable: false, width: 150 },
                         , { text: 'GOC', dataField: 'GOC', filtertype: 'input', editable: false, width: 150 },
                         , { text: 'Center', dataField: 'Center', filtertype: 'input', editable: false, width: 150 },
                         , { text: 'FTEType', dataField: 'FTEType', filtertype: 'input', editable: false, width: 150 },
                         , { text: 'DecPastPlan', dataField: 'DecPastPlan', filtertype: 'input', editable: false, width: 150 },
                         , { text: 'JanPlan', dataField: 'JanPlan', filtertype: 'input', editable: false, width: 150 },
                         , { text: 'FebPlan', dataField: 'FebPlan', filtertype: 'input', editable: false, width: 150 },
                         , { text: 'MarPlan', dataField: 'MarPlan', filtertype: 'input', editable: false, width: 150 },
                         , { text: 'AprPlan', dataField: 'AprPlan', filtertype: 'input', editable: false, width: 150 },
                         , { text: 'MayPlan', dataField: 'MayPlan', filtertype: 'input', editable: false, width: 150 },
                         , { text: 'JunPlan', dataField: 'JunPlan', filtertype: 'input', editable: false, width: 150 },
                         , { text: 'JulPlan', dataField: 'JulPlan', filtertype: 'input', editable: false, width: 150 },
                         , { text: 'AugPlan', dataField: 'AugPlan', filtertype: 'input', editable: false, width: 150 },
                         , { text: 'SepPlan', dataField: 'SepPlan', filtertype: 'input', editable: false, width: 150 },
                         , { text: 'OctPlan', dataField: 'OctPlan', filtertype: 'input', editable: false, width: 150 },
                         , { text: 'NovPlan', dataField: 'NovPlan', filtertype: 'input', editable: false, width: 150 },
                         , { text: 'DecPlan', dataField: 'DecPlan', filtertype: 'input', editable: false, width: 150 },

               ]

           });


        }
    });


}

function viewSelected() {
    renderGridDataSelectedFile(rowid);
}


function useForThisMonth() {
    var listOfParams = {
        pFile: rowid
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Set selection as default file...",
        url: '/Plan/setAsDefault',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            renderGridDataSelectedFile(rowid);
            renderVersion();
        }
    });

}