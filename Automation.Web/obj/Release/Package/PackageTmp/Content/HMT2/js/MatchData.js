﻿var SelectedGOC;

var gocSel;

$(document).ready(function () {
    _hideMenu();

   
    $('#divToView').hide();
    $('#divToView2').hide();


    $("#jqxExpanderReq").jqxExpander({ width: '100%', expanded: false, theme: 'blackberry' });
    $("#jqxExpanderDriver").jqxExpander({ width: '100%', expanded: false, theme: 'blackberry' });
    $("#jqxExpanderMOU").jqxExpander({ width: '100%', expanded: false, theme: 'blackberry' });
    $("#jqxExpanderNonMOU").jqxExpander({ width: '100%', expanded: false, theme: 'blackberry' });
    $("#jqxExpanderCenter").jqxExpander({ width: '100%', expanded: false, theme: 'blackberry' });
    renderComboBox('#cbGOC', 'Select a GOC', 'getGocList', false);

    $("#popUpAddRole").jqxWindow({
        width: 1100, height: 600, resizable: false, isModal: true, autoOpen: false, cancelButton: $("#Cancel"), modalOpacity: 0.7
    });


});

function renderComboBox(div, textDefault, procedure, pMultiple) {

    $(div).find('option').remove().end();

    _callServer({
        loadingMsg: "", async: false,
        url: '/Forecast/' + procedure,
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            $.each(response, function (index, element) {
                //$("#cbMyCalendar").append(new Option(element.Value, element.ID));
                $(div).append($('<option>', {
                    value: element.Value,
                    text: element.Value
                }));

            })

            $(div).multiselect({
                click: function (event, ui) {
                    SelectedGOC = ui.value;

                    gocSel = ui.value;
                     renderGrids(ui.value);
                }
            });


            $(div).multiselect({
                multiple: pMultiple,
                header: "Select an option",
                noneSelectedText: textDefault,
                selectedList: 1

            }).multiselectfilter();

            $(div).css('width', '100px');
        }
    });
}

function renderGrids(GOC)
{
    $('#divToView').show();
    $('#divToView2').show();

    var listOfParams = {
        GOC: GOC
    }

    renderPrincipalList(listOfParams);
    renderSOEIDRequisition(listOfParams);

    renderMOUList(listOfParams);
    renderNonMOUList(listOfParams);
    renderDriverList(listOfParams);

    renderCenterList(listOfParams);

}

function renderPrincipalList(listOfParams)
{
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/MatchData/getRoleList',
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {

            var response = jQuery.parseJSON(json);

            var source =
                  {
                      unboundmode: true,
                      localdata: response,
                      datafields:
                              [
                                    { name: 'ID', type: 'string' }
                                  , { name: 'RoleID', type: 'string' }
                                  , { name: 'Name', type: 'string' }
                                  , { name: 'MOU_NonMOU', type: 'string' }
                                  , { name: 'MOU_NonMOU_Desc', type: 'string' }
                                  , { name: 'GOCRole', type: 'string' }
                                  , { name: 'StartDate', type: 'date' }
                                  , { name: 'EndDate', type: 'date' }
                                  , { name: 'Driver', type: 'string' }
                                  , { name: 'CSSDriver', type: 'string' }
                                  , { name: 'IsDirectStaf', type: 'bool' }
                                  , { name: 'SOEID', type: 'string' }
                                  , { name: 'GEID', type: 'string' }
                                  , { name: 'GOCEmployee', type: 'string' }
                                  , { name: 'Requisition', type: 'string' }
                                  , { name: 'Number', type: 'string' }
                                  , { name: 'Center', type: 'string' }
                                  , { name: 'FirstName', type: 'string' }
                                  , { name: 'LastName', type: 'string' }
                                  
                              ],
                      datatype: "json"
                  };

            var dataAdapter = new $.jqx.dataAdapter(source);


            //Remove old grids
            var $currentJqxGrid = $("#gridPrincipal");
            var $newJqxGridDiv = $("<div id='jqxGridTemp'></div>");
            $currentJqxGrid.before($newJqxGridDiv);
            $newJqxGridDiv.attr("id", $("#gridPrincipal").attr("id"));
            $newJqxGridDiv.attr("class", $("#gridPrincipal").attr("class"));
            $currentJqxGrid.remove();

            var DeleteRole = function (row, datafield, value) {
                return '<center><i class="fa fa-trash" title="Delete this Role" style=" font-size: 23px; color: red;" onclick="deleteRole()" ></i></center>';
            }
            var getNameFromGDW = function (row, datafield, value) {
                return '<center><i class="fa fa-search" title="Get role name from GDW" style=" font-size: 23px; color: blue;" onclick="getNameRoleFromGDW()" ></i></center>';
            }

            var initrowdetails = function (index, parentElement, gridElement, record) {


                var grid = $($(parentElement).children()[0]);

                var listOfParams2 = {
                    ROLE: record.RoleID
                }



                _callServer({
                    loadingMsgType: "fullLoading",
                    loadingMsg: "Loading data...",
                    url: '/MatchData/getRoleListInside',
                    type: "post",
                    data: {
                        'data': JSON.stringify(listOfParams2)
                    },
                    success: function (json) {

                        var response = jQuery.parseJSON(json);

                        var nestedData;

                        nestedData = response;

                        var orderssource = {
                            localdata: nestedData,
                            datafields:
                               [
                                      { name: 'ID', type: 'string' }
                                    , { name: 'RoleID', type: 'string' }
                                    , { name: 'StartDate', type: 'string' }
                                    , { name: 'EndDate', type: 'string' }
                                    , { name: 'Driver', type: 'string' }
                                    , { name: 'CssDriver', type: 'string' }
                                    , { name: 'SOEID', type: 'string' }
                                    , { name: 'Goc', type: 'string' }
                                    , { name: 'Requisition', type: 'string' }
                                    , { name: 'Number', type: 'string' }
                               ],
                            datatype: "json"
                        };

                        var DeleteRole2 = function (row, datafield, value) {

                            var rowData = $(grid).jqxGrid('getrowdata', row);

                            return '<center><i class="fa fa-trash" title="Delete this Role" style=" font-size: 23px; color: red;" onclick="deleteRoleEmployee(' + rowData.ID + ')" ></i></center>';
                        }

                        var dataAdapter2 = new $.jqx.dataAdapter(orderssource);

                        grid.jqxGrid(
                        {
                            width: '80%',
                            height: 300,
                            theme: 'energyblue',
                            editable: true,
                            enabletooltips: true,
                            selectionmode: 'multiplecellsadvanced',
                            source: dataAdapter2,
                            showfilterrow: true,
                            filterable: true,
                            columns: [
                              { text: '', editable: false, cellsrenderer: DeleteRole2, width: 50 },
                              { text: 'Start', filtertype: 'input', dataField: 'StartDate' },
                              { text: 'End', filtertype: 'input', dataField: 'EndDate', editable: false },
                              { text: 'Driver', filtertype: 'input', dataField: 'Driver', editable: false },
                              { text: 'Css Driver', filtertype: 'input', dataField: 'CssDriver', editable: false },
                              { text: 'SOEID', filtertype: 'input', datafield: 'SOEID', editable: false },
                              { text: 'Requisition', filtertype: 'input', dataField: 'Requisition', editable: false },
                              { text: 'Req Data', filtertype: 'input', dataField: 'Number', editable: false }
                            ]
                        });

                    }});
            };


            $("#gridPrincipal").jqxGrid(
                    {
                        width: '100%',
                        height: 550,
                        theme: 'energyblue',
                        editable: true,
                        enabletooltips: true,
                        selectionmode: 'multiplecellsadvanced',
                        source: source,
                        rowdetails: true,
                        showfilterrow: true,
                        filterable: true,
                        initrowdetails: initrowdetails,
                        rowdetailstemplate: { rowdetails: "<div id='grid' style='margin: 0px;top:5px;'></div>", rowdetailsheight: 400, rowdetailshidden: true },
                        columns: [
                          //{ text: '', editable: false, cellsrenderer: DeleteRole, width: 50 },
                          { text: 'Role ID', filtertype: 'input', dataField: 'RoleID', width: '14%', columntype: 'input', editable: false },
                          { text: 'Role Name', filtertype: 'input', dataField: 'Name', width: '10%' },
                          //{ text: '', editable: false, cellsrenderer: getNameFromGDW, width: 50 },
                          { text: 'MOU NonMOU', filtertype: 'input', dataField: 'MOU_NonMOU', width: '7%', editable: false },
                          { text: 'Description', filtertype: 'input', dataField: 'MOU_NonMOU_Desc', width: '7%', editable: false },
                          { text: 'Driver', filtertype: 'input', dataField: 'Driver', width: '10%', editable: false },
                          { text: 'Css Driver', filtertype: 'input', dataField: 'CSSDriver', width: '7%', editable: false },

                          {
                              text: 'Start Date', datafield: 'StartDate', columntype: 'datetimeinput', width: '7%', align: 'right', cellsalign: 'right', cellsformat: 'd'
                              //,validation: function (cell, value) {
                              //    if (value == "")
                              //        return true;
                              //    var year = value.getFullYear();
                              //    if (year >= 2017) {
                              //        return { result: false, message: "Ship Date should be before 1/1/2017" };
                              //    }
                              //    return true;
                              //}
                          },
                          {
                              text: 'End Date', datafield: 'EndDate', columntype: 'datetimeinput', width: '7%', align: 'right', cellsalign: 'right', cellsformat: 'd'
                              //,validation: function (cell, value) {
                              //    if (value == "")
                              //        return true;
                              //    var year = value.getFullYear();
                              //    if (year >= 2017) {
                              //        return { result: false, message: "Ship Date should be before 1/1/2017" };
                              //    }
                              //    return true;
                              //}
                          },
                          { text: 'Is Direct Staff', filtertype: 'input', datafield: 'IsDirectStaf', threestatecheckbox: true, columntype: 'checkbox', width: 50 },
                          { text: 'SOEID', filtertype: 'input', dataField: 'SOEID', width: '7%', editable: false },
                          { text: 'FirstName', filtertype: 'input', dataField: 'FirstName', width: '7%', editable: false },
                          { text: 'LastName', filtertype: 'input', dataField: 'LastName', width: '7%', editable: false },
                          { text: 'GEID', filtertype: 'input', dataField: 'GEID', width: '7%', editable: false },
                          { text: 'Requisition', filtertype: 'input', dataField: 'Requisition', width: '7%', editable: false },
                          { text: 'Center', filtertype: 'input', dataField: 'Center', width: '7%', editable: false }
                        ]
                    });

        }

    });

}

function renderSOEIDRequisition(listOfParams)
{
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/MatchData/getEmployeeRequisition',
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var source =
                  {
                      unboundmode: true,
                      localdata: response,
                      datafields:
                              [
                                { name: 'Requisition', type: 'string' }
                              , { name: 'SOEID', type: 'string' }
                              , { name: 'GEID', type: 'string' }
                              , { name: 'Name', type: 'string' }
                              , { name: 'Goc', type: 'string' }
                              , { name: 'HiringDate', type: 'string' }
                              , { name: 'ClassificationLetter', type: 'string' }
                              , { name: 'Description', type: 'string' }

                              ],
                      datatype: "json"
                  };

            var dataAdapter = new $.jqx.dataAdapter(source);

            $("#gridRequisition").jqxGrid(
            {
                width: '100%',
                source: dataAdapter,
                pageable: true,
                autoheight: true,
                enabletooltips: true,
                sortable: true,
                filterable: true,
                theme: 'energyblue',
                rendered: function (type) {
                    // select all grid cells.
                    var gridCells = $('#gridRequisition').find('.jqx-grid-cell');
                    // initialize the jqxDragDrop plug-in. Set its drop target to the second Grid.
                    gridCells.jqxDragDrop({
                        appendTo: 'body', dragZIndex: 99999,
                        dropAction: 'none',
                        initFeedback: function (feedback) {
                            feedback.height(25);
                        },
                        dropTarget: $('#gridPrincipal'), revert: true
                    });
                    gridCells.off('dragStart');
                    gridCells.off('dragEnd');
                    gridCells.off('dropTargetEnter');
                    gridCells.off('dropTargetLeave');
                    // disable revert when the dragged cell is over the second Grid.
                    gridCells.on('dropTargetEnter', function () {
                        gridCells.jqxDragDrop({ revert: false });
                    });
                    // enable revert when the dragged cell is outside the second Grid.
                    gridCells.on('dropTargetLeave', function () {
                        gridCells.jqxDragDrop({ revert: true });
                    });
                    // initialize the dragged object.
                    gridCells.on('dragStart', function (event) {
                        var value = $(this).text();
                        var position = $.jqx.position(event.args);
                        var cell = $("#gridRequisition").jqxGrid('getcellatposition', position.left, position.top);
                        var rowData = $("#gridRequisition").jqxGrid('getrowdata', cell.row)
                        value = rowData.Requisition + ' | ' + rowData.SOEID + ' | ' + rowData.GEID

                        $(this).jqxDragDrop('data', {
                            value: rowData.Requisition + '|' + rowData.SOEID + '|' + rowData.GEID
                        });
                    });
                    // set the new cell value when the dragged cell is dropped over the second Grid.      
                    gridCells.on('dragEnd', function (event) {
                        var value = $(this).text();
                        var position = $.jqx.position(event.args);
                        var cell = $("#gridPrincipal").jqxGrid('getcellatposition', position.left, position.top);
                        if (cell != null) {
                            var fields = event.args.value.split('|');
                            var rows = $("#gridPrincipal").jqxGrid('getrows');


                            for (var i = 0; i < rows.length; i ++)
                            {
                                if (rows[i].SOEID == fields[1])
                                {
                                    $("#gridPrincipal").jqxGrid('setcellvalue', i, 'SOEID', '');
                                    $("#gridPrincipal").jqxGrid('setcellvalue', i, 'GEID', '');
                                    $("#gridPrincipal").jqxGrid('setcellvalue', i, 'Requisition','');
                                }
                                if (rows[i].GEID == fields[2]) {
                                    $("#gridPrincipal").jqxGrid('setcellvalue', i, 'SOEID', '');
                                    $("#gridPrincipal").jqxGrid('setcellvalue', i, 'GEID', '');
                                    $("#gridPrincipal").jqxGrid('setcellvalue', i, 'Requisition', '');
                                }
                            }
                            
                            $("#gridPrincipal").jqxGrid('setcellvalue', cell.row, 'SOEID', fields[1]);
                            $("#gridPrincipal").jqxGrid('setcellvalue', cell.row, 'GEID', fields[2]);
                            $("#gridPrincipal").jqxGrid('setcellvalue', cell.row, 'Requisition', fields[0]);
                        }
                    });
                },
                selectionmode: 'singlecell',
                columns: [

                  { text: 'Requisition', filtertype: 'input', dataField: 'Requisition', width: 100 }
                , { text: 'SOEID', filtertype: 'input', dataField: 'SOEID', width: 100 }
                , { text: 'GEID', filtertype: 'input', dataField: 'GEID', width: 100 }
                , { text: 'Name', filtertype: 'input', dataField: 'Name', width: 100 }
                , { text: 'Goc', filtertype: 'input', dataField: 'Goc', width: 100 }
                , { text: 'HiringDate', filtertype: 'input', dataField: 'HiringDate', width: 100 }
                , { text: 'ClassificationLetter', filtertype: 'input', dataField: 'ClassificationLetter', width: 30 }
                , { text: 'Description', filtertype: 'input', dataField: 'Description', width: 100 }

                ]
            });


        }
    });

}

function renderMOUList(listOfParams) {
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/MatchData/getMOUList',
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var source =
                  {
                      unboundmode: true,
                      localdata: response,
                      datafields:
                              [
                              { name: 'MOUAlias', type: 'string' },
                              { name: 'Name', type: 'string' },
                              { name: 'Description', type: 'string' },
                              { name: 'WorkTypeName', type: 'string' },
                              { name: 'ReceivingPhysicalLocationName', type: 'string' },
                              { name: 'ReceivingManagedSegmentID', type: 'string' },
                              { name: 'ReceivingManagedSegmentName', type: 'string' },
                              { name: 'Driver', type: 'string' },
                              { name: 'CssDriver', type: 'string' }

                              ],
                      datatype: "json"
                  };

            var dataAdapter = new $.jqx.dataAdapter(source);

            $("#gridMOU").jqxGrid(
            {
                width: '100%',
                source: dataAdapter,
                pageable: true,
                autoheight: true,
                sortable: true,
                filterable: true,
                enabletooltips: true,
                theme: 'energyblue',
                rendered: function (type) {
                    // select all grid cells.
                    var gridCells = $('#gridMOU').find('.jqx-grid-cell');
                    // initialize the jqxDragDrop plug-in. Set its drop target to the second Grid.
                    gridCells.jqxDragDrop({
                        appendTo: 'body', dragZIndex: 99999,
                        dropAction: 'none',
                        initFeedback: function (feedback) {
                            feedback.height(25);
                        },
                        dropTarget: $('#gridPrincipal'), revert: true
                    });
                    gridCells.off('dragStart');
                    gridCells.off('dragEnd');
                    gridCells.off('dropTargetEnter');
                    gridCells.off('dropTargetLeave');
                    // disable revert when the dragged cell is over the second Grid.
                    gridCells.on('dropTargetEnter', function () {
                        gridCells.jqxDragDrop({ revert: false });
                    });
                    // enable revert when the dragged cell is outside the second Grid.
                    gridCells.on('dropTargetLeave', function () {
                        gridCells.jqxDragDrop({ revert: true });
                    });
                    // initialize the dragged object.
                    gridCells.on('dragStart', function (event) {
                        var value = $(this).text();
                        var position = $.jqx.position(event.args);
                        var cell = $("#gridMOU").jqxGrid('getcellatposition', position.left, position.top);
                        var rowData = $("#gridMOU").jqxGrid('getrowdata', cell.row)
                        value = rowData.MOUAlias + '|' + rowData.Name + '|' + rowData.Driver + '|' + rowData.CssDriver

                        $(this).jqxDragDrop('data', {
                            value: rowData.MOUAlias + '|' + rowData.Name + '|' + rowData.Driver + '|' + rowData.CssDriver
                        });
                    });
                    // set the new cell value when the dragged cell is dropped over the second Grid.      
                    gridCells.on('dragEnd', function (event) {
                        var value = $(this).text();
                        var position = $.jqx.position(event.args);
                        var cell = $("#gridPrincipal").jqxGrid('getcellatposition', position.left, position.top);
                        if (cell != null) {
                            var fields = event.args.value.split('|');

                            $("#gridPrincipal").jqxGrid('setcellvalue', cell.row, 'MOU_NonMOU', fields[0]);
                            $("#gridPrincipal").jqxGrid('setcellvalue', cell.row, 'MOU_NonMOU_Desc', fields[1]);
                            $("#gridPrincipal").jqxGrid('setcellvalue', cell.row, 'Driver', fields[2]);
                            $("#gridPrincipal").jqxGrid('setcellvalue', cell.row, 'CSSDriver', fields[3]);
                        }
                    });
                },
                selectionmode: 'singlecell',
                columns: [

                  { text: 'MOU', filtertype: 'input', dataField: 'MOUAlias', width: 100 }
                , { text: 'Name', filtertype: 'input', dataField: 'Name', width: 100 }
                , { text: 'Description', filtertype: 'input', dataField: 'Description', width: 100 }
                , { text: 'Work Type', filtertype: 'input', dataField: 'WorkTypeName', width: 100 }
                , { text: 'Physical Location', filtertype: 'input', dataField: 'ReceivingPhysicalLocationName', width: 100 }
                , { text: 'Mgd Seg', filtertype: 'input', dataField: 'ReceivingManagedSegmentID', width: 100 }
                , { text: 'Mgd Seg Desc', filtertype: 'input', dataField: 'ReceivingManagedSegmentName', width: 100 }
                , { text: 'Driver', filtertype: 'input', dataField: 'Driver', width: 100 }
                , { text: 'CssDriver', filtertype: 'input', dataField: 'CssDriver', width: 100 }

                ]
            });


        }
    });

}

function renderNonMOUList(listOfParams) {
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/MatchData/getNonMOUList',
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var source =
                  {
                      unboundmode: true,
                      localdata: response,
                      datafields:
                              [
                              { name: 'NonMOUAlias', type: 'string' },
                              { name: 'Name', type: 'string' },
                              { name: 'Type', type: 'string' },
                              { name: 'ManagedSegmentName', type: 'string' },
                              { name: 'Driver', type: 'string' },
                              { name: 'CssDriver', type: 'string' }

                              ],
                      datatype: "json"
                  };

            var dataAdapter = new $.jqx.dataAdapter(source);

            $("#gridNonMOU").jqxGrid(
            {
                width: '100%',
                source: dataAdapter,
                pageable: true,
                autoheight: true,
                filterable: true,
                sortable: true,
                enabletooltips: true,
                theme: 'energyblue',
                rendered: function (type) {
                    // select all grid cells.
                    var gridCells = $('#gridNonMOU').find('.jqx-grid-cell');
                    // initialize the jqxDragDrop plug-in. Set its drop target to the second Grid.
                    gridCells.jqxDragDrop({
                        appendTo: 'body', dragZIndex: 99999,
                        dropAction: 'none',
                        initFeedback: function (feedback) {
                            feedback.height(25);
                        },
                        dropTarget: $('#gridPrincipal'), revert: true
                    });
                    gridCells.off('dragStart');
                    gridCells.off('dragEnd');
                    gridCells.off('dropTargetEnter');
                    gridCells.off('dropTargetLeave');
                    // disable revert when the dragged cell is over the second Grid.
                    gridCells.on('dropTargetEnter', function () {
                        gridCells.jqxDragDrop({ revert: false });
                    });
                    // enable revert when the dragged cell is outside the second Grid.
                    gridCells.on('dropTargetLeave', function () {
                        gridCells.jqxDragDrop({ revert: true });
                    });
                    // initialize the dragged object.
                    gridCells.on('dragStart', function (event) {
                        var value = $(this).text();
                        var position = $.jqx.position(event.args);
                        var cell = $("#gridNonMOU").jqxGrid('getcellatposition', position.left, position.top);
                        var rowData = $("#gridNonMOU").jqxGrid('getrowdata', cell.row)
                        value: rowData.NonMOUAlias + '|' + rowData.Name + '|' + rowData.Driver + '|' + rowData.CssDriver

                        $(this).jqxDragDrop('data', {
                            value: rowData.NonMOUAlias + '|' + rowData.Name + '|' + rowData.Driver + '|' + rowData.CssDriver
                        });
                    });
                    // set the new cell value when the dragged cell is dropped over the second Grid.      
                    gridCells.on('dragEnd', function (event) {
                        var value = $(this).text();
                        var position = $.jqx.position(event.args);
                        var cell = $("#gridPrincipal").jqxGrid('getcellatposition', position.left, position.top);
                        if (cell != null) {
                            var fields = event.args.value.split('|');

                            $("#gridPrincipal").jqxGrid('setcellvalue', cell.row, 'MOU_NonMOU', fields[0]);
                            $("#gridPrincipal").jqxGrid('setcellvalue', cell.row, 'MOU_NonMOU_Desc', fields[1]);
                            $("#gridPrincipal").jqxGrid('setcellvalue', cell.row, 'Driver', fields[2]);
                            $("#gridPrincipal").jqxGrid('setcellvalue', cell.row, 'CSSDriver', fields[3]);
                        }
                    });
                },
                selectionmode: 'singlecell',
                columns: [

                  { text: 'NON MOU', filtertype: 'input', dataField: 'NonMOUAlias', width: 100 }
                , { text: 'Name', filtertype: 'input', dataField: 'Name', width: 100 }
                , { text: 'Type', filtertype: 'input', dataField: 'Type', width: 100 }
                , { text: 'ManagedSegmentName Type', filtertype: 'input', dataField: 'ManagedSegmentName', width: 100 }
                , { text: 'Driver', filtertype: 'input', dataField: 'Driver', width: 100 }
                , { text: 'CssDriver', filtertype: 'input', dataField: 'CssDriver', width: 100 }
                ]
            });


        }
    });

}

function renderDriverList(listOfParams) {
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/MatchData/getDriverList',
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var source =
                  {
                      unboundmode: true,
                      localdata: response,
                      datafields:
                              [
                              { name: 'Driver', type: 'string' },
                              { name: 'CSSDriver', type: 'string' },
                              { name: 'Source', type: 'string' }

                              ],
                      datatype: "json"
                  };

            var dataAdapter = new $.jqx.dataAdapter(source);

            $("#gridDriver").jqxGrid(
            {
                width: '100%',
                source: dataAdapter,
                pageable: true,
                autoheight: true,
                enabletooltips: true,
                filterable: true,
                sortable: true,
                theme: 'energyblue',
                rendered: function (type) {
                    // select all grid cells.
                    var gridCells = $('#gridDriver').find('.jqx-grid-cell');
                    // initialize the jqxDragDrop plug-in. Set its drop target to the second Grid.
                    gridCells.jqxDragDrop({
                        appendTo: 'body', dragZIndex: 99999,
                        dropAction: 'none',
                        initFeedback: function (feedback) {
                            feedback.height(25);
                        },
                        dropTarget: $('#gridPrincipal'), revert: true
                    });
                    gridCells.off('dragStart');
                    gridCells.off('dragEnd');
                    gridCells.off('dropTargetEnter');
                    gridCells.off('dropTargetLeave');
                    // disable revert when the dragged cell is over the second Grid.
                    gridCells.on('dropTargetEnter', function () {
                        gridCells.jqxDragDrop({ revert: false });
                    });
                    // enable revert when the dragged cell is outside the second Grid.
                    gridCells.on('dropTargetLeave', function () {
                        gridCells.jqxDragDrop({ revert: true });
                    });
                    // initialize the dragged object.
                    gridCells.on('dragStart', function (event) {
                        var value = $(this).text();
                        var position = $.jqx.position(event.args);
                        var cell = $("#gridDriver").jqxGrid('getcellatposition', position.left, position.top);
                        var rowData = $("#gridDriver").jqxGrid('getrowdata', cell.row)
                        value = rowData.Driver + '|' + rowData.CSSDriver

                        $(this).jqxDragDrop('data', {
                            value: rowData.Driver + '|' + rowData.CSSDriver
                        });
                    });
                    // set the new cell value when the dragged cell is dropped over the second Grid.      
                    gridCells.on('dragEnd', function (event) {
                        var value = $(this).text();
                        var position = $.jqx.position(event.args);
                        var cell = $("#gridPrincipal").jqxGrid('getcellatposition', position.left, position.top);
                        if (cell != null) {
                            var fields = event.args.value.split('|');

                            $("#gridPrincipal").jqxGrid('setcellvalue', cell.row, 'Driver', fields[0]);
                            $("#gridPrincipal").jqxGrid('setcellvalue', cell.row, 'CSSDriver', fields[1]);
                        }
                    });
                },
                selectionmode: 'singlecell',
                columns: [

                  { text: 'Driver', filtertype: 'input', dataField: 'Driver', width: 100 }
                , { text: 'CSS Driver', filtertype: 'input', dataField: 'CSSDriver', width: 100 }
                , { text: 'Source', filtertype: 'input', dataField: 'Source', width: 100 }
                ]
            });


        }
    });

}

function renderCenterList(listOfParams) {
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/MatchData/getCenterList',
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var source =
                  {
                      unboundmode: true,
                      localdata: response,
                      datafields:
                              [
                              { name: 'ID', type: 'string' },
                              { name: 'Center', type: 'string' }

                              ],
                      datatype: "json"
                  };

            var dataAdapter = new $.jqx.dataAdapter(source);

            $("#gridCenter").jqxGrid(
            {
                width: '100%',
                source: dataAdapter,
                pageable: true,
                autoheight: true,
                filterable: true,
                enabletooltips: true,
                sortable: true,
                theme: 'energyblue',
                rendered: function (type) {
                    // select all grid cells.
                    var gridCells = $('#gridCenter').find('.jqx-grid-cell');
                    // initialize the jqxDragDrop plug-in. Set its drop target to the second Grid.
                    gridCells.jqxDragDrop({
                        appendTo: 'body', dragZIndex: 99999,
                        dropAction: 'none',
                        initFeedback: function (feedback) {
                            feedback.height(25);
                        },
                        dropTarget: $('#gridPrincipal'), revert: true
                    });
                    gridCells.off('dragStart');
                    gridCells.off('dragEnd');
                    gridCells.off('dropTargetEnter');
                    gridCells.off('dropTargetLeave');
                    // disable revert when the dragged cell is over the second Grid.
                    gridCells.on('dropTargetEnter', function () {
                        gridCells.jqxDragDrop({ revert: false });
                    });
                    // enable revert when the dragged cell is outside the second Grid.
                    gridCells.on('dropTargetLeave', function () {
                        gridCells.jqxDragDrop({ revert: true });
                    });
                    // initialize the dragged object.
                    gridCells.on('dragStart', function (event) {
                        var value = $(this).text();
                        var position = $.jqx.position(event.args);
                        var cell = $("#gridCenter").jqxGrid('getcellatposition', position.left, position.top);
                        var rowData = $("#gridCenter").jqxGrid('getrowdata', cell.row)
                        value = rowData.Center 

                        $(this).jqxDragDrop('data', {
                            value: rowData.Center
                        });
                    });
                    // set the new cell value when the dragged cell is dropped over the second Grid.      
                    gridCells.on('dragEnd', function (event) {
                        var value = $(this).text();
                        var position = $.jqx.position(event.args);
                        var cell = $("#gridPrincipal").jqxGrid('getcellatposition', position.left, position.top);
                        if (cell != null) {
                            var fields = event.args.value.split('|');

                            $("#gridPrincipal").jqxGrid('setcellvalue', cell.row, 'Center', fields[0]);
                        }
                    });
                },
                selectionmode: 'singlecell',
                columns: [

                  { text: 'Center', filtertype: 'input', dataField: 'Center', width: '90%' }
                ]
            });


        }
    });

}

function BTNaddNewRole()
{
    $("#popUpAddRole").jqxWindow('open');

    findGocMethod('#jqxGocAR', '100%');

    $('#jqxGocAR').on('select',
    function () {
        loadGocDataAddNewRole();
    });
    
    $('#jqxGocAR').val(SelectedGOC);

    loadGocDataAddNewRole();

    renderPhysicalLocationAddNewRole('');

    renderDirectStaff('Direct Staff', '#cbDirectStaftAR');

    renderIsFTeable('Is FTeble', "#cbFteableAR");

    $('#txtStatusAR').val('Vacant');

    $("#TotalRoles").jqxNumberInput({ width: '50px', height: '25px', spinButtons: true, decimalDigits: 0, max: 50, min: 1, digits: 2 });
    $('#TotalRoles').val(1);
}

function findGocMethod(nameTag, widthValue) {
    var timer;
    $(nameTag).jqxInput({
        placeHolder: "Enter a GOC", height: 25, width: widthValue,
        source: function (query, response) {
            var data2;

            var listOfParams = {
                data: query
            }

            _callServer({
                loadingMsg: "", async: false,
                url: '/NonMOU/getProbablyGOC',
                data: {
                    'data': JSON.stringify(listOfParams)
                },
                type: "post",
                success: function (json) {
                    var response = jQuery.parseJSON(json);
                    loadAdapter(response);

                }
            });

            function loadAdapter(data2) {


                var dataAdapter = new $.jqx.dataAdapter
                (
                    {
                        datatype: "jsonp",
                        localdata: data2,
                        datafields:
                        [
                            { name: 'GOCID' }, { name: 'GOCName' }
                        ],

                    },
                    {
                        autoBind: true,
                        formatData: function (data) {
                            data.name_startsWith = query;
                            return data;
                        },
                        loadComplete: function (data) {
                            if (data.length > 3) {
                                response($.map(data, function (item) {
                                    return {
                                        label: item.GOCName,
                                        value: item.GOCName
                                    }
                                }));
                            }
                        }
                    }
                );
            }
        }
    });


}

function loadGocDataAddNewRole() {
    var listOfParams = {
        pGOC: $('#jqxGocAR').val()
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/RoleReview/getGOCData',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {

            var response = jQuery.parseJSON(json);

            $.each((response), function (index, element) {

                $('#txtMSLVL09AR').val(element.ManagedSegmentID + ' ' + element.ManagedSegmentName);
                $('#txtGeographyAR').val(element.ManagedGeographyID + ' ' + element.ManagedGeographyName);
                $('#txtRegionAR').val(element.Region);

                $('#txtLVIDAR').val(element.LegalVehiculeID);
                $('#txtChargeOutAR').val(element.Market);
            });
        }
    });

}

function renderPhysicalLocationAddNewRole(value) {

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/NonMOU/getPhysicalLocation',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = (response);

            var source =
                {
                    localdata: data2,
                    datatype: "json",
                    datafields: [
                        { name: 'ID' },
                        { name: 'Name' }
                    ],
                    async: false
                };

            var dataAdapter = new $.jqx.dataAdapter(source);

            $("#cbPhysicalLocationAR").jqxComboBox({ selectedIndex: 0, source: dataAdapter, displayMember: "Name", valueMember: "Name", width: '99%', height: 25 });

            if (value != '') {
                $("#cbPhysicalLocationAR").jqxComboBox('selectItem', value);
            }

            $("#cbPhysicalLocationAR").on('select', function (event) {
                if (event.args) {
                    var item = event.args.item;
                    if (item) {

                    }
                }
            });

        }
    });

}

function renderDirectStaff(variable, container) {
    data2 = jQuery.parseJSON('[{"Description": "Direct Staff"},{"Description": "Temp"}]');

    var source =
        {
            localdata: data2,
            datatype: "json",
            datafields: [

                { name: 'Description' }
            ],
            async: false
        };

    var dataAdapter = new $.jqx.dataAdapter(source);

    $(container).jqxComboBox({ selectedIndex: 0, source: dataAdapter, displayMember: "Description", valueMember: "Description", width: '99%', height: 25 });

    if (variable != '') {
        $(container).jqxComboBox('selectItem', variable);
    }

    $(container).on('select', function (event) {
        if (event.args) {
            var item = event.args.item;
        }
    });


}

function renderIsFTeable(variable, container) {
    data2 = jQuery.parseJSON('[{"Description": "Is FTeble"},{"Description": "Not FTeable"}]');

    var source =
        {
            localdata: data2,
            datatype: "json",
            datafields: [

                { name: 'Description' }
            ],
            async: false
        };

    var dataAdapter = new $.jqx.dataAdapter(source);

    $(container).jqxComboBox({ selectedIndex: 0, source: dataAdapter, displayMember: "Description", valueMember: "Description", width: '99%', height: 25 });

    if (variable != '') {
        $(container).jqxComboBox('selectItem', variable);
    }

    $(container).on('select', function (event) {
        if (event.args) {
            var item = event.args.item;
        }
    });


}

function AddNewRoleInput() {

    var listOfParams = {
        pName: $('#txtNameAR').val(),
        pGOC: $('#jqxGocAR').val(),
        pPL: $("#cbPhysicalLocationAR").val(),
        pDS: $("#cbDirectStaftAR").val(),
        pFTE: $("#cbFteableAR").val(),
        pTotal: $('#TotalRoles').val()
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        url: '/RoleReview/saveNewRoles',
        type: "post",
        success: function (json) {
            renderGrids(SelectedGOC);
        }
    });


}

function MatchDataFunction()
{
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/MatchData/MatchAllData',
        data: {
            'data': JSON.stringify($('#gridPrincipal').jqxGrid('getdisplayrows'))
        },
        type: "post",
        success: function (json) {
            renderGrids(SelectedGOC);
        }
    });

}

function deleteRoleEmployee(pID)
{
    var listOfParams = {
        id: pID
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/MatchData/deleteRoleEmployee',
        type: "post",
        data: {
            'data': JSON.stringify(listOfParams)
        },
        success: function (json) {
            
            renderGrids(gocSel);
        }
    });


}