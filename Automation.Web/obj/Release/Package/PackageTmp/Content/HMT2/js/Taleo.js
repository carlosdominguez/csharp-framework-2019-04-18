﻿var rowid;

$(document).ready(function () {
    _hideMenu();

    XLSX.createJsXlsx({
        idElement: "js-xlsx-plugin",
        fileInput: {
            dropText: "Drop an Excel file here...",
            hideDrop: false,
            inputText: "... or click here to select a file"
        },
        onSuccess: function (jsonData) {

            var excelSource = jsonData;

            _callServer({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading data...",
                url: '/Taleo/uploadReenFile',
                data: {
                    'data': JSON.stringify(excelSource["Open"])
                },
                type: "post",
                success: function (json) {
                    renderGridDataSelectedFile(0);
                }
            });
        }
    });


    renderVersion();

    renderGridDataSelectedFile(0);


});

function renderVersion() {

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "",
        url: '/Taleo/getVersionList',
        type: "post",
        success: function (json) {

            result = jQuery.parseJSON(json);


            var source =
                {
                    localdata: result,
                    datafields:
                    [
                        { name: 'ID', type: 'string' },
                        { name: 'UploadBy', type: 'string' },
                        { name: 'UploadDate', type: 'string' },
                        { name: 'Enable', type: 'Boolean' },
                    ],
                    datatype: "array",
                    updaterow: function (rowid, rowdata) {
                    }
                };

            var dataAdapter = new $.jqx.dataAdapter(source);

            var imgEnable = function (row, datafield, value) {

                var rowData = $('#jqxgridVersion').jqxGrid('getrowdata', row);

                if (rowData.Enable == "true") {
                    return 'Selected File'
                }
                else {
                    return 'Unselected'
                }


            }

            $("#jqxgridVersion").jqxGrid(
             {
                 width: 600,
                 source: dataAdapter,
                 pageable: true,
                 autoheight: true,
                 columnsresize: true,
                 columns: [
                   { text: 'Upload ID', columntype: 'textbox', datafield: 'ID', width: '3%' },
                   { text: 'Upload By', columntype: 'textbox', datafield: 'UploadBy', width: '31%' },
                   { text: 'Upload Date', columntype: 'textbox', datafield: 'UploadDate', width: '31%' },
                   { text: 'Enable', columntype: 'textbox', cellsrenderer: imgEnable, width: '31%' }
                 ]
             });

            $("#jqxVersion").jqxDropDownButton({ width: 400, height: 25 });


            //Salvar nueva seleccion
            $("#jqxgridVersion").on('rowselect', function (event) {
                event.stopImmediatePropagation();
                var args = event.args;
                var row = $("#jqxgridVersion").jqxGrid('getrowdata', args.rowindex);
                var dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;">' + row['ID'] + ' | ' + row['UploadBy'] + ' | ' + row['UploadDate'] + '</div>';
                $("#jqxVersion").jqxDropDownButton('setContent', dropDownContent);

                rowid = row['ID'];
            });


            $.each((result), function (index, element) {
                if (element.Enable) {
                    var dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;">' + element.ID + ' | ' + element.UploadBy + ' | ' + element.UploadDate + '</div>';
                    $("#jqxVersion").jqxDropDownButton('setContent', dropDownContent);

                    rowid = element.ID;
                }
            });

        }



    });


}

function renderGridDataSelectedFile(ID) {
    var listOfParams = {
        pFile: ID
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/Taleo/getListReeng',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {

            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                         { name: 'ID', type: 'string' }
                        ,{ name: 'UploadID', type: 'string' },
                        ,{ name: 'BusinessUnit', type: 'string' }
	                    ,{ name: 'Business', type: 'string' }
	                    ,{ name: 'BusinessDetail', type: 'string' }
	                    ,{ name: 'ManagedGeography', type: 'string' }
	                    ,{ name: 'PhysicalRegion', type: 'string' }
	                    ,{ name: 'ManagedFunction', type: 'string' }
	                    ,{ name: 'OrgLevel1', type: 'string' }
	                    ,{ name: 'OrgLevel2', type: 'string' }
	                    ,{ name: 'OrgLevel3', type: 'string' }
	                    ,{ name: 'OrgLevel4', type: 'string' }
	                    ,{ name: 'OrgLevel5', type: 'string' }
	                    ,{ name: 'OrgLevel6', type: 'string' }
	                    ,{ name: 'OrgLevel7', type: 'string' }
	                    ,{ name: 'OrgLevel8', type: 'string' }
	                    ,{ name: 'OrgLevel9', type: 'string' }
	                    ,{ name: 'OrgLevel10', type: 'string' }
	                    ,{ name: 'OrgLevel11', type: 'string' }
	                    ,{ name: 'OrgLevel12', type: 'string' }
	                    ,{ name: 'MGLevel1', type: 'string' }
	                    ,{ name: 'MGLevel2', type: 'string' }
	                    ,{ name: 'MGLevel3', type: 'string' }
	                    ,{ name: 'MGLevel4', type: 'string' }
	                    ,{ name: 'MGLevel5', type: 'string' }
	                    ,{ name: 'MGLevel6', type: 'string' }
	                    ,{ name: 'MGLevel7', type: 'string' }
	                    ,{ name: 'CSWWorkflowName', type: 'string' }
	                    ,{ name: 'SourceSystem', type: 'string' }
	                    ,{ name: 'GOC', type: 'string' }
	                    ,{ name: 'GOCDescription', type: 'string' }
	                    ,{ name: 'Recruiter', type: 'string' }
	                    ,{ name: 'RecruiterGEID', type: 'string' }
	                    ,{ name: 'RelationshipIndicator', type: 'string' }
	                    ,{ name: 'Requisition_', type: 'string' }
	                    ,{ name: 'AlternateReq_', type: 'string' }
	                    ,{ name: 'JobCode', type: 'string' }
	                    ,{ name: 'JobFunction', type: 'string' }
	                    ,{ name: 'RequisitionTitle', type: 'string' }
	                    ,{ name: 'C_ofPositions', type: 'string' }
	                    ,{ name: 'LefttoHire', type: 'string' }
	                    ,{ name: 'CurrentReqStatus', type: 'string' }
	                    ,{ name: 'ReqStatusDate', type: 'string' }
	                    ,{ name: 'FirstApprovalDate', type: 'string' }
	                    ,{ name: 'ReqCreationDate', type: 'string' }
	                    ,{ name: 'InternalPostingStatus', type: 'string' }
	                    ,{ name: 'InternalPostingDate', type: 'string' }
	                    ,{ name: 'InternalUnpostingDate', type: 'string' }
	                    ,{ name: 'PersonReplacing', type: 'string' }
	                    ,{ name: 'PersonReplacingSOEID', type: 'string' }
	                    ,{ name: 'EmployeeReferalBonus', type: 'string' }
	                    ,{ name: 'EligibleforCampsHire', type: 'string' }
	                    ,{ name: 'HiringManager', type: 'string' }
	                    ,{ name: 'HiringManagerGEID', type: 'string' }
	                    ,{ name: 'HRRepresentative', type: 'string' }
	                    ,{ name: 'FinancialJustification', type: 'string' }
	                    ,{ name: 'RequisitionJustification', type: 'string' }
	                    ,{ name: 'IsitaProductionJob', type: 'string' }
	                    ,{ name: 'FullTimePartTime', type: 'string' }
	                    ,{ name: 'OvertimeStatus', type: 'string' }
	                    ,{ name: 'ReqSalaryGrade', type: 'string' }
	                    ,{ name: 'MappedOfficerTitle', type: 'string' }
	                    ,{ name: 'OfficerTitle', type: 'string' }
	                    ,{ name: 'OfficerTitleCode', type: 'string' }
	                    ,{ name: 'StandardGrade', type: 'string' }
	                    ,{ name: 'Grade', type: 'string' }
	                    ,{ name: 'WorldRegion', type: 'string' }
	                    ,{ name: 'Country', type: 'string' }
	                    ,{ name: 'StateProvince', type: 'string' }
	                    ,{ name: 'City', type: 'string' }
	                    ,{ name: 'CSSCenter', type: 'string' }
	                    ,{ name: 'LocationCode', type: 'string' }
	                    ,{ name: 'CSC', type: 'string' }
	                    ,{ name: 'OLM', type: 'string' }
	                    ,{ name: 'CompanyCode', type: 'string' }
	                    ,{ name: 'DaysOpen', type: 'string' }
	                    ,{ name: 'AgeGrouping', type: 'string' }
	                    ,{ name: 'AccountExpenseCode', type: 'string' }
	                    ,{ name: 'Applications', type: 'string' }
	                    ,{ name: 'Internal', type: 'string' }
	                    ,{ name: 'External', type: 'string' }
	                    ,{ name: 'New', type: 'string' }
	                    ,{ name: 'Testing1', type: 'string' }
	                    ,{ name: 'Reviewed', type: 'string' }
	                    ,{ name: 'PhoneScreen', type: 'string' }
	                    ,{ name: 'Testing2', type: 'string' }
	                    ,{ name: 'Interview', type: 'string' }
	                    ,{ name: 'OfferCreation', type: 'string' }
	                    ,{ name: 'OfferExtended', type: 'string' }
	                    ,{ name: 'OfferAccepted', type: 'string' }
	                    ,{ name: 'Hired', type: 'string' }
	                    ,{ name: 'OfferDeclined', type: 'string' }
	                    ,{ name: 'ReqEmployeeStatus', type: 'string' }
	                    ,{ name: 'ReqCreatorUser', type: 'string' }
	                    ,{ name: 'APACLegalVehicle', type: 'string' }
	                    , { name: 'SumofOpenDaysLefttoHire', type: 'string' }
                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            $('#jqxGridLastFile').jqxGrid(
           {
               source: dataAdapter,
               theme: 'blackberry',
               width: '100%',
               // autoheight: true,
               // autorowheight: true,
               pagesize: 10,
               pageable: true,
               filterable: true,
               showfilterrow: true,
               columnsresize: true,
               selectionmode: 'singlecell',
               enablebrowserselection: true,
               columns: [
                           { text: 'UploadID', dataField: 'UploadID', filtertype: 'input', editable: false, width: 60 }
                        , { text: 'BusinessUnit', dataField: 'BusinessUnit', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'Business', dataField: 'Business', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'BusinessDetail', dataField: 'BusinessDetail', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'ManagedGeography', dataField: 'ManagedGeography', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'PhysicalRegion', dataField: 'PhysicalRegion', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'ManagedFunction', dataField: 'ManagedFunction', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'OrgLevel1', dataField: 'OrgLevel1', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'OrgLevel2', dataField: 'OrgLevel2', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'OrgLevel3', dataField: 'OrgLevel3', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'OrgLevel4', dataField: 'OrgLevel4', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'OrgLevel5', dataField: 'OrgLevel5', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'OrgLevel6', dataField: 'OrgLevel6', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'OrgLevel7', dataField: 'OrgLevel7', filtertype: 'input', editable: false, width: 150 },
	                    ,  { text: 'OrgLevel8', dataField: 'OrgLevel8', filtertype: 'input', editable: false, width:150 },
	                    ,  { text: 'OrgLevel9', dataField: 'OrgLevel9', filtertype: 'input', editable: false, width:150 },
	                    , { text: 'OrgLevel10', dataField: 'OrgLevel10', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'OrgLevel11', dataField: 'OrgLevel11', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'OrgLevel12', dataField: 'OrgLevel12', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'MGLevel1', dataField: 'MGLevel1', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'MGLevel2', dataField: 'MGLevel2', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'MGLevel3', dataField: 'MGLevel3', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'MGLevel4', dataField: 'MGLevel4', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'MGLevel5', dataField: 'MGLevel5', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'MGLevel6', dataField: 'MGLevel6', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'MGLevel7', dataField: 'MGLevel7', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'CSWWorkflowName', dataField: 'CSWWorkflowName', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'SourceSystem', dataField: 'SourceSystem', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'GOC', dataField: 'GOC', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'GOCDescription', dataField: 'GOCDescription', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'Recruiter', dataField: 'Recruiter', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'RecruiterGEID', dataField: 'RecruiterGEID', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'RelationshipIndicator', dataField: 'RelationshipIndicator', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'Requisition_', dataField: 'Requisition_', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'AlternateReq_', dataField: 'AlternateReq_', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'JobCode', dataField: 'JobCode', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'JobFunction', dataField: 'JobFunction', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'RequisitionTitle', dataField: 'RequisitionTitle', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'C_ofPositions', dataField: 'C_ofPositions', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'LefttoHire', dataField: 'LefttoHire', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'CurrentReqStatus', dataField: 'CurrentReqStatus', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'ReqStatusDate', dataField: 'ReqStatusDate', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'FirstApprovalDate', dataField: 'FirstApprovalDate', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'ReqCreationDate', dataField: 'ReqCreationDate', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'InternalPostingStatus', dataField: 'InternalPostingStatus', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'InternalPostingDate', dataField: 'InternalPostingDate', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'InternalUnpostingDate', dataField: 'InternalUnpostingDate', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'PersonReplacing', dataField: 'PersonReplacing', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'PersonReplacingSOEID', dataField: 'PersonReplacingSOEID', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'EmployeeReferalBonus', dataField: 'EmployeeReferalBonus', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'EligibleforCampsHire', dataField: 'EligibleforCampsHire', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'HiringManager', dataField: 'HiringManager', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'HiringManagerGEID', dataField: 'HiringManagerGEID', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'HRRepresentative', dataField: 'HRRepresentative', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'FinancialJustification', dataField: 'FinancialJustification', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'RequisitionJustification', dataField: 'RequisitionJustification', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'IsitaProductionJob', dataField: 'IsitaProductionJob', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'FullTimePartTime', dataField: 'FullTimePartTime', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'OvertimeStatus', dataField: 'OvertimeStatus', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'ReqSalaryGrade', dataField: 'ReqSalaryGrade', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'MappedOfficerTitle', dataField: 'MappedOfficerTitle', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'OfficerTitle', dataField: 'OfficerTitle', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'OfficerTitleCode', dataField: 'OfficerTitleCode', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'StandardGrade', dataField: 'StandardGrade', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'Grade', dataField: 'Grade', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'WorldRegion', dataField: 'WorldRegion', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'Country', dataField: 'Country', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'StateProvince', dataField: 'StateProvince', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'City', dataField: 'City', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'CSSCenter', dataField: 'CSSCenter', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'LocationCode', dataField: 'LocationCode', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'CSC', dataField: 'CSC', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'OLM', dataField: 'OLM', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'CompanyCode', dataField: 'CompanyCode', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'DaysOpen', dataField: 'DaysOpen', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'AgeGrouping', dataField: 'AgeGrouping', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'AccountExpenseCode', dataField: 'AccountExpenseCode', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'Applications', dataField: 'Applications', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'Internal', dataField: 'Internal', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'External', dataField: 'External', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'New', dataField: 'New', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'Testing1', dataField: 'Testing1', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'Reviewed', dataField: 'Reviewed', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'PhoneScreen', dataField: 'PhoneScreen', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'Testing2', dataField: 'Testing2', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'Interview', dataField: 'Interview', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'OfferCreation', dataField: 'OfferCreation', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'OfferExtended', dataField: 'OfferExtended', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'OfferAccepted', dataField: 'OfferAccepted', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'Hired', dataField: 'Hired', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'OfferDeclined', dataField: 'OfferDeclined', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'ReqEmployeeStatus', dataField: 'ReqEmployeeStatus', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'ReqCreatorUser', dataField: 'ReqCreatorUser', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'APACLegalVehicle', dataField: 'APACLegalVehicle', filtertype: 'input', editable: false, width: 150 },
	                    , { text: 'SumofOpenDaysLefttoHire', dataField: 'SumofOpenDaysLefttoHire', filtertype: 'input', editable: false, width: 150 },

               ]

           });


        }
    });


}

function viewSelected() {
    renderGridDataSelectedFile(rowid);
}


function useForThisMonth() {
    var listOfParams = {
        pFile: rowid
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Set selection as default file...",
        url: '/Taleo/setAsDefault',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            renderGridDataSelectedFile(rowid);
            renderVersion();
        }
    });

}