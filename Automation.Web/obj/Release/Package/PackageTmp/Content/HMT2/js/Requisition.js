﻿$(document).ready(function () {
    _hideMenu();

    renderGridReq(0);
    renderPosition();
    renderReqStatus();

    $("#popUpRoles").jqxWindow({
        width: 1100,height:600, resizable: false, isModal: true, autoOpen: false, cancelButton: $("#Cancel"), modalOpacity: 0.01
    });

    $("#jqxExpanderListReq").jqxExpander({ width: '100%', height: 'auto', expanded: true, showArrow: true, theme: 'blackberry' });

    $('#reqAdmin').hide();
});

function renderPosition()
{
    $('#position').jqxNumberInput({ inputMode: 'simple', min: 0, width: '70px', height: '25px', inputMode: 'simple', spinButtons: true, spinButtonsStep: 1, decimalDigits: 0 });

    $('#position').on('valueChanged', function (event) {

        var name = '#' + event.currentTarget.id;
        var value = parseFloat(event.args.value);

        if (parseFloat(value) < parseFloat(0.00)) {
            $(name + " > :input").css("background-color", "#ff8080");
        }
        if (parseFloat(value) == parseFloat(0.00)) {
            $(name + " > :input").css("background-color", "#FFFFFF");
        }
        if (parseFloat(value) > parseFloat(0.00)) {
            $(name + " > :input").css("background-color", "#99CCFF");
        }

    });

    $("#dateReq").jqxDateTimeInput({ width: '300px', height: '25px' });
}

function renderGridReq(ID) {
    var listOfParams = {
        pFile: ID
    }

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/Requisition/getRequisition',
        type: "post",
        success: function (json) {

            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                             { name: 'ID', type: 'string' }
                           , { name: 'Number', type: 'string' }
                           , { name: 'Name', type: 'string' }
                           , { name: 'DateCreated', type: 'string' }
                           , { name: 'NumberPositions', type: 'string' }
                           , { name: 'Clevel', type: 'string' }
                           , { name: 'StatusID', type: 'string' }
                           , { name: 'DaysOpen', type: 'string' }
                           , { name: 'GOC', type: 'string' }
                           , { name: 'Expr1', type: 'string' }
                           , { name: 'BusinessUnit', type: 'string' }
                           , { name: 'Business', type: 'string' }
                           , { name: 'BusinessDetail', type: 'string' }
                           , { name: 'ManagedGeography', type: 'string' }
                           , { name: 'PhysicalRegion', type: 'string' }
                           , { name: 'ManagedFunction', type: 'string' }
                           , { name: 'OrgLevel1', type: 'string' }
                           , { name: 'OrgLevel2', type: 'string' }
                           , { name: 'OrgLevel3', type: 'string' }
                           , { name: 'OrgLevel4', type: 'string' }
                           , { name: 'OrgLevel5', type: 'string' }
                           , { name: 'OrgLevel6', type: 'string' }
                           , { name: 'OrgLevel7', type: 'string' }
                           , { name: 'OrgLevel8', type: 'string' }
                           , { name: 'OrgLevel9', type: 'string' }
                           , { name: 'OrgLevel10', type: 'string' }
                           , { name: 'OrgLevel11', type: 'string' }
                           , { name: 'OrgLevel12', type: 'string' }
                           , { name: 'MGLevel1', type: 'string' }
                           , { name: 'MGLevel2', type: 'string' }
                           , { name: 'MGLevel3', type: 'string' }
                           , { name: 'MGLevel4', type: 'string' }
                           , { name: 'MGLevel5', type: 'string' }
                           , { name: 'MGLevel6', type: 'string' }
                           , { name: 'MGLevel7', type: 'string' }
                           , { name: 'CSWWorkflowName', type: 'string' }
                           , { name: 'SourceSystem', type: 'string' }
                           , { name: 'GOCDescription', type: 'string' }
                           , { name: 'Recruiter', type: 'string' }
                           , { name: 'RecruiterGEID', type: 'string' }
                           , { name: 'RelationshipIndicator', type: 'string' }
                           , { name: 'Requisition_', type: 'string' }
                           , { name: 'AlternateReq_', type: 'string' }
                           , { name: 'JobCode', type: 'string' }
                           , { name: 'JobFunction', type: 'string' }
                           , { name: 'RequisitionTitle', type: 'string' }
                           , { name: 'C_ofPositions', type: 'string' }
                           , { name: 'LefttoHire', type: 'string' }
                           , { name: 'CurrentReqStatus', type: 'string' }
                           , { name: 'ReqStatusDate', type: 'string' }
                           , { name: 'FirstApprovalDate', type: 'string' }
                           , { name: 'ReqCreationDate', type: 'string' }
                           , { name: 'InternalPostingStatus', type: 'string' }
                           , { name: 'InternalPostingDate', type: 'string' }
                           , { name: 'InternalUnpostingDate', type: 'string' }
                           , { name: 'PersonReplacing', type: 'string' }
                           , { name: 'PersonReplacingSOEID', type: 'string' }
                           , { name: 'EmployeeReferalBonus', type: 'string' }
                           , { name: 'EligibleforCampsHire', type: 'string' }
                           , { name: 'HiringManager', type: 'string' }
                           , { name: 'HiringManagerGEID', type: 'string' }
                           , { name: 'HRRepresentative', type: 'string' }
                           , { name: 'FinancialJustification', type: 'string' }
                           , { name: 'RequisitionJustification', type: 'string' }
                           , { name: 'IsitaProductionJob', type: 'string' }
                           , { name: 'FullTimePartTime', type: 'string' }
                           , { name: 'OvertimeStatus', type: 'string' }
                           , { name: 'ReqSalaryGrade', type: 'string' }
                           , { name: 'MappedOfficerTitle', type: 'string' }
                           , { name: 'OfficerTitle', type: 'string' }
                           , { name: 'OfficerTitleCode', type: 'string' }
                           , { name: 'StandardGrade', type: 'string' }
                           , { name: 'Grade', type: 'string' }
                           , { name: 'WorldRegion', type: 'string' }
                           , { name: 'Country', type: 'string' }
                           , { name: 'StateProvince', type: 'string' }
                           , { name: 'City', type: 'string' }
                           , { name: 'CSSCenter', type: 'string' }
                           , { name: 'LocationCode', type: 'string' }
                           , { name: 'CSC', type: 'string' }
                           , { name: 'OLM', type: 'string' }
                           , { name: 'CompanyCode', type: 'string' }
                           , { name: 'AgeGrouping', type: 'string' }
                           , { name: 'AccountExpenseCode', type: 'string' }
                           , { name: 'Applications', type: 'string' }
                           , { name: 'Internal', type: 'string' }
                           , { name: 'External', type: 'string' }
                           , { name: 'New', type: 'string' }
                           , { name: 'Testing1', type: 'string' }
                           , { name: 'Reviewed', type: 'string' }
                           , { name: 'PhoneScreen', type: 'string' }
                           , { name: 'Testing2', type: 'string' }
                           , { name: 'Interview', type: 'string' }
                           , { name: 'OfferCreation', type: 'string' }
                           , { name: 'OfferExtended', type: 'string' }
                           , { name: 'OfferAccepted', type: 'string' }
                           , { name: 'Hired', type: 'string' }
                           , { name: 'OfferDeclined', type: 'string' }
                           , { name: 'ReqEmployeeStatus', type: 'string' }
                           , { name: 'ReqCreatorUser', type: 'string' }
                           , { name: 'APACLegalVehicle', type: 'string' }
                           , { name: 'SumofOpenDaysLefttoHire', type: 'string' }
                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            var imgSelectReq = function (row, datafield, value) {

                var rowData = $('#jqxGridReq').jqxGrid('getrowdata', row);

               rowData.ID;

               return '<i class="fa fa-search" style=" font-size: 23px; color: #4286f4; margin-left:4px; margin-top:4px;" onClick="reviewReq(' + rowData.ID + ',' + row + ')"></i>';
            }



            $('#jqxGridReq').jqxGrid(
           {
               source: dataAdapter,
               theme: 'blackberry',
               width: '100%',
               // autoheight: true,
               // autorowheight: true,
               pagesize: 20,
               pageable: true,
               filterable: true,
               showfilterrow: true,
               columnsresize: true,
               selectionmode: 'singlecell',
               enablebrowserselection: true,
               columns: [
                             { text: '', filtertype: 'none', width: '40', columntype: 'image', pinned: false, cellsrenderer: imgSelectReq, pinned: true },
                           , { text: 'Number', dataField: 'Number', filtertype: 'input', editable: false, width: 150, pinned: true }
                           , { text: 'Name', dataField: 'Name', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'DateCreated', dataField: 'DateCreated', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'NumberPositions', dataField: 'NumberPositions', filtertype: 'checkedlist', editable: false, width: 150 }
                           , { text: 'Clevel', dataField: 'Clevel', filtertype: 'checkedlist', editable: false, width: 150 }
                           , { text: 'DaysOpen', dataField: 'DaysOpen', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'GOC', dataField: 'GOC', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'Status', dataField: 'Expr1', filtertype: 'checkedlist', editable: false, width: 150 }
                           , { text: 'BusinessUnit', dataField: 'BusinessUnit', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'Business', dataField: 'Business', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'BusinessDetail', dataField: 'BusinessDetail', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'ManagedGeography', dataField: 'ManagedGeography', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'PhysicalRegion', dataField: 'PhysicalRegion', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'ManagedFunction', dataField: 'ManagedFunction', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OrgLevel1', dataField: 'OrgLevel1', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OrgLevel2', dataField: 'OrgLevel2', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OrgLevel3', dataField: 'OrgLevel3', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OrgLevel4', dataField: 'OrgLevel4', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OrgLevel5', dataField: 'OrgLevel5', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OrgLevel6', dataField: 'OrgLevel6', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OrgLevel7', dataField: 'OrgLevel7', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OrgLevel8', dataField: 'OrgLevel8', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OrgLevel9', dataField: 'OrgLevel9', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'MGLevel1', dataField: 'MGLevel1', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'MGLevel2', dataField: 'MGLevel2', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'MGLevel3', dataField: 'MGLevel3', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'MGLevel4', dataField: 'MGLevel4', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'MGLevel5', dataField: 'MGLevel5', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'SourceSystem', dataField: 'SourceSystem', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'GOCDescription', dataField: 'GOCDescription', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'Recruiter', dataField: 'Recruiter', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'RecruiterGEID', dataField: 'RecruiterGEID', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'JobCode', dataField: 'JobCode', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'JobFunction', dataField: 'JobFunction', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'RequisitionTitle', dataField: 'RequisitionTitle', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'C_ofPositions', dataField: 'C_ofPositions', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'LefttoHire', dataField: 'LefttoHire', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'CurrentReqStatus', dataField: 'CurrentReqStatus', filtertype: 'input', editable: false, width: 150 }
                           , { text: 'ReqStatusDate', dataField: 'ReqStatusDate' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'FirstApprovalDate', dataField: 'FirstApprovalDate' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'ReqCreationDate', dataField: 'ReqCreationDate' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'InternalPostingStatus', dataField: 'InternalPostingStatus' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'InternalPostingDate', dataField: 'InternalPostingDate' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'InternalUnpostingDate', dataField: 'InternalUnpostingDate' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'PersonReplacing', dataField: 'PersonReplacing' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'PersonReplacingSOEID', dataField: 'PersonReplacingSOEID' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'EmployeeReferalBonus', dataField: 'EmployeeReferalBonus' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'EligibleforCampsHire', dataField: 'EligibleforCampsHire' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'HiringManager', dataField: 'HiringManager' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'HiringManagerGEID', dataField: 'HiringManagerGEID' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'HRRepresentative', dataField: 'HRRepresentative' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'FinancialJustification', dataField: 'FinancialJustification' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'FullTimePartTime', dataField: 'FullTimePartTime' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OvertimeStatus', dataField: 'OvertimeStatus' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'ReqSalaryGrade', dataField: 'ReqSalaryGrade' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'MappedOfficerTitle', dataField: 'MappedOfficerTitle' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OfficerTitle', dataField: 'OfficerTitle' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OfficerTitleCode', dataField: 'OfficerTitleCode' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'StandardGrade', dataField: 'StandardGrade' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'Grade', dataField: 'Grade' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'WorldRegion', dataField: 'WorldRegion' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'Country', dataField: 'Country' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'StateProvince', dataField: 'StateProvince' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'City', dataField: 'City' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'CSSCenter', dataField: 'CSSCenter' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'LocationCode', dataField: 'LocationCode' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'CSC', dataField: 'CSC' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'OLM', dataField: 'OLM' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'CompanyCode', dataField: 'CompanyCode' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'ReqEmployeeStatus', dataField: 'ReqEmployeeStatus' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'ReqCreatorUser', dataField: 'ReqCreatorUser' , filtertype: 'input', editable: false, width: 150 }
                           , { text: 'SumofOpenDaysLefttoHire', dataField: 'SumofOpenDaysLefttoHire' , filtertype: 'input', editable: false, width: 150 }
	                    

               ]

           });


        }
    });
   
}

function reviewReq(ID, row)
{
    $('#reqAdmin').show();
    var dataChoose = $('#jqxGridReq').jqxGrid('getrowdata', row);

    $('#lblReq').text('Requisition: ' + dataChoose.Number);
    $('#txtTitle').val(dataChoose.Name);
    
    var i = dataChoose.NumberPositions
    $('#position').val(i);
    

    var dat = new Date(dataChoose.DateCreated.split("T")[0]);
  
    $('#dateReq ').jqxDateTimeInput('setDate', dat);

    renderTaleoData(dataChoose);

    $("#cbStatus").jqxComboBox('selectItem', dataChoose.Expr1);

    renderSelectRoles(dataChoose.Number);
    renderSelectedRoles(dataChoose.Number);

    $("#jqxExpanderListReq").jqxExpander({ expanded: false });

}

function openPopUpRole()
{
    $("#popUpRoles").jqxWindow('open');

}


function renderTaleoData(dataChoose) {

    var source =
          {
              unboundmode: true,
              localdata: dataChoose,
              datafields:
                      [
                            { name: 'ID', type: 'string' }
                          , { name: 'Number', type: 'string' }
                          , { name: 'Name', type: 'string' }
                          , { name: 'DateCreated', type: 'string' }
                          , { name: 'NumberPositions', type: 'string' }
                          , { name: 'Clevel', type: 'string' }
                          , { name: 'StatusID', type: 'string' }
                          , { name: 'DaysOpen', type: 'string' }
                          , { name: 'GOC', type: 'string' }
                          , { name: 'Expr1', type: 'string' }
                          , { name: 'BusinessUnit', type: 'string' }
                          , { name: 'Business', type: 'string' }
                          , { name: 'BusinessDetail', type: 'string' }
                          , { name: 'ManagedGeography', type: 'string' }
                          , { name: 'PhysicalRegion', type: 'string' }
                          , { name: 'ManagedFunction', type: 'string' }
                          , { name: 'OrgLevel1', type: 'string' }
                          , { name: 'OrgLevel2', type: 'string' }
                          , { name: 'OrgLevel3', type: 'string' }
                          , { name: 'OrgLevel4', type: 'string' }
                          , { name: 'OrgLevel5', type: 'string' }
                          , { name: 'OrgLevel6', type: 'string' }
                          , { name: 'OrgLevel7', type: 'string' }
                          , { name: 'OrgLevel8', type: 'string' }
                          , { name: 'OrgLevel9', type: 'string' }
                          , { name: 'OrgLevel10', type: 'string' }
                          , { name: 'OrgLevel11', type: 'string' }
                          , { name: 'OrgLevel12', type: 'string' }
                          , { name: 'MGLevel1', type: 'string' }
                          , { name: 'MGLevel2', type: 'string' }
                          , { name: 'MGLevel3', type: 'string' }
                          , { name: 'MGLevel4', type: 'string' }
                          , { name: 'MGLevel5', type: 'string' }
                          , { name: 'MGLevel6', type: 'string' }
                          , { name: 'MGLevel7', type: 'string' }
                          , { name: 'CSWWorkflowName', type: 'string' }
                          , { name: 'SourceSystem', type: 'string' }
                          , { name: 'GOCDescription', type: 'string' }
                          , { name: 'Recruiter', type: 'string' }
                          , { name: 'RecruiterGEID', type: 'string' }
                          , { name: 'RelationshipIndicator', type: 'string' }
                          , { name: 'Requisition_', type: 'string' }
                          , { name: 'AlternateReq_', type: 'string' }
                          , { name: 'JobCode', type: 'string' }
                          , { name: 'JobFunction', type: 'string' }
                          , { name: 'RequisitionTitle', type: 'string' }
                          , { name: 'C_ofPositions', type: 'string' }
                          , { name: 'LefttoHire', type: 'string' }
                          , { name: 'CurrentReqStatus', type: 'string' }
                          , { name: 'ReqStatusDate', type: 'string' }
                          , { name: 'FirstApprovalDate', type: 'string' }
                          , { name: 'ReqCreationDate', type: 'string' }
                          , { name: 'InternalPostingStatus', type: 'string' }
                          , { name: 'InternalPostingDate', type: 'string' }
                          , { name: 'InternalUnpostingDate', type: 'string' }
                          , { name: 'PersonReplacing', type: 'string' }
                          , { name: 'PersonReplacingSOEID', type: 'string' }
                          , { name: 'EmployeeReferalBonus', type: 'string' }
                          , { name: 'EligibleforCampsHire', type: 'string' }
                          , { name: 'HiringManager', type: 'string' }
                          , { name: 'HiringManagerGEID', type: 'string' }
                          , { name: 'HRRepresentative', type: 'string' }
                          , { name: 'FinancialJustification', type: 'string' }
                          , { name: 'RequisitionJustification', type: 'string' }
                          , { name: 'IsitaProductionJob', type: 'string' }
                          , { name: 'FullTimePartTime', type: 'string' }
                          , { name: 'OvertimeStatus', type: 'string' }
                          , { name: 'ReqSalaryGrade', type: 'string' }
                          , { name: 'MappedOfficerTitle', type: 'string' }
                          , { name: 'OfficerTitle', type: 'string' }
                          , { name: 'OfficerTitleCode', type: 'string' }
                          , { name: 'StandardGrade', type: 'string' }
                          , { name: 'Grade', type: 'string' }
                          , { name: 'WorldRegion', type: 'string' }
                          , { name: 'Country', type: 'string' }
                          , { name: 'StateProvince', type: 'string' }
                          , { name: 'City', type: 'string' }
                          , { name: 'CSSCenter', type: 'string' }
                          , { name: 'LocationCode', type: 'string' }
                          , { name: 'CSC', type: 'string' }
                          , { name: 'OLM', type: 'string' }
                          , { name: 'CompanyCode', type: 'string' }
                          , { name: 'AgeGrouping', type: 'string' }
                          , { name: 'AccountExpenseCode', type: 'string' }
                          , { name: 'Applications', type: 'string' }
                          , { name: 'Internal', type: 'string' }
                          , { name: 'External', type: 'string' }
                          , { name: 'New', type: 'string' }
                          , { name: 'Testing1', type: 'string' }
                          , { name: 'Reviewed', type: 'string' }
                          , { name: 'PhoneScreen', type: 'string' }
                          , { name: 'Testing2', type: 'string' }
                          , { name: 'Interview', type: 'string' }
                          , { name: 'OfferCreation', type: 'string' }
                          , { name: 'OfferExtended', type: 'string' }
                          , { name: 'OfferAccepted', type: 'string' }
                          , { name: 'Hired', type: 'string' }
                          , { name: 'OfferDeclined', type: 'string' }
                          , { name: 'ReqEmployeeStatus', type: 'string' }
                          , { name: 'ReqCreatorUser', type: 'string' }
                          , { name: 'APACLegalVehicle', type: 'string' }
                          , { name: 'SumofOpenDaysLefttoHire', type: 'string' }
                      ],
              datatype: "json"
          };

    var dataAdapter = new $.jqx.dataAdapter(source);

    $('#gridTaleo').jqxGrid(
   {
       source: dataAdapter,
       theme: 'blackberry',
       height: 90,
       width: '100%',
       pageable: false,
       columnsresize: true,
       selectionmode: 'singlecell',
       enablebrowserselection: true,
       columns: [
                     { text: 'Number', dataField: 'Number', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'Name', dataField: 'Name', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'DateCreated', dataField: 'DateCreated', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'NumberPositions', dataField: 'NumberPositions', filtertype: 'checkedlist', editable: false, width: 150 }
                   , { text: 'Clevel', dataField: 'Clevel', filtertype: 'checkedlist', editable: false, width: 150 }
                   , { text: 'DaysOpen', dataField: 'DaysOpen', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'GOC', dataField: 'GOC', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'Status', dataField: 'Expr1', filtertype: 'checkedlist', editable: false, width: 150 }
                   , { text: 'BusinessUnit', dataField: 'BusinessUnit', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'Business', dataField: 'Business', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'BusinessDetail', dataField: 'BusinessDetail', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'ManagedGeography', dataField: 'ManagedGeography', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'PhysicalRegion', dataField: 'PhysicalRegion', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'ManagedFunction', dataField: 'ManagedFunction', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'OrgLevel1', dataField: 'OrgLevel1', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'OrgLevel2', dataField: 'OrgLevel2', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'OrgLevel3', dataField: 'OrgLevel3', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'OrgLevel4', dataField: 'OrgLevel4', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'OrgLevel5', dataField: 'OrgLevel5', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'OrgLevel6', dataField: 'OrgLevel6', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'OrgLevel7', dataField: 'OrgLevel7', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'OrgLevel8', dataField: 'OrgLevel8', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'OrgLevel9', dataField: 'OrgLevel9', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'MGLevel1', dataField: 'MGLevel1', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'MGLevel2', dataField: 'MGLevel2', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'MGLevel3', dataField: 'MGLevel3', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'MGLevel4', dataField: 'MGLevel4', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'MGLevel5', dataField: 'MGLevel5', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'SourceSystem', dataField: 'SourceSystem', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'GOCDescription', dataField: 'GOCDescription', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'Recruiter', dataField: 'Recruiter', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'RecruiterGEID', dataField: 'RecruiterGEID', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'JobCode', dataField: 'JobCode', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'JobFunction', dataField: 'JobFunction', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'RequisitionTitle', dataField: 'RequisitionTitle', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'C_ofPositions', dataField: 'C_ofPositions', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'LefttoHire', dataField: 'LefttoHire', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'CurrentReqStatus', dataField: 'CurrentReqStatus', filtertype: 'input', editable: false, width: 150 }
                   , { text: 'ReqStatusDate', dataField: 'ReqStatusDate' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'FirstApprovalDate', dataField: 'FirstApprovalDate' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'ReqCreationDate', dataField: 'ReqCreationDate' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'InternalPostingStatus', dataField: 'InternalPostingStatus' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'InternalPostingDate', dataField: 'InternalPostingDate' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'InternalUnpostingDate', dataField: 'InternalUnpostingDate' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'PersonReplacing', dataField: 'PersonReplacing' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'PersonReplacingSOEID', dataField: 'PersonReplacingSOEID' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'EmployeeReferalBonus', dataField: 'EmployeeReferalBonus' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'EligibleforCampsHire', dataField: 'EligibleforCampsHire' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'HiringManager', dataField: 'HiringManager' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'HiringManagerGEID', dataField: 'HiringManagerGEID' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'HRRepresentative', dataField: 'HRRepresentative' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'FinancialJustification', dataField: 'FinancialJustification' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'FullTimePartTime', dataField: 'FullTimePartTime' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'OvertimeStatus', dataField: 'OvertimeStatus' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'ReqSalaryGrade', dataField: 'ReqSalaryGrade' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'MappedOfficerTitle', dataField: 'MappedOfficerTitle' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'OfficerTitle', dataField: 'OfficerTitle' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'OfficerTitleCode', dataField: 'OfficerTitleCode' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'StandardGrade', dataField: 'StandardGrade' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'Grade', dataField: 'Grade' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'WorldRegion', dataField: 'WorldRegion' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'Country', dataField: 'Country' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'StateProvince', dataField: 'StateProvince' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'City', dataField: 'City' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'CSSCenter', dataField: 'CSSCenter' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'LocationCode', dataField: 'LocationCode' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'CSC', dataField: 'CSC' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'OLM', dataField: 'OLM' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'CompanyCode', dataField: 'CompanyCode' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'ReqEmployeeStatus', dataField: 'ReqEmployeeStatus' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'ReqCreatorUser', dataField: 'ReqCreatorUser' , filtertype: 'input', editable: false, width: 150 }
                   , { text: 'SumofOpenDaysLefttoHire', dataField: 'SumofOpenDaysLefttoHire' , filtertype: 'input', editable: false, width: 150 }
       ]

   });


}

function renderSelectRoles(ReqNum)
{
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/Requisition/getTotalRolesList',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                       { name: 'ID', type: 'string' },
                      , { name: 'RoleID', type: 'string' },
                      , { name: 'RoleTypeID', type: 'string' },
                      , { name: 'FromType', type: 'string' },
                      , { name: 'Number', type: 'string' },
                      , { name: 'IDType', type: 'string' },
                      , { name: 'CreationDate', type: 'string' },
                      , { name: 'CreatedBy', type: 'string' },
                      , { name: 'IsActive', type: 'string' },
                      , { name: 'ModifyBy', type: 'string' },
                      , { name: 'ModifyDate', type: 'string' },
                      , { name: 'Name', type: 'string' },
                      , { name: 'Description', type: 'string' },
                      , { name: 'GOCID', type: 'string' },
                      , { name: 'MSL09', type: 'string' },
                      , { name: 'Process', type: 'string' },
                      , { name: 'SubProcess', type: 'string' },
                      , { name: 'MgdGeaography', type: 'string' },
                      , { name: 'MgdRegion', type: 'string' },
                      , { name: 'MgdCountry', type: 'string' },
                      , { name: 'LVID', type: 'string' },
                      , { name: 'PhysicalLocationID', type: 'string' },
                      , { name: 'ChargeOutProfileID', type: 'string' },
                      , { name: 'RoleIsActive', type: 'string' },
                      , { name: 'RoleCreaetedBy', type: 'string' },
                      , { name: 'CreatedDate', type: 'string' },
                      , { name: 'LastUpdateBy', type: 'string' },
                      , { name: 'LastUpdateDate', type: 'date' },
                      , { name: 'StatusID', type: 'string' },
                      , { name: 'IsFteable', type: 'string' },
                      , { name: 'IsDirectStaf', type: 'string' },
                      , { name: 'FTEPercentage', type: 'string' },
                      , { name: 'Status', type: 'string' },
                      , { name: 'ManagedSegment', type: 'string' },
                      , { name: 'ManagedGeography', type: 'string' },
                      , { name: 'PhysicalLocation', type: 'string' },
                      , { name: 'ChargeOutProfile', type: 'string' }
                      , { name: 'RoleAuto', type: 'string' }
                      , { name: 'GOCName', type: 'string' }
                      , { name: 'CssDriver', type: 'string' }
                     , { name: 'CountReq', type: 'string' }
                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            var imgViewSource = function (row, datafield, value) {

                var rowData = $('#jqxGridSelectRole').jqxGrid('getrowdata', row);

                var ID = rowData.IDType;
                var Number = rowData.Number;

                return '<center>' + Number + '   <i class="icon-search" style=" font-size: 23px; color: #4286f4;" onclick="openPopUpLiveStage(' + ID + ')" ></center>';
            }

            var imgDSTemp = function (row, datafield, value) {

                var rowData = $('#jqxGridSelectRole').jqxGrid('getrowdata', row);

                var directStaft = rowData.IsDirectStaf;

                if (directStaft) {
                    return '<div>Direct Staft</div>';
                } else {
                    return '<div>Temp</div>';
                }
            }

            var imgAddRole = function (row, datafield, value) {

                var rowData = $('#jqxGridSelectRole').jqxGrid('getrowdata', row);

                var ID = rowData.RoleID;
                var CountReq = rowData.CountReq;

                if (CountReq == 0) {
                    
                    return '<center><input type="button" value="Add" class="btn btn-success" onclick="addSelectedRow(' + ID + ',' + ReqNum + ')"/></center>';
                } else {
                    return '<center><input type="button" value="Add" class="btn btn-success" onclick="" disabled="disabled"/></center>';
                }
            }


            $('#jqxGridSelectRole').jqxGrid(
           {
               source: dataAdapter,
               theme: 'blackberry',
               width: '100%',
               selectionmode: 'multiplecellsadvanced',
               autoheight: true,
               pagesize: 10,
               pageable: true,
               filterable: true,
               showfilterrow: true,
               columnsresize: true,
               //         selectionmode: 'singlecell',
               enablebrowserselection: true,
               columns: [
               { text: '',width: '10%', editable: false, cellsrenderer: imgAddRole },
               { text: 'Open Req', dataField: 'CountReq', filtertype: 'checkedlist', width: '10%', editable: false },
               { text: 'Role ID', dataField: 'RoleAuto', columntype: 'textbox', filtercondition: 'starts_with', width: '15%', editable: false },
               { text: 'MOU/Non MOU ID#', dataField: 'Number', columntype: 'textbox', filtercondition: 'starts_with', width: '15%', editable: false, cellsrenderer: imgViewSource },
               { text: 'Center', dataField: 'PhysicalLocation', filtertype: 'checkedlist', width: '15%', editable: false },
               { text: 'Status', dataField: 'Status', filtertype: 'checkedlist', width: '15%', editable: false },
               { text: 'Source', dataField: 'FromType', filtertype: 'checkedlist', width: '15%', editable: false },
               { text: 'Driver', dataField: 'CssDriver', filtertype: 'checkedlist', width: '15%', editable: false },
               { text: 'FTE Type', columntype: 'textbox', filtercondition: 'starts_with', width: '15%', editable: false, cellsrenderer: imgDSTemp },
               { text: 'GOC', dataField: 'GOCName', filtertype: 'input', width: '550', editable: false },
               { text: 'Managed Segment L09', dataField: 'MSL09', filtertype: 'checkedlist', width: '15%', editable: false },
               { text: 'Managed Geography', dataField: 'MgdGeaography', filtertype: 'input', width: '15%', editable: false },
               { text: 'Last Update By', dataField: 'LastUpdateBy', filtertype: 'input', width: '15%', editable: false },
               { text: 'Last Update Date', dataField: 'LastUpdateDate', cellsformat: 'd', filtertype: 'imput', width: '15%', editable: false }

               ]

           });

        }
    });

}

function renderReqStatus() {

    _callServer({
        loadingMsg: "", async: false,
        url: '/Requisition/getReqStatus',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = (response);

            var source =
                {
                    localdata: data2,
                    datatype: "json",
                    datafields: [
                        { name: 'ID' },
                        { name: 'Name' }
                    ],
                    async: false
                };

            var dataAdapter = new $.jqx.dataAdapter(source);

            $("#cbStatus").jqxComboBox({ selectedIndex: 0, source: dataAdapter, displayMember: "Type", valueMember: "Name", width: '99%', height: 25 });

            $("#cbStatus").on('select', function (event) {
                if (event.args) {
                    var item = event.args.item;
                    if (item) {

                    }
                }
            });
        }
    });


}

function addSelectedRow(ID,ReqNum)
{
    var listOfParams = {

        pRoleID: ID,
        pReqID: ReqNum
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/Requisition/addRoleToReq',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            renderSelectedRoles(ReqNum);
        }
    });
}

function renderSelectedRoles(ReqNum) {
    var listOfParams = {
        pReqID: ReqNum
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/Requisition/renderRoleInReq',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            if (response) {
                response;
                //cargar grid

                //

                var data2 = response;

                var source =
               {
                   unboundmode: true,
                   localdata: data2,
                   datafields:
                           [
                           { name: 'ID', type: 'string' },
                          , { name: 'RoleID', type: 'string' },
                          , { name: 'RoleIDnumber', type: 'string' },
                          , { name: 'RoleTypeID', type: 'string' },
                          , { name: 'FromType', type: 'string' },
                          , { name: 'Number', type: 'string' },
                          , { name: 'IDType', type: 'string' },
                          , { name: 'CreationDate', type: 'string' },
                          , { name: 'CreatedBy', type: 'string' },
                          , { name: 'IsActive', type: 'string' },
                          , { name: 'ModifyBy', type: 'string' },
                          , { name: 'ModifyDate', type: 'string' },
                          , { name: 'Name', type: 'string' },
                          , { name: 'Description', type: 'string' },
                          , { name: 'GOCID', type: 'string' },
                          , { name: 'MSL09', type: 'string' },
                          , { name: 'Process', type: 'string' },
                          , { name: 'SubProcess', type: 'string' },
                          , { name: 'MgdGeaography', type: 'string' },
                          , { name: 'MgdRegion', type: 'string' },
                          , { name: 'MgdCountry', type: 'string' },
                          , { name: 'LVID', type: 'string' },
                          , { name: 'PhysicalLocationID', type: 'string' },
                          , { name: 'ChargeOutProfileID', type: 'string' },
                          , { name: 'RoleIsActive', type: 'string' },
                          , { name: 'RoleCreaetedBy', type: 'string' },
                          , { name: 'CreatedDate', type: 'string' },
                          , { name: 'LastUpdateBy', type: 'string' },
                          , { name: 'LastUpdateDate', type: 'date' },
                          , { name: 'StatusID', type: 'string' },
                          , { name: 'IsFteable', type: 'string' },
                          , { name: 'IsDirectStaf', type: 'string' },
                          , { name: 'FTEPercentage', type: 'string' },
                          , { name: 'Status', type: 'string' },
                          , { name: 'ManagedSegment', type: 'string' },
                          , { name: 'ManagedGeography', type: 'string' },
                          , { name: 'PhysicalLocation', type: 'string' },
                          , { name: 'ChargeOutProfile', type: 'string' }
                          , { name: 'RoleAuto', type: 'string' }
                          , { name: 'GOCName', type: 'string' }
                          , { name: 'CssDriver', type: 'string' }
                         , { name: 'CountReq', type: 'string' }
                           ],
                   datatype: "json"
               };
                var dataAdapter = new $.jqx.dataAdapter(source);

                var imgDelete = function (row, datafield, value) {

                    var rowData = $('#jqxRoleList').jqxGrid('getrowdata', row);

                    return '<i class="fa fa-trash" style=" font-size: 23px; color: red; margin-left:4px; margin-top:4px;" onClick="deleteSelectedRole(' + rowData.ID + ',' + ReqNum +')"></i>';
                }


                var imgViewSource = function (row, datafield, value) {

                    var rowData = $('#jqxRoleList').jqxGrid('getrowdata', row);

                    var ID = rowData.IDType;
                    var Number = rowData.Number;

                    return '<center>' + Number + '   <i class="icon-search" style=" font-size: 23px; color: #4286f4;" onclick="openPopUpLiveStage(' + ID + ')" ></center>';
                }

                var imgDSTemp = function (row, datafield, value) {

                    var rowData = $('#jqxRoleList').jqxGrid('getrowdata', row);

                    var directStaft = rowData.IsDirectStaf;

                    if (directStaft) {
                        return '<div>Direct Staft</div>';
                    } else {
                        return '<div>Temp</div>';
                    }
                }

                $('#jqxRoleList').jqxGrid(
               {
                   source: dataAdapter,

                   width: '100%',
                   selectionmode: 'none',
                   theme: 'blackberry',
                   autoheight: true,
                   autorowheight: true,
                   pageable: false,
                   filterable: false,
                   columnsresize: true,
                   altrows: true,
                   columns: [
                   { text: 'Delete', filtertype: 'none', width: '5%', columntype: 'image', pinned: false, cellsrenderer: imgDelete },
                   { text: 'Role ID', dataField: 'RoleAuto', columntype: 'textbox', filtercondition: 'starts_with', width: '15%', editable: false },
                   { text: 'Center', dataField: 'PhysicalLocation', filtertype: 'checkedlist', width: '15%', editable: false },
                   { text: 'Status', dataField: 'Status', filtertype: 'checkedlist', width: '15%', editable: false },
                   { text: 'Source', dataField: 'FromType', filtertype: 'checkedlist', width: '15%', editable: false },
                   { text: 'Driver', dataField: 'CssDriver', filtertype: 'checkedlist', width: '15%', editable: false },
                   { text: 'MOU/Non MOU ID#', dataField: 'Number', columntype: 'textbox', filtercondition: 'starts_with', width: '15%', editable: false},
                   { text: 'FTE Type', columntype: 'textbox', filtercondition: 'starts_with', width: '15%', editable: false, cellsrenderer: imgDSTemp },
                   { text: 'GOC', dataField: 'GOCName', filtertype: 'input', width: '550', editable: false },
                   { text: 'Managed Segment L09', dataField: 'MSL09', filtertype: 'checkedlist', width: '15%', editable: false },
                   { text: 'Managed Geography', dataField: 'MgdGeaography', filtertype: 'input', width: '15%', editable: false },
                   { text: 'Last Update By', dataField: 'LastUpdateBy', filtertype: 'input', width: '15%', editable: false },
                   { text: 'Last Update Date', dataField: 'LastUpdateDate', cellsformat: 'd', filtertype: 'imput', width: '15%', editable: false }
                   ]

               });

            }

        }
    });
}

function deleteSelectedRole(ID, ReqNum) {
    var listOfParams = {

        pID: ID
    }

    _callServer({
        loadingMsg: "", async: false,
        url: '/Requisition/deleteSelectedRole',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            renderSelectedRoles(ReqNum);
        }
    });
}