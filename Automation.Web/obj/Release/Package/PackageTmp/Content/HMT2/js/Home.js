var DS = '0';
var Center = '0';


$(document).ready(function () {
    _hideMenu();

    getReport();

  
    //getTotalRole();
    //getTotalEmployee();
    //getTotalNonMOU();

    renderComboBoxTemp('#cbTypeEmployee', 'Select type', 'getGocList', true);
    renderComboBoxCenter('#cbCenter', 'Select center', 'getGocList', true);

    getTotalMOU();
});

function getTotalMOU() {
    DS = '0';
    Center = '0';

    var dsselected = $('#cbTypeEmployee').multiselect("getChecked");
    var centerselected = $('#cbCenter').multiselect("getChecked");

    $.each(dsselected, function (index, value) {
        if (index == 0) {
            DS = value.defaultValue;
        }
        else {
            DS = DS + '~' + value.defaultValue;
        }
    });

    $.each(centerselected, function (index, value) {
        if (index == 0) {
            Center = value.defaultValue;
        }
        else {
            Center = Center + '~' + value.defaultValue;
        }
    });

    var listOfParams = {
        DS: DS,
        CENTER: Center
    }
    //Save app info data
    _callServer({
        loadingMsg: "Get total MOU...",
        url: '/HMT2/getReportDasthboard',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (total) {
            var response = jQuery.parseJSON(total);

            $('#planJan').text(response[0].JAN);
            $('#planFeb').text(response[0].FEB);
            $('#planMar').text(response[0].MAR);
            $('#planApr').text(response[0].APR);
            $('#planMay').text(response[0].MAY);
            $('#planJun').text(response[0].JUN);
            $('#planJul').text(response[0].JUL);
            $('#planAug').text(response[0].AUG);
            $('#planSep').text(response[0].SEP);
            $('#planOct').text(response[0].OCT);
            $('#planNov').text(response[0].NOV);
            $('#planDec').text(response[0].DEC);

            $('#mouJan').text(response[1].JAN);
            $('#mouFeb').text(response[1].FEB);
            $('#mouMar').text(response[1].MAR);
            $('#mouApr').text(response[1].APR);
            $('#mouMay').text(response[1].MAY);
            $('#mouJun').text(response[1].JUN);
            $('#mouJul').text(response[1].JUL);
            $('#mouAug').text(response[1].AUG);
            $('#mouSep').text(response[1].SEP);
            $('#mouOct').text(response[1].OCT);
            $('#mouNov').text(response[1].NOV);
            $('#mouDec').text(response[1].DEC);

            $('#NONmouJan').text(response[2].JAN);
            $('#NONmouFeb').text(response[2].FEB);
            $('#NONmouMar').text(response[2].MAR);
            $('#NONmouApr').text(response[2].APR);
            $('#NONmouMay').text(response[2].MAY);
            $('#NONmouJun').text(response[2].JUN);
            $('#NONmouJul').text(response[2].JUL);
            $('#NONmouAug').text(response[2].AUG);
            $('#NONmouSep').text(response[2].SEP);
            $('#NONmouOct').text(response[2].OCT);
            $('#NONmouNov').text(response[2].NOV);
            $('#NONmouDec').text(response[2].DEC);

            $('#demandJan').text(response[3].JAN);
            $('#demandFeb').text(response[3].FEB);
            $('#demandMar').text(response[3].MAR);
            $('#demandApr').text(response[3].APR);
            $('#demandMay').text(response[3].MAY);
            $('#demandJun').text(response[3].JUN);
            $('#demandJul').text(response[3].JUL);
            $('#demandAug').text(response[3].AUG);
            $('#demandSep').text(response[3].SEP);
            $('#demandOct').text(response[3].OCT);
            $('#demandNov').text(response[3].NOV);
            $('#demandDec').text(response[3].DEC);

            $('#ActualJan').text(response[4].JAN);
            $('#ActualFeb').text(response[4].FEB);
            $('#ActualMar').text(response[4].MAR);
            $('#ActualApr').text(response[4].APR);
            $('#ActualMay').text(response[4].MAY);
            $('#ActualJun').text(response[4].JUN);
            $('#ActualJul').text(response[4].JUL);
            $('#ActualAug').text(response[4].AUG);
            $('#ActualSep').text(response[4].SEP);
            $('#ActualOct').text(response[4].OCT);
            $('#ActualNov').text(response[4].NOV);
            $('#ActualDec').text(response[4].DEC);
        }
    });



}

function getTotalNonMOU() {
    var listOfParams = {
        country: 'cr'
    }

    //Save app info data
    _callServer({
        loadingMsg: "Get total MOU...",
        url: '/HMT2/getTotalNonMOU',
        data: {
            'listOfParams': JSON.stringify({
                listOfParams: listOfParams
            })
        },
        type: "post",
        success: function (total) {

            $('#lblNonMOU').text(total);

        }
    });



}

function getReport()
{

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "",
        url: '/HMT2/getBarReport',
        type: "post",
        success: function (json) {

            result = jQuery.parseJSON(json);


            var settings = {
                title: "Total filled roles per center",
                description: "",
                showLegend: true,
                enableAnimations: true,
                padding: { left: 20, top: 2, right: 20, bottom: 2 },
                titlePadding: { left: 90, top: 0, right: 0, bottom: 4 },
                source: result,
                xAxis:
                {
                    dataField: 'CENTER',
                    gridLines: { visible: true },
                    flip: false
                },
               
                colorScheme: 'scheme01',
                seriesGroups:
                    [
                        {
                            type: 'column',
                            orientation: 'horizontal',
                            columnsGapPercent: 50,
                            toolTipFormatSettings: { thousandsSeparator: ',' },
                            series: [
                                    { dataField: 'DS', displayText: 'Total Filled Roles DS' },
                                    { dataField: 'TEMP', displayText: 'Total Filled Roles Temp' }
                            ]
                        }
                    ]
            };
            // setup the chart
            $('#chartContainer5').jqxChart(settings);

        }



    });


}


function getTotalRole() {
    var listOfParams = {
        country: 'cr'
    }


    _callServer({
        loadingMsg: "Get total role...",
        url: '/HMT2/getTotalRole',
        data: {
            'listOfParams': JSON.stringify({
                listOfParams: listOfParams
            })
        },
        type: "post",
        success: function (total) {

            $('#lblTotalRole').text(total);

        }
    });



}

function getTotalEmployee() {
    var listOfParams = {
        country: 'cr'
    }

    _callServer({
        loadingMsg: "Get total employee...",
        url: '/HMT2/getTotalEmployee',
        data: {
            'listOfParams': JSON.stringify({
                listOfParams: listOfParams
            })
        },
        type: "post",
        success: function (total) {

            $('#lblTotalEmployee').text(total);

        }
    });



}

function renderComboBoxTemp(div, textDefault, procedure, pMultiple) {

    $(div).find('option').remove().end();

                $(div).append($('<option>', {
                    value: 'DS',
                    text: 'Direct Staff'
                }));

                $(div).append($('<option>', {
                    value: 'FTE',
                    text: 'FTE'
                }));

            $(div).multiselect({
                click: function (event, ui) {

                    getTotalMOU();
                }
            });


            $(div).multiselect({
                multiple: pMultiple,
                header: "Select an option",
                noneSelectedText: textDefault,
                selectedList: 1

            }).multiselectfilter();

            $(div).css('width', '100px');
}


function renderComboBoxCenter(div, textDefault, procedure, pMultiple) {

    $(div).find('option').remove().end();

    $(div).append($('<option>', {
        value: 'COSTA RICA',
        text: 'COSTA RICA'
    }));

    $(div).append($('<option>', {
        value: 'DELAWARE',
        text: 'DELAWARE'
    }));

    $(div).append($('<option>', {
        value: 'EMEA',
        text: 'EMEA'
    }));
    $(div).append($('<option>', {
        value: 'MANILA',
        text: 'MANILA'
    }));
    $(div).append($('<option>', {
        value: 'MUMBAI',
        text: 'MUMBAI'
    }));
    $(div).append($('<option>', {
        value: 'NYC',
        text: 'NYC'
    }));
    $(div).append($('<option>', {
        value: 'TAMPA',
        text: 'TAMPA'
    }));

    $(div).multiselect({
        click: function (event, ui) {

            getTotalMOU();

        }
    });


    $(div).multiselect({
        multiple: pMultiple,
        header: "Select an option",
        noneSelectedText: textDefault,
        selectedList: 1

    }).multiselectfilter();

    $(div).css('width', '100px');
}