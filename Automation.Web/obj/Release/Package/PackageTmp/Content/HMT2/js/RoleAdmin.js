﻿var MyGrids = "";
var GoContinue = 0;

var ListGoc;
var ListManGeo;
var ListManSegment;
var ListRegion;
var ListLocation;
var ListChageOut;

$(document).ready(function () {
    _hideMenu();

    $("#popUpRoles").jqxWindow({
        width: 1100, resizable: false, isModal: true, autoOpen: false, cancelButton: $("#Cancel"), modalOpacity: 0.01
    });

    
});



function viewSelectedGrid(DIV) {
    $('#viewRoles').hide();
    $('#viewPenRev').hide();
    $('#viewNonMOU').hide();
    $('#viewMOU').hide();

    $(DIV).show();
}

function openSearchWindow()
{
    GetListTotalRole();
    $("#popUpRoles").jqxWindow('open');
    
}

function openSearchWindowNonMOU() {
    GetListTotalRoleNonMOU();
    $("#popUpRoles").jqxWindow('open');

}

function GetListTotalRole() {
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/RoleReview/getTotalRolesList',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                       { name: 'ID', type: 'string' },
                      , { name: 'RoleID', type: 'string' },
                      , { name: 'RoleTypeID', type: 'string' },
                      , { name: 'FromType', type: 'string' },
                      , { name: 'Number', type: 'string' },
                      , { name: 'IDType', type: 'string' },
                      , { name: 'CreationDate', type: 'string' },
                      , { name: 'CreatedBy', type: 'string' },
                      , { name: 'IsActive', type: 'string' },
                      , { name: 'ModifyBy', type: 'string' },
                      , { name: 'ModifyDate', type: 'string' },
                      , { name: 'Name', type: 'string' },
                      , { name: 'Description', type: 'string' },
                      , { name: 'GOCID', type: 'string' },
                      , { name: 'MSL09', type: 'string' },
                      , { name: 'Process', type: 'string' },
                      , { name: 'SubProcess', type: 'string' },
                      , { name: 'MgdGeaography', type: 'string' },
                      , { name: 'MgdRegion', type: 'string' },
                      , { name: 'MgdCountry', type: 'string' },
                      , { name: 'LVID', type: 'string' },
                      , { name: 'PhysicalLocationID', type: 'string' },
                      , { name: 'ChargeOutProfileID', type: 'string' },
                      , { name: 'RoleIsActive', type: 'string' },
                      , { name: 'RoleCreaetedBy', type: 'string' },
                      , { name: 'CreatedDate', type: 'string' },
                      , { name: 'LastUpdateBy', type: 'string' },
                      , { name: 'LastUpdateDate', type: 'date' },
                      , { name: 'StatusID', type: 'string' },
                      , { name: 'IsFteable', type: 'string' },
                      , { name: 'IsDirectStaf', type: 'string' },
                      , { name: 'FTEPercentage', type: 'string' },
                      , { name: 'Status', type: 'string' },
                      , { name: 'ManagedSegment', type: 'string' },
                      , { name: 'ManagedGeography', type: 'string' },
                      , { name: 'PhysicalLocation', type: 'string' },
                      , { name: 'ChargeOutProfile', type: 'string' }
                      , { name: 'RoleAuto', type: 'string' }
                      , { name: 'GOCName', type: 'string' }
                      , { name: 'CssDriver', type: 'string' }
                       ], width: '120',
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            var imgViewSource = function (row, datafield, value) {

                var rowData = $('#jqxGridRoles').jqxGrid('getrowdata', row);

                var ID = rowData.IDType;
                var Number = rowData.Number;

                return '<center>' + Number + '   <i class="icon-search" style=" font-size: 23px; color: #4286f4;" onclick="openPopUpLiveStage(' + ID + ')" ></center>';
            }

            var imgDSTemp = function (row, datafield, value) {

                var rowData = $('#jqxGridRoles').jqxGrid('getrowdata', row);

                var directStaft = rowData.IsDirectStaf;

                if (directStaft) {
                    return '<div>Direct Staft</div>';
                } else {
                    return '<div>Temp</div>';
                }
            }


            $('#jqxGridRoles').jqxGrid(
           {
               source: dataAdapter,
               theme: 'blackberry',
               width: '100%',
               selectionmode: 'multiplecellsadvanced',
               pagesize: 10,
               pageable: true,
               filterable: true,
               showfilterrow: true,
               columnsresize: true,
               selectionmode: 'checkbox',
               //         selectionmode: 'singlecell',
               enablebrowserselection: true,
               columns: [
               { text: 'Center', dataField: 'PhysicalLocation', filtertype: 'checkedlist', width: '120', editable: false },
               { text: 'Status', dataField: 'Status', filtertype: 'checkedlist', width: '120', editable: false },
               { text: 'Source', dataField: 'FromType', filtertype: 'checkedlist', width: '120', editable: false },
               { text: 'Driver', dataField: 'CssDriver', filtertype: 'checkedlist', width: '120', editable: false },
               { text: 'MOU/Non MOU ID#', dataField: 'Number', columntype: 'textbox', filtercondition: 'starts_with', width: '120', editable: false, cellsrenderer: imgViewSource },
               { text: 'Role ID', dataField: 'RoleAuto', columntype: 'textbox', filtercondition: 'starts_with', width: '120', editable: false },
               { text: 'FTE Type', columntype: 'textbox', filtercondition: 'starts_with', width: '120', editable: false, cellsrenderer: imgDSTemp },
               { text: 'GOC', dataField: 'GOCName', filtertype: 'input', width: '120', editable: false },
               { text: 'Managed Segment L09', dataField: 'MSL09', filtertype: 'checkedlist', width: '120', editable: false },
               { text: 'Managed Geography', dataField: 'MgdGeaography', filtertype: 'input', width: '120', editable: false },
               { text: 'Last Update By', dataField: 'LastUpdateBy', filtertype: 'input', width: '120', editable: false },
               { text: 'Last Update Date', dataField: 'LastUpdateDate', cellsformat: 'd', filtertype: 'imput', width: '120', editable: false }

               ]

           });

        }
    });
}

function GetListTotalRoleNonMOU() {
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/RoleReview/getTotalRolesListNM',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                       { name: 'ID', type: 'string' },
                      , { name: 'RoleID', type: 'string' },
                      , { name: 'RoleTypeID', type: 'string' },
                      , { name: 'FromType', type: 'string' },
                      , { name: 'Number', type: 'string' },
                      , { name: 'IDType', type: 'string' },
                      , { name: 'CreationDate', type: 'string' },
                      , { name: 'CreatedBy', type: 'string' },
                      , { name: 'IsActive', type: 'string' },
                      , { name: 'ModifyBy', type: 'string' },
                      , { name: 'ModifyDate', type: 'string' },
                      , { name: 'Name', type: 'string' },
                      , { name: 'Description', type: 'string' },
                      , { name: 'GOCID', type: 'string' },
                      , { name: 'MSL09', type: 'string' },
                      , { name: 'Process', type: 'string' },
                      , { name: 'SubProcess', type: 'string' },
                      , { name: 'MgdGeaography', type: 'string' },
                      , { name: 'MgdRegion', type: 'string' },
                      , { name: 'MgdCountry', type: 'string' },
                      , { name: 'LVID', type: 'string' },
                      , { name: 'PhysicalLocationID', type: 'string' },
                      , { name: 'ChargeOutProfileID', type: 'string' },
                      , { name: 'RoleIsActive', type: 'string' },
                      , { name: 'RoleCreaetedBy', type: 'string' },
                      , { name: 'CreatedDate', type: 'string' },
                      , { name: 'LastUpdateBy', type: 'string' },
                      , { name: 'LastUpdateDate', type: 'date' },
                      , { name: 'StatusID', type: 'string' },
                      , { name: 'IsFteable', type: 'string' },
                      , { name: 'IsDirectStaf', type: 'string' },
                      , { name: 'FTEPercentage', type: 'string' },
                      , { name: 'Status', type: 'string' },
                      , { name: 'ManagedSegment', type: 'string' },
                      , { name: 'ManagedGeography', type: 'string' },
                      , { name: 'PhysicalLocation', type: 'string' },
                      , { name: 'ChargeOutProfile', type: 'string' }
                      , { name: 'RoleAuto', type: 'string' }
                      , { name: 'GOCName', type: 'string' }
                      , { name: 'CssDriver', type: 'string' }
                       ], width: '120',
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            var imgViewSource = function (row, datafield, value) {

                var rowData = $('#jqxGridRoles').jqxGrid('getrowdata', row);

                var ID = rowData.IDType;
                var Number = rowData.Number;

                return '<center>' + Number + '   <i class="icon-search" style=" font-size: 23px; color: #4286f4;" onclick="openPopUpLiveStage(' + ID + ')" ></center>';
            }

            var imgDSTemp = function (row, datafield, value) {

                var rowData = $('#jqxGridRoles').jqxGrid('getrowdata', row);

                var directStaft = rowData.IsDirectStaf;

                if (directStaft) {
                    return '<div>Direct Staft</div>';
                } else {
                    return '<div>Temp</div>';
                }
            }


            $('#jqxGridRoles').jqxGrid(
           {
               source: dataAdapter,
               theme: 'blackberry',
               width: '100%',
               selectionmode: 'multiplecellsadvanced',
               pagesize: 10,
               pageable: true,
               filterable: true,
               showfilterrow: true,
               columnsresize: true,
               selectionmode: 'checkbox',
               //         selectionmode: 'singlecell',
               enablebrowserselection: true,
               columns: [
               { text: 'Center', dataField: 'PhysicalLocation', filtertype: 'checkedlist', width: '120', editable: false },
               { text: 'Status', dataField: 'Status', filtertype: 'checkedlist', width: '120', editable: false },
               { text: 'Source', dataField: 'FromType', filtertype: 'checkedlist', width: '120', editable: false },
               { text: 'Driver', dataField: 'CssDriver', filtertype: 'checkedlist', width: '120', editable: false },
               { text: 'MOU/Non MOU ID#', dataField: 'Number', columntype: 'textbox', filtercondition: 'starts_with', width: '120', editable: false, cellsrenderer: imgViewSource },
               { text: 'Role ID', dataField: 'RoleAuto', columntype: 'textbox', filtercondition: 'starts_with', width: '120', editable: false },
               { text: 'FTE Type', columntype: 'textbox', filtercondition: 'starts_with', width: '120', editable: false, cellsrenderer: imgDSTemp },
               { text: 'GOC', dataField: 'GOCName', filtertype: 'input', width: '120', editable: false },
               { text: 'Managed Segment L09', dataField: 'MSL09', filtertype: 'checkedlist', width: '120', editable: false },
               { text: 'Managed Geography', dataField: 'MgdGeaography', filtertype: 'input', width: '120', editable: false },
               { text: 'Last Update By', dataField: 'LastUpdateBy', filtertype: 'input', width: '120', editable: false },
               { text: 'Last Update Date', dataField: 'LastUpdateDate', cellsformat: 'd', filtertype: 'imput', width: '120', editable: false }

               ]

           });

        }
    });
}


function getSelectedDataTotal() {
    var rows = $("#jqxGridRoles").jqxGrid('selectedrowindexes');
    var selectedRecords = new Array();
    for (var iCount = 0; iCount < rows.length; iCount++) {
        var row = $("#jqxGridRoles").jqxGrid('getrowdata', rows[iCount]);
        selectedRecords[selectedRecords.length] = row;
    }

    var listOfParams = {
        data: JSON.stringify(selectedRecords)
    }

    _callServer({
        loadingMsg: "", 
        url: '/RoleAdmin/saveSearch',
        data: {
            'data': JSON.stringify(listOfParams)
        },
        type: "post",
        success: function (json) {
            
            $("#popUpRoles").jqxWindow('hide');
            AddHTML();

        }
    });
}

function AddHTML() {
    _callServer({
        loadingMsg: "",
        url: '/RoleAdmin/getHTML',
        type: "post",
        success: function (json) {

            $('#RoleCreate').append(json);
            Create();
        }
    });
}


function Create() {

    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/RoleAdmin/getMOU',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var result = response;

                jQuery.each(result, function (index, itemData) {

                    MyGrids = MyGrids + "#jqxGData" + itemData.MOU + "|" + itemData.MOU + "|" + itemData.ID + ",";

                    renderMOUData("#jqxGTitle" + itemData.MOU, itemData)
                    //renderGridData("#jqxGData" + itemData.MOU, itemData.ID);

                   // renderValidations(itemData.MOU);


                });

                MyGrids = MyGrids.substring(0, MyGrids.length - 1);
            
        }
    });

}

function renderValidations(MOU) {
   // $("#valNot" + MOU).jqxNotification({
   //     width: '100%', position: "top-left", opacity: 0.9, appendContainer: "#notC" + MOU,
   //     autoOpen: false, animationOpenDelay: 500, autoClose: true, autoCloseDelay: 15000, template: "error"
   // });
}

function renderGridData(grid, MouID) {

    var listOfParams = {
        MOUID: MouID
    }

    $.ajax({
        type: "POST",
        url: "Role.aspx/getRolesByMOU",
        data: JSON.stringify({
            listOfParams: listOfParams
        }),
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response) {
                var data = response.d;

                // renderer for grid cells.
                var numberrenderer = function (row, column, value) {
                    return '<div style="text-align: center; margin-top: 5px;">' + (1 + value) + '</div>';
                }
                // create Grid datafields and columns arrays.

                var cssclass = 'jqx-widget-header';
                if (theme != '') cssclass += ' jqx-widget-header-' + theme;

                var source =
              {
                  unboundmode: true,
                  localdata: data,
                  datafields:
                          [
                          { name: 'ID', type: 'string' },
                          { name: 'RoleID', type: 'string' },
                          { name: 'Name', type: 'string' },
                          { name: 'Description', type: 'string' },
                          { name: 'GOCID', type: 'string' },
                          { name: 'MSL09', type: 'string' },
                          { name: 'ManagedSegment', type: 'string' },
                          { name: 'Process', type: 'string' },
                          { name: 'SubProcess', type: 'string' },
                          { name: 'MgdGeaography', type: 'string' },
                          { name: 'ManagedGeography', type: 'string' },
                          { name: 'MgdRegion', type: 'string' },
                          { name: 'MgdCountry', type: 'string' },
                          { name: 'LVID', type: 'string' },
                          { name: 'PhysicalLocationID', type: 'string' },
                          { name: 'PhysicalLocation', type: 'string' },
                          { name: 'ChargeOutProfileID', type: 'string' },
                          { name: 'ChargeOutProfile', type: 'string' },
                          { name: 'IsActive', type: 'string' },
                          { name: 'CreatedBy', type: 'string' },
                          { name: 'CreatedDate', type: 'string' },
                          { name: 'LastUpdateBy', type: 'string' },
                          { name: 'LastUpdateDate', type: 'string' },
                          { name: 'StatusID', type: 'string' },
                          { name: 'Status', type: 'string' },
                          { name: 'RoleTypeID', type: 'string' },
                          { name: 'Number', type: 'string' },
                          { name: 'IDType', type: 'string' },
                          { name: 'IsFteable', type: 'string' }
                          ],
                  datatype: "json"
              };
                var dataAdapter = new $.jqx.dataAdapter(source);

                var imgRemoveRole = function (row, datafield, value) {

                    var rowData = $(grid).jqxGrid('getrowdata', row);

                    return '<center><img onClick="deleteRole(' + rowData.RoleID + ')" style="margin-left: 5px;margin-top: 5px; cursor:pointer; width:20px; height:20px"   src="../Images/Delete.png"/></center>';
                }

                // initialize jqxGrid
                $(grid).jqxGrid(
               {
                   width: '100%',
                   source: dataAdapter,
                   editable: true,
                   columnsresize: true,
                   theme: 'classic',
                   selectionmode: 'multiplecellsadvanced',
                   autoheight: true,
                   autorowheight: true,
                   columns: [
                   { pinned: true, exportable: false, text: "", columntype: 'number', cellclassname: cssclass, cellsrenderer: numberrenderer },
                   { text: 'Delete', filtertype: 'none', width: '5%', columntype: 'image', pinned: false, cellsrenderer: imgRemoveRole },
                   { text: 'Role ID', dataField: 'RoleID', filtertype: 'checkedlist', width: '6%', editable: false },
                   //{ text: 'Alloted', dataField: 'isAlloted', columntype: 'checkbox', width: 100, editable: true, resizable: false, type: 'bool' },
                   {
                       text: 'Name', dataField: 'Name', columntype: 'textbox', width: '40%', align: 'center', validation: function (cell, value) {
                           if (value.length < 3 && value.length > 0) {
                               return { result: false, message: "Please write a valied name." };
                           } return true;
                       }
                   },
                   {
                       text: 'Description', dataField: 'Description', columntype: 'textbox', width: '40%', align: 'center',
                       validation: function (cell, value) {
                           if (value.length < 3 && value.length > 0) {
                               return { result: false, message: "Please write a valied name." };
                           }
                           return true;
                       }
                   }
                   ,
                   {
                       text: 'Is FTeable', datafield: 'IsFteable', threestatecheckbox: true, columntype: 'checkbox', width: '6%'
                   }
                   ]
               });

                $(grid).on('cellvaluechanged', function (event) {
                    // event arguments.
                    var args = event.args;
                    // column data field.
                    var datafield = event.args.datafield;

                    if (datafield == "IsFteable") {
                        var rowBoundIndex = args.rowindex; var value = args.newvalue; var oldvalue = args.oldvalue;

                        if (value != oldvalue) {
                            var row = $(grid).jqxGrid('getrowdata', args.rowindex);
                            var rowid;
                            if (value) {

                                rowid = row.RoleID.substring(0, 2).toString() + "1" + row.ID.toString();
                            } else {
                                rowid = row.RoleID.substring(0, 2).toString() + "0" + row.ID.toString();
                            }

                            $(grid).jqxGrid('setcellvalue', rowBoundIndex, 'RoleID', rowid);
                        }


                    }

                });
            }
        }
    });

}

function renderMOUData(grid, data) {


    var source =
   {
       unboundmode: true,
       localdata: data,
       datafields:
               [
                   { name: 'ID', type: 'string' },
                   { name: 'MOU', type: 'string' },
                   { name: 'Name', type: 'string' },
                   { name: 'ManagedGeographyID', type: 'string' },
                   { name: 'ManagedGeographyName', type: 'string' },
                   { name: 'ManagedSegmentID', type: 'string' },
                   { name: 'ManagedSegmentName', type: 'string' },
                   { name: 'ApprovalSOEID', type: 'string' },
                   { name: 'ApprovalName', type: 'string' },
                   { name: 'ApprovedDate', type: 'string' },
                   { name: 'FTE', type: 'string' },
                   { name: 'TotalRole', type: 'string' },
                   { name: 'GOC', type: 'string' },
                   { name: 'ChargeOutGOC', type: 'string' }

               ],
       datatype: "json"
   };

    var imgAddRole = function (row, datafield, value) {

        var rowData = $(grid).jqxGrid('getrowdata', row);

        return '<center><img onClick="addRole(' + rowData.ID + ',&#39;' + rowData.MOU + '&#39;,&#39;' + rowData.Name + '&#39;)" style="margin-left: 5px;margin-top: 5px; cursor:pointer; width:25px; height:25px"   src="../Images/add.png"/></center>';
    }

    var htmlGOC = function (row, datafield, value) {

        var rowData = $(grid).jqxGrid('getrowdata', row);

        return '<center>' +rowData.GOC + '</center>' ;
    }


    var dataAdapter = new $.jqx.dataAdapter(source);

    $(grid).jqxGrid(
   {
       source: dataAdapter,
       theme: 'blackberry',
       width: '100%',
       autoheight: true,
       autorowheight: true,
       pageable: false,
       filterable: false,
       showfilterrow: false,
       selectionmode: 'multiplecellsadvanced',
       columns: [
       { text: 'MOU', dataField: 'MOU', filtertype: 'checkedlist', width: '6%', editable: false },
       { text: 'Name', dataField: 'Name', filtertype: 'checkedlist', width: '10%', editable: false },
       { text: 'Managed Geography', dataField: 'ManagedGeographyName', filtertype: 'input', width: '10%', editable: false },
       { text: 'Managed Segment', dataField: 'ManagedSegmentName', filtertype: 'input', width: '14%', editable: false },
       { text: 'GOC', dataField: 'GOC', filtertype: 'none', width: '10%', editable: false, cellsrenderer: htmlGOC },
       { text: 'SOEID Approval', dataField: 'ApprovalSOEID', filtertype: 'input', width: '6%', editable: false },
       { text: 'Approval Name', dataField: 'ApprovalName', filtertype: 'input', width: '10%', editable: false },
       { text: 'Approved Date', dataField: 'ApprovedDate', filtertype: 'input', width: '10%', editable: false },
       { text: 'FTE', dataField: 'FTE', filtertype: 'input', width: '3%', editable: false },
       { text: 'Total Roles', dataField: 'TotalRole', filtertype: 'input', width: '3%', editable: false },
       { text: 'View all Role', filtertype: 'none', width: '5%', columntype: 'image', pinned: false, cellsrenderer: imgAddRole },
       { text: 'Add Role', filtertype: 'none', width: '5%', columntype: 'image', pinned: false, cellsrenderer: imgAddRole }
       ]

   });

}