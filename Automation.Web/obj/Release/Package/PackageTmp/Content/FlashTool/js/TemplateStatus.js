var _uploadParameters = {};

$(document).ready(function () {
    loadFilesTables();
    $('#fine-uploader-manual-trigger').fineUploader({
        template: 'qq-template-manual-trigger',
        request: {
            endpoint: _getViewVar("SubAppPath") + '/FlashTool/Upload'
        },
        thumbnails: {
            placeholders: {
                waitingPath: _getViewVar("SubAppPath") + '/Content/Shared/plugins/fine-uploader/placeholders/waiting-generic.png',
                notAvailablePath: _getViewVar("SubAppPath") + '/Content/Shared/plugins/fine-uploader/placeholders/not_available-generic.png'
            }
        },
        callbacks: {
            onAllComplete: function (succeeded, failed) {
                if (failed.length == 0) {
                    loadFilesTables();
                    _showAlert({
                        type: "success",
                        title: "Message",
                        content: "The file was uploaded successfully",
                        animateScrollTop: true
                    });
                }
            },
            onError: function (id, name, errorReason, xhrOrXdr) {
                if (name) {
                    _showDetailAlert({
                        title: "Message",
                        shortMsg: "An error ocurred with the file '" + name + "'.",
                        longMsg: "<pre>" + errorReason + "</pre>",
                        type: "Error",
                        viewLabel: "View Details"
                    });
                }
            },
            onSubmit: function (id, name) {
                //$('#fine-uploader-manual-trigger').fineUploader('setParams', { 'typeFile': $("#selTypeFile").val() });
            },
            onManualRetry: function (id, name) {
                //$('#fine-uploader-manual-trigger').fineUploader('setParams', { 'typeFile': $("#selTypeFile").val() });
            }
        },
        autoUpload: false
    });
    //$("#contenttabletblFileTable").append('<button type="submit" class="btn btn-success">Save</button>');
    //On click upload files
    $('#trigger-upload').click(function () {

        //TODO: Add validation to select typeFile
        var $sel = $('.selTypeFile');
        var lValue = true;
        var lLenght = $sel.length;
        $($sel).each(function () {
            if (lLenght = 2) {
                var lName = $('select.selTypeFile').map(function () {
                    return this.value
                }).get()
                if (lName[0] === "GDW" && lName[1]==="SOW") {
                        console.log('nombre: ' + lName);
                        //lNames.push(lName);
                }
            }
        });
        $(".selTypeFile").each(function () {
            var lSelected = $(this).val();
            //console.log($(this).val());
            if (lSelected == "") {
                lValue = false;
            } 
        });
        if (lValue) {
            console.log('entre');
            $('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
        } else {
            _showAlert({
                content: "Please select the type of the file"
            });
        }
       
    });

    //On Change Select Type Upload
    $('body').on('change', '.selTypeFile', function () {
        var $sel = $(this);
        var idFile = $sel.parent().attr("qq-file-id");
        var typeFile = $sel.val();
        var uiIdFile = $('#fine-uploader-manual-trigger').fineUploader('getUuid', idFile);
        var parameterName = "typeFile_" + uiIdFile;
        _uploadParameters[parameterName] = typeFile;
        $('#fine-uploader-manual-trigger').fineUploader('setParams', _uploadParameters);
    });
});
//Fn to get Date
function fnGetDate() {
    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();
    if (dateObj.getDay() == 1) {
        lYesterday = day - 3;
        console.log("Es Lunes " + year + "-" + month + "-" + lYesterday);
        return gTodayDate = year + "-" + month + "-" + lYesterday;
    }
    console.log("no es lunes "+year + "-" + month + "-" + day);
    return gTodayDate = year + "-" + month + "-" + day;
}
function loadFilesTables() {
    function getDate(lVal) {
        var lDate = new Date();
        var lMonth = lDate.getMonth() + 1;
        if (lVal == 'm') {
            return lMonth
        }
        if (lVal == 'y') {
            return lDate.getFullYear()
        }
    }
    $.jqxGridApi.create({
        showTo: "#tblFileTable",
        options: {
            //for comments or descriptions
            height: "260",
            autoheight: false,
            autorowheight: false,
            showfilterrow: true,
            sortable: true,
            editable: true,
            //'singlerow', 'multiplerows', 'checkbox', 'multiplerowsadvanced' or 'multiplerowsextended'.
            selectionmode: "singlerow",
            resizable: true
        },
        sp: {
            Name: "[dbo].[spFlashGetAllFeedNames]",
            Params: [
                { Name: "@pMonth", Value: getDate('m') },
                { Name: "@pYear", Value: getDate('y') }
            ]
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set",
            //rows: docToUpload
        },
        groups: [],
        columns: [
            //type: string - text - number - int - float - date - time 
            //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
            //cellsformat: ddd, MMM dd, yyyy h:mm tt
            { name: 'FileID', type: 'number', hidden: true },
            { name: 'FeedNames', text: 'FeedsName/System', width: '40%', type: 'string', filtertype: 'input', align: 'center', editable: false },
            {
                name: 'Receive', text: 'File Status', width: '30%', type: 'string', filtertype: 'input', editable: false, filterable: true, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                    var dataRecord = $("#tblFileTable").jqxGrid('getrowdata', rowIndex);
                    var htmlResult = '';

                    //Add Status
                    switch (dataRecord.Receive) {
                        case "True":
                            htmlResult += '<span class="badge badge-md badge-info" style="margin-top: 5px; margin-left: 5px;"> Received </span>';
                            break;

                        case "False":
                            htmlResult += '<span class="badge badge-md badge-warning" style="margin-top: 5px; margin-left: 5px;"> Not Received </span>';
                            break;

                        default:
                            htmlResult += '<span class="badge badge-md badge-warning" style="margin-top: 5px; margin-left: 5px;">' + dataRecord.Receive + '</span>';
                            break;
                    }

                    return htmlResult;
                },
            },
            { name: 'UploadFullDate', text: 'Date', width: '30%', type: 'string', filtertype: 'input' }
        ],
        columngroups:
        [
          { text: 'BD-8', align: 'center', name: 'BD8' },
        ],
        
        ready: function () { }
    });
    var imagerenderer = function (row, datafield, value) {
        return '<a href="#"><img style="margin: 10px;" height="20" width="20" src="../../Content/FlashTool/img/dEx.png"/></a>';
    }

    $.jqxGridApi.create({
        showTo: "#tblDailyFeed",
        options: {
            //for comments or descriptions
            height: "193",
            columnsresize: true,
            autoheight: false,
            autorowheight: false,
            sortable: true,
            editable: false,
            rowDetails: true,
            //'singlerow', 'multiplerows', 'checkbox', 'multiplerowsadvanced' or 'multiplerowsextended'.
            selectionmode: "singlerow"
        },
        sp: {
            Name: "[dbo].[spFlashGetAllFeedNamesDAILY]",
            Params: [
                { Name: "@pMonth", Value: getDate('m') },
                { Name: "@pYear", Value: getDate('y') }
            ]
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set",
            //rows: docToUploadDaily
        },
        columns: [
            //type: string - text - number - int - float - date - time 
            //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
            //cellsformat: ddd, MMM dd, yyyy h:mm tt
            { name: 'FileID', type: 'number', hidden: true },
            { name: 'FeedNames', text: 'FeedsName/System', width: '40%', type: 'string', filtertype: 'input', align: 'center', editable: false },
            {
                name: 'Receive', text: 'File Status', width: '30%', type: 'string', filtertype: 'input', editable: false, filterable: true, cellsalign: 'center', align: 'center', cellsrenderer: function (rowIndex, datafield, value) {
                    var dataRecord = $("#tblDailyFeed").jqxGrid('getrowdata', rowIndex);
                    var htmlResult = '';

                    //Add Status
                    console.log();
                    switch (dataRecord.Receive) {
                        case "True":
                            htmlResult += '<span class="badge badge-md badge-info" style="margin-top: 5px; margin-left: 5px;"> Received </span>';
                            break;

                        case "False":
                            htmlResult += '<span class="badge badge-md badge-warning" style="margin-top: 5px; margin-left: 5px;"> Not Received </span>';
                            break;

                        default:
                            htmlResult += '<span class="badge badge-md badge-warning" style="margin-top: 5px; margin-left: 5px;">' + dataRecord.Receive + '</span>';
                            break;
                    }

                    return htmlResult;
                },
            },
            { name: 'UploadFullDate', text: 'Date', width: '30%', type: 'string', filtertype: 'input' }
        ],
        ready: function () {

        }
    });
    
    $("#excel").click(function () {
        var lSelectFile;
        $("select")
        .change(function () {
            var str = "";
            $("select option:selected").each(function () {
                str += $(this).val() + " ";
            });
            lSelectFile = str;
        })
        .trigger("change");

        console.log(lSelectFile);
        function fnDisclaimer() {
            var htmlContentModal = '';
            htmlContentModal += "<b>If you don't see specific data, you would have to process it in flash detail.</b> ";
            _showModal({
                modalId: "modalUpload",
                width: '70%',
                buttons: [{
                    name: "<i id='okBtn' class='fa fa-save'></i> OK",
                    class: "btn-success",
                    closeModalOnClick: false,
                    onClick: function ($modal) {
                        //Add Comments
                        fnGetExcel(lSelectFile);
                        $modal.find(".close").click();
                    }
                }],
                addCloseButton: false,
                title: "Download mappings",
                contentHtml: htmlContentModal,
                onReady: function ($modal) { 
                }
            });
            
        }
        if (lSelectFile === " ") {

        } else {
            fnDisclaimer();
        }
        

    });
    $("#downloadTemplate").click(function () {
        _downloadExcel({
            sql: "[dbo].[spFlashGetMappingTemplate]",
            filename: "ExpenseCodeMappingTemplate.xls",
            success: {
                msg: "Generating template... Please Wait, this operation may take some time to complete.",
            }

        });
    });
    $("#downloadGocList").click(function () {
        _downloadExcel({
            sql: "[dbo].[spFlashGetGocList]",
            filename: "GocList",
            success: {
                msg: "Generating template... Please Wait, this operation may take some time to complete.",
            }

        });
    });
    function fnGetExcel(lSelectFile) {
        _downloadExcel({
            sql: "[dbo].[spFlashGetExcel"+lSelectFile+"]",
            filename: lSelectFile + "_Report.xls",
            success: {
                msg: "Generating report... Please Wait, this operation may take some time to complete.",
            }
           
        });
       // $('body').delegate('.closeButton', 'click', function () {
       //     // do your stuff
       // });
       //// $modal.find(".close").click();
    }
    function fnLastExpenseCodeMappingUpdate() {
        _callProcedure({
            loadingMsgType: "Loading last update",
            loadingMsg: "Loading...",
            name: "[dbo].[spFlashLastExpenseCodeMappingUpdate]",
            params: [

            ],
            success: {
                fn: function (responseList) {
                    var lastUpdate = responseList[0].LastModifiedDate;
                    lastUpdate = new Date(lastUpdate).toUTCString();
                    lastUpdate = lastUpdate.split(' ').slice(0, 4).join(' ') 
                    $('#lastUpdate').text("Last update: " + lastUpdate);
                }
            },
        });
    }
    fnLastExpenseCodeMappingUpdate();
}

_showLoadingFullPage({
    msg: "Loading Feeds"
});

//button click
_hideLoadingFullPage()


