$(document).ready(function () {
    //$(".btnViewDetail").click(function () {
    //    var lSelectedRow = $.jqxGridApi.getOneSelectedRow("#tblFileTable", true);
    //    _showModal({
    //        modalId: "modalFormNewItem",
    //        addCloseButton: true,
    //        title: "Flash Feed Detail",
    //        contentHtml: "00002	1990140003	Computer Asset Clearing	1990141900	Computer Asset Clearing	100015674	Payables	Purchase Invoices	INSIGHT DIRECT USA INCORPORATED	0912422947	25-Aug-2017	7681040	APC Smart-UPS 1500 LCD - UPS - 1 kW - 15	OCT-17	02-Oct-2017	13500	0	USD	13500	0	13499.7	Global Customer Operations [L7	Branch Operations (CBNA CFNA) 	CBNA Branch - NAIT CBNA [L9]	0	Branch Operations (CBNA CFNA) [L8]	BALANCE SHEET	BALANCE SHEET	160410	Property, Plant and Equipment (L5)	Not Applicable	Not Applicable	Not Applicable	Not Applicable	Not Applicable	Not Applicable	Not Applicable	Not Applicable	BALANCE SHEET",
    //    });
    //});
    //_hideMenu();
    //Load all flash custom segments
    //lShowModal();

    //$(".btnEdit").click(function () {
    //    EditUser();
    //});
    //$(".btnAdd").click(function () {
    //    AddUser();
    //});
    //$(".btnCustomize").click(function () {
    //    CustomizeSegment();
    //});


    //$("#checkAllSegments").on('change', function () {
    //    alert('here');
    //    var inputCheck = $(this);
    //    console.log("Checked -" + inputCheck.is(":checked"));

    //});

    //$("#contenttabletblFileTable").append('<button type="submit" class="btn btn-success">Save</button>');
    $(".btnDelete").click(function () {
        fnDisableSegment();
    });
    fnLoadFlashCustomSegments();
});

//function lShowModal() {
//    var classListRoles = this;
//    //On click in btn add new user
//    $(".btnAddNewRole").click(function () {
//        console.log('click');
//        classListRoles.addNewUser();
//    });
//    //On click in btn edit user
//    $(".btnEditRole").click(function () {
//        classListRoles.editUser();
//    });

//    //On click in btn delete user
//    $(".btnDeleteRole").click(function () {
//        classListRoles.deleteUser();
//    });

//    this.createFormRole = function (options) {
//        var objRole = options.objRole;
//        var formHtml =
//            '<div class="form-horizontal"> ' +
//            '    <div class="col-md-10"> ' +
//            '        <div class="form-group"> ' +
//            '            <label class="col-md-4 control-label"> ' +
//            '                <i class="fa fa-asterisk iconasterisk"></i> Name ' +
//            '            </label> ' +
//            '            <div class="col-md-8"> ' +
//            '               <input type="text" class="txtName form-control" value="' + objRole.Name + '" placeholder="eg. ISSUE_LOG_MANAGER"> ' +
//            '            </div> ' +
//            '        </div> ' +
//            '        <div class="form-group"> ' +
//            '            <label class="col-md-4 control-label"> ' +
//            '                <i class="fa fa-asterisk iconasterisk"></i> Marketplace Code ' +
//            '            </label> ' +
//            '            <div class="col-md-8"> ' +
//            '               <input type="text" class="txtEERSMarketplaceRoleID form-control" value="' + objRole.EERSMarketplaceRoleID + '" placeholder="eg. 165986_2"> ' +
//            '            </div> ' +
//            '        </div> ' +
//            '        <div class="form-group"> ' +
//            '            <label class="col-md-4 control-label"> ' +
//            '                <i class="fa fa-asterisk iconasterisk"></i> Marketplace Name ' +
//            '            </label> ' +
//            '            <div class="col-md-8"> ' +
//            '               <input type="text" class="txtEERSFunctionCode form-control" value="' + objRole.EERSFunctionCode + '" placeholder="eg. Issue Log - Manager"> ' +
//            '            </div> ' +
//            '        </div> ' +
//            '        <div class="form-group"> ' +
//            '            <label class="col-md-4 control-label"> ' +
//            '                <i class="fa fa-asterisk iconasterisk"></i> EERS Function Descrition ' +
//            '            </label> ' +
//            '            <div class="col-md-8"> ' +
//            '               <input type="text" class="txtEERSFunctionDescription form-control" value="' + objRole.EERSFunctionDescription + '" placeholder="eg. [RO] - Users with ready only access"> ' +
//            '            </div> ' +
//            '        </div> ' +
//            '        <div class="form-group"> ' +
//            '            <label class="col-md-4 control-label"> ' +
//            '                <i class="fa fa-asterisk iconasterisk"></i> Ignore Role in EERS? ' +
//            '            </label> ' +
//            '            <div class="col-md-8"> ' +
//            '               <input value="1" type="radio" name="chkFlagEERSIgnore" class="rbtFlagGlobalYes skin-square-green" ' + ((objRole.EERSIgnore == "True") ? "checked" : "") + ' > ' +
//            '               <label class="iradio-label form-label hover" style="margin-right: 25px;">Yes </label> ' +
//            '               <input value="0" type="radio" name="chkFlagEERSIgnore" class="rbtFlagGlobalNo skin-square-green" ' + ((objRole.EERSIgnore == "False") ? "checked" : "") + ' > ' +
//            '               <label class="iradio-label form-label hover" style="margin-right: 25px;">No </label> ' +
//            '            </div> ' +
//            '        </div> ' +
//            '    </div> ' +
//            '    <div style="clear: both;"></div> ' +
//            '</div> ';
//        _showModal({
//            width: "75%",
//            title: "Role Information",
//            contentHtml: formHtml,
//            buttons: [{
//                name: "Save",
//                class: "btn-success",
//                closeModalOnClick: true,
//                onClick: function ($modal) {
//                    objRole.Name = $modal.find(".txtName").val();
//                    objRole.EERSMarketplaceRoleID = $modal.find(".txtEERSMarketplaceRoleID").val();
//                    objRole.EERSFunctionCode = $modal.find(".txtEERSFunctionCode").val();
//                    objRole.EERSFunctionDescription = $modal.find(".txtEERSFunctionDescription").val();
//                    objRole.EERSIgnore = ($modal.find('[name="chkFlagEERSIgnore"]:checked').val() == '1' ? 'True' : 'False');
//                    if (options.onSave) {
//                        options.onSave(objRole);
//                    }
//                }
//            }],
//            addCloseButton: true,
//            onReady: function () {
//                _GLOBAL_SETTINGS.iCheck();
//            }
//        });
//    };
//}
/*
function CustomizeSegment() {

    var formHtml =
            '<div class="row"> ' +
            '    <div class="col-md-12"> ' +
            '        <div class="pull-left"> ' +
            '            <Label>Name for segments:</label> ' +
            '               <input placeholder="Set The Name" type="text" name="setSegmentName" class="rbtFlagIsDeletedYes skin-square-green"> ' +
            '       </div><br/>' +
            '    <div class="col-md-12"> ' +
            '            <p>Please check the Segments that you need to map to the selected user:</p> ' +
            '        <div class="selUserOfRoles"> ' +
            '                                   <div>' +
            '                                       <input type="checkbox" class="ManagedSegments" name="flashSegments" value="Cyber Security">'+
            '                                       <label>Cyber Security</label>' +
            '                                   </div>'+  
            '                                   <div>' +
            '                                       <input type="checkbox" class="ManagedSegments" name="flashSegments" value="Security">' +
            '                                       <label>Security</label>' +
            '                                   </div>' +
            '                                   <div>' +
            '                                       <input type="checkbox" class="ManagedSegments" name="flashSegments" value="Cyber Security">' +
            '                                       <label>Network</label>' +
            '                                   </div>' +
            '        </div> ' +
            '        <div style="text-align: center; padding-right: 39px; padding-bottom: 8px;" class="pull-right"> ' +
            '            Check all <br /> ' +
            '            <input type="checkbox" class="skin-square-green checkAllSegments" id="checkAllSegments" name="checkAllSegments" "> ' +
            '        </div> ' +
            '        </div> ' +
            '        <div style="clear:both;"></div> ' +
            '    </div> ' +
                        '        <p>Please select the user to grand access:</p> ' +
            '        <div class="selUserOfRoles"> ' +
            '                                   <select>' +
            '                                       <option value="a">RE65657</option>' +
            '                                       <option value="b">RE65658</option>' +
            '                                       <option value="c">RE65659</option>' +
            '                                   </select> ' +
            '        </div> ' +
            //'    <div class="col-md-12"> ' +
            //'        <h3>Is Active?</h3> ' +
            //'        <p>User was deleted or not deleted:</p> ' +
            //'        <div class="form-group"> ' +
            //'           <input value="1" type="radio" name="chkFlagIsDeleted" class="rbtFlagIsDeletedYes skin-square-green" ' + ((objUser.IsDeleted) ? "checked" : "") + ' > ' +
            //'           <label class="iradio-label form-label hover" style="margin-right: 25px;">Deleted </label> ' +
            //'           <input value="0" type="radio" name="chkFlagIsDeleted" class="rbtFlagIsDeletedNo skin-square-green" ' + ((objUser.IsDeleted == false) ? "checked" : "") + ' > ' +
            //'           <label class="iradio-label form-label hover" style="margin-right: 25px;">Not Deleted </label> ' +
            //'        </div> ' +
            //'    </div> ' +
            '</div> ';
   
    _showModal({
        width: "75%",
        title: "Customize Segments",
        contentHtml: formHtml,

        buttons: [{
            name: "Save",
            class: "btn-success",
            closeModalOnClick: false,
            onClick: function ($modal) {
                
            }
        }],
        addCloseButton: true,
        onReady: function ($modal) {
            $modal.find("#checkAllSegments").click(function () {
                $("input:checkbox").prop("checked", this.checked);
            });
        }

    });
}
*/
/*
function AddUser() {
    var formHtml =
            '<div class="container">'+
                '<div class="row">'+
                    '<div class="col col-lg-2"><div>SOEID</div>'+
                    '   <div>RE65657</div>' +
                    '   <div>RE65658</div>' +
                    '   <div>RE65659</div>' +
                    '</div>' +
                    '<div class="col col-lg-2"><div>Segments</div>' +
                    '   <div>Network L9</div>' +
                    '   <div>Cyber Security</div>' +
                    '   <div>Cyber Security & Networks</div>' +
                    '</div>' +
                    '<div class="col col-lg-2">3 of three columns</div>' +
                '</div>'+
            '</div>'+
            '<div class="row"> ' +
            '    <div class="col-md-12"> ' +
            //'        <h3>Select User</h3> ' +
            //'        <p>Users</p> ' +
            '        <div class="selUserOfRoles"> ' +   

            '        </div> ' +
            '    <div class="col-md-12"> ' +
            '        <div class="pull-left"> ' +
            //'            <h3>Set Roles</h3> ' +
            '            <p>Please check the Segments that you need to map to the selected user:</p> ' +
            '        <div class="selUserOfRoles"> ' +   
            '                                   <select>' +
            '                                       <option value="volvo">Cyber Security</option>' +
            '                                       <option value="volvo">Network</option>' +
            '                                       <option value="volvo">Cyber Security & Networks</option>' +
            '                                   </select> ' +
            '        </div> ' +
            '        </div> ' +
            '        <div style="text-align: center; padding-right: 39px; padding-bottom: 8px;" class="pull-right"> ' +
            '            Check all <br /> ' +
            '            <input type="checkbox" class="skin-square-green checkAllUserXRoles"> ' +
            '        </div> ' +
            '        <div style="clear:both;"></div> ' +
            //'        <div class="well transparent" id="roleCategories"></div> ' +
            '    </div> ' +
            //'    <div class="col-md-12"> ' +
            //'        <h3>Is Active?</h3> ' +
            //'        <p>User was deleted or not deleted:</p> ' +
            //'        <div class="form-group"> ' +
            //'           <input value="1" type="radio" name="chkFlagIsDeleted" class="rbtFlagIsDeletedYes skin-square-green" ' + ((objUser.IsDeleted) ? "checked" : "") + ' > ' +
            //'           <label class="iradio-label form-label hover" style="margin-right: 25px;">Deleted </label> ' +
            //'           <input value="0" type="radio" name="chkFlagIsDeleted" class="rbtFlagIsDeletedNo skin-square-green" ' + ((objUser.IsDeleted == false) ? "checked" : "") + ' > ' +
            //'           <label class="iradio-label form-label hover" style="margin-right: 25px;">Not Deleted </label> ' +
            //'        </div> ' +
            //'    </div> ' +
            '</div> ';
    _showModal({
        width: "75%",
        title: "User Information",
        contentHtml: formHtml,
        buttons: [{
            name: "Save",
            class: "btn-success",
            closeModalOnClick: false,
            onClick: function ($modal) {
                objUser.SOEID = $modal.find(".selUserOfRoles select").val();

                if (objUser.SOEID) {
                    //objUser.IsDeleted = ($modal.find('[name="chkFlagIsDeleted"]:checked').val() == '1' ? 'True' : 'False');

                    //Saves roles of user
                    objUserXRoles.saveUserXRoles();

                    //On Complete ajaxs reload list
                    _execOnAjaxComplete(function () {

                        //Save IsDeleted and Create User
                        if (options.onSave) {
                            options.onSave(objUser);
                        }

                        //Close Modal
                        $modal.find("[data-dismiss='modal']").click();
                    });
                } else {
                    _showNotification("error", "Please select an employee", "ErrorMissingEmp");
                }
            }
        }],
        addCloseButton: true,

    });
}
*/
/*
function EditUser() {
    var formHtml =
            '<div class="row"> ' +
            '    <div class="col-md-12"> ' +
            '        <div class="pull-left"> ' +
            '            <Label>Name for segments:</label> ' +
            '               <input placeholder="Set The Name" type="text" name="setSegmentName" class="rbtFlagIsDeletedYes skin-square-green"> ' +
            '       </div><br/>' +
            '    <div class="col-md-12"> ' +
            '            <p>Please check the Segments that you need to map to the selected user:</p> ' +
            '        <div class="selUserOfRoles"> ' +
            '                                   <div>' +
            '                                       <input type="checkbox" class="ManagedSegments" name="flashSegments" value="Cyber Security" checked>' +
            '                                       <label>Cyber Security</label>' +
            '                                   </div>' +
            '                                   <div>' +
            '                                       <input type="checkbox" class="ManagedSegments" name="flashSegments" value="Security">' +
            '                                       <label>Security</label>' +
            '                                   </div>' +
            '                                   <div>' +
            '                                       <input type="checkbox" class="ManagedSegments" name="flashSegments" value="Cyber Security">' +
            '                                       <label>Network</label>' +
            '                                   </div>' +
            '                                   <div>' +
            '                                       <input type="checkbox" class="ManagedSegments" name="flashSegments" value="Cyber Security">' +
            '                                       <label>Distributed Platform Admin [L9]</label>' +
            '                                   </div>' +
            '                                   <div>' +
            '                                       <input type="checkbox" class="ManagedSegments" name="flashSegments" value="Cyber Security" checked>' +
            '                                       <label>GCB Core Infra Service [L8]</label>' +
            '                                   </div>' +
            '        </div> ' +
            '        <div style="text-align: center; padding-right: 39px; padding-bottom: 8px;" class="pull-right"> ' +
            '            Check all <br /> ' +
            '            <input type="checkbox" class="skin-square-green checkAllSegments" id="checkAllSegments" name="checkAllSegments" "> ' +
            '        </div> ' +
            '        </div> ' +
            '        <div style="clear:both;"></div> ' +
            '    </div> ' +
                        '        <p>Please select the user to grand access:</p> ' +
            '        <div class="selUserOfRoles"> ' +
            '                                   <select>' +
            '                                       <option value="a">RE65657</option>' +
            '                                       <option value="b" selected>RE65658</option>' +
            '                                       <option value="c">RE65659</option>' +
            '                                   </select> ' +
            '        </div> ' +
            //'    <div class="col-md-12"> ' +
            //'        <h3>Is Active?</h3> ' +
            //'        <p>User was deleted or not deleted:</p> ' +
            //'        <div class="form-group"> ' +
            //'           <input value="1" type="radio" name="chkFlagIsDeleted" class="rbtFlagIsDeletedYes skin-square-green" ' + ((objUser.IsDeleted) ? "checked" : "") + ' > ' +
            //'           <label class="iradio-label form-label hover" style="margin-right: 25px;">Deleted </label> ' +
            //'           <input value="0" type="radio" name="chkFlagIsDeleted" class="rbtFlagIsDeletedNo skin-square-green" ' + ((objUser.IsDeleted == false) ? "checked" : "") + ' > ' +
            //'           <label class="iradio-label form-label hover" style="margin-right: 25px;">Not Deleted </label> ' +
            //'        </div> ' +
            //'    </div> ' +
            '<script>' +
            '   $("#checkAllSegments").click(function(){ ' +
            '                                           $("input:checkbox").prop("checked", this.checked);' +
            '   });' +
            '</script>' +
            '</div> ';
    _showModal({
        width: "75%",
        title: "User Information",
        contentHtml: formHtml,
        buttons: [{
            name: "Save",
            class: "btn-success",
            closeModalOnClick: false,
            onClick: function ($modal) {
                objUser.SOEID = $modal.find(".selUserOfRoles select").val();

                if (objUser.SOEID) {
                    //objUser.IsDeleted = ($modal.find('[name="chkFlagIsDeleted"]:checked').val() == '1' ? 'True' : 'False');

                    //Saves roles of user
                    objUserXRoles.saveUserXRoles();

                    //On Complete ajaxs reload list
                    _execOnAjaxComplete(function () {

                        //Save IsDeleted and Create User
                        if (options.onSave) {
                            options.onSave(objUser);
                        }

                        //Close Modal
                        $modal.find("[data-dismiss='modal']").click();
                    });
                } else {
                    _showNotification("error", "Please select an employee", "ErrorMissingEmp");
                }
            }
        }],
        addCloseButton: true,
        
    });
}
*/
//fn to disable the selected segments
function fnDisableSegment() {
    var objRowSelected = $.jqxGridApi.getOneSelectedRow("#flashEditSegmentsGrid", true);
    //fn return object by name an rowId
    var lFnGetDataFromGrid = function () {
        var lRowsSelected = $("#flashEditSegmentsGrid").jqxGrid('selectedrowindexes');
        var lSegmentsName = new Array();
        var lSelectedRecords = new Array();
        for (var i = 0; i < lRowsSelected.length; i++) {
            var lRow = $("#flashEditSegmentsGrid").jqxGrid('getrowdata', lRowsSelected[i]);
            lSelectedRecords[lSelectedRecords.length] = lRow.ID;
            lSegmentsName[lSegmentsName.length] = '<li>' + lRow.FlashSegmentName + '</li>'
        }
        return {
            SegmentsName: lSegmentsName,
            SelectedRecords: lSelectedRecords
        };

    }
    
    var fnSegmentsChoose = function () {
        var lSegments = lFnGetDataFromGrid().SegmentsName;
        return lSegments.join(" ");
    }
    
    var lRowsIds = lFnGetDataFromGrid().SelectedRecords.join(", ");
    
    if (objRowSelected) {
        var htmlContentModal = "<b><ol>" + fnSegmentsChoose(); + "</ol></b>";
        var src = "/FlashTool/FlashEditSegments/";
        _showModal({
            width: '35%',
            modalId: "modalDel",
            addCloseButton: true,
            buttons: [{
                name: "Delete",
                class: "btn-danger",
                onClick: function () {
                    //Save change to get the ID
                    _callProcedure({
                        loadingMsgType: "fullLoading",
                        loadingMsg: "Deleting Segments '" + lRowsIds + "'...",
                        name: "[dbo].[spFlashDisableFlashSegment]",
                        params: [
                            { "Name": "@pFlashSegmentID", "Value": lRowsIds }
                            //{ "Name": "@SessionSOEID", "Value": _getSOEID() },
                           
                        ],
                        success: {
                            fn: function (response) {
                                _showNotification("success", "The Segment '" + fnSegmentsChoose() + "' was deleted successfully.");
                                //Fn To reload de data
                                fnLoadFlashCustomSegments();
                            }
                        }
                    });
                }
            }],
            title: "Are you sure you want to delete this Segments?",
            contentHtml: htmlContentModal
        });
    }
}
//Fn to Load all the flash Segments
function fnLoadFlashCustomSegments() {
    
    $.jqxGridApi.create({
        showTo: "#flashEditSegmentsGrid",
        options: {
            //for comments or descriptions
            height: "400px",
            width: "600px",
            autoheight: true,
            autorowheight: false,
            selectionmode: "multiplerows",
            showfilterrow: true,
            sortable: true,
            editable: true
            //'singlerow', 'multiplerows', 'checkbox', 'multiplerowsadvanced' or 'multiplerowsextended'.
        },
        sp: {
            Name: "[dbo].[spFlashGetAllFlashSegments]",
            Params: [ ]
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set"
        },
        groups: [],
        columns: [
            { name: 'ID', type: 'int', hidden: true },
            { name: 'FlashSegmentName', text: 'Segment Name', width: '40%', type: 'string', editable: false },
            { name: 'CreatedBy', text: 'Create By', width: '25%', type: 'string', editable: false },
            { name: 'CreatedDate', text: 'Creation Date', width: '35%', type: 'string', cellsformat: 'D' },    
        ],
        ready: function () {
            $("#flashEditSegmentsGrid").on('rowselect', function (event) {
                $("#sendParameter").attr("href", _getViewVar("SubAppPath") + "/FlashTool/FlashEditSegments?ID=" + event.args.row.ID +"&name="+ event.args.row.FlashSegmentName);
            });

        }
    });
}

_showLoadingFullPage({
    msg: "Test Loading"
});

//button click
_hideLoadingFullPage()


