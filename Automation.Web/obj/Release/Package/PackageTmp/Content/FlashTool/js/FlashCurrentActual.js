//TODO: Put in a global JS
var businessDay;
var BDStart;
var BDEnd;

function fnLoadFlashCustomSegments() {
    $.jqxGridApi.create({
        showTo: "#flashSegmentsGrid",
        options: {
            width: "470px",
            autoheight: true,
            autorowheight: true,
            //selectionmode: "multiplerows",
            showfilterrow: true,
            sortable: true,
            editable: true
            //'singlerow', 'multiplerows', 'checkbox', 'multiplerowsadvanced' or 'multiplerowsextended'.
        },
        sp: {
            Name: "[dbo].[spFlashGetAllFlashSegments]",
            Params: []
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set"
        },
        groups: [],
        columns: [
            { name: 'ID', type: 'int', hidden: true },
            { name: 'FlashSegmentName', text: 'Segment Name', width: '180px', type: 'string', editable: false, cellsalign: 'center', align: 'center' },
            { name: 'CreatedBy', text: 'Created By', width: '100px', type: 'string', editable: false, cellsalign: 'center', align: 'center' },
            { name: 'CreatedDate', text: 'Creation Date', width: '180px', type: 'string', cellsformat: 'D', cellsalign: 'center', align: 'center' },
        ],
        ready: function () {
            $("#flashSegmentsGrid").on('rowselect', function (event) {
                $("#flashbtn").prop("disabled", false);
                $("#actualBtn").prop("disabled", false);
                //$("#sendParameter").attr("href", _getViewVar("SubAppPath") + "/FlashTool/FlashCategoryDetail?ID=" + event.args.row.ID + "&name=" + event.args.row.FlashSegmentName);
                $("#sendBridgeParameter").attr("href", _getViewVar("SubAppPath") + "/FlashTool/FlashCurrentActual?ID=" + event.args.row.ID + "&name=" + event.args.row.FlashSegmentName);
            });

        }
    });
}
function fnGoToSegments() {
    fnCheckBD();
    var htmlContentModal = 'Choose at least one segment';
    var formHtml =
                '<div class="content-body">' +
                    '<div class="center">' +
                        '<div id="flashSegmentsGrid"></div>' +
                        '<a id="sendBridgeParameter">' +
                            '<button type="button" id="actualBtn" class="btnCustomize btn btn-primary bottom15 pull-right"><i class="fa fa-external-link"></i> Actuals Monitoring</button>' +
                        '</a>' +
                    '</div>' +
                '</div>';
    fnLoadFlashCustomSegments();
    _showModal({
        modalId: "modalUpload",
        width: '40%',
        //buttons: [{
        //    name: "<i class='fa fa-save'></i> OK",
        //    class: "btn-success",
        //    closeModalOnClick: true,
        //    onClick: function ($modal) {
        //        //Validate required Comment
        //        window.location.href = _getViewVar("SubAppPath") + '/FlashTool/FlashCurrentActual';
        //    }
        //}],
        addCloseButton: false,
        title: "Message: ",
        contentHtml: formHtml,
        onReady: function ($modal) { }
    });

}
function fnCheckBD() {
    _callProcedure({
        loadingMsgType: "Loading business day",
        loadingMsg: "Loading...",
        name: "[dbo].[spFlashCheckBusinessDay]",
        params: [

        ],
        success: {
            fn: function (responseList) {
                businessDay = responseList;
                fnCheckBDFromArray(businessDay);
            }
        },
    });
}
function fnCheckBDFromArray(businessDay) {
    var currentDate = fnGetDate();
    businessDay.map(function (obj, index) {
        if (obj.BusinessDay == "1") {
            BDStart = obj.WorkDay.split(' ')[0];
        }
        if (obj.BusinessDay == "10") {
            BDEnd = obj.WorkDay.split(' ')[0];
        }
        
    }).filter(isFinite)
    console.log(BDStart)
    console.log(BDEnd);
    if (Date.parse(currentDate) > Date.parse(BDEnd) && Date.parse(currentDate) < Date.parse(BDStart)) {
        _showAlert({
            id: "MessageBDValidation",
            showTo: $("#flashSegmentsGrid").parent(),
            type: 'info',
            title: "Message",
            content: "Data is only processed between BD1 and BD10"
        });
        
    } else {
        _showAlert({
            id: "MessageBDValidation",
            showTo: $("#flashSegmentsGrid").parent(),
            type: 'error',
            title: "Message",
            content: "Data is only processed between BD10 and BD1"
        });
        //document.getElementById("actualBtn").disabled = true;
    }
}
$(document).ready(function () {
    $("#editComment").click(function () {
        showModalAddCommentEdit();
    });    $("#SaveMonitoring").click(function () {
        fnSaveMonitoring();
    });
    LoadActualsMonitoring();
    

    //$("#contenttabletblFileTable").append('<button type="submit" class="btn btn-success">Save</button>');
});
//Fn to get Date
function fnGetDate() {
    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();
    if (dateObj.getDay() == 1) {
        lYesterday = day - 3;
        console.log("Es Lunes " + year + "-" + month + "-" + lYesterday);
        return gTodayDate = year + "-" + month + "-" + lYesterday;
    }
    console.log("no es lunes " + year + "-" + month + "-" + day);
    return gTodayDate = year + "-" + month + "-" + day;
}
function getDate(lVal) {
    var lDate = new Date();
    var lMonth = lDate.getMonth() + 1;
    if (lVal == 'm') {
        return lMonth
    }
    if (lVal == 'y') {
        return lDate.getFullYear()
    }
}
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
if (typeof getParameterByName('ID') === 'undefined' || !getParameterByName('ID')) {
    console.log('Entre');
    fnGoToSegments();
}
function LoadActualsMonitoring() {
    $("#nameOfSegment").html( getParameterByName('name'));
 
    var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties, data) {

        if (columnfield == 'today') {
            if (value == "") {
                value = 0;
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(value, "c2"); + '</span></div>';
            } else {
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(value, "c2"); + '</span></div>';
            }
        }
        if (columnfield == 'CurrentPrior') {
            if (value == "") {
                value = 0;
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(value, "c2"); + '</span></div>';
            } else {
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(value, "c2"); + '</span></div>';
            }
        }
        if (columnfield == 'PendingEntries') {
            if (value == "") {
                value = 0;
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(value, "c2"); + '</b></span></div></center>';
            } else {
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(value, "c2"); + '</b</span></div></center>';
            }
        } 
        if (columnfield == 'yesterdayPrior') {
            //console.log(data);
            if (value == "") {
                value = 0;
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(value, "c2") + '</b></span></div>';
            } else {
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(value, "c2") + '</b></span></div>';
            }
        }
        if (columnfield == 'PriorActual') {
            //console.log(data);
            if (value == "") {
                value = 0;
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(value, "c2") + '</b></span></div>';
            } else {
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(value, "c2") + '</b></span></div>';
            }
        }

    }
    if (typeof getParameterByName('ID') === 'undefined' || !getParameterByName('ID')) {
        console.log('vacio');
    } else {
        $.jqxGridApi.create({
            showTo: "#ActualsMonitoring",
            options: {
                sortable: true,
                editable: true,
                showstatusbar: true,
                statusbarheight: 25,
                showaggregates: true,

            },
            sp: {
                Name: "[dbo].[spFlashGetSegmentsByFlashLevelACTUALSMONITORING]",
                Params: [
                        { Name: "@pIdSegment", Value: getParameterByName('ID') },
                ]
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                dataBinding: "Large Data Set"
               
            },
            columns: [
                //columna segment ID
                 {
                     name: 'FlashAccount', text: 'Segment', width: '25%', type: 'string', filtertype: 'input', filterable: true, cellsalign: 'center', align: 'center',
                     editable: false, pinned: true, 
                 },
                 {
                     name: 'today', text: 'Flash', width: '15%', type: 'string', filtertype: 'input', filterable: true, cellsalign: 'right', align: 'Center',
                     editable: false, cellsrenderer: cellsrenderer,
                     cellsformat: 'c2', aggregates: [{
                         '<b id="priorActual">Total':
                         function (aggregatedValue, currentValue, column, record) {
                             var total = $("#ActualsMonitoring").jqxGrid('getcolumnaggregateddata', 'today', ['sum']);
                             sum = total.sum
                             this.priorActual = sum;
                             return sum;
                         }
                     }],
                 },
                 //actual del mes anterior esta en el flash detail
                {
                    name: 'PriorActual', datafield: 'PriorActual', text: 'Prior Month', width: '15%', type: 'number', filtertype: 'input',
                    editable: false, cellsalign: 'right', align: 'Center', cellsrenderer: cellsrenderer,
                    cellsformat: 'c2', aggregates: [{
                        '<b id="priorActual">Total':
                        function (aggregatedValue, currentValue, column, record) {
                            var total = $("#ActualsMonitoring").jqxGrid('getcolumnaggregateddata', 'PriorActual', ['sum']);
                            sum = total.sum
                            this.priorActual = sum;
                            return sum;
                        }
                    }],


                },
                 //Actual del dia anterior este esta en pearl--- bd2 primer dia va a estar en blanco
                 {
                     name: 'yesterdayPrior', text: 'Yesterday', width: '16%', type: 'string', filtertype: 'input', filterable: true, cellsalign: 'right', align: 'center',
                     editable: false, cellsrenderer: cellsrenderer,
                     cellsformat: 'c2', aggregates: [{
                         '<b id="priorActual">Total':
                         function (aggregatedValue, currentValue, column, record) {
                             var total = $("#ActualsMonitoring").jqxGrid('getcolumnaggregateddata', 'yesterdayPrior', ['sum']);
                             sum = total.sum
                             this.priorActual = sum;
                             return sum;
                         }
                     }],
                 },
                 //reporte de balance de pearl de el dia se corre a medio dia
                 {
                     name: 'CurrentPrior', text: 'Current', width: '19%', type: 'string', filtertype: 'input', filterable: true, cellsalign: 'right', align: 'center',
                     editable: false, cellsrenderer: cellsrenderer,
                     cellsformat: 'c2', aggregates: [{
                         '<b id="priorActual">Total':
                         function (aggregatedValue, currentValue, column, record) {
                             var total = $("#ActualsMonitoring").jqxGrid('getcolumnaggregateddata', 'CurrentPrior', ['sum']);
                             sum = total.sum
                             this.priorActual = sum;
                             return sum;
                         }
                     }],
                 },
                 //fourmula = current - yesterday
                 {
                     name: 'IDPreviousDayActuals', text: 'DoD Variance', width: '17%', type: 'Number', filtertype: 'input', filterable: true, cellsalign: 'right', align: 'center',
                     editable: false, cellsformat: 'c2', aggregates: ['sum'],
                     cellsrenderer:
                        function (index, datafield, value, defaultvalue, column, rowdata) {
                            if (rowdata.CurrentPrior == null || rowdata.CurrentPrior === "" ) {
                                rowdata.CurrentPrior = 0;
                            }
                            if (rowdata.yesterdayPrior == null || rowdata.yesterdayPrior === "" || rowdata.yesterdayPrior === null) {
                                rowdata.yesterdayPrior = 0;
                            }
                            //console.log('Current' + rowdata.CurrentPrior);
                            //console.log('yesterday' + rowdata.yesterdayPrior);
                            var total = parseFloat(rowdata.CurrentPrior) - parseFloat(rowdata.yesterdayPrior);
                            //console.log(rowdata);

                            if (rowdata.IDPreviousDayActuals != total) {
                                //console.log(rowdata);
                                //$("#ActualsMonitoring").jqxGrid('setcellvalue', rowdata.uid, "IDPreviousDayActuals", total);
                            }
                            //Base de datos no se valido isNull
                            return "<div style='margin: 4px;' class='jqx-right-align'>" + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(total, "c2"); + "</div>";
                        },
                    aggregates: [{
                        '<b>Total</b>':
                                function (aggregatedValue, currentValue, column, rowdata) {
                                    if (rowdata.CurrentPrior == null || rowdata.CurrentPrior === "") {
                                        rowdata.CurrentPrior = 0;
                                    }
                                    if (rowdata.yesterdayPrior == null || rowdata.yesterdayPrior === "" || rowdata.yesterdayPrior === null) {
                                        rowdata.yesterdayPrior = 0;
                                    }
                                    var total = parseFloat(rowdata.CurrentPrior) - parseFloat(rowdata.yesterdayPrior);
                                    //console.log(rowdata);
                                    //var aggregatedValue = isNaN(parseInt(aggregatedValue)) ? 0 : parseInt(aggregatedValue)
                                    //console.log(aggregatedValue);
                                    if (isNaN(aggregatedValue)) {
                                        aggregatedValue = 0;
                                    }
                                    var lShowTotal = aggregatedValue + total;
                                    return lShowTotal;//$.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(lShowTotal, "c2");
                                }
                    }]
                 },
                 //editable
                 {
                     name: 'PendingEntries', text: 'Pending', width: '13%', type: 'Number', filtertype: 'input', filterable: true, cellsalign: 'right', align: 'center',
                     cellsrenderer: cellsrenderer, aggregates: ['sum'], cellsformat: 'c2'
                 },
                 //ExpectedActuals = actualPearl + PendingEntries es igual al expected
                 {
                     name: 'expectedActuals', datafield: 'Expected', text: 'Expected', width: '17%', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center',
                     editable: false, aggregates: ['sum'], cellsformat: 'c2', cellsrenderer:
                     function (index, datafield, value, defaultvalue, column, rowdata) {
                         if (rowdata.CurrentPrior == null || rowdata.CurrentPrior === "") {
                             rowdata.CurrentPrior = 0;
                         }
                         if (rowdata.PendingEntries == null || rowdata.PendingEntries === "") {
                             rowdata.PendingEntries = 0;
                         }
                         //var lMonth = parseFloat(rowdata.today) + parseFloat(rowdata.Accrual) + parseFloat(rowdata.UnpostedCanada) + parseFloat(rowdata.APS);
                         var total = parseFloat(rowdata.CurrentPrior) + parseFloat(rowdata.PendingEntries);
                         total = total || 0;
                         return "<div style='margin: 4px;' class='jqx-right-align'>" + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(total, "c2"); + "</div>";
                     },
                     aggregates: [{
                         '<b>Total</b>':
                                 function (aggregatedValue, currentValue, column, rowdata) {
                                     if (rowdata.CurrentPrior == null || rowdata.CurrentPrior === "") {
                                         rowdata.CurrentPrior = 0;
                                     }
                                     if (rowdata.PendingEntries == null || rowdata.PendingEntries === "") {
                                         rowdata.PendingEntries = 0;
                                     }
                                     //var lMonth = parseFloat(rowdata.today) + parseFloat(rowdata.Accrual) + parseFloat(rowdata.UnpostedCanada) + parseFloat(rowdata.APS);
                                     var total = parseFloat(rowdata.CurrentPrior) + parseFloat(rowdata.PendingEntries);
                                     total = total || 0;
                                     
                                     //console.log(rowdata);
                                     //var aggregatedValue = isNaN(parseInt(aggregatedValue)) ? 0 : parseInt(aggregatedValue)
                                     //console.log(aggregatedValue);
                                     if (isNaN(aggregatedValue)) {
                                         aggregatedValue = 0;
                                     }
                                     var lShowTotal = aggregatedValue + total;
                                     return lShowTotal;//$.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(lShowTotal, "c2");
                                 }
                     }]
                 },
                 //expected - currentflash (# de flash)
                 {
                     name: 'IDPearlActualVsFlash', text: 'Actuals vs Flash', width: '13%', type: 'string', filtertype: 'input', filterable: true, cellsalign: 'right', align: 'center',
                     aggregates: ['sum'], cellsformat: 'c2', editable: false,
                     cellsrenderer:
                     function (index, datafield, value, defaultvalue, column, rowdata) {
                         if (rowdata.expectedActuals == null || rowdata.expectedActuals === "") {
                             rowdata.expectedActuals = 0;
                         }
                         if (rowdata.today == null || rowdata.today === "") {
                             rowdata.today = 0;
                         }
                        
                         var total = parseFloat(rowdata.expectedActuals) - parseFloat(rowdata.today);
                         //if (rowdata.IDPreviousDayActuals != total) {
                         //    $("#ActualsMonitoring").jqxGrid('setcellvalue', rowdata.uid, "IDPreviousDayActuals", total);
                         //}
                         total = total || 0;
                         return "<div style='margin: 4px;' class='jqx-right-align'>" + $.jqxGridApi.localStorageFindById("#ActualsMonitoring").dataAdapter.formatNumber(total, "c2"); + "</div>";
                     },
                     aggregates: [{
                         '<b>Total</b>':
                                 function (aggregatedValue, currentValue, column, rowdata) {
                                     if (rowdata.expectedActuals == null || rowdata.expectedActuals === "") {
                                         rowdata.expectedActuals = 0;
                                     }
                                     if (rowdata.today == null || rowdata.today === "") {
                                         rowdata.today = 0;
                                     }

                                     var total = parseFloat(rowdata.expectedActuals) - parseFloat(rowdata.today);
                                     total = total || 0;

                                     //console.log(rowdata);
                                     //var aggregatedValue = isNaN(parseInt(aggregatedValue)) ? 0 : parseInt(aggregatedValue)
                                     //console.log(aggregatedValue);
                                     if (isNaN(aggregatedValue)) {
                                         aggregatedValue = 0;
                                     }
                                     var lShowTotal = aggregatedValue + total;
                                     return lShowTotal;//$.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(lShowTotal, "c2");
                                 }
                     }]
                 },
                {
                    name: 'Comment', width: '80px', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center', editable: false, cellsrenderer: function (rowIndex, datafield, value) {
                        var dataRecord = $("#ActualsMonitoring").jqxGrid('getrowdata', rowIndex);
                        //console.log(dataRecord.Comment);
                        var htmlComment = "<div style='padding: 10px; font-size: 15px; max-height: 500px; overflow-y:scroll; width: 520px;'>";
                        htmlComment += "<b>Comment:</b> " + dataRecord.Comment + " <br>";
                        htmlComment += "</div>";
                        var htmlResult = "";

                        if (dataRecord.Comment) {
                            htmlResult +=
                                '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + htmlComment + '" data-title="Comments History" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png"/> ' +
                                '</div>';
                        }
                        return htmlResult;
                    }
                },
            ],


            ready: function () {
                $("#ActualsMonitoring").on("rowclick", function (event) {
                    _GLOBAL_SETTINGS.tooltipsPopovers();
                });
            }

        });
    }

}

_showLoadingFullPage({
    msg: "Loading Actuals Monitoring"
});
function fnSaveMonitoring() {

    var rowData = $.jqxGridApi.getOneSelectedRow('#ActualsMonitoring', true);
    if (rowData) {
        var lPriorActual = rowData["PriorActual"];
        var lPending = typeof rowData["PendingEntries"] === 'undefined' ? 0 : rowData["PendingEntries"];
        var FlashAccount = rowData["FlashAccount"]

        var htmlContentModal = '';
        htmlContentModal += "<b>Do you want to save this record? </b> ";
        _showModal({
            modalId: "modalUpload",
            width: '70%',
            buttons: [{
                name: "<i class='fa fa-save'></i> Save",
                class: "btn-success",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    //Add Comments
                    fnSaveMonitoringToDB(lPriorActual, getParameterByName('name'),lPending, FlashAccount, function () {
                        _showNotification("success", "Actuals Monitoring data was save successfully", "AlertReject");
                        //Close Modal
                        $modal.find(".close").click();
                    });
                }
            }],
            addCloseButton: true,
            title: "Save Flash template",
            contentHtml: htmlContentModal,
            onReady: function ($modal) { }
        });
    } else {
        fnLoadFlashFile();
    }

}
function fnSaveMonitoringToDB(lPriorActual, MSDESCR, lPending, FlashAccount, onSuccess) {

    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Saving data...",
        name: "[dbo].[spFlashSaveActualsMonitoring]",
        params: [
            { "Name": "@pPriorActual", "Value": lPriorActual },

            { "Name": "@pSegmentId", "Value": getParameterByName('ID') },
            { "Name": "@pMSDesccr", "Value": MSDESCR },
            { "Name": "@pPending", "Value": lPending },
            { "Name": "@pFlashExpenseAccount", "Value": FlashAccount },

            { "Name": "@pMonth", "Value": getDate('m') },
            { "Name": "@pYear", "Value": getDate('y') },
            { "Name": "@pSessionSOEID", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                _showNotification("success", "Data was added successfully.", "AlertAddComment");
                onSuccess();
            }
        }
    });

}

function showModalAddCommentEdit() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#ActualsMonitoring', true);
    if (rowData) {
        var lPriorActual = rowData["PriorActual"];
        var lPending = typeof rowData["PendingEntries"] === 'undefined' ? 0 : rowData["PendingEntries"];
        var FlashAccount = rowData["FlashAccount"];
        var lComment = rowData["Comment"];
        if (lComment == 'undefined') {
            lComment = '';
        }
        var htmlContentModal = '';
        htmlContentModal += "<b>Description: </b>" + rowData["FlashAccount"] + "<br/>";
        htmlContentModal += "<b>Pending: </b>" + lPending + "<br/>";
        htmlContentModal += "<b>Comments: </b><br/>";
        htmlContentModal += "<textarea class='form-control txtDetailComment' placeholder='(optional)' style='height:100px !important;'>" + lComment + "</textarea>";

        _showModal({
            modalId: "modalUpload",
            width: '70%',
            buttons: [{
                name: "<i class='fa fa-save'></i> Save",
                class: "btn-success",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    //Add Comments
                    lComment = $(".txtDetailComment").val();

                    addDetailComment(lPriorActual, getParameterByName('name'),FlashAccount, lPending, lComment, function () {
                        _showNotification("success", "Actuals monitoring data was save successfully", "AlertReject");
                        rowData["PendingEntries"] = lPending;
                        rowData["Comment"] = lComment;
                        $('#ActualsMonitoring').jqxGrid('updaterow', rowData.boundindex, rowData);
                        //Close Modal
                        $modal.find(".close").click();
                    });
                }
            }],
            addCloseButton: true,
            title: "Add Comment",
            contentHtml: htmlContentModal,
            onReady: function ($modal) { }
        });
    }

}

function addDetailComment(lPriorActual, MSDESCR, FlashAccount, lPending, Comment, onSuccess) {
    
    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Adding extra file comments...",
        name: "[dbo].[spFlashInsertActualsMonitoringComment]",
        params: [
            { "Name": "@pAction", "Value": "Insert" },
            { "Name": "@pPriorActual", "Value": lPriorActual },
            { "Name": "@pSegmentId", "Value": getParameterByName('ID') },
            { "Name": "@pMSDesccr", "Value": MSDESCR },
            { "Name": "@pFlashExpenseAccount", "Value": FlashAccount }, 
            { "Name": "@pPending", "Value": lPending },
            { "Name": "@pComment", "Value": Comment },
            { "Name": "@pMonth", "Value": getDate('m') },
            { "Name": "@pYear", "Value": getDate('y') },
            { "Name": "@pSessionSOEID", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                //Return New ID
                _showNotification("success", "Data was added successfully.", "AlertAddComment");

                //$modal.find(".closeModal-modalUpload").click();
                //insertToFlashBridge();
                //
                onSuccess();
            }
        }
    });

}
//button click
_hideLoadingFullPage()


