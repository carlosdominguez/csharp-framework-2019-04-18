$(document).ready(function () {
    $("#flashbtn").prop("disabled", true);
    $("#bridgeBtn").prop("disabled", true);  
    fnLoadFlashCustomSegments();
});

function fnLoadFlashCustomSegments() {
    
    $.jqxGridApi.create({
        showTo: "#flashCategory",
        options: {
            width: "470px",
            autoheight: true,
            autorowheight: true,
            //selectionmode: "multiplerows",
            showfilterrow: true,
            sortable: true,
            editable: true
            //'singlerow', 'multiplerows', 'checkbox', 'multiplerowsadvanced' or 'multiplerowsextended'.
        },
        sp: {
            Name: "[dbo].[spFlashGetAllFlashSegments]",
            Params: [ ]
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set"
        },
        groups: [],
        columns: [
            { name: 'ID', type: 'int', hidden: true },
            { name: 'FlashSegmentName', text: 'Segment Name', width: '180px', type: 'string', editable: false, cellsalign: 'center', align: 'center' },
            { name: 'CreatedBy', text: 'Created By', width: '100px', type: 'string', editable: false, cellsalign: 'center', align: 'center' },
            { name: 'CreatedDate', text: 'Creation Date', width: '180px', type: 'string', cellsformat: 'D', cellsalign: 'center', align: 'center' }, 
        ],
        ready: function () {
            $("#flashCategory").on('rowselect', function (event) {
                $("#flashbtn").prop("disabled", false);
                $("#bridgeBtn").prop("disabled", false);
                $("#sendParameter").attr("href", _getViewVar("SubAppPath") + "/FlashTool/FlashCategoryDetail?ID=" + event.args.row.ID + "&name=" + event.args.row.FlashSegmentName);
                $("#sendBridgeParameter").attr("href", _getViewVar("SubAppPath") + "/FlashTool/FlashBridge?ID=" + event.args.row.ID + "&name=" + event.args.row.FlashSegmentName);
            });

        }
    });
}
_showLoadingFullPage({
    msg: "Test Loading"
});

//button click
_hideLoadingFullPage()


