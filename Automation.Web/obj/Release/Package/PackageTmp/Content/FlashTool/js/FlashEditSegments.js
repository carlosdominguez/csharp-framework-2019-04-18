$(document).ready(function () {

    //$("#contenttabletblFileTable").append('<button type="submit" class="btn btn-success">Save</button>');
    fnLoadFromIdFlashCustomSegments();
    $("#addNewSegments").click(function () {
        fnCustomizeSegment();
    });
    //select levels to customize
    $(".dropdown-menu li a").click(function () {
        var selText = $(this).text();
        $(this).parents('.btn-group').find('.dropdown-toggle').html(selText + ' <span class="caret"></span>');
        //clean div for new data
        $("#column").empty();
        fnCustomShowSegmentsizeSegment(selText);
    });
    $("#lSearch").keyup(function () {
        console.log("Handler for .keyup() called.");
        var input, filter, ul, li, a, i;
        input = document.getElementById('lSearch');
        filter = input.value.toUpperCase();
        ul = document.getElementById("column");
        li = ul.getElementsByTagName('li');

        // Loop through all list items, and hide those who don't match the search query
        for (i = 0; i < li.length; i++) {
            label = li[i].getElementsByTagName("label")[0];
            if (label.innerHTML.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    });
    $("#SaveNewSegments").click(function () {
        var checkedValues = $('input:checkbox:checked').map(function () {
            return this.value;
        }).get();
        //ToDO: Add Modal
        checkedValues.length == 0 ? alert('must check at least one') : fnSaveSelectedSegments(checkedValues);
        console.log(checkedValues);
        
    });
});

//function lShowModal() {
//    var classListRoles = this;
//    //On click in btn add new user
//    $(".btnAddNewRole").click(function () {
//        console.log('click');
//        classListRoles.addNewUser();
//    });
//    //On click in btn edit user
//    $(".btnEditRole").click(function () {
//        classListRoles.editUser();
//    });

//    //On click in btn delete user
//    $(".btnDeleteRole").click(function () {
//        classListRoles.deleteUser();
//    });

//    this.createFormRole = function (options) {
//        var objRole = options.objRole;
//        var formHtml =
//            '<div class="form-horizontal"> ' +
//            '    <div class="col-md-10"> ' +
//            '        <div class="form-group"> ' +
//            '            <label class="col-md-4 control-label"> ' +
//            '                <i class="fa fa-asterisk iconasterisk"></i> Name ' +
//            '            </label> ' +
//            '            <div class="col-md-8"> ' +
//            '               <input type="text" class="txtName form-control" value="' + objRole.Name + '" placeholder="eg. ISSUE_LOG_MANAGER"> ' +
//            '            </div> ' +
//            '        </div> ' +
//            '        <div class="form-group"> ' +
//            '            <label class="col-md-4 control-label"> ' +
//            '                <i class="fa fa-asterisk iconasterisk"></i> Marketplace Code ' +
//            '            </label> ' +
//            '            <div class="col-md-8"> ' +
//            '               <input type="text" class="txtEERSMarketplaceRoleID form-control" value="' + objRole.EERSMarketplaceRoleID + '" placeholder="eg. 165986_2"> ' +
//            '            </div> ' +
//            '        </div> ' +
//            '        <div class="form-group"> ' +
//            '            <label class="col-md-4 control-label"> ' +
//            '                <i class="fa fa-asterisk iconasterisk"></i> Marketplace Name ' +
//            '            </label> ' +
//            '            <div class="col-md-8"> ' +
//            '               <input type="text" class="txtEERSFunctionCode form-control" value="' + objRole.EERSFunctionCode + '" placeholder="eg. Issue Log - Manager"> ' +
//            '            </div> ' +
//            '        </div> ' +
//            '        <div class="form-group"> ' +
//            '            <label class="col-md-4 control-label"> ' +
//            '                <i class="fa fa-asterisk iconasterisk"></i> EERS Function Descrition ' +
//            '            </label> ' +
//            '            <div class="col-md-8"> ' +
//            '               <input type="text" class="txtEERSFunctionDescription form-control" value="' + objRole.EERSFunctionDescription + '" placeholder="eg. [RO] - Users with ready only access"> ' +
//            '            </div> ' +
//            '        </div> ' +
//            '        <div class="form-group"> ' +
//            '            <label class="col-md-4 control-label"> ' +
//            '                <i class="fa fa-asterisk iconasterisk"></i> Ignore Role in EERS? ' +
//            '            </label> ' +
//            '            <div class="col-md-8"> ' +
//            '               <input value="1" type="radio" name="chkFlagEERSIgnore" class="rbtFlagGlobalYes skin-square-green" ' + ((objRole.EERSIgnore == "True") ? "checked" : "") + ' > ' +
//            '               <label class="iradio-label form-label hover" style="margin-right: 25px;">Yes </label> ' +
//            '               <input value="0" type="radio" name="chkFlagEERSIgnore" class="rbtFlagGlobalNo skin-square-green" ' + ((objRole.EERSIgnore == "False") ? "checked" : "") + ' > ' +
//            '               <label class="iradio-label form-label hover" style="margin-right: 25px;">No </label> ' +
//            '            </div> ' +
//            '        </div> ' +
//            '    </div> ' +
//            '    <div style="clear: both;"></div> ' +
//            '</div> ';
//        _showModal({
//            width: "75%",
//            title: "Role Information",
//            contentHtml: formHtml,
//            buttons: [{
//                name: "Save",
//                class: "btn-success",
//                closeModalOnClick: true,
//                onClick: function ($modal) {
//                    objRole.Name = $modal.find(".txtName").val();
//                    objRole.EERSMarketplaceRoleID = $modal.find(".txtEERSMarketplaceRoleID").val();
//                    objRole.EERSFunctionCode = $modal.find(".txtEERSFunctionCode").val();
//                    objRole.EERSFunctionDescription = $modal.find(".txtEERSFunctionDescription").val();
//                    objRole.EERSIgnore = ($modal.find('[name="chkFlagEERSIgnore"]:checked').val() == '1' ? 'True' : 'False');
//                    if (options.onSave) {
//                        options.onSave(objRole);
//                    }
//                }
//            }],
//            addCloseButton: true,
//            onReady: function () {
//                _GLOBAL_SETTINGS.iCheck();
//            }
//        });
//    };
//}
function fnCustomizeSegment() {
    $("#showSegments").show();
    //var formHtml =
    //        '<div class="row"> ' +
    //        '    <div class="col-md-12"> ' +
    //        '        <div class="pull-left"> ' +
    //        '            <Label>Name for segments:</label> ' +
    //        '               <input placeholder="Set The Name" type="text" name="setSegmentName" class="rbtFlagIsDeletedYes skin-square-green"> ' +
    //        '       </div><br/>' +
    //        '    <div class="col-md-12"> ' +
    //        '            <p>Please check the Segments that you need to map to the selected user:</p> ' +
    //        '        <div class="selUserOfRoles"> ' +
    //        '                                   <div>' +
    //        '                                       <input type="checkbox" class="ManagedSegments" name="flashSegments" value="Cyber Security">'+
    //        '                                       <label>Cyber Security</label>' +
    //        '                                   </div>'+  
    //        '                                   <div>' +
    //        '                                       <input type="checkbox" class="ManagedSegments" name="flashSegments" value="Security">' +
    //        '                                       <label>Security</label>' +
    //        '                                   </div>' +
    //        '                                   <div>' +
    //        '                                       <input type="checkbox" class="ManagedSegments" name="flashSegments" value="Cyber Security">' +
    //        '                                       <label>Network</label>' +
    //        '                                   </div>' +
    //        '        </div> ' +
    //        '        <div style="text-align: center; padding-right: 39px; padding-bottom: 8px;" class="pull-right"> ' +
    //        '            Check all <br /> ' +
    //        '            <input type="checkbox" class="skin-square-green checkAllSegments" id="checkAllSegments" name="checkAllSegments" "> ' +
    //        '        </div> ' +
    //        '        </div> ' +
    //        '        <div style="clear:both;"></div> ' +
    //        '    </div> ' +
    //                    '        <p>Please select the user to grand access:</p> ' +
    //        '        <div class="selUserOfRoles"> ' +
    //        '                                   <select>' +
    //        '                                       <option value="a">RE65657</option>' +
    //        '                                       <option value="b">RE65658</option>' +
    //        '                                       <option value="c">RE65659</option>' +
    //        '                                   </select> ' +
    //        '        </div> ' +
    //        //'    <div class="col-md-12"> ' +
    //        //'        <h3>Is Active?</h3> ' +
    //        //'        <p>User was deleted or not deleted:</p> ' +
    //        //'        <div class="form-group"> ' +
    //        //'           <input value="1" type="radio" name="chkFlagIsDeleted" class="rbtFlagIsDeletedYes skin-square-green" ' + ((objUser.IsDeleted) ? "checked" : "") + ' > ' +
    //        //'           <label class="iradio-label form-label hover" style="margin-right: 25px;">Deleted </label> ' +
    //        //'           <input value="0" type="radio" name="chkFlagIsDeleted" class="rbtFlagIsDeletedNo skin-square-green" ' + ((objUser.IsDeleted == false) ? "checked" : "") + ' > ' +
    //        //'           <label class="iradio-label form-label hover" style="margin-right: 25px;">Not Deleted </label> ' +
    //        //'        </div> ' +
    //        //'    </div> ' +
    //        '</div> ';
   
    //_showModal({
    //    width: "75%",
    //    title: "Customize Segments",
    //    contentHtml: formHtml,

    //    buttons: [{
    //        name: "Save",
    //        class: "btn-success",
    //        closeModalOnClick: false,
    //        onClick: function ($modal) {
                
    //        }
    //    }],
    //    addCloseButton: true,
    //    onReady: function ($modal) {
    //        $modal.find("#checkAllSegments").click(function () {
    //            $("input:checkbox").prop("checked", this.checked);
    //        });
    //    }

    //});
}
function AddUser() {
    var formHtml =
            '<div class="container">'+
                '<div class="row">'+
                    '<div class="col col-lg-2"><div>SOEID</div>'+
                    '   <div>RE65657</div>' +
                    '   <div>RE65658</div>' +
                    '   <div>RE65659</div>' +
                    '</div>' +
                    '<div class="col col-lg-2"><div>Segments</div>' +
                    '   <div>Network L9</div>' +
                    '   <div>Cyber Security</div>' +
                    '   <div>Cyber Security & Networks</div>' +
                    '</div>' +
                    '<div class="col col-lg-2">3 of three columns</div>' +
                '</div>'+
            '</div>'+
            '<div class="row"> ' +
            '    <div class="col-md-12"> ' +
            //'        <h3>Select User</h3> ' +
            //'        <p>Users</p> ' +
            '        <div class="selUserOfRoles"> ' +   

            '        </div> ' +
            '    <div class="col-md-12"> ' +
            '        <div class="pull-left"> ' +
            //'            <h3>Set Roles</h3> ' +
            '            <p>Please check the Segments that you need to map to the selected user:</p> ' +
            '        <div class="selUserOfRoles"> ' +   
            '                                   <select>' +
            '                                       <option value="volvo">Cyber Security</option>' +
            '                                       <option value="volvo">Network</option>' +
            '                                       <option value="volvo">Cyber Security & Networks</option>' +
            '                                   </select> ' +
            '        </div> ' +
            '        </div> ' +
            '        <div style="text-align: center; padding-right: 39px; padding-bottom: 8px;" class="pull-right"> ' +
            '            Check all <br /> ' +
            '            <input type="checkbox" class="skin-square-green checkAllUserXRoles"> ' +
            '        </div> ' +
            '        <div style="clear:both;"></div> ' +
            //'        <div class="well transparent" id="roleCategories"></div> ' +
            '    </div> ' +
            //'    <div class="col-md-12"> ' +
            //'        <h3>Is Active?</h3> ' +
            //'        <p>User was deleted or not deleted:</p> ' +
            //'        <div class="form-group"> ' +
            //'           <input value="1" type="radio" name="chkFlagIsDeleted" class="rbtFlagIsDeletedYes skin-square-green" ' + ((objUser.IsDeleted) ? "checked" : "") + ' > ' +
            //'           <label class="iradio-label form-label hover" style="margin-right: 25px;">Deleted </label> ' +
            //'           <input value="0" type="radio" name="chkFlagIsDeleted" class="rbtFlagIsDeletedNo skin-square-green" ' + ((objUser.IsDeleted == false) ? "checked" : "") + ' > ' +
            //'           <label class="iradio-label form-label hover" style="margin-right: 25px;">Not Deleted </label> ' +
            //'        </div> ' +
            //'    </div> ' +
            '</div> ';
    _showModal({
        width: "75%",
        title: "User Information",
        contentHtml: formHtml,
        buttons: [{
            name: "Save",
            class: "btn-success",
            closeModalOnClick: false,
            onClick: function ($modal) {
                objUser.SOEID = $modal.find(".selUserOfRoles select").val();

                if (objUser.SOEID) {
                    //objUser.IsDeleted = ($modal.find('[name="chkFlagIsDeleted"]:checked').val() == '1' ? 'True' : 'False');

                    //Saves roles of user
                    objUserXRoles.saveUserXRoles();

                    //On Complete ajaxs reload list
                    _execOnAjaxComplete(function () {

                        //Save IsDeleted and Create User
                        if (options.onSave) {
                            options.onSave(objUser);
                        }

                        //Close Modal
                        $modal.find("[data-dismiss='modal']").click();
                    });
                } else {
                    _showNotification("error", "Please select an employee", "ErrorMissingEmp");
                }
            }
        }],
        addCloseButton: true,

    });
}
function EditUser() {
    var formHtml =
            '<div class="row"> ' +
            '    <div class="col-md-12"> ' +
            '        <div class="pull-left"> ' +
            '            <Label>Name for segments:</label> ' +
            '               <input placeholder="Set The Name" type="text" name="setSegmentName" class="rbtFlagIsDeletedYes skin-square-green"> ' +
            '       </div><br/>' +
            '    <div class="col-md-12"> ' +
            '            <p>Please check the Segments that you need to map to the selected user:</p> ' +
            '        <div class="selUserOfRoles"> ' +
            '                                   <div>' +
            '                                       <input type="checkbox" class="ManagedSegments" name="flashSegments" value="Cyber Security" checked>' +
            '                                       <label>Cyber Security</label>' +
            '                                   </div>' +
            '                                   <div>' +
            '                                       <input type="checkbox" class="ManagedSegments" name="flashSegments" value="Security">' +
            '                                       <label>Security</label>' +
            '                                   </div>' +
            '                                   <div>' +
            '                                       <input type="checkbox" class="ManagedSegments" name="flashSegments" value="Cyber Security">' +
            '                                       <label>Network</label>' +
            '                                   </div>' +
            '                                   <div>' +
            '                                       <input type="checkbox" class="ManagedSegments" name="flashSegments" value="Cyber Security">' +
            '                                       <label>Distributed Platform Admin [L9]</label>' +
            '                                   </div>' +
            '                                   <div>' +
            '                                       <input type="checkbox" class="ManagedSegments" name="flashSegments" value="Cyber Security" checked>' +
            '                                       <label>GCB Core Infra Service [L8]</label>' +
            '                                   </div>' +
            '        </div> ' +
            '        <div style="text-align: center; padding-right: 39px; padding-bottom: 8px;" class="pull-right"> ' +
            '            Check all <br /> ' +
            '            <input type="checkbox" class="skin-square-green checkAllSegments" id="checkAllSegments" name="checkAllSegments" "> ' +
            '        </div> ' +
            '        </div> ' +
            '        <div style="clear:both;"></div> ' +
            '    </div> ' +
                        '        <p>Please select the user to grand access:</p> ' +
            '        <div class="selUserOfRoles"> ' +
            '                                   <select>' +
            '                                       <option value="a">RE65657</option>' +
            '                                       <option value="b" selected>RE65658</option>' +
            '                                       <option value="c">RE65659</option>' +
            '                                   </select> ' +
            '        </div> ' +
            //'    <div class="col-md-12"> ' +
            //'        <h3>Is Active?</h3> ' +
            //'        <p>User was deleted or not deleted:</p> ' +
            //'        <div class="form-group"> ' +
            //'           <input value="1" type="radio" name="chkFlagIsDeleted" class="rbtFlagIsDeletedYes skin-square-green" ' + ((objUser.IsDeleted) ? "checked" : "") + ' > ' +
            //'           <label class="iradio-label form-label hover" style="margin-right: 25px;">Deleted </label> ' +
            //'           <input value="0" type="radio" name="chkFlagIsDeleted" class="rbtFlagIsDeletedNo skin-square-green" ' + ((objUser.IsDeleted == false) ? "checked" : "") + ' > ' +
            //'           <label class="iradio-label form-label hover" style="margin-right: 25px;">Not Deleted </label> ' +
            //'        </div> ' +
            //'    </div> ' +
            '<script>' +
            '   $("#checkAllSegments").click(function(){ ' +
            '                                           $("input:checkbox").prop("checked", this.checked);' +
            '   });' +
            '</script>' +
            '</div> ';
    _showModal({
        width: "75%",
        title: "User Information",
        contentHtml: formHtml,
        buttons: [{
            name: "Save",
            class: "btn-success",
            closeModalOnClick: false,
            onClick: function ($modal) {
                objUser.SOEID = $modal.find(".selUserOfRoles select").val();

                if (objUser.SOEID) {
                    //objUser.IsDeleted = ($modal.find('[name="chkFlagIsDeleted"]:checked').val() == '1' ? 'True' : 'False');

                    //Saves roles of user
                    objUserXRoles.saveUserXRoles();

                    //On Complete ajaxs reload list
                    _execOnAjaxComplete(function () {

                        //Save IsDeleted and Create User
                        if (options.onSave) {
                            options.onSave(objUser);
                        }

                        //Close Modal
                        $modal.find("[data-dismiss='modal']").click();
                    });
                } else {
                    _showNotification("error", "Please select an employee", "ErrorMissingEmp");
                }
            }
        }],
        addCloseButton: true,
        
    });
}
function deleteRol() {
    //Delete 
    var objRowSelected = $.jqxGridApi.getOneSelectedRow("#RolGrid", true);
    if (objRowSelected) {
        var htmlContentModal = "<b>SOEID ID:</b>" + objRowSelected["Soeid"] + "<br/>";

        _showModal({
            width: '35%',
            modalId: "modalDel",
            addCloseButton: true,
            buttons: [{
                name: "Delete",
                class: "btn-danger",
                onClick: function () {
                    //Save change to get the ID
                    _callProcedure({
                        loadingMsgType: "fullLoading",
                        loadingMsg: "Deleting Issue '" + objRowSelected["Soeid"] + "'...",
                        name: "[dbo].[spMakerCheckerAdminIssue]",
                        params: [
                            { "Name": "@Action", "Value": "Delete" },
                            { "Name": "@SessionSOEID", "Value": _getSOEID() },
                            { "Name": "@IssueID", "Value": objRowSelected["Soeid"] }
                        ],
                        success: {
                            fn: function (response) {
                                _showNotification("success", "The issue '" + objRowSelected["Soeid"] + "' was deleted successfully.");

                                //Refresh issue List
                                if (loadMakerCheckerIssueList) {
                                    loadMakerCheckerIssueList();
                                }
                            }
                        }
                    });
                }
            }],
            title: "Are you sure you want to delete this User?",
            contentHtml: htmlContentModal
        });
    }
}
//Fn to get value of QS
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
//Method Of use
//getParameterByName('ID');
//getParameterByName('name');
function fnLoadFromIdFlashCustomSegments() {
    
    //console.log(getParameterByName('name'));
    $("#nameOfSegment").html("Edit " + getParameterByName('name'));
    //console.log(getParameterByName('ID'));
    $.jqxGridApi.create({
        showTo: "#flashEditSegmentsGrid",
        options: {
            //for comments or descriptions
            height: "500px",
            width: "460px",
            autoheight: true,
            autorowheight: true,
            selectionmode: "multiplerow",
            showfilterrow: true,
            sortable: true,
            editable: true
            //'singlerow', 'multiplerows', 'checkbox', 'multiplerowsadvanced' or 'multiplerowsextended'.

        },
        sp: {
            Name: "[dbo].[spFlashGetManagementSegmentsByFlash]",
            Params: [
                { Name: "@pFlashSegmentID", Value: getParameterByName('ID') }
            ]
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set",
        },
        groups: [],
        columns: [
            { name: 'ID', type: 'int', hidden: true },
            { name: 'FlashSegmentID', width: '180px', type: 'int', hidden: true, editable: false },
            { name: 'ManagementSegmentID', width: '180px', type: 'int', hidden: true, editable: false },
            { name: 'ManagementSegmentParentID',  width: '180px', type: 'int', hidden: true, editable: false },
            { name: 'ManagementSegmentDescription', text: 'Description', width: '180px', type: 'string', editable: false },
            { name: 'CreatedBy', text: 'Create By', width: '100px', type: 'string', editable: false },
            { name: 'CreatedDate', text: 'Creation Date', width: '180px', type: 'Date', cellsformat: 'D' },
           
        ],
        ready: function () {
            $(".btnDelete").click(function () {
                fnDisableSegment();
            });
            //$("#flashEditSegmentsGrid").on('rowselect', function (event) {
            //    $(".btnDelete").attr("data", + event.args.row.ID);
            //    console.log('delete ' + event.args.row.ID);
            //});
        }
    });
}
//fn to show the segments with parameter level
function fnCustomShowSegmentsizeSegment(value) {
    var $getLevel = $('<input>').attr({
        type: 'hidden',
        id: 'levelId',
        name: 'levelId',
        value: value
    })
    $("#chklist").append($getLevel);
    //console.log(value);
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Listing Segments.",
        name: "[dbo].[spGetManagementSegmentByLevel]",
        params: [
            { "Name": "@pLevel", "Value": value }
        ],
        //Show message only for the last 
        success: {
            showTo: $(column),
            msg: "Data load.",
            fn: function (responseList) {
                function addCheckbox(responseList) {
                    //console.log(responseList);
                    var container = $('#chklist');
                    var inputs = container.find('input');
                    var divs = $("#chklist");
                    var col = 0;
                    for (var i = 0; i < responseList.length; i++) {
                        //toDo:??
                        col % 2 ? setColumns() : setColumns();
                        //fn to show columns with module and the others
                        col++
                    }
                    function setColumns() {
                        var $div = $("<li>", { "class": "col-md-3" });
                        $('<input />', { type: 'checkbox', id: responseList[i].Description, value: responseList[i].ID }).appendTo($div);
                        $('<label />', { 'for': 'cb', text: responseList[i].Description }).appendTo($div);
                        $("#column").append($div);
                    }
                    $("#chklist").show("slow");
                }
                addCheckbox(responseList);
            }
        }
    });
}
//fn to save 
function fnSaveSelectedSegments(lSegmentsIds) {
    var lSegmentList = lSegmentsIds.join(", ")
    var lgetLevel = document.getElementById("levelId").value
    //fnCustomShowSegmentsizeSegment(value);
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Saving Segments...",
        name: "[dbo].[spFlashMapManagementSegmentByFlash]",
        params: [
            { "Name": "@pFlashSegmentID", "Value": getParameterByName('ID') },
            { "Name": "@pManagementSegmentsList", "Value": lSegmentList },
            { "Name": "@pCreatedBy", "Value": _getSOEID() },
            { "Name": "@pLevel", "Value": lgetLevel }
        ],
        success: {
            //showTo: $(idMainJqxGrid).parent(),
            msg: "Name was save successfully.",
            fn: function () {
                _showModal({
                    width: "75%",
                    title: "Segments Save Successfully",
                    contentHtml: "Segments Save Successfully",
                    buttons: [{
                        name: "Ok",
                        class: "btn-success",
                        closeModalOnClick: true,
                        //onClick: function ($modal) {
                        //    objRole.Name = $modal.find(".txtName").val();
                        //    objRole.EERSMarketplaceRoleID = $modal.find(".txtEERSMarketplaceRoleID").val();
                        //    objRole.EERSFunctionCode = $modal.find(".txtEERSFunctionCode").val();
                        //    objRole.EERSFunctionDescription = $modal.find(".txtEERSFunctionDescription").val();
                        //    objRole.EERSIgnore = ($modal.find('[name="chkFlagEERSIgnore"]:checked').val() == '1' ? 'True' : 'False');

                        //    if (options.onSave) {
                        //        options.onSave(objRole);
                        //    }
                        //}
                    }],
                    addCloseButton: true,
                    onReady: function () {
                    }
                    
                });
                $('#lastId').remove();
                $("#column").empty();
                $("#showSegments").hide();
                fnLoadFromIdFlashCustomSegments();

            }
        }
    });

}
//fn to delete
function fnDisableSegment() {
    var objRowSelected = $.jqxGridApi.getOneSelectedRow("#flashEditSegmentsGrid", true);
    //fn return object by name an rowId
    var lFnGetDataFromGrid = function () {
        var lRowsSelected = $("#flashEditSegmentsGrid").jqxGrid('selectedrowindexes');
        var lSegmentsName = new Array();
        var lSelectedRecords = new Array();
        for (var i = 0; i < lRowsSelected.length; i++) {
            var lRow = $("#flashEditSegmentsGrid").jqxGrid('getrowdata', lRowsSelected[i]);
            lSelectedRecords[lSelectedRecords.length] = lRow.ManagementSegmentID;
            lSegmentsName[lSegmentsName.length] = '<li>' + lRow.ManagementSegmentDescription + '</li>'
        }
        return {
            SegmentsName: lSegmentsName,
            SelectedRecords: lSelectedRecords
        };

    }

    var fnSegmentsChoose = function () {
        var lSegments = lFnGetDataFromGrid().SegmentsName;
        return lSegments.join(" ");
    }

    var lRowsIds = lFnGetDataFromGrid().SelectedRecords.join(", ");

    if (objRowSelected) {
        var htmlContentModal = "<b><ol>" + fnSegmentsChoose(); + "</ol></b>";
        var src = "/FlashTool/FlashEditSegments/";
        _showModal({
            width: '35%',
            modalId: "modalDel",
            addCloseButton: true,
            buttons: [{
                name: "Delete",
                class: "btn-danger",
                onClick: function () {
                    //Save change to get the ID
                    _callProcedure({
                        loadingMsgType: "fullLoading",
                        loadingMsg: "Deleting Segments '" + lRowsIds + "'...",
                        name: "[dbo].[spFlashDisableManagementSegmentsByFlash]",
                        params: [
                            { "Name": "@pFlashSegmentID", "Value": getParameterByName('ID')},
                            { "Name": "@pManagementSegmentsList", "Value": lRowsIds }

                        ],
                        success: {
                            fn: function (response) {
                                _showNotification("success", "The Segment '" + fnSegmentsChoose() + "' was deleted successfully.");
                                //Fn To reload de data
                                //fnLoadFlashCustomSegments();
                            }
                        }
                    });
                }
            }],
            title: "Are you sure you want to delete this Segments?",
            contentHtml: htmlContentModal
        });
    }
}
_showLoadingFullPage({
    msg: "Loading"
});


//button click
_hideLoadingFullPage()


