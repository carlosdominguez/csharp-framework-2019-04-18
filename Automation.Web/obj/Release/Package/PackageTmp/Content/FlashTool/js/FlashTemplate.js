$(document).ready(function () {
    //$(".btnViewDetail").click(function () {
    //    var lSelectedRow = $.jqxGridApi.getOneSelectedRow("#tblFileTable", true);
    //    _showModal({
    //        modalId: "modalFormNewItem",
    //        addCloseButton: true,
    //        title: "Flash Feed Detail",
    //        contentHtml: "00002	1990140003	Computer Asset Clearing	1990141900	Computer Asset Clearing	100015674	Payables	Purchase Invoices	INSIGHT DIRECT USA INCORPORATED	0912422947	25-Aug-2017	7681040	APC Smart-UPS 1500 LCD - UPS - 1 kW - 15	OCT-17	02-Oct-2017	13500	0	USD	13500	0	13499.7	Global Customer Operations [L7	Branch Operations (CBNA CFNA) 	CBNA Branch - NAIT CBNA [L9]	0	Branch Operations (CBNA CFNA) [L8]	BALANCE SHEET	BALANCE SHEET	160410	Property, Plant and Equipment (L5)	Not Applicable	Not Applicable	Not Applicable	Not Applicable	Not Applicable	Not Applicable	Not Applicable	Not Applicable	BALANCE SHEET",
    //    });
    //});
    _hideMenu();
    loadFilesTables2();
    //$("#contenttabletblFileTable").append('<button type="submit" class="btn btn-success">Save</button>');
});

var data = [{
    FlashId: "Test1",
    FlashTab: "Employee Salaries and Overtime (L7)",
    SepActual: "3.472.363",
    ELRP2P: "65,069",
    YesterdayELR:"0",
    NewActivity:"0",
    Accrual: "35.000",
    UnposedCanada: "1.721.266.50",
    aps:"0"
}, {
    FlashId: "Test2",
    FlashTab: "Benefits / Fringe (L7)",
    SepActual: "210.152",
    ELRP2P: "0",
    YesterdayELR: "0",
    NewActivity: "0",
    Accrual: "0",
    UnposedCanada: "210.152.15",
    aps: "0"
}, {
    FlashId: "Test3",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "Security Contracts (L7)",
    Total: "60"
}, {
    FlashId: "Test4",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "Technical Services (L8)",
    Total: "138,466"
}, {
    FlashId: "Test5",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "Telecom Data Networks (L8)",
    Total: "10,341"
}, {
    FlashId: "Test6",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "Software Maintenance (L8)",
    Total: "37,584"
}, {
    FlashId: "Test7",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "Other Technology Cost (L8)",
    Total: "39,806"
}, {
    FlashId: "Test8",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "Telecom Voice (L8)",
    Total: "18,458"
}, {
    FlashId: "Test9",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "Other Hardware Expense (L8)",
    Total: "95,277"
}, {
    FlashId: "Test10",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "IT Contractors - Technology Infrastructure (L8)",
    Total: "8,824"
}, {
    FlashId: "Tes11",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "IT 3rd Party Technical Service Providers - Technology Infrastructure (L8)",
    Total: "3,480,527"
}, {
    FlashId: "Test12",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "Total Postage (L7)",
    Total: "3,185"
}, {
    FlashId: "Test13",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "Travel and Entertainment (L6)",
    Total: "12,037"
}, {
    FlashId: "Test13",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "Freight and Cartage (L7)",
    Total: "48.00"
}, {
    FlashId: "Test14",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "Printing and Binding (L7)",
    Total: "11,591"
}, {
    FlashId: "Test15",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "Stationery and Supplies (L7)",
    Total: "48.00"
}, {
    FlashId: "Test16",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "Printing and Binding (L7)",
    Total: "71"
}, {
    FlashId: "Test17",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "Stationery and Supplies (L7)",
    Total: "76"
}, {
    FlashId: "Test18",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "Rent(7)",
    Total: "80"
}, {
    FlashId: "Test19",
    FlashTab: "Product Management [L7]",
    FlashAccount: "Variable Incentive Compensation (L7)",
    Total: "12,037"
}, {
    FlashId: "Test20",
    FlashTab: "Product Management [L7]",
    FlashAccount: "Employee Salaries and Overtime (L7)",
    Total: "11,591"
}, {
    FlashId: "Test21",
    FlashTab: "Product Management [L7]",
    FlashAccount: "Discretionary Incentive Compensation (L7)",
    Total: "97,181"
}, {
    FlashId: "Test22",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "IT Contractors - Technology Infrastructure (L8)",
    Total: "75,984"
}, {
    FlashId: "Test23",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "IT 3rd Party Technical Service Providers - Technology Infrastructure (L8)",
    Total: "275,167"
}, {
    FlashId: "Test24",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "Total Postage (L7)",
    Total: "4"
}, {
    FlashId: "Test25",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "Travel and Entertainment (L6)",
    Total: "4,610"
}, {
    FlashId: "Test26",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "Freight and Cartage (L7)",
    Total: "75"
}, {
    FlashId: "Test27",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "Meetings & Events (L6)",
    Total: "3,148"
}, {
    FlashId: "Test28",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "Stationery and Supplies (L7)",
    Total: "875"
}, {
    FlashId: "Test29",
    FlashTab: "Branch Operations (CBNA CFNA) [L8]",
    FlashAccount: "Software Purchases (L8)",
    Total: "159"
}, {
    FlashId: "Test30",
    FlashTab: "Product Management [L7]",
    FlashAccount: "Education and Training Expenses (L6)",
    Total: "35100"
}];

function loadFilesTables2() {
    $.jqxGridApi.create({
        showTo: "#divPivotGrid",
        options: {
            //for comments or descriptions
            height: "500",
            autoheight: false,
            autorowheight: false,
            showfilterrow: true,
            sortable: true,
            editable: true,
            //'singlerow', 'multiplerows', 'checkbox', 'multiplerowsadvanced' or 'multiplerowsextended'.
            selectionmode: "singlerow"
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set Local",
            rows: data
        },
        groups: [],
        columns: [
            { name: 'FlashTab', width: '400px', type: 'string', filtertype: 'input' },
            { name: 'FlashAccount', width: '400px', type: 'string', filtertype: 'input' },
            { name: 'Total', width: '400px', type: 'string', filtertype: 'input' }
        ],
        ready: function () {}
    });
}

function loadFilesTables() {

    var source =
    {
        localdata: data,
        datatype: "array",
        datafields:
        [
            { name: 'FlashTab', type: 'string' },
            { name: 'FlashAccount', type: 'string' },
            { name: 'Total', type: 'number' },
        ]
    };
    var dataAdapter = new $.jqx.dataAdapter(source);
    // initialize jqxGrid
    $("#divPivotGrid").jqxGrid(
    {
        autoheight: true,
        source: dataAdapter,
        altrows: true,
        sortable: true,
        editable: true,
        selectionmode: 'multiplecellsextended',
        columns: [
          { text: 'FlashTab', datafield: 'FlashTab', width: 300 },
          { text: 'FlashAccount', datafield: 'FlashAccount', width: 250 },
          { text: 'Total', datafield: 'Total', width: 200 },
        ]
    });   
}

_showLoadingFullPage({
    msg: "Test Loading"
});


//button click
_hideLoadingFullPage()


