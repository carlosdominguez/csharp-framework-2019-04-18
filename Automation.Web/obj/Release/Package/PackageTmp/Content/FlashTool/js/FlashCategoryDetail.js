//TODO: Put in a global JS
function fnLoadFlashCustomSegments() {
    $.jqxGridApi.create({
        showTo: "#flashSegmentsGrid",
        options: {
            width: "470px",
            autoheight: true,
            autorowheight: true,
            //selectionmode: "multiplerows",
            showfilterrow: true,
            sortable: true,
            editable: true
            //'singlerow', 'multiplerows', 'checkbox', 'multiplerowsadvanced' or 'multiplerowsextended'.
        },
        sp: {
            Name: "[dbo].[spFlashGetAllFlashSegments]",
            Params: []
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set"
        },
        groups: [],
        columns: [
            { name: 'ID', type: 'int', hidden: true },
            { name: 'FlashSegmentName', text: 'Segment Name', width: '180px', type: 'string', editable: false, cellsalign: 'center', align: 'center' },
            { name: 'CreatedBy', text: 'Created By', width: '100px', type: 'string', editable: false, cellsalign: 'center', align: 'center' },
            { name: 'CreatedDate', text: 'Creation Date', width: '180px', type: 'string', cellsformat: 'D', cellsalign: 'center', align: 'center' },
        ],
        ready: function () {
            $("#flashSegmentsGrid").on('rowselect', function (event) {
                $("#flashbtn").prop("disabled", false);
                //$("#bridgeBtn").prop("disabled", false);
                $("#sendParameter").attr("href", _getViewVar("SubAppPath") + "/FlashTool/FlashCategoryDetail?ID=" + event.args.row.ID + "&name=" + event.args.row.FlashSegmentName);
                $("#sendBridgeParameter").attr("href", _getViewVar("SubAppPath") + "/FlashTool/FlashBridge?ID=" + event.args.row.ID + "&name=" + event.args.row.FlashSegmentName);
            });

        }
    });
}
function fnGoToSegments() {

    var htmlContentModal = 'Choose at least one segment';
    var formHtml =
                '<div class="content-body">' +
                    '<div class="center">' +
                        '<div id="flashSegmentsGrid"></div>' +
                        '<a id="sendParameter">' +
                            '<button type="button" id="flashbtn" class="btnDelete btn btn-info top15 left15 bottom15 pull-right">' +
                                '<i class="fa fa-external-link"></i> Flash' +
                            '</button>' +
                        '</a>' +
                    '</div>' +
                '</div>';
    fnLoadFlashCustomSegments();
    _showModal({
        modalId: "modalUpload",
        width: '40%',
        //buttons: [{
        //    name: "<i class='fa fa-save'></i> OK",
        //    class: "btn-success",
        //    closeModalOnClick: true,
        //    onClick: function ($modal) {
        //        //Validate required Comment
        //        window.location.href = _getViewVar("SubAppPath") + '/FlashTool/FlashCategory';
        //    }
        //}],
        addCloseButton: false,
        title: "Choose at least one segment",
        contentHtml: formHtml,
        onReady: function ($modal) { }
    });

}
$(document).ready(function () {
    //_hideMenu();
    fnLoadFlashFile();
    // initialize the input fields.
    $("#editComment").click(function () {
        showModalAddCommentEdit();
    });    $("#SaveFlash").click(function () {
        fnSaveFlash();
    });    $("#diff").click(function () {
        fnShowDiffMoM();
    });    
    //$("#contenttabletblFileTable").append('<button type="submit" class="btn btn-success">Save</button>');
});

//Fn to get Date
function fnGetDate() {
    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();
        if (dateObj.getDay() == 1) {
            lYesterday = day - 3;
            console.log("Es Lunes " + year + "-" + month + "-" + lYesterday);
            return gTodayDate = year + "-" + month + "-" + lYesterday;
        }
        console.log("no es lunes "+year + "-" + month + "-" + day);
    return gTodayDate = year + "-" + month + "-" + day;
}
function getDate(lVal) {
    var lDate = new Date();
    var lMonth = lDate.getMonth() + 1;
    if (lVal == 'm') {
        return lMonth
    }
    if (lVal == 'y') {
        return lDate.getFullYear()
    }
}
//Fn to get value of QS
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

console.log(getParameterByName('ID'));
if (typeof getParameterByName('ID') === 'undefined' || !getParameterByName('ID')) {
    console.log('Entre');
    fnGoToSegments();
}

function fnLoadFlashFile() {
    $("#nameOfSegment").html("Flash Segment: <br />" + getParameterByName('name'));
    var lEditableCells = function (index, value, defaultvalue, column, rowdata) {
        var value = defaultvalue ? '' : 0;
        //total = total ? 'Nan' : 0;

        if (value) {
            return '<center><div style="background-color:lightBlue; line-height: 25px;;"><span><b>' + value + '</b></span></div></center>';
        }
        //} else {
        //    return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #008000;">' + value + '</span>';
        //}
        return "<center><div style='background-color:lightBlue; line-height: 25px;'><span><b>" + value + "</b></span></div></center>";
    }
    //add color to cells
    var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties, data) {

        if (columnfield == 'Accrual') {
            if (value == "") {
                value = 0;
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(value, "c2"); + '</b></span></div></center>';
            } else {
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(value, "c2"); + '</b></span></div></center>';
            }
        }
        if (columnfield == 'UnpostedCanada') {
            if (value == "") {
                value = 0;
                return '<center><div style="background-color:lightBlue; line-height: 25px;;"><span><b>' + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(value, "c2"); + '</b></span></div></center>';
            } else {
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(value, "c2"); + '</b></span></div></center>';
            }
        }
        if (columnfield == 'APS') {
            if (value == "") {
                value = 0;
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(value, "c2"); + '</b></span></div></center>';
            } else {
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(value, "c2"); + '</b</span></div></center>';
            }
        }
        if (columnfield == 'PriorActual') {
            //console.log(data);
            if (value == "") {
                value = 0;
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(value, "c2") + '</b></span></div>';
            } else {
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(value, "c2") + '</b></span></div>';
            }
        }

    }
    var groupsrenderer = function (text, group, expanded) {
        return "" + group + "";
    }
    $("#Grid").jqxTooltip();
    if (typeof getParameterByName('ID') === 'undefined' || !getParameterByName('ID')) {
        console.log('vacio');
    } else {
        $.jqxGridApi.create({
            showTo: "#Grid",
            options: {
                //for comments or descriptions
                height: "500",
                editable: true,
                filterable: true,
                sortable: true,
                autorowheight: false,
                autowidth: true,
                showfilterrow: true,
                showstatusbar: true,
                statusbarheight: 25,
                //selectionmode: 'singlecell',
                showaggregates: true,
                groupable: true,
                sortable: true,
                sortcolumn: 'Order',
                sortdirection: 'asc',
                altrows: true,                   

            },

            sp: {
                Name: "[dbo].[spFlashGetSegmentsByFlashLevel]",
                Params: [
                        { Name: "@pIdSegment", Value: getParameterByName('ID') },
                        { Name: "@vCurrentMonth", Value: getDate('m') }
                ]
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                dataBinding: "Large Data Set",
                //rows: data
            },
            //groups: ['FlashAccount'],
            columns: [

                {
                    name: 'ExpenseCategory', datafield: 'SubCategory', text: 'Expense Category', width: '200px', type: 'string', pinned: true,
                    filtertype: 'checkedlist', cellsalign: 'Left', align: 'center', editable: false, cellsalign: 'left', align: 'center',
                    groupsrenderer: groupsrenderer,
                    
                },
                {
                    name: 'FlashAccount', groupable: false, text: 'Expense Account', width: '200px', type: 'string', filtertype: 'input', pinned: true,
                    editable: false, cellsalign: 'Left', align: 'center',
                    
                },
                {
                    name: 'PriorActual', datafield: 'PriorActual', text: 'Prior Actual', width: '190px', type: 'number', filtertype: 'input', cellsformat: 'c2', editable: false, cellsalign: 'right', align: 'Center',
                    cellsrenderer: cellsrenderer,
                   
                    aggregates: [{
                        '<b id="priorActual">Total':
                        function (aggregatedValue, currentValue, column, record) {
                            var total = $("#Grid").jqxGrid('getcolumnaggregateddata', 'PriorActual', ['sum']);
                            sum = total.sum
                            this.priorActual = sum;
                            return sum
                            '</b>'
                        }
                    }],


                },
                ////{ name: 'Seg', width: '80px', type: 'string', filtertype: 'input', editable: false, aggregates: ['sum'] },
                {
                    name: 'today', datafield: 'today', text: 'ELR (P2P)', width: '150px', type: 'number', filtertype: 'input', cellsalign: 'right', align: 'center', cellsformat: 'c2', editable: false,
                    aggregates: [{
                        '<b>Total</b>':
                        function (aggregatedValue, currentValue, column, record) {
                            var total = $("#Grid").jqxGrid('getcolumnaggregateddata', 'today', ['sum']);
                            sum = total.sum
                            return sum
                        }
                    }],
                    cellsrenderer:
                    function (index, value, defaultvalue, column, rowdata) {
                        //var total = parseFloat(rowdata.PriorActual) - parseFloat(rowdata.yesterday);
                        if (defaultvalue === "") {
                            defaultvalue = 0;
                            var total = defaultvalue;
                            return "<div style='margin: 4px;' class='jqx-right-align'>" + total + "</div>";
                        }

                    }
                },
                {
                    name: 'yesterday', datafield: 'yesterday', text: 'Yesterday ELR', width: '180px', groupable: true, type: 'number', filtertype: 'input', cellsalign: 'center', align: 'center', cellsformat: 'c2', editable: false,
                    aggregates: [{
                        '<b>Total</b>':
                        function (aggregatedValue, currentValue, column, record) {
                            var total = $("#Grid").jqxGrid('getcolumnaggregateddata', 'yesterday', ['sum']);
                            sum = total.sum
                            return sum
                        }
                    }],
                },
                {
                    name: 'NewActivity', datafield: 'NewActivity', width: '190px', text: 'New Activity', groupable: true, type: 'number', filtertype: 'input', cellsalign: 'center', align: 'center', editable: false, cellsformat: 'c2', aggregates: ['sum'],
                    cellsrenderer:
                    function (index, datafield, value, defaultvalue, column, rowdata) {
                        if (rowdata.today == null || rowdata.today === "") {
                            rowdata.today = 0;
                        }
                        if (rowdata.yesterday == null || rowdata.yesterday === "") {
                            rowdata.yesterday = 0;
                        }

                        var total = parseFloat(rowdata.today) - parseFloat(rowdata.yesterday);
                        if (rowdata.NewActivity != total) {
                            $("#Grid").jqxGrid('setcellvalue', rowdata.uid, "NewActivity", total);
                        }
                        //Base de datos no se valido isNull
                        return "<div style='margin: 4px;' class='jqx-right-align'>" + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(total, "c2"); + "</div>";
                    },
                    
                    //aggregates: [{

                    //    '<b>Total</b>':
                    //    function (aggregatedValue, currentValue, column, record) {
                    //        var total = $("#Grid").jqxGrid('getcolumnaggregateddata', 'NewActivity', ['sum']);
                    //        sum = total.sum
                    //        return sum
                    //    }
                    //}],
                },
                {
                    name: 'Accrual', width: '120px', groupable: true, type: 'number', filtertype: 'input', cellsalign: 'center', align: 'center',
                    cellsrenderer: cellsrenderer,
                },
                { name: 'check', text: 'Flat to Prior', width: '90px', type: 'string', columntype: 'checkbox', cellsalign: 'center', align: 'center' },
                {
                    name: 'UnpostedCanada', text: 'Unposted ', width: '120px', groupable: true, type: 'number', filtertype: 'input', cellsalign: 'center', align: 'center',
                    cellsrenderer: cellsrenderer, cellsformat: 'c2'
                },
                {
                    name: 'APS', width: '120px', groupable: true, type: 'number', filtertype: 'input', cellsalign: 'center', align: 'center', cellsformat: 'c2', editable: false,
                    aggregates: [{
                        '<b>Total</b>':
                        function (aggregatedValue, currentValue, column, record) {
                            var total = $("#Grid").jqxGrid('getcolumnaggregateddata', 'APS', ['sum']);
                            sum = total.sum
                            return sum
                        }
                    }],
                },
                {
                    name: 'BAW', width: '120px', groupable: true, type: 'number', filtertype: 'input', cellsalign: 'center', align: 'center', cellsformat: 'c2', editable: false,
                    aggregates: [{
                        '<b>Total</b>':
                        function (aggregatedValue, currentValue, column, record) {
                            var total = $("#Grid").jqxGrid('getcolumnaggregateddata', 'BAW', ['sum']);
                            sum = total.sum
                            return sum
                        }
                    }],
                },

                ////listo elr+ var sum=(accrual + unposted + aps)
                {
                    name: 'Month', text: 'Flash', width: '120px', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center', editable: false, cellsformat: 'c2', 
                    cellsrenderer:
                    function (index, datafield, value, defaultvalue, column, rowdata) {
                        if (rowdata.today == null || rowdata.today === "") {
                            rowdata.today = 0;
                        }
                        if (rowdata.Accrual == null || rowdata.Accrual === "") {
                            rowdata.Accrual = 0;
                        }
                        if (rowdata.UnpostedCanada == null || rowdata.UnpostedCanada === "") {
                            rowdata.UnpostedCanada = 0;
                        }
                        if (rowdata.APS == null || rowdata.APS === "") {
                            rowdata.APS = 0;
                        }
                        if (rowdata.BAW == null || rowdata.BAW === "") {
                            rowdata.BAW = 0;
                        }
                        var total = parseFloat(rowdata.today) + parseFloat(rowdata.Accrual) + parseFloat(rowdata.UnpostedCanada) + parseFloat(rowdata.APS) + parseFloat(rowdata.BAW);
                        //totalFlash = total;
                        // rowdata.Month = total;
                       // $("#Grid").jqxGrid('updaterow', rowdata.uid, rowdata);
                        
                        if (rowdata.Month != total){
                            $("#Grid").jqxGrid('setcellvalue', rowdata.uid, "Month", total);
                            this.totalFlash = total;
                            
                        }
                        

                        return "<div style='margin: 4px;' class='jqx-right-align'>" + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(total, "c2"); + "</div>";
                    },
                    aggregates: [{
                        '<b id="flashTotal">Total':
                        function (aggregatedValue, currentValue, column, record) {
                            var total = $("#Grid").jqxGrid('getcolumnaggregateddata', 'Month', ['sum']);
                            sum = total.sum
                            return sum
                            '</b>'
                        }
                        
                    }],

                },
                {
                    name: 'momVariance', datafield: 'momVariance', text: 'MoM Variance', width: '110px', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center', editable: false,
                    aggregates: ['sum'],
                    cellsrenderer:
                    function (index, datafield, value, defaultvalue, column, rowdata) {
                        if (rowdata.today == null || rowdata.today === "") {
                            rowdata.today = 0;
                        }
                        if (rowdata.Accrual == null || rowdata.Accrual === "") {
                            rowdata.Accrual = 0;
                        }
                        if (rowdata.UnpostedCanada == null || rowdata.UnpostedCanada === "") {
                            rowdata.UnpostedCanada = 0;
                        }
                        if (rowdata.APS == null || rowdata.APS === "") {
                            rowdata.APS = 0;
                        }
                        var lMonth = parseFloat(rowdata.today) + parseFloat(rowdata.Accrual) + parseFloat(rowdata.UnpostedCanada) + parseFloat(rowdata.APS);
                        var total = parseFloat(rowdata.PriorActual) - parseFloat(lMonth);
                        total = total || 0;
                        return "<div style='margin: 4px;' class='jqx-right-align'>" + $.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(total, "c2"); + "</div>";
                    },
                    aggregates: [{
                        '<b>Total</b>':
                                function (aggregatedValue, currentValue, column, rowdata) {
                                    if (rowdata.today == null || rowdata.today === "") {
                                        rowdata.today = 0;
                                    }
                                    if (rowdata.Accrual == null || rowdata.Accrual === "") {
                                        rowdata.Accrual = 0;
                                    }
                                    if (rowdata.UnpostedCanada == null || rowdata.UnpostedCanada === "") {
                                        rowdata.UnpostedCanada = 0;
                                    }
                                    if (rowdata.APS == null || rowdata.APS === "") {
                                        rowdata.APS = 0;
                                    }
                                    var lMonth = parseFloat(rowdata.today) + parseFloat(rowdata.Accrual) + parseFloat(rowdata.UnpostedCanada) + parseFloat(rowdata.APS);
                                    var total = parseFloat(rowdata.PriorActual) - parseFloat(lMonth);
                                    total = total || 0;
                                    //var aggregatedValue = isNaN(parseInt(aggregatedValue)) ? 0 : parseInt(aggregatedValue)
                                    //console.log(aggregatedValue);
                                    if (isNaN(aggregatedValue)) {
                                        aggregatedValue = 0;
                                    }
                                    var lShowTotal = aggregatedValue + total;
                                    return lShowTotal;//$.jqxGridApi.localStorageFindById("#Grid").dataAdapter.formatNumber(lShowTotal, "c2");
                                }
                    }]
                },

                ////listo flash menos previous month tiene que ser primero flash --ToDo
                {
                    name: 'Comment', width: '80px', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center', editable: false,cellsrenderer: function (rowIndex, datafield, value) {
                        var dataRecord = $("#Grid").jqxGrid('getrowdata', rowIndex);
                        //console.log(dataRecord.Comment);
                        var htmlComment = "<div style='padding: 10px; font-size: 15px; max-height: 500px; overflow-y:scroll; width: 520px;'>";
                        htmlComment += "<b>Comment:</b> " + dataRecord.Comment + " <br>";
                        htmlComment += "</div>";
                        var htmlResult = "";

                        if (dataRecord.Comment) {
                            htmlResult +=
                                '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + htmlComment + '" data-title="Comments History" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                                '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png"/> ' +
                                '</div>';
                        }
                        return htmlResult;
                    }
                },
                {
                    name: 'IDComment', width: '190px', type: 'number', filtertype: 'input', align: 'center', editable: false, cellsalign: 'center'
                    , hidden: 'true'
                },
                ////{ name: 'One Timer', width: '110px', type: 'string', filtertype: 'input',columntype: 'checkbox' }
            ],
            ready: function () {
                $('#Grid').jqxGrid('addgroup', 'ExpenseCategory');
                //$('#Grid').on('loadComplete', function (event) {
                //    $('#Grid').jqxGrid('addgroup', 'ExpenseCategory');
                //});
                //
                //var summaryData = $("#Grid").jqxGrid('getcolumnaggregateddata', 'NewActivity', ['sum']);
                //console.log("summaryData " + summaryData);
                //Order by Column Order
                $("#Grid").jqxGrid('sortby', 'Order', 'asc');
                $("#Grid").on('cellvaluechanged', function (event) {
                    if (event.args.datafield === "check") {
                        console.log('check ' + event.args);
                        var dataRecord = $("#Grid").jqxGrid('getrowdata', event.args.rowindex);
                        if (event.args.newvalue) {
                            console.log('check');
                            if (!dataRecord.PriorActual) {
                                dataRecord.PriorActual = 0;
                            }
                            if (!dataRecord.today) {
                                dataRecord.today = 0;
                            }
                            //dataRecord.PriorActual = dataRecord.PriorActual ? 'undefined' : 0;
                            dataRecord.UnpostedCanada = ((dataRecord.today) - (dataRecord.PriorActual));
                        } else {
                            dataRecord.UnpostedCanada = 0;
                        }
                        $("#Grid").jqxGrid('updaterow', event.args.rowindex, dataRecord);
                    }
                });

                //Add popup of information
                $("#Grid").on("rowclick", function (event) {
                    _GLOBAL_SETTINGS.tooltipsPopovers();
                });
               
            }


        });
        $.jqxGridApi.create({
            showTo: "#FteGrid",
            options: {
                //for comments or descriptions
                height: "300",
                editable: true,
                filterable: true,
                sortable: true,
                autorowheight: false,
                showfilterrow: true,
                showstatusbar: true,
                statusbarheight: 25,
                //selectionmode: 'singlecell',
                showaggregates: true,
                groupable: true,
                sortable: true,
                sortcolumn: 'Order',
                sortdirection: 'asc',
                altrows: true,                   

            },

            sp: {
                Name: "[dbo].[spFlashGetGDWBySegment]",
                Params: [
                        { Name: "@vCurrentMonth", Value: getDate('m') }
                        ,{ Name: "@pIdSegment", Value: getParameterByName('ID') },
                ]
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                dataBinding: "Large Data Set",
                //rows: data
            },
            //groups: ['FlashAccount'],
            columns: [
                {
                    name: 'SegmentName', text: 'Segment Name', width: '200px', type: 'string', filtertype: 'input', editable: false,
                    cellsalign: 'center', align: 'center'
                },
                {
                    name: 'Description', text: 'Description', width: '200px', type: 'string', filtertype: 'input', editable: false,
                    cellsalign: 'center', align: 'center'
                },
                //{ name: 'P2PAccount', datafield: 'account', text: 'P2P account', type: 'number', editable: false},
                {
                    name: 'Total', text: 'Total', width: '100px', type: 'string',
                    cellsalign: 'Left', align: 'center', editable: false, cellsalign: 'left', align: 'center',
                    aggregates: [{
                        '<b id="TotalCurrentMonth">Total</b>':
                        function (aggregatedValue, currentValue, column, record) {
                            var total = $("#FteGrid").jqxGrid('getcolumnaggregateddata', 'Total', ['sum']);
                            sum = total.sum
                            return sum
                        }
                    }],
                    
                },
                {
                    name: 'Date', groupable: false, text: 'Date', width: '150px', type: 'string', filtertype: 'input',
                    editable: false, cellsalign: 'Left', align: 'center', hidden: 'True'
                    
                },
            ]
        })
        $.jqxGridApi.create({
            showTo: "#FtePrevGrid",
            options: {
                //for comments or descriptions
                height: "300",
                editable: true,
                filterable: true,
                sortable: true,
                autorowheight: false,
                showfilterrow: true,
                showstatusbar: true,
                statusbarheight: 25,
                //selectionmode: 'singlecell',
                showaggregates: true,
                groupable: true,
                sortable: true,
                sortcolumn: 'Order',
                sortdirection: 'asc',
                altrows: true,


            },

            sp: {
                Name: "[dbo].[spFlashGetPrevMonthGDWToFlash]",
                Params: [
                        { Name: "@vCurrentMonth", Value: getDate('m') }
                        ,{ Name: "@pIdSegment", Value: getParameterByName('ID') },
                ]
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                dataBinding: "Large Data Set",
                //rows: data
            },
            //groups: ['FlashAccount'],
            columns: [
                {
                    name: 'SegmentName', text: 'Segment Name', width: '200px', type: 'string', filtertype: 'input', editable: false,
                    cellsalign: 'center', align: 'center'
                },
                {
                    name: 'Description', text: 'Description', width: '200px', type: 'string', filtertype: 'input', editable: false,
                    cellsalign: 'center', align: 'center'
                },
                //{ name: 'P2PAccount', datafield: 'account', text: 'P2P account', type: 'number', editable: false},
                {
                    name: 'PrevMonthTotal', text: 'Total', width: '100px', type: 'string',
                    cellsalign: 'Left', align: 'center', editable: false, cellsalign: 'left', align: 'center',
                    aggregates: [{
                        '<b id="PrevMonthTotal">Total</b>':
                        function (aggregatedValue, currentValue, column, record) {
                            var total = $("#FtePrevGrid").jqxGrid('getcolumnaggregateddata', 'PrevMonthTotal', ['sum']);
                            sum = total.sum
                            return sum
                        }
                    }],

                },
                {
                    name: 'Date', groupable: false, text: 'Date', width: '200px', type: 'string', filtertype: 'input',
                    editable: false, cellsalign: 'Left', align: 'center', hidden: 'True'

                },
            ]
        })
    }
    

}

function fnSaveFlash() {
   
    var rowData = $.jqxGridApi.getOneSelectedRow('#Grid', true);
    if (rowData) {
        var lPriorActual = rowData["PriorActual"];
        var lAccrual = typeof rowData["Accrual"] === 'undefined' ? 0 : rowData["Accrual"];
        var lUnposted = typeof rowData["UnpostedCanada"] === 'undefined' ? 0 : rowData["UnpostedCanada"];
        var ExpenseCategory = rowData["ExpenseCategory"]
        var FlashAccount = rowData["FlashAccount"]

        var htmlContentModal = '';
        htmlContentModal += "<b>Do you want to save this record? </b> ";
        _showModal({
            modalId: "modalUpload",
            width: '70%',
            buttons: [{
                name: "<i class='fa fa-save'></i> Save",
                class: "btn-success",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    //Add Comments
                    fnSaveConsolidated(lPriorActual, getParameterByName('name'), ExpenseCategory, FlashAccount, lAccrual, lUnposted, function () {
                        _showNotification("success", "Flash data was save successfully", "AlertReject");
                        //Close Modal
                        $modal.find(".close").click();
                    });
                }
            }],
            addCloseButton: true,
            title: "Save Flash template",
            contentHtml: htmlContentModal,
            onReady: function ($modal) { }
        });
    } else {
        fnLoadFlashFile();
    }
 
}
function fnSaveConsolidated(lPriorActual, MSDESCR, ExpenseCategory, FlashAccount, lAccrual, lUnposted, onSuccess) {
   
    this.totalFlash = $('#flashTotal').html()
    this.totalFlash = this.totalFlash.replace('Total:$', '');
    this.totalFlash = _replaceAll(",", "", this.totalFlash);
    //var lTotalFlash = this.totalFlash.split('"').join('');


    this.priorActual = $('#priorActual').html().replace('Total:$', '');
    this.priorActual = this.priorActual.replace('Total:$', '');
    this.priorActual = _replaceAll(",", "", this.priorActual);
    //var lTotalPrior = this.priorActual.replace(/^"(.*)"$/, '$1');
    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Saving data...",
        name: "[dbo].[spFlashSaveBridgeConsolidated]",
        params: [
            { "Name": "@pPriorActual", "Value": lPriorActual },

            { "Name": "@pSegmentId", "Value": getParameterByName('ID') },
            { "Name": "@pMSDesccr", "Value": MSDESCR },

            { "Name": "@pPriorActualTotalSegment", "Value": this.totalFlash },
            { "Name": "@pFlashExpenseCategory", "Value": ExpenseCategory },
            { "Name": "@pFlashExpenseAccount", "Value": FlashAccount },
    
            { "Name": "@pAccrual", "Value": parseFloat(lAccrual) },
            { "Name": "@pUnposted", "Value": parseFloat(lUnposted) },

            { "Name": "@pFlashTotalSegment", "Value": this.priorActual },
            { "Name": "@pMonth", "Value": getDate('m') },
            { "Name": "@pYear", "Value": getDate('y') },
            { "Name": "@pSessionSOEID", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                _showNotification("success", "Data was added successfully.", "AlertAddComment");
                onSuccess();
            }
        }
    });

}
function showModalAddCommentEdit() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#Grid', true);
    if (rowData) {
        var lPriorActual = rowData["PriorActual"];

        var lAccrual = typeof rowData["Accrual"] === 'undefined' ? 0 : rowData["Accrual"];
        var lUnposted = typeof rowData["UnpostedCanada"] === 'undefined' ? 0 : rowData["UnpostedCanada"];
        var lComment = rowData["Comment"];
        //if (lComment = "")
        //{
        //    lComment = ""
        //}
        //var lComment = typeof rowData["Comment"] === 'undefined' ? 0 : ' ';
        //if (!$(".txtDetailComment").val()) {
        //    lComment = rowData["Comment"];
        //}
        //if (!lComment) {
        //    lComment = $(".txtDetailComment").val();
        //}
        var htmlContentModal = '';
        htmlContentModal += "<b>Expense Category: </b> " + rowData["ExpenseCategory"] + " <br/>";
        htmlContentModal += "<b>Description: </b>" + rowData["FlashAccount"] + "<br/>";
        htmlContentModal += "<b>Accrual: </b>" + lAccrual + "<br/>";
        htmlContentModal += "<b>Unposted: </b>" + lUnposted + "<br/>";
        htmlContentModal += "<b>Comments: </b><br/>";
        htmlContentModal += "<textarea class='form-control txtDetailComment' placeholder='(optional)' style='height:100px !important;'>" + lComment + "</textarea>";

        _showModal({
            modalId: "modalUpload",
            width: '70%',
            buttons: [{
                name: "<i class='fa fa-save'></i> Save",
                class: "btn-success",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    //Add Comments
                    lComment = $(".txtDetailComment").val();

                    addDetailComment(lPriorActual, getParameterByName('name'), rowData["PriorActual"], rowData["FlashAccount"], rowData["ExpenseCategory"], lAccrual, lUnposted, lComment, function () {
                        _showNotification("success", "Flash data was save successfully", "AlertReject");
                        rowData["Accrual"] = lAccrual;
                        rowData["UnpostedCanada"] = lUnposted;
                        rowData["Comment"] = lComment;
                        $('#Grid').jqxGrid('updaterow', rowData.boundindex, rowData);
                        //Close Modal
                        $modal.find(".close").click();
                    });
                }
            }],
            addCloseButton: true,
            title: "Add Comment",
            contentHtml: htmlContentModal,
            onReady: function ($modal) { }
        });
    }

}
function addDetailComment(lPriorActual, MSDESCR, PriorActual, FlashAccount, ExpenseCategory, lAccrual, lUnposted, Comment, onSuccess) {
    //this.totalFlash = $('#flashTotal').html()
    //this.totalFlash = this.totalFlash.replace('Total:$', '');
    //this.totalFlash = _replaceAll(",", "", this.totalFlash);
    ////var lTotalFlash = this.totalFlash.split('"').join('');


    this.priorActual = $('#priorActual').html().replace('Total:$', '');
    this.priorActual = this.priorActual.replace('Total:$', '');
    this.priorActual = _replaceAll(",", "", this.priorActual);
    var lTotalPrior = this.priorActual.replace(/^"(.*)"$/, '$1');
    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Adding extra file comments...",
        name: "[dbo].[spFlashInsertComment]",
        params: [
            { "Name": "@pAction", "Value": "Insert" },

            { "Name": "@pPriorActual", "Value": lPriorActual },

            { "Name": "@pSegmentId", "Value": getParameterByName('ID') },
            { "Name": "@pMSDesccr", "Value": MSDESCR },
            { "Name": "@pFlashExpenseAccount", "Value": FlashAccount },
            { "Name": "@pFlashExpenseCategory", "Value": ExpenseCategory },
            { "Name": "@pAccrual", "Value": parseFloat(lAccrual) },
            { "Name": "@pUnposted", "Value": parseFloat(lUnposted) },
            { "Name": "@pComment", "Value": Comment},
            { "Name": "@pMonth", "Value": getDate('m') },
            { "Name": "@pYear", "Value": getDate('y') },
            { "Name": "@pSessionSOEID", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                //Return New ID
                _showNotification("success", "Data was added successfully.", "AlertAddComment"); 

                //$modal.find(".closeModal-modalUpload").click();
                //insertToFlashBridge();
                //
                onSuccess();
            }
        }
    });
   
}
/*
function insertToFlashBridge() {
    function fnSetValue(value) {
        if (typeof variable === 'undefined' || !variable) {
            return value = 0
        };
        return value;
    }
    var displayValue = $('#Grid').jqxGrid('getcelltext', true);
    var rowData = $.jqxGridApi.getOneSelectedRow('#Grid', true);
    var lP2pAccount = rowData["P2PAccount"];
    var lMoM = fnSetValue(rowData["momVariance"]);
    var lActual = fnSetValue(rowData["PriorActual"]);
    var lFlash = fnSetValue(rowData["Month"]);
    var lIdSegment = parseInt(getParameterByName('ID'));
    var lOnetimmer = "null";
    var lFlashDescription = getParameterByName('name');
    var lFlashAccount = rowData["FlashAccount"];
    var lExpenseCategory = rowData["ExpenseCategory"];

    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "",
        name: "[dbo].[spFlashInsertBridgeDetail]",
        params: [  
            { "Name": "@pIdSegment", "Value": lIdSegment },
            { "Name": "@pP2paccount", "Value": lP2pAccount },
            { "Name": "@pMSDesccr", "Value": lFlashDescription },
            { "Name": "@pPriorActual", "Value": lActual },
            { "Name": "@pFlash", "Value": lFlash },
            { "Name": "@pMOM", "Value": lMoM },
            { "Name": "@pFlashExpenseAccount", "Value": lFlashAccount },
            { "Name": "@pFlashExpenseCategory", "Value": lExpenseCategory },
            { "Name": "@pOneTimmer", "Value": lOnetimmer },
            { "Name": "@pSessionSOEID", "Value": _getSOEID() },
        ],
        success: {
            fn: function (responseList) {
                //Return New ID
                console.log(responseList);
                _showNotification("success", "Bridge was added successfully.", "AlertAddComment");
                //$modal.find(".close").click();
                //onSuccess(responseList);
            }
        }
    });
}
*/
function fnShowDiffMoM() {
    var PrevM = document.getElementById("PrevMonthTotal");
    var CurM = document.getElementById("TotalCurrentMonth");
    console.log(CurM - PrevM);
}

_showLoadingFullPage({
    msg: "Loading Segments"
});
//button click
_hideLoadingFullPage()