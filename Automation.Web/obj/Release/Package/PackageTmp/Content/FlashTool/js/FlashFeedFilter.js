$(document).ready(function () {
    //$(".btnViewDetail").click(function () {
    //    var lSelectedRow = $.jqxGridApi.getOneSelectedRow("#tblFileTable", true);
    //    _showModal({
    //        modalId: "modalFormNewItem",
    //        addCloseButton: true,
    //        title: "Flash Feed Detail",
    //        contentHtml: "00002	1990140003	Computer Asset Clearing	1990141900	Computer Asset Clearing	100015674	Payables	Purchase Invoices	INSIGHT DIRECT USA INCORPORATED	0912422947	25-Aug-2017	7681040	APC Smart-UPS 1500 LCD - UPS - 1 kW - 15	OCT-17	02-Oct-2017	13500	0	USD	13500	0	13499.7	Global Customer Operations [L7	Branch Operations (CBNA CFNA) 	CBNA Branch - NAIT CBNA [L9]	0	Branch Operations (CBNA CFNA) [L8]	BALANCE SHEET	BALANCE SHEET	160410	Property, Plant and Equipment (L5)	Not Applicable	Not Applicable	Not Applicable	Not Applicable	Not Applicable	Not Applicable	Not Applicable	Not Applicable	BALANCE SHEET",
    //    });
    //});
    //_hideMenu();
    loadFilesTables();
    //$("#contenttabletblFileTable").append('<button type="submit" class="btn btn-success">Save</button>');
});

function loadFilesTables() {
    var data = [{
        "LVID": 2,
        "Account": 6201240001,
        "ACCT DESCRIPTION": "IT 3rd Party Technical Service Providers - Technology Infrastructure",
        "CitiGL Local Extension Parent Account": 6201240000,
        "CitiGL Local Extension Parent Account Description": "IT 3rd Party Technical Service Providers - Technology Infrastructure",
        "GOC": 100101172,
        "Source": "Payables",
        "Category": "Purchase Invoices",
        "Vendor": "COLLABERA INCORPORATED",
        "Invoice": "NEMSIN04491904",
        "Invoice Date": "30-Sep-2017",
        "PO Number": null,
        "Description": "ITINMS_1010876053_2017-09-30_IN_TS_ST",
        "Month": "OCT-17",
        "Effective Date": "04-Oct-2017",
        "Entered DR": 1400,
        "Entered CR": 0,
        "Currency Code": "USD",
        "Accounted DR": 1400,
        "Accounted CR": 0,
        "NET": 1400,
        "MS L7": "Global Customer Operations [L7",
        "MS L8": "Branch Operations (CBNA CFNA) ",
        "MS L9": "CBNA Branch - NAIT CBNA [L9]",
        "MS L10": 0,
        "FLASH TAB": "Branch Operations (CBNA CFNA) [L8]",
        "TYPE": "3rd Party",
        "G3": "NON COMP",
        "ACCT DESCRIPTION": 561050,
        "CitiGL Local Extension Parent Account": "Technology / Communications (L5)",
        "CitiGL Local Extension Parent Account Description": "561100",
        "PMF 5": "Information Technology (L6)",
        "PMF 5 Descr": "581420",
        "PMF 6": "Information Technology - Contract Workers (L7)",
        "PMF 6 Descr": "581470",
        "PMF 7": "IT 3rd Party Technical Service Providers - Technology Infrastructure (L8)",
        "PMF 7 Descr": "581471",
        "PMF 8": "IT 3rd Party Technical Service Providers - Technology Infrastructure (L9)",
        "PMF 8 Descr": "IT 3rd Party Technical Service Providers - Technology Infrastructure (L8)",
        "PMF 9": "",
        "PMF 9 Descr": "",
        "FLASH ACCOUNT": ""
    },
 {
     "LVID": 1279,
     "Account": 6200110001,
     "ACCT DESCRIPTION": "Telecom Data Usage Expense",
     "CitiGL Local Extension Parent Account": 6200110000,
     "CitiGL Local Extension Parent Account Description": "Telecom Data Usage",
     "GOC": 100037507,
     "Source": "Payables",
     "Category": "Purchase Invoices",
     "Vendor": "LEVEL 3 COMMUNICATIONS LLC",
     "Invoice": "61298870",
     "Invoice Date": "01-Oct-2017",
     "PO Number": null,
     "Description": "19LIBXD;9724642009",
     "Month": "OCT-17",
     "Effective Date": "12-Oct-2017",
     "Entered DR": 0,
     "Entered CR": 0,
     "Currency Code": "USD",
     "Accounted DR": 0,
     "Accounted CR": 0,
     "NET": 0.06,
     "MS L7": "Global Customer Operations [L7",
     "MS L8": "Branch Operations (CBNA CFNA) ",
     "MS L9": "CBNA Branch - CTI NA Infrastru",
     "MS L10": 0,
     "FLASH TAB": "Branch Operations (CBNA CFNA) [L8]",
     "TYPE": "3rd Party",
     "G3": "NON COMP",
     "ACCT DESCRIPTION": 561050,
     "CitiGL Local Extension Parent Account": "Technology / Communications (L5)",
     "CitiGL Local Extension Parent Account Description": "562100",
     "PMF 5": "Communications (L6)",
     "PMF 5 Descr": "562103",
     "PMF 6": "Telecom Voice / Data (L7)",
     "PMF 6 Descr": "562105",
     "PMF 7": "Telecom Data Networks (L8)",
     "PMF 7 Descr": "562120",
     "PMF 8": "Telecom Data Usage (L9)         ",
     "PMF 8 Descr": "Telecom Data Networks (L8)",
     "PMF 9": "",
     "PMF 9 Descr": "",
     "FLASH ACCOUNT": ""
 }];
    var source =
    {
        localdata: data,
        datatype: "json",
        datafields:
        [
            { name: 'LVID', type: 'number' },
            { name: 'Account', type: 'number' },
            { name: 'ACCT DESCRIPTION', type: 'string' },
            { name: 'CitiGL Local Extension Parent Account', type: 'number' },
            { name: 'CitiGL Local Extension Parent Account Description', type: 'string' },
            { name: 'GOC', type: 'number' },
            { name: 'Source', type: 'string' },
            { name: 'Category', type: 'string' },
            { name: 'Vendor', type: 'string' },
            { name: 'Invoice', type: 'number' },
            { name: 'Invoice Date', type: 'string' },
            { name: 'PO Number', type: 'number' },
            { name: 'Description', type: 'string' },
            { name: 'Month', type: 'string' },
            { name: 'Effective Date', type: 'string' },
            { name: 'Entered DR', type: 'number' },
            { name: 'Entered CR', type: 'number' },
            { name: 'Currency Code', type: 'number' },
            { name: 'Accounted DR', type: 'number' },
            { name: 'Accounted CR', type: 'number' },
            { name: 'NET', type: 'number' },
            { name: 'MS L7', type: 'string' },
            { name: 'MS L8', type: 'string' },
            { name: 'MS L9', type: 'string' },
            { name: 'MS L10', type: 'number' },
            { name: 'FLASH TAB', type: 'string' },
            { name: 'TYPE', type: 'string' },
            { name: 'G3', type: 'string' },
            { name: 'ACCT DESCRIPTION', type: 'number' },
            { name: 'CitiGL Local Extension Parent Account', type: 'string' },
            { name: 'CitiGL Local Extension Parent Account Description', type: 'string' },
            { name: 'PMF 5', type: 'string' },
            { name: 'PMF 5 Descr', type: 'string' },
            { name: 'PMF 6', type: 'string' },
            { name: 'PMF 6 Descr', type: 'string' },
            { name: 'PMF 7', type: 'string' },
            { name: 'PMF 7 Descr', type: 'string' },
            { name: 'PMF 8', type: 'string' },
            { name: 'PMF 8 Descr', type: 'string' },
            { name: 'PMF 9', type: 'string' },
            { name: 'PMF 9 Descr', type: 'string' },
            { name: 'FLASH ACCOUNT', type: 'string' }
        ],
        datatype: "json"
    };
    var addfilter = function () {
        var filtergroup = new $.jqx.filter();
        var filter_or_operator = 1;
        var filtervalue = 'FLASH TAB';
        var filtercondition = 'equal';
        var filter1 = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);

        filtergroup.addfilter(filter_or_operator, filter1);
        // add the filters.
        $("#divPivotGrid").jqxGrid('addfilter', 'FLASH TAB', filtergroup);
        // apply the filters.
        $("#divPivotGrid").jqxGrid('applyfilters');
    }
    var dataAdapter = new $.jqx.dataAdapter(source);
    // initialize jqxGrid
    $("#divPivotGrid").jqxGrid(
    {
        autoheight: true,
        source: dataAdapter,
        altrows: true,
        sortable: true,
        ready: function () {
            addfilter();
        },
        autoshowfiltericon: true,
        columns: [
                  { text: 'LVID', datafield: 'LVID', filtertype: 'LVID', width: 80, },
                  { text: 'Account', datafield: 'Account', filtertype: 'Account', },
                  { text: 'ACCT DESCRIPTION', datafield: 'ACCT DESCRIPTION', filtertype: 'ACCT DESCRIPTION', },
                  { text: 'CitiGL Local Extension Parent Account', datafield: 'CitiGL Local Extension Parent Account', filtertype: 'CitiGL Local Extension Parent Account', },

                  



        ]
    });

    $('#events').jqxPanel({ width: 300, height: 80 });
    $("#divPivotGrid").on("filter", function (event) {
        $("#events").jqxPanel('clearcontent');
        var filterinfo = $("#divPivotGrid").jqxGrid('getfilterinformation');
        var eventData = "Triggered 'filter' event";
        for (i = 0; i < filterinfo.length; i++) {
            var eventData = "Filter Column: " + filterinfo[i].filtercolumntext;
            $('#events').jqxPanel('prepend', '<div style="margin-top: 5px;">' + eventData + '</div>');
        }
    });
    $('#clearfilteringbutton').jqxButton({ height: 25 });
    $('#filterbackground').jqxCheckBox({ checked: true, height: 25 });
    $('#filtericons').jqxCheckBox({ checked: false, height: 25 });
    // clear the filtering.
    $('#clearfilteringbutton').click(function () {
        $("#divPivotGrid").jqxGrid('clearfilters');
    });
    // show/hide filter background
    $('#filterbackground').on('change', function (event) {
        $("#divPivotGrid").jqxGrid({ showfiltercolumnbackground: event.args.checked });
    });
    // show/hide filter icons
    $('#filtericons').on('change', function (event) {
        $("#divPivotGrid").jqxGrid({ autoshowfiltericon: !event.args.checked });
    });
}

_showLoadingFullPage({
    msg: "Test Loading"
});


//button click
_hideLoadingFullPage()


