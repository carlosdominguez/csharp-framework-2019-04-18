//TODO: Put in a global JS
function fnLoadFlashCustomSegments() {
    $.jqxGridApi.create({
        showTo: "#flashSegmentsGrid",
        options: {
            width: "470px",
            autoheight: true,
            autorowheight: true,
            //selectionmode: "multiplerows",
            showfilterrow: true,
            sortable: true,
            editable: true
            //'singlerow', 'multiplerows', 'checkbox', 'multiplerowsadvanced' or 'multiplerowsextended'.
        },
        sp: {
            Name: "[dbo].[spFlashGetAllFlashSegments]",
            Params: []
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set"
        },
        groups: [],
        columns: [
            { name: 'ID', type: 'int', hidden: true },
            { name: 'FlashSegmentName', text: 'Segment Name', width: '180px', type: 'string', editable: false, cellsalign: 'center', align: 'center' },
            { name: 'CreatedBy', text: 'Created By', width: '100px', type: 'string', editable: false, cellsalign: 'center', align: 'center' },
            { name: 'CreatedDate', text: 'Creation Date', width: '180px', type: 'string', cellsformat: 'D', cellsalign: 'center', align: 'center' },
        ],
        ready: function () {
            $("#flashSegmentsGrid").on('rowselect', function (event) {
                $("#flashbtn").prop("disabled", false);
                //$("#bridgeBtn").prop("disabled", false);
                $("#sendParameter").attr("href", _getViewVar("SubAppPath") + "/FlashTool/FlashFpa?ID=" + event.args.row.ID + "&name=" + event.args.row.FlashSegmentName);
                $("#sendBridgeParameter").attr("href", _getViewVar("SubAppPath") + "/FlashTool/FlashFpa?ID=" + event.args.row.ID + "&name=" + event.args.row.FlashSegmentName);
            });

        }
    });
}
function fnGoToSegments() {

    var htmlContentModal = 'Choose at least one segment';
    var formHtml =
                '<div class="content-body">' +
                    '<div class="center">' +
                        '<div id="flashSegmentsGrid"></div>' +
                        '<a id="sendParameter">' +
                            '<button type="button" id="flashbtn" class="btnDelete btn btn-info top15 left15 bottom15 pull-right">' +
                                '<i class="fa fa-external-link"></i> Go to segment' +
                            '</button>' +
                        '</a>' +
                    '</div>' +
                '</div>';
    fnLoadFlashCustomSegments();
    _showModal({
        modalId: "modalUpload",
        width: '35%',
        //buttons: [{
        //    name: "<i class='fa fa-save'></i> OK",
        //    class: "btn-success",
        //    closeModalOnClick: true,
        //    onClick: function ($modal) {
        //        //Validate required Comment
        //        window.location.href = _getViewVar("SubAppPath") + '/FlashTool/FlashCategory';
        //    }
        //}],
        addCloseButton: false,
        title: "Message: ",
        contentHtml: formHtml,
        onReady: function ($modal) { }
    });

}
//Fn to get Date
function fnGetDate() {
    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();
    if (dateObj.getDay() == 1) {
        lYesterday = day - 3;
        console.log("Es Lunes " + year + "-" + month + "-" + lYesterday);
        return gTodayDate = year + "-" + month + "-" + lYesterday;
    }
    console.log("no es lunes " + year + "-" + month + "-" + day);
    return gTodayDate = year + "-" + month + "-" + day;
}
//Fn to get value of QS
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

console.log(getParameterByName('ID'));
if (typeof getParameterByName('ID') === 'undefined' || !getParameterByName('ID')) {
    console.log('Entre');
    fnGoToSegments();
} else {
    fnLoadFPA();
}
$(document).ready(function () {
    $("#showModalAddComment").click(function () {
        showModalAddComment();
    });

    $("#exportToExcel").click(function () {
        _downloadExcel({
            spName: "[spFlashGetSegmentsByFlashLevelFPA]",
            spParams: [
                { "Name": "@pIdSegment", "Value": getParameterByName('ID') },
                { "Name": "@pToday", "Value": fnGetDate() }
            ],
            filename: "FP&A-" + getParameterByName('name')+ "-" + fnGetDate(),
            success: {
                msg: "Please wait, generating report..."
            }
        });      
    });
});
function getDate(lVal) {
    var lDate = new Date();
    var lMonth = lDate.getMonth() + 1;
    if (lVal == 'm') {
        return lMonth
    }
    if (lVal == 'y') {
        return lDate.getFullYear()
    }
    //return lVal == 'm' ? lDate.getFullYear() : lMonth ;
}

function fnLoadFPA() {
    $("#nameOfSegment").html("FP&A <br />" + getParameterByName('name'));
    var columnsSum = function (row, columnfield, value, defaulthtml, columnproperties) {
        var rows = $('#GridFPA').jqxGrid('getrows');
        var result = "";
        for (var i = 0; i < rows.length; i++) {
            firstColumnData.push(rows[i].FlashIDCCAR);
        }
        console.log(firstColumnData);
    }
   // columnsSum();
    //add color to cells
    var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
        if (columnfield == 'CCAR') {
            if (value == "") {
                value = 0;
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#GridFPA").dataAdapter.formatNumber(value, "c2"); + '</b></span></div></center>';
            } else {
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#GridFPA").dataAdapter.formatNumber(value, "c2"); + '</b></span></div></center>';
            }
        }
        if (columnfield == 'Outllok') {
            if (value == "") {
                value = 0;
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#GridFPA").dataAdapter.formatNumber(value, "c2"); + '</b></span></div></center>';
            } else {
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#GridFPA").dataAdapter.formatNumber(value, "c2"); + '</b></span></div></center>';
            }
        }
        if (columnfield == 'Accrual') {
            if (value == "") {
                value = 0;
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#GridFPA").dataAdapter.formatNumber(value, "c2"); + '</b></span></div></center>';
            } else {
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#GridFPA").dataAdapter.formatNumber(value, "c2"); + '</b></span></div></center>';
            }
        }
        if (columnfield == 'UnpostedCanada') {
            if (value == "") {
                value = 0;
                return '<center><div style="background-color:lightBlue; line-height: 25px;;"><span><b>' + $.jqxGridApi.localStorageFindById("#GridFPA").dataAdapter.formatNumber(value, "c2"); + '</b></span></div></center>';
            } else {
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#GridFPA").dataAdapter.formatNumber(value, "c2"); + '</b></span></div></center>';
            }
        }
        if (columnfield == 'APS') {
            if (value == "") {
                value = 0;
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#GridFPA").dataAdapter.formatNumber(value, "c2"); + '</b></span></div></center>';
            } else {
                return '<center><div style="background-color:lightBlue; line-height: 25px;"><span><b>' + $.jqxGridApi.localStorageFindById("#GridFPA").dataAdapter.formatNumber(value, "c2"); + '</b</span></div></center>';
            }
        }
        if (columnfield == 'PriorActual') {
            //console.log(data);
            if (value == "") {
                value = 0;
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#GridFPA").dataAdapter.formatNumber(value, "c2") + '</b></span></div>';
            } else {
                return '<div style="line-height: 25px; text-align: right;"><span>' + $.jqxGridApi.localStorageFindById("#GridFPA").dataAdapter.formatNumber(value, "c2") + '</b></span></div>';
            }
        }
    }
    var toThemeProperty = function (className) {
        return className + " " + className + "-" + theme;
    }
    var columncheckboxRenderer = function (row, column, value) {
        console.log('aa');
    }

    //_downloadExcel({
    //    spName: "[spMakerCheckerListIssues]",
    //    spParams: [
    //        { "Name": "@Year", "Value": getSelectedYear() },
    //        { "Name": "@Quarter", "Value": getSelectedQuarter() }
    //    ],
    //    filename: "MakerCheckerIssueList-" + moment().format('MMM-DD-YYYY-HH-MM'),
    //    success: {
    //        msg: "Please wait, generating report..."
    //    }
    //});

    //_downloadExcel({
    //    sql: sql,
    //    filename: _createCustomID() + "_QueryManagerReport.xls",
    //    success: {
    //        msg: "Generating report... Please Wait, this operation may take some time to complete."
    //    }
    //});

    //the magic for the grid
    $.jqxGridApi.create({
        showTo: "#GridFPA",
        options: {
            //for comments or descriptions
            height: "500",
            editable: true,
            //autoshowfiltericon: true,
            groupable: true,
            filterable: true,
            sortable: true,
            autorowheight: false,
            showfilterrow: true,
            showstatusbar: true,
            statusbarheight: 25,
            selectionmode: 'singlecell',
            showaggregates: true,
            selectionmode: "singlerow",
        },
        sp: {
            Name: "[dbo].[spFlashGetSegmentsByFlashLevelFPA]",
            Params: [
                    { Name: "@pIdSegment", Value: getParameterByName('ID') },
                    { Name: "@vCurrentMonth", Value: getDate('m') }
            ]
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set",
            //rows: data
        },
        //groups: ['SubCategory'],

        columns: [
            {
                name: 'ExpenseCategory', datafield: 'SubCategory', text: 'Expense Category', width: '150px', type: 'string', align: 'center',
                filtertype: 'checkedlist', editable: false, align: 'center'
            },
            {
                name: 'FlashAccount', text: 'Expense Account', width: '200px', type: 'string', filtertype: 'input', editable: false,
                cellsalign: 'Left', align: 'center'
            },
             //Hidden Columns Start
            {
                name: 'PriorActual', datafield: 'PriorActual', text: 'Prior Actual', width: '190px', type: 'number', filtertype: 'input', cellsformat: 'c2',
                editable: false, cellsalign: 'right', align: 'Center',
                hidden: 'True'
               
            },
            {
                name: 'today', datafield: 'today', text: 'ELR (P2P)', width: '150px', type: 'number', filtertype: 'input', cellsalign: 'right',
                align: 'center', cellsformat: 'c2', editable: false,
                hidden: 'True'       
            },
            {
                name: 'yesterday', datafield: 'yesterday', text: 'Yesterday ELR', width: '180px', groupable: true, type: 'number', filtertype: 'input',
                cellsalign: 'center', align: 'center', cellsformat: 'c2', editable: false,
                hidden: 'True'
                
            },
            {
                name: 'Accrual', width: '120px', groupable: true, type: 'number', filtertype: 'input', cellsalign: 'center', align: 'center',
                cellsrenderer: cellsrenderer, hidden: 'True'
            },
            {
                name: 'UnpostedCanada', text: 'Unposted ', width: '120px', groupable: true, type: 'number', filtertype: 'input', cellsalign: 'center',
                align: 'center', cellsrenderer: cellsrenderer, cellsformat: 'c2',
                hidden: 'True'
            },
            {
                name: 'APS', width: '120px', groupable: true, type: 'number', filtertype: 'input', cellsalign: 'center', align: 'center', cellsformat: 'c2', editable: false,
                hidden: 'True'
            },
            {
                name: 'BAW', width: '120px', groupable: true, type: 'number', filtertype: 'input', cellsalign: 'center', align: 'center', cellsformat: 'c2', editable: false,
                hidden: 'True'
            },
            ////listo elr+ var sum=(accrual + unposted + aps)
            {
                name: 'Month', text: 'Flash', width: '120px', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center', editable: false, cellsformat: 'c2',
                cellsrenderer:
                function (index, datafield, value, defaultvalue, column, rowdata) {
                    if (rowdata.today == null || rowdata.today === "") {
                        rowdata.today = 0;
                    }
                    if (rowdata.Accrual == null || rowdata.Accrual === "") {
                        rowdata.Accrual = 0;
                    }
                    if (rowdata.UnpostedCanada == null || rowdata.UnpostedCanada === "") {
                        rowdata.UnpostedCanada = 0;
                    }
                    if (rowdata.APS == null || rowdata.APS === "") {
                        rowdata.APS = 0;
                    }
                    var total = parseFloat(rowdata.today) + parseFloat(rowdata.Accrual) + parseFloat(rowdata.UnpostedCanada) + parseFloat(rowdata.APS);
                    //totalFlash = total;
                    // rowdata.Month = total;
                    // $("#Grid").jqxGrid('updaterow', rowdata.uid, rowdata);

                    if (rowdata.Month != total) {
                        $("#GridFPA").jqxGrid('setcellvalue', rowdata.uid, "Month", total);
                        this.totalFlash = total;

                    }


                    return "<div style='margin: 4px;' class='jqx-right-align'>" + $.jqxGridApi.localStorageFindById("#GridFPA").dataAdapter.formatNumber(total, "c2"); + "</div>";
                },
                aggregates: [{
                    '<b id="flashTotal">Total':
                    function (aggregatedValue, currentValue, column, record) {
                        var total = $("#GridFPA").jqxGrid('getcolumnaggregateddata', 'Month', ['sum']);
                        sum = total.sum
                        return sum
                        '</b>'
                    }

                }],

            },
            //flash menos previous mont tiene que ser primero flash --ToDo
            //{ name: 'Comment', width: '110px', type: 'numer', filtertype: 'input', editable: false, editable: false, align: 'center' },
            //2 columnas que [pueden ingresar datos]
            {
                name: 'CCAR', text: 'CCAR', width: '110px', type: 'number', filtertype: 'input', align: 'center', cellsrenderer: cellsrenderer,
                cellsformat: 'c2',  aggregates: ['sum'], editoptions: {
                    dataInit: function (elem) {
                        $(elem).numeric();
                    }
                },
                cellsrenderer:
                function (index, datafield, value, defaultvalue, column, rowdata) {
                    if (rowdata.CCAR == null || rowdata.CCAR === "") {
                        rowdata.CCAR = 0;
                    }
                    return "<div style='margin: 4px; background-color:lightBlue; line-height: 25px;' class='jqx-right-align'>" + $.jqxGridApi.localStorageFindById("#GridFPA").dataAdapter.formatNumber(rowdata.CCAR, "c2"); + "</div>";
                },

            },
             // a partir de aqui calculado ver excel
            {   //flash menos ccar
                name: 'FlashIDCCAR', text: 'Flash I/(D) CCAR', width: '150px', type: 'number', filtertype: 'input', editable: false,
                align: 'center', cellsformat: 'c2',
                cellsrenderer:
                function (index, datafield, value, defaultvalue, column, rowdata) {
                    var total = parseFloat(rowdata.Month) - parseFloat(rowdata.CCAR);
                    return "<div style='margin: 4px;' class='jqx-right-align'>" + $.jqxGridApi.localStorageFindById("#GridFPA").dataAdapter.formatNumber(total, "c2"); + "</div>";
                },
                aggregates: [{
                    '<b>Total</b>':
                    function (aggregatedValue, currentValue, column, record) {
                        var lFlash = $("#GridFPA").jqxGrid('getcolumnaggregateddata', 'Month', ['sum']);
                        var lOutlook = $("#GridFPA").jqxGrid('getcolumnaggregateddata', 'CCAR', ['sum']);

                        var uno = lFlash.sum;
                        var dos = lOutlook.sum;
                        var resta = uno - dos;

                        //sum = lFlash.sum + lOutlook.sum;
                        //console.log('sum '+ sum);
                        return resta;
                    }
                }]

            },
            {
                name: 'Outlook', text: 'Outlook', width: '110px', type: 'number', filtertype: 'input', cellsrenderer: cellsrenderer,
                aggregates: ['sum'], align: 'center', cellsformat: 'c2', editoptions: {
                    dataInit: function (elem) {
                        $(elem).numeric();
                    }
                },
                cellsrenderer:
               function (index, datafield, value, defaultvalue, column, rowdata) {
                   if (rowdata.Outllok == null || rowdata.Outllok === "") {
                       rowdata.Outllok = 0;
                   }
                   return "<div style='margin: 4px; background-color:lightBlue; line-height: 25px;' class='jqx-right-align'>" + $.jqxGridApi.localStorageFindById("#GridFPA").dataAdapter.formatNumber(rowdata.Outllok, "c2"); + "</div>";
               },
            },
            {   ///flash menos outlook
                name: 'FlashIDOutlook', text: 'Flash I/(D) Outlook', width: '130px', type: 'number', filtertype: 'input', editable: false,
                align: 'center', aggregates: ['sum'], cellsformat: 'c2',
                cellsrenderer:
                function (index, datafield, value, defaultvalue, column, rowdata) {
                    var total = parseFloat(rowdata.Outllok) - parseFloat(rowdata.Month);
                    return "<div style='margin: 4px;' class='jqx-right-align'>" + $.jqxGridApi.localStorageFindById("#GridFPA").dataAdapter.formatNumber(total, "c2"); + "</div>";
                },
                aggregates: [{
                    '<b>Total</b>':
                    function (aggregatedValue, currentValue, column, record) {
                        var lFlash = $("#GridFPA").jqxGrid('getcolumnaggregateddata', 'Month', ['sum']);
                        var lOutlook = $("#GridFPA").jqxGrid('getcolumnaggregateddata', 'Outlook', ['sum']);

                        var uno = lFlash.sum;
                        var dos = lOutlook.sum;
                        var resta = uno - dos;

                        //sum = lFlash.sum + lOutlook.sum;
                        //console.log('sum '+ sum);
                        return resta;
                    }
                }]
            },
            {
                name: 'Comment', width: '80px', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center', editable: false, cellsrenderer: function (rowIndex, datafield, value) {
                    var dataRecord = $("#GridFPA").jqxGrid('getrowdata', rowIndex);
                    //console.log(dataRecord.Comment);
                    var htmlComment = "<div style='padding: 10px; font-size: 15px; max-height: 500px; overflow-y:scroll; width: 520px;'>";
                    htmlComment += "<b>Main drivers:</b> <br />" + dataRecord.Comment + "";
                    htmlComment += "</div>";
                    var htmlResult = "";
                    if (dataRecord.Comment) {
                        htmlResult +=
                            '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + htmlComment + '" data-title="Variance explanation:" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                            '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png"/> ' +
                            '</div>';
                    }
                    return htmlResult;
                }
            }

        ],
        ready: function () {
            $("#GridFPA").on('cellvaluechanged', function (event) {
                //Allow only one default
                if (event.args.datafield == "check") {
                    console.log(event)
                    var dataRecord = $("#GridFPA").jqxGrid('getrowdata', event.args.rowindex);
                    if (event.args.newvalue) {
                        dataRecord.UnpostedCanada = dataRecord.prevMonth - dataRecord.APS - dataRecord.Elr;
                    } else {
                        dataRecord.UnpostedCanada = 0;
                    }
                    //Set new value
                    $("#GridFPA").jqxGrid('updaterow', event.args.rowindex, dataRecord);
                }
            });

            $("#GridFPA").on("rowclick", function (event) {
                _GLOBAL_SETTINGS.tooltipsPopovers();
            });

        }
    });

}

function showModalAddComment() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#GridFPA', true);
    if (rowData) {
        //var lIdComment = rowData["IDComment"];
        var lSegment = getParameterByName('ID');
        var lComment = rowData["Comment"];
        var CCAR = rowData["CCAR"] === 'undefined' ? 0 : rowData["CCAR"];
        var Outlook = rowData["Outlook"] === 'undefined' ? 0 : rowData["Outlook"];

        var htmlContentModal = '';
        htmlContentModal += "<b>Variance explanation: </b><br/>";
        htmlContentModal += "<b>CCAR: </b>" + CCAR + "<br/>";
        htmlContentModal += "<b>Outlook: </b>" + Outlook + "<br/>";
        htmlContentModal += "<textarea class='form-control txtDetailComment' placeholder='(optional)' style='height:100px !important;'>" + lComment + "</textarea>";

        _showModal({
            modalId: "modalUpload",
            width: '70%',
            buttons: [{
                name: "<i class='fa fa-save'></i> Save",
                class: "btn-success",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    //Add Comments
                    lComment = $(".txtDetailComment").val();
                   
                    addDetailComment(lSegment, lComment, rowData["FlashAccount"], rowData["ExpenseCategory"], CCAR, Outlook, function () {
                        _showNotification("success", "Explanation was save successfully", "AlertReject");
                        rowData["Comment"] = lComment;
                        $('#GridFPA').jqxGrid('updaterow', rowData.boundindex, rowData);
                        //Close Modal
                        $modal.find(".close").click();
                    });
                }
            }],
            addCloseButton: true,
            title: "Variance explanation: Main drivers",
            contentHtml: htmlContentModal,
            onReady: function ($modal) { }
        });
    }

}
function addDetailComment(lSegment, lComment, FlashAccount, ExpenseCategory, CCAR, Outlook,   onSuccess) {
    //var lTotalPrior = this.priorActual.replace(/^"(.*)"$/, '$1');
    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Adding explanation...",
        name: "[dbo].[spFlashSaveFPAComments]",
        params: [
            { "Name": "@pSegmentId", "Value": lSegment },
            { "Name": "@pComment", "Value": lComment },
            { "Name": "@pFlashAccount", "Value": FlashAccount },
            { "Name": "@pExpenseCategory", "Value": ExpenseCategory },
            { "Name": "@pCCAR", "Value": CCAR },
            { "Name": "@pOutlook", "Value": Outlook },
            { "Name": "@pMonth", "Value": getDate('m') },
            { "Name": "@pYear", "Value": getDate('y') },
            { "Name": "@pSoeid", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                //Return New ID
                _showNotification("success", "Data was added successfully.", "AlertAddComment");

                //$modal.find(".closeModal-modalUpload").click();
                //insertToFlashBridge();
                //
                onSuccess();
            }
        }
    });

}

_showLoadingFullPage({
    msg: "Loading Segments"
});


//button click
_hideLoadingFullPage()


