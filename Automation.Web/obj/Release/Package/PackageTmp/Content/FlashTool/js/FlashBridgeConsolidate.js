$(document).ready(function () {
    //fnGoToSegments();
    $("#generateConsolidate").click(function () {
        fnGenerateSummary();
    });
    $("#showModalAddComment").click(function () {
        showModalAddComment();
    });
    $("#FlashBridge").click(function () {
        fnGenerateFlashBridge();
    });
    
    //fnGenerateSummary();
    //$("#contenttabletblFileTable").append('<button type="submit" class="btn btn-success">Save</button>');
});

function getDate(lVal) {
    var lDate = new Date();
    var lMonth = lDate.getMonth() + 1;
    if (lVal == 'm') {
        return lMonth
    }
    if (lVal == 'y') {
        return lDate.getFullYear()
    }
    //return lVal == 'm' ? lDate.getFullYear() : lMonth ;
}

function fnGenerateSummary() {
    $.jqxGridApi.create({
        showTo: "#CTISummary",
        options: {
            //for comments or descriptions
            autoheight: true,
            editable: true,
            autorowheight: false,
            showfilterrow: true,
            sortable: true,
            altrows: true,
            selectionmode: 'singlecell',
            showstatusbar: true,
            statusbarheight: 25,
            showaggregates: true,
            edittype: 'custom',
            selectionmode: "singlerow"
            //'singlerow', 'multiplerows', 'checkbox', 'multiplerowsadvanced' or 'multiplerowsextended'.

        },
        sp: {
            Name: "[dbo].[spFlashGetBridgeConsolidate]",
            Params: [
                { Name: "@pMonth", Value: getDate('m') },
                { Name: "@pYear", Value: getDate('y') }
            ]
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set"
        },
        columns: [
            { name: 'BCIDSegment', text: 'SegmentId', width: '19%', type: 'string', filtertype: 'input', filterable: true, cellsalign: 'center', align: 'center', hidden: true },

            //one timmer van aqui 
            //one timmer del mes anterior
            //{ name: 'flashTotal', text: 'Flash Total', width: '19%', type: 'string', filtertype: 'input', filterable: true, cellsalign: 'center', align: 'center' },
            { name: 'BCSegmentDescription', text: 'Segment Description', type: 'string', width: '20%', editable: false, },
           
            //flash suma de todos los flashes
            { //suma de atodas las varianzas esta en el correo
                name: 'BCPriorActualTotal', text: 'Prior Actual', type: 'number', datafield: 'BCPriorActualTotal', width: 150, cellsalign: 'right', align: 'center', cellsformat: 'c2', editable: false,
                aggregates: [{
                    //flash - [ expected + currentmontitems] = unexplained = 0 --> should be 0
                    //----if != 0( dont allow to save)
                    '<b>Total</b>':
                    function (aggregatedValue, currentValue, column, record) {
                        var total = $("#CTISummary").jqxGrid('getcolumnaggregateddata', 'BCPriorActualTotal', ['sum']);
                        sum = total.sum
                        return sum;
                    }
                }]
            },
             {
                 //si no es cero show alert revise por que no da 0, unexplained variance should be 0

                 ///Cambiar en el storeProcedure este es PriorActual y lo muestra como como flash
                 name: 'BCFlashTotal', text: 'Flash Total', type: 'number', width: '15%', cellsalign: 'right', align: 'center', cellsformat: 'c2', editable: false,
                 aggregates: [{
                     //flash - [ expected + currentmontitems] = unexplained = 0 --> should be 0
                     //----if != 0( dont allow to save)
                     '<b>Total</b>':
                     function (aggregatedValue, currentValue, column, record) {
                         var total = $("#CTISummary").jqxGrid('getcolumnaggregateddata', 'BCFlashTotal', ['sum']);
                         sum = total.sum
                         return sum;
                     }
                 }]
             },
            //misma formula de bridge flash - prev
            
            //formula flash - prevMonth
            /*
            {//poner todas las cuentas
                name: 'ExpenseAccount', text: 'Expense Account', columntype: 'combobox', width: '19%', type: 'string', filtertype: 'input', filterable: true, cellsalign: 'center', align: 'center', list: {
                    array: ExpenseAccount,
                    datafield: 'ExpenseAccountId',
                    displayfield: 'ExpenseAccountName',
                    createeditor: function (row, value, editor) {
                        $.jqxGridApi.createEditor('combobox', 'ExpenseAccountId', row, value, editor);
                    }
                }
            },
            */
            {
                name: 'momVariance', datafield: 'momVariance', text: 'MoM Variance', width: '19%', type: 'string', filtertype: 'input', cellsalign: 'right', align: 'right', editable: false,
                aggregates: ['sum'],
                cellsrenderer:
                function (index, datafield, value, defaultvalue, column, rowdata) {
                    if (rowdata.BCPriorActualTotal == null || rowdata.BCPriorActualTotal === "") {
                        rowdata.BCPriorActualTotal = 0;
                    }
                    if (rowdata.BCFlashTotal == null || rowdata.BCFlashTotal === "") {
                        rowdata.BCFlashTotal = 0;
                    }
                    var total = parseFloat(rowdata.BCPriorActualTotal) - parseFloat(rowdata.BCFlashTotal);
                    total = total || 0;
                    return "<div style='margin: 4px;' class='jqx-right-align'>" + $.jqxGridApi.localStorageFindById("#CTISummary").dataAdapter.formatNumber(total, "c2"); + "</div>";
                },
                //aggregatesrenderer: function (aggregates, column, element) {
                //    var sumQuantity = 100;

                //    sum = sumQuantity.sum

                //    return sum;
                //}
                aggregates: [{
                    '<b>Total</b>':
                            function (aggregatedValue, currentValue, column, rowdata) {
                                if (rowdata.BCPriorActualTotal == null || rowdata.BCPriorActualTotal === "") {
                                    rowdata.BCPriorActualTotal = 0;
                                }
                                if (rowdata.BCFlashTotal == null || rowdata.BCFlashTotal === "") {
                                    rowdata.BCFlashTotal = 0;
                                }
                                var total = parseFloat(rowdata.BCPriorActualTotal) - parseFloat(rowdata.BCFlashTotal);
                                total = total || 0;
                                //var aggregatedValue = isNaN(parseInt(aggregatedValue)) ? 0 : parseInt(aggregatedValue)
                                //console.log(aggregatedValue);
                                if (isNaN(aggregatedValue)) {
                                    aggregatedValue = 0;
                                }
                                var lShowTotal = aggregatedValue + total;
                                return $.jqxGridApi.localStorageFindById("#CTISummary").dataAdapter.formatNumber(lShowTotal, "c2");
                            }
                }]

            },
            {
                name: 'Comment', width: '80px', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center', editable: false, cellsrenderer: function (rowIndex, datafield, value) {
                    var dataRecord = $("#CTISummary").jqxGrid('getrowdata', rowIndex);
                    //console.log(dataRecord.Comment);
                    var htmlComment = "<div style='padding: 10px; font-size: 15px; max-height: 500px; overflow-y:scroll; width: 520px;'>";
                    htmlComment += "<b>Main drivers:</b> <br />" + dataRecord.Comment + "";
                    htmlComment += "</div>";
                    var htmlResult = "";
                    if (dataRecord.Comment) {
                        htmlResult +=
                            '<div rel="popover" data-color-class="success" data-container="body" data-toggle="popover" data-placement="left" data-content="' + htmlComment + '" data-title="Variance explanation:" data-trigger="click" data-html="true" data-original-title="" title="" style="float: right; margin-left: 2px;"> ' +
                            '       <img style="margin-right: 5px; margin-top: 7px;" height="20" width="20" src="' + _getViewVar("SubAppPath") + '/Content/Shared/images/comment.png"/> ' +
                            '</div>';
                    }
                    return htmlResult;
                }
            }

           //{ name: 'Date', text: 'Account', width: '19%', type: 'string', filtertype: 'input' },
            //todo lo que tengo en flash detail que agarre los nietos expensed account /// intellisence
            /*
            {//jala onetimer y run rates
                name: 'ActionMonthID', text: 'One Timer / Run Rate', columntype: 'dropdownlist', type: 'string', width: '35%', cellsalign: 'center', align: 'center', list: {
                    array: source,
                    datafield: 'ActionMonthID',
                    displayfield: 'ActionMonthName',
                    text: 'One Timer  Run Rate',
                    createeditor: function (row, value, editor) {
                        $.jqxGridApi.createEditor('dropdownlist', 'ActionMonthID', row, value, editor);
                    }
                }
            },
            */
            //"month - year"
            //{ name: 'Date', text: 'Month', width: '19%', type: 'string', filtertype: 'input', filterable: true, cellsalign: 'center', align: 'center' },
            //{ name: 'Segments', text: 'Managed Segment', width: '19%', type: 'string', filtertype: 'input', filterable: true, cellsalign: 'center', align: 'center' }
            //{
            //    name: 'ddl'
            //    , text: 'One Timer / Run Rate'
            //    , width: '19%'
            //    , type: 'string'
            //    , datafield: 'name'
            //    , columntype: 'dropdownlist'

            //    , createeditor: function (row, value, editor) {
            //        //console.log('aaa');
            //        //var list=["1","2","3"]
            //        editor.jqxDropDownList({
            //            source: source, displayMember:'name', valueMember: 'value'
            //        });
            //    }
            //},
            //comboBox
            //agregar NRows para justificar 

            //columna de primero suma de subtotales de todos los onetimers del mes anterior query  select * from one timerprevmonth
            //difference vs Expected antes de currenItems     formula = flash - expected
        ],


        ready: function () {
            //Add popup of information
            $("#CTISummary").on("rowclick", function (event) {
                _GLOBAL_SETTINGS.tooltipsPopovers();
            });
        }

    });
    $("#addrowbutton").on('click', function () {
        var commit = $("#tblFileTable").jqxGrid('addrow', null, {});
    })
}
function fnGenerateFlashBridge() {
    $.jqxGridApi.create({
        showTo: "#FlashBridge",
        options: {
            //for comments or descriptions
            autoheight: true,
            editable: true,
            autorowheight: false,
            showfilterrow: true,
            sortable: true,
            altrows: true,
            selectionmode: 'singlecell',
            showstatusbar: true,
            statusbarheight: 25,
            showaggregates: true,
            edittype: 'custom',
            selectionmode: "singlerow"
            //'singlerow', 'multiplerows', 'checkbox', 'multiplerowsadvanced' or 'multiplerowsextended'.

        },
        sp: {
            Name: "[dbo].[spFlashGetFlashBridgeToNormalized]",
            Params: [
                { Name: "@pMonth", Value: getDate('m') },
                { Name: "@pYear", Value: getDate('y') }
            ]
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Large Data Set"
        },
        columns: [
            { name: 'SegmentId', text: 'SegmentId', width: '19%', type: 'string', filtertype: 'input', filterable: true, cellsalign: 'center', align: 'center', hidden: true },

            //one timmer van aqui 
            //one timmer del mes anterior
            //{ name: 'flashTotal', text: 'Flash Total', width: '19%', type: 'string', filtertype: 'input', filterable: true, cellsalign: 'center', align: 'center' },
            {
                name: 'BridgeMoM', text: 'Bridge MoM', type: 'number', width: '20%', editable: false, cellsformat: 'c2',
                aggregates: [{
                    //flash - [ expected + currentmontitems] = unexplained = 0 --> should be 0
                    //----if != 0( dont allow to save)
                    '<b>Total</b>':
                    function (aggregatedValue, currentValue, column, record) {
                        var total = $("#FlashBridge").jqxGrid('getcolumnaggregateddata', 'BridgeMoM', ['sum']);
                        sum = total.sum
                        return sum;
                    }
                }]
            },

            //flash suma de todos los flashes
            { //suma de atodas las varianzas esta en el correo
                name: 'ExpenseAccountId', text: 'ExpenseAccountId', type: 'number', datafield: 'BCPriorActualTotal', width: 150, cellsalign: 'right', align: 'center', editable: false, hidden: true
            },
            { 
                name: 'BridgeTypeId', text: 'BridgeTypeId', type: 'number', datafield: 'BCPriorActualTotal', width: 150, cellsalign: 'right', align: 'center', editable: false, hidden: true
            },
            //misma formula de bridge flash - prev
            {
                //si no es cero show alert revise por que no da 0, unexplained variance should be 0
                name: 'ExpenseAccount', text: 'Expense Account', type: 'string', width: '15%', cellsalign: 'center', align: 'center', editable: false,
            },
      
            {
                name: 'FlashSegmentName', datafield: 'FlashSegmentName', text: 'Segment Name', width: '29%', type: 'string', filtertype: 'input', cellsalign: 'center', align: 'center', editable: false,
            },
            { //suma de atodas las varianzas esta en el correo
                name: 'BridgeType', text: 'Bridge Type', type: 'string', datafield: 'BridgeType', width: 150, cellsalign: 'center', align: 'center', editable: false,
            }
           //{ name: 'Date', text: 'Account', width: '19%', type: 'string', filtertype: 'input' },
            //todo lo que tengo en flash detail que agarre los nietos expensed account /// intellisence
            /*
            {//jala onetimer y run rates
                name: 'ActionMonthID', text: 'One Timer / Run Rate', columntype: 'dropdownlist', type: 'string', width: '35%', cellsalign: 'center', align: 'center', list: {
                    array: source,
                    datafield: 'ActionMonthID',
                    displayfield: 'ActionMonthName',
                    text: 'One Timer  Run Rate',
                    createeditor: function (row, value, editor) {
                        $.jqxGridApi.createEditor('dropdownlist', 'ActionMonthID', row, value, editor);
                    }
                }
            },
            */
            //"month - year"
            //{ name: 'Date', text: 'Month', width: '19%', type: 'string', filtertype: 'input', filterable: true, cellsalign: 'center', align: 'center' },
            //{ name: 'Segments', text: 'Managed Segment', width: '19%', type: 'string', filtertype: 'input', filterable: true, cellsalign: 'center', align: 'center' }
            //{
            //    name: 'ddl'
            //    , text: 'One Timer / Run Rate'
            //    , width: '19%'
            //    , type: 'string'
            //    , datafield: 'name'
            //    , columntype: 'dropdownlist'

            //    , createeditor: function (row, value, editor) {
            //        //console.log('aaa');
            //        //var list=["1","2","3"]
            //        editor.jqxDropDownList({
            //            source: source, displayMember:'name', valueMember: 'value'
            //        });
            //    }
            //},
            //comboBox
            //agregar NRows para justificar 

            //columna de primero suma de subtotales de todos los onetimers del mes anterior query  select * from one timerprevmonth
            //difference vs Expected antes de currenItems     formula = flash - expected
        ],


        ready: function () {
            //Add popup of information
            $("#CTISummary").on("rowclick", function (event) {
                _GLOBAL_SETTINGS.tooltipsPopovers();
            });
        }

    });
    $("#addrowbutton").on('click', function () {
        var commit = $("#tblFileTable").jqxGrid('addrow', null, {});
    })
}
function showModalAddComment() {
    var rowData = $.jqxGridApi.getOneSelectedRow('#CTISummary', true);
    if (rowData) {
        //var lIdComment = rowData["IDComment"];
        var lSegment = rowData["BCIDSegment"]
        var lComment;
        var htmlContentModal = '';
        htmlContentModal += "<b>Variance explanation: </b><br/>";
        htmlContentModal += "<textarea class='form-control txtDetailComment' placeholder='(optional)' style='height:100px !important;'></textarea>";

        _showModal({
            modalId: "modalUpload",
            width: '70%',
            buttons: [{
                name: "<i class='fa fa-save'></i> Save",
                class: "btn-success",
                closeModalOnClick: false,
                onClick: function ($modal) {
                    //Add Comments
                    lComment = $(".txtDetailComment").val();
                   
                    addDetailComment(lSegment, lComment, function () {
                        _showNotification("success", "Explanation was save successfully", "AlertReject");
                        rowData["Comment"] = lComment;
                        $('#CTISummary').jqxGrid('updaterow', rowData.boundindex, rowData);
                        //Close Modal
                        $modal.find(".close").click();
                    });
                }
            }],
            addCloseButton: true,
            title: "Variance explanation: Main drivers",
            contentHtml: htmlContentModal,
            onReady: function ($modal) { }
        });
    }

}
function addDetailComment(lSegment, lComment, onSuccess) {
    //var lTotalPrior = this.priorActual.replace(/^"(.*)"$/, '$1');
    _callProcedure({
        loadingMsgType: "topBar",
        loadingMsg: "Adding Variance explanation...",
        name: "[dbo].[spFlashSaveCommentsBridgeConsolidate]",
        params: [
            { "Name": "@pSegmentId", "Value": lSegment },
            { "Name": "@pComment", "Value": lComment },
            { "Name": "@pMonth", "Value": getDate('m') },
            { "Name": "@pYear", "Value": getDate('y') },
            { "Name": "@pSoeid", "Value": _getSOEID() }
        ],
        success: {
            fn: function (responseList) {
                //Return New ID
                _showNotification("success", "Data was added successfully.", "AlertAddComment");

                //$modal.find(".closeModal-modalUpload").click();
                //insertToFlashBridge();
                //
                onSuccess();
            }
        }
    });

}
_showLoadingFullPage({
    msg: "Loading Bridge Data"
});

//button click
_hideLoadingFullPage()


