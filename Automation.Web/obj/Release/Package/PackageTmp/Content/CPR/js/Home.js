$(document).ready(function () {
    //Create MCA Summary Link
    _createMCASummaryLink({
        idContainer: "lblMCASummary"
    });

    //On Click Select Process
    $(".btnSelectProcess").click(function () {
        _createPopupSelectProcess({
            height: "500",
            onSelect: function (objProcess) {
                console.log(objProcess);
            }
        });
    });
});
