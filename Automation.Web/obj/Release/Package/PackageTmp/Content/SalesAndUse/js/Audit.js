﻿
$(document).ready(function () {
    loadAudit();

});


function loadAudit() {
    $.jqxGridApi.create({
        showTo: "#tbl_audit",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_AUDIT]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'ID', type: 'string', text: "ID", width: '5%' },
            { name: 'Module', type: 'string', text: "Module", width: '10%' },
            { name: 'DbTable', text: "DbTable", type: 'string', width: '15%' },
            { name: 'TablePK', type: 'string', text: "TablePK", width: '10%' },
            { name: 'Action', type: 'string', text: "Action", width: '10%' },
            { name: 'SOEID', text: "SOEID", type: 'string', width: '10%' },
            { name: 'AuditDate', type: 'date', text: "AuditDate", width: '10%', cellsformat: "M/d/yyyy" },
            { name: 'FieldName', type: 'string', text: "FieldName", width: '10%' },
            { name: 'OldValue', type: 'string', text: "OldValue", width: '10%' },
            { name: 'NewValue', type: 'string', text: "NewValue", width: '10%' }
        ],

        ready: function () {
            _hideLoadingFullPage();
        }
    });
}

