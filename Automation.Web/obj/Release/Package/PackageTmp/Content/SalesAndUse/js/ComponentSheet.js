﻿

$(document).ready(function () {
    loadComponentSheet();
});

function loadComponentSheet() {
    $.jqxGridApi.create({
        showTo: "#tblComponentSheet",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_CS_COMPONENTSHEET]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'idComponentSheet',type: 'string', hidden: true },
            { name: 'idJurisdictionLocation', text: "Log ID", type: 'string', width: '8%' },
            { name: 'oldLog', text: "Old Log", type: 'string', width: '8%' },
            { name: 'lvid', type: 'string', text: "LVID", width: '10%' },
            { name: 'legalEntityName', type: 'string', text: "LE", width: '20%' },
            { name: 'fein', type: 'string', text: "FEIN", width: '10%' },
            { name: 'frequency', type: 'string', text: "FREQUENCY", width: '10%' },
            { name: 'stateDivSub', type: 'string', text: "STATE", width: '10%' },
            { name: 'jurisdiction', type: 'string', text: "JURISDICTION", width: '10%' },
            { name: 'account', type: 'string', text: "ACCOUNT", width: '10%' },
            { name: 'onlineUserId', type: 'string', text: "USER ID", width: '30%' },
            { name: 'onlinePassword', type: 'string', text: "PASS", width: '30%' },
            { name: 'website', type: 'string', text: "LINK", width: '70%' },
            { name: 'bankAccount',type: 'string', hidden: true },
            { name: 'routingNumber',type: 'string', hidden: true },
            { name: 'bankInfo',type: 'string', hidden: true },
            { name: 'accrualComponent',type: 'string', hidden: true },
            { name: 'paymentBookedThru',type: 'string', hidden: true },
            { name: 'webTracking',type: 'string', hidden: true },
            { name: 'contactInformation',type: 'string', hidden: true },
            { name: 'mailingAddress',type: 'string', hidden: true },
            { name: 'comments',type: 'string', hidden: true },
            { name: 'createdBy',type: 'string', hidden: true },
            { name: 'createdDate',type: 'string', hidden: true },
            { name: 'updatedBy',type: 'string', hidden: true },
            { name: 'updatedDate',type: 'string', hidden: true }
        ],

        ready: function () {
            _hideLoadingFullPage();
            $('#tblComponentSheet').on('rowdoubleclick', function (event) {
                if (typeof event.args.group == "undefined") {
                    //_showLoadingFullPage();
                    var row = event.args.rowindex;
                    var datarow = $("#tblComponentSheet").jqxGrid('getrowdata', row);
                    showModal(datarow);
                }
            });
        }
    });
}


function showModal(row) {
        _showModal({
            modalId: "ModalComponentSheet",
            title: "Component Sheet - LogID:" + row.idJurisdictionLocation,
            width: "98%",
            contentAjaxUrl: "/SalesAndUse/ModalComponentSheet",
            contentAjaxType: "POST",
            contentAjaxParams: {
                idComponentSheet: row.idComponentSheet,
                idJurisdictionLocation: row.idJurisdictionLocation,
                oldLog: row.oldLog,
                lvid: row.lvid,
                legalEntityName: row.legalEntityName,
                fein: row.fein,
                frequency: row.frequency,
                stateDivSub: row.stateDivSub,
                jurisdiction: row.jurisdiction,
                account: row.account,
                onlineUserId: row.onlineUserId,
                onlinePassword: row.onlinePassword,
                website: row.website,
                bankAccount:row.bankAccount,
                routingNumber:row.routingNumber,
                bankInfo: row.bankInfo,
                accrualComponent: row.accrualComponent,
                paymentBookedThru: row.paymentBookedThru,
                webTracking: row.webTracking,
                contactInformation: row.contactInformation,
                mailingAddress: row.mailingAddress,
                comments: row.comments,
                createdBy: row.createdBy,
                createdDate: row.createdDate,
                updatedBy: row.updatedBy,
                updatedDate: row.updatedDate
            },
            buttons: [{
                name: "Save Changes",
                class: "btn-info",
                onClick: function ($modal) {
                    var myObject = new Object();

                    myObject.idComponentSheet = $('#txt_componentId').val();
                    myObject.onlineUserId  = $('#txt_onlineUserId').val();
                    myObject.onlinePassword = $('#txt_onlinePassword').val();
                    myObject.website = $('#txt_website').val();
                    myObject.bankAccount  = $('#txt_bankAccount').val();
                    myObject.routingNumber   = $('#txt_routingNumber').val();
                    myObject.bankInfo    = $('#txt_bankInformation').val();
                    myObject.accrualComponent   = $('#txt_accrualComponent').val();
                    myObject.paymentBookedThru   = $('#txt_paymentBooked').val();
                    myObject.webTracking      = $('#txt_webTracking').val();
                    myObject.contactInformation  = $('#txt_contactInformation').val();
                    myObject.mailingAddress     = $('#txt_mailingAddress').val();
                    myObject.comments = $('#txt_comments').val();

                    var componentSheet = JSON.stringify(myObject);

                    //INSERT NEW INFORMATION
                    _callServer({
                        url: '/SalesAndUse/ComponentUpdate',
                        closeModalOnClick: false,
                        data: { 'pjson': componentSheet },
                        type: "post",
                        success: function (savingStatus) {
                            loadComponentSheet();
                            _showNotification("success", "Component Sheet updated successfully.");
                        }
                    });
                    
                }
            }],
            onReady: function ($modal) {

            },
            class: " compactModal"
        });
}

