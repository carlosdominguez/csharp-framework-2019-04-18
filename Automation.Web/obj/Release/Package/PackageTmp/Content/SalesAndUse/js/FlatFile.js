﻿
var FFheader;
var FFinvoice;
var FFfooter;
var selectedRow;

$(document).ready(function () {
    loadFlatFileGeneral();
});


function loadFlatFileGeneral() {
    $.jqxGridApi.create({
        showTo: "#tbl_FlatFile",
        options: {
            //for comments or descriptions
            height: "400",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_FLATFILE_GENERAL]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
			{ name: 'idFlatFile', type: 'string', hidden: true },
			{ name: 'flatfileName', type: 'string', width: '30%' },
			{ name: 'totalInvoices', type: 'string', width: '5%' },
			{ name: 'invoiceP2P', type: 'string', width: '5%' },
			{ name: 'invoiceTotal', type: 'string', width: '15%' },
			{ name: 'mailType', type: 'string', width: '5%' },
			{ name: 'status', type: 'string', width: '10%' },
			{ name: 'playbackImported', type: 'string', width: '10%' },
			{ name: 'runDate', type: 'string', width: '10%' },
			{ name: 'createdBy', type: 'string', width: '10%' }
        ],


        ready: function () {
            _hideLoadingFullPage();

            $('#tbl_FlatFile').on('rowdoubleclick', function (event) {
                if (typeof event.args.group == "undefined") {
                    //_showLoadingFullPage();
                    var row = event.args.rowindex;
                    var datarow = $("#tbl_FlatFile").jqxGrid('getrowdata', row);
                    showFlatFileModal(datarow);
                }
            });

            $('#tbl_FlatFile').on('rowclick', function (event) {
                if (typeof event.args.group == "undefined") {
                    //_showLoadingFullPage();
                    var row = event.args.rowindex;
                    var datarow = $("#tbl_FlatFile").jqxGrid('getrowdata', row);
                    selectedRow = datarow;
                }
            });
          
        }
    });
}

function showFlatFileModal(datarow) {

    _showModal({
        modalId: "ModalFlatFile",
        title: "FLAT FILE",
        width: "98%",
        contentAjaxUrl: "/SalesAndUse/ModalFlatFile",
        contentAjaxParams: {
            idFlatFile: datarow.idFlatFile,
            flatfileName: datarow.flatfileName
        },
        onReady: function ($modal) {      
            //LOAD HEADER
            _callServer({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading Preview Header...",
                url: '/Ajax/ExecQuery',
                data: { 'pjsonSql': JSON.stringify("SELECT [header] FROM [tblFlatFileHeader] WHERE [idFlatFile] = " + datarow.idFlatFile + "") },
                type: "post",
                success: function (resultList) {

                    var objFunction = resultList[0];
                    $("#txt_flatfile_header").val(objFunction.header);
                    FFheader = objFunction.header + '\r\n';
                }
            });

            //LOAD INVOICE
            _callServer({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading Preview Header...",
                url: '/Ajax/ExecQuery',
                data: { 'pjsonSql': JSON.stringify("[dbo].[NSP_LT_FLATFILE_OLD] " + datarow.idFlatFile + "") },
                type: "post",
                success: function (resultList) {
                    $("#txt_flatfile_invoice").val("");
                    for (var i = 0; i < resultList.length; i++) {
                        var objFunction = resultList[i];

                        var fields = objFunction.Return.split('NEWL%');

                        var textarea = document.getElementById("txt_flatfile_invoice");
                        textarea.value = fields.join("\r\n");
                        FFinvoice = fields.join("\r\n");
                        FFinvoice = FFinvoice + '\r\n';
                    }
                }
            });
            //LOAD FOOTER
            _callServer({
                loadingMsgType: "fullLoading",
                loadingMsg: "Loading Preview Header...",
                url: '/Ajax/ExecQuery',
                data: { 'pjsonSql': JSON.stringify("SELECT [footer] FROM [SAU].[dbo].[tblFlatFileFooter] WHERE [idFlatFile] = " + datarow.idFlatFile + "") },
                type: "post",
                success: function (resultList) {
                    var objFunction = resultList[0];
                    $("#txt_flatfile_footer").val(objFunction.footer);
                    FFfooter = objFunction.footer + '\r\n';
                }
            });

        },
        class: " compactModal"
    });
}

function showPlaybackModal() {
    if (selectedRow != null) {
        _showModal({
            modalId: "ModalPlayback",
            title: "PLAYBACK FLAT FILE",
            width: "98%",
            contentAjaxUrl: "/SalesAndUse/ModalPlayback",
            contentAjaxParams: {
                idFlatFile: selectedRow.idFlatFile,
                flatfileName: selectedRow.flatfileName
            },
            onReady: function ($modal) {

                $('#fine-uploader-manual-trigger').fineUploader({
                    template: 'qq-template-manual-trigger',
                    request: {
                        endpoint: '/SalesAndUse/UploadReport1'
                    },
                    thumbnails: {
                        placeholders: {
                            waitingPath: '/Content/Shared/plugins/fine-uploader/placeholders/waiting-generic.png',
                            notAvailablePath: '/Content/Shared/plugins/fine-uploader/placeholders/not_available-generic.png'
                        }
                    },
                    callbacks: {
                        onAllComplete: function (succeeded, failed) {
                            if (failed.length == 0) {
                                _showAlert({
                                    type: "success",
                                    title: "Message",
                                    content: "The file was uploaded successfully"
                                });
                            }
                        },
                        onError: function (id, name, errorReason, xhrOrXdr) {
                            if (name) {
                                _showDetailAlert({
                                    title: "Message",
                                    shortMsg: "An error ocurred with the file '" + name + "'.",
                                    longMsg: errorReason,
                                    type: "Error",
                                    viewLabel: "View Details"
                                });
                            }
                        },
                    },
                    autoUpload: false
                });
                $('#trigger-upload').click(function () {
                    $('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');
                    loadFlatFileGeneral();
                });

                if (selectedRow.playbackImported == "YES") {
                    $('#fine-uploader-manual-trigger').attr("disabled", "disabled");
                    $('#trigger-upload').attr("disabled", "disabled");
                }

                //LOAD PLAYBAK INFO & TABLE
                loadPlaybackInfoTable();


                if (_getUserInfo().Roles.includes('FLATFILE')) {
                    $("#fine-uploader-manual-trigger").prop("disabled", false);
                    $("#trigger-upload").prop("disabled", false);
                } else {
                    $("#fine-uploader-manual-trigger").prop("disabled", true);
                    $("#trigger-upload").prop("disabled", true);
                }

                _callServer({
                    loadingMsgType: "fullLoading",
                    loadingMsg: "Loading Log ID Details...",
                    url: '/Ajax/ExecQuery',
                    data: { 'pjsonSql': JSON.stringify("NSP_FF_PLAYBACK_GENERAL " + selectedRow.idFlatFile + "") },
                    type: "post",
                    success: function (resultList) {
                            var objFunction = resultList[0];
                            $("#lbl_p_fileName").text(objFunction.playbackFileName);
                            $("#lbl_p_invoices").text(objFunction.countInvoice);
                            $("#lbl_p_totalAmount").text(objFunction.Total);
                            $("#lbl_p_date").text(objFunction.PlaybackImportDate);
                    }
                });
            },
            class: " compactModal"
        });
    }
}

function downloadFF() {
    var FFinvoice_NEW = FFinvoice.substring(0, FFinvoice.length - 2);

    var ffDownload = FFheader + FFinvoice_NEW + FFfooter;



    var ffName = $("#lblFlatFileName").text();

    //DOWNLOAD TXT
    download(ffName, ffDownload);

}

function loadPlaybackInfoTable() {
    $.jqxGridApi.create({
        showTo: "#tblPlaybackDetails",
        options: {
            //for comments or descriptions
            height: "250",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_FF_PLAYBACK]",
            Params: [
                { Name: "@idFlatFile", Value: selectedRow.idFlatFile }
            ]
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
			{ name: 'InvoiceNumber', type: 'string', width: '10%' },
			{ name: 'invoiceAmount', type: 'string', width: '10%' },
			{ name: 'LogID', type: 'string', width: '5%' },
			{ name: 'P2PXactn', type: 'string', width: '10%' },
			{ name: 'FFExportDate', type: 'string', width: '10%' },
			{ name: 'PlaybackImportDate', type: 'string', width: '10%' },
			{ name: 'playbackFileName', type: 'string', width: '45%' }
        ],


        ready: function () {
            _hideLoadingFullPage();


        }
    });
}