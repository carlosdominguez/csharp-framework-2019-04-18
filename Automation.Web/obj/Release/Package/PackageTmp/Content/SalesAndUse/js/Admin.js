﻿
//#region Variables
var tableNameActual = "tblAdmin_LegalEntity";
var tableIdActual = "idLegalEntity";
var selectLegalEntity = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT [idLegalEntity] as [id],[legalEntityName] as [text] FROM [SAU].[dbo].[tblAdmin_LegalEntity] WHERE [active] = 1";
var selectCorpCode = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT [idCorpCode] as [id],[corpCode] as [text] FROM [SAU].[dbo].[tblAdmin_CorpCode] where [active] = 1;";
var selectFein = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT  [idFEIN] as [id],[fein] as [text] FROM [SAU].[dbo].[tblAdmin_FEIN] where [active] = 1;";
var selectType = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT [idType] as [id],[type] as [text] FROM [SAU].[dbo].[tblAdmin_Type] WHERE active = 1 ";
var selectP2PAccount = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT [idP2PAccount] as [id],[account] as [text] FROM [SAU].[dbo].[tblAdmin_P2PAccount] where active = 1;";
//#endregion

//#region Document Ready
$(document).ready(function () {

    //CREATE DROPLIST TABLE BUTTON
    $("#jqxdropdownbutton").jqxDropDownButton({ template: "summer", width: "100%", height: 30 });

    //LOAD MASS TABLES
    loadPartialViewDBTable();

    //LOAD DROPLISTS
    loadDropDowns(selectLegalEntity, "0", "drp_LVID_LVID_LegalEntity");
    loadDropDowns(selectCorpCode, "0", "drp_LVID_LVID_P2PCorpCode");
    loadDropDowns(selectFein, "0", "drp_LVID_LVID_FEIN");
    loadDropDowns(selectType, "0", "drp_Checklist_Checklist_Type");
    loadDropDowns(selectP2PAccount, "0", "drp_P2P_LVID_Account");

    //LOAD MANUAL TABLES
    loadLVID();
    loadChecklist();
    loadDropDownLVIDs();
    loadP2PGocLvid();
});
//#endregion

//#region ON CLICK TABS
$('.tag-switch a').click(function () {
    var contentid = $(this).attr("href");
    var contentfirst = contentid + " li:first";
    $(".level2 ul li").removeClass("active");
    $(contentfirst).addClass("active");
    var tabName = $(contentfirst).attr("value");
    var tableName = $(contentfirst).attr("valueTable");
    var tableId = $(contentfirst).attr("tableid");
    $(contentfirst).click();
    var contentfirst = contentid + " li:first a";
    $(contentfirst).click();
})

$('.tab-pane a').click(function () {
    var tabName = $(this).attr("value");
    tableNameActual = $(this).attr("valueTable");
    tableIdActual = $(this).attr("tableid");

    loadPartialViewDBTable();

})
//#endregion

//#region LOAD TABLE MASS
function loadPartialViewDBTable() {

    if (tableNameActual && tableIdActual) {
        _callServer({
            loadingMsg: "Loading table '" + tableNameActual + "' information...",
            url: "/SalesAndUse/PartialViewDBTable?" + $.param({
                ptbl: tableNameActual,
                pcolKey: tableIdActual,
                pidElementContent: "content-db-table"
            }),
            success: function (htmlResponse) {
                $("#content-db-table").html(htmlResponse);
            }
        });
    }

};
//#endregion

//#region LOAD SINGLE TABLE

function loadLVID() {
    $.jqxGridApi.create({
        showTo: "#tbl_LVID",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_ADMIN_LVID]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
			{ name: 'idLVID', type: 'string', hidden: true },
			{ name: 'lvid', type: 'string', width: '10%' },
			{ name: 'legalEntityName', type: 'string', width: '30%' },
			{ name: 'corpCode', type: 'string', width: '10%' },
			{ name: 'fein', type: 'string', width: '20%' },
			{ name: 'entityId', type: 'string', width: '20%' },
			{ name: 'active', type: 'string', width: '10%' }
        ],

        ready: function () {
            _hideLoadingFullPage();
            $('#tbl_LVID').on('rowdoubleclick', function (event) {
                // event.args.rowindex is a bound index.
                if (typeof event.args.group == "undefined") {
                    var row = event.args.rowindex;
                    var datarow = $("#tbl_LVID").jqxGrid('getrowdata', row);



                    $('#txt_LVID_LVID_ID').val(datarow.idLVID);

                    $('#txt_LVID_LVID_LVID').val(datarow.lvid);

                    setDroplistValue(datarow.legalEntityName, 'drp_LVID_LVID_LegalEntity');
                    setDroplistValue(datarow.corpCode, 'drp_LVID_LVID_P2PCorpCode');
                    setDroplistValue(datarow.fein, 'drp_LVID_LVID_FEIN');

                    $('#txt_LVID_LVID_EntityID').val(datarow.entityId);

                    if (datarow.active == "True") {
                        $("#chk_LVID_LVID_Active").prop("checked", true);
                    } else {
                        $("#chk_LVID_LVID_Active").prop("checked", false);
                    }


                    $("#btnLvid_Update").prop("disabled", false);
                    $("#btnLvid_AddNew").prop("disabled", true);

                }
            });
        }
    });
}

function loadChecklist() {
    $.jqxGridApi.create({
        showTo: "#tbl_Checklist",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_ADMIN_CHECKLIST]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
			{ name: 'idChecklist', type: 'string', hidden: true },
			{ name: 'checklist', type: 'string', width: '50%' },
			{ name: 'type', type: 'string', width: '10%' },
			{ name: 'description', type: 'string', width: '20%' },
			{ name: 'order', type: 'string', width: '10%' },
			{ name: 'active', type: 'string', width: '10%' }
        ],

        ready: function () {
            _hideLoadingFullPage();
            $('#tbl_Checklist').on('rowdoubleclick', function (event) {
                // event.args.rowindex is a bound index.
                if (typeof event.args.group == "undefined") {
                    var row = event.args.rowindex;
                    var datarow = $("#tbl_Checklist").jqxGrid('getrowdata', row);

                    $('#txt_Checklist_Checklist_ID').val(datarow.idChecklist);
                    $('#txt_Checklist_Checklist_Checklist').val(datarow.checklist);
                    $('#txt_Checklist_Checklist_Order').val(datarow.order);
                    

                    setDroplistValue(datarow.type, 'drp_Checklist_Checklist_Type');


                    if (datarow.active == "True") {
                        $("#chk_Checklist_Checklist_Active").prop("checked", true);
                    } else {
                        $("#chk_Checklist_Checklist_Active").prop("checked", false);
                    }


                    $("#btnChecklist_Update").prop("disabled", false);
                    $("#btnChecklist_AddNew").prop("disabled", true);
                }
            });
        }
    });
}

function loadP2PGocLvid() {
    $.jqxGridApi.create({
        showTo: "#tbl_P2PLVID",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_ADMIN_P2PLVID]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
			{ name: 'idP2PGocsLvid', type: 'string', hidden: true },
			{ name: 'lvid', type: 'string', width: '10%' },
			{ name: 'goc', type: 'string', width: '15%' },
			{ name: 'account', type: 'string', width: '15%' },
			{ name: 'corpCode', type: 'string', width: '10%' },
			{ name: 'fein', type: 'string', width: '10%' },
			{ name: 'companyName', type: 'string', width: '10%' },
            { name: 'legalEntityName', type: 'string', width: '20%' },
            { name: 'active', type: 'string', width: '20%' }
        ],

        ready: function () {
            _hideLoadingFullPage();
            $('#tbl_P2PLVID').on('rowdoubleclick', function (event) {
                // event.args.rowindex is a bound index.
                //if (typeof event.args.group == "undefined") {
                var row = event.args.rowindex;
                var datarow = $("#tbl_P2PLVID").jqxGrid('getrowdata', row);

                var rows1 = $('#tblLVIDMasterDrop').jqxGrid('getrows');
                for (var y = 0; y < rows1.length; y++) {
                    if (datarow.lvid == rows1[y].lvid) {
                        $("#tblLVIDMasterDrop").jqxGrid('selectrow', y);
                        if (datarow.active == "True") {
                            $("#chk_P2P_LVID_Active").prop("checked", true);
                        } else {
                            $("#chk_P2P_LVID_Active").prop("checked", false);
                        }
                    }
                }

                $("#txt_P2P_LVID_GOC").val(datarow.goc);
                $("#txt_P2P_LVID_CompanyName").val(datarow.companyName);
                $("#txt_P2P_LVID_GOC_ID").val(datarow.idP2PGocsLvid);




                $("#drp_P2P_LVID_Account option").filter(function () {
                    return $(this).text() == datarow.account;
                }).prop('selected', true);

                $("#btnP2PLvidGoc_Update").prop("disabled", false);
                $("#btnP2PLvidGoc_AddNew").prop("disabled", true);


            });
        }
    });
}

//#region LOAD TABLE DROPLIST LVID-GOC P2P
function loadDropDownLVIDs() {
    $.jqxGridApi.create({
        showTo: "#tblLVIDMasterDrop",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[NSP_LT_ADMIN_LVID]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
			{ name: 'idLVID', type: 'string', hidden: true },
			{ name: 'lvid', type: 'string', width: '20%' },
			{ name: 'legalEntityName', type: 'string', width: '40%' },
			{ name: 'corpCode', type: 'string', width: '20%' },
			{ name: 'fein', type: 'string', width: '20%' }
        ],

        ready: function () {
            _hideLoadingFullPage();
            //$('#tblLVIDMasterDrop').on('rowdoubleclick', function (event) {
            //    // event.args.rowindex is a bound index.
            //    if (typeof event.args.group == "undefined") {
            //        var row = event.args.rowindex;
            //        var datarow = $("#tblLVIDMasterDrop").jqxGrid('getrowdata', row);
            //        // showEntitiMasterModal(datarow);
            //    }
            //});

            $("#tblLVIDMasterDrop").on('rowselect', function (event) {
                var args = event.args;
                var row = $("#tblLVIDMasterDrop").jqxGrid('getrowdata', args.rowindex);
                var dropDownContent = '<div id="selectedLVIDID" style="position: relative; width:100%; margin-left: 3px; margin-top: 5px;" idLVID="' + row.idLVID + '" >' + row['lvid'] + ' - ' + row['legalEntityName'] + '</div>';
                $("#jqxdropdownbutton").jqxDropDownButton('setContent', dropDownContent);


                $('#txt_P2P_LVID_CorpCode').val(row['corpCode']);
                $('#txt_P2P_LVID_FEIN').val(row['fein']);
                $('#txt_P2P_LVID_LegalName').val(row['legalEntityName']);

                $("#txt_P2P_LVID_GOC").focus();

            });



        }
    });
}


//#endregion

//#endregion

//#region LOAD DROPLISTS
function loadDropDowns(selectQuery, selectedId, dropList) {
    //Set default value  
    _callServer({
        loadingMsg: "Loading Global Process...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify(selectQuery) },
        type: "post",
        success: function (resultList) {
            $("#" + dropList + "").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $("#" + dropList + "").append($('<option>', { value: objFunction.id, text: objFunction.text, selected: (objFunction.id == selectedId) }));
            }
        }
    });
};

//#endregion

//#region UTILITIES

$(function () {
    $('a[title]').tooltip();
});

function setDroplistValue(textToSelect, droplist) {
    var dd = document.getElementById(droplist);
    for (var i = 0; i < dd.options.length; i++) {
        if (dd.options[i].text == textToSelect) {
            dd.selectedIndex = i;
            break;
        }
    }
}


//#endregion

//#region P2P TABS ___________________________________________________________________
function cleanP2PLVID() {
    $("#jqxdropdownbutton").val("");
    $("#txt_P2P_LVID_GOC").val("");
    $("#drp_P2P_LVID_Account").val("0");
    $("#txt_P2P_LVID_CorpCode").val("");
    $("#txt_P2P_LVID_FEIN").val("");
    $("#txt_P2P_LVID_CompanyName").val("");
    $("#txt_P2P_LVID_LegalName").val("");
    $("#chk_P2P_LVID_Active").prop("checked", false);
    $("#btnP2PLvidGoc_Update").prop("disabled", true);
    $("#btnP2PLvidGoc_AddNew").prop("disabled", false);
}
//-- Clean Fields
$("#btnClearFields_goc_lvid").click(function () {
    cleanP2PLVID();

});
//-- Click Update
$("#btnP2PLvidGoc_Update").click(function () {

    // SET VARIABLES 
    var myObject = new Object();
    myObject.lvidId = $("#txt_P2P_LVID_GOC_ID").val();
    myObject.lvidGocsId = $("#selectedLVIDID").attr("idLVID");
    myObject.goc = $('#txt_P2P_LVID_GOC').val();
    myObject.accountId = $("#drp_P2P_LVID_Account option:selected").val();
    myObject.companyName = $('#txt_P2P_LVID_CompanyName').val();

    if ($('#chk_P2P_LVID_Active').val() != 'on') {
        myObject.active = '0';
    } else {
        myObject.active = '1';
    }

    var p2pLvidGoc = JSON.stringify(myObject);



    //UPDATE INFORMATION

    //--Validate Information 
    if (myObject.lvidId != "") {
        _callServer({
            url: '/SalesAndUse/P2PLvidUPDATE',
            closeModalOnClick: false,
            data: { 'pjson': p2pLvidGoc },
            type: "post",
            success: function (savingStatus) {
                cleanP2PLVID();
                loadP2PGocLvid();
                _showNotification("success", "Data was saved successfully.");

            }
        });
    } else {
        _showNotification("error", "Please select a function or status.");
    }





});
//-- Click Add New
$("#btnP2PLvidGoc_AddNew").click(function () {

    // SET VARIABLES 
    var myObject = new Object();
    myObject.lvidId = $("#selectedLVIDID").attr("idLVID");
    myObject.goc = $('#txt_P2P_LVID_GOC').val();
    myObject.accountId = $("#drp_P2P_LVID_Account option:selected").val();
    myObject.companyName = $('#txt_P2P_LVID_CompanyName').val();

    if ($('#chk_P2P_LVID_Active').val() != 'on') {
        myObject.active = '0';
    } else {
        myObject.active = '1';
    }



    var p2pLvidGoc = JSON.stringify(myObject);

    //UPDATE INFORMATION

    _callServer({
        url: '/SalesAndUse/P2PLvidINSERT',
        closeModalOnClick: false,
        data: { 'pjson': p2pLvidGoc },
        type: "post",
        success: function (savingStatus) {
            cleanP2PLVID();
            loadP2PGocLvid();
            _showNotification("success", "Data was saved successfully.");
        }
    });
});
//#endregion _________________________________________________________________________

//#region LVID TABS __________________________________________________________________
function cleanLVID() {
    $("#txt_LVID_LVID_LVID").val("");
    $("#drp_LVID_LVID_LegalEntity").val("0");
    $("#drp_LVID_LVID_P2PCorpCode").val("0");
    $("#drp_LVID_LVID_FEIN").val("0");
    $("#txt_LVID_LVID_EntityID").val("");
    $("#chk_LVID_LVID_Active").prop("checked", false);
    $("#btnLvid_Update").prop("disabled", true);
    $("#btnLvid_AddNew").prop("disabled", false);
}
//--Clean Fields
$("#btnClearFields_LVID").click(function () {
    cleanLVID();
});

//-- Click Update
$("#btnLvid_Update").click(function () {
    // SET VARIABLES 
    var myObject = new Object();
    myObject.id = $('#txt_LVID_LVID_ID').val();
    myObject.lvid = $('#txt_LVID_LVID_LVID').val();
    myObject.legalentity = $("#drp_LVID_LVID_LegalEntity option:selected").val();
    myObject.corpcode = $("#drp_LVID_LVID_P2PCorpCode option:selected").val();
    myObject.fein = $("#drp_LVID_LVID_FEIN option:selected").val();
    myObject.entityid = $('#txt_LVID_LVID_EntityID').val();

    if ($('#chk_LVID_LVID_Active').val() != 'on') {
        myObject.active = '0';
    } else {
        myObject.active = '1';
    }

    var Lvid = JSON.stringify(myObject);

    //UPDATE INFORMATION
    if (myObject.id != "") {
        _callServer({
            url: '/SalesAndUse/LvidUPDATE',
            closeModalOnClick: false,
            data: { 'pjson': Lvid },
            type: "post",
            success: function (savingStatus) {
                cleanLVID();
                loadLVID();
                _showNotification("success", "Data was saved successfully.");
            }
        });
    } else {
        _showNotification("error", "Please select a function or status.");
    }


});

//-- Click Add New
$("#btnLvid_AddNew").click(function () {
    // SET VARIABLES 
    var myObject = new Object();
    myObject.id = $('#txt_LVID_LVID_ID').val();
    myObject.lvid = $('#txt_LVID_LVID_LVID').val();
    myObject.legalentity = $("#drp_LVID_LVID_LegalEntity option:selected").val();
    myObject.corpcode = $("#drp_LVID_LVID_P2PCorpCode option:selected").val();
    myObject.fein = $("#drp_LVID_LVID_FEIN option:selected").val();
    myObject.entityid = $('#txt_LVID_LVID_EntityID').val();

    if ($('#chk_LVID_LVID_Active').val() != 'on') {
        myObject.active = '0';
    } else {
        myObject.active = '1';
    }

    var Lvid = JSON.stringify(myObject);

    //UPDATE INFORMATION

    _callServer({
        url: '/SalesAndUse/LvidINSERT',
        closeModalOnClick: false,
        data: { 'pjson': Lvid },
        type: "post",
        success: function (savingStatus) {
            cleanLVID();
            loadLVID();
            _showNotification("success", "Data was saved successfully.");
        }
    });
});

//#endregion _________________________________________________________________________

//#region CHECKLIST TABS _____________________________________________________________

function CleanChecklist() {
    $("#txt_Checklist_Checklist_ID").val("");
    $("#drp_Checklist_Checklist_Type").val("0");
    $("#txt_Checklist_Checklist_Checklist").val("");
    $("#txt_Checklist_Checklist_Order").val("");
    $("#chk_Checklist_Checklist_Active").prop("checked", false);
    $("#btnChecklist_Update").prop("disabled", true);
    $("#btnChecklist_AddNew").prop("disabled", false);

}

//--Clean Fields
$("#btnClearFields_Checklist").click(function () {
    CleanChecklist();
});

//-- Click Update
$("#btnChecklist_Update").click(function () {
    // SET VARIABLES 
    var myObject = new Object();
    myObject.id = $('#txt_Checklist_Checklist_ID').val();
    myObject.type = $("#drp_Checklist_Checklist_Type option:selected").val();
    myObject.checklist = $('#txt_Checklist_Checklist_Checklist').val();
    myObject.order = $('#txt_Checklist_Checklist_Order').val();

    switch ($('#chk_Checklist_Checklist_Active').is(':checked')) {
        case true: myObject.active = 1;
            break;
        case false: myObject.active = 0;
            break;
    }

    //if ($('#chk_Checklist_Checklist_Active').val() != 'on') {
    //    myObject.active = '0';
    //} else {
    //    myObject.active = '1';
    //}

    var checklist = JSON.stringify(myObject);

    //UPDATE INFORMATION

    _callServer({
        url: '/SalesAndUse/ChecklistUPDATE',
        closeModalOnClick: false,
        data: { 'pjson': checklist },
        type: "post",
        success: function (savingStatus) {
            CleanChecklist();
            loadChecklist();
            _showNotification("success", "Data was saved successfully.");
        }
    });
});

//-- Click Add New
$("#btnChecklist_AddNew").click(function () {
    // SET VARIABLES 
    var myObject = new Object();
    myObject.id = $('#txt_Checklist_Checklist_ID').val();
    myObject.type = $("#drp_Checklist_Checklist_Type option:selected").val();
    myObject.checklist = $('#txt_Checklist_Checklist_Checklist').val();
    myObject.order = $('#txt_Checklist_Checklist_Order').val();

    if ($('#chk_Checklist_Checklist_Active').val() != 'on') {
        myObject.active = '0';
    } else {
        myObject.active = '1';
    }

    var checklist = JSON.stringify(myObject);

    //UPDATE INFORMATION

    _callServer({
        url: '/SalesAndUse/ChecklistINSERT',
        closeModalOnClick: false,
        data: { 'pjson': checklist },
        type: "post",
        success: function (savingStatus) {
            CleanChecklist();
            loadChecklist();
            _showNotification("success", "Data was saved successfully.");
        }
    });
});

//#endregion _________________________________________________________________________


