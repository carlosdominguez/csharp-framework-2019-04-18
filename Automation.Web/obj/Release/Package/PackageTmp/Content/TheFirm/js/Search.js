﻿var taxID, docID;
$(document).ready(function () {
    validateUser();
    initDropListLoads();
    $("#txt_dueDateFrom").datepicker();
    $("#txt_dueDateTo").datepicker();
    $("#jqxdropdownbutton_list_logID").jqxDropDownButton({ template: "summer", width: "100%", height: 30 });
});

//TABLE LOADS
//ELEMENTS LOAD
function initDropListLoads() {
    var qry_team = "SELECT [teamID] AS [id], [teamDescription] AS [text] FROM [dbo].[Team];",
        qry_fyear = "SELECT [filingYear] AS [id],  CONVERT(NVARCHAR(50),[filingYear]) AS [text] FROM [dbo].[TaxDocument] GROUP BY [filingYear],[filingYear] ORDER BY [text] ASC;",
        qry_dType = "SELECT  [documentTypeDescription] AS [id], DT.[documentTypeDescription] AS [text] FROM [dbo].[TaxDocument] TD JOIN [dbo].[Document] D ON TD.[docID] = D.[documentID] JOIN [dbo].[DocumentType] DT ON D.[documentTypeID] = DT.[documentTypeID] GROUP BY DT.[documentTypeID],DT.[documentTypeDescription];";

    loadDropDowns(qry_team, "drp_Team");
    $("#drp_FilingMonth").val(null).trigger("change");
    loadDropDowns(qry_fyear,"drp_FilingYear");
    loadDropDowns(qry_dType, "drp_DocumentType");
    
}

function loadDropDownLogID(value) {
    $.jqxGridApi.create({
        showTo: "#tbldropLogID",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[spFirmListLogIdentifierByTeam]",
            Params: [
                { Name: "@teamID", Value: value }
            ]
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
			{ name: 'logID', type: 'string', hidden: true },
            { name: 'logNumber', type: 'string', width: '40%' },
            { name: 'frequencyDescription', type: 'string', width: '60%' }
        ],

        ready: function () {
            _hideLoadingFullPage();


            $("#tbldropLogID").on('rowselect', function (event) {
                var args = event.args;
                var row = $("#tbldropLogID").jqxGrid('getrowdata', args.rowindex);
                var dropDownContent = '<div id="selectedLogID" style="position: relative; width:100%; margin-left: 3px; margin-top: 5px;" idLogID="' + row.logID + '" >' + row['logNumber'] + ' - ' + row['frequencyDescription'] + '</div>';
                $("#jqxdropdownbutton_list_logID").jqxDropDownButton('setContent', dropDownContent);
                $("#jqxdropdownbutton_list_logID").jqxDropDownButton('close');

                console.log($("#jqxdropdownbutton_list_logID").val(), $("#selectedLogID")[0].attributes[2].nodeValue);

            });

        }
    });
}
//TRAN METHODS
function loadDropDowns(selectQuery, dropList) {
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading details for search...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify(selectQuery) },
        type: "post",
        success: function (resultList) {
            $("#" + dropList + "").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $("#" + dropList + "").append($('<option>', { value: objFunction.id, text: objFunction.text }));
            }
            $("#" + dropList + "").val(null).trigger("change");
            if (dropList == "drp_Team")
                validateUser();
        }
    });
};

function validateEmpty(value) {
    var val;
    if (value == null || value == "") {
        val = '0';
    } else {
        val = value.toString();
    }
    return val;
}

function validateUser() {

    //if (!_getUserInfo().Roles.includes("SUPER_ADMIN")) {
    //    $("#drp_Team").prop("disabled", true);
    //    _callServer({
    //        loadingMsgType: "fullLoading",
    //        loadingMsg: "Loading Log ID Details...",
    //        url: '/Ajax/ExecQuery',
    //        data: { 'pjsonSql': JSON.stringify(" SELECT [teamID] FROM [dbo].[UserTable] WHERE [SOEID] ='" + _getUserInfo().SOEID + "'") },
    //        type: "post",
    //        success: function (resultList) {
    //            $("#drp_Team").val(resultList[0].teamID);
    //            $("#drp_Team").val(resultList[0].teamID).trigger("change");
    //        }
    //    });
    //}

    if (!_getUserInfo().Roles.includes("ADMIN") && !_getUserInfo().Roles.includes("MANAGER")) {
        $("#DeletePanel").hide();
    }
}

//BUTTONS ACTIONS

$("#btnSearch").click(function () {
    _showLoadingFullPage({ msg: "Loading Results..." });
    $.jqxGridApi.create({
        showTo: "#tbl_SearchResult",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true  
        },
        sp: {
            Name: "[dbo].[spFirmListSearch]",
            Params: [
                { Name: "@soeid", Value: validateEmpty($("#drp_SOEID :selected").val()) },
                { Name: "@logid", Value: validateEmpty($("#selectedLogID").attr("idLogID")) },
                { Name: "@fmonth", Value: validateEmpty($("#drp_FilingMonth :selected").val()) },
                { Name: "@fyear", Value: validateEmpty($("#drp_FilingYear :selected").val()) },
                { Name: "@docType", Value: validateEmpty($("#drp_DocumentType :selected").text()) },
                { Name: "@team", Value: validateEmpty($("#drp_Team :selected").text()) },
                { Name: "@keyword", Value: validateEmpty($("#txt_Keywords").val()) },
                { Name: "@fdateFrom", Value: validateEmpty($("#txt_dueDateFrom").val()) },
                { Name: "@fdateTo", Value: validateEmpty($("#txt_dueDateTo").val()) }
            ]
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
            { name: 'logID', type: 'string', hidden: true },
            { name: 'versionNumber', text: "Version #", type: 'int', width: '5%' },
            { name: 'logNumber', text: "Log ID", type: 'int', width: '5%' },
            { name: 'filingYear', text: "Year", type: 'int', width: '5%' },
            { name: 'filingMonth', text: "Month", type: 'int', width: '5%' },
            { name: 'docName', text: "Document Name", type: 'string', width: '35%' },
            { name: 'teamDescription', text: "Team", type: 'string', width: '13%' },
            { name: 'documentTypeDescription', text: "Doc Type", type: 'string', width: '10%' },
            { name: 'auditTimestamp', text: "Date", type: 'Date', width: '10%' },
            { name: 'searchKeys', text: "Keywords", type: 'string', width: '6%' },
            { name: 'SOEID', text: "SOEID", type: 'string', width: '6%' },
            { name: 'docLocation', type: 'string', hidden: true },
            { name: 'taxDocumentID', type: 'string', hidden: true },
            { name: 'documentID', type: 'string', hidden: true }
        ],

        ready: function () {
            //COUNT TOTAL IN TABLE
            _hideLoadingFullPage();
            var rowCount = $("#tbl_SearchResult").jqxGrid('getrows').length;
            $("#tableRowCount").text('Row count: ' + rowCount);

            $('#tbl_SearchResult').on('rowdoubleclick', function (event) {
                // event.args.rowindex is a bound index.
                if (typeof event.args.group == "undefined") {
                    var row = event.args.rowindex;
                    var datarow = $("#tbl_SearchResult").jqxGrid('getrowdata', row);

                    $('#linkFile').attr("href", datarow.docLocation + "\\" + datarow.docName);
                    $('#linkFile').click();
                }
            });

            $('#tbl_SearchResult').on('rowclick', function (event) {
                // event.args.rowindex is a bound index.
                if (typeof event.args.group == "undefined") {
                    var row = event.args.rowindex;
                    var datarow = $("#tbl_SearchResult").jqxGrid('getrowdata', row);

                    $("#txt_DocumentName").val(datarow.docName);
                    $("#txt_Team").val(datarow.teamDescription);
                    $("#txt_LogID").val(datarow.logNumber);
                    taxID = datarow.taxDocumentID;
                    docID = datarow.documentID;
                }
            });

        }
    });
});

$("#linkFile").click(function (e) {

    var getselectedrowindexes = $('#tbl_SearchResult').jqxGrid('getselectedrowindexes');
    var selectedRowData = $('#tbl_SearchResult').jqxGrid('getrowdata', getselectedrowindexes[0]);

    var $btn = $(this);

    e.preventDefault();

    _callServer({
        url: '/TheFirm/DownloadPDF',
        data: {
            'thefirmpath': selectedRowData.docLocation,
            'fileName': selectedRowData.docName
        },
        type: "post",
        success: function (urlToDownload) {
            window.open(_replaceAll("~", "", urlToDownload), '_blank');
        }
    });
});

$("#drp_Team").change(function () {
    if ($("#drp_Team :selected").val()) {
        var qry_soeid = "SELECT FWU.[SOEID] AS [id] , FWU.[Name] + ' ('+ FWU.[SOEID]+')' AS [text]  FROM [dbo].[tblFirmAFrwkUser] FWU JOIN [dbo].[UserTable] UT ON FWU.[SOEID] = UT.[SOEID] WHERE FWU.[isDeleted] = 0  ORDER BY FWU.[SOEID];";
        //"SELECT FWU.[SOEID] AS [id] , FWU.[Name] + ' ('+ FWU.[SOEID]+')' AS [text]  FROM [dbo].[tblFirmAFrwkUser] FWU JOIN [dbo].[UserTable] UT ON FWU.[SOEID] = UT.[SOEID] WHERE FWU.[isDeleted] = 0 AND UT.[teamID] = '" + $("#drp_Team :selected").val() + "'  ORDER BY FWU.[SOEID];";
        loadDropDowns(qry_soeid, "drp_SOEID");
        $("#jqxdropdownbutton_list_logID").val("");
        loadDropDownLogID($("#drp_Team :selected").val());
    }
});

$("#drp_Team").select2({
    placeholder: 'Choose a Team:',
    allowClear: true,
    clear: true
}).on('select2-open', function () {
    // Adding Custom Scrollbar 
    $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
});

$("#drp_SOEID").select2({
    placeholder: 'Choose a SOEID:',
    allowClear: true,
    clear: true 
}).on('select2-open', function () {
    // Adding Custom Scrollbar 
    $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
});

$("#drp_LogId").select2({
    placeholder: 'Choose a Log Identifier:',
    allowClear: true

}).on('select2-open', function () {
    // Adding Custom Scrollbar 
    $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
});

$("#drp_FilingMonth").select2({
    placeholder: 'Choose a Document Type:',
    allowClear: true
}).on('select2-open', function () {
    // Adding Custom Scrollbar 
    $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
});

$("#drp_FilingYear").select2({
    placeholder: 'Choose a Filling Year:',
    allowClear: true
}).on('select2-open', function () {
    // Adding Custom Scrollbar 
    $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
});

$("#drp_DocumentType").select2({
    placeholder: 'Choose a Document Type:',
    allowClear: true
}).on('select2-open', function () {
    // Adding Custom Scrollbar 
    $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
});

$("#btnDeleteDoc").click(function () {
    var htmlContentModal = "<b>This action will delete de selected document " + $("#txt_DocumentName").val()  + "<br/>";
    _showModal({
        width: '35%',
        modalId: "modalDel",
        addCloseButton: true,
        buttons: [{
            name: "Yes",
            class: "btn-danger",
            onClick: function () {
                _callProcedure({
                    loadingMsgType: "fullLoading",
                    loadingMsg: "Updating Docs..",
                    name: "[dbo].[spFirmDeleteDocument]",
                    params: [
                        { Name: '@taxDocumentID', Value: taxID },
                        { Name: '@documentID', Value: docID },
                        { Name: '@SOEID', Value: _getUserInfo().SOEID }
                    ],
                    success: {
                        fn: function (responseList) {
                            //console.log(responseList[0].docLocation + '\\' + $("#txt_DocumentName").val());
                            _callServer({
                                loadingMsgType: "fullLoading",
                                loadingMsg: "Checking documents in Share Folder...",
                                url: '/TheFirm/DeleteFile',
                                data: {
                                    thefirmpath: _encodeSkipSideminder(responseList[0].docLocation + '\\' + $("#txt_DocumentName").val())
                                },
                                success: function (resultMsg) {
                                    _showNotification("success", "File have been deleted");
                                    //$("#tbl_SearchResult").jqxGrid('clear');
                                    $("#btnSearch").trigger("click");
                                }
                            });
                           
                        }
                    },
                });
            }
        }],
        title: "Are you sure you want to delete this document?",
        contentHtml: htmlContentModal
    });
});