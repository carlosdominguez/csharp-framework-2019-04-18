﻿

$(document).ready(function () {
    initUpload();
    initUpload1();
    initDropListLoads();
    $("#jqxdropdownbutton_list_logID").jqxDropDownButton({ template: "summer", width: "100%", height: 30 });
    validateUser();
});

//TABLE LOADS
function initUpload() {
    var fnClickUpload = function () {
        if (!validateEmpty()) {
            _showNotification("error", "Empty field found. Please select all the necessary details.");
        } else {
            validateDocVersion(function (version) {
                getExpirationDate($("#drp_FileRetenPeriod :selected").val(), function (ExpirationDate) {
                    startUploading(version, ExpirationDate);
                });
            });

            function startUploading(version, ExpirationDate) {
                var FileName = getNewFileName(version);
                var docDetails = getDocDetails(version);

                $('#fine-uploader-manual-trigger').fineUploader('setParams', {
                    'newFileName': FileName,
                    'docExpDate': ExpirationDate,
                    'jsonDocDetails': _toJSON(docDetails)
                });

                $('#fine-uploader-manual-trigger').fineUploader('uploadStoredFiles');

                //$('#fine-uploader-manual-trigger').fineUploader('reset')
            }
        }
    };

    $('#fine-uploader-manual-trigger').fineUploader({
        template: 'qq-template-manual-trigger',
        request: {
            endpoint: '/TheFirm/UploadFile'
        },
        thumbnails: {
            placeholders: {
                waitingPath: '/Content/Shared/plugins/fine-uploader/placeholders/waiting-generic.png',
                notAvailablePath: '/Content/Shared/plugins/fine-uploader/placeholders/not_available-generic.png'
            }
        },
        callbacks: {
            onComplete: function (id,name,resonseJSON,xhr) {
                console.log(id, name, resonseJSON, xhr)
            },
            onAllComplete: function (succeeded, failed) {
                //console.log(succeeded, failed, this);
                if (failed.length == 0) {
                    _showAlert({
                        type: "success",
                        title: "Message",
                        content: "The file was uploaded successfully"
                    });
                    clearSingleUpload();
                    $('#fine-uploader-manual-trigger').fineUploader('reset');

                    $('#trigger-upload').click(fnClickUpload);

                }
            },
            onError: function (id, name, errorReason, xhrOrXdr) {
                if (name) {
                    _showDetailAlert({
                        title: "Message",
                        shortMsg: "An error ocurred with the file '" + name + "'.",
                        longMsg: errorReason,
                        type: "Error",
                        viewLabel: "View Details"
                    });
                }
            },
            onSubmit: function (id, name) {
                //Add parameter of locationToSave
                $('#fine-uploader-manual-trigger').fineUploader('setParams', {
                    'team': $("#drp_Team :selected").val(),
                    'logid': $("#txt_logIdentifer").val()
                });
            },
            onManualRetry: function (id, name) {
                //Add parameter of locationToSave
                $('#fine-uploader-manual-trigger').fineUploader('setParams', {
                    'team': $("#drp_Team :selected").val(),
                    'logid': $("#txt_logIdentifer").val()
                });
            }
        },
        validation: {
            allowedExtensions: ['pdf'],
            itemLimit: 1
        },
        autoUpload: false
    });
    $('#trigger-upload').click(fnClickUpload);
}

function initUpload1() {
    $('#fine-uploader-manual-trigger1').fineUploader({
        template: 'qq-template-manual-trigger1',
        request: {
            endpoint: '/TheFirm/UploadFileM'
        },
        thumbnails: {
            placeholders: {
                waitingPath: '/Content/Shared/plugins/fine-uploader/placeholders/waiting-generic.png',
                notAvailablePath: '/Content/Shared/plugins/fine-uploader/placeholders/not_available-generic.png'
            }
        },
        callbacks: {
            onComplete: function (id,name,resonseJSON,xhr) {
                //console.log(id, name, resonseJSON, xhr)
            },
            onAllComplete: function (succeeded, failed) {
                if (failed.length == 0) {
                    _showAlert({
                        type: "success",
                        title: "Message",
                        content: "The file was uploaded successfully"
                    });
                   
                }
            },
            onError: function (id, name, errorReason, xhrOrXdr) {
                var response = JSON.parse(xhrOrXdr.responseText);
                if (name) {
                    _showDetailAlert({
                        title: "Message",
                        shortMsg: "An error ocurred with the file '" + name + "'.",
                        longMsg: response["msg"],
                        type: "Error",
                        viewLabel: "View Details"
                    });
                }
            },
            onSubmit: function (id, name) {
                //Add parameter of locationToSave
                $('#fine-uploader-manual-trigger').fineUploader('setParams', {
                    'team': $("#drp_Team :selected").val(),
                    'logid': $("#txt_logIdentifer").val()
                });
            },
            onManualRetry: function (id, name) {
                //Add parameter of locationToSave
                $('#fine-uploader-manual-trigger').fineUploader('setParams', {
                    'team': $("#drp_Team :selected").val(),
                    'logid': $("#txt_logIdentifer").val()
                });
            }
        },
        validation: {
            allowedExtensions: ['pdf']
        },
        autoUpload: false
    });
    $('#trigger-upload2').click(function () {
        if (false) {
            _showNotification("error", "Empty field found. Please select all the necessary details.");
        } else {
            getExpirationDate("699F457F-04AF-41C7-802C-162FC649D525", function (ExpirationDate) {
                StartUploading( ExpirationDate);
            });

            function StartUploading(ExperationDate) {
                var docDetailsM = { teamID: $("#drp_TeamM :selected").val(), filingM: $("#drp_FilingMonthM :selected").val(), filingY: (new Date()).getFullYear(), keys: $("#txt_KeywordsM").val(), teamDescrip: $("#drp_TeamM :selected").text().replace(/\s/g, ''), docType: $("#drp_DocTypeM :selected").val() }

                $('#fine-uploader-manual-trigger1').fineUploader('setParams', {
                    'jsonDocDetails': _toJSON(docDetailsM),
                    'docExpDate': ExperationDate
                });

                $('#fine-uploader-manual-trigger1').fineUploader('uploadStoredFiles')
            }
        }
    });
}

//ELEMENTS LOAD
function initDropListLoads() {
    var qry_team = "SELECT [teamID] AS [id], [teamDescription] AS [text] FROM [dbo].[Team];",
        qry_dType = "SELECT [documentTypeID] as [id] ,[documentTypeDescription] as [text] FROM [TheFirm].[dbo].[DocumentType] WHERE [active] = 1 order by [text];",
        qry_dRType = "SELECT [id] as [id] ,[specification] as [text] FROM [TheFirm].[dbo].[DocumentRetentionType] WHERE [active] = 1;";



    loadDropDowns(qry_team, "drp_Team");
    loadDropDowns(qry_team, "drp_TeamM");
    $("#drp_FilingMonth").val(null).trigger("change");
    $("#drp_FilingMonthM").val(null).trigger("change");
    loadDropDowns(qry_dType, "drp_DocType");
    loadDropDowns(qry_dType, "drp_DocTypeM");
    loadDropDowns(qry_dRType, "drp_FileRetenPeriod", function () {
        $("#drp_FileRetenPeriod").val($("#drp_FileRetenPeriod").find("option").first().val());
        $("#drp_FileRetenPeriod").trigger("change");
    });
    $("#txt_FilingYear").val("2017");
    
    //$("#drp_FileRetenPeriod").select2('0');
}

function loadDropDownLogID(value) {
    $.jqxGridApi.create({
        showTo: "#tbldropLogID",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[spFirmListLogIdentifierByTeam]",
            Params: [
                { Name: "@teamID", Value: value }
            ]
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
			{ name: 'logID', type: 'string', hidden: true },
            { name: 'logNumber', type: 'string', width: '40%' },
            { name: 'frequencyDescription', type: 'string', width: '60%' }
        ],

        ready: function () {
            _hideLoadingFullPage();


            $("#tbldropLogID").on('rowselect', function (event) {
                var args = event.args;
                var row = $("#tbldropLogID").jqxGrid('getrowdata', args.rowindex);
                var dropDownContent = '<div id="selectedLogID" style="position: relative; width:100%; margin-left: 3px; margin-top: 5px;" idLogID="' + row.logID + '" >' + row['logNumber'] + ' - ' + row['frequencyDescription'] + '</div>';
                $("#jqxdropdownbutton_list_logID").jqxDropDownButton('setContent', dropDownContent);
                $("#jqxdropdownbutton_list_logID").jqxDropDownButton('close');

                console.log($("#jqxdropdownbutton_list_logID").val(), $("#selectedLogID")[0].attributes[2].nodeValue);

            });

        }
    });
}

//TRAN METHODS
function loadDropDowns(selectQuery, dropList, onSuccess) {
    //Set default value  
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Log ID Details...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify(selectQuery) },
        type: "post",
        success: function (resultList) {
            $("#" + dropList + "").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $("#" + dropList + "").append($('<option>', { value: objFunction.id, text: objFunction.text }));
                $("#" + dropList + "").val(null).trigger("change");

                if (dropList == "drp_Team") {
                    validateUser();
                }

                if (onSuccess) {
                    onSuccess();
                }
            }
        }
    });
}

function validateUser() {

    //if (!_getUserInfo().Roles.includes("SUPER_ADMIN")) {
    //    $("#drp_Team").prop("disabled", true);
    //    $("#drp_TeamM").prop("disabled", true);
    //    _callServer({
    //        loadingMsgType: "fullLoading",
    //        loadingMsg: "Loading Log ID Details...",
    //        url: '/Ajax/ExecQuery',
    //        data: { 'pjsonSql': JSON.stringify(" SELECT [teamID] FROM [dbo].[UserTable] WHERE [SOEID] ='" + _getUserInfo().SOEID + "'") },
    //        type: "post",
    //        success: function (resultList) {
    //            $("#drp_Team").val(resultList[0].teamID);
    //            $("#drp_Team").trigger("change");
    //            $("#drp_TeamM").val(resultList[0].teamID);
    //            $("#drp_TeamM").trigger("change");
    //        }
    //    });
    //}

}

function validateEmpty() {
    var notEmpty;

    if ($("#drp_Team :selected").val() != "0" && ($("#selectedLogID").val != "" && $("#selectedLogID").val() != 'undefined') && $("#drp_DocType :selected").val() != "0" && $("#txt_FilingYear").val() != "" && $("#drp_FilingMonth :selected").val() != "0" && $("#drp_FileRetenPeriod :selected").val() != "0") {
        notEmpty = true;
    } else {
        notEmpty = false;
    }
    return notEmpty;
}

function getNewFileName(ver) {
    var newFileName = "TampaTaxGroup_" +
        $("#drp_Team :selected").text().replace(/\s/g, '') + "_" +
        $("#selectedLogID").text().replace(/\s/g, '').split("-")[0] + "_" +
        $("#selectedLogID").text().replace(/\s/g, '').split("-")[1] + "_" +
        $("#drp_FilingMonth :selected").val() + "_" +
        $("#txt_FilingYear").val() + "_" +
        ver + ".pdf";
    return newFileName;
}

function validateDocVersion(fnOnSuccess) {
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Validating Docs..",
        name: "[dbo].[spFirmGetDocumentVersion]",
        params: [
            { Name: '@logID', Value: $("#selectedLogID").attr("idLogID") },
            { Name: '@FilingMonth', Value: $("#drp_FilingMonth :selected").val() },
            { Name: '@FilingYear', Value: $("#txt_FilingYear").val() }
        ],
        success: {
            fn: function (responseList) {
                var result = 1;
                if (responseList.length != 0) {
                    result = Number(responseList[0].versionNumber) + 1;
                }

                if (fnOnSuccess) {
                    fnOnSuccess(result);
                }
            }
        },
    });
}

function getExpirationDate(val, fnOnSuccess) {
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Log ID Details...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify("SELECT [daysToRetain] FROM [dbo].[DocumentRetentionType] WHERE [id] = '" + val + "';") },
        type: "post",
        success: function (resultList) {
            var date = new Date();
            date.setDate(date.getDate() + resultList[0].daysToRetain);

            if (fnOnSuccess) {
                fnOnSuccess(moment(date).format('M/D/YYYY h:mm:ss A'));
            }
        }
    });
}

function getDocDetails(ver) {
    var details = {
        logId: $("#selectedLogID").attr("idLogID"),
        departmentID: "05FEC7CB-268C-4EE2-A9DC-62B5835E50D6",
        versionNumber: ver,
        Year: $("#txt_FilingYear").val(),
        Month:  $("#drp_FilingMonth :selected").val(),
        searchKey: $("#txt_Keywords").val(),
        docType: $("#drp_DocType :selected").val()
    };

    return details;
}

function clearSingleUpload() {
    $("#drp_DocType").val(null).trigger("change");
    $("#txt_FilingYear").val("");
    $("#drp_FilingMonth").val(null).trigger("change");
    $("#txt_Keywords").val("");
    $("#jqxdropdownbutton_list_logID").val("");
}

function clearMultipleUpload() {
    $("#drp_DocTypeM").val(null).trigger("change");
    $("#drp_FilingMonthM").val(null).trigger("change");
    $("#txt_KeywordsM").val("");
}

//BUTTONS ACTIONS
$("#drp_Team").change(function () {
    if ($("#drp_Team :selected").val()) {
        //var qry_logid = "SELECT LI.[logID] AS [id], LI.[logNumber] + ' - ' + PF.[frequencyDescription] AS [text] FROM [TheFirm].[dbo].[LogIdentifier] LI JOIN [TheFirm].[dbo].[PeriodFrequency] PF ON LI.[periodFrequencyID] = PF.[periodFrequencyID] WHERE LI.[IsDeleted] = 0  AND LI.[teamID] = '" + $("#drp_Team :selected").val() + "' ORDER BY LI.[LogNumber];";
        //loadDropDowns(qry_logid, "drp_logIdentifer");
        $("#jqxdropdownbutton_list_logID").val("");
        loadDropDownLogID($("#drp_Team :selected").val());
    }
});

$("#drp_Team").select2({
    placeholder: 'Choose a Team:',
    allowClear: true
}).on('select2-open', function () {
    // Adding Custom Scrollbar 
    $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
});

$("#drp_DocType").select2({
    placeholder: 'Choose a lDocument Type:',
    allowClear: true
}).on('select2-open', function () {
    // Adding Custom Scrollbar 
    $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
});

$("#drp_FilingMonth").select2({
    placeholder: 'Choose a Filing Month:',
    allowClear: true
}).on('select2-open', function () {
    // Adding Custom Scrollbar 
    $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
});

$("#drp_FileRetenPeriod").select2({
    placeholder: 'Choose a Retention Period:',
    allowClear: true
}).on('select2-open', function () {
    // Adding Custom Scrollbar 
    $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
});

$("#drp_TeamM").select2({
    placeholder: 'Choose a Retention Period:',
    allowClear: true
}).on('select2-open', function () {
    // Adding Custom Scrollbar 
    $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
});

$("#drp_DocTypeM").select2({
    placeholder: 'Choose a Retention Period:',
    allowClear: true
}).on('select2-open', function () {
    // Adding Custom Scrollbar 
    $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
});

$("#drp_FilingMonthM").select2({
    placeholder: 'Choose a Filing Month:',
    allowClear: true
}).on('select2-open', function () {
    // Adding Custom Scrollbar 
    $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
});