﻿$(document).ready(function () {
    loadLogIdList();
    initDropListLoads();
    loadShareList();
    validateUserRole();
});


var qry_period = "SELECT '0' AS [id], '-- Select --' AS  [text] UNION SELECT CONVERT(VARCHAR(50),[periodFrequencyID]) as [id], [frequencyDescription] as [text] FROM [TheFirm].[dbo].[PeriodFrequency];",
    qry_team = "SELECT '0' AS [id], '-- Select --' AS [text] UNION SELECT CONVERT(VARCHAR(50),[teamID]) AS [id], [teamDescription] AS [text] FROM [TheFirm].[dbo].[Team];",
    qry_country = "SELECT '0' as [id], '-- SELECT --' as [text] UNION SELECT CONVERT(VARCHAR(50),[countryID]) as [id] ,[countryDescription] as [text] FROM [TheFirm].[dbo].[Country];",
    selectedLog,
    selectedDoc;

//TABLE LOADS
function loadLogIdList() {
    $.jqxGridApi.create({
        showTo: "#tbl_LogIdentifierList",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[spFirmListLogIdentifier]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
			{ name: 'logID', type: 'string', hidden: true },
			{ name: 'logNumber', text: "Log Number", type: 'int', width: '20%' },
            { name: 'periodFrequencyID', type: 'string', hidden: true },
            { name: 'teamID', type: 'string', hidden: true },
			{ name: 'teamDescription', type: 'string', text: "Team Description", width: '40%' },
            { name: 'frequencyDescription', type: 'string', text: "Period Frequency", width: '30%' },
            { name: 'Active', type: 'integer', text: "Active", width: '10%' }
        ],

        ready: function () {
            loadDocumentTypeList();
            loadUserList();
            $('#tbl_LogIdentifierList').on('rowselect', function (event) {
                // event.args.rowindex is a bound index.
                if (typeof event.args.group == "undefined") {
                    var args = event.args;
                    var row = $("#tbl_LogIdentifierList").jqxGrid('getrowdata', args.rowindex);
                    //var dropDownContent = '<div id="selectedLOGID" style="position: relative; width:100%; margin-left: 3px; margin-top: 5px;" idLOGID="' + row.idJurisdictionLocation + '" >' + row['idJurisdictionLocation'] + ' - ' + row['legalEntityName'] + '</div>';
                    //$("#jqxdropdownbutton_LOGID").jqxDropDownButton('setContent', dropDownContent);

                    $('#txtLogIdID').val(row['logNumber']);
                    $('#drp_periodDescription').val(row['periodFrequencyID']);
                    $('#drp_TeamDescription').val(row['teamID']);
                    $('#btnLogId_Add').prop("disabled", true);
                    $('#txtLogIdID').prop("disabled", true);
                    $('#drp_periodDescription').prop("disabled", true);
                    $('#drp_TeamDescription').prop("disabled", true);
                    $("#chkLogidActive").prop("checked", row['Active'] == 'True');
                    selectedLog = row['logID'];
                }
            });
        }
    });
}

function loadDocumentTypeList() {
    $.jqxGridApi.create({
        showTo: "#tbl_DocumentList",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[spFirmListDocumentType]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
			{ name: 'documentTypeID', type: 'string', hidden: true },
			{ name: 'documentTypeDescription', text: "Description",type: 'string', width: '70%' },
			{ name: 'Active', type: 'string', text: "Active" ,width: '30%' }
        ],

        ready: function () {
            $('#tbl_DocumentList').on('rowselect', function (event) {
                // event.args.rowindex is a bound index.
                if (typeof event.args.group == "undefined") {
                    var args = event.args;
                    var row = $("#tbl_DocumentList").jqxGrid('getrowdata', args.rowindex);
                    
                    $("#chkDocTypeActivate").prop("checked", row['Active'] == 'True');
                    $('#txtDocTypeDesc').val(row['documentTypeDescription']);
                    selectedDoc = row['documentTypeID'];
                    $('#btnDocType_Add').prop("disabled", true);
                }
            });
        }
    });
}

function loadUserList() {
    $.jqxGridApi.create({
        showTo: "#tbl_UserList",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: true,
            showfilterrow: true
        },
        sp: {
            Name: "[dbo].[spFirmListUsers]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
			{ name: 'SOEID', type: 'string',text: "SOEID",  width: '10%' },
			{ name: 'Name', type: 'string', text: "Description", width: '20%' },
			{ name: 'Email', type: 'string', text: "Email", width: '25%' },
            { name: 'Roles', type: 'string', text: "Roles", width: '20%' },
            { name: 'departmentID', type: 'string', hidden: true },
            { name: 'deptDescription', type: 'string', text: "Department", width: '15%' },
            { name: 'teamID', type: 'string', hidden: true },
            { name: 'teamDescription', type: 'string', text: "Team", width: '15%' },
            { name: 'delAuthority', type: 'int', text: "Authority", width: '10%' },
            { name: 'countryID', type: 'string', hidden: true },
            { name: 'countryDescription', type: 'string', text: "Country", width: '10%' },
            { name: 'CreatedBy', type: 'string', text: "Created By", width: '10%' },
            { name: 'CreatedDate', type: 'Date', text: "Creation Date", width: '10%' },
            { name: 'Active', type: 'string', text: "Active", width: '5%' }
        ],

        ready: function () {
            _hideLoadingFullPage();
            $('#tbl_UserList').on('rowselect', function (event) {
                // event.args.rowindex is a bound index.
                if (typeof event.args.group == "undefined") {
                    var args = event.args;
                    var row = $("#tbl_UserList").jqxGrid('getrowdata', args.rowindex);

                    $('#txt_SOEID').val(row['SOEID']);
                    $('#drp_UserTeam').val(row['teamID']);
                    $('#drp_UserCountry').val(row['countryID']);
                    $("#chkUserActive").prop("checked", row['Active'] == 'True');
                    $("#chkUserAuthority").prop("checked", row['delAuthority'] == 'True');
                    //$('#btnUser_Add').prop("disabled", true);
                    //$('#txt_SOEID').prop("disabled", true);
                }
            });
        }
    });
}

function loadShareList() {
    $.jqxGridApi.create({
        showTo: "#tbl_ShareList",
        options: {
            //for comments or descriptions
            height: "600",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: false,
            sortable: true,
            groupable: false,
            showfilterrow: true,
            editable: true
        },
        sp: {
            Name: "[dbo].[spFirmListShareDrive]"
        },
        source: {
            dataBinding: "Large Data Set"
        },
        group: [],
        columns: [
			{ name: 'countryID', type: 'string', hidden: true },
			{ name: 'countryDescription', type: 'string', text: "Country", width: '30%', editable: false },
			{ name: 'nasLocation', type: 'string', text: "Email", width: '70%' },
        ],

        ready: function () {
            _hideLoadingFullPage();
            $('#tbl_ShareList').on('cellvaluechanged', function (event) {
                console.log("Hello")
                // event.args.rowindex is a bound index.
                if (typeof event.args.group == "undefined") {
                    var args = event.args;
                    var row = $("#tbl_ShareList").jqxGrid('getrowdata', args.rowindex);

                    _callProcedure({
                        loadingMsgType: "topBar",
                        loadingMsg: "Updating Share Drive",
                        name: "[dbo].[spFirmUpdateShareDrive]",
                        params: [
                            { Name: '@countryID', Value: row['countryID'] },
                            { Name: '@nasLocation', Value: row['nasLocation'] },
                            { Name: '@SOEID', Value: _getUserInfo().SOEID }
                        ],
                        success: {
                            fn: function (responseList) {
                                _showNotification("success", "Data Updated", row['countryID'])
                            }
                        }
                    });
                }
            });
        }
    });
}

tbl_ShareList

//ELEMENTS LOAD
function loadDropDowns(selectQuery, selectedId, dropList) {
    //Set default value  
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Log ID Details...",
        url: '/Ajax/ExecQuery',
        data: { 'pjsonSql': JSON.stringify(selectQuery) },
        type: "post",
        success: function (resultList) {
            $("#" + dropList + "").contents().remove();
            for (var i = 0; i < resultList.length; i++) {
                var objFunction = resultList[i];
                $("#" + dropList + "").append($('<option>', { value: objFunction.id, text: objFunction.text, selected: (objFunction.id == selectedId) }));
            }
        }
    });
};

function initDropListLoads() {
    loadDropDowns(qry_period, "0", "drp_periodDescription");
    loadDropDowns(qry_team, "0", "drp_TeamDescription");
    loadDropDowns(qry_team, "0", "drp_UserTeam");
    loadDropDowns(qry_country, "0", "drp_UserCountry");
}

//TRAN METHODS
function validateLogId() {
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Validating log identifer..",
        name: "[dbo].[spFirmValLogIdentifier]",
        params: [
            { Name: '@logNumber', Value: $('#txtLogIdID').val() },
            { Name: '@teamID', Value: $('#drp_TeamDescription :selected').val() },
            { Name: '@periodFrequencyID', Value: $('#drp_periodDescription :selected').val() }
        ],
        success: {
            fn: function (responseList) {
                if (responseList[0].EXISTVALUE == 'True') {
                    _showNotification("error", "Log Identifier " + $("#txtLogIdID").val() + " for " + $('#drp_TeamDescription :selected').text() + " already exist", $("#txtLogIdID").val())
                } else {
                    addLogId();
                }
            }
        }
    });
}

function addLogId() {
    var active;
    switch ($("#chkLogidActive").is(':checked')) {
        case true: active = 0;
            break;
        case false: active = 1;
            break;
    }
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Validating log identifer..",
        name: "[dbo].[spFirmAddLogIdentifier]",
        params: [
            { Name: '@logNumber', Value: $('#txtLogIdID').val() },
            { Name: '@periodFrequencyID', Value: $('#drp_periodDescription :selected').val() },
            { Name: '@teamID', Value: $('#drp_TeamDescription :selected').val() },
            { Name: '@Active', Value: active },
            { Name: '@SOEID', Value: _getUserInfo().SOEID }
        ],
        success: {
            fn: function (responseList) {

                _showNotification("success", "Log Identifier " + $("#txtLogIdID").val() + " for " + $('#drp_TeamDescription :selected').text() + " has been created", $("#txtLogIdID").val())
                loadLogIdList();
            }
        }
    });
}

function updateLogId() {
    var active;
    switch ($("#chkLogidActive").is(':checked')) {
        case true: active = 0;
            break;
        case false: active = 1;
            break;
    }
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Validating log identifer..",
        name: "[dbo].[spFirmUpdateLogIdentifier]",
        params: [
            { Name: '@logID', Value: selectedLog },
            { Name: '@periodFrequencyID', Value: $('#drp_periodDescription :selected').val() },
            { Name: '@teamID', Value: $('#drp_TeamDescription :selected').val() },
            { Name: '@Active', Value: active },
            { Name: '@SOEID', Value: _getUserInfo().SOEID }
        ],
        success: {
            fn: function (responseList) {
                _showNotification("success", "Log Identifier " + $("#txtLogIdID").val() + " for " + $('#drp_TeamDescription :selected').text() + " has been Modified", $("#txtLogIdID").val())
                loadLogIdList();
            }
        }
    });
}

function validateDocType() {
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Validating log identifer..",
        name: "[dbo].[spFirmValDocumentType]",
        params: [
            { Name: '@documentTypeDescription', Value: $('#txtDocTypeDesc').val() }
        ],
        success: {
            fn: function (responseList) {
                if (responseList[0].EXISTVALUE == 'True') {
                    _showNotification("error", "DocumentType " + $("#txtDocTypeDesc").val() + " already exist", selectedDoc)
                } else {
                    addDocType();
                }
            }
        }
    });
}

function addDocType() {
    var active;
    switch ($("#chkDocTypeActivate").is(':checked')) {
        case true: active = 1;
            break;
        case false: active = 0;
            break;
    }
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Validating log identifer..",
        name: "[dbo].[spFirmAddDocumentType]",
        params: [
            { Name: '@documentTypeDescription', Value: $('#txtDocTypeDesc').val() },
            { Name: '@Active', Value: active },
            { Name: '@SOEID', Value: _getUserInfo().SOEID }
        ],
        success: {
            fn: function (responseList) {
                _showNotification("success", "Document Type " + $("#txtDocTypeDesc").val() + " has been created", $("#txtDocTypeDesc").val())
                loadLogIdList();
            }
        }
    });
}

function updateDocType() {
    var active;
    switch ($("#chkDocTypeActivate").is(':checked')) {
        case true: active = 1;
            break;
        case false: active = 0;
            break;
    }
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Validating log identifer..",
        name: "[dbo].[spFirmUpdateDocumentType]",
        params: [
            { Name: '@documentTypeID', Value: selectedDoc },
            { Name: '@documentTypeDescription', Value: $('#txtDocTypeDesc').val() },
            { Name: '@Active', Value: active },
            { Name: '@SOEID', Value: _getUserInfo().SOEID }
        ],
        success: {
            fn: function (responseList) {
                _showNotification("success", "Document Type " + $("#txtDocTypeDesc").val() + " has been Modified", selectedDoc)
                loadDocumentTypeList();
            }
        }
    });
}

function validateUser() {
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Validating log identifer..",
        name: "[dbo].[spFirmValUser]",
        params: [
            { Name: '@SOEID', Value: $('#txt_SOEID').val() }
        ],
        success: {
            fn: function (responseList) {
                if (responseList[0].EXISTVALUE == 'True') {
                    _showNotification("error", "User " + $("#txt_SOEID").val() + " already exist", $("#txt_SOEID").val())
                } else {
                    addUser();
                }
            }
        }
    });
}

function addUser() {
    var active;
    switch ($("#chkUserActive").is(':checked')) {
        case true: active = 0;
            break;
        case false: active = 1;
            break;
    }

    var active2;
    switch ($("#chkUserAuthority").is(':checked')) {
        case true: active2 = 1;
            break;
        case false: active2 = 0;
            break;
    }
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Validating log identifer..",
        name: "[dbo].[spFirmAddUser]",
        params: [
            { Name: '@SOEID', Value: $('#txt_SOEID').val() },
            { Name: '@teamID', Value: $('#drp_UserTeam').val() },
            { Name: '@countryID', Value: $('#drp_UserCountry').val() },
            { Name: '@isAuthority', Value: active2 },
            { Name: '@IsDeleted', Value: active },
            { Name: '@SOEIDA', Value: _getUserInfo().SOEID }
        ],
        success: {
            fn: function (responseList) {
                _showNotification("success", "User " + $("#txt_SOEID").val() + " has been created", $("#txt_SOEID").val())
                loadUserList();
            }
        }
    });
}

function updateUser() {
    var active;
    switch ($("#chkUserActive").is(':checked')) {
        case true: active = 0;
            break;
        case false: active = 1;
            break;
    }

    var active2;
    switch ($("#chkUserAuthority").is(':checked')) {
        case true: active2 = 1;
            break;
        case false: active2 = 0;
            break;
    }
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Validating log identifer..",
        name: "[dbo].[spFirmUpdateUser]",
        params: [
            { Name: '@SOEID', Value: $('#txt_SOEID').val() },
            { Name: '@teamID', Value: $('#drp_UserTeam').val() },
            { Name: '@countryID', Value: $('#drp_UserCountry').val() },
            { Name: '@isAuthority', Value: active2 },
            { Name: '@IsDeleted', Value: active },
            { Name: '@SOEIDA', Value: _getUserInfo().SOEID }
        ],
        success: {
            fn: function (responseList) {
                _showNotification("success", "User " + $("#txt_SOEID").val() + " has been Modified", $("#txt_SOEID").val())
                loadUserList();
            }
        }
    });
}

function validateUserRole(){
    if (!_getUserInfo().Roles.includes("MANAGER")) {
        $("#tab2").hide();
        $("#tab3").hide();
        $("#tab4").hide();

        $("#Menu2").hide();
        $("#Menu3").hide();
        $("#Menu4").hide();
    }
}

//BUTTONS ACTIONS
$('#btnClearLogId').click(function () {
    $('#txtLogIdID').val('');
    $('#drp_periodDescription').val('0');
    $('#drp_TeamDescription').val('0');
    $('#btnLogId_Add').prop("disabled", false);
    $('#txtLogIdID').prop("disabled", false);
    $('#chkLogidActive').prop("checked", false);
    $('#drp_periodDescription').prop("disabled", false);
    $('#drp_TeamDescription').prop("disabled", false);
    selectedLog = '';
});

$('#btnLogId_Add').click(function () {

     if ($('#txtLogIdID').val() != "" && $('#drp_TeamDescription :selected').val() != 0 && $('#drp_periodDescription :selected').val() != 0) {
         validateLogId();

    } else {
        _showNotification("error", "Select a value to proceed")
    }
});

$('#btnLogId_Update').click(function () {

    if ($('#drp_TeamDescription :selected').val() != 0 && $('#drp_periodDescription :selected').val() != 0) {
        updateLogId();
    } else {
        _showNotification("error", "Select a value to proceed")
    }
});

$('#btnClearDocType').click(function () {
    $("#chkDocTypeActivate").prop("checked", false);
    $('#txtDocTypeDesc').val('');
    $('#btnDocType_Add').prop("disabled", false);
    selectedDoc = '';
});

$('#btnDocType_Add').click(function () {

    if ($('#txtDocTypeDesc').val() != "") {
        validateDocType();
    } else {
        _showNotification("error", "Select a value to proceed")
    }
});

$('#btnDocType_Update').click(function () {

    if ($('#txtDocTypeDesc').val() != "") {
        updateDocType()
    } else {
        _showNotification("error", "Select a value to proceed")
    }
});

$('#btnUser_Clear').click(function () {
    $('#txt_SOEID').val('');
    $('#drp_UserTeam').val('0');
    $('#drp_UserCountry').val('0');
    $('#chkUserActive').prop("checked", false);
    $('#chkUserAuthority').prop("checked", false);
    //$('#btnUser_Add').prop("disabled", false);
});

$('#btnUser_Add').click(function () {

    if ($('#txt_SOEID').val() != "" && $('#drp_UserTeam :selected').val() != 0 && $('#drp_UserCountry :selected').val() != 0) {
        validateUser();
    } else {
        _showNotification("error", "Select a value to proceed")
    }
});

$('#btnUser_Update').click(function () {

    if ($('#txt_SOEID').val() != "" && $('#drp_UserTeam :selected').val() != 0 && $('#drp_UserCountry :selected').val() != 0) {
        updateUser();
    } else {
        _showNotification("error", "Select a value to proceed")
    }
});