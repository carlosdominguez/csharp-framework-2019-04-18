function _setProcessTreeValue() {
    $(".contentIssueForm").find("#jqxDropDownProcessOriginated").css('pointer-events', 'none');
    $(".contentIssueForm").find("#jqxDropDownProcessOriginated").css("cursor", "not-allowed");

    var selContent = '<div class="fieldID' + pnameElement + '" value="0" style="padding: 8px;">Disable when Responsible Party is "Business", "IT" or "Third Party".</div>';
    $.cookie("HTMLContent" + pnameElement, selContent);
    $("#jqxDropDown" + pnameElement).jqxDropDownButton('setContent', selContent);
    $('#HF' + pnameElement + 'ID').val("0");
}

function _initProcessTree(pnameElement, options) {
    //First load clear cache of selected Process
    $.removeCookie("HTMLContent" + pnameElement);

    //If not exists create it
    if ($("#jqxDropDown" + pnameElement).length > 0) {
        var _processList = [];

        //Set "--Select Process --" in dropdown.
        var dropDownContent = '<div class="fieldID' + pnameElement + '" name="fieldID' + pnameElement + '" value="0" style="padding: 8px;">-- Select Process --</div>';

        // Initialize jqxDropDownButton
        $("#jqxDropDown" + pnameElement).jqxDropDownButton({
            width: '100%', height: 34
        });

        //Load last value on post back
        if ($.cookie("HTMLContent" + pnameElement)) {
            dropDownContent = $.cookie("HTMLContent" + pnameElement);
        }

        $("#jqxDropDown" + pnameElement).jqxDropDownButton('setContent', dropDownContent);

        // on click clear parent 
        $(".btnClear" + pnameElement).click(function () {
            $("#jqxDropDown" + pnameElement).jqxDropDownButton('setContent', '<div class="fieldID' + pnameElement + '" value="0" style="padding: 8px;">-- Select Process --</div>');
            $('#HF' + pnameElement + 'ID').val(0);

            //Fix bug with JQuery Validate on change set success
            $('#HF' + pnameElement + 'ID').trigger("keyup");
        });

        _callProcedure({
            loadingMsgType: "fullLoading",
            loadingMsg: "Loading Process...",
            name: "[dbo].[STPR_PROCESS_LIST]",
            params: [],
            success: {
                fn: function (responseList) {
                    _processList = responseList;

                    //Set current value
                    if ($('#HF' + pnameElement + 'ID').val() != "0") {
                        var resultList = $.grep(_processList, function (v) {
                            return v.ID == $('#HF' + pnameElement + 'ID').val();
                        });

                        if (resultList.length > 0) {
                            var selectedProcess = resultList[0];
                            var selectedContent = '<div class="fieldID' + pnameElement + '" value="' + selectedProcess.ID + '" style="padding: 8px;">L' + selectedProcess.Level + ' - ' + selectedProcess.Name + '</div>';
                            $.cookie("HTMLContent" + pnameElement, selectedContent);
                            $("#jqxDropDown" + pnameElement).jqxDropDownButton('setContent', selectedContent);
                        }
                    }

                    //Create Process Affected
                    createTableProcess({
                        processList: _processList,
                        idTable: "#jqxGrid" + pnameElement,
                        onReady: function () {
                            var $jqxGrid = $("#jqxGrid" + pnameElement);

                            //On select row event
                            $jqxGrid.on('cellclick', function (event) {
                                var row = $jqxGrid.jqxGrid('getrowdata', event.args.rowindex);

                                if (row.CantChilds == 0) {
                                    var rowSelectEvent = setInterval(function () {

                                        var args = event.args;
                                        if ($jqxGrid.attr("ClickedRowBtnChild") != args.rowindex) {
                                            var row = $jqxGrid.jqxGrid('getrowdata', args.rowindex);

                                            var dropDownContent = '<div class="fieldID' + pnameElement + '" value="' + row.ID + '" style="padding: 8px;">L' + row.Level + ' - ' + row.Name + '</div>';
                                            $.cookie("HTMLContent" + pnameElement, dropDownContent);
                                            $("#jqxDropDown" + pnameElement).jqxDropDownButton('setContent', dropDownContent);
                                            $("#jqxDropDown" + pnameElement).jqxDropDownButton('close');

                                            //Update aspx id
                                            $('#HF' + pnameElement + 'ID').val(row.ID);

                                            //Fix bug with JQuery Validate on change set success
                                            $('#HF' + pnameElement + 'ID').trigger("keyup");
                                        } else {
                                            $jqxGrid.attr("ClickedRowBtnChild", "0");
                                        }

                                        clearInterval(rowSelectEvent);
                                    }, 200);
                                }
                                else {
                                    alert("You need to select the lowest level of the process");

                                }

                            });

                            if (options && options.onSuccess) {
                                options.onSuccess();
                            }
                        }
                    });
                }
            }
        });

        //Fix Width bug on post back
        $("#dropDownButtonContentjqxDropDown" + pnameElement).css({
            width: '100%'
        });
    }

    function createTableProcess(options) {
        //Create table
        $.jqxGridApi.create({
            showTo: options.idTable,
            options: {
                //for comments or descriptions
                width: "600",
                height: "400",
                autoheight: false,
                autorowheight: false,
                //selectionmode: 'checkbox',
                showfilterrow: true,
                editable: false,
                sortable: true
            },
            source: {
                // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
                dataBinding: "Large Data Set Local",
                rows: options.processList
            },
            columns: [
                { name: 'IsOpen', type: 'bool', hidden: true },
                { name: 'CantChilds', type: 'number', hidden: true },
                { name: 'ID', type: 'string', hidden: true },
                { name: 'IDParent', type: 'string', hidden: true },
                { name: 'Level', type: 'number', hidden: true },
                { name: 'Key', text: 'Key', type: 'string', hidden: true },

                {
                    name: 'Name', text: 'NameName', width: '100%', type: 'html', filtertype: 'input', cellsrenderer: function (rowIndex, datafield, value) {
                        var resultHTML = '';
                        var dataRecord = $(options.idTable).jqxGrid('getrowdata', rowIndex);
                        var paddingXLevel = 35;
                        var padding = paddingXLevel * (dataRecord.Level - 1);
                        var htmlOpen = " (-)";
                        var classIsOpenedOrClosed = "isOpened";

                        //Set default open
                        if (typeof dataRecord.IsOpen == "undefined") {
                            dataRecord.IsOpen = true;
                        }
                        if (dataRecord.IsOpen == false) {
                            htmlOpen = " (+)";
                            classIsOpenedOrClosed = "isClosed";
                        }

                        if (padding == 0) {
                            padding = 2;
                        }

                        var htmlDeleted = "<code class='pull-right' style='margin-top: 5px;margin-right: 3px;'>Parent was deleted, please reasign</code>";

                        var resultList = $.grep(options.processList, function (v) {
                            return v.ID == dataRecord.IDParent;
                        });

                        if (resultList.length > 0 || dataRecord.IDParent === "") {
                            htmlDeleted = "";
                        }
                        if (dataRecord.CantChilds > 0) {
                            resultHTML =
                                 ' <button type="button" rowUid="' + dataRecord.uid + '" idProcess="' + dataRecord.ID + '" idParent="' + dataRecord.IDParent + '" class="btn btn-xs btnOpenChilds btn-success ' + classIsOpenedOrClosed + '" aria-label="Left Align" style="margin-left: ' + padding + 'px; margin-top: 0px;">' +
                                 '    ' + htmlOpen +
                                 ' </button>' +
                                 ' <span style="line-height: 28px; padding-left: 2px;" > L' + dataRecord.Level + ' ' + value + ' (' + dataRecord.CantChilds + ')</span>' + htmlDeleted;
                        } else {
                            resultHTML =
                                 ' <button type="button" rowUid="' + dataRecord.uid + '" idProcess="' + dataRecord.ID + '" idParent="' + dataRecord.IDParent + '" class="btn btn-xs btnOpenChilds ' + classIsOpenedOrClosed + ' disabled" aria-label="Left Align" style="margin-left: ' + padding + 'px; margin-top: 0px;">' +
                                 '    ' + htmlOpen +
                                 ' </button>' +
                                 ' <span style="line-height: 28px; padding-left: 2px;" > L' + dataRecord.Level + ' ' + value + ' (' + dataRecord.CantChilds + ')</span>' + htmlDeleted;
                        }

                        return resultHTML;
                    }
                }//,

                //{ name: 'FullName', type: 'string', width: '35%' }
            ],
            ready: function () {
                //Add event
                $(options.idTable).on("click", ".btnOpenChilds", function () {
                    var $btn = $(this);
                    $(options.idTable).attr("ClickedRowBtnChild", $btn.attr("rowuid"));
                    var idProcess = $btn.attr("idProcess");
                    var isOpen = $btn.hasClass("isOpened");
                    var childsToHideOrShow = getChildsRecursive(idProcess, options.processList);

                    var clickedProcess;
                    var resultClickedProcess = $.grep(options.processList, function (p) {
                        return p.ID == idProcess;
                    });

                    if (resultClickedProcess.length > 0) {
                        clickedProcess = resultClickedProcess[0];
                    }

                    clickedProcess.IsOpen = !isOpen;

                    $(options.processList).each(function () {
                        var objProcess = this;

                        //Set default value
                        if (typeof objProcess.IsVisible == "undefined") {
                            objProcess.IsVisible = true;
                        }

                        var resultChildFound = $.grep(childsToHideOrShow, function (c) {
                            return c.ID == objProcess.ID;
                        });

                        if (resultChildFound.length > 0) {
                            objProcess.IsVisible = !isOpen;
                            objProcess.IsOpen = !isOpen;
                        }
                    });

                    var processToShow = $.grep(options.processList, function (p) {
                        return p.IsVisible;
                    });

                    //Refresh table with new array
                    $.jqxGridApi.localStorageFindById(options.idTable).fnReload(processToShow);
                    $(options.idTable).jqxGrid('updatebounddata');

                });

                if (options.onReady) {
                    options.onReady();
                }
            }
        });
    }

    function getChildsRecursive(idProcess, processList) {
        var result = [];
        var childs = $.grep(processList, function (v) {
            return v.IDParent == idProcess;
        });

        //Add childs
        $.merge(result, childs);

        //Get Childs of childs
        for (var i = 0; i < childs.length; i++) {
            $.merge(result, getChildsRecursive(childs[i].ID, processList));
        }

        //Return result
        return result;
    }
}