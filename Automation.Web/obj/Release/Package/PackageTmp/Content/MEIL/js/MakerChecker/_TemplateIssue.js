//@ sourceURL=MEIL/_TemplateIssue.js
/// <reference path="../../../Shared/plugins/util/global.js" />
/// <reference path="../MEILGlobal.js" />

$(document).ready(function () {
    // Load New Issue
    if ($("#HFAction").val() == "New") {
        loadNewIssue();
    }

    // Load Edit Issue
    if ($("#HFAction").val() == "Edit") {
        loadObjIssueByID($("#HFIssueID").val(), function (objIssue) {
            loadEditIssue(objIssue);
        });
    } else {
        // Process Area
        _initProcessTree("ProcessArea", {
            onSuccess: function () {}
        });
    }

    // Validate Form
    loadValidations();

    // On click Save
    $(".btnSave").click(function () {
        saveIssue();
    });
});

function generateMakerCheckerID() {
    return "MC" + _createCustomID(false);
}

function loadReportingPeriod(customOptions) {
    var reportingPeriodMoment;
    var cantBackMonth = 4;
    var cantForward = 4;
    var resultList = [];

    //Default Options.
    var options = {
        showTo: "",
        reportingPeriod: ""
    };

    //Hacer un merge con las opciones que nos enviaron y las default.
    $.extend(true, options, customOptions);

    //If not exist set current month
    if (!options.reportingPeriod) {
        options.reportingPeriod = moment().format('MMMM YYYY');
    }
    reportingPeriodMoment = moment(options.reportingPeriod, 'MMMM YYYY');

    var currentMonthId = parseInt(reportingPeriodMoment.format('M'));
    var currentYear = parseInt(reportingPeriodMoment.format('YYYY'));

    //Add Current Month
    var tempMonthId = currentMonthId;
    var tempYear = currentYear;
    var tempPeriod = currentMonthId + " " + currentYear;
    resultList.push({
        id: tempYear + "" + moment(tempPeriod, 'M YYYY').format('MM'),
        year: tempYear,
        monthId: tempMonthId,
        monthName: moment(tempPeriod, 'M YYYY').format('MMMM'),
        period: moment(tempPeriod, 'M YYYY').format('MMMM YYYY')
    });

    //Get Back Months
    for (var i = 0; i < cantBackMonth; i++) {
        if (tempMonthId == 1) {
            tempMonthId = 12;
            tempYear = tempYear - 1;
        } else {
            tempMonthId--;
        }

        tempPeriod = tempMonthId + " " + tempYear;
        resultList.push({
            id: tempYear + "" + moment(tempPeriod, 'M YYYY').format('MM'),
            year: tempYear,
            monthId: tempMonthId,
            monthName: moment(tempPeriod, 'M YYYY').format('MMMM'),
            period: moment(tempPeriod, 'M YYYY').format('MMMM YYYY')
        });
    }

    //Get Forward Months
    tempMonthId = currentMonthId;
    tempYear = currentYear;
    tempPeriod = "";
    for (var i = 0; i < cantBackMonth; i++) {
        if (tempMonthId == 12) {
            tempMonthId = 1;
            tempYear = tempYear + 1;
        } else {
            tempMonthId++;
        }

        tempPeriod = tempMonthId + " " + tempYear;
        resultList.push({
            id: tempYear + "" + moment(tempPeriod, 'M YYYY').format('MM'),
            year: tempYear,
            monthId: tempMonthId,
            monthName: moment(tempPeriod, 'M YYYY').format('MMMM'),
            period: moment(tempPeriod, 'M YYYY').format('MMMM YYYY')
        });
    }

    //Sort array by id
    resultList.sort(function(a, b) {
        if (parseFloat(a.id) < parseFloat(b.id)) {
            return -1;
        }
        if (parseFloat(a.id) > parseFloat(b.id)) {
            return 1;
        }
        return 0;
    });

    //Add options to select
    $(options.showTo).contents().remove();
    for (var i = 0; i < resultList.length; i++) {
        var objTemp = resultList[i];
        console.log(objTemp.period, options.reportingPeriod);
        $(options.showTo).append($('<option>', {
            year: objTemp.year,
            month: objTemp.monthId,
            value: objTemp.period,
            text: objTemp.period,
            selected: (objTemp.period == options.reportingPeriod)
        }));
    }
}

function loadSelectOfCountries(selectedCountry) {
    var sql = "                             \
        SELECT                              \
            '0'	            AS [ID],        \
            '-- Select --'	AS [Country]    \
        UNION                               \
        SELECT                              \
            [dsc_Country] AS [ID],          \
            [dsc_Country] AS [Country]      \
        FROM                                \
            [dbo].[tlkpCountry]             \
        WHERE                               \
            [dsc_Country] <> '- Regional'   \
        ORDER BY                            \
            (2) ASC                         ";

    _callServer({
        loadingMsg: "Loading Countries...",
        url: '/Shared/ExecQuery',
        data: { 'pjsonSql': _toJSON(sql) },
        type: "post",
        success: function (resultList) {
            $("#selCountry").contents().remove();

            //Add Options
            for (var i = 0; i < resultList.length; i++) {
                var objTemp = resultList[i];
                $("#selCountry").append($('<option>', {
                    value: objTemp.ID,
                    text: objTemp.Country,
                    selected: (selectedCountry == objTemp.ID)
                }));
            }

            $("#selCountry").select2();
        }
    });
};

function loadSelectOfReasons(selectedReasonID) {
    var sql = "                            \
        SELECT                             \
        '0'				AS [ID],           \
	    '-- Select --'	AS [Description]   \
        UNION                              \
        SELECT                             \
            [ID],                          \
            [Description]                  \
        FROM                               \
            [dbo].[tblMakerCheckerReason]  \
        where                              \
            [IsDeleted] = 0                ";

    _callServer({
        loadingMsg: "Loading Resons...",
        url: '/Shared/ExecQuery',
        data: { 'pjsonSql':  _toJSON(sql) },
        type: "post",
        success: function (resultList) {
            $("#selReason").contents().remove();

            //Add Options
            for (var i = 0; i < resultList.length; i++) {
                var objTemp = resultList[i];
                $("#selReason").append($('<option>', {
                    value: objTemp.ID,
                    text: objTemp.Description,
                    selected: (selectedReasonID == objTemp.ID)
                }));
            }
        }
    });
};

function loadValidations() {
    var rules = {};

    // Accountable Person
    rules[$("#selAccountablePerson select").attr("name")] = {
        required: true,
        valueNotEquals: 0
    };

    // Process Area
    rules["HFProcessAreaID"] = {
        required: true,
        valueNotEquals: 0
    };

    // Country
    rules["selCountry"] = {
        required: true,
        valueNotEquals: 0
    };
    
    // Reason
    rules["selReason"] = {
        required: true,
        valueNotEquals: 0
    };

    // Short Description of Error
    rules["txtDescription"] = {
        required: true
    };

    _validateForm({
        form: '#formMakerCheckerIssue',
        rules: rules
        //{

            //formfield_ddlStatus: { required: true, valueNotEquals: 0 },
            //formfield_txtOtherReason: {
            //    required:
            //        {
            //            depends: function () {
            //                return $("#ddlCreationReason").find('option:selected').text() == 'Other';
            //            }
            //        }
            //},
            //formfield_ddlRegion: { required: true, valueNotEquals: 0 },
            //formfield_ddlCountry: { required: true, valueNotEquals: 0 },
            ////Depends on EUC Definition
            //formfield_ddlFrecuencyOfUse: {
            //    required: {
            //        depends: function () {
            //            return $("input[type=radio][name=EUCDefinition]:checked").val() == 'Yes';
            //        }
            //    }, valueNotEquals: 0
            //},
            //GoldCopymoved: {
            //    required: {
            //        depends: function () {
            //            return $("input[type=radio][name=EUCDefinition]:checked").val() == 'Yes';
            //        }
            //    }
            //},
            //formfield_ddlEUCExists: {
            //    required: {
            //        depends: function () {
            //            if (($("input[type=radio][name=chkDataQualityIssue]:checked").val() == 'No') && ($("input[type=radio][name=chkCoreSystem]:checked").val() == 'No')) {
            //                return true;
            //            } else {
            //                return false;
            //            }
            //        }
            //    }, valueNotEquals: 0
            //}
        //}
    });
}

function loadObjIssueByID(pissueID, pfnOnSuccess) {
    _callProcedure({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading Issue...",
        name: "[dbo].[spMakerCheckerListIssues]",
        params: [
            { "Name": "@IssueID", "Value": pissueID }
        ],
        success: {
            fn: function (responseList) {
                if (responseList.length > 0) {
                    pfnOnSuccess(responseList[0]);
                }
            }
        }
    });
}

function loadNewIssue() {
    // New Issue ID
    $("#lblMCIssueID").html(generateMakerCheckerID());

    //Accountable Person
    _createSelectSOEID({
        id: "#selAccountablePerson",
        selSOEID: ""
    });

    // Date of Occurrence
    var currentDate = moment().format('MMM DD, YYYY');
    $("#dateOfOccurrence").val(currentDate);
    $("#dateOfOccurrence").datepicker();

    // Reporting Period
    loadReportingPeriod({
        showTo: "#selReportingPeriod"
    });

    // Country
    loadSelectOfCountries();

    // Reason
    loadSelectOfReasons();

    //Reported By
    _createSelectSOEID({
        id: "#selReportedBy",
        selSOEID: _getSOEID()
    });

    // Action Resolution Date
    $("#ActionResolutionDate").val();
    $("#ActionResolutionDate").datepicker();
}

function loadEditIssue(objEditIssue) {
    // Issue ID
    $("#lblMCIssueID").html(objEditIssue.IssueID);

    //Accountable Person
    _createSelectSOEID({
        id: "#selAccountablePerson",
        selSOEID: objEditIssue.AccountablePersonSOEID
    });

    // Process Area
    $("#HFProcessAreaID").val(objEditIssue.ProcessID);
    _initProcessTree("ProcessArea", {
        onSuccess: function () { }
    });

    // Accountable Person Role
    $('#selAccountablePersonRole option[value="' + objEditIssue.AccountablePersonRole + '"]').prop('selected', true);

    // Date of Occurrence
    //console.log(objEditIssue.DateOfOccurrence);
    var currentDate = moment(objEditIssue.DateOfOccurrence, 'M/DD/YYYY hh:mm:ss A').format('MMM DD, YYYY');
    $("#dateOfOccurrence").val(currentDate);
    $("#dateOfOccurrence").datepicker();

    // Reporting Period
    loadReportingPeriod({
        showTo: "#selReportingPeriod",
        reportingPeriod: objEditIssue.ReportingPeriod
    });

    // Country
    loadSelectOfCountries(objEditIssue.Country);

    // Reason
    loadSelectOfReasons(objEditIssue.ReasonID);

    // Description
    $("#txtDescription").val(objEditIssue.Description);

    // Reported By
    _createSelectSOEID({
        id: "#selReportedBy",
        selSOEID: objEditIssue.ReportedBy
    });

    // Reported By Role
    $('#selReportedByRole option[value="' + objEditIssue.ReportedByRole + '"]').prop('selected', true);

    // Action Taken
    $("#txtActionTaken").val(objEditIssue.ActionTaken);

    // Action Resolution Date
    if (objEditIssue.ActionResolutionDate) {
        var ActionResolutionDate = moment(objEditIssue.ActionResolutionDate, 'M/DD/YYYY hh:mm:ss A').format('MMM DD, YYYY');
        $("#ActionResolutionDate").val(ActionResolutionDate);
        $("#ActionResolutionDate").datepicker();
    }

    // Status
    $('#selStatus option[value="' + objEditIssue.Status + '"]').prop('selected', true);

}

function getObjIssue() {
    var objIssue = {
        IssueID: { FieldName: 'Issue ID', FieldValue: $("#lblMCIssueID").html() },
        AccountablePersonSOEID: { FieldName: 'Accountable Person', FieldValue: $("#selAccountablePerson select").val() },
        ProcessID: { FieldName: 'Process Area', FieldValue: $(".fieldIDProcessArea").attr("value") },
        AccountablePersonRole: { FieldName: 'Accountable Person Role', FieldValue: $("#selAccountablePersonRole").val() },
        DateOfOccurrence: { FieldName: 'Date of Occurrence', FieldValue: moment($("#dateOfOccurrence").val(), 'MMM DD, YYYY').format('YYYY-MM-DD') },
        ReportingPeriod: { FieldName: 'Reporting Period', FieldValue: $("#selReportingPeriod").find("option:selected").val() },
        ReportingPeriodYear: { FieldName: 'Reporting Period Year', FieldValue: $("#selReportingPeriod").find("option:selected").attr("year") },
        ReportingPeriodMonth: { FieldName: 'Reporting Period Month', FieldValue: $("#selReportingPeriod").find("option:selected").attr("month") },
        Country: { FieldName: 'Country', FieldValue: $("#selCountry").val() },
        ReasonID: { FieldName: 'Reason', FieldValue: $("#selReason").val() },
        Description: { FieldName: 'Short Description of Error', FieldValue: $("#txtDescription").val() },
        ReportedBySOEID: { FieldName: 'Reported By', FieldValue: $("#selReportedBy select").val() },
        ReportedByRole: { FieldName: 'Reported By Role', FieldValue: $("#selReportedByRole").val() },
        ActionTaken: { FieldName: 'Action Taken', FieldValue: $("#txtActionTaken").val() },
        ActionResolutionDate: { FieldName: 'Action Resolution Date', FieldValue: ($("#ActionResolutionDate").val() ? moment($("#ActionResolutionDate").val(), 'MMM DD, YYYY').format('YYYY-MM-DD') : "") },
        Status: { FieldName: 'Status', FieldValue: $("#selStatus").val() },
    };

    return objIssue;
}

function saveIssue() {
    if ($("#formMakerCheckerIssue").valid()) {
        var objIssue = getObjIssue();
        var action = "Add";

        if ($("#HFAction").val() == "Edit") {
            action = "Edit";
        }

        //Save change to get the ID
        _callProcedure({
            loadingMsgType: "topBar",
            loadingMsg: "Saving Issue...",
            name: "[dbo].[spMakerCheckerAdminIssue]",
            params: [
                { "Name": "@Action", "Value": action },
                { "Name": "@SessionSOEID", "Value": _getSOEID() },
                { "Name": "@IssueID", "Value": objIssue.IssueID.FieldValue },
                { "Name": "@AccountablePersonSOEID", "Value": objIssue.AccountablePersonSOEID.FieldValue },
                { "Name": "@AccountablePersonRole", "Value": objIssue.AccountablePersonRole.FieldValue },
                { "Name": "@ProcessID", "Value": objIssue.ProcessID.FieldValue },
                { "Name": "@DateOfOccurrence", "Value": objIssue.DateOfOccurrence.FieldValue },
                { "Name": "@ReportingPeriod", "Value": objIssue.ReportingPeriod.FieldValue },
                { "Name": "@ReportingPeriodYear", "Value": objIssue.ReportingPeriodYear.FieldValue },
                { "Name": "@ReportingPeriodMonth", "Value": objIssue.ReportingPeriodMonth.FieldValue },
                { "Name": "@Country", "Value": objIssue.Country.FieldValue },
                { "Name": "@ReasonID", "Value": objIssue.ReasonID.FieldValue },
                { "Name": "@Description", "Value": objIssue.Description.FieldValue },
                { "Name": "@ReportedBy", "Value": objIssue.ReportedBySOEID.FieldValue },
                { "Name": "@ReportedByRole", "Value": objIssue.ReportedByRole.FieldValue },
                { "Name": "@ActionTaken", "Value": objIssue.ActionTaken.FieldValue },
                { "Name": "@ActionResolutionDate", "Value": objIssue.ActionResolutionDate.FieldValue },
                { "Name": "@Status", "Value": objIssue.Status.FieldValue }
            ],
            success: {
                fn: function (response) {
                    _showAlert({
                        type: "success",
                        content: "The issue was saved successfully.",
                        animateScrollTop: true
                    });

                    //Refresh issue List
                    if (loadMakerCheckerIssueList) {
                        loadMakerCheckerIssueList();
                    }
                }
            }
        });
    }
}