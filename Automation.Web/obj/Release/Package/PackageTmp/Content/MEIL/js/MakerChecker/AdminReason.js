﻿/// <reference path="../../../Shared/plugins/util/global.js" />

var idMainJqxGrid = "#tblReasons";

$(document).ready(function () {
    //_hideMenu();

    //Add new row to idMainJqxGrid
    $(".btnAddNew").click(function () {
        addNewRow();
    });

    //Delete selected row to idMainJqxGrid
    $(".btnDelete").click(function () {
        deleteRow();
    });

    //Save table idMainJqxGrid
    $(".btnSave").click(function () {
        saveTable();
    });

    //Reload Table idMainJqxGrid
    $(".btnReload").click(function () {
        loadTableReasons();
    });

    //Load table 
    loadTableReasons();
});

function loadTableReasons() {
    $.jqxGridApi.create({
        showTo: idMainJqxGrid,
        options: {
            //for comments or descriptions
            height: "450",
            autoheight: false,
            autorowheight: false,
            selectionmode: "singlerow",
            showfilterrow: true,
            sortable: true,
            editable: true,
            groupable: false
        },
        sp: {
            Name: "[dbo].[spMakerCheckerAdminReason]",
            Params: [
                { Name: "@Action", Value: "List" }
            ]
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling
            dataBinding: "Large Data Set"
        },
        columns: [
            //type: string - text - number - int - float - date - time 
            //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
            //cellsformat: ddd, MMM dd, yyyy h:mm tt
            { name: 'ID', type: 'number', hidden: true },
            { name: 'Description', text: 'Reason', width: '100%', type: 'string', filtertype: 'input' }
        ],
        ready: function () { }
    });
}

function addNewRow() {
    var $jqxGrid = $(idMainJqxGrid);
    var datarow = {
        ID: 0,
        Description: ''
    };
    var commit = $jqxGrid.jqxGrid('addrow', null, datarow);

    _showNotification("success", "An empty row was added in the table.");
}

function deleteRow() {
    //Delete 
    var objRowSelected = $.jqxGridApi.getOneSelectedRow(idMainJqxGrid, true);
    if (objRowSelected) {
        var htmlContentModal = "";
        htmlContentModal += "<b>Reason: </b>" + objRowSelected['Description'] + "<br/>";

        _showModal({
            width: '40%',
            modalId: "modalDelRow",
            addCloseButton: true,
            buttons: [{
                name: "Delete",
                class: "btn-danger",
                onClick: function () {
                    if (objRowSelected.ID != 0) {
                        _callProcedure({
                            loadingMsgType: "fullLoading",
                            loadingMsg: "Saving reasons...",
                            name: "[dbo].[spMakerCheckerAdminReason]",
                            params: [
                                { "Name": "@Action", "Value": "Delete" },
                                { "Name": "@IDReason", "Value": objRowSelected.ID },
                                { "Name": "@Description", "Value": objRowSelected.Description },
                                { "Name": "@SessionSOEID", "Value": _getSOEID() }
                            ],
                            //Show message only for the last 
                            success: {
                                showTo: $(idMainJqxGrid).parent(),
                                msg: "Row was deleted successfully.",
                                fn: function () {
                                    var reloadData = setInterval(function () {
                                        //console.log("Reloading data from #tblReasons");
                                        //$.jqxGridApi.localStorageFindById(idMainJqxGrid).fnReload();
                                        //$(idMainJqxGrid).jqxGrid('updateBoundData');

                                        loadTableReasons();

                                        //Remove changes
                                        $.jqxGridApi.rowsChangedFindById(idMainJqxGrid).rows = [];

                                        clearInterval(reloadData);
                                    }, 500);
                                }
                            }
                        });
                    } else {
                        $(idMainJqxGrid).jqxGrid('deleterow', objRowSelected.uid);
                    }
                }
            }],
            title: "Are you sure you want to delete this row?",
            contentHtml: htmlContentModal
        });
    }
}

function saveTable() {
    var rowsChanged = $.jqxGridApi.rowsChangedFindById(idMainJqxGrid).rows;
    
    //Validate changes
    if (rowsChanged.length > 0) {
        var fnSuccess = {
            showTo: $(idMainJqxGrid).parent(),
            msg: "The data was saved successfully.",
            fn: function () {
                var reloadData = setInterval(function () {
                    //console.log("Reloading data from #tblReasons");
                    //$.jqxGridApi.localStorageFindById(idMainJqxGrid).fnReload();
                    //$(idMainJqxGrid).jqxGrid('updateBoundData');
                    
                    loadTableReasons();

                    //Remove changes
                    $.jqxGridApi.rowsChangedFindById(idMainJqxGrid).rows = [];

                    clearInterval(reloadData);
                }, 1500);
            }
        };

        for (var i = 0; i < rowsChanged.length; i++) {
            var objRow = rowsChanged[i];
            var action = "Edit";

            if (objRow.ID == 0) {
                var action = "Insert";
            }

            _callProcedure({
                loadingMsgType: "fullLoading",
                loadingMsg: "Saving reasons...",
                name: "[dbo].[spMakerCheckerAdminReason]",
                params: [
                    { "Name": "@Action", "Value": action },
                    { "Name": "@IDReason", "Value": objRow.ID },
                    { "Name": "@Description", "Value": objRow.Description },
                    { "Name": "@SessionSOEID", "Value": _getSOEID() }
                ],
                //Show message only for the last 
                success: (i == rowsChanged.length - 1) ? fnSuccess : null
            });
        }
    } else {
        _showAlert({
            showTo: $(idMainJqxGrid).parent(),
            type: 'error',
            title: "Message",
            content: "No changes detected."
        });
    }
}