/// <reference path="../../../Shared/plugins/util/global.js" />
/// <reference path="MakerCheckerGlobal.js" />
$(document).ready(function () {

    //Load table of issues
    loadMakerCheckerIssueList();

    //Hide Menu
    _hideMenu();

    //On Click Delete Issue
    $(".btnDeleteIssue").click(function () {
        deleteIssue();
    });

    //On Click Download Excel
    $(".btnDownload").click(function () {
        downloadIssueList();
    });
});

function loadMakerCheckerIssueList() {
    $.jqxGridApi.create({
        showTo: "#tblMakerCheckerIssues",
        options: {
            //for comments or descriptions
            height: "500",
            autoheight: false,
            autorowheight: false,
            showfilterrow: true,
            sortable: true,
            editable: true,
            selectionmode: "singlerow"
        },
        sp: {
            Name: "[dbo].[spMakerCheckerListIssuesPaging]",
            Params: [],
        },
        source: {
            // Large Data Set / Virtual Paging / Virtual Scrolling / Server Paging / Server Scrolling / Large Data Set Local
            dataBinding: "Server Paging",
            pagesize: 200
        },
        groups: [],
        columns: [
            //type: string - text - number - int - float - date - time 
            //filtertype: number - input - date - range - textbox - default - list - checkedlist - bool - boolean
            //cellsformat: ddd, MMM dd, yyyy h:mm tt
            { name: 'CreatedDate', text: 'Created Date', width: '130px', type: 'date', filtertype: 'date', cellsformat: 'MMM dd, yyyy' },
            { name: 'CreatedBy', text: 'Created By', width: '100px', type: 'string', filtertype: 'input' },
            { name: 'IssueID', text: 'Issue ID', width: '160px', type: 'string', filtertype: 'input' },
            { name: 'AccountablePersonSOEID', text: 'Accountable Person', width: '150px', type: 'string', filtertype: 'input' },
            { name: 'ManagerAccountableSOEID', text: 'Manager Accountable', width: '150px', type: 'string', filtertype: 'input' },
            { name: 'AccountablePersonRole', text: 'Role', width: '110px', type: 'string', filtertype: 'input' },
            { name: 'Process', text: 'Process Area', width: '315px', type: 'string', filtertype: 'input' },
            { name: 'Country', text: 'Country', width: '135px', type: 'string', filtertype: 'input' },
            { name: 'Reason', text: 'Reason', width: '290px', type: 'string', filtertype: 'input' },
            { name: 'DateOfOccurrence', text: 'Date Of Ocurrence', width: '130px', type: 'date', filtertype: 'date', cellsformat: 'MMM dd, yyyy' },
            { name: 'ReportingPeriod', text: 'Reporting Period', width: '120px', type: 'string', filtertype: 'input' },
            { name: 'Description', text: 'Short Description', width: '500px', type: 'string', filtertype: 'input' }
        ],
        ready: function () {
            //On select row event
            $("#tblMakerCheckerIssues").on('rowselect', function (event) {
                $(".btnViewIssue").attr("href", _getViewVar("SubAppPath") + "/MEIL/MakerChecker/AdminIssue?pissueID=" + event.args.row.IssueID);
            });
        }
    });
}

function downloadIssueList() {
    _downloadExcel({
        spName: "[spMakerCheckerListIssues]",
        spParams: [],
        filename: "MakerCheckerIssueList-" + moment().format('MMM-DD-YYYY-HH-MM'),
        success: {
            msg: "Please wait, generating report..."
        }
    });
}

function deleteIssue() {
    //Delete 
    var objRowSelected = $.jqxGridApi.getOneSelectedRow("#tblMakerCheckerIssues", true);
    if (objRowSelected) {
        var htmlContentModal = "<b>Issue ID:</b>" + objRowSelected["IssueID"] + "<br/>";

        _showModal({
            width: '35%',
            modalId: "modalDel",
            addCloseButton: true,
            buttons: [{
                name: "Delete",
                class: "btn-danger",
                onClick: function () {
                    //Save change to get the ID
                    _callProcedure({
                        loadingMsgType: "fullLoading",
                        loadingMsg: "Deleting Issue '" + objRowSelected["IssueID"] + "'...",
                        name: "[dbo].[spMakerCheckerAdminIssue]",
                        params: [
                            { "Name": "@Action", "Value": "Delete" },
                            { "Name": "@SessionSOEID", "Value": _getSOEID() },
                            { "Name": "@IssueID", "Value": objRowSelected["IssueID"] }
                        ],
                        success: {
                            fn: function (response) {
                                _showNotification("success", "The issue '" + objRowSelected["IssueID"] + "' was deleted successfully.");

                                //Refresh issue List
                                if (loadMakerCheckerIssueList) {
                                    loadMakerCheckerIssueList();
                                }
                            }
                        }
                    });
                }
            }],
            title: "Are you sure you want to delete this row?",
            contentHtml: htmlContentModal
        });
    }
}