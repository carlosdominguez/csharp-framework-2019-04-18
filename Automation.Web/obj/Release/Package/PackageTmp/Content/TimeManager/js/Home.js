$(document).ready(function () {
    
    GetProjects();

});

function GetProjects()
{
    _callServer({
        loadingMsgType: "fullLoading",
        loadingMsg: "Loading data...",
        url: '/Home/getTotalProjects',
        type: "post",
        success: function (json) {
            var response = jQuery.parseJSON(json);

            var data2 = response;

            var source =
           {
               unboundmode: true,
               localdata: data2,
               datafields:
                       [
                            { name: 'RoleID', type: 'string' }
                          , { name: 'NumberRole', type: 'string' }
                          , { name: 'NameRole', type: 'string' }
                          , { name: 'GOCRole', type: 'string' }
                          , { name: 'MSRole', type: 'string' }
                          , { name: 'MGRole', type: 'string' }
                          , { name: 'RegionRole', type: 'string' }
                          , { name: 'CountryRole', type: 'string' }
                          , { name: 'LVIDRole', type: 'string' }
                          , { name: 'CreatedByRole', type: 'string' }
                          , { name: 'CreatedDateRole', type: 'string' }
                          , { name: 'LastUpdateRole', type: 'string' }
                       ],
               datatype: "json"
           };
            var dataAdapter = new $.jqx.dataAdapter(source);

            var imgViewSource = function (row, datafield, value) {

                var rowData = $('#jqxGridRoles').jqxGrid('getrowdata', row);

                var ID = rowData.IDType;
                var Number = rowData.Number;

                return '<center>' + Number + '   <i class="icon-search" style=" font-size: 23px; color: #4286f4;" onclick="openPopUpLiveStage(' + ID + ')" ></center>';
            }

            var imgDSTemp = function (row, datafield, value) {

                var rowData = $('#jqxGridRoles').jqxGrid('getrowdata', row);

                var directStaft = rowData.IsDirectStaf;

                if (IsDirectStafRole == "Yes") {
                    return '<div style="margin-left: 10%;">Direct Staft</div>';
                } else {
                    return '<div  style="margin-left: 10%;">Temp</div>';
                }
            }

            var imgSelectRole = function (row, datafield, value) {

                var rowData = $('#jqxGridRoles').jqxGrid('getrowdata', row);

                var ret = '';
                //return '<button type="button" class="btn btn-secondary btn-icon bottom15 right15"   onClick="reviewRole(' + rowData.RoleID + ',' + row + ')"><i class="fa fa-edit "></i></button>';
                ret = ret + '<button type="button" class="btn btn-primary"  title="Edit Row" onClick="reviewRole(' + rowData.RoleID + ',' + row + ')"><i class="fa fa-edit"></i></button>';
                //ret = ret + '<button type="button" class="btn btn-primary"  title="Edit Requisition" onClick="reviewRole(' + rowData.RoleID + ',' + row + ')"><i class="fa fa-edit"></i></button>'
                //ret = ret + '<button type="button" class="btn btn-primary"  title="Edit MOU/NONMOU" onClick="reviewRole(' + rowData.RoleID + ',' + row + ')"><i class="fa fa-edit"></i></button>'
                //ret = ret + '<button type="button" class="btn btn-primary"  title="Edit MOU/NONMOU" onClick="reviewRole(' + rowData.RoleID + ',' + row + ')"><i class="fa fa-edit"></i></button>'
                return ret;

            }

            var initrowdetails = function (index, parentElement, gridElement, record) {
                var id = record.uid.toString();

                var gridRole = $($(parentElement).children()[0].children[1]);
                var gridMOU = $($(parentElement).children()[1].children[1]);
                var gridReq = $($(parentElement).children()[2].children[1]);
                var gridDriver = $($(parentElement).children()[3].children[1]);
                var gridEmployee = $($(parentElement).children()[4].children[1]);

                var nestedData;

                nestedData = $('#jqxGridRoles').jqxGrid('getrowdata', index);

                var orderssource = {
                    localdata: nestedData,
                    datafields:
                       [
                           { name: 'RoleID', type: 'string' },
                           { name: 'NumberRole', type: 'string' },
                           { name: 'NameRole', type: 'string' },
                           { name: 'GOCRole', type: 'string' },
                           { name: 'MSRole', type: 'string' },
                           { name: 'MGRole', type: 'string' },
                           { name: 'RegionRole', type: 'string' },
                           { name: 'CountryRole', type: 'string' },
                           { name: 'LVIDRole', type: 'string' },
                           { name: 'CreatedByRole', type: 'string' },
                           { name: 'CreatedDateRole', type: 'string' },
                           { name: 'LastUpdateRole', type: 'string' },
                           { name: 'LastUpdateDateRole', type: 'string' },
                           { name: 'IsFteableRole', type: 'string' },
                           { name: 'IsDirectStafRole', type: 'string' },
                           { name: 'RoleIDRoleTypeID', type: 'string' },
                           { name: 'AutoNumericSource', type: 'string' },
                           { name: 'SourceID', type: 'string' },
                           { name: 'IsActiveSource', type: 'string' },
                           { name: 'StartMonth', type: 'string' },
                           { name: 'StartYear', type: 'string' },
                           { name: 'EndMonth', type: 'string' },
                           { name: 'EndYear', type: 'string' },
                           { name: 'DriverID', type: 'string' },
                           { name: 'SourceStatus', type: 'string' },
                           { name: 'SourceName', type: 'string' },
                           { name: 'DriverName', type: 'string' },
                           { name: 'DriverType', type: 'string' },
                           { name: 'RLSDriver', type: 'string' },
                           { name: 'SourceDriver', type: 'string' },
                           { name: 'IsAdd', type: 'string' },
                           { name: 'IsLess', type: 'string' },
                           { name: 'ImpactFRODemand', type: 'string' },
                           { name: 'ImpactType', type: 'string' },
                           { name: 'StatusRole', type: 'string' },
                           { name: 'mgdGeographyDescription', type: 'string' },
                           { name: 'ManagedSegmentDescription', type: 'string' },
                           { name: 'LocationID', type: 'string' },
                           { name: 'LocationName', type: 'string' },
                           { name: 'CenterID', type: 'string' },
                           { name: 'ProfileID', type: 'string' },
                           { name: 'Profile', type: 'string' },
                           { name: 'GOCDescription', type: 'string' },
                           { name: 'EmployeeID', type: 'string' },
                           { name: 'StartDate', type: 'string' },
                           { name: 'EndDate', type: 'string' },
                           { name: 'IsActiveEmployeeRole', type: 'string' },
                           { name: 'ReqID', type: 'string' },
                           { name: 'IsInternalMovement', type: 'string' },
                           { name: 'SOEID', type: 'string' },
                           { name: 'FirstName', type: 'string' },
                           { name: 'LastName', type: 'string' },
                           { name: 'HiringDate', type: 'string' },
                           { name: 'AttritionDate', type: 'string' },
                           { name: 'IsActive', type: 'string' },
                           { name: 'ClassificationID', type: 'string' },
                           { name: 'ClassificationLetter', type: 'string' },
                           { name: 'ClassificationDescription', type: 'string' },
                           { name: 'RequisitionNumber', type: 'string' },
                           { name: 'RequisitionName', type: 'string' },
                           { name: 'RequisitionCreationDate', type: 'string' },
                           { name: 'RequisitionStatusID', type: 'string' },
                           { name: 'Center', type: 'string' },
                           { name: 'PosibleStart', type: 'string' },
                           { name: 'PosibleEnd', type: 'string' }
                       ],
                    datatype: "json"
                };

                var nestedGridAdapter = new $.jqx.dataAdapter(orderssource);
                nestedGridAdapter.dataBind();

                if (gridRole != null) {

                    gridRole.jqxGrid({
                        source: nestedGridAdapter,
                        theme: 'energyblue',
                        width: '100%',
                        autoheight: true,
                        autorowheight: true,
                        pageable: false,
                        filterable: false,
                        showfilterrow: false,
                        columnsresize: true,
                        enablebrowserselection: true,
                        columns: [
                            { text: 'Number Role', dataField: 'NumberRole', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Name Role', dataField: 'NameRole', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Center', dataField: 'Center', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Location Name', dataField: 'LocationName', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Profile', dataField: 'Profile', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Managed Segment', dataField: 'ManagedSegmentDescription', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Managed Geography', dataField: 'mgdGeographyDescription', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Region ', dataField: 'RegionRole', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Country', dataField: 'CountryRole', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'LVID', dataField: 'LVIDRole', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Created By', dataField: 'CreatedByRole', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Created Date', dataField: 'CreatedDateRole', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Last Update', dataField: 'LastUpdateRole', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Last Update Date', dataField: 'LastUpdateDateRole', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Is Fteable', dataField: 'IsFteableRole', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Is Direct Staft', dataField: 'IsDirectStafRole', filtertype: 'checkedlist', width: '9%', editable: false }
                        ]

                    });
                    gridMOU.jqxGrid({
                        source: nestedGridAdapter,
                        theme: 'energyblue',
                        width: '100%',
                        autoheight: true,
                        autorowheight: true,
                        pageable: false,
                        filterable: false,
                        showfilterrow: false,
                        columnsresize: true,
                        enablebrowserselection: true,
                        columns: [
                            { text: 'Auto Numeric', dataField: 'AutoNumericSource', width: '9%', editable: false },
                            { text: 'Posible Start', dataField: 'PosibleStart', width: '9%', editable: false },
                            { text: 'Posible End', dataField: 'PosibleEnd', width: '9%', editable: false }
                        ]

                    });
                    gridDriver.jqxGrid({
                        source: nestedGridAdapter,
                        theme: 'energyblue',
                        width: '100%',
                        autoheight: true,
                        autorowheight: true,
                        pageable: false,
                        filterable: false,
                        showfilterrow: false,
                        columnsresize: true,
                        enablebrowserselection: true,
                        columns: [
                            { text: 'Driver Name', dataField: 'DriverName', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Source Driver', dataField: 'SourceDriver', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Driver Type', dataField: 'DriverType', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'RLS Driver', dataField: 'RLSDriver', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Impact FRO Demand', dataField: 'ImpactFRODemand', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Impact Type', dataField: 'ImpactType', filtertype: 'checkedlist', width: '9%', editable: false }

                        ]

                    });
                    gridReq.jqxGrid({
                        source: nestedGridAdapter,
                        theme: 'energyblue',
                        width: '100%',
                        autoheight: true,
                        autorowheight: true,
                        pageable: false,
                        filterable: false,
                        showfilterrow: false,
                        columnsresize: true,
                        enablebrowserselection: true,
                        columns: [
                            { text: 'Requisition Number', dataField: 'RequisitionNumber', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Requisition Name ', dataField: 'RequisitionName', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Creation Date ', dataField: 'RequisitionCreationDate', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Requisition Status', dataField: 'RequisitionStatusID', filtertype: 'checkedlist', width: '9%', editable: false }
                        ]

                    });
                    gridEmployee.jqxGrid({
                        source: nestedGridAdapter,
                        theme: 'energyblue',
                        width: '100%',
                        autoheight: true,
                        autorowheight: true,
                        pageable: false,
                        filterable: false,
                        showfilterrow: false,
                        columnsresize: true,
                        enablebrowserselection: true,
                        columns: [
                            { text: 'SOEID', dataField: 'SOEID', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'First Name', dataField: 'FirstName', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Last Name', dataField: 'LastName', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Hiring Date', dataField: 'HiringDate', filtertype: 'checkedlist', width: '30%', editable: false },
                            { text: 'Classification', dataField: 'ClassificationLetter', filtertype: 'checkedlist', width: '9%', editable: false },
                            { text: 'Description', dataField: 'ClassificationDescription', filtertype: 'checkedlist', width: '9%', editable: false }
                        ]

                    });


                }
            };

            var addfilter = function () {
                var filtergroup = new $.jqx.filter();
                var filter_or_operator = 1;
                var filtervalue = $('#HFRoleID').val();
                var filtercondition = 'contains';
                var filter1 = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);

                filtergroup.addfilter(filter_or_operator, filter1);

                // add the filters.
                $("#jqxGridRoles").jqxGrid('addfilter', 'NumberRole', filtergroup);
                // apply the filters.
                $("#jqxGridRoles").jqxGrid('applyfilters');
            }


            $('#jqxGridRoles').jqxGrid(
           {
               source: dataAdapter,
               theme: 'energyblue',
               width: '100%',
               selectionmode: 'multiplecellsadvanced',
               sortable: true,
               height: 700,
               autoheight: false,
               autorowheight: false,
               pageable: false,
               filterable: true,
               editable: true,
               showfilterrow: true,
               columnsresize: true,
               selectionmode: 'checkbox',
               enablebrowserselection: true,
               groupable: true,
               rowdetails: true,
               autoloadstate: false,
               autosavestate: false,
               initrowdetails: initrowdetails,
               ready: function () {
                   addfilter();
               },
               rowdetailstemplate: { rowdetails: '<fieldset><legend>Role Info</legend><div id="grid1"></div></fieldset><fieldset><legend>MOU Info</legend><div id="grid2"></div></fieldset><fieldset><legend>Requisition Info</legend><div id="grid3"></div></fieldset><fieldset><legend>Driver Info</legend><div id="grid4"></div></fieldset><fieldset><legend>Employee Info</legend><div id="grid5"></div></fieldset></br></br>', rowdetailsheight: 650, rowdetailshidden: true },
               groups: ['Center', 'GOCDescription'],
               columns: [

                       { text: 'Role', dataField: 'NumberRole', width: '10%', filtertype: 'input', editable: false },
                       { text: 'Role Status', dataField: 'StatusRole', width: '6%', filtertype: 'input', editable: true },
                       { text: 'Center', dataField: 'Center', width: '6%', filtertype: 'input', editable: false },
                       { text: 'MOU/NonMOU #', dataField: 'AutoNumericSource', width: '10%', filtertype: 'input', editable: false },
                       { text: 'GOC', dataField: 'GOCDescription', width: '10%', filtertype: 'input', editable: false },
                       {
                           text: 'Driver', dataField: 'DriverName', columntype: 'combobox', width: '10%', filtertype: 'input', editable: true,
                           createeditor: function (row, column, editor) {
                               // assign a new data source to the combobox.
                               var list = ['New Work MOU',
                                            'Transfer In MOU',
                                            'Transfer out MOU',
                                            'Project MOU',
                                            'Investments',
                                            'Reengineering',
                                            'Right Placement Out',
                                            'Reduction Placement Out',
                                            'Divestiture',
                                            'Right Placement In MOU',
                                            'Replacements',
                                            'Attrition',
                                            'Approved Resources',
                                            'Mobility Out (Leave FRO)',
                                            'Employee Type Reclass',
                                            'Internal Movements',
                                            'Internal Restructure',
                                            'Work Absorption MOU',
                                            'Plan'];



                               editor.jqxComboBox({ autoDropDownHeight: true, source: list, promptText: "Please Choose:" });
                           }
                       },
                       { text: 'Is DirectStaf', dataField: 'IsDirectStafRole', width: '3%', filtertype: 'input', editable: false },
                       { text: 'Requisition', dataField: 'RequisitionNumber', width: '6%', filtertype: 'input', editable: false },
                       { text: 'SOEID', dataField: 'SOEID', width: '6%', filtertype: 'input', editable: false },
                       { text: 'Posible Start', dataField: 'PosibleStart', width: '10%', editable: false },
                       { text: 'Posible End', dataField: 'PosibleEnd', width: '10%', editable: false },
                       { text: 'Hiring Date', dataField: 'HiringDate', width: '6%', editable: false }

               ]

           });

            var listSource = [
                               { text: 'Role', value: 'NumberRole', checked: true },
                               { text: 'Center', value: 'Center', checked: true },
                               { text: 'MOU/NonMOU #', value: 'AutoNumericSource', checked: true },
                               { text: 'GOC', value: 'GOCDescription', checked: true },
                               { text: 'Driver', value: 'DriverName', checked: true },
                               { text: 'Is DirectStaf', value: 'IsDirectStafRole', checked: true },
                               { text: 'Requisition', value: 'RequisitionNumber', checked: true },
                               { text: 'SOEID', value: 'SOEID', checked: true },
                               { text: 'Employee Classification', value: 'ClassificationDescription', checked: true },
                               { text: 'Role Status', value: 'StatusRole', checked: true },
            ];








            $("#jqxlistbox").jqxListBox({ source: listSource, width: 240, height: 240, checkboxes: true });
            $("#jqxlistbox").on('checkChange', function (event) {
                $("#jqxGridRoles").jqxGrid('beginupdate');
                if (event.args.checked) {
                    $("#jqxGridRoles").jqxGrid('showcolumn', event.args.value);
                }
                else {
                    $("#jqxGridRoles").jqxGrid('hidecolumn', event.args.value);
                }
                $("#jqxGridRoles").jqxGrid('endupdate');
            });


        }
    });
}