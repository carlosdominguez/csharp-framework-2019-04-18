﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomReportViewer.aspx.cs" Inherits="Automation.Web.Views.Shared.CustomReportViewer" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lessIE7" style="background-color:white;"> <![endif]-->
<!--[if IE 7]>    <html class="IE7" style="background-color:white;"> <![endif]-->
<!--[if IE 8]>    <html class="IE8" style="background-color:white;"> <![endif]-->
<!--[if IE 9]>    <html class="IE9" style="background-color:white;"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="upIE9" style="background-color:white;">
<!--<![endif]-->
<head>
    <title>Report</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
</head>
<body class="report-body" style="background-color:white;overflow-x: auto;">

    <form id="form1" runat="server">
        <div>
            <asp:toolkitscriptmanager runat="server" id="ToolkitScriptManager_Master" enablepagemethods="true"
                scriptmode="Release" asyncpostbacktimeout="3600" enablepartialrendering="true">
            </asp:toolkitscriptmanager>
            <rsweb:ReportViewer ID="ReportViewer" runat="server" Width="100%" Height="100%" ShowBackButton="True" ProcessingMode="Local"
                SizeToReportContent="True" CssClass="AutoWH" OnBack="ReportViewer_Back" OnDrillthrough="ReportViewer_Drillthrough"
                Visible="true" AsyncRendering="false" BackColor="White">
            </rsweb:ReportViewer>
            <asp:hiddenfield id="HFIframeID" runat="server" value="" />
        </div>
        <%--CSS Automation Framework Input and Selects Style--%>
        <link href="<%# subAppPath %>/Content/Shared/plugins/bootstrap/3.3.7/css/bootstrap.css?v=<%# filesVersion %>" rel="stylesheet" />
        <link href="<%# subAppPath %>/Content/Shared/plugins/bootstrap/3.3.7/css/bootstrap-theme.min.css?v=<%# filesVersion %>" rel="stylesheet" />
        <link href="<%# subAppPath %>/Content/Shared/fonts/font-awesome/css/font-awesome.css?v=<%# filesVersion %>" rel="stylesheet" />
        
        <%--CSS Datepicker plugin--%>
        <link href="<%# subAppPath %>/Content/Shared/plugins/jqueryui/jquery-ui.min.css?v=<%# filesVersion %>" rel="stylesheet" />
        <link href="<%# subAppPath %>/Content/Shared/plugins/datepicker/datepicker.css?v=<%# filesVersion %>" rel="stylesheet" />
        <link href="<%# subAppPath %>/Content/Shared/css/style.css?v=<%# filesVersion %>" rel="stylesheet" />

        <%--JS Datepicker plugin--%>
        <script src="<%# subAppPath %>/Content/Shared/plugins/jquery/jquery-1.11.2.min.js?v=<%# filesVersion %>"></script>
        <script src="<%# subAppPath %>/Content/Shared/plugins/jqueryui/jquery-ui.min.js?v=<%# filesVersion %>"></script>
        <script src="<%# subAppPath %>/Content/Shared/plugins/bootstrap/3.3.7/js/bootstrap.min.js?v=<%# filesVersion %>"></script>

        <%--JS Fix Report Viewer--%>
        <%--<script src="/Content/Shared/plugins/report-viewer/fixreportviewer.js"></script>--%>

        <script type="text/javascript">
            console.log('JS: CustomReportViewer.aspx');

            $(document).ready(function () {
                if (parent._hideLoadingFullPage) {
                    parent._hideLoadingFullPage();
                }

                if (parent.resizeAllIframes) {
                    parent.resizeAllIframes();
                }

                console.log("clientHeight: " + document.body.scrollHeight);
            });

            Sys.Application.add_load(function () {
                console.log('iFrame add_propertyChanged...');
                $find("ReportViewer").add_propertyChanged(viewerPropertyChanged);
            });

            //var prm = Sys.WebForms.PageRequestManager.getInstance();
            //prm.add_initializeRequest(function (s, e) {
            //    $(".hasDatepicker").each(function () {
            //        var $dateInput = $(this);

            //        if ($dateInput.attr("addTime")) {
            //            var dateValue = $dateInput.val();

            //            $dateInput.val(dateValue + $dateInput.attr("addTime"));
            //            console.log(dateValue + $dateInput.attr("addTime"));
            //        }
            //    });
            //});

            $("input").on('click', function () {
                if (parent.resizeAllIframes) {
                    parent.resizeAllIframes();
                }
            });

            function viewerPropertyChanged(sender, e) {
                if (parent.resizeAllIframes) {
                    var iframeId = $('#<%= HFIframeID.ClientID %>').val();
                    parent.resizeAllIframes();
                }

                $("a[target='_top']").attr("target", "_blank");

                loadStyles();

                parent.bodyIframe = document.body;
            }

            function loadStyles() {
                //Set botton to right
                var $submitButton = $('[id*=ParameterTable_ReportViewer_]').find("[type='submit']");
                var $tdView = $submitButton.parent().parent().parent().parent().parent();
                var $trParameterRow = $("#ParametersRowReportViewer");
                var $trCollapseRow = $trParameterRow.next();
                var $trActionsReport = $trCollapseRow.next();
                var $trResultReport = $trActionsReport.next();
                var $tableReport = $trParameterRow.closest("table");

                //var $newTableFilter = $("<table></table");

                ////Move rows to avoid submit button lost
                //$newTableFilter.append($trParameterRow);
                //$newTableFilter.append($trCollapseRow);
                //$newTableFilter.append($trActionsReport);
                //$tableReport.insertBefore($newTableFilter);

                //$trParameterRow.css("position", "fixed");
                //$trParameterRow.css("width", "900px");

                $trCollapseRow.css("display", "none");

                //$trActionsReport.css("position", "fixed");
                //$trActionsReport.css("top", "66px");
                //$trActionsReport.css("z-index", "1");

                //$trResultReport.css("position", "fixed");
                //$trResultReport.css("top", "92px");

                $tdView.css("position", "fixed");
                $tdView.css("right", "0px");
                $tdView.css("background-color", "white");

                $tdView.closest("table").css("max-width", "850px");
                $tdView.prev().css("padding-right", "150px");

                //Add bootstrap style
                $submitButton.addClass("btn btn-primary facebook");
                $("input[type='text']").each(function () {
                    $input = $(this);
                    $input.addClass("form-control");
                    if ($input.attr("readonly")) {
                        $input.css("float", "left");
                    }
                });
                var $texts = $("[style='font-family:Verdana;font-size:8pt;'],[style='font-family: Verdana; font-size: 8pt;']");
                $texts.css("font-family", "'Open Sans', Arial, Helvetica, sans-serif");
                $texts.css("font-size", "14px");
                $("input[type='checkbox']").css("width", "20px");
                $("input[type='checkbox']").css("height", "20px");
                $("select").addClass("form-control");
                $("body").css("background-color", "white");

                if (parent.resizeAllIframes) {
                    $("body").css("overflow-y", "hidden");
                }
                
                //if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {

                //add time to date, to avoid errors
                var addTimeToDateInputFn = function () {
                    $(".hasDatepicker").each(function () {
                        var $dateInput = $(this);
                        var dateValue = $dateInput.val();

                        if ($dateInput.attr("addTime") && (dateValue.indexOf('12:00:00 AM') == -1)) {
                            $dateInput.val(dateValue + $dateInput.attr("addTime"));
                            console.log(dateValue + $dateInput.attr("addTime"));
                        }
                    });
                };

                //var addTimeToDateInputOneFn = function () {
                //    var $dateInput = $(this);
                //    var dateValue = $dateInput.val();

                //    if ($dateInput.attr("addTime") && (dateValue.indexOf('12:00:00 AM') == -1)) {
                //        $dateInput.val(dateValue + $dateInput.attr("addTime"));
                //        console.log(dateValue + $dateInput.attr("addTime"));
                //    }
                //};
                
                $trActionsReport.find("input[alt='First Page']").mouseover(addTimeToDateInputFn);
                $trActionsReport.find("input[alt='Previous Page']").mouseover(addTimeToDateInputFn);
                $trActionsReport.find("input[alt='Next Page']").mouseover(addTimeToDateInputFn);
                $trActionsReport.find("input[alt='Last Page']").mouseover(addTimeToDateInputFn);
                $("a[onclick]a[onkeypress]").mouseover(addTimeToDateInputFn);
                //$submitButton.click(addTimeToDateInputFn);

                //Remove time on load
                $('tr[isparameterrow="true"] td span').each(function (i, o) {
                    var $label = $(o);
                    if (($label.text().indexOf('M/D/YYYY') > -1) ||
                        ($label.text().indexOf('MM/DD/YYYY') > -1) || 
                        ($label.text().toLowerCase().indexOf('date') > -1)) {
                        var $dateInput = $label.parent().next().find('input[type="text"]');
                        var dateValue = $dateInput.val();

                        if (dateValue && (dateValue.indexOf('12:00:00 AM') > -1)) {
                            $dateInput.val(dateValue.replace(" 12:00:00 AM", ""));
                            $dateInput.attr("addTime", " 12:00:00 AM");
                        }

                        $dateInput.datepicker({
                            dateFormat: "m/d/yy"
                        });

                        //$dateInput.change(addTimeToDateInputOneFn);
                        //console.log($(o).text().indexOf('M/D/YYYY') > -1);
                        //$(o).parent().next().find('input[type="text"]').attr('type', 'date');
                    }
                });
                //}
            }
        </script>
    </form>

</body>
</html>