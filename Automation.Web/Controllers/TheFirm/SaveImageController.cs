﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
namespace Automation.Web.Controllers.TheFirm
{
    [RoutePrefix("TheFirm")]
    public class SaveImageMController : Controller
    {
        [Route("SaveImage")]
        public ActionResult SaveImage()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            return View("~/Views/TheFirm/SaveImage.cshtml", viewModel);
        }

        [Route("UploadFile")]
        public ActionResult UploadFile(string newFileName, string docExpDate, string jsonDocDetails)
        {
            Dictionary<string, object> result = new Dictionary<string, object> { { "success", true } };


                var objDocDetails = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonDocDetails);
                var objFile = GlobalUtil.GetFileFromCurrentRequest();
                string fullPathSaved = GlobalUtil.FileSaveAs("ImportPDFs", objFile.Filename, objFile.InputStream);
                var nasLocation = (new GlobalModel()).excecuteProcedureReturn("[dbo].[spFirmGetNasLocation]", new List<Dictionary<string, string>> {
                    #region SP Variables
                    new Dictionary<string, string>
                    {
                        { "Name", "@SOEID" },
                        { "Value", GlobalUtil.GetSOEID()}
                    }
                    #endregion
                });
               
                string fileName = System.IO.Path.GetFileName(fullPathSaved);
                string fullNewPath = nasLocation[0]["nasLocation"] + @"\" + newFileName;

                if (Directory.Exists(nasLocation[0]["nasLocation"]))
                {
                    if (!System.IO.File.Exists(fullNewPath))
                    {
                        var docID = (new GlobalModel()).excecuteProcedureReturn("[dbo].[spFirmAddDocument]", new List<Dictionary<string, string>> {
                            #region SP Variables
                            new Dictionary<string, string>
                            {
                                { "Name", "@docLocation" },
                                { "Value", nasLocation[0]["nasLocation"]}
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@docName" },
                                { "Value", newFileName}
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@sourceFileName" },
                                { "Value", fileName}
                            }
                            ,new Dictionary<string, string>
                            {
                                { "Name", "@expirationDate" },
                                { "Value", docExpDate}
                            }
                            ,new Dictionary<string, string>
                            {
                                { "Name", "@documentTypeID" },
                                { "Value", objDocDetails["docType"]}
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEID" },
                                { "Value", GlobalUtil.GetSOEID()}
                            }
                            #endregion
                        });

                        (new GlobalModel()).excecuteProcedureNoReturn("[dbo].[spFirmAddTaxDocument]", new List<Dictionary<string, string>> {
                            #region SP Variables
                            new Dictionary<string, string>
                            {
                                { "Name", "@docID" },
                                { "Value", docID[0]["docID"]}
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@logID" },
                                { "Value", objDocDetails["logId"]}
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@versionNumber" },
                                { "Value", objDocDetails["versionNumber"]}
                            }
                            ,new Dictionary<string, string>
                            {
                                { "Name", "@filingYear" },
                                { "Value", objDocDetails["Year"]}
                            }
                            ,new Dictionary<string, string>
                            {
                                { "Name", "@filingMonth" },
                                { "Value", objDocDetails["Month"]}
                            }
                            ,new Dictionary<string,string>
                            {
                                { "Name", "@searchKeys" },
                                { "Value", objDocDetails["searchKey"]}
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEID" },
                                { "Value", GlobalUtil.GetSOEID()}
                            }
                            #endregion
                        });

                        System.IO.File.Copy(fullPathSaved, fullNewPath);
                    }
                    else
                    {
                        result["msg"] = "File " + newFileName + "Already Exist in " + fullNewPath;
                        result["success"] = false;
                    }
                }
                else {
                    result["msg"] = "Directory " + nasLocation[0]["nasLocation"] + @"\ does not exist";
                    result["success"] = false;
                }

                string uploadFolder = GlobalUtil.GetValueAppConfig("appSettings", "UploadFolder") + "ImportPDFs" + @"\";
                string fullPathDelete = @"" + Server.MapPath(uploadFolder);
                System.IO.DirectoryInfo di = new DirectoryInfo(@"" + fullPathDelete);
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Route("UploadFileM")]
        public ActionResult UploadFileM(string jsonDocDetails, string docExpDate)
        {
            Dictionary<string, object> result = new Dictionary<string, object> { { "success", true } };

            var objDocDetails = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonDocDetails);
            var objFile = GlobalUtil.GetFileFromCurrentRequest();
            string fullPathSaved = GlobalUtil.FileSaveAs("ImportPDFs", objFile.Filename, objFile.InputStream);
            string logIdentifier = Regex.Match(objFile.Filename, @"L(.*)-").Groups[1].Value;
            int docVnumber = 0;
            string newFileName = "";
            //Get log Id
            #region Log Id 
            var logId = (new GlobalModel()).excecuteProcedureReturn("[dbo].[spFirmGetLogId]", new List<Dictionary<string, string>> {
                #region SP Variables
                new Dictionary<string, string>
                {
                    { "Name", "@logNumber" },
                    { "Value", logIdentifier},

                },
                new Dictionary<string, string>
                {
                    { "Name", "@teamID" },
                    { "Value", objDocDetails["teamID"]},

                }
                #endregion
            });
            #endregion

            //GET VERSION
            #region version
            var docVer = (new GlobalModel()).excecuteProcedureReturn("[dbo].[spFirmGetDocumentVersion]", new List<Dictionary<string, string>> {
                #region SP Variables
                new Dictionary<string, string>
                {
                    { "Name", "@logID" },
                    { "Value", logId[0]["logID"]},

                },
                new Dictionary<string, string>
                {
                    { "Name", "@FilingMonth" },
                    { "Value", objDocDetails["filingM"]},

                },
                new Dictionary<string, string>
                {
                    { "Name", "@FilingYear" },
                    { "Value", objDocDetails["filingY"]},

                }
                #endregion
            });
            #endregion

            if (docVer.Count != 0)
                docVnumber = Int32.Parse(docVer[0]["versionNumber"]) + 1;
            else 
                docVnumber = 1;

            newFileName = "TampaTaxGroup_" + objDocDetails["teamDescrip"] + "_" + logIdentifier + "_" + logId[0]["frequencyDescription"] + "_" + objDocDetails["filingM"] + "_" + objDocDetails["filingY"] + "_" + docVnumber + ".pdf";

            //GET LOCATION
            #region location
            var nasLocation = (new GlobalModel()).excecuteProcedureReturn("[dbo].[spFirmGetNasLocation]", new List<Dictionary<string, string>> {
                #region SP Variables
                new Dictionary<string, string>
                {
                    { "Name", "@SOEID" },
                    { "Value", GlobalUtil.GetSOEID()}
                }
                #endregion
            });
            #endregion

            string fileName = System.IO.Path.GetFileName(fullPathSaved);
            string fullNewPath = nasLocation[0]["nasLocation"] + @"\" + newFileName;

            if (Directory.Exists(nasLocation[0]["nasLocation"]))
            {
                if (!System.IO.File.Exists(fullNewPath))
                {

                    var docID = (new GlobalModel()).excecuteProcedureReturn("[dbo].[spFirmAddDocument]", new List<Dictionary<string, string>> {
                            #region SP Variables
                            new Dictionary<string, string>
                            {
                                { "Name", "@docLocation" },
                                { "Value", nasLocation[0]["nasLocation"]}
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@docName" },
                                { "Value", newFileName}
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@sourceFileName" },
                                { "Value", fileName}
                            }
                            ,new Dictionary<string, string>
                            {
                                { "Name", "@expirationDate" },
                                { "Value", docExpDate}
                            }
                            ,new Dictionary<string, string>
                            {
                                { "Name", "@documentTypeID" },
                                { "Value", objDocDetails["docType"]}
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEID" },
                                { "Value", GlobalUtil.GetSOEID()}
                            }
                            #endregion
                        });

                    (new GlobalModel()).excecuteProcedureNoReturn("[dbo].[spFirmAddTaxDocument]", new List<Dictionary<string, string>> {
                            #region SP Variables
                            new Dictionary<string, string>
                            {
                                { "Name", "@docID" },
                                { "Value", docID[0]["docID"]}
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@logID" },
                                { "Value", logId[0]["logID"]}
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@versionNumber" },
                                { "Value", docVnumber.ToString()}
                            }
                            ,new Dictionary<string, string>
                            {
                                { "Name", "@filingYear" },
                                { "Value", objDocDetails["filingY"]}
                            }
                            ,new Dictionary<string, string>
                            {
                                { "Name", "@filingMonth" },
                                { "Value", objDocDetails["filingM"]}
                            }
                            ,new Dictionary<string,string>
                            {
                                { "Name", "@searchKeys" },
                                { "Value", objDocDetails["keys"]}
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEID" },
                                { "Value", GlobalUtil.GetSOEID()}
                            }
                            #endregion
                        });

                    System.IO.File.Copy(fullPathSaved, fullNewPath);
                }
                else {
                    result["msg"] = "File " + newFileName + "already Exist in " + fullNewPath;
                    result["success"] = false;
                }
            } else {
                result["msg"] = "Directory " + nasLocation[0]["nasLocation"] + @"\ does not exist";
                result["success"] = false;
            }
                

            string uploadFolder = GlobalUtil.GetValueAppConfig("appSettings", "UploadFolder") + "ImportPDFs" + @"\";
            string fullPathDelete = @"" + Server.MapPath(uploadFolder);
            System.IO.DirectoryInfo di = new DirectoryInfo(@"" + fullPathDelete);
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }    
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}

