﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace Automation.Web.Controllers.TheFirm
{
    [RoutePrefix("TheFirm")]
    public class SDMigrationController : Controller
    {
        [Route("SDMigration")]
        public ActionResult SDMigration()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            return View("~/Views/TheFirm/SDMigration.cshtml", viewModel);
        }

    }

}
