﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace Automation.Web.Controllers.TheFirm
{

        [RoutePrefix("TheFirm")]
        public class SearchController : Controller
        {
            [Route("Search")]
            public ActionResult Search()
            {
                Dictionary<string, object> viewModel = new Dictionary<string, object>();

                return View("~/Views/TheFirm/Search.cshtml", viewModel);
            }

            [Route("DownloadPDF")]
            public ActionResult DownloadPDF(string thefirmpath, string fileName)
            {
                string fullPathToCopy = "";
                string hrefPath = "";
                try
                {
                    //SET UP PATHS AND FILES NAMES
                    string uploadFolder = GlobalUtil.GetValueAppConfig("appSettings", "UploadFolder") + "TheFirmDocs" + @"\";
                    hrefPath = uploadFolder + fileName;
                    fullPathToCopy = @"" + Server.MapPath(uploadFolder) + fileName;

                    //DELETE OLD FILES STORED ON THE DOWNLOADFOLDER
                    System.IO.DirectoryInfo di = new DirectoryInfo(@"" + Server.MapPath(uploadFolder));
                    foreach (FileInfo file in di.GetFiles())
                    {
                        try
                        {
                            file.Delete();
                        }
                        catch (Exception)
                        {}
                    }

                    //COPY NEW FILE FRO SHAREDRIVE TO DOWNLOAD FOLDER
                    System.IO.File.Copy(thefirmpath + @"\" + fileName, fullPathToCopy, true);
                }
                catch (Exception)
                {
                    hrefPath = "Error!";
                    throw;
                }
                return Content(hrefPath);
            }

            [Route("DeleteFile")]
            public ActionResult DeleteFile(string thefirmpath)
            {
                thefirmpath = GlobalUtil.DecodeSkipSideminder(thefirmpath);
                string hrefPath = "";
                try
                {
                    if (System.IO.File.Exists(thefirmpath)) {
                        System.IO.File.Delete(thefirmpath);
                    }
                }
                catch (Exception)
                {
                    hrefPath = "Error!";
                    throw;
                }
                return Content(hrefPath);
            }
        }


}
