using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.QMemo
{
    [RoutePrefix("QMemo")]
    public class MasterTableController : Controller
    {
        //
        // GET: /QMemo/MasterTable
        [Route("MasterTable")]
        public ActionResult MasterTable()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            viewModel.Add("HFQMemoFilter", (Request.Params["pqmemo"] == null ? "" : Request.Params["pqmemo"]));
            viewModel.Add("HFBusinessFilter", (Request.Params["pbusiness"] == null ? "" : Request.Params["pbusiness"]));
            viewModel.Add("HFBusinessIDFilter", (Request.Params["pidbusiness"] == null ? "" : Request.Params["pidbusiness"]));

            return View("~/Views/QMemo/MasterTable.cshtml", viewModel);
        }

        //
        // GET: /QMemo/MasterTableView
        [Route("MasterTableView")]
        public ActionResult MasterTableView()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            viewModel.Add("HFTypeReportFilter", (Request.Params["ptype"] == null ? "" : Request.Params["ptype"]));
            viewModel.Add("HFYearFilter", (Request.Params["pyear"] == null ? "" : Request.Params["pyear"]));
            viewModel.Add("HFQuarterFilter", (Request.Params["pquarter"] == null ? "" : Request.Params["pquarter"]));
            viewModel.Add("HFQMemoAccountFilter", (Request.Params["pqmemoAccount"] == null ? "" : Request.Params["pqmemoAccount"]));
            viewModel.Add("HFQMemoAccountIDFilter", (Request.Params["pqmemoAccountID"] == null ? "" : Request.Params["pqmemoAccountID"]));
            viewModel.Add("HFBusinessFilter", (Request.Params["pbusiness"] == null ? "" : Request.Params["pbusiness"]));
            viewModel.Add("HFBusinessIDFilter", (Request.Params["pbusinessID"] == null ? "" : Request.Params["pbusinessID"]));
            viewModel.Add("HFOnlyMapped", (Request.Params["ponlyMapped"] == null ? "" : Request.Params["ponlyMapped"]));
            viewModel.Add("HFBalance", (Request.Params["pbalance"] == null ? "" : Request.Params["pbalance"]));

            return View("~/Views/QMemo/MasterTableView.cshtml", viewModel);
        }
    }
}
