﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using Excel = Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Security;

namespace Automation.Web.Controllers.MEIL
{
    [RoutePrefix("MEIL/MakerChecker")]
    public class MakerCheckerController : Controller
    {
        //
        // GET: /MEIL/MakerChecker/AdminIssue
        [Route("AdminIssue")]
        public ActionResult AdminIssue()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            var pissueID = (String.IsNullOrEmpty(Request.QueryString.Get("pissueID"))) ? "" : Request.QueryString.Get("pissueID");

            //CreatePDFFromHTML("TestFile", String.Format("<html><body>Hello world: {0}</body></html>", DateTime.Now));

            viewModel.Add("HFAdminIssueID", pissueID);
            return View("~/Views/MEIL/MakerChecker/AdminIssue.cshtml", viewModel);
        }

        // GET: /MEIL/MakerChecker/AdminIssue
        [Route("BulkUpload")]
        public ActionResult BulkUpload()
        {

            return View("~/Views/MEIL/MakerChecker/BulkUpload.cshtml");
            
        }
        public static string CleanInvalidCharsExcelDropDown(string text)
        {
            // From xml spec valid chars: 
            // #x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD] | [#x10000-#x10FFFF]     
            // any Unicode character, excluding the surrogate blocks, FFFE, and FFFF. 
            //string re = @"[^\x09\x0A\x0D\x20-\uD7FF\uE000-\uFFFD\u10000-\u10FFFF]";
            //return Regex.Replace(text, re, "");
            //string textSpecial = "Escape characters ： . - < > & \" \'";
            //string xmlText = SecurityElement.Escape(text);
            //return xmlText;
            text = text.Replace(",", "");
            text = text.Replace("'", "");
            text = text.Replace("[", "");
            text = text.Replace("]", "");
            text = text.Replace("&", ""); 
            return text;
        }
        [ValidateInput(false)]
        [Route("GetExcelTemplate")]

        public void GetExcelTemplate()
        {
            
        //List Of Role
        string[] filterRole = new string[2];
            filterRole[0] = "Maker";
            filterRole[1] = "Checker";
            //var rowsResult = string[] filterRole;
            for (int i = 0; i < filterRole.Length; i++)
            {
                filterRole[i] = filterRole[i];
            }

            //List Of Reported BY Role
            string[] filterReportedByRole = new string[] { "Checker", "Maker", "Team Lead", "AAI", "Peer/Other Group"}; ;
            
            //var rowsResult = string[] filterRole;
            for (int i = 0; i < filterReportedByRole.Length; i++)
            {
                filterReportedByRole[i] = filterReportedByRole[i];
            }

            //List Process Area
            var spName = "[dbo].[STPR_PROCESS_LIST]";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>>());
            string[] filterProcessArea = new string[rowsResult.Count];
            for (int i = 0; i < rowsResult.Count; i++)
            {
                filterProcessArea[i] = rowsResult[i]["Category"] + '-' + rowsResult[i]["Name"];

            }
            //WebUtility.HtmlEncode(
            //List Countries
            var spNameForCountries = "[dbo].[spGetListOfCountriesBulkUpload]";
            var rowsResultForCountries = (new GlobalModel()).excecuteProcedureReturn(spNameForCountries, new List<Dictionary<string, string>>());
            //string[] filterCountry = new string[1];
            //filterCountry[0] = "Test, - . (Hola)";
            string[] filterCountry = new string[rowsResultForCountries.Count];

            for (int i = 0; i < rowsResultForCountries.Count; i++)
            {
                filterCountry[i] = CleanInvalidCharsExcelDropDown(rowsResultForCountries[i]["ID"]);
            }

            //List of reasons
            var spNameForReason = "[dbo].[spGetListOfReasonsBulkUpload]";
            var rowsResultForReasons = (new GlobalModel()).excecuteProcedureReturn(spNameForReason, new List<Dictionary<string, string>>());
            string[] filterReason = new string[rowsResultForReasons.Count];

            for (int i = 0; i < rowsResultForReasons.Count; i++)
            {
                filterReason[i] = rowsResultForReasons[i]["Description"];
            }

            //List of Reporting Period
            var spNameForReportingPeriod = "[dbo].[spGetListOfReportingPeriodBulkUpload]";
            var rowsResultForReportingPeriod = (new GlobalModel()).excecuteProcedureReturn(spNameForReportingPeriod, new List<Dictionary<string, string>>());
            string[] ReportingPeriod = new string[rowsResultForReportingPeriod.Count];

            for (int i = 0; i < rowsResultForReportingPeriod.Count; i++)
            {
                ReportingPeriod[i] = rowsResultForReportingPeriod[i]["MONTH"];
            }

            string[] ddl_itemRole = filterRole;
            string[] ddl_itemProcessArea = filterProcessArea;
            string[] ddl_itemCountry = filterCountry;
            string[] ddl_itemReason = filterReason;
            string[] ddl_itemReportingPeriod = ReportingPeriod;
            string[] ddl_itemReportedByRole = filterReportedByRole;

           


            DataTable dtResult = new DataTable();
            //a1
            dtResult.Columns.Add("Accountable Person");
            //b1
            dtResult.Columns.Add("Role");
            //c1
            dtResult.Columns.Add("Process Area");
            //d1
            dtResult.Columns.Add("Date of Occurrence");
            //e1
            dtResult.Columns.Add("Reporting Period");
            //f1
            dtResult.Columns.Add("Country");
            //g1
            dtResult.Columns.Add("Reason");
            //h1
            dtResult.Columns.Add("Short Description of Error");
            //i1
            dtResult.Columns.Add("Resolution Date");
            //j1
            dtResult.Columns.Add("Status");
            //k1
            dtResult.Columns.Add("Action Taken");
            //l1
            dtResult.Columns.Add("Reported By");
            //m1
            dtResult.Columns.Add("ReportedByRole");


            List<Dictionary<String, Object>> columnDefinitions = new List<Dictionary<string, Object>>();

            columnDefinitions.Add(new Dictionary<String, Object>
            {
                { "ColName", "Role" },
                { "ExcelColumn", "B2" },
                { "ExcelFormula", "=DropDownInfoIgnore!$A$2:$A$" + (ddl_itemRole.Length + 1) },
                { "DropDownList", ddl_itemRole }
            });

            columnDefinitions.Add(new Dictionary<String, Object>
            {
                { "ColName", "Process Area" },
                { "ExcelColumn", "C2" },
                { "ExcelFormula", "=DropDownInfoIgnore!$B$2:$B$" + (ddl_itemProcessArea.Length + 1) },
                { "DropDownList", ddl_itemProcessArea }
            });

            columnDefinitions.Add(new Dictionary<String, Object>
            {
                { "ColName", "Reporting Period" },
                { "ExcelColumn", "E2" },
                { "ExcelFormula", "=DropDownInfoIgnore!$C$2:$C$" + (ddl_itemReportingPeriod.Length + 1) },
                { "DropDownList", ddl_itemReportingPeriod }
            });

            columnDefinitions.Add(new Dictionary<String, Object>
            {
                { "ColName", "Country" },
                { "ExcelColumn", "F2" },
                { "ExcelFormula", "=DropDownInfoIgnore!$D$2:$D$" + (ddl_itemCountry.Length + 1) },
                { "DropDownList", ddl_itemCountry }
            });

            columnDefinitions.Add(new Dictionary<String, Object>
            {
                { "ColName", "Reason" },
                { "ExcelColumn", "G2" },
                { "ExcelFormula", "=DropDownInfoIgnore!$E$2:$E$" + (ddl_itemReason.Length + 1) },
                { "DropDownList", ddl_itemReason }
            });

            columnDefinitions.Add(new Dictionary<String, Object>
            {
                { "ColName", "ReportedByRole" },
                { "ExcelColumn", "M2" },
                { "ExcelFormula", "=DropDownInfoIgnore!$F$2:$F$" + (ddl_itemReportedByRole.Length + 1) },
                { "DropDownList", ddl_itemReportedByRole }
            });
            Automation.Utils.Excel.FasterExcelDropDowns(HttpContext.Response, dtResult, columnDefinitions, "BulkUploadTemplate");

            //object oMissing = System.Reflection.Missing.Value;
            //Microsoft.Office.Interop.Excel.Application app;
            //Microsoft.Office.Interop.Excel.Worksheet wksheet;
            //Microsoft.Office.Interop.Excel.Workbook wkbook;

            //app = new Microsoft.Office.Interop.Excel.Application();
            //app.Visible = false;
            //wkbook = app.Workbooks.Add(true);
            //wksheet = (Microsoft.Office.Interop.Excel.Worksheet)wkbook.ActiveSheet;

            //wksheet.Range["A1"].Value2 = "Accountable Person";
            //wksheet.Range["B1"].Value2 = "Role";
            //wksheet.Range["C1"].Value2 = "Process Area";
            //wksheet.Range["D1"].Value2 = "Date of Occurrence";
            //wksheet.Range["E1"].Value2 = "Reporting Period";
            //wksheet.Range["F1"].Value2 = "Country";
            //wksheet.Range["G1"].Value2 = "Reason";
            //wksheet.Range["H1"].Value2 = "Short Description of Error";
            //wksheet.Range["I1"].Value2 = "Resolution Date";
            //wksheet.Range["J1"].Value2 = "Status";
            //wksheet.Range["K1"].Value2 = "Action Taken";
            //wksheet.Range["L1"].Value2 = "Reported By";
            //wksheet.Range["M1"].Value2 = "ReportedByRole";

            ////Role-------------------------------------------------------------------------------------------------------------------------------

            //Microsoft.Office.Interop.Excel.Range xlsRangeRole;
            //xlsRangeRole = wksheet.get_Range("B2", "B2");
            //Microsoft.Office.Interop.Excel.DropDowns xlDropDownsRole;
            //Microsoft.Office.Interop.Excel.DropDown xlDropDownRole;
            //xlsRangeRole.ColumnWidth = 25;
            //xlDropDownsRole = ((Microsoft.Office.Interop.Excel.DropDowns)(wksheet.DropDowns(oMissing)));
            //xlDropDownRole = xlDropDownsRole.Add((double)xlsRangeRole.Left, (double)xlsRangeRole.Top, (double)xlsRangeRole.Width, (double)xlsRangeRole.Height, true);

            ////Add item into drop down list
            //for (int i = 0; i < ddl_itemRole.Count(); i++)
            //{
            //    xlDropDownRole.AddItem(ddl_itemRole[i], i + 1);
            //}
            //xlDropDownRole.Width = 105;
            ////ReportedByRole-------------------------------------------------------------------------------------------------------------------------------

            //Microsoft.Office.Interop.Excel.Range xlsRangeReportedByRole;
            //xlsRangeReportedByRole = wksheet.get_Range("M2", "M2");
            //Microsoft.Office.Interop.Excel.DropDowns xlDropDownsReportedByRole;
            //Microsoft.Office.Interop.Excel.DropDown xlDropDownReportedByRole;
            //xlsRangeReportedByRole.ColumnWidth = 25;
            //xlDropDownsReportedByRole = ((Microsoft.Office.Interop.Excel.DropDowns)(wksheet.DropDowns(oMissing)));
            //xlDropDownReportedByRole = xlDropDownsRole.Add((double)xlsRangeReportedByRole.Left, (double)xlsRangeReportedByRole.Top, (double)xlsRangeReportedByRole.Width, (double)xlsRangeReportedByRole.Height, true);

            ////Add item into drop down list
            //for (int i = 0; i < ddl_itemReportedByRole.Count(); i++)
            //{
            //    xlDropDownReportedByRole.AddItem(ddl_itemReportedByRole[i], i + 1);
            //}
            //xlDropDownReportedByRole.Width = 105;

            ////ProcessArea-------------------------------------------------------------------------------------------------------------------------------

            //Microsoft.Office.Interop.Excel.Range xlsRange;
            //xlsRange = wksheet.get_Range("C2", "C2");
            //Microsoft.Office.Interop.Excel.DropDowns xlDropDownsProcessArea;
            //Microsoft.Office.Interop.Excel.DropDown xlDropDownProcessArea;
            //xlsRange.ColumnWidth = 35;
            //xlDropDownsProcessArea = ((Microsoft.Office.Interop.Excel.DropDowns)(wksheet.DropDowns(oMissing)));
            //xlDropDownProcessArea = xlDropDownsProcessArea.Add((double)xlsRange.Left, (double)xlsRange.Top, (double)xlsRange.Width, (double)xlsRange.Height, true);

            ////Add item into drop down list
            //for (int i = 0; i < ddl_itemProcessArea.Count(); i++)
            //{
            //    xlDropDownProcessArea.AddItem(ddl_itemProcessArea[i], i + 1);
            //}
            //xlDropDownProcessArea.Width = 165;

            ////Country-------------------------------------------------------------------------------------------------------------------------------
            //Microsoft.Office.Interop.Excel.Range xlsRangeCountry;
            //xlsRangeCountry = wksheet.get_Range("F2", "F2");

            //Microsoft.Office.Interop.Excel.DropDowns xlDropDownsCountry;
            //Microsoft.Office.Interop.Excel.DropDown xlDropDownCountry;

            //xlDropDownsCountry = ((Microsoft.Office.Interop.Excel.DropDowns)(wksheet.DropDowns(oMissing)));
            //xlDropDownCountry = xlDropDownsCountry.Add((double)xlsRangeCountry.Left, (double)xlsRangeCountry.Top, (double)xlsRangeCountry.Width, (double)xlsRangeCountry.Height, true);
            //xlsRangeCountry.ColumnWidth = 25;
            ////Add item into drop down list
            //for (int i = 0; i < ddl_itemCountry.Count(); i++)
            //{
            //    xlDropDownCountry.AddItem(ddl_itemCountry[i], i + 1);
            //}
            //xlDropDownCountry.Width = 80;

            ////Reason-------------------------------------------------------------------------------------------------------------------------------
            //Microsoft.Office.Interop.Excel.Range xlsRangeReason;
            //xlsRangeReason = wksheet.get_Range("G2", "G2");

            //Microsoft.Office.Interop.Excel.DropDowns xlDropDownsReason;
            //Microsoft.Office.Interop.Excel.DropDown xlDropDownReason;

            //xlDropDownsReason = ((Microsoft.Office.Interop.Excel.DropDowns)(wksheet.DropDowns(oMissing)));
            //xlDropDownReason = xlDropDownsReason.Add((double)xlsRangeReason.Left, (double)xlsRangeReason.Top, (double)xlsRangeReason.Width, (double)xlsRangeReason.Height, true);
            //xlsRangeReason.ColumnWidth = 30;
            ////Add item into drop down list
            //for (int i = 0; i < ddl_itemReason.Count(); i++)
            //{
            //    xlDropDownReason.AddItem(ddl_itemReason[i], i + 1);
            //}
            //xlDropDownReason.Width = 135;

            ////Reporting Period-------------------------------------------------------------------------------------------------------------------------------
            //Microsoft.Office.Interop.Excel.Range xlsRangeReportingPeriod;
            //xlsRangeReportingPeriod = wksheet.get_Range("E2", "E2");

            //Microsoft.Office.Interop.Excel.DropDowns xlDropDownsReportingPeriod;
            //Microsoft.Office.Interop.Excel.DropDown xlDropDownReportingPeriod;

            //xlDropDownsReportingPeriod = ((Microsoft.Office.Interop.Excel.DropDowns)(wksheet.DropDowns(oMissing)));
            //xlDropDownReportingPeriod = xlDropDownsReportingPeriod.Add((double)xlsRangeReportingPeriod.Left, (double)xlsRangeReportingPeriod.Top, (double)xlsRangeReportingPeriod.Width, (double)xlsRangeReportingPeriod.Height, true);
            //xlsRangeReportingPeriod.ColumnWidth = 18;
            ////Add item into drop down list
            //for (int i = 0; i < ddl_itemReportingPeriod.Count(); i++)
            //{
            //    xlDropDownReportingPeriod.AddItem(ddl_itemReportingPeriod[i], i + 1);

            //}
            //xlDropDownReportingPeriod.Width = 80;
            ////return ""//app.Visible = true;

            //app.Visible = true;
        }
        //
        // GET: /MEIL/BulkUploadTemplate/
        [Route("BulkUploadExcel")]
        public ActionResult BulkUploadExcel(string pjsonData, string pidProcess)
        {
            DataTable ptable = (DataTable)JsonConvert.DeserializeObject(pjsonData, (typeof(DataTable)));

            //Save data in the database
            (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[spMakerCheckerAdminIssueBulkUpload]", new List<Dictionary<string, string>> {

                new Dictionary<string, string>
                {
                    { "Name", "@SOEIDUploader" },
                    { "Value", GlobalUtil.GetSOEID() }
                }
            });

            return Content("Uploaded");
        }
        public string CreatePDFFromHTML(string pname, string phtml)
        {
            //string uploadFolder = GlobalUtil.GetValueAppConfig("appSettings", "UploadFolder") + "TempPDF" + @"/";
            //string uploadFolderMap = Server.MapPath(uploadFolder);

            ////If not exist TempPDF Create it
            //var directory = new FileInfo(uploadFolderMap).Directory;
            //if (directory == null || (directory != null && directory.Exists == false))
            //{
            //    //Create TempPDF Folder
            //    Directory.CreateDirectory(uploadFolderMap);

            //}

            ////Delete files from UploadFolder/TempPDF
            //var fileGenerationDir = new DirectoryInfo(uploadFolderMap);
            //fileGenerationDir.GetFiles("*", SearchOption.AllDirectories).ToList().ForEach(objFile => { objFile.Delete(); });

            //// Directory to write
            ////sPathToWritePdfTo: C:\\Users\\CD25867\\Documents\\TeamFoundationProjects\\ARE.HRTT\\Solution\\HRTT.Web\\Temp\\DevelopmentPlanReports\\DevelopmentPlan_CD25867_2017050813353011.pdf
            //var sPathToWritePdfTo = uploadFolderMap + pname + "_" + GlobalUtil.CreateCustomID() + ".pdf";

            ////Create new file
            //var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            //var pdfBytes = htmlToPdf.GeneratePdf(phtml);
            //System.IO.File.WriteAllBytes(sPathToWritePdfTo, pdfBytes);

            //return sPathToWritePdfTo;
            return "";
        }

        //
        // GET: /MEIL/MakerChecker/AdminReason

        [Route("AdminReason")]
        public ActionResult AdminReason()
        {
            return View("~/Views/MEIL/MakerChecker/AdminReason.cshtml");
        }

        //
        // GET: /MEIL/MakerChecker/ListIssues
        [Route("ListIssues")]
        public ActionResult ListIssues()
        {
            return View("~/Views/MEIL/MakerChecker/ListIssues.cshtml");
        }

        //
        // GET: /MEIL/MakerChecker/PartialViewAdminIssue

        [Route("PartialViewAdminIssue")]
        public ActionResult PartialViewDBTable()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            var pissueID = (String.IsNullOrEmpty(Request.QueryString.Get("pissueID"))) ? "" : Request.QueryString.Get("pissueID");
            var paction = (String.IsNullOrEmpty(Request.QueryString.Get("paction"))) ? "" : Request.QueryString.Get("paction");

            viewModel.Add("HFIssueID", pissueID);
            viewModel.Add("HFAction", paction);
            return View("~/Views/MEIL/MakerChecker/_TemplateIssue.cshtml", viewModel);
        }


    }
}
