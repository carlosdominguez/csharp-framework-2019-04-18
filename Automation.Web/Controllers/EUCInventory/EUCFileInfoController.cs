using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.EUCInventory
{
    [RoutePrefix("EUCInventory/EUCFileInfo")]
    public class EUCFileInfoController : Controller
    {
        //
        // GET: /
        [Route("/")]
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            //Get Encode Employee Assessment SOEID
            string EUCID = (Request.QueryString["EUCID"] == null) ? "0" : Request.QueryString["EUCID"];
            viewModel.Add("EUCID", EUCID);
            string TABINDEX = (Request.QueryString["TABINDEX"] == null) ? "-1" : Request.QueryString["TABINDEX"];
            viewModel.Add("TABINDEX", TABINDEX);
            return View("~/Views/EUCInventory/EUCFileInfo.cshtml", viewModel);
        }

        [Route("EUCInventory/saveProduct")]
        public ActionResult saveProductRelated(string OrgID, string EUCID, string ProductRelated)
        {
            (new GlobalModel()).excecuteProcedureNoReturn("SP_Name_?", new List<Dictionary<string, string>> { 
                    new Dictionary<string, string>
                    {
                        { "Name", "@pOrgID" },
                        { "Value", OrgID }
                    },
                       new Dictionary<string, string>
                    {
                        { "Name", "@pOrgID" },
                        { "Value", OrgID }
                    },
                       new Dictionary<string, string>
                    {
                        { "Name", "@pOrgID" },
                        { "Value", OrgID }
                    }
                });
            return Content("saved!");
        }

        [Route("tblEUC_Save")]
        public ActionResult tblEUC_Save(string p_environmentStatus, string p_creationReason, string p_otherCreationReason, 
            string p_statusDetails, string p_region, string p_country, string p_center, string p_manager, string p_delegate, 
            string p_goc, string p_otherStakeholder, string p_techContactSOEID, string p_Name, string p_mainPurpose, 
            string p_description, string p_businessTaxonomy, string p_sourceOfUse, string p_fileType, string p_numbersOfUsers, 
            string p_frecuencyOfUse, string p_goldCopyMoved, string p_productionPath, string p_userManual, string p_OrgID
            , string p_globalEUCID, 
            //Documentation Info
            string p_ReviewDate, string p_ReviewedBy, string p_DocumentationPathBlueworks,
            //Elimination Strategy
            string p_dataQualityIssue ,string p_IMRID ,string p_missingFunctionality ,string p_workOrderID , 
            string p_whyThisEUCExist ,string p_explainWhyThisEUCExist ,string p_isTherePlanToEliminateThisEUC ,
            string p_thisEUCCanNotBeEliminated ,string p_reasonWhyEUCNotBeEliminated ,string p_planCreatedDate ,
            string p_retirementScheduledDate ,string p_planWillBeCreatedDate
            )
        {
            var result = (new GlobalModel()).excecuteProcedureReturn("spMaintenance_InsertEUCGeneralInfo", 
                new List<Dictionary<string, string>>{
                new Dictionary<string,string>{{"Name","@p_environmentStatus"},{"Value",p_environmentStatus}},
                new Dictionary<string,string>{{"Name","@p_creationReason"},{"Value",p_creationReason}},
                new Dictionary<string,string>{{"Name","@p_otherCreationReason"},{"Value",p_otherCreationReason}},
                new Dictionary<string,string>{{"Name","@p_statusDetails"},{"Value",p_statusDetails}},
                new Dictionary<string,string>{{"Name","@p_region"},{"Value",p_region}},
                new Dictionary<string,string>{{"Name","@p_country"},{"Value",p_country}},
                new Dictionary<string,string>{{"Name","@p_center"},{"Value",p_center}},
                new Dictionary<string,string>{{"Name","@p_manager"},{"Value",p_manager}},
                new Dictionary<string,string>{{"Name","@p_delegate"},{"Value",p_delegate}},
                new Dictionary<string,string>{{"Name","@p_goc"},{"Value",p_goc}},
                new Dictionary<string,string>{{"Name","@p_otherStakeholder"},{"Value",p_otherStakeholder}},
                new Dictionary<string,string>{{"Name","@p_techContactSOEID"},{"Value",p_techContactSOEID}},
                new Dictionary<string,string>{{"Name","@p_Name"},{"Value",p_Name}},
                new Dictionary<string,string>{{"Name","@p_mainPurpose"},{"Value",p_mainPurpose}},
                new Dictionary<string,string>{{"Name","@p_description"},{"Value",p_description}},
                new Dictionary<string,string>{{"Name","@p_businessTaxonomy"},{"Value",p_businessTaxonomy}},
                new Dictionary<string,string>{{"Name","@p_sourceOfUse"},{"Value",p_sourceOfUse}},
                new Dictionary<string,string>{{"Name","@p_fileType"},{"Value",p_fileType}},
                new Dictionary<string,string>{{"Name","@p_numbersOfUsers"},{"Value",p_numbersOfUsers}},
                new Dictionary<string,string>{{"Name","@p_frecuencyOfUse"},{"Value",p_frecuencyOfUse}},
                new Dictionary<string,string>{{"Name","@p_goldCopyMoved"},{"Value",p_goldCopyMoved}},
                new Dictionary<string,string>{{"Name","@p_productionPath"},{"Value",p_productionPath}},
                new Dictionary<string,string>{{"Name","@p_userManual"},{"Value",p_userManual}},
                new Dictionary<string,string>{{"Name","@p_OrgID"},{"Value",p_OrgID}},
                new Dictionary<string,string>{{"Name","@p_globalEUCID"},{"Value",p_globalEUCID}}
            });
            if (result.Count>0)
            {
                (new GlobalModel()).excecuteProcedureNoReturn("spMaintenance_InsertEUCDocumentationInfo", 
                    new List<Dictionary<string, string>>{
                new Dictionary<string,string>{{"Name","@p_EUCID"},{"Value",result[0]["ID"]}},
                new Dictionary<string,string>{{"Name","@p_OrgID"},{"Value",p_OrgID}},
                new Dictionary<string,string>{{"Name","@p_ReviewDate"},{"Value",p_ReviewDate}},
                new Dictionary<string,string>{{"Name","@p_ReviewedBy"},{"Value",p_ReviewedBy}},
                new Dictionary<string,string>{{"Name","@p_DocumentationPathBlueworks"},{"Value",p_DocumentationPathBlueworks}}});

                (new GlobalModel()).excecuteProcedureNoReturn("spMaintenance_InsertEUCEliminationStrategyInfo", 
                    new List<Dictionary<string, string>>{
                    new Dictionary<string,string>{{"Name","@p_EUCID"},{"Value",result[0]["ID"]}},
                    new Dictionary<string,string>{{"Name","@p_OrgId"},{"Value",p_OrgID}},
                    new Dictionary<string,string>{{"Name","@p_dataQualityIssue"},{"Value",p_dataQualityIssue}},
                    new Dictionary<string,string>{{"Name","@p_IMRID"},{"Value",p_IMRID}},
                    new Dictionary<string,string>{{"Name","@p_missingFunctionality"},{"Value",p_missingFunctionality}},
                    new Dictionary<string,string>{{"Name","@p_workOrderID"},{"Value",p_workOrderID}},
                    new Dictionary<string,string>{{"Name","@p_whyThisEUCExist"},{"Value",p_whyThisEUCExist}},
                    new Dictionary<string,string>{{"Name","@p_explainWhyThisEUCExist"},{"Value",p_explainWhyThisEUCExist}},
                    new Dictionary<string,string>{{"Name","@p_isTherePlanToEliminateThisEUC"},{"Value",p_isTherePlanToEliminateThisEUC}},
                    new Dictionary<string,string>{{"Name","@p_thisEUCCanNotBeEliminated"},{"Value",p_thisEUCCanNotBeEliminated}},
                    new Dictionary<string,string>{{"Name","@p_reasonWhyEUCNotBeEliminated"},{"Value",p_reasonWhyEUCNotBeEliminated}},
                    new Dictionary<string,string>{{"Name","@p_planCreatedDate"},{"Value",p_planCreatedDate}},
                    new Dictionary<string,string>{{"Name","@p_retirementScheduledDate"},{"Value",p_retirementScheduledDate}},
                    new Dictionary<string,string>{{"Name","@p_planWillBeCreatedDate"},{"Value",p_planWillBeCreatedDate}}
                });

                return Content(result[0]["ID"]);
            }
            else
            {
                return Content("0");
            }            
        }

        [Route("tblEUC_Criticality")]
        public ActionResult tblEUC_CriticalityAnswers( string p_OrgID,string p_isMCAProcessRelated,string p_MCAProcess,
            string p_isControlForAProcess,string p_COBRequired,string p_isProcessEndNeeded,string p_usedFor,
            string p_materialityLevel,string p_SOXRelated,string p_dataClasification,string p_criticality,
            //Answers Info
            string p_EUCID,string p_Answers)
        {
            var resultAnswers = (new GlobalModel()).excecuteProcedureNoReturn("spCriticality_InsertCriticalityAnswers",
                new List<Dictionary<string, string>>{
                new Dictionary<string,string>{{"Name","@pEUCID"},{"Value",p_EUCID}},
                new Dictionary<string,string>{{"Name","@pAnswers"},{"Value",p_Answers}}
            });
            
           
                var resultCriticality = (new GlobalModel()).excecuteProcedureReturn("spCriticality_InsertEUCCriticalityInfo",
                new List<Dictionary<string, string>>{
                new Dictionary<string,string>{{"Name","@p_EUCID"},{"Value",p_EUCID}},
                new Dictionary<string,string>{{"Name","@p_OrgID"},{"Value",p_OrgID}},
                new Dictionary<string,string>{{"Name","@p_isMCAProcessRelated"},{"Value",p_isMCAProcessRelated}},
                new Dictionary<string,string>{{"Name","@p_MCAProcess"},{"Value",p_MCAProcess}},
                new Dictionary<string,string>{{"Name","@p_isControlForAProcess"},{"Value",p_isControlForAProcess}},
                new Dictionary<string,string>{{"Name","@p_COBRequired"},{"Value",p_COBRequired}},
                new Dictionary<string,string>{{"Name","@p_isProcessEndNeeded"},{"Value",p_isProcessEndNeeded}},
                new Dictionary<string,string>{{"Name","@p_usedFor"},{"Value",p_usedFor}},
                new Dictionary<string,string>{{"Name","@p_materialityLevel"},{"Value",p_materialityLevel}},
                new Dictionary<string,string>{{"Name","@p_SOXRelated"},{"Value",p_SOXRelated}},
                new Dictionary<string,string>{{"Name","@p_dataClasification"},{"Value",p_dataClasification}},
                new Dictionary<string,string>{{"Name","@p_criticality"},{"Value",p_criticality}}
            });


                return Content(resultCriticality[0]["criticality"].ToString());
            
            //if (result.Count > 0)
            //{
            //    return Content("1");
            //}
            //else
            //{
            //    return Content("0");
            //}
        }

        //[Route("tblEUC_Documentation")]
        //public ActionResult tblEUC_Documentation(string p_EUCID,string p_OrgID,string p_ReviewDate,string p_ReviewedBy,
        //    string p_DocumentationPathBlueworks) { 
        //    (new GlobalModel()).excecuteProcedureNoReturn("spMaintenance_InsertEUCDocumentationInfo",
        //        new List<Dictionary<string,string>>{
        //        new Dictionary<string,string>{{"Name","@p_EUCID"},{"Value",p_EUCID}},
        //        new Dictionary<string,string>{{"Name","@p_OrgID"},{"Value",p_OrgID}},
        //        new Dictionary<string,string>{{"Name","@p_ReviewDate"},{"Value",p_ReviewDate}},
        //        new Dictionary<string,string>{{"Name","@p_ReviewedBy"},{"Value",p_ReviewedBy}},
        //        new Dictionary<string,string>{{"Name","@p_DocumentationPathBlueworks"},{"Value",p_DocumentationPathBlueworks}}
        //    });
        //    return Content("EUC documentation saved!");
        //}
    }
}
