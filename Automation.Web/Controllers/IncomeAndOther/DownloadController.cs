﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.IncomeAndOther
{
    [RoutePrefix("IncomeAndOther")]
    public class DownloadController : Controller
    {
        //
        // GET: /

        [Route("Download")]
        public ActionResult Download()
        {
            return View("~/Views/IncomeAndOther/Download.cshtml");
        }


        [Route("DownloadPDF")]
        public ActionResult DownloadPDF(string thefirmpath, string fileName)
        {
            string fullPathToCopy = "";
            string hrefPath = "";
            try
            {
                //SET UP PATHS AND FILES NAMES
                string uploadFolder = GlobalUtil.GetValueAppConfig("appSettings", "UploadFolder") + "TheFirmDocs" + @"\";
                hrefPath = uploadFolder + fileName;
                fullPathToCopy = @"" + Server.MapPath(uploadFolder)  + fileName;

                //DELETE OLD FILES STORED ON THE DOWNLOADFOLDER
                System.IO.DirectoryInfo di = new DirectoryInfo(@"" + Server.MapPath(uploadFolder));
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }

                //COPY NEW FILE FRO SHAREDRIVE TO DOWNLOAD FOLDER
                System.IO.File.Copy(thefirmpath+ @"\" + fileName, fullPathToCopy, true);
            }
            catch (Exception)
            {
                hrefPath = "Error!";
                throw;
            }
            return Content(hrefPath);
        }
    }
}
