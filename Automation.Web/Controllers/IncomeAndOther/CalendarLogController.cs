﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.IncomeAndOther
{
    [RoutePrefix("IncomeAndOther")]
    public class CalendarLogController : Controller
    {
        #region Return View
        [Route("CalendarLog")]
        public ActionResult CalendarLog()
        {
            return View("~/Views/IncomeAndOther/CalendarLog.cshtml");
        }
        #endregion

        //Modal Template
        [Route("ModalTemplate")]
        public ActionResult ModalTemplate()
        {
            Dictionary<string, object> viewBagResult = new Dictionary<string, object>();

            #region View Bag Variables
            string logID = Request.QueryString["logID"];
            viewBagResult.Add("logID", logID);

            string entityID = Request.QueryString["entityID"];
            viewBagResult.Add("entityID", entityID);

            string entityName = Request.QueryString["entityName"];
            viewBagResult.Add("entityName", entityName);

            string function1 = Request.QueryString["function1"];
            viewBagResult.Add("function1", function1);

            string frequency = Request.QueryString["frequency"];
            viewBagResult.Add("frequency", frequency);

            string reviewer = Request.QueryString["reviewer"];
            string completeReviewer = reviewer + " (" + Request.QueryString["statusSOEID"] + ")";

            viewBagResult.Add("reviewer", completeReviewer);

            string preparer = Request.QueryString["preparer"];
            string completePreparer = preparer + " (" + Request.QueryString["statusPreparedSOEID"] + ")";
            viewBagResult.Add("preparer", completePreparer);

            string filingDueDate = Request.QueryString["filingDueDate"];
            viewBagResult.Add("filingDueDate", filingDueDate);

            string filingID = Request.QueryString["filingID"];
            viewBagResult.Add("filingID", filingID);

            string status = Request.QueryString["status"];
            viewBagResult.Add("status", status);

            string statusPrepared = Request.QueryString["statusPrepared"];
            viewBagResult.Add("statusPrepared", statusPrepared);

            string statusDesc = Request.QueryString["statusDesc"];
            viewBagResult.Add("statusDesc", statusDesc);

            string preparedDdate = Request.QueryString["preparedDdate"];
            viewBagResult.Add("preparedDdate", (String.IsNullOrEmpty(preparedDdate) ? "No Date Found" : preparedDdate));

            string reviewedDate = Request.QueryString["reviewedDate"];
            viewBagResult.Add("reviewedDate", (String.IsNullOrEmpty(reviewedDate) ? "No Date Found" : reviewedDate));

            string commentFlag = Request.QueryString["commentFlag"];
            viewBagResult.Add("commentFlag", commentFlag);

            string counts = Request.QueryString["counts"];
            viewBagResult.Add("counts", counts);

            string comments = Request.QueryString["comments"];
            viewBagResult.Add("comments", comments);

            string statusDate = Request.QueryString["statusDate"];
            viewBagResult.Add("statusDate", statusDate);

            string statusSOEID = Request.QueryString["statusSOEID"];
            viewBagResult.Add("statusSOEID", statusSOEID);

            string statusPreparedDate = Request.QueryString["statusPreparedDate"];
            viewBagResult.Add("statusPreparedDate", statusPreparedDate);

            string statusPreparedSOEID = Request.QueryString["statusPreparedSOEID"];
            viewBagResult.Add("statusPreparedSOEID", statusPreparedSOEID);

            string ttiCode = Request.QueryString["ttiCode"];
            viewBagResult.Add("ttiCode", ttiCode);

            string EIN = Request.QueryString["EIN"];
            viewBagResult.Add("EIN", EIN);

            string state = Request.QueryString["state"];
            viewBagResult.Add("state", state);

            string city = Request.QueryString["city"];
            viewBagResult.Add("city", city);

            string branch = Request.QueryString["branch"];
            viewBagResult.Add("branch", branch);

            string dueDay = Request.QueryString["dueDay"];
            viewBagResult.Add("dueDay", dueDay);

            string dueMonth = Request.QueryString["dueMonth"];
            viewBagResult.Add("dueMonth", dueMonth);

            #endregion

            return View("~/Views/IncomeAndOther/ModalTemplate.cshtml", viewBagResult);
        }

        #region Action Buttons
        // Preparer - Add Comments and BUs
        [ValidateInput(false)]
        [Route("PreparerEdit")]
        public ActionResult PreparerEdit(string pjson, string comments, string accounts)
        {
          
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_FC_PreparerEdit";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@accounts" },
                    { "Value", accounts}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", comments }
                }
            });
            return Content("Saved!");
        }

        // Preparer - Prepare the Filing
        [ValidateInput(false)]
        [Route("PreparerSave")]
        public ActionResult PreparerSave(string pjson, string comments)
        {
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_FC_PreparerSave";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", comments }
                }
            });
            return Content("Prepared!");
        }

        // Preparer - Delete Filing
        [ValidateInput(false)]
        [Route("PreparerDelete")]
        public ActionResult PreparerDelete(string pjson, string comments)
        {
            //REVIVAR - NO SE PUEDE ELIMINAR
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_FC_PreparerDelete";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", comments }
                }
            });
            return Content("Deleted!");
        }

        // Reviewer - Add Comments and Accounts
        [ValidateInput(false)]
        [Route("ReviewerEdit")]
        public ActionResult ReviewerEdit(string pjson, string comments, string accounts, string duedate)
        {
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_FC_ReviewerEdit";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@accounts" },
                    { "Value", accounts }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", comments }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@duedate" },
                    { "Value", duedate }
                }
            });
            return Content("Saved!");

        }

        // Reviewer - Review Filing
        [ValidateInput(false)]
        [Route("ReviewerSave")]
        public ActionResult ReviewerSave(string pjson, string comments, string accounts, string duedate)
        {
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_FC_ReviewerSave";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@accounts" },
                    { "Value", accounts }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", comments }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@duedate" },
                    { "Value", duedate }
                }
            });
            return Content("Saved!");
        }

        // Reviewer - Delete Filing
        [ValidateInput(false)]
        [Route("ReviewerDelete")]
        public ActionResult ReviewerDelete(string pjson, string comments)
        {
            //REVIVAR - NO SE PUEDE ELIMINAR
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_FC_PreparerDelete";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", comments }
                }
            });
            return Content("Deleted!");
        }

        // All - Add Comments
        [ValidateInput(false)]
        [Route("CompletedEdit")]
        public ActionResult CompletedEdit(string pjson, string comments)
        {

            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_FC_CompletedEdit";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", comments }
                }
            });
            return Content("Saved!");
        }

        #endregion 
    }

}
