﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace Automation.Web.Controllers.IncomeAndOther
{
    [RoutePrefix("IncomeAndOther")]
    public class AdminController : Controller
    {
        [Route("Admin")]
        public ActionResult Admin()
        {
            return View("~/Views/IncomeAndOther/Admin.cshtml");
        }

        //ModalEntities
        [Route("ModalEntities")]
        public ActionResult ModalEntities()
        {
            Dictionary<string, object> viewBagResult = new Dictionary<string, object>();

            #region View Bag Variables

            string logID = Request.QueryString["logID"];
            string entityID = Request.QueryString["entityID"];
            string entityName = Request.QueryString["entityName"];
            string function = Request.QueryString["function"];
            string frequency = Request.QueryString["frequency"];
            string dueDay = Request.QueryString["dueDay"];
            string dueMonth = Request.QueryString["dueMonth"];
            string active = Request.QueryString["active"];
            string counts = Request.QueryString["counts"];
            string preparer = Request.QueryString["preparer"];
            string reviewer = Request.QueryString["reviewer"];
            string ttiCode = Request.QueryString["ttiCode"];
            string EIN = Request.QueryString["EIN"];
            string state = Request.QueryString["state"];
            string city = Request.QueryString["city"];
            string branch = Request.QueryString["branch"];

            viewBagResult.Add("logID", logID);
            viewBagResult.Add("entityID", entityID);
            viewBagResult.Add("entityName", entityName);
            viewBagResult.Add("function", function);
            viewBagResult.Add("frequency", frequency);
            viewBagResult.Add("dueDay", dueDay);
            viewBagResult.Add("dueMonth", dueMonth);
            viewBagResult.Add("active", active);
            viewBagResult.Add("counts", counts);
            viewBagResult.Add("preparer", preparer);
            viewBagResult.Add("reviewer", reviewer);
            viewBagResult.Add("ttiCode", ttiCode);
            viewBagResult.Add("EIN", EIN);
            viewBagResult.Add("state", state);
            viewBagResult.Add("city", city);
            viewBagResult.Add("branch", branch);

            #endregion

            return View("~/Views/IncomeAndOther/ModalEntities.cshtml", viewBagResult);
        }


        //ModalFunction
        [Route("ModalFunction")]
        public ActionResult ModalFunction()
        {
            Dictionary<string, object> viewBagResult = new Dictionary<string, object>();

            #region View Bag Variables

            string funcID = Request.QueryString["funcID"];
            string function = Request.QueryString["function1"];
            string active = Request.QueryString["active"];

            viewBagResult.Add("funcID", funcID);
            viewBagResult.Add("function", function);
            viewBagResult.Add("active", active);
            #endregion

            return View("~/Views/IncomeAndOther/ModalFunction.cshtml", viewBagResult);
        }




        #region Action Buttons
        // Add - Edit Entity
        [ValidateInput(false)]
        [Route("EntityEdit")]
        public ActionResult EntityEdit(string pjson, string entityid)
        {

            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_AD_EntityEdit";

           
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                 new Dictionary<string, string>
                {
                    { "Name", "@entityID" },
                    { "Value", entityid}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@logID" },
                    { "Value", data["logID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@entityName" },
                    { "Value", data["entityName"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@function " },
                    { "Value", data["function"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@frequency" },
                    { "Value", data["frequency"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@ttiCode" },
                    { "Value", data["ttiCode"]}
                },
                //new Dictionary<string, string>
                //{
                //    { "Name", "@EIN" },
                //    { "Value", data["EIN"]}
                //},
                //new Dictionary<string, string>
                //{
                //    { "Name", "@state" },
                //    { "Value", data["state"]}
                //},
                //new Dictionary<string, string>
                //{
                //    { "Name", "@city" },
                //    { "Value", data["city"]}
                //},
                //new Dictionary<string, string>
                //{
                //    { "Name", "@branch" },
                //    { "Value", data["branch"]}
                //}
                
                new Dictionary<string, string>
                {
                    { "Name", "@dueDay" },
                    { "Value", data["dueDay"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@dueMonth" },
                    { "Value", data["dueMonth"]}
                },
                //new Dictionary<string, string>
                //{
                //    { "Name", "@reviewer" },
                //    { "Value", data["reviewer"]}
                //},
                //new Dictionary<string, string>
                //{
                //    { "Name", "@preparer" },
                //    { "Value", data["preparer"]}
                //},
                new Dictionary<string, string>
                {
                    { "Name", "@active" },
                    { "Value", data["active"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@counts" },
                    { "Value", data["counts"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                }
                #endregion
            });

            



            return Content("Saved!");
        }

        // Add - Edit Function
        [ValidateInput(false)]
        [Route("FunctionEdit")]
        public ActionResult FunctionEdit(string functionId,string functionName, string status)
        {
            var spName = "NSP_AD_FunctionEdit";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@funcID" },
                    { "Value", functionId}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@functionName" },
                    { "Value", functionName}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@status" },
                    { "Value", status }
                }
            });
            return Content("Prepared!");
        }

        // Add - Edit Filing Calendar
        [ValidateInput(false)]
        [Route("SaveFilingCalendar")]
        public ActionResult SaveFilingCalendar(string identity, string filingDueDate, string calendarType, string dateDueCorpTax)
        {
            //REVIVAR - NO SE PUEDE ELIMINAR
            var spName = "NSP_AD_FilingCalendarEdit";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", "NewFilingCalendar"}
                },new Dictionary<string, string>
                {
                    { "Name", "@entityID" },
                    { "Value", identity}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@duedate" },
                    { "Value", filingDueDate }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@calendartype" },
                    { "Value", calendarType }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@dateDueCorpTax" },
                    { "Value", dateDueCorpTax }
                }
            });
            return Content("Saved!");
        }

        // Reviewer - Add Comments and Accounts
        [ValidateInput(false)]
        [Route("ReviewerEdit")]
        public ActionResult ReviewerEdit(string pjson, string comments, string accounts, string duedate)
        {
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_FC_ReviewerEdit";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@accounts" },
                    { "Value", accounts }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", comments }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@duedate" },
                    { "Value", duedate }
                }
            });
            return Content("Saved!");

        }

        // Reviewer - Review Filing
        [ValidateInput(false)]
        [Route("ReviewerSave")]
        public ActionResult ReviewerSave(string pjson, string comments, string accounts, string duedate)
        {
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_FC_ReviewerSave";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@accounts" },
                    { "Value", accounts }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", comments }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@duedate" },
                    { "Value", duedate }
                }
            });
            return Content("Saved!");
        }

        // Reviewer - Delete Filing
        [ValidateInput(false)]
        [Route("ReviewerDelete")]
        public ActionResult ReviewerDelete(string pjson, string comments)
        {
            //REVIVAR - NO SE PUEDE ELIMINAR
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_FC_PreparerDelete";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", comments }
                }
            });
            return Content("Deleted!");
        }

        // All - Add Comments
        [ValidateInput(false)]
        [Route("CompletedEdit")]
        public ActionResult CompletedEdit(string pjson, string comments)
        {

            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_FC_CompletedEdit";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", comments }
                }
            });
            return Content("Saved!");
        }



        // GET: /Upload
        [Route("UploadYE")]
        public ActionResult UploadYE()
        {
            Dictionary<string, object> result = new Dictionary<string, object> { { "success", true } };

            try
            {
                var objFile = GlobalUtil.GetFileFromCurrentRequest();
                string fullPathSaved;
                DataTable ptable;

                        //Save excel file in the server
                        fullPathSaved = GlobalUtil.FileSaveAs("YEBusinessCalendar", objFile.Filename, objFile.InputStream);
                        ptable = GlobalUtil.GetDataTableFromExcel(fullPathSaved);

                        //Save data in the database
                        (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[NSP_YE_InsertNewCalendar]", new List<Dictionary<string, string>> {
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEIDUploader" },
                                { "Value", GlobalUtil.GetSOEID() }
                            }
                        });

                string uploadFolder = GlobalUtil.GetValueAppConfig("appSettings", "UploadFolder") + "YEBusinessCalendar" + @"\";
                string fullPathDelete = @"" + Server.MapPath(uploadFolder);

                System.IO.DirectoryInfo di = new DirectoryInfo(@"" + fullPathDelete);
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }

            }
            catch (Exception ex)
            {
                //return new FileUploaderResult(false, error: ex.Message);
                result["success"] = false;
                result.Add("error", ex.ToString());
            }

            // the anonymous object in the result below will be convert to json and set back to the browser
            //return new FileUploaderResult(true, new { extraInformation = 12345 });

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        //GENERATE NEW FILING CALENDAR
        [ValidateInput(false)]
        [Route("GenerateNewFilingCalendar")]
        public ActionResult GenerateNewFilingCalendar(string value)
        {
            //REVIVAR - NO SE PUEDE ELIMINAR
            var spName = "NSP_AD_GenerateNewFilingCalendar";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                }
            });
            return Content("Saved!");
        }


        //ACTIVE FILING CALENDAR YEAR
        [ValidateInput(false)]
        [Route("ActivateYear")]
        public ActionResult ActivateYear(string activeYear)
        {
            //REVIVAR - NO SE PUEDE ELIMINAR
            var spName = "NSP_AD_ActivateYear";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@activeYear" },
                    { "Value",  activeYear}
                }
            });
            return Content("Saved!");
        }
        

    }
}
