﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.IncomeAndOther
{
    [RoutePrefix("IncomeAndOther")]
    public class MassUpdateController : Controller
    {
        //
        // GET: /

        [Route("MassUpdate")]
        public ActionResult MassUpdate()
        {
            return View("~/Views/IncomeAndOther/MassUpdate.cshtml");
        }


        [ValidateInput(false)]
        [Route("UpdateFilingMass")]
        public ActionResult UpdateFilingMass(string listFilingIDs, string newFilingDueDate,string firstValues,string firstFilingID)
        {

            var spName = "NSP_MU_Update";

            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                 new Dictionary<string, string>
                {
                    { "Name", "@listFilingIDs" },
                    { "Value", listFilingIDs}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@newFilingDueDate" },
                    { "Value", newFilingDueDate}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@firstValues" },
                    { "Value", firstValues}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@firstFilingID" },
                    { "Value", firstFilingID}
                }
                #endregion
            });





            return Content("Saved!");
        }

        [ValidateInput(false)]
        [Route("DeleteFilingMass")]
        public ActionResult DeleteFilingMass(string listFilingIDs)
        {

            var spName = "NSP_MU_Delete";

            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                 new Dictionary<string, string>
                {
                    { "Name", "@listFilingIDs" },
                    { "Value", listFilingIDs}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                }
                #endregion
            });



    

            return Content("Saved!");
        }


    }
}
