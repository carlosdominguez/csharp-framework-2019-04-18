﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.IncomeAndOther
{
    [RoutePrefix("IncomeAndOther")]
    public class ReturnPackageController : Controller
    {
        //
        // GET: /

        [Route("ReturnPackage")]
        public ActionResult ReturnPackage()
        {
            return View("~/Views/IncomeAndOther/ReturnPackage.cshtml");
        }

        //Modal Template
        [Route("ModalTemplateRP")]
        public ActionResult ModalTemplate()
        {
            Dictionary<string, object> viewBagResult = new Dictionary<string, object>();

            #region View Bag Variables
            string logID = Request.QueryString["logID"];
            viewBagResult.Add("logID", logID);

            string entityID = Request.QueryString["entityID"];
            viewBagResult.Add("entityID", entityID);

            string entityName = Request.QueryString["entityName"];
            viewBagResult.Add("entityName", entityName);

            string function1 = Request.QueryString["function1"];
            viewBagResult.Add("function1", function1);

            string frequency = Request.QueryString["frequency"];
            viewBagResult.Add("frequency", frequency);

            string reviewer = Request.QueryString["reviewer"];
            string completeReviewer = reviewer + " (" + Request.QueryString["statusSOEID"] + ")";

            viewBagResult.Add("reviewer", completeReviewer);

            string preparer = Request.QueryString["preparer"];
            string completePreparer = preparer + " (" + Request.QueryString["statusPreparedSOEID"] + ")";
            viewBagResult.Add("preparer", completePreparer);

            string filingDueDate = Request.QueryString["filingDueDate"];
            viewBagResult.Add("filingDueDate", filingDueDate);

            string date_DueCorpTax = Request.QueryString["date_DueCorpTax"];
            viewBagResult.Add("date_DueCorpTax", date_DueCorpTax);

            string filingID = Request.QueryString["filingID"];
            viewBagResult.Add("filingID", filingID);

            string status = Request.QueryString["status"];
            viewBagResult.Add("status", status);

            string statusPrepared = Request.QueryString["statusPrepared"];
            viewBagResult.Add("statusPrepared", statusPrepared);

            string statusDesc = Request.QueryString["statusDesc"];
            viewBagResult.Add("statusDesc", statusDesc);

            string preparedDdate = Request.QueryString["preparedDdate"];
            viewBagResult.Add("preparedDdate", preparedDdate);

            string reviewedDate = Request.QueryString["reviewedDate"];
            viewBagResult.Add("reviewedDate", reviewedDate);

            string commentFlag = Request.QueryString["commentFlag"];
            viewBagResult.Add("commentFlag", commentFlag);

            string counts = Request.QueryString["counts"];
            viewBagResult.Add("counts", counts);

            string comments = Request.QueryString["comments"];
            viewBagResult.Add("comments", comments);

            string statusDate = Request.QueryString["statusDate"];
            viewBagResult.Add("statusDate", statusDate);

            string statusSOEID = Request.QueryString["statusSOEID"];
            viewBagResult.Add("statusSOEID", statusSOEID);

            string statusPreparedDate = Request.QueryString["statusPreparedDate"];
            viewBagResult.Add("statusPreparedDate", statusPreparedDate);

            string statusPreparedSOEID = Request.QueryString["statusPreparedSOEID"];
            viewBagResult.Add("statusPreparedSOEID", statusPreparedSOEID);

            string ttiCode = Request.QueryString["ttiCode"];
            viewBagResult.Add("ttiCode", ttiCode);

            string EIN = Request.QueryString["EIN"];
            viewBagResult.Add("EIN", EIN);

            string state = Request.QueryString["state"];
            viewBagResult.Add("state", state);

            string city = Request.QueryString["city"];
            viewBagResult.Add("city", city);

            string branch = Request.QueryString["branch"];
            viewBagResult.Add("branch", branch);

            string dueDay = Request.QueryString["dueDay"];
            viewBagResult.Add("dueDay", dueDay);

            string dueMonth = Request.QueryString["dueMonth"];
            viewBagResult.Add("dueMonth", dueMonth);

            #endregion

            return View("~/Views/IncomeAndOther/ModalReturnPackage.cshtml", viewBagResult);
        }

        #region Action Buttons
        // Preparer - Add Comments and BUs
        [ValidateInput(false)]
        [Route("PreparerEditRP")]
        public ActionResult PreparerEdit(string pjson, string comments, string accounts, string calendarType, string dateInput, string dateIputSent, string dateInputCopy, string dateInputSubmittedCorpTaxSP,
          string checkInputSentLEM, string checkInputReceiveLEM, string checkInputSubmittedCorpTax)
        {

            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_FC_PreparerEdit";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@accounts" },
                    { "Value", accounts}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", comments }
                }
            });

            var spName1 = "NSP_FC_PreparerSave_RP_Dates";
            var rowsResult1 = (new GlobalModel()).excecuteProcedureNoReturn(spName1, new List<Dictionary<string, string>> {

                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                }, new Dictionary<string, string>
                {
                    { "Name", "@calendarType" },
                    { "Value", calendarType}
                },
                 new Dictionary<string, string>
                {
                    { "Name", "@dateInput" },
                    { "Value", dateInput }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@dateIputSent" },
                    { "Value", dateIputSent }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@dateInputCopy" },
                    { "Value", dateInputCopy }
                },
                 new Dictionary<string, string>
                {
                    { "Name", "@dateInputSubmittedCorpTaxSP" },
                    { "Value", dateInputSubmittedCorpTaxSP }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@checkInputSentLEM" },
                    { "Value", checkInputSentLEM }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@checkInputReceiveLEM" },
                    { "Value", checkInputReceiveLEM }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@checkInputSubmittedCorpTax" },
                    { "Value", checkInputSubmittedCorpTax }
                }
            });


            return Content("Saved!");
        }

        // Preparer - Prepare the Filing
        [ValidateInput(false)]
        [Route("PreparerSaveRP")]
        public ActionResult PreparerSave(string pjson, string comments, string calendarType,string dateInput, string dateIputSent, string dateInputCopy, string dateInputSubmittedCorpTaxSP,
          string checkInputSentLEM, string checkInputReceiveLEM, string checkInputSubmittedCorpTax)
        {
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_FC_PreparerSave";
            var rowsResult = (new GlobalModel()).excecuteProcedureNoReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", comments }
                }
            });

            var spName1 = "NSP_FC_PreparerSave_RP_Dates";
            var rowsResult1 = (new GlobalModel()).excecuteProcedureNoReturn(spName1, new List<Dictionary<string, string>> {
                
                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                }, new Dictionary<string, string>
                {
                    { "Name", "@calendarType" },
                    { "Value", calendarType}
                },
                 new Dictionary<string, string>
                {
                    { "Name", "@dateInput" },
                    { "Value", dateInput }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@dateIputSent" },
                    { "Value", dateIputSent }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@dateInputCopy" },
                    { "Value", dateInputCopy }
                },
                 new Dictionary<string, string>
                {
                    { "Name", "@dateInputSubmittedCorpTaxSP" },
                    { "Value", dateInputSubmittedCorpTaxSP }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@checkInputSentLEM" },
                    { "Value", checkInputSentLEM }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@checkInputReceiveLEM" },
                    { "Value", checkInputReceiveLEM }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@checkInputSubmittedCorpTax" },
                    { "Value", checkInputSubmittedCorpTax }
                }
            });

            return Content("Prepared!");
        }

        // Preparer - Delete Filing
        [ValidateInput(false)]
        [Route("PreparerDeleteRP")]
        public ActionResult PreparerDelete(string pjson, string comments)
        {
            //REVIVAR - NO SE PUEDE ELIMINAR
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_FC_PreparerDelete";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", comments }
                }
            });
            return Content("Deleted!");
        }

        //ReviewerReturnRP
        [ValidateInput(false)]
        [Route("ReviewerReturnRP")]
        public ActionResult ReviewerReturnRP(string pjson, string comments)
        {
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_FC_ReviewerReturn_RP";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", comments }
                }
            });
            return Content("Saved!");

        }


        //ReviewerReturnToPreparer
        [ValidateInput(false)]
        [Route("ReviewerReturnToPreparer")]
        public ActionResult ReviewerReturnToPreparer(string pjson, string comments)
        {
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_FC_ReviewerReturnToPreparer_RP";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", comments }
                }
            });
            return Content("Saved!");

        }

        // Reviewer - Add Comments and Accounts
        [ValidateInput(false)]
        [Route("ReviewerEditRP")]
        public ActionResult ReviewerEdit(string pjson, string comments, string accounts, string duedate, string calendarType, string dateInput, string dateIputSent, string dateInputCopy, string dateInputSubmittedCorpTaxSP,
          string checkInputSentLEM, string checkInputReceiveLEM, string checkInputSubmittedCorpTax)
        {
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_FC_ReviewerEdit_RP";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@accounts" },
                    { "Value", accounts }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", comments }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@duedate" },
                    { "Value", duedate }
                }
            });

            var spName1 = "NSP_FC_PreparerSave_RP_Dates";
            var rowsResult1 = (new GlobalModel()).excecuteProcedureNoReturn(spName1, new List<Dictionary<string, string>> {

                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                }, new Dictionary<string, string>
                {
                    { "Name", "@calendarType" },
                    { "Value", calendarType}
                },
                 new Dictionary<string, string>
                {
                    { "Name", "@dateInput" },
                    { "Value", dateInput }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@dateIputSent" },
                    { "Value", dateIputSent }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@dateInputCopy" },
                    { "Value", dateInputCopy }
                },
                 new Dictionary<string, string>
                {
                    { "Name", "@dateInputSubmittedCorpTaxSP" },
                    { "Value", dateInputSubmittedCorpTaxSP }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@checkInputSentLEM" },
                    { "Value", checkInputSentLEM }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@checkInputReceiveLEM" },
                    { "Value", checkInputReceiveLEM }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@checkInputSubmittedCorpTax" },
                    { "Value", checkInputSubmittedCorpTax }
                }
            });


            return Content("Saved!");

        }

        // Reviewer - Review Filing
        [ValidateInput(false)]
        [Route("ReviewerSaveRP")]
        public ActionResult ReviewerSave(string pjson, string comments, string accounts,string duedate, string calendarType, string dateInput, string dateIputSent, string dateInputCopy, string dateInputSubmittedCorpTaxSP,
          string checkInputSentLEM, string checkInputReceiveLEM, string checkInputSubmittedCorpTax)
        {
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_FC_ReviewerSave_RP";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@accounts" },
                    { "Value", accounts }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", comments }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@duedate" },
                    { "Value", duedate }
                }
            });




            var spName1 = "NSP_FC_PreparerSave_RP_Dates";
            var rowsResult1 = (new GlobalModel()).excecuteProcedureNoReturn(spName1, new List<Dictionary<string, string>> {

                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                }, new Dictionary<string, string>
                {
                    { "Name", "@calendarType" },
                    { "Value", calendarType}
                },
                 new Dictionary<string, string>
                {
                    { "Name", "@dateInput" },
                    { "Value", dateInput }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@dateIputSent" },
                    { "Value", dateIputSent }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@dateInputCopy" },
                    { "Value", dateInputCopy }
                },
                 new Dictionary<string, string>
                {
                    { "Name", "@dateInputSubmittedCorpTaxSP" },
                    { "Value", dateInputSubmittedCorpTaxSP }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@checkInputSentLEM" },
                    { "Value", checkInputSentLEM }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@checkInputReceiveLEM" },
                    { "Value", checkInputReceiveLEM }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@checkInputSubmittedCorpTax" },
                    { "Value", checkInputSubmittedCorpTax }
                }
            });





            return Content("Saved!");
        }

        // Reviewer - Delete Filing
        [ValidateInput(false)]
        [Route("ReviewerDeleteRP")]
        public ActionResult ReviewerDelete(string pjson, string comments)
        {
            //REVIVAR - NO SE PUEDE ELIMINAR
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_FC_PreparerDelete";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", comments }
                }
            });
            return Content("Deleted!");
        }

        // All - Add Comments
        [ValidateInput(false)]
        [Route("CompletedEditRP")]
        public ActionResult CompletedEdit(string pjson, string comments)
        {

            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_FC_CompletedEdit";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@filingID" },
                    { "Value", data["filingID"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value",  GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", comments }
                }
            });
            return Content("Saved!");
        }

        #endregion 

    }
}
