using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.SupportDocProdCtrl
{
    [RoutePrefix("SupportDocProdCtrl")]
    public class SupportDocProdCtrlController : Controller
    {
        // ROUTE METHODS
        // ----------------------------------------------------------
        //
        // GET: /SupportDocProdCtrl/ProcessList
        [Route("ProcessList")]
        public ActionResult ProcessList()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            var pviewType = (String.IsNullOrEmpty(Request.QueryString.Get("pviewType"))) ? "" : Request.QueryString.Get("pviewType");
            var pidPFDetail = (String.IsNullOrEmpty(Request.QueryString.Get("pidPFDetail"))) ? "" : Request.QueryString.Get("pidPFDetail");
            var pidFrequency = (String.IsNullOrEmpty(Request.QueryString.Get("pidFrequency"))) ? "" : Request.QueryString.Get("pidFrequency");
            var pprocessDate = (String.IsNullOrEmpty(Request.QueryString.Get("pprocessDate"))) ? "" : Request.QueryString.Get("pprocessDate");
            var pyear = (String.IsNullOrEmpty(Request.QueryString.Get("pyear"))) ? "" : Request.QueryString.Get("pyear");
            var pmonth = (String.IsNullOrEmpty(Request.QueryString.Get("pmonth"))) ? "" : Request.QueryString.Get("pmonth");
            var padd = (String.IsNullOrEmpty(Request.QueryString.Get("padd"))) ? "0" : Request.QueryString.Get("padd");

            //When the email have Pending in the link
            if (pviewType == "Pending")
            {
                var userInfo = GlobalUtil.GetUserInfo();

                //User is Checker
                if (userInfo["Roles"].Contains("SUPPORTDOCPRODCTRL_CHECKER"))
                {
                    pviewType = "Checker";
                }
                else
                {
                    //User is Maker
                    if (userInfo["Roles"].Contains("SUPPORTDOCPRODCTRL_MAKER"))
                    {
                        pviewType = "Maker";
                    }
                    else
                    {
                        //User is Stakeholder
                        if (userInfo["Roles"].Contains("SUPPORTDOCPRODCTRL_STAKEHOLDER"))
                        {
                            pviewType = "Stakeholder";
                        }
                        else
                        {
                            //User is Admin
                            if (userInfo["Roles"].Contains("SUPPORTDOCPRODCTRL_ADMIN") || userInfo["Roles"].Contains("SUPPORTDOCPRODCTRL_SUPER_ADMIN"))
                            {
                                pviewType = "All";
                            }
                            else
                            {
                                return RedirectToAction("Index", "SupportDocProdCtrl");
                            }
                        }
                    }
                }
            }

            viewModel.Add("HFViewType", pviewType);
            viewModel.Add("HFIDPFDetail", pidPFDetail);
            viewModel.Add("HFIDFrequency", pidFrequency);
            viewModel.Add("HFProcessDate", pprocessDate);
            viewModel.Add("HFYear", pyear);
            viewModel.Add("HFMonth", pmonth);
            viewModel.Add("HFAddStakeholder", padd);

            return View("~/Views/SupportDocProdCtrl/ProcessList.cshtml", viewModel);
        }

        //
        // GET: /SupportDocProdCtrl/AdminProcessFile
        [Route("AdminProcessFile")]
        public ActionResult AdminProcessFile()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            return View("~/Views/SupportDocProdCtrl/AdminProcessFile.cshtml", viewModel);
        }

        //
        // GET: /SupportDocProdCtrl/PartialViewAdminProcessFile

        [Route("PartialViewAdminProcessFile")]
        public ActionResult PartialViewAdminProcessFile()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            var pidProcessFile = (String.IsNullOrEmpty(Request.QueryString.Get("pidProcessFile"))) ? "" : Request.QueryString.Get("pidProcessFile");
            var paction = (String.IsNullOrEmpty(Request.QueryString.Get("paction"))) ? "" : Request.QueryString.Get("paction");

            viewModel.Add("HFIDProcessFile", pidProcessFile);
            viewModel.Add("HFAction", paction);
            return View("~/Views/SupportDocProdCtrl/_TemplateProcessFile.cshtml", viewModel);
        }

        //
        // GET: /SupportDocProdCtrl/Authentication
        [Route("Authentication")]
        public ActionResult Authentication()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            viewModel.Add("WindowsIdentity", System.Security.Principal.WindowsIdentity.GetCurrent().Name);
            return View("~/Views/SupportDocProdCtrl/Authentication.cshtml", viewModel);
        }

        //
        // GET: /SupportDocProdCtrl/AuditLog
        [Route("AuditLog")]
        public ActionResult AuditLog()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            return View("~/Views/SupportDocProdCtrl/AuditLog.cshtml", viewModel);
        }

        // AJAX METHODS
        // ----------------------------------------------------------
        //
        // GET: /SupportDocProdCtrl/DownloadFile
        [Route("DownloadFile")]
        public ActionResult DownloadFile(string shareFolderFullPathFile, string fileName)
        {
            shareFolderFullPathFile = GlobalUtil.DecodeSkipSideminder(shareFolderFullPathFile);
            fileName = GlobalUtil.DecodeSkipSideminder(fileName);

            //Remove space error "500: Server Error [00-0002]" by certificate
            fileName = fileName.Replace(" ", "");
            fileName = fileName.Replace("&", "And");
            fileName = fileName.Replace("(", "");
            fileName = fileName.Replace(")", "");

            string fullPathToCopy = "";
            string hrefPath = "";
            try
            {
                //SET UP PATHS AND FILES NAMES
                string uploadFolder = GlobalUtil.GetValueAppConfig("appSettings", "UploadFolder") + "DownloadFiles" + @"/";
                hrefPath = uploadFolder + fileName;
                fullPathToCopy = @"" + Server.MapPath(uploadFolder) + fileName;

                //DELETE OLD FILES STORED ON THE DOWNLOADFOLDER
                System.IO.DirectoryInfo di = new DirectoryInfo(@"" + Server.MapPath(uploadFolder));
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }

                //COPY NEW FILE FRO SHAREDRIVE TO DOWNLOAD FOLDER
                System.IO.File.Copy(shareFolderFullPathFile, fullPathToCopy, true);
            }
            catch (Exception e)
            {
                if (e.Message.Contains("Could not find file"))
                {
                    hrefPath = "Could not find file";
                }
                else
                {
                    hrefPath = "Error!";
                    throw;
                }
            }
            return Content(hrefPath);
        }

        //
        // GET: /SupportDocProdCtrl/CreateIfNotExistsFolderPath
        [Route("CreateIfNotExistsFolderPath")]
        public ActionResult CreateIfNotExistsFolderPath(string typeAuthentication, string user, string pass, string folderPath)
        {
            user = GlobalUtil.DecodeSkipSideminder(user);
            pass = GlobalUtil.DecodeAsciiString(pass);
            folderPath = GlobalUtil.DecodeSkipSideminder(folderPath);

            var resulMsg = "Pending";

            if (typeAuthentication == "WindowsAuthentication")
            {
                var userData = user.Split('\\');
                using (UserImpersonation userImperso = new UserImpersonation(userData[1].ToUpper(), userData[0].ToUpper(), pass))
                {
                    if (userImperso.ImpersonateValidUser())
                    {
                        try
                        {
                            FileInfo fileInfo = new FileInfo(folderPath);
                            Directory.CreateDirectory(fileInfo.Directory.FullName);
                            resulMsg = "Directory '" + folderPath + "' was created!";
                        }
                        catch (Exception e)
                        {
                            if (e.Message.Contains("Access to the path") && e.Message.Contains("is denied"))
                            {
                                resulMsg = "Access to the path '" + folderPath + "' is denied for user '" + user + "'";
                            }
                            else
                            {
                                throw e;
                            }
                        }
                    }
                    else
                    {
                        resulMsg = "User or Password incorrect";
                    }
                }
            }
            else
            {
                user = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                try
                {
                    FileInfo fileInfo = new FileInfo(folderPath);
                    Directory.CreateDirectory(fileInfo.Directory.FullName);
                    resulMsg = "Directory '" + folderPath + "' was created!";
                }
                catch (Exception e)
                {
                    if (e.Message.Contains("Access to the path") && e.Message.Contains("is denied"))
                    {
                        resulMsg = "Access to the path '" + folderPath + "' is denied for user '" + user + "'";
                    }
                    else
                    {
                        throw e;
                    }
                }
            }

            return Content(resulMsg);
        }
        
        //
        // GET: /SupportDocProdCtrl/CheckAccess
        [Route("CheckAccess")]
        public ActionResult CheckAccess(string typeAuthentication, string user, string pass, string pathToTest)
        {
            user = GlobalUtil.DecodeSkipSideminder(user);
            pass = GlobalUtil.DecodeAsciiString(pass);
            pathToTest = GlobalUtil.DecodeSkipSideminder(pathToTest);

            var resulMsg = "Pending"; 

            if (typeAuthentication == "WindowsAuthentication")
            {
                var userData = user.Split('\\');
                using (UserImpersonation userImperso = new UserImpersonation(userData[1].ToUpper(), userData[0].ToUpper(), pass))
                {
                    if (userImperso.ImpersonateValidUser())
                    {
                        try
                        {
                            resulMsg = "Success! The user '" + user + "' has access to the " + GlobalUtil.CheckPathIsFileOrDirectory(pathToTest) + " '" + pathToTest + "'";
                        }
                        catch (Exception e)
                        {
                            if (e.Message.Contains("Access to the path") && e.Message.Contains("is denied"))
                            {
                                resulMsg = "Access to the path '" + pathToTest + "' is denied for user '" + user + "'";
                            }
                            else
                            {
                                if (e.Message.Contains("The file cannot be accessed by the system"))
                                {
                                    resulMsg = "Path '" + pathToTest + "' not exist or it is incorrect because cannot be accessed by the system with user '" + user + "'";
                                }
                                else
                                {
                                    if (e.Message.Contains("The network path was not found"))
                                    {
                                        resulMsg = "The network path '" + pathToTest + "' was not found.";
                                    }
                                    else
                                    {
                                        throw e;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        resulMsg = "User or Password incorrect";
                    }
                }
            }
            else
            {
                user = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                try
                {
                    resulMsg = "Success! The user '" + user + "' has access to the " + GlobalUtil.CheckPathIsFileOrDirectory(pathToTest) + " '" + pathToTest + "'";
                }
                catch (Exception e)
                {
                    if (e.Message.Contains("Access to the path") && e.Message.Contains("is denied"))
                    {
                        resulMsg = "Access to the path '" + pathToTest + "' is denied for user '" + user + "'";
                    }
                    else
                    {
                        if (e.Message.Contains("The file cannot be accessed by the system"))
                        {
                            resulMsg = "Path '" + pathToTest + "' not exist or it is incorrect because cannot be accessed by the system with user '" + user + "'";
                        }
                        else
                        {
                            if (e.Message.Contains("The network path was not found"))
                            {
                                resulMsg = "The network path '" + pathToTest + "' was not found.";
                            }
                            else
                            {
                                throw e;
                            }
                        }
                    }
                }
            }

            return Content(resulMsg);
        }

        //
        // GET: /SupportDocProdCtrl/CheckProcessFiles
        [Route("CheckProcessFiles")]
        public ActionResult CheckProcessFiles(string jsonProcessFiles, string typeAuthentication, string user, string pass)
        {
            DataTable tblProcessFiles = (DataTable)JsonConvert.DeserializeObject(jsonProcessFiles, (typeof(DataTable)));

            string resultMsg = "No data found.";
            user = GlobalUtil.DecodeSkipSideminder(user);
            pass = GlobalUtil.DecodeAsciiString(pass);

            //Validate Authentication to scan folder
            if (typeAuthentication == "WindowsAuthentication")
            {
                var userData = user.Split('\\');
                using (UserImpersonation userImperso = new UserImpersonation(userData[1].ToUpper(), userData[0].ToUpper(), pass))
                {
                    if (userImperso.ImpersonateValidUser())
                    {
                        try
                        {
                            //Update File status column
                            tblProcessFiles = scanFiles(tblProcessFiles);
                        }
                        catch (Exception e)
                        {
                            if (e.Message.Contains("Access to the path") && e.Message.Contains("is denied"))
                            {
                                resultMsg = "Access to the Share Folder is denied for user '" + user + "', please go to then menu in 'Authentication' and set a valid configuration.";
                            }
                            else
                            {
                                throw e;
                            }
                        }
                    }
                    else
                    {
                        resultMsg = "User or Password incorrect";
                    }
                }
            }
            else
            {
                user = user = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                try
                {
                    //Update File status column
                    tblProcessFiles = scanFiles(tblProcessFiles);
                }
                catch (Exception e)
                {
                    if (e.Message.Contains("Access to the path") && e.Message.Contains("is denied"))
                    {
                        resultMsg = "Access to the Share Folder is denied for user '" + user + "', please go to then menu in 'Authentication' and set a valid configuration.";
                    }
                    else
                    {
                        throw e;
                    }
                }
            }

            //Save data in the database
            if (tblProcessFiles != null && tblProcessFiles.Rows.Count > 0)
            {
                (new GlobalModel()).SaveDataTableDB(tblProcessFiles, "[dbo].[spSDocProdCtrlUploadCheckPFDetailStatus]", new List<Dictionary<string, string>> { 
                    new Dictionary<string, string>
                    {
                        { "Name", "@SOEIDUploader" },
                        { "Value", GlobalUtil.GetSOEID() }
                    }
                });

                resultMsg = tblProcessFiles.Rows.Count + " files checked";
            }

            return Content(resultMsg);
        }

        //
        // GET: /SupportDocProdCtrl/Upload
        [ValidateInput(false)]
        [Route("Upload")]
        public ActionResult Upload(string jsonProcessFile, string filePrefix, string comment, string userRole, string isExtraFile)
        {
            Dictionary<string, object> result = new Dictionary<string, object> { { "success", true } };
            Dictionary<string, string> objProcessFile = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonProcessFile);
            string locationToSave = objProcessFile["FolderFullPath"];

            try
            {
                var objFile = GlobalUtil.GetFileFromCurrentRequest(false);
                var fileName = filePrefix + objFile.Filename;
                
                //Copy file to share folder
                var fullPathSaved = GlobalUtil.FileSaveAs(locationToSave, fileName, objFile.InputStream, false);

                //When is a Process File
                //if (isExtraFile == "0")
                //{
                //    (new GlobalModel()).excecuteProcedureNoReturn("[dbo].[spSDocProdCtrlAdminPFDetailAuditLog]", new List<Dictionary<string, string>> { 
                //        new Dictionary<string, string>{
                //            { "Name", "@Action" }, { "Value",  "Insert" }
                //        },
                //        new Dictionary<string, string>{
                //            { "Name", "@Type" }, { "Value",  "File Uploaded" }
                //        },
                //        new Dictionary<string, string>{
                //            { "Name", "@IDPFDetails" }, { "Value",  objProcessFile["IDPFDetail"] }
                //        },
                //        new Dictionary<string, string>{
                //            { "Name", "@FileStatus" }, { "Value",  "Received" }
                //        },
                //        new Dictionary<string, string>{
                //            { "Name", "@StatusByChecker" }, { "Value",  objProcessFile["StatusByChecker"] }
                //        },
                //        new Dictionary<string, string>{
                //            { "Name", "@Comment" }, { "Value",  comment }
                //        },
                //        new Dictionary<string, string>{
                //            { "Name", "@UserRole" }, { "Value",  userRole }
                //        },
                //        new Dictionary<string, string> {
                //            { "Name", "@SessionSOEID" }, { "Value", GlobalUtil.GetSOEID() }
                //        } 
                //    });
                //}

                //When is a Process File Extra 
                if (isExtraFile == "1")
                {
                    string[] fileInfo = fileName.Split('.');
                    (new GlobalModel()).excecuteProcedureNoReturn("[dbo].[spSDocProdCtrlAdminPFDetailAuditLogFile]", new List<Dictionary<string, string>> { 
                        new Dictionary<string, string>{
                            { "Name", "@Action" }, { "Value",  "Insert" }
                        },
                        new Dictionary<string, string>{
                            { "Name", "@IDPFDetail" }, { "Value",  objProcessFile["IDPFDetail"] }
                        },
                        new Dictionary<string, string>{
                            { "Name", "@IDPFDetailAuditLog" }, { "Value",  objProcessFile["IDPFDetailAuditLog"] }
                        },
                        new Dictionary<string, string>{
                            { "Name", "@FileFullPath" }, { "Value",  locationToSave + fileName }
                        },
                        new Dictionary<string, string>{
                            { "Name", "@FileName" }, { "Value",  fileInfo[0] }
                        },
                        new Dictionary<string, string>{
                            { "Name", "@FileExt" }, { "Value",  fileInfo[1] }
                        }
                    });
                }

                result.Add("fullPathSaved", fullPathSaved);
            }
            catch (Exception ex)
            {
                //return new FileUploaderResult(false, error: ex.Message);
                result["success"] = false;
                result.Add("error", ex.ToString());
            }

            // the anonymous object in the result below will be convert to json and set back to the browser
            //return new FileUploaderResult(true, new { extraInformation = 12345 });

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /SupportDocProdCtrl/DeleteFile
        [Route("DeleteFile")]
        public ActionResult DeleteFile(string pathToDelete)
        {
            pathToDelete = GlobalUtil.DecodeSkipSideminder(pathToDelete);

            //Delete File
            System.IO.File.Delete(pathToDelete);

            return Content("Deleted");
        }


        //
        // GET: /SupportDocProdCtrl/AdminDetailExtraComment
        [ValidateInput(false)]
        [Route("AdminDetailExtraComment")]
        public ActionResult AdminDetailExtraComment(string idPFDetail, string comment)
        {
            string idPFDetailExtraComment = "0";
            
            var resultList = (new GlobalModel()).excecuteProcedureReturn("[dbo].[spSDocProdCtrlAdminDetailExtraComment]", new List<Dictionary<string, string>> { 
                new Dictionary<string, string>{
                    { "Name", "@Action" }, { "Value",  "Insert" }
                },
                new Dictionary<string, string>{
                    { "Name", "@IDPFDetail" }, { "Value",  idPFDetail }
                },
                new Dictionary<string, string>{
                    { "Name", "@Comment" }, { "Value",  comment }
                },
                new Dictionary<string, string> {
                    { "Name", "@SessionSOEID" }, { "Value", GlobalUtil.GetSOEID() }
                } 
            });
            idPFDetailExtraComment = resultList[0]["IDPFDetailExtraComment"];

            // the anonymous object in the result below will be convert to json and set back to the browser
            //return new FileUploaderResult(true, new { extraInformation = 12345 });

            return Content(idPFDetailExtraComment);
        }

        // CLASS METHODS
        // ----------------------------------------------------------
        //
        public DataTable scanFiles(DataTable tblProcessFiles)
        {
            var distinctDocs = new Dictionary<string, string>();
            var checkedExceptionNoAccess = false;

            for (int i = 0; i < tblProcessFiles.Rows.Count; i++)
            {
                //tblProcessFiles.Rows[0]["FileStatus"] = "Test";
                string fileFullPath = tblProcessFiles.Rows[i]["FileFullPath"].ToString();

                //Validate file
                FileInfo fileInfo = new FileInfo(fileFullPath);

                if (!checkedExceptionNoAccess)
                {
                    //Throw no access exception
                    var isReadOnly = fileInfo.IsReadOnly;
                    checkedExceptionNoAccess = true;
                }

                //Update File Status
                tblProcessFiles.Rows[i]["FileStatus"] = (fileInfo.Exists) ? "Received" : "Not Received";
            }

            return tblProcessFiles;
        }

        public string exportFiles(List<Dictionary<string, string>> docList)
        {
            string docsPathCopied = "";
            var distinctDocs = new Dictionary<string, string>();
            
            foreach (var docItem in docList)
            {
                string fileFullPathExport = docItem["RegionFolderPath"] + @"Approved\" + docItem["FilePath"];

                //Only if not exists document to avoid duplicates
                if (!distinctDocs.ContainsKey(fileFullPathExport))
                {
                    FileInfo fileInfo = new FileInfo(fileFullPathExport);
                    Directory.CreateDirectory(fileInfo.Directory.FullName);

                    //Export fiile to new location
                    System.IO.File.Copy(docItem["FileFullPath"], fileFullPathExport, true);
                    docsPathCopied += fileFullPathExport + "\n";

                    distinctDocs.Add(fileFullPathExport, "1");
                }
            }

            return docsPathCopied;
        }
    }
}
