﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.Home
{
    [RoutePrefix("/")]
    public class HomeController : Controller
    {
        //
        // GET: /
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            var defaultAppKey = GlobalUtil.GetAppKey();
            string redirectToHomeApp = GlobalUtil.GetValueAppConfig("appSettings", "RedirectToHome");
            string DBIsConfigured = GlobalUtil.GetValueAppConfig("appSettings", "DBIsConfigured");

            if (DBIsConfigured == "1")
            {
                //Calculate if is under maintenance
                var tableName = GlobalUtil.GetDBObjectFullName("GlobalVar");
                var isUnder = "0";
                var underMsg = "";

                var sqlUnder =
                    "SELECT " +
                    "    (SELECT [VAR_VALUE]  FROM " + tableName + " WHERE [VAR_NAME] = 'IS_UNDERMAINTENANCE' ) AS IS_UNDERMAINTENANCE, " +
                    "    (SELECT [VAR_VALUE]  FROM " + tableName + " WHERE [VAR_NAME] = 'UNDERMAINTENANCE_MESSAGE' ) AS UNDERMAINTENANCE_MESSAGE ";

                //Get resul
                List<Dictionary<string, string>> resultList = (new GlobalModel()).excecuteQuery(sqlUnder);

                if (resultList.Count > 0)
                {
                    isUnder = resultList[0]["IS_UNDERMAINTENANCE"];
                    underMsg = resultList[0]["UNDERMAINTENANCE_MESSAGE"];
                }

                if (isUnder == "1")
                {

                    viewModel.Add("UnderMaintenanceMessage", underMsg);
                    return View("~/Views/Shared/UnderMaintenance.cshtml", viewModel);
                }
            }
            
            if (redirectToHomeApp == "1")
            {
                return RedirectToAction("Index", defaultAppKey + "/");
            }
            else
            {
                return View("~/Views/Shared/Home.cshtml");
            }
        }

    }
}
