using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.IQueryMetrics
{
    [RoutePrefix("IQueryMetrics")]
    public class ImportDataController : Controller
    {
        //
        // GET: /
        [Route("ImportData")]
        public ActionResult Index()
        {
            return View("~/Views/IQueryMetrics/ImportData.cshtml");
        }



        [Route("UploadReport1")]
        public ActionResult UploadReport1()
        {
            Dictionary<string, object> result = new Dictionary<string, object> { { "success", true } };

            //try
            //{
                var objFile = GlobalUtil.GetFileFromCurrentRequest();
                string fullPathSaved;
                DataTable ptable;

                //Save excel file in the server
                fullPathSaved = GlobalUtil.FileSaveAs("ImportReports", objFile.Filename, objFile.InputStream);
                if (objFile.Filename.Contains(".csv"))
                {
                    ptable = GlobalUtil.GetDataTableFromCSV(fullPathSaved);
                }
                else {
                    ptable = GlobalUtil.GetDataTableFromExcel(fullPathSaved);
                }



                string fileName = System.IO.Path.GetFileName(fullPathSaved);

                if (fileName.Contains("Volume"))
                {
                    //Save data in the database
                    (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[NSP_ID_UPLOAD_VOLUME]", new List<Dictionary<string, string>> {
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEIDUploader" },
                                { "Value", GlobalUtil.GetSOEID() }
                            }
                        });
                }

                if (fileName.Contains("content_leaderboard"))
                {
                    //Save data in the database
                    (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[NSP_ID_UPLOAD_CONTENT_LEADERBOARD]", new List<Dictionary<string, string>> {
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEIDUploader" },
                                { "Value", GlobalUtil.GetSOEID() }
                            }
                        });
                }

                if (fileName.Contains("Usage"))
                {
                    //Save data in the database
                    (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[NSP_ID_UPLOAD_USAGE_MASTER]", new List<Dictionary<string, string>> {
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEIDUploader" },
                                { "Value", GlobalUtil.GetSOEID() }
                            }
                        });
                }

            if (fileName.Contains("Police"))
            {
                //Save data in the database
                (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[NSP_ID_UPLOAD_POLICE]", new List<Dictionary<string, string>> {
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEIDUploader" },
                                { "Value", GlobalUtil.GetSOEID() }
                            }
                        });
            }

            if (fileName.Contains("MasterData"))
            {
                //Save data in the database
                (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[NSP_ID_UPLOAD_MASTERDATA]", new List<Dictionary<string, string>> {
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEIDUploader" },
                                { "Value", GlobalUtil.GetSOEID() }
                            }
                        });
            }





            string uploadFolder = GlobalUtil.GetValueAppConfig("appSettings", "UploadFolder") + "ImportReports" + @"\";
                string fullPathDelete = @"" + Server.MapPath(uploadFolder);

                System.IO.DirectoryInfo di = new DirectoryInfo(@"" + fullPathDelete);
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }

            //}
            //catch (Exception ex)
            //{
            //    ////return new FileUploaderResult(false, error: ex.Message);
            //    //result["success"] = false;
            //    //result.Add("error", ex.ToString());

            //    int s = 0;


            //}

            // the anonymous object in the result below will be convert to json and set back to the browser
            //return new FileUploaderResult(true, new { extraInformation = 12345 });

            return Json(result, JsonRequestBehavior.AllowGet);
        }





    }
}
