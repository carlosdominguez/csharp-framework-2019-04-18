using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.IQueryMetrics
{
    [RoutePrefix("IQueryMetrics")]
    public class HomeController : Controller
    {
        //
        // GET: /
        [Route("/")]
        public ActionResult Index()
        {
            return View("~/Views/IQueryMetrics/Home.cshtml");
        }

    }
}
