using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace Automation.Web.Controllers.IQueryMetrics
{
    [RoutePrefix("IQueryMetrics")]
    public class PoliceController : Controller
    {
        //
        // GET: /
        [Route("Police")]
        public ActionResult Index()
        {
            return View("~/Views/IQueryMetrics/Police.cshtml");
        }


        [ValidateInput(false)]
        [Route("UpdatePolice")]
        public ActionResult UpdatePolice(string pjson)
        {
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_AC_POLICE_UPDATE";


            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                 new Dictionary<string, string>
                {
                    { "Name", "@AP" },
                    { "Value", data["AP"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@emailReference" },
                    { "Value", data["emailReference"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value", data["soeid"]}
                }
                ,
                new Dictionary<string, string>
                {
                    { "Name", "@changeSoeid" },
                    { "Value", data["Changesoeid"]}
                }
        });
            return Content("Saved!");
        }


        //Modal Template
        [Route("ModalPolice")]
        public ActionResult ModalPolice()
        {
            Dictionary<string, object> viewBagResult = new Dictionary<string, object>();

            #region View Bag Variables
            string SOEID = Request.QueryString["SOEID"];
            string Name = Request.QueryString["Name"];
            string Status = Request.QueryString["Status"];
            string Approval = Request.QueryString["Approval"];
            string ApprovedDate = Request.QueryString["ApprovedDate"];
            string ApprovedBy = Request.QueryString["ApprovedBy"];
            string EmailReference = Request.QueryString["EmailReference"];
            string IQueryGroup = Request.QueryString["IQueryGroup"];


            viewBagResult.Add("SOEID", SOEID);
            viewBagResult.Add("Name", Name);
            viewBagResult.Add("Status", Status);
            viewBagResult.Add("Approval", Approval);
            viewBagResult.Add("ApprovedDate", ApprovedDate);
            viewBagResult.Add("ApprovedBy", ApprovedBy);
            viewBagResult.Add("EmailReference", EmailReference);
            viewBagResult.Add("IQueryGroup", IQueryGroup);

            #endregion

            return View("~/Views/IQueryMetrics/ModalPolice.cshtml", viewBagResult);
        }

    }




}
