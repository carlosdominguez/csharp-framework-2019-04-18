using AttributeRouting;
using AttributeRouting.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using Automation.Web.Models.CashFlow;
using System.Globalization;
using Automation.Core.Model;
using Automation.Utils;

namespace Automation.Web.Controllers.CF
{
    [RoutePrefix("CF")]
    public class HomeController : Controller
    {
        //
        // GET: /CF/
        [Route("/")]
        public ActionResult Index()
        {
            return View("~/Views/CF/Home.cshtml");
        }

        [Route("download_ExcelJson")]
        public void download_ExcelJson(string data)
        {
       
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            DataTable ptable = (DataTable)JsonConvert.DeserializeObject(objParameter["excel"], (typeof(DataTable)));
            string ReportName = objParameter["ReportName"].ToString();

            Excel.FasterExcel(HttpContext.Response, ptable, ReportName);

           // return Content("");
        }
    }
}
