﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using Automation.Web.Models.CashFlow;
using System.Globalization;
using Automation.Core.Model;



namespace Automation.Web.Controllers.CF
{

    [RoutePrefix("CF")]
    public class EditCheckController : Controller
    {

        [Route("EditCheck/{account?}")]
        public ActionResult Index(string account)
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            viewModel.Add("Account", account);

            return (View("~/Views/CF/EditCheck.cshtml", viewModel));
        }

        [Route("procEC_GetFilterBU")]
        public ActionResult procEC_GetFilterBU(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int pID = Convert.ToInt32(objParameter["pID"].ToString());
           
            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_CountryBU join
                    c in DAO.tblCF_R_RegionCountry on p.CountryID equals c.CountryID
                    where c.RegionID == pID
                    select new
                    {
                        p.ID,
                        Value = p.BU

                    }
            });

            return Content(o["item"].ToString());

        }
        [Route("procEC_GetEditCheckData")]
        public ActionResult procEC_GetEditCheckData(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int pID = Convert.ToInt32(objParameter["pID"].ToString());
            int DateID  = Convert.ToInt32(objParameter["Date"].ToString());

            tblCF_M_MonthYearQuater DateData = (from p in DAO.tblCF_M_MonthYearQuater where p.ID == DateID select p).SingleOrDefault<tblCF_M_MonthYearQuater>();

            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_EditCheck
                    where p.RegionID == pID && p.Year == DateData.Year && p.Quater == DateData.Quater
                    select new
                    {
                        p.ID,
                        p.BU,
                        p.Type,
                        p.CheckID,
                        p.Description,
                        p.Operator,
                        p.DollarTolAmt,
                        p.DollarTolPer,
                        p.LeftTotal,
                        p.RightTotal,
                        variance = p.LeftTotal - p.RightTotal,
                        p.Validation

                    }
            });

            return Content(o["item"].ToString());

        }


        [Route("procEC_GetEditCheckDataDetail")]
        public ActionResult procEC_GetEditCheckDataDetail(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int pID = Convert.ToInt32(objParameter["pID"].ToString());
            int DateID = Convert.ToInt32(objParameter["Date"].ToString());

            tblCF_M_MonthYearQuater DateData = (from p in DAO.tblCF_M_MonthYearQuater where p.ID == DateID select p).SingleOrDefault<tblCF_M_MonthYearQuater>();

            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_EditCheckDetail join c in DAO.tblCF_EditCheck
                    on p.CheckID equals c.ID 
                    where c.RegionID == pID && c.Year == DateData.Year && c.Quater == DateData.Quater
                    orderby p.IsRight
                    select new
                    {
                        p.CheckID,
                        p.IsSum,
                        p.IsRight,
                        p.Account,
                        p.Description,
                        p.IceCode,
                        p.Month,
                        p.Value

                    }
            });

            return Content(o["item"].ToString());

        }


        [Route("procEC_RunEditCheck")]
        public ActionResult procEC_RunEditCheck(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int pID = Convert.ToInt32(objParameter["pID"].ToString());
            int DateID = Convert.ToInt32(objParameter["Date"].ToString());
            string BU = objParameter["BU"].ToString();

            tblCF_M_MonthYearQuater DateData = (from p in DAO.tblCF_M_MonthYearQuater where p.ID == DateID select p).SingleOrDefault<tblCF_M_MonthYearQuater>();

            Dictionary<String, String> obj = new Dictionary<string, string>();


            DAO.procCF_RunEditCheck(pID, DateData.Quater, DateData.Year, BU);

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_EditCheck
                    where p.RegionID == pID && p.Year == DateData.Year && p.Quater == DateData.Quater
                    select new
                    {
                        p.ID,
                        p.BU,
                        p.Type,
                        p.CheckID,
                        p.Description,
                        p.Operator,
                        p.DollarTolAmt,
                        p.DollarTolPer,
                        p.LeftTotal,
                        p.RightTotal,
                        variance = p.LeftTotal - p.RightTotal,
                        p.Validation

                    }
            });

            return Content(o["item"].ToString());

        }

    }





}