﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using Automation.Web.Models.CashFlow;
using System.Globalization;
using Automation.Core.Model;
using Automation.Core.Class;
using System.IO;


namespace Automation.Web.Controllers.CF
{
    [RoutePrefix("CF")]
    public class MasterTableCFController : Controller
    {
        [Route("MasterTableCF")]
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            viewModel.Add("Region", "");
            viewModel.Add("Year", "");
            viewModel.Add("Month", "");

            return View("~/Views/CF/MasterTable.cshtml", viewModel);
        }

        [Route("getCF_MasterTableDetail")]
        public ActionResult getCF_MasterTableDetail(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int Region = Convert.ToInt32(objParameter["Region"].ToString());
            int Quater = Convert.ToInt32(objParameter["Quater"].ToString());

            tblCF_R_Region tblRegion = (from p in DAO.tblCF_R_Region where p.ID == Region select p).SingleOrDefault<tblCF_R_Region>();
            tblCF_M_MonthYearQuater dates = (from p in DAO.tblCF_M_MonthYearQuater where p.ID == Quater select p).SingleOrDefault<tblCF_M_MonthYearQuater>();


            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                     from p in DAO.tblCF_Master_Detail
                     join r in DAO.tblCF_R_Region on p.Region equals r.ID
                     join rc in DAO.tblCF_R_RegionCountry on r.ID equals rc.RegionID
                     from c in DAO.tblCF_CountryBU 
                     where rc.CountryID == c.CountryID && p.BU == c.BU
                     join country in DAO.tblCF_R_Country on c.CountryID equals country.ID
                     where p.Region == tblRegion.ID && p.Quater == dates.Quater && p.Year == dates.Year && p.tblCF_CF_Account.Enable == true 
                     from master in DAO.tblCF_Master_DetailManual
                     where p.Region == master.Region && p.BU == master.BU && p.CFAccountID == master.CFAccountID && p.Quater == master.Quater && p.Year == master.Year
                     select new
                     {
                         p.ID,
                         r.Region,
                         countryID = country.ID,
                         country.Country,
                         cfAccountID = p.tblCF_CF_Account.ID,
                         p.tblCF_CF_Account.CFAccount,
                         p.tblCF_CF_Account.Description,
                         p.tblCF_CF_Account.tblCF_CF_TypeAccount.Name,
                         typeID = p.tblCF_CF_Account.tblCF_CF_TypeAccount.ID,
                         p.Quater,
                         p.Year,
                         p.M1,
                         p.M2,
                         p.M3,
                         p.PastQuater,
                         p.CurrentQuater,
                         p.BU,
                         ManualUpdate = master.CurrentQuater
                     }
                     });


            return Content(o["item"].ToString());
        }

        [Route("getCF_MasterTableRunMapping")]
        public ActionResult getCF_MasterTableRunMapping(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int Region = Convert.ToInt32(objParameter["Region"].ToString());
            int Quater = Convert.ToInt32(objParameter["Quater"].ToString());
            //int Year = Convert.ToInt32(objParameter["Year"].ToString());
            string User = objParameter["User"].ToString();

            tblCF_M_MonthYearQuater dates = (from p in DAO.tblCF_M_MonthYearQuater where p.ID == Quater select p).SingleOrDefault<tblCF_M_MonthYearQuater>();


            string BU = objParameter["BU"].ToString();
            int Country = Convert.ToInt32(objParameter["Country"].ToString());
            int Type = Convert.ToInt32(objParameter["Type"].ToString());
            int CFAccount = Convert.ToInt32(objParameter["CFAccount"].ToString());

            if(BU == "0")
            {
                BU = "";
            }

            tblCF_R_Region tblRegion = (from p in DAO.tblCF_R_Region where p.ID == Region select p).SingleOrDefault<tblCF_R_Region>();

            if(Type == 0) { 
                List<tblCF_CF_TypeAccount> typeAccount = (from p in DAO.tblCF_CF_TypeAccount select p).ToList<tblCF_CF_TypeAccount>();
                foreach(tblCF_CF_TypeAccount type in typeAccount)
                {
                    //DAO.procCF_RunMapping(tblRegion.ID, 0, 0, type.ID, Quater, Year);

                    //DAO.procCF_RunMappingMasterDetail(tblRegion.ID, 0, 0, type.ID, Quater, Year, User);

                    DAO.procCF_RunMappingByBU(tblRegion.ID, Country, CFAccount, type.ID, dates.Quater, dates.Year, BU, User);
                }
            }
            else
            {
                DAO.procCF_RunMappingByBU(tblRegion.ID, Country, CFAccount, Type, dates.Quater, dates.Year, BU, User);
                 
    }


            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_Master_Detail
                    join r in DAO.tblCF_R_Region on p.Region equals r.ID
                    join rc in DAO.tblCF_R_RegionCountry on r.ID equals rc.RegionID
                    from c in DAO.tblCF_CountryBU
                    where rc.CountryID == c.CountryID && p.BU == c.BU
                    join country in DAO.tblCF_R_Country on c.CountryID equals country.ID
                    where p.Region == tblRegion.ID && p.Quater == dates.Quater && p.Year == dates.Year && p.tblCF_CF_Account.Enable == true
                    from master in DAO.tblCF_Master_DetailManual
                    where p.Region == master.Region && p.BU == master.BU && p.CFAccountID == master.CFAccountID && p.Quater == master.Quater && p.Year == master.Year
                    select new
                    {
                        p.ID,
                        r.Region,
                        countryID = country.ID,
                        country.Country,
                        cfAccountID = p.tblCF_CF_Account.ID,
                        p.tblCF_CF_Account.CFAccount,
                        p.tblCF_CF_Account.Description,
                        p.tblCF_CF_Account.tblCF_CF_TypeAccount.Name,
                        typeID = p.tblCF_CF_Account.tblCF_CF_TypeAccount.ID,
                        p.Quater,
                        p.Year,
                        p.M1,
                        p.M2,
                        p.M3,
                        p.PastQuater,
                        p.CurrentQuater,
                        p.BU,
                        ManualUpdate = master.CurrentQuater
                    }



            });


            return Content(o["item"].ToString());
        }


        [Route("getCF_comboBoxBU")]
        public ActionResult getCF_comboBoxBU(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int Region = Convert.ToInt32(objParameter["Region"].ToString());
            //nt Quater = Convert.ToInt32(objParameter["Quater"].ToString());
            //int Year = Convert.ToInt32(objParameter["Year"].ToString());

            tblCF_R_Region tblRegion = (from p in DAO.tblCF_R_Region where p.ID == Region select p).SingleOrDefault<tblCF_R_Region>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_CountryBU 
                    join r in DAO.tblCF_R_RegionCountry on p.CountryID  equals r.CountryID
                    where r.RegionID == tblRegion.ID
                    select new
                    {
                        p.BU
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("getCF_comboBoxCountry")]
        public ActionResult getCF_comboBoxCountry(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int Region = Convert.ToInt32(objParameter["Region"].ToString());
            //nt Quater = Convert.ToInt32(objParameter["Quater"].ToString());
            //int Year = Convert.ToInt32(objParameter["Year"].ToString());

            tblCF_R_Region tblRegion = (from p in DAO.tblCF_R_Region where p.ID == Region select p).SingleOrDefault<tblCF_R_Region>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_R_Country
                    join r in DAO.tblCF_R_RegionCountry on p.ID equals r.CountryID
                    where r.RegionID == tblRegion.ID
                    select new
                    {
                        p.ID,
                        p.Country
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("getCF_comboBoxType")]
        public ActionResult getCF_comboBoxType(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_CF_TypeAccount
                    select new
                    {
                        p.ID,
                        p.Name
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("getCF_comboBoxCFAccount")]
        public ActionResult getCF_comboBoxCFAccount(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int Region = Convert.ToInt32(objParameter["Region"].ToString());
            int Quater = Convert.ToInt32(objParameter["Quater"].ToString());
            //int Year = Convert.ToInt32(objParameter["Year"].ToString());
            tblCF_M_MonthYearQuater dates = (from p in DAO.tblCF_M_MonthYearQuater where p.ID == Quater select p).SingleOrDefault<tblCF_M_MonthYearQuater>();
            tblCF_R_Region tblRegion = (from p in DAO.tblCF_R_Region where p.ID == Region select p).SingleOrDefault<tblCF_R_Region>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_CF_Account
                    where p.RegionID == tblRegion.ID && p.QuaterApply == dates.Quater && p.YearApply == dates.Year && p.Enable == true
                    select new
                    {
                        p.ID,
                        p.CFAccount
                    }
            });

            return Content(o["item"].ToString());
        }

        [Route("getCF_DdownloadInTXT")]
        public ActionResult getCF_DdownloadInTXT(string data)
        {

            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int Region = Convert.ToInt32(objParameter["Region"].ToString());
            int Quater = Convert.ToInt32(objParameter["Quater"].ToString());

            tblCF_M_MonthYearQuater dates = (from p in DAO.tblCF_M_MonthYearQuater where p.ID == Quater select p).SingleOrDefault<tblCF_M_MonthYearQuater>();
            tblCF_R_Region tblRegion = (from p in DAO.tblCF_R_Region where p.ID == Region select p).SingleOrDefault<tblCF_R_Region>();


            JObject Detail = null;
            Detail = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_Master_Detail
                    join r in DAO.tblCF_R_Region on p.Region equals r.ID
                    join rc in DAO.tblCF_R_RegionCountry on r.ID equals rc.RegionID
                    from c in DAO.tblCF_CountryBU
                    where rc.CountryID == c.CountryID && p.BU == c.BU
                    join country in DAO.tblCF_R_Country on c.CountryID equals country.ID
                    where p.Region == Region && p.Quater == dates.Quater && p.Year == dates.Year && p.tblCF_CF_Account.Enable == true
                    from master in DAO.tblCF_Master_DetailManual
                    where p.Region == master.Region && p.BU == master.BU && p.CFAccountID == master.CFAccountID && p.Quater == master.Quater && p.Year == master.Year
                    select new
                    {
                        p.BU,
                        p.tblCF_CF_Account.CFAccount,
                        p.tblCF_CF_Account.Description,
                        //p.tblCF_CF_Account.tblCF_CF_TypeAccount.Name,
                        //typeID = p.tblCF_CF_Account.tblCF_CF_TypeAccount.ID,
                        //p.Quater,
                        //p.Year,
                        //p.M1,
                        //p.M2,
                        //p.M3,
                        //p.PastQuater,
                        CurrentQuater = p.CurrentQuater + master.CurrentQuater,
                        // p.BU,
               
                    }
            });

            return Content(Detail["item"].ToString());

            //DataTable second = (DataTable)JsonConvert.DeserializeObject(Detail["item"].ToString().ToString(), (typeof(DataTable)));


            ////QMemo.Utils.Excel.Download(this, second, "Filter Report");
            //string filename = "Cable.txt";
            //// Exporting Data to text file
            //ExportDataTabletoFile(second, "\t", false, Server.MapPath("~/Content/CF/Files/Cable.txt"));
            //#region download notepad or text file.
            //Response.ContentType = "application/octet-stream";
            //Response.AppendHeader("Content-Disposition", "attachment;filename=" + filename);
            //string aaa = Server.MapPath("~/Files/" + filename);
            //Response.TransmitFile(Server.MapPath("~/Content/CF/Files/" + filename));
                
            //HttpContext.ApplicationInstance.CompleteRequest();
            //Response.End();


            //return null;

        }


        public void ExportDataTabletoFile(DataTable datatable, string delimited, bool exportcolumnsheader, string file)
        {
            StreamWriter str = new StreamWriter(file, false, System.Text.Encoding.Default);
            if (exportcolumnsheader)
            {
                string Columns = string.Empty;
                foreach (DataColumn column in datatable.Columns)
                {
                    Columns += column.ColumnName + delimited;
                }
                str.WriteLine(Columns.Remove(Columns.Length - 1, 1));
            }
            foreach (DataRow datarow in datatable.Rows)
            {
                string row = string.Empty;

                foreach (object items in datarow.ItemArray)
                {

                    row += items.ToString() + delimited;
                }
                str.WriteLine(row.Remove(row.Length - 1, 1));

            }
            str.Flush();
            str.Close();

        }



    }
}

