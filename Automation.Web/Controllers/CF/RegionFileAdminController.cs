﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using Automation.Web.Models.CashFlow;
using Automation.Core.Class;

namespace Automation.Web.Controllers.CF
{
    [RoutePrefix("CF")]
    public class RegionFileAdminController : Controller
    {
        [Route("RegionFileAdmin")]
        public ActionResult Index()
        {
            return View("~/Views/CF/RegionFileAdmin.cshtml");
        }

        [Route("getRegions")]
        public ActionResult getRegion(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            //string RoleID = objParameter["RoleID"].ToString();
            string userSOEID = GlobalUtil.GetSOEID();

            List<string> query = (from u in DAO.tblAFrwkUserXRoles join r in DAO.tblAFrwkRoles on u.IDRole equals r.ID where u.SOEID == userSOEID select r.Name).ToList<string>();

            List<tblCF_R_Region> RegionsList = new List<tblCF_R_Region>();

            foreach (string p in query)
            {
                tblCF_R_Region Region ;
                if (p == "CF_NAM")
                {
                    Region = (from d in DAO.tblCF_R_Region where d.Region == "NAM" select d).SingleOrDefault<tblCF_R_Region>();
                    RegionsList.Add(Region);

                }
                if (p == "CF_LATAM")
                {
                    Region = (from d in DAO.tblCF_R_Region where d.Region == "LATAM" select d).SingleOrDefault<tblCF_R_Region>();
                    RegionsList.Add(Region);
                }
                if (p == "CF_Mexico")
                {
                    Region = (from d in DAO.tblCF_R_Region where d.Region == "Mexico" select d).SingleOrDefault<tblCF_R_Region>();
                    RegionsList.Add(Region);
                }
                if (p == "CF_EMEA")
                {
                    Region = (from d in DAO.tblCF_R_Region where d.Region == "EMEA" select d).SingleOrDefault<tblCF_R_Region>();
                    RegionsList.Add(Region);
                }
                if (p == "CF_APAC")
                {
                    Region = (from d in DAO.tblCF_R_Region where d.Region == "APAC" select d).SingleOrDefault<tblCF_R_Region>();
                    RegionsList.Add(Region);
                }
                if (p == "CF_9LP")
                {
                    Region = (from d in DAO.tblCF_R_Region where d.Region == "9LP" select d).SingleOrDefault<tblCF_R_Region>();
                    RegionsList.Add(Region);
                }
                if (p == "CF_ADMIN")
                {
                    Region = (from d in DAO.tblCF_R_Region where d.Region == "Admin Region" select d).SingleOrDefault<tblCF_R_Region>();
                    RegionsList.Add(Region);
                    Region = (from d in DAO.tblCF_R_Region where d.Region == "EditCheck" select d).SingleOrDefault<tblCF_R_Region>();
                    RegionsList.Add(Region);

                }

            }
           


            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in RegionsList
                    select new 
                    {
                        p.ID,
                        p.Region
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("addRegion")]
        public ActionResult addRegion(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            string Region = objParameter["Region"].ToString();

            tblCF_R_Region newRegion = new tblCF_R_Region();

            newRegion.Region = Region;

            DAO.tblCF_R_Region.Add(newRegion);

            DAO.SaveChanges();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_R_Region
                    select new
                    {
                        p.ID,
                        p.Region
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("deleteRegions")]
        public ActionResult deleteRegions(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int Region = Convert.ToInt32(objParameter["Region"].ToString());

            tblCF_R_Region newRegion = (from p in DAO.tblCF_R_Region where p.ID == Region select p).SingleOrDefault<tblCF_R_Region>();

            DAO.tblCF_R_Region.Remove(newRegion);

            DAO.SaveChanges();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_R_Region
                    select new
                    {
                        p.ID,
                        p.Region
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("getCountry")]
        public ActionResult getCountry(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int Region = Convert.ToInt32(objParameter["Region"].ToString());

            tblCF_R_Region RegionSelected = (from p in DAO.tblCF_R_Region where p.ID == Region select p).SingleOrDefault<tblCF_R_Region>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_R_Country
                    join p2 in DAO.tblCF_R_RegionCountry on p.ID equals p2.CountryID
                    where p2.RegionID == RegionSelected.ID
                    orderby p.Country
                    select new
                    {
                        p.ID,
                        p.Country
                    }
            });


            return Content(o["item"].ToString());
        }


        [Route("getCountry")]
        public ActionResult getBUbyRegion(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int Region = Convert.ToInt32(objParameter["Region"].ToString());

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_CountryBU
                    join p2 in DAO.tblCF_R_Country on p.CountryID  equals p2.ID
                    join p3 in DAO.tblCF_R_RegionCountry on p2.ID equals p3.CountryID
                    where p3.RegionID == Region
                    select new
                    {
                        p.BU
                    }
            });


            return Content(o["item"].ToString());
        }


        [Route("addCountry")]
        public ActionResult addCountry(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            string Country = objParameter["Country"].ToString();
            int Region = Convert.ToInt32(objParameter["Region"].ToString());

            tblCF_R_Country newCountry = new tblCF_R_Country();
            tblCF_R_Region RegionSelected = (from p in DAO.tblCF_R_Region where p.ID == Region select p).SingleOrDefault<tblCF_R_Region>();

            newCountry.Country = Country;

            DAO.tblCF_R_Country.Add(newCountry);
            DAO.SaveChanges();

            tblCF_R_Country CountrySelected = (from p in DAO.tblCF_R_Country where p.Country == Country select p).SingleOrDefault<tblCF_R_Country>();

            tblCF_R_RegionCountry newCR = new tblCF_R_RegionCountry();

            newCR.RegionID = RegionSelected.ID;
            newCR.CountryID = CountrySelected.ID;

            DAO.tblCF_R_RegionCountry.Add(newCR);

            DAO.SaveChanges();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_R_Country
                    join p2 in DAO.tblCF_R_RegionCountry on p.ID equals p2.CountryID
                    where p2.RegionID == RegionSelected.ID
                    orderby p.Country
                    select new
                    {
                        p.ID,
                        p.Country
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("deleteCountry")]
        public ActionResult deleteCountry(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            string Country = objParameter["Country"].ToString();
            int Region = Convert.ToInt32(objParameter["Region"].ToString());

            tblCF_R_Country newCountry = (from p in DAO.tblCF_R_Country where p.Country == Country select p).SingleOrDefault<tblCF_R_Country>();
            tblCF_R_Region RegionSelected = (from p in DAO.tblCF_R_Region where p.ID == Region select p).SingleOrDefault<tblCF_R_Region>();

            tblCF_R_RegionCountry RegionCountry = (from p in DAO.tblCF_R_RegionCountry where p.RegionID == RegionSelected.ID && p.CountryID == newCountry.ID select p).SingleOrDefault<tblCF_R_RegionCountry>();

            DAO.tblCF_R_RegionCountry.Remove(RegionCountry);
            DAO.tblCF_R_Country.Remove(newCountry);

            DAO.SaveChanges();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_R_Country
                    join p2 in DAO.tblCF_R_RegionCountry on p.ID equals p2.CountryID
                    where p2.RegionID == RegionSelected.ID
                    orderby p.Country
                    select new
                    {
                        p.ID,
                        p.Country
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("getCountryBU")]
        public ActionResult getCountryBU(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            string Country = objParameter["Country"].ToString();
              
            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_CountryBU
                    where p.tblCF_R_Country.Country == Country
                    orderby p.BU
                    select new
                    {
                        p.BU
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("addCountryBU")]
        public ActionResult addCountryBU(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            string Country = objParameter["Country"].ToString();
            int BU = Convert.ToInt32(objParameter["BU"].ToString());
            int Region = Convert.ToInt32(objParameter["Region"].ToString());


            List<tblCF_CountryBU> EXIST = (from p in DAO.tblCF_CountryBU where p.tblCF_R_Country.Country == Country && p.BU == BU select p).ToList<tblCF_CountryBU>();
           
            if(EXIST.Count() == 0) { 
                tblCF_R_Country newCountry = (from p in DAO.tblCF_R_Country join r in DAO.tblCF_R_RegionCountry on p.ID equals r.CountryID where p.Country == Country  && r.RegionID == Region select p).SingleOrDefault<tblCF_R_Country>();

                tblCF_CountryBU newCR = new tblCF_CountryBU();

                newCR.CountryID = newCountry.ID;
                newCR.BU = BU;

                DAO.tblCF_CountryBU.Add(newCR);

                DAO.SaveChanges();
            }
            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_CountryBU
                    where p.tblCF_R_Country.Country == Country
                    orderby p.BU
                    select new
                    {
                        p.BU
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("deleteCountryBU")]
        public ActionResult deleteCountryBU(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            string Country = objParameter["Country"].ToString();
            int BU = Convert.ToInt32(objParameter["BU"].ToString());

            tblCF_CountryBU newCR = (from p in DAO.tblCF_CountryBU where p.tblCF_R_Country.Country == Country && p.BU == BU select p ).SingleOrDefault<tblCF_CountryBU>();

            DAO.tblCF_CountryBU.Remove(newCR);

            DAO.SaveChanges();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_CountryBU
                    where p.tblCF_R_Country.Country == Country
                    orderby p.BU
                    select new
                    {
                        p.BU
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("getFile")]
        public ActionResult getFile(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int Region = Convert.ToInt32(objParameter["Region"].ToString());

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_M_File join c in DAO.tblCF_M_FileRegion on p.ID equals c.FileID
                    where c.tblCF_R_Region.ID == Region
                    orderby p.Name
                    select new
                    {
                        p.ID,
                        p.Name,
                        c.AskCountry,
                        c.AskBU,
                        c.AskMonthYear,
                        c.AskQuater,
                        p.IsCreated,
                        p.NeedUpdate
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("addFile")]
        public ActionResult addFile(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int Region = Convert.ToInt32(objParameter["Region"].ToString());

            string Name = objParameter["Name"].ToString();
            bool UseCountry = Convert.ToBoolean(objParameter["UseCountry"].ToString());
            bool UseBU = Convert.ToBoolean(objParameter["UseBU"].ToString());
            bool UseMonthYear = Convert.ToBoolean(objParameter["UseMonthYear"].ToString());
            bool UseQuater = Convert.ToBoolean(objParameter["UseQuater"].ToString());

            tblCF_R_Region modelRegion = (from p in DAO.tblCF_R_Region where p.ID == Region select p).SingleOrDefault<tblCF_R_Region>();

            tblCF_M_File modelFile = new tblCF_M_File();

            modelFile.Name = Name;
            modelFile.tblName = "tblCF_M_" + Name;
            modelFile.vwName = "vwCF_M_" + Name;
            modelFile.IsCreated = false;
            modelFile.NeedUpdate = false;

            DAO.tblCF_M_File.Add(modelFile);

            DAO.SaveChanges();

            modelFile.tblName = "tblCF_M_" + modelFile.ID;
            modelFile.vwName = "vwCF_M_" + modelFile.ID;
            DAO.SaveChanges();

            tblCF_M_FileRegion modelFileRegion = new tblCF_M_FileRegion();

            modelFileRegion.FileID = modelFile.ID;
            modelFileRegion.RegionID = modelRegion.ID;
            modelFileRegion.AskBU = UseBU;
            modelFileRegion.AskCountry = UseCountry;
            modelFileRegion.AskMonthYear = UseMonthYear;
            modelFileRegion.AskQuater = UseQuater;

            DAO.tblCF_M_FileRegion.Add(modelFileRegion);

            DAO.SaveChanges();


            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_M_File
                    join c in DAO.tblCF_M_FileRegion on p.ID equals c.FileID
                    where c.tblCF_R_Region.ID == Region
                    orderby p.Name
                    select new
                    {
                        p.ID,
                        p.Name,
                        c.AskCountry,
                        c.AskBU,
                        c.AskMonthYear,
                        c.AskQuater,
                        p.IsCreated,
                        p.NeedUpdate
                    }
            });



            return Content(o["item"].ToString());
        }

        [Route("deleteFile")]
        public ActionResult deleteFile(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["ID"].ToString());

            tblCF_M_File modelFile = (from p in DAO.tblCF_M_File where p.ID == ID select p).SingleOrDefault<tblCF_M_File>();

            tblCF_M_FileRegion modelFileRegion = (from p in DAO.tblCF_M_FileRegion where p.FileID == modelFile.ID select p).SingleOrDefault<tblCF_M_FileRegion>();

            List<tblCF_M_FileColumn> coulumns = (from p in DAO.tblCF_M_FileColumn where p.FileID == modelFile.ID select p).ToList<tblCF_M_FileColumn>();

            foreach(tblCF_M_FileColumn col in coulumns)
            {
                DAO.tblCF_M_FileColumn.Remove(col);
            }
            DAO.SaveChanges();

            string Region = modelFileRegion.tblCF_R_Region.Region;

            DAO.tblCF_M_FileRegion.Remove(modelFileRegion);

            DAO.SaveChanges();

            DAO.tblCF_M_File.Remove(modelFile);

            DAO.SaveChanges();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_M_File
                    join c in DAO.tblCF_M_FileRegion on p.ID equals c.FileID
                    where c.tblCF_R_Region.Region == Region
                    orderby p.Name
                    select new
                    {
                        p.ID,
                        p.Name,
                        c.AskCountry,
                        c.AskBU,
                        c.AskMonthYear,
                        c.AskQuater,
                        p.NeedUpdate
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("updateFile")]
        public ActionResult updateFile(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["ID"].ToString());

            bool BU = Convert.ToBoolean(objParameter["BU"].ToString());
            bool Country = Convert.ToBoolean(objParameter["Country"].ToString());
            bool MonthYear = Convert.ToBoolean(objParameter["MonthYear"].ToString());
            bool Quater = Convert.ToBoolean(objParameter["Quater"].ToString());

            tblCF_M_File modelFile = (from p in DAO.tblCF_M_File where p.ID == ID select p).SingleOrDefault<tblCF_M_File>();

            tblCF_M_FileRegion modelFileRegion = (from p in DAO.tblCF_M_FileRegion where p.FileID == modelFile.ID select p).SingleOrDefault<tblCF_M_FileRegion>();

            string Region = modelFileRegion.tblCF_R_Region.Region;

            modelFileRegion.AskBU = BU;
            modelFileRegion.AskCountry = Country;
            modelFileRegion.AskMonthYear = MonthYear;
            modelFileRegion.AskQuater = Quater;

            DAO.SaveChanges();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_M_File
                    join c in DAO.tblCF_M_FileRegion on p.ID equals c.FileID
                    where c.tblCF_R_Region.Region == Region
                    orderby p.Name
                    select new
                    {
                        p.ID,
                        p.Name,
                        c.AskCountry,
                        c.AskBU,
                        c.AskMonthYear,
                        c.AskQuater,
                        p.IsCreated
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("getColumnType")]
        public ActionResult getColumnType(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

          

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_M_TypeColumn
                    orderby p.ID
                    select new
                    {
                        p.ID,
                        p.Name
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("getColumnOrder")]
        public ActionResult getColumnOrder(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["ID"].ToString());

            List<tblCF_M_ColumnOrder> colSelected = (from p in DAO.tblCF_M_FileColumn
                                                     join c in DAO.tblCF_M_ColumnOrder on p.ColumnOrderID equals c.ID
                                                     where p.FileID == ID
                                                     select c).ToList<tblCF_M_ColumnOrder>();

            List<tblCF_M_ColumnOrder> col = (from c in DAO.tblCF_M_ColumnOrder
                                                     select c).ToList<tblCF_M_ColumnOrder>();


            col.RemoveAll(a => colSelected.Exists(b => a.ID == b.ID));


            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in col.AsEnumerable<tblCF_M_ColumnOrder>()
                    select new
                    {
                        p.ID,
                        p.ColOrder,
                        p.NameTemp
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("addColumnFile")]
        public ActionResult addColumnFile(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["IDFile"].ToString());

            tblCF_M_File File = (from p in DAO.tblCF_M_File where p.ID == ID select p).SingleOrDefault<tblCF_M_File>();
            File.NeedUpdate = true;

            string ColumnName = objParameter["ColumnName"].ToString();
            int Type = Convert.ToInt32(objParameter["Type"].ToString());
            int Order = Convert.ToInt32(objParameter["Order"].ToString());
            bool IsTable = Convert.ToBoolean(objParameter["IsTable"].ToString());
            string IsTableName = objParameter["IsTableName"].ToString();

            tblCF_M_FileColumn newCol = new tblCF_M_FileColumn();

            newCol.FileID = ID;
            newCol.ColumnName = ColumnName;
            newCol.ColumnNameInside = "column_" + ID.ToString() ;
            newCol.Type = Type;
            newCol.ColumnOrderID = Order;
            newCol.IsTable = IsTable;
            newCol.IsTableName = IsTableName;
            newCol.IsTableNameInside = "CF_TI_" + ID.ToString();

            DAO.tblCF_M_FileColumn.Add(newCol);

            DAO.SaveChanges();




            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_M_FileColumn
                    where p.FileID == ID
                    select new
                    {
                        p.ID,
                        p.ColumnName,
                        p.IsTable,
                        p.IsTableName,
                        Order = p.tblCF_M_ColumnOrder.NameTemp,
                        ColumnType = p.tblCF_M_TypeColumn.Name
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("updateColumnFile")]
        public ActionResult updateColumnFile(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int IDColumn = Convert.ToInt32(objParameter["IDColumn"].ToString());

            int ID = Convert.ToInt32(objParameter["IDFile"].ToString());
            string ColumnName = objParameter["ColumnName"].ToString();
            int Type = Convert.ToInt32(objParameter["Type"].ToString());
            int Order = Convert.ToInt32(objParameter["Order"].ToString());
            bool IsTable = Convert.ToBoolean(objParameter["IsTable"].ToString());
            string IsTableName = objParameter["IsTableName"].ToString();

            tblCF_M_File File = (from p in DAO.tblCF_M_File where p.ID == ID select p).SingleOrDefault<tblCF_M_File>();
            File.NeedUpdate = true;

            tblCF_M_FileColumn newCol = (from p in DAO.tblCF_M_FileColumn where p.ID == IDColumn select p).SingleOrDefault<tblCF_M_FileColumn>();

            newCol.ColumnName = ColumnName;
            newCol.Type = Type;
            newCol.ColumnOrderID = Order;
            newCol.IsTable = IsTable;
            newCol.IsTableName = IsTableName;

            DAO.SaveChanges();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_M_FileColumn
                    where p.FileID == ID
                    select new
                    {
                        p.ID,
                        p.ColumnName,
                        p.IsTable,
                        p.IsTableName,
                        Order = p.tblCF_M_ColumnOrder.NameTemp,
                        ColumnType = p.tblCF_M_TypeColumn.Name
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("deleteColumnFile")]
        public ActionResult deleteColumnFile(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int IDColumn = Convert.ToInt32(objParameter["IDColumn"].ToString());

            int ID = Convert.ToInt32(objParameter["IDFile"].ToString());
            tblCF_M_File File = (from p in DAO.tblCF_M_File where p.ID == ID select p).SingleOrDefault<tblCF_M_File>();
            File.NeedUpdate = true;

            tblCF_M_FileColumn newCol = (from p in DAO.tblCF_M_FileColumn where p.ID == IDColumn select p).SingleOrDefault<tblCF_M_FileColumn>();

            DAO.tblCF_M_FileColumn.Remove(newCol);

            DAO.SaveChanges();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_M_FileColumn
                    where p.FileID == ID
                    select new
                    {
                        p.ID,
                        p.ColumnName,
                        p.IsTable,
                        p.IsTableName,
                        Order = p.tblCF_M_ColumnOrder.NameTemp,
                        ColumnType = p.tblCF_M_TypeColumn.Name
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("getColumnFile")]
        public ActionResult getColumnFile(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["IDFile"].ToString());

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_M_FileColumn
                    where p.FileID == ID
                    select new
                    {
                        p.ID,
                        p.ColumnName,
                        p.IsTable,
                        p.IsTableName,
                        Order = p.tblCF_M_ColumnOrder.NameTemp,
                        ColumnType = p.tblCF_M_TypeColumn.Name
                    }
            });


            return Content(o["item"].ToString());
        }


        [Route("getColumnFile")]
        public ActionResult getAllColumnsOrder(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_M_ColumnOrder
                    select new
                    {
                        Order = p.NameTemp
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("createTableDynamic")]
        public ActionResult createTableDynamic(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int Region = Convert.ToInt32(objParameter["Region"].ToString());
            int FileID = Convert.ToInt32(objParameter["FileID"].ToString());
            string SOEID = objParameter["SOEID"].ToString();

            DAO.procCF_CreateDynamicTable(FileID, SOEID);

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_M_File
                    join c in DAO.tblCF_M_FileRegion on p.ID equals c.FileID
                    where c.tblCF_R_Region.ID == Region
                    orderby p.Name
                    select new
                    {
                        p.ID,
                        p.Name,
                        c.AskCountry,
                        c.AskBU,
                        c.AskMonthYear,
                        c.AskQuater,
                        p.IsCreated,
                        p.NeedUpdate
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("updateTableDynamic")]
        public ActionResult updateTableDynamic(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int Region = Convert.ToInt32(objParameter["Region"].ToString());
            int FileID = Convert.ToInt32(objParameter["FileID"].ToString());
            string SOEID = objParameter["SOEID"].ToString();

            DAO.procCF_UpdateDynamicTable(FileID, SOEID);

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_M_File
                    join c in DAO.tblCF_M_FileRegion on p.ID equals c.FileID
                    where c.tblCF_R_Region.ID == Region
                    orderby p.Name
                    select new
                    {
                        p.ID,
                        p.Name,
                        c.AskCountry,
                        c.AskBU,
                        c.AskMonthYear,
                        c.AskQuater,
                        p.IsCreated,
                        p.NeedUpdate
                    }
            });


            return Content(o["item"].ToString());
        }


        [Route("getCF_AllFileColumn")]
        public ActionResult getCF_AllFileColumn(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int FileID = Convert.ToInt32(objParameter["FileID"].ToString());


            tblCF_M_FileRegion fileregion = (from p in DAO.tblCF_M_FileRegion where p.FileID == FileID select p).SingleOrDefault<tblCF_M_FileRegion>();

            JObject o = null;
            tblCF_M_File adminMonth = (from p in DAO.tblCF_M_File where p.Name == "Default Month" select p).SingleOrDefault<tblCF_M_File>();
            tblCF_M_File adminQuater = (from p in DAO.tblCF_M_File where p.Name == "Default Quater" select p).SingleOrDefault<tblCF_M_File>();
            tblCF_M_File adminBU = (from p in DAO.tblCF_M_File where p.Name == "DefaultBU" select p).SingleOrDefault<tblCF_M_File>();
            tblCF_M_File adminCountry = (from p in DAO.tblCF_M_File where p.Name == "DefaultCountry" select p).SingleOrDefault<tblCF_M_File>();


            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_M_FileColumn
                    where p.FileID == FileID || p.FileID == adminMonth.ID || p.FileID == adminQuater.ID || p.FileID == adminBU.ID || p.FileID == adminCountry.ID
                    select new
                    {
                        p.ID,
                        p.ColumnName
                    }
            }); ;

            return Content(o["item"].ToString());
        }

        [Route("getCF_EditCheckFileSelected")]
        public ActionResult getCF_EditCheckFileSelected(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int RegionID = Convert.ToInt32(objParameter["RegionID"].ToString());
            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_EditCheckFile
                    where p.RegionID == RegionID
                    select new
                    {
                        p.ID,
                        p.FileID,
                        p.colFDL,
                        p.colDescription,
                        p.colIceCode,
                        p.colMonth,
                        p.colValue
                    }
            }); ;

            return Content(o["item"].ToString());
        }


        [Route("getCF_EditCheckFileSelected")]
        public ActionResult saveCF_EditCheckFileSelected(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int RegionID = Convert.ToInt32(objParameter["RegionID"].ToString());
            int FileID = Convert.ToInt32(objParameter["FileID"].ToString());
            int colFDL = Convert.ToInt32(objParameter["colFDL"].ToString());
            int colDescription = Convert.ToInt32(objParameter["colDescription"].ToString());
            int colIceCode = Convert.ToInt32(objParameter["colIceCode"].ToString());
            int colMonth = Convert.ToInt32(objParameter["colMonth"].ToString());
            int colValue = Convert.ToInt32(objParameter["colValue"].ToString());


            List<tblCF_EditCheckFile> ecLog = (from p in DAO.tblCF_EditCheckFile where p.RegionID == RegionID select p).ToList<tblCF_EditCheckFile>();

            if (ecLog.Count() == 0)
            {
                tblCF_EditCheckFile New = new tblCF_EditCheckFile();

                New.RegionID = RegionID;
                New.FileID = FileID;
                New.colFDL = colFDL;
                New.colDescription = colDescription;
                New.colIceCode = colIceCode;
                New.colMonth = colMonth;
                New.colValue = colValue;
                DAO.tblCF_EditCheckFile.Add(New);
                DAO.SaveChanges();
            }
            else {
                
                ecLog[0].RegionID = RegionID;
                ecLog[0].FileID = FileID;
                ecLog[0].colFDL = colFDL;
                ecLog[0].colDescription = colDescription;
                ecLog[0].colIceCode = colIceCode;
                ecLog[0].colMonth = colMonth;
                ecLog[0].colValue = colValue;
                DAO.SaveChanges();
            }


            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_EditCheckFile
                    where p.RegionID == RegionID
                    select new
                    {
                        p.ID,
                        p.FileID,
                        p.colFDL,
                        p.colDescription,
                        p.colIceCode,
                        p.colMonth,
                        p.colValue
                    }
            }); ;

            return Content(o["item"].ToString());
        }
    }
}