﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using Automation.Web.Models.CashFlow;
using System.Globalization;
using Automation.Core.Model;

namespace Automation.Web.Controllers.CF
{
    [RoutePrefix("CF")]
    public class UploadFileController : Controller
    {

        [Route("UploadFile")]
        public ActionResult Index()
        {
            return View("~/Views/CF/UploadFile.cshtml");
        }

        public ActionResult Upload(FormCollection formCollection)
        {
            if (Request != null)
            {
                HttpPostedFileBase file = Request.Files["UploadedFile"];

                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                }
            }

            return null;
        }



        [Route("getQuater")]
        public ActionResult getQuater(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            DateTime _date = DateTime.Now;
            int Month = _date.Month;
            int Year = _date.Year;

            int Quater = 0;
            if (Month > 0 && Month < 4)
            {
                Quater = 1;
            }
            if (Month > 3 && Month < 7)
            {
                Quater = 2;
            }
            if (Month > 6 && Month < 10)
            {
                Quater = 3;
            }
            if (Month > 8 && Month < 13)
            {
                Quater = 4;
            }

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int count = (from p in DAO.tblCF_M_MonthYearQuater where p.Month == Month && p.Year == Year && p.Quater == Quater select p).ToList<tblCF_M_MonthYearQuater>().Count();

            if(count == 0)
            {
                tblCF_M_MonthYearQuater newDate = new tblCF_M_MonthYearQuater();

                newDate.Month = Month;
                newDate.Quater = Quater;
                newDate.Year = Year;
                newDate.MonthYear = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Month)  + " " + Year;

                DAO.tblCF_M_MonthYearQuater.Add(newDate);

                DAO.SaveChanges();

            }

            List<tblCF_M_MonthYearQuater> quater = (from p in DAO.tblCF_M_MonthYearQuater select p).ToList<tblCF_M_MonthYearQuater>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                   (from p in DAO.tblCF_M_MonthYearQuater
                    where p.Month == 3 || p.Month == 6 || p.Month == 9 || p.Month == 12 
                    select new
                    {
                        p.ID,
                       Year = p.Year,
                       Quater = p.Quater
                    })
            });


            return Content(o["item"].ToString());
        }

        [Route("getMonthYear")]
        public ActionResult getMonthYear(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            DateTime _date = DateTime.Now;
            int Month = _date.Month;
            int Year = _date.Year;

            int Quater = 0;
            if (Month > 0 && Month < 4)
            {
                Quater = 1;
            }
            if (Month > 3 && Month < 7)
            {
                Quater = 2;
            }
            if (Month > 6 && Month < 10)
            {
                Quater = 3;
            }
            if (Month > 8 && Month < 13)
            {
                Quater = 4;
            }

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int count = (from p in DAO.tblCF_M_MonthYearQuater where p.Month == Month && p.Year == Year && p.Quater == Quater select p).ToList<tblCF_M_MonthYearQuater>().Count();

            if (count == 0)
            {
                tblCF_M_MonthYearQuater newDate = new tblCF_M_MonthYearQuater();

                newDate.Month = Month;
                newDate.Quater = Quater;
                newDate.Year = Year;
                newDate.MonthYear = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Month) + " " + Year;

                DAO.tblCF_M_MonthYearQuater.Add(newDate);

                DAO.SaveChanges();

            }


            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_M_MonthYearQuater
                    orderby p.ID
                    select new
                    {
                        MonthYear = p.MonthYear
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("getBUfromFile")]
        public ActionResult getBUfromFile(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["IDFile"].ToString());

            tblCF_M_File file = (from p in DAO.tblCF_M_File where p.ID == ID select p).SingleOrDefault<tblCF_M_File>();
            
            
            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_CountryBU join c in DAO.tblCF_R_Country on p.CountryID equals c.ID
                    join r in DAO.tblCF_R_RegionCountry on c.ID equals r.CountryID
                    where r.RegionID == file.tblCF_M_FileRegion.RegionID
                    orderby p.BU
                    select new
                    {
                        BU = p.BU
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("getCountryfromFile")]
        public ActionResult getCountryfromFile(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["IDFile"].ToString());

            tblCF_M_File file = (from p in DAO.tblCF_M_File where p.ID == ID select p).SingleOrDefault<tblCF_M_File>();


            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from c in DAO.tblCF_R_Country
                    join r in DAO.tblCF_R_RegionCountry on c.ID equals r.CountryID
                    where r.RegionID == file.tblCF_M_FileRegion.RegionID
                    orderby c.Country
                    select new
                    {
                        Country = c.Country
                    }
            });


            return Content(o["item"].ToString());
        }

        [ValidateInput(false)]
        [Route("uploadFile")]
        public ActionResult uploadFile(string data)
        {
           var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

           var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

           DataTable ptable = (DataTable)JsonConvert.DeserializeObject(objParameter["data"], (typeof(DataTable)));

           string pCountrySelected = objParameter["pCountrySelected"].ToString();
           string pMonthYearSelected = objParameter["pMonthYearSelected"].ToString();
           string pQuaterSelected = objParameter["pQuaterSelected"].ToString();
           string pBUSelected = objParameter["pBUSelected"].ToString();
           string SOEID = objParameter["SOEID"].ToString();
           int FileID = Convert.ToInt32(objParameter["FileID"].ToString());
           string Region = objParameter["Region"].ToString();

            CashFlowEntities DAO = new CashFlowEntities();

            tblCF_M_File File = (from p in DAO.tblCF_M_File where p.ID == FileID select p).SingleOrDefault<tblCF_M_File>();

            List<tblCF_M_FileColumn> Columns = File.tblCF_M_FileColumn.ToList<tblCF_M_FileColumn>().ToList<tblCF_M_FileColumn>();

            DataTable PrincipalTable = (DataTable)JsonConvert.DeserializeObject(objParameter["data"], (typeof(DataTable)));

            DataTable UDT_5Columns;
            DataTable UDT_10Columns;
            DataTable UDT_20Columns;
            DataTable UDT_30Columns;
            DataTable UDT_40Columns;
            DataTable UDT_50Columns;
            DataTable UDT_60Columns;
            DataTable UDT_70Columns;
            DataTable UDT_80Columns;
            DataTable UDT_90Columns;
            DataTable UDT_100Columns;
            DataTable UDT_110Columns;
            DataTable UDT_120Columns;
            DataTable UDT_130Columns;
            DataTable UDT_140Columns;
            DataTable UDT_150Columns;
            DataTable UDT_160Columns;
            DataTable UDT_170Columns;
            DataTable UDT_180Columns;
            DataTable UDT_190Columns;
            DataTable UDT_200Columns;

            int TotalColumns = PrincipalTable.Columns.Count;
            //Lookup for columns

            //foreach(tblCF_M_FileColumn col in Columns)
            //{
            //    try
            //    {
            //        PrincipalTable.Columns[col.ColumnName.Trim()].SetOrdinal(col.tblCF_M_ColumnOrder.ColOrder - 1);
            //    }
            //    catch
            //    {
            //        PrincipalTable.Columns.Add(col.ColumnName.Trim()).SetOrdinal(col.tblCF_M_ColumnOrder.ColOrder - 1);
            //    }
            //}


            int columnsDT = PrincipalTable.Columns.Count;
            int var = 1;

            while (var == 1)
            {
                switch (columnsDT)
                {
                    case 5:
                    case 10:
                    case 20:
                    case 30:
                    case 40:
                    case 50:
                    case 60:
                    case 70:
                    case 80:
                    case 90:
                    case 100:
                    case 110:
                    case 120:
                    case 130:
                    case 140:
                    case 150:
                    case 160:
                    case 170:
                    case 180:
                    case 190:
                    case 200:
                        var = 0;
                        break;
                    default:
                        PrincipalTable.Columns.Add("add" + columnsDT.ToString() );
                        columnsDT++;
                        var = 1;
                        break;
                }
            }



        DataTable table = new DataTable();
   
            table.Columns.Add("[A]", typeof(int));
            table.Columns.Add("[B]", typeof(int));
            table.Columns.Add("[C]", typeof(int));
            table.Columns.Add("[D]", typeof(int));
            table.Columns.Add("[E]", typeof(int));

            if (TotalColumns <= 5) { UDT_5Columns = PrincipalTable; } else {  UDT_5Columns = table.Clone(); ; };
            table.Columns.Add("[F]", typeof(int));
            table.Columns.Add("[G]", typeof(int));
            table.Columns.Add("[H]", typeof(int));
            table.Columns.Add("[I]", typeof(int));
            table.Columns.Add("[J]", typeof(int));
            if (TotalColumns > 5  && TotalColumns <= 10  )  {  UDT_10Columns = PrincipalTable; } else {  UDT_10Columns  = table.Clone(); ; };
            table.Columns.Add("[K]", typeof(int));
            table.Columns.Add("[L]", typeof(int));
            table.Columns.Add("[M]", typeof(int));
            table.Columns.Add("[N]", typeof(int));
            table.Columns.Add("[O]", typeof(int));
            table.Columns.Add("[P]", typeof(int));
            table.Columns.Add("[Q]", typeof(int));
            table.Columns.Add("[R]", typeof(int));
            table.Columns.Add("[S]", typeof(int));
            table.Columns.Add("[T]", typeof(int));
            if (TotalColumns > 10  && TotalColumns <= 20  ) { UDT_20Columns = PrincipalTable; } else {  UDT_20Columns  = table.Clone(); ; };
            table.Columns.Add("[U]", typeof(int));
            table.Columns.Add("[V]", typeof(int));
            table.Columns.Add("[W]", typeof(int));
            table.Columns.Add("[X]", typeof(int));
            table.Columns.Add("[Y]", typeof(int));
            table.Columns.Add("[Z]", typeof(int));
            table.Columns.Add("[AA]", typeof(int));
            table.Columns.Add("[AB]", typeof(int));
            table.Columns.Add("[AC]", typeof(int));
            table.Columns.Add("[AD]", typeof(int));
            if (TotalColumns > 20  && TotalColumns <= 30  ) {  UDT_30Columns = PrincipalTable; } else {  UDT_30Columns  = table.Clone(); ; };
            table.Columns.Add("[AE]", typeof(int));
            table.Columns.Add("[AF]", typeof(int));
            table.Columns.Add("[AG]", typeof(int));
            table.Columns.Add("[AH]", typeof(int));
            table.Columns.Add("[AI]", typeof(int));
            table.Columns.Add("[AJ]", typeof(int));
            table.Columns.Add("[AK]", typeof(int));
            table.Columns.Add("[AL]", typeof(int));
            table.Columns.Add("[AM]", typeof(int));
            table.Columns.Add("[AN]", typeof(int));
            if (TotalColumns > 30  && TotalColumns <= 40  ) {  UDT_40Columns = PrincipalTable; } else {  UDT_40Columns  = table.Clone(); ; };
            table.Columns.Add("[AO]", typeof(int));
            table.Columns.Add("[AP]", typeof(int));
            table.Columns.Add("[AQ]", typeof(int));
            table.Columns.Add("[AR]", typeof(int));
            table.Columns.Add("[AS]", typeof(int));
            table.Columns.Add("[AT]", typeof(int));
            table.Columns.Add("[AU]", typeof(int));
            table.Columns.Add("[AV]", typeof(int));
            table.Columns.Add("[AW]", typeof(int));
            table.Columns.Add("[AX]", typeof(int));
            if (TotalColumns > 40  && TotalColumns <= 50  ) {  UDT_50Columns = PrincipalTable; } else {  UDT_50Columns  = table.Clone(); ; };
            table.Columns.Add("[AY]", typeof(int));
            table.Columns.Add("[AZ]", typeof(int));
            table.Columns.Add("[BA]", typeof(int));
            table.Columns.Add("[BB]", typeof(int));
            table.Columns.Add("[BC]", typeof(int));
            table.Columns.Add("[BD]", typeof(int));
            table.Columns.Add("[BE]", typeof(int));
            table.Columns.Add("[BF]", typeof(int));
            table.Columns.Add("[BG]", typeof(int));
            table.Columns.Add("[BH]", typeof(int));
            if (TotalColumns > 50  && TotalColumns <= 60  ) {  UDT_60Columns = PrincipalTable; } else {  UDT_60Columns  = table.Clone(); ; };
            table.Columns.Add("[BI]", typeof(int));
            table.Columns.Add("[BJ]", typeof(int));
            table.Columns.Add("[BK]", typeof(int));
            table.Columns.Add("[BL]", typeof(int));
            table.Columns.Add("[BM]", typeof(int));
            table.Columns.Add("[BN]", typeof(int));
            table.Columns.Add("[BO]", typeof(int));
            table.Columns.Add("[BP]", typeof(int));
            table.Columns.Add("[BQ]", typeof(int));
            table.Columns.Add("[BR]", typeof(int));
            if (TotalColumns > 60  && TotalColumns <= 70  ) {  UDT_70Columns = PrincipalTable; } else {  UDT_70Columns  = table.Clone(); ; };
            table.Columns.Add("[BS]", typeof(int));
            table.Columns.Add("[BT]", typeof(int));
            table.Columns.Add("[BU]", typeof(int));
            table.Columns.Add("[BV]", typeof(int));
            table.Columns.Add("[BW]", typeof(int));
            table.Columns.Add("[BX]", typeof(int));
            table.Columns.Add("[BY]", typeof(int));
            table.Columns.Add("[BZ]", typeof(int));
            table.Columns.Add("[CA]", typeof(int));
            table.Columns.Add("[CB]", typeof(int));
            if (TotalColumns > 70  && TotalColumns <= 80  ) {  UDT_80Columns = PrincipalTable; } else {  UDT_80Columns  = table.Clone(); ; };
            table.Columns.Add("[CC]", typeof(int));
            table.Columns.Add("[CD]", typeof(int));
            table.Columns.Add("[CE]", typeof(int));
            table.Columns.Add("[CF]", typeof(int));
            table.Columns.Add("[CG]", typeof(int));
            table.Columns.Add("[CH]", typeof(int));
            table.Columns.Add("[CI]", typeof(int));
            table.Columns.Add("[CJ]", typeof(int));
            table.Columns.Add("[CK]", typeof(int));
            table.Columns.Add("[CL]", typeof(int));
            if (TotalColumns > 80  && TotalColumns <= 90  ) {  UDT_90Columns = PrincipalTable; } else {  UDT_90Columns  = table.Clone(); ; };
            table.Columns.Add("[CM]", typeof(int));
            table.Columns.Add("[CN]", typeof(int));
            table.Columns.Add("[CO]", typeof(int));
            table.Columns.Add("[CP]", typeof(int));
            table.Columns.Add("[CQ]", typeof(int));
            table.Columns.Add("[CR]", typeof(int));
            table.Columns.Add("[CS]", typeof(int));
            table.Columns.Add("[CT]", typeof(int));
            table.Columns.Add("[CU]", typeof(int));
            table.Columns.Add("[CV]", typeof(int));
            if (TotalColumns > 90  && TotalColumns <= 100 ) {  UDT_100Columns = PrincipalTable;} else {  UDT_100Columns = table.Clone(); ; };
            table.Columns.Add("[CW]", typeof(int));
            table.Columns.Add("[CX]", typeof(int));
            table.Columns.Add("[CY]", typeof(int));
            table.Columns.Add("[CZ]", typeof(int));
            table.Columns.Add("[DA]", typeof(int));
            table.Columns.Add("[DB]", typeof(int));
            table.Columns.Add("[DC]", typeof(int));
            table.Columns.Add("[DD]", typeof(int));
            table.Columns.Add("[DE]", typeof(int));
            table.Columns.Add("[DF]", typeof(int));
            if (TotalColumns > 100 && TotalColumns <= 110 ) {  UDT_110Columns = PrincipalTable;} else {  UDT_110Columns = table.Clone(); ; };
            table.Columns.Add("[DG]", typeof(int));
            table.Columns.Add("[DH]", typeof(int));
            table.Columns.Add("[DI]", typeof(int));
            table.Columns.Add("[DJ]", typeof(int));
            table.Columns.Add("[DK]", typeof(int));
            table.Columns.Add("[DL]", typeof(int));
            table.Columns.Add("[DM]", typeof(int));
            table.Columns.Add("[DN]", typeof(int));
            table.Columns.Add("[DO]", typeof(int));
            table.Columns.Add("[DP]", typeof(int));
            if (TotalColumns > 110 && TotalColumns <= 120 ) {  UDT_120Columns = PrincipalTable;} else {  UDT_120Columns = table.Clone(); ; };
            table.Columns.Add("[DQ]", typeof(int));
            table.Columns.Add("[DR]", typeof(int));
            table.Columns.Add("[DS]", typeof(int));
            table.Columns.Add("[DT]", typeof(int));
            table.Columns.Add("[DU]", typeof(int));
            table.Columns.Add("[DV]", typeof(int));
            table.Columns.Add("[DW]", typeof(int));
            table.Columns.Add("[DX]", typeof(int));
            table.Columns.Add("[DY]", typeof(int));
            table.Columns.Add("[DZ]", typeof(int));
            if (TotalColumns > 120 && TotalColumns <= 130 ) {  UDT_130Columns = PrincipalTable;} else {  UDT_130Columns = table.Clone(); ; };
            table.Columns.Add("[EA]", typeof(int));
            table.Columns.Add("[EB]", typeof(int));
            table.Columns.Add("[EC]", typeof(int));
            table.Columns.Add("[ED]", typeof(int));
            table.Columns.Add("[EE]", typeof(int));
            table.Columns.Add("[EF]", typeof(int));
            table.Columns.Add("[EG]", typeof(int));
            table.Columns.Add("[EH]", typeof(int));
            table.Columns.Add("[EI]", typeof(int));
            table.Columns.Add("[EJ]", typeof(int));
            if (TotalColumns > 130 && TotalColumns <= 140 ) {  UDT_140Columns = PrincipalTable;} else {  UDT_140Columns = table.Clone(); ; };
            table.Columns.Add("[EK]", typeof(int));
            table.Columns.Add("[EL]", typeof(int));
            table.Columns.Add("[EM]", typeof(int));
            table.Columns.Add("[EN]", typeof(int));
            table.Columns.Add("[EO]", typeof(int));
            table.Columns.Add("[EP]", typeof(int));
            table.Columns.Add("[EQ]", typeof(int));
            table.Columns.Add("[ER]", typeof(int));
            table.Columns.Add("[ES]", typeof(int));
            table.Columns.Add("[ET]", typeof(int));
            if (TotalColumns > 140 && TotalColumns <= 150 ) {  UDT_150Columns = PrincipalTable;} else {  UDT_150Columns = table.Clone(); ; };
            table.Columns.Add("[EU]", typeof(int));
            table.Columns.Add("[EV]", typeof(int));
            table.Columns.Add("[EW]", typeof(int));
            table.Columns.Add("[EX]", typeof(int));
            table.Columns.Add("[EY]", typeof(int));
            table.Columns.Add("[EZ]", typeof(int));
            table.Columns.Add("[FA]", typeof(int));
            table.Columns.Add("[FB]", typeof(int));
            table.Columns.Add("[FC]", typeof(int));
            table.Columns.Add("[FD]", typeof(int));
            if (TotalColumns > 150 && TotalColumns <= 160 ) {  UDT_160Columns = PrincipalTable;} else {  UDT_160Columns = table.Clone(); ; };
            table.Columns.Add("[FE]", typeof(int));
            table.Columns.Add("[FF]", typeof(int));
            table.Columns.Add("[FG]", typeof(int));
            table.Columns.Add("[FH]", typeof(int));
            table.Columns.Add("[FI]", typeof(int));
            table.Columns.Add("[FJ]", typeof(int));
            table.Columns.Add("[FK]", typeof(int));
            table.Columns.Add("[FL]", typeof(int));
            table.Columns.Add("[FM]", typeof(int));
            table.Columns.Add("[FN]", typeof(int));
            if (TotalColumns > 160 && TotalColumns <= 170 ) {  UDT_170Columns = PrincipalTable;} else {  UDT_170Columns = table.Clone(); ; };
            table.Columns.Add("[FO]", typeof(int));
            table.Columns.Add("[FP]", typeof(int));
            table.Columns.Add("[FQ]", typeof(int));
            table.Columns.Add("[FR]", typeof(int));
            table.Columns.Add("[FS]", typeof(int));
            table.Columns.Add("[FT]", typeof(int));
            table.Columns.Add("[FU]", typeof(int));
            table.Columns.Add("[FV]", typeof(int));
            table.Columns.Add("[FW]", typeof(int));
            table.Columns.Add("[FX]", typeof(int));
            if (TotalColumns > 170 && TotalColumns <= 180 ) {  UDT_180Columns = PrincipalTable;} else {  UDT_180Columns = table.Clone(); ; };
            table.Columns.Add("[FY]", typeof(int));
            table.Columns.Add("[FZ]", typeof(int));
            table.Columns.Add("[GA]", typeof(int));
            table.Columns.Add("[GB]", typeof(int));
            table.Columns.Add("[GC]", typeof(int));
            table.Columns.Add("[GD]", typeof(int));
            table.Columns.Add("[GE]", typeof(int));
            table.Columns.Add("[GF]", typeof(int));
            table.Columns.Add("[GG]", typeof(int));
            table.Columns.Add("[GH]", typeof(int));
            if (TotalColumns > 180 && TotalColumns <= 190 ) {  UDT_190Columns = PrincipalTable;} else {  UDT_190Columns = table.Clone(); ; };
            table.Columns.Add("[GI]", typeof(int));
            table.Columns.Add("[GJ]", typeof(int));
            table.Columns.Add("[GK]", typeof(int));
            table.Columns.Add("[GL]", typeof(int));
            table.Columns.Add("[GM]", typeof(int));
            table.Columns.Add("[GN]", typeof(int));
            table.Columns.Add("[GO]", typeof(int));
            table.Columns.Add("[GP]", typeof(int));
            table.Columns.Add("[GQ]", typeof(int));
            table.Columns.Add("[GR]", typeof(int));
            if (TotalColumns > 190 && TotalColumns <= 200)  {  UDT_200Columns = PrincipalTable;} else {  UDT_200Columns = table.Clone(); ; };


             (new GlobalModel()).SaveDataTableDB(
                  UDT_5Columns,
                  UDT_10Columns,
                  UDT_20Columns,
                  UDT_30Columns,
                  UDT_40Columns,
                  UDT_50Columns,
                  UDT_60Columns,
                  UDT_70Columns,
                  UDT_80Columns,
                  UDT_90Columns,
                  UDT_100Columns,
                  UDT_110Columns,
                  UDT_120Columns,
                  UDT_130Columns,
                  UDT_140Columns,
                  UDT_150Columns,
                  UDT_160Columns,
                  UDT_170Columns,
                  UDT_180Columns,
                  UDT_190Columns,
                  UDT_200Columns,
                 "[dbo].[procCF_UploadDynamicTable]", new List<Dictionary<string, string>> {
                            new Dictionary<string, string>
                            {
                                { "Name", "@FileID" },
                                { "Value", FileID.ToString() }
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEID" },
                                { "Value", SOEID }
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@ColumnCount" },
                                { "Value", columnsDT.ToString() }
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@Country" },
                                { "Value", pCountrySelected }
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@Bu" },
                                { "Value", pBUSelected }
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@MonthYear" },
                                { "Value", pMonthYearSelected }
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@Quater" },
                                { "Value", pQuaterSelected }
                            }
                        });

            return null;

        }


        [ValidateInput(false)]
        [Route("uploadFileHeaderFix")]
        public ActionResult uploadFileHeaderFix(string data)
        {
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            DataTable ptable = (DataTable)JsonConvert.DeserializeObject(objParameter["data"], (typeof(DataTable)));

            string pCountrySelected = objParameter["pCountrySelected"].ToString();
            string pMonthYearSelected = objParameter["pMonthYearSelected"].ToString();
            string pQuaterSelected = objParameter["pQuaterSelected"].ToString();
            string SOEID = objParameter["SOEID"].ToString();
            int FileID = Convert.ToInt32(objParameter["FileID"].ToString());
            string Region = objParameter["Region"].ToString();

            string letter = objParameter["letter"].ToString();
            string keyword = objParameter["keyword"].ToString();

            CashFlowEntities DAO = new CashFlowEntities();

            tblCF_M_File File = (from p in DAO.tblCF_M_File where p.ID == FileID select p).SingleOrDefault<tblCF_M_File>();

            List<tblCF_M_FileColumn> Columns = File.tblCF_M_FileColumn.ToList<tblCF_M_FileColumn>().ToList<tblCF_M_FileColumn>();

            DataTable PrincipalTable = (DataTable)JsonConvert.DeserializeObject(objParameter["data"], (typeof(DataTable)));

            DataTable UDT_5Columns;
            DataTable UDT_10Columns;
            DataTable UDT_20Columns;
            DataTable UDT_30Columns;
            DataTable UDT_40Columns;
            DataTable UDT_50Columns;
            DataTable UDT_60Columns;
            DataTable UDT_70Columns;
            DataTable UDT_80Columns;
            DataTable UDT_90Columns;
            DataTable UDT_100Columns;
            DataTable UDT_110Columns;
            DataTable UDT_120Columns;
            DataTable UDT_130Columns;
            DataTable UDT_140Columns;
            DataTable UDT_150Columns;
            DataTable UDT_160Columns;
            DataTable UDT_170Columns;
            DataTable UDT_180Columns;
            DataTable UDT_190Columns;
            DataTable UDT_200Columns;

            int TotalColumns = PrincipalTable.Columns.Count;

            int columnsDT = PrincipalTable.Columns.Count;
            int var = 1;

            while (var == 1)
            {
                switch (columnsDT)
                {
                    case 5:
                    case 10:
                    case 20:
                    case 30:
                    case 40:
                    case 50:
                    case 60:
                    case 70:
                    case 80:
                    case 90:
                    case 100:
                    case 110:
                    case 120:
                    case 130:
                    case 140:
                    case 150:
                    case 160:
                    case 170:
                    case 180:
                    case 190:
                    case 200:
                        var = 0;
                        break;
                    default:
                        PrincipalTable.Columns.Add("add" + columnsDT.ToString());
                        columnsDT++;
                        var = 1;
                        break;
                }
            }



            DataTable table = new DataTable();

            table.Columns.Add("[ID]", typeof(int));
            table.Columns.Add("[A]", typeof(int));
            table.Columns.Add("[B]", typeof(int));
            table.Columns.Add("[C]", typeof(int));
            table.Columns.Add("[D]", typeof(int));
            table.Columns.Add("[E]", typeof(int));

            if (TotalColumns <= 5) { UDT_5Columns = PrincipalTable; } else { UDT_5Columns = table.Clone(); ; };
            table.Columns.Add("[F]", typeof(int));
            table.Columns.Add("[G]", typeof(int));
            table.Columns.Add("[H]", typeof(int));
            table.Columns.Add("[I]", typeof(int));
            table.Columns.Add("[J]", typeof(int));
            if (TotalColumns > 5 && TotalColumns <= 10) { UDT_10Columns = PrincipalTable; } else { UDT_10Columns = table.Clone(); ; };
            table.Columns.Add("[K]", typeof(int));
            table.Columns.Add("[L]", typeof(int));
            table.Columns.Add("[M]", typeof(int));
            table.Columns.Add("[N]", typeof(int));
            table.Columns.Add("[O]", typeof(int));
            table.Columns.Add("[P]", typeof(int));
            table.Columns.Add("[Q]", typeof(int));
            table.Columns.Add("[R]", typeof(int));
            table.Columns.Add("[S]", typeof(int));
            table.Columns.Add("[T]", typeof(int));
            if (TotalColumns > 10 && TotalColumns <= 20) { UDT_20Columns = PrincipalTable; } else { UDT_20Columns = table.Clone(); ; };
            table.Columns.Add("[U]", typeof(int));
            table.Columns.Add("[V]", typeof(int));
            table.Columns.Add("[W]", typeof(int));
            table.Columns.Add("[X]", typeof(int));
            table.Columns.Add("[Y]", typeof(int));
            table.Columns.Add("[Z]", typeof(int));
            table.Columns.Add("[AA]", typeof(int));
            table.Columns.Add("[AB]", typeof(int));
            table.Columns.Add("[AC]", typeof(int));
            table.Columns.Add("[AD]", typeof(int));
            if (TotalColumns > 20 && TotalColumns <= 30) { UDT_30Columns = PrincipalTable; } else { UDT_30Columns = table.Clone(); ; };
            table.Columns.Add("[AE]", typeof(int));
            table.Columns.Add("[AF]", typeof(int));
            table.Columns.Add("[AG]", typeof(int));
            table.Columns.Add("[AH]", typeof(int));
            table.Columns.Add("[AI]", typeof(int));
            table.Columns.Add("[AJ]", typeof(int));
            table.Columns.Add("[AK]", typeof(int));
            table.Columns.Add("[AL]", typeof(int));
            table.Columns.Add("[AM]", typeof(int));
            table.Columns.Add("[AN]", typeof(int));
            if (TotalColumns > 30 && TotalColumns <= 40) { UDT_40Columns = PrincipalTable; } else { UDT_40Columns = table.Clone(); ; };
            table.Columns.Add("[AO]", typeof(int));
            table.Columns.Add("[AP]", typeof(int));
            table.Columns.Add("[AQ]", typeof(int));
            table.Columns.Add("[AR]", typeof(int));
            table.Columns.Add("[AS]", typeof(int));
            table.Columns.Add("[AT]", typeof(int));
            table.Columns.Add("[AU]", typeof(int));
            table.Columns.Add("[AV]", typeof(int));
            table.Columns.Add("[AW]", typeof(int));
            table.Columns.Add("[AX]", typeof(int));
            if (TotalColumns > 40 && TotalColumns <= 50) { UDT_50Columns = PrincipalTable; } else { UDT_50Columns = table.Clone(); ; };
            table.Columns.Add("[AY]", typeof(int));
            table.Columns.Add("[AZ]", typeof(int));
            table.Columns.Add("[BA]", typeof(int));
            table.Columns.Add("[BB]", typeof(int));
            table.Columns.Add("[BC]", typeof(int));
            table.Columns.Add("[BD]", typeof(int));
            table.Columns.Add("[BE]", typeof(int));
            table.Columns.Add("[BF]", typeof(int));
            table.Columns.Add("[BG]", typeof(int));
            table.Columns.Add("[BH]", typeof(int));
            if (TotalColumns > 50 && TotalColumns <= 60) { UDT_60Columns = PrincipalTable; } else { UDT_60Columns = table.Clone(); ; };
            table.Columns.Add("[BI]", typeof(int));
            table.Columns.Add("[BJ]", typeof(int));
            table.Columns.Add("[BK]", typeof(int));
            table.Columns.Add("[BL]", typeof(int));
            table.Columns.Add("[BM]", typeof(int));
            table.Columns.Add("[BN]", typeof(int));
            table.Columns.Add("[BO]", typeof(int));
            table.Columns.Add("[BP]", typeof(int));
            table.Columns.Add("[BQ]", typeof(int));
            table.Columns.Add("[BR]", typeof(int));
            if (TotalColumns > 60 && TotalColumns <= 70) { UDT_70Columns = PrincipalTable; } else { UDT_70Columns = table.Clone(); ; };
            table.Columns.Add("[BS]", typeof(int));
            table.Columns.Add("[BT]", typeof(int));
            table.Columns.Add("[BU]", typeof(int));
            table.Columns.Add("[BV]", typeof(int));
            table.Columns.Add("[BW]", typeof(int));
            table.Columns.Add("[BX]", typeof(int));
            table.Columns.Add("[BY]", typeof(int));
            table.Columns.Add("[BZ]", typeof(int));
            table.Columns.Add("[CA]", typeof(int));
            table.Columns.Add("[CB]", typeof(int));
            if (TotalColumns > 70 && TotalColumns <= 80) { UDT_80Columns = PrincipalTable; } else { UDT_80Columns = table.Clone(); ; };
            table.Columns.Add("[CC]", typeof(int));
            table.Columns.Add("[CD]", typeof(int));
            table.Columns.Add("[CE]", typeof(int));
            table.Columns.Add("[CF]", typeof(int));
            table.Columns.Add("[CG]", typeof(int));
            table.Columns.Add("[CH]", typeof(int));
            table.Columns.Add("[CI]", typeof(int));
            table.Columns.Add("[CJ]", typeof(int));
            table.Columns.Add("[CK]", typeof(int));
            table.Columns.Add("[CL]", typeof(int));
            if (TotalColumns > 80 && TotalColumns <= 90) { UDT_90Columns = PrincipalTable; } else { UDT_90Columns = table.Clone(); ; };
            table.Columns.Add("[CM]", typeof(int));
            table.Columns.Add("[CN]", typeof(int));
            table.Columns.Add("[CO]", typeof(int));
            table.Columns.Add("[CP]", typeof(int));
            table.Columns.Add("[CQ]", typeof(int));
            table.Columns.Add("[CR]", typeof(int));
            table.Columns.Add("[CS]", typeof(int));
            table.Columns.Add("[CT]", typeof(int));
            table.Columns.Add("[CU]", typeof(int));
            table.Columns.Add("[CV]", typeof(int));
            if (TotalColumns > 90 && TotalColumns <= 100) { UDT_100Columns = PrincipalTable; } else { UDT_100Columns = table.Clone(); ; };
            table.Columns.Add("[CW]", typeof(int));
            table.Columns.Add("[CX]", typeof(int));
            table.Columns.Add("[CY]", typeof(int));
            table.Columns.Add("[CZ]", typeof(int));
            table.Columns.Add("[DA]", typeof(int));
            table.Columns.Add("[DB]", typeof(int));
            table.Columns.Add("[DC]", typeof(int));
            table.Columns.Add("[DD]", typeof(int));
            table.Columns.Add("[DE]", typeof(int));
            table.Columns.Add("[DF]", typeof(int));
            if (TotalColumns > 100 && TotalColumns <= 110) { UDT_110Columns = PrincipalTable; } else { UDT_110Columns = table.Clone(); ; };
            table.Columns.Add("[DG]", typeof(int));
            table.Columns.Add("[DH]", typeof(int));
            table.Columns.Add("[DI]", typeof(int));
            table.Columns.Add("[DJ]", typeof(int));
            table.Columns.Add("[DK]", typeof(int));
            table.Columns.Add("[DL]", typeof(int));
            table.Columns.Add("[DM]", typeof(int));
            table.Columns.Add("[DN]", typeof(int));
            table.Columns.Add("[DO]", typeof(int));
            table.Columns.Add("[DP]", typeof(int));
            if (TotalColumns > 110 && TotalColumns <= 120) { UDT_120Columns = PrincipalTable; } else { UDT_120Columns = table.Clone(); ; };
            table.Columns.Add("[DQ]", typeof(int));
            table.Columns.Add("[DR]", typeof(int));
            table.Columns.Add("[DS]", typeof(int));
            table.Columns.Add("[DT]", typeof(int));
            table.Columns.Add("[DU]", typeof(int));
            table.Columns.Add("[DV]", typeof(int));
            table.Columns.Add("[DW]", typeof(int));
            table.Columns.Add("[DX]", typeof(int));
            table.Columns.Add("[DY]", typeof(int));
            table.Columns.Add("[DZ]", typeof(int));
            if (TotalColumns > 120 && TotalColumns <= 130) { UDT_130Columns = PrincipalTable; } else { UDT_130Columns = table.Clone(); ; };
            table.Columns.Add("[EA]", typeof(int));
            table.Columns.Add("[EB]", typeof(int));
            table.Columns.Add("[EC]", typeof(int));
            table.Columns.Add("[ED]", typeof(int));
            table.Columns.Add("[EE]", typeof(int));
            table.Columns.Add("[EF]", typeof(int));
            table.Columns.Add("[EG]", typeof(int));
            table.Columns.Add("[EH]", typeof(int));
            table.Columns.Add("[EI]", typeof(int));
            table.Columns.Add("[EJ]", typeof(int));
            if (TotalColumns > 130 && TotalColumns <= 140) { UDT_140Columns = PrincipalTable; } else { UDT_140Columns = table.Clone(); ; };
            table.Columns.Add("[EK]", typeof(int));
            table.Columns.Add("[EL]", typeof(int));
            table.Columns.Add("[EM]", typeof(int));
            table.Columns.Add("[EN]", typeof(int));
            table.Columns.Add("[EO]", typeof(int));
            table.Columns.Add("[EP]", typeof(int));
            table.Columns.Add("[EQ]", typeof(int));
            table.Columns.Add("[ER]", typeof(int));
            table.Columns.Add("[ES]", typeof(int));
            table.Columns.Add("[ET]", typeof(int));
            if (TotalColumns > 140 && TotalColumns <= 150) { UDT_150Columns = PrincipalTable; } else { UDT_150Columns = table.Clone(); ; };
            table.Columns.Add("[EU]", typeof(int));
            table.Columns.Add("[EV]", typeof(int));
            table.Columns.Add("[EW]", typeof(int));
            table.Columns.Add("[EX]", typeof(int));
            table.Columns.Add("[EY]", typeof(int));
            table.Columns.Add("[EZ]", typeof(int));
            table.Columns.Add("[FA]", typeof(int));
            table.Columns.Add("[FB]", typeof(int));
            table.Columns.Add("[FC]", typeof(int));
            table.Columns.Add("[FD]", typeof(int));
            if (TotalColumns > 150 && TotalColumns <= 160) { UDT_160Columns = PrincipalTable; } else { UDT_160Columns = table.Clone(); ; };
            table.Columns.Add("[FE]", typeof(int));
            table.Columns.Add("[FF]", typeof(int));
            table.Columns.Add("[FG]", typeof(int));
            table.Columns.Add("[FH]", typeof(int));
            table.Columns.Add("[FI]", typeof(int));
            table.Columns.Add("[FJ]", typeof(int));
            table.Columns.Add("[FK]", typeof(int));
            table.Columns.Add("[FL]", typeof(int));
            table.Columns.Add("[FM]", typeof(int));
            table.Columns.Add("[FN]", typeof(int));
            if (TotalColumns > 160 && TotalColumns <= 170) { UDT_170Columns = PrincipalTable; } else { UDT_170Columns = table.Clone(); ; };
            table.Columns.Add("[FO]", typeof(int));
            table.Columns.Add("[FP]", typeof(int));
            table.Columns.Add("[FQ]", typeof(int));
            table.Columns.Add("[FR]", typeof(int));
            table.Columns.Add("[FS]", typeof(int));
            table.Columns.Add("[FT]", typeof(int));
            table.Columns.Add("[FU]", typeof(int));
            table.Columns.Add("[FV]", typeof(int));
            table.Columns.Add("[FW]", typeof(int));
            table.Columns.Add("[FX]", typeof(int));
            if (TotalColumns > 170 && TotalColumns <= 180) { UDT_180Columns = PrincipalTable; } else { UDT_180Columns = table.Clone(); ; };
            table.Columns.Add("[FY]", typeof(int));
            table.Columns.Add("[FZ]", typeof(int));
            table.Columns.Add("[GA]", typeof(int));
            table.Columns.Add("[GB]", typeof(int));
            table.Columns.Add("[GC]", typeof(int));
            table.Columns.Add("[GD]", typeof(int));
            table.Columns.Add("[GE]", typeof(int));
            table.Columns.Add("[GF]", typeof(int));
            table.Columns.Add("[GG]", typeof(int));
            table.Columns.Add("[GH]", typeof(int));
            if (TotalColumns > 180 && TotalColumns <= 190) { UDT_190Columns = PrincipalTable; } else { UDT_190Columns = table.Clone(); ; };
            table.Columns.Add("[GI]", typeof(int));
            table.Columns.Add("[GJ]", typeof(int));
            table.Columns.Add("[GK]", typeof(int));
            table.Columns.Add("[GL]", typeof(int));
            table.Columns.Add("[GM]", typeof(int));
            table.Columns.Add("[GN]", typeof(int));
            table.Columns.Add("[GO]", typeof(int));
            table.Columns.Add("[GP]", typeof(int));
            table.Columns.Add("[GQ]", typeof(int));
            table.Columns.Add("[GR]", typeof(int));
            if (TotalColumns > 190 && TotalColumns <= 200) { UDT_200Columns = PrincipalTable; } else { UDT_200Columns = table.Clone(); ; };


            (new GlobalModel()).SaveDataTableDB(
                 UDT_5Columns,
                 UDT_10Columns,
                 UDT_20Columns,
                 UDT_30Columns,
                 UDT_40Columns,
                 UDT_50Columns,
                 UDT_60Columns,
                 UDT_70Columns,
                 UDT_80Columns,
                 UDT_90Columns,
                 UDT_100Columns,
                 UDT_110Columns,
                 UDT_120Columns,
                 UDT_130Columns,
                 UDT_140Columns,
                 UDT_150Columns,
                 UDT_160Columns,
                 UDT_170Columns,
                 UDT_180Columns,
                 UDT_190Columns,
                 UDT_200Columns,
                "[dbo].[procCF_UploadDynamicTableWithFixHeader]", new List<Dictionary<string, string>> {
                            new Dictionary<string, string>
                            {
                                { "Name", "@FileID" },
                                { "Value", FileID.ToString() }
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEID" },
                                { "Value", SOEID }
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@ColumnCount" },
                                { "Value", columnsDT.ToString() }
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@Country" },
                                { "Value", pCountrySelected }
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@Bu" },
                                { "Value", null }
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@MonthYear" },
                                { "Value", pMonthYearSelected }
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@Quater" },
                                { "Value", pQuaterSelected }
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@HeaderFix" },
                                { "Value", "BU" }
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@ColumnFix" },
                                { "Value", letter }
                            }
                       });
       
            return null;

        }


        [Route("getLogFile")]
        public ActionResult getLogFile(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["IDFile"].ToString());

            //JObject o = null;
            //o = JObject.FromObject(new
            //{
            //    item =
            //       from p in DAO.tblCF_M_FileLog where p.FileID == ID where p.FileID == ID
            //       join ask in DAO.tblCF_M_FileRegion on p.FileID equals ask.FileID
            //       join c in DAO.tblCF_R_Country on p.CountryID equals c.ID into country_temp
            //       from ct in country_temp.DefaultIfEmpty()
            //       join da in DAO.tblCF_M_MonthYearQuater on p.MonthYearID equals da.ID  into date_temp
            //       from dat in date_temp.DefaultIfEmpty()
            //       //join q in DAO.tblCF_M_MonthYearQuater on p.QuaterID equals q.ID into quater_temp
            //       //from qt in quater_temp.DefaultIfEmpty()
            //       orderby p.ID descending
            //        select new
            //        {
            //           p.ID
            //          ,p.DateUpload
            //          ,p.SOEID
            //          ,p.InProgress
            //          ,p.StartTime
            //          ,p.EndTime
            //          ,p.FileID
            //          ,p.MonthUpload
            //          ,p.YearUpload
            //          ,p.QuarterUpload
            //          ,p.TotalRow
            //          ,ct.Country
            //          ,p.BUID
            //          ,dat.MonthYear
            //          ,dat.Quater
            //          ,ask.AskCountry
            //          ,ask.AskBU
            //          ,ask.AskMonthYear
            //          ,ask.AskQuater
            //        }
            //});

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                   from p in DAO.procCF_GetLogFile(ID)
                   orderby p.ID descending
                   select new
                   {
                       p.ID
                     ,
                       p.DateUpload
                     ,
                       p.SOEID
                     ,
                       p.InProgress
                     ,
                       p.StartTime
                     ,
                       p.EndTime
                     ,
                       p.FileID
                     ,
                       p.MonthUpload
                     ,
                       p.YearUpload
                     ,
                       p.QuarterUpload
                     ,
                       p.TotalRow
                     ,
                       p.Country
                     ,
                       p.BUID
                     ,
                       p.MonthYear
                     ,
                       p.Quater
                     ,
                       p.AskCountry
                     ,
                       p.AskBU
                     ,
                       p.AskMonthYear
                     ,
                       p.AskQuater
                   }
            });































            return Content(o["item"].ToString());
        }



        [Route("getLogFileOnly")]
        public ActionResult getLogFileOnly(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["IDFile"].ToString());

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                   from p in DAO.procCF_GetLogFileOnlyFiles(ID)
                   orderby p.MonthUpload  descending
                   select new
                   {
                       
                       p.SOEID
                     ,
                       p.FileID
                     ,
                       p.MonthUpload
                     ,
                       p.YearUpload
                     ,
                       p.QuarterUpload
                     ,
                       p.MonthYear
                     ,
                       p.Quater
                     ,
                       p.AskCountry
                     ,
                       p.AskBU
                     ,
                       p.AskMonthYear
                     ,
                       p.AskQuater
                   }
            });


            return Content(o["item"].ToString());
        }



        [Route("getYear")]
        public ActionResult getYear(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            DateTime _date = DateTime.Now;
            int Month = _date.Month;
            int Year = _date.Year;

            int Quater = 0;
            if (Month > 0 && Month < 4)
            {
                Quater = 1;
            }
            if (Month > 3 && Month < 7)
            {
                Quater = 2;
            }
            if (Month > 6 && Month < 10)
            {
                Quater = 3;
            }
            if (Month > 8 && Month < 13)
            {
                Quater = 4;
            }

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int count = (from p in DAO.tblCF_M_MonthYearQuater where p.Month == Month && p.Year == Year && p.Quater == Quater select p).ToList<tblCF_M_MonthYearQuater>().Count();

            if (count == 0)
            {
                tblCF_M_MonthYearQuater newDate = new tblCF_M_MonthYearQuater();

                newDate.Month = Month;
                newDate.Quater = Quater;
                newDate.Year = Year;
                newDate.MonthYear = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Month) + " " + Year;

                DAO.tblCF_M_MonthYearQuater.Add(newDate);

                DAO.SaveChanges();

            }

            List<tblCF_M_MonthYearQuater> quater = (from p in DAO.tblCF_M_MonthYearQuater select p).ToList<tblCF_M_MonthYearQuater>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                   (from p in DAO.tblCF_M_MonthYearQuater
                    select new
                    {
                        Year = p.Year
                    }).Distinct()
            });


            return Content(o["item"].ToString());
        }
    }
}