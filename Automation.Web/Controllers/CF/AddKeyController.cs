﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using Automation.Web.Models.CashFlow;
using System.Globalization;
using Automation.Core.Model;


namespace Automation.Web.Controllers.CF
{

    [RoutePrefix("CF")]
    public class AddKeyController : Controller
    {

        [Route("AddKey/{account?}")]
        public ActionResult Index(string account)
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            viewModel.Add("Account", account);

            return (View("~/Views/CF/AddKey.cshtml", viewModel));
        }

        [Route("getCF_AccountByID")]
        public ActionResult getCF_AccountByID(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int AccountID = Convert.ToInt32(objParameter["AccountID"].ToString());
            

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_CF_Account
                    where p.ID == AccountID
                    orderby p.CFAccount
                    select new
                    {
                        p.ID,
                        p.CFAccount,
                        p.Description,
                        p.tblCF_R_Region.Region,
                        p.tblCF_R_RegionCountry.tblCF_R_Country.Country,
                        p.YearApply,
                        p.QuaterApply,
                        p.RegionID,
                        dateID = ((from d in DAO.tblCF_M_MonthYearQuater where d.Quater == p.QuaterApply && d.Year == p.YearApply select d).FirstOrDefault<tblCF_M_MonthYearQuater>().ID)
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("getCF_BatchOrder")]
        public ActionResult getCF_BatchOrder(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_M_ColumnOrder
                    orderby p.ColOrder
                    select new
                    {
                        p.ID,
                        p.ColOrder
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("getCF_ActionOnResult")]
        public ActionResult getCF_ActionOnResult(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_Key_PreAction
                    select new
                    {
                        p.ID,
                        p.Action
                    }
            });

            return Content(o["item"].ToString());
        }

        [Route("getCF_FileList")]
        public ActionResult getCF_FileList(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int AccountID = Convert.ToInt32(objParameter["AccountID"].ToString());

            tblCF_CF_Account tblAccount = (from p in DAO.tblCF_CF_Account where p.ID == AccountID select p).SingleOrDefault<tblCF_CF_Account>();


            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_M_File
                    join c in DAO.tblCF_M_FileRegion on p.ID equals c.FileID
                    where p.IsCreated == true && c.RegionID == tblAccount.RegionID
                    select new
                    {
                        p.ID,
                        p.Name,
                        c.AskBU,
                        c.AskMonthYear
                    }
            });

            return Content(o["item"].ToString());
        }

        [Route("getCF_ActionOnColumn")]
        public ActionResult getCF_ActionOnColumn(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();


            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_Key_Action
                    select new
                    {
                        p.ID,
                        p.Action
                    }
            });

            return Content(o["item"].ToString());
        }

        [Route("getCF_FileColumn")]
        public ActionResult getCF_FileColumn(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int FileID = Convert.ToInt32(objParameter["FileID"].ToString());

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_M_FileColumn join
                    c in DAO.tblCF_M_TypeColumn on p.Type equals c.ID
                    where p.FileID == FileID //&& 
                                             //c.Name == "int" || 
                                             //c.Name == "numeric(31,2)" ||
                                             //c.Name == "numeric(31,3)" ||
                                             //c.Name == "numeric(31,4)" ||
                                             //c.Name == "numeric(31,5)" 
                        select new
                    {
                        p.ID,
                        p.ColumnName,
                    }
            });

            return Content(o["item"].ToString());
        }


        [Route("add_CF_Rule")]
        public ActionResult add_CF_Rule(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int AccountID = Convert.ToInt32(objParameter["AccountID"].ToString());
            int FileID = Convert.ToInt32(objParameter["FileID"].ToString());
            int ColumnId = Convert.ToInt32(objParameter["ColumnId"].ToString());
            int TypeActionColumn = Convert.ToInt32(objParameter["TypeActionColumn"].ToString());
            string mask = objParameter["mask"].ToString();

            int ActionOnResult = 1007;

            try
            {
                 ActionOnResult = Convert.ToInt32(objParameter["ActionOnResult"].ToString());
            }
            catch {
                
            }

            tblCF_CF_Account accountapply = (from p in DAO.tblCF_CF_Account where p.ID == AccountID select p).SingleOrDefault<tblCF_CF_Account>();


            tblCF_CF_Rule Rule = new tblCF_CF_Rule();

             Rule.CFAccountID = AccountID;
             Rule.FileID = FileID;
             Rule.ColumnID = ColumnId;
             Rule.TypeActionInColumn = TypeActionColumn;
             Rule.Value = 0;
             Rule.VarcharInKey = "";
             DAO.tblCF_CF_Rule.Add(Rule);
             DAO.SaveChanges();
             Rule.VarcharInKey = "@K" + Rule.ID.ToString();
             Rule.Mask = mask;
             Rule.ActionOnResult = ActionOnResult;
             Rule.IsConditionalKey = false;
            Rule.Conditional = "";
            Rule.FinalKey = "";

             DAO.SaveChanges();

             tblCF_Key key = new tblCF_Key();

             tblCF_M_FileRegion FILESELECT = (from p in DAO.tblCF_M_FileRegion where p.FileID == FileID select p).SingleOrDefault<tblCF_M_FileRegion>();

             if (accountapply.QuaterApply == 1)
             {
                 key.whereSQL = " LogID in (select id from tblCF_M_FileLog where FileID = " + FileID + " AND MonthYearID IN  (SELECT ID FROM [dbo].[tblCF_M_MonthYearQuater] WHERE Quater in (4,1) and Year in ( " + accountapply.YearApply + ", " + (accountapply.YearApply - 1) + ") )) ";
             }
             else
             {
                 key.whereSQL = " LogID in (select id from tblCF_M_FileLog where FileID = " + FileID + " AND MonthYearID IN  (SELECT ID FROM [dbo].[tblCF_M_MonthYearQuater] WHERE Quater in (" + (accountapply.QuaterApply - 1) + "," + accountapply.QuaterApply + ") and Year = " + accountapply.YearApply + ")) ";
             }

             key.RuleID = Rule.ID;
             key.ColumnID = 0;
             key.ColumnActionID = 1;
             key.ValueSearch = "0";

             DAO.tblCF_Key.Add(key);

             DAO.SaveChanges();

            var query1 = (from p in DAO.tblCF_CF_Rule
                          join action in DAO.tblCF_Key_PreAction on p.ActionOnResult equals action.ID
                          join f in DAO.tblCF_M_File on p.FileID equals f.ID into data_file
                          from file in data_file.DefaultIfEmpty()
                          join c in DAO.tblCF_M_FileColumn on p.ColumnID equals c.ID into data_Columns
                          from columns in data_Columns.DefaultIfEmpty()
                          where p.CFAccountID == AccountID && p.IsConditionalKey == false
                          select new
                          {
                              p.ID,
                              p.CFAccountID,
                              file.Name,
                              columns.ColumnName,
                              ActionColumn = p.tblCF_Key_Action.Action,
                              p.Value,
                              p.VarcharInKey,
                              p.Mask,
                              ActionOnResult = action.Action,
                              p.IsConditionalKey,
                              p.Conditional,
                              p.FinalKey
                          }).ToList();

            var query2 = (from p in DAO.tblCF_CF_Rule
                          join action in DAO.tblCF_Key_PreAction on p.ActionOnResult equals action.ID
                          join f in DAO.tblCF_M_File on p.FileID equals f.ID into data_file
                          from file in data_file.DefaultIfEmpty()
                          join c in DAO.tblCF_M_FileColumn on p.ColumnID equals c.ID into data_Columns
                          from columns in data_Columns.DefaultIfEmpty()
                          where p.CFAccountID == AccountID && p.IsConditionalKey == true
                          select new
                          {
                              p.ID,
                              p.CFAccountID,
                              Name = p.Conditional,
                              ColumnName = p.FinalKey,
                              ActionColumn = p.tblCF_Key_Action.Action,
                              p.Value,
                              p.VarcharInKey,
                              p.Mask,
                              ActionOnResult = "Is Conditional",
                              p.IsConditionalKey,
                              p.Conditional,
                              p.FinalKey
                          }).ToList();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in query1.Concat(query2)
                    select new
                    {
                        p.ID,
                        p.CFAccountID,
                        p.Name,
                        p.ColumnName,
                        p.ActionColumn,
                        p.Value,
                        p.VarcharInKey,
                        p.Mask,
                        p.ActionOnResult,
                        p.IsConditionalKey,
                        p.Conditional ,
                        p.FinalKey
        }
            });

            return Content(o["item"].ToString());
        }

        [Route("get_CF_Rule")]
        public ActionResult get_CF_Rule(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int AccountID = Convert.ToInt32(objParameter["AccountID"].ToString());

            var query1 = (from p in DAO.tblCF_CF_Rule
                          join action in DAO.tblCF_Key_PreAction on p.ActionOnResult equals action.ID
                          join f in DAO.tblCF_M_File on p.FileID equals f.ID into data_file
                          from file in data_file.DefaultIfEmpty()
                          join c in DAO.tblCF_M_FileColumn on p.ColumnID equals c.ID into data_Columns
                          from columns in data_Columns.DefaultIfEmpty()
                          where p.CFAccountID == AccountID && p.IsConditionalKey == false
                          select new
                          {
                              p.ID,
                              p.CFAccountID,
                              file.Name,
                              columns.ColumnName,
                              ActionColumn = p.tblCF_Key_Action.Action,
                              p.Value,
                              p.VarcharInKey,
                              p.Mask,
                              ActionOnResult = action.Action,
                              p.IsConditionalKey,
                              p.Conditional,
                              p.FinalKey
                          }).ToList();

            var query2 = (from p in DAO.tblCF_CF_Rule
                          join action in DAO.tblCF_Key_PreAction on p.ActionOnResult equals action.ID
                          join f in DAO.tblCF_M_File on p.FileID equals f.ID into data_file
                          from file in data_file.DefaultIfEmpty()
                          join c in DAO.tblCF_M_FileColumn on p.ColumnID equals c.ID into data_Columns
                          from columns in data_Columns.DefaultIfEmpty()
                          where p.CFAccountID == AccountID && p.IsConditionalKey == true
                          select new
                          {
                              p.ID,
                              p.CFAccountID,
                              Name = p.Conditional,
                              ColumnName = p.FinalKey,
                              ActionColumn = p.tblCF_Key_Action.Action,
                              p.Value,
                              p.VarcharInKey,
                              p.Mask,
                              ActionOnResult = "Is Conditional",
                              p.IsConditionalKey,
                              p.Conditional,
                              p.FinalKey
                          }).ToList();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in query1.Concat(query2)
                    select new
                    {
                        p.ID,
                        p.CFAccountID,
                        p.Name,
                        p.ColumnName,
                        p.ActionColumn,
                        p.Value,
                        p.VarcharInKey,
                        p.Mask,
                        p.ActionOnResult,
                        p.IsConditionalKey,
                        p.Conditional,
                        p.FinalKey
                    }
            });

            return Content(o["item"].ToString());
        }

        [Route("delete_CF_Rule")]
        public ActionResult delete_CF_Rule(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int AccountID = Convert.ToInt32(objParameter["AccountID"].ToString());
            int ID = Convert.ToInt32(objParameter["ID"].ToString());

            tblCF_CF_Rule Rule = (from p in DAO.tblCF_CF_Rule where p.ID == ID select p).SingleOrDefault<tblCF_CF_Rule>();

            foreach (tblCF_Key key in Rule.tblCF_Key.ToList<tblCF_Key>())
            {
                DAO.tblCF_Key.Remove(key);
            }
            DAO.SaveChanges();

            DAO.tblCF_CF_Rule.Remove(Rule);

            DAO.SaveChanges();

            var query1 = (from p in DAO.tblCF_CF_Rule
                          join action in DAO.tblCF_Key_PreAction on p.ActionOnResult equals action.ID
                          join f in DAO.tblCF_M_File on p.FileID equals f.ID into data_file
                          from file in data_file.DefaultIfEmpty()
                          join c in DAO.tblCF_M_FileColumn on p.ColumnID equals c.ID into data_Columns
                          from columns in data_Columns.DefaultIfEmpty()
                          where p.CFAccountID == AccountID && p.IsConditionalKey == false
                          select new
                          {
                              p.ID,
                              p.CFAccountID,
                              file.Name,
                              columns.ColumnName,
                              ActionColumn = p.tblCF_Key_Action.Action,
                              p.Value,
                              p.VarcharInKey,
                              p.Mask,
                              ActionOnResult = action.Action,
                              p.IsConditionalKey,
                              p.Conditional,
                              p.FinalKey
                          }).ToList();

            var query2 = (from p in DAO.tblCF_CF_Rule
                          join action in DAO.tblCF_Key_PreAction on p.ActionOnResult equals action.ID
                          join f in DAO.tblCF_M_File on p.FileID equals f.ID into data_file
                          from file in data_file.DefaultIfEmpty()
                          join c in DAO.tblCF_M_FileColumn on p.ColumnID equals c.ID into data_Columns
                          from columns in data_Columns.DefaultIfEmpty()
                          where p.CFAccountID == AccountID && p.IsConditionalKey == true
                          select new
                          {
                              p.ID,
                              p.CFAccountID,
                              Name = p.Conditional,
                              ColumnName = p.FinalKey,
                              ActionColumn = p.tblCF_Key_Action.Action,
                              p.Value,
                              p.VarcharInKey,
                              p.Mask,
                              ActionOnResult = "Is Conditional",
                              p.IsConditionalKey,
                              p.Conditional,
                              p.FinalKey
                          }).ToList();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in query1.Concat(query2)
                    select new
                    {
                        p.ID,
                        p.CFAccountID,
                        p.Name,
                        p.ColumnName,
                        p.ActionColumn,
                        p.Value,
                        p.VarcharInKey,
                        p.Mask,
                        p.ActionOnResult,
                        p.IsConditionalKey,
                        p.Conditional,
                        p.FinalKey
                    }
            });

            return Content(o["item"].ToString());
        }

        [Route("getCF_AllFileColumn")]
        public ActionResult getCF_AllFileColumn(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["ID"].ToString());

            tblCF_CF_Rule Rule = (from p in DAO.tblCF_CF_Rule where p.ID == ID select p).SingleOrDefault<tblCF_CF_Rule>();

            tblCF_M_FileRegion fileregion = (from p in DAO.tblCF_M_FileRegion where p.FileID == Rule.FileID select p).SingleOrDefault<tblCF_M_FileRegion>();

            tblCF_M_FileColumn columnCountry = new tblCF_M_FileColumn();
            tblCF_M_FileColumn columnBU = new tblCF_M_FileColumn();

            JObject o = null;
            tblCF_M_File adminMonth = (from p in DAO.tblCF_M_File where p.Name == "Default Month" select p).SingleOrDefault<tblCF_M_File>();
            tblCF_M_File adminQuater = (from p in DAO.tblCF_M_File where p.Name == "Default Quater" select p).SingleOrDefault<tblCF_M_File>();
            tblCF_M_File adminBU = (from p in DAO.tblCF_M_File where p.Name == "DefaultBU" select p).SingleOrDefault<tblCF_M_File>();
            tblCF_M_File adminCountry = (from p in DAO.tblCF_M_File where p.Name == "DefaultCountry" select p).SingleOrDefault<tblCF_M_File>();


            //if (fileregion.AskCountry == true && fileregion.AskBU == true && fileregion.AskQuater == true && fileregion.AskMonthYear == true)

            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_M_FileColumn
                    where p.FileID == Rule.FileID || p.FileID == adminMonth.ID || p.FileID == adminQuater.ID || p.FileID == adminBU.ID || p.FileID == adminCountry.ID
                    select new
                    {
                        p.ID,
                        p.ColumnName
                    }
            }); ;

            return Content(o["item"].ToString());
        }

        [Route("getCF_AllFileColumn")]
        public ActionResult getCF_TypeWhere(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ColumnID = Convert.ToInt32(objParameter["ColumnID"].ToString());

            tblCF_M_FileColumn column = (from p in DAO.tblCF_M_FileColumn
                                         where p.ID == ColumnID select p).SingleOrDefault<tblCF_M_FileColumn>();

            tblCF_M_TypeColumn type = (from p in DAO.tblCF_M_TypeColumn
                                       where p.ID == column.Type
                                       select p).SingleOrDefault<tblCF_M_TypeColumn>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item = from p in type.tblCF_M_MapActions
                       select new
                       {
                           p.ID,
                           p.ActionName,
                           p.SQL
                       }
            });

            return Content(o["item"].ToString());
        }


        [Route("getCF_WhereList")]
        public ActionResult getCF_WhereList(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["ID"].ToString());

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_Key join
                    c in DAO.tblCF_M_FileColumn on p.ColumnID equals c.ID join
                    t in DAO.tblCF_M_MapActions on p.ColumnActionID equals t.ID
                    where p.RuleID == ID
                    select new
                    {
                        p.ID,
                        p.RuleID,
                        c.ColumnName,
                        t.ActionName,
                        p.ValueSearch
                    }
            });

            return Content(o["item"].ToString());
        }

        [Route("deleteCF_WhereList")]
        public ActionResult deleteCF_WhereList(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["ID"].ToString());

            tblCF_Key key = (from p in DAO.tblCF_Key where p.ID == ID select p).SingleOrDefault<tblCF_Key>();
            int IDRule = key.RuleID;
            DAO.tblCF_Key.Remove(key);

            DAO.SaveChanges();


            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_Key
                    join
c in DAO.tblCF_M_FileColumn on p.ColumnID equals c.ID
                    join
t in DAO.tblCF_M_MapActions on p.ColumnActionID equals t.ID
                    where p.RuleID == IDRule
                    select new
                    {
                        p.ID,
                        p.RuleID,
                        c.ColumnName,
                        t.ActionName,
                        p.ValueSearch
                    }
            });

            return Content(o["item"].ToString());
        }


        [Route("addCF_WhereList")]
        public ActionResult addCF_WhereList(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["ID"].ToString());
            int ColumnID = Convert.ToInt32(objParameter["ColumnID"].ToString());
            int ColumnActionID = Convert.ToInt32(objParameter["ColumnActionID"].ToString());
            string Value = objParameter["Value"].ToString();

            tblCF_Key key = new tblCF_Key();

            tblCF_M_FileColumn column = (from p in DAO.tblCF_M_FileColumn where p.ID == ColumnID select p).SingleOrDefault<tblCF_M_FileColumn>();

            tblCF_M_MapActions map = (from p in DAO.tblCF_M_MapActions where p.ID == ColumnActionID select p).SingleOrDefault<tblCF_M_MapActions>();

            String[] substrings;
            int count;

            if (column.tblCF_M_File.Name == "DefaultColumns" || column.tblCF_M_File.Name == "DefaultCountry" || column.tblCF_M_File.Name == "DefaultBU" || column.tblCF_M_File.Name == "Default Month" || column.tblCF_M_File.Name == "Default Quater")
            {
                if (column.ColumnName == "Country")
                {
                    switch (map.ActionName)
                    {
                        case "EQUAL":
                            key.innerJoin = " inner join tblCF_R_Country ON tblCF_R_Country.ID = tblCF_M_FileLog.CountryID";
                            key.whereSQL = " tblCF_R_Country.Country = '" + Value + "' ";
                            break;
                        case "LIKE":
                            key.innerJoin = " inner join tblCF_R_Country ON tblCF_R_Country.ID = tblCF_M_FileLog.CountryID";
                            key.whereSQL = " tblCF_R_Country.Country like '%" + Value + "%' ";
                            break;
                        case "IN":
                            key.innerJoin = " inner join tblCF_R_Country ON tblCF_R_Country.ID = tblCF_M_FileLog.CountryID";
                            key.whereSQL = "  tblCF_R_Country.Country  in ( ";

                            substrings = Value.Split(',');

                            count = 0;
                            foreach (var substring in substrings)
                            {
                                if (count == 0)
                                {
                                    key.whereSQL = key.whereSQL + "'" + substring.TrimStart().TrimEnd() + "'";
                                }
                                else
                                {
                                    key.whereSQL = key.whereSQL + ",'" + substring.TrimStart().TrimEnd() + "'";
                                }

                                count = count + 1;
                            }
                            key.whereSQL = key.whereSQL + ") ";
                            break;
                        case "NOT IN":
                            key.innerJoin = " inner join tblCF_R_Country ON tblCF_R_Country.ID = tblCF_M_FileLog.CountryID";
                            key.whereSQL = "  tblCF_R_Country.Country  not in ( ";
                            substrings = Value.Split(',');
                            count = 0;
                            foreach (var substring in substrings)
                            {
                                if (count == 0)
                                {
                                    key.whereSQL = key.whereSQL + "'" + substring.TrimStart().TrimEnd() + "'";
                                }
                                else
                                {
                                    key.whereSQL = key.whereSQL + ",'" + substring.TrimStart().TrimEnd() + "'";
                                }

                                count = count + 1;
                            }
                            key.whereSQL = key.whereSQL + ") ";
                            break;
                        case "START WITH":
                            key.innerJoin = " inner join tblCF_R_Country ON tblCF_R_Country.ID = tblCF_M_FileLog.CountryID";
                            key.whereSQL = "  tblCF_R_Country.Country = '" + Value + "%' ";

                            break;
                        case "END WITH":
                            key.innerJoin = " inner join tblCF_R_Country ON tblCF_R_Country.ID = tblCF_M_FileLog.CountryID";
                            key.whereSQL = "  tblCF_R_Country.Country  like '%" + Value + "' ";

                            break;
                        case "ANY THAT HAVE":
                            key.innerJoin = " inner join tblCF_R_Country ON tblCF_R_Country.ID = tblCF_M_FileLog.CountryID";
                            key.whereSQL = "  tblCF_R_Country.Country  like '%" + Value + "%' ";

                            break;
                        case "NOT EQUAL":
                            key.innerJoin = " inner join tblCF_R_Country ON tblCF_R_Country.ID = tblCF_M_FileLog.CountryID";
                            key.whereSQL = "  tblCF_R_Country.Country  != '" + Value + "' ";

                            break;
                    }
                }
                if (column.ColumnName == "BU")
                {
                    switch (map.ActionName)
                    {
                        case "EQUAL":
                            key.whereSQL = " tblCF_M_FileLog.BUID = '" + Value + "' ";
                            break;
                        case "LIKE":
                            key.whereSQL = " tblCF_M_FileLog.BUID  like '%" + Value + "%' ";
                            break;
                        case "IN":
                            key.whereSQL = "  tblCF_M_FileLog.BUID  in ( ";

                            substrings = Value.Split(',');

                            count = 0;
                            foreach (var substring in substrings)
                            {
                                if (count == 0)
                                {
                                    key.whereSQL = key.whereSQL + "'" + substring.TrimStart().TrimEnd() + "'";
                                }
                                else
                                {
                                    key.whereSQL = key.whereSQL + ",'" + substring.TrimStart().TrimEnd() + "'";
                                }

                                count = count + 1;
                            }
                            key.whereSQL = key.whereSQL + ") ";
                            break;
                        case "NOT IN":
                            key.whereSQL = " tblCF_M_FileLog.BUID   not in ( ";
                            substrings = Value.Split(',');
                            count = 0;
                            foreach (var substring in substrings)
                            {
                                if (count == 0)
                                {
                                    key.whereSQL = key.whereSQL + "'" + substring.TrimStart().TrimEnd() + "'";
                                }
                                else
                                {
                                    key.whereSQL = key.whereSQL + ",'" + substring.TrimStart().TrimEnd() + "'";
                                }

                                count = count + 1;
                            }
                            key.whereSQL = key.whereSQL + ") ";
                            break;
                        case "START WITH":
                            key.whereSQL = " tblCF_M_FileLog.BUID  LIKE '" + Value + "%' ";

                            break;
                        case "END WITH":
                            key.whereSQL = " tblCF_M_FileLog.BUID  like '%" + Value + "' ";

                            break;
                        case "ANY THAT HAVE":
                            key.whereSQL = " tblCF_M_FileLog.BUID   like '%" + Value + "%' ";

                            break;
                        case "NOT EQUAL":
                            key.whereSQL = "  tblCF_M_FileLog.BUID   != '" + Value + "' ";

                            break;
                    }
                }
                if (column.ColumnName == "Month Number")
                {
                    switch (map.ActionName)
                    {
                        case "EQUAL":
                            key.whereSQL = " tblCF_M_MonthYearQuater.Month = '" + Value + "' ";
                            break;
                        case "LIKE":
                            key.whereSQL = " tblCF_M_MonthYearQuater.Month like '%" + Value + "%' ";
                            break;
                        case "IN":
                            key.whereSQL = "  tblCF_M_MonthYearQuater.Month in ( ";

                            substrings = Value.Split(',');

                            count = 0;
                            foreach (var substring in substrings)
                            {
                                if (count == 0)
                                {
                                    key.whereSQL = key.whereSQL + "'" + substring.TrimStart().TrimEnd() + "'";
                                }
                                else
                                {
                                    key.whereSQL = key.whereSQL + ",'" + substring.TrimStart().TrimEnd() + "'";
                                }

                                count = count + 1;
                            }
                            key.whereSQL = key.whereSQL + ") ";
                            break;
                        case "NOT IN":
                            key.whereSQL = " tblCF_M_MonthYearQuater.Month not in ( ";
                            substrings = Value.Split(',');
                            count = 0;
                            foreach (var substring in substrings)
                            {
                                if (count == 0)
                                {
                                    key.whereSQL = key.whereSQL + "'" + substring.TrimStart().TrimEnd() + "'";
                                }
                                else
                                {
                                    key.whereSQL = key.whereSQL + ",'" + substring.TrimStart().TrimEnd() + "'";
                                }

                                count = count + 1;
                            }
                            key.whereSQL = key.whereSQL + ") ";
                            break;
                        case "START WITH":
                            key.whereSQL = " tblCF_M_MonthYearQuater.Month LIKE '" + Value + "%' ";

                            break;
                        case "END WITH":
                            key.whereSQL = " tblCF_M_MonthYearQuater.Month like '%" + Value + "' ";

                            break;
                        case "ANY THAT HAVE":
                            key.whereSQL = " tblCF_M_MonthYearQuater.Month like '%" + Value + "%' ";

                            break;
                        case "NOT EQUAL":
                            key.whereSQL = " tblCF_M_MonthYearQuater.Month != '" + Value + "' ";

                            break;
                    }
                }
                if (column.ColumnName == "Quater Number")
                {
                    switch (map.ActionName)
                    {
                        case "EQUAL":
                            key.whereSQL = " tblCF_M_MonthYearQuater.Quater = '" + Value + "' ";
                            break;
                        case "LIKE":
                            key.whereSQL = " tblCF_M_MonthYearQuater.Quater like '%" + Value + "%' ";
                            break;
                        case "IN":
                            key.whereSQL = "  tblCF_M_MonthYearQuater.Quater in ( ";

                            substrings = Value.Split(',');

                            count = 0;
                            foreach (var substring in substrings)
                            {
                                if (count == 0)
                                {
                                    key.whereSQL = key.whereSQL + "'" + substring.TrimStart().TrimEnd() + "'";
                                }
                                else
                                {
                                    key.whereSQL = key.whereSQL + ",'" + substring.TrimStart().TrimEnd() + "'";
                                }

                                count = count + 1;
                            }
                            key.whereSQL = key.whereSQL + ") ";
                            break;
                        case "NOT IN":
                            key.whereSQL = " tblCF_M_MonthYearQuater.Quater not in ( ";
                            substrings = Value.Split(',');
                            count = 0;
                            foreach (var substring in substrings)
                            {
                                if (count == 0)
                                {
                                    key.whereSQL = key.whereSQL + "'" + substring.TrimStart().TrimEnd() + "'";
                                }
                                else
                                {
                                    key.whereSQL = key.whereSQL + ",'" + substring.TrimStart().TrimEnd() + "'";
                                }

                                count = count + 1;
                            }
                            key.whereSQL = key.whereSQL + ") ";
                            break;
                        case "START WITH":
                            key.whereSQL = " tblCF_M_MonthYearQuater.Quater LIKE '" + Value + "%' ";

                            break;
                        case "END WITH":
                            key.whereSQL = " tblCF_M_MonthYearQuater.Quater like '%" + Value + "' ";

                            break;
                        case "ANY THAT HAVE":
                            key.whereSQL = " tblCF_M_MonthYearQuater.Quater like '%" + Value + "%' ";

                            break;
                        case "NOT EQUAL":
                            key.whereSQL = " tblCF_M_MonthYearQuater.Quater != '" + Value + "' ";

                            break;
                    }
                }



            }
            else
            {


                switch (map.ActionName)
                {
                    case "EQUAL":
                        if (column.IsTable)
                        {
                            key.innerJoin = " inner join " + column.IsTableNameInside + " on " + column.tblCF_M_File.tblName + "." + column.ColumnNameInside + " = " + column.IsTableNameInside + ".ID";

                            key.whereSQL = " " + column.IsTableNameInside + ".Name  = '" + Value + "' ";
                        }
                        else
                        {
                            key.whereSQL = " " + column.ColumnNameInside + " = '" + Value + "' ";
                        }
                        break;
                    case "LIKE":
                        if (column.IsTable)
                        {
                            key.innerJoin = " inner join " + column.IsTableNameInside + " on " + column.tblCF_M_File.tblName + "." + column.ColumnNameInside + " = " + column.IsTableNameInside + ".ID";

                            key.whereSQL = " " + column.IsTableNameInside + ".Name  like '%" + Value + "%' ";
                        }
                        else
                        {
                            key.whereSQL = " " + column.ColumnNameInside + "  like '%" + Value + "%' ";
                        }
                        break;
                    case "IN":
                        if (column.IsTable)
                        {
                            key.innerJoin = " inner join " + column.IsTableNameInside + " on " + column.tblCF_M_File.tblName + "." + column.ColumnNameInside + " = " + column.IsTableNameInside + ".ID";

                            key.whereSQL = " " + column.IsTableNameInside + ".Name  in ( ";

                            substrings = Value.Split(',');

                            count = 0;
                            foreach (var substring in substrings)
                            {
                                if (count == 0)
                                {
                                    key.whereSQL = key.whereSQL + "'" + substring.TrimStart().TrimEnd() + "'";
                                }
                                else
                                {
                                    key.whereSQL = key.whereSQL + ",'" + substring.TrimStart().TrimEnd() + "'";
                                }

                                count = count + 1;
                            }
                            key.whereSQL = key.whereSQL + ") ";
                        }
                        else
                        {
                            key.whereSQL = " " + column.ColumnNameInside + " in ( ";
                            substrings = Value.Split(',');

                            count = 0;
                            foreach (var substring in substrings)
                            {
                                if (count == 0)
                                {
                                    key.whereSQL = key.whereSQL + "'" + substring.TrimStart().TrimEnd() + "'";
                                }
                                else
                                {
                                    key.whereSQL = key.whereSQL + ",'" + substring.TrimStart().TrimEnd() + "'";
                                }

                                count = count + 1;
                            }
                            key.whereSQL = key.whereSQL + ") ";
                        }
                        break;
                    case "NOT IN":
                        if (column.IsTable)
                        {
                            key.innerJoin = " inner join " + column.IsTableNameInside + " on " + column.tblCF_M_File.tblName + "." + column.ColumnNameInside + " = " + column.IsTableNameInside + ".ID";

                            key.whereSQL = " " + column.IsTableNameInside + ".Name  not in ( ";

                            substrings = Value.Split(',');

                            count = 0;
                            foreach (var substring in substrings)
                            {
                                if (count == 0)
                                {
                                    key.whereSQL = key.whereSQL + "'" + substring.TrimStart().TrimEnd() + "'";
                                }
                                else
                                {
                                    key.whereSQL = key.whereSQL + ",'" + substring.TrimStart().TrimEnd() + "'";
                                }

                                count = count + 1;
                            }
                            key.whereSQL = key.whereSQL + ") ";
                        }
                        else
                        {
                            key.whereSQL = " " + column.ColumnNameInside + " not in ( ";
                            substrings = Value.Split(',');

                            count = 0;
                            foreach (var substring in substrings)
                            {
                                if (count == 0)
                                {
                                    key.whereSQL = key.whereSQL + "'" + substring.TrimStart().TrimEnd() + "'";
                                }
                                else
                                {
                                    key.whereSQL = key.whereSQL + ",'" + substring.TrimStart().TrimEnd() + "'";
                                }

                                count = count + 1;
                            }
                            key.whereSQL = key.whereSQL + ") ";
                        }
                        break;
                    case "START WITH":
                        if (column.IsTable)
                        {
                            key.innerJoin = " inner join " + column.IsTableNameInside + " on " + column.tblCF_M_File.tblName + "." + column.ColumnNameInside + " = " + column.IsTableNameInside + ".ID";

                            key.whereSQL = " " + column.IsTableNameInside + ".Name  = '" + Value + "%' ";
                        }
                        else
                        {
                            key.whereSQL = " " + column.ColumnNameInside + "  = '" + Value + "%' ";
                        }
                        break;
                    case "END WITH":
                        if (column.IsTable)
                        {
                            key.innerJoin = " inner join " + column.IsTableNameInside + " on " + column.tblCF_M_File.tblName + "." + column.ColumnNameInside + " = " + column.IsTableNameInside + ".ID";

                            key.whereSQL = " " + column.IsTableNameInside + ".Name  like '%" + Value + "' ";
                        }
                        else
                        {
                            key.whereSQL = " " + column.ColumnNameInside + "  like '%" + Value + "' ";
                        }
                        break;
                    case "ANY THAT HAVE":
                        if (column.IsTable)
                        {
                            key.innerJoin = " inner join " + column.IsTableNameInside + " on " + column.tblCF_M_File.tblName + "." + column.ColumnNameInside + " = " + column.IsTableNameInside + ".ID";

                            key.whereSQL = " " + column.IsTableNameInside + ".Name  like '%" + Value + "%' ";
                        }
                        else
                        {
                            key.whereSQL = " " + column.ColumnNameInside + "  like '%" + Value + "%' ";
                        }
                        break;
                    case "NOT EQUAL":
                        if (column.IsTable)
                        {
                            key.innerJoin = " inner join " + column.IsTableNameInside + " on " + column.tblCF_M_File.tblName + "." + column.ColumnNameInside + " = " + column.IsTableNameInside + ".ID";

                            key.whereSQL = " " + column.IsTableNameInside + ".Name  != '" + Value + "' ";
                        }
                        else
                        {
                            key.whereSQL = " " + column.ColumnNameInside + "  != '" + Value + "' ";
                        }
                        break;
                    case "GREATER THAN":
                        if (column.IsTable)
                        {
                            key.innerJoin = " inner join " + column.IsTableNameInside + " on " + column.tblCF_M_File.tblName + "." + column.ColumnNameInside + " = " + column.IsTableNameInside + ".ID";

                            key.whereSQL = " " + column.IsTableNameInside + ".Name  > '" + Value + "' ";
                        }
                        else
                        {
                            key.whereSQL = " " + column.ColumnNameInside + "  > '" + Value + "' ";
                        }
                        break;
                    case "LESS THAN":
                        if (column.IsTable)
                        {
                            key.innerJoin = " inner join " + column.IsTableNameInside + " on " + column.tblCF_M_File.tblName + "." + column.ColumnNameInside + " = " + column.IsTableNameInside + ".ID";

                            key.whereSQL = " " + column.IsTableNameInside + ".Name  < '" + Value + "' ";
                        }
                        else
                        {
                            key.whereSQL = " " + column.ColumnNameInside + " < '" + Value + "' ";
                        }
                        break;
                    case "GREATER THAN OR EQUAL":
                        if (column.IsTable)
                        {
                            key.innerJoin = " inner join " + column.IsTableNameInside + " on " + column.tblCF_M_File.tblName + "." + column.ColumnNameInside + " = " + column.IsTableNameInside + ".ID";

                            key.whereSQL = " " + column.IsTableNameInside + ".Name  >= '" + Value + "' ";
                        }
                        else
                        {
                            key.whereSQL = " " + column.ColumnNameInside + "  >= '" + Value + "' ";
                        }
                        break;
                    case "LESS THAN OR EQUAL":
                        if (column.IsTable)
                        {
                            key.innerJoin = " inner join " + column.IsTableNameInside + " on " + column.tblCF_M_File.tblName + "." + column.ColumnNameInside + " = " + column.IsTableNameInside + ".ID";

                            key.whereSQL = " " + column.IsTableNameInside + ".Name  <= '" + Value + "' ";
                        }
                        else
                        {
                            key.whereSQL = " " + column.ColumnNameInside + "  <= '" + Value + "' ";
                        }
                        break;
                    case "NOT EQUAL TO":
                        if (column.IsTable)
                        {
                            key.innerJoin = " inner join " + column.IsTableNameInside + " on " + column.tblCF_M_File.tblName + "." + column.ColumnNameInside + " = " + column.IsTableNameInside + ".ID";

                            key.whereSQL = " " + column.IsTableNameInside + ".Name  <> '" + Value + "' ";
                        }
                        else
                        {
                            key.whereSQL = " " + column.ColumnNameInside + "  <> '" + Value + "' ";
                        }
                        break;
                    case "BETWEEN":
                        if (column.IsTable)
                        {
                            key.innerJoin = " inner join " + column.IsTableNameInside + " on " + column.tblCF_M_File.tblName + "." + column.ColumnNameInside + " = " + column.IsTableNameInside + ".ID";

                            key.whereSQL = " " + column.IsTableNameInside + ".Name  BETWEEN  ";

                            substrings = Value.Split(',');

                            count = 0;
                            foreach (var substring in substrings)
                            {
                                if (count == 0)
                                {
                                    key.whereSQL = key.whereSQL + "'" + substring.TrimStart().TrimEnd() + "'";
                                }
                                else
                                {
                                    key.whereSQL = key.whereSQL + " and '" + substring.TrimStart().TrimEnd() + "'";
                                }

                                count = count + 1;
                            }
                            key.whereSQL = key.whereSQL + " ";
                        }
                        else
                        {
                            key.whereSQL = " " + column.ColumnNameInside + " BETWEEN  ";
                            substrings = Value.Split(',');

                            count = 0;
                            foreach (var substring in substrings)
                            {
                                if (count == 0)
                                {
                                    key.whereSQL = key.whereSQL + "'" + substring.TrimStart().TrimEnd() + "'";
                                }
                                else
                                {
                                    key.whereSQL = key.whereSQL + " and '" + substring.TrimStart().TrimEnd() + "'";
                                }

                                count = count + 1;
                            }
                            key.whereSQL = key.whereSQL + " ";
                        }
                        break;
                    case "YEAR":
                        if (column.IsTable)
                        {
                            key.innerJoin = " inner join " + column.IsTableNameInside + " on " + column.tblCF_M_File.tblName + "." + column.ColumnNameInside + " = " + column.IsTableNameInside + ".ID";

                            key.whereSQL = " datepart(year," + column.IsTableNameInside + ".Name)  = '" + Value + "' ";
                        }
                        else
                        {
                            key.whereSQL = "datepart(year, " + column.ColumnNameInside + ")  = '" + Value + "' ";
                        }
                        break;
                    case "MONTH":
                        if (column.IsTable)
                        {
                            key.innerJoin = " inner join " + column.IsTableNameInside + " on " + column.tblCF_M_File.tblName + "." + column.ColumnNameInside + " = " + column.IsTableNameInside + ".ID";

                            key.whereSQL = " datepart(month," + column.IsTableNameInside + ".Name)  = '" + Value + "' ";
                        }
                        else
                        {
                            key.whereSQL = " datepart(month," + column.ColumnNameInside + ")  = '" + Value + "' ";
                        }
                        break;
                    case "DAY":
                        if (column.IsTable)
                        {
                            key.innerJoin = " inner join " + column.IsTableNameInside + " on " + column.tblCF_M_File.tblName + "." + column.ColumnNameInside + " = " + column.IsTableNameInside + ".ID";

                            key.whereSQL = " datepart(day," + column.IsTableNameInside + ".Name)  = '" + Value + "' ";
                        }
                        else
                        {
                            key.whereSQL = " datepart(day," + column.ColumnNameInside + ")  = '" + Value + "' ";
                        }
                        break;
                    case "QUATER":
                        if (column.IsTable)
                        {
                            key.innerJoin = " inner join " + column.IsTableNameInside + " on " + column.tblCF_M_File.tblName + "." + column.ColumnNameInside + " = " + column.IsTableNameInside + ".ID";

                            key.whereSQL = " datepart(quater," + column.IsTableNameInside + ".Name)  = '" + Value + "' ";
                        }
                        else
                        {
                            key.whereSQL = " datepart(quater," + column.ColumnNameInside + ")  = '" + Value + "' ";
                        }
                        break;
                    case "FALSE":
                        if (column.IsTable)
                        {
                            key.innerJoin = " inner join " + column.IsTableNameInside + " on " + column.tblCF_M_File.tblName + "." + column.ColumnNameInside + " = " + column.IsTableNameInside + ".ID";

                            key.whereSQL = " " + column.IsTableNameInside + ".Name  = 0 ";
                        }
                        else
                        {
                            key.whereSQL = " " + column.ColumnNameInside + "  = 0 ";
                        }
                        break;
                    case "TRUE":
                        if (column.IsTable)
                        {
                            key.innerJoin = " inner join " + column.IsTableNameInside + " on " + column.tblCF_M_File.tblName + "." + column.ColumnNameInside + " = " + column.IsTableNameInside + ".ID";

                            key.whereSQL = " " + column.IsTableNameInside + ".Name  = 1 ";
                        }
                        else
                        {
                            key.whereSQL = " " + column.ColumnNameInside + "  = 1";
                        }
                        break;
                    case "FALSE & TRUE":
                        if (column.IsTable)
                        {
                            key.innerJoin = " inner join " + column.IsTableNameInside + " on " + column.tblCF_M_File.tblName + "." + column.ColumnNameInside + " = " + column.IsTableNameInside + ".ID";

                            key.whereSQL = " " + column.IsTableNameInside + ".Name  in (1,2)";
                        }
                        else
                        {
                            key.whereSQL = " " + column.ColumnNameInside + "  in (1,2) ";
                        }
                        break;
                }
            }


            key.RuleID = ID;
            key.ColumnID = ColumnID;
            key.ColumnActionID = ColumnActionID;
            key.ValueSearch = Value;


            DAO.tblCF_Key.Add(key);

            DAO.SaveChanges();


            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_Key
                    join
c in DAO.tblCF_M_FileColumn on p.ColumnID equals c.ID
                    join
t in DAO.tblCF_M_MapActions on p.ColumnActionID equals t.ID
                    where p.RuleID == ID
                    select new
                    {
                        p.ID,
                        p.RuleID,
                        c.ColumnName,
                        t.ActionName,
                        p.ValueSearch
                    }
            });

            return Content(o["item"].ToString());
        }


        [Route("getCF_AskYearBU")]
        public ActionResult getCF_AskYearBU(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["ID"].ToString());

            tblCF_CF_Rule Rule = (from p in DAO.tblCF_CF_Rule where p.ID == ID select p).SingleOrDefault<tblCF_CF_Rule>();
            

            JObject o = null;
            o = JObject.FromObject(new
            {
                item = from p in DAO.tblCF_M_FileRegion 
                       where p.FileID == Rule.FileID
                       select new
                       {
                           p.AskMonthYear,
                           p.AskBU
                       }
            });

            return Content(o["item"].ToString());
        }


        [Route("addRuleBuildKey")]
        public ActionResult addRuleBuildKey(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["ID"].ToString());
            int Type = Convert.ToInt32(objParameter["Type"].ToString());
            string Value = objParameter["Value"].ToString();

            List<tblCF_RuleBuildKey> ListKey = (from p in DAO.tblCF_RuleBuildKey where p.Type == Type && p.RuleID == ID select p).ToList<tblCF_RuleBuildKey>();

            

            tblCF_RuleBuildKey NewKey = new tblCF_RuleBuildKey();

            NewKey.RuleID = ID;
            NewKey.Value = Value;
            NewKey.Type = Type;
            NewKey.StringOrder = ListKey.Count() + 1;

            DAO.tblCF_RuleBuildKey.Add(NewKey);

            DAO.SaveChanges();

            string keyFormatter = "";

            foreach (tblCF_RuleBuildKey c in ListKey)
            {
                keyFormatter = keyFormatter + c.Value;
            }

            keyFormatter = keyFormatter + Value;

            return Content(keyFormatter.ToString());
        }


        [Route("deleteRuleBuildKey")]
        public ActionResult deleteRuleBuildKey(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["ID"].ToString());
            int Type = Convert.ToInt32(objParameter["Type"].ToString());

            try
            {
               
                List<tblCF_RuleBuildKey> ListKey = (from p in DAO.tblCF_RuleBuildKey where p.Type == Type && p.RuleID == ID select p).ToList<tblCF_RuleBuildKey>();

                int count = ListKey.Count();

                tblCF_RuleBuildKey NewKey = (from p in DAO.tblCF_RuleBuildKey where p.RuleID == ID && p.Type == Type && p.StringOrder == count select p).SingleOrDefault<tblCF_RuleBuildKey>();

                DAO.tblCF_RuleBuildKey.Remove(NewKey);

                DAO.SaveChanges();
            }
            catch { }
            List<tblCF_RuleBuildKey> ListKeyNew = (from p in DAO.tblCF_RuleBuildKey where p.Type == Type && p.RuleID == ID select p).ToList<tblCF_RuleBuildKey>();

            string keyFormatter = "";

            foreach (tblCF_RuleBuildKey c in ListKeyNew)
            {
                keyFormatter = keyFormatter + c.Value;
            }

            return Content(keyFormatter.ToString());
        }


        [Route("getRuleBuildKey")]
        public ActionResult getRuleBuildKey(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["ID"].ToString());
            int Type = Convert.ToInt32(objParameter["Type"].ToString());

            List<tblCF_RuleBuildKey> ListKey = (from p in DAO.tblCF_RuleBuildKey where p.Type == Type && p.RuleID == ID select p).ToList<tblCF_RuleBuildKey>();

            string keyFormatter = "";

            foreach (tblCF_RuleBuildKey c in ListKey)
            {
                keyFormatter = keyFormatter + c.Value;
            }

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblCF_RuleBuildKey where p.Type == Type && p.RuleID == ID 
                    orderby p.StringOrder 
                    select new
                    {
                        p.RuleID ,
                        p.StringOrder,
                        p.Type,
                        p.Value
                    }
            });

            return Content(o["item"].ToString());
        }


        [Route("removeRuleBuildKey")]
        public ActionResult removeRuleBuildKey(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["ID"].ToString());
          
            List<tblCF_RuleBuildKey> ListKey = (from p in DAO.tblCF_RuleBuildKey where p.RuleID == ID select p).ToList<tblCF_RuleBuildKey>();

            foreach (tblCF_RuleBuildKey c in ListKey)
            {
                DAO.tblCF_RuleBuildKey.Remove(c);
            }

            DAO.SaveChanges();

            return Content("");
        }

        [Route("saveRuleBuildKey")]
        public ActionResult saveRuleBuildKey(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["ID"].ToString());
            int Type = Convert.ToInt32(objParameter["Type"].ToString());
            int order = Convert.ToInt32(objParameter["ORDER"].ToString());
            string VALUE = objParameter["Value"].ToString();

            tblCF_RuleBuildKey NewKey = new tblCF_RuleBuildKey();

            NewKey.RuleID = ID;
            NewKey.Value = VALUE;
            NewKey.Type = Type;
            NewKey.StringOrder = order;

            DAO.tblCF_RuleBuildKey.Add(NewKey);

            DAO.SaveChanges();


            return Content("");
        }

        [Route("add_CF_RuleConditional")]
        public ActionResult add_CF_RuleConditional(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int AccountID = Convert.ToInt32(objParameter["AccountID"].ToString());
            string mask = objParameter["mask"].ToString();
            string conditional = objParameter["conditional"].ToString();
            string FinalKey = objParameter["FinalKey"].ToString();

            int ActionOnResult = 1007;

            try
            {
                ActionOnResult = Convert.ToInt32(objParameter["ActionOnResult"].ToString());
            }
            catch
            {

            }

            tblCF_CF_Account accountapply = (from p in DAO.tblCF_CF_Account where p.ID == AccountID select p).SingleOrDefault<tblCF_CF_Account>();


            tblCF_CF_Rule Rule = new tblCF_CF_Rule();

            Rule.CFAccountID = AccountID;
            Rule.Value = 0;
            Rule.VarcharInKey = "";
            DAO.tblCF_CF_Rule.Add(Rule);
            DAO.SaveChanges();
            Rule.VarcharInKey = "@K" + Rule.ID.ToString();
            Rule.Mask = mask;
            Rule.ActionOnResult = ActionOnResult;
            Rule.IsConditionalKey = true;
            Rule.Conditional = conditional;
            Rule.FinalKey = FinalKey;

            DAO.SaveChanges();


            var query1 = (from p in DAO.tblCF_CF_Rule
                          join action in DAO.tblCF_Key_PreAction on p.ActionOnResult equals action.ID
                          join f in DAO.tblCF_M_File on p.FileID equals f.ID into data_file
                          from file in data_file.DefaultIfEmpty()
                          join c in DAO.tblCF_M_FileColumn on p.ColumnID equals c.ID into data_Columns
                          from columns in data_Columns.DefaultIfEmpty()
                          where p.CFAccountID == AccountID && p.IsConditionalKey == false
                          select new
                          {
                              p.ID,
                              p.CFAccountID,
                              file.Name,
                              columns.ColumnName,
                              ActionColumn = p.tblCF_Key_Action.Action,
                              p.Value,
                              p.VarcharInKey,
                              p.Mask,
                              ActionOnResult = action.Action,
                              p.IsConditionalKey,
                              p.Conditional,
                              p.FinalKey
                          }).ToList();

            var query2 = (from p in DAO.tblCF_CF_Rule
                          join action in DAO.tblCF_Key_PreAction on p.ActionOnResult equals action.ID
                          join f in DAO.tblCF_M_File on p.FileID equals f.ID into data_file
                          from file in data_file.DefaultIfEmpty()
                          join c in DAO.tblCF_M_FileColumn on p.ColumnID equals c.ID into data_Columns
                          from columns in data_Columns.DefaultIfEmpty()
                          where p.CFAccountID == AccountID && p.IsConditionalKey == true
                          select new
                          {
                              p.ID,
                              p.CFAccountID,
                              Name = p.Conditional,
                              ColumnName = p.FinalKey,
                              ActionColumn = p.tblCF_Key_Action.Action,
                              p.Value,
                              p.VarcharInKey,
                              p.Mask,
                              ActionOnResult = "Is Conditional",
                              p.IsConditionalKey,
                              p.Conditional,
                              p.FinalKey
                          }).ToList();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in query1.Concat(query2)
                    select new
                    {
                        p.ID,
                        p.CFAccountID,
                        p.Name,
                        p.ColumnName,
                        p.ActionColumn,
                        p.Value,
                        p.VarcharInKey,
                        p.Mask,
                        p.ActionOnResult,
                        p.IsConditionalKey,
                        p.Conditional,
                        p.FinalKey
                    }
            });

            return Content(o["item"].ToString());
        }

        [Route("update_CF_Rule")]
        public ActionResult update_CF_Rule(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["ID"].ToString());
            int ColumnId = Convert.ToInt32(objParameter["ColumnId"].ToString());
            int TypeActionColumn = Convert.ToInt32(objParameter["TypeActionColumn"].ToString());
            string mask = objParameter["mask"].ToString();
            int ActionOnResult = 1007;

            try
            {
                ActionOnResult = Convert.ToInt32(objParameter["ActionOnResult"].ToString());
            }
            catch
            {

            }

            tblCF_CF_Rule Rule = (from p in DAO.tblCF_CF_Rule where p.ID == ID select p).SingleOrDefault<tblCF_CF_Rule>();

            Rule.ColumnID = ColumnId;
            Rule.TypeActionInColumn = TypeActionColumn;
            Rule.Mask = mask;
            Rule.ActionOnResult = ActionOnResult;

            DAO.SaveChanges();

            

            var query1 = (from p in DAO.tblCF_CF_Rule
                          join action in DAO.tblCF_Key_PreAction on p.ActionOnResult equals action.ID
                          join f in DAO.tblCF_M_File on p.FileID equals f.ID into data_file
                          from file in data_file.DefaultIfEmpty()
                          join c in DAO.tblCF_M_FileColumn on p.ColumnID equals c.ID into data_Columns
                          from columns in data_Columns.DefaultIfEmpty()
                          where p.CFAccountID == Rule.CFAccountID && p.IsConditionalKey == false
                          select new
                          {
                              p.ID,
                              p.CFAccountID,
                              file.Name,
                              columns.ColumnName,
                              ActionColumn = p.tblCF_Key_Action.Action,
                              p.Value,
                              p.VarcharInKey,
                              p.Mask,
                              ActionOnResult = action.Action,
                              p.IsConditionalKey,
                              p.Conditional,
                              p.FinalKey
                          }).ToList();

            var query2 = (from p in DAO.tblCF_CF_Rule
                          join action in DAO.tblCF_Key_PreAction on p.ActionOnResult equals action.ID
                          join f in DAO.tblCF_M_File on p.FileID equals f.ID into data_file
                          from file in data_file.DefaultIfEmpty()
                          join c in DAO.tblCF_M_FileColumn on p.ColumnID equals c.ID into data_Columns
                          from columns in data_Columns.DefaultIfEmpty()
                          where p.CFAccountID == Rule.CFAccountID && p.IsConditionalKey == true
                          select new
                          {
                              p.ID,
                              p.CFAccountID,
                              Name = p.Conditional,
                              ColumnName = p.FinalKey,
                              ActionColumn = p.tblCF_Key_Action.Action,
                              p.Value,
                              p.VarcharInKey,
                              p.Mask,
                              ActionOnResult = "Is Conditional",
                              p.IsConditionalKey,
                              p.Conditional,
                              p.FinalKey
                          }).ToList();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in query1.Concat(query2)
                    select new
                    {
                        p.ID,
                        p.CFAccountID,
                        p.Name,
                        p.ColumnName,
                        p.ActionColumn,
                        p.Value,
                        p.VarcharInKey,
                        p.Mask,
                        p.ActionOnResult,
                        p.IsConditionalKey,
                        p.Conditional,
                        p.FinalKey
                    }
            });

            return Content(o["item"].ToString());
        }

        [Route("update_CF_RuleConditional")]
        public ActionResult update_CF_RuleConditional(string data)
        {
            CashFlowEntities DAO = new CashFlowEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["ID"].ToString());
            string mask = objParameter["mask"].ToString();
            string conditional = objParameter["conditional"].ToString();
            string FinalKey = objParameter["FinalKey"].ToString();

            int ActionOnResult = 1007;

            try
            {
                ActionOnResult = Convert.ToInt32(objParameter["ActionOnResult"].ToString());
            }
            catch
            {

            }

            tblCF_CF_Rule Rule = (from p in DAO.tblCF_CF_Rule where p.ID == ID select p).SingleOrDefault<tblCF_CF_Rule>();

            Rule.Mask = mask;
            Rule.ActionOnResult = ActionOnResult;
            Rule.IsConditionalKey = true;
            Rule.Conditional = conditional;
            Rule.FinalKey = FinalKey;

            DAO.SaveChanges();


            var query1 = (from p in DAO.tblCF_CF_Rule
                          join action in DAO.tblCF_Key_PreAction on p.ActionOnResult equals action.ID
                          join f in DAO.tblCF_M_File on p.FileID equals f.ID into data_file
                          from file in data_file.DefaultIfEmpty()
                          join c in DAO.tblCF_M_FileColumn on p.ColumnID equals c.ID into data_Columns
                          from columns in data_Columns.DefaultIfEmpty()
                          where p.CFAccountID == Rule.CFAccountID && p.IsConditionalKey == false
                          select new
                          {
                              p.ID,
                              p.CFAccountID,
                              file.Name,
                              columns.ColumnName,
                              ActionColumn = p.tblCF_Key_Action.Action,
                              p.Value,
                              p.VarcharInKey,
                              p.Mask,
                              ActionOnResult = action.Action,
                              p.IsConditionalKey,
                              p.Conditional,
                              p.FinalKey
                          }).ToList();

            var query2 = (from p in DAO.tblCF_CF_Rule
                          join action in DAO.tblCF_Key_PreAction on p.ActionOnResult equals action.ID
                          join f in DAO.tblCF_M_File on p.FileID equals f.ID into data_file
                          from file in data_file.DefaultIfEmpty()
                          join c in DAO.tblCF_M_FileColumn on p.ColumnID equals c.ID into data_Columns
                          from columns in data_Columns.DefaultIfEmpty()
                          where p.CFAccountID == Rule.CFAccountID && p.IsConditionalKey == true
                          select new
                          {
                              p.ID,
                              p.CFAccountID,
                              Name = p.Conditional,
                              ColumnName = p.FinalKey,
                              ActionColumn = p.tblCF_Key_Action.Action,
                              p.Value,
                              p.VarcharInKey,
                              p.Mask,
                              ActionOnResult = "Is Conditional",
                              p.IsConditionalKey,
                              p.Conditional,
                              p.FinalKey
                          }).ToList();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in query1.Concat(query2)
                    select new
                    {
                        p.ID,
                        p.CFAccountID,
                        p.Name,
                        p.ColumnName,
                        p.ActionColumn,
                        p.Value,
                        p.VarcharInKey,
                        p.Mask,
                        p.ActionOnResult,
                        p.IsConditionalKey,
                        p.Conditional,
                        p.FinalKey
                    }
            });

            return Content(o["item"].ToString());
        }

    }


}