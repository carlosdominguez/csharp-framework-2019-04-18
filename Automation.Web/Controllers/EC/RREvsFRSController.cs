﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Automation.Web.Models.EC;


using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace Automation.Web.Controllers.EC
{

    [RoutePrefix("EC")]
    public class RREvsFRSController : Controller
    {
        [Route("RREvsFRS")]
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            string subAppPath = GlobalUtil.GetSubAppPath();
            viewModel.Add("SubAppPath", subAppPath);
            return (View("~/Views/EC/RREvsFRS.cshtml", viewModel));

        }


        [ValidateInput(false)]
        [Route("uploadReenFile")]
        public ActionResult uploadReenFile(string data)
        {


            var objParameter = JsonConvert.DeserializeObject(data);

            DataTable ptable = (DataTable)JsonConvert.DeserializeObject(data, (typeof(DataTable)));

            (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[procEC_UploadRRE_FRS_ManualUpload]", new List<Dictionary<string, string>> { 
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEID" },
                                { "Value", GlobalUtil.GetSOEID() }
                            }
                        });

            return null;

        }

        [Route("getVersionList")]
        public ActionResult getVersionList()
        {

            QMemoDBEntities2 DAO = new QMemoDBEntities2();


            tblEC_File file = (from p in DAO.tblEC_File where p.Name == "RRE QMemo" select p).SingleOrDefault<tblEC_File>();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblEC_DataSourceFile
                    where p.FileID == file.ID
                    orderby p.ID descending
                    select new
                    {
                        p.ID,
                        p.UploadBy,
                        p.UploadDate
                    }

            });

            return Content(o["item"].ToString());

        }

        [Route("getListReeng")]
        public ActionResult getListReeng(string data)
        {
            QMemoDBEntities2 DAO = new QMemoDBEntities2();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            int pFile = Convert.ToInt32(objParameter["pFile"].ToString());

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblEC_FileData
                    where p.DataSourceFileID == pFile && p.DataSourceFileID == pFile - 1
                    orderby p.ID descending
                    select new
                    {
                        p.ID,
                        p.tblEC_FileShortNameDescr.Name,
                        p.tblEC_FileShortNameDescr.Description,
                        p.tblEC_IceCodeMapping.IceCode,
                        p.BU,
                        p.Balance,
                        Source = p.tblEC_DataSourceFile.tblEC_File.Name
                    }

            });

            return Content(o["item"].ToString());
        }

    }
}
