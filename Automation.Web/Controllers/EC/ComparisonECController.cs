﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

using Automation.Web.Models.EC;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text.RegularExpressions;


namespace Automation.Web.Controllers.EC
{
    [RoutePrefix("EC")]
    public class ComparisonECController : Controller
    {

        //
        // GET: /
        [Route("ComparisonEC")]
        public ActionResult Index()
        {
            return View("~/Views/EC/ComparisonEC.cshtml");
        }


        [Route("getEditCheckSystems")]
        public ActionResult getEditCheckSystems(string data)
        {
            QMemoDBEntities2 DAO = new QMemoDBEntities2();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            string RoleID = objParameter["RoleID"].ToString();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblEC_Business
                    select new
                    {
                        p.ID,
                        p.BU,
                        p.Name
                    }
            });


            return Content(o["item"].ToString());

        }

        [Route("getPeriod")]
        public ActionResult getPeriod(string data)
        {
            QMemoDBEntities2 DAO = new QMemoDBEntities2();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.SP_EditCheck_GetPeriod()
                    select new
                    {
                        Value = p.Period,
                        ID = p.Period
                    }
            });


            return Content(o["item"].ToString());

        }


        [Route("getBusiness")]
        public ActionResult getBusiness(string data)
        {
            QMemoDBEntities2 DAO = new QMemoDBEntities2();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string SOEID = objParameter["pSOEID"].ToString();
            string Perio = objParameter["pPeriod"].ToString();

            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procEC_GetGridBusiness(SOEID, Perio)
                    select new
                    {
                        p.BusinessID,
                        p.Name,
                        p.Pass1,
                        p.Fail1,
                        p.Lift1,
                        p.Total1,
                        p.Header1,
                        p.Pass2,
                        p.Fail2,
                        p.Lift2,
                        p.Total2,
                        p.Header2


                    }
            });


            return Content(o["item"].ToString());

        }

        [Route("procEC_GetGridBusinessDetailCompare")]
        public ActionResult procEC_GetGridBusinessDetailCompare(string data)
        {
            QMemoDBEntities2 DAO = new QMemoDBEntities2();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string SOEID = objParameter["pSOEID"].ToString();
            string Perio = objParameter["pPeriod"].ToString();
            int runID = Convert.ToInt32(objParameter["pRunID"].ToString());

            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procEC_GetGridBusinessDetailCompare(SOEID, Perio, runID)
                    select new
                    {
                        p.ID,
                        p.Description,
                        p.CheckID,
                        p.Business1Left,
                        p.Business2Left,
                        p.VarianceLeft,
                        p.Operator,
                        p.Business1Right,
                        p.Business2Right,
                        p.VarianceRight,
                        p.Val,
                        p.B1ID,
                        p.B2ID,
                        p.VAL1,
                        p.VAL2

                    }
            });


            return Content(o["item"].ToString());

        }

        [Route("procEC_RunEditCheckMultiple")]
        public ActionResult procEC_RunEditCheckMultiple(string data)
        {
            QMemoDBEntities2 DAO = new QMemoDBEntities2();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string SOEID = objParameter["pSOEID"].ToString();
            string Perio = objParameter["pPeriod"].ToString();
            string BU = objParameter["pBU"].ToString();
            int runID = Convert.ToInt32(objParameter["pRunID"].ToString());
            string Product = objParameter["pProduct"].ToString();
            string type  = objParameter["pType"].ToString();
            string BU2 = objParameter["pBU2"].ToString();

            Dictionary <String, String> obj = new Dictionary<string, string>();

            DAO.procEC_RunEditCheckMultiple(runID, Perio, SOEID, BU, Product,type, BU2);


            return Content("");

        }


        [Route("procEC_GetGridBusinessDetailCompareInside")]
        public ActionResult procEC_GetGridBusinessDetailCompareInside(string data)
        {
            QMemoDBEntities2 DAO = new QMemoDBEntities2();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string SOEID = objParameter["pSOEID"].ToString();
            string Perio = objParameter["pPeriod"].ToString();
            string runID = objParameter["pRunID"].ToString();

            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procEC_GetGridBusinessDetailCompareInside(SOEID, Perio, runID)
                    select new
                    {
                        p.ID,
                        p.LSing,
                        p.LAccount,
                        p.LAccountDescr,
                        p.LIceCode,
                        p.LB1,
                        p.LB2,
                        p.LVariance,
                        p.RSing,
                        p.RAccount,
                        p.RAccountDescr,
                        p.RIceCode,
                        p.RB1,
                        p.RB2,
                        p.RVariance,
                        p.BUL,
                        p.BUR,
                        p.LPrior,
                        p.RPrior

                    }
            });


            return Content(o["item"].ToString());

        }



        [Route("procEC_GetFilter")]
        public ActionResult procEC_GetFilter(string data)
        {
            QMemoDBEntities2 DAO = new QMemoDBEntities2();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int pID = Convert.ToInt32(objParameter["pID"].ToString());
            string PerioD = objParameter["pPeriod"].ToString();
            string Type = objParameter["pType"].ToString();
            string Soeid = objParameter["pSOEID"].ToString();

            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procEC_GetFilter(pID, PerioD, Type, Soeid)
                    select new
                    {
                        p.ID,
                        p.Value

                    }
            });


            return Content(o["item"].ToString());

        }

        [Route("procEC_GetB1B2")]
        public ActionResult procEC_GetB1B2(string data)
        {
            QMemoDBEntities2 DAO = new QMemoDBEntities2();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int pID = Convert.ToInt32(objParameter["pID"].ToString());
            string PerioD = objParameter["pPeriod"].ToString();

            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procEC_GetB1B2(pID, PerioD)
                    select new
                    {
                        p.ID,
                        p.SubBusinessName

                    }
            });


            return Content(o["item"].ToString());

        }


        [Route("procEC_GetGridBusinessDetailCompareFilter")]
        public ActionResult procEC_GetGridBusinessDetailCompareFilter(string data)
        {
            QMemoDBEntities2 DAO = new QMemoDBEntities2();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string SOEID = objParameter["pSOEID"].ToString();
            string Perio = objParameter["pPeriod"].ToString();
            int runID = Convert.ToInt32(objParameter["pRunID"].ToString());

            string checkID = objParameter["pCHECKID"].ToString();
            string OPERATOR = objParameter["pOPERATOR"].ToString();
            string VALIDATION = objParameter["pVALIDATION"].ToString();
            string ACCOUNT = objParameter["pACCOUNT"].ToString();

            string Product = objParameter["pProduct"].ToString();

            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procEC_GetGridBusinessDetailCompareFilter(SOEID, Perio, runID, checkID, OPERATOR, VALIDATION, ACCOUNT, Product)
                    select new
                    {
                        p.ID,
                        p.Description,
                        p.CheckID,
                        p.Business1Left,
                        p.Business2Left,
                        p.VarianceLeft,
                        p.Operator,
                        p.Business1Right,
                        p.Business2Right,
                        p.VarianceRight,
                        p.Val,
                        p.B1ID,
                        p.B2ID,
                        p.VAL1,
                        p.VAL2

                    }
            });


            return Content(o["item"].ToString());

        }


        [Route("procEC_GetFilterBU")]
        public ActionResult procEC_GetFilterBU(string data)
        {
            QMemoDBEntities2 DAO = new QMemoDBEntities2();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int pID = Convert.ToInt32(objParameter["pID"].ToString());
            string PerioD = objParameter["pPeriod"].ToString();

            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procEC_GetFilterBU(pID, PerioD)
                    select new
                    {
                        p.ID,
                        p.Value

                    }
            });


            return Content(o["item"].ToString());

        }


        [Route("procEC_GetCBBU")]
        public ActionResult procEC_GetCBBU(string data)
        {
            QMemoDBEntities2 DAO = new QMemoDBEntities2();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int pID = Convert.ToInt32(objParameter["pID"].ToString());
            string PerioD = objParameter["pPeriod"].ToString();

            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procEC_GetFilterBU(pID, PerioD)
                    select new
                    {
                        p.ID,
                        p.Value

                    }
            });


            return Content(o["item"].ToString());

        }

        [Route("procEC_GetCBSelQMemo")]
        public ActionResult procEC_GetCBSelQMemo(string data)
        {
            QMemoDBEntities2 DAO = new QMemoDBEntities2();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int pID = Convert.ToInt32(objParameter["pID"].ToString());
            string PerioD = objParameter["pPeriod"].ToString();

            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procEC_GetFilterBU(pID, PerioD)
                    select new
                    {
                        p.ID,
                        p.Value

                    }
            });


            return Content(o["item"].ToString());

        }

    }
}