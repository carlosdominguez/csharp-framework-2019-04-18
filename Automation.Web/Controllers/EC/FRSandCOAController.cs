﻿
using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Automation.Web.Models.EC;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace Automation.Web.Controllers.EC
{

        [RoutePrefix("EC")]
        public class FRSandCOAController : Controller
        {
            [Route("FRSandCOA")]
            public ActionResult Index()
            {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            string subAppPath = GlobalUtil.GetSubAppPath();
            viewModel.Add("SubAppPath", subAppPath);
            return (View("~/Views/EC/FRSandCOA.cshtml", viewModel));

            }

            [ValidateInput(false)]
            [Route("uploadReenFile")]
            public ActionResult uploadReenFile(string data)
            {


                var objParameter = JsonConvert.DeserializeObject(data);

                DataTable ptable = (DataTable)JsonConvert.DeserializeObject(data, (typeof(DataTable)));

      

            (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[procEC_STGUploadFRSandCOA]", new List<Dictionary<string, string>> {
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEID" },
                                { "Value", GlobalUtil.GetSOEID() }
                            }
                        });

                return null;

            }



        }
 
}