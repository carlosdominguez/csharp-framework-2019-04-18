﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

using Automation.Web.Models.EC;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace Automation.Web.Controllers.EC
{

    [RoutePrefix("EC")]
    public class QMemoECController : Controller
    {

        //
        // GET: /
        [Route("QMemoEC")]
        public ActionResult Index()
        {
            return View("~/Views/EC/QMemoEC.cshtml");
        }


        [Route("getBusiness")]
        public ActionResult getBusiness(string data)
        {
            QMemoDBEntities2 DAO = new QMemoDBEntities2();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string SOEID = objParameter["pSOEID"].ToString();
            string Perio = objParameter["pPeriod"].ToString();

            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procEC_GetGridBusinessSingle(SOEID, Perio)
                    select new
                    {
                        p.BusinessID,
                        p.Name,
                        p.Pass,
                        p.Fail,
                        p.Lift,
                        p.Total


                    }
            });


            return Content(o["item"].ToString());

        }

        

        [Route("procEC_RunEditCheck")]
        public ActionResult procEC_RunEditCheck(string data)
        {
            QMemoDBEntities2 DAO = new QMemoDBEntities2();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int ID = Convert.ToInt32(objParameter["pID"].ToString());
            string SOEID = objParameter["pSOEID"].ToString();
            string Perio = objParameter["pPeriod"].ToString();

            DAO.procEC_RunEditCheck(ID, Perio, SOEID);


            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procEC_GetGridBusinessSingle2(SOEID,Perio,ID)
                    select new
                    {
                        p.BusinessID,
                        p.Name,
                        p.Pass,
                        p.Fail,
                        p.Lift,
                        p.Total


                    }
            });


            return Content(o["item"].ToString());

        }


        [Route("procEC_GetGridBusinessDetailCompare")]
        public ActionResult procEC_GetGridBusinessDetailCompare(string data)
        {
            QMemoDBEntities2 DAO = new QMemoDBEntities2();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string SOEID = objParameter["pSOEID"].ToString();
            string Perio = objParameter["pPeriod"].ToString();
            int runID = Convert.ToInt32(objParameter["pRunID"].ToString());

            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procEC_GetGridBusinessDetail(SOEID, Perio, runID)
                    select new
                    {
                        p.ID,
                        p.Description,
                        p.CheckID,
                        p.Business1Left,
                        p.Operator,
                        p.Business1Right,
                        p.VAL1,
                        p.RULEID

                    }
            });


            return Content(o["item"].ToString());

        }


        [Route("procEC_GetGridBusinessDetailCompareInside")]
        public ActionResult procEC_GetGridBusinessDetailCompareInside(string data)
        {
            QMemoDBEntities2 DAO = new QMemoDBEntities2();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string SOEID = objParameter["pSOEID"].ToString();
            string Perio = objParameter["pPeriod"].ToString();
            string runID = objParameter["pRunID"].ToString();

            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procEC_GetGridBusinessDetailInside(SOEID, Perio, runID)
                    select new
                    {
                        p.ID,
                        p.LSing,
                        p.LAccount,
                        p.LAccountDescr,
                        p.LIceCode,
                        p.LB1,
                        p.RSing,
                        p.RAccount,
                        p.RAccountDescr,
                        p.RIceCode,
                        p.RB1,
                        p.BUL,
                        p.BUR,
                        p.LPrior,
                        p.RPrior

                    }
            });


            return Content(o["item"].ToString());

        }




        [Route("procEC_GetGridBusinessDetailCompareFilter")]
        public ActionResult procEC_GetGridBusinessDetailCompareFilter(string data)
        {
            QMemoDBEntities2 DAO = new QMemoDBEntities2();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string SOEID = objParameter["pSOEID"].ToString();
            string Perio = objParameter["pPeriod"].ToString();
            int runID = Convert.ToInt32(objParameter["pRunID"].ToString());

            string checkID = objParameter["pCHECKID"].ToString();
            string OPERATOR = objParameter["pOPERATOR"].ToString();
            string VALIDATION = objParameter["pVALIDATION"].ToString();
            string ACCOUNT = objParameter["pACCOUNT"].ToString();

            string Product = objParameter["pProduct"].ToString();
            string ManagedSegment = objParameter["pMS"].ToString();
            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procEC_GetGridBusinessDetailFilter(SOEID, Perio, runID, checkID, OPERATOR, VALIDATION, ACCOUNT, Product,ManagedSegment)
                    select new
                    {
                        p.ID,
                        p.Description,
                        p.CheckID,
                        p.Business1Left,
                        p.Operator,
                        p.Business1Right,
                        p.VAL1,
                        p.RULEID

                    }
            });


            return Content(o["item"].ToString());

        }


        [Route("procEC_GetBusinessFilter")]
        public ActionResult procEC_GetBusinessFilter(string data)
        {
            QMemoDBEntities2 DAO = new QMemoDBEntities2();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string Name = objParameter["pName"].ToString();


            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procEC_GetBusinessFilter(Name)
                    select new
                    {
                        p.ID,
                        Value = p.NAME


                    }
            });


            return Content(o["item"].ToString());

        }


        [Route("procEC_GetInfoQMemo")]
        public ActionResult procEC_GetInfoQMemo(string data)
        {
            QMemoDBEntities2 DAO = new QMemoDBEntities2();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int shortid = Convert.ToInt32(objParameter["shortid"].ToString());

            string period = objParameter["period"].ToString();


            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procEC_GetInfoQMemo(shortid, period)
                    select new
                    {
                        p.Name,
                        p.Year,
                        p.Month,
                        p.Quater,
                        p.Value,
                        p.PriorMonth,
                        p.FileType
                    }
            });


            return Content(o["item"].ToString());

        }



        [Route("procEC_UploadManualTemplateAdjustment")]
        public ActionResult procEC_UploadManualTemplateAdjustment(string data)
        {
            QMemoDBEntities2 DAO = new QMemoDBEntities2();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


            string period = objParameter["period"].ToString();
            string bu = objParameter["bu"].ToString();
            int account = Convert.ToInt32(objParameter["account"].ToString());
            decimal balance = Convert.ToDecimal(objParameter["balance"].ToString());
            string business = objParameter["business"].ToString();
            string sOEID_UPLOADER = objParameter["sOEID_UPLOADER"].ToString();
            string fILE_PATH = objParameter["fILE_PATH"].ToString();


            string QMEMO = (from p in DAO.tblEC_FileShortNameDescr where p.ID == account select p).SingleOrDefault<tblEC_FileShortNameDescr>().Name;

            DAO.procEC_UploadManualTemplateAdjustment(period, bu, QMEMO, balance, business, sOEID_UPLOADER, fILE_PATH);
                  


            return Content("true");

        }
    }
}