﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace Automation.Web.Controllers.SalesAndUse
{
    [RoutePrefix("SalesAndUse")]
    public class NewCalendarController : Controller
    {
        [Route("NewCalendar")]
        public ActionResult Filings()
        {
            return View("~/Views/SalesAndUse/NewCalendar.cshtml");
        }

        [ValidateInput(false)]
        [Route("GenerateNewCalendar")]
        public ActionResult GenerateNewCalendar(string soeid)
        {
            var spName = "NSP_YE_GENERATE_FILING_CALENDAR";


            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                }
                #endregion
            });


            return Content("Saved!");
        }
    }
}
