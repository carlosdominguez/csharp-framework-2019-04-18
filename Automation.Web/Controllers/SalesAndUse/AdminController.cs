﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace Automation.Web.Controllers.SalesAndUse
{
    [RoutePrefix("SalesAndUse")]
    public class AdminController : Controller
    {
        [Route("Admin")]
        public ActionResult Admin()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            var tblName = (String.IsNullOrEmpty(Request.QueryString.Get("ptbl"))) ? "" : Request.QueryString.Get("ptbl");
            var colKey = (String.IsNullOrEmpty(Request.QueryString.Get("pcolKey"))) ? "" : Request.QueryString.Get("pcolKey");
            var idElementContent = (String.IsNullOrEmpty(Request.QueryString.Get("pidElementContent"))) ? "" : Request.QueryString.Get("pidElementContent");

            viewModel.Add("TableName", tblName);
            viewModel.Add("ColumnKey", colKey);
            viewModel.Add("IDElementContent", idElementContent);

            //if (String.IsNullOrEmpty(tblName))
            //{
            //    GlobalUtil.AddSessionMessage("info", "Query String Parameter for Table Name [ptbl] is missing.", true);
            //}

            //if (String.IsNullOrEmpty(colKey))
            //{
            //    GlobalUtil.AddSessionMessage("info", "Query String Parameter for Column Key [pcolKey] is missing.", true);
            //}

            //if (String.IsNullOrEmpty(idElementContent))
            //{
            //    GlobalUtil.AddSessionMessage("info", "Query String Parameter for ID element to render table [pidElementContent] is missing.", true);
            //}
            return View("~/Views/SalesAndUse/Admin.cshtml",viewModel);
        }

        [Route("PartialViewDBTable")]
        public ActionResult PartialViewDBTable()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            var tblName = (String.IsNullOrEmpty(Request.QueryString.Get("ptbl"))) ? "" : Request.QueryString.Get("ptbl");
            var colKey = (String.IsNullOrEmpty(Request.QueryString.Get("pcolKey"))) ? "" : Request.QueryString.Get("pcolKey");
            var idElementContent = (String.IsNullOrEmpty(Request.QueryString.Get("pidElementContent"))) ? "" : Request.QueryString.Get("pidElementContent");

            viewModel.Add("TableName", tblName);
            viewModel.Add("ColumnKey", colKey);
            viewModel.Add("IDElementContent", idElementContent);

            if (String.IsNullOrEmpty(tblName))
            {
                GlobalUtil.AddSessionMessage("info", "Query String Parameter for Table Name [ptbl] is missing.", true);
            }

            if (String.IsNullOrEmpty(colKey))
            {
                GlobalUtil.AddSessionMessage("info", "Query String Parameter for Column Key [pcolKey] is missing.", true);
            }

            if (String.IsNullOrEmpty(idElementContent))
            {
                GlobalUtil.AddSessionMessage("info", "Query String Parameter for ID element to render table [pidElementContent] is missing.", true);
            }
            return View("~/Views/SalesAndUse/TableAdminTemplate.cshtml", viewModel);
        }

        [ValidateInput(false)]
        [Route("P2PLvidINSERT")]
        public ActionResult P2PLvidINSERT(string pjson)
        {

            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_AC_ADMIN_P2PLVID_INSERT";


            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                 new Dictionary<string, string>
                {
                    { "Name", "@idlvid" },
                    { "Value", data["lvidId"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@goc" },
                    { "Value", data["goc"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@idaccount " },
                    { "Value", data["accountId"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@companyname" },
                    { "Value", data["companyName"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@active" },
                    { "Value", data["active"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                }
                #endregion
            });





            return Content("Saved!");
        }

        [ValidateInput(false)]
        [Route("P2PLvidUPDATE")]
        public ActionResult P2PLvidUPDATE(string pjson)
        {

            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_AC_ADMIN_P2PLVID_UPDATE";


            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                 new Dictionary<string, string>
                {
                    { "Name", "@idlvid" },
                    { "Value", data["lvidId"]}
                },
                 new Dictionary<string, string>
                {
                    { "Name", "@lvidGocsId" },
                    { "Value", data["lvidGocsId"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@goc" },
                    { "Value", data["goc"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@idaccount " },
                    { "Value", data["accountId"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@companyname" },
                    { "Value", data["companyName"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@active" },
                    { "Value", data["active"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                }
                #endregion
            });





            return Content("Saved!");
        }


        [ValidateInput(false)]
        [Route("LvidINSERT")]
        public ActionResult LvidINSERT(string pjson)
        {

            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "[NSP_AC_ADMIN_LVID_INSERT]";


            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                 new Dictionary<string, string>
                {
                    { "Name", "@lvid" },
                    { "Value", data["lvid"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@idLegalEntity" },
                    { "Value", data["legalentity"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@idCorpCode " },
                    { "Value", data["corpcode"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@idFEIN" },
                    { "Value", data["fein"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@entityId" },
                    { "Value", data["entityid"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@active" },
                    { "Value", data["active"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                }
                #endregion
            });





            return Content("Saved!");
        }


        [ValidateInput(false)]
        [Route("LvidUPDATE")]
        public ActionResult LvidUPDATE(string pjson)
        {

            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "[NSP_AC_ADMIN_LVID_UPDATE]";


            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                new Dictionary<string, string>
                {
                    { "Name", "@idlvid" },
                    { "Value", data["lvid"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@lvid" },
                    { "Value", data["lvid"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@idLegalEntity" },
                    { "Value", data["legalentity"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@idCorpCode " },
                    { "Value", data["corpcode"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@idFEIN" },
                    { "Value", data["fein"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@entityId" },
                    { "Value", data["entityid"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@active" },
                    { "Value", data["active"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                }
                #endregion
            });





            return Content("Saved!");
        }


        [ValidateInput(false)]
        [Route("ChecklistINSERT")]
        public ActionResult ChecklistINSERT(string pjson)
        {

            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_AC_ADMIN_CHECKLIST_INSERT";


            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                 new Dictionary<string, string>
                {
                    { "Name", "@checklist" },
                    { "Value", data["checklist"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@type" },
                    { "Value", data["type"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@order" },
                    { "Value", data["order"]}
                },
             
                new Dictionary<string, string>
                {
                    { "Name", "@active" },
                    { "Value", data["active"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                }
                #endregion
            });


            return Content("Saved!");
        }


        [ValidateInput(false)]
        [Route("ChecklistUPDATE")]
        public ActionResult ChecklistUPDATE(string pjson)
        {

            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_AC_ADMIN_CHECKLIST_UPDATE";


            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables

                new Dictionary<string, string>
                {
                    { "Name", "@id" },
                    { "Value", data["id"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@checklist" },
                    { "Value", data["checklist"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@type" },
                    { "Value", data["type"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@order" },
                    { "Value", data["order"]}
                },

                new Dictionary<string, string>
                {
                    { "Name", "@active" },
                    { "Value", data["active"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                }
                #endregion
            });


            return Content("Saved!");
        }




        
    }


}
