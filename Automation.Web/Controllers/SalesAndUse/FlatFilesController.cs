﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace Automation.Web.Controllers.SalesAndUse
{
    [RoutePrefix("SalesAndUse")]
    public class FlatFilesController : Controller
    {
        [Route("FlatFiles")]
        public ActionResult FlatFiles()
        {
            return View("~/Views/SalesAndUse/FlatFiles.cshtml");
        }

        //Modal Template
        [Route("ModalNewFlatFile")]
        public ActionResult ModalNewFlatFile()
        {
            Dictionary<string, object> viewBagResult = new Dictionary<string, object>();

            return View("~/Views/SalesAndUse/ModalNewFlatFile.cshtml", viewBagResult);
        }

        [Route("ModalFlatFile")]
        public ActionResult ModalFlatFile()
        {
            Dictionary<string, object> viewBagResult = new Dictionary<string, object>();

            string idFlatFile = Request.QueryString["idFlatFile"];
            string flatFileName = Request.QueryString["flatfileName"];

            viewBagResult.Add("idFlatFile", idFlatFile);
            viewBagResult.Add("flatfileName", flatFileName);

            return View("~/Views/SalesAndUse/ModalFlatFile.cshtml", viewBagResult);
        }

        [Route("ModalPlayback")]
        public ActionResult ModalPlayback()
        {
            Dictionary<string, object> viewBagResult = new Dictionary<string, object>();

            string idFlatFile = Request.QueryString["idFlatFile"];
            string flatFileName = Request.QueryString["flatfileName"];

            viewBagResult.Add("idFlatFile", idFlatFile);
            viewBagResult.Add("flatfileName", flatFileName);

            return View("~/Views/SalesAndUse/ModalPlayback.cshtml", viewBagResult);
        }

        [ValidateInput(false)]
        [Route("NewFlatFile")]
        public ActionResult NewFlatFile(string pjson)
        {
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);


            var spName = "NSP_AC_FLATFILE_NEWFF";
            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                 new Dictionary<string, string>
                {
                    { "Name", "@idFiling" },
                    { "Value", "1"}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@ffName" },
                    { "Value", data["ffName"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@ffHours" },
                    { "Value", data["ffHours"]}
                }
                #endregion
            });


            return Content("Saved!");
        }

        [Route("UploadReport1")]
        public ActionResult UploadReport1()
        {
            Dictionary<string, object> result = new Dictionary<string, object> { { "success", true } };

            try
            {
                var objFile = GlobalUtil.GetFileFromCurrentRequest();
                string fullPathSaved;
                DataTable ptable;

                //Save excel file in the server
                fullPathSaved = GlobalUtil.FileSaveAs("Playback", objFile.Filename, objFile.InputStream);
                DataTable tbl = new DataTable(); //tbl For Playback Details
                string[] tbl3 = new string[5]; //tbl for playback footer

                for (int col = 0; col < 17; col++)
                    tbl.Columns.Add(new DataColumn("Column" + (col + 1).ToString()));

                string[] lines = System.IO.File.ReadAllLines(fullPathSaved);
                lines = lines.Where(x => !string.IsNullOrEmpty(x)).ToArray();

                foreach (string line in lines)
                {
                    var cols = line.Split('~');

                    DataRow dr;

                    switch (cols[0]) {
                        case "20":
                            dr = tbl.NewRow();
                            for (int cIndex = 0; cIndex < 17; cIndex++)
                            {
                                dr[cIndex] = cols[cIndex];
                            }

                            tbl.Rows.Add(dr);
                            break;
                        case "30":
                            for (int cIndex = 0; cIndex < 5; cIndex++)
                            {
                                tbl3[cIndex] = cols[cIndex];
                            }
                            break;
                        default:
                            break;
                    }
                }
                tbl.Columns.Remove("Column1");
                ptable = tbl;
                string fileName = System.IO.Path.GetFileName(fullPathSaved);


                    //Save data in the database
                    (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[NSP_FF_UPLOAD_PLAYBACK]", new List<Dictionary<string, string>> {
                            new Dictionary<string, string>
                            {
                                { "Name", "@soeid" },
                                { "Value", GlobalUtil.GetSOEID() }
                            },new Dictionary<string, string>
                            {
                                { "Name", "@fileName" },
                                { "Value", fileName }
                            },new Dictionary<string, string>
                            {
                                { "Name", "@fIAmount" },
                                { "Value", tbl3[3] }
                            },new Dictionary<string, string>
                            {
                                { "Name", "@fCAmount" },
                                { "Value", tbl3[4] }
                            }

                        });
                

                string uploadFolder = GlobalUtil.GetValueAppConfig("appSettings", "UploadFolder") + "Playback" + @"\";
                string fullPathDelete = @"" + Server.MapPath(uploadFolder);

                System.IO.DirectoryInfo di = new DirectoryInfo(@"" + fullPathDelete);
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }

            }
            catch (Exception ex)
            {
                int s = 0;
             }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}
