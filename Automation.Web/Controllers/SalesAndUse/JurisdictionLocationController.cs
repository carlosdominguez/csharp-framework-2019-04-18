﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace Automation.Web.Controllers.SalesAndUse
{
    [RoutePrefix("SalesAndUse")]
    public class JurisdictionLocationController : Controller
    {
        [Route("JurisdictionLocation")]
        public ActionResult JurisdictionLocation()
        {
            return View("~/Views/SalesAndUse/JurisdictionLocation.cshtml");
        }



        //Modal Template
        [Route("ModalJurisdictionLocation")]
        public ActionResult ModalJurisdictionLocation()
        {
            Dictionary<string, object> viewBagResult = new Dictionary<string, object>();

            #region View Bag Variables
            string idJurisdictionLocation = Request.QueryString["idJurisdictionLocation"];
            string responsableSoeid = Request.QueryString["responsableSoeid"];
            string idLVID = Request.QueryString["idLVID"];
            string lvid = Request.QueryString["lvid"];
            string idLegalEntity = Request.QueryString["idLegalEntity"];
            string legalEntityName = Request.QueryString["legalEntityName"];
            string idCorpCode = Request.QueryString["idCorpCode"];
            string corpCode = Request.QueryString["corpCode"];
            string idFEIN = Request.QueryString["idFEIN"];
            string fein = Request.QueryString["fein"];
            string idType = Request.QueryString["idType"];
            string type = Request.QueryString["type"];
            string idState = Request.QueryString["idState"];
            string stateDivSub = Request.QueryString["stateDivSub"];
            string idAccount = Request.QueryString["idAccount"];
            string account = Request.QueryString["account"];
            string frequency = Request.QueryString["frequency"];
            string idJurisdiction = Request.QueryString["idJurisdiction"];
            string jurisdiction = Request.QueryString["jurisdiction"];
            string monthDue = Request.QueryString["monthDue"];
            string dueDay = Request.QueryString["dueDay"];
            string effectiveDate = Request.QueryString["effectiveDate"];
            string ctCorp = Request.QueryString["ctCorp"];
            string P2PLVID = Request.QueryString["P2PLVID"];
            string goc = Request.QueryString["goc"];
            string idP2PGocsLvid = Request.QueryString["idP2PGocsLvid"];
            string vendorSupplier = Request.QueryString["vendorSupplier"];
            string vendorSiteCode = Request.QueryString["vendorSiteCode"];
            string idCommodityCode = Request.QueryString["idCommodityCode"];
            string commodityCode = Request.QueryString["commodityCode"];
            string active = Request.QueryString["active"];

            viewBagResult.Add("idJurisdictionLocation", idJurisdictionLocation);
            viewBagResult.Add("responsableSoeid", responsableSoeid);
            viewBagResult.Add("idLVID", idLVID);
            viewBagResult.Add("lvid", lvid);
            viewBagResult.Add("idLegalEntity", idLegalEntity);
            viewBagResult.Add("legalEntityName", legalEntityName);
            viewBagResult.Add("idCorpCode", idCorpCode);
            viewBagResult.Add("corpCode", corpCode);
            viewBagResult.Add("idFEIN", idFEIN);
            viewBagResult.Add("fein", fein);
            viewBagResult.Add("idType", idType);
            viewBagResult.Add("type", type);
            viewBagResult.Add("idState", idState);
            viewBagResult.Add("stateDivSub", stateDivSub);
            viewBagResult.Add("idAccount", idAccount);
            viewBagResult.Add("account", account);
            viewBagResult.Add("frequency", frequency);
            viewBagResult.Add("idJurisdiction", idJurisdiction);
            viewBagResult.Add("jurisdiction", jurisdiction);
            viewBagResult.Add("monthDue", monthDue);
            viewBagResult.Add("dueDay", dueDay);
            viewBagResult.Add("effectiveDate", effectiveDate);
            viewBagResult.Add("ctCorp", ctCorp);
            viewBagResult.Add("P2PLVID", P2PLVID);
            viewBagResult.Add("goc", goc);
            viewBagResult.Add("idP2PGocsLvid", idP2PGocsLvid);
            viewBagResult.Add("vendorSupplier", vendorSupplier);
            viewBagResult.Add("vendorSiteCode", vendorSiteCode);
            viewBagResult.Add("idCommodityCode", idCommodityCode);
            viewBagResult.Add("commodityCode", commodityCode);
            viewBagResult.Add("active", active);

            #endregion

            return View("~/Views/SalesAndUse/ModalJurisdictionLocation.cshtml", viewBagResult);
        }



        [ValidateInput(false)]
        [Route("JurisdictionInsert")]
        public ActionResult JurisdictionInsert(string pjson)
        {

            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_AC_JURISDICTION_INSERT";


            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                new Dictionary<string, string>
                {
                    { "Name", "@responsableSoeid" },
                    { "Value", data["responsableSoeid"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@lvidId" },
                    { "Value", data["lvidId"]}
                },
                new Dictionary<string, string> {
                    { "Name", "@idType" },
                    { "Value", data["idType"]}
                 },
                new Dictionary<string, string>{
                    { "Name", "@idState" },
                    { "Value", data["active"]}
                },
                 new Dictionary<string, string> {
                    { "Name", "@idAccount" },
                    { "Value", data["idAccount"]}
                 },
                 new Dictionary<string, string>{
                    { "Name", "@frequency" },
                    { "Value", data["frequency"]}
                 },
                new Dictionary<string, string>{
                    { "Name", "@idJurisdiction" },
                    { "Value", data["idJurisdiction"]}
                },
                 new Dictionary<string, string>{
                    { "Name", "@monthDue" },
                    { "Value", data["monthDue"]}
                 },
                 new Dictionary<string, string>{
                    { "Name", "@dueDay" },
                    { "Value", data["dueDay"]}
                 },
                 new Dictionary<string, string> {
                    { "Name", "@effectiveDate" },
                    { "Value", data["effectiveDate"]}
                 },
                 new Dictionary<string, string>{
                    { "Name", "@ctCorp" },
                    { "Value", "1"} //PENDING
                 },
                 new Dictionary<string, string> {
                    { "Name", "@p2plvid" },
                    { "Value", data["p2plvid"]}
                 },
                 new Dictionary<string, string>{
                    { "Name", "@vendorSupplier" },
                    { "Value", data["vendorSupplier"]}
                 },
                 new Dictionary<string, string>{
                    { "Name", "@vendorSiteCode" },
                    { "Value", data["vendorSiteCode"]}
                 },
                 new Dictionary<string, string>{
                    { "Name", "@idCommodityCode" },
                    { "Value", data["idCommodityCode"]}
                 },
                 new Dictionary<string, string> {
                    { "Name", "@active" },
                    { "Value", data["active"]}
                 },
                 new Dictionary<string, string> {
                    { "Name", "@ckAch" },
                    { "Value", data["ckAch"]}
                 },
                 new Dictionary<string, string>{
                    { "Name", "@idP2PVendorSite" },
                    { "Value", data["idP2PVendorSite"]}
                 },
                 new Dictionary<string, string> {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                 }     
            });

            #endregion



            return Content("Saved!");
        }


        [ValidateInput(false)]
        [Route("JurisdictionUpdate")]
        public ActionResult JurisdictionUpdate(string pjson)
        {

            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_AC_JURISDICTION_UPDATE";


            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                new Dictionary<string, string>
                {
                    { "Name", "@jurisditionId" },
                    { "Value", data["jurisdictionId"]}
                },new Dictionary<string, string>
                {
                    { "Name", "@responsableSoeid" },
                    { "Value", data["responsableSoeid"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@lvidId" },
                    { "Value", data["lvidId"]}
                },
                new Dictionary<string, string> {
                    { "Name", "@idType" },
                    { "Value", data["idType"]}
                 },
                new Dictionary<string, string>{
                    { "Name", "@idState" },
                    { "Value", data["idState"]}
                },
                 new Dictionary<string, string> {
                    { "Name", "@idAccount" },
                    { "Value", data["idAccount"]}
                 },
                 new Dictionary<string, string>{
                    { "Name", "@frequency" },
                    { "Value", data["frequency"]}
                 },
                new Dictionary<string, string>{
                    { "Name", "@idJurisdiction" },
                    { "Value", data["idJurisdiction"]}
                },
                 new Dictionary<string, string>{
                    { "Name", "@monthDue" },
                    { "Value", data["monthDue"]}
                 },
                 new Dictionary<string, string>{
                    { "Name", "@dueDay" },
                    { "Value", data["dueDay"]}
                 },
                 new Dictionary<string, string> {
                    { "Name", "@effectiveDate" },
                    { "Value", data["effectiveDate"]}
                 },
                 new Dictionary<string, string>{
                    { "Name", "@ctCorp" },
                    { "Value", "1"} //PENDING
                 },
                 new Dictionary<string, string> {
                    { "Name", "@p2plvid" },
                    { "Value", data["p2plvid"]}
                 },
                 new Dictionary<string, string>{
                    { "Name", "@vendorSupplier" },
                    { "Value", data["vendorSupplier"]}
                 },
                 new Dictionary<string, string>{
                    { "Name", "@vendorSiteCode" },
                    { "Value", data["vendorSiteCode"]}
                 },
                 new Dictionary<string, string>{
                    { "Name", "@idCommodityCode" },
                    { "Value", data["idCommodityCode"]}
                 },
                 new Dictionary<string, string> {
                    { "Name", "@active" },
                    { "Value", data["active"]}
                 },
                 new Dictionary<string, string> {
                    { "Name", "@ckAch" },
                    { "Value", data["ckAch"]}
                 },
                 new Dictionary<string, string>{
                    { "Name", "@idP2PVendorSite" },
                    { "Value", data["idP2PVendorSite"]}
                 },
                 new Dictionary<string, string> {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                 }
            });

            #endregion



            return Content("Saved!");
        }

    }
}
