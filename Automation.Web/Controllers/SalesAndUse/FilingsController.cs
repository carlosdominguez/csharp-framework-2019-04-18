﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace Automation.Web.Controllers.SalesAndUse
{
    [RoutePrefix("SalesAndUse")]
    public class FilingsController : Controller
    {
        [Route("Filings")]
        public ActionResult Filings()
        {
            return View("~/Views/SalesAndUse/Filings.cshtml");
        }


        [Route("ModalFiling")]
        public ActionResult ModalFiling()
        {
            Dictionary<string, object> viewBagResult = new Dictionary<string, object>();

            #region View Bag Variables
            string idJurisdictionLocation = Request.QueryString["idJurisdictionLocation"];
            string type = Request.QueryString["type"];
            string state = Request.QueryString["stateDivSub"];
            string jurisdiction = Request.QueryString["jurisdiction"];
            string lvid = Request.QueryString["lvid"];
            string legalEntityName = Request.QueryString["legalEntityName"];
            string corpCode = Request.QueryString["corpCode"];
            string fein = Request.QueryString["fein"];
            string frequency = Request.QueryString["frequency"];
            string commodity = Request.QueryString["commodity"];
            string totalAmount = Request.QueryString["totalAmount"];

            viewBagResult.Add("idJurisdictionLocation", idJurisdictionLocation);
            viewBagResult.Add("type", type);
            viewBagResult.Add("state", state);
            viewBagResult.Add("jurisdiction", jurisdiction);
            viewBagResult.Add("lvid", lvid);
            viewBagResult.Add("legalEntityName", legalEntityName);
            viewBagResult.Add("corpCode", corpCode);
            viewBagResult.Add("fein", fein);
            viewBagResult.Add("frequency", frequency);
            viewBagResult.Add("commodity", commodity);
            viewBagResult.Add("totalAmount", totalAmount);

            #endregion

            return View("~/Views/SalesAndUse/ModalFiling.cshtml", viewBagResult);
        }


        [ValidateInput(false)]
        [Route("FilingEditSave")]
        public ActionResult FilingEditSave(string pjson)
        {
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_AC_FILING_UPDATE";


            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                 new Dictionary<string, string>
                {
                    { "Name", "@filingid" },
                    { "Value", data["filingid"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@duedate" },
                    { "Value", data["duedate"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@filingType " },
                    { "Value", data["filingType"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@totaldue" },
                    { "Value", data["totaldue"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@taxsavings" },
                    { "Value", data["taxsavings"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@newcomment" },
                    { "Value", data["newcomment"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                }
                #endregion
            });





            return Content("Saved!");
        }


        [ValidateInput(false)]
        [Route("FilingPaymentAdd")]
        public ActionResult FilingPaymentAdd(string pjson)
        {
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_AC_PAYMENT_INSERT";

            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                 new Dictionary<string, string>
                {
                    { "Name", "@paymentCategory" },
                    { "Value", data["paymentCategory"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@paymentMethod" },
                    { "Value", data["paymentMethod"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@paymentAmount " },
                    { "Value", data["paymentAmount"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@paymentid" },
                    { "Value", data["paymentid"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@p2pLvid" },
                    { "Value", data["p2pLvid"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@vendor" },
                    { "Value", data["vendor"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", data["comments"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@filingId" },
                    { "Value", data["filingId"]}
                }
                #endregion
            });





            return Content("Saved!");
        }

        [ValidateInput(false)]
        [Route("FilingPaymentUpdate")]
        public ActionResult FilingPaymentUpdate(string pjson)
        {
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_AC_PAYMENT_UPDATE";


            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                 new Dictionary<string, string>
                {
                    { "Name", "@paymentCategory" },
                    { "Value", data["paymentCategory"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@paymentMethod" },
                    { "Value", data["paymentMethod"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@paymentAmount" },
                    { "Value", data["paymentAmount"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@paymentid" },
                    { "Value", data["paymentid"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@p2pLvid" },
                    { "Value", data["p2pLvid"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@vendor" },
                    { "Value", data["vendor"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", data["comments"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@filingId" },
                    { "Value", data["filingId"]}
                }
                #endregion
            });





            return Content("Saved!");
        }

        [ValidateInput(false)]
        [Route("FilingPaymentDelete")]
        public ActionResult FilingPaymentDelete(string pjson)
        {
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_AC_PAYMENT_DELETE";


            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@idpayment" },
                    { "Value", data["paymentId"]}
                }
                #endregion
            });





            return Content("Saved!");
        }

        [ValidateInput(false)]
        [Route("FilingPaymentSubmit")]
        public ActionResult FilingPaymentSubmit(string pjson)
        {


            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_AC_PAYMENT_SUBMIT";

            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                new Dictionary<string, string>
                {
                    { "Name", "@filingid" },
                    { "Value", data["filingId"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                }
                #endregion
            });





            return Content("Saved!");

        }

        [ValidateInput(false)]
        [Route("FilingReviewSave")]
        public ActionResult FilingReviewSave(string pjson)
        {
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_AC_REVIEW_UPDATE";


            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                 new Dictionary<string, string>
                {
                    { "Name", "@filingId" },
                    { "Value", data["filingId"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@status" },
                    { "Value", data["status"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@rejectedBy " },
                    { "Value", data["rejectedBy"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@rejectedReason" },
                    { "Value", data["rejectedReason"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@rejectedDescription" },
                    { "Value", data["rejectedDescription"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                }
                #endregion
            });
            return Content("Saved!");
        }

        [ValidateInput(false)]
        [Route("FilingChecklistValidate")]
        public ActionResult FilingChecklistValidate(string pjson)
        {
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_AC_CHECKLIST_AUTO";


            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                 new Dictionary<string, string>
                {
                    { "Name", "@filingid" },
                    { "Value", data["filingid"]}
                }
                #endregion
            });
            return Content("Saved!");
        }

        [ValidateInput(false)]
        [Route("ChecklistSave")]
        public ActionResult ChecklistSave(string pjson)
        {
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_AC_CHECKLIST_UPDATE";


            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                 new Dictionary<string, string>
                {
                    { "Name", "@checklistid" },
                    { "Value", data["checklistid"]}
                }, new Dictionary<string, string>
                {
                    { "Name", "@checkliststatus" },
                    { "Value", data["checkliststatus"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@checklistcomment" },
                    { "Value", data["checklistcomment"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                }
                #endregion
            });
            return Content("Saved!");
        }


        [ValidateInput(false)]
        [Route("FilingChecklistSubmit")]
        public ActionResult FilingChecklistSubmit(string pjson)
        {
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_AC_CHECKLIST_SUBMIT";


            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                 new Dictionary<string, string>
                {
                    { "Name", "@filingid" },
                    { "Value", data["filingid"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                }
                #endregion
            });
            return Content("Saved!");
        }
        

        [ValidateInput(false)]
        [Route("FilingMailSave")]
        public ActionResult FilingMailSave(string pjson)
        {
            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_AC_MAIL_UPDATE";


            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                 new Dictionary<string, string>
                {
                    { "Name", "@mailedDate" },
                    { "Value", data["mailedDate"]}
                }, new Dictionary<string, string>
                {
                    { "Name", "@mailedBy" },
                    { "Value", data["mailedBy"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@confirmationFile" },
                    { "Value", data["confirmationFile"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@comments" },
                    { "Value", data["comments"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@filingId" },
                    { "Value", data["filingId"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                }
                #endregion
            });

            return Content(rowsResult[0]["Return"]);
        }
    }
}
