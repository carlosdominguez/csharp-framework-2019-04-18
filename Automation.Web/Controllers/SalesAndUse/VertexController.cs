using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace Automation.Web.Controllers.SalesAndUse
{
    [RoutePrefix("SalesAndUse")]
    public class VertexController : Controller
    {
        //
        // GET: /
        [Route("Vertex")]
        public ActionResult IndexVertex()
        {
            return View("~/Views/SalesAndUse/Vertex.cshtml");
        }

        [Route("ELA")]
        public ActionResult IndexELA()
        {
            return View("~/Views/SalesAndUse/ELA.cshtml");
        }

        [Route("VertexAdmin")]
        public ActionResult IndexAdmin()
        {
            return View("~/Views/SalesAndUse/VertexAdmin.cshtml");
        }

        [Route("UploadVertex")]
        public ActionResult UploadVertex(string ImpMonth, string ImpYear)
        {
            Dictionary<string, object> result = new Dictionary<string, object> { { "success", true } };
            var objFile = GlobalUtil.GetFileFromCurrentRequest(false);
            string fullPathSaved;
            DataTable ptable = new DataTable();

            //Save excel file in the server
            fullPathSaved = GlobalUtil.FileSaveAs("ImportReports", objFile.Filename, objFile.InputStream);
            //fileName = objFile.Filename;
            ptable = GlobalUtil.GetDataTableFromExcel(fullPathSaved, "detail");


            //Save data in the database
            (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[NSP_VF_UPLOAD_P2PFILE]", new List<Dictionary<string, string>> {
                            new Dictionary<string, string>
                            {
                                { "Name", "@soeid" },
                                { "Value", GlobalUtil.GetSOEID() }
                            },new Dictionary<string, string>
                            {
                                { "Name", "@fileName" },
                                { "Value", objFile.Filename }
                            }
                            ,new Dictionary<string, string>
                            {
                                { "Name", "@ImpMonth" },
                                { "Value", ImpMonth}
                            }
                            ,new Dictionary<string, string>
                            {
                                { "Name", "@ImpYear" },
                                { "Value", ImpYear }
                            }
                        });


            string uploadFolder = GlobalUtil.GetValueAppConfig("appSettings", "UploadFolder") + "ImportReports" + @"\";
            string fullPathDelete = @"" + Server.MapPath(uploadFolder);

            System.IO.DirectoryInfo di = new DirectoryInfo(@"" + fullPathDelete);
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }


            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Route("UploadELA")]
        public ActionResult UploadELA(string ImpMonth, string ImpYear)
        {
            Dictionary<string, object> result = new Dictionary<string, object> { { "success", true } };
            var objFile = GlobalUtil.GetFileFromCurrentRequest(false);
            string fullPathSaved;
            DataTable ptable = new DataTable();

            //Save excel file in the server
            fullPathSaved = GlobalUtil.FileSaveAs("ImportReports", objFile.Filename, objFile.InputStream);
            //fileName = objFile.Filename;
            ptable = GlobalUtil.GetDataTableFromExcel(fullPathSaved, "Data");


            //Save data in the database
            (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[NSP_VF_UPLOAD_ELAFILE]", new List<Dictionary<string, string>> {
                            new Dictionary<string, string>
                            {
                                { "Name", "@soeid" },
                                { "Value", GlobalUtil.GetSOEID() }
                            },new Dictionary<string, string>
                            {
                                { "Name", "@fileName" },
                                { "Value", objFile.Filename }
                            }
                            ,new Dictionary<string, string>
                            {
                                { "Name", "@ImpMonth" },
                                { "Value", ImpMonth}
                            }
                            ,new Dictionary<string, string>
                            {
                                { "Name", "@ImpYear" },
                                { "Value", ImpYear }
                            }
                        });

            string uploadFolder = GlobalUtil.GetValueAppConfig("appSettings", "UploadFolder") + "ImportReports" + @"\";
            string fullPathDelete = @"" + Server.MapPath(uploadFolder);

            System.IO.DirectoryInfo di = new DirectoryInfo(@"" + fullPathDelete);
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }


            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
