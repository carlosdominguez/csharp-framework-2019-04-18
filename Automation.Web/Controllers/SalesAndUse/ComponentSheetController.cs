﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace Automation.Web.Controllers.SalesAndUse
{
    [RoutePrefix("SalesAndUse")]
    public class ComponentSheetController : Controller
    {
        [Route("ComponentSheet")]
        public ActionResult ComponentSheet()
        {
            return View("~/Views/SalesAndUse/ComponentSheet.cshtml");
        }

        //Modal Template
        [ValidateInput(false)]
        [Route("ModalComponentSheet")]
        public ActionResult ModalComponentSheet()
        {
            Dictionary<string, object> viewBagResult = new Dictionary<string, object>();

            #region View Bag Variables

            string idComponentSheet = Request["idComponentSheet"];
            string idJurisdictionLocation = Request["idJurisdictionLocation"];
            string  oldLog = Request["oldLog"];
            string lvid = Request["lvid"];
            string legalEntityName = Request["legalEntityName"];
            string fein = Request["fein"];
            string frequency = Request["frequency"];
            string stateDivSub = Request["stateDivSub"];
            string jurisdiction = Request["jurisdiction"];
            string account = Request["account"];
            string onlineUserId = Request["onlineUserId"];
            string onlinePassword = Request["onlinePassword"];
            string website = Request["website"];
            string bankAccount = Request["bankAccount"];
            string routingNumber = Request["routingNumber"];
            string bankInfo = Request["bankInfo"];
            string accrualComponent = Request["accrualComponent"];
            string paymentBookedThru = Request["paymentBookedThru"]; 
            string webTracking = Request["webTracking"];
            string contactInformation = Request["contactInformation"];
            string mailingAddress = Request["mailingAddress"];
            string comments = Request["comments"];
            string createdBy = Request["createdBy"];
            string createdDate = Request["createdDate"];
            string updatedBy = Request["updatedBy"];
            string updatedDate = Request["updatedDate"];



            viewBagResult.Add("idComponentSheet", idComponentSheet);
            viewBagResult.Add("idJurisdictionLocation", idJurisdictionLocation);
            viewBagResult.Add("oldLog", oldLog);
            viewBagResult.Add("lvid", lvid);
            viewBagResult.Add("legalEntityName", legalEntityName);
            viewBagResult.Add("fein", fein);
            viewBagResult.Add("frequency", frequency);
            viewBagResult.Add("stateDivSub", stateDivSub);
            viewBagResult.Add("jurisdiction", jurisdiction);
            viewBagResult.Add("account", account);
            viewBagResult.Add("onlineUserId", onlineUserId);
            viewBagResult.Add("onlinePassword", onlinePassword);
            viewBagResult.Add("website", website);
            viewBagResult.Add("bankAccount", bankAccount);
            viewBagResult.Add("routingNumber", routingNumber);
            viewBagResult.Add("bankInfo", bankInfo);
            viewBagResult.Add("accrualComponent", accrualComponent);
            viewBagResult.Add("paymentBookedThru", paymentBookedThru);
            viewBagResult.Add("webTracking", webTracking);
            viewBagResult.Add("contactInformation", contactInformation);
            viewBagResult.Add("mailingAddress", mailingAddress);
            viewBagResult.Add("comments", comments);
            viewBagResult.Add("createdBy", createdBy);
            viewBagResult.Add("createdDate", createdDate);
            viewBagResult.Add("updatedBy", updatedBy);
            viewBagResult.Add("updatedDate", updatedDate);

            #endregion

            return View("~/Views/SalesAndUse/ModalComponentSheet.cshtml", viewBagResult);
        }

        [ValidateInput(false)]
        [Route("ComponentUpdate")]
        public ActionResult ComponentUpdate(string pjson)
        {

            var data = JsonConvert.DeserializeObject<Dictionary<string, string>>(pjson);
            var spName = "NSP_AC_COMPONENTSHEET_UPDATE";


            var rowsResult = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> {
                #region SP Variables
                new Dictionary<string, string>
                {
                    { "Name", "@idComponentSheet" },
                    { "Value", data["idComponentSheet"]}
                },new Dictionary<string, string>
                {
                    { "Name", "@onlineUserId" },
                    { "Value", data["onlineUserId"]}
                },
                new Dictionary<string, string>
                {
                    { "Name", "@onlinePassword" },
                    { "Value", data["onlinePassword"]}
                },
                new Dictionary<string, string> {
                    { "Name", "@website" },
                    { "Value", data["website"]}
                 },
                new Dictionary<string, string>{
                    { "Name", "@bankAccount" },
                    { "Value", data["bankAccount"]}
                },
                 new Dictionary<string, string> {
                    { "Name", "@routingNumber" },
                    { "Value", data["routingNumber"]}
                 },
                 new Dictionary<string, string>{
                    { "Name", "@bankInfo" },
                    { "Value", data["bankInfo"]}
                 },
                new Dictionary<string, string>{
                    { "Name", "@accrualComponent" },
                    { "Value", data["accrualComponent"]}
                },
                 new Dictionary<string, string>{
                    { "Name", "@paymentBookedThru" },
                    { "Value", data["paymentBookedThru"]}
                 },
                 new Dictionary<string, string>{
                    { "Name", "@webTracking" },
                    { "Value", data["webTracking"]}
                 },
                 new Dictionary<string, string> {
                    { "Name", "@contactInformation" },
                    { "Value", data["contactInformation"]}
                 },
                 new Dictionary<string, string> {
                    { "Name", "@mailingAddress" },
                    { "Value", data["mailingAddress"]}
                 },
                 new Dictionary<string, string>{
                    { "Name", "@comments" },
                    { "Value", data["comments"]}
                 },
                 new Dictionary<string, string> {
                    { "Name", "@soeid" },
                    { "Value", GlobalUtil.GetSOEID()}
                 }
            });

            #endregion



            return Content("Saved!");
        }


    }
}
