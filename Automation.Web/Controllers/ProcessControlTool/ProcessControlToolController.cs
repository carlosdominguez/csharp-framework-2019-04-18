using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web.Mvc;

namespace Automation.Web.Controllers.ProcessControlTool
{
    [RoutePrefix("ProcessControlTool")]
    public class ProcessControlToolController : Controller
    {
        //
        // GET: /ProcessControlTool/ActivityList
        [Route("ActivityList")]
        public ActionResult ActivityList()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            var pidActivityDetail = (String.IsNullOrEmpty(Request.QueryString.Get("pidActivityDetail"))) ? "" : Request.QueryString.Get("pidActivityDetail");
            var putcDateTimeSLA = (String.IsNullOrEmpty(Request.QueryString.Get("putcDateTimeSLA"))) ? "" : Request.QueryString.Get("putcDateTimeSLA");
            var pcontactView = (String.IsNullOrEmpty(Request.QueryString.Get("pcontactView"))) ? "" : Request.QueryString.Get("pcontactView");

            viewModel.Add("HFIDActivityDetail", pidActivityDetail);
            viewModel.Add("HFUATDateTimeSLA", putcDateTimeSLA);
            viewModel.Add("HFContactView", pcontactView);

            return View("~/Views/ProcessControlTool/ActivityList.cshtml", viewModel);
        }

        //
        // GET: /ProcessControlTool/AdminActivity
        [Route("AdminActivity")]
        public ActionResult AdminActivity()
        {
            return View("~/Views/ProcessControlTool/AdminActivity.cshtml");
        }

        //
        // GET: /ProcessControlTool/FrequencyManager
        [Route("FrequencyManager")]
        public ActionResult FrequencyManager()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            return View("~/Views/ProcessControlTool/FrequencyManager.cshtml", viewModel);
        }
        
        //
        // GET: /ProcessControlTool/CreateIfNotExistsFolderPath
        [Route("CreateIfNotExistsFolderPath")]
        public ActionResult CreateIfNotExistsFolderPath(string typeAuthentication, string user, string pass, string folderPath)
        {
            user = GlobalUtil.DecodeSkipSideminder(user);
            pass = GlobalUtil.DecodeAsciiString(pass);
            folderPath = GlobalUtil.DecodeSkipSideminder(folderPath);

            var resulMsg = "Pending";

            if (typeAuthentication == "WindowsAuthentication")
            {
                var userData = user.Split('\\');
                using (UserImpersonation userImperso = new UserImpersonation(userData[1].ToUpper(), userData[0].ToUpper(), pass))
                {
                    if (userImperso.ImpersonateValidUser())
                    {
                        try
                        {
                            FileInfo fileInfo = new FileInfo(folderPath);
                            Directory.CreateDirectory(fileInfo.Directory.FullName);
                            resulMsg = "Directory '" + folderPath + "' was created!";
                        }
                        catch (Exception e)
                        {
                            if (e.Message.Contains("Access to the path") && e.Message.Contains("is denied"))
                            {
                                resulMsg = "Access to the path '" + folderPath + "' is denied for user '" + user + "'";
                            }
                            else
                            {
                                throw e;
                            }
                        }
                    }
                    else
                    {
                        resulMsg = "User or Password incorrect";
                    }
                }
            }
            else
            {
                user = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                try
                {
                    FileInfo fileInfo = new FileInfo(folderPath);
                    Directory.CreateDirectory(fileInfo.Directory.FullName);
                    resulMsg = "Directory '" + folderPath + "' was created!";
                }
                catch (Exception e)
                {
                    if (e.Message.Contains("Access to the path") && e.Message.Contains("is denied"))
                    {
                        resulMsg = "Access to the path '" + folderPath + "' is denied for user '" + user + "'";
                    }
                    else
                    {
                        throw e;
                    }
                }
            }

            return Content(resulMsg);
        }

        //
        // GET: /ProcessControlTool/AdminBusinessStructure
        [Route("AdminBusinessStructure")]
        public ActionResult AdminBusinessStructure()
        {
            return View("~/Views/ProcessControlTool/AdminBusinessStructure.cshtml");
        }

        //
        // GET: /ProcessControlTool/PartialViewActivityForm
        [Route("PartialViewActivityForm")]
        public ActionResult PartialViewActivityForm()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            var pidTeam = (String.IsNullOrEmpty(Request.QueryString.Get("pidTeam"))) ? "" : Request.QueryString.Get("pidTeam");
            var pidActivity = (String.IsNullOrEmpty(Request.QueryString.Get("pidActivity"))) ? "" : Request.QueryString.Get("pidActivity");
            var paction = (String.IsNullOrEmpty(Request.QueryString.Get("paction"))) ? "" : Request.QueryString.Get("paction");

            viewModel.Add("HFIDTeam", pidTeam);
            viewModel.Add("HFIDActivity", pidActivity);
            viewModel.Add("HFAction", paction);
            return View("~/Views/ProcessControlTool/PartialViewActivityForm.cshtml", viewModel);
        }

        // GET: /ProcessControlTool/CheckAccess
        [Route("CheckAccess")]
        public ActionResult CheckAccess(string typeAuthentication, string user, string pass, string pathToTest)
        {
            user = GlobalUtil.DecodeSkipSideminder(user);
            pass = GlobalUtil.DecodeAsciiString(pass);
            pathToTest = GlobalUtil.DecodeSkipSideminder(pathToTest);

            var resulMsg = "Pending";

            if (typeAuthentication == "WindowsAuthentication")
            {
                var userData = user.Split('\\');
                using (UserImpersonation userImperso = new UserImpersonation(userData[1].ToUpper(), userData[0].ToUpper(), pass))
                {
                    if (userImperso.ImpersonateValidUser())
                    {
                        try
                        {
                            resulMsg = "Success! The user '" + user + "' has access to the " + GlobalUtil.CheckPathIsFileOrDirectory(pathToTest);
                        }
                        catch (Exception e)
                        {
                            if (e.Message.Contains("Access to the path") && e.Message.Contains("is denied"))
                            {
                                resulMsg = "Access to the path '" + pathToTest + "' is denied for user '" + user + "'";
                            }
                            else
                            {
                                throw e;
                            }
                        }
                    }
                    else
                    {
                        resulMsg = "User or Password incorrect";
                    }
                }
            }
            else
            {
                user = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                try
                {
                    resulMsg = "Success! The user '" + user + "' has access to the " + GlobalUtil.CheckPathIsFileOrDirectory(pathToTest);
                }
                catch (Exception e)
                {
                    if (e.Message.Contains("Access to the path") && e.Message.Contains("is denied"))
                    {
                        resulMsg = "Access to the path '" + pathToTest + "' is denied for user '" + user + "'";
                    }
                    else if (e.Message.Contains("Could not find file"))
                    {
                        resulMsg = "Success! The user '" + user + "' has access, but the folder or file path '" + pathToTest + "' not exists.";
                    }
                    else
                    {
                        throw e;
                    }
                }
            }

            return Content(resulMsg);
        }

        //
        // GET: /ProcessControlTool/UploadFrequency/
        [Route("UploadFrequency")]
        public ActionResult UploadFrequency(string pjsonData)
        {
            String pyearGenerated = Request.Params["pyearGenerated"];
            DataTable ptable = (DataTable)Newtonsoft.Json.JsonConvert.DeserializeObject(pjsonData, (typeof(DataTable)));

            //Save data in the database
            (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[spPCTUploadFrequency]", new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@YearGenerated" },
                    { "Value", pyearGenerated }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@SOEIDUploader" },
                    { "Value", GlobalUtil.GetSOEID() }
                }
            });

            return Content("Uploaded");
        }

        //
        // GET: /ProcessControlTool/UploadToShareDrive
        [ValidateInput(false)]
        [Route("UploadToShareDrive")]
        public ActionResult UploadToShareDrive(string pshareDriveFiles, string pisAdditionalFile, string padditionalFolderName)
        {
            //result: EXAMPLE DATA
            //[{
            //    Description:    "IS Analysis - Sub_1105 Report"
            //    FileExt:        "xlsx"
            //    FileName:       "IS Analysis - Sub_1105 Report AUTO"
            //    FolderPath:     "\\sjovnascsc0002.wlb.lac.nsroot.net\cscqt0019\CAA_NAM_GCG\Official Records\SD-TEST\2018\08\Auto Loans\CAA - Reporting - Non Standard\RMCR - IS Analysis - Sub_1105 Report AUTO\"
            //    FolderStructure:"@TeamStructure\@FileName.@FileExt"
            //    FullPath:       "\\sjovnascsc0002.wlb.lac.nsroot.net\cscqt0019\CAA_NAM_GCG\Official Records\SD-TEST\2018\08\Auto Loans\CAA - Reporting - Non Standard\RMCR - IS Analysis - Sub_1105 Report AUTO\IS Analysis - Sub_1105 Report AUTO.xlsx"
            //    ID:             9 
            //}]
            Dictionary<string, object> result = new Dictionary<string, object> { { "success", true } };
            List<Dictionary<string, string>> shareDriveFiles = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(pshareDriveFiles);

            try
            {
                var objFile = GlobalUtil.GetFileFromCurrentRequest(false);
                var fileName = objFile.Filename;
                var shareFolderToSave = "";

                //When is 0 find the path to save file
                if(pisAdditionalFile == "0")
                {
                    //Look ShareFolderToSave
                    foreach (var itemFile in shareDriveFiles)
                    {
                        var tempFileName = itemFile["FileName"] + "." + itemFile["FileExt"];
                        if (tempFileName == fileName)
                        {
                            //FullPath: "\\sjovnascsc0002.wlb.lac.nsroot.net\cscqt0019\CAA_NAM_GCG\Official Records\SD-TEST\2018\08\Auto Loans\CAA - Reporting - Non Standard\RMCR - IS Analysis - Sub_1105 Report AUTO\Minutes\Minute notes.pdf"
                            shareFolderToSave = itemFile["FullPath"];

                            //Remove file name
                            shareFolderToSave = shareFolderToSave.Replace(tempFileName, "");

                            //Exists foreach
                            break;
                        }
                    }
                }
                else
                {
                    //When is additional file, get the base folder path to save
                    //FolderPath:"\\sjovnascsc0002.wlb.lac.nsroot.net\cscqt0019\CAA_NAM_GCG\Official Records\SD-TEST\2018\08\Auto Loans\CAA - Reporting - Non Standard\RMCR - IS Analysis - Sub_1105 Report AUTO\"
                    shareFolderToSave = shareDriveFiles[0]["FolderPath"] + padditionalFolderName + @"\";
                }
                
                //Copy file to share folder
                var fullPathSaved = GlobalUtil.FileSaveAs(shareFolderToSave, fileName, objFile.InputStream, false);
                
                result.Add("fullPathSaved", fullPathSaved);
            }
            catch (Exception ex)
            {
                //return new FileUploaderResult(false, error: ex.Message);
                result["success"] = false;
                result.Add("error", ex.ToString());
            }

            // the anonymous object in the result below will be convert to json and set back to the browser
            //return new FileUploaderResult(true, new { extraInformation = 12345 });

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
    }
}
