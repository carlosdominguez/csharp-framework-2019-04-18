using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.ProcessControlTool
{
    [RoutePrefix("ProcessControlTool")]
    public class HomeController : Controller
    {
        //
        // GET: /ProcessControlTool/
        [Route("/")]
        public ActionResult Index()
        {
            return View("~/Views/ProcessControlTool/Home.cshtml");
        }

    }
}
