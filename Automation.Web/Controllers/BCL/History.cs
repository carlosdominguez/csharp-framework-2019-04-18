﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using Automation.Web.Models.CashFlow;
using System.Globalization;
using Automation.Core.Model;

namespace Automation.Web.Controllers.BCL
{
    [RoutePrefix("BCL")]
    public class HistoryController : Controller
    {
        [Route("History")]
        public ActionResult Index()
        {
            return View("~/Views/BCL/History.cshtml");
        }
    }
}