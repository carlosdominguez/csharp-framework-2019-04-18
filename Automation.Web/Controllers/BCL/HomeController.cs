using AttributeRouting;
using AttributeRouting.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using Automation.Web.Models.BCL;
using System.Globalization;
using Automation.Core.Model;

namespace Automation.Web.Controllers.BCL
{
    [RoutePrefix("BCL")]
    public class HomeController : Controller
    {
        //
        // GET: /BCL/
        [Route("/")]
        public ActionResult Index()
        {
            return View("~/Views/BCL/Home.cshtml");
        }

        [Route("get_Maker")]
        public ActionResult get_Maker(string data)
        {
            ToDoEntities DAO = new ToDoEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            string SOEID = objParameter["ID"].ToString();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.qryMakers
                    where p.SOEID == SOEID
                    orderby p.SOEID
                    select new
                    {
                        p.SOEID,
                        p.UserName
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("get_Checker")]
        public ActionResult get_Checker(string data)
        {
            ToDoEntities DAO = new ToDoEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();
            string SOEID = objParameter["ID"].ToString();
            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.qryCheckers
                    where p.SOEID != SOEID
                    orderby p.SOEID
                    select new
                    {
                        p.SOEID,
                        p.UserName
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("get_Project")]
        public ActionResult get_Project(string data)
        {
            ToDoEntities DAO = new ToDoEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblProjects
                    where p.IsActive == true
                    select new
                    {
                        p.ID,
                        p.Name
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("get_CheckerCorrect")]
        public ActionResult get_CheckerCorrect(string data)
        {
            ToDoEntities DAO = new ToDoEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();
            string SOEID = objParameter["ID"].ToString();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.qryCheckers
                    where p.SOEID == SOEID
                    orderby p.SOEID
                    select new
                    {
                        p.SOEID,
                        p.UserName
                    }
            });


            return Content(o["item"].ToString());
        }


        [Route("addRequest")]
        public ActionResult addRequest(string data)
        {
            ToDoEntities DAO = new ToDoEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();
            string SOEID = objParameter["ID"].ToString();


            int  projectID = Convert.ToInt32 (objParameter["projectID"].ToString());
            string requesterID = objParameter["requesterID"].ToString();
            string approverID = objParameter["approverID"].ToString();

            proc_Create_Request_Result request = (DAO.proc_Create_Request(projectID, requesterID, approverID)).SingleOrDefault<proc_Create_Request_Result>();

            short IDAdjustment        = Convert.ToInt16(objParameter["IDAdjustment"].ToString());
            short IDAdjustmentID      = Convert.ToInt16(objParameter["IDAdjustmentID"].ToString());
            short IDAdjustmentContact = Convert.ToInt16(objParameter["IDAdjustmentContact"].ToString());
            short IDReason            = Convert.ToInt16(objParameter["IDReason"].ToString());
            short IDMTC               = Convert.ToInt16(objParameter["IDMTC"].ToString());

            string Adjustment       = objParameter["Adjustment"].ToString();
            string AdjustmentID     = objParameter["AdjustmentID"].ToString();
            string AdjustmentContact = objParameter["AdjustmentContact"].ToString();
            string Reason           = objParameter["Reason"].ToString();
            string MTC              = objParameter["MTC"].ToString();

            DAO.proc_Add_RequestMetadata(request.ID,IDAdjustment       ,Adjustment        ,requesterID);
            DAO.proc_Add_RequestMetadata(request.ID,IDAdjustmentID     ,AdjustmentID      ,requesterID);
            DAO.proc_Add_RequestMetadata(request.ID,IDAdjustmentContact,AdjustmentContact ,requesterID);
            DAO.proc_Add_RequestMetadata(request.ID,IDReason           ,Reason            ,requesterID);
            DAO.proc_Add_RequestMetadata(request.ID, IDMTC, MTC, requesterID);

            string Reference = objParameter["Reference"].ToString();

            DAO.proc_Add_Reference(request.ID, requesterID, Reference);

            short stepID1 = Convert.ToInt16(objParameter["stepID1"].ToString());
            short stepID2 = Convert.ToInt16(objParameter["stepID2"].ToString());
            short stepID3 = Convert.ToInt16(objParameter["stepID3"].ToString());
            short stepID4 = Convert.ToInt16(objParameter["stepID4"].ToString());
            short stepID5 = Convert.ToInt16(objParameter["stepID5"].ToString());
            short stepID6 = Convert.ToInt16(objParameter["stepID6"].ToString());
            short stepID7 = Convert.ToInt16(objParameter["stepID7"].ToString());
            short stepID8 = Convert.ToInt16(objParameter["stepID8"].ToString());
            short stepID9 = Convert.ToInt16(objParameter["stepID9"].ToString());
            short stepID10 = Convert.ToInt16(objParameter["stepID10"].ToString());
            short stepID11 = Convert.ToInt16(objParameter["stepID11"].ToString());
            short stepID12 = Convert.ToInt16(objParameter["stepID12"].ToString());
            short stepID13 = Convert.ToInt16(objParameter["stepID13"].ToString());
            short stepID14 = Convert.ToInt16(objParameter["stepID14"].ToString());

            byte statusStepID1 = Convert.ToByte(objParameter["statusStepID1"].ToString());
            byte statusStepID2 = Convert.ToByte(objParameter["statusStepID2"].ToString());
            byte statusStepID3 = Convert.ToByte(objParameter["statusStepID3"].ToString());
            byte statusStepID4 = Convert.ToByte(objParameter["statusStepID4"].ToString());
            byte statusStepID5 = Convert.ToByte(objParameter["statusStepID5"].ToString());
            byte statusStepID6 = Convert.ToByte(objParameter["statusStepID6"].ToString());
            byte statusStepID7 = Convert.ToByte(objParameter["statusStepID7"].ToString());
            byte statusStepID8 = Convert.ToByte(objParameter["statusStepID8"].ToString());
            byte statusStepID9 = Convert.ToByte(objParameter["statusStepID9"].ToString());
            byte statusStepID10 = Convert.ToByte(objParameter["statusStepID10"].ToString());
            byte statusStepID11 = Convert.ToByte(objParameter["statusStepID11"].ToString());
            byte statusStepID12 = Convert.ToByte(objParameter["statusStepID12"].ToString());
            byte statusStepID13 = Convert.ToByte(objParameter["statusStepID13"].ToString());
            byte statusStepID14 = Convert.ToByte(objParameter["statusStepID14"].ToString());

            string descStepID1 = objParameter["descStepID1"].ToString();
            string descStepID2 = objParameter["descStepID2"].ToString();
            string descStepID3 = objParameter["descStepID3"].ToString();
            string descStepID4 = objParameter["descStepID4"].ToString();
            string descStepID5 = objParameter["descStepID5"].ToString();
            string descStepID6 = objParameter["descStepID6"].ToString();
            string descStepID7 = objParameter["descStepID7"].ToString();
            string descStepID8 = objParameter["descStepID8"].ToString();
            string descStepID9 = objParameter["descStepID9"].ToString();
            string descStepID10 = objParameter["descStepID10"].ToString();
            string descStepID11 = objParameter["descStepID11"].ToString();
            string descStepID12 = objParameter["descStepID12"].ToString();
            string descStepID13 = objParameter["descStepID13"].ToString();
            string descStepID14 = objParameter["descStepID14"].ToString();

            DAO.proc_Add_Attestation(request.ID, stepID1, 25, requesterID, statusStepID1, descStepID1, 1);
            DAO.proc_Add_Attestation(request.ID, stepID2, 25, requesterID, statusStepID2, descStepID2, 1);
            DAO.proc_Add_Attestation(request.ID, stepID3, 25, requesterID, statusStepID3, descStepID3, 1);
            DAO.proc_Add_Attestation(request.ID, stepID4, 25, requesterID, statusStepID4, descStepID4, 1);
            DAO.proc_Add_Attestation(request.ID, stepID5, 25, requesterID, statusStepID5, descStepID5, 1);
            DAO.proc_Add_Attestation(request.ID, stepID6, 25, requesterID, statusStepID6, descStepID6, 1);
            DAO.proc_Add_Attestation(request.ID, stepID7, 25, requesterID, statusStepID7, descStepID7, 1);
            DAO.proc_Add_Attestation(request.ID, stepID8, 25, requesterID, statusStepID8, descStepID8, 1);
            DAO.proc_Add_Attestation(request.ID, stepID9, 25, requesterID, statusStepID9, descStepID9, 1);
            DAO.proc_Add_Attestation(request.ID, stepID10, 25, requesterID, statusStepID10, descStepID10, 1);
            DAO.proc_Add_Attestation(request.ID, stepID11, 25, requesterID, statusStepID11, descStepID11, 1);
            DAO.proc_Add_Attestation(request.ID, stepID12, 25, requesterID, statusStepID12, descStepID12, 1);
            DAO.proc_Add_Attestation(request.ID, stepID13, 25, requesterID, statusStepID13, descStepID13, 1);
            DAO.proc_Add_Attestation(request.ID, stepID14, 25, requesterID, statusStepID14, descStepID14, 1);


            byte statusID = Convert.ToByte(objParameter["statusID"].ToString());

            DAO.proc_Change_RequestStatus(request.ID,requesterID,statusID);


            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.qryCheckers
                    where p.SOEID == SOEID
                    orderby p.SOEID
                    select new
                    {
                        p.SOEID,
                        p.UserName
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("getRequestMetaData")]
        public ActionResult getRequestMetaData(string data)
        {
            ToDoEntities DAO = new ToDoEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();
            int  ID = Convert.ToInt32 (objParameter["ID"].ToString());

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblRequestMetadatas 
      
                    where p.RequestID == ID
                    select new
                    {
                        p.ProjectMetadataID,
                        p.Value,
                        p.tblProjectMetadata.Name 
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("getPathList")]
        public ActionResult getPathList(string data)
        {
            ToDoEntities DAO = new ToDoEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();
            int ID = Convert.ToInt32(objParameter["ID"].ToString());

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblRequestReferences 
                    where p.RequestID == ID
                    select new
                    {
                        Path = p.Link
                    }
            });


            return Content(o["item"].ToString());
        }


        [Route("addRequestChecker")]
        public ActionResult addRequestChecker(string data)
        {
            ToDoEntities DAO = new ToDoEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();
            string SOEID = objParameter["ID"].ToString();


            int requestId = Convert.ToInt32(objParameter["requestId"].ToString());
            string requesterID = objParameter["requesterID"].ToString();
            string approverID = objParameter["approverID"].ToString();


            short stepID1 = Convert.ToInt16(objParameter["stepID1"].ToString());
            short stepID2 = Convert.ToInt16(objParameter["stepID2"].ToString());
            short stepID3 = Convert.ToInt16(objParameter["stepID3"].ToString());
            short stepID4 = Convert.ToInt16(objParameter["stepID4"].ToString());
            short stepID5 = Convert.ToInt16(objParameter["stepID5"].ToString());
            short stepID6 = Convert.ToInt16(objParameter["stepID6"].ToString());
            short stepID7 = Convert.ToInt16(objParameter["stepID7"].ToString());
            short stepID8 = Convert.ToInt16(objParameter["stepID8"].ToString());
            short stepID9 = Convert.ToInt16(objParameter["stepID9"].ToString());


            byte statusStepID1 = Convert.ToByte(objParameter["statusStepID1"].ToString());
            byte statusStepID2 = Convert.ToByte(objParameter["statusStepID2"].ToString());
            byte statusStepID3 = Convert.ToByte(objParameter["statusStepID3"].ToString());
            byte statusStepID4 = Convert.ToByte(objParameter["statusStepID4"].ToString());
            byte statusStepID5 = Convert.ToByte(objParameter["statusStepID5"].ToString());
            byte statusStepID6 = Convert.ToByte(objParameter["statusStepID6"].ToString());
            byte statusStepID7 = Convert.ToByte(objParameter["statusStepID7"].ToString());
            byte statusStepID8 = Convert.ToByte(objParameter["statusStepID8"].ToString());
            byte statusStepID9 = Convert.ToByte(objParameter["statusStepID9"].ToString());


            string descStepID1 = objParameter["descStepID1"].ToString();
            string descStepID2 = objParameter["descStepID2"].ToString();
            string descStepID3 = objParameter["descStepID3"].ToString();
            string descStepID4 = objParameter["descStepID4"].ToString();
            string descStepID5 = objParameter["descStepID5"].ToString();
            string descStepID6 = objParameter["descStepID6"].ToString();
            string descStepID7 = objParameter["descStepID7"].ToString();
            string descStepID8 = objParameter["descStepID8"].ToString();
            string descStepID9 = objParameter["descStepID9"].ToString();


            DAO.proc_Add_Attestation(requestId, stepID1, 26, requesterID, statusStepID1, descStepID1, 1);
            DAO.proc_Add_Attestation(requestId, stepID2, 26, requesterID, statusStepID2, descStepID2, 1);
            DAO.proc_Add_Attestation(requestId, stepID3, 26, requesterID, statusStepID3, descStepID3, 1);
            DAO.proc_Add_Attestation(requestId, stepID4, 26, requesterID, statusStepID4, descStepID4, 1);
            DAO.proc_Add_Attestation(requestId, stepID5, 26, requesterID, statusStepID5, descStepID5, 1);
            DAO.proc_Add_Attestation(requestId, stepID6, 26, requesterID, statusStepID6, descStepID6, 1);
            DAO.proc_Add_Attestation(requestId, stepID7, 26, requesterID, statusStepID7, descStepID7, 1);
            DAO.proc_Add_Attestation(requestId, stepID8, 26, requesterID, statusStepID8, descStepID8, 1);
            DAO.proc_Add_Attestation(requestId, stepID9, 26, requesterID, statusStepID9, descStepID9, 1);



            byte statusID = Convert.ToByte(objParameter["statusID"].ToString());

            DAO.proc_Change_RequestStatus(requestId, requesterID, statusID);


            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.qryCheckers
                    where p.SOEID == SOEID
                    orderby p.SOEID
                    select new
                    {
                        p.SOEID,
                        p.UserName
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("rejectRequestChecker")]
        public ActionResult rejectRequestChecker(string data)
        {
            ToDoEntities DAO = new ToDoEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();
            string SOEID = objParameter["ID"].ToString();


            int requestId = Convert.ToInt32(objParameter["requestId"].ToString());
            string requesterID = objParameter["requesterID"].ToString();
            string approverID = objParameter["approverID"].ToString();


            short stepID1 = Convert.ToInt16(objParameter["stepID1"].ToString());
            short stepID2 = Convert.ToInt16(objParameter["stepID2"].ToString());
            short stepID3 = Convert.ToInt16(objParameter["stepID3"].ToString());
            short stepID4 = Convert.ToInt16(objParameter["stepID4"].ToString());
            short stepID5 = Convert.ToInt16(objParameter["stepID5"].ToString());
            short stepID6 = Convert.ToInt16(objParameter["stepID6"].ToString());
            short stepID7 = Convert.ToInt16(objParameter["stepID7"].ToString());
            short stepID8 = Convert.ToInt16(objParameter["stepID8"].ToString());
            short stepID9 = Convert.ToInt16(objParameter["stepID9"].ToString());


            byte statusStepID1 = Convert.ToByte(objParameter["statusStepID1"].ToString());
            byte statusStepID2 = Convert.ToByte(objParameter["statusStepID2"].ToString());
            byte statusStepID3 = Convert.ToByte(objParameter["statusStepID3"].ToString());
            byte statusStepID4 = Convert.ToByte(objParameter["statusStepID4"].ToString());
            byte statusStepID5 = Convert.ToByte(objParameter["statusStepID5"].ToString());
            byte statusStepID6 = Convert.ToByte(objParameter["statusStepID6"].ToString());
            byte statusStepID7 = Convert.ToByte(objParameter["statusStepID7"].ToString());
            byte statusStepID8 = Convert.ToByte(objParameter["statusStepID8"].ToString());
            byte statusStepID9 = Convert.ToByte(objParameter["statusStepID9"].ToString());


            string descStepID1 = objParameter["descStepID1"].ToString();
            string descStepID2 = objParameter["descStepID2"].ToString();
            string descStepID3 = objParameter["descStepID3"].ToString();
            string descStepID4 = objParameter["descStepID4"].ToString();
            string descStepID5 = objParameter["descStepID5"].ToString();
            string descStepID6 = objParameter["descStepID6"].ToString();
            string descStepID7 = objParameter["descStepID7"].ToString();
            string descStepID8 = objParameter["descStepID8"].ToString();
            string descStepID9 = objParameter["descStepID9"].ToString();


            DAO.proc_Add_Attestation(requestId, stepID1, 26, requesterID, statusStepID1, descStepID1, 1);
            DAO.proc_Add_Attestation(requestId, stepID2, 26, requesterID, statusStepID2, descStepID2, 1);
            DAO.proc_Add_Attestation(requestId, stepID3, 26, requesterID, statusStepID3, descStepID3, 1);
            DAO.proc_Add_Attestation(requestId, stepID4, 26, requesterID, statusStepID4, descStepID4, 1);
            DAO.proc_Add_Attestation(requestId, stepID5, 26, requesterID, statusStepID5, descStepID5, 1);
            DAO.proc_Add_Attestation(requestId, stepID6, 26, requesterID, statusStepID6, descStepID6, 1);
            DAO.proc_Add_Attestation(requestId, stepID7, 26, requesterID, statusStepID7, descStepID7, 1);
            DAO.proc_Add_Attestation(requestId, stepID8, 26, requesterID, statusStepID8, descStepID8, 1);
            DAO.proc_Add_Attestation(requestId, stepID9, 26, requesterID, statusStepID9, descStepID9, 1);



            byte statusID = Convert.ToByte(objParameter["statusID"].ToString());

            DAO.proc_Change_RequestStatus(requestId, requesterID, statusID);


            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.qryCheckers
                    where p.SOEID == SOEID
                    orderby p.SOEID
                    select new
                    {
                        p.SOEID,
                        p.UserName
                    }
            });


            return Content(o["item"].ToString());
        }


        [Route("addRequestBSS")]
        public ActionResult addRequestBSS(string data)
        {
            ToDoEntities DAO = new ToDoEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();
            string SOEID = objParameter["ID"].ToString();


            short projectID = Convert.ToInt16(objParameter["projectID"].ToString());
            string requesterID = objParameter["requesterID"].ToString();
            string approverID = objParameter["approverID"].ToString();

            proc_Create_Request_Result request = (DAO.proc_Create_Request(projectID, requesterID, approverID)).SingleOrDefault<proc_Create_Request_Result>();

            short IDAdjustment = Convert.ToInt16(objParameter["IDAdjustment"].ToString());
            short IDAdjustmentID = Convert.ToInt16(objParameter["IDAdjustmentID"].ToString());
            short IDAdjustmentContact = Convert.ToInt16(objParameter["IDAdjustmentContact"].ToString());
            short IDReason = Convert.ToInt16(objParameter["IDReason"].ToString());
            short IDMTC = Convert.ToInt16(objParameter["IDMTC"].ToString());

            string Adjustment = objParameter["Adjustment"].ToString();
            string AdjustmentID = objParameter["AdjustmentID"].ToString();
            string AdjustmentContact = objParameter["AdjustmentContact"].ToString();
            string Reason = objParameter["Reason"].ToString();
            string MTC = objParameter["MTC"].ToString();

            DAO.proc_Add_RequestMetadata(request.ID, IDAdjustment, Adjustment, requesterID);
            DAO.proc_Add_RequestMetadata(request.ID, IDAdjustmentID, AdjustmentID, requesterID);
            DAO.proc_Add_RequestMetadata(request.ID, IDAdjustmentContact, AdjustmentContact, requesterID);
            DAO.proc_Add_RequestMetadata(request.ID, IDReason, Reason, requesterID);
            DAO.proc_Add_RequestMetadata(request.ID, IDMTC, MTC, requesterID);

            string Reference = objParameter["Reference"].ToString();

            DAO.proc_Add_Reference(request.ID, requesterID, Reference);

            short stepID1 = Convert.ToInt16(objParameter["stepID1"].ToString());
            short stepID2 = Convert.ToInt16(objParameter["stepID2"].ToString());
            short stepID3 = Convert.ToInt16(objParameter["stepID3"].ToString());
            short stepID4 = Convert.ToInt16(objParameter["stepID4"].ToString());
            short stepID5 = Convert.ToInt16(objParameter["stepID5"].ToString());
            short stepID6 = Convert.ToInt16(objParameter["stepID6"].ToString());
            short stepID7 = Convert.ToInt16(objParameter["stepID7"].ToString());


            byte statusStepID1 = Convert.ToByte(objParameter["statusStepID1"].ToString());
            byte statusStepID2 = Convert.ToByte(objParameter["statusStepID2"].ToString());
            byte statusStepID3 = Convert.ToByte(objParameter["statusStepID3"].ToString());
            byte statusStepID4 = Convert.ToByte(objParameter["statusStepID4"].ToString());
            byte statusStepID5 = Convert.ToByte(objParameter["statusStepID5"].ToString());
            byte statusStepID6 = Convert.ToByte(objParameter["statusStepID6"].ToString());
            byte statusStepID7 = Convert.ToByte(objParameter["statusStepID7"].ToString());


            string descStepID1 = objParameter["descStepID1"].ToString();
            string descStepID2 = objParameter["descStepID2"].ToString();
            string descStepID3 = objParameter["descStepID3"].ToString();
            string descStepID4 = objParameter["descStepID4"].ToString();
            string descStepID5 = objParameter["descStepID5"].ToString();
            string descStepID6 = objParameter["descStepID6"].ToString();
            string descStepID7 = objParameter["descStepID7"].ToString();


            DAO.proc_Add_Attestation(request.ID, stepID1, 25, requesterID, statusStepID1, descStepID1, 1);
            DAO.proc_Add_Attestation(request.ID, stepID2, 25, requesterID, statusStepID2, descStepID2, 1);
            DAO.proc_Add_Attestation(request.ID, stepID3, 25, requesterID, statusStepID3, descStepID3, 1);
            DAO.proc_Add_Attestation(request.ID, stepID4, 25, requesterID, statusStepID4, descStepID4, 1);
            DAO.proc_Add_Attestation(request.ID, stepID5, 25, requesterID, statusStepID5, descStepID5, 1);
            DAO.proc_Add_Attestation(request.ID, stepID6, 25, requesterID, statusStepID6, descStepID6, 1);
            DAO.proc_Add_Attestation(request.ID, stepID7, 25, requesterID, statusStepID7, descStepID7, 1);

            byte statusID = Convert.ToByte(objParameter["statusID"].ToString());

            DAO.proc_Change_RequestStatus(request.ID, requesterID, statusID);


            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.qryCheckers
                    where p.SOEID == SOEID
                    orderby p.SOEID
                    select new
                    {
                        p.SOEID,
                        p.UserName
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("addRequestCheckerBSS")]
        public ActionResult addRequestCheckerBSS(string data)
        {
            ToDoEntities DAO = new ToDoEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();
            string SOEID = objParameter["ID"].ToString();


            int requestId = Convert.ToInt32(objParameter["requestId"].ToString());
            string requesterID = objParameter["requesterID"].ToString();
            string approverID = objParameter["approverID"].ToString();


            short stepID1 = Convert.ToInt16(objParameter["stepID1"].ToString());
            short stepID2 = Convert.ToInt16(objParameter["stepID2"].ToString());
            short stepID3 = Convert.ToInt16(objParameter["stepID3"].ToString());
            short stepID4 = Convert.ToInt16(objParameter["stepID4"].ToString());
            short stepID5 = Convert.ToInt16(objParameter["stepID5"].ToString());
            short stepID6 = Convert.ToInt16(objParameter["stepID6"].ToString());
            short stepID7 = Convert.ToInt16(objParameter["stepID7"].ToString());



            byte statusStepID1 = Convert.ToByte(objParameter["statusStepID1"].ToString());
            byte statusStepID2 = Convert.ToByte(objParameter["statusStepID2"].ToString());
            byte statusStepID3 = Convert.ToByte(objParameter["statusStepID3"].ToString());
            byte statusStepID4 = Convert.ToByte(objParameter["statusStepID4"].ToString());
            byte statusStepID5 = Convert.ToByte(objParameter["statusStepID5"].ToString());
            byte statusStepID6 = Convert.ToByte(objParameter["statusStepID6"].ToString());
            byte statusStepID7 = Convert.ToByte(objParameter["statusStepID7"].ToString());



            string descStepID1 = objParameter["descStepID1"].ToString();
            string descStepID2 = objParameter["descStepID2"].ToString();
            string descStepID3 = objParameter["descStepID3"].ToString();
            string descStepID4 = objParameter["descStepID4"].ToString();
            string descStepID5 = objParameter["descStepID5"].ToString();
            string descStepID6 = objParameter["descStepID6"].ToString();
            string descStepID7 = objParameter["descStepID7"].ToString();



            DAO.proc_Add_Attestation(requestId, stepID1, 26, requesterID, statusStepID1, descStepID1, 1);
            DAO.proc_Add_Attestation(requestId, stepID2, 26, requesterID, statusStepID2, descStepID2, 1);
            DAO.proc_Add_Attestation(requestId, stepID3, 26, requesterID, statusStepID3, descStepID3, 1);
            DAO.proc_Add_Attestation(requestId, stepID4, 26, requesterID, statusStepID4, descStepID4, 1);
            DAO.proc_Add_Attestation(requestId, stepID5, 26, requesterID, statusStepID5, descStepID5, 1);
            DAO.proc_Add_Attestation(requestId, stepID6, 26, requesterID, statusStepID6, descStepID6, 1);
            DAO.proc_Add_Attestation(requestId, stepID7, 26, requesterID, statusStepID7, descStepID7, 1);




            byte statusID = Convert.ToByte(objParameter["statusID"].ToString());

            DAO.proc_Change_RequestStatus(requestId, requesterID, statusID);


            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.qryCheckers
                    where p.SOEID == SOEID
                    orderby p.SOEID
                    select new
                    {
                        p.SOEID,
                        p.UserName
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("rejectRequestCheckerBSS")]
        public ActionResult rejectRequestCheckerBSS(string data)
        {
            ToDoEntities DAO = new ToDoEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();
            string SOEID = objParameter["ID"].ToString();


            int requestId = Convert.ToInt32(objParameter["requestId"].ToString());
            string requesterID = objParameter["requesterID"].ToString();
            string approverID = objParameter["approverID"].ToString();


            short stepID1 = Convert.ToInt16(objParameter["stepID1"].ToString());
            short stepID2 = Convert.ToInt16(objParameter["stepID2"].ToString());
            short stepID3 = Convert.ToInt16(objParameter["stepID3"].ToString());
            short stepID4 = Convert.ToInt16(objParameter["stepID4"].ToString());
            short stepID5 = Convert.ToInt16(objParameter["stepID5"].ToString());
            short stepID6 = Convert.ToInt16(objParameter["stepID6"].ToString());
            short stepID7 = Convert.ToInt16(objParameter["stepID7"].ToString());



            byte statusStepID1 = Convert.ToByte(objParameter["statusStepID1"].ToString());
            byte statusStepID2 = Convert.ToByte(objParameter["statusStepID2"].ToString());
            byte statusStepID3 = Convert.ToByte(objParameter["statusStepID3"].ToString());
            byte statusStepID4 = Convert.ToByte(objParameter["statusStepID4"].ToString());
            byte statusStepID5 = Convert.ToByte(objParameter["statusStepID5"].ToString());
            byte statusStepID6 = Convert.ToByte(objParameter["statusStepID6"].ToString());
            byte statusStepID7 = Convert.ToByte(objParameter["statusStepID7"].ToString());



            string descStepID1 = objParameter["descStepID1"].ToString();
            string descStepID2 = objParameter["descStepID2"].ToString();
            string descStepID3 = objParameter["descStepID3"].ToString();
            string descStepID4 = objParameter["descStepID4"].ToString();
            string descStepID5 = objParameter["descStepID5"].ToString();
            string descStepID6 = objParameter["descStepID6"].ToString();
            string descStepID7 = objParameter["descStepID7"].ToString();



            DAO.proc_Add_Attestation(requestId, stepID1, 26, requesterID, statusStepID1, descStepID1, 1);
            DAO.proc_Add_Attestation(requestId, stepID2, 26, requesterID, statusStepID2, descStepID2, 1);
            DAO.proc_Add_Attestation(requestId, stepID3, 26, requesterID, statusStepID3, descStepID3, 1);
            DAO.proc_Add_Attestation(requestId, stepID4, 26, requesterID, statusStepID4, descStepID4, 1);
            DAO.proc_Add_Attestation(requestId, stepID5, 26, requesterID, statusStepID5, descStepID5, 1);
            DAO.proc_Add_Attestation(requestId, stepID6, 26, requesterID, statusStepID6, descStepID6, 1);
            DAO.proc_Add_Attestation(requestId, stepID7, 26, requesterID, statusStepID7, descStepID7, 1);

            byte statusID = Convert.ToByte(objParameter["statusID"].ToString());

            DAO.proc_Change_RequestStatus(requestId, requesterID, statusID);


            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.qryCheckers
                    where p.SOEID == SOEID
                    orderby p.SOEID
                    select new
                    {
                        p.SOEID,
                        p.UserName
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("getStepData")]
        public ActionResult getStepData(string data)
        {
            ToDoEntities DAO = new ToDoEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();


            int requestId = Convert.ToInt32(objParameter["requestId"].ToString());
            string requesterID = objParameter["requesterID"].ToString();
            string approverID = objParameter["approverID"].ToString();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblAttestations 
                    join s in DAO.tblStatus on p.StatusID equals s.ID  
                    where p.RequestID == requestId && p.RoleID == 25
                    select new
                    {
                        p.StepID,
                        p.StatusID,
                        s.Name,
                        p.Description
                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("checkIfIsOKOptima")]
        public ActionResult checkIfIsOKOptima(string data)
        {
            ToDoEntities DAO = new ToDoEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();


            int requestId = Convert.ToInt32(objParameter["requestId"].ToString());
            string requesterID = objParameter["requesterID"].ToString();
            string approverID = objParameter["approverID"].ToString();
            int type = Convert.ToInt32(objParameter["type"].ToString());

            tblRequest Request = (from p in DAO.tblRequests where p.ID == requestId select p).SingleOrDefault<tblRequest>();




            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblRequests 
                    
                    where p.ProjectID == type && p.RequesterID  == requesterID && p.ApproverID  == approverID && p.ID == requestId && p.StatusID == 0
                    select new
                    {
                        p.ID
                    }
            });


            return Content(o["item"].ToString());
            
        }


        [Route("pendingForApprove")]
        public ActionResult pendingForApprove(string data)
        {
            ToDoEntities DAO = new ToDoEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            string SOEID = objParameter["SOEID"].ToString();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.qryRequests

                    where p.ApproverID == SOEID && p.RequestStatus == "Pending for approval/validation"
                    select new
                    {
                       p.ProjectID
                      ,p.ProjectName
                      ,p.RequestID
                      ,p.RequestStatus
                      ,p.RequesterID
                      ,p.CreatedDate
                      ,p.ApproverID
                      ,p.Metadata
                      ,p.AdjustmentID
                    }
            });


            return Content(o["item"].ToString());

        }

        [Route("myHistory")]
        public ActionResult myHistory(string data)
        {
            ToDoEntities DAO = new ToDoEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            string SOEID = objParameter["SOEID"].ToString();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.qryRequests

                    where p.ApproverID == SOEID || p.RequesterID == SOEID
                    orderby p.RequestID descending
                    select new
                    {
                        p.ProjectID
                      ,
                        p.ProjectName
                      ,
                        p.RequestID
                      ,
                        p.RequestStatus
                      ,
                        p.RequesterID
                      ,
                        p.CreatedDate
                      ,
                        p.ApproverID
                      ,
                        p.Metadata
                      ,
                        p.AdjustmentID
                    }
            });


            return Content(o["item"].ToString());

        }


        [Route("changeApprover")]
        public ActionResult changeApprover(string data)
        {
            ToDoEntities DAO = new ToDoEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["ID"].ToString());
            string SOEID = objParameter["SOEID"].ToString();

            string newApprover = objParameter["newApprover"].ToString();

            DAO.proc_Change_RequestApprover(ID, newApprover);

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.qryRequests

                    where p.ApproverID == SOEID || p.RequesterID == SOEID
                    orderby p.RequestID descending
                    select new
                    {
                        p.ProjectID
                      ,
                        p.ProjectName
                      ,
                        p.RequestID
                      ,
                        p.RequestStatus
                      ,
                        p.RequesterID
                      ,
                        p.CreatedDate
                      ,
                        p.ApproverID
                      ,
                        p.Metadata
                      ,
                        p.AdjustmentID
                    }
            });


            return Content(o["item"].ToString());

        }


        [Route("changePath")]
        public ActionResult changePath(string data)
        {
            ToDoEntities DAO = new ToDoEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["ID"].ToString());
            string SOEID = objParameter["SOEID"].ToString();

            string newPath = objParameter["newPath"].ToString();

            List<tblRequestReference> references = (from p in DAO.tblRequestReferences where p.RequestID == ID select p).ToList<tblRequestReference>();

            foreach (tblRequestReference refe in references)
            {
                DAO.tblRequestReferences.Remove(refe);
            }
            DAO.SaveChanges();

            DAO.proc_Add_Reference(ID, SOEID, newPath);

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.qryRequests

                    where p.ApproverID == SOEID || p.RequesterID == SOEID
                    orderby p.RequestID descending
                    select new
                    {
                        p.ProjectID
                      ,
                        p.ProjectName
                      ,
                        p.RequestID
                      ,
                        p.RequestStatus
                      ,
                        p.RequesterID
                      ,
                        p.CreatedDate
                      ,
                        p.ApproverID
                      ,
                        p.Metadata
                      ,
                        p.AdjustmentID
                    }
            });


            return Content(o["item"].ToString());

        }

        [Route("cancelRequest")]
        public ActionResult cancelRequest(string data)
        {
            ToDoEntities DAO = new ToDoEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["ID"].ToString());
            string SOEID = objParameter["SOEID"].ToString();

            tblStatu status = (from p in DAO.tblStatus where p.Name == "Cancel By Maker" select p).SingleOrDefault<tblStatu>();

            try { DAO.proc_Change_RequestStatus(ID, SOEID, status.ID); } catch { DAO.proc_Change_RequestStatus(ID, SOEID, 8); }
            

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.qryRequests

                    where p.ApproverID == SOEID || p.RequesterID == SOEID
                    orderby p.RequestID descending
                    select new
                    {
                        p.ProjectID
                      ,
                        p.ProjectName
                      ,
                        p.RequestID
                      ,
                        p.RequestStatus
                      ,
                        p.RequesterID
                      ,
                        p.CreatedDate
                      ,
                        p.ApproverID
                      ,
                        p.Metadata
                      ,
                        p.AdjustmentID
                    }
            });


            return Content(o["item"].ToString());

        }

        [Route("getDataToShow")]
        public ActionResult getDataToShow(string data)
        {
            ToDoEntities DAO = new ToDoEntities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int ID = Convert.ToInt32(objParameter["ID"].ToString());


            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.qryRequestByStepForCheckerMakers 
                    where p.RequestID == ID
                    select new
                    {
                        p.Milestone,
                        p.StepName,
                        p.ProjectID,
                        p.RequestID,
                        p.StepID,
                        p.RequestStatus,
                        p.CheckerStatus,
                        p.MakerStatus,
                        p.Comments,
                        p.CheckerStepFilled,
                        p.MakerStepFilled,
                        p.CheckerCommentsPending,
                        p.MakerCommentsPending,
                        p.LatestMakerComments,
                        p.LatestCheckerComments
                    }
            });


            return Content(o["item"].ToString());

        }


        [Route("proc_AddUsers")]
        public ActionResult proc_AddUsers(string data)
        {
            ToDoEntities DAO = new ToDoEntities();



            DAO.proc_AddUsers();


            return Content("");
        }



    }
}
