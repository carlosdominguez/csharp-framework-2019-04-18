﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using Automation.Web.Models.CashFlow;
using System.Globalization;
using Automation.Core.Model;

namespace Automation.Web.Controllers.BCL
{
    [RoutePrefix("BCL")]
    public class BSSAdjustmentController : Controller
    {

        [Route("BSSAdjustment")]
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            viewModel.Add("Type", "");
            viewModel.Add("Aprover", "");
            viewModel.Add("Maker", "");

            return View("~/Views/CF/BSSAdjustment.cshtml", viewModel);
        }

        [Route("BSSAdjustment/{Type?}/{Aprover?}/{Maker?}")]
        public ActionResult IndexBSSAdjustment(string Type, string Aprover, string Maker)
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            viewModel.Add("Type", Type);
            viewModel.Add("Aprover", Aprover);
            viewModel.Add("Maker", Maker);

            return View("~/Views/BCL/BSSAdjustment.cshtml", viewModel);
        }
    }
}