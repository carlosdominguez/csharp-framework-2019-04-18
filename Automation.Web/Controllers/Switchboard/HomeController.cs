using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Automation.Web.Models.Switchboard;


using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;


namespace Automation.Web.Controllers.Switchboard
{
    [RoutePrefix("Switchboard")]
    public class HomeController : Controller
    {
        //
        // GET: /
        [Route("/")]
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            string subAppPath = GlobalUtil.GetSubAppPath();
            viewModel.Add("SubAppPath", subAppPath);
            return View("~/Views/Switchboard/Home.cshtml", viewModel);

        }

        [Route("getSwitchboardData")]
        public ActionResult getSwitchboardTask()
        {
            TITANEntities1 DAO = new TITANEntities1();
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            //var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            //Dictionary<String, String> obj = new Dictionary<string, string>();

            //string goc = objParameter["GOC"].ToString().Split(' ')[0];

            DateTime Today = DateTime.Today.AddDays(-1);
            DateTime Tomorrow = Today.AddDays(2);

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.qryOperationCalendars
                    where p.Date > Today && p.Date < Tomorrow
                    orderby p.DateUTC ascending
                    select new
                    {
                       p.EventID
                      ,p.NodeID
                      ,p.GOC
                      ,p.EventName
                      ,p.StatusID
                      ,p.StatusName
                      ,p.Frequence
                      ,p.Month
                      ,p.BusinessDay
                      ,p.BusinessDayString
                      ,p.Date
                      ,p.StartTime
                      ,p.Duration
                      ,p.EndTime
                      ,p.Timezone
                      ,p.DateUTC
                      ,p.Taxonomy
                      ,p.ProcessName
                      ,p.System
                      ,p.MCA
                    }
            });


            return Content(o["item"].ToString());

        }

    }
}
