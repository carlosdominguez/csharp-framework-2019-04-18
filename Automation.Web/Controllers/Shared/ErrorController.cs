﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Automation.Core.Class;

namespace Automation.Web.Controllers.Shared
{
    [RoutePrefix("Error")]
    public class ErrorController : Controller
    {
        //
        // GET: /Error/Offline/

        [Route("Offline")]
        public ActionResult Index()
        {
            return View("~/Views/Shared/Offline.cshtml");
        }

        //
        // GET: /Error/PageNotFound/

        [Route("PageNotFound")]
        public ActionResult PageNotFound()
        {
            return View("~/Views/Shared/PageNotFound.cshtml");
        }

        //
        // GET: /Error/AccessDenied/

        [Route("AccessDenied")]
        public ActionResult AccessDenied()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            var appRequestRoleURL = GlobalUtil.GetValueAppConfig("appSettings", "AccessDeniedRequestRoleURL");
            var appReportIssueURL = GlobalUtil.GetValueAppConfig("appSettings", "AccessDeniedReportIssueURL");

            viewModel.Add("RequestRoleURL", appRequestRoleURL);
            viewModel.Add("ReportIssueURL", appReportIssueURL);

            return View("~/Views/Shared/AccessDenied.cshtml", viewModel);
        }
    }
}
