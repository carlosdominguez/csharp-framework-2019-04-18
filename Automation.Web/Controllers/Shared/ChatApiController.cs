﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.Shared
{
    public class ChatApiController : Controller
    {
        //
        // GET: /Shared/ChatApi

        public ActionResult ChatApi()
        {
            return View("~/Views/Shared/_PartialChatApi.cshtml");
        }

    }
}
