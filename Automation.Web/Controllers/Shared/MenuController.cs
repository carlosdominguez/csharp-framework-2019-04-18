﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.Shared
{
    [RoutePrefix("Shared")]
    public class MenuController : Controller
    {
        //
        // GET: /Shared/Menu
        [Route("Menu")]
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            viewModel.Add("UserInfo", GlobalUtil.GetUserInfo());

            return View("~/Views/Shared/_PartialMenu.cshtml", viewModel);
        }

    }
}
