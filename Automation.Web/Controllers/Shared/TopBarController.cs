﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.Shared
{
    [RoutePrefix("Shared")]
    public class TopBarController : Controller
    {
        //
        // GET: /Shared/TopBar
        [Route("TopBar")]
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            
            viewModel.Add("UserInfo", GlobalUtil.GetUserInfo());
            viewModel.Add("AppShortName", GlobalUtil.GetCurrentAppInfo().ShortName);
            viewModel.Add("SubAppPath", GlobalUtil.GetSubAppPath());
            viewModel.Add("ServerURL", GlobalUtil.GetUrlBase());
            viewModel.Add("AppKey", GlobalUtil.GetAppKey());
            viewModel.Add("Enviroment", GlobalUtil.GetEnviroment());
            viewModel.Add("SimulateUser", Session["TestUser"]);

            return View("~/Views/Shared/_PartialTopBar.cshtml", viewModel);
        }

    }
}
