﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers
{
    [RoutePrefix("Ajax")]
    public class AjaxController : AsyncController
    {
        //
        // GET: /Ajax/JqxGrid/

        [ValidateInput(false)]
        [Route("JqxGrid")]
        public ActionResult JqxGrid()
        {
            //Get resul
            List<Dictionary<string, string>> resultList = null;

            //Get JqxGrid Definition Json
            string json = Request.Params["pjqxGridJson"];

            //Get JqxGrid Definition
            JqxGrid objJqxGrid = GlobalUtil.GetJqxGridFromJson(json);

            //Get where filter
            string pwhere = GlobalUtil.GetWhereFilters();

            //Procedure Name
            var spDBName = objJqxGrid.sp.Name;

            //Procedure Params
            var spDBParams = objJqxGrid.sp.Params;

            //SQL
            var sqlDB = objJqxGrid.sp.SQL;

            //Add pwhere to procedure params
            if (spDBParams == null)
            {
                spDBParams = new List<Dictionary<string, string>>();
            }
            if (Request.Params["recordstartindex"] != null)
            {
                spDBParams.Add(new Dictionary<string, string>() { { "Name", "@StrWhere" }, { "Value", pwhere } });
                spDBParams.Add(new Dictionary<string, string>() { { "Name", "@StrOrderBy" }, { "Value", GlobalUtil.GetOrderBy() } });
                spDBParams.Add(new Dictionary<string, string>() { { "Name", "@StartIndexRow" }, { "Value", Request.Params["recordstartindex"] } });
                spDBParams.Add(new Dictionary<string, string>() { { "Name", "@EndIndexRow" }, { "Value", Request.Params["recordendindex"] } });

                //HttpContext.Current.Session["jqxGridFilterID"] = GlobalUtil.CreateCustomID();
                //HttpContext.Current.Session["jqxGridWhere"] = pwhere;
                //HttpContext.Current.Session["jqxGridSort"] = GlobalUtil.GetOrderBy();
            }
            if (Request.Params["pmenuAction"] != null)
            {
                spDBParams.Add(new Dictionary<string, string>() { { "Name", "@MenuAction" }, { "Value", Request.Params["pmenuAction"] } });
            }
            if (Request.Params["pmenuParam"] != null)
            {
                spDBParams.Add(new Dictionary<string, string>() { { "Name", "@MenuParam" }, { "Value", Request.Params["pmenuParam"] } });
            }
            if (Request.Params["pyear"] != null)
            {
                spDBParams.Add(new Dictionary<string, string>() { { "Name", "@Year" }, { "Value", Request.Params["pyear"] } });
            }
            if (Request.Params["pmonth"] != null)
            {
                spDBParams.Add(new Dictionary<string, string>() { { "Name", "@Month" }, { "Value", Request.Params["pmonth"] } });
            }

            //Columns DB To Select
            var pcolsToSelect = new List<Dictionary<string, string>>();
            foreach (var col in objJqxGrid.columns)
            {
                if (col != null && !(col.ContainsKey("computedcolumn") && col.ContainsKey("computedcolumn") == true))
                {
                    pcolsToSelect.Add(new Dictionary<string, string>() { { "Name", col["name"].ToString() } });
                }
            }

            //Get resul
            if (String.IsNullOrEmpty(sqlDB))
            {
                resultList = (new GlobalModel()).getResultList(spDBName, spDBParams, pcolsToSelect);
            }
            else
            {
                resultList = (new GlobalModel()).getResultListBySQL(sqlDB);
            }

            //Excecute procedure
            //result = (new GlobalModel()).excecuteProcedureReturn(spDBName, spDBParams);

            //html = Session_Util.SerializeObject(resultList);
            var jsonAction = Json(resultList, JsonRequestBehavior.AllowGet);
            jsonAction.MaxJsonLength = int.MaxValue;

            return jsonAction;
        }

        //
        // GET: /Ajax/CallProcedure/

        [ValidateInput(false)]
        [Route("CallProcedure")]
        public async Task<ActionResult> CallProcedure()
        {
            List<Dictionary<string, string>> result = new List<Dictionary<string, string>>();

            //Get JqxGrid Definition Json
            string json = Request.Params["pdataProcedure"];
            string hasResponse = String.IsNullOrEmpty(Request.Params["presponse"]) ? "1" : Request.Params["presponse"];

            //Get JqxGrid Definition
            JqxGrid objJqxGrid = GlobalUtil.GetJqxGridFromJson(json);

            //Procedure Name
            var spDBName = objJqxGrid.sp.Name;

            //Procedure Params
            var spDBParams = objJqxGrid.sp.Params;

            //Excecute procedure
            if (hasResponse == "1")
            {
                result = (new GlobalModel()).excecuteProcedureReturn(spDBName, spDBParams);
            }
            else
            {
                int affectedRows = (new GlobalModel()).excecuteProcedureNoReturn(spDBName, spDBParams);
                result.Add(new Dictionary<string, string> { 
                    {"AffectedRows", affectedRows.ToString()}
                });
            }

            return await Task.Run(() => Json(result, JsonRequestBehavior.AllowGet));
        }

        //
        // GET: /Ajax/ExecQuery/

        [ValidateInput(false)]
        [Route("ExecQuery")]
        public async Task<ActionResult> ExecQuery(string pjsonSql)
        {
            var sql = System.Web.Helpers.Json.Decode(pjsonSql);
            
            //Get resul
            List<Dictionary<string, string>> resultList = (new GlobalModel()).excecuteQuery(sql);

            if (Request.QueryString["pforceSessionRefresh"] != null && Request.QueryString["pforceSessionRefresh"] == "1")
            {
                //Refresh session with new app
                GlobalUtil.SessionStart(true);
            }

            return await Task.Run(() => Json(resultList, JsonRequestBehavior.AllowGet));
        }

        //
        // GET: /Ajax/DownloadExcel/

        [ValidateInput(false)]
        [Route("DownloadExcel")]
        public void DownloadExcel()
        {
            DataTable dtResult = null;
            var jsonSQL = Request.QueryString["pjsonSql"];
            var fileName = Request.QueryString["pfilename"];

            jsonSQL = jsonSQL.Replace(@"_i_", @"%");
            jsonSQL = jsonSQL.Replace(@"_u_", @"'");
            jsonSQL = jsonSQL.Replace(@"_u1_", @"<>");
            jsonSQL = jsonSQL.Replace(@"_u2_", @"""");
            jsonSQL = jsonSQL.Replace(@"_u3_", @"*");
            jsonSQL = jsonSQL.Replace(@"_y1_", @".");
            jsonSQL = jsonSQL.Replace(@"_y2_", @"[");
            jsonSQL = jsonSQL.Replace(@"_y3_", @"]");
            jsonSQL = jsonSQL.Replace(@"_y4_", @"=");
            jsonSQL = jsonSQL.Replace(@"_y5_", @"<");
            jsonSQL = jsonSQL.Replace(@"_y6_", @">");
            jsonSQL = jsonSQL.Replace(@"_y7_", @" ");

            string sql = System.Web.Helpers.Json.Decode(jsonSQL);

            //Excecute procedure
            dtResult = (new GlobalModel()).getDataTableBySQL(sql, 0);

            Automation.Utils.Excel.FasterExcel(HttpContext.Response, dtResult, Request.Params["pfilename"]);
        }

        //
        // GET: /Ajax/GetAppsInfo

        [Route("GetAppsInfo")]
        public ActionResult List()
        {
            string _pathJsonAppsInfo = GlobalUtil.GetValueGlobalConfig("appSettings", "PathAppsInfo");
            var jsonAppsInfo = new StringBuilder(System.IO.File.ReadAllText(@"" + Server.MapPath(_pathJsonAppsInfo)));

            return Content(jsonAppsInfo.ToString());
        }

        //
        // POST: /Ajax/SaveAppsInfo

        [ValidateInput(false)]
        [Route("SaveAppsInfo")]
        public ActionResult Save(string pjson)
        {
            //write string to file
            string _pathJsonDBStructure = GlobalUtil.GetValueGlobalConfig("appSettings", "PathAppsInfo");
            System.IO.File.WriteAllText(@"" + Server.MapPath(_pathJsonDBStructure), pjson);

            return Content("Saved!");
        }
    }
}
