﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using System.Net;
using System.Collections.Specialized;
using System.IO;
using Microsoft.SharePoint.Client;
using System.Web.SessionState;

namespace Automation.Web.Controllers
{
    [RoutePrefix("Shared")]
    [SessionState(SessionStateBehavior.ReadOnly)] 

    public class SharedController : AsyncController
    {
        //
        // GET: /Shared/SimulateUser/

        [Route("SimulateUser")]
        public ActionResult SimulateUser()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            return View("~/Views/Shared/SimulateUser.cshtml", viewModel);
        }

        //
        // GET: /Shared/JqxGrid/

        [ValidateInput(false)]
        [Route("JqxGrid")]
        public ActionResult JqxGrid()
        {
            //Get resul
            List<Dictionary<string, string>> resultList = null;

            //Get JqxGrid Definition Json
            string json = Request.Params["pjqxGridJson"];

            //Get JqxGrid Definition
            JqxGrid objJqxGrid = GlobalUtil.GetJqxGridFromJson(json);

            //Get where filter
            string pwhere = GlobalUtil.GetWhereFilters();

            //Get Order By Filter
            string porderBy = GlobalUtil.GetOrderBy();

            //Procedure Name
            var spDBName = objJqxGrid.sp.Name;

            //Procedure Params
            var spDBParams = objJqxGrid.sp.Params;

            //SQL
            var sqlDB = objJqxGrid.sp.SQL;

            //Add pwhere to procedure params
            if (spDBParams == null)
            {
                spDBParams = new List<Dictionary<string, string>>();
            }
            if (Request.Params["recordstartindex"] != null)
            {
                spDBParams.Add(new Dictionary<string, string>() { { "Name", "@StrWhere" }, { "Value", pwhere } });
                spDBParams.Add(new Dictionary<string, string>() { { "Name", "@StrOrderBy" }, { "Value", porderBy } });
                spDBParams.Add(new Dictionary<string, string>() { { "Name", "@StartIndexRow" }, { "Value", Request.Params["recordstartindex"] } });
                spDBParams.Add(new Dictionary<string, string>() { { "Name", "@EndIndexRow" }, { "Value", Request.Params["recordendindex"] } });

                //Session["jqxGridFilterID"] = GlobalUtil.CreateCustomID();
                //Session["jqxGridWhere"] = pwhere;
                //Session["jqxGridSort"] = porderBy;
            }
            if (Request.Params["pmenuAction"] != null)
            {
                spDBParams.Add(new Dictionary<string, string>() { { "Name", "@MenuAction" }, { "Value", Request.Params["pmenuAction"] } });
            }
            if (Request.Params["pmenuParam"] != null)
            {
                spDBParams.Add(new Dictionary<string, string>() { { "Name", "@MenuParam" }, { "Value", Request.Params["pmenuParam"] } });
            }
            if (Request.Params["pyear"] != null)
            {
                spDBParams.Add(new Dictionary<string, string>() { { "Name", "@Year" }, { "Value", Request.Params["pyear"] } });
            }
            if (Request.Params["pmonth"] != null)
            {
                spDBParams.Add(new Dictionary<string, string>() { { "Name", "@Month" }, { "Value", Request.Params["pmonth"] } });
            }

            //Columns DB To Select
            var pcolsToSelect = new List<Dictionary<string, string>>();
            foreach (var col in objJqxGrid.columns)
            {
                if (col != null && !(col.ContainsKey("computedcolumn") && col.ContainsKey("computedcolumn") == true))
                {
                    pcolsToSelect.Add(new Dictionary<string, string>() { { "Name", col["name"].ToString() } });
                }
            }

            //Get resul
            if (String.IsNullOrEmpty(sqlDB))
            {
                //(new GlobalModel()).excecuteProcedureReturn(
                resultList = (new GlobalModel()).getResultListJqxGrid(spDBName, spDBParams, pcolsToSelect);
            }
            else
            {
                resultList = (new GlobalModel()).getResultListBySQL(sqlDB);
            }

            //Excecute procedure
            //result = (new GlobalModel()).excecuteProcedureReturn(spDBName, spDBParams);

            //html = Session_Util.SerializeObject(resultList);
            JsonResult jsonAction = null;
            int totalRows = 0;

            if (resultList.Count > 0)
            {
                if (json.Contains("Server Paging"))
                {
                    totalRows = Convert.ToInt32(resultList[0]["TotalRows"]);
                    jsonAction = Json(new
                    {
                        FilterID = GlobalUtil.CreateCustomID(),
                        Where = pwhere,
                        Sort = porderBy,
                        TotalRows = totalRows,
                        Rows = resultList
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    jsonAction = Json(resultList, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                jsonAction = Json(resultList, JsonRequestBehavior.AllowGet);
            }

            jsonAction.MaxJsonLength = int.MaxValue;

            return jsonAction;
        }

        //
        // GET: /Shared/CallProcedure/

        [ValidateInput(false)]
        [Route("CallProcedure")]
        public async Task<ActionResult> CallProcedure()
        {
            List<Dictionary<string, string>> result = new List<Dictionary<string, string>>();

            //Get JqxGrid Definition Json
            string json = Request.Params["pdataProcedure"];
            string hasResponse = String.IsNullOrEmpty(Request.Params["presponse"]) ? "1" : Request.Params["presponse"];

            //Get JqxGrid Definition
            JqxGrid objJqxGrid = GlobalUtil.GetJqxGridFromJson(json);

            //Procedure Name
            var spDBName = objJqxGrid.sp.Name;

            //Procedure Params
            var spDBParams = objJqxGrid.sp.Params;

            //Excecute procedure
            if (hasResponse == "1")
            {
                result = (new GlobalModel()).excecuteProcedureReturn(spDBName, spDBParams);
            }
            else
            {
                int affectedRows = (new GlobalModel()).excecuteProcedureNoReturn(spDBName, spDBParams);
                result.Add(new Dictionary<string, string> { 
                    {"AffectedRows", affectedRows.ToString()}
                });
            }

            return await Task.Run(() => Json(result, JsonRequestBehavior.AllowGet));
        }

        //
        // GET: /Shared/UnderManteinance/

        //[ValidateInput(false)]
        //[Route("UnderManteinance")]
        //public async Task<ActionResult> UnderManteinance()
        //{
        //    Dictionary<string, object> viewModel = new Dictionary<string, object>();
        //    var tableName = GlobalUtil.GetDBObjectFullName("GlobalVar");
        //    var isUnder = "0";
        //    var underMsg = "";

        //    var sqlUnder = 
        //        "SELECT " +
        //        "    (SELECT [VAR_VALUE]  FROM " + tableName + " WHERE [VAR_NAME] = 'IS_UNDERMAINTENANCE' ) AS IS_UNDERMAINTENANCE, " +
        //        "    (SELECT [VAR_VALUE]  FROM " + tableName + " WHERE [VAR_NAME] = 'UNDERMAINTENANCE_MESSAGE' ) AS UNDERMAINTENANCE_MESSAGE ";

        //    //Get resul
        //    List<Dictionary<string, string>> resultList = (new GlobalModel()).excecuteQuery(sqlUnder);

        //    if (resultList.Count > 0)
        //    {
        //        isUnder = resultList[0]["IS_UNDERMAINTENANCE"];
        //        underMsg = resultList[0]["UNDERMAINTENANCE_MESSAGE"];
        //    }

        //    viewModel.Add("UnderMaintenanceMessage", underMsg);

        //    return await Task.Run(() => View("~/Views/Shared/UnderMaintenance.cshtml", viewModel));
        //}
        
        //
        // GET: /Shared/MCAUpdate/

        [ValidateInput(false)]
        [Route("MCAUpdate")]
        public async Task<ActionResult> MCAUpdate()
        {
            var sharePointSiteUrl = GlobalUtil.GetValueGlobalConfig("appSettings", "MCASharePointSiteUrl");
            var sharePointListTitle = GlobalUtil.GetValueGlobalConfig("appSettings", "MCASharePointListTitle");
            var availableSharePointColumns = new Dictionary<string, bool> { 
                { "ID", true },
                { "Title", true },
                { "GPL", true },
                { "RPL", true },
                { "Function", true },
                { "FRO_x0020_Centers", true },
                { "Critical_x0020_Process_x0020_Lev", true },
                { "Critical_x0020_Process_x0020_Lev0", true },
                { "Critical_x0020_Process_x0020_Lev1", true },
                { "Critical_x0020_Process_x0020_Lev2", true },
                { "Critical_x0020_Process_x0020__x0", true },
                { "Critical_x0020_Process_x0020__x00", true },
                { "Platform_x0020__x0028_Systems_x0", true },
                { "Control_x0020_Level_x0020_1", true },
                { "Control_x0020_Level_x0020_2", true },
                { "Control_x0020_Level_x0020_3", true },
                { "Control_x0020_Level_x0020_4", true },
                { "Control_x0020_Level_x0020_5", true },
                { "Created", true },
                { "Author", true },
                { "Modified", true },
                { "Editor", true }
            };
            var clientContext = new ClientContext(sharePointSiteUrl);
            List mcaList = clientContext.Web.Lists.GetByTitle(sharePointListTitle);

            CamlQuery query = CamlQuery.CreateAllItemsQuery();
            //CamlQuery query = CamlQuery.CreateAllItemsQuery(100);
            ListItemCollection items = mcaList.GetItems(query);

            clientContext.Load(items);
            clientContext.ExecuteQuery();

            DataTable dtMCA = new DataTable();
            bool dtColumnsReady = false;

            foreach (ListItem listItem in items)
            {
                if (!dtColumnsReady)
                {
                    //Create columns for tables
                    foreach (var sharePointCol in availableSharePointColumns.Keys)
                    //foreach (var sharePointCol in listItem.FieldValues.Keys)
                    {
                        dtMCA.Columns.Add(sharePointCol, typeof(String));
                    }

                    dtColumnsReady = true;
                }

                //Add new row
                DataRow dr = dtMCA.NewRow();
                foreach (var sharePointCol in availableSharePointColumns.Keys)
                //foreach (var sharePointCol in listItem.FieldValues.Keys)
                {
                    switch (sharePointCol)
                    {
                        case "Author":
                        case "Editor":
                            dr[sharePointCol] = ((FieldUserValue)listItem[sharePointCol]).LookupValue;
                            break;

                        default:
                            if (listItem[sharePointCol] != null)
                            {
                                dr[sharePointCol] = GlobalUtil.StripHTML(listItem[sharePointCol].ToString());
                            }
                            else
                            {
                                dr[sharePointCol] = null;
                            }

                            break;
                    }
                    
                }

                dtMCA.Rows.Add(dr);
            }

            //Save data in the database
            (new GlobalModel()).SaveDataTableDB(dtMCA, "[Automation].[dbo].[spAFrwkMCAUpload]");

            //Get Info of all columns in the sharepoint
            //var columnsInfo = MCAGetColumnsInfo(items);
            
            return await Task.Run(() => Content(dtMCA.Rows.Count + " MCA Controls Processed"));
            //return await Task.Run(() => Content(JsonConvert.SerializeObject(dtMCA, Formatting.Indented)));
        }
        
        public Dictionary<string, Dictionary<string, string>> MCAGetColumnsInfo(ListItemCollection items)
        {

            var columnsInfo = new Dictionary<string, Dictionary<string, string>>();

            foreach (ListItem listItem in items)
            {
                foreach (string columnName in listItem.FieldValues.Keys)
                {
                    //Add Column Information
                    if (!columnsInfo.ContainsKey(columnName))
                    {
                        columnsInfo.Add(columnName, new Dictionary<string, string>());
                        columnsInfo[columnName]["MaxLength"] = "0";
                        //columnInfo[columnName]["MaxText"] = "";
                        columnsInfo[columnName]["Type"] = "";
                    }

                    if (listItem[columnName] != null)
                    {
                        if (int.Parse(columnsInfo[columnName]["MaxLength"]) < (listItem[columnName].ToString()).Length)
                        {
                            columnsInfo[columnName]["MaxLength"] = (listItem[columnName].ToString()).Length + "";
                            //columnInfo[columnName]["MaxText"] = listItem[columnName].ToString();
                            columnsInfo[columnName]["Type"] = listItem[columnName].GetType().ToString();
                        }
                    }
                }
            }

            return columnsInfo;
        }

        //
        // GET: /Shared/ExecQuery/

        [ValidateInput(false)]
        [Route("ExecQuery")]
        public async Task<ActionResult> ExecQuery(string pjsonSql)
        {
            var sql = System.Web.Helpers.Json.Decode(pjsonSql);
            
            //Get resul
            List<Dictionary<string, string>> resultList = (new GlobalModel()).excecuteQuery(sql);

            if (Request.QueryString["pforceSessionRefresh"] != null && Request.QueryString["pforceSessionRefresh"] == "1")
            {
                //Refresh session with new app
                GlobalUtil.SessionStart(true);
            }

            return await Task.Run(() => Json(resultList, JsonRequestBehavior.AllowGet));
        }

        //
        // GET: /Shared/FindEmployee/

        [ValidateInput(false)]
        [Route("FindEmployee")]
        public async Task<ActionResult> FindEmployee()
        {
            var spName = Request.QueryString["spName"];
            var query = (String.IsNullOrEmpty(Request.QueryString["q"]) ? "" : Request.QueryString["q"]);

            var resultList = (new GlobalModel()).excecuteProcedureReturn(spName, new List<Dictionary<string, string>> { 
                new Dictionary<string, string>
                {
                    { "Name", "@SearchQuery" },
                    { "Value", query }
                }
            });

            return await Task.Run(() => Json(resultList, JsonRequestBehavior.AllowGet));
        }

        //
        // GET: /Shared/KillCurrentProcess/

        [ValidateInput(false)]
        [Route("KillCurrentProcess")]
        public ContentResult KillCurrentProcess()
        {
            //Force to kill Current Process to clean RAM for "w3wp.exe IIS Worker Process"
            System.Diagnostics.Process.GetCurrentProcess().Kill();

            return Content("Done");
        }

        //
        // GET: /Shared/CleanMemory/

        [ValidateInput(false)]
        [Route("CleanMemory")]
        public ContentResult CleanMemory()
        {
            //Force to Garbage Collector to clean RAM for "w3wp.exe IIS Worker Process"
            System.Diagnostics.Process.GetCurrentProcess().Dispose();
            GC.Collect();

            return Content("Done");
        }

        // GET: /Shared/DownloadFile
        [ValidateInput(false)]
        [Route("DownloadFile")]
        public FileResult DownloadFile()
        {
            string pfileName = Request.Params["pfileName"];
            string purlCopyFrom = Request.Params["purlCopyFrom"];
            
            byte[] fileBytes = System.IO.File.ReadAllBytes(purlCopyFrom);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, pfileName);
        }

        [ValidateInput(false)]
        [Route("DownloadExcel")]
        public void DownloadExcel()
        {
            DataTable dtResult = null;
            var sql = Request.QueryString["psql"];
            var fileName = Request.QueryString["pfilename"];
            var sheetName = (String.IsNullOrEmpty(Request.QueryString.Get("psheetname"))) ? "Report" : Request.QueryString.Get("psheetname");
            var pathToSave = GlobalUtil.DecodeSkipSideminder((String.IsNullOrEmpty(Request.QueryString.Get("ppathToSave"))) ? "" : Request.QueryString.Get("ppathToSave"));

            sql = GlobalUtil.DecodeSkipSideminder(sql);

            //Excecute procedure
            dtResult = (new GlobalModel()).getDataTableBySQL(sql, 0);

            Automation.Utils.Excel.FasterExcel(HttpContext.Response, dtResult, Request.Params["pfilename"], sheetName, pathToSave);
        }


        [ValidateInput(false)]
        [Route("DownloadExcelJson")]
        public void DownloadExcelJson()
        {
            DataTable dtResult = null;
            var fileName = Request.QueryString["pfilename"];
            var sheetName = (String.IsNullOrEmpty(Request.QueryString.Get("psheetname"))) ? "Report" : Request.QueryString.Get("psheetname");
            var pathToSave = GlobalUtil.DecodeSkipSideminder((String.IsNullOrEmpty(Request.QueryString.Get("ppathToSave"))) ? "" : Request.QueryString.Get("ppathToSave"));
            
            //Excecute procedure
            dtResult = (DataTable)JsonConvert.DeserializeObject(Request.Params["jsonData"], (typeof(DataTable)));

            Automation.Utils.Excel.FasterExcel(HttpContext.Response, dtResult, Request.Params["pfilename"], sheetName, pathToSave);
        }

        [ValidateInput(false)]
        [Route("DownloadExcelTempFile")]
        public void DownloadExcelTempFile()
        {
            DataTable dtResult = null;
            var sql = Request.QueryString["psql"];
            var fileName = Request.QueryString["pfilename"];
            var sheetName = (String.IsNullOrEmpty(Request.QueryString.Get("psheetname"))) ? "Report" : Request.QueryString.Get("psheetname");
            var pathToSave = GlobalUtil.DecodeSkipSideminder((String.IsNullOrEmpty(Request.QueryString.Get("ppathToSave"))) ? "" : Request.QueryString.Get("ppathToSave"));

            sql = GlobalUtil.DecodeSkipSideminder(sql);

            //Excecute procedure
            dtResult = (new GlobalModel()).getDataTableBySQL(sql, 0);

            string tempDownloadsFolder = "~/Content/Shared/tempData/";
            string fullPathTempDownloadsFolder = @"" + Server.MapPath(tempDownloadsFolder);

            Automation.Utils.Excel.FasterExcelCopyServer(HttpContext.Response, dtResult, Request.Params["pfilename"], fullPathTempDownloadsFolder, sheetName, pathToSave);
        }

        //
        // GET: /Shared/RefreshSession/

        [ValidateInput(false)]
        [Route("RefreshSession")]
        public ActionResult RefreshSession()
        {
            //Refresh session with new app
            GlobalUtil.SessionStart(true);

            return Json(new { msg = "Session Refreshed!" }, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Shared/AddSessionMessage/

        [ValidateInput(false)]
        [Route("AddSessionMessage")]
        public ActionResult AddSessionMessage(string pmessage)
        {
            GlobalUtil.AddSessionMessage("Error", pmessage, false, null);
            return Json(new { msg = "Session Message Done!" }, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Shared/ConnectSSOHttpClient/

        [ValidateInput(false)]
        [Route("ConnectSSOHttpClient")]
        public ActionResult ConnectSSOHttpClient()
        {
            //var urlToConnect = "https://cedt-jira.nam.nsroot.net/jira/secure/project/ViewProjects.jspa";
            var urlToConnect = "https://host50.dcs.citicorp.com:14009";
            var SSOUserID = "CD25867";
            var SSOPassword = "Abc123!17";
            var response = "";
            var responseText = "";
            var currentURL = "";
            var SMAgent = "";

            using (var client = new CustomWebClient())
            {
                //Get Single Sing On Form
                responseText = client.DownloadString(urlToConnect);
                currentURL = client.ResponseUri.AbsoluteUri;

                SMAgent = Strings.Mid(responseText, Strings.InStr(responseText, @"type=hidden name=smagentname value=""") + 36, 255);
                SMAgent = Strings.Left(SMAgent, Strings.InStr(SMAgent, @"""") - 1);

                var PostStr = "SMENC=ISO-8859-1&SMLOCALE=US-EN&USER=" + HttpUtility.UrlEncode(SSOUserID) + "&PASSWORD=" + HttpUtility.UrlEncode(SSOPassword) + "&target=%2FGLweb_modules_Rel_1.0%2Fhtml%2Findex.jsp&smquerydata=&smauthreason=0&smagentname=" + HttpUtility.UrlEncode(SMAgent) + "&postpreservationdata=";

                //Send Login User and Password
                var data = new NameValueCollection();
                data["SMENC"] = "ISO-8859-1";
                data["SMLOCALE"] = "US-EN";
                data["USER"] = HttpUtility.UrlEncode(SSOUserID);
                data["PASSWORD"] = HttpUtility.UrlEncode(SSOPassword);
                data["target"] = "/GLweb_modules_Rel_1.0/html/index.jsp";
                data["smquerydata"] = "";
                data["smauthreason"] = "0";
                data["smagentname"] = SMAgent;
                data["postpreservationdata"] = "";

                client.Headers["Accept"] = "image/gif, image/jpeg, image/pjpeg, image/pjpeg, application/x-shockwave-flash, application/xaml+xml, application/vnd.ms-xpsdocument, application/x-ms-xbap, application/x-ms-application, application/x-silverlight, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*";
                client.Headers["Referer"] = currentURL;
                client.Headers["Accept-Language"] = "en-us";
                client.Headers["User-Agent"] = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 1.1.4322; .NET CLR 3.0.04506.30; .NET CLR 3.0.04506.648; InfoPath.1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)";
                client.Headers["Content-Type"] = "application/x-www-form-urlencoded";
                client.Headers["Accept-Encoding"] = "gzip, deflate";
                client.Headers["Host"] = "host50.dcs.citicorp.com:14009";
                //client.Headers["Content-Length"] = Strings.Len(PostStr).ToString();
                //client.Headers["Connection"] = "Keep-Alive";
                client.Headers["Cache-Control"] = "no-cache";

                byte[] responseArray = client.UploadValues(currentURL, "POST", data);
                responseText = System.Text.Encoding.UTF8.GetString(responseArray);

                //HttpReq.open("POST", CurrentURL, False)
                //HttpReq.setOption(MSXML2.SERVERXMLHTTP_OPTION.SXH_OPTION_IGNORE_SERVER_SSL_CERT_ERROR_FLAGS, 13056)
                //HttpReq.setRequestHeader("Accept", "image/gif, image/jpeg, image/pjpeg, image/pjpeg, application/x-shockwave-flash, application/xaml+xml, application/vnd.ms-xpsdocument, application/x-ms-xbap, application/x-ms-application, application/x-silverlight, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*")
                //HttpReq.setRequestHeader("Referer", CurrentURL)
                //HttpReq.setRequestHeader("Accept-Language", "en-us")
                //HttpReq.setRequestHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 1.1.4322; .NET CLR 3.0.04506.30; .NET CLR 3.0.04506.648; InfoPath.1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)")
                //HttpReq.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
                //HttpReq.setRequestHeader("Accept-Encoding", "gzip, deflate")
                //HttpReq.setRequestHeader("Host", "host50.dcs.citicorp.com:14009")
                //HttpReq.setRequestHeader("Content-Length", CStr(Len(PostStr)))
                //HttpReq.setRequestHeader("Connection", "Keep-Alive")
                //HttpReq.setRequestHeader("Cache-Control", "no-cache")
                //HttpReq.send(PostStr)
            }

            /*var HTTPRequest = new MSXML2.ServerXMLHTTP();
            HTTPRequest.open("GET", urlToConnect, false);
            HTTPRequest.setOption(MSXML2.SERVERXMLHTTP_OPTION.SXH_OPTION_IGNORE_SERVER_SSL_CERT_ERROR_FLAGS, 13056);
            HTTPRequest.send();

            var responseText = "";
            var responseBody = (System.Array)HTTPRequest.responseBody;
            for (int i = 1; i < responseBody.Length; i++)
            {
                responseText = responseText + Char.ConvertFromUtf32(Convert.ToInt32(responseBody.GetValue(i - 1)));
            }*/

            
            //var CurrentURL = HttpReq.getOption((MSXML2.SERVERXMLHTTP_OPTION) MSXML2.SERVERXMLHTTP_OPTION.SXH_OPTION_URL);
            //dynamic optionUrl = -1; 
            // MSXML2.SERVERXMLHTTP_OPTION.SXH_OPTION_URL;
            //var CurrentURL = HTTPRequest.getOption(optionUrl);

            //var CurrentURL = objHttpReq.GetOption("SXH_OPTION_URL");
            //HttpReq.getResponseHeader("Location");
            //(SERVERXMLHTTP_OPTION option, VARIANT * value);

            //return Content(responseText);
            return Json(new
            {
                msg = "Session Refreshed!",
                SMAgent = SMAgent,
                CurrentURL = currentURL,
                response = responseText
            }, JsonRequestBehavior.AllowGet);
        }
        
        //
        // GET: /Shared/ConnectSSO/

        //[ValidateInput(false)]
        //[Route("ConnectSSO")]
        //public ActionResult ConnectSSO()
        //{
        //    //var urlToConnect = "https://cedt-jira.nam.nsroot.net/jira/secure/project/ViewProjects.jspa";
        //    var urlToConnect = "https://host50.dcs.citicorp.com:14009";
        //    var SSOUserID = "CD25867";
        //    var SSOPassword = "Abc123!17";

        //    var HTTPRequest = new MSXML2.ServerXMLHTTP();
        //    HTTPRequest.open("GET", urlToConnect, false);
        //    HTTPRequest.setOption(MSXML2.SERVERXMLHTTP_OPTION.SXH_OPTION_IGNORE_SERVER_SSL_CERT_ERROR_FLAGS, 13056);
        //    HTTPRequest.send();

        //    var responseText = "";
        //    var responseBody = (System.Array)HTTPRequest.responseBody;
        //    for (int i = 1; i < responseBody.Length; i++)
        //    {
        //        responseText = responseText + Char.ConvertFromUtf32(Convert.ToInt32(responseBody.GetValue(i - 1)));
        //    }

        //    var SMAgent = Strings.Mid(responseText, Strings.InStr(responseText, @"type=hidden name=smagentname value=""") + 36, 255);
        //    SMAgent = Strings.Left(SMAgent, Strings.InStr(SMAgent, @"""") - 1);

        //    //var PostStr = "SMENC=ISO-8859-1&SMLOCALE=US-EN&USER=" + HttpUtility.UrlEncode(SSOUserID) + "&PASSWORD=" + HttpUtility.UrlEncode(SSOPassword) + "&target=%2FGLweb_Access_Controller%2Fhtml%2Findex.jsp&smquerydata=&smauthreason=0&smagentname=" + HttpUtility.UrlEncode(SMAgent) + "&postpreservationdata=";
        //    var PostStr = "SMENC=ISO-8859-1&SMLOCALE=US-EN&USER=" + HttpUtility.UrlEncode(SSOUserID) + "&PASSWORD=" + HttpUtility.UrlEncode(SSOPassword) + "&target=%2FGLweb_modules_Rel_1.0%2Fhtml%2Findex.jsp&smquerydata=&smauthreason=0&smagentname=" + HttpUtility.UrlEncode(SMAgent) + "&postpreservationdata=";
        //    //var CurrentURL = HttpReq.getOption((MSXML2.SERVERXMLHTTP_OPTION) MSXML2.SERVERXMLHTTP_OPTION.SXH_OPTION_URL);
        //    //dynamic optionUrl = -1; // MSXML2.SERVERXMLHTTP_OPTION.SXH_OPTION_URL;
        //    //var CurrentURL = HTTPRequest.getOption(optionUrl);

        //    var currentURL = "";
        //    using (var client = new CustomWebClient())
        //    {
        //        //Get Single Sing On Form
        //        client.DownloadString(urlToConnect);
        //        currentURL = client.ResponseUri.AbsoluteUri;
        //    }

        //    //var CurrentURL = objHttpReq.GetOption("SXH_OPTION_URL");
        //    //HttpReq.getResponseHeader("Location");
        //    //(SERVERXMLHTTP_OPTION option, VARIANT * value);  

        //    HTTPRequest.open("POST", currentURL, true);
        //    HTTPRequest.setOption(MSXML2.SERVERXMLHTTP_OPTION.SXH_OPTION_IGNORE_SERVER_SSL_CERT_ERROR_FLAGS, 13056);
        //    HTTPRequest.setRequestHeader("Accept", "image/gif, image/jpeg, image/pjpeg, image/pjpeg, application/x-shockwave-flash, application/xaml+xml, application/vnd.ms-xpsdocument, application/x-ms-xbap, application/x-ms-application, application/x-silverlight, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*");
        //    HTTPRequest.setRequestHeader("Referer", currentURL);
        //    HTTPRequest.setRequestHeader("Accept-Language", "en-us");
        //    HTTPRequest.setRequestHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 1.1.4322; .NET CLR 3.0.04506.30; .NET CLR 3.0.04506.648; InfoPath.1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)");
        //    HTTPRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        //    HTTPRequest.setRequestHeader("Accept-Encoding", "gzip, deflate");
        //    HTTPRequest.setRequestHeader("Host", "host50.dcs.citicorp.com:14009");
        //    HTTPRequest.setRequestHeader("Content-Length", Strings.Len(PostStr).ToString());
        //    HTTPRequest.setRequestHeader("Connection", "Keep-Alive");
        //    HTTPRequest.setRequestHeader("Cache-Control", "no-cache");
        //    HTTPRequest.send(PostStr);

        //    responseText = "";
        //    var responseBodyLogin = (System.Array)HTTPRequest.responseBody;
        //    for (int i = 1; i < responseBodyLogin.Length; i++)
        //    {
        //        responseText = responseText + Char.ConvertFromUtf32(Convert.ToInt32(responseBodyLogin.GetValue(i - 1)));
        //    }

        //    //return Content(responseText);
        //    return Json(new { 
        //        msg = "Session Refreshed!", 
        //        SMAgent = SMAgent,
        //        PostStr = PostStr,
        //        CurrentURL = currentURL,
        //        response = responseText
        //    }, JsonRequestBehavior.AllowGet);
        //}
    }
}
