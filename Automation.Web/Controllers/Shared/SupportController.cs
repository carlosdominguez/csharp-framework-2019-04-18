﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.Shared
{
    public class SupportController : Controller
    {
        //
        // GET: /SharedSupport

        public ActionResult Index()
        {
            return View("~/Views/Shared/_PartialSupport.cshtml");
        }

    }
}
