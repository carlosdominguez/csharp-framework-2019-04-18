﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.Configuration
{
    [RoutePrefix("Configuration")]
    public class AdminExceptionController : Controller
    {
        //
        // GET: /Configuration/AdminException

        [Route("AdminException")]  
        public ActionResult Index()
        {
            return View("~/Views/Configuration/AdminException.cshtml");
        }

        //
        // GET: /Configuration/AdminException/List

        [Route("AdminException/List")]
        public ActionResult List()
        {
            var tblException = GlobalUtil.GetDBObjectFullName("Exceptions");
            var sql = "SELECT TOP 100 * FROM " + tblException + " ORDER BY [ID] DESC ";

            //Get resul
            List<Dictionary<string, string>> resultList = (new GlobalModel()).getResultListBySQL(sql);
            return Json(resultList, JsonRequestBehavior.AllowGet);
        }
    }
}
