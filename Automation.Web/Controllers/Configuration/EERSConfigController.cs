﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.Configuration
{
    [RoutePrefix("Configuration")]
    public class EERSConfigController : Controller
    {
        //
        // GET: /Configuration/EERSConfig/

        [Route("EERSConfig")]    
        public ActionResult Index()
        {
            var resultList = GlobalUtil.ListAppConfig();
            return View("~/Views/Configuration/EERSConfig.cshtml", resultList);
        }


    }
}
