﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.Configuration
{
    [RoutePrefix("Configuration")]
    public class DataBaseController : Controller
    {
        //
        // GET: /Configuration/DataBase

        [Route("DataBase")]  
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            //Get database information
            var listResult = GlobalUtil.CheckDatabaseConnection();
            var currentApp = GlobalUtil.GetCurrentAppInfo();
            Dictionary<string, string> objBDInfo = null;
            Dictionary<string, string> objIISInfo = new Dictionary<string,string>();

            if (listResult.Count == 1)
            {
                objBDInfo = listResult[0];
            }
            viewModel.Add("objBDInfo", objBDInfo);

            //Get Application CSID
            viewModel.Add("applicationCSID", currentApp.CSID);

            //Get Application Name
            viewModel.Add("applicationName", GlobalUtil.GetSubAppPath() + @"/" + GlobalUtil.GetAppKey());

            //Set IIS Information
            objIISInfo.Add("ReportServerUrl", GlobalUtil.GetReportServerUrl());
            objIISInfo.Add("ConnectionString", GlobalUtil.GetConnectionString());
            objIISInfo.Add("LocalIP", Request.ServerVariables["LOCAL_ADDR"]);
            objIISInfo.Add("RemoteIP", Request.ServerVariables["REMOTE_ADDR"]);
            objIISInfo.Add("MachineName", System.Environment.MachineName);
            objIISInfo.Add("CMDWhoami", GlobalUtil.runCMD("whoami"));
            objIISInfo.Add("WindowsIdentity", System.Security.Principal.WindowsIdentity.GetCurrent().Name);
            objIISInfo.Add("URLBase", (Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/')));
            viewModel.Add("objIISInfo", objIISInfo);

            //Load Entities Information
            viewModel.Add("EntitiesInfo", GlobalUtil.GetListEntitiesConnString());
            
            return View("~/Views/Configuration/DataBase.cshtml", viewModel);
        }

        class WritablePropertiesOnlyResolver : DefaultContractResolver
        {
            protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
            {
                IList<JsonProperty> props = base.CreateProperties(type, memberSerialization);
                var propsFilter = props.Where(p => 
                    ( p.PropertyName != "Length" && 
                      p.PropertyName != "Position" && 
                      p.PropertyName != "ReadTimeout" &&
                      p.PropertyName != "WriteTimeout" &&
                      p.PropertyName != "LastActivityDate" &&

                      //This operation requires IIS integrated pipeline mode
                      p.PropertyName != "IsWebSocketRequest" &&
                      p.PropertyName != "WebSocketRequestedProtocols" &&
                      p.PropertyName != "Headers" &&
                      p.PropertyName != "SubStatusCode" &&
                      p.PropertyName != "CurrentNotification" &&
                      p.PropertyName != "IsPostNotification" &&

                      //This operation requires IIS version 7.5 or higher running in integrated pipeline mode.
                      p.PropertyName != "ClientDisconnectedToken" &&
                      
                      p.PropertyName != "LastUpdatedDate" )
                ).ToList();
                return propsFilter;
            }
        }

        //
        // GET: /Configuration/DataBase/GetRequestInfo

        [Route("DataBase/GetRequestInfo")]
        public ActionResult GetRequestInfo()
        {
            Dictionary<string, string> serverVariables = new Dictionary<string, string>();
            Dictionary<string, object> resultToPrint = new Dictionary<string, object>();

            foreach (var item in Request.ServerVariables)
            {
                serverVariables.Add(item.ToString(), Request.ServerVariables[item.ToString()]);
            }

            resultToPrint.Add("_SERVER_VARIABLES:", serverVariables);
            resultToPrint.Add("_REQUEST:", Request);

            string addPre = (Request.QueryString["pre"] == null) ? "0" : Request.QueryString["pre"];
            var jsonResult = JsonConvert.SerializeObject(resultToPrint, Formatting.Indented,
                                new JsonSerializerSettings(){
                                    DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Ignore,
                                    NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore,
                                    MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore,
                                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                                    //MaxDepth = 1,
                                    ContractResolver = new WritablePropertiesOnlyResolver()
                                });

            if (addPre == "1")
            {
                jsonResult = "<pre>" + jsonResult + "</pre>";
            }

            return Content(jsonResult);
        }

        //
        // GET: /Configuration/DataBase/GetHostingEnviromentInfo

        [Route("DataBase/GetHostingEnviromentInfo")]
        public ActionResult GetHostingEnviromentInfo()
        {
            Dictionary<string, object> resultToPrint = new Dictionary<string, object>();

            resultToPrint.Add("ApplicationID:", System.Web.Hosting.HostingEnvironment.ApplicationID);
            resultToPrint.Add("ApplicationHost:", System.Web.Hosting.HostingEnvironment.ApplicationHost);
            resultToPrint.Add("ApplicationPhysicalPath:", System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath);
            resultToPrint.Add("ApplicationVirtualPath:", System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
            resultToPrint.Add("SiteName:", System.Web.Hosting.HostingEnvironment.SiteName);

            string addPre = (Request.QueryString["pre"] == null) ? "0" : Request.QueryString["pre"];
            var jsonResult = JsonConvert.SerializeObject(resultToPrint, Formatting.Indented);

            if (addPre == "1")
            {
                jsonResult = "<pre>" + jsonResult + "</pre>";
            }

            return Content(jsonResult);
        }

        //
        // GET: /Configuration/DataBase/GetLocalJsonStructure

        [Route("DataBase/GetJsonStructure")]
        public ActionResult GetJsonStructure()
        {
            string pathKey = null;
            switch (Request.QueryString["pType"])
            {
                case "Automation":
                    pathKey = "PathDBAutomationStructure";
                    break;
                default:
                    pathKey = "PathDBLocalStructure";
                    break;
            }

            string _pathJsonDBStructure = GlobalUtil.GetValueGlobalConfig("appSettings", pathKey);
            var jsonDBStructure = new StringBuilder(System.IO.File.ReadAllText(@"" + Server.MapPath(_pathJsonDBStructure)));

            //if (string.IsNullOrWhiteSpace(jsonDBStructure.ToString()))
            //{
            //    _pathJsonDBStructure = GlobalUtil.GetValueGlobalConfig("appSettings", path);
            //    jsonDBStructure = new StringBuilder(System.IO.File.ReadAllText(@"" + Server.MapPath(_pathJsonDBStructure)));
            //}
            return Content(jsonDBStructure.ToString());
        }

        //
        // GET: /Configuration/DataBase/UpdateDBIsConfigured

        [ValidateInput(false)]
        [Route("DataBase/UpdateDBIsConfigured")]
        public ActionResult UpdateDBIsConfigured(string pDBIsConfigured)
        {
            //Set database is configured
            GlobalUtil.SaveFileConfig("App", "AppSettings", "DBIsConfigured", pDBIsConfigured, "Update");

            //If pDBIsConfigured == "1" refresh userInfo
            if (pDBIsConfigured == "1")
            {
                GlobalUtil.SessionStart(true);
            }

            return Content("Saved!");
        }

        //
        // GET: /Configuration/DataBase/SaveJsonStructure

        [ValidateInput(false)]
        [Route("DataBase/SaveJsonStructure")]
        public ActionResult SaveJsonStructure(string pjson, string pType)
        {
            if (String.IsNullOrEmpty(pjson))
            {
                pjson = "[]";
            }

            string pathKey = null;
            switch (pType)
            {
                case "Automation":
                    pathKey = "PathDBAutomationStructure";
                    break;
                default:
                    pathKey = "PathDBLocalStructure";
                    break;
            }

            //write string to file
            string _pathJsonDBStructure = GlobalUtil.GetValueGlobalConfig("appSettings", pathKey);
            System.IO.File.WriteAllText(@"" + Server.MapPath(_pathJsonDBStructure), pjson);

            return Content("Saved!");
        }

        ////
        //// GET: /Configuration/DataBase/SaveJsonStructure

        //[ValidateInput(false)]
        //[Route("DataBase/SaveAutomationJsonStructure")]
        //public ActionResult SaveAutomationJsonStructure(string pjson)
        //{
        //    if (String.IsNullOrEmpty(pjson))
        //    {
        //        pjson = "[]";
        //    }

        //    //write string to file
        //    string _pathJsonDBStructure = GlobalUtil.GetValueAppConfig("appSettings", "PathJsonAutomationDBStructure");
        //    System.IO.File.WriteAllText(@"" + Server.MapPath(_pathJsonDBStructure), pjson);

        //    return Content("Saved!");
        //}
    }
}
