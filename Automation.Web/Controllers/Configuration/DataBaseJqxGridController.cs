﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.Configuration
{
    [RoutePrefix("Configuration")]
    public class DataBaseJqxGridController : Controller
    {
        //
        // GET: /Configuration/DataBaseJqxGrid

        [Route("DataBaseJqxGrid")]  
        public ActionResult Index()
        {
            string pType = null;

            if (this.Request.QueryString.HasKeys())
                foreach (string key in this.Request.QueryString.AllKeys)
                {
                    switch (key)
                    {
                        case "pType":
                            pType = this.Request.QueryString[key];
                            break;
                    }
                }

            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            viewModel.Add("pType", pType);

            return View("~/Views/Configuration/DataBaseJqxGrid.cshtml", viewModel);
        }

        //
        // GET: /Configuration/DataBaseJqxGrid/GetJsonStructure

        [Route("DataBaseJqxGrid/GetJsonStructure")]
        public ActionResult GetJsonStructure()
        {
            string _pathJsonDBStructure = GlobalUtil.GetValueGlobalConfig("appSettings", "PathJsonDBStructure");
            var jsonDBStructure = new StringBuilder(System.IO.File.ReadAllText(@"" + Server.MapPath(_pathJsonDBStructure)));

            return Content(jsonDBStructure.ToString());
        }

        //
        // GET: /Configuration/DataBaseJqxGrid/SaveJsonStructure

        [ValidateInput(false)]
        [Route("DataBaseJqxGrid/SaveJsonStructure")]
        public ActionResult SaveJsonStructure(string pjson)
        {
            //var _data = new List<Dictionary<string, string>> { 
            //    new Dictionary<string, string>
            //    {
            //        { "prefix", "soeid" },
            //        { "objectType", "EmailProfile" }, 
            //        { "objectName", "FRO_GLOBAL" }, 
            //        //{ "existsQuery", "Test" }, 
            //        //{ "createQuery", "Test" }
            //        { "existsQuery", "select count(1) from msdb.dbo.sysmail_profile sp where sp.name = 'FRO_GLOBAL'" }, 
            //        { "createQuery", "USE [msdb]\nGO\nIF NOT EXISTS (select * from msdb.dbo.sysmail_profile sp where sp.name = 'FRO_GLOBAL')\nBEGIN\n -- Create a New Mail Profile for Notifications\n EXECUTE msdb.dbo.sysmail_add_profile_sp\n     @profile_name = 'FRO_GLOBAL',\n     @description = 'Profile for sending Automated DB Notifications'\n     \n -- Create an Account for the Notifications\n EXECUTE msdb.dbo.sysmail_add_account_sp\n  @account_name = 'FRO_GLOBAL_ACCOUNT',\n  @description = 'Account for Automated DB Notifications',\n  @email_address = 'dl.fro.global.euc.team@imcla.lac.nsroot.net',  \n  @replyto_address = 'no_reply@citi.com',\n  @display_name = 'FRO Global Notifications',\n  @mailserver_name = 'mailhub-vip.ny.ssmb.com',\n  @port    = 25\n -- Add the Account to the Profile\n EXECUTE msdb.dbo.sysmail_add_profileaccount_sp\n  @profile_name = 'FRO_GLOBAL',\n  @account_name = 'FRO_GLOBAL_ACCOUNT',\n  @sequence_number = 1\nEND" }
            //    },
            //    new Dictionary<string, string>
            //    {
            //        { "prefix", "FROU_tbl" },
            //        { "objectType", "Table" }, 
            //        { "objectName", "Exeptions" }, 
            //        //{ "existsQuery", "Test" }, 
            //        //{ "createQuery", "Test" }
            //        { "existsQuery", "select count(1) from msdb.dbo.sysmail_profile sp where sp.name = 'FRO_GLOBAL'" }, 
            //        { "createQuery", "-- ********************************************************************************************\n'-- Object:  Table [dbo].[tblS_Exceptions]    Script Date: 6/24/2015 9:48:07 AM \n'-- ********************************************************************************************\nCREATE TABLE [dbo].[tblS_Exceptions](\n [ID] [int] IDENTITY(1,1) NOT NULL,\n [SOE_ID_SESSION] [varchar](10) NULL,\n [CREATED_DATE] [datetime] NULL,\n [METHOD] [varchar](max) NULL,\n [EXCEPTION] [varchar](max) NULL,\n CONSTRAINT [PK_tblS_Exceptions] PRIMARY KEY CLUSTERED \n(\n [ID] ASC\n)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]\n) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]\nGO" }
            //    }
            //};

            //string json = JsonConvert.SerializeObject(_data);

            if (String.IsNullOrEmpty(pjson))
            {
                pjson = "[]";
            }

            //write string to file
            string _pathJsonDBStructure = GlobalUtil.GetValueGlobalConfig("appSettings", "PathJsonDBStructure");
            System.IO.File.WriteAllText(@"" + Server.MapPath(_pathJsonDBStructure), pjson);

            return Content("Saved!");
        }
    }
}
