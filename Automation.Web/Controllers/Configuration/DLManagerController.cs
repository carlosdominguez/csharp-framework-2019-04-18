﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Threading.Tasks;

namespace Automation.Web.Controllers.Configuration
{
    [RoutePrefix("Configuration")]
    public class DLManagerController : Controller
    {
        //
        // GET: /Configuration/DLManager

        [Route("DLManager")]  
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            return View("~/Views/Configuration/DLManager.cshtml", viewModel);
        }


        // GET: /Configuration/DLManager/GetAutomationIDDL/

        [ValidateInput(false)]
        [Route("DLManager/GetAutomationIDDL")]
        public async Task<ActionResult> GetAutomationIDDL()
        {
            String emailDL = (Request.Params["pemailDL"] == null) ? "" : Request.Params["pemailDL"];
            String descriptionDL = (Request.Params["pdescriptionDL"] == null) ? "" : Request.Params["pdescriptionDL"];
            String idDL = GlobalUtil.GetAutomationIDDL(emailDL, descriptionDL);
            return await Task.Run(() => Content(idDL));
        }

        // GET: /Configuration/DLManager/AddAutomationDL/

        [ValidateInput(false)]
        [Route("DLManager/AddAutomationDL")]
        public async Task<ActionResult> AddAutomationDL()
        {
            String emailDL = (Request.Params["pemailDL"] == null) ? "" : Request.Params["pemailDL"];
            String descriptionDL = (Request.Params["pdescriptionDL"] == null) ? "" : Request.Params["pdescriptionDL"];
            var idDL = GlobalUtil.AddAutomationDL(emailDL, descriptionDL);
            return await Task.Run(() => Content(idDL));
        }

        // GET: /Configuration/DLManager/GetACUsersByDL

        [ValidateInput(false)]
        [Route("DLManager/GetACUsersByDL")]
        public async Task<JsonResult> GetACUsersByDL()
        {
            String emailDL = (Request.Params["pemailDL"] == null) ? "" : Request.Params["pemailDL"];
            String emailGuid = (Request.Params["pemailGuid"] == null) ? "" : Request.Params["pemailGuid"];
            var result = GlobalUtil.GetACUsersByACGroup(emailDL, emailGuid);
            return await Task.Run(() => Json(result, JsonRequestBehavior.AllowGet));
        }

        // GET: /Configuration/DLManager/GetACUsersBySAMAccountName

        [ValidateInput(false)]
        [Route("DLManager/GetACUsersBySAMAccountName")]
        public async Task<JsonResult> GetACUsersBySAMAccountName()
        {
            String SAMAccountName = (Request.Params["pSAMAccountName"] == null) ? "" : Request.Params["pSAMAccountName"];
            var result = GlobalUtil.GetACUsersByACGroup("", "", SAMAccountName);
            return await Task.Run(() => Json(result, JsonRequestBehavior.AllowGet));
        }

        // GET: /Configuration/DLManager/UpdateCurrentUserDL

        [ValidateInput(false)]
        [Route("DLManager/UpdateCurrentUserDL")]
        public async Task<JsonResult> UpdateCurrentUserDL()
        {
            var result = GlobalUtil.UpdateCurrentUserDL();
            return await Task.Run(() => Json(result, JsonRequestBehavior.AllowGet));
        }
        
        // GET: /Configuration/DLManager/GetACGroupsBySOEID

        [ValidateInput(false)]
        [Route("DLManager/GetACGroupsBySOEID")]
        public async Task<JsonResult> GetACGroupsBySOEID()
        {
            String soeid = (Request.Params["pacsoeid"] == null) ? "" : Request.Params["pacsoeid"];
            var result = GlobalUtil.GetACGroupsBySOEID(soeid);
            return await Task.Run(() => Json(result, JsonRequestBehavior.AllowGet));
        }

        // GET: /Configuration/DLManager/GetACGroupsBySOEIDAM

        [ValidateInput(false)]
        [Route("DLManager/GetACGroupsBySOEIDAM")]
        public async Task<JsonResult> GetACGroupsBySOEIDAM()
        {
            String soeid = (Request.Params["pacsoeid"] == null) ? "" : Request.Params["pacsoeid"];
            var result = GlobalUtil.GETACGroupsBySOEIDAM(soeid);
            return await Task.Run(() => Json(result, JsonRequestBehavior.AllowGet));
        }

        // GET: /Configuration/DLManager/GetAllPropertiesBySAMAccount

        [ValidateInput(false)]
        [Route("DLManager/GetAllPropertiesBySAMAccount")]
        public async Task<JsonResult> GetAllPropertiesBySAMAccount()
        {
            String psAMAccount = (Request.Params["psAMAccount"] == null) ? "" : Request.Params["psAMAccount"];
            var result = GlobalUtil.GetAllPropertiesBySAMAccount(psAMAccount);
            return await Task.Run(() => Json(result, JsonRequestBehavior.AllowGet));
        }

        // GET: /Configuration/DLManager/GetAllPropertiesOfEmail

        [ValidateInput(false)]
        [Route("DLManager/GetAllPropertiesOfEmail")]
        public async Task<JsonResult> GetAllPropertiesOfEmail()
        {
            String emailDL = (Request.Params["pemail"] == null) ? "" : Request.Params["pemail"];
            var result = GlobalUtil.GetAllPropertiesOfEmail(emailDL);
            return await Task.Run(() => Json(result, JsonRequestBehavior.AllowGet));
        }

        // GET: /Configuration/DLManager/UpdateAutomationDL/

        [ValidateInput(false)]
        [Route("DLManager/UpdateAutomationDL")]
        public async Task<ActionResult> UpdateAutomationDL()
        {
            String emailGuid = (Request.Params["pemailGuid"] == null) ? "" : Request.Params["pemailGuid"];
            String emailLastUpdate = (Request.Params["pemailLastUpdate"] == null) ? "" : Request.Params["pemailLastUpdate"];
            String forced = (Request.Params["pforced"] == null) ? "" : Request.Params["pforced"];
            var countDLUpdated = GlobalUtil.UpdateAutomationDL(emailGuid, emailLastUpdate, forced);
            return await Task.Run(() => Content(countDLUpdated));
        }

        // GET: /Configuration/DLManager/UpdateAllAutomationDL/

        [ValidateInput(false)]
        [Route("DLManager/UpdateAllAutomationDL")]
        public async Task<ActionResult> UpdateAllAutomationDL()
        {
            var countDLUpdated = GlobalUtil.UpdateAllAutomationDL();
            return await Task.Run(() => Content(countDLUpdated));
        }
    }
}
