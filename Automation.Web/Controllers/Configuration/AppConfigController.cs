﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.Configuration
{
    [RoutePrefix("Configuration")]
    public class AppConfigController : Controller
    {
        //
        // GET: /Configuration/AppConfig/

        [Route("AppConfig")]    
        public ActionResult Index()
        {
            var resultList = GlobalUtil.ListAppConfig();
            return View("~/Views/Configuration/AppConfig.cshtml", resultList);
        }

        //
        // GET: /Configuration/AppConfig/List

        [Route("AppConfig/List")]
        public ActionResult List()
        {
            var resultList = GlobalUtil.ListAppConfig();

            var jsonResult = Json(resultList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;

            return jsonResult;
        }

        //
        // GET: /Configuration/AppConfig/Save

        [ValidateInput(false)]
        [Route("AppConfig/Save")]
        public ActionResult Save(string pjson)
        {
            var resultList = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(pjson);
            var IsDefaultConnStringChanged = false;

            //Save values
            foreach (Dictionary<string, string> objConfig in resultList)
            {
                GlobalUtil.SaveFileConfig("App", objConfig["section"], objConfig["name"], objConfig["value"], "Update");

                if (objConfig["name"] == "DefaultConnString")
                {
                    IsDefaultConnStringChanged = true;
                }
            }

            //Set DBIsConfigured to 0
            if (IsDefaultConnStringChanged)
            {
                GlobalUtil.SaveFileConfig("App", "AppSettings", "DBIsConfigured", "0", "Update");
            }

            return Content("Saved!");
        }

    }
}
