﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.Configuration
{
    [RoutePrefix("Configuration")]
    public class AdminDBTableController : Controller
    {
        //
        // GET: /Configuration/AdminDBTable

        [Route("AdminDBTable")]  
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            var tblName = (String.IsNullOrEmpty(Request.QueryString.Get("ptbl"))) ? "" : Request.QueryString.Get("ptbl");
            var colKey = (String.IsNullOrEmpty(Request.QueryString.Get("pcolKey"))) ? "" : Request.QueryString.Get("pcolKey");

            viewModel.Add("TableName", tblName);
            viewModel.Add("ColumnKey", colKey);
            
            if (String.IsNullOrEmpty(tblName))
            {
                GlobalUtil.AddSessionMessage("info", "Query String Parameter for Table Name [ptbl] is missing.", true);
            }

            if (String.IsNullOrEmpty(colKey))
            {
                GlobalUtil.AddSessionMessage("info", "Query String Parameter for Column Key [pcolKey] is missing.", true);
            }
            return View("~/Views/Configuration/AdminDBTable.cshtml", viewModel);
        }

        //
        // GET: /Configuration/PartialViewDBTable

        [Route("PartialViewDBTable")]
        public ActionResult PartialViewDBTable()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            var tblName = (String.IsNullOrEmpty(Request.QueryString.Get("ptbl"))) ? "": Request.QueryString.Get("ptbl");
            var colKey = (String.IsNullOrEmpty(Request.QueryString.Get("pcolKey"))) ? "": Request.QueryString.Get("pcolKey");
            var idElementContent = (String.IsNullOrEmpty(Request.QueryString.Get("pidElementContent"))) ? "" : Request.QueryString.Get("pidElementContent");
            var hasIdentity = (String.IsNullOrEmpty(Request.QueryString.Get("phasIdentity"))) ? "" : Request.QueryString.Get("phasIdentity");

            viewModel.Add("TableName", tblName);
            viewModel.Add("ColumnKey", colKey);
            viewModel.Add("IDElementContent", idElementContent);
            viewModel.Add("HasIdentity", hasIdentity);

            if (String.IsNullOrEmpty(tblName))
            {
                GlobalUtil.AddSessionMessage("info", "Query String Parameter for Table Name [ptbl] is missing.", true);
            }

            if (String.IsNullOrEmpty(colKey))
            {
                GlobalUtil.AddSessionMessage("info", "Query String Parameter for Column Key [pcolKey] is missing.", true);
            }

            if (String.IsNullOrEmpty(idElementContent))
            {
                GlobalUtil.AddSessionMessage("info", "Query String Parameter for ID element to render table [pidElementContent] is missing.", true);
            }
            return View("~/Views/Configuration/_DBTable.cshtml", viewModel);
        }

        //
        // GET: /Configuration/AdminDBTable/List

        [Route("AdminDBTable/List")]
        public ActionResult List(string ptable)
        {
            List<Dictionary<string, string>> resultList = null;
            var sql = "SELECT TOP 2000 * FROM " + ptable;

            if (String.IsNullOrEmpty(ptable))
            {
                resultList = new List<Dictionary<string, string>>();
            }
            else
            {
                resultList = (new GlobalModel()).getResultListBySQL(sql);
            }

            return Json(resultList, JsonRequestBehavior.AllowGet);
        }
    }
}
