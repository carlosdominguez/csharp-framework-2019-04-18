﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.Configuration
{
    [RoutePrefix("Configuration")]
    public class AppManagerController : Controller
    {
        //
        // GET: /Configuration/AppManager

        [Route("AppManager")]  
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            return View("~/Views/Configuration/AppManager.cshtml", viewModel);
        }

        //
        // GET: /Configuration/AppManager/AppInfoList

        [Route("AppManager/AppInfoList")]
        public ActionResult AppInfoList()
        {
            var currentAppKey = GlobalUtil.GetAppKey();
            var globalAppsList = GlobalUtil.GetListDictionayFromJsonTextFile("Global", "PathAppsList");
            var appsListResult = new List<AppInfo>();

            foreach (Dictionary<string, string> objGlobalApp in globalAppsList)
            {
                var objAppInfo = new AppInfo();
                if (GlobalUtil.StructureOkInProject(objGlobalApp["KeyName"]))
                {
                    objAppInfo = GlobalUtil.GetAppInfoFromPathJson(objGlobalApp["PathAppInfo"]);
                    objAppInfo.StructureOkInProject = true;
                    objAppInfo.IsDefault = (currentAppKey == objAppInfo.KeyName);
                }
                else
                {
                    objAppInfo.KeyName = objGlobalApp["KeyName"];
                    objAppInfo.StructureOkInProject = false;
                    objAppInfo.IsDefault = false;
                }
                appsListResult.Add(objAppInfo);
            }

            return Json(appsListResult, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Configuration/AppManager/GetCurrentAppInfo

        [Route("AppManager/GetCurrentAppInfo")]
        public ActionResult GetCurrentAppInfo()
        {
            var currentApp = GlobalUtil.GetCurrentAppInfo();
            currentApp.StructureOkInProject = GlobalUtil.StructureOkInProject(currentApp.KeyName);
            return Json(currentApp, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Configuration/AppManager/GetCurrentAppData

        [Route("AppManager/GetCurrentAppData")]
        public ActionResult GetCurrentAppData()
        {
            var currentAppData = GlobalUtil.GetCurrentAppData();
            return Json(currentAppData, JsonRequestBehavior.AllowGet);
        }

        //
        // POST: /Configuration/AppManager/SaveAppInfo

        [ValidateInput(false)]
        [Route("AppManager/SaveAppInfo")]
        public ActionResult SaveAppInfo(string pjson)
        {
            string msg = "Saved!";
            var appInfo = JsonConvert.DeserializeObject<AppInfo>(pjson);

            //If not exist in Project Structure Create it
            if (!GlobalUtil.StructureOkInProject(appInfo.KeyName))
            {
                msg = GlobalUtil.CreateAppStructureIfNotExists(appInfo);
            }

            //Get path info
            var pathAppInfo = GlobalUtil.GetValueAppConfig("appSettings", "PathAppInfo", appInfo.KeyName);

            //write string to file and use AppInfoJsonContract to ignore "IsDefault" and "StructureOkInProject"
            var jsonToSave = JsonConvert.SerializeObject(appInfo, Formatting.Indented, new JsonSerializerSettings(){
                ContractResolver = new AppInfoJsonContract()
            });
            System.IO.File.WriteAllText(@"" + Server.MapPath(pathAppInfo), jsonToSave);

            //If not exist in apps-list
            var existsInGlobal = false;
            var globalAppsList = GlobalUtil.GetListDictionayFromJsonTextFile("Global", "PathAppsList");
            foreach (Dictionary<string, string> objGlobalApp in globalAppsList)
            {
                if (objGlobalApp["KeyName"] == appInfo.KeyName)
                {
                    existsInGlobal = true;
                    break;
                }
            }
            if (!existsInGlobal)
            {
                var newGlobalApp = new Dictionary<string, string>();
                newGlobalApp["KeyName"] = appInfo.KeyName;
                newGlobalApp["PathAppInfo"] = pathAppInfo;

                globalAppsList.Add(newGlobalApp);

                string jsonGlobalAppsList = JsonConvert.SerializeObject(globalAppsList, Formatting.Indented);
                string pathGlobalAppsList = GlobalUtil.GetValueGlobalConfig("appSettings", "PathAppsList");

                //write string to file "\Content\Shared\config\apps-list.txt"
                System.IO.File.WriteAllText(@"" + Server.MapPath(pathGlobalAppsList), jsonGlobalAppsList);
            }

            //If is default app
            if (appInfo.IsDefault)
            {
                //write string to file
                GlobalUtil.SaveFileConfig("Global", "AppSettings", "DefaultAppKey", appInfo.KeyName, "Update");

                //Refresh session with new app
                GlobalUtil.SessionStart(true);
            }

            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }


        //
        // POST: /Configuration/AppManager/SaveAppData

        [ValidateInput(false)]
        [Route("AppManager/SaveAppData")]
        public ActionResult SaveAppData(string pjsonAppData, string pkeyAppName)
        {
            string msg = "Saved!";

            //Get path data
            var pathAppData = GlobalUtil.GetValueAppConfig("appSettings", "PathAppData", pkeyAppName);
            System.IO.File.WriteAllText(@"" + Server.MapPath(pathAppData), pjsonAppData);

            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }

        //
        // POST: /Configuration/AppManager/SetDefaultApp

        //[ValidateInput(false)]
        //[Route("AppManager/SetDefaultApp")]
        //public ActionResult SetDefaultApp(string pdefaultApp)
        //{
        //    string msg = "Saved!";

        //    //Update only if the new default app is different from the current app
        //    if (GlobalUtil.GetAppKey() != pdefaultApp)
        //    {
        //        //Validate if exists the app
        //        string filesInfo = GlobalUtil.CreateAppStructureIfNotExists(pdefaultApp);
                
        //        if(!String.IsNullOrEmpty(filesInfo))
        //        {
        //            msg = filesInfo;
        //        }

        //        //write string to file
        //        GlobalUtil.SaveFileConfig("Global", "AppSettings", "DefaultAppKey", pdefaultApp, "Update");
        //    }

        //    //Refresh session with new app
        //    GlobalUtil.SessionStart(true);

        //    return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        //}

    }
}
