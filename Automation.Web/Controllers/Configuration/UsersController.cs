﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.Configuration
{
    [RoutePrefix("Configuration")]
    public class UsersController : Controller
    {
        //
        // GET: /Configuration/Users

        [Route("Users")]  
        public ActionResult Index()
        {
            return View("~/Views/Configuration/Users.cshtml");
        }

    }
}
