﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.Configuration
{
    [RoutePrefix("Configuration")]
    public class ReportsController : Controller
    {
        //
        // GET: /Configuration/Reports

        [Route("Reports")]  
        public ActionResult Index()
        {
            return View("~/Views/Configuration/Reports.cshtml");
        }

        //
        // GET: /Configuration/Reports2

        [Route("Reports2")]
        public ActionResult Reports2()
        {
            return View("~/Views/Configuration/Reports2.cshtml");
        }
    }
}
