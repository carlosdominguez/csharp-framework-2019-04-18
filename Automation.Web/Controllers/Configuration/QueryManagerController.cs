﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.Configuration
{
    [RoutePrefix("Configuration")]
    public class QueryManagerController : Controller
    {
        //
        // GET: /Configuration/QueryManager

        [Route("QueryManager")]  
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            return View("~/Views/Configuration/QueryManager.cshtml", viewModel);
        }

        //
        // GET: /Configuration/QueryManager/List

        [Route("QueryManager/List")]
        public ActionResult List(string ptable)
        {
            List<Dictionary<string, string>> resultList = null;
            var sql = "SELECT TOP 100 * FROM " + ptable;

            if (String.IsNullOrEmpty(ptable))
            {
                resultList = new List<Dictionary<string, string>>();
            }
            else
            {
                resultList = (new GlobalModel()).getResultListBySQL(sql);
            }

            return Json(resultList, JsonRequestBehavior.AllowGet);
        }
    }
}
