using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.AMSUtility
{
    [RoutePrefix("AMSUtility")]
    public class ImportFixedAssetsController : Controller
    {
        //
        // GET: /AMSUtility/
        [Route("ImportFixedAssets")]
        public ActionResult Index()
        {
            return View("~/Views/AMSUtility/ImportFixedAssets.cshtml");
        }

        [Route("UploadAssets")]
        public ActionResult UploadAssets(string State, string Overwrite)
        {
            Dictionary<string, object> result = new Dictionary<string, object> { { "success", true } };
            var objFile = GlobalUtil.GetFileFromCurrentRequest(false);
            string fullPathSaved;
            DataTable ptable = new DataTable();

            //Save excel file in the server
            fullPathSaved = GlobalUtil.FileSaveAs("ImportReports", objFile.Filename, objFile.InputStream);
            //fileName = objFile.Filename;
            ptable = GlobalUtil.GetDataTableFromExcel(fullPathSaved, "Report");
            
            

            //Save data in the database
            (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[spAMSUPL_FixedAssets]", new List<Dictionary<string, string>> {
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEID" },
                                { "Value", GlobalUtil.GetSOEID() }
                            },new Dictionary<string, string>
                            {
                                { "Name", "@State" },
                                { "Value", State }
                            },new Dictionary<string, string>
                            {
                                { "Name", "@Overwrite" },
                                { "Value", Overwrite }
                            }
                        });


            string uploadFolder = GlobalUtil.GetValueAppConfig("appSettings", "UploadFolder") + "ImportReports" + @"\";
            string fullPathDelete = @"" + Server.MapPath(uploadFolder);

            System.IO.DirectoryInfo di = new DirectoryInfo(@"" + fullPathDelete);
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }

    
}
