using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;

namespace Automation.Web.Controllers.CPR
{
    [RoutePrefix("CPR")]
    public class CPRController : Controller
    {
        //
        // GET: /CPR/StartEvent/
        [Route("StartEvent")]
        public ActionResult StartEvent()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            return View("~/Views/CPR/StartEvent.cshtml", viewModel);
        }

        //
        // GET: /CPR/ListEvent/
        [Route("ListEvent")]
        public ActionResult ListEvent()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            var pviewType = (String.IsNullOrEmpty(Request.QueryString.Get("pviewType"))) ? "" : Request.QueryString.Get("pviewType");

            viewModel.Add("HFViewType", pviewType);

            return View("~/Views/CPR/ListEvent.cshtml", viewModel);
        }
        
        //
        // GET: /CPR/ListProcessActivity/
        [Route("ListProcessActivity")]
        public ActionResult ListProcessActivity()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            var pviewType = (String.IsNullOrEmpty(Request.QueryString.Get("pviewType"))) ? "" : Request.QueryString.Get("pviewType");

            viewModel.Add("HFViewType", pviewType);

            return View("~/Views/CPR/ListProcessActivity.cshtml", viewModel);
        }

        //
        // GET: /CPR/UploadProcessActivity/
        [Route("UploadProcessActivity")]
        public ActionResult UploadProcessActivity(string pjsonData, string pidProcess)
        {
            DataTable ptable = (DataTable)JsonConvert.DeserializeObject(pjsonData, (typeof(DataTable)));
            
            //Save data in the database
            (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[spCPRAFrwkUploadProcessActivity]", new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@IDProcess" },
                    { "Value", pidProcess }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@SOEIDUploader" },
                    { "Value", GlobalUtil.GetSOEID() }
                }
                //new Dictionary<string, string>
                //{
                //    { "Name", "@FileID" },
                //    { "Value", GlobalUtil.CreateCustomID() }
                //}
            });

            return Content("Uploaded");
        }

        //
        // GET: /CPR/UploadRecurrence/
        [Route("UploadRecurrence")]
        public ActionResult UploadRecurrence(string pjsonData)
        {
            DataTable ptable = (DataTable)JsonConvert.DeserializeObject(pjsonData, (typeof(DataTable)));

            //Save data in the database
            (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[spCPRAFrwkUploadRecurrence]", new List<Dictionary<string, string>> {
                new Dictionary<string, string>
                {
                    { "Name", "@SOEIDUploader" },
                    { "Value", GlobalUtil.GetSOEID() }
                }
            });

            return Content("Uploaded");
        }
        
        //
        // GET: /CPR/MyProcessToRecover/
        [Route("MyProcessToRecover")]
        public ActionResult MyProcessToRecover()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            var pidCommunication = (String.IsNullOrEmpty(Request.QueryString.Get("pidCommunication"))) ? "" : Request.QueryString.Get("pidCommunication");
            viewModel.Add("HFIDCommunication", pidCommunication);

            return View("~/Views/CPR/MyProcessToRecover.cshtml", viewModel);
        }
        
        //
        // GET: /CPR/ProcessTree/
        [Route("ProcessTree")]
        public ActionResult ProcessTree()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            return View("~/Views/CPR/ProcessTree.cshtml", viewModel);
        }

        //
        // GET: /CPR/Recurrence/
        [Route("Recurrence")]
        public ActionResult Recurrence()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            return View("~/Views/CPR/Recurrence.cshtml", viewModel);
        }
    }
}