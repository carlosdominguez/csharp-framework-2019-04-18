﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Automation.Web.Models.HMT2;


using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
namespace Automation.Web.Controllers.HMT2
{
    [RoutePrefix("HMT2")]
    public class ForecastController : Controller
    {
        [Route("Forecast")]
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            string subAppPath = GlobalUtil.GetSubAppPath();
            viewModel.Add("SubAppPath", subAppPath);
            return (View("~/Views/HMT2/Forecast.cshtml", viewModel));
        }

        [Route("getHTMLGrid")]
        public ActionResult getHTMLGrid(string data)
        {
            HMT2Entities DAO = new HMT2Entities();
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();


            DateTime  Date = Convert.ToDateTime(objParameter["Date"].ToString());
            string MS = objParameter["MS"].ToString();
            string center = objParameter["Center"].ToString();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procFC_GetHTMLForecast(Date,MS,center)
                    select new
                    {
                        p.OrderDiv,
                        p.ID,
                        p.MS,
                        p.GridName
                    }
            });


            return Content(o["item"].ToString());

        }


        [Route("getHTMLGridCenter")]
        public ActionResult getHTMLGridCenter(string data)
        {
            HMT2Entities DAO = new HMT2Entities();
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();


            DateTime Date = Convert.ToDateTime(objParameter["Date"].ToString());
            string MS = objParameter["MS"].ToString();
            string center = objParameter["Center"].ToString();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procFC_GetHTMLForecastCenter(Date, MS, center)
                    select new
                    {
                        p.OrderDiv,
                        p.ID,
                        p.Center,
                        p.GridName
                    }
            });


            return Content(o["item"].ToString());

        }


        [Route("getForecastMS")]
        public ActionResult getForecastMS(string data)
        {
            HMT2Entities DAO = new HMT2Entities();
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            DateTime  Date = Convert.ToDateTime(objParameter["Date"].ToString());
            int MS = Convert.ToInt32(objParameter["MS"].ToString());
            string Center = objParameter["Center"].ToString();


            

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procFC_GetManagedSegmentValues(Date,MS,Center)
                    select new
                    {
                        p.ID  ,
		                p.ManagedSegment ,
		                p.GOCDescription ,
		                p.GOC ,
		                p.ACTUALJAN ,
		                p.ACTUALFEB ,
		                p.ACTUALMAR ,
		                p.ACTUALAPR ,
		                p.ACTUALMAY ,
		                p.ACTUALJUN ,
		                p.ACTUALJUL ,
		                p.ACTUALAGS ,
		                p.ACTUALSEP ,
		                p.ACTUALOCT ,
		                p.ACTUALNOV ,
		                p.ACTUALDEC ,
		                p.DEMANDJAN ,
		                p.DEMANDFEB ,
		                p.DEMANDMAR ,
		                p.DEMANDAPR ,
		                p.DEMANDMAY ,
		                p.DEMANDJUN ,
		                p.DEMANDJUL ,
		                p.DEMANDAGS ,
		                p.DEMANDSEP ,
		                p.DEMANDOCT ,
		                p.DEMANDNOV ,
		                p.DEMANDDEC ,
		                p.BWJAN ,
		                p.BWFEB ,
		                p.BWMAR ,
		                p.BWAPR ,
		                p.BWMAY ,
		                p.BWJUN ,
		                p.BWJUL ,
		                p.BWAGS ,
		                p.BWSEP ,
		                p.BWOCT ,
		                p.BWNOV ,
		                p.BWDEC ,
                        p.JANAModify,
                        p.FEBAModify,
                        p.MARAModify,
                        p.APRAModify,
                        p.MAYAModify,
                        p.JUNAModify,
                        p.JULAModify,
                        p.AUGAModify,
                        p.SEPAModify,
                        p.OCTAModify,
                        p.NOVAModify,
                        p.DECAModify

                    }
            });


            return Content(o["item"].ToString());

        }

        //[Route("getPlan")]
        //public ActionResult getPlan(string data)
        //{
        //    HMT2Entities DAO = new HMT2Entities();
        //    var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        //    var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //    Dictionary<String, String> obj = new Dictionary<string, string>();

        //    string Date = objParameter["Date"].ToString();
        //    string goc = objParameter["GOC"].ToString().Split(' ')[0];
        //    //string ms = objParameter["MS"].ToString();
        //    //string center = objParameter["Center"].ToString();
        //    //string global = objParameter["Global"].ToString();

        //    JObject o = null;
        //    o = JObject.FromObject(new
        //    {
        //        item =
        //            from p in DAO.tblFC_Plan
        //            where p.GOC == goc && p.tblFC_RowCategory.tblFC_Dates.Value == Date && p.tblFC_RowCategory.tblFC_Type.Type == "Plan"
        //            orderby p.tblD_Driver.Source 
        //            select new
        //            {
        //                p.ID,
        //                p.DriverID,
        //                p.GOC,
        //                p.RowCategoryID,
        //                p.IsDS,
        //                p.JAN,
        //                p.FEB,
        //                p.MAR,
        //                p.APR,
        //                p.MAY,
        //                p.JUN,
        //                p.JUL,
        //                p.AUG,
        //                p.SEP,
        //                p.OCT,
        //                p.NOV,
        //                p.DEC,
        //                p.tblD_Driver.Name,
        //                p.tblFC_Center.Center,
        //                p.tblD_Driver.Source 

        //            }
        //    });


        //    return Content(o["item"].ToString());

        //}

        //[Route("getForeCast")]
        //public ActionResult getForeCast(string data)
        //{
        //    HMT2Entities DAO = new HMT2Entities();
        //    var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        //    var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //    Dictionary<String, String> obj = new Dictionary<string, string>();

        //    string Date = objParameter["Date"].ToString();
        //    string goc = objParameter["GOC"].ToString().Split(' ')[0];
        //    //string ms = objParameter["MS"].ToString();
        //    //string center = objParameter["Center"].ToString();
        //    //string global = objParameter["Global"].ToString();

        //    JObject o = null;
        //    o = JObject.FromObject(new
        //    {
        //        item =
        //            from p in DAO.tblFC_Plan
        //            where p.GOC == goc && p.tblFC_RowCategory.tblFC_Dates.Value == Date && p.tblFC_RowCategory.tblFC_Type.Type == "ForeCast"
        //            orderby p.tblD_Driver.Source 
        //            select new
        //            {
        //                p.ID,
        //                p.DriverID,
        //                p.GOC,
        //                p.RowCategoryID,
        //                p.IsDS,
        //                p.JAN,
        //                p.FEB,
        //                p.MAR,
        //                p.APR,
        //                p.MAY,
        //                p.JUN,
        //                p.JUL,
        //                p.AUG,
        //                p.SEP,
        //                p.OCT,
        //                p.NOV,
        //                p.DEC,
        //                p.tblD_Driver.Name,
        //                p.tblFC_Center.Center,
        //                p.tblFC_Source.Source  

        //            }
        //    });


        //    return Content(o["item"].ToString());

        //}

        //[Route("getGap")]
        //public ActionResult getGap(string data)
        //{
        //    HMT2Entities DAO = new HMT2Entities();
        //    var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        //    var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //    Dictionary<String, String> obj = new Dictionary<string, string>();

        //    string Date = objParameter["Date"].ToString();
        //    string goc = objParameter["GOC"].ToString().Split(' ')[0];
        //    //string ms = objParameter["MS"].ToString();
        //    //string center = objParameter["Center"].ToString();
        //    //string global = objParameter["Global"].ToString();

        //    JObject o = null;
        //    o = JObject.FromObject(new
        //    {
        //        item =
        //            from p in DAO.tblFC_Plan
        //            where p.GOC == goc && p.tblFC_RowCategory.tblFC_Dates.Value == Date && p.tblFC_RowCategory.tblFC_Type.Type == "GAP"
        //            orderby p.tblD_Driver.Source 
        //            select new
        //            {
        //                p.ID,
        //                p.DriverID,
        //                p.GOC,
        //                p.RowCategoryID,
        //                p.IsDS,
        //                p.JAN,
        //                p.FEB,
        //                p.MAR,
        //                p.APR,
        //                p.MAY,
        //                p.JUN,
        //                p.JUL,
        //                p.AUG,
        //                p.SEP,
        //                p.OCT,
        //                p.NOV,
        //                p.DEC,
        //                p.tblFC_Center.Center,
        //                Name = "GAP",
        //                p.tblD_Driver.Source 

        //            }
        //    });


        //    return Content(o["item"].ToString());

        //}

        [Route("getForecastMS")]
        public ActionResult getForecastMSDriver(string data)
        {
            HMT2Entities DAO = new HMT2Entities();
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            DateTime  Date = Convert.ToDateTime(objParameter["Date"].ToString());
            int MS = Convert.ToInt32(objParameter["MS"].ToString());
            string Center = objParameter["Center"].ToString();

            string DS = objParameter["DS"].ToString();
            string CSSDriver = objParameter["CSSDriver"].ToString();
            string GOC = objParameter["GOC"].ToString();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procFC_GetManagedSegmentValuesByDriver(Date,MS,Center,DS,CSSDriver,GOC)
                    select new
                    {
                        p.ID  ,
		                p.ManagedSegment ,
		                p.CSSDriver ,
		                p.JAN ,
		                p.FEB ,
		                p.MAR ,
		                p.APR ,
		                p.MAY ,
		                p.JUN ,
		                p.JUL ,
		                p.AUG ,
		                p.SEP ,
		                p.OCT ,
		                p.NOV ,
		                p.DEC 
                    }
            });


            return Content(o["item"].ToString());

        }

        [Route("getGocList")]
        public ActionResult getGocList()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWR_GOC
                    select new
                    {
                        ID = p.GOCID,
                        Value = p.GOCName
                    }

            });

            return Content(o["item"].ToString());

        }

        [Route("getManagedSegment")]
        public ActionResult getManagedSegment()
        {
            HMT2Entities DAO = new HMT2Entities();


            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procFC_getManagedSegment()
                    select new
                    {
                       ID = p.ManagedSegmentID  ,
                       Value = p.ManagedSegmentName  
                    }

            });

            return Content(o["item"].ToString());

        }

        [Route("getCenter")]
        public ActionResult getCenter()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblFC_Center 
                    select new
                    {
                        ID = p.ID,
                        Value = p.Center
                    }

            });

            return Content(o["item"].ToString());

        }

        [Route("getDate")]
        public ActionResult getDate()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblFC_Dates
                    orderby p.ID descending
                    select new
                    {
                        ID = p.ID,
                        Value = p.Value 
                    }

            });

            return Content(o["item"].ToString());

        }

        [Route("getCSSDriver")]
        public ActionResult getCSSDriver()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblD_CSSDriver
                    select new
                    {
                        ID = p.ID,
                        Value = p.Name 
                    }

            });

            return Content(o["item"].ToString());

        }

        [Route("getGOCValues")]
        public ActionResult getGOCValues()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWR_GOC
                    select new
                    {
                        ID = p.GOCID ,
                        Value = p.GOCName 
                    }

            });

            return Content(o["item"].ToString());

        }


        [Route("getRowDetailManagedSegmentView")]
        public ActionResult getRowDetailManagedSegmentView(string data)
        {
            HMT2Entities DAO = new HMT2Entities();
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            DateTime Date = Convert.ToDateTime(objParameter["Date"].ToString());

            string[] words = objParameter["MS"].ToString().Split(' ');

            int MS = Convert.ToInt32(words[0]);

            string Center = objParameter["Center"].ToString();
            
            string DS = objParameter["DS"].ToString();
            string CSSDriver = objParameter["Driver"].ToString();
            string GOC = objParameter["GOC"].ToString();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procFC_GetManagedSegmentValuesByDriverInsideGOC(MS,CSSDriver,Date, Center, DS,GOC)
                    select new
                    {
                        p.ID,
                        p.GOC ,
                        p.IsDirectStaff,
                        p.JAN,
                        p.FEB,
                        p.MAR,
                        p.APR,
                        p.MAY,
                        p.JUN,
                        p.JUL,
                        p.AUG,
                        p.SEP,
                        p.OCT,
                        p.NOV,
                        p.DEC
                    }
            });


            return Content(o["item"].ToString());

        }


        [Route("getRowDetailManagedSegmentView")]
        public ActionResult getRowDetailCenterView(string data)
        {
            HMT2Entities DAO = new HMT2Entities();
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            DateTime Date = Convert.ToDateTime(objParameter["Date"].ToString());

            string[] words = objParameter["MS"].ToString().Split(' ');

            string CenterName = words[0];

            string Center = objParameter["Center"].ToString();

            string DS = objParameter["DS"].ToString();
            string CSSDriver = objParameter["Driver"].ToString();
            string GOC = objParameter["GOC"].ToString();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procFC_GetCenterValuesByDriverInsideGOC(CenterName, CSSDriver, Date, Center, DS, GOC)
                    select new
                    {
                        p.ID,
                        p.GOC,
                        p.IsDirectStaff,
                        p.JAN,
                        p.FEB,
                        p.MAR,
                        p.APR,
                        p.MAY,
                        p.JUN,
                        p.JUL,
                        p.AUG,
                        p.SEP,
                        p.OCT,
                        p.NOV,
                        p.DEC
                    }
            });


            return Content(o["item"].ToString());

        }

        [Route("getRowDetailGOCbyDrivers")]
        public ActionResult getRowDetailGOCbyDrivers(string data)
        {
            HMT2Entities DAO = new HMT2Entities();
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            DateTime Date = Convert.ToDateTime(objParameter["Date"].ToString());

            string[] words = objParameter["MS"].ToString().Split(' ');

            int MS = Convert.ToInt32(words[0]);

            string Center = objParameter["Center"].ToString();
            
            string DS = objParameter["DS"].ToString();
            string CSSDriver = objParameter["Driver"].ToString();

            string[] wordsGOC = objParameter["GOC"].ToString().Split(' ');

            string GOC = wordsGOC[0].ToString();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procFC_GetManagedSegmentValuesByDriverInsideGOCInsideDriver(MS,CSSDriver,Date, Center, DS,GOC)
                    select new
                    {
                        p.ID,
                        p.Driver,
                        p.JAN,
                        p.FEB,
                        p.MAR,
                        p.APR,
                        p.MAY,
                        p.JUN,
                        p.JUL,
                        p.AUG,
                        p.SEP,
                        p.OCT,
                        p.NOV,
                        p.DEC
                    }
            });


            return Content(o["item"].ToString());

        }

        [Route("getRowDetailGOCbyDriversCenter")]
        public ActionResult getRowDetailGOCbyDriversCenter(string data)
        {
            HMT2Entities DAO = new HMT2Entities();
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            DateTime Date = Convert.ToDateTime(objParameter["Date"].ToString());

            string[] words = objParameter["MS"].ToString().Split(' ');

            string MS = words[0];

            string Center = objParameter["Center"].ToString();

            string DS = objParameter["DS"].ToString();
            string CSSDriver = objParameter["Driver"].ToString();

            string[] wordsGOC = objParameter["GOC"].ToString().Split(' ');

            string GOC = wordsGOC[0].ToString();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procFC_GetCenterValuesByDriverInsideGOCInsideDriver(MS, CSSDriver, Date, Center, DS, GOC)
                    select new
                    {
                        p.ID,
                        p.Driver,
                        p.JAN,
                        p.FEB,
                        p.MAR,
                        p.APR,
                        p.MAY,
                        p.JUN,
                        p.JUL,
                        p.AUG,
                        p.SEP,
                        p.OCT,
                        p.NOV,
                        p.DEC
                    }
            });


            return Content(o["item"].ToString());

        }

        [Route("getFCDetailGrid")]
        public ActionResult getFCDetailGrid(string data)
        {
            HMT2Entities DAO = new HMT2Entities();
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            DateTime Date = Convert.ToDateTime(objParameter["Date"].ToString());

            string[] words = objParameter["MS"].ToString().Split(' ');

            int MS = Convert.ToInt32(words[0]);

            string Center = objParameter["Center"].ToString();
            
            string DS = objParameter["DS"].ToString();
            string CSSDriver = objParameter["Driver"].ToString();
            string GOC = objParameter["GOC"].ToString();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procFC_GetManagedSegmentValuesFCDetail(MS,CSSDriver,Date, Center, DS,GOC)
                    orderby p.GOC 
                    select new
                    {
                       p.ID
                      ,p.PeriodID
                      ,p.CSSDriverID
	                  ,p.GOC
                      ,p.StartMonth
                      ,p.StartYear
                      ,p.EndMonth
                      ,p.EndYear
                      ,p.CreatedBy
                      ,p.CreatedDate
                      ,p.Enable
                      ,p.HaveEndDate
                      ,p.Total
	                  ,p.IsDS
	                  ,p.CSSDriver
                      ,p.Center
                    }
            });


            return Content(o["item"].ToString());

        }


        [Route("getFCDetailGridCenter")]
        public ActionResult getFCDetailGridCenter(string data)
        {
            HMT2Entities DAO = new HMT2Entities();
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            DateTime Date = Convert.ToDateTime(objParameter["Date"].ToString());

            string[] words = objParameter["MS"].ToString().Split(' ');

            string MS = words[0];

            string Center = objParameter["Center"].ToString();

            string DS = objParameter["DS"].ToString();
            string CSSDriver = objParameter["Driver"].ToString();
            string GOC = objParameter["GOC"].ToString();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procFC_GetCenterValuesFCDetail(MS, CSSDriver, Date, Center, DS, GOC)
                    orderby p.GOC
                    select new
                    {
                        p.ID
                      ,
                        p.PeriodID
                      ,
                        p.CSSDriverID
                      ,
                        p.GOC
                      ,
                        p.StartMonth
                      ,
                        p.StartYear
                      ,
                        p.EndMonth
                      ,
                        p.EndYear
                      ,
                        p.CreatedBy
                      ,
                        p.CreatedDate
                      ,
                        p.Enable
                      ,
                        p.HaveEndDate
                      ,
                        p.Total
                      ,
                        p.IsDS
                      ,
                        p.CSSDriver
                      ,
                        p.Center
                    }
            });


            return Content(o["item"].ToString());

        }

        [Route("addForeCastValues")]
        public ActionResult addForeCastValues(string data)
        {
            HMT2Entities DAO = new HMT2Entities();
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            //string[] words = objParameter["MS"].ToString().Split(' ');

            //int MS = Convert.ToInt32(words[0]);

            //string Center = objParameter["Center"].ToString();

            //string DS = objParameter["DS"].ToString();
            //string CSSDriver = objParameter["Driver"].ToString();
            //string GOC = objParameter["GOC"].ToString();

            DateTime DATE = Convert.ToDateTime(objParameter["DATE"].ToString());
           
            string CSSDriver = objParameter["CSSDriver"].ToString();
            int? CSSDriverID = (from p in DAO.tblD_CSSDriver where p.Name == CSSDriver select p).SingleOrDefault<tblD_CSSDriver>().ID;

            string[] gocWord = objParameter["GOC"].ToString().Split(' ');
            string GOC = gocWord[0];

            bool? IsDS;
            if(objParameter["IsDS"].ToString() == "Direct Staff"){ IsDS = true; }else{ IsDS = false; }

            string Center = objParameter["Center"].ToString();
            int? CenterID = (from p in DAO.tblFC_Center where p.Center == Center select p).SingleOrDefault<tblFC_Center>().ID;

            int? Total = Convert.ToInt32(objParameter["Total"].ToString());

            DateTime Start =  Convert.ToDateTime(objParameter["Start"].ToString());
            DateTime End =  Convert.ToDateTime(objParameter["End"].ToString());
            
            Boolean HaveEnd = Convert.ToBoolean(objParameter["HaveEnd"].ToString());

            string driver = objParameter["driver"].ToString();
            int? DriverID = (from p in DAO.tblD_Driver where p.Name == driver select p).SingleOrDefault<tblD_Driver>().ID;

            string userSOEID;
            userSOEID = GlobalUtil.GetSOEID();

            DAO.procFC_AddForeCastValues(DATE, DriverID, GOC, IsDS, CenterID, Total, Start, End, HaveEnd, userSOEID);

            return Content("1");

        }

        [Route("deleteForeCastDetail")]
        public ActionResult deleteForeCastDetail(string data)
        {
            HMT2Entities DAO = new HMT2Entities();
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();


            int ID = Convert.ToInt32(objParameter["ID"].ToString());


            DAO.procFC_DeleteManualInput(ID);


            return Content("1");

        }

        [Route("getPosibleDrivers")]
        public ActionResult getPosibleDrivers(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            string CSSdriver = objParameter["Driver"].ToString();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblD_Driver
                    where p.tblD_CSSDriver.Name == CSSdriver
                    select new
                    {
                        ID = p.ID ,
                        Value = p.Name 
                    }

            });

            return Content(o["item"].ToString());

        }


         [Route("ReprocessData")]
        public ActionResult ReprocessData()
         {
             HMT2Entities DAO = new HMT2Entities();

             DateTime da = new DateTime();

             da = DateTime.Now;

             DAO.procFC_CalculateForecast(da);

             return Content("");

        }



         [Route("getForecastCenterDriver")]
         public ActionResult getForecastCenterDriver(string data)
         {
             HMT2Entities DAO = new HMT2Entities();
             var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
             Dictionary<String, String> obj = new Dictionary<string, string>();

             DateTime Date = Convert.ToDateTime(objParameter["Date"].ToString());
             int MS = Convert.ToInt32(objParameter["MS"].ToString());
             string Center = objParameter["Center"].ToString();

             string DS = objParameter["DS"].ToString();
             string CSSDriver = objParameter["CSSDriver"].ToString();
             string GOC = objParameter["GOC"].ToString();

             JObject o = null;
             o = JObject.FromObject(new
             {
                 item =
                     from p in DAO.procFC_GetManagedSegmentValuesByDriverCenter(Date, MS, Center, DS, CSSDriver, GOC)
                     select new
                     {
                         p.ID,
                         p.ManagedSegment,
                         p.CSSDriver,
                         p.JAN,
                         p.FEB,
                         p.MAR,
                         p.APR,
                         p.MAY,
                         p.JUN,
                         p.JUL,
                         p.AUG,
                         p.SEP,
                         p.OCT,
                         p.NOV,
                         p.DEC
                     }
             });


             return Content(o["item"].ToString());

         }


    }
}
