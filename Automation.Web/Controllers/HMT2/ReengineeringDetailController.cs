﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Automation.Web.Models.HMT2;


using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;


namespace Automation.Web.Controllers.HMT2
{
    public class ReengineeringDetailController : Controller
    {
        [Route("ReengineeringDetail")]
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            string subAppPath = GlobalUtil.GetSubAppPath();
            viewModel.Add("SubAppPath", subAppPath);
            return (View("~/Views/HMT2/ReengineeringDetail.cshtml", viewModel));
        }

        [Route("getTotalReeng")]
        public ActionResult getTotalReeng(string data)
        {
            HMT2Entities DAO = new HMT2Entities();
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();


            DateTime Date = Convert.ToDateTime(objParameter["Date"].ToString());
         
            tblFC_Dates SelectDate = (from p in DAO.tblFC_Dates where p.MONTH == Date.Month && p.YEAR == Date.Year select p).SingleOrDefault<tblFC_Dates>();


            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWReen_DataFirst  where p.DateID == SelectDate.ID 
                    select new
                    {
                           p.DateID
                          ,p.Center
                          ,p.JAN
                          ,p.FEB
                          ,p.MAR
                          ,p.APR
                          ,p.MAY
                          ,p.JUN
                          ,p.JUL
                          ,p.AUG
                          ,p.SEP
                          ,p.OCT
                          ,p.NOV
                          ,p.DEC
                    }
            });


            return Content(o["item"].ToString());

        }

        [Route("getTotalReeng")]
        public ActionResult getIdToDownload(string data)
        {
            HMT2Entities DAO = new HMT2Entities();
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();


            DateTime Date = Convert.ToDateTime(objParameter["Date"].ToString());

            tblFC_Dates SelectDate = (from p in DAO.tblFC_Dates where p.MONTH == Date.Month && p.YEAR == Date.Year select p).SingleOrDefault<tblFC_Dates>();

            return Content(SelectDate.ID.ToString());

        }
    }
}
