﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Automation.Web.Models.HMT2;


using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace Automation.Web.Controllers.HMT2
{

    [RoutePrefix("HMT2")]
    public class RoleAdminController : Controller
    {
        [Route("RoleAdmin")]
        public ActionResult Index()
        {
            return (View("~/Views/HMT2/RoleAdmin.cshtml"));
        }

        [Route("getTotalRoles")]
        public ActionResult getTotalRoles()
        {
            HMT2Entities DAO = new HMT2Entities();

            var total = from p in DAO.VWR_CompleteList select p;

            return Content(total.Count().ToString());
        }

        [Route("getTotalRolesPendingReview")]
        public ActionResult getTotalRolesPendingReview()
        {
            HMT2Entities DAO = new HMT2Entities();

            var total = from p in DAO.VWR_CompleteList where p.FromType == "MOU" select p;

            return Content(total.Count().ToString());
        }

        [Route("getTotalRolesPendingReview")]
        public ActionResult saveSearch(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                DataTable dtFullKeySearch = (DataTable)JsonConvert.DeserializeObject(objParameter["data"].ToString(), (typeof(DataTable)));

                DataTable dt = dtFullKeySearch.DefaultView.ToTable(false, "RoleID");

                (new GlobalModel()).SaveDataTableDB(dt, "procSaveWhatToReview", new List<Dictionary<string, string>> { 
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEID" },
                                { "Value", GlobalUtil.GetSOEID() }
                            }
                        });

            return null;
        }

        [Route("getHTML")]
        public ActionResult getHTML()
        {
            HMT2Entities DAO = new HMT2Entities();

            string userSOEID = GlobalUtil.GetSOEID();

            string html = DAO.procR_GetFromSelectionHTML(userSOEID).ToList<procR_GetFromSelectionHTML_Result>().SingleOrDefault().HTML.ToString();

            return Content(html);

        }

        [Route("getMOU")]
        public ActionResult getMOU()
        {
            HMT2Entities DAO = new HMT2Entities();

            string userSOEID = GlobalUtil.GetSOEID();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procR_GetFromSelectionMOU(userSOEID)
                    select new
                    {
                        p.ID
                      ,
                        p.MOU
                      ,
                        p.Name
                      ,
                        p.ManagedGeographyID
                      ,
                        p.ManagedGeographyName
                      ,
                        p.ManagedSegmentID
                      ,
                        p.ManagedSegmentName
                      ,
                        p.InitialDate
                      ,
                        p.Stage
                      ,
                        p.StageName
                      ,
                        p.ApprovalSOEID
                      ,
                        p.ApprovalName
                      ,
                        p.ApprovedDate
                      ,
                        p.FTE
                      ,
                        p.FTEHiredToDate
                      ,
                        p.TotalRole
                      ,
                        p.GOC
                    }

            });

            return Content(o["item"].ToString());

        }
    }
}
