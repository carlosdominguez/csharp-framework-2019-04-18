﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Automation.Web.Models.HMT2;


using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace Automation.Web.Controllers.HMT2
{
    [RoutePrefix("HMT2")]
    public class DriverController : Controller
    {
        [Route("Driver")]
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            string subAppPath = GlobalUtil.GetSubAppPath();
            viewModel.Add("SubAppPath", subAppPath);
            return (View("~/Views/HMT2/Driver.cshtml", viewModel));
        }


        [Route("getTotalDriver")]
        public ActionResult getTotalDriver()
        {
            HMT2Entities DAO = new HMT2Entities();

            var total = from p in DAO.VWD_DriverData select p;

            return Content(total.Count().ToString());
        }

        [Route("getTotalCSSCategory")]
        public ActionResult getTotalCSSCategory()
        {
            HMT2Entities DAO = new HMT2Entities();

            var total = from p in DAO.tblD_CSSDriver select p;

            return Content(total.Count().ToString());
        }

        [Route("getTotalDriverMOU")]
        public ActionResult getTotalDriverMOU()
        {
            HMT2Entities DAO = new HMT2Entities();

            var total = from p in DAO.tblD_DriverMOUtype select p;

            return Content(total.Count().ToString());
        }

        [Route("getTotalDriverNonMOU")]
        public ActionResult getTotalDriverNonMOU()
        {
            HMT2Entities DAO = new HMT2Entities();

            var total = from p in DAO.tblD_DriverNonMOUtype  select p;

            return Content(total.Count().ToString());
        }


        [Route("getListDriver")]
        public ActionResult getListDriver()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWD_DriverData 
                    select new
                    {
                           p.ID
                          ,p.CssDriver
                          ,p.Name
                          ,p.RLSDriver
                          ,p.Source
                          ,p.IsAdd
                          ,p.IsLess
                          ,p.ImpactFRODemand
                          ,p.ImpactType
                          ,p.CreatedBy
                          ,p.CreatedDate
                          ,p.ModifyBy
                          ,p.ModifyDate
                    }

            });

            return Content(o["item"].ToString());
        }

        [Route("getCSSDriver")]
        public ActionResult getCSSDriver()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblD_CSSDriver 
                    select new
                    {
                        p.ID
                          ,
                        p.Name
                    }

            });

            return Content(o["item"].ToString());
        }

        [Route("getRLSDriver")]
        public ActionResult getRLSDriver()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblD_RLSDriver
                    select new
                    {
                        p.ID
                          ,
                        p.Name
                    }

            });

            return Content(o["item"].ToString());
        }

        [Route("getSource")]
        public ActionResult getSource()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblF_FeedType 
                    select new
                    {
                        p.ID
                          ,
                        p.Name
                    }

            });

            return Content(o["item"].ToString());
        }

        [Route("getImpactToHCWalk")]
        public ActionResult getImpactToHCWalk()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblD_ImpactToHCwalk 
                    select new
                    {
                        p.ID
                          ,
                        p.Name
                    }

            });

            return Content(o["item"].ToString());
        }

        [Route("getImpacFRODemand")]
        public ActionResult getImpacFRODemand()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblD_DemandFRO 
                    select new
                    {
                        p.ID
                          ,
                        p.Name
                    }

            });

            return Content(o["item"].ToString());
        }

        [Route("getImpacType")]
        public ActionResult getImpacType()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblD_ImpactType
                    select new
                    {
                        p.ID
                          ,
                        p.Name
                    }

            });

            return Content(o["item"].ToString());
        }


        [Route("insertNewDriver")]
        public ActionResult insertNewDriver(string data)
        {
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string CSSDRIVER = objParameter["CSSDRIVER"].ToString();
            string NAME = objParameter["NAME"].ToString();
            string RLSDRIVER = objParameter["RLSDRIVER"].ToString();
            string SOURCE = objParameter["SOURCE"].ToString();
            string IMPACTHCWALK = objParameter["IMPACTHCWALK"].ToString();
            string IMPACTFRO = objParameter["IMPACTFRO"].ToString();
            string IMPACTTYPE = objParameter["IMPACTTYPE"].ToString();
            string userSOEID = GlobalUtil.GetSOEID();

            HMT2Entities DAO = new HMT2Entities();

            DAO.procD_AddNewDriver(CSSDRIVER, NAME, RLSDRIVER, SOURCE, IMPACTHCWALK, IMPACTFRO, IMPACTTYPE, userSOEID);

            return null;
        }

        [Route("deleteDriver")]
        public ActionResult deleteDriver(string data)
        {
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int ID = Convert.ToInt32(objParameter["pID"].ToString());
            
            HMT2Entities DAO = new HMT2Entities();

            tblD_Driver driver = (from p in DAO.tblD_Driver where p.ID == ID select p).SingleOrDefault<tblD_Driver>();
            
            DAO.tblD_Driver.Remove(driver);

            DAO.SaveChanges();

            return null;
        }

        [Route("getDriverCB")]
        public ActionResult getDriverCB()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblD_Driver 
                    where p.Source == "MOU"
                    select new
                    {
                        p.ID
                          ,
                        p.Name
                    }

            });

            return Content(o["item"].ToString());
        }

        [Route("getDriverCBNONMOU")]
        public ActionResult getDriverCBNONMOU()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblD_Driver 
                    where p.Source == "NONMOU"
                    select new
                    {
                        p.ID
                          ,
                        p.Name
                    }

            });

            return Content(o["item"].ToString());
        }

        [Route("getTypeMOU")]
        public ActionResult getTypeMOU()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWD_MOUtype 
                    select new
                    {
                        p.ID
                          ,
                        p.Name
                    }

            });

            return Content(o["item"].ToString());
        }

        [Route("getTypeNONMOU")]
        public ActionResult getTypeNONMOU()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblNM_Type 
                    select new
                    {
                        ID = p.ID
                          ,
                        Name= p.Type 
                    }

            });

            return Content(o["item"].ToString());
        }

        [Route("getListDriver")]
        public ActionResult getListDriverMOU()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWD_DriverMOU
                    where p.Source == "MOU"
                    select new
                    {
                        p.ID,

                        p.Name 
                          ,
                        p.CssDriver
                          ,
                        p.Driver 
                          ,
                        p.RLSDriver
                          ,
                        p.Source
                          ,
                        p.IsAdd
                          ,
                        p.IsLess
                          ,
                        p.ImpactFRODemand
                          ,
                        p.ImpactType
                    }

            });

            return Content(o["item"].ToString());
        }

        [Route("getListDriver")]
        public ActionResult getListDriverNONMOU()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWD_DriverNonMOU 
                    where p.Source == "NONMOU"
                    select new
                    {
                        p.ID,

                        p.Name
                          ,
                        p.CssDriver
                          ,
                        p.Driver
                          ,
                        p.RLSDriver
                          ,
                        p.Source
                          ,
                        p.IsAdd
                          ,
                        p.IsLess
                          ,
                        p.ImpactFRODemand
                          ,
                        p.ImpactType
                    }

            });

            return Content(o["item"].ToString());
        }

        [Route("deleteDriverMOU")]
        public ActionResult deleteDriverMOU(string data)
        {
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int ID = Convert.ToInt32(objParameter["pID"].ToString());

            HMT2Entities DAO = new HMT2Entities();

            tblD_DriverMOUtype driver = (from p in DAO.tblD_DriverMOUtype where p.ID == ID select p).SingleOrDefault<tblD_DriverMOUtype>();

            DAO.tblD_DriverMOUtype.Remove(driver);

            DAO.SaveChanges();

            return null;
        }

        [Route("deleteDriverNONMOU")]
        public ActionResult deleteDriverNONMOU(string data)
        {
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int ID = Convert.ToInt32(objParameter["pID"].ToString());

            HMT2Entities DAO = new HMT2Entities();

            tblD_DriverNonMOUtype driver = (from p in DAO.tblD_DriverNonMOUtype where p.ID == ID select p).SingleOrDefault<tblD_DriverNonMOUtype>();

            DAO.tblD_DriverNonMOUtype.Remove(driver);

            DAO.SaveChanges();

            return null;
        }

        [Route("insertNewDriverMOU")]
        public ActionResult insertNewDriverMOU(string data)
        {
            try
            {
                var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                string TYPE = objParameter["TYPE"].ToString();
                string DRIVER = objParameter["DRIVER"].ToString();

                HMT2Entities DAO = new HMT2Entities();

                DAO.procD_AddNewDriverMOU(TYPE, DRIVER);
            }
            catch
            {
            }
            return null;
        }

        [Route("insertNewDriverNONMOU")]
        public ActionResult insertNewDriverNONMOU(string data)
        {
            try{
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string TYPE = objParameter["TYPE"].ToString();
            string DRIVER = objParameter["DRIVER"].ToString();

            HMT2Entities DAO = new HMT2Entities();

            DAO.procD_AddNewDriverNONMOU(TYPE, DRIVER);
            }catch{
            }
            return null;
        }
    }

}
