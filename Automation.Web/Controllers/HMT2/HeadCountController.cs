﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Automation.Web.Models.HMT2;


using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace Automation.Web.Controllers.HMT2
{

    [RoutePrefix("HMT2")]
    public class HeadCountController : Controller
    {
        [Route("HeadCount")]
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            string subAppPath = GlobalUtil.GetSubAppPath();
            viewModel.Add("SubAppPath", subAppPath);
            return (View("~/Views/HMT2/HeadCount.cshtml", viewModel));
        }

        [Route("getHClist")]
        public ActionResult getHClist()
        {
            HMT2Entities DAO = new HMT2Entities();

            string userSOEID = GlobalUtil.GetSOEID();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWE_EmployeeListSourceHMT 
                    select new
                    {
                       p.SOEID,
                      p.FirstName,
                      p.LastName,
                      p.HiringDate,
                      p.GEID,
                      p.RITSID,
                      p.PRIMARY_ID,
                      p.NAME,
                      p.EMAIL_ADDR,
                      p.LOCATION,
                      p.WORK_ADDRESS1,
                      p.WORK_ADDRESS3,
                      p.WORK_CITY,
                      p.WORK_STATE,
                      p.WORK_POSTAL,
                      p.WORK_COUNTRY,
                      p.CNTRY_DESCR,
                      p.WORK_BUILDING,
                      p.REGION,
                      p.RPT_REGION,
                      p.GOC,
                      p.EMPL_STATUS,
                      p.TERMINATION_DT,
                      p.EMPL_CLASS,
                      p.ClassDescription,
                      p.JOB_FAMILY,
                      p.JOBFAM_NAME,
                      p.JOB_FUNCTION,
                      p.JOBFUNC_NAME,
                      p.JOBCODE,
                      p.JOBTITLE,
                      p.OFFCR_TITLE_CD,
                      p.OFFCR_TTL_DESC,
                      p.DirectManagerID,
                      p.DirectManager_GEID,
                      p.SupervisorID,
                      p.Supervisor_GEID,
                      p.ManagedSegmentID,
                      p.Managed_Segment,
                      p.Grade,
                      p.HireDate,
                      p.TransferDate

                    }

            });

            return Content(o["item"].ToString());

        }

        [Route("getListReeng")]
        public ActionResult getRequisition()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWR_Requisition
                    orderby p.ID descending
                    select new
                    {
                        p.ID,
                        p.Number,
                        p.Name,
                        p.DateCreated,
                        p.NumberPositions,
                        p.Clevel,
                        p.StatusID,
                        p.DaysOpen,
                        p.GOC,
                        p.Expr1,
                        //p.BusinessUnit,
                        //p.Business,
                        //p.BusinessDetail,
                        //p.ManagedGeography,
                        //p.PhysicalRegion,
                        //p.ManagedFunction,
                        //p.OrgLevel1,
                        //p.OrgLevel2,
                        //p.OrgLevel3,
                        //p.OrgLevel4,
                        //p.OrgLevel5,
                        //p.OrgLevel6,
                        //p.OrgLevel7,
                        //p.OrgLevel8,
                        //p.OrgLevel9,
                        //p.OrgLevel10,
                        //p.OrgLevel11,
                        //p.OrgLevel12,
                        //p.MGLevel1,
                        //p.MGLevel2,
                        //p.MGLevel3,
                        //p.MGLevel4,
                        //p.MGLevel5,
                        //p.MGLevel6,
                        //p.MGLevel7,
                        //p.CSWWorkflowName,
                        //p.SourceSystem,
                        //p.GOCDescription,
                        //p.Recruiter,
                        p.RecruiterGEID,
                       // p.RelationshipIndicator,
                       // p.Requisition_,
                       // p.AlternateReq_,
                        p.JobCode,
                        p.JobFunction,
                        p.RequisitionTitle,
                        //p.C_ofPositions,
                       // p.LefttoHire,
                       // p.CurrentReqStatus,
                       // p.ReqStatusDate,
                        p.FirstApprovalDate,
                     //   p.ReqCreationDate,
                        p.InternalPostingStatus,
                     //   p.InternalPostingDate,
                      //  p.InternalUnpostingDate,
                        p.PersonReplacing,
                        p.PersonReplacingSOEID,
                      //  p.EmployeeReferalBonus,
                      //  p.EligibleforCampsHire,
                        p.HiringManager,
                        p.HiringManagerGEID,
                        p.HRRepresentative,
                     //   p.FinancialJustification,
                        p.RequisitionJustification,
                        p.IsitaProductionJob,
                     //   p.FullTimePartTime,
                        p.OvertimeStatus,
                        p.ReqSalaryGrade,
                        p.MappedOfficerTitle,
                        p.OfficerTitle,
                        p.OfficerTitleCode,
                     //   p.StandardGrade,
                        p.Grade,
                        p.WorldRegion,
                        p.Country,
                        p.StateProvince,
                        p.City,
                        p.CSSCenter,
                        p.LocationCode,
                        p.CSC,
                    //    p.OLM,
                        p.CompanyCode,
                    //    p.AgeGrouping,
                        p.AccountExpenseCode//,
                    //    p.Applications,
                    //    p.Internal,
                    //    p.External,
                    //    p.New,
                    //    p.Testing1,
                    //    p.Reviewed,
                    //    p.PhoneScreen,
                    //    p.Testing2,
                    //    p.Interview,
                    //    p.OfferCreation,
                    //    p.OfferExtended,
                    //    p.OfferAccepted,
                    //    p.Hired,
                    //    p.OfferDeclined,
                    //    p.ReqEmployeeStatus,
                    //    p.ReqCreatorUser,
                    //    p.APACLegalVehicle,
                    //    p.SumofOpenDaysLefttoHire
                    }

            });

            return Content(o["item"].ToString());
        }

        [Route("getReqStatus")]
        public ActionResult getReqStatus()
        {
            HMT2Entities DAO = new HMT2Entities();

            string userSOEID;
            userSOEID = GlobalUtil.GetSOEID();


            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblR_ReqStatus
                    select new
                    {
                        p.ID,
                        p.Name
                    }

            });

            return Content(o["item"].ToString());
        }

        [Route("getTotalRolesList")]
        public ActionResult getTotalRolesList()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = null;

            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWR_ROLESREQ
                    select new
                    {
                        p.ID
                      ,
                        p.RoleID
                      ,
                        p.RoleTypeID
                      ,
                        p.FromType
                      ,
                        p.Number
                      ,
                        p.IDType
                      ,
                        p.CreationDate
                      ,
                        p.CreatedBy
                      ,
                        p.IsActive
                      ,
                        p.ModifyBy
                      ,
                        p.ModifyDate
                      ,
                        p.Name
                      ,
                        p.Description
                      ,
                        p.GOCID
                      ,
                        p.MSL09
                      ,
                        p.Process
                      ,
                        p.SubProcess
                      ,
                        p.MgdGeaography
                      ,
                        p.MgdRegion
                      ,
                        p.MgdCountry
                      ,
                        p.LVID
                      ,
                        p.PhysicalLocationID
                      ,
                        p.ChargeOutProfileID
                      ,
                        p.RoleIsActive
                      ,
                        p.RoleCreaetedBy
                      ,
                        p.CreatedDate
                      ,
                        p.LastUpdateBy
                      ,
                        p.LastUpdateDate
                      ,
                        p.StatusID
                      ,
                        p.IsFteable
                      ,
                        p.IsDirectStaf
                      ,
                        p.FTEPercentage
                      ,
                        p.Status
                      ,
                        p.ManagedSegment
                      ,
                        p.ManagedGeography
                      ,
                        p.PhysicalLocation
                      ,
                        p.ChargeOutProfile
                      ,
                        p.RoleAuto
                      ,
                        p.GOCName
                      ,
                        p.CssDriver
                      ,
                        p.CountReq
                    }
            });


            return Content(o["item"].ToString());


        }

        [Route("addRoleToReq")]
        public ActionResult addRoleToReq(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            //Charter Info

            int roleID = Convert.ToInt32(objParameter["pRoleID"].ToString());
            string reqID = objParameter["pReqID"].ToString();
            string userSOEID = GlobalUtil.GetSOEID();


            tblR_Requisition req = (from c in DAO.tblR_Requisition where c.Number == reqID select c).SingleOrDefault<tblR_Requisition>();

            DAO.procR_AddRoleToReq(roleID, req.ID, userSOEID);


            return Content("");
        }

        [Route("renderRoleInReq")]
        public ActionResult renderRoleInReq(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string reqID = objParameter["pReqID"].ToString();

            tblR_Requisition req = (from c in DAO.tblR_Requisition where c.Number == reqID select c).SingleOrDefault<tblR_Requisition>();

            JObject o = null;

            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWR_RolesActiveInReq
                    where p.IsActive == true && p.ReqID == req.ID
                    select new
                    {
                        p.ID
                      ,
                        p.RoleID
                      ,
                        p.RoleTypeID
                      ,
                        p.FromType
                      ,
                        p.Number
                      ,
                        p.IDType
                      ,
                        p.RoleIDnumber
                      ,
                        p.CreationDate
                      ,
                        p.CreatedBy
                      ,
                        p.IsActive
                      ,
                        p.ModifyBy
                      ,
                        p.ModifyDate
                      ,
                        p.Name
                      ,
                        p.Description
                      ,
                        p.GOCID
                      ,
                        p.MSL09
                      ,
                        p.Process
                      ,
                        p.SubProcess
                      ,
                        p.MgdGeaography
                      ,
                        p.MgdRegion
                      ,
                        p.MgdCountry
                      ,
                        p.LVID
                      ,
                        p.PhysicalLocationID
                      ,
                        p.ChargeOutProfileID
                      ,
                        p.RoleIsActive
                      ,
                        p.RoleCreaetedBy
                      ,
                        p.CreatedDate
                      ,
                        p.LastUpdateBy
                      ,
                        p.LastUpdateDate
                      ,
                        p.StatusID
                      ,
                        p.IsFteable
                      ,
                        p.IsDirectStaf
                      ,
                        p.FTEPercentage
                      ,
                        p.Status
                      ,
                        p.ManagedSegment
                      ,
                        p.ManagedGeography
                      ,
                        p.PhysicalLocation
                      ,
                        p.ChargeOutProfile
                      ,
                        p.RoleAuto
                      ,
                        p.GOCName
                      ,
                        p.CssDriver

                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("deleteSelectedRole")]
        public ActionResult deleteSelectedRole(string data)
        {
            //HMT2Entities DAO = new HMT2Entities();

            //var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            ////Charter Info

            //int ID = Convert.ToInt32(objParameter["pID"].ToString());


            //tblR_ReqRoleID roleReq = (from c in DAO.tblR_ReqRoleID where c.ID == ID select c).SingleOrDefault<tblR_ReqRoleID>();

            //DAO.tblR_ReqRoleID.Remove(roleReq);

            //DAO.SaveChanges();

            return Content("");


        }


        [Route("getTotalOpen")]
        public ActionResult getTotalOpen()
        {
            HMT2Entities DAO = new HMT2Entities();
            var total = from p in DAO.VWE_PendingForStart where p.Complete == "Pending" select p;
            return Content(total.Count().ToString());
        }
        [Route("getTotalFroNoReq")]
        public ActionResult getTotalFroNoReq()
        {
            HMT2Entities DAO = new HMT2Entities();
            var total = from p in DAO.VWE_EmployeeNotReq where p.EMPL_STATUS == "A" select p;
            return Content(total.Count().ToString());
        }
        [Route("getTotalGDW")]
        public ActionResult getTotalGDW()
        {
            HMT2Entities DAO = new HMT2Entities();
            var total = from p in DAO.VWE_ChangeInEmployee where p.isCheck == false select p;
            return Content(total.Count().ToString());
        }

        [Route("getTotalHeadCount")]
        public ActionResult getTotalHeadCount()
        {
            HMT2Entities DAO = new HMT2Entities();
            var total = from p in DAO.VWE_EmployeeListSourceHMT where p.EMPL_STATUS == "A" select p;
            return Content(total.Count().ToString());
        }

        [Route("getHClist")]
        public ActionResult renderjqxGridOpen()
        {
            HMT2Entities DAO = new HMT2Entities();

            string userSOEID = GlobalUtil.GetSOEID();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWE_PendingForStart
                    where p.Complete == "Pending"
                    select new
                    {   
                           p.ID
                          ,p.GOC
                          ,p.Requisition
                          ,p.RequisitionTitle
                          ,p.PersonReplacing
                          ,p.PersonReplacingSOEID
                          ,p.StartDate
                          ,p.GEID
                          ,p.SOEID
                          ,p.CandidatesName
                          ,p.CandidateID
                          ,p.InternalExternal
                          ,p.CSC
                          ,p.FinanceJustification
                          ,p.EmployeeID
                          ,p.LastRole
                          ,p.LastRoleDriver
                          ,p.NewRole
                          ,p.NewRoleDriver
                          ,p.RequisitionID
                          ,p.Automatic
                          ,p.MOUNONMOU
                          ,p.Complete
                          ,p.RoleID
                          ,p.LastRoleID
                          ,p.LastDriver
                          ,p.NewDriver
                          ,p.GOCName
                    }

            });

            return Content(o["item"].ToString());

        }

        [Route("GetListFroNoReq")]
        public ActionResult GetListFroNoReq()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = null;

            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWE_EmployeeNotReq
                    where p.EMPL_STATUS == "A"
                    orderby p.SOEID 
                    select new
                    {
                          p.ID,
                          p.SOEID,
                          p.FirstName,
                          p.LastName,
                          p.Goc,
                          p.HiringDate,
	                      p.AttritionDate,
                          p.BLDG_ZONE,
                          p.REGION,
                          p.EMPL_CLASS,
	                      p.Description,
                          p.FTEEligible,
                          p.JOBTITLE,
                          p.DirectManagerID,
                          p.DirectManager_GEID,
                          p.SupervisorID,
                          p.Supervisor_GEID,
                          p.ManagedSegmentID,
                          p.Managed_Segment,
                          p.Grade
                    }
            });


            return Content(o["item"].ToString());


        }

        [Route("GetHeadCount")]
        public ActionResult GetHeadCount()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = null;

            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWE_EmployeeListSourceHMT
                    where p.EMPL_STATUS == "A"
                    orderby p.SOEID
                    select new
                    {
                       p.GEID
                      ,p.SOEID
                      ,p.RITSID
                      ,p.LastName 
                      ,p.FirstName 
                      ,p.NAME
                      ,p.EMAIL_ADDR
                      ,p.LOCATION
                      ,p.WORK_ADDRESS1
                      ,p.WORK_ADDRESS3
                      ,p.WORK_CITY
                      ,p.WORK_STATE
                      ,p.WORK_POSTAL
                      ,p.WORK_COUNTRY
                      ,p.CNTRY_DESCR
                      ,p.WORK_BUILDING
                      ,p.REGION
                      ,p.RPT_REGION
                      ,p.GOC
                      ,p.EMPL_STATUS
                      ,p.TERMINATION_DT
                      ,p.EMPL_CLASS
                      ,p.JOB_FAMILY
                      ,p.JOBFAM_NAME
                      ,p.JOB_FUNCTION
                      ,p.JOBFUNC_NAME
                      ,p.JOBCODE
                      ,p.JOBTITLE
                      ,p.OFFCR_TITLE_CD
                      ,p.OFFCR_TTL_DESC
                      ,p.DirectManagerID
                      ,p.DirectManager_GEID
                      ,p.SupervisorID
                      ,p.Supervisor_GEID
                      ,p.ManagedSegmentID
                      ,p.Managed_Segment
                      ,p.Grade
                      ,p.HireDate
                      ,p.TransferDate
                      ,p.ClassDescription 
                    }
            });


            return Content(o["item"].ToString());


        }

        [Route("getChangesInEmployee")]
        public ActionResult getChangesInEmployee()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = null;

            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWE_ChangeInEmployee
                    orderby p.DateChange 
                    select new
                    {
                       p.ID
                      ,p.SOEID
                      ,p.NewValue
                      ,p.OldValue
                      ,p.DateChange
                      ,p.isCheck
                      ,p.FirstName
                      ,p.LastName
                      ,p.HiringDate
                      ,p.GOC
                      ,p.REGION
                      ,p.WORK_COUNTRY
                      ,p.ClassDescription
                      ,p.Status
                    }
            });


            return Content(o["item"].ToString());


        }




        [Route("getRolesByGOC")]
        public ActionResult getRolesByGOC(string data)
        {

            HMT2Entities DAO = new HMT2Entities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            string goc = objParameter["goc"].ToString().Split(' ')[0];

            JObject o = null;

            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWR_MatchDataList where p.GOCRole == goc
                    select new
                    {
                        p.RoleID 


                    }
            });


            return Content(o["item"].ToString());

        }

        [Route("getDriverList")]
        public ActionResult getDriverList()
        {

            HMT2Entities DAO = new HMT2Entities();


            JObject o = null;

            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblD_Driver
                    select new
                    {
                        p.Name

                    }
            });


            return Content(o["item"].ToString());

        }


        [Route("applyPendingForStart")]
        public ActionResult applyPendingForStart(string data)
        {

            HMT2Entities DAO = new HMT2Entities();

            DataTable ptable = (DataTable)JsonConvert.DeserializeObject(data, (typeof(DataTable)));

            (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[procR_PendingStartApplyChanges]", new List<Dictionary<string, string>> { 
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEID" },
                                { "Value", GlobalUtil.GetSOEID() }
                            }
                        });

            return null;

        }
    }
}
