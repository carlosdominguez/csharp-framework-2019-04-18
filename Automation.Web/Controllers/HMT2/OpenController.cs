﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Automation.Web.Models.HMT2;


using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace Automation.Web.Controllers.HMT2
{
    [RoutePrefix("HMT2")]
    public class OpenController : Controller
    {
        [Route("Open")]
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            string subAppPath = GlobalUtil.GetSubAppPath();
            viewModel.Add("SubAppPath", subAppPath);
            return (View("~/Views/HMT2/Open.cshtml", viewModel));
        }


        [Route("getTotalRolesList")]
        public ActionResult getTotalRolesList()
        {
            HMT2Entities DAO = new HMT2Entities();

            return Content("");
        }

        [ValidateInput(false)][Route("uploadReenFile")]
        public ActionResult uploadReenFile(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var objParameter = JsonConvert.DeserializeObject(data);

            DataTable ptable = (DataTable)JsonConvert.DeserializeObject(data, (typeof(DataTable)));

            List<tblF_FeedColumn> columnNames = (from c in DAO.tblF_FeedColumn where c.FeedId == 1002 orderby c.OrderNum ascending select c).ToList<tblF_FeedColumn>();


            int columnIndex = 0;
            foreach (tblF_FeedColumn column in columnNames)
            {
                try
                {
                    ptable.Columns[column.ColumnName].SetOrdinal(columnIndex);
                }
                catch
                {
                    ptable.Columns.Add(column.ColumnName).SetOrdinal(columnIndex);
                }

                columnIndex++;
            }

            int desiredSize = columnNames.Count;

            while (ptable.Columns.Count > desiredSize)
            {
                ptable.Columns.RemoveAt(desiredSize);
            }


            (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[procOpen_UploadFILE]", new List<Dictionary<string, string>> { 
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEID" },
                                { "Value", GlobalUtil.GetSOEID() }
                            }
                        });

            return null;

        }

        [Route("getVersionList")]
        public ActionResult getVersionList()
        {

            HMT2Entities DAO = new HMT2Entities();


            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblOpen_UploadDate
                    orderby p.ID descending
                    select new
                    {
                        p.ID,
                        p.UploadBy,
                        p.UploadDate,
                        p.Enable
                    }

            });

            return Content(o["item"].ToString());

        }

        [Route("getListReeng")]
        public ActionResult getListReeng(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            int pFile = Convert.ToInt32(objParameter["pFile"].ToString());

            tblOpen_UploadDate FILE;

            if (pFile == 0)
            {
                FILE = (from p in DAO.tblOpen_UploadDate where p.Enable == true select p).SingleOrDefault<tblOpen_UploadDate>();
            }
            else
            {
                FILE = (from p in DAO.tblOpen_UploadDate where p.ID == pFile select p).SingleOrDefault<tblOpen_UploadDate>();
            }
            JObject o;
            try
            {


                o = JObject.FromObject(new
                {
                    item =
                        from p in DAO.tblOpen_File
                        where p.UploadID == FILE.ID
                        orderby p.ID descending
                        select new
                        {
                            p.ID
                            ,
                            p.UploadID
                            ,
                            p.BusinessUnit
                            ,
                            p.Business
                            ,
                            p.BusinessDetail
                            ,
                            p.ManagedGeographY
                            ,
                            p.PhysicalRegion
                            ,
                            p.ManagedFunction
                            ,
                            p.OrgLevel1
                            ,
                            p.OrgLevel2
                            ,
                            p.OrgLevel3
                            ,
                            p.OrgLevel4
                            ,
                            p.OrgLevel5
                            ,
                            p.OrgLevel6
                            ,
                            p.OrgLevel7
                            ,
                            p.OrgLevel8
                            ,
                            p.OrgLevel9
                            ,
                            p.OrgLevel10
                            ,
                            p.OrgLevel11
                            ,
                            p.OrgLevel12
                            ,
                            p.MGLevel1
                            ,
                            p.MGLevel2
                            ,
                            p.MGLevel3
                            ,
                            p.MGLevel4
                            ,
                            p.MGLevel5
                            ,
                            p.MGLevel6
                            ,
                            p.MGLevel7
                            ,
                            p.CSWWorkflowName
                            ,
                            p.SourceSystem
                            ,
                            p.GOC
                            ,
                            p.GOCDescription
                            ,
                            p.Recruiter
                            ,
                            p.RecruiterGEID
                            ,
                            p.RelationshipIndicator
                            ,
                            p.Requisition
                            ,
                            p.AlternateReq
                            ,
                            p.JobCode
                            ,
                            p.JobFunction
                            ,
                            p.RequisitionTitle
                            ,
                            p.CountPositions
                            ,
                            p.LeftToHire
                            ,
                            p.CurrentReqStatus
                            ,
                            p.ReqStatusDate
                            ,
                            p.FirstApprovalDate
                            ,
                            p.ReqCreationDate
                            ,
                            p.InternalPostingStatus
                            ,
                            p.InternalPostingDate
                            ,
                            p.InternalUnpostingDate
                            ,
                            p.PersonReplacing
                            ,
                            p.PersonReplacingSOEID
                            ,
                            p.EmployeeReferalBonus
                            ,
                            p.EligibleforCampsHire
                            ,
                            p.HiringManager
                            ,
                            p.HiringManagerGEID
                            ,
                            p.HRRepresentative
                            ,
                            p.FinancialJustification
                            ,
                            p.RequisitionJustification
                            ,
                            p.IsitaProductionJob
                            ,
                            p.FulltimePartTime
                            ,
                            p.OvertimeStatus
                            ,
                            p.ReqSalaryGrade
                            ,
                            p.MappedOfficerTitle
                            ,
                            p.OfficerTitle
                            ,
                            p.OfficerTitleCode
                            ,
                            p.StandardGrade
                            ,
                            p.Grade
                            ,
                            p.WorldRegion
                            ,
                            p.Country
                            ,
                            p.StateProvince
                            ,
                            p.City
                            ,
                            p.CSSCenter
                            ,
                            p.LocationCode
                            ,
                            p.CSC
                            ,
                            p.OLM
                            ,
                            p.CompanyCode
                            ,
                            p.DaysOpen
                            ,
                            p.AgeGrouping
                            ,
                            p.AccountExpenseCode
                            ,
                            p.Applications
                            ,
                            p.Internal
                            ,
                            p.External
                            ,
                            p.New
                            ,
                            p.Testing1
                            ,
                            p.Reviewed
                            ,
                            p.PhoneScreen
                            ,
                            p.Testing2
                            ,
                            p.Interview
                            ,
                            p.OfferCreation
                            ,
                            p.OfferExtended
                            ,
                            p.OfferAccepted
                            ,
                            p.Hired
                            ,
                            p.OfferDeclined
                            ,
                            p.ReqEmployeeStatus
                            ,
                            p.ReqCreatorUser
                            ,
                            p.APACLegalVehicle
                            ,
                            p.SumOfOpenDaysLeftToHire
                            
                        }

                });
            }
            catch
            {
                o = JObject.FromObject(new
                {
                    item =
                        from p in DAO.tblOpen_File
                        orderby p.ID descending
                        select new
                        {
                            p.ID

                            ,
                            p.UploadID
                            ,
                            p.BusinessUnit
                            ,
                            p.Business
                            ,
                            p.BusinessDetail
                            ,
                            p.ManagedGeographY
                            ,
                            p.PhysicalRegion
                            ,
                            p.ManagedFunction
                            ,
                            p.OrgLevel1
                            ,
                            p.OrgLevel2
                            ,
                            p.OrgLevel3
                            ,
                            p.OrgLevel4
                            ,
                            p.OrgLevel5
                            ,
                            p.OrgLevel6
                            ,
                            p.OrgLevel7
                            ,
                            p.OrgLevel8
                            ,
                            p.OrgLevel9
                            ,
                            p.OrgLevel10
                            ,
                            p.OrgLevel11
                            ,
                            p.OrgLevel12
                            ,
                            p.MGLevel1
                            ,
                            p.MGLevel2
                            ,
                            p.MGLevel3
                            ,
                            p.MGLevel4
                            ,
                            p.MGLevel5
                            ,
                            p.MGLevel6
                            ,
                            p.MGLevel7
                            ,
                            p.CSWWorkflowName
                            ,
                            p.SourceSystem
                            ,
                            p.GOC
                            ,
                            p.GOCDescription
                            ,
                            p.Recruiter
                            ,
                            p.RecruiterGEID
                            ,
                            p.RelationshipIndicator
                            ,
                            p.Requisition
                            ,
                            p.AlternateReq
                            ,
                            p.JobCode
                            ,
                            p.JobFunction
                            ,
                            p.RequisitionTitle
                            ,
                            p.CountPositions
                            ,
                            p.LeftToHire
                            ,
                            p.CurrentReqStatus
                            ,
                            p.ReqStatusDate
                            ,
                            p.FirstApprovalDate
                            ,
                            p.ReqCreationDate
                            ,
                            p.InternalPostingStatus
                            ,
                            p.InternalPostingDate
                            ,
                            p.InternalUnpostingDate
                            ,
                            p.PersonReplacing
                            ,
                            p.PersonReplacingSOEID
                            ,
                            p.EmployeeReferalBonus
                            ,
                            p.EligibleforCampsHire
                            ,
                            p.HiringManager
                            ,
                            p.HiringManagerGEID
                            ,
                            p.HRRepresentative
                            ,
                            p.FinancialJustification
                            ,
                            p.RequisitionJustification
                            ,
                            p.IsitaProductionJob
                            ,
                            p.FulltimePartTime
                            ,
                            p.OvertimeStatus
                            ,
                            p.ReqSalaryGrade
                            ,
                            p.MappedOfficerTitle
                            ,
                            p.OfficerTitle
                            ,
                            p.OfficerTitleCode
                            ,
                            p.StandardGrade
                            ,
                            p.Grade
                            ,
                            p.WorldRegion
                            ,
                            p.Country
                            ,
                            p.StateProvince
                            ,
                            p.City
                            ,
                            p.CSSCenter
                            ,
                            p.LocationCode
                            ,
                            p.CSC
                            ,
                            p.OLM
                            ,
                            p.CompanyCode
                            ,
                            p.DaysOpen
                            ,
                            p.AgeGrouping
                            ,
                            p.AccountExpenseCode
                            ,
                            p.Applications
                            ,
                            p.Internal
                            ,
                            p.External
                            ,
                            p.New
                            ,
                            p.Testing1
                            ,
                            p.Reviewed
                            ,
                            p.PhoneScreen
                            ,
                            p.Testing2
                            ,
                            p.Interview
                            ,
                            p.OfferCreation
                            ,
                            p.OfferExtended
                            ,
                            p.OfferAccepted
                            ,
                            p.Hired
                            ,
                            p.OfferDeclined
                            ,
                            p.ReqEmployeeStatus
                            ,
                            p.ReqCreatorUser
                            ,
                            p.APACLegalVehicle
                            ,
                            p.SumOfOpenDaysLeftToHire

                        }

                });
            }
            return Content(o["item"].ToString());
        }


        [Route("setAsDefault")]
        public ActionResult setAsDefault(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            int pFile = Convert.ToInt32(objParameter["pFile"].ToString());

            DAO.procOpen_ChangeEnable(pFile);

            return null;
        }

    }
}


