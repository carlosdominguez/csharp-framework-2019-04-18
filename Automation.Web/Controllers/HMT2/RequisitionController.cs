﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Automation.Web.Models.HMT2;


using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace Automation.Web.Controllers.HMT2
{

    [RoutePrefix("HMT2")]
    public class RequisitionController : Controller
    {
        [Route("Requisition")]
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            string subAppPath = GlobalUtil.GetSubAppPath();
            viewModel.Add("SubAppPath", subAppPath);
            return (View("~/Views/HMT2/PendingStart.cshtml", viewModel));
        }

        [Route("getReqList")]
        public ActionResult getReqList()
        {
            HMT2Entities DAO = new HMT2Entities();

            string userSOEID = GlobalUtil.GetSOEID();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWR_Requisition
                    select new
                    {      
                        p.ID
                           ,p.Number
                          ,p.Name
                          ,p.DateCreated
                          ,p.NumberPositions
                          ,p.Clevel
                          ,p.StatusID
                          ,p.DaysOpen
                          ,p.GOC
                          ,p.Expr1 
                     //     ,p.BusinessUnit
                     //     ,p.Business
                     //     ,p.BusinessDetail
                     //     ,p.ManagedGeography
                     //     ,p.PhysicalRegion
                     //     ,p.ManagedFunction
                     //     ,p.OrgLevel1
                     //     ,p.OrgLevel2
                     //     ,p.OrgLevel3
                     //     ,p.OrgLevel4
                     //     ,p.OrgLevel5
                     //     ,p.OrgLevel6
                     //     ,p.OrgLevel8
                     //     ,p.OrgLevel7
                     //     ,p.OrgLevel9
                     //     ,p.MGLevel1
                     //     ,p.MGLevel2
                     //     ,p.MGLevel3
                     //     ,p.MGLevel4
                     //     ,p.MGLevel5
                     //     ,p.MGLevel6
                     //     ,p.CSWWorkflowName
                     //     ,p.SourceSystem
                     //     ,p.Expr2
                     //     ,p.GOCDescription
                     //     ,p.Recruiter
                     //     ,p.RecruiterGEID
                     //     ,p.RelationshipIndicator
                     //     ,p.LefttoHire
                     //     ,p.ReqStatusDate
                     //     ,p.FirstApprovalDate
                     //     ,p.ReqCreationDate
                     //     ,p.InternalPostingStatus
                     //     ,p.InternalPostingDate
                     //     ,p.InternalUnpostingDate
                     //     ,p.PersonReplacing
                     //     ,p.PersonReplacingSOEID
                     //     ,p.EmployeeReferalBonus
                     //     ,p.EligibleforCampsHire
                     //     ,p.HiringManagerGEID
                     //     ,p.HiringManager
                     //     ,p.FinancialJustification
                     //     ,p.RequisitionJustification
                     //     ,p.IsitaProductionJob
                     //     ,p.FullTimePartTime
                     //     ,p.OvertimeStatus
                     //     ,p.ReqSalaryGrade
                     //     ,p.MappedOfficerTitle
                     //     ,p.OfficerTitle
                     //     ,p.OfficerTitleCode
                     //     ,p.StandardGrade
                     //     ,p.Grade
                     //     ,p.Country
                     //     ,p.WorldRegion
                     //     ,p.CSSCenter
                     //     ,p.LocationCode
                     //     ,p.OLM
                     //     ,p.CSC
                     //     ,p.CompanyCode
                     //    
                    }

            });

            return Content(o["item"].ToString());

        }

        [Route("getListReeng")]
        public ActionResult getRequisition()
        {
            HMT2Entities DAO = new HMT2Entities();
            
            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWR_Requisition
                    orderby p.ID descending
                    select new
                    {
                        p.ID,
                        p.Number,
                        p.Name,
                        p.DateCreated,
                        p.NumberPositions,
                        p.Clevel,
                        p.StatusID,
                        p.DaysOpen,
                        p.GOC,
                        p.Expr1 ,
                    //   p.BusinessUnit,
                    //   p.Business,
                    //   p.BusinessDetail,
                    //   p.ManagedGeography,
                    //   p.PhysicalRegion,
                    //   p.ManagedFunction,
                    //   p.OrgLevel1,
                    //   p.OrgLevel2,
                    //   p.OrgLevel3,
                    //   p.OrgLevel4,
                    //   p.OrgLevel5,
                    //   p.OrgLevel6,
                    //   p.OrgLevel7,
                    //   p.OrgLevel8,
                    //   p.OrgLevel9,
                    //   p.OrgLevel10,
                    //   p.OrgLevel11,
                    //   p.OrgLevel12,
                    //   p.MGLevel1,
                    //   p.MGLevel2,
                    //   p.MGLevel3,
                    //   p.MGLevel4,
                    //   p.MGLevel5,
                    //   p.MGLevel6,
                    //   p.MGLevel7,
                    //   p.CSWWorkflowName,
                    //   p.SourceSystem,
                    //   p.GOCDescription,
                    //   p.Recruiter,
                    //   p.RecruiterGEID,
                    //   p.RelationshipIndicator,
                    //   p.Requisition_,
                    //   p.AlternateReq_,
                    //   p.JobCode,
                    //   p.JobFunction,
                    //   p.RequisitionTitle,
                    //   p.C_ofPositions,
                    //   p.LefttoHire,
                    //   p.CurrentReqStatus,
                    //   p.ReqStatusDate,
                    //   p.FirstApprovalDate,
                    //   p.ReqCreationDate,
                    //   p.InternalPostingStatus,
                    //   p.InternalPostingDate,
                    //   p.InternalUnpostingDate,
                    //   p.PersonReplacing,
                    //   p.PersonReplacingSOEID,
                    //   p.EmployeeReferalBonus,
                    //   p.EligibleforCampsHire,
                    //   p.HiringManager,
                    //   p.HiringManagerGEID,
                    //   p.HRRepresentative,
                    //   p.FinancialJustification,
                    //   p.RequisitionJustification,
                    //   p.IsitaProductionJob,
                    //   p.FullTimePartTime,
                    //   p.OvertimeStatus,
                    //   p.ReqSalaryGrade,
                    //   p.MappedOfficerTitle,
                    //   p.OfficerTitle,
                    //   p.OfficerTitleCode,
                    //   p.StandardGrade,
                    //   p.Grade,
                    //   p.WorldRegion,
                    //   p.Country,
                    //   p.StateProvince,
                    //   p.City,
                    //   p.CSSCenter,
                    //   p.LocationCode,
                    //   p.CSC,
                    //   p.OLM,
                    //   p.CompanyCode,
                    //   p.AgeGrouping,
                    //   p.AccountExpenseCode,
                    //   p.Applications,
                    //   p.Internal,
                    //   p.External,
                    //   p.New,
                    //   p.Testing1,
                    //   p.Reviewed,
                    //   p.PhoneScreen,
                    //   p.Testing2,
                    //   p.Interview,
                    //   p.OfferCreation,
                    //   p.OfferExtended,
                    //   p.OfferAccepted,
                    //   p.Hired,
                    //   p.OfferDeclined,
                    //   p.ReqEmployeeStatus,
                    //   p.ReqCreatorUser,
                    //   p.APACLegalVehicle,
                    //   p.SumofOpenDaysLefttoHire
                    }

            });

            return Content(o["item"].ToString());
        }

        [Route("getReqStatus")]
        public ActionResult getReqStatus()
        {
            HMT2Entities DAO = new HMT2Entities();

            string userSOEID;
            userSOEID = GlobalUtil.GetSOEID();


            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblR_ReqStatus
                    select new
                    {
                        p.ID,
                        p.Name 
                    }

            });

            return Content(o["item"].ToString());
        }

        [Route("getTotalRolesList")]
        public ActionResult getTotalRolesList()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = null;

            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWR_ROLESREQ
                    select new
                    {
                        p.ID
                      ,
                        p.RoleID
                      ,
                        p.RoleTypeID
                      ,
                        p.FromType
                      ,
                        p.Number
                      ,
                        p.IDType
                      ,
                        p.CreationDate
                      ,
                        p.CreatedBy
                      ,
                        p.IsActive
                      ,
                        p.ModifyBy
                      ,
                        p.ModifyDate
                      ,
                        p.Name
                      ,
                        p.Description
                      ,
                        p.GOCID
                      ,
                        p.MSL09
                      ,
                        p.Process
                      ,
                        p.SubProcess
                      ,
                        p.MgdGeaography
                      ,
                        p.MgdRegion
                      ,
                        p.MgdCountry
                      ,
                        p.LVID
                      ,
                        p.PhysicalLocationID
                      ,
                        p.ChargeOutProfileID
                      ,
                        p.RoleIsActive
                      ,
                        p.RoleCreaetedBy
                      ,
                        p.CreatedDate
                      ,
                        p.LastUpdateBy
                      ,
                        p.LastUpdateDate
                      ,
                        p.StatusID
                      ,
                        p.IsFteable
                      ,
                        p.IsDirectStaf
                      ,
                        p.FTEPercentage
                      ,
                        p.Status
                      ,
                        p.ManagedSegment
                      ,
                        p.ManagedGeography
                      ,
                        p.PhysicalLocation
                      ,
                        p.ChargeOutProfile
                      ,
                        p.RoleAuto
                      ,
                        p.GOCName
                      ,
                        p.CssDriver
                      ,
                        p.CountReq
                    }
            });


            return Content(o["item"].ToString());


        }

        [Route("addRoleToReq")]
        public ActionResult addRoleToReq(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            //Charter Info

            int roleID = Convert.ToInt32(objParameter["pRoleID"].ToString());
            string reqID = objParameter["pReqID"].ToString();
            string userSOEID = GlobalUtil.GetSOEID();


            tblR_Requisition req = (from c in DAO.tblR_Requisition where c.Number == reqID select c).SingleOrDefault<tblR_Requisition>();

            DAO.procR_AddRoleToReq(roleID, req.ID, userSOEID);

            
            return Content("");
        }

        [Route("renderRoleInReq")]
        public ActionResult renderRoleInReq(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string reqID = objParameter["pReqID"].ToString();
            
            tblR_Requisition req = (from c in DAO.tblR_Requisition where c.Number == reqID select c).SingleOrDefault<tblR_Requisition>();

            JObject o = null;

            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWR_RolesActiveInReq
                    where p.IsActive == true && p.ReqID == req.ID 
                    select new
                    {
                        p.ID
                      ,
                        p.RoleID
                      ,
                        p.RoleTypeID
                      ,
                        p.FromType
                      ,
                        p.Number
                      ,
                        p.IDType
                      ,
                        p.RoleIDnumber 
                      ,
                        p.CreationDate
                      ,
                        p.CreatedBy
                      ,
                        p.IsActive
                      ,
                        p.ModifyBy
                      ,
                        p.ModifyDate
                      ,
                        p.Name
                      ,
                        p.Description
                      ,
                        p.GOCID
                      ,
                        p.MSL09
                      ,
                        p.Process
                      ,
                        p.SubProcess
                      ,
                        p.MgdGeaography
                      ,
                        p.MgdRegion
                      ,
                        p.MgdCountry
                      ,
                        p.LVID
                      ,
                        p.PhysicalLocationID
                      ,
                        p.ChargeOutProfileID
                      ,
                        p.RoleIsActive
                      ,
                        p.RoleCreaetedBy
                      ,
                        p.CreatedDate
                      ,
                        p.LastUpdateBy
                      ,
                        p.LastUpdateDate
                      ,
                        p.StatusID
                      ,
                        p.IsFteable
                      ,
                        p.IsDirectStaf
                      ,
                        p.FTEPercentage
                      ,
                        p.Status
                      ,
                        p.ManagedSegment
                      ,
                        p.ManagedGeography
                      ,
                        p.PhysicalLocation
                      ,
                        p.ChargeOutProfile
                      ,
                        p.RoleAuto
                      ,
                        p.GOCName
                      ,
                        p.CssDriver

                    }
            });


            return Content(o["item"].ToString());
        }

        [Route("deleteSelectedRole")]
        public ActionResult deleteSelectedRole(string data)
        {
            //HMT2Entities DAO = new HMT2Entities();

            //var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            ////Charter Info

            //int ID = Convert.ToInt32(objParameter["pID"].ToString());


            //tblR_ReqRoleID roleReq = (from c in DAO.tblR_ReqRoleID where c.ID == ID select c).SingleOrDefault<tblR_ReqRoleID>();

            //DAO.tblR_ReqRoleID.Remove(roleReq);

            //DAO.SaveChanges();

            return Content("");
        }
        
    }
}
