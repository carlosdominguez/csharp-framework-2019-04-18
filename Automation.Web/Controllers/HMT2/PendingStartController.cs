﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Automation.Web.Models.HMT2;


using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace Automation.Web.Controllers.HMT2
{
    [RoutePrefix("HMT2")]
    public class PendingStartController : Controller
    {
        [Route("PendingStart")]
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            string subAppPath = GlobalUtil.GetSubAppPath();
            viewModel.Add("SubAppPath", subAppPath);
            return (View("~/Views/HMT2/PendingStart.cshtml", viewModel));
        }


        [Route("getTotalRolesList")]
        public ActionResult getTotalRolesList()
        {
            HMT2Entities DAO = new HMT2Entities();

            return Content("");
        }

        [ValidateInput(false)]
        [Route("uploadReenFile")]
        public ActionResult uploadReenFile(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var objParameter = JsonConvert.DeserializeObject(data);

            DataTable ptable = (DataTable)JsonConvert.DeserializeObject(data, (typeof(DataTable)));

            List<tblF_FeedColumn> columnNames = (from c in DAO.tblF_FeedColumn where c.FeedId == 7 orderby c.OrderNum ascending select c).ToList<tblF_FeedColumn>();

            int columnIndex = 0;
            foreach (tblF_FeedColumn column in columnNames)
            {
                try
                {
                    ptable.Columns[column.ColumnName].SetOrdinal(columnIndex);
                }
                catch
                {
                    ptable.Columns.Add(column.ColumnName).SetOrdinal(columnIndex);
                }

                columnIndex++;
            }

            int desiredSize = columnNames.Count ;

            while (ptable.Columns.Count > desiredSize)
            {
                ptable.Columns.RemoveAt(desiredSize);
            }



            (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[procTaleo_UploadFILE]", new List<Dictionary<string, string>> { 
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEID" },
                                { "Value", GlobalUtil.GetSOEID() }
                            }
                        });

            return null;

        }

        [Route("getVersionList")]
        public ActionResult getVersionList()
        {

            HMT2Entities DAO = new HMT2Entities();


            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblTaleo_UploadDate
                    orderby p.ID descending
                    select new
                    {
                        p.ID,
                        p.UploadBy,
                        p.UploadDate,
                        p.Enable
                    }

            });

            return Content(o["item"].ToString());

        }

        [Route("getListReeng")]
        public ActionResult getListReeng(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            int pFile = Convert.ToInt32(objParameter["pFile"].ToString());

            tblTaleo_UploadDate FILE;

            int FileID = 0;
            try
            {
                if (pFile == 0)
                {
                    FILE = (from p in DAO.tblTaleo_UploadDate where p.Enable == true select p).SingleOrDefault<tblTaleo_UploadDate>();
                    FileID = FILE.ID;
                }
                else
                {
                    FILE = (from p in DAO.tblTaleo_UploadDate where p.ID == pFile select p).SingleOrDefault<tblTaleo_UploadDate>();
                    FileID = FILE.ID;
                }
            }
            catch
            {
                FileID = 0;
            }




            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblTaleo_File
                    where p.UploadID == FileID
                    orderby p.ID descending
                    select new
                    {
                        p.ID 
	                    ,p.UploadID
                        ,
                        p.Business
                        ,
                        p.SubBusiness
                        ,
                        p.ManagedGeograph
                        ,
                        p.PhysicalRegion
                        ,
                        p.ManagedFunction
                        ,
                        p.OrgLevel1
                        ,
                        p.OrgLevel2
                        ,
                        p.OrgLevel3
                        ,
                        p.OrgLevel4
                        ,
                        p.OrgLevel5
                        ,
                        p.OrgLevel6
                        ,
                        p.OrgLevel7
                        ,
                        p.OrgLevel8
                        ,
                        p.OrgLevel9
                        ,
                        p.OrgLevel10
                        ,
                        p.OrgLevel11
                        ,
                        p.OrgLevel12
                        ,
                        p.MGLevel1
                        ,
                        p.MGLevel2
                        ,
                        p.MGLevel3
                        ,
                        p.MGLevel4
                        ,
                        p.CSWWorkflowName
                        ,
                        p.SourceSystem
                        ,
                        p.GOC
                        ,
                        p.GOCDescription
                        ,
                        p.Recruiter
                        ,
                        p.RecruiterGEID
                        ,
                        p.Requisition
                        ,
                        p.AlternateReq
                        ,
                        p.ReqSalaryGrade
                        ,
                        p.JobCode
                        ,
                        p.JobFunction
                        ,
                        p.RequisitionTitle
                        ,
                        p.FirstApprovalDate
                        ,
                        p.InternalPostingStatus
                        ,
                        p.ExternalPostingStatus
                        ,
                        p.PersonReplacing
                        ,
                        p.PersonReplacingSOEID
                        ,
                        p.HiringManager
                        ,
                        p.HiringManagerGEID
                        ,
                        p.HRRepresentative
                        ,
                        p.FinanceJustification
                        ,
                        p.RequisitionJustification
                        ,
                        p.IsitaProductionJob
                        ,
                        p.FulltimeParttime
                        ,
                        p.EligibleforCampusHire
                        ,
                        p.OvertimeStatus
                        ,
                        p.MappedOfficerTitle
                        ,
                        p.OfficerTitle
                        ,
                        p.OfficerTitleCode
                        ,
                        p.MappedStandardGrade
                        ,
                        p.Grade
                        ,
                        p.WorldRegion
                        ,
                        p.Country
                        ,
                        p.StateProvince
                        ,
                        p.City
                        ,
                        p.CSSCenter
                        ,
                        p.LocationCode
                        ,
                        p.TimetoStart
                        ,
                        p.TimetoOfferAccept
                        ,
                        p.TimetoOfferExtend
                        ,
                        p.CompanyCode
                        ,
                        p.AccountExpenseCode
                        ,
                        p.JobOfferIsTentative
                        ,
                        p.StartMonth
                        ,
                        p.StartDate
                        ,
                        p.StartPendingStarts
                        ,
                        p.OfferAcceptDate
                        ,
                        p.JobOfferExtendedDate
                        ,
                        p.CandidateID
                        ,
                        p.CandidatesName
                        ,
                        p.GEID
                        ,
                        p.SOEID
                        ,
                        p.ApplicationCurrentCSWStatus
                        ,
                        p.CurrentSalaryGradeInternalCandidatesonly
                        ,
                        p.InternalExternal
                        ,
                        p.GeneralSource
                        ,
                        p.MajorSourceApplication
                        ,
                        p.MinorSourceApplication
                        ,
                        p.MajorSourceOffer
                        ,
                        p.MinorSourceOffer
                        ,
                        p.PriorEmployer
                        ,
                        p.RecruitingAgencyCost
                        ,
                        p.RecentCollegeGraduate
                        ,
                        p.OtherFees
                        ,
                        p.CSC
                        ,
                        p.APACLegalVehicle
                    }

            });

            return Content(o["item"].ToString());
        }


        [Route("setAsDefault")]
        public ActionResult setAsDefault(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            int pFile = Convert.ToInt32(objParameter["pFile"].ToString());

            DAO.procTaleo_ChangeEnable(pFile);

            return null;
        }

        [Route("getRequisition")]
        public ActionResult getRequisition()
        {
            HMT2Entities DAO = new HMT2Entities();

            string userSOEID = GlobalUtil.GetSOEID();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWNM_NonMOU
                    select new
                    {
                        p.RequestID
                           ,
                        p.Name

                    }

            });

            return Content(o["item"].ToString());

        }

    }
}


