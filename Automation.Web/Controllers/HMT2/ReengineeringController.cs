﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Automation.Web.Models.HMT2;


using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace Automation.Web.Controllers.HMT2
{
    [RoutePrefix("HMT2")]
    public class ReengineeringController : Controller
    {
        [Route("Reengineering")]
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            string subAppPath = GlobalUtil.GetSubAppPath();
            viewModel.Add("SubAppPath", subAppPath);
            return (View("~/Views/HMT2/Reengineering.cshtml", viewModel));
        }


        [Route("getTotalRolesList")]
        public ActionResult getTotalRolesList()
        {
            HMT2Entities DAO = new HMT2Entities();

            return Content("");
        }

        [Route("uploadReenFile")]
        public ActionResult uploadReenFile(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var objParameter = JsonConvert.DeserializeObject(data);

            DataTable ptable = (DataTable)JsonConvert.DeserializeObject(data, (typeof(DataTable)));

            List<tblF_FeedColumn> columnNames = (from c in DAO.tblF_FeedColumn where c.FeedId == 1 orderby c.OrderNum ascending select c).ToList<tblF_FeedColumn>();


            int columnIndex = 0;
            foreach (tblF_FeedColumn column in columnNames)
            {
                try
                {
                    ptable.Columns[column.ColumnName].SetOrdinal(columnIndex);
                }
                catch
                {
                  //  ptable.Columns.Add(column.ColumnName).SetOrdinal(columnIndex);
                }

                columnIndex++;
            }


            (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[procReengUploadFILE]", new List<Dictionary<string, string>> { 
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEID" },
                                { "Value", GlobalUtil.GetSOEID() }
                            }
                        });

            return null;

        }

        [Route("getVersionList")]
        public ActionResult getVersionList()
        {

                HMT2Entities DAO = new HMT2Entities();


                JObject o = JObject.FromObject(new
                {
                    item =
                        from p in DAO.tblReen_UploadDate 
                        orderby p.ID descending  
                        select new
                        {
                            p.ID ,
                            p.UploadBy ,
                            p.UploadDate ,
                            p.Enable 
                        }

                });

                return Content(o["item"].ToString());
          
        }

        [Route("getListReeng")]
        public ActionResult getListReeng(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            int pFile = Convert.ToInt32(objParameter["pFile"].ToString());

            tblReen_UploadDate FILE;

            if (pFile == 0)
            {
                FILE = (from p in DAO.tblReen_UploadDate where p.Enable == true select p).SingleOrDefault<tblReen_UploadDate>();
            }
            else
            {
                FILE = (from p in DAO.tblReen_UploadDate where p.ID == pFile select p).SingleOrDefault<tblReen_UploadDate>();
            }

            


            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblReen_File 
                    where p.UploadID == FILE.ID 
                    orderby p.ID descending
                    select new
                    {
                        p.ID,
                        p.UploadID,
                        p.ReengID
                      ,p.InitiativeType
                      ,p.IncludeExclude
                      ,p.FinanceFRO
                      ,p.Title
                      ,p.InitiativeDescription
                      ,p.FinanceFROTransformation
                      ,p.KeyInitiatives
                      ,p.GPLReengineeringCategory
                      ,p.Process
                      ,p.SavesAccruingCenter
                      ,p.CenterImpact
                      ,p.Region
                      ,p.ConfidenceLevel
                      ,p.FTEReleaseStatus
                      ,p.ProjectStatus
                      ,p.ActualStartDate
                      ,p.ActualEndDate
                      ,p.PlanFTEOpportunities
                      ,p.GPL
                      ,p.GPO
                      ,p.RPL
                      ,p.SubProcess
                      ,p.HCSavesMOUorRP
                      ,p.IndicateEReqHCSavesMOU
                      ,p.EmployeeNameMOUTitle
                      ,p.Month
                      ,p.QuarterImpact
                      ,p.Years
                      ,p.GOC
                      ,p.NewCSSMaturitySubCategory
                      ,p.CurrentYearJan
                      ,p.CurrentYearFeb
                      ,p.CurrentYearMar
                      ,p.CurrentYearApr
                      ,p.CurrentYearMay
                      ,p.CurrentYearJun
                      ,p.CurrentYearJul
                      ,p.CurrentYearAug
                      ,p.CurrentYearSep
                      ,p.CurrentYearOct
                      ,p.CurrentYearNov
                      ,p.CurrentYearDec
                      ,p.CurrentYearTotalFTE
                    }

            });

            return Content(o["item"].ToString());
        }


        [Route("setAsDefault")]
        public ActionResult setAsDefault(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            int pFile = Convert.ToInt32(objParameter["pFile"].ToString());

            DAO.procReeng_ChangeEnable(pFile);

            return null;
        }
        
    }
}

 
