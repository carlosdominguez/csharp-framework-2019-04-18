﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Automation.Web.Models.HMT2;


using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace Automation.Web.Controllers.HMT2
{

    [RoutePrefix("HMT2")]
    public class MatchDataController : Controller
    {
        [Route("MatchData")]
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            string subAppPath = GlobalUtil.GetSubAppPath();
            viewModel.Add("SubAppPath", subAppPath);
            return (View("~/Views/HMT2/MatchData.cshtml", viewModel));
        }

        [Route("getEmployeeRequisition")]
        public ActionResult getEmployeeRequisition(string data)
        {
            HMT2Entities DAO = new HMT2Entities();
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            string goc = objParameter["GOC"].ToString().Split(' ')[0];

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWE_EmployeeRequisition
                    where p.Goc == goc 
                    select new
                    {
                       p.Requisition
                      ,p.SOEID
                      ,p.GEID
                      ,p.Name
                      ,p.Goc
                      ,p.HiringDate
                      ,p.ClassificationLetter
                      ,p.Description
                    }
            });


            return Content(o["item"].ToString());

        }

        [Route("getRoleList")]
        public ActionResult getRoleList(string data)
        {
            HMT2Entities DAO = new HMT2Entities();
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            string goc = objParameter["GOC"].ToString().Split(' ')[0];

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWR_MatchDataListEmployee
                    where p.GOCRole == goc
                    select new
                    {
                       p.ID
                      ,p.RoleID
                      ,p.Name
                      ,p.MOU_NonMOU
                      ,p.MOU_NonMOU_Desc
                      ,p.GOCRole
                      ,p.StartDate
                      ,p.EndDate
                      ,p.Driver
                      ,p.CSSDriver
                      ,p.SOEID
                      ,p.GEID
                      ,p.GOCEmployee
                      ,p.Requisition
                      ,p.Number
                      ,p.Center
                      ,p.IsDirectStaf 
                      ,p.FirstName
                      ,p.LastName

                    }
            });


            return Content(o["item"].ToString());

        }

        [Route("getRoleList")]
        public ActionResult getRoleListInside(string data)
        {
            HMT2Entities DAO = new HMT2Entities();
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            string role = objParameter["ROLE"].ToString().Split(' ')[0];

            tblR_Role roleClass = (from p in DAO.tblR_Role where p.RoleID == role select p).SingleOrDefault<tblR_Role>();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWR_RoleEmployeeMatchDataHistory
                    where p.RoleID == roleClass.ID
                    select new
                    {
                        p.ID
                      ,
                        p.RoleID
                      ,
                        p.StartDate
                      ,
                        p.EndDate
                      ,
                        p.Driver
                      ,
                        p.CssDriver
                      ,
                        p.SOEID 
                      ,
                        p.Goc
                      ,
                        p.Requisition
                      ,
                        p.Number

                    }
            });


            return Content(o["item"].ToString());

        }

        [Route("getMOUList")]
        public ActionResult getMOUList(string data)
        {

            HMT2Entities DAO = new HMT2Entities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            string goc = objParameter["GOC"].ToString().Split(' ')[0];

            JObject o = null;

            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWR_MOUbyGOC
                    where p.GOCID == goc
                    select new
                    {
                        p.MOUAlias,
                        p.Name,
                        p.Description,
                        p.WorkTypeName,
                        p.ReceivingPhysicalLocationName,
                        p.ReceivingManagedSegmentID,
                        p.ReceivingManagedSegmentName,
                        p.Driver ,
                        p.CssDriver

                    }
            });


            return Content(o["item"].ToString());

        }

        [Route("getNonMOUList")]
        public ActionResult getNonMOUList(string data)
        {

            HMT2Entities DAO = new HMT2Entities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            string goc = objParameter["GOC"].ToString().Split(' ')[0];

            JObject o = null;

            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWNM_NonMOU
                    where p.ReceivingGOC == goc
                    select new
                    {
                        p.NonMOUAlias,
                        p.Name,
                        p.Type,
                        p.ManagedSegmentName,
                        p.DriverName,
                        p.CssDriver 

                    }
            });


            return Content(o["item"].ToString());

        }

        [Route("getDriverList")]
        public ActionResult getDriverList(string data)
        {

            HMT2Entities DAO = new HMT2Entities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            string goc = objParameter["GOC"].ToString().Split(' ')[0];

            JObject o = null;

            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblD_Driver
                    select new
                    {
                        Driver = p.Name ,
                        CSSDriver = p.tblD_CSSDriver.Name,
                        p.Source

                    }
            });


            return Content(o["item"].ToString());

        }

        [Route("MatchAllData")]
        public ActionResult MatchAllData(string data)
        {

            HMT2Entities DAO = new HMT2Entities();

            //var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            //var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            //Dictionary<String, String> obj = new Dictionary<string, string>();

            DataTable ptable = (DataTable)JsonConvert.DeserializeObject(data, (typeof(DataTable)));
            
            try
            {
                ptable.Columns.Remove("dataindex");
            }
            catch { }

            try
            {
                ptable.Columns.Remove("LastName");
            }
            catch { }

            try
            {
                ptable.Columns.Remove("FirstName");
            }
            catch { }


            (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[procR_MatchData]", new List<Dictionary<string, string>> { 
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEID" },
                                { "Value", GlobalUtil.GetSOEID() }
                            }
                        });

            return null;

        }

        [Route("getCenterList")]
        public ActionResult getCenterList(string data)
        {

            HMT2Entities DAO = new HMT2Entities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            string goc = objParameter["GOC"].ToString().Split(' ')[0];

            JObject o = null;

            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblFC_Center
                    select new
                    {
                        p.ID,
                        p.Center

                    }
            });


            return Content(o["item"].ToString());

        }


        [Route("deleteRoleEmployee")]
        public ActionResult deleteRoleEmployee(string data)
        {

            HMT2Entities DAO = new HMT2Entities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int id = Convert.ToInt32(objParameter["id"].ToString());

            tblR_RoleEmployee objectr = (from p in DAO.tblR_RoleEmployee where p.ID == id select p).SingleOrDefault<tblR_RoleEmployee>();


            DAO.tblR_RoleEmployee.Remove(objectr);

            DAO.SaveChanges();

            return Content("");

        }


        [Route("deleteRole")]
        public ActionResult deleteRole(string data)
        {

            HMT2Entities DAO = new HMT2Entities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int id = Convert.ToInt32(objParameter["id"].ToString());

            DAO.procR_DeleteRole(id);

            return Content("");

        }

    }
}
