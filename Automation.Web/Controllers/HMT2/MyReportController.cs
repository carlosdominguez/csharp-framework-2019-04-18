﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Automation.Web.Models.HMT2;


using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace Automation.Web.Controllers.HMT2
{

        [RoutePrefix("HMT2")]
        public class MyReportController : Controller
        {
            [Route("MyReport")]
            public ActionResult Index()
            {
                return (View("~/Views/HMT2/MyReport.cshtml"));
            }


            [Route("getTotalRolesList")]
            public ActionResult getTotalRolesList()
            {
                HMT2Entities DAO = new HMT2Entities();

                JObject o = null;

                o = JObject.FromObject(new
                {
                    item =
                        from p in DAO.VWR_CompleteList
                        select new
                        {
                            p.ID
                          ,
                            p.RoleID
                          ,
                            p.RoleTypeID
                          ,
                            p.FromType
                          ,
                            p.Number
                          ,
                            p.IDType
                          ,
                            p.CreationDate
                          ,
                            p.CreatedBy
                          ,
                            p.IsActive
                          ,
                            p.ModifyBy
                          ,
                            p.ModifyDate
                          ,
                            p.Name
                          ,
                            p.Description
                          ,
                            p.GOCID
                          ,
                            p.MSL09
                          ,
                            p.Process
                          ,
                            p.SubProcess
                          ,
                            p.MgdGeaography
                          ,
                            p.MgdRegion
                          ,
                            p.MgdCountry
                          ,
                            p.LVID
                          ,
                            p.PhysicalLocationID
                          ,
                            p.ChargeOutProfileID
                          ,
                            p.RoleIsActive
                          ,
                            p.RoleCreaetedBy
                          ,
                            p.CreatedDate
                          ,
                            p.LastUpdateBy
                          ,
                            p.LastUpdateDate
                          ,
                            p.StatusID
                          ,
                            p.IsFteable
                          ,
                            p.IsDirectStaf
                          ,
                            p.FTEPercentage
                          ,
                            p.Status
                          ,
                            p.ManagedSegment
                          ,
                            p.ManagedGeography
                          ,
                            p.PhysicalLocation
                          ,
                            p.ChargeOutProfile
                          ,
                            p.RoleAuto
                          ,
                            p.GOCName
                        }
                });


                return Content(o["item"].ToString());


            }


        }

    
}
