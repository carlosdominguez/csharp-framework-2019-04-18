using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Automation.Web.Models.HMT2;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Automation.Web.Controllers.HMT2
{
    [RoutePrefix("HMT2")]
    public class HomeController : Controller
    {
        //
        // GET: /
        [Route("/")]
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            
            string subAppPath = GlobalUtil.GetSubAppPath();
            viewModel.Add("SubAppPath", subAppPath);
            //return View("~/Views/HMT2/Home.cshtml", viewModel);
            return View("~/Views/HMT2/Dashboard.cshtml", viewModel);
        }

        //
        // GET: /

        [Route("getReportDasthboard")]
        public ActionResult getReportDasthboard(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            string DS = objParameter["DS"].ToString();
            string CENTER = objParameter["CENTER"].ToString();

            JObject o = null;
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procFC_getDashboardRecords(CENTER,DS)

                    select new
                    {
                        p.Report,
                        p.JAN,
                        p.FEB,
                        p.MAR,
                        p.APR,
                        p.MAY,
                        p.JUN,
                        p.JUL,
                        p.AUG,
                        p.SEP,
                        p.OCT,
                        p.NOV,
                        p.DEC
                    }
            });


            return Content(o["item"].ToString());

        }


        [Route("getTotalMOU")]
        public ActionResult getTotalMOU(object listOfParams)
        {
            HMT2Entities DAO = new HMT2Entities();

            var total = DAO.VWM_MOUList.Count();


            return Content(total.ToString());

        }

        [Route("getTotalNonMOU")]
        public ActionResult getTotalNonMOU(object listOfParams)
        {
            HMT2Entities DAO = new HMT2Entities();

            int Status = (from p in DAO.tblNM_RequestStatus where p.Status == "Complete" select p).SingleOrDefault<tblNM_RequestStatus>().ID;

            var total = (from p in DAO.VWNM_NonMOU where p.Status == Status select p).Count();

            return Content(total.ToString());

        }

        [Route("getTotalRole")]
        public ActionResult getTotalRole(object listOfParams)
        {
            HMT2Entities DAO = new HMT2Entities();

            procFC_getRolesReport_Result result = DAO.procFC_getRolesReport().SingleOrDefault<procFC_getRolesReport_Result>();

            return Content(result.Role.ToString());
        }


        [Route("getTotalEmployee")]
        public ActionResult getTotalEmployee(object listOfParams)
        {
            HMT2Entities DAO = new HMT2Entities();

            procFC_getHCReport_Result result = DAO.procFC_getHCReport().SingleOrDefault<procFC_getHCReport_Result>();

            return Content(result.Roles.ToString());
        }


        [Route("getBarReport")]
        public ActionResult getBarReport(object listOfParams)
        {
            HMT2Entities DAO = new HMT2Entities();


            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procFC_getHomeReport() 
                    select new
                    {
                        p.CENTER,
                        p.DS ,
                        p.TEMP
                        
                    }

            });

            return Content(o["item"].ToString());
        }


        [Route("runJobs")]
        public ActionResult runJobs()
        {
            HMT2Entities DAO = new HMT2Entities();

            //DAO.procE_UpdateEmployeeDataFromGDW();
            DAO.procR_CreateMOURoleID();
            //DAO.procFC_CreateMonth();
            //DAO.procR_CreateNonMOURoleID();
            DAO.procFC_FixGoc();

            return null;
        }
    }
}
