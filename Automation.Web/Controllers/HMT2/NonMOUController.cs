﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Automation.Web.Models.HMT2;


using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;



namespace Automation.Web.Controllers.HMT2
{

    [RoutePrefix("HMT2")]
    public class NonMOUController : Controller
    {
        [Route("NonMOU")]
        public   ActionResult Index()
        {
            return ( View("~/Views/HMT2/NonMOU.cshtml"));
        }


        

       [Route("getGocList")]
        public ActionResult   getGocList()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWR_GOC
                    select new
                    {
                        p.GOCName
                    }

            });

            return Content( o["item"].ToString());

        }

       [Route("getFunctionManagedSegment")]
        public ActionResult   getFunctionManagedSegment()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWGhost_FROFunctionManagedSegment
                    select new
                    {
                        p.RowNumber,
                        p.ManagedGeographyName,
                        p.ManagedSegmentName,
                        p.ManagerName
                    }

            });

            return Content( o["item"].ToString());

        }

       [Route("getFunctionManagedSegmentSelected")]
        public ActionResult   getFunctionManagedSegmentSelected(string data)
        {
            try
            {
                var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string pNonMOU = objParameter["pNonMOU"].ToString();

                HMT2Entities DAO = new HMT2Entities();


                tblNM_Migration migration = (from c in DAO.tblNM_Migration where c.RequestAutoID == pNonMOU select c).SingleOrDefault<tblNM_Migration>();

                JObject o = JObject.FromObject(new
                {
                    item =
                        from p in DAO.VWGhost_FROFunctionManagedSegment
                        where p.RowNumber == migration.FroFunctionsMS
                        select new
                        {
                            p.RowNumber,
                            p.ManagedGeographyName,
                            p.ManagedSegmentName,
                            p.ManagerName
                        }

                });

                return Content(o["item"].ToString());
            }
            catch
            {
                HMT2Entities DAO = new HMT2Entities();
                JObject o = JObject.FromObject(new
                {
                    item =
                        from p in DAO.VWGhost_FROFunctionManagedSegment
                        where p.RowNumber == 0
                        select new
                        {
                            p.RowNumber,
                            p.ManagedGeographyName,
                            p.ManagedSegmentName,
                            p.ManagerName
                        }

                });

                return Content(o["item"].ToString());
            }
        }


       [Route("insertNewRequestDraft")]
        public ActionResult   insertNewRequestDraft()
        {
            HMT2Entities DAO = new HMT2Entities();

            string userSOEID;
            userSOEID = GlobalUtil.GetSOEID();


            procNM_createDraftMOU_Result query = DAO.procNM_createDraftMOU(userSOEID).SingleOrDefault<procNM_createDraftMOU_Result>(); ;



            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblNM_Migration
                    where p.RequestID == query.ID
                    select new
                    {
                        p.ID
                    }

            });


            return Content( o["item"].ToString());

        }

       [Route("insertFuncionManagedRow")]
        public ActionResult   insertFuncionManagedRow(string data)
        {

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            string pNonMOU = objParameter["pNonMOU"].ToString();
            int pFunctionManagedRow = Convert.ToInt32(objParameter["pFunctionManagedRow"].ToString());

            string userSOEID;

            userSOEID = GlobalUtil.GetSOEID();


            HMT2Entities DAO = new HMT2Entities();

            tblNM_Migration migration = (from c in DAO.tblNM_Migration where c.RequestAutoID == pNonMOU select c).SingleOrDefault<tblNM_Migration>();

  
            migration.FroFunctionsMS = pFunctionManagedRow;

            DAO.SaveChanges();

            DAO.procNM_createModifyID(migration.ID);


            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblNM_Migration
                    where p.ID == migration.ID
                    select new
                    {
                        p.RequestAutoID
                    }

            });

            return Content( o["item"].ToString());
        }


       //[Route("insertFuncionManagedRow")]
       // public ActionResult   insertFuncionManagedRow(string data)
       // {
       //     var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

       //      var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
       //     Dictionary<String, String> obj = new Dictionary<string, string>();



       //     decimal pFTE = Convert.ToDecimal(objParameter["pFTE"].ToString());
       //     string pBudgetGoc = objParameter["pBudgetGoc"].ToString();
       //     string pChargeOut = objParameter["pChargeOut"].ToString();
       //     int pFunctionManagedRow = Convert.ToInt32(objParameter["pFunctionManagedRow"].ToString());
       //     int pRequestID = Convert.ToInt32(objParameter["pRequestID"].ToString());

       //     string userSOEID;
       //     userSOEID = GlobalUtil.GetSOEID();


       //     HMT2Entities DAO = new HMT2Entities();

       //     DAO.procNM_insertMigrationToRequest(pRequestID, pFTE, pBudgetGoc, pChargeOut, pFunctionManagedRow);


       //     return Content( null);
       // }

       [Route("getMigrations")]
        public ActionResult   getMigrations(string data)
        {
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int pRequestID = Convert.ToInt32(objParameter["pRequestID"].ToString());

            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWNM_MigrationRequest
                    where p.RequestID == pRequestID
                    select new
                    {
                        p.ID,
                        p.RequestID,
                        p.FTE,
                        p.BudgetGoc,
                        p.ChargeOutGOC,
                        p.Expr1
                    }

            });

            return Content( o["item"].ToString());


        }

       [Route("saveDraft")]
        public ActionResult   saveDraft(string data)
        {
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int pRequestID = Convert.ToInt32(objParameter["pRequestID"].ToString());
            string name = objParameter["pName"].ToString();
            string description = objParameter["pSummary"].ToString();


            HMT2Entities DAO = new HMT2Entities();

            tblNM_RequestStatus STATUS = (from p in DAO.tblNM_RequestStatus where p.Status == "Draft" select p).SingleOrDefault();

            tblNM_Request Request = (from p in DAO.tblNM_Request where p.ID == pRequestID select p).SingleOrDefault();

            Request.Status = STATUS.ID;
            Request.Name = name;
            Request.Description = description;

            DAO.SaveChanges();

            return Content(null);

        }

       [Route("saveQueue")]
        public ActionResult   saveQueue(string data)
        {
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int pRequestID = Convert.ToInt32(objParameter["pRequestID"].ToString());
            string name = objParameter["pName"].ToString();
            string description = objParameter["pSummary"].ToString();


            HMT2Entities DAO = new HMT2Entities();

            tblNM_RequestStatus STATUS = (from p in DAO.tblNM_RequestStatus where p.Status == "Queue" select p).SingleOrDefault();

            tblNM_Request Request = (from p in DAO.tblNM_Request where p.ID == pRequestID select p).SingleOrDefault();
            List<tblNM_Migration> Migration = (from p in DAO.tblNM_Migration where p.RequestID == pRequestID select p).ToList<tblNM_Migration>();

            if (Migration.Count == 0)
            {
                return Content("Error");
            }
            else
            {
                foreach (tblNM_Migration M in Migration)
                {
                    M.Status = STATUS.ID;
                    DAO.SaveChanges();
                    DAO.procNM_createID(M.ID);
                }

                Request.Status = STATUS.ID;
                Request.Name = name;
                Request.Description = description;

                DAO.SaveChanges();

                return Content( "Complete");

            }
        }

       [Route("saveQueueFromList")]
        public ActionResult   saveQueueFromList(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            Dictionary<String, String> obj = new Dictionary<string, string>();

            int pRequestID = Convert.ToInt32(objParameter["pRequestID"].ToString());

            tblNM_Request request = (from p in DAO.tblNM_Request where p.ID == pRequestID select p).SingleOrDefault<tblNM_Request>();


            string name = request.Name;
            string description = request.Description;


            tblNM_RequestStatus STATUS = (from p in DAO.tblNM_RequestStatus where p.Status == "Queue" select p).SingleOrDefault();

            tblNM_Request Request = (from p in DAO.tblNM_Request where p.ID == pRequestID select p).SingleOrDefault();
            List<tblNM_Migration> Migration = (from p in DAO.tblNM_Migration where p.RequestID == pRequestID select p).ToList<tblNM_Migration>();

            if (Migration.Count == 0)
            {
                return Content( "Error");
            }
            else
            {
                foreach (tblNM_Migration M in Migration)
                {
                    M.Status = STATUS.ID;
                    DAO.SaveChanges();
                    DAO.procNM_createID(M.ID);
                }

                Request.Status = STATUS.ID;
                Request.Name = name;
                Request.Description = description;

                DAO.SaveChanges();

                return Content("Complete");

            }
        }

       [Route("deleteDraftFromList")]
        public ActionResult   deleteDraftFromList(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int pRequestID = Convert.ToInt32(objParameter["pRequestID"].ToString());

            tblNM_Request request = (from p in DAO.tblNM_Request where p.ID == pRequestID select p).SingleOrDefault<tblNM_Request>();

            tblNM_RequestStatus STATUS = (from p in DAO.tblNM_RequestStatus where p.Status == "Delete" select p).SingleOrDefault();

            request.Status = STATUS.ID;

            DAO.SaveChanges();
            return Content("Complete");
        }

       [Route("deleteQueueFromList")]
        public ActionResult   deleteQueueFromList(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int pRequestID = Convert.ToInt32(objParameter["pRequestID"].ToString());

            tblNM_Migration request = (from p in DAO.tblNM_Migration where p.ID == pRequestID select p).SingleOrDefault<tblNM_Migration>();

            tblNM_RequestStatus STATUS = (from p in DAO.tblNM_RequestStatus where p.Status == "Delete" select p).SingleOrDefault();

            request.Status = STATUS.ID;

            DAO.SaveChanges();
            return Content("Complete");
        }

       [Route("getListDraft")]
        public ActionResult   getListDraft()
        {
            HMT2Entities DAO = new HMT2Entities();

            string userSOEID;
            userSOEID = GlobalUtil.GetSOEID();


            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWNM_NonMOUDiscoverySaveData
                    where p.CreatedBy == userSOEID & p.Status != 1
                    select new
                    {
                        p.MIGRATIONID
                      ,
                        p.FTE
                      ,
                        p.FUNCTIONID
                      ,
                        p.RequestID
                      ,
                        p.Status
                      ,
                        p.NONMOUALIAS
                      ,
                        p.FUNCTIONMS
                      ,
                        p.FUNCTIONMSNAME
                      ,
                        p.FUNCTIONMG
                      ,
                        p.FUNCTIONMGNAME
                      ,
                        p.FUNCTIONMANAGER
                      ,
                        p.STATUSNAME
                      ,
                        p.Name
                      ,
                        p.TYPEID
                      ,
                        p.Scope
                      ,
                        p.Benefits
                      ,
                        p.TYPENAME
                      ,
                        p.FTECOST
                      ,
                        p.PL
                      ,
                        p.SITEMANAGERSOEID
                      ,
                        p.SITEFINANCESOEID
                      ,
                        p.MSLVL09
                      ,
                        p.Description
                      ,
                        p.SITEMANAGENAME
                      ,
                        p.SITEFINANCENAME
                      ,
                        p.PLNAME
                      ,
                        p.PLCOST
                      ,
                        p.PLMGID
                      ,
                        p.NameRequest
                      ,
                        p.CreatedBy
                      ,
                        p.Date
                      ,
                        p.StatusRequest
                      ,
                        p.Initiated
                      ,
                        p.FTEAvaible
                    }

            });

            return Content(o["item"].ToString());

        }

       [Route("getListQueue")]
        public ActionResult   getListQueue()
        {
            HMT2Entities DAO = new HMT2Entities();

            string userSOEID;
            userSOEID = GlobalUtil.GetSOEID();



            string pSTATUS = (from p in DAO.tblNM_RequestStatus where p.Status == "Queue" select p).SingleOrDefault().Status;



            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWNM_NonMOUDiscoverySaveData
                    where p.CreatedBy == userSOEID & p.Status == 2
                    orderby p.MIGRATIONID descending
                    select new
                    {
                        p.MIGRATIONID
                      ,
                        p.FTE
                      ,
                        p.FUNCTIONID
                      ,
                        p.RequestID
                      ,
                        p.Status
                      ,
                        p.NONMOUALIAS
                      ,
                        p.FUNCTIONMS
                      ,
                        p.FUNCTIONMSNAME
                      ,
                        p.FUNCTIONMG
                      ,
                        p.FUNCTIONMGNAME
                      ,
                        p.FUNCTIONMANAGER
                      ,
                        p.STATUSNAME
                      ,
                        p.Name
                      ,
                        p.TYPEID
                      ,
                        p.Scope
                      ,
                        p.Benefits
                      ,
                        p.TYPENAME
                      ,
                        p.FTECOST
                      ,
                        p.PL
                      ,
                        p.SITEMANAGERSOEID
                      ,
                        p.SITEFINANCESOEID
                      ,
                        p.MSLVL09
                      ,
                        p.Description
                      ,
                        p.SITEMANAGENAME
                      ,
                        p.SITEFINANCENAME
                      ,
                        p.PLNAME
                      ,
                        p.PLCOST
                      ,
                        p.PLMGID
                      ,
                        p.NameRequest
                      ,
                        p.CreatedBy
                      ,
                        p.Date
                      ,
                        p.StatusRequest
                      ,
                        p.Initiated
                      ,
                        p.FTEAvaible

                    }

            });


            return Content(o["item"].ToString());

        }

       [Route("getDraftByID")]
        public ActionResult   getDraftByID(string data)
        {
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int pRequestID = Convert.ToInt32(objParameter["pID"].ToString());

            HMT2Entities DAO = new HMT2Entities();

            string userSOEID;
            userSOEID = GlobalUtil.GetSOEID();


            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblNM_Request
                    where p.ID == pRequestID
                    select new
                    {
                        p.ID,
                        p.Name,
                        p.Description
                    }

            });

            return Content(o["item"].ToString());

        }

       [Route("deleteMigrationList")]
        public ActionResult   deleteMigrationList(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int pRequestID = Convert.ToInt32(objParameter["pRequestID"].ToString());

            tblNM_Migration request = (from p in DAO.tblNM_Migration where p.ID == pRequestID select p).SingleOrDefault<tblNM_Migration>();

            List<tblNM_BudgetChargeOutGOC> gocs = (from p in DAO.tblNM_BudgetChargeOutGOC where p.MigrationID == pRequestID select p).ToList<tblNM_BudgetChargeOutGOC>();

            foreach (tblNM_BudgetChargeOutGOC G in gocs)
            {
                DAO.tblNM_BudgetChargeOutGOC.Remove(G);
                DAO.SaveChanges();
            }

            DAO.tblNM_Migration.Remove(request);

            DAO.SaveChanges();

            return Content("Complete");
        }

       [Route("getDiscoveryData")]
        public ActionResult   getDiscoveryData(string data)
        {
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int pRequestID = Convert.ToInt32(objParameter["pID"].ToString());

            HMT2Entities DAO = new HMT2Entities();

            string userSOEID;
            userSOEID = GlobalUtil.GetSOEID();


            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWNM_CharterInfo
                    where p.ID == pRequestID
                    select new
                    {
                        p.ID,
                        p.RequestAutoID,
                        p.Name,
                        p.FTE,
                        p.Scope,
                        p.Benefits,
                        p.ManagedGeographyName,
                        p.TypeName,
                        p.ManagedSegmentName
                    }

            });

            return Content(o["item"].ToString());
        }

       [Route("getNonMOUType")]
        public ActionResult   getNonMOUType()
        {
            HMT2Entities DAO = new HMT2Entities();

            string userSOEID;
            userSOEID = GlobalUtil.GetSOEID();


            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblNM_Type
                    select new
                    {
                        p.ID,
                        p.Type
                    }

            });

            return Content(o["item"].ToString());
        }

       [Route("getRequisition")]
       public ActionResult getRequisition()
       {
           HMT2Entities DAO = new HMT2Entities();

           string userSOEID;
           userSOEID = GlobalUtil.GetSOEID();


           JObject o = JObject.FromObject(new
           {
               item =
                   from p in DAO.procNM_DropDownRequisition()
                   select new
                   {
                       p.Requisition
                   }

           });

           return Content(o["item"].ToString());
       }

       [Route("getWorkingType")]
        public ActionResult   getWorkingType()
        {
            HMT2Entities DAO = new HMT2Entities();

            string userSOEID;
            userSOEID = GlobalUtil.GetSOEID();


            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblNM_RoleWorkingTeam
                    select new
                    {
                        p.ID,
                        p.Role
                    }

            });

            return Content(o["item"].ToString());
        }

       [Route("getProbablyUser")]
        public ActionResult   getProbablyUser(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string partData = objParameter["data"].ToString();


            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWE_FROEmployee
                    where p.SOEID.Contains(partData) || p.FIRST_NAME.Contains(partData) || p.LAST_NAME.Contains(partData)
                    select new
                    {
                        p.SOEID,
                        p.NAME
                    }

            });

            return Content(o["item"].ToString());
        }

       [Route("insertWorkingTeam")]
        public ActionResult   insertWorkingTeam(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string pSOEID = objParameter["pSOEID"].ToString();
            string pWorking = objParameter["pWorkingTeam"].ToString();
            string pNonMOU = objParameter["pNonMOU"].ToString();

            DAO.procNM_insertWorkingTeam(pSOEID, pWorking, pNonMOU);

            return Content(null);
        }

       [Route("getWorkingTeamList")]
        public ActionResult   getWorkingTeamList(string data)
        {

            HMT2Entities DAO = new HMT2Entities();
             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string pNonMOU = objParameter["pNonMOU"].ToString();

            tblNM_Migration migration = (from p in DAO.tblNM_Migration where p.RequestAutoID == pNonMOU select p).FirstOrDefault<tblNM_Migration>();


            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWNM_WorkingTeam
                    where p.MigrationID == migration.ID
                    select new
                    {
                        p.ID,
                        p.Role,
                        p.NAME
                    }

            });

            return Content(o["item"].ToString());
        }

       [Route("deleteWorkingTeam")]
        public ActionResult   deleteWorkingTeam(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int pID = Convert.ToInt32(objParameter["pID"].ToString());

            tblNM_WorkingTeam workingTeam = (from p in DAO.tblNM_WorkingTeam where p.ID == pID select p).SingleOrDefault<tblNM_WorkingTeam>();

            DAO.tblNM_WorkingTeam.Remove(workingTeam);

            DAO.SaveChanges();

            return Content("Complete");
        }

       [Route("getMilestones")]
        public ActionResult   getMilestones(string data)
        {

            HMT2Entities DAO = new HMT2Entities();
             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string pNonMOU = objParameter["pNonMOU"].ToString();

            tblNM_Migration migration = (from p in DAO.tblNM_Migration where p.RequestAutoID == pNonMOU select p).FirstOrDefault<tblNM_Migration>();


            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblNM_MigrationMilestone
                    where p.MigrationID == migration.ID
                    orderby p.tblNM_Milestones.StepOrder
                    select new
                    {
                        p.ID,
                        p.Enable,
                        p.tblNM_Milestones.Milestone,
                        p.StartDate,
                        p.EndDate,
                        p.Comments
                    }

            });

            return Content(o["item"].ToString());
        }

       [Route("getPhysicalLocation")]
        public ActionResult   getPhysicalLocation()
        {
            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblR_PhysicalLocation orderby p.Name 
                    select new
                    {
                        p.ID,
                        p.Name
                    }

            });

            return Content(o["item"].ToString());
        }

       [Route("getProbablyGOC")]
        public ActionResult   getProbablyGOC(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string partData = objParameter["data"].ToString();


            JObject o = JObject.FromObject(new
            {
                item =
                    from c in
                        (from p in DAO.VWNM_AllGoc
                         where p.GOCID.Contains(partData) || p.GOCName.Contains(partData)
                         select p).Take(15)
                    select new
                    {
                        c.GOCID,
                        c.GOCName
                    }

            });

            return Content(o["item"].ToString());
        }

       [Route("getFinancialGOC")]
        public ActionResult   getFinancialGOC(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string type = objParameter["GOCTYPE"].ToString();
            string NonMOU = objParameter["NonMOU"].ToString();

            tblNM_FinancialTypeGOC TYPE = (from p in DAO.tblNM_FinancialTypeGOC where p.Name == type select p).SingleOrDefault<tblNM_FinancialTypeGOC>();
            tblNM_Migration migration = (from p in DAO.tblNM_Migration where p.RequestAutoID == NonMOU select p).FirstOrDefault<tblNM_Migration>();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWNM_FinancialGOC
                    where p.MigrationID == migration.ID & p.TypeGOC == TYPE.ID
                    select new
                    {
                        p.ID
                      ,
                        p.MigrationID
                      ,
                        p.TypeGOC
                      ,
                        p.GOC
                      ,
                        p.Market
                      ,
                        p.Region
                      ,
                        p.MngdGeography
                      ,
                        p.Customer
                      ,
                        p.MngdSegment
                      ,
                        p.LegalVehicule
                      ,
                        p.Expr1
                      ,
                        p.GOCName
                    }

            });

            return Content(o["item"].ToString());
        }


        //[WebMethod(EnableSession = true)]
        //public ActionResult   deleteMigrationList(string data)
        //{
        //    HMT2Entities DAO = new HMT2Entities();

        //    var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        //     var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

        //    int pID = Convert.ToInt32(objParameter["pID"].ToString());

        //    tblNM_FinancialGoc GOC = (from p in DAO.tblNM_FinancialGoc where p.ID == pID select p).SingleOrDefault<tblNM_FinancialGoc>();


        //    DAO.tblNM_FinancialGoc.Remove(GOC);
        //    DAO.SaveChanges();

        //    return Content( "Complete";
        //}

       [Route("deleteFinancialGocList")]
        public ActionResult   deleteFinancialGocList(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int pID = Convert.ToInt32(objParameter["pID"].ToString());

            tblNM_FinancialGoc GOC = (from p in DAO.tblNM_FinancialGoc where p.ID == pID select p).SingleOrDefault<tblNM_FinancialGoc>();


            DAO.tblNM_FinancialGoc.Remove(GOC);
            DAO.SaveChanges();

            return Content("Complete");
        }

       [Route("saveFinancialGOC")]
        public ActionResult   saveFinancialGOC(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string type = objParameter["GOCTYPE"].ToString();
            string NonMOU = objParameter["NonMOU"].ToString();

            string GOC = objParameter["GOC"].ToString();

            Decimal Market = Convert.ToDecimal(objParameter["Market"].ToString());

            tblNM_FinancialTypeGOC TYPE = (from p in DAO.tblNM_FinancialTypeGOC where p.Name == type select p).SingleOrDefault<tblNM_FinancialTypeGOC>();
            tblNM_Migration migration = (from p in DAO.tblNM_Migration where p.RequestAutoID == NonMOU select p).FirstOrDefault<tblNM_Migration>();

            DAO.procNM_insertFinancialGOC(TYPE.ID, migration.ID, GOC, Market);

            return Content("CONTINUE");
        }

       [Route("getFinancialDetail")]
        public ActionResult   getFinancialDetail(string data)
        {
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            HMT2Entities DAO = new HMT2Entities();

            string NonMOU = objParameter["NonMOU"].ToString();
            tblNM_Migration migration = (from p in DAO.tblNM_Migration where p.RequestAutoID == NonMOU select p).FirstOrDefault<tblNM_Migration>();

            string userSOEID;
            userSOEID = GlobalUtil.GetSOEID();


            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblNM_FinancialDetail
                    where p.MigrationID == migration.ID
                    select new
                    {
                        p.ID,
                        p.tblNM_ReceiveSideData.Name,
                        p.Year,
                        p.JAN,
                        p.FEB,
                        p.MAR,
                        p.APR,
                        p.MAY,
                        p.JUN,
                        p.JUL,
                        p.AUG,
                        p.SEP,
                        p.OCT,
                        p.NOV,
                        p.DEC
                    }

            });

            return Content(o["item"].ToString());
        }

       [Route("getApprovalType")]
        public ActionResult   getApprovalType()
        {
            HMT2Entities DAO = new HMT2Entities();

            string userSOEID;
            userSOEID = GlobalUtil.GetSOEID();


            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblNM_RoleApproval
                    select new
                    {
                        p.ID,
                        p.Role
                    }

            });

            return Content(o["item"].ToString());
        }

       [Route("saveApprovalOrder")]
        public ActionResult   saveApprovalOrder(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string type = objParameter["TYPE"].ToString();
            string NonMOU = objParameter["NonMOU"].ToString();
            string SOEID = objParameter["SOEID"].ToString();
            int order = Convert.ToInt32(objParameter["ORDER"].ToString());

            tblNM_RoleApproval ApprovalType = (from p in DAO.tblNM_RoleApproval where p.Role == type select p).SingleOrDefault<tblNM_RoleApproval>();

            tblNM_Migration migration = (from p in DAO.tblNM_Migration where p.RequestAutoID == NonMOU select p).FirstOrDefault<tblNM_Migration>();

            DAO.procNM_addApprovalOrder(ApprovalType.ID, migration.ID, SOEID, order);

            return Content("CONTINUE");
        }

       [Route("getApprovalListOrder")]
        public ActionResult   getApprovalListOrder(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string NonMOU = objParameter["NonMOU"].ToString();

            tblNM_Migration migration = (from p in DAO.tblNM_Migration where p.RequestAutoID == NonMOU select p).FirstOrDefault<tblNM_Migration>();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWNM_ApprovalList
                    where p.MigrationID == migration.ID
                    select new
                    {
                        p.ID
                      ,
                        p.ApproveOrder
                      ,
                        p.Role
                      ,
                        p.NAME

                    }

            });

            return Content(o["item"].ToString());
        }

       [Route("deleteApprovalListOrder")]
        public ActionResult   deleteApprovalListOrder(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int pID = Convert.ToInt32(objParameter["pID"].ToString());

            tblNM_Approval Approval = (from p in DAO.tblNM_Approval where p.ID == pID select p).SingleOrDefault<tblNM_Approval>();


            DAO.tblNM_Approval.Remove(Approval);
            DAO.SaveChanges();

            return Content("Complete");
        }

       [Route("getAttachmentList")]
        public ActionResult   getAttachmentList(string data)
        {
            try
            {
                HMT2Entities DAO = new HMT2Entities();

                 var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string NonMOU = objParameter["NonMOU"].ToString();

                tblNM_Migration migration = (from p in DAO.tblNM_Migration where p.RequestAutoID == NonMOU select p).FirstOrDefault<tblNM_Migration>();

                JObject o = JObject.FromObject(new
                {
                    item =
                        from p in DAO.tblNM_Attachment
                        where p.MigrationID == migration.ID
                        select new
                        {
                            p.ID
                          ,
                            p.Path
                        }

                });

                return Content( o["item"].ToString());
            }
            catch { return Content(null); };
        }

       [Route("deleteAttachment")]
        public ActionResult   deleteAttachment(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int pID = Convert.ToInt32(objParameter["pID"].ToString());

            tblNM_Attachment attach = (from p in DAO.tblNM_Attachment where p.ID == pID select p).SingleOrDefault<tblNM_Attachment>();

            string _pathToSave = AppDomain.CurrentDomain.BaseDirectory.ToString() + attach.Path;

            //File.Delete(_pathToSave);

            DAO.tblNM_Attachment.Remove(attach);
            DAO.SaveChanges();

            return Content("Complete");
        }

       [Route("getAttachmentData")]
        public ActionResult   getAttachmentData(string data)
        {
            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int pRequestID = Convert.ToInt32(objParameter["pID"].ToString());

            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblNM_Attachment
                    where p.ID == pRequestID
                    select new
                    {
                        p.Path
                    }

            });

            return Content(o["item"].ToString());
        }


       [Route("saveNonMOU")]
        public ActionResult   saveNonMOU(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            //Charter Info
            string txtNonMOUID = objParameter["txtNonMOUID"].ToString();

            tblNM_Migration migration = (from p in DAO.tblNM_Migration where p.RequestAutoID == txtNonMOUID select p).FirstOrDefault<tblNM_Migration>();
           
            decimal txtFTEDiscovery = Convert.ToDecimal(objParameter["txtFTEDiscovery"].ToString());
            string txtNameDiscovery = objParameter["txtNameDiscovery"].ToString();
            string jqxDropDownMOUType = objParameter["jqxDropDownMOUType"].ToString();

            int MOUTypeID = 0;
            tblNM_Type MOUType = (from p in DAO.tblNM_Type where p.Type == jqxDropDownMOUType select p).SingleOrDefault<tblNM_Type>();
            try
            {
                MOUTypeID = MOUType.ID;
            }
            catch
            {
            }


            string ScopeDiscovery = objParameter["ScopeDiscovery"].ToString();
            string BenefitsDiscovery = objParameter["BenefitsDiscovery"].ToString();
            //Migration Info

            DateTime dateInitiated = Convert.ToDateTime(objParameter["dateInitiated"].ToString());

            DateTime dateAvaible = Convert.ToDateTime(objParameter["dateAvaible"].ToString());
            //string jqxGridMilestones = objParameter["jqxGridMilestones"].ToString();
            //  DataTable dtMilestones = (DataTable)JsonConvert.DeserializeObject(jqxGridMilestones, (typeof(DataTable)));
            //Financials
            decimal receivingFTE = Convert.ToDecimal(objParameter["receivingFTE"].ToString());
            string cbPhysicalLocation = objParameter["cbPhysicalLocation"].ToString();

            int PhysicalLocID = 0;
            tblR_PhysicalLocation PhysicalLoc = (from p in DAO.tblR_PhysicalLocation where p.Name == cbPhysicalLocation select p).SingleOrDefault<tblR_PhysicalLocation>();
            try
            {
                PhysicalLocID = PhysicalLoc.ID;
            }
            catch
            {
            }
            string jqxSiteManager = objParameter["jqxSiteManager"].ToString();
            string jqxSiteFinanceManger = objParameter["jqxSiteFinanceManger"].ToString();
            string MSLVL09 = objParameter["MSLVL09"].ToString();
            decimal dsTotal = Convert.ToDecimal(objParameter["dsTotal"].ToString().Replace("$ ", ""));
            decimal lblDSJAN = Convert.ToDecimal(objParameter["lblDSJAN"].ToString().Replace("$ ", ""));
            decimal lblDSFEB = Convert.ToDecimal(objParameter["lblDSFEB"].ToString().Replace("$ ", ""));
            decimal lblDSMAR = Convert.ToDecimal(objParameter["lblDSMAR"].ToString().Replace("$ ", ""));
            decimal lblDSAPR = Convert.ToDecimal(objParameter["lblDSAPR"].ToString().Replace("$ ", ""));
            decimal lblDSMAY = Convert.ToDecimal(objParameter["lblDSMAY"].ToString().Replace("$ ", ""));
            decimal lblDSJUN = Convert.ToDecimal(objParameter["lblDSJUN"].ToString().Replace("$ ", ""));
            decimal lblDSJUL = Convert.ToDecimal(objParameter["lblDSJUL"].ToString().Replace("$ ", ""));
            decimal lblDSAUG = Convert.ToDecimal(objParameter["lblDSAUG"].ToString().Replace("$ ", ""));
            decimal lblDSSEP = Convert.ToDecimal(objParameter["lblDSSEP"].ToString().Replace("$ ", ""));
            decimal lblDSOCT = Convert.ToDecimal(objParameter["lblDSOCT"].ToString().Replace("$ ", ""));
            decimal lblDSNOV = Convert.ToDecimal(objParameter["lblDSNOV"].ToString().Replace("$ ", ""));
            decimal lblDSDEC = Convert.ToDecimal(objParameter["lblDSDEC"].ToString().Replace("$ ", ""));
            decimal lblDSTotal = Convert.ToDecimal(objParameter["lblDSTotal"].ToString().Replace("$ ", ""));
            decimal osTotal = Convert.ToDecimal(objParameter["osTotal"].ToString().Replace("$ ", ""));
            decimal lblOSJAN = Convert.ToDecimal(objParameter["lblOSJAN"].ToString().Replace("$ ", ""));
            decimal lblOSFEB = Convert.ToDecimal(objParameter["lblOSFEB"].ToString().Replace("$ ", ""));
            decimal lblOSMAR = Convert.ToDecimal(objParameter["lblOSMAR"].ToString().Replace("$ ", ""));
            decimal lblOSAPR = Convert.ToDecimal(objParameter["lblOSAPR"].ToString().Replace("$ ", ""));
            decimal lblOSMAY = Convert.ToDecimal(objParameter["lblOSMAY"].ToString().Replace("$ ", ""));
            decimal lblOSJUN = Convert.ToDecimal(objParameter["lblOSJUN"].ToString().Replace("$ ", ""));
            decimal lblOSJUL = Convert.ToDecimal(objParameter["lblOSJUL"].ToString().Replace("$ ", ""));
            decimal lblOSAUG = Convert.ToDecimal(objParameter["lblOSAUG"].ToString().Replace("$ ", ""));
            decimal lblOSSEP = Convert.ToDecimal(objParameter["lblOSSEP"].ToString().Replace("$ ", ""));
            decimal lblOSOCT = Convert.ToDecimal(objParameter["lblOSOCT"].ToString().Replace("$ ", ""));
            decimal lblOSNOV = Convert.ToDecimal(objParameter["lblOSNOV"].ToString().Replace("$ ", ""));
            decimal lblOSDEC = Convert.ToDecimal(objParameter["lblOSDEC"].ToString().Replace("$ ", ""));
            decimal lblOSTOTAL = Convert.ToDecimal(objParameter["lblOSTOTAL"].ToString().Replace("$ ", ""));
            decimal fteJAN = Convert.ToDecimal(objParameter["fteJAN"].ToString().Replace("$ ", ""));
            decimal fteFEB = Convert.ToDecimal(objParameter["fteFEB"].ToString().Replace("$ ", ""));
            decimal fteMAR = Convert.ToDecimal(objParameter["fteMAR"].ToString().Replace("$ ", ""));
            decimal fteAPR = Convert.ToDecimal(objParameter["fteAPR"].ToString().Replace("$ ", ""));
            decimal fteMAY = Convert.ToDecimal(objParameter["fteMAY"].ToString().Replace("$ ", ""));
            decimal fteJUN = Convert.ToDecimal(objParameter["fteJUN"].ToString().Replace("$ ", ""));
            decimal fteJUL = Convert.ToDecimal(objParameter["fteJUL"].ToString().Replace("$ ", ""));
            decimal fteAUG = Convert.ToDecimal(objParameter["fteAUG"].ToString().Replace("$ ", ""));
            decimal fteSEP = Convert.ToDecimal(objParameter["fteSEP"].ToString().Replace("$ ", ""));
            decimal fteOCT = Convert.ToDecimal(objParameter["fteOCT"].ToString().Replace("$ ", ""));
            decimal fteNOV = Convert.ToDecimal(objParameter["fteNOV"].ToString().Replace("$ ", ""));
            decimal fteDEC = Convert.ToDecimal(objParameter["fteDEC"].ToString().Replace("$ ", ""));
            decimal fteTOTAL = Convert.ToDecimal(objParameter["fteTOTAL"].ToString().Replace("$ ", ""));
            //decimal trainCostTotal = Convert.ToDecimal(objParameter["trainCostTotal"].ToString().Replace("$ ",""));
            //decimal tecCostTOTAL = Convert.ToDecimal(objParameter["tecCostTOTAL"].ToString().Replace("$ ",""));
            decimal FTETotalCostJAN = Convert.ToDecimal(objParameter["FTETotalCostJAN"].ToString().Replace("$ ", ""));
            decimal FTETotalCostFEB = Convert.ToDecimal(objParameter["FTETotalCostFEB"].ToString().Replace("$ ", ""));
            decimal FTETotalCostMAR = Convert.ToDecimal(objParameter["FTETotalCostMAR"].ToString().Replace("$ ", ""));
            decimal FTETotalCostAPR = Convert.ToDecimal(objParameter["FTETotalCostAPR"].ToString().Replace("$ ", ""));
            decimal FTETotalCostMAY = Convert.ToDecimal(objParameter["FTETotalCostMAY"].ToString().Replace("$ ", ""));
            decimal FTETotalCostJUN = Convert.ToDecimal(objParameter["FTETotalCostJUN"].ToString().Replace("$ ", ""));
            decimal FTETotalCostJUL = Convert.ToDecimal(objParameter["FTETotalCostJUL"].ToString().Replace("$ ", ""));
            decimal FTETotalCostAUG = Convert.ToDecimal(objParameter["FTETotalCostAUG"].ToString().Replace("$ ", ""));
            decimal FTETotalCostSEP = Convert.ToDecimal(objParameter["FTETotalCostSEP"].ToString().Replace("$ ", ""));
            decimal FTETotalCostOCT = Convert.ToDecimal(objParameter["FTETotalCostOCT"].ToString().Replace("$ ", ""));
            decimal FTETotalCostNOV = Convert.ToDecimal(objParameter["FTETotalCostNOV"].ToString().Replace("$ ", ""));
            decimal FTETotalCostDEC = Convert.ToDecimal(objParameter["FTETotalCostDEC"].ToString().Replace("$ ", ""));
            decimal FTETotalCostTOTAL = Convert.ToDecimal(objParameter["FTETotalCostTOTAL"].ToString().Replace("$ ", ""));
            //decimal totalCostJAN = Convert.ToDecimal(objParameter["totalCostJAN"].ToString().Replace("$ ",""));
            //decimal totalCostFEB = Convert.ToDecimal(objParameter["totalCostFEB"].ToString().Replace("$ ",""));
            //decimal totalCostMAR = Convert.ToDecimal(objParameter["totalCostMAR"].ToString().Replace("$ ",""));
            //decimal totalCostAPR = Convert.ToDecimal(objParameter["totalCostAPR"].ToString().Replace("$ ",""));
            //decimal totalCostMAY = Convert.ToDecimal(objParameter["totalCostMAY"].ToString().Replace("$ ",""));
            //decimal totalCostJUN = Convert.ToDecimal(objParameter["totalCostJUN"].ToString().Replace("$ ",""));
            //decimal totalCostJUL = Convert.ToDecimal(objParameter["totalCostJUL"].ToString().Replace("$ ",""));
            //decimal totalCostAUG = Convert.ToDecimal(objParameter["totalCostAUG"].ToString().Replace("$ ",""));
            //decimal totalCostSEP = Convert.ToDecimal(objParameter["totalCostSEP"].ToString().Replace("$ ",""));
            //decimal totalCostOCT = Convert.ToDecimal(objParameter["totalCostOCT"].ToString().Replace("$ ",""));
            //decimal totalCostNOV = Convert.ToDecimal(objParameter["totalCostNOV"].ToString().Replace("$ ",""));
            //decimal totalCostDEC = Convert.ToDecimal(objParameter["totalCostDEC"].ToString().Replace("$ ",""));
            //decimal totalCostTOTAL = Convert.ToDecimal(objParameter["totalCostTOTAL"].ToString().Replace("$ ",""));
            decimal dsTotal2 = Convert.ToDecimal(objParameter["dsTotal2"].ToString().Replace("$ ", ""));
            decimal lblDSJAN2 = Convert.ToDecimal(objParameter["lblDSJAN2"].ToString().Replace("$ ", ""));
            decimal lblDSFEB2 = Convert.ToDecimal(objParameter["lblDSFEB2"].ToString().Replace("$ ", ""));
            decimal lblDSMAR2 = Convert.ToDecimal(objParameter["lblDSMAR2"].ToString().Replace("$ ", ""));
            decimal lblDSAPR2 = Convert.ToDecimal(objParameter["lblDSAPR2"].ToString().Replace("$ ", ""));
            decimal lblDSMAY2 = Convert.ToDecimal(objParameter["lblDSMAY2"].ToString().Replace("$ ", ""));
            decimal lblDSJUN2 = Convert.ToDecimal(objParameter["lblDSJUN2"].ToString().Replace("$ ", ""));
            decimal lblDSJUL2 = Convert.ToDecimal(objParameter["lblDSJUL2"].ToString().Replace("$ ", ""));
            decimal lblDSAUG2 = Convert.ToDecimal(objParameter["lblDSAUG2"].ToString().Replace("$ ", ""));
            decimal lblDSSEP2 = Convert.ToDecimal(objParameter["lblDSSEP2"].ToString().Replace("$ ", ""));
            decimal lblDSOCT2 = Convert.ToDecimal(objParameter["lblDSOCT2"].ToString().Replace("$ ", ""));
            decimal lblDSNOV2 = Convert.ToDecimal(objParameter["lblDSNOV2"].ToString().Replace("$ ", ""));
            decimal lblDSDEC2 = Convert.ToDecimal(objParameter["lblDSDEC2"].ToString().Replace("$ ", ""));
            decimal lblDSTotal2 = Convert.ToDecimal(objParameter["lblDSTotal2"].ToString().Replace("$ ", ""));
            decimal osTotal2 = Convert.ToDecimal(objParameter["osTotal2"].ToString().Replace("$ ", ""));
            decimal lblOSJAN2 = Convert.ToDecimal(objParameter["lblOSJAN2"].ToString().Replace("$ ", ""));
            decimal lblOSFEB2 = Convert.ToDecimal(objParameter["lblOSFEB2"].ToString().Replace("$ ", ""));
            decimal lblOSMAR2 = Convert.ToDecimal(objParameter["lblOSMAR2"].ToString().Replace("$ ", ""));
            decimal lblOSAPR2 = Convert.ToDecimal(objParameter["lblOSAPR2"].ToString().Replace("$ ", ""));
            decimal lblOSMAY2 = Convert.ToDecimal(objParameter["lblOSMAY2"].ToString().Replace("$ ", ""));
            decimal lblOSJUN2 = Convert.ToDecimal(objParameter["lblOSJUN2"].ToString().Replace("$ ", ""));
            decimal lblOSJUL2 = Convert.ToDecimal(objParameter["lblOSJUL2"].ToString().Replace("$ ", ""));
            decimal lblOSAUG2 = Convert.ToDecimal(objParameter["lblOSAUG2"].ToString().Replace("$ ", ""));
            decimal lblOSSEP2 = Convert.ToDecimal(objParameter["lblOSSEP2"].ToString().Replace("$ ", ""));
            decimal lblOSOCT2 = Convert.ToDecimal(objParameter["lblOSOCT2"].ToString().Replace("$ ", ""));
            decimal lblOSNOV2 = Convert.ToDecimal(objParameter["lblOSNOV2"].ToString().Replace("$ ", ""));
            decimal lblOSDEC2 = Convert.ToDecimal(objParameter["lblOSDEC2"].ToString().Replace("$ ", ""));
            decimal lblOSTOTAL2 = Convert.ToDecimal(objParameter["lblOSTOTAL2"].ToString().Replace("$ ", ""));
            decimal fteJAN2 = Convert.ToDecimal(objParameter["fteJAN2"].ToString().Replace("$ ", ""));
            decimal fteFEB2 = Convert.ToDecimal(objParameter["fteFEB2"].ToString().Replace("$ ", ""));
            decimal fteMAR2 = Convert.ToDecimal(objParameter["fteMAR2"].ToString().Replace("$ ", ""));
            decimal fteAPR2 = Convert.ToDecimal(objParameter["fteAPR2"].ToString().Replace("$ ", ""));
            decimal fteMAY2 = Convert.ToDecimal(objParameter["fteMAY2"].ToString().Replace("$ ", ""));
            decimal fteJUN2 = Convert.ToDecimal(objParameter["fteJUN2"].ToString().Replace("$ ", ""));
            decimal fteJUL2 = Convert.ToDecimal(objParameter["fteJUL2"].ToString().Replace("$ ", ""));
            decimal fteAUG2 = Convert.ToDecimal(objParameter["fteAUG2"].ToString().Replace("$ ", ""));
            decimal fteSEP2 = Convert.ToDecimal(objParameter["fteSEP2"].ToString().Replace("$ ", ""));
            decimal fteOCT2 = Convert.ToDecimal(objParameter["fteOCT2"].ToString().Replace("$ ", ""));
            decimal fteNOV2 = Convert.ToDecimal(objParameter["fteNOV2"].ToString().Replace("$ ", ""));
            decimal fteDEC2 = Convert.ToDecimal(objParameter["fteDEC2"].ToString().Replace("$ ", ""));
            decimal fteTOTAL2 = Convert.ToDecimal(objParameter["fteTOTAL2"].ToString().Replace("$ ", ""));
            //decimal trainCostTotal2 = Convert.ToDecimal(objParameter["trainCostTotal2"].ToString().Replace("$ ",""));
            //decimal tecCostTOTAL2 = Convert.ToDecimal(objParameter["tecCostTOTAL2"].ToString().Replace("$ ",""));
            decimal FTETotalCostJAN2 = Convert.ToDecimal(objParameter["FTETotalCostJAN2"].ToString().Replace("$ ", ""));
            decimal FTETotalCostFEB2 = Convert.ToDecimal(objParameter["FTETotalCostFEB2"].ToString().Replace("$ ", ""));
            decimal FTETotalCostMAR2 = Convert.ToDecimal(objParameter["FTETotalCostMAR2"].ToString().Replace("$ ", ""));
            decimal FTETotalCostAPR2 = Convert.ToDecimal(objParameter["FTETotalCostAPR2"].ToString().Replace("$ ", ""));
            decimal FTETotalCostMAY2 = Convert.ToDecimal(objParameter["FTETotalCostMAY2"].ToString().Replace("$ ", ""));
            decimal FTETotalCostJUN2 = Convert.ToDecimal(objParameter["FTETotalCostJUN2"].ToString().Replace("$ ", ""));
            decimal FTETotalCostJUL2 = Convert.ToDecimal(objParameter["FTETotalCostJUL2"].ToString().Replace("$ ", ""));
            decimal FTETotalCostAUG2 = Convert.ToDecimal(objParameter["FTETotalCostAUG2"].ToString().Replace("$ ", ""));
            decimal FTETotalCostSEP2 = Convert.ToDecimal(objParameter["FTETotalCostSEP2"].ToString().Replace("$ ", ""));
            decimal FTETotalCostOCT2 = Convert.ToDecimal(objParameter["FTETotalCostOCT2"].ToString().Replace("$ ", ""));
            decimal FTETotalCostNOV2 = Convert.ToDecimal(objParameter["FTETotalCostNOV2"].ToString().Replace("$ ", ""));
            decimal FTETotalCostDEC2 = Convert.ToDecimal(objParameter["FTETotalCostDEC2"].ToString().Replace("$ ", ""));
            decimal FTETotalCostTOTAL2 = Convert.ToDecimal(objParameter["FTETotalCostTOTAL2"].ToString().Replace("$ ", ""));
            //decimal totalCostJAN2 = Convert.ToDecimal(objParameter["totalCostJAN2"].ToString().Replace("$ ",""));
            //decimal totalCostFEB2 = Convert.ToDecimal(objParameter["totalCostFEB2"].ToString().Replace("$ ",""));
            //decimal totalCostMAR2 = Convert.ToDecimal(objParameter["totalCostMAR2"].ToString().Replace("$ ",""));
            //decimal totalCostAPR2 = Convert.ToDecimal(objParameter["totalCostAPR2"].ToString().Replace("$ ",""));
            //decimal totalCostMAY2 = Convert.ToDecimal(objParameter["totalCostMAY2"].ToString().Replace("$ ",""));
            //decimal totalCostJUN2 = Convert.ToDecimal(objParameter["totalCostJUN2"].ToString().Replace("$ ",""));
            //decimal totalCostJUL2 = Convert.ToDecimal(objParameter["totalCostJUL2"].ToString().Replace("$ ",""));
            //decimal totalCostAUG2 = Convert.ToDecimal(objParameter["totalCostAUG2"].ToString().Replace("$ ",""));
            //decimal totalCostSEP2 = Convert.ToDecimal(objParameter["totalCostSEP2"].ToString().Replace("$ ",""));
            //decimal totalCostOCT2 = Convert.ToDecimal(objParameter["totalCostOCT2"].ToString().Replace("$ ",""));
            //decimal totalCostNOV2 = Convert.ToDecimal(objParameter["totalCostNOV2"].ToString().Replace("$ ",""));
            //decimal totalCostDEC2 = Convert.ToDecimal(objParameter["totalCostDEC2"].ToString().Replace("$ ",""));
            //decimal totalCostTOTAL2 = Convert.ToDecimal(objParameter["totalCostTOTAL2"].ToString().Replace("$ ",""));

            decimal dsTotal3 = Convert.ToDecimal(objParameter["dsTotal3"].ToString().Replace("$ ", ""));
            decimal lblDSJAN3 = Convert.ToDecimal(objParameter["lblDSJAN3"].ToString().Replace("$ ", ""));
            decimal lblDSFEB3 = Convert.ToDecimal(objParameter["lblDSFEB3"].ToString().Replace("$ ", ""));
            decimal lblDSMAR3 = Convert.ToDecimal(objParameter["lblDSMAR3"].ToString().Replace("$ ", ""));
            decimal lblDSAPR3 = Convert.ToDecimal(objParameter["lblDSAPR3"].ToString().Replace("$ ", ""));
            decimal lblDSMAY3 = Convert.ToDecimal(objParameter["lblDSMAY3"].ToString().Replace("$ ", ""));
            decimal lblDSJUN3 = Convert.ToDecimal(objParameter["lblDSJUN3"].ToString().Replace("$ ", ""));
            decimal lblDSJUL3 = Convert.ToDecimal(objParameter["lblDSJUL3"].ToString().Replace("$ ", ""));
            decimal lblDSAUG3 = Convert.ToDecimal(objParameter["lblDSAUG3"].ToString().Replace("$ ", ""));
            decimal lblDSSEP3 = Convert.ToDecimal(objParameter["lblDSSEP3"].ToString().Replace("$ ", ""));
            decimal lblDSOCT3 = Convert.ToDecimal(objParameter["lblDSOCT3"].ToString().Replace("$ ", ""));
            decimal lblDSNOV3 = Convert.ToDecimal(objParameter["lblDSNOV3"].ToString().Replace("$ ", ""));
            decimal lblDSDEC3 = Convert.ToDecimal(objParameter["lblDSDEC3"].ToString().Replace("$ ", ""));
            decimal lblDSTotal3 = Convert.ToDecimal(objParameter["lblDSTotal3"].ToString().Replace("$ ", ""));
            decimal osTotal3 = Convert.ToDecimal(objParameter["osTotal3"].ToString().Replace("$ ", ""));
            decimal lblOSJAN3 = Convert.ToDecimal(objParameter["lblOSJAN3"].ToString().Replace("$ ", ""));
            decimal lblOSFEB3 = Convert.ToDecimal(objParameter["lblOSFEB3"].ToString().Replace("$ ", ""));
            decimal lblOSMAR3 = Convert.ToDecimal(objParameter["lblOSMAR3"].ToString().Replace("$ ", ""));
            decimal lblOSAPR3 = Convert.ToDecimal(objParameter["lblOSAPR3"].ToString().Replace("$ ", ""));
            decimal lblOSMAY3 = Convert.ToDecimal(objParameter["lblOSMAY3"].ToString().Replace("$ ", ""));
            decimal lblOSJUN3 = Convert.ToDecimal(objParameter["lblOSJUN3"].ToString().Replace("$ ", ""));
            decimal lblOSJUL3 = Convert.ToDecimal(objParameter["lblOSJUL3"].ToString().Replace("$ ", ""));
            decimal lblOSAUG3 = Convert.ToDecimal(objParameter["lblOSAUG3"].ToString().Replace("$ ", ""));
            decimal lblOSSEP3 = Convert.ToDecimal(objParameter["lblOSSEP3"].ToString().Replace("$ ", ""));
            decimal lblOSOCT3 = Convert.ToDecimal(objParameter["lblOSOCT3"].ToString().Replace("$ ", ""));
            decimal lblOSNOV3 = Convert.ToDecimal(objParameter["lblOSNOV3"].ToString().Replace("$ ", ""));
            decimal lblOSDEC3 = Convert.ToDecimal(objParameter["lblOSDEC3"].ToString().Replace("$ ", ""));
            decimal lblOSTOTAL3 = Convert.ToDecimal(objParameter["lblOSTOTAL3"].ToString().Replace("$ ", ""));
            decimal fteJAN3 = Convert.ToDecimal(objParameter["fteJAN3"].ToString().Replace("$ ", ""));
            decimal fteFEB3 = Convert.ToDecimal(objParameter["fteFEB3"].ToString().Replace("$ ", ""));
            decimal fteMAR3 = Convert.ToDecimal(objParameter["fteMAR3"].ToString().Replace("$ ", ""));
            decimal fteAPR3 = Convert.ToDecimal(objParameter["fteAPR3"].ToString().Replace("$ ", ""));
            decimal fteMAY3 = Convert.ToDecimal(objParameter["fteMAY3"].ToString().Replace("$ ", ""));
            decimal fteJUN3 = Convert.ToDecimal(objParameter["fteJUN3"].ToString().Replace("$ ", ""));
            decimal fteJUL3 = Convert.ToDecimal(objParameter["fteJUL3"].ToString().Replace("$ ", ""));
            decimal fteAUG3 = Convert.ToDecimal(objParameter["fteAUG3"].ToString().Replace("$ ", ""));
            decimal fteSEP3 = Convert.ToDecimal(objParameter["fteSEP3"].ToString().Replace("$ ", ""));
            decimal fteOCT3 = Convert.ToDecimal(objParameter["fteOCT3"].ToString().Replace("$ ", ""));
            decimal fteNOV3 = Convert.ToDecimal(objParameter["fteNOV3"].ToString().Replace("$ ", ""));
            decimal fteDEC3 = Convert.ToDecimal(objParameter["fteDEC3"].ToString().Replace("$ ", ""));
            decimal fteTOTAL3 = Convert.ToDecimal(objParameter["fteTOTAL3"].ToString().Replace("$ ", ""));
            //decimal trainCostTotal3 = Convert.ToDecimal(objParameter["trainCostTotal3"].ToString().Replace("$ ",""));
            //decimal tecCostTOTAL3 = Convert.ToDecimal(objParameter["tecCostTOTAL3"].ToString().Replace("$ ",""));
            decimal FTETotalCostJAN3 = Convert.ToDecimal(objParameter["FTETotalCostJAN3"].ToString().Replace("$ ", ""));
            decimal FTETotalCostFEB3 = Convert.ToDecimal(objParameter["FTETotalCostFEB3"].ToString().Replace("$ ", ""));
            decimal FTETotalCostMAR3 = Convert.ToDecimal(objParameter["FTETotalCostMAR3"].ToString().Replace("$ ", ""));
            decimal FTETotalCostAPR3 = Convert.ToDecimal(objParameter["FTETotalCostAPR3"].ToString().Replace("$ ", ""));
            decimal FTETotalCostMAY3 = Convert.ToDecimal(objParameter["FTETotalCostMAY3"].ToString().Replace("$ ", ""));
            decimal FTETotalCostJUN3 = Convert.ToDecimal(objParameter["FTETotalCostJUN3"].ToString().Replace("$ ", ""));
            decimal FTETotalCostJUL3 = Convert.ToDecimal(objParameter["FTETotalCostJUL3"].ToString().Replace("$ ", ""));
            decimal FTETotalCostAUG3 = Convert.ToDecimal(objParameter["FTETotalCostAUG3"].ToString().Replace("$ ", ""));
            decimal FTETotalCostSEP3 = Convert.ToDecimal(objParameter["FTETotalCostSEP3"].ToString().Replace("$ ", ""));
            decimal FTETotalCostOCT3 = Convert.ToDecimal(objParameter["FTETotalCostOCT3"].ToString().Replace("$ ", ""));
            decimal FTETotalCostNOV3 = Convert.ToDecimal(objParameter["FTETotalCostNOV3"].ToString().Replace("$ ", ""));
            decimal FTETotalCostDEC3 = Convert.ToDecimal(objParameter["FTETotalCostDEC3"].ToString().Replace("$ ", ""));
            decimal FTETotalCostTOTAL3 = Convert.ToDecimal(objParameter["FTETotalCostTOTAL3"].ToString().Replace("$ ", ""));

            decimal dsJan = Convert.ToDecimal(objParameter["dsJan"].ToString().Replace("$ ", ""));
            decimal dsFeb = Convert.ToDecimal(objParameter["dsFeb"].ToString().Replace("$ ", ""));
            decimal dsMar = Convert.ToDecimal(objParameter["dsMar"].ToString().Replace("$ ", ""));
            decimal dsApr = Convert.ToDecimal(objParameter["dsApr"].ToString().Replace("$ ", ""));
            decimal dsMay = Convert.ToDecimal(objParameter["dsMay"].ToString().Replace("$ ", ""));
            decimal dsJun = Convert.ToDecimal(objParameter["dsJun"].ToString().Replace("$ ", ""));
            decimal dsJul = Convert.ToDecimal(objParameter["dsJul"].ToString().Replace("$ ", ""));
            decimal dsAug = Convert.ToDecimal(objParameter["dsAug"].ToString().Replace("$ ", ""));
            decimal dsSep = Convert.ToDecimal(objParameter["dsSep"].ToString().Replace("$ ", ""));
            decimal dsOct = Convert.ToDecimal(objParameter["dsOct"].ToString().Replace("$ ", ""));
            decimal dsNov = Convert.ToDecimal(objParameter["dsNov"].ToString().Replace("$ ", ""));
            decimal dsDec = Convert.ToDecimal(objParameter["dsDec"].ToString().Replace("$ ", ""));
            decimal osJAN = Convert.ToDecimal(objParameter["osJAN"].ToString().Replace("$ ", ""));
            decimal osFEB = Convert.ToDecimal(objParameter["osFEB"].ToString().Replace("$ ", ""));
            decimal osMAR = Convert.ToDecimal(objParameter["osMAR"].ToString().Replace("$ ", ""));
            decimal osAPR = Convert.ToDecimal(objParameter["osAPR"].ToString().Replace("$ ", ""));
            decimal osMAY = Convert.ToDecimal(objParameter["osMAY"].ToString().Replace("$ ", ""));
            decimal osJUN = Convert.ToDecimal(objParameter["osJUN"].ToString().Replace("$ ", ""));
            decimal osJUL = Convert.ToDecimal(objParameter["osJUL"].ToString().Replace("$ ", ""));
            decimal osAUG = Convert.ToDecimal(objParameter["osAUG"].ToString().Replace("$ ", ""));
            decimal osSEP = Convert.ToDecimal(objParameter["osSEP"].ToString().Replace("$ ", ""));
            decimal osOCT = Convert.ToDecimal(objParameter["osOCT"].ToString().Replace("$ ", ""));
            decimal osNOV = Convert.ToDecimal(objParameter["osNOV"].ToString().Replace("$ ", ""));
            decimal osDEC = Convert.ToDecimal(objParameter["osDEC"].ToString().Replace("$ ", ""));
            decimal dsJan2 = Convert.ToDecimal(objParameter["dsJan2"].ToString().Replace("$ ", ""));
            decimal dsFeb2 = Convert.ToDecimal(objParameter["dsFeb2"].ToString().Replace("$ ", ""));
            decimal dsMar2 = Convert.ToDecimal(objParameter["dsMar2"].ToString().Replace("$ ", ""));
            decimal dsApr2 = Convert.ToDecimal(objParameter["dsApr2"].ToString().Replace("$ ", ""));
            decimal dsMay2 = Convert.ToDecimal(objParameter["dsMay2"].ToString().Replace("$ ", ""));
            decimal dsJun2 = Convert.ToDecimal(objParameter["dsJun2"].ToString().Replace("$ ", ""));
            decimal dsJul2 = Convert.ToDecimal(objParameter["dsJul2"].ToString().Replace("$ ", ""));
            decimal dsAug2 = Convert.ToDecimal(objParameter["dsAug2"].ToString().Replace("$ ", ""));
            decimal dsSep2 = Convert.ToDecimal(objParameter["dsSep2"].ToString().Replace("$ ", ""));
            decimal dsOct2 = Convert.ToDecimal(objParameter["dsOct2"].ToString().Replace("$ ", ""));
            decimal dsNov2 = Convert.ToDecimal(objParameter["dsNov2"].ToString().Replace("$ ", ""));
            decimal dsDec2 = Convert.ToDecimal(objParameter["dsDec2"].ToString().Replace("$ ", ""));
            decimal osJAN2 = Convert.ToDecimal(objParameter["osJAN2"].ToString().Replace("$ ", ""));
            decimal osFEB2 = Convert.ToDecimal(objParameter["osFEB2"].ToString().Replace("$ ", ""));
            decimal osMAR2 = Convert.ToDecimal(objParameter["osMAR2"].ToString().Replace("$ ", ""));
            decimal osAPR2 = Convert.ToDecimal(objParameter["osAPR2"].ToString().Replace("$ ", ""));
            decimal osMAY2 = Convert.ToDecimal(objParameter["osMAY2"].ToString().Replace("$ ", ""));
            decimal osJUN2 = Convert.ToDecimal(objParameter["osJUN2"].ToString().Replace("$ ", ""));
            decimal osJUL2 = Convert.ToDecimal(objParameter["osJUL2"].ToString().Replace("$ ", ""));
            decimal osAUG2 = Convert.ToDecimal(objParameter["osAUG2"].ToString().Replace("$ ", ""));
            decimal osSEP2 = Convert.ToDecimal(objParameter["osSEP2"].ToString().Replace("$ ", ""));
            decimal osOCT2 = Convert.ToDecimal(objParameter["osOCT2"].ToString().Replace("$ ", ""));
            decimal osNOV2 = Convert.ToDecimal(objParameter["osNOV2"].ToString().Replace("$ ", ""));
            decimal osDEC2 = Convert.ToDecimal(objParameter["osDEC2"].ToString().Replace("$ ", ""));

            decimal dsJan3 = Convert.ToDecimal(objParameter["dsJan3"].ToString().Replace("$ ", ""));
            decimal dsFeb3 = Convert.ToDecimal(objParameter["dsFeb3"].ToString().Replace("$ ", ""));
            decimal dsMar3 = Convert.ToDecimal(objParameter["dsMar3"].ToString().Replace("$ ", ""));
            decimal dsApr3 = Convert.ToDecimal(objParameter["dsApr3"].ToString().Replace("$ ", ""));
            decimal dsMay3 = Convert.ToDecimal(objParameter["dsMay3"].ToString().Replace("$ ", ""));
            decimal dsJun3 = Convert.ToDecimal(objParameter["dsJun3"].ToString().Replace("$ ", ""));
            decimal dsJul3 = Convert.ToDecimal(objParameter["dsJul3"].ToString().Replace("$ ", ""));
            decimal dsAug3 = Convert.ToDecimal(objParameter["dsAug3"].ToString().Replace("$ ", ""));
            decimal dsSep3 = Convert.ToDecimal(objParameter["dsSep3"].ToString().Replace("$ ", ""));
            decimal dsOct3 = Convert.ToDecimal(objParameter["dsOct3"].ToString().Replace("$ ", ""));
            decimal dsNov3 = Convert.ToDecimal(objParameter["dsNov3"].ToString().Replace("$ ", ""));
            decimal dsDec3 = Convert.ToDecimal(objParameter["dsDec3"].ToString().Replace("$ ", ""));
            decimal osJAN3 = Convert.ToDecimal(objParameter["osJAN3"].ToString().Replace("$ ", ""));
            decimal osFEB3 = Convert.ToDecimal(objParameter["osFEB3"].ToString().Replace("$ ", ""));
            decimal osMAR3 = Convert.ToDecimal(objParameter["osMAR3"].ToString().Replace("$ ", ""));
            decimal osAPR3 = Convert.ToDecimal(objParameter["osAPR3"].ToString().Replace("$ ", ""));
            decimal osMAY3 = Convert.ToDecimal(objParameter["osMAY3"].ToString().Replace("$ ", ""));
            decimal osJUN3 = Convert.ToDecimal(objParameter["osJUN3"].ToString().Replace("$ ", ""));
            decimal osJUL3 = Convert.ToDecimal(objParameter["osJUL3"].ToString().Replace("$ ", ""));
            decimal osAUG3 = Convert.ToDecimal(objParameter["osAUG3"].ToString().Replace("$ ", ""));
            decimal osSEP3 = Convert.ToDecimal(objParameter["osSEP3"].ToString().Replace("$ ", ""));
            decimal osOCT3 = Convert.ToDecimal(objParameter["osOCT3"].ToString().Replace("$ ", ""));
            decimal osNOV3 = Convert.ToDecimal(objParameter["osNOV3"].ToString().Replace("$ ", ""));
            decimal osDEC3 = Convert.ToDecimal(objParameter["osDEC3"].ToString().Replace("$ ", ""));


            string comment = objParameter["comment"].ToString();

            string userSOEID;
            userSOEID = GlobalUtil.GetSOEID();

           
            //cls_DataBaseMethods DB = new cls_DataBaseMethods();
            //ArrayList List = new ArrayList();
            //List = DB.saveDiscoveryData

            var rowsResult = (new GlobalModel()).excecuteProcedureNoReturn("procNM_SaveDiscoveryStage", new List<Dictionary<string, string>> 
            { 
                     new Dictionary<string, string>{{ "Name", "@SOEID" }, { "Value",userSOEID}},
                    new Dictionary<string, string>{{ "Name", "@MigrationID" }, { "Value",migration.ID.ToString()}},
                    new Dictionary<string, string>{{ "Name", "@FTE" }, { "Value",	             txtFTEDiscovery.ToString()		}},
                    new Dictionary<string, string>{{ "Name", "@Name" }, { "Value",	             txtNameDiscovery.ToString()		}},
                    new Dictionary<string, string>{{ "Name", "@TypeCharter" }, { "Value",	             MOUTypeID.ToString()		}},
                    new Dictionary<string, string>{{ "Name", "@Scope" }, { "Value",	             ScopeDiscovery		}},
                    new Dictionary<string, string>{{ "Name", "@Benefits" }, { "Value",	             BenefitsDiscovery		}},
        new Dictionary<string, string>{{ "Name", "@DateInitiated" }, {"Value",	             dateInitiated.ToString()		}},
        new Dictionary<string, string>{{"Name","@DateFTEAvaible" }, {"Value",	             dateAvaible.ToString()		}},
            new Dictionary<string, string>{{"Name","@FTEStandarCost"}, {"Value",	 receivingFTE.ToString()		}},
            new Dictionary<string, string>{{"Name","@PhysicalLocationID"}, {"Value",	             PhysicalLocID.ToString()		}},
            new Dictionary<string, string>{{"Name","@SiteManager"}, {"Value",	             jqxSiteManager		}},
            new Dictionary<string, string>{{"Name","@SiteFinanceManager"}, {"Value",	             jqxSiteFinanceManger		}},
        new Dictionary<string, string>{{"Name","@MSLVL09"}, {"Value",	             MSLVL09		}},
        new Dictionary<string, string>{{"Name","@dsTotal"}, {"Value",	             dsTotal.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblDSJAN"}, {"Value",	             lblDSJAN.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblDSFEB"}, {"Value",	             lblDSFEB.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblDSMAR"}, {"Value",	             lblDSMAR.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblDSAPR"}, {"Value",	             lblDSAPR.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblDSMAY"}, {"Value",	             lblDSMAY.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblDSJUN"}, {"Value",	             lblDSJUN.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblDSJUL"}, {"Value",	             lblDSJUL.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblDSAUG"}, {"Value",	             lblDSAUG.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblDSSEP"}, {"Value",	             lblDSSEP.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblDSOCT"}, {"Value",	             lblDSOCT.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblDSNOV"}, {"Value",	             lblDSNOV.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblDSDEC"}, {"Value",	             lblDSDEC.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblDSTotal"}, {"Value",	             lblDSTotal.ToString()		}},
        new Dictionary<string, string>{{"Name","@osTotal"}, {"Value",	             osTotal.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblOSJAN"}, {"Value",	             lblOSJAN.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblOSFEB"}, {"Value",	             lblOSFEB.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblOSMAR"}, {"Value",	             lblOSMAR.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblOSAPR"}, {"Value",	             lblOSAPR.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblOSMAY"}, {"Value",	             lblOSMAY.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblOSJUN"}, {"Value",	             lblOSJUN.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblOSJUL"}, {"Value",	             lblOSJUL.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblOSAUG"}, {"Value",	             lblOSAUG.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblOSSEP"}, {"Value",	             lblOSSEP.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblOSOCT"}, {"Value",	             lblOSOCT.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblOSNOV"}, {"Value",	             lblOSNOV.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblOSDEC"}, {"Value",	             lblOSDEC.ToString()		}},
        new Dictionary<string, string>{{"Name","@lblOSTOTAL"}, {"Value",	             lblOSTOTAL.ToString()		}},
        new Dictionary<string, string>{{"Name","@fteJAN"}, {"Value",	             fteJAN.ToString()		}},
        new Dictionary<string, string>{{"Name","@fteFEB"}, {"Value",	             fteFEB.ToString()		}},
        new Dictionary<string, string>{{"Name","@fteMAR"}, {"Value",	             fteMAR.ToString()		}},
        new Dictionary<string, string>{{"Name","@fteAPR"}, {"Value",	             fteAPR.ToString()		}},
        new Dictionary<string, string>{{"Name","@fteMAY"}, {"Value",	             fteMAY.ToString()		}},
        new Dictionary<string, string>{{"Name","@fteJUN"}, {"Value",	             fteJUN.ToString()		}},
        new Dictionary<string, string>{{"Name","@fteJUL"}, {"Value",	             fteJUL.ToString()		}},
        new Dictionary<string, string>{{"Name","@fteAUG"}, {"Value",	             fteAUG.ToString()		}},
        new Dictionary<string, string>{{"Name","@fteSEP"}, {"Value",	             fteSEP.ToString()		}},
        new Dictionary<string, string>{{"Name","@fteOCT"}, {"Value",	             fteOCT.ToString()		}},
        new Dictionary<string, string>{{"Name","@fteNOV"}, {"Value",	             fteNOV.ToString()		}},
        new Dictionary<string, string>{{"Name","@fteDEC"}, {"Value",	             fteDEC.ToString()		}},
        new Dictionary<string, string>{{"Name","@fteTOTAL"}, {"Value",	             fteTOTAL.ToString()		}},
        new Dictionary<string, string>{{"Name","@FTETotalCostJAN"}, {"Value",	             FTETotalCostJAN.ToString()		}},
        new Dictionary<string, string>{{"Name","@FTETotalCostFEB"}, {"Value",	             FTETotalCostFEB.ToString()		}},
        new Dictionary<string, string>{{"Name","@FTETotalCostMAR"}, {"Value",	             FTETotalCostMAR.ToString()		}},
        new Dictionary<string, string>{{"Name","@FTETotalCostAPR"}, {"Value",	             FTETotalCostAPR.ToString()		}},
        new Dictionary<string, string>{{"Name","@FTETotalCostMAY"}, {"Value",	             FTETotalCostMAY.ToString()		}},
        new Dictionary<string, string>{{"Name","@FTETotalCostJUN"}, {"Value",	             FTETotalCostJUN.ToString()		}},
        new Dictionary<string, string>{{"Name","@FTETotalCostJUL"}, {"Value",	             FTETotalCostJUL.ToString()		}},
        new Dictionary<string, string>{{"Name","@FTETotalCostAUG"}, {"Value",	             FTETotalCostAUG.ToString()		}},
        new Dictionary<string, string>{{"Name","@FTETotalCostSEP"}, {"Value",	             FTETotalCostSEP.ToString()		}},
        new Dictionary<string, string>{{"Name","@FTETotalCostOCT"}, {"Value",	             FTETotalCostOCT.ToString()		}},
        new Dictionary<string, string>{{"Name","@FTETotalCostNOV"}, {"Value",	             FTETotalCostNOV.ToString()		}},
        new Dictionary<string, string>{{"Name","@FTETotalCostDEC"}, {"Value",	             FTETotalCostDEC.ToString()		}},
        new Dictionary<string, string>{{"Name","@FTETotalCostTOTAL"}, {"Value",	             FTETotalCostTOTAL.ToString()		}},
        new Dictionary<string, string>{{"Name","@2dsTotal"}, {"Value",	             dsTotal2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblDSJAN"}, {"Value",	             lblDSJAN2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblDSFEB"}, {"Value",	             lblDSFEB2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblDSMAR"}, {"Value",	             lblDSMAR2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblDSAPR"}, {"Value",	             lblDSAPR2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblDSMAY"}, {"Value",	             lblDSMAY2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblDSJUN"}, {"Value",	             lblDSJUN2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblDSJUL"}, {"Value",	             lblDSJUL2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblDSAUG"}, {"Value",	             lblDSAUG2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblDSSEP"}, {"Value",	             lblDSSEP2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblDSOCT"}, {"Value",	             lblDSOCT2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblDSNOV"}, {"Value",	             lblDSNOV2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblDSDEC"}, {"Value",	             lblDSDEC2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblDSTotal"}, {"Value",	             lblDSTotal2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2osTotal"}, {"Value",	             osTotal2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblOSJAN"}, {"Value",	             lblOSJAN2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblOSFEB"}, {"Value",	             lblOSFEB2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblOSMAR"}, {"Value",	             lblOSMAR2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblOSAPR"}, {"Value",	             lblOSAPR2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblOSMAY"}, {"Value",	             lblOSMAY2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblOSJUN"}, {"Value",	             lblOSJUN2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblOSJUL"}, {"Value",	             lblOSJUL2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblOSAUG"}, {"Value",	             lblOSAUG2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblOSSEP"}, {"Value",	             lblOSSEP2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblOSOCT"}, {"Value",	             lblOSOCT2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblOSNOV"}, {"Value",	             lblOSNOV2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblOSDEC"}, {"Value",	             lblOSDEC2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2lblOSTOTAL"}, {"Value",	             lblOSTOTAL2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2fteJAN"}, {"Value",	             fteJAN2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2fteFEB"}, {"Value",	             fteFEB2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2fteMAR"}, {"Value",	             fteMAR2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2fteAPR"}, {"Value",	             fteAPR2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2fteMAY"}, {"Value",	             fteMAY2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2fteJUN"}, {"Value",	             fteJUN2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2fteJUL"}, {"Value",	             fteJUL2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2fteAUG"}, {"Value",	             fteAUG2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2fteSEP"}, {"Value",	             fteSEP2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2fteOCT"}, {"Value",	             fteOCT2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2fteNOV"}, {"Value",	             fteNOV2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2fteDEC"}, {"Value",	             fteDEC2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2fteTOTAL"}, {"Value",	             fteTOTAL2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2FTETotalCostJAN"}, {"Value",	             FTETotalCostJAN2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2FTETotalCostFEB"}, {"Value",	             FTETotalCostFEB2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2FTETotalCostMAR"}, {"Value",	             FTETotalCostMAR2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2FTETotalCostAPR"}, {"Value",	             FTETotalCostAPR2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2FTETotalCostMAY"}, {"Value",	             FTETotalCostMAY2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2FTETotalCostJUN"}, {"Value",	             FTETotalCostJUN2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2FTETotalCostJUL"}, {"Value",	             FTETotalCostJUL2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2FTETotalCostAUG"}, {"Value",	             FTETotalCostAUG2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2FTETotalCostSEP"}, {"Value",	             FTETotalCostSEP2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2FTETotalCostOCT"}, {"Value",	             FTETotalCostOCT2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2FTETotalCostNOV"}, {"Value",	             FTETotalCostNOV2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2FTETotalCostDEC"}, {"Value",	             FTETotalCostDEC2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2FTETotalCostTOTAL"}, {"Value",	             FTETotalCostTOTAL2.ToString()		}},
        new Dictionary<string, string>{{"Name","@3dsTotal"}, {"Value",	dsTotal3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblDSJAN"}, {"Value",	             lblDSJAN3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblDSFEB"}, {"Value",	             lblDSFEB3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblDSMAR"}, {"Value",	             lblDSMAR3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblDSAPR"}, {"Value",	             lblDSAPR3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblDSMAY"}, {"Value",	             lblDSMAY3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblDSJUN"}, {"Value",	             lblDSJUN3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblDSJUL"}, {"Value",	             lblDSJUL3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblDSAUG"}, {"Value",	             lblDSAUG3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblDSSEP"}, {"Value",	             lblDSSEP3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblDSOCT"}, {"Value",	             lblDSOCT3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblDSNOV"}, {"Value",	             lblDSNOV3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblDSDEC"}, {"Value",	             lblDSDEC3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblDSTotal"}, {"Value",	             lblDSTotal3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3osTotal"}, {"Value",	             osTotal3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblOSJAN"}, {"Value",	             lblOSJAN3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblOSFEB"}, {"Value",	             lblOSFEB3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblOSMAR"}, {"Value",	             lblOSMAR3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblOSAPR"}, {"Value",	             lblOSAPR3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblOSMAY"}, {"Value",	             lblOSMAY3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblOSJUN"}, {"Value",	             lblOSJUN3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblOSJUL"}, {"Value",	             lblOSJUL3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblOSAUG"}, {"Value",	             lblOSAUG3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblOSSEP"}, {"Value",	             lblOSSEP3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblOSOCT"}, {"Value",	             lblOSOCT3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblOSNOV"}, {"Value",	             lblOSNOV3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblOSDEC"}, {"Value",	             lblOSDEC3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3lblOSTOTAL"}, {"Value",	             lblOSTOTAL3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3fteJAN"}, {"Value",	             fteJAN3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3fteFEB"}, {"Value",	             fteFEB3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3fteMAR"}, {"Value",	             fteMAR3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3fteAPR"}, {"Value",	             fteAPR3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3fteMAY"}, {"Value",	             fteMAY3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3fteJUN"}, {"Value",	             fteJUN3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3fteJUL"}, {"Value",	             fteJUL3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3fteAUG"}, {"Value",	             fteAUG3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3fteSEP"}, {"Value",	             fteSEP3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3fteOCT"}, {"Value",	             fteOCT3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3fteNOV"}, {"Value",	             fteNOV3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3fteDEC"}, {"Value",	             fteDEC3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3fteTOTAL"}, {"Value",	             fteTOTAL3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3FTETotalCostJAN"}, {"Value",	             FTETotalCostJAN3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3FTETotalCostFEB"}, {"Value",	             FTETotalCostFEB3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3FTETotalCostMAR"}, {"Value",	             FTETotalCostMAR3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3FTETotalCostAPR"}, {"Value",	             FTETotalCostAPR3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3FTETotalCostMAY"}, {"Value",	             FTETotalCostMAY3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3FTETotalCostJUN"}, {"Value",	             FTETotalCostJUN3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3FTETotalCostJUL"}, {"Value",	             FTETotalCostJUL3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3FTETotalCostAUG"}, {"Value",	             FTETotalCostAUG3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3FTETotalCostSEP"}, {"Value",	             FTETotalCostSEP3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3FTETotalCostOCT"}, {"Value",	             FTETotalCostOCT3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3FTETotalCostNOV"}, {"Value",	             FTETotalCostNOV3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3FTETotalCostDEC"}, {"Value",	             FTETotalCostDEC3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3FTETotalCostTOTAL"}, {"Value",	             FTETotalCostTOTAL3.ToString()		}},
        new Dictionary<string, string>{{"Name","@dsJan"}, {"Value",	dsJan.ToString()		}},
        new Dictionary<string, string>{{"Name","@dsFeb"}, {"Value",	             dsFeb.ToString()		}},
        new Dictionary<string, string>{{"Name","@dsMar"}, {"Value",	             dsMar.ToString()		}},
        new Dictionary<string, string>{{"Name","@dsApr"}, {"Value",	             dsApr.ToString()		}},
        new Dictionary<string, string>{{"Name","@dsMay"}, {"Value",	             dsMay.ToString()		}},
        new Dictionary<string, string>{{"Name","@dsJun"}, {"Value",	             dsJun.ToString()		}},
        new Dictionary<string, string>{{"Name","@dsJul"}, {"Value",	             dsJul.ToString()		}},
        new Dictionary<string, string>{{"Name","@dsAug"}, {"Value",	             dsAug.ToString()		}},
        new Dictionary<string, string>{{"Name","@dsSep"}, {"Value",	             dsSep.ToString()		}},
        new Dictionary<string, string>{{"Name","@dsOct"}, {"Value",	             dsOct.ToString()		}},
        new Dictionary<string, string>{{"Name","@dsNov"}, {"Value",	             dsNov.ToString()		}},
        new Dictionary<string, string>{{"Name","@dsDec"}, {"Value",	             dsDec.ToString()		}},
        new Dictionary<string, string>{{"Name","@osJAN"}, {"Value",	             osJAN.ToString()		}},
        new Dictionary<string, string>{{"Name","@osFEB"}, {"Value",	             osFEB.ToString()		}},
        new Dictionary<string, string>{{"Name","@osMAR"}, {"Value",	             osMAR.ToString()		}},
        new Dictionary<string, string>{{"Name","@osAPR"}, {"Value",	             osAPR.ToString()		}},
        new Dictionary<string, string>{{"Name","@osMAY"}, {"Value",	             osMAY.ToString()		}},
        new Dictionary<string, string>{{"Name","@osJUN"}, {"Value",	             osJUN.ToString()		}},
        new Dictionary<string, string>{{"Name","@osJUL"}, {"Value",	             osJUL.ToString()		}},
        new Dictionary<string, string>{{"Name","@osAUG"}, {"Value",	             osAUG.ToString()		}},
        new Dictionary<string, string>{{"Name","@osSEP"}, {"Value",	             osSEP.ToString()		}},
        new Dictionary<string, string>{{"Name","@osOCT"}, {"Value",	             osOCT.ToString()		}},
        new Dictionary<string, string>{{"Name","@osNOV"}, {"Value",	             osNOV.ToString()		}},
        new Dictionary<string, string>{{"Name","@osDEC"}, {"Value",	             osDEC.ToString()		}},
        new Dictionary<string, string>{{"Name","@2dsJan"}, {"Value",	             dsJan2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2dsFeb"}, {"Value",	             dsFeb2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2dsMar"}, {"Value",	             dsMar2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2dsApr"}, {"Value",	             dsApr2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2dsMay"}, {"Value",	             dsMay2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2dsJun"}, {"Value",	             dsJun2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2dsJul"}, {"Value",	             dsJul2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2dsAug"}, {"Value",	             dsAug2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2dsSep"}, {"Value",	             dsSep2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2dsOct"}, {"Value",	             dsOct2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2dsNov"}, {"Value",	             dsNov2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2dsDec"}, {"Value",	             dsDec2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2osJAN"}, {"Value",	             osJAN2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2osFEB"}, {"Value",	             osFEB2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2osMAR"}, {"Value",	             osMAR2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2osAPR"}, {"Value",	             osAPR2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2osMAY"}, {"Value",	             osMAY2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2osJUN"}, {"Value",	             osJUN2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2osJUL"}, {"Value",	             osJUL2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2osAUG"}, {"Value",	             osAUG2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2osSEP"}, {"Value",	             osSEP2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2osOCT"}, {"Value",	             osOCT2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2osNOV"}, {"Value",	             osNOV2.ToString()		}},
        new Dictionary<string, string>{{"Name","@2osDEC"}, {"Value",	             osDEC2.ToString()	}},
        new Dictionary<string, string>{{"Name","@3dsJan"}, {"Value",	dsJan3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3dsFeb"}, {"Value",	             dsFeb3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3dsMar"}, {"Value",	             dsMar3.ToString()	}},
        new Dictionary<string, string>{{"Name","@3dsApr"}, {"Value",	             dsApr3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3dsMay"}, {"Value",	             dsMay3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3dsJun"}, {"Value",	             dsJun3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3dsJul"}, {"Value",	             dsJul3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3dsAug"}, {"Value",	             dsAug3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3dsSep"}, {"Value",	             dsSep3.ToString()	}},
        new Dictionary<string, string>{{"Name","@3dsOct"}, {"Value",	             dsOct3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3dsNov"}, {"Value",	             dsNov3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3dsDec"}, {"Value",	             dsDec3.ToString()	}},
        new Dictionary<string, string>{{"Name","@3osJAN"}, {"Value",	             osJAN3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3osFEB"}, {"Value",	             osFEB3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3osMAR"}, {"Value",	             osMAR3.ToString()	}},
        new Dictionary<string, string>{{"Name","@3osAPR"}, {"Value",	             osAPR3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3osMAY"}, {"Value",	             osMAY3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3osJUN"}, {"Value",	             osJUN3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3osJUL"}, {"Value",	             osJUL3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3osAUG"}, {"Value",	             osAUG3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3osSEP"}, {"Value",	             osSEP3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3osOCT"}, {"Value",	             osOCT3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3osNOV"}, {"Value",	             osNOV3.ToString()		}},
        new Dictionary<string, string>{{"Name","@3osDEC"}, {"Value",	             osDEC3.ToString()		}},
        new Dictionary<string, string>{{"Name","@Comment"}, {"Value",	comment		}}

                    });

            //JObject o = JObject.FromObject(new
            //{
            //    item =
            //        from p in DAO.tblNM_Attachment
            //        where p.ID == pRequestID
            //        select new
            //        {
            //            p.Path
            //        }

            //});

            return Content(null);
        }


       [Route("getDataDiscovery")]
        public ActionResult   getDataDiscovery(string data)
        {

            HMT2Entities DAO = new HMT2Entities();
             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string pNonMOU = objParameter["pNonMOU"].ToString();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWNM_DiscoveryStage
                    where p.RequestAutoID == pNonMOU
                    select new
                    {
                        p.ID
                      ,
                        p.FTE
                      ,
                        p.FroFunctionsMS
                      ,
                        p.RequestID
                      ,
                        p.Status
                      ,
                        p.RequestAutoID
                      ,
                        p.Name
                      ,
                        p.Scope
                      ,
                        p.Benefits
                      ,
                        p.Type
                      ,
                        p.Initiated
                      ,
                        p.FTEAvaible
                      ,
                        p.FTEStandarCost
                      ,
                        p.Expr1
                      ,
                        p.SiteManager
                      ,
                        p.SiteFinanceManager
                    }

            });

            return Content(o["item"].ToString());
        }

       [Route("getCommentConversation")]
        public ActionResult   getCommentConversation(string data)
        {

            HMT2Entities DAO = new HMT2Entities();
             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string pNonMOU = objParameter["pNonMOU"].ToString();

            tblNM_Migration mig = (from c in DAO.tblNM_Migration where c.RequestAutoID == pNonMOU select c).SingleOrDefault<tblNM_Migration>();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblNM_Comment
                    where p.MigrationID == mig.ID
                    select new
                    {
                        p.ID
                      ,
                        p.Comment
                    }

            });

            return Content( o["item"].ToString());
        }


       [Route("getValidationDiscoveryForm")]
        public ActionResult   getValidationDiscoveryForm(string data)
        {

            HMT2Entities DAO = new HMT2Entities();
             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string pNonMOU = objParameter["pNonMOU"].ToString();

            tblNM_Migration mig = (from c in DAO.tblNM_Migration where c.RequestAutoID == pNonMOU select c).SingleOrDefault<tblNM_Migration>();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procNM_DiscoveyValidation(mig.ID)
                    select new
                    {
                        p.ERROR
                      ,
                        p.WARNING
                      ,
                        p.STEP
                    }

            });

            return Content(o["item"].ToString());
        }

       [Route("subbmitForApprovalDiscovery")]
        public ActionResult   subbmitForApprovalDiscovery(string data)
        {

            HMT2Entities DAO = new HMT2Entities();
             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string pNonMOU = objParameter["pNonMOU"].ToString();

            tblNM_Migration mig = (from c in DAO.tblNM_Migration where c.RequestAutoID == pNonMOU select c).SingleOrDefault<tblNM_Migration>();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procNM_SubbmitForApproval(mig.ID)
                    select new
                    {
                        p.ERROR
                      ,
                        p.WARNING
                      ,
                        p.STEP
                    }

            });

            return Content(o["item"].ToString());
        }


       [Route("getListPendingForApproval")]
        public ActionResult   getListPendingForApproval()
        {
            HMT2Entities DAO = new HMT2Entities();

            string userSOEID;
            userSOEID = GlobalUtil.GetSOEID();



            string pSTATUS = (from p in DAO.tblNM_RequestStatus where p.Status == "Approval" select p).SingleOrDefault().Status;



            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWNM_ApprovalNonMOU
                    where p.SOEID == userSOEID && p.Active == true
                    orderby p.ID descending
                    select new
                    {
                        p.ID,
                        p.RequestAutoID,
                        p.Name,
                        p.FTE,
                        p.ManagedSegmentName,
                        p.ManagedGeographyName,
                        p.DateAction,
                        p.ApprovalID
                    }

            });

            return Content(o["item"].ToString());

        }


       [Route("getMigrationDetail")]
        public ActionResult   getMigrationDetail(string data)
        {
            
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
           
           

            int pRequestID = Convert.ToInt32(objParameter["pID"].ToString());

            HMT2Entities DAO = new HMT2Entities();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procNM_MigrationDetail(pRequestID)
                    select new
                    {
                        p.HTML,
                        p.Number
                    }

            });

            return Content(o["item"].ToString());
        }


       [Route("approveNonMOU")]
        public ActionResult   approveNonMOU(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string pRequestID = objParameter["pID"].ToString();

            tblNM_Migration NM = (from p in DAO.tblNM_Migration where p.RequestAutoID == pRequestID select p).SingleOrDefault<tblNM_Migration>();
            string userSOEID;
            userSOEID = GlobalUtil.GetSOEID();


            DAO.procNM_ApproveNonMOU(NM.ID, userSOEID);

            return Content(null);
        }

       [Route("rejectNonMOU")]
        public ActionResult   rejectNonMOU(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string pRequestID = objParameter["pID"].ToString();

            tblNM_Migration NM = (from p in DAO.tblNM_Migration where p.RequestAutoID == pRequestID select p).SingleOrDefault<tblNM_Migration>();
            string userSOEID;
            userSOEID = GlobalUtil.GetSOEID();


            DAO.procNM_RejectNonMOU(NM.ID, userSOEID);

            return Content(null);
        }


       [Route("getListLiveStage")]
        public ActionResult   getListLiveStage()
        {
            HMT2Entities DAO = new HMT2Entities();

            string userSOEID;
            userSOEID = GlobalUtil.GetSOEID();



            string pSTATUS = (from p in DAO.tblNM_RequestStatus where p.Status == "Approval" select p).SingleOrDefault().Status;



            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWNM_RequestGrid
                    where p.Status == 5
                    orderby p.ID descending
                    select new
                    {
                        p.ID,
                        p.RequestAutoID,
                        p.Name,
                        p.FTE,
                        p.ManagedSegmentName,
                        p.ManagedGeographyName,
                    }

            });

            return Content(o["item"].ToString());

        }

       [Route("getTotals")]
        public ActionResult   getTotals()
        {
            HMT2Entities DAO = new HMT2Entities();

            string userSOEID;
            userSOEID = GlobalUtil.GetSOEID();


            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procNM_Totals(userSOEID)
                    select new
                    {
                        p.Request,
                        p.Discovery,
                        p.Approval,
                        p.Live
                    }

            });

            return Content(o["item"].ToString());

        }


       [Route("getFinancialMSLVL09")]
        public ActionResult   getFinancialMSLVL09()
        {
            HMT2Entities DAO = new HMT2Entities();

            string userSOEID;
            userSOEID = GlobalUtil.GetSOEID();


            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWAutomation_MSL09
                    select new
                    {

                        p.Description
                    }

            });

            return Content(o["item"].ToString());
        }


       [Route("getMSLVL09Selected")]
        public ActionResult   getMSLVL09Selected(string data)
        {

            HMT2Entities DAO = new HMT2Entities();
             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string pNonMOU = objParameter["pNonMOU"].ToString();

            tblNM_Migration m = (from c in DAO.tblNM_Migration where c.RequestAutoID == pNonMOU select c).SingleOrDefault<tblNM_Migration>();
            tblNM_FinancialsGeneralInfo FGI = (from c in DAO.tblNM_FinancialsGeneralInfo where c.MigrationID == m.ID select c).SingleOrDefault<tblNM_FinancialsGeneralInfo>();



            try
            {
                int id = Convert.ToInt32(FGI.ManagedSegmentL09);

                JObject o = JObject.FromObject(new
                {
                    item =
                        from p in DAO.VWAutomation_MSL09
                        where p.ID == id
                        select new
                        {
                            p.Description
                        }

                });

                return Content( o["item"].ToString());
            }
            catch
            {
                int id = 8262;

                JObject o = JObject.FromObject(new
                {
                    item =
                        from p in DAO.VWAutomation_MSL09
                        where p.ID == id
                        select new
                        {
                            p.Description
                        }

                });

                return Content(o["item"].ToString());
            }

        }

       [Route("addNewOther")]
        public ActionResult   addNewOther(string data)
        {

            HMT2Entities DAO = new HMT2Entities();
             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string pNonMOU = objParameter["pNonMOU"].ToString();

            tblNM_Migration migration = (from c in DAO.tblNM_Migration where c.RequestAutoID == pNonMOU select c).SingleOrDefault<tblNM_Migration>();

            string startMonth = objParameter["pStartM"].ToString();
            string endMonth = objParameter["pEndM"].ToString();

            int startYear = Convert.ToInt32(objParameter["pStartY"].ToString());
            int endYear = Convert.ToInt32(objParameter["pEndY"].ToString());

            DAO.procNM_AddNewOther(migration.ID, startMonth, endMonth, startYear, endYear);


            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblNM_FinancialEndDate
                    where p.MigrationID == migration.ID
                    select new
                    {
                        p.ID,
                        p.StartMonth,
                        p.StartYear,
                        p.EndMonth,
                        p.EndYear
                    }

            });

            return Content(o["item"].ToString());
        }

       [Route("getOthersList")]
        public ActionResult   getOthersList(string data)
        {

            HMT2Entities DAO = new HMT2Entities();
             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string pNonMOU = objParameter["pNonMOU"].ToString();

            tblNM_Migration migration = (from c in DAO.tblNM_Migration where c.RequestAutoID == pNonMOU select c).SingleOrDefault<tblNM_Migration>();

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblNM_FinancialEndDate
                    where p.MigrationID == migration.ID
                    select new
                    {
                        p.ID,
                        p.StartMonth,
                        p.StartYear,
                        p.EndMonth,
                        p.EndYear
                    }

            });

            return Content(o["item"].ToString());
        }

       [Route("deleteNewOther")]
        public ActionResult   deleteNewOther(string data)
        {

            HMT2Entities DAO = new HMT2Entities();
             var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            int pID = Convert.ToInt32(objParameter["pID"].ToString());

            tblNM_FinancialEndDate finEndDate = (from p in DAO.tblNM_FinancialEndDate where p.ID == pID select p).SingleOrDefault<tblNM_FinancialEndDate>();

            int IDmIGRATION = finEndDate.MigrationID;

            DAO.procNM_DeleteNewOther(pID);

            JObject o = JObject.FromObject(new
            {
                item =
                    from p in DAO.tblNM_FinancialEndDate
                    where p.MigrationID == IDmIGRATION
                    select new
                    {
                        p.ID,
                        p.StartMonth,
                        p.StartYear,
                        p.EndMonth,
                        p.EndYear
                    }

            });

            return Content(o["item"].ToString());
        }


    }
}
