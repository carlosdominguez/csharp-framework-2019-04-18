﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Automation.Web.Models.HMT2;


using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace Automation.Web.Controllers.HMT2
{

    [RoutePrefix("HMT2")]
    public class RoleReviewController : Controller
    {
        [Route("RoleReview")]
        public ActionResult Index()
        {
            return (View("~/Views/HMT2/RoleReview.cshtml"));
        }

       
        [Route("getTotalRoles")]
        public ActionResult getTotalRoles()
        {
            HMT2Entities DAO = new HMT2Entities();

            var total = from p in DAO.VWR_CompleteList where p.RoleIsActive == true select p;

            return Content(total.Count().ToString());
        }

    [Route("getTotalRolesPendingReview")]
    public ActionResult getTotalRolesPendingReview()
    {
        HMT2Entities DAO = new HMT2Entities();

        var total = from p in DAO.VWR_CompleteList where p.FromType == "MOU" select p;

        return Content(total.Count().ToString());
    }

    [Route("getTotalRolesMOU")]
    public ActionResult getTotalRolesMOU()
    {
        HMT2Entities DAO = new HMT2Entities();

        var total = from p in DAO.VWR_CompleteList where p.FromType == "NON MOU" select p;

        return Content(total.Count().ToString());
    }

    [Route("getTotalRolesNonMOU")]
    public ActionResult getTotalRolesNonMOU()
    {
        HMT2Entities DAO = new HMT2Entities();

        var total = from p in DAO.VWR_GocRoleList where p.TotalRoles > 0 select p;

        return Content(total.Count().ToString());
    }

    [Route("getTotalRolesList")]
    public ActionResult getTotalRolesList()
    {
        HMT2Entities DAO = new HMT2Entities();

        JObject o = null;

        o = JObject.FromObject(new
        {
            item =
                from p in DAO.VWR_CompleteList
                where p.RoleIsActive == true
                select new
                {
                   p.ID
            ,p.RoleID
            ,p.RoleTypeID
            ,p.FromType
            ,p.Number
            ,p.IDType
            ,p.CreationDate
            ,p.CreatedBy
            ,p.IsActive
            ,p.ModifyBy
            ,p.ModifyDate
            ,p.Name
            ,p.Description
            ,p.GOCID
            ,p.MSL09
            ,p.Process
            ,p.SubProcess
            ,p.MgdGeaography
            ,p.MgdRegion
            ,p.MgdCountry
            ,p.LVID
            ,p.PhysicalLocationID
            ,p.ChargeOutProfileID
            ,p.RoleIsActive
            ,p.RoleCreaetedBy
            ,p.CreatedDate
            ,p.LastUpdateBy
            ,p.LastUpdateDate
            ,p.StatusID
            ,p.IsFteable
            ,p.IsDirectStaf
            ,p.FTEPercentage
            ,p.Status
            ,p.ManagedSegment
            ,p.ManagedGeography
            ,p.PhysicalLocation
            ,p.ChargeOutProfile
            ,p.RoleAuto
            ,p.GOCName 
            ,p.CssDriver 
            ,p.mouName
                }
        });


        return Content(o["item"].ToString());


    }

    [Route("getPendingForReviewDetail")]
    public ActionResult getPendingForReviewDetail()
    {
        HMT2Entities DAO = new HMT2Entities();

        JObject o = null;

        o = JObject.FromObject(new
        {
            item =
                from p in DAO.VWR_CompleteList
                where p.StatusID == 1
                select new
                {
                    p.ID
            ,
                    p.RoleID
            ,
                    p.RoleTypeID
            ,
                    p.FromType
            ,
                    p.Number
            ,
                    p.IDType
            ,
                    p.CreationDate
            ,
                    p.CreatedBy
            ,
                    p.IsActive
            ,
                    p.ModifyBy
            ,
                    p.ModifyDate
            ,
                    p.Name
            ,
                    p.Description
            ,
                    p.GOCID
            ,
                    p.MSL09
            ,
                    p.Process
            ,
                    p.SubProcess
            ,
                    p.MgdGeaography
            ,
                    p.MgdRegion
            ,
                    p.MgdCountry
            ,
                    p.LVID
            ,
                    p.PhysicalLocationID
            ,
                    p.ChargeOutProfileID
            ,
                    p.RoleIsActive
            ,
                    p.RoleCreaetedBy
            ,
                    p.CreatedDate
            ,
                    p.LastUpdateBy
            ,
                    p.LastUpdateDate
            ,
                    p.StatusID
            ,
                    p.IsFteable
            ,
                    p.IsDirectStaf
            ,
                    p.FTEPercentage
            ,
                    p.Status
            ,
                    p.ManagedSegment
            ,
                    p.ManagedGeography
            ,
                    p.PhysicalLocation
            ,
                    p.ChargeOutProfile
            ,
                    p.RoleAuto
                }
        });


        return Content(o["item"].ToString());


    }

    [Route("getMOUDashboard")]
    public ActionResult getMOUDashboard()
    {
        HMT2Entities DAO = new HMT2Entities();

        JObject o = null;

        o = JObject.FromObject(new
        {
            item =
                from p in DAO.VWR_ApproveMOU
                orderby p.ApprovedDate descending
                select new
                {
                    ID = p.ID,
                    MOU = p.MOU,
                    Name = p.Name,
                    ManagedGeographyID = p.ManagedGeographyID,
                    ManagedGeographyName = p.ManagedGeographyName,
                    ManagedSegmentID = p.ManagedSegmentID,
                    ManagedSegmentName = p.ManagedSegmentName,
                    ApprovalSOEID = p.ApprovalSOEID,
                    ApprovalName = p.ApprovalName,
                    ApprovedDate = p.ApprovedDate,
                    FTE = p.FTE,
                    TotalRole = p.TotalRole,
                    p.CssDriver,
                    p.ApproveID,
                    p.Number,
                    p.Approve,
                    p.ApproveBy,
                    p.ApproveDate
                    
                }
        });


        return Content(o["item"].ToString());
    }


    [Route("getTotalRolesList")]
    public ActionResult acceptMOU(string data)
    {
        var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        int ID = Convert.ToInt32(objParameter["ID"].ToString());
        string SOEID = objParameter["SOEID"].ToString();


        HMT2Entities DAO = new HMT2Entities();

            DAO.procR_Approve(ID, true, true, SOEID);

            //VWR_ApproveMOU AM = new VWR_ApproveMOU();

            //AM = (from p in DAO.VWR_ApproveMOU where p.ID == ID select p).SingleOrDefault<VWR_ApproveMOU>();



            //if ((from p in DAO.tblR_NumberApprove where p.Number == AM.MOU select p).ToList<tblR_NumberApprove>().Count() > 0)
            //{
            //    tblR_NumberApprove Approve = (from p in DAO.tblR_NumberApprove where p.Number == AM.MOU select p).SingleOrDefault<tblR_NumberApprove>();

            //    Approve.Approve = true;
            //    Approve.ApproveBy = SOEID;
            //    Approve.ApproveDate = DateTime.Today;
            //    Approve.Number = AM.MOU;

            //    DAO.SaveChanges();

            //}
            //else
            //{
            //    tblR_NumberApprove Approve = new tblR_NumberApprove();

            //    Approve.Approve = true;
            //    Approve.ApproveBy = SOEID;
            //    Approve.ApproveDate =  DateTime.Today;
            //    Approve.Number = AM.MOU;

            //    DAO.tblR_NumberApprove.Add(Approve);

            //    DAO.SaveChanges();

            //}


            return Content("");

    }

 [Route("getTotalRolesList")]
     public ActionResult UnAcceptMOU(string data)
{
    var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

    int ID = Convert.ToInt32(objParameter["ID"].ToString());
    string SOEID = objParameter["SOEID"].ToString();

    HMT2Entities DAO = new HMT2Entities();

            DAO.procR_Approve(ID, false, true, SOEID);

    //        VWR_ApproveMOU AM = new VWR_ApproveMOU();
    //AM = (from p in DAO.VWR_ApproveMOU where p.ID == ID select p).SingleOrDefault<VWR_ApproveMOU>();

    //tblR_NumberApprove Approve = (from p in DAO.tblR_NumberApprove where p.Number == AM.MOU select p).SingleOrDefault<tblR_NumberApprove>();

    //Approve.Approve = false;
    //Approve.ApproveBy = SOEID;
    //Approve.ApproveDate = DateTime.Today;

    //DAO.SaveChanges();

    return Content("");

}



    [Route("getTotalRolesList")]
    public ActionResult getTotalRolesListMOU(string data)
    {
        var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        string pMOU = objParameter["pMOU"].ToString();


        HMT2Entities DAO = new HMT2Entities();

        JObject o = null;

        o = JObject.FromObject(new
        {
            item =
                from p in DAO.VWR_CompleteList
                where p.Number == pMOU
                select new
                {
                    p.ID
            ,
                    p.RoleID
            ,
                    p.RoleTypeID
            ,
                    p.FromType
            ,
                    p.Number
            ,
                    p.IDType
            ,
                    p.CreationDate
            ,
                    p.CreatedBy
            ,
                    p.IsActive
            ,
                    p.ModifyBy
            ,
                    p.ModifyDate
            ,
                    p.Name
            ,
                    p.Description
            ,
                    p.GOCID
            ,
                    p.MSL09
            ,
                    p.Process
            ,
                    p.SubProcess
            ,
                    p.MgdGeaography
            ,
                    p.MgdRegion
            ,
                    p.MgdCountry
            ,
                    p.LVID
            ,
                    p.PhysicalLocationID
            ,
                    p.ChargeOutProfileID
            ,
                    p.RoleIsActive
            ,
                    p.RoleCreaetedBy
            ,
                    p.CreatedDate
            ,
                    p.LastUpdateBy
            ,
                    p.LastUpdateDate
            ,
                    p.StatusID
            ,
                    p.IsFteable
            ,
                    p.IsDirectStaf
            ,
                    p.FTEPercentage
            ,
                    p.Status
            ,
                    p.ManagedSegment
            ,
                    p.ManagedGeography
            ,
                    p.PhysicalLocation
            ,
                    p.ChargeOutProfile
            ,
                    p.RoleAuto
            ,
                    p.GOCName
                }
        });


        return Content(o["item"].ToString());


    }

    [Route("getNonMOUDashboard")]
    public ActionResult getNonMOUDashboard()
    {
        HMT2Entities DAO = new HMT2Entities();

        JObject o = null;
        //select * from [dbo].[VWNM_NonMOU]
        o = JObject.FromObject(new
        {
            item =
                from p in DAO.VWNM_ApproveNM where p.Status == 5 orderby p.NonMOU descending
                select new
                {
                    p.NonMOU
            ,
                    p.FTE
            ,
                    p.FroFunctionsMS
            ,
                    p.RequestID
            ,
                    p.Status
            ,
                    p.NonMOUAlias
            ,
                    p.Name
            ,
                    p.Type
            ,
                    p.Scope
            ,
                    p.Benefits
            ,
                    p.FTEStandarCost
            ,
                    p.PhysicalLocationID
            ,
                    p.SiteManager
            ,
                    p.SiteFinanceManager
            ,
                    p.ManagedGeographyID
            ,
                    p.ManagedGeographyName
            ,
                    p.ManagedSegmentID
            ,
                    p.ManagedSegmentName
            ,
                    p.ChargeOutGOC
            ,
                    p.ReceivingGOC
            ,
                    p.TotalDirectStafft
            ,
                    p.TotalTemporal
            ,
                    p.CssDriver,
                    p.ApproveID,
                    p.Number,
                    p.Approve,
                    p.ApproveBy,
                    p.ApproveDate
               
                }
        });


        return Content(o["item"].ToString());


    }


    [Route("getTotalRolesList")]
    public ActionResult acceptNONMOU(string data)
    {
        var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        int ID = Convert.ToInt32(objParameter["ID"].ToString());
        string SOEID = objParameter["SOEID"].ToString();


        HMT2Entities DAO = new HMT2Entities();

            DAO.procR_Approve(ID, true, false, SOEID);


        //VWNM_ApproveNM AM = new VWNM_ApproveNM();

        //AM = (from p in DAO.VWNM_ApproveNM where p.NonMOU == ID select p).SingleOrDefault<VWNM_ApproveNM>();



        //if ((from p in DAO.tblR_NumberApprove where p.Number == AM.NonMOUAlias select p).ToList<tblR_NumberApprove>().Count() > 0)
        //{
        //    tblR_NumberApprove Approve = (from p in DAO.tblR_NumberApprove where p.Number == AM.NonMOUAlias select p).SingleOrDefault<tblR_NumberApprove>();

        //    Approve.Approve = true;
        //    Approve.ApproveBy = SOEID;
        //    Approve.ApproveDate = DateTime.Today;
        //    Approve.Number = AM.NonMOUAlias;

        //    DAO.SaveChanges();

        //}
        //else
        //{
        //    tblR_NumberApprove Approve = new tblR_NumberApprove();

        //    Approve.Approve = true;
        //    Approve.ApproveBy = SOEID;
        //    Approve.ApproveDate = DateTime.Today;
        //    Approve.Number = AM.NonMOUAlias;

        //    DAO.tblR_NumberApprove.Add(Approve);

        //    DAO.SaveChanges();

        //}


        return Content("");

    }

    [Route("getTotalRolesList")]
    public ActionResult UnAcceptNONMOU(string data)
    {
        var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

        int ID = Convert.ToInt32(objParameter["ID"].ToString());
        string SOEID = objParameter["SOEID"].ToString();

        HMT2Entities DAO = new HMT2Entities();

            DAO.procR_Approve(ID, false, false, SOEID);

            //VWNM_ApproveNM AM = new VWNM_ApproveNM();
            //AM = (from p in DAO.VWNM_ApproveNM where p.NonMOU == ID select p).SingleOrDefault<VWNM_ApproveNM>();

            //tblR_NumberApprove Approve = (from p in DAO.tblR_NumberApprove where p.Number == AM.NonMOUAlias select p).SingleOrDefault<tblR_NumberApprove>();

            //Approve.Approve = false;
            //Approve.ApproveBy = SOEID;
            //Approve.ApproveDate = DateTime.Today;

            //DAO.SaveChanges();

            return Content("");

    }


    [Route("getUnassignGDW")]
    public ActionResult getUnassignGDW()
    {
        HMT2Entities DAO = new HMT2Entities();

        JObject o = null;
        //select * from [dbo].[VWNM_NonMOU]
        o = JObject.FromObject(new
        {
            item =
                from p in DAO.VWR_UnassignGDW
                select new
                {
                   p.ID
            ,p.Name
            ,p.GOCID
            ,p.MSL09
            ,p.Process
            ,p.SubProcess
            ,p.MgdGeaography
            ,p.MgdRegion
            ,p.MgdCountry
            ,p.LVID
            ,p.PhysicalLocationID
            ,p.ChargeOutProfileID
            ,p.RoleIsActive
            ,p.RoleCreaetedBy
            ,p.CreatedDate
            ,p.LastUpdateBy
            ,p.LastUpdateDate
            ,p.StatusID
            ,p.IsFteable
            ,p.IsDirectStaf
            ,p.FTEPercentage
            ,p.Status
            ,p.ManagedSegment
            ,p.ManagedGeography
            ,p.PhysicalLocation
            ,p.ChargeOutProfile
            ,p.RoleAuto
            ,p.GOCName
            ,p.SOEID
            ,p.StartDate
            ,p.FirstName
            ,p.LastName
            ,p.EMPL_CLASS
            ,p.Classification
                }
        });


        return Content(o["item"].ToString());


    }



    [Route("getGOCData")]
    public ActionResult getGOCData(string data)
    {

        HMT2Entities DAO = new HMT2Entities();
        var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

        string GOC = objParameter["pGOC"].ToString();

        JObject o = JObject.FromObject(new
        {
            item =
                from p in DAO.VWR_GOC
                where p.GOCName == GOC
                select new
                {
                    p.ManagedSegmentID ,
                    p.ManagedSegmentName ,
                    p.ManagedGeographyID ,
                    p.ManagedGeographyName,
                    p.Region,
                    p.LegalVehiculeID ,
                    p.Market
                }

        });

        return Content(o["item"].ToString());
    }

    [Route("saveChanges")]
    public ActionResult saveChanges(string data)
    {

        HMT2Entities DAO = new HMT2Entities();
        var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

        string role = objParameter["pRole"].ToString();
        string name = objParameter["pName"].ToString();
        string goc = objParameter["pGOC"].ToString();
        string pl = objParameter["pPL"].ToString();
        string ds = objParameter["pDS"].ToString();
        string fte = objParameter["pFTE"].ToString();
        string userSOEID = GlobalUtil.GetSOEID();

        DAO.procR_SaveChanges(role, name, goc, pl, ds, fte, userSOEID);
        
        return Content("");
    }


    [Route("getMOUDashboardFilter")]
    public ActionResult getMOUDashboardFilter(string data)
    {
        HMT2Entities DAO = new HMT2Entities();
        var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

        string mou = objParameter["pMOU"].ToString();

        JObject o = null;

        o = JObject.FromObject(new
        {
            item =
                from p in DAO.VWR_ApproveMOU
                where p.MOU == mou
                select new
                {
                    ID = p.ID,
                    MOU = p.MOU,
                    Name = p.Name,
                    ManagedGeographyID = p.ManagedGeographyID,
                    ManagedGeographyName = p.ManagedGeographyName,
                    ManagedSegmentID = p.ManagedSegmentID,
                    ManagedSegmentName = p.ManagedSegmentName,
                    ApprovalSOEID = p.ApprovalSOEID,
                    ApprovalName = p.ApprovalName,
                    ApprovedDate = p.ApprovedDate,
                    FTE = p.FTE,
                    TotalRole = p.TotalRole,
                    p.CssDriver,
                    p.ApproveID,
                    p.Number,
                    p.Approve,
                    p.ApproveBy,
                    p.ApproveDate

                }
        });


        return Content(o["item"].ToString());


    }


    [Route("getNonMOUDashboardFilter")]
    public ActionResult getNonMOUDashboardFilter(string data)
    {
        HMT2Entities DAO = new HMT2Entities();
         var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


        string pNonMOU = objParameter["pNonMOU"].ToString();
        
        JObject o = null;
        //select * from [dbo].[VWNM_NonMOU]
        o = JObject.FromObject(new
        {
            item =
                from p in DAO.VWNM_NonMOU
                where p.Status == 5 && p.NonMOUAlias == pNonMOU
                select new
                {
                    p.NonMOU
            ,
                    p.FTE
            ,
                    p.FroFunctionsMS
            ,
                    p.RequestID
            ,
                    p.Status
            ,
                    p.NonMOUAlias
            ,
                    p.Name
            ,
                    p.Type
            ,
                    p.Scope
            ,
                    p.Benefits
            ,
                    p.FTEStandarCost
            ,
                    p.PhysicalLocationID
            ,
                    p.SiteManager
            ,
                    p.SiteFinanceManager
            ,
                    p.ManagedGeographyID
            ,
                    p.ManagedGeographyName
            ,
                    p.ManagedSegmentID
            ,
                    p.ManagedSegmentName
            ,
                    p.ChargeOutGOC
            ,
                    p.ReceivingGOC
            ,
                    p.TotalDirectStafft
            ,
                    p.TotalTemporal
            ,
                    p.CssDriver

                }
        });


        return Content(o["item"].ToString());


    }

    [Route("getTypeInfo")]
    public ActionResult getTypeInfo(string data)
    {
        HMT2Entities DAO = new HMT2Entities();
         var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


         string pNumber = objParameter["pNumber"].ToString();
         string pRole = objParameter["pRole"].ToString();

         tblR_Role role = (from p in DAO.tblR_Role where p.RoleID == pRole select p).SingleOrDefault<tblR_Role>();

        JObject o = null;
        //select * from [dbo].[VWNM_NonMOU]
        o = JObject.FromObject(new
        {
            item =
                from p in DAO.tblR_RoleIDRoleType
                where p.RoleID == role.ID && p.Number == pNumber
                select new
                {
                    p.PosibleStartDateMonth 
            ,
                    p.PosibleStartDateYear 
            ,
                    p.PosibleEndDateYear 
            ,
                    p.PosibleEndDateMonth 
            ,
                    p.IsEndDate 
                }
        });


        return Content(o["item"].ToString());


    }

    [Route("saveChanges")]
    public ActionResult saveChangesDates(string data)
    {

        HMT2Entities DAO = new HMT2Entities();
        var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

        int StartMonth = Convert.ToInt32(objParameter["StartMonth"].ToString());
        int StartYear = Convert.ToInt32(objParameter["StartYear"].ToString());
        int EndMonth = Convert.ToInt32(objParameter["EndMonth"].ToString());
        int EndYear = Convert.ToInt32(objParameter["EndYear"].ToString());
        Boolean HaveEndDate = Convert.ToBoolean(objParameter["HaveEndDate"].ToString());
        string role = objParameter["pRole"].ToString();

        tblR_RoleIDRoleType roleType = (from p in DAO.tblR_RoleIDRoleType where p.IsActive == true && p.tblR_Role.RoleID == role select p).SingleOrDefault<tblR_RoleIDRoleType>();

        roleType.PosibleStartDateMonth = StartMonth;
        roleType.PosibleStartDateYear = StartYear;
        roleType.IsEndDate = HaveEndDate;
        if (HaveEndDate == true)
        {
            roleType.PosibleEndDateMonth = EndMonth;
            roleType.PosibleEndDateYear = EndYear;
        }
        DAO.SaveChanges();

        return Content("");
    }
    
    [Route("getTypeInfo")]
    public ActionResult getRequisitionActive(string data)
    {
        HMT2Entities DAO = new HMT2Entities();
        JObject o = null;
        try
        {
            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string pRole = objParameter["pRole"].ToString();

            tblR_Role role = (from p in DAO.tblR_Role where p.RoleID == pRole select p).SingleOrDefault<tblR_Role>();

            VWR_RolesActiveInReq req = (from p in DAO.VWR_RolesActiveInReq where p.RoleID == role.ID && p.IsActive == true select p).SingleOrDefault<VWR_RolesActiveInReq>();

            tblR_Requisition objReq = (from p in DAO.tblR_Requisition where p.ID == req.ReqID select p).SingleOrDefault<tblR_Requisition>();


            //select * from [dbo].[VWNM_NonMOU]
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWR_Requisition
                    where p.Number == objReq.Number
                    select new
                    {
                        p.Number,
                        p.Name,
                        p.DateCreated,
                        p.NumberPositions,
                        p.Clevel,
                        p.StatusID,
                        p.DaysOpen,
                        p.GOC,
                        p.TaleoID,
                        p.Expr1,
                      //  p.BusinessUnit,
                      //  p.Business,
                      //  p.BusinessDetail,
                      //  p.ManagedGeography,
                      //  p.PhysicalRegion,
                      //  p.ManagedFunction,
                      //  p.OrgLevel1,
                      //  p.OrgLevel2,
                      //  p.OrgLevel3,
                      //  p.OrgLevel4,
                      //  p.OrgLevel5,
                      //  p.OrgLevel6,
                      //  p.OrgLevel8,
                      //  p.OrgLevel7,
                      //  p.OrgLevel9,
                      //  p.OrgLevel10,
                      //  p.OrgLevel11,
                      //  p.OrgLevel12,
                      //  p.MGLevel1,
                      //  p.MGLevel2,
                      //  p.MGLevel3,
                      //  p.MGLevel4,
                      //  p.MGLevel5,
                      //  p.MGLevel6,
                      //  p.MGLevel7,
                      //  p.CSWWorkflowName,
                      //  p.SourceSystem,
                      //  p.Expr2,
                      //  p.GOCDescription,
                      //  p.Recruiter,
                        p.RecruiterGEID,
                     //   p.RelationshipIndicator,
                     //   p.Requisition_,
                     //   p.AlternateReq_,
                        p.JobCode,
                        p.JobFunction,
                        p.RequisitionTitle,
                     //   p.C_ofPositions,
                     //   p.LefttoHire,
                     //   p.CurrentReqStatus,
                     //   p.ReqStatusDate,
                     //   p.FirstApprovalDate,
                     //   p.ReqCreationDate,
                     //   p.InternalPostingStatus,
                     //   p.InternalPostingDate,
                     //   p.InternalUnpostingDate,
                     //   p.PersonReplacing,
                     //   p.PersonReplacingSOEID,
                     //   p.EmployeeReferalBonus,
                     //   p.EligibleforCampsHire,
                     //   p.HiringManagerGEID,
                     //   p.HiringManager,
                     //   p.HRRepresentative,
                     //   p.FinancialJustification,
                     //   p.RequisitionJustification,
                     //   p.IsitaProductionJob,
                     //   p.FullTimePartTime,
                     //   p.OvertimeStatus,
                     //   p.ReqSalaryGrade,
                     //   p.MappedOfficerTitle,
                     //   p.OfficerTitle,
                     //   p.OfficerTitleCode,
                     //   p.StandardGrade,
                     //   p.Grade,
                     //   p.Country,
                     //   p.WorldRegion,
                     //   p.StateProvince,
                     //   p.City,
                     //   p.CSSCenter,
                     //   p.LocationCode,
                     //   p.OLM,
                     //   p.CSC,
                     //   p.CompanyCode,
                     //   p.Expr3,
                     //   p.AgeGrouping,
                     //   p.AccountExpenseCode,
                     //   p.Applications,
                     //   p.Internal,
                     //   p.External,
                     //   p.New,
                     //   p.Testing1,
                     //   p.Reviewed,
                     //   p.PhoneScreen,
                     //   p.Testing2,
                     //   p.Interview,
                     //   p.OfferCreation,
                     //   p.OfferExtended,
                     //   p.OfferAccepted,
                     //   p.Hired,
                     //   p.OfferDeclined,
                     //   p.ReqEmployeeStatus,
                     //   p.ReqCreatorUser,
                     //   p.APACLegalVehicle,
                     //   p.SumofOpenDaysLefttoHire,
                     //   p.ID
                    }
            });
        }
        catch
        {
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWR_Requisition
                    where p.Number == "0"
                    select new
                    {
                        p.Number,
                        p.Name,
                        p.DateCreated,
                        p.NumberPositions,
                        p.Clevel,
                        p.StatusID,
                        p.DaysOpen,
                        p.GOC,
                        p.TaleoID,
                        p.Expr1,
                     //   p.BusinessUnit,
                     //   p.Business,
                     //   p.BusinessDetail,
                     //   p.ManagedGeography,
                     //   p.PhysicalRegion,
                     //   p.ManagedFunction,
                     //   p.OrgLevel1,
                     //   p.OrgLevel2,
                     //   p.OrgLevel3,
                     //   p.OrgLevel4,
                     //   p.OrgLevel5,
                     //   p.OrgLevel6,
                     //   p.OrgLevel8,
                     //   p.OrgLevel7,
                     //   p.OrgLevel9,
                     //   p.OrgLevel10,
                     //   p.OrgLevel11,
                     //   p.OrgLevel12,
                     //   p.MGLevel1,
                     //   p.MGLevel2,
                     //   p.MGLevel3,
                     //   p.MGLevel4,
                     //   p.MGLevel5,
                     //   p.MGLevel6,
                     //   p.MGLevel7,
                     //   p.CSWWorkflowName,
                     //   p.SourceSystem,
                     //   p.Expr2,
                     //   p.GOCDescription,
                     //   p.Recruiter,
                     //   p.RecruiterGEID,
                     //   p.RelationshipIndicator,
                     //   p.Requisition_,
                     //   p.AlternateReq_,
                     //   p.JobCode,
                     //   p.JobFunction,
                     //   p.RequisitionTitle,
                     //   p.C_ofPositions,
                     //   p.LefttoHire,
                     //   p.CurrentReqStatus,
                     //   p.ReqStatusDate,
                     //   p.FirstApprovalDate,
                     //   p.ReqCreationDate,
                     //   p.InternalPostingStatus,
                     //   p.InternalPostingDate,
                     //   p.InternalUnpostingDate,
                     //   p.PersonReplacing,
                     //   p.PersonReplacingSOEID,
                     //   p.EmployeeReferalBonus,
                     //   p.EligibleforCampsHire,
                     //   p.HiringManagerGEID,
                     //   p.HiringManager,
                     //   p.HRRepresentative,
                     //   p.FinancialJustification,
                     //   p.RequisitionJustification,
                     //   p.IsitaProductionJob,
                     //   p.FullTimePartTime,
                     //   p.OvertimeStatus,
                     //   p.ReqSalaryGrade,
                     //   p.MappedOfficerTitle,
                     //   p.OfficerTitle,
                     //   p.OfficerTitleCode,
                     //   p.StandardGrade,
                     //   p.Grade,
                     //   p.Country,
                     //   p.WorldRegion,
                     //   p.StateProvince,
                     //   p.City,
                     //   p.CSSCenter,
                     //   p.LocationCode,
                     //   p.OLM,
                     //   p.CSC,
                     //   p.CompanyCode,
                     //   p.Expr3,
                     //   p.AgeGrouping,
                     //   p.AccountExpenseCode,
                     //   p.Applications,
                     //   p.Internal,
                     //   p.External,
                     //   p.New,
                     //   p.Testing1,
                     //   p.Reviewed,
                     //   p.PhoneScreen,
                     //   p.Testing2,
                     //   p.Interview,
                     //   p.OfferCreation,
                     //   p.OfferExtended,
                     //   p.OfferAccepted,
                     //   p.Hired,
                     //   p.OfferDeclined,
                     //   p.ReqEmployeeStatus,
                     //   p.ReqCreatorUser,
                     //   p.APACLegalVehicle,
                     //   p.SumofOpenDaysLefttoHire,
                     //   p.ID
                    }
            });


        }

        return Content(o["item"].ToString());


    }

    [Route("getMOUNMlistFromRole")]
    public ActionResult getMOUNMlistFromRole(string data)
    {
        HMT2Entities DAO = new HMT2Entities();
        JObject o = null;

            var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string pRole = objParameter["pRole"].ToString();

            tblR_Role role = (from p in DAO.tblR_Role where p.RoleID == pRole select p).SingleOrDefault<tblR_Role>();

            //select * from [dbo].[VWNM_NonMOU]
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.procR_GetMOUNMListFromRole(role.ID)
                    select new
                    {
                        p.ID,
                        p.Alias,
                        p.TYPE,
                        p.NAME,
                        p.MANAGEDGEOGRAPHYID,
                        p.MANAGEDGEOGRAPHYNAME,
                        p.MANAGEDSEGMENTID,
                        p.MANAGEDSEGMENTNAME,
                        p.CSSDRIVER,
                        p.CreationDate 

                    }
            });


        return Content(o["item"].ToString());


    }

    [Route("getEmployeeHistory")]
    public ActionResult getEmployeeHistory(string data)
    {
        HMT2Entities DAO = new HMT2Entities();
        JObject o = null;

        var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

        string pRole = objParameter["pRole"].ToString();

        tblR_Role role = (from p in DAO.tblR_Role where p.RoleID == pRole select p).SingleOrDefault<tblR_Role>();
        tblR_RoleEmployee RoleEmployee = (from p in DAO.tblR_RoleEmployee where p.RoleID == role.ID select p).FirstOrDefault<tblR_RoleEmployee>();
        //select * from [dbo].[VWNM_NonMOU]
        o = JObject.FromObject(new
        {
            item =
                from p in DAO.tblE_EmployeeData
                where p.ID == RoleEmployee.EmployeeID 
                select new
                {
                    p.ID,
                    p.SOEID ,
                    p.FirstName,
                    p.LastName ,
                    p.IsActive,
                    p.HiringDate ,
                    p.AttritionDate,
                    p.ClassificationID

                }
        });


        return Content(o["item"].ToString());


    }


    [Route("getGOCRoleList")]
    public ActionResult getGOCRoleList()
    {
        HMT2Entities DAO = new HMT2Entities();
        JObject o = null;
       
            o = JObject.FromObject(new
            {
                item =
                    from p in DAO.VWR_GocRoleList 
                    select new
                    {
                        p.ManagedGeographyID,
                        p.ManagedGeographyName,
                        p.GOCID,
                        p.GOCName,
                        p.ManagedSegmentID,
                        p.ManagedSegmentName,
                        p.LegalVehiculeID,
                        p.LegalVehiculeName,
                        p.ManagedGeographyName_L02,
                        p.Region,
                        p.ManagedSegmentName_L06,
                        p.CustomerManagedSegment,
                        p.ManagedSegmentName_L09,
                        p.L07,
                        p.Market,
                        p.TotalRoles
                     
                    }
            });

        return Content(o["item"].ToString());


    }

    [Route("getTotalRolesListGOC")]
    public ActionResult getTotalRolesListGOC(string data)
    {
        var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        string pGOC = objParameter["pGOC"].ToString();


        HMT2Entities DAO = new HMT2Entities();

        JObject o = null;

        o = JObject.FromObject(new
        {
            item =
                from p in DAO.VWR_CompleteList
                where p.GOCID == pGOC
                select new
                {
                    p.ID
            ,
                    p.RoleID
            ,
                    p.RoleTypeID
            ,
                    p.FromType
            ,
                    p.Number
            ,
                    p.IDType
            ,
                    p.CreationDate
            ,
                    p.CreatedBy
            ,
                    p.IsActive
            ,
                    p.ModifyBy
            ,
                    p.ModifyDate
            ,
                    p.Name
            ,
                    p.Description
            ,
                    p.GOCID
            ,
                    p.MSL09
            ,
                    p.Process
            ,
                    p.SubProcess
            ,
                    p.MgdGeaography
            ,
                    p.MgdRegion
            ,
                    p.MgdCountry
            ,
                    p.LVID
            ,
                    p.PhysicalLocationID
            ,
                    p.ChargeOutProfileID
            ,
                    p.RoleIsActive
            ,
                    p.RoleCreaetedBy
            ,
                    p.CreatedDate
            ,
                    p.LastUpdateBy
            ,
                    p.LastUpdateDate
            ,
                    p.StatusID
            ,
                    p.IsFteable
            ,
                    p.IsDirectStaf
            ,
                    p.FTEPercentage
            ,
                    p.Status
            ,
                    p.ManagedSegment
            ,
                    p.ManagedGeography
            ,
                    p.PhysicalLocation
            ,
                    p.ChargeOutProfile
            ,
                    p.RoleAuto
            ,
                    p.GOCName
                }
        });


        return Content(o["item"].ToString());


    }

    [Route("getMOUNMList")]
    public ActionResult getMOUNMList()
    {
        HMT2Entities DAO = new HMT2Entities();
        JObject o = null;

        o = JObject.FromObject(new
        {
            item =
                from p in DAO.VWR_ListMOUNM 
                select new
                {
                    p.ID,
                    p.Alias,
                    p.TYPE,
                    p.NAME,
                    p.MANAGEDGEOGRAPHYID,
                    p.MANAGEDGEOGRAPHYNAME,
                    p.MANAGEDSEGMENTID,
                    p.MANAGEDSEGMENTNAME,
                    p.CSSDRIVER

                }
        });


        return Content(o["item"].ToString());


    }

    [Route("saveChanges")]
    public ActionResult saveNewRoles(string data)
    {

        HMT2Entities DAO = new HMT2Entities();
        var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

        
        string name = objParameter["pName"].ToString();
        string goc = objParameter["pGOC"].ToString();
        string pl = objParameter["pPL"].ToString();
        string ds = objParameter["pDS"].ToString();
        string fte = objParameter["pFTE"].ToString();
        int total = Convert.ToInt32(objParameter["pTotal"].ToString());
        string userSOEID = GlobalUtil.GetSOEID();

        DAO.procR_AddNewRoleByGOC(name, "", goc, pl, ds, fte, userSOEID, total);

        return Content("");
    }

    [Route("saveChanges")]
    public ActionResult LinkageNewMOU(string data)
    {

        HMT2Entities DAO = new HMT2Entities();
        var objParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


        string NUMBER = objParameter["pMOUNMnumber"].ToString();
        string ROLE = objParameter["pRole"].ToString();

        string userSOEID = GlobalUtil.GetSOEID();

        DAO.procR_LinkageRoleWithMOU(NUMBER, ROLE, userSOEID);

        return Content("");
    }

    


    }
}