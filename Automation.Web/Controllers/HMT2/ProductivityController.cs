﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Automation.Web.Models.HMT2;


using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;
using System.Reflection;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace Automation.Web.Controllers.HMT2
{
    [RoutePrefix("HMT2")]
    public class ProductivityController : Controller
    {
        [Route("Productivity")]
        public ActionResult Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            //string subAppPath = GlobalUtil.GetSubAppPath();
            //viewModel.Add("SubAppPath", subAppPath);
            return (View("~/Views/HMT2/Productivity.cshtml", viewModel));
        }

        [Route("uploadProductivityFile")]
        public ActionResult uploadProductivityFile(string data)
        {
            HMT2Entities DAO = new HMT2Entities();

            var objParameter = JsonConvert.DeserializeObject(data);

            DataTable ptable = (DataTable)JsonConvert.DeserializeObject(data, (typeof(DataTable)));

            (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[SPHMT2_UP_PRODUCTIVITY_FILE]", new List<Dictionary<string, string>> {
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEID" },
                                { "Value", GlobalUtil.GetSOEID() }
                            }
                        });

            return null;
        }
    }
}

 
