using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.AutomationFramework
{
    [RoutePrefix("AutomationFramework")]
    public class HomeController : Controller
    {
        //
        // GET: /
        [Route("/")]
        public ActionResult Index()
        {
            return View("~/Views/AutomationFramework/Home.cshtml");
        }
        //
        // GET: /
        [Route("Dashboard")]
        public ActionResult Dashboard()
        {
            return View("~/Views/Shared/Home.cshtml");
        }
        //
        // GET: /AutomationFramework/AppList

        [Route("AppList")]
        public ActionResult AppList()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            return View("~/Views/AutomationFramework/AppList.cshtml", viewModel);
        }

        //
        // GET: /AutomationFramework/ChartExamples

        [Route("ChartExamples")]
        public ActionResult ChartExamples()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            return View("~/Views/AutomationFramework/ChartExamples.cshtml", viewModel);
        }
    }
}
