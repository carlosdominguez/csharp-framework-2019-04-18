﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.CSSUniversity
{
    [RoutePrefix("CSSUniversity/Admin/UploadFeedFiles")]
    public class UploadFilesController : Controller
    {
        //
        // GET: /
        [Route("/")]
        public ActionResult Index()
        {
            return View("~/Views/CSSUniversity/UploadFiles.cshtml");
        }

        //
        // GET: /Upload
        [Route("Upload")]
        public ActionResult Upload()
        {
            Dictionary<string, object> result = new Dictionary<string, object> { { "success", true } };

            try
            {
                var objFile = GlobalUtil.GetFileFromCurrentRequest();
                string fullPathSaved;
                DataTable ptable;

                switch (Request.Params["typeUpload"])
                {
                    case "GLMS Transcript":
                        //Save excel file in the server
                        fullPathSaved = GlobalUtil.FileSaveAs("GLMSTranscript", objFile.Filename, objFile.InputStream);
                        ptable = GlobalUtil.GetDataTableFromExcel(fullPathSaved);

                        //Save data in the database
                        (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[FROU_spUploadTemplateGLMSTranscript]", new List<Dictionary<string, string>> { 
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEIDUploader" },
                                { "Value", GlobalUtil.GetSOEID() }
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@FileID" },
                                { "Value", GlobalUtil.CreateCustomID() }
                            }
                        });
                        break;

                    case "Udemy Catalog":
                        //Save excel file in the server
                        fullPathSaved = GlobalUtil.FileSaveAs("UdemyCatalog", objFile.Filename, objFile.InputStream);
                        ptable = GlobalUtil.GetDataTableFromExcel(fullPathSaved);

                        //Save data in the database
                        (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[FROU_spUploadTemplateUdemyCourses]", new List<Dictionary<string, string>> { 
                            new Dictionary<string, string>
                            {
                                { "Name", "@SOEIDUploader" },
                                { "Value", GlobalUtil.GetSOEID() }
                            },
                            new Dictionary<string, string>
                            {
                                { "Name", "@FileID" },
                                { "Value", GlobalUtil.CreateCustomID() }
                            }
                        });
                        break;
                }
            }
            catch (Exception ex)
            {
                //return new FileUploaderResult(false, error: ex.Message);
                result["success"] = false;
                result.Add("error", ex.ToString());
            }

            // the anonymous object in the result below will be convert to json and set back to the browser
            //return new FileUploaderResult(true, new { extraInformation = 12345 });

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
