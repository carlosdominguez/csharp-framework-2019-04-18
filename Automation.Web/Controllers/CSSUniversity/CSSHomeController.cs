﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.CSSUniversity
{
    [RoutePrefix("CSSUniversity")]
    public class CSSHomeController : Controller
    {
        //
        // GET: /
        [Route("/")]
        public ActionResult Index()
        {
            return View("~/Views/CSSUniversity/CSSHome.cshtml");
        }

    }
}
