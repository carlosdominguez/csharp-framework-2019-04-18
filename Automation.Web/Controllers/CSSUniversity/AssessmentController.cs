﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.CSSUniversity
{
    [RoutePrefix("CSSUniversity/Assessment")]
    public class AssessmentController : AsyncController
    {
        //
        // GET: /CSSUniversity/Assessment
        [Route("/")]
        public async Task<ActionResult> Index()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();

            //Get Encode Employee Assessment SOEID
            string HFEmpsID = (Request.QueryString["pempsid"] == null) ? "" : Request.QueryString["pempsid"];
            viewModel.Add("HFEmpsID", HFEmpsID);

            //Is view only the assessment?
            string HFViewOnly = (Request.QueryString["pvo"] == null) ? "" : Request.QueryString["pvo"];
            viewModel.Add("HFViewOnly", HFViewOnly);

            //Is view only Menu?
            string HFViewOnlyMenu = (Request.QueryString["pme"] == null) ? "" : Request.QueryString["pme"]; 
            viewModel.Add("HFViewOnlyMenu", HFViewOnlyMenu);

            //Is view other assessment?
            string HFViewOtherAssessment = (Request.QueryString["pvoa"] == null) ? "" : Request.QueryString["pvoa"]; 
            viewModel.Add("HFViewOtherAssessment", HFViewOtherAssessment);

            //Get Global Process to show other assessment
            string HFOtherIDGlobalProcess = (Request.QueryString["pidgp"] == null) ? "" : Request.QueryString["pidgp"]; 
            viewModel.Add("HFOtherIDGlobalProcess", HFOtherIDGlobalProcess);

            //Get Process Role to show other assessment
            string HFOtherIDProcessRole = (Request.QueryString["pidpr"] == null) ? "" : Request.QueryString["pidpr"]; 
            viewModel.Add("HFOtherIDProcessRole", HFOtherIDProcessRole);

            //Get Job Level to show other assessment
            string HFOtherIDJobLevel = (Request.QueryString["pidjl"] == null) ? "" : Request.QueryString["pidjl"];
            viewModel.Add("HFOtherIDJobLevel", HFOtherIDJobLevel);

            //Get Global Process Name to show other assessment
            string HFOtherGlobalProcess = (Request.QueryString["pidgpd"] == null) ? "" : Request.QueryString["pidgpd"];
            viewModel.Add("HFOtherGlobalProcess", HFOtherGlobalProcess);

            //Get Process Role Name to show other assessment
            string HFOtherProcessRole = (Request.QueryString["pidprd"] == null) ? "" : Request.QueryString["pidprd"]; 
            viewModel.Add("HFOtherProcessRole", HFOtherProcessRole);

            //Get Job Level Name to show other assessment
            string HFOtherJobLevel = (Request.QueryString["pidjld"] == null) ? "" : Request.QueryString["pidjld"];
            viewModel.Add("HFOtherJobLevel", HFOtherJobLevel);

            //Load information of employees
            string psoeidEmployee = GlobalUtil.DecodeAsciiString(HFEmpsID);
            var FROU_spAssessmentGetInfoEmployee = "FROU_spAssessmentGetInfoEmployee";
            var dataEmployee = (new GlobalModel()).excecuteProcedureReturn(FROU_spAssessmentGetInfoEmployee, new List<Dictionary<string, string>> { 
                new Dictionary<string, string>
                {
                    { "Name", "@SOEID" },
                    { "Value", psoeidEmployee }
                }
            })[0];

            viewModel.Add("lblEmployeeName", dataEmployee["EmployeeName"]);
            viewModel.Add("lblEmployeeSOEID", dataEmployee["EmployeeSOEID"]);
            viewModel.Add("lblManagerName", dataEmployee["ManagerName"]);
            viewModel.Add("lblManagerSOEID", dataEmployee["ManagerSOEID"]);

            viewModel.Add("HFCurrentIDGlobalProcess", dataEmployee["IDGlobalProcess"]);
            viewModel.Add("HFCurrentIDProcessRole", dataEmployee["IDProcessRole"]);
            viewModel.Add("HFCurrentIDJobLevel", dataEmployee["IDJobLevel"]);

            viewModel.Add("HFCurrentGlobalProcess", dataEmployee["GlobalProcess"]);
            viewModel.Add("HFCurrentProcessRole", dataEmployee["ProcessRole"]);
            viewModel.Add("HFCurrentJobLevel", dataEmployee["JobLevel"]);

            viewModel.Add("txtDiscussionDate", dataEmployee["LastDiscussionDate"]);
            viewModel.Add("lblAssessmentPorcent", dataEmployee["AssessmentPorcent"] + "%");
            viewModel.Add("lblCompletedTrainings", dataEmployee["CompletedTrainings"]);
            viewModel.Add("lblPendingTrainings", dataEmployee["PendingTrainings"]);
            viewModel.Add("lblDueDatePast", " (" + dataEmployee["DueDatePast"] + " overdue)");
            viewModel.Add("lblCompletionPercentage", dataEmployee["CompletionPercentage"] + " %");

            return await Task.Run(() => View("~/Views/CSSUniversity/Assessment.cshtml", viewModel));
        }
    }
}
