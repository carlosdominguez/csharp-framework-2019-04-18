using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.SupportDoc
{
    [RoutePrefix("SupportDoc")]
    public class SupportDocController : Controller
    {
        // ROUTE METHODS
        // ----------------------------------------------------------
        //
        // GET: /SupportDoc/DocumentList
        [Route("DocumentList")]
        public ActionResult DocumentList()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            var pviewType = (String.IsNullOrEmpty(Request.QueryString.Get("pviewType"))) ? "" : Request.QueryString.Get("pviewType");
            var pempsID = (String.IsNullOrEmpty(Request.QueryString.Get("pempsid"))) ? "" : Request.QueryString.Get("pempsid");
            var pidRegion = (String.IsNullOrEmpty(Request.QueryString.Get("pidRegion"))) ? "" : Request.QueryString.Get("pidRegion");
            var pyear = (String.IsNullOrEmpty(Request.QueryString.Get("pyear"))) ? "" : Request.QueryString.Get("pyear");
            var pmonth = (String.IsNullOrEmpty(Request.QueryString.Get("pmonth"))) ? "" : Request.QueryString.Get("pmonth");
            var pnotificationStatus = (String.IsNullOrEmpty(Request.QueryString.Get("notificationStatus"))) ? "" : Request.QueryString.Get("notificationStatus");
            var soeidResponsible = GlobalUtil.DecodeAsciiString(pempsID);
            if (String.IsNullOrEmpty(soeidResponsible))
            {
                soeidResponsible = GlobalUtil.GetSOEID();
            }

            viewModel.Add("HFViewType", pviewType);
            viewModel.Add("HFIDRegion", pidRegion);
            viewModel.Add("HFYear", pyear);
            viewModel.Add("HFMonth", pmonth);
            viewModel.Add("HFSOEIDResponsible", soeidResponsible);
            viewModel.Add("HFNotificationStatus", pnotificationStatus);
            
            return View("~/Views/SupportDoc/DocumentList.cshtml", viewModel);
        }

        //
        // GET: /SupportDoc/DocumentUpload
        [Route("DocumentUpload")]
        public ActionResult DocumentUpload()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            var basePath = GlobalUtil.GetValueAppConfig("appSettings", "ShareFolderMexico");
            var pempsID = (String.IsNullOrEmpty(Request.QueryString.Get("pempsid"))) ? "" : Request.QueryString.Get("pempsid");
            var pyear = (String.IsNullOrEmpty(Request.QueryString.Get("pyear"))) ? "" : Request.QueryString.Get("pyear");
            var pmonth = (String.IsNullOrEmpty(Request.QueryString.Get("pmonth"))) ? "" : Request.QueryString.Get("pmonth");
            var pnotificationStatus = (String.IsNullOrEmpty(Request.QueryString.Get("notificationStatus"))) ? "" : Request.QueryString.Get("notificationStatus");
            var soeidResponsible = GlobalUtil.DecodeAsciiString(pempsID);

            if (String.IsNullOrEmpty(soeidResponsible))
            {
                soeidResponsible = GlobalUtil.GetSOEID();
            }

            viewModel.Add("HFBasePath", basePath);
            viewModel.Add("HFYear", pyear);
            viewModel.Add("HFMonth", pmonth);
            viewModel.Add("HFSOEIDResponsible", soeidResponsible);
            viewModel.Add("HFNotificationStatus", pnotificationStatus);

            return View("~/Views/SupportDoc/DocumentUpload.cshtml", viewModel);
        }

        //
        // GET: /SupportDoc/DocumentEscalation
        [Route("DocumentEscalation")]
        public ActionResult DocumentEscalation()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            var basePath = GlobalUtil.GetValueAppConfig("appSettings", "ShareFolderMexico");
            var basePathExport = GlobalUtil.GetValueAppConfig("appSettings", "ShareFolderMexicoExport");
            
            viewModel.Add("HFBasePath", basePath);
            viewModel.Add("HFBasePathExport", basePathExport);
            
            return View("~/Views/SupportDoc/DocumentEscalation.cshtml", viewModel);
        }

        //
        // GET: /SupportDoc/DocumentAdmin
        [Route("DocumentAdmin")]
        public ActionResult DocumentAdmin()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            return View("~/Views/SupportDoc/DocumentAdmin.cshtml", viewModel);
        }

        //
        // GET: /SupportDoc/RegionAdmin
        [Route("RegionAdmin")]
        public ActionResult RegionAdmin()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            return View("~/Views/SupportDoc/RegionAdmin.cshtml", viewModel);
        }


        //
        // GET: /SupportDoc/Authentication
        [Route("Authentication")]
        public ActionResult Authentication()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            viewModel.Add("WindowsIdentity", System.Security.Principal.WindowsIdentity.GetCurrent().Name);
            return View("~/Views/SupportDoc/Authentication.cshtml", viewModel);
        }

        //
        // GET: /SupportDoc/AuditLog
        [Route("AuditLog")]
        public ActionResult AuditLog()
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            return View("~/Views/SupportDoc/AuditLog.cshtml", viewModel);
        }

        // AJAX METHODS
        // ----------------------------------------------------------
        //
        // GET: /SupportDoc/CheckAccess
        [Route("CheckAccess")]
        public ActionResult CheckAccess(string typeAuthentication, string user, string pass, string pathToTest)
        {
            user = GlobalUtil.DecodeSkipSideminder(user);
            pass = GlobalUtil.DecodeAsciiString(pass);
            pathToTest = GlobalUtil.DecodeSkipSideminder(pathToTest);

            var resulMsg = "Pending"; 

            if (typeAuthentication == "WindowsAuthentication")
            {
                var userData = user.Split('\\');
                using (UserImpersonation userImperso = new UserImpersonation(userData[1].ToUpper(), userData[0].ToUpper(), pass))
                {
                    if (userImperso.ImpersonateValidUser())
                    {
                        try
                        {
                            resulMsg = "Success! The user '" + user + "' has access to the " + GlobalUtil.CheckPathIsFileOrDirectory(pathToTest);
                        }
                        catch (Exception e)
                        {
                            if (e.Message.Contains("Access to the path") && e.Message.Contains("is denied"))
                            {
                                resulMsg = "Access to the path '" + pathToTest + "' is denied for user '" + user + "'";
                            }
                            else
                            {
                                throw e;
                            }
                        }
                    }
                    else
                    {
                        resulMsg = "User or Password incorrect";
                    }
                }
            }
            else
            {
                user = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                try
                {
                    resulMsg = "Success! The user '" + user + "' has access to the " + GlobalUtil.CheckPathIsFileOrDirectory(pathToTest);
                }
                catch (Exception e)
                {
                    if (e.Message.Contains("Access to the path") && e.Message.Contains("is denied"))
                    {
                        resulMsg = "Access to the path '" + pathToTest + "' is denied for user '" + user + "'";
                    }
                    else
                    {
                        throw e;
                    }
                }
            }

            return Content(resulMsg);
        }

        //
        // GET: /SupportDoc/Upload
        [Route("Upload")]
        public ActionResult Upload(string idRegion, string year, string month, string locationToSave)
        {
            Dictionary<string, object> result = new Dictionary<string, object> { { "success", true } };

            try
            {
                var objFile = GlobalUtil.GetFileFromCurrentRequest(false);
                
                //Copy file to share folder
                var fullPathSaved = GlobalUtil.FileSaveAs(locationToSave, objFile.Filename, objFile.InputStream, false);

                //Update Path File Received, create DataTable with files
                DataTable dt = new DataTable();
                dt.Columns.Add("FilePath");
                dt.Columns.Add("FileExists");

                FileInfo fileInfo = new FileInfo(fullPathSaved);

                DataRow tempRow = dt.NewRow();
                tempRow["FilePath"] = fullPathSaved;
                tempRow["FileExists"] = (fileInfo.Exists) ? "1" : "0";
                dt.Rows.Add(tempRow);

                //Save data in the database
                (new GlobalModel()).SaveDataTableDB(dt, "[dbo].[spSDocUploadCheckFilesStatus]", new List<Dictionary<string, string>> { 
                    new Dictionary<string, string>
                    {
                        { "Name", "@SOEIDUploader" },
                        { "Value", GlobalUtil.GetSOEID() }
                    },
                    new Dictionary<string, string>
                    {
                        { "Name", "@IDRegion" },
                        { "Value", idRegion }
                    },
                    new Dictionary<string, string>
                    {
                        { "Name", "@Year" },
                        { "Value", year }
                    },
                    new Dictionary<string, string>
                    {
                        { "Name", "@Month" },
                        { "Value", month }
                    },
                    new Dictionary<string, string>
                    {
                        { "Name", "@IsUpload" },
                        { "Value", "1" }
                    }
                });

                result.Add("fullPathSaved", fullPathSaved);
            }
            catch (Exception ex)
            {
                //return new FileUploaderResult(false, error: ex.Message);
                result["success"] = false;
                result.Add("error", ex.ToString());
            }

            // the anonymous object in the result below will be convert to json and set back to the browser
            //return new FileUploaderResult(true, new { extraInformation = 12345 });

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /SupportDoc/CheckDocumentsByResponsible
        [Route("CheckDocumentsByResponsible")]
        public ActionResult CheckDocumentsByResponsible(string year, string month, string responsibleSOEID, string notificationStatus, string typeAuthentication, string user, string pass)
        {
            string resultMsg = "No data found to check file status";

            user = GlobalUtil.DecodeSkipSideminder(user);
            pass = GlobalUtil.DecodeAsciiString(pass);

            //Create DataTable with files
            DataTable dt = null;

            var whereSQL = "";
            if (!String.IsNullOrEmpty(notificationStatus))
            {
                whereSQL = "WHERE ( [Year] = " + year + " AND [Month] = " + month + " AND [NotificationStatus] ='" + notificationStatus + "' AND [ResponsilbleSOEID] LIKE '%" + responsibleSOEID + "%')";
            }
            else
            {
                whereSQL = "WHERE ( [Year] = " + year + " AND [Month] = " + month + " AND [ResponsilbleSOEID] LIKE '%" + responsibleSOEID + "%')";
            }

            var docList = (new GlobalModel()).excecuteProcedureReturn("[dbo].[spSDocPeriodListPaging]", new List<Dictionary<string, string>> { 
                new Dictionary<string, string>
                {
                    { "Name", "@StrWhere" },
                    { "Value",  whereSQL }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@EndIndexRow" },
                    { "Value", "100000" }
                } 
            });

            //Validate Authentication to scan folder
            if (typeAuthentication == "WindowsAuthentication")
            {
                var userData = user.Split('\\');
                using (UserImpersonation userImperso = new UserImpersonation(userData[1].ToUpper(), userData[0].ToUpper(), pass))
                {
                    if (userImperso.ImpersonateValidUser())
                    {
                        try
                        {
                            dt = scanFiles(docList);
                        }
                        catch (Exception e)
                        {
                            if (e.Message.Contains("Access to the path") && e.Message.Contains("is denied"))
                            {
                                resultMsg = "Access to the Share Folder is denied for user '" + user + "', please go to then menu in 'Authentication' and set a valid configuration.";
                            }
                            else
                            {
                                throw e;
                            }
                        }
                    }
                    else
                    {
                        resultMsg = "User or Password incorrect";
                    }
                }
            }
            else
            {
                user = user = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                try
                {
                    dt = scanFiles(docList);
                }
                catch (Exception e)
                {
                    if (e.Message.Contains("Access to the path") && e.Message.Contains("is denied"))
                    {
                        resultMsg = "Access to the Share Folder is denied for user '" + user + "', please go to then menu in 'Authentication' and set a valid configuration.";
                    }
                    else
                    {
                        throw e;
                    }
                }
            }

            //Save data in the database
            if (dt != null)
            {
                (new GlobalModel()).SaveDataTableDB(dt, "[dbo].[spSDocUploadCheckFilesStatus]", new List<Dictionary<string, string>> { 
                    new Dictionary<string, string>
                    {
                        { "Name", "@SOEIDUploader" },
                        { "Value", GlobalUtil.GetSOEID() }
                    },
                    new Dictionary<string, string>
                    {
                        { "Name", "@Year" },
                        { "Value", year }
                    },
                    new Dictionary<string, string>
                    {
                        { "Name", "@Month" },
                        { "Value", month }
                    }
                });

                if (docList.Count > 0)
                {
                    resultMsg = docList.Count + " records with " + dt.Rows.Count + " files checked for " + (new DateTime(Int32.Parse(year), Int32.Parse(month), 1)).ToString("MMMM yyyy");
                }
            }

            return Content(resultMsg);
        }

        //
        // GET: /SupportDoc/CheckFilesInShareDrive
        [Route("CheckFilesInShareDrive")]
        public ActionResult CheckFilesInShareDrive(string proofOwnerSOEID, string responsibleSOEID, string idRegion, string year, string month, string typeAuthentication, string user, string pass)
        {
            string resultMsg = "No data found to check file status";
            string strWhere = "WHERE [IDRegion] = " + idRegion + " AND [Year] = " + year + " AND [Month] = " + month + " ";

            user = GlobalUtil.DecodeSkipSideminder(user);
            pass = GlobalUtil.DecodeAsciiString(pass);

            //Create DataTable with files
            DataTable dt = null;

            //When exists responsible
            if (!String.IsNullOrEmpty(responsibleSOEID))
            {
                strWhere += " AND [ResponsibleSOEID] LIKE '%" + responsibleSOEID + "%' ";
            }

            //When exists Proof Owner
            if (!String.IsNullOrEmpty(proofOwnerSOEID))
            {
                strWhere += " AND [ProofOwnerSOEID] LIKE '%" + proofOwnerSOEID + "%' ";
            }

            var docList = (new GlobalModel()).excecuteProcedureReturn("[dbo].[spSDocPeriodListPaging]", new List<Dictionary<string, string>> { 
                new Dictionary<string, string>
                {
                    { "Name", "@StrWhere" },
                    { "Value", strWhere }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@EndIndexRow" },
                    { "Value", "99000000" }
                } 
            });

            //Validate Authentication to scan folder
            if (typeAuthentication == "WindowsAuthentication")
            {
                var userData = user.Split('\\');
                using (UserImpersonation userImperso = new UserImpersonation(userData[1].ToUpper(), userData[0].ToUpper(), pass))
                {
                    if (userImperso.ImpersonateValidUser())
                    {
                        try
                        {
                            dt = scanFiles(docList);
                        }
                        catch (Exception e)
                        {
                            if (e.Message.Contains("Access to the path") && e.Message.Contains("is denied"))
                            {
                                resultMsg = "Access to the Share Folder is denied for user '" + user + "', please go to then menu in 'Authentication' and set a valid configuration.";
                            }
                            else
                            {
                                throw e;
                            }
                        }
                    }
                    else
                    {
                        resultMsg = "User or Password incorrect";
                    }
                }
            }
            else
            {
                user = user = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                try
                {
                    dt = scanFiles(docList);
                }
                catch (Exception e)
                {
                    if (e.Message.Contains("Access to the path") && e.Message.Contains("is denied"))
                    {
                        resultMsg = "Access to the Share Folder is denied for user '" + user + "', please go to then menu in 'Authentication' and set a valid configuration.";
                    }
                    else
                    {
                        throw e;
                    }
                }
            }

            //Save data in the database
            if (dt != null)
            {
                (new GlobalModel()).SaveDataTableDB(dt, "[dbo].[spSDocUploadCheckFilesStatus]", new List<Dictionary<string, string>> { 
                    new Dictionary<string, string>
                    {
                        { "Name", "@SOEIDUploader" },
                        { "Value", GlobalUtil.GetSOEID() }
                    },
                    new Dictionary<string, string>
                    {
                        { "Name", "@IDRegion" },
                        { "Value", idRegion }
                    },
                    new Dictionary<string, string>
                    {
                        { "Name", "@Year" },
                        { "Value", year }
                    },
                    new Dictionary<string, string>
                    {
                        { "Name", "@Month" },
                        { "Value", month }
                    }
                });

                if (docList.Count > 0)
                {
                    resultMsg = docList.Count + " records with " + dt.Rows.Count + " files checked for " + (new DateTime(Int32.Parse(year), Int32.Parse(month), 1)).ToString("MMMM yyyy"); 
                }
            }

            return Content(resultMsg);
        }

        //
        // GET: /SupportDoc/CheckDocuments
        [Route("CheckDocuments")]
        public ActionResult CheckDocuments(string year, string month)
        {
            var basePath = GlobalUtil.GetValueAppConfig("appSettings", "ShareFolderMexico");
            string[] filesArray = null;

            using (UserImpersonation user = new UserImpersonation("CD25867", "LAC", "XXXXXX"))
            {
                if (user.ImpersonateValidUser())
                {
                    filesArray = Directory.GetFiles(@"" + basePath, "*.*", SearchOption.AllDirectories);

                    //Create DataTable with files
                    DataTable dt = new DataTable();
                    dt.Columns.Add("FilePath");

                    for (int i = 0; i < filesArray.Length; i++)
                    {
                        DataRow tempRow = dt.NewRow();
                        tempRow["FilePath"] = filesArray[i];
                        dt.Rows.Add(tempRow);
                    }

                    //Save data in the database
                    (new GlobalModel()).SaveDataTableDB(dt, "[dbo].[spSDocUploadCheckFilesStatus]", new List<Dictionary<string, string>> { 
                        new Dictionary<string, string>
                        {
                            { "Name", "@SOEIDUploader" },
                            { "Value", GlobalUtil.GetSOEID() }
                        },
                        new Dictionary<string, string>
                        {
                            { "Name", "@Year" },
                            { "Value", year }
                        },
                        new Dictionary<string, string>
                        {
                            { "Name", "@Month" },
                            { "Value", month }
                        }
                    });
                }
            }

            return Content("Files found: " + filesArray.Length);
        }

        //
        // GET: /SupportDoc/ExportDocuments
        [Route("ExportDocuments")]
        public ActionResult ExportDocuments(string year, string month, string typeAuthentication, string user, string pass)
        {
            Dictionary<string, string> responseResult = new Dictionary<string, string> { 
                { "success", "1" },
                { "msg", "" }
            };
            user = GlobalUtil.DecodeSkipSideminder(user);
            pass = GlobalUtil.DecodeAsciiString(pass);

            var docListToExport = (new GlobalModel()).excecuteProcedureReturn("[dbo].[spSDocPeriodListPaging]", new List<Dictionary<string, string>> { 
                new Dictionary<string, string>
                {
                    { "Name", "@StrWhere" },
                    { "Value", "WHERE ( [Year] = " + year + " AND [Month] = " + month + " AND [FileStatus] = 'Received' AND [StatusByPO] = 'Approved')" }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@EndIndexRow" },
                    { "Value", "100000" }
                } 
            });

            //Validate Authentication to scan folder
            if (typeAuthentication == "WindowsAuthentication")
            {
                var userData = user.Split('\\');
                using (UserImpersonation userImperso = new UserImpersonation(userData[1].ToUpper(), userData[0].ToUpper(), pass))
                {
                    if (userImperso.ImpersonateValidUser())
                    {
                        try
                        {
                            responseResult["msg"] = exportFiles(docListToExport);
                        }
                        catch (Exception e)
                        {
                            responseResult["success"] = "0";
                            if (e.Message.Contains("Access to the path") && e.Message.Contains("is denied"))
                            {
                                responseResult["msg"] = "Access to the Share Folder is denied for user '" + user + "', please go to then menu in 'Authentication' and set a valid configuration.";
                            }
                            else
                            {
                                throw e;
                            }
                        }
                    }
                    else
                    {
                        responseResult["success"] = "0";
                        responseResult["msg"] = "User or Password incorrect";
                    }
                }
            }
            else
            {
                user = user = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                try
                {
                    responseResult["msg"] = exportFiles(docListToExport);
                }
                catch (Exception e)
                {
                    responseResult["success"] = "0";
                    if (e.Message.Contains("Access to the path") && e.Message.Contains("is denied"))
                    {
                        responseResult["msg"] = "Access to the Share Folder is denied for user '" + user + "', please go to then menu in 'Authentication' and set a valid configuration.";
                    }
                    else
                    {
                        throw e;
                    }
                }
            }

            return Json(responseResult, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /SupportDoc/DownloadDoc
        [Route("DownloadDoc")]
        public void DownloadDoc(string docShareFolderFilePath, string fileName, string ext, string typeAuthentication, string user, string pass)
        {
            string fullPathToCopy = "";
            string hrefPath = "";
            user = GlobalUtil.DecodeSkipSideminder(user);
            pass = GlobalUtil.DecodeAsciiString(pass);

            docShareFolderFilePath = GlobalUtil.DecodeSkipSideminder(docShareFolderFilePath);
            fileName = GlobalUtil.DecodeSkipSideminder(fileName);

            //SET UP PATHS AND FILES NAMES
            string uploadFolder = GlobalUtil.GetValueAppConfig("appSettings", "UploadFolder") + "TempDownloads" + @"\";
            hrefPath = uploadFolder + fileName;
            fullPathToCopy = @"" + Server.MapPath(uploadFolder) + fileName;

            //DELETE OLD FILES STORED ON THE DOWNLOADFOLDER
            System.IO.DirectoryInfo di = new DirectoryInfo(@"" + Server.MapPath(uploadFolder));
            foreach (FileInfo file in di.GetFiles())
            {
                if (!file.Name.Contains("TempPublish"))
                {
                    file.Delete();
                }
            }

            //Validate Authentication to scan folder
            if (typeAuthentication == "WindowsAuthentication")
            {
                var userData = user.Split('\\');
                using (UserImpersonation userImperso = new UserImpersonation(userData[1].ToUpper(), userData[0].ToUpper(), pass))
                {
                    if (userImperso.ImpersonateValidUser())
                    {
                        try
                        {
                            //COPY NEW FILE FRO SHAREDRIVE TO DOWNLOAD FOLDER
                            System.IO.File.Copy(docShareFolderFilePath, fullPathToCopy, true);
                        }
                        catch (Exception e)
                        {
                            if (e.Message.Contains("Access to the path") && e.Message.Contains("is denied"))
                            {
                                throw new Exception("Access to the Share Folder is denied for user '" + user + "', please go to then menu in 'Authentication' and set a valid configuration.");
                            }
                            else
                            {
                                throw e;
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("User or Password incorrect");
                    }
                }
            }
            else
            {
                user = user = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                try
                {
                    //COPY NEW FILE FRO SHAREDRIVE TO DOWNLOAD FOLDER
                    System.IO.File.Copy(docShareFolderFilePath, fullPathToCopy, true);
                }
                catch (Exception e)
                {
                    if (e.Message.Contains("Access to the path") && e.Message.Contains("is denied"))
                    {
                        throw new Exception("Access to the Share Folder is denied for user '" + user + "', please go to then menu in 'Authentication' and set a valid configuration.");
                    }
                    else
                    {
                        throw e;
                    }
                }
            }

            //Write it back to the client
            //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=" + fileName);

            Response.WriteFile(fullPathToCopy);
            Response.Flush();
            Response.End();
            //Response.BinaryWrite(pck.GetAsByteArray());

            //return Content(hrefPath);
        }

        //
        // GET: /SupportDoc/UploadDocData
        [Route("UploadDocData")]
        public ActionResult UploadDocData(string pjsonData, string pidRegion, string pyear, string pmonth)
        {
            DataTable ptable = (DataTable)JsonConvert.DeserializeObject(pjsonData, (typeof(DataTable)));
            
            //Save data in the database
            (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[spSDocUploadDocInfo]", new List<Dictionary<string, string>> { 
                new Dictionary<string, string>
                {
                    { "Name", "@SOEIDUploader" },
                    { "Value", GlobalUtil.GetSOEID() }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@FileID" },
                    { "Value", GlobalUtil.CreateCustomID() }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@IDRegion" },
                    { "Value", pidRegion }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@Year" },
                    { "Value", pyear }
                },
                new Dictionary<string, string>
                {
                    { "Name", "@Month" },
                    { "Value", pmonth }
                }
            });

            return Content("Uploaded");
        }
        
        // CLASS METHODS
        // ----------------------------------------------------------
        //
        public DataTable scanFiles(List<Dictionary<string, string>> docList)
        {
            var distinctDocs = new Dictionary<string, string>();
            var checkedExceptionNoAccess = false;

            //Create DataTable with files
            DataTable dt = new DataTable();
            dt.Columns.Add("FilePath");
            dt.Columns.Add("FileExists");

            foreach (var docItem in docList)
            {
                string fileFullPath = docItem["FileFullPath"];

                //Only if not exists document to avoid duplicates
                if (!distinctDocs.ContainsKey(fileFullPath))
                {
                    try
                    {
                        FileInfo fileInfo = new FileInfo(fileFullPath);

                        if (!checkedExceptionNoAccess)
                        {
                            //Throw no access exception
                            var isReadOnly = fileInfo.IsReadOnly;
                            checkedExceptionNoAccess = true;
                        }

                        //Get if FilePath exists
                        DataRow tempRow = dt.NewRow();
                        tempRow["FilePath"] = fileFullPath;
                        tempRow["FileExists"] = (fileInfo.Exists) ? "1" : "0";
                        dt.Rows.Add(tempRow);

                        distinctDocs.Add(fileFullPath, "1");
                    }
                    catch(Exception e)
                    {
                        string eMsg = "PathIssue: '" + fileFullPath + "' " + System.Environment.NewLine + System.Environment.NewLine;
                        eMsg = eMsg + e.Message;
                        var CustomEx = new Exception(eMsg);
                        //GlobalUtil.SaveExceptionInDB(CustomEx);
                        throw CustomEx;
                    }
                }
            }

            return dt;
        }

        public string exportFiles(List<Dictionary<string, string>> docList)
        {
            string docsPathCopied = "";
            var distinctDocs = new Dictionary<string, string>();
            
            foreach (var docItem in docList)
            {
                string fileFullPathExport = docItem["RegionFolderPath"] + @"Approved\" + docItem["FilePath"];

                //Only if not exists document to avoid duplicates
                if (!distinctDocs.ContainsKey(fileFullPathExport))
                {
                    FileInfo fileInfo = new FileInfo(fileFullPathExport);
                    Directory.CreateDirectory(fileInfo.Directory.FullName);

                    //Export fiile to new location
                    System.IO.File.Copy(docItem["FileFullPath"], fileFullPathExport, true);
                    docsPathCopied += fileFullPathExport + "\n";

                    distinctDocs.Add(fileFullPathExport, "1");
                }
            }

            return docsPathCopied;
        }
    }
}
