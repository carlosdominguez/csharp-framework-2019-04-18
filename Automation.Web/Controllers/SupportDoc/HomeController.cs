using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Automation.Web.Controllers.SupportDoc
{
    [RoutePrefix("SupportDoc")]
    public class HomeController : Controller
    {
        //
        // GET: /
        [Route("/")]
        public ActionResult Index()
        {
            //Show Document Upload as default page
            //return RedirectToAction("DocumentUpload", "SupportDoc");
            return View("~/Views/SupportDoc/Home.cshtml");
        }
    }
}
