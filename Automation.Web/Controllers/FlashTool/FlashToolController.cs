using AttributeRouting;
using AttributeRouting.Web.Mvc;
using Automation.Core.Class;
using Automation.Core.JqxGrid;
using Automation.Core.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Globalization;

namespace Automation.Web.Controllers.FlashTool
{
    [RoutePrefix("FlashTool")]
    public class FlashToolController : Controller
    {
        //
        // GET: /FlashTool/TemplateStatus/
        [Route("TemplateStatus")]
        public ActionResult Index()
        {
            return View("~/Views/FlashTool/TemplateStatus.cshtml");
        }

        // GET: /FlashTool/FlashCategory/
        [Route("FlashCategory")]
        public ActionResult FlashCategory()
        {
            return View("~/Views/FlashTool/FlashCategory.cshtml");
        }

        // GET: /FlashTool/FlashCategoryDetail/
        [Route("FlashCategoryDetail")]
        public ActionResult FlashCategoryDetail()
        {
            return View("~/Views/FlashTool/FlashCategoryDetail.cshtml");
        }
        
        // GET: /FlashTool/FlashAdminConsole/
        [Route("FlashTemplate")]
        public ActionResult FlashAdminConsole()
        {
            return View("~/Views/FlashTool/FlashAdminConsole.cshtml");
        }
        // GET: /FlashTool/FlashBridge/
        [Route("FlashTemplate")]
        public ActionResult FlashBridge()
        {
            return View("~/Views/FlashTool/FlashBridge.cshtml");
        }
        // GET: /FlashTool/FlashBridgeConsolidate/
        [Route("FlashTemplate")]
        public ActionResult FlashBridgeConsolidate()
        {
            return View("~/Views/FlashTool/FlashBridgeConsolidate.cshtml");
        }
        // GET: /FlashTool/Fp&A/
        [Route("FlashTemplate")]
        public ActionResult FlashFpa()
        {
            return View("~/Views/FlashTool/FlashFpa.cshtml");
        }
        // GET: /FlashTool/CurrentActual/
        [Route("FlashTemplate")]
        public ActionResult FlashCurrentActual()
        {
            return View("~/Views/FlashTool/FlashCurrentActual.cshtml");
        }

        // GET: /FlashTool/FlashCustomSegmentsNames/
        [Route("FlashTemplate")]
        public ActionResult FlashCustomSegmentsNames()
        {
            return View("~/Views/FlashTool/FlashCustomSegmentsNames.cshtml");
        }

        // GET: /FlashTool/FlashEditSegments/
        [Route("FlashTemplate")]
        public ActionResult FlashEditSegments()
        {
            return View("~/Views/FlashTool/FlashEditSegments.cshtml");
        }

        // GET: /FlashTool/FlashListSegments/
        [Route("FlashTemplate")]
        public ActionResult FlashListSegments()
        {
            return View("~/Views/FlashTool/FlashListSegments.cshtml");
        }

        // GET: /FlashTool/FlashFullReports/
        [Route("FlashFullReports")]
        public ActionResult FlashFullReportsSegments()
        {
            return View("~/Views/FlashTool/FlashFullReports.cshtml");
        }

        // GET: /FlashTool/DeleteRAWDATA/
        [Route("FlashFullReports")]
        public ActionResult FlashDeleteRawData()
        {
            return View("~/Views/FlashTool/FlashDeleteRawData.cshtml");
        }

        [Route("FlashTemplate")]
        public void DeleteFilesInFolders(string FilePath)
        {
            
            //SET UP PATHS AND FILES NAMES
            string fullPathToCopy = "";
            string hrefPath = "";
            string uploadFolder = GlobalUtil.GetValueAppConfig("appSettings", "UploadFolder") + FilePath + @"/";
            hrefPath = uploadFolder;
            fullPathToCopy = @"" + Server.MapPath(uploadFolder);
            ////DELETE OLD FILES STORED ON THE DOWNLOADFOLDER
            System.IO.DirectoryInfo di = new DirectoryInfo(@"" + Server.MapPath(uploadFolder));
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            
        }
        //
        // GET: /FlashTool/Upload
        [Route("Upload")]
        public ActionResult Upload()
        {
            Dictionary<string, object> result = new Dictionary<string, object> { { "success", true } };

            try
            {
                var objFile = GlobalUtil.GetFileFromCurrentRequest();
               

                string fullPathSaved;
                DataTable ptable;
                var typeFile = Request.Params["typeFile_" + Request.Params["qquuid"]];
                    
                switch (typeFile)
                {
                    case "COA":
                        //Save excel file in the server
                        fullPathSaved = GlobalUtil.FileSaveAs("COA", objFile.Filename, objFile.InputStream);
                        ptable = GlobalUtil.GetDataTableFromExcel(fullPathSaved, "P2P CoA_Table");
                        ptable.Rows[0].Delete();
                        ptable.Rows[1].Delete();
                        ptable.Columns.RemoveAt(0);
                        ptable.Columns.RemoveAt(17);
                        ptable.AcceptChanges();

                        //Save data in the database
                        (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].spFlashImportCOA_type", new List<Dictionary<string,string>>
                            {
                                    new Dictionary<string, string>
                                    {
                                        { "Name", "@SOEIDUploader" },
                                        { "Value", GlobalUtil.GetSOEID() }
                                    }
                            }
                        );
                        
                        DeleteFilesInFolders("/COA");
                        break;
                    case "DSMT":
                        fullPathSaved = GlobalUtil.FileSaveAs("DSMT", objFile.Filename, objFile.InputStream);
                        ptable = GlobalUtil.GetDataTableFromExcel(fullPathSaved, "Sheet1");

                        //Fix Double DataType in Column that must be String
                        DataTable dtDataTypeFix = ptable.Clone();
                        dtDataTypeFix.Columns["DSMT_TREENODE_L1"].DataType = typeof(String);
                        dtDataTypeFix.Columns["DSMT_TREENODE_L2"].DataType = typeof(String);
                        dtDataTypeFix.Columns["DSMT_TREENODE_L3"].DataType = typeof(String);
                        dtDataTypeFix.Columns["DSMT_TREENODE_L4"].DataType = typeof(String);
                        dtDataTypeFix.Columns["DSMT_TREENODE_L5"].DataType = typeof(String);
                        dtDataTypeFix.Columns["DSMT_TREENODE_L6"].DataType = typeof(String);
                        dtDataTypeFix.Columns["DSMT_TREENODE_L7"].DataType = typeof(String);
                        dtDataTypeFix.Columns["DSMT_TREENODE_L8"].DataType = typeof(String);
                        dtDataTypeFix.Columns["DSMT_TREENODE_L9"].DataType = typeof(String);

                        foreach (DataRow row in ptable.Rows)
                        {
                            dtDataTypeFix.ImportRow(row);
                        }

                        (new GlobalModel()).SaveDataTableDB(dtDataTypeFix, "[dbo].spFlashImportDSMT_type", new List<Dictionary<string, string>>
                            {
                                 new Dictionary<string, string>
                                    {
                                        { "Name", "@SOEIDUploader" },
                                        { "Value", GlobalUtil.GetSOEID() }
                                    }
                            }
                        );
                        DeleteFilesInFolders("/DSMT");
                        break;
                    case "GDW":
                        fullPathSaved = GlobalUtil.FileSaveAs("GDW", objFile.Filename, objFile.InputStream);
                        ptable = GlobalUtil.GetDataTableFromExcel(fullPathSaved, "Detail Report");
                        ptable.Rows[0].Delete();
                        ptable.Rows[1].Delete();
                        ptable.Rows[2].Delete();
                        ptable.AcceptChanges();
                        (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].spFlashImportFTE_type", new List<Dictionary<string, string>>
                            {
                                 new Dictionary<string, string>
                                    {
                                        { "Name", "@SOEIDUploader" },
                                        { "Value", GlobalUtil.GetSOEID() }
                                    }
                            }
                        );
                        DeleteFilesInFolders("/GDW");
                        break;
                    case "SOW":
                        fullPathSaved = GlobalUtil.FileSaveAs("GDW", objFile.Filename, objFile.InputStream);
                        ptable = GlobalUtil.GetDataTableFromExcel(fullPathSaved, "Detail Report");
                        ptable.Rows[0].Delete();
                        ptable.Rows[1].Delete();
                        ptable.Rows[2].Delete();
                        ptable.AcceptChanges();
                        (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].spFlashImportNE_type", new List<Dictionary<string, string>>
                            {
                                 new Dictionary<string, string>
                                    {
                                        { "Name", "@SOEIDUploader" },
                                        { "Value", GlobalUtil.GetSOEID() }
                                    }
                            }
                        );
                        DeleteFilesInFolders("/GDW");
                        break;
                    case "PEARL":
                        fullPathSaved = GlobalUtil.FileSaveAs("PEARL", objFile.Filename, objFile.InputStream);
                        ptable = GlobalUtil.GetDataTableFromExcel(fullPathSaved, "Sheet1");

                        //Fix Double DataType in Column that must be String
                        DataTable dtDataTypeFixPearl = ptable.Clone();
                        dtDataTypeFixPearl.Columns["Base USD Amount"].DataType = typeof(String);
                        foreach (DataRow row in ptable.Rows)
                        {
                            dtDataTypeFixPearl.ImportRow(row);
                        }

                        (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].spFlashImportPEARL_type", new List<Dictionary<string, string>>
                        {
                            new Dictionary<string, string>
                               {
                                   { "Name", "@SOEIDUploader" },
                                   { "Value", GlobalUtil.GetSOEID() }
                               }
                        }
                        );
                        DeleteFilesInFolders("/PEARL");
                        break;
                    case "ELR":
                        fullPathSaved = GlobalUtil.FileSaveAs("ELR", objFile.Filename, objFile.InputStream);
                        ptable = GlobalUtil.GetDataTableFromExcel(fullPathSaved, "Expense Ledger Monthly Detail");
                        ptable.Rows[0].Delete();
                        ptable.Rows[1].Delete();
                        ptable.Rows[2].Delete();
                        ptable.Rows[3].Delete();
                        //ptable.Rows.RemoveAt(ptable.Rows.Count - 1);
                        ptable.AcceptChanges();
                        (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].spFlashImportP2pELR_Type", new List<Dictionary<string, string>>
                        {
                            new Dictionary<string, string>
                               {
                                   { "Name", "@SOEIDUploader" },
                                   { "Value", GlobalUtil.GetSOEID() }
                               }
                        }
                        );
                        DeleteFilesInFolders("/ELR");
                        break;
                    case "COACUST":
                        fullPathSaved = GlobalUtil.FileSaveAs("COACUST", objFile.Filename, objFile.InputStream);
                        ptable = GlobalUtil.GetDataTableFromExcel(fullPathSaved, "P2P translator");
                        ptable.Rows[0].Delete();
                        ptable.Rows[1].Delete();
                        //ptable.Rows[2].Delete();
                        //ptable.Columns.RemoveAt(0);
                        //ptable.Columns.RemoveAt(25);
                        ptable.AcceptChanges();
                        (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].spFlashImportCOACustomize_type", new List<Dictionary<string, string>>
                        {
                            new Dictionary<string, string>
                               {
                                   { "Name", "@SOEIDUploader" },
                                   { "Value", GlobalUtil.GetSOEID() }
                               }
                        }
                        );
                        DeleteFilesInFolders("/COACUST");
                        break;
                    case "APSCUSTOMER":
                        fullPathSaved = GlobalUtil.FileSaveAs("APSCUSTOMER", objFile.Filename, objFile.InputStream);
                        ptable = GlobalUtil.GetDataTableFromExcel(fullPathSaved, "Sheet1");
                        (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].spFlashImportCustomerAPS", new List<Dictionary<string, string>>
                        {
                            new Dictionary<string, string>
                               {
                                   { "Name", "@SOEIDUploader" },
                                   { "Value", GlobalUtil.GetSOEID() }
                               }
                        }
                        );
                        DeleteFilesInFolders("/APSCUSTOMER");
                        break;
                    case "APSSERVICE":
                        fullPathSaved = GlobalUtil.FileSaveAs("APSSERVICE", objFile.Filename, objFile.InputStream);
                        ptable = GlobalUtil.GetDataTableFromExcel(fullPathSaved, "Sheet1");
                        (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].spFlashImportServiceAPS", new List<Dictionary<string, string>>
                        {
                            new Dictionary<string, string>
                               {
                                   { "Name", "@SOEIDUploader" },
                                   { "Value", GlobalUtil.GetSOEID() }
                               }
                        }
                        );
                        DeleteFilesInFolders("/APSSERVICE");
                        break;
                    case "PearlActuals":
                        fullPathSaved = GlobalUtil.FileSaveAs("PearlActuals", objFile.Filename, objFile.InputStream);
                        ptable = GlobalUtil.GetDataTableFromExcel(fullPathSaved, "Sheet1");
                        DataTable dtDataTypeFixPearlActuals = ptable.Clone();
                        dtDataTypeFixPearlActuals.Columns["Base USD Amount"].DataType = typeof(String);
                        foreach (DataRow row in ptable.Rows)
                        {
                            //decimal amount;
                            //decimal.TryParse("6.06657e+006", NumberStyles.Any, CultureInfo.InvariantCulture, out amount);
                            //row["Base USD Amount"] = Decimal.Parse(row["Base USD Amount"].ToString(), System.Globalization.NumberStyles.Float);
                            dtDataTypeFixPearlActuals.ImportRow(row);


                        }
                        (new GlobalModel()).SaveDataTableDB(dtDataTypeFixPearlActuals, "[dbo].spFlashImportPEARLACTUALS_type", new List<Dictionary<string, string>>
                        {
                            new Dictionary<string, string>
                               {
                                   { "Name", "@SOEIDUploader" },
                                   { "Value", GlobalUtil.GetSOEID() }
                               }
                        }
                        );
                        DeleteFilesInFolders("/PearlActuals");
                        break;
                    case "PearlMapping":
                        fullPathSaved = GlobalUtil.FileSaveAs("PearlMapping", objFile.Filename, objFile.InputStream);
                        ptable = GlobalUtil.GetDataTableFromExcel(fullPathSaved, "Pearl Mapping");
                        (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].spFlashImportPEARLMAPPING_type", new List<Dictionary<string, string>>
                        {
                            new Dictionary<string, string>
                               {
                                   { "Name", "@SOEIDUploader" },
                                   { "Value", GlobalUtil.GetSOEID() }
                               }
                        }
                        );
                        DeleteFilesInFolders("/PearlMapping");
                        break;
                    case "BAW":
                        fullPathSaved = GlobalUtil.FileSaveAs("BAW", objFile.Filename, objFile.InputStream);
                        ptable = GlobalUtil.GetDataTableFromExcel(fullPathSaved, "P2P GL Journal Entries With Inv");
                        ptable.Rows[0].Delete();
                        ptable.Rows[1].Delete();
                        ptable.AcceptChanges();

                        DataTable dtDataTypeFixBawReport = ptable.Clone();
                        dtDataTypeFixBawReport.Columns[1].DataType = typeof(String);
                        foreach (DataRow row in ptable.Rows)
                        {
                            //decimal amount;
                            //decimal.TryParse("6.06657e+006", NumberStyles.Any, CultureInfo.InvariantCulture, out amount);
                            //row["Base USD Amount"] = Decimal.Parse(row["Base USD Amount"].ToString(), System.Globalization.NumberStyles.Float);
                            dtDataTypeFixBawReport.ImportRow(row);


                        }

                        (new GlobalModel()).SaveDataTableDB(dtDataTypeFixBawReport, "[dbo].spFlashImportBAW_type", new List<Dictionary<string, string>>
                        {
                            new Dictionary<string, string>
                               {
                                   { "Name", "@SOEIDUploader" },
                                   { "Value", GlobalUtil.GetSOEID() }
                               }
                        }
                        );
                        DeleteFilesInFolders("/Baw");
                        break;
                    case "ExpenseCodeMapping":
                        fullPathSaved = GlobalUtil.FileSaveAs("ExpenseCodeMapping", objFile.Filename, objFile.InputStream);
                        ptable = GlobalUtil.GetDataTableFromExcel(fullPathSaved, "Report");
                        ptable.AcceptChanges();
                        (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[spFlashImportExpenseCodeMappingType]", new List<Dictionary<string, string>>
                        {
                          
                        }
                        );
                        DeleteFilesInFolders("/ExpenseCodeMapping");
                        break;
                    case "PearlMonitoring":
                        fullPathSaved = GlobalUtil.FileSaveAs("PearlMonitoring", objFile.Filename, objFile.InputStream);
                        ptable = GlobalUtil.GetDataTableFromExcel(fullPathSaved, "Sheet1");
                        ptable.Rows[0].Delete();
                        ptable.Rows[1].Delete();
                        ptable.Rows[2].Delete();
                        ptable.AcceptChanges();
                        (new GlobalModel()).SaveDataTableDB(ptable, "[dbo].[spFlashImportPEARLACTUALSMONITORING_type]", new List<Dictionary<string, string>>
                        {
                             new Dictionary<string, string>
                               {
                                   { "Name", "@SOEIDUploader" },
                                   { "Value", GlobalUtil.GetSOEID() }
                               }
                        }
                        );
                        DeleteFilesInFolders("/PearlMonitoring");
                        break;
                }
            }
            catch (Exception ex)
            {
                //return new FileUploaderResult(false, error: ex.Message);
                result["success"] = false;
                result.Add("error", ex.ToString());
            }

            // the anonymous object in the result below will be convert to json and set back to the browser
            //return new FileUploaderResult(true, new { extraInformation = 12345 });

            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }



}
